const production = !process.env.ROLLUP_WATCH;
module.exports = {
    future: {
        purgeLayersByDefault: true,
        removeDeprecatedGapUtilities: true,
    },
    plugins: [require("daisyui")],
    purge: {
        content: ["./src/**/*.svelte"],
        enabled: production, // disable purge in dev
    },
    // daisyui: {
    //   themes: [
    //     {
    //       light: {
    //         primary: "#DF5356",
    //         "primary-focus": "#DC2626",
    //         secondary: "#E6EDF5",
    //         "secondary-focus": "#E6EDF5",
    //       },
    //     },
    //   ],
    // },
    theme: {
        extend: {
            fontFamily: {},
            colors: {
                "dark-red": "#DF5356",
                "light-red": "#FDF2F2",
                "dark-blue": "#355592",
                "light-blue": "#3F8CFF",
                "lighter-blue": "#BAD8FB",
                "lighter-gray": "#E6EDF5",
                "font-black": "#0A1629",
                "gray-label": "#7D8592",
                "lightest-blue": "F4F9FD",
            },
            spacing: {
                "w-btn": "157px",
                "w-seach": "995px",
                "h-seach": "92",
                "w-search-ip": "307px",
                "30px": "30px",
                "21px": "21px",
                "22px": "88px",
                "39px": "39px",
                "32px": "32px",
                "33px": "33px",
                "23px": "23px",
                6.6: "38px",
                6.5: "1.5rem",
                7.5: "31px",
                18: "18px",
                22: "88px",
                106: "550px",
                360: "360px",
                5.5: "88px",
                450: "450px",
                100: "465px",
                300: "300px",
            },
            animation: {
                "spin-slow": "spin 3s linear infinite",
            },
        },
    },
    daisyui: {
        themes: [{
            mytheme: {
                primary: "#DF5356" /* Primary color */ ,
                "primary-focus": "#dc2626" /* Primary color - focused */ ,
                "primary-content": "#ffffff" /* Foreground content color to use on primary color */ ,

                secondary: "#E6EDF5" /* Secondary color */ ,
                "secondary-focus": "#e2e8f0" /* Secondary color - focused */ ,
                "secondary-content": "#ffffff" /* Foreground content color to use on secondary color */ ,

                accent: "#37cdbe" /* Accent color */ ,
                "accent-focus": "#2aa79b" /* Accent color - focused */ ,
                "accent-content": "#ffffff" /* Foreground content color to use on accent color */ ,

                neutral: "#3d4451" /* Neutral color */ ,
                "neutral-focus": "#2a2e37" /* Neutral color - focused */ ,
                "neutral-content": "#ffffff" /* Foreground content color to use on neutral color */ ,

                "base-100": "#ffffff" /* Base color of page, used for blank backgrounds */ ,
                "base-200": "#f9fafb" /* Base color, a little darker */ ,
                "base-300": "#d1d5db" /* Base color, even more darker */ ,
                "base-content": "#1f2937" /* Foreground content color to use on base color */ ,

                info: "#2094f3" /* Info */ ,
                success: "#009485" /* Success */ ,
                warning: "#ff9900" /* Warning */ ,
                error: "#ff5724" /* Error */ ,
            },
        }, ],
    },
};