import { API_BASE_URL, CUSTOMER_BASE_URL } from '../config';
import { deleteCookie, getCookie, setCookie } from '../utils/cookie';
import { userToken } from '../stores';

import { toastAlert } from '../utils/toaster';

export const fetchApi = async (method, url, data) => {
  let res;
  let bearer = 'Bearer ' + getCookie('cookie') || '';

  res = await fetch(`${API_BASE_URL}/${url}`, {
    method: method,
    body:
      method === 'POST' || method === 'PUT' || method === 'PATCH' ? data : null,
    headers: {
      Authorization: bearer,
      'Content-Type': 'application/json',
    },
  });
  if (
    res.status === 401 &&
    !url.includes('login') &&
    !url.includes('adminRefreshToken')
  ) {
    // login with refresh_token, else logout
    const prevRes = await reLogin(method, url, data); // handle prev request and not show prev request error message
    return prevRes;
  } else if (res.status >= 300) {
    // TODO: Handle 500 error
    // TODO: Integrate sentry or 3rd party for err handling
    // const json = await res.json();
    // json could be object of string array - toastAlert
    //   {
    //     "name": [
    //         "This field may not be blank."
    //     ],
    //     "name_masked": [
    //         "This field may not be blank."
    //     ],
    //     "status": [
    //         "Must be a valid boolean."
    //     ]
    //   }
  }
  return res;
};

export const reLogin = async (method, url, data) => {
  // relogin
  const res = await fetchApi(
    'POST',
    'auth/getRefreshToken',
    JSON.stringify({
      refresh_token: getCookie('refresh_token') || '',
    }),
  );
  const json = await res.json();
  if (res.status >= 300) {
    useLogout();
  } else {
    console.log('util relogin res:', json);
    // handle token
    userToken.set(json.access_token);
    setCookie('cookie', json.access_token);
    setCookie('refresh_token', json.refresh_token);
    // toastAlert('info', 'Login success');
    return fetchApi(method, url, data);
  }
};

export const useLogout = (hideToast) => {
  userToken.set(null);
  if (!hideToast) {
    toastAlert('error', 'Logging out.');
  }
  deleteCookie('cookie');
  window.location.pathname = '/';
};

export const uploadAPI = async (method, url, fileKey = 'image', file) => {
  // https://stackoverflow.com/questions/35192841/how-do-i-post-with-multipart-form-data-using-fetch
  let res;
  let bearer = 'Bearer ' + getCookie('cookie') || '';

  const headers = new Headers({
    Authorization: bearer,
  });

  let fd = new FormData();
  fd.append(fileKey, file);

  switch (method) {
    case 'POST':
      res = await fetch(`${CUSTOMER_BASE_URL}/${url}`, {
        method: method,
        body: fd,
        headers: headers,
      });
      return res;
    case 'PUT':
      res = await fetch(`${CUSTOMER_BASE_URL}/${url}`, {
        method: method,
        body: fd,
        headers: headers,
      });
      return res;
    case 'PATCH':
      res = await fetch(`${CUSTOMER_BASE_URL}/${url}`, {
        method: method,
        body: fd,
        headers: headers,
      });
      return res;
    default:
      res = await fetch(`${CUSTOMER_BASE_URL}/${url}`, {
        method: method,
        body: fd,
        headers: headers,
      });
      return res;
  }
};
