import { API_BASE_URL } from '../config';
import { getCookie } from '../utils/cookie';

export const uploadAPI = async (method, url, file) => {
  // https://stackoverflow.com/questions/35192841/how-do-i-post-with-multipart-form-data-using-fetch
  let res;
  let bearer = 'Bearer ' + getCookie('cookie') || '';

  const headers = new Headers({
    Authorization: bearer,
  });

  let fd = new FormData();
  fd.append('files', file);

  switch (method) {
    case 'POST':
      res = await fetch(`${API_BASE_URL}/${url}`, {
        method: method,
        body: fd,
        headers: headers,
      });
      return res;
    case 'PUT':
      res = await fetch(`${API_BASE_URL}/${url}`, {
        method: method,
        body: fd,
        headers: headers,
      });
      return res;
    case 'PATCH':
      res = await fetch(`${API_BASE_URL}/${url}`, {
        method: method,
        body: fd,
        headers: headers,
      });
      return res;
    default:
      res = await fetch(`${API_BASE_URL}/${url}`, {
        method: method,
        body: fd,
        headers: headers,
      });
      return res;
  }
};
