export const serialize = (obj) => {
    var str = [];
    for (var p in obj)
        if (Object.prototype.hasOwnProperty.call(obj, p)) {
            str.push(
                encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]).toLowerCase(),
            );
        }
    return str.join('&');
};
export const serializeCaseSensetive = (obj) => {
    var str = [];
    for (var p in obj)
        if (Object.prototype.hasOwnProperty.call(obj, p)) {
            str.push(
                encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]),
            );
        }
    return str.join('&');
};
export const serializeAdminLeadStatus = (obj) => {
    var str = [];
    for (var p in obj)
        if (obj.hasOwnProperty(p)) {
            if (p === 'bankers' || p === 'co_broker' || p === 'lead_status') {
                obj[p].forEach((v) => {
                    str.push(encodeURIComponent(p) + '=' + encodeURIComponent(v));
                });
            } else {
                str.push(
                    encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]).toLowerCase(),
                );
            }
        }
    return str.join('&');
};