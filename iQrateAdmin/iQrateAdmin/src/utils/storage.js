export const setUserInfo = (data) => {
  return localStorage.setItem('user_info', JSON.stringify(data));
};

export const getUserInfo = () => {
  return JSON.parse(localStorage.getItem('user_info'));
};

export const setKvInfo = (data) => {
  return localStorage.setItem('kv', JSON.stringify(data));
};
