import 'toastify-js/src/toastify.css';

import Toastify from 'toastify-js';

export const toastAlert = (type, message) => {
  let style = {
    background: '#E8F4FE',
    color: '#2094F3',
  };

  switch (type) {
    case 'success':
      style = {
        background: '#E5F4F3',
        color: '#009485',
      };
      break;
    case 'info':
      style = {
        background: '#E8F4FE',
        color: '#2094F3',
      };
      break;
    case 'warning':
      style = {
        background: '#FFF5E5',
        color: '#FF9900',
      };
      break;
    case 'error':
      style = {
        background: '#FFEEE9',
        color: '#FF5724',
      };
      break;
    default:
      style = {
        background: '#E8F4FE',
        color: '#2094F3',
      };
      break;
  }

  Toastify({
    text: message || 'An unknown error has occurred!',
    duration: 5000,
    close: true,
    gravity: 'top', // `top` or `bottom`
    position: 'right', // `left`, `center` or `right`
    stopOnFocus: true, // Prevents dismissing of toast on hover
    style: style,
  }).showToast();
};
