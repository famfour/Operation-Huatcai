export const calculateTotalInterestRate = (
  equation,
  interestRate,
  bankSpread,
) => {
  let result = 0.0;

  switch (equation) {
    case '+':
      result = parseFloat(interestRate) + parseFloat(bankSpread);
      break;

    case '-':
      result = parseFloat(interestRate) - parseFloat(bankSpread);
      break;

    case '=':
      result = parseFloat(interestRate);
      break;

    default:
      break;
  }

  return Number.isNaN(result) ? (0.0).toFixed(2) : result.toFixed(2);
};
