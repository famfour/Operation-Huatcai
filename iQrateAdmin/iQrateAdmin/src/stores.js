/* eslint-disable import/no-extraneous-dependencies, import/prefer-default-export */
import { readable, writable } from 'svelte/store';
import { getCookie } from './utils/cookie';

export const userToken = writable(getCookie('cookie') || '');
export const userRefreshToken = writable(getCookie('refresh_token') || '');
export const userDetail = writable(
  JSON.parse(localStorage.getItem('user_info')) || null,
);
export const allKv = writable(JSON.parse(localStorage.getItem('kv')) || null);

export const pendingOtpEmail = writable(
  localStorage.getItem('pendingOtpEmail') || null,
);
export const pendingOtpPhone = writable(
  localStorage.getItem('pendingOtpPhone') || null,
);

export const LoanPackageKV = readable({
  status: [
    {
      value: true,
      label: 'Active',
    },
    {
      value: false,
      label: 'Inactive',
    },
  ],
});

export const userListCheckboxes = writable(
  localStorage.getItem('userChecklist') || [],
);
export const payoutListCheckboxes = writable(
  localStorage.getItem('payoutChecklist') || [],
);
export const leadtListCheckboxes = writable(
  localStorage.getItem('leadChecklist') || [],
);
