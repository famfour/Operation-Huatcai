# iqrate

## About

CRM (PDF generation, Social sharing, document upload and download)

## Getting Started

- [Figma design board](https://www.figma.com/file/wy7qYybbhNKQd1xRwgyECW/IQRATE---Mobile-Application-Hi-Fi?node-id=0%3A1)
- [Confluence](https://digitalprizm.atlassian.net/wiki/home)
- [Slack group for flutter team](https://digitalprizmworkspace.slack.com/archives/C02V0BG8RGF)

## Requirements

- The project requires flutter 2.8.1 to be installed on your system. If you want build for iOS, you will need a system with macOS.
- The state management used tool used is GetX
- Package for on device data storage used is, Hive

## Project Structure

1.  main.dart (This is the starting point of the application.)

    - There are some changes that need to be made before using the GetX package. Instead of wrapping the builder with MaterialApp(), it has to be wrapped into GetMaterialApp()
    - ResponsiveWrapper.builder is used for adding breakpoints according to the screen size of the device the app is being runned on.
    - initial route is the inital screen where the app would navigate after the splash screen
    - local and localization services is useful if we need the app in different languages. Currently, the service only includes English as the language, more can be added later on.
    - Theme includes the base colour of the app. If we make no other changes, this color will be reflected everywhere.

2.  Views

    - This inclueds all the screens(The UI of the app). We try to keep it as a stateless widget wrapping everything with Obx(()=>{}) that needs to be upadted.
      - Obx is used when we need to update the state of a widget without using a stateful widget. Wrap the widget with Obx like
        ```
        Obx(()=>Center(
        Container(
            child: Center(
                child: Text("The height/color of this container can be updated without the use of a stateful widget since it has been wrapped inside Obx"),
                    ),
                ),
            ),
        ),
        ```

3.  Widgets

    - This includes all the widgets that are being used inside the UI. We keep the widgets seperated from the UI in order to make it reusable throughout the app and hence making it easier to update the widgets by updating the code in a single place.

4.  Utils

    - This folder contains functions for connection check (whether it is on mobile data or wifi) and also which device is being used.

5.  Store

    - This folder contains hive storage files. As stated earlier, Hive is used for storing data like bearer tokens and other similar details to local storage.

6.  Service

    - This folder contains everything related to the API services.
      - url.dart contains all the urls which are used throughout the app. The base url is the one with the domain followed by the API endpoints
      - core_services.dart contains get and post functions for the APIs. There are 5 functions as of now:
        1. getwithauth(){}
        2. postwithauth(){}
        3. getwithoutauth(){}
        4. postwithoutauth(){}
        5. postfilewithauth(){}
      - The get and post with auth functions require bearer tokens to complete the request which we will be getting from the hive strings.
      - The get and post without auth does not have any requirements other than the API end points and the data that needs to be passed.
      - The post file with auth function requires the bearer token and multipart for uploading files.
      - api_response_getter.dart contains functions which checks if the API is successfully completed, if the API has sent any response or an error, etc.

7.  Router

    - This folder contains files which are related to navigation between different views.
      - nav_router.dart contains the routes generated.
      - In the route_constants.dart we define names of the routes and the corresponding pages.

8.  Models

    - Response Models
      - This contains files with models of the API response. Test the APIs from Postman and copy the response, go to [quicktype.io](https://app.quicktype.io), generate the model in dart format and keep creating new model files for every API. Try to not make any field required and use null safety whereever possible.
    - Send Models
      - This contains files with the send models. An example would be creating a model for login/signup. We create the model and map the variables according to the API and use it with the postwithauth(){} function. and map the response with the reponse model.

9.  DeviceManager

    - assets.dart file is used for defining the assets that are being used throughout the app.
    - colors.dart file is used for defining color constants as well as gradients that are being used throughout the app
    - container_styles.dart is used for defining the different container styles
    - date_time.dart uses the intl package for parsing date time. The intl package can be used to parse differnt currencies and number formats along with dates and time.
    - device_manager.dart
    - text_styles.dart defines text styles for different text formats.

10. Controller
    - Contains files for functions and variables for using it in views and widgets. It acts as a binding between the views, models and widgets.
    - For defining variables that need to be updated, we use .obs eg. int test = 0.obs. These varibales can be updated by updating the value. We access these variables by manipulating the value, eg. test.value++
    - The controllers are all Get controllers defined in the GetX package.
    - For accessing the controllers, we need to define it as a varibale:
      - TestController testController = Get.put(TestController());
    - If a controller is already defined previously, we can simply access it by:
      - TestController testController = Get.find();
