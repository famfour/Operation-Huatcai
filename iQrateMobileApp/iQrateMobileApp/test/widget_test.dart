// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:iqrate/Router/nav_router.dart';
import 'package:iqrate/Views/dashboard_view2.dart';
import 'package:iqrate/Views/email_verification.dart';

import 'package:iqrate/Widgets/require_text_field.dart';

void main() {
  // SignInController signInController = Get.put(SignInController());
  testWidgets('Require text field smoke test', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    TextEditingController textEditingController = TextEditingController();
    await tester.pumpWidget(MaterialApp(
        home: Scaffold(
      body: Column(
        children: [
          RequireTextField(type: Type.email, controller: textEditingController),
        ],
      ),
    )));

    var textField = find.byType(TextFormField);
    expect(textField, findsOneWidget);
    await tester.enterText(textField, "rishabh");
  });

  testWidgets("Should throw error if widget do not match",
      (WidgetTester tester) async {
    WidgetController.hitTestWarningShouldBeFatal = true;
    // EmailVerificationController emailVerificationController =
    //     Get.put(EmailVerificationController());

    await tester.pumpWidget(
      GetMaterialApp(
        home: const EmailVerification(),
        getPages: NavRouter.generateRoute,
      ),
    );
  });

  //============= dashboard 2 view screen test===================

  testWidgets(
      "Should throw error if widget do not match for dashboard 2 view screen",
      (WidgetTester tester) async {
    WidgetController.hitTestWarningShouldBeFatal = true;
    // DashboardController2 controller = Get.put(DashboardController2());
    await tester.pumpWidget(const GetMaterialApp(home: DashboardView2()));
    expect(find.byType(Text), findsWidgets);
    expect(find.byType(ListView), findsOneWidget);
  });
}
