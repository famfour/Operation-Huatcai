import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:iqrate/Widgets/lead_list_appbar_options.dart';

void main() {
  testWidgets('Widgtes present in the appBar', (WidgetTester tester) async {
    await tester.pumpWidget(
        const MaterialApp(home: LeadsAppBarOptions(windowWidth: 400)));

    final titleFinder = find.text('Lead List');
    expect(titleFinder, findsOneWidget);

    final icons = find.byType(Icon);
    expect(icons, findsWidgets);

    final space = find.byType(SizedBox);
    expect(space, findsWidgets);
  });
}
