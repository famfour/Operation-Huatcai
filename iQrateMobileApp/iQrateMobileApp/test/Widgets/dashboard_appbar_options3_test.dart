import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:iqrate/Widgets/dashboard_appbar_options3.dart';

void main() {
  testWidgets('Widgtes present in the appBar', (WidgetTester tester) async {
    await tester.pumpWidget(
        const MaterialApp(home: DashboardAppBarOptions3(windowWidth: 400)));

    final titleFinder = find.text('Loan Package');
    expect(titleFinder, findsOneWidget);

    final icon = find.byType(Icon);
    expect(icon, findsOneWidget);

    final svgIcon = find.byType(SvgPicture);
    expect(svgIcon, findsWidgets);
  });
}
