import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';

import 'package:iqrate/Controller/new_password_controller.dart';
import 'package:iqrate/Views/new_password_screen.dart';
import 'package:iqrate/Widgets/new_to_iqrate.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';

void main() {
  testWidgets(
      "Should throw error if widget not matched for forget new password screen",
      (WidgetTester tester) async {
    WidgetController.hitTestWarningShouldBeFatal = true;

    NewPasswordController newPasswordController =
        Get.put(NewPasswordController());

    await tester.pumpWidget(MaterialApp(home: NewPasswordScreen()));

    var textField = find.byType(RequireTextField);
    expect(textField, findsWidgets);

    var passwordField = find.byKey(const Key("password"));
    expect(passwordField, findsOneWidget);
    await tester.enterText(passwordField, "pass#123");

    var primaryButton = find.byType(PrimaryButton);
    expect(primaryButton, findsOneWidget);

    expect(find.byType(NewToiQrate), findsOneWidget);

    expect(newPasswordController.confirmPasswordTextController.text.isNotEmpty,
        newPasswordController.confirmPasswordTextController.text.isNotEmpty);
    expect(newPasswordController.passwordTextController.text.isNotEmpty,
        newPasswordController.passwordTextController.text.isNotEmpty);
  });
}
