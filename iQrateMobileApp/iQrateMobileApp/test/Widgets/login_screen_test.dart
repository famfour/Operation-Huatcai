import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/login_controller.dart';
import 'package:iqrate/Views/login_screen.dart';
import 'package:iqrate/Widgets/forget_password.dart';
import 'package:iqrate/Widgets/google_signin_button.dart';
import 'package:iqrate/Widgets/new_to_iqrate.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';

void main() {
  testWidgets("Should throw error if widget not matched for login screen",
      (WidgetTester tester) async {
    WidgetController.hitTestWarningShouldBeFatal = true;

    SignInController signInController = Get.put(SignInController());

    await tester.pumpWidget(MaterialApp(home: LoginScreen()));

    var textField = find.byType(RequireTextField);
    expect(textField, findsWidgets);

    var passwordField = find.byKey(const Key("password"));
    expect(passwordField, findsOneWidget);
    await tester.enterText(passwordField, "test");

    var primaryButton = find.byType(PrimaryButton);
    expect(primaryButton, findsOneWidget);

    var forgetPassword = find.byType(ForgetPassword);
    expect(forgetPassword, findsOneWidget);

    expect(find.byType(NewToiQrate), findsOneWidget);
    expect(find.byType(GoogleSignIn), findsOneWidget);

    expect(signInController.emailTextController.text.isNotEmpty,
        signInController.emailTextController.text.isNotEmpty);
    expect(signInController.passwordTextController.text.isNotEmpty,
        signInController.passwordTextController.text.isNotEmpty);
  });
}
