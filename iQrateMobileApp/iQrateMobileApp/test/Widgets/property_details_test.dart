import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/property_details_controller.dart';
// import 'package:iqrate/Controller/sign_up_controller.dart';
import 'package:iqrate/Router/nav_router.dart';
// import 'package:iqrate/Views/sign_up.dart';
// import 'package:iqrate/Widgets/google_signin_button.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';

void main() {
  testWidgets(
      "Should throw error if widget not matched for Property details screen",
      (WidgetTester tester) async {
    WidgetController.hitTestWarningShouldBeFatal = true;

    PropertyDetailsController controller = Get.put(PropertyDetailsController());

    await tester.pumpWidget(
      GetMaterialApp(
        // home:  PropertyDetailsView(data: ,),
        getPages: NavRouter.generateRoute,
      ),
    );

    var textField = find.byType(RequireTextField);
    expect(textField, findsWidgets);

    var priceField = find.byKey(const Key("Price"));
    expect(priceField, findsOneWidget);
    await tester.enterText(priceField, "123");

    var fullCountryField = find.byKey(const Key("Country"));
    expect(fullCountryField, findsOneWidget);
    await tester.enterText(fullCountryField, "India");

    var postalField = find.byKey(const Key("Postal"));
    expect(postalField, findsOneWidget);
    await tester.enterText(postalField, "1207");

    var streetField = find.byKey(const Key("Street"));
    expect(streetField, findsOneWidget);
    await tester.enterText(streetField, "kolkata");

    var unitField = find.byKey(const Key("Unit"));
    expect(unitField, findsOneWidget);
    await tester.enterText(unitField, "123");

    var primaryButton = find.byType(PrimaryButton);
    expect(primaryButton, findsOneWidget);

    expect(controller.propertyPurchasePriceController.text.isNotEmpty,
        controller.propertyPurchasePriceController.text.isNotEmpty);

    expect(controller.countryController.text.isNotEmpty,
        controller.countryController.text.isNotEmpty);

    expect(controller.postCodeController.text.isNotEmpty,
        controller.postCodeController.text.isNotEmpty);

    expect(controller.streetNameController.text.isNotEmpty,
        controller.streetNameController.text.isNotEmpty);

    expect(controller.unitNoController.text.isNotEmpty,
        controller.unitNoController.text.isNotEmpty);
  });
}
