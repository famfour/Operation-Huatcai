import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:iqrate/Views/lead_list_screen.dart';

void main() {
  testWidgets("Should throw error if widget do not match for Lead list screen",
      (WidgetTester tester) async {
    WidgetController.hitTestWarningShouldBeFatal = true;

    // LeadsViewController leadsViewController = Get.put(LeadsViewController());

    await tester.pumpWidget(const GetMaterialApp(home: LeadListScreen()));

    expect(find.byKey(const Key("leadStatusDropdown")), findsOneWidget);
    expect(find.byKey(const Key("leadList")), findsOneWidget);
  });
}
