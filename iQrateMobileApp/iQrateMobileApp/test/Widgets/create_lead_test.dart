import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/leads_view_controller.dart';
import 'package:iqrate/Views/create_lead_screen.dart';
// import 'package:iqrate/Controller/leads_view_controller.dart';
// import 'package:iqrate/Views/leads_view.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';
import 'package:iqrate/Widgets/secondary_button.dart';
// import 'package:iqrate/Widgets/secondary_button.dart';

void main() {
  testWidgets(
      "Should throw error if widget do not match for create Lead screen",
      (WidgetTester tester) async {
    WidgetController.hitTestWarningShouldBeFatal = true;

    LeadsViewController leadsViewController = Get.put(LeadsViewController());

    await tester.pumpWidget(const GetMaterialApp(home: CreateLeadScreen()));

    expect(find.byType(SecondaryButton), findsWidgets);

    var primaryButton = find.byType(PrimaryButton);
    expect(primaryButton, findsWidgets);

    var textField = find.byType(RequireTextField);
    expect(textField, findsWidgets);

    expect(find.byKey(const Key("name")), findsOneWidget);
    expect(find.byKey(const Key("phone")), findsOneWidget);
    expect(find.byKey(const Key("email")), findsOneWidget);

    expect(leadsViewController.nameController.text.isNotEmpty,
        leadsViewController.nameController.text.isNotEmpty);
    expect(leadsViewController.emailController.text.isNotEmpty,
        leadsViewController.emailController.text.isNotEmpty);
    expect(leadsViewController.phoneController.text.isNotEmpty,
        leadsViewController.phoneController.text.isNotEmpty);
  });
}
