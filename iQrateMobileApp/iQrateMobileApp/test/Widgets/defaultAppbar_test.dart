// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:iqrate/Widgets/default_appbar.dart';

void main() {
  testWidgets('Should throw error if widget donot match',
      (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
        home: DefaultAppBar(
      title: 'Demo title',
      windowHeight: 76,
    )));

    var textWidget = find.byType(Text);
    expect(textWidget, findsOneWidget);

    var svg = find.byType(SvgPicture);
    expect(svg, findsOneWidget);
  });
}
