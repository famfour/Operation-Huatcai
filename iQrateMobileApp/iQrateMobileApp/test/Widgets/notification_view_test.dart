// import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:iqrate/Views/notifications_view.dart';
import 'package:iqrate/Widgets/default_appbar.dart';

void main() {
  testWidgets('Should throw error if widget donot match',
      (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(home: NotificationsView()));

    var appBar = find.byType(DefaultAppBar);
    expect(appBar, findsOneWidget);

    var list = find.byType(ListView);
    expect(list, findsOneWidget);

    var card = find.byType(NotificationCard);
    expect(card, findsWidgets);
  });
}
