import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:iqrate/Views/rates_view.dart';

void main() {
  testWidgets("Should throw error if widget do not match for rates view screen",
      (WidgetTester tester) async {
    await tester.pumpWidget(const GetMaterialApp(home: RatesView()));

    var cardsList = find.byType(CardsList);
    expect(cardsList, findsOneWidget);
  });

  testWidgets('Should throw error if widget not found',
      (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(home: CardsList()));

    var listViewBuilder = find.byType(ListView);
    expect(listViewBuilder, findsOneWidget);

    /*var cards = find.byType(LoanPackageCard);
    expect(cards, findsWidgets);*/
  });
}
