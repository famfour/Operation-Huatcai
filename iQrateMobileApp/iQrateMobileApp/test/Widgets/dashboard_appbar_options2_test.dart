import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:iqrate/Widgets/dashbaord_appbar_options2.dart';
import 'package:iqrate/Widgets/primary_button.dart';

void main() {
  testWidgets('Widgtes present in the appBar', (WidgetTester tester) async {
    await tester.pumpWidget(
        const MaterialApp(home: DashboardAppBarOptions2(windowWidth: 400)));

    final titleFinder = find.text('Total estimated Payout');
    expect(titleFinder, findsOneWidget);

    final icon = find.byType(Icon);
    expect(icon, findsOneWidget);

    final svgIcon = find.byType(SvgPicture);
    expect(svgIcon, findsWidgets);

    final primaryButton = find.byType(PrimaryButton);
    expect(primaryButton, findsOneWidget);
  });
}
