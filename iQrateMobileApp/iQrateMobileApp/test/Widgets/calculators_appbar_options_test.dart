import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:iqrate/Widgets/calculators_appbar_options.dart';

void main() {
  testWidgets('Widgtes present in the appBar', (WidgetTester tester) async {
    await tester.pumpWidget(
        const MaterialApp(home: CalculatorsAppBarOptions(windowWidth: 400)));

    final titleFinder = find.text('Calculator');
    expect(titleFinder, findsOneWidget);

    final icons = find.byType(Icon);
    expect(icons, findsOneWidget);
  });
}
