import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:iqrate/Views/authenticator_otp.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';

void main() {
  testWidgets(
      "Should throw error if widget do not match for authenticator otp screen",
      (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(home: AuthenticatorOtp()));

    var continueButton = find.byType(PrimaryButton);
    expect(continueButton, findsOneWidget);

    var texts = find.byType(Text);
    expect(texts, findsWidgets);

    var padding = find.byType(SizedBox);
    expect(padding, findsWidgets);

    var svgAsset = find.byType(SvgPicture);
    expect(svgAsset, findsWidgets);

    var textField = find.byType(RequireTextField);
    expect(textField, findsOneWidget);
  });
}
