import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:iqrate/Views/calculators_view.dart';
import 'package:iqrate/Widgets/calculator_button.dart';

void main() {
  testWidgets(
      "Should throw error if widget do not match for calculators view screen",
      (WidgetTester tester) async {
    WidgetController.hitTestWarningShouldBeFatal = true;

    //CalculatorsViewController controller = Get.put(CalculatorsViewController());

    await tester.pumpWidget(GetMaterialApp(home: CalculatorsView()));

    var calculatorButton = find.byType(CalculatorButton);
    expect(calculatorButton, findsWidgets);

    var listview = find.byType(ListView);
    expect(listview, findsOneWidget);
  });
}
