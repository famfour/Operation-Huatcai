import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/forget_password_controller.dart';
import 'package:iqrate/Views/forget_password_screen.dart';
import 'package:iqrate/Widgets/new_to_iqrate.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';

void main() {
  testWidgets(
      "Should throw error if widget not matched for forget password screen",
      (WidgetTester tester) async {
    WidgetController.hitTestWarningShouldBeFatal = true;

    ForgetPasswordController forgetPasswordController =
        Get.put(ForgetPasswordController());

    await tester.pumpWidget(MaterialApp(home: ForgetPasswordScreen()));

    var textField = find.byType(RequireTextField);
    expect(textField, findsOneWidget);
    await tester.enterText(textField, "test");

    var primaryButton = find.byType(PrimaryButton);
    expect(primaryButton, findsOneWidget);

    expect(find.byType(NewToiQrate), findsOneWidget);
    expect(forgetPasswordController.emailTextController.text.isNotEmpty,
        forgetPasswordController.emailTextController.text.isNotEmpty);
  });
}
