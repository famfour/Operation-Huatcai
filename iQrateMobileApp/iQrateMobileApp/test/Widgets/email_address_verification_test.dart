import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/email_address_verification_controller.dart';
import 'package:iqrate/Views/email_address_verification_screen.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';

void main() {
  testWidgets(
      "Should throw error if widget not matched for email address verification screen",
      (WidgetTester tester) async {
    WidgetController.hitTestWarningShouldBeFatal = true;

    EmailAddressVerificationScreenController
        emailAddressVerificationScreenController =
        Get.put(EmailAddressVerificationScreenController());

    await tester
        .pumpWidget(const MaterialApp(home: EmailAddressVerificationScreen()));

    var textField = find.byType(RequireTextField);
    expect(textField, findsOneWidget);
    //await tester.enterText(textField, "test");

    var primaryButton = find.byType(PrimaryButton);
    expect(primaryButton, findsOneWidget);

    expect(
        emailAddressVerificationScreenController
            .emailVerificationOtpController.text.isNotEmpty,
        emailAddressVerificationScreenController
            .emailVerificationOtpController.text.isNotEmpty);
  });
}
