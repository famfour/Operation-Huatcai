import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/sign_up_controller.dart';
import 'package:iqrate/Router/nav_router.dart';
import 'package:iqrate/Views/sign_up.dart';
import 'package:iqrate/Widgets/google_signin_button.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';

void main() {
  testWidgets("Should throw error if widget not matched for sign up screen",
      (WidgetTester tester) async {
    WidgetController.hitTestWarningShouldBeFatal = true;

    SignUpController signUpController = Get.put(SignUpController());

    await tester.pumpWidget(
      GetMaterialApp(
        home: const SignUp(),
        getPages: NavRouter.generateRoute,
      ),
    );

    var textField = find.byType(RequireTextField);
    expect(textField, findsWidgets);

    var passwordField = find.byKey(const Key("password"));
    expect(passwordField, findsOneWidget);
    await tester.enterText(passwordField, "pass#123");

    var fullNameField = find.byKey(const Key("fullname"));
    expect(fullNameField, findsOneWidget);
    await tester.enterText(fullNameField, "test name");

    var emailField = find.byKey(const Key("email"));
    expect(emailField, findsOneWidget);
    await tester.enterText(emailField, "test@gmail.com");

    var phoneField = find.byKey(const Key("phone"));
    expect(phoneField, findsOneWidget);
    await tester.enterText(phoneField, "9999999999");

    expect(find.text('Form submitted'), findsNothing);

    var primaryButton = find.byType(PrimaryButton);
    expect(primaryButton, findsOneWidget);

    expect(find.byType(GoogleSignIn), findsOneWidget);

    await tester.tap(primaryButton);
    await tester.pump();

    expect(find.text("submitted"), findsNothing);

    expect(signUpController.emailController.text.isNotEmpty,
        signUpController.emailController.text.isNotEmpty);
    expect(signUpController.passwordController.text.isNotEmpty,
        signUpController.passwordController.text.isNotEmpty);
    expect(signUpController.nameController.text.isNotEmpty,
        signUpController.nameController.text.isNotEmpty);
    expect(signUpController.phoneController.text.isNotEmpty,
        signUpController.phoneController.text.isNotEmpty);
  });
}
