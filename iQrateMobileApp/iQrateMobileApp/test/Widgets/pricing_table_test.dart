import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:iqrate/Views/pricing_table_screen.dart';
import 'package:iqrate/Widgets/primary_button.dart';

void main() {
  testWidgets(
      "Should throw error if widget do not match for Pricing Table screen",
      (WidgetTester tester) async {
    WidgetController.hitTestWarningShouldBeFatal = true;

    // PricingTableScreenController pricingTableScreenController =
    //     Get.put(PricingTableScreenController());

    await tester.pumpWidget(const GetMaterialApp(home: PricingTableScreen()));

    var cupertinoSwitch = find.byType(CupertinoSwitch);
    expect(cupertinoSwitch, findsOneWidget);

    var primaryButton = find.byType(PrimaryButton);
    expect(primaryButton, findsOneWidget);
  });
}
