import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
// import 'package:iqrate/Controller/leads_view_controller.dart';
import 'package:iqrate/Views/leads_view.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/secondary_button.dart';
// import 'package:iqrate/Widgets/secondary_button.dart';

void main() {
  testWidgets("Should throw error if widget do not match for Lead list screen",
      (WidgetTester tester) async {
    WidgetController.hitTestWarningShouldBeFatal = true;

    // LeadsViewController leadsViewController = Get.put(LeadsViewController());

    await tester.pumpWidget(const GetMaterialApp(home: LeadsView()));

    expect(find.byType(SecondaryButton), findsWidgets);

    var primaryButton = find.byType(PrimaryButton);
    expect(primaryButton, findsWidgets);

    //expect(find.byType(RequireTextField), findsWidgets);
    //expect(find.byType(FormFieldTitle), findsWidgets);
  });
}
