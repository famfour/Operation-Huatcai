import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:iqrate/DeviceManager/hive_string.dart';
import 'package:iqrate/Model/send_model.dart/login_send_model.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';

void main() async {
  WidgetsFlutterBinding
      .ensureInitialized(); // This helps in using of widgets before the app has been initialized
  await Hive
      .initFlutter(); //Hive is used for storing of strings like user name and user ids etc.
  await Hive.openBox(HiveString.hiveName);
  // late Box hive = Hive.box(HiveString.hiveName);

  test("Login", () async {
    LoginSend loginSend = LoginSend(
      email: "sethia.rishabh007@gmail.com",
      password: "password",
    );
    var data = await CoreService()
        .postWithoutAuth(url: baseUrl + loginUrl, body: loginSend.toJson());
    debugPrint(data);
    // if (data == null) {
    // } else {
    //   if (data["statusCode"] == 401.toString()) {
    //     debugPrint("Invalid Credentials");
    //   } else {
    //     var result = LoginResponse.fromJson(data);
    //     if (result.status == 200) {
    //       debugPrint("Data: " + data);
    //       debugPrint("Result: " + result.toString());
    //       debugPrint("Success");
    //     } else {
    //       // Get.snackbar(StringUtils.error, StringUtils.error,
    //       //     snackPosition: SnackPosition.TOP, backgroundColor: Colors.white);
    //     }
    //   }
  });
}
