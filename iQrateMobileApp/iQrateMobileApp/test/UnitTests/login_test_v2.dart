import 'package:flutter_test/flutter_test.dart';
import 'package:iqrate/Model/send_model.dart/login_send_model.dart';
import 'package:iqrate/Provider/login_provider.dart';
import 'package:iqrate/Service/url.dart';

void main() async {
  test("Login", () async {
    LoginSend loginSend = LoginSend(
      email: "sethia.rishabh007@gmail.com",
      password: "password",
    );
    // var data = await LoginControllerV2().callAPILogin(endpoint: baseUrl + loginUrl, loginSend : loginSend);
    //LoginProvider
    var data = await LoginProvider()
        .postWithoutAuth(url: baseUrl + loginUrl, body: loginSend.toJson());
    expect(data, isNotNull);
  });
}
//flutter test test/UnitTests/login_test_v2.dart