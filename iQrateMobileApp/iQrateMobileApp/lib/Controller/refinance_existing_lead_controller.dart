import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:isolate';
import 'dart:ui';

import 'package:android_path_provider/android_path_provider.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:iqrate/Controller/leads_view_controller.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/container_styles.dart';
import 'package:iqrate/DeviceManager/screen_constants.dart';
import 'package:iqrate/Model/response_model.dart/refinance_new_lead_package_response_model_v2.dart';
import 'package:iqrate/Model/response_model.dart/refinance_report_pdf_response_model.dart';
import 'package:iqrate/Model/response_model.dart/refinance_report_response_model_v2.dart';
import 'package:iqrate/Model/send_model.dart/refinance_report_send_model.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/secondary_button.dart';
import 'package:open_filex/open_filex.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

import '../DeviceManager/hive_string.dart';
import '../Model/header_model.dart';
import '../Model/response_model.dart/bank_forms_response_model.dart';
import '../Model/response_model.dart/fetch_lead_list_response_model.dart';
import '../Model/response_model.dart/new_purchase_response_model.dart';
import '../Model/response_model.dart/refinance_response_model.dart';
import '../Model/response_model.dart/refinance_total_response_model.dart';
import '../Model/send_model.dart/refinance_send_model.dart';
import '../Model/send_model.dart/refinance_total_send_model.dart';
import '../Model/send_model.dart/bank_package_model_refinance.dart';
import '../Service/core_services.dart';
import '../Service/url.dart';
import '../Utils/config.dart';

import 'package:http/http.dart' as http;

class RefinanceExistingLeadController extends GetxController {
  var activeStep = 0.obs;

  late List<Client> leadDetails;

  late String leadId;

  var lowestPackageListModel = Fixed().obs;
  var floatingPackageListModel = Fixed().obs;

  RxBool lowestFixedPackageOneCheckbox = false.obs;
  RxBool lowestFixedPackageTwoCheckbox = false.obs;

  RxBool lowsetFloatingPackageOneCheckbox = false.obs;
  RxBool lowsetFloatingPackageTwoCheckbox = false.obs;

  List<BankPackageModel> bankPackageModelList = <BankPackageModel>[];

  var isExpand = false.obs;
  var is3Years = false.obs;
  var is200K = false.obs;

  final Box hive = Hive.box(HiveString.hiveName);

  Rx<RefinanceTotalResponseModel> resultMainApplicant =
      RefinanceTotalResponseModel().obs;

  Rx<RefinanceResponseModel> resultMainApplicantStep2 =
      RefinanceResponseModel().obs;

  Rx<RefinanceReportResponseModelV2> resultMainApplicantStep3 =
      RefinanceReportResponseModelV2().obs;

  Rx<RefinanceReportPdfResponseModel> resultMainApplicantStep4 =
      RefinanceReportPdfResponseModel().obs;

  BankFormsResponseModel bankFormsResponseModel = BankFormsResponseModel();

  @override
  void onInit() {
    setExistingBanksDropdown();
    setPropertyTypeDropdown();
    setPropertyStatusDropdown();

    leadDetails = Get.arguments;
    debugPrint(jsonEncode(leadDetails));
    leadId = leadDetails[0].lead.toString();
    debugPrint("============lead size======" + leadDetails.length.toString());

    if (leadDetails.length == 1) {
      emailControllerMainApplicant =
          TextEditingController(text: leadDetails[0].email ?? '');
      nameControllerMainApplicant =
          TextEditingController(text: leadDetails[0].name ?? '');
      countryCodeControllerMainApplicant = TextEditingController(
          text: leadDetails[0].countryCode.toString().isEmpty
              ? ''
              : "+" + leadDetails[0].countryCode.toString());

      //debugPrint("===========countryCodeControllerMainApplicant===="+leadDetails[0].countryCode.toString());

      phoneControllerMainApplicant = TextEditingController(
          text: leadDetails[0].phoneNumber.toString().isEmpty
              ? ''
              : leadDetails[0].phoneNumber.toString());
      dobControllerMainApplicant = TextEditingController(
          text: leadDetails[0].dob == null
              ? ''
              : AppConfig.getDateFormat(leadDetails[0].dob));

      annualIncomeControllerMainApplicant = TextEditingController(
          text: leadDetails[0].annualIncome == null
              ? "0"
              : leadDetails[0].annualIncome.toString().replaceAll(".0", ""));
    } else if (leadDetails.length == 2) {
      annualIncomeControllerJointApplicant1 = TextEditingController(
          text: leadDetails[1].annualIncome == null
              ? "0"
              : leadDetails[1].annualIncome.toString().replaceAll(".0", ""));

      emailControllerMainApplicant =
          TextEditingController(text: leadDetails[0].email ?? '');
      nameControllerMainApplicant =
          TextEditingController(text: leadDetails[0].name ?? '');
      countryCodeControllerMainApplicant = TextEditingController(
          text: leadDetails[0].countryCode.toString().isEmpty
              ? ''
              : "+" + leadDetails[0].countryCode.toString());
      phoneControllerMainApplicant = TextEditingController(
          text: leadDetails[0].phoneNumber.toString().isEmpty
              ? ''
              : leadDetails[0].phoneNumber.toString());
      dobControllerMainApplicant = TextEditingController(
          text: leadDetails[0].dob == null
              ? ''
              : AppConfig.getDateFormat(leadDetails[0].dob));

      emailControllerJointApplicant1 =
          TextEditingController(text: leadDetails[1].email ?? '');
      nameControllerJointApplicant1 =
          TextEditingController(text: leadDetails[1].name ?? '');

      countryCodeControllerJointApplicant1 = TextEditingController(
          text: leadDetails[1].countryCode.toString().isEmpty
              ? ''
              : leadDetails[1].countryCode.toString());
      phoneControllerJointApplicant1 = TextEditingController(
          text: leadDetails[1].phoneNumber.toString().isEmpty
              ? ''
              : leadDetails[1].phoneNumber.toString());
      dobControllerJointApplicant1 = TextEditingController(
          text: leadDetails[1].dob == null
              ? ''
              : AppConfig.getDateFormat(leadDetails[1].dob));

      annualIncomeControllerJointApplicant1 = TextEditingController(
          text: leadDetails[1].annualIncome == null
              ? "0"
              : leadDetails[1].annualIncome.toString().replaceAll(".0", ""));
    } else if (leadDetails.length == 3) {
      emailControllerMainApplicant =
          TextEditingController(text: leadDetails[0].email ?? '');
      nameControllerMainApplicant =
          TextEditingController(text: leadDetails[0].name ?? '');
      countryCodeControllerMainApplicant = TextEditingController(
          text: leadDetails[0].countryCode.toString().isEmpty
              ? ''
              : "+" + leadDetails[0].countryCode.toString());
      phoneControllerMainApplicant = TextEditingController(
          text: leadDetails[0].phoneNumber.toString().isEmpty
              ? ''
              : leadDetails[0].phoneNumber.toString());
      dobControllerMainApplicant =
          TextEditingController(text: leadDetails[0].dob ?? '');

      annualIncomeControllerMainApplicant = TextEditingController(
          text: leadDetails[0].annualIncome == null
              ? "0"
              : leadDetails[0].annualIncome.toString().replaceAll(".0", ""));

      emailControllerJointApplicant1 =
          TextEditingController(text: leadDetails[1].email ?? '');
      nameControllerJointApplicant1 =
          TextEditingController(text: leadDetails[1].name ?? '');

      countryCodeControllerJointApplicant1 = TextEditingController(
          text: leadDetails[1].countryCode.toString().isEmpty
              ? ''
              : leadDetails[1].countryCode.toString());
      phoneControllerJointApplicant1 = TextEditingController(
          text: leadDetails[1].phoneNumber.toString().isEmpty
              ? ''
              : leadDetails[1].phoneNumber.toString());
      dobControllerJointApplicant1 =
          TextEditingController(text: leadDetails[1].dob ?? '');

      annualIncomeControllerJointApplicant1 = TextEditingController(
          text: leadDetails[1].annualIncome == null
              ? "0"
              : leadDetails[1].annualIncome.toString().replaceAll(".0", ""));

      emailControllerJointApplicant2 =
          TextEditingController(text: leadDetails[2].email ?? '');
      nameControllerJointApplicant2 =
          TextEditingController(text: leadDetails[2].name ?? '');
      countryCodeControllerJointApplicant2 = TextEditingController(
          text: leadDetails[2].countryCode.toString().isEmpty
              ? ''
              : leadDetails[2].countryCode.toString());
      phoneControllerJointApplicant2 = TextEditingController(
          text: leadDetails[2].phoneNumber.toString().isEmpty
              ? ''
              : leadDetails[2].phoneNumber.toString());
      dobControllerJointApplicant2 =
          TextEditingController(text: leadDetails[2].dob ?? '');

      annualIncomeControllerJointApplicant2 = TextEditingController(
          text: leadDetails[2].annualIncome == null
              ? "0"
              : leadDetails[2].annualIncome.toString().replaceAll(".0", ""));
    } else if (leadDetails.length == 4) {
      emailControllerMainApplicant =
          TextEditingController(text: leadDetails[0].email ?? '');
      nameControllerMainApplicant =
          TextEditingController(text: leadDetails[0].name ?? '');
      countryCodeControllerMainApplicant = TextEditingController(
          text: leadDetails[0].countryCode.toString().isEmpty
              ? ''
              : "+" + leadDetails[0].countryCode.toString());
      phoneControllerMainApplicant = TextEditingController(
          text: leadDetails[0].phoneNumber.toString().isEmpty
              ? ''
              : leadDetails[0].phoneNumber.toString());
      dobControllerMainApplicant =
          TextEditingController(text: leadDetails[0].dob ?? '');

      annualIncomeControllerMainApplicant = TextEditingController(
          text: leadDetails[0].annualIncome == null
              ? "0"
              : leadDetails[0].annualIncome.toString().replaceAll(".0", ""));

      emailControllerJointApplicant1 =
          TextEditingController(text: leadDetails[1].email ?? '');
      nameControllerJointApplicant1 =
          TextEditingController(text: leadDetails[1].name ?? '');

      countryCodeControllerJointApplicant1 = TextEditingController(
          text: leadDetails[1].countryCode.toString().isEmpty
              ? ''
              : leadDetails[1].countryCode.toString());
      phoneControllerJointApplicant1 = TextEditingController(
          text: leadDetails[1].phoneNumber.toString().isEmpty
              ? ''
              : leadDetails[1].phoneNumber.toString());
      dobControllerJointApplicant1 =
          TextEditingController(text: leadDetails[1].dob ?? '');

      annualIncomeControllerJointApplicant1 = TextEditingController(
          text: leadDetails[1].annualIncome == null
              ? "0"
              : leadDetails[1].annualIncome.toString().replaceAll(".0", ""));

      emailControllerJointApplicant2 =
          TextEditingController(text: leadDetails[2].email ?? '');
      nameControllerJointApplicant2 =
          TextEditingController(text: leadDetails[2].name ?? '');
      countryCodeControllerJointApplicant2 = TextEditingController(
          text: leadDetails[2].countryCode.toString().isEmpty
              ? ''
              : leadDetails[2].countryCode.toString());
      phoneControllerJointApplicant2 = TextEditingController(
          text: leadDetails[2].phoneNumber.toString().isEmpty
              ? ''
              : leadDetails[2].phoneNumber.toString());
      dobControllerJointApplicant2 =
          TextEditingController(text: leadDetails[2].dob ?? '');

      annualIncomeControllerJointApplicant2 = TextEditingController(
          text: leadDetails[2].annualIncome == null
              ? "0"
              : leadDetails[2].annualIncome.toString().replaceAll(".0", ""));

      emailControllerJointApplicant3 =
          TextEditingController(text: leadDetails[3].email ?? '');
      nameControllerJointApplicant3 =
          TextEditingController(text: leadDetails[3].name ?? '');
      countryCodeControllerJointApplicant3 = TextEditingController(
          text: leadDetails[3].countryCode.toString().isEmpty
              ? ''
              : leadDetails[3].countryCode.toString());
      phoneControllerJointApplicant3 = TextEditingController(
          text: leadDetails[3].phoneNumber.toString().isEmpty
              ? ''
              : leadDetails[3].phoneNumber.toString());
      dobControllerJointApplicant3 =
          TextEditingController(text: leadDetails[3].dob ?? '');

      annualIncomeControllerJointApplicant3 = TextEditingController(
          text: leadDetails[3].annualIncome == null
              ? "0"
              : leadDetails[3].annualIncome.toString().replaceAll(".0", ""));
    }

    monthlyFixedIncomeControllerMainApplicant =
        TextEditingController(text: '0');
    monthlyFixedIncomeControllerJointApplicant1 =
        TextEditingController(text: '0');
    monthlyFixedIncomeControllerJointApplicant2 =
        TextEditingController(text: '0');
    monthlyFixedIncomeControllerJointApplicant3 =
        TextEditingController(text: '0');

    monthlyRentalIncomeControllerMainApplicant =
        TextEditingController(text: '0');
    monthlyRentalIncomeControllerJointApplicant1 =
        TextEditingController(text: '0');
    monthlyRentalIncomeControllerJointApplicant2 =
        TextEditingController(text: '0');
    monthlyRentalIncomeControllerJointApplicant3 =
        TextEditingController(text: '0');

    getPermission();

    _prepareSaveDir();
    debugData();

    super.onInit();
  }

  setExistingBanksDropdown() async {
    try {
      var data = await CoreService()
          .getWithAuth(url: baseUrl + resourcesBankFormsUrls);
      if (data == null) {
      } else {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            setExistingBanksDropdown();
          }
        } else {
          bankFormsResponseModel = BankFormsResponseModel.fromJson(data);
          for (int i = 0; i < bankFormsResponseModel.results!.length; i++) {
            existingBanks.add(bankFormsResponseModel.results![i]);
          }
        }
      }
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  debugData() {
    if (kDebugMode) {
      nameControllerMainApplicant.text = "Shohel";
      phoneControllerMainApplicant.text = "1716920198";
      emailControllerMainApplicant.text = "shohel@gmail.com";
      dobControllerMainApplicant.text = "03-08-2001";
      monthlyFixedIncomeControllerMainApplicant.text = "46000";
      annualIncomeControllerMainApplicant.text = "100000";
      monthlyRentalIncomeControllerMainApplicant.text = "10000";
      yearOfPropertyPurchaseController.text = "2019";
      approximateValuationController.text = "15000000";
      outstandingLoanAmountController.text = "9000000";
      existingInterestRateController.text = "7";
      preferredLoanTenureController.text = "20";
      totalCashRebateController.text = "1000000";
      undisbursedLoanAmountController.text = "8000000";
      dateLoanWithCurrentBankController.text = "20-07-2020";
      dateofExpiryController.text = "20-07-2023";
    }
  }

  var isCalculated = false.obs;
  RxDouble totalMonthlyIncomeMainApplicant = 0.0.obs;

  List<String> dropdownEmploymentType = [
    'Salaried',
    'Self-Employed',
    'Unemployed (Homemaker / Retiree / Student)'
  ];

  String? selectedEmploymentTypeMainApplicant = "Salaried";
  String? selectedEmploymentTypeJointApplicant1 = "Salaried";
  String? selectedEmploymentTypeJointApplicant2 = "Salaried";
  String? selectedEmploymentTypeJointApplicant3 = "Salaried";

  late TextEditingController emailControllerMainApplicant;
  late TextEditingController emailControllerJointApplicant1;
  late TextEditingController emailControllerJointApplicant2;
  late TextEditingController emailControllerJointApplicant3;

  late TextEditingController nameControllerMainApplicant;
  late TextEditingController nameControllerJointApplicant1;
  late TextEditingController nameControllerJointApplicant2;
  late TextEditingController nameControllerJointApplicant3;

  late TextEditingController countryCodeControllerMainApplicant;
  late TextEditingController countryCodeControllerJointApplicant1;
  late TextEditingController countryCodeControllerJointApplicant2;
  late TextEditingController countryCodeControllerJointApplicant3;

  late TextEditingController phoneControllerMainApplicant;
  late TextEditingController phoneControllerJointApplicant1;
  late TextEditingController phoneControllerJointApplicant2;
  late TextEditingController phoneControllerJointApplicant3;

  late TextEditingController dobControllerMainApplicant;
  late TextEditingController dobControllerJointApplicant1;
  late TextEditingController dobControllerJointApplicant2;
  late TextEditingController dobControllerJointApplicant3;

  TextEditingController annualIncomeControllerMainApplicant =
      TextEditingController();
  TextEditingController annualIncomeControllerJointApplicant1 =
      TextEditingController();
  TextEditingController annualIncomeControllerJointApplicant2 =
      TextEditingController();
  TextEditingController annualIncomeControllerJointApplicant3 =
      TextEditingController();

  late TextEditingController monthlyFixedIncomeControllerMainApplicant;
  late TextEditingController monthlyFixedIncomeControllerJointApplicant1;
  late TextEditingController monthlyFixedIncomeControllerJointApplicant2;
  late TextEditingController monthlyFixedIncomeControllerJointApplicant3;

  late TextEditingController monthlyRentalIncomeControllerMainApplicant;
  late TextEditingController monthlyRentalIncomeControllerJointApplicant1;
  late TextEditingController monthlyRentalIncomeControllerJointApplicant2;
  late TextEditingController monthlyRentalIncomeControllerJointApplicant3;

  var outstandingLoanAmountController = TextEditingController();
  var existingInterestRateController = TextEditingController();
  var preferredLoanTenureController = TextEditingController(text: '0');
  var dateLoanWithCurrentBankController = TextEditingController();
  var dateofExpiryController = TextEditingController();
  var totalCashRebateController = TextEditingController(text: '0');
  var undisbursedLoanAmountController = TextEditingController(text: '0');

  var outstandingLoanTenureController = TextEditingController(text: '1');

  var approximateValuationController = TextEditingController();

  var yearOfPropertyPurchaseController = TextEditingController();

  var newPurchaseCalcResponseModel = NewPurchaseResponseModel().obs;

  var packagesGenerated = false;

  List<OneBankForm> existingBanks = <OneBankForm>[];
  String? selectedExistingBank;

  List<String> leadNamesForDropdown = [];
  String? selectedApplicant = '1';

  String? selectedProperties = '1';
  String? selectedHousingLoad = '1';

  List<String> dropdownLongerLoanTenure = ['No'];
  String? selectedLongerLoanTenure = "No";

  List<String> dropdownPropertyType = []; //Values for property type
  List<String> dropDownPropertyStatus = []; // Values for property status
  List<String> dropDownPropertyTypeKey = []; //Keys for propertyType
  List<String> dropDownPropertyStatusKey = []; //Keys for property Status
  String? selectedPropertyType = "HDB";
  String? selectedPropertyStatus = "Completed";
  String propertyTypeValueToPassInAPI = 'hdb'; //Value to pass in API
  String propertyStatusValueToPassInAPI =
      'completed'; //Value to pass in API property Status

  LeadsViewController leadsViewController = Get.find();

  setPropertyTypeDropdown() {
    for (var element in leadsViewController.kvStoreValues) {
      if (element.code == "property_type") {
        //debugPrint("=====kvstore====="+element.value.toString());
        element.value.forEach((key, value) {
          dropdownPropertyType.add(value);
          dropDownPropertyTypeKey.add(key);
          //debugPrint("array_key" + key );
        });
      }
    }
  }

  setPropertyStatusDropdown() {
    for (var element in leadsViewController.kvStoreValues) {
      if (element.code == "property_status") {
        log('------------');
        log('#######');
        log('------------');
        log('#######');
        log('------------');
        //debugPrint("=====kvstore====="+element.value.toString());
        element.value.forEach((key, value) {
          dropDownPropertyStatus.add(value);
          dropDownPropertyStatusKey.add(key);
          //debugPrint("array_key" + key );
        });
      }
    }
  }

  List<String> lockInPeriodDropDown = [
    'Yes',
    'No',
    'Unsure',
  ];
  String? selectedLockInPeriod = "Yes";
  List<String> cashRebateOrSubsidyDropDown = [
    'Yes',
    'No',
    'Unsure',
  ];
  String? selectedCashRebateOrSubsidy = "Yes";
  List<String> fullyDisbursedDropDown = [
    'Yes',
    'No',
    'Unsure',
  ];
  String? selectedFullyDisbursed = "Yes";

  List<String> earlyRepaymentPenaltyDropDown = [
    '0.75%',
    '1.50%',
    'Unsure',
  ];
  String? selectedRepaymentPenalty = "1.50%";
  List<String> cancellationPenaltyDropDown = [
    '0.75%',
    '1.00%',
    '1.50%',
    'Unsure',
  ];
  String? selectedCancellationPenalty = "0.75%";

  var step = 1.obs;
  var isSummeryCheckBoxEnable = true.obs;

  List nameList = [];
  List phoneList = [];
  List countryCodeList = [];
  List emailAddressList = [];
  List dobList = [];
  List employementTypeList = [];
  List monthlyFixedIncomeList = [];
  List annualIncomeList = [];
  List monthlyRentalIncomeList = [];

  addBasicDataToList() {
    for (int i = 0; i < leadDetails.length; i++) {
      nameList.add(leadDetails[i].name);
      phoneList.add(leadDetails[i].phoneNumber);
      countryCodeList.add(leadDetails[i].countryCode);
      emailAddressList.add(leadDetails[i].email);
      dobList.add(leadDetails[i].dob);
    }
  }

  void onCancel() {
    isCalculated.value = false;
    Get.back();
  }

  clearStep1ListData() {
    annualIncomeList.clear();
    monthlyFixedIncomeList.clear();
    monthlyRentalIncomeList.clear();
    employementTypeList.clear();
    dobList.clear();
    debugPrint("clearStep1ListData:: ");
  }

  clearPackageSelection() {
    lowestFixedPackageOneCheckbox.value = false;
    lowestFixedPackageTwoCheckbox.value = false;
    lowsetFloatingPackageOneCheckbox.value = false;
    lowsetFloatingPackageTwoCheckbox.value = false;
    debugPrint("clearPackageSelection:: ");
  }

  clearRefinanceSavingsCheckBox() {
    summaryCheckBox.value = false;
    savingsFromRefinancingCheckBox.value = false;
    package1CheckBox.value = false;
    package2CheckBox.value = false;
    package3CheckBox.value = false;
    package4CheckBox.value = false;
    exportFieldsCheckBoxesList.clear();
    debugPrint("clearRefinanceSavingsCheckBox:: ");
  }

  addDetailsToListForMainApplicant() {
    employementTypeList.add(selectedEmploymentTypeMainApplicant.toString());
    dobList.add(dobControllerMainApplicant.text);
    annualIncomeList.add(
      int.tryParse(
        annualIncomeControllerMainApplicant.text
            .toString()
            .replaceAll(',', '')
            .replaceAll('\$', ''),
      ),
    );
    monthlyFixedIncomeList.add(
      int.tryParse(
        monthlyFixedIncomeControllerMainApplicant.text
            .toString()
            .replaceAll(',', '')
            .replaceAll('\$', ''),
      ),
    );
    monthlyRentalIncomeList.add(
      int.tryParse(
        monthlyRentalIncomeControllerMainApplicant.text
            .toString()
            .replaceAll(',', '')
            .replaceAll('\$', ''),
      ),
    );
  }

  addDetailsToListJointApplicant1() {
    addDetailsToListForMainApplicant();

    employementTypeList.add(selectedEmploymentTypeJointApplicant1.toString());
    dobList.add(dobControllerJointApplicant1.text);

    annualIncomeList.add(
      int.tryParse(
        annualIncomeControllerJointApplicant1.text
            .toString()
            .replaceAll(',', '')
            .replaceAll('\$', ''),
      ),
    );
    monthlyFixedIncomeList.add(
      int.tryParse(monthlyFixedIncomeControllerJointApplicant1.text
          .toString()
          .replaceAll(',', '')
          .replaceAll('\$', '')),
    );
    monthlyRentalIncomeList.add(
      int.tryParse(monthlyRentalIncomeControllerJointApplicant1.text
          .toString()
          .replaceAll(',', '')
          .replaceAll('\$', '')),
    );
  }

  addDetailsToListJointApplicant2() {
    addDetailsToListJointApplicant1();

    employementTypeList.add(selectedEmploymentTypeJointApplicant2.toString());
    dobList.add(dobControllerJointApplicant2.text);

    annualIncomeList.add(
      int.tryParse(
        annualIncomeControllerJointApplicant2.text
            .toString()
            .replaceAll(',', '')
            .replaceAll('\$', ''),
      ),
    );
    monthlyFixedIncomeList.add(
      int.tryParse(
        monthlyFixedIncomeControllerJointApplicant2.text
            .toString()
            .replaceAll(',', '')
            .replaceAll('\$', ''),
      ),
    );

    monthlyRentalIncomeList.add(
      int.tryParse(
        monthlyRentalIncomeControllerJointApplicant2.text
            .toString()
            .replaceAll(',', '')
            .replaceAll('\$', ''),
      ),
    );
  }

  addDetailsToListJointApplicant3() {
    addDetailsToListJointApplicant2();

    employementTypeList.add(selectedEmploymentTypeJointApplicant3.toString());

    dobList.add(dobControllerJointApplicant3.text);

    annualIncomeList.add(
      int.tryParse(
        annualIncomeControllerJointApplicant3.text
            .toString()
            .replaceAll(',', '')
            .replaceAll('\$', ''),
      ),
    );
    monthlyFixedIncomeList.add(
      int.tryParse(
        monthlyFixedIncomeControllerJointApplicant3.text
            .toString()
            .replaceAll(',', '')
            .replaceAll('\$', ''),
      ),
    );
    monthlyRentalIncomeList.add(
      int.tryParse(
        monthlyRentalIncomeControllerJointApplicant3.text
            .toString()
            .replaceAll(',', '')
            .replaceAll('\$', ''),
      ),
    );
  }

  onTapCalculateStep1Check() {
    if (leadDetails.length == 1) {
      if (nameControllerMainApplicant.text.isNotEmpty &&
          phoneControllerMainApplicant.text.isNotEmpty &&
          countryCodeControllerMainApplicant.text.isNotEmpty &&
          emailControllerMainApplicant.text.isNotEmpty &&
          dobControllerMainApplicant.text.isNotEmpty &&
          monthlyFixedIncomeControllerMainApplicant.text.isNotEmpty &&
          annualIncomeControllerMainApplicant.text.isNotEmpty &&
          monthlyRentalIncomeControllerMainApplicant.text.isNotEmpty) {
        onTapCalculateStep1();
      } else {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "All fields are mandatory",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      }
    } else if (leadDetails.length == 2) {
      if (nameControllerMainApplicant.text.isNotEmpty &&
          phoneControllerMainApplicant.text.isNotEmpty &&
          countryCodeControllerMainApplicant.text.isNotEmpty &&
          emailControllerMainApplicant.text.isNotEmpty &&
          dobControllerMainApplicant.text.isNotEmpty &&
          monthlyFixedIncomeControllerMainApplicant.text.isNotEmpty &&
          annualIncomeControllerMainApplicant.text.isNotEmpty &&
          monthlyRentalIncomeControllerMainApplicant.text.isNotEmpty &&
          nameControllerJointApplicant1.text.isNotEmpty &&
          phoneControllerJointApplicant1.text.isNotEmpty &&
          countryCodeControllerJointApplicant1.text.isNotEmpty &&
          emailControllerJointApplicant1.text.isNotEmpty &&
          dobControllerJointApplicant1.text.isNotEmpty &&
          monthlyFixedIncomeControllerJointApplicant1.text.isNotEmpty &&
          annualIncomeControllerJointApplicant1.text.isNotEmpty &&
          monthlyRentalIncomeControllerJointApplicant1.text.isNotEmpty) {
        onTapCalculateStep1();
      } else {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "All fields are mandatory",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      }
    } else if (leadDetails.length == 3) {
      if (nameControllerMainApplicant.text.isNotEmpty &&
          phoneControllerMainApplicant.text.isNotEmpty &&
          countryCodeControllerMainApplicant.text.isNotEmpty &&
          emailControllerMainApplicant.text.isNotEmpty &&
          dobControllerMainApplicant.text.isNotEmpty &&
          monthlyFixedIncomeControllerMainApplicant.text.isNotEmpty &&
          annualIncomeControllerMainApplicant.text.isNotEmpty &&
          monthlyRentalIncomeControllerMainApplicant.text.isNotEmpty &&
          nameControllerJointApplicant1.text.isNotEmpty &&
          phoneControllerJointApplicant1.text.isNotEmpty &&
          countryCodeControllerJointApplicant1.text.isNotEmpty &&
          emailControllerJointApplicant1.text.isNotEmpty &&
          dobControllerJointApplicant1.text.isNotEmpty &&
          monthlyFixedIncomeControllerJointApplicant1.text.isNotEmpty &&
          annualIncomeControllerJointApplicant1.text.isNotEmpty &&
          monthlyRentalIncomeControllerJointApplicant1.text.isNotEmpty &&
          nameControllerJointApplicant2.text.isNotEmpty &&
          phoneControllerJointApplicant2.text.isNotEmpty &&
          countryCodeControllerJointApplicant2.text.isNotEmpty &&
          emailControllerJointApplicant2.text.isNotEmpty &&
          dobControllerJointApplicant2.text.isNotEmpty &&
          monthlyFixedIncomeControllerJointApplicant2.text.isNotEmpty &&
          annualIncomeControllerJointApplicant2.text.isNotEmpty &&
          monthlyRentalIncomeControllerJointApplicant2.text.isNotEmpty) {
        onTapCalculateStep1();
      } else {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "All fields are mandatory",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      }
    } else if (leadDetails.length == 4) {
      if (nameControllerMainApplicant.text.isNotEmpty &&
          phoneControllerMainApplicant.text.isNotEmpty &&
          countryCodeControllerMainApplicant.text.isNotEmpty &&
          emailControllerMainApplicant.text.isNotEmpty &&
          dobControllerMainApplicant.text.isNotEmpty &&
          monthlyFixedIncomeControllerMainApplicant.text.isNotEmpty &&
          annualIncomeControllerMainApplicant.text.isNotEmpty &&
          monthlyRentalIncomeControllerMainApplicant.text.isNotEmpty &&
          nameControllerJointApplicant1.text.isNotEmpty &&
          phoneControllerJointApplicant1.text.isNotEmpty &&
          countryCodeControllerJointApplicant1.text.isNotEmpty &&
          emailControllerJointApplicant1.text.isNotEmpty &&
          dobControllerJointApplicant1.text.isNotEmpty &&
          monthlyFixedIncomeControllerJointApplicant1.text.isNotEmpty &&
          annualIncomeControllerJointApplicant1.text.isNotEmpty &&
          monthlyRentalIncomeControllerJointApplicant1.text.isNotEmpty &&
          nameControllerJointApplicant2.text.isNotEmpty &&
          phoneControllerJointApplicant2.text.isNotEmpty &&
          countryCodeControllerJointApplicant2.text.isNotEmpty &&
          emailControllerJointApplicant2.text.isNotEmpty &&
          dobControllerJointApplicant2.text.isNotEmpty &&
          monthlyFixedIncomeControllerJointApplicant2.text.isNotEmpty &&
          annualIncomeControllerJointApplicant2.text.isNotEmpty &&
          monthlyRentalIncomeControllerJointApplicant2.text.isNotEmpty &&
          nameControllerJointApplicant3.text.isNotEmpty &&
          phoneControllerJointApplicant3.text.isNotEmpty &&
          countryCodeControllerJointApplicant3.text.isNotEmpty &&
          emailControllerJointApplicant3.text.isNotEmpty &&
          dobControllerJointApplicant3.text.isNotEmpty &&
          monthlyFixedIncomeControllerJointApplicant3.text.isNotEmpty &&
          annualIncomeControllerJointApplicant3.text.isNotEmpty &&
          monthlyRentalIncomeControllerJointApplicant3.text.isNotEmpty) {
        onTapCalculateStep1();
      } else {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "All fields are mandatory",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      }
    }
  }

  onTapCalculateStep1() async {
    clearStep1ListData();

    debugPrint(int.tryParse(monthlyFixedIncomeControllerMainApplicant.text)
        .toString());
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);

    if (leadDetails.length == 1) {
      addDetailsToListForMainApplicant();
    } else if (leadDetails.length == 2) {
      addDetailsToListJointApplicant1();
    } else if (leadDetails.length == 3) {
      addDetailsToListJointApplicant2();
    } else if (leadDetails.length == 4) {
      addDetailsToListJointApplicant3();
    }

    RefinanceTotalSendModel refinanceTotalSendModel = RefinanceTotalSendModel(
      noOfApplicants: leadDetails.length,
      annualIncome: annualIncomeList,
      monthlyFixedIncome: monthlyFixedIncomeList,
      monthlyRentalIncome: monthlyRentalIncomeList,
      employementType: employementTypeList,
      dob: dobList,
      token: hive.get(HiveString.token.toString()),
    );

    log('message');
    log(jsonEncode(refinanceTotalSendModel).toString());

    var data = await CoreService().postWithAuth(
      url: baseUrl + refinanceTotalUrl,
      body: refinanceTotalSendModel.toJson(),
    );

    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
      // Get.back();
      if (Get.isDialogOpen ?? false) Get.back();
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          onTapCalculateStep1();
        }
      } else {
        if (data["message"] == null) {
          isCalculated.value = true;
          resultMainApplicant.value =
              RefinanceTotalResponseModel.fromJson(data);
          if (resultMainApplicant.value.totalMonthlyIncome != null) {
            totalMonthlyIncomeMainApplicant.value =
                resultMainApplicant.value.totalMonthlyIncome!.toDouble();
          }
        } else {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: data["message"][0],
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }
      }
    }
  }

  onTapCalculateStep2Check() async {
    if (selectedLockInPeriod == "Yes") {
      if (dateofExpiryController.text.isEmpty) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "All fields are mandatory",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });

        return;
      }
    }

    if (yearOfPropertyPurchaseController.text.isEmpty ||
        approximateValuationController.text.isEmpty ||
        outstandingLoanAmountController.text.isEmpty ||
        existingInterestRateController.text.isEmpty ||
        dateLoanWithCurrentBankController.text.isEmpty ||
        totalCashRebateController.text.isEmpty ||
        undisbursedLoanAmountController.text.isEmpty ||
        selectedExistingBank == null) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "All fields are mandatory",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else {
      var currentYear = DateTime.now().year;
      if (int.tryParse(yearOfPropertyPurchaseController.text)! < 1992) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg:
                "Please enter valid year., The year of purchase must be at least 1992.",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      } else if (int.tryParse(yearOfPropertyPurchaseController.text)! >
          currentYear) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg:
                "Year of property purchase should not be greater than current year",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      } else {
        if (selectedLockInPeriod != 'Yes' &&
            (selectedLockInPeriod == 'Unsure' ||
                selectedLockInPeriod == 'No' ||
                selectedCashRebateOrSubsidy == 'Unsure' ||
                selectedFullyDisbursed == 'Unsure' ||
                selectedRepaymentPenalty == 'Unsure' ||
                selectedCancellationPenalty == 'Unsure')) {
          Get.defaultDialog(
            barrierDismissible: false,
            title: 'Attention',
            content: SizedBox(
              height: Get.height * 0.5,
              width: Get.width * 0.8,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const SizedBox(),
                  Text(
                    'The saving does not take into consideration any early repayment penalty/cash rebate clawback/cancellation penalty by your current bank.',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: AppColors.kPrimaryColor,
                      fontSize: FontSize.s18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    child: Column(
                      children: [
                        PrimaryButton(
                          windowHeight: Get.height,
                          windowWidth: Get.width,
                          buttonTitle: 'I Understand',
                          onPressed: () async {
                            Get.back();
                            onTapCalculateStep2();
                          },
                        ),
                        SizedBox(
                          height: Get.height * 0.03,
                        ),
                        SecondaryButton(
                          windowHeight: Get.height,
                          windowWidth: Get.width,
                          kGradientBoxDecoration: ContainerStyles
                              .kGradientBoxDecorationSecondaryButton,
                          kInnerDecoration:
                              ContainerStyles.kInnerDecorationSecondaryButton,
                          onPressed: () {
                            Get.back();
                          },
                          buttonTitle: 'Input the values',
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        } else {
          if (selectedLockInPeriod == "Yes") {
            if (selectedLockInPeriod == 'Unsure' ||
                selectedCashRebateOrSubsidy == 'Unsure' ||
                selectedFullyDisbursed == 'Unsure' ||
                selectedRepaymentPenalty == 'Unsure' ||
                selectedCancellationPenalty == 'Unsure') {
              Get.defaultDialog(
                barrierDismissible: false,
                title: 'Attention',
                content: SizedBox(
                  height: Get.height * 0.5,
                  width: Get.width * 0.8,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const SizedBox(),
                      Text(
                        'The saving does not take into consideration any early repayment penalty/cash rebate clawback/cancellation penalty by your current bank.',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: AppColors.kPrimaryColor,
                          fontSize: FontSize.s18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10.0),
                        child: Column(
                          children: [
                            PrimaryButton(
                              windowHeight: Get.height,
                              windowWidth: Get.width,
                              buttonTitle: 'I Understand',
                              onPressed: () async {
                                Get.back();

                                if (selectedLockInPeriod == 'Yes') {
                                  var lockInExpiry = DateTime.parse(
                                      dateofExpiryController.text
                                          .split('-')
                                          .reversed
                                          .join('-'));
                                  var todayDate = DateTime.now();
                                  var difference =
                                      lockInExpiry.difference(todayDate).inDays;
                                  debugPrint((difference.toString()));

                                  if (difference > 180) {
                                    /* Get.snackbar(
                                      'Attention',
                                      'You are advised not to refinance now as you might incur prepayment penalty from your existing bank',
                                      snackPosition: SnackPosition.TOP,
                                      backgroundColor: Colors.white,
                                      colorText: Colors.black,
                                      duration: const Duration(seconds: 3),
                                    );*/
                                    await Future.delayed(
                                      const Duration(milliseconds: 1000),
                                    );
                                    if (Get.isDialogOpen ?? false) Get.back();
                                    //onTapCalculateStep2();

                                    Get.defaultDialog(
                                      barrierDismissible: false,
                                      title: 'Attention',
                                      content: SizedBox(
                                        height: Get.height * 0.5,
                                        width: Get.width * 0.8,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            const SizedBox(),
                                            Text(
                                              'You are advised not to refinance now as you might incur prepayment penalty from your existing bank',
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: AppColors.kPrimaryColor,
                                                fontSize: FontSize.s18,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 10.0),
                                              child: Column(
                                                children: [
                                                  PrimaryButton(
                                                    windowHeight: Get.height,
                                                    windowWidth: Get.width,
                                                    buttonTitle: 'I Understand',
                                                    onPressed: () async {
                                                      Get.back();

                                                      await Future.delayed(
                                                        const Duration(
                                                            milliseconds: 1000),
                                                      );
                                                      if (Get.isDialogOpen ??
                                                          false) Get.back();
                                                      onTapCalculateStep2();
                                                    },
                                                  ),
                                                  SizedBox(
                                                    height: Get.height * 0.03,
                                                  ),
                                                  SecondaryButton(
                                                    windowHeight: Get.height,
                                                    windowWidth: Get.width,
                                                    kGradientBoxDecoration:
                                                        ContainerStyles
                                                            .kGradientBoxDecorationSecondaryButton,
                                                    kInnerDecoration:
                                                        ContainerStyles
                                                            .kInnerDecorationSecondaryButton,
                                                    onPressed: () {
                                                      Get.back();
                                                    },
                                                    buttonTitle:
                                                        'Input the values',
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    );

                                    return;
                                  }
                                }
                                onTapCalculateStep2();
                              },
                            ),
                            SizedBox(
                              height: Get.height * 0.03,
                            ),
                            SecondaryButton(
                              windowHeight: Get.height,
                              windowWidth: Get.width,
                              kGradientBoxDecoration: ContainerStyles
                                  .kGradientBoxDecorationSecondaryButton,
                              kInnerDecoration: ContainerStyles
                                  .kInnerDecorationSecondaryButton,
                              onPressed: () {
                                Get.back();
                              },
                              buttonTitle: 'Input the values',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );

              return;
            }

            var lockInExpiry = DateTime.parse(
                dateofExpiryController.text.split('-').reversed.join('-'));
            var todayDate = DateTime.now();
            var difference = lockInExpiry.difference(todayDate).inDays;
            debugPrint((difference.toString()));
            if (difference > 180) {
              Get.defaultDialog(
                barrierDismissible: false,
                title: 'Attention',
                content: SizedBox(
                  height: Get.height * 0.5,
                  width: Get.width * 0.8,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const SizedBox(),
                      Text(
                        'You are advised not to refinance now as you might incur prepayment penalty from your existing bank',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: AppColors.kPrimaryColor,
                          fontSize: FontSize.s18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10.0),
                        child: Column(
                          children: [
                            PrimaryButton(
                              windowHeight: Get.height,
                              windowWidth: Get.width,
                              buttonTitle: 'I Understand',
                              onPressed: () async {
                                Get.back();

                                /*await Future.delayed(
                                  const Duration(
                                      milliseconds: 4500),
                                );*/
                                if (Get.isDialogOpen ?? false) Get.back();
                                onTapCalculateStep2();
                              },
                            ),
                            SizedBox(
                              height: Get.height * 0.03,
                            ),
                            SecondaryButton(
                              windowHeight: Get.height,
                              windowWidth: Get.width,
                              kGradientBoxDecoration: ContainerStyles
                                  .kGradientBoxDecorationSecondaryButton,
                              kInnerDecoration: ContainerStyles
                                  .kInnerDecorationSecondaryButton,
                              onPressed: () {
                                Get.back();
                              },
                              buttonTitle: 'Go back',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            } else {
              onTapCalculateStep2();
            }
          }
        }
      }
    }
  }

  onTapCalculateStep2() async {
    debugPrint(selectedRepaymentPenalty);
    debugPrint(selectedCancellationPenalty);
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);

    RefinanceSendModel refinanceTotalSendModel = RefinanceSendModel(
      noOfApplicants: leadDetails.length,
      outstandingLoanAmount: int.tryParse(
        outstandingLoanAmountController.text
            .toString()
            .replaceAll(',', '')
            .replaceAll('\$', ''),
      ),
      earlyRepaymentPenalty: selectedLockInPeriod == "Yes"
          ? selectedRepaymentPenalty.toString() == "Unsure" ||
                  selectedRepaymentPenalty.toString() == "No" ||
                  earlyRepaymentPenaltyDropDown.toString() == 'Unsure'
              ? 0.0
              : double.tryParse(
                  selectedRepaymentPenalty
                      .toString()
                      .replaceAll('%', '')
                      .toString(),
                )
          : 0.0,
      undisbursedLoanAmount: selectedFullyDisbursed.toString() == "Unsure" ||
              selectedFullyDisbursed.toString() == "Yes"
          ? 0
          : int.tryParse(
              undisbursedLoanAmountController.text
                  .toString()
                  .replaceAll(',', '')
                  .replaceAll('\$', ''),
            ),
      cancellationPenalty: selectedFullyDisbursed == "No"
          ? selectedCancellationPenalty.toString() == "Unsure" ||
                  selectedCancellationPenalty.toString() == "Yes"
              ? 0.0
              : double.tryParse(
                  selectedCancellationPenalty.toString().replaceAll('%', ''))
          : 0.0,
    );

    var data = await CoreService().postWithAuth(
      url: baseUrl + refinanceUrl,
      body: refinanceTotalSendModel.toJson(),
    );

    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
      // Get.back();
      if (Get.isDialogOpen ?? false) Get.back();
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          onTapCalculateStep2();
        }
      } else {
        if (data["message"] == null) {
          isCalculated.value = true;
          resultMainApplicantStep2.value =
              RefinanceResponseModel.fromJson(data);
          debugPrint(resultMainApplicantStep2.value.cancellationPenaltyAmount
              .toString());
        } else {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: data["message"][0],
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }
      }
    }
  }

  getPropertyIndex() {
    //dropdownPropertyType.for

    int index = dropdownPropertyType
        .indexWhere((element) => element == selectedPropertyType);
    index++;
    debugPrint("====dropdownPropertyType====" + index.toString());
    return index;
  }

  onTapContinueToReports() async {
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);

    RefinanceReportSendModel refinanceReportSendModel =
        RefinanceReportSendModel(
      noOfApplicants: leadDetails.length,
      outstandingLoanAmount: int.tryParse(
        outstandingLoanAmountController.text
            .toString()
            .replaceAll(',', '')
            .replaceAll('\$', ''),
      ),
      outstandinLoanTenure: int.tryParse(
        outstandingLoanTenureController.text
            .toString()
            .replaceAll(',', '')
            .replaceAll('\$', ''),
      ),
      preferredLoanTenure: int.tryParse(preferredLoanTenureController.text),
      name: nameControllerMainApplicant.text,
      cashRebateSubsidy: int.tryParse(
        totalCashRebateController.text.replaceAll(',', '').replaceAll('\$', ''),
      ),
      existingBank: selectedExistingBank,
      existingInterestRate:
          double.tryParse(existingInterestRateController.text),
      approximatePropertyPrice: int.tryParse(approximateValuationController.text
          .toString()
          .replaceAll(',', '')
          .replaceAll('\$', '')),
      propertyType: getPropertyIndex(),
      yearOfPurchase: int.tryParse(yearOfPropertyPurchaseController.text),
      earlyRepaymentPenaltyAmount: resultMainApplicantStep2
          .value.earlyRepaymentPenaltyAmount!
          .round()
          .toString()
          .replaceAll(',', ''),
      cancellationPenaltyAmount: resultMainApplicantStep2
          .value.cancellationPenaltyAmount!
          .round()
          .toString()
          .replaceAll(',', ''),
      existingLoanSubsidy: int.tryParse(totalCashRebateController.text
          .toString()
          .replaceAll(',', '')
          .replaceAll('\$', '')),
      dob: dobControllerMainApplicant.text.toString(),
      loanCategory: "Refinance",
      bankDetails: bankPackageModelList,
      // propertyStatus: selectedPropertyStatus,
      propertyStatus: "completed",
      token: hive.get(HiveString.token.toString()),
    );

    log('message');
    log(jsonEncode(refinanceReportSendModel.toJson()).toString());

    var data = await CoreService().postWithAuth(
      url: baseUrl + refinanceReportUrl,
      body: refinanceReportSendModel.toJson(),
    );

    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
      // Get.back();
      if (Get.isDialogOpen ?? false) Get.back();
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          onTapContinueToReports();
        }
      } else {
        if (data["message"] == null) {
          log(jsonEncode(data));

          Get.back();

          isCalculated.value = true;

          resultMainApplicantStep3.value =
              RefinanceReportResponseModelV2.fromJson(data);
          // debugPrint(resultMainApplicantStep3.value.loanDetails!.existingBank);
        } else {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: data["message"][0],
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }
      }
    }
  }

  List exportFieldsCheckBoxesList = [];

  var summaryCheckBox = false.obs;
  var savingsFromRefinancingCheckBox = false.obs;
  var package1CheckBox = false.obs;
  var package2CheckBox = false.obs;
  var package3CheckBox = false.obs;
  var package4CheckBox = false.obs;

//!To be implemented yet.________________________________

  onTapDownloadReports() async {
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);

    RefinanceReportSendModel refinanceReportSendModel =
        RefinanceReportSendModel(
      noOfApplicants: leadDetails.length,
      outstandingLoanAmount: int.tryParse(
        outstandingLoanAmountController.text
            .toString()
            .replaceAll(',', '')
            .replaceAll('\$', ''),
      ),
      outstandinLoanTenure: int.tryParse(
        outstandingLoanTenureController.text
            .toString()
            .replaceAll(',', '')
            .replaceAll('\$', ''),
      ),
      preferredLoanTenure: int.tryParse(preferredLoanTenureController.text),
      name: nameControllerMainApplicant.text,
      existingBank: "DBS",
      existingInterestRate:
          double.tryParse(existingInterestRateController.text),
      propertyType: getPropertyIndex(),
      approximatePropertyPrice: int.tryParse(approximateValuationController.text
          .toString()
          .replaceAll(',', '')
          .replaceAll('\$', '')),
      yearOfPurchase: int.tryParse(yearOfPropertyPurchaseController.text),
      cashRebateSubsidy: int.tryParse(
        totalCashRebateController.text.replaceAll(',', '').replaceAll('\$', ''),
      ),
      earlyRepaymentPenaltyAmount: resultMainApplicantStep2
          .value.earlyRepaymentPenaltyAmount!
          .round()
          .toString()
          .replaceAll(',', ''),
      cancellationPenaltyAmount: resultMainApplicantStep2
          .value.cancellationPenaltyAmount!
          .round()
          .toString()
          .replaceAll(',', ''),
      existingLoanSubsidy: int.tryParse(totalCashRebateController.text
          .toString()
          .replaceAll(',', '')
          .replaceAll('\$', '')),
      dob: dobControllerMainApplicant.text.toString(),
      loanCategory: "Refinance",
      bankDetails: bankPackageModelList,
      exportFields: exportFieldsCheckBoxesList,
      propertyStatus: "completed",
      token: hive.get(HiveString.token.toString()),
    );

    log(refinanceReportSendModel.toJson().toString());

    HeaderModel headerModel =
        HeaderModel(authorization: "Bearer ${hive.get(HiveString.token)}");
    http.Response data = await http
        .post(Uri.parse(baseUrl + refinanceExportReportUrl),
            body: jsonEncode(refinanceReportSendModel.toJson()),
            headers: headerModel.toHeader())
        .timeout(const Duration(seconds: 200));

    log("response code::: " + data.statusCode.toString());
    log("response::: " + data.body.toString());

    if (Get.isDialogOpen ?? false) Get.back();
    if (data.body.isEmpty) {
      if (Get.isDialogOpen ?? false) Get.back();
    } else {
      if (data.body.contains('message')) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: data.body.contains('message')
                ? jsonDecode(data.body)['message']
                : "Something went wrong",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      } else {
        resultMainApplicantStep4.value =
            RefinanceReportPdfResponseModel.fromJson(jsonDecode(data.body));

        onDownloadPdfButtonTap(
          resultMainApplicantStep4.value.url.toString(),
          ("${resultMainApplicantStep4.value.url}.pdf"),
        );
      }
      if (Get.isDialogOpen ?? false) Get.back();
    }
    if (Get.isDialogOpen ?? false) Get.back();
  }

  void getPermission() async {
    await Permission.storage.request();
  }

  onDownloadPdfButtonTap(String url, String filename) {
    openFile(
      //url: invoices.value.invoicePdf,
      // url: invoices.value.invoicePdf.toString(),
      // fileName: "Invoice ${invoices.value.number}.pdf",
      url: url,
      fileName: filename,
    );
  }

  Future openFile({String? url, String? fileName}) async {
    debugPrint('url' + url!);

    final file = await downloadFile(url, fileName!);
    if (file == null) return;

    debugPrint('Path: ${file.path}');

    OpenFilex.open(file.path);
  }

  // ignore: body_might_complete_normally_nullable
  Future<File?> downloadFile(String url, String name) async {
    Future.delayed(const Duration(milliseconds: 0), () {
      FocusManager.instance.primaryFocus!.unfocus();
      Fluttertoast.showToast(
        timeInSecForIosWeb: 3,
        msg: "Downloading",
        backgroundColor: Colors.green,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        textColor: Colors.white,
        fontSize: 20.0,
      );
    });
    final taskId = await FlutterDownloader.enqueue(
        url: url,
        savedDir: _localPath,
        showNotification: true,
        // show download progress in status bar (for Android)
        openFileFromNotification: true,
        // click on notification to open downloaded file (for Android)
        saveInPublicStorage: true);

    Get.snackbar("Downloaded", "Download completed",
        backgroundColor: Colors.green,
        colorText: Colors.white,
        mainButton: TextButton(
            onPressed: () async {
              debugPrint(">>>>>>>>");
              await FlutterDownloader.open(taskId: taskId!);
            },
            child: const Text(
              "Open",
              style: TextStyle(color: Colors.white),
            )),
        duration: const Duration(seconds: 7));
  }

  late String _localPath;

  Future<void> _prepareSaveDir() async {
    _localPath = (await _findLocalPath())!;
    final savedDir = Directory(_localPath);
    bool hasExisted = await savedDir.exists();
    if (!hasExisted) {
      savedDir.create();
    }
  }

  Future<String?> _findLocalPath() async {
    String? externalStorageDirPath;
    if (Platform.isAndroid) {
      try {
        externalStorageDirPath = await AndroidPathProvider.downloadsPath;
      } catch (e) {
        final directory = await getExternalStorageDirectory();
        externalStorageDirPath = directory?.path;
      }
    } else if (Platform.isIOS) {
      externalStorageDirPath =
          (await getApplicationDocumentsDirectory()).absolute.path;
    }
    return externalStorageDirPath;
  }

  static void downloadCallback(
      String id, DownloadTaskStatus status, int progress) {
    if (kDebugMode) {
      print(
          'Background Isolate Callback: task ($id) is in status ($status) and process ($progress)');
    }

    final SendPort send =
        IsolateNameServer.lookupPortByName('downloader_send_port')!;
    send.send([id, status, progress]);
  }

//!To be implemented yet.________________________________

  onTapYourSavingsFromRefinancing() {
    Get.toNamed(refinanceSavingsPackageExistingLead);
  }

  onTapContinueToGeneratePackages() {
    generatePackages();
  }

  getBankID() {
    //dropdownPropertyType.for

    OneBankForm item = existingBanks
        .firstWhere((element) => element.name == selectedExistingBank);
    debugPrint("====dropdownPropertyType====" + item.id.toString());
    return item.id.toString();
  }

  generatePackages() async {
    debugPrint("############");
    debugPrint("############");
    debugPrint(getPropertyIndex().toString());
    debugPrint("############");
    debugPrint("############");
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);

    var data = await CoreService().getWithAuthAndQuery(
        url: baseUrl + refinanceNewLeadStep2CalculatorUrl,
        query: {
          "exising_bank": getBankID(),
          "has_200k": is200K.value.toString(),
          "loan_amount": outstandingLoanAmountController.text
              .toString()
              .replaceAll(',', '')
              .replaceAll('\$', ''),
          "property_status": selectedPropertyStatus.toString().toLowerCase(),
          "property_type": propertyTypeValueToPassInAPI.toString(),
          "rate_type": "floating",
          "sell_within_3_years": is3Years.value.toString(),
        });
    if (data == null) {
      if (Get.isDialogOpen ?? false) Get.back();
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          generatePackages();
        }
      } else {
        //log(jsonEncode(data));

        if (Get.isDialogOpen ?? false) Get.back();
        debugPrint("############");
        debugPrint("******");
        debugPrint(data["floating"].toString());
        debugPrint(data["fixed"].toString());
        debugPrint("############");
        debugPrint("******");

        if (data.toString() != "{fixed: null, floating: null}") {
          log('message');
          log(data["fixed"].toString());
          packagesGenerated = true;
          var result = RefinanceNewLeadPackageResponseModelV2.fromJson(data);

          if (data["fixed"].toString() != null.toString()) {
            lowestPackageListModel.value = result.fixed!;
          }
          if (data["floating"].toString() != null.toString()) {
            floatingPackageListModel.value = result.floating!;
          }

          Get.toNamed(generatedPackagesRefinanceExistingLead);
        } else {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "No packages found for your entered details",
              backgroundColor: Colors.white,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.black,
              fontSize: 20.0,
            );
          });
        }
      }
    }
  }

  RxInt tableDataLengthMortgageRepaymentTableMltPackage1 = 0.obs;
  var yearOneToFiveList = [];
  var yearSixToTenList = [];
  var yearElevenToFifteenList = [];
  var sixteenToTwentyList = [];
  var twentyOneToTwnetyFiveList = [];
  var twentySixToThirtyList = [];
  var thirtyOneToThirtyFiveList = [];
  String subHeading = "";

  onTapMltMortgageRepaymentTableMltPackage1() {
    subHeading = "Based On Maximum Loan Tenure";
    yearOneToFiveList.clear();
    yearSixToTenList.clear();
    yearElevenToFifteenList.clear();
    sixteenToTwentyList.clear();
    twentyOneToTwnetyFiveList.clear();
    twentySixToThirtyList.clear();
    thirtyOneToThirtyFiveList.clear();
    mltMortgageRepaymentViewDataPackage1();
    Get.toNamed(mortgageRepaymentTableRefinanceExistingLeadMlt);
  }

  onTapMltMortgageRepaymentTablePltPackage1() {
    subHeading = "Based On Preferred Loan Tenure";
    yearOneToFiveList.clear();
    yearSixToTenList.clear();
    yearElevenToFifteenList.clear();
    sixteenToTwentyList.clear();
    twentyOneToTwnetyFiveList.clear();
    twentySixToThirtyList.clear();
    thirtyOneToThirtyFiveList.clear();
    if (resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure !=
        null) {
      pltMortgageRepaymentViewDataPackage1();
      Get.toNamed(mortgageRepaymentTableRefinanceExistingLeadPlt);
    } else {
      FocusManager.instance.primaryFocus!.unfocus();
      Fluttertoast.showToast(
        timeInSecForIosWeb: 3,
        msg: "You do not have any preferred loan tenure",
        backgroundColor: Colors.red,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        textColor: Colors.white,
        fontSize: 20.0,
      );
    }
  }

  onTapMltMortgageRepaymentTableMltPackage2() {
    yearOneToFiveList.clear();
    yearSixToTenList.clear();
    yearElevenToFifteenList.clear();
    sixteenToTwentyList.clear();
    twentyOneToTwnetyFiveList.clear();
    twentySixToThirtyList.clear();
    thirtyOneToThirtyFiveList.clear();
    mltMortgageRepaymentViewDataPackage2();
    Get.toNamed(mortgageRepaymentTableRefinanceExistingLeadMlt);
  }

  onTapMltMortgageRepaymentTablePltPackage2() {
    yearOneToFiveList.clear();
    yearSixToTenList.clear();
    yearElevenToFifteenList.clear();
    sixteenToTwentyList.clear();
    twentyOneToTwnetyFiveList.clear();
    twentySixToThirtyList.clear();
    thirtyOneToThirtyFiveList.clear();
    if (resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure !=
        null) {
      pltMortgageRepaymentViewDataPackage2();
      Get.toNamed(mortgageRepaymentTableRefinanceExistingLeadPlt);
    } else {
      FocusManager.instance.primaryFocus!.unfocus();
      Fluttertoast.showToast(
        timeInSecForIosWeb: 3,
        msg: "You do not have any preferred loan tenure",
        backgroundColor: Colors.red,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        textColor: Colors.white,
        fontSize: 20.0,
      );
    }
  }

  onTapMltMortgageRepaymentTableMltPackage3() {
    yearOneToFiveList.clear();
    yearSixToTenList.clear();
    yearElevenToFifteenList.clear();
    sixteenToTwentyList.clear();
    twentyOneToTwnetyFiveList.clear();
    twentySixToThirtyList.clear();
    thirtyOneToThirtyFiveList.clear();
    mltMortgageRepaymentViewDataPackage3();
    Get.toNamed(mortgageRepaymentTableRefinanceExistingLeadMlt);
  }

  onTapMltMortgageRepaymentTablePltPackage3() {
    yearOneToFiveList.clear();
    yearSixToTenList.clear();
    yearElevenToFifteenList.clear();
    sixteenToTwentyList.clear();
    twentyOneToTwnetyFiveList.clear();
    twentySixToThirtyList.clear();
    thirtyOneToThirtyFiveList.clear();
    if (resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure !=
        null) {
      pltMortgageRepaymentViewDataPackage3();
      Get.toNamed(mortgageRepaymentTableRefinanceExistingLeadPlt);
    } else {
      FocusManager.instance.primaryFocus!.unfocus();
      Fluttertoast.showToast(
        timeInSecForIosWeb: 3,
        msg: "You do not have any preferred loan tenure",
        backgroundColor: Colors.red,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        textColor: Colors.white,
        fontSize: 20.0,
      );
    }
  }

  onTapMltMortgageRepaymentTableMltPackage4() {
    yearOneToFiveList.clear();
    yearSixToTenList.clear();
    yearElevenToFifteenList.clear();
    sixteenToTwentyList.clear();
    twentyOneToTwnetyFiveList.clear();
    twentySixToThirtyList.clear();
    thirtyOneToThirtyFiveList.clear();
    mltMortgageRepaymentViewDataPackage4();
    Get.toNamed(mortgageRepaymentTableRefinanceExistingLeadMlt);
  }

  onTapMltMortgageRepaymentTablePltPackage4() {
    yearOneToFiveList.clear();
    yearSixToTenList.clear();
    yearElevenToFifteenList.clear();
    sixteenToTwentyList.clear();
    twentyOneToTwnetyFiveList.clear();
    twentySixToThirtyList.clear();
    thirtyOneToThirtyFiveList.clear();
    if (resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure !=
        null) {
      pltMortgageRepaymentViewDataPackage4();
      Get.toNamed(mortgageRepaymentTableRefinanceExistingLeadPlt);
    } else {
      FocusManager.instance.primaryFocus!.unfocus();
      Fluttertoast.showToast(
        timeInSecForIosWeb: 3,
        msg: "You do not have any preferred loan tenure",
        backgroundColor: Colors.red,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        textColor: Colors.white,
        fontSize: 20.0,
      );
    }
  }

  mltMortgageRepaymentViewDataPackage1() {
    tableDataLengthMortgageRepaymentTableMltPackage1.value =
        resultMainApplicantStep3.value.packageDetails![1].maximumLoanTenure!
            .mortgagePaymentList!.length;

    debugPrint("mortgage size:: " +
        tableDataLengthMortgageRepaymentTableMltPackage1.value.toString());

    for (int i = 0;
        i < tableDataLengthMortgageRepaymentTableMltPackage1.value;
        i++) {
      if (resultMainApplicantStep3.value.packageDetails![1].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              1 ||
          resultMainApplicantStep3.value.packageDetails![1].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              2 ||
          resultMainApplicantStep3.value.packageDetails![1].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              3 ||
          resultMainApplicantStep3.value.packageDetails![1].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              4 ||
          resultMainApplicantStep3.value.packageDetails![1].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              5) {
        yearOneToFiveList.add(resultMainApplicantStep3.value.packageDetails![1]
            .maximumLoanTenure!.mortgagePaymentList![i]);

        debugPrint(
            "yearOneToFiveList size:: " + yearOneToFiveList.length.toString());
      } else if (resultMainApplicantStep3.value.packageDetails![1]
                  .maximumLoanTenure!.mortgagePaymentList![i].year ==
              6 ||
          resultMainApplicantStep3.value.packageDetails![1].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              7 ||
          resultMainApplicantStep3.value.packageDetails![1].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              8 ||
          resultMainApplicantStep3.value.packageDetails![1].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              9 ||
          resultMainApplicantStep3.value.packageDetails![1].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              10) {
        yearSixToTenList.add(resultMainApplicantStep3.value.packageDetails![1]
            .maximumLoanTenure!.mortgagePaymentList![i]);

        debugPrint(
            "yearSixToTenList size:: " + yearSixToTenList.length.toString());
      } else if (resultMainApplicantStep3.value.packageDetails![1]
                  .maximumLoanTenure!.mortgagePaymentList![i].year ==
              11 ||
          resultMainApplicantStep3.value.packageDetails![1].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              12 ||
          resultMainApplicantStep3.value.packageDetails![1].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              13 ||
          resultMainApplicantStep3.value.packageDetails![1].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              14 ||
          resultMainApplicantStep3.value.packageDetails![1].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              15) {
        yearElevenToFifteenList.add(resultMainApplicantStep3.value
            .packageDetails![1].maximumLoanTenure!.mortgagePaymentList![i]);

        debugPrint("yearElevenToFifteenList size:: " +
            yearElevenToFifteenList.length.toString());
      } else if (resultMainApplicantStep3.value.packageDetails![1]
                  .maximumLoanTenure!.mortgagePaymentList![i].year ==
              15 ||
          resultMainApplicantStep3.value.packageDetails![1].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              17 ||
          resultMainApplicantStep3.value.packageDetails![1].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              18 ||
          resultMainApplicantStep3.value.packageDetails![1].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              19 ||
          resultMainApplicantStep3.value.packageDetails![1].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              20) {
        sixteenToTwentyList.add(resultMainApplicantStep3.value
            .packageDetails![1].maximumLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![1]
                  .maximumLoanTenure!.mortgagePaymentList![i].year ==
              21 ||
          resultMainApplicantStep3.value.packageDetails![1].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              22 ||
          resultMainApplicantStep3.value.packageDetails![1].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              23 ||
          resultMainApplicantStep3.value.packageDetails![1].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              24 ||
          resultMainApplicantStep3.value.packageDetails![1].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              25) {
        twentyOneToTwnetyFiveList.add(resultMainApplicantStep3.value
            .packageDetails![1].maximumLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![1]
                  .maximumLoanTenure!.mortgagePaymentList![i].year ==
              26 ||
          resultMainApplicantStep3.value.packageDetails![1].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              27 ||
          resultMainApplicantStep3.value.packageDetails![1].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              28 ||
          resultMainApplicantStep3.value.packageDetails![1].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              29 ||
          resultMainApplicantStep3.value.packageDetails![1].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              30) {
        twentySixToThirtyList.add(resultMainApplicantStep3.value
            .packageDetails![1].maximumLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![1]
                  .maximumLoanTenure!.mortgagePaymentList![i].year ==
              31 ||
          resultMainApplicantStep3.value.packageDetails![1].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              32 ||
          resultMainApplicantStep3.value.packageDetails![1].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              33 ||
          resultMainApplicantStep3.value.packageDetails![1].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              34 ||
          resultMainApplicantStep3.value.packageDetails![1].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              35) {
        thirtyOneToThirtyFiveList.add(resultMainApplicantStep3.value
            .packageDetails![1].maximumLoanTenure!.mortgagePaymentList![i]);
      }
    }
  }

  pltMortgageRepaymentViewDataPackage1() {
    tableDataLengthMortgageRepaymentTableMltPackage1.value =
        resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure!
            .mortgagePaymentList!.length;

    for (int i = 0;
        i < tableDataLengthMortgageRepaymentTableMltPackage1.value;
        i++) {
      if (resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              1 ||
          resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              2 ||
          resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              3 ||
          resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              4 ||
          resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              5) {
        yearOneToFiveList.add(resultMainApplicantStep3.value.packageDetails![1]
            .preferedLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![1]
                  .preferedLoanTenure!.mortgagePaymentList![i].year ==
              6 ||
          resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              7 ||
          resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              8 ||
          resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              9 ||
          resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              10) {
        yearSixToTenList.add(resultMainApplicantStep3.value.packageDetails![1]
            .preferedLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![1]
                  .preferedLoanTenure!.mortgagePaymentList![i].year ==
              11 ||
          resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              12 ||
          resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              13 ||
          resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              14 ||
          resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              15) {
        yearElevenToFifteenList.add(resultMainApplicantStep3.value
            .packageDetails![1].preferedLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![1]
                  .preferedLoanTenure!.mortgagePaymentList![i].year ==
              15 ||
          resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              17 ||
          resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              18 ||
          resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              19 ||
          resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              20) {
        sixteenToTwentyList.add(resultMainApplicantStep3.value
            .packageDetails![1].preferedLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![1]
                  .preferedLoanTenure!.mortgagePaymentList![i].year ==
              21 ||
          resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              22 ||
          resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              23 ||
          resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              24 ||
          resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              25) {
        twentyOneToTwnetyFiveList.add(resultMainApplicantStep3.value
            .packageDetails![1].preferedLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![1]
                  .preferedLoanTenure!.mortgagePaymentList![i].year ==
              26 ||
          resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              27 ||
          resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              28 ||
          resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              29 ||
          resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              30) {
        twentySixToThirtyList.add(resultMainApplicantStep3.value
            .packageDetails![1].preferedLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![1]
                  .preferedLoanTenure!.mortgagePaymentList![i].year ==
              31 ||
          resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              32 ||
          resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              33 ||
          resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              34 ||
          resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              35) {
        thirtyOneToThirtyFiveList.add(resultMainApplicantStep3.value
            .packageDetails![1].preferedLoanTenure!.mortgagePaymentList![i]);
      }
    }
  }

  mltMortgageRepaymentViewDataPackage2() {
    tableDataLengthMortgageRepaymentTableMltPackage1.value =
        resultMainApplicantStep3.value.packageDetails![2].maximumLoanTenure!
            .mortgagePaymentList!.length;

    for (int i = 0;
        i < tableDataLengthMortgageRepaymentTableMltPackage1.value;
        i++) {
      if (resultMainApplicantStep3.value.packageDetails![2].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              1 ||
          resultMainApplicantStep3.value.packageDetails![2].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              2 ||
          resultMainApplicantStep3.value.packageDetails![2].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              3 ||
          resultMainApplicantStep3.value.packageDetails![2].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              4 ||
          resultMainApplicantStep3.value.packageDetails![2].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              5) {
        yearOneToFiveList.add(resultMainApplicantStep3.value.packageDetails![2]
            .maximumLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![2]
                  .maximumLoanTenure!.mortgagePaymentList![i].year ==
              6 ||
          resultMainApplicantStep3.value.packageDetails![2].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              7 ||
          resultMainApplicantStep3.value.packageDetails![2].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              8 ||
          resultMainApplicantStep3.value.packageDetails![2].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              9 ||
          resultMainApplicantStep3.value.packageDetails![2].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              10) {
        yearSixToTenList.add(resultMainApplicantStep3.value.packageDetails![2]
            .maximumLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![2]
                  .maximumLoanTenure!.mortgagePaymentList![i].year ==
              11 ||
          resultMainApplicantStep3.value.packageDetails![2].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              12 ||
          resultMainApplicantStep3.value.packageDetails![2].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              13 ||
          resultMainApplicantStep3.value.packageDetails![2].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              14 ||
          resultMainApplicantStep3.value.packageDetails![2].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              15) {
        yearElevenToFifteenList.add(resultMainApplicantStep3.value
            .packageDetails![2].maximumLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![2]
                  .maximumLoanTenure!.mortgagePaymentList![i].year ==
              15 ||
          resultMainApplicantStep3.value.packageDetails![2].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              17 ||
          resultMainApplicantStep3.value.packageDetails![2].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              18 ||
          resultMainApplicantStep3.value.packageDetails![2].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              19 ||
          resultMainApplicantStep3.value.packageDetails![2].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              20) {
        sixteenToTwentyList.add(resultMainApplicantStep3.value
            .packageDetails![2].maximumLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![2]
                  .maximumLoanTenure!.mortgagePaymentList![i].year ==
              21 ||
          resultMainApplicantStep3.value.packageDetails![2].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              22 ||
          resultMainApplicantStep3.value.packageDetails![2].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              23 ||
          resultMainApplicantStep3.value.packageDetails![2].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              24 ||
          resultMainApplicantStep3.value.packageDetails![2].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              25) {
        twentyOneToTwnetyFiveList.add(resultMainApplicantStep3.value
            .packageDetails![2].maximumLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![2]
                  .maximumLoanTenure!.mortgagePaymentList![i].year ==
              26 ||
          resultMainApplicantStep3.value.packageDetails![2].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              27 ||
          resultMainApplicantStep3.value.packageDetails![2].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              28 ||
          resultMainApplicantStep3.value.packageDetails![2].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              29 ||
          resultMainApplicantStep3.value.packageDetails![2].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              30) {
        twentySixToThirtyList.add(resultMainApplicantStep3.value
            .packageDetails![2].maximumLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![2]
                  .maximumLoanTenure!.mortgagePaymentList![i].year ==
              31 ||
          resultMainApplicantStep3.value.packageDetails![2].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              32 ||
          resultMainApplicantStep3.value.packageDetails![2].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              33 ||
          resultMainApplicantStep3.value.packageDetails![2].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              34 ||
          resultMainApplicantStep3.value.packageDetails![2].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              35) {
        thirtyOneToThirtyFiveList.add(resultMainApplicantStep3.value
            .packageDetails![2].maximumLoanTenure!.mortgagePaymentList![i]);
      }
    }
  }

  mltMortgageRepaymentViewDataPackage3() {
    tableDataLengthMortgageRepaymentTableMltPackage1.value =
        resultMainApplicantStep3.value.packageDetails![3].maximumLoanTenure!
            .mortgagePaymentList!.length;

    for (int i = 0;
        i < tableDataLengthMortgageRepaymentTableMltPackage1.value;
        i++) {
      if (resultMainApplicantStep3.value.packageDetails![3].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              1 ||
          resultMainApplicantStep3.value.packageDetails![3].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              2 ||
          resultMainApplicantStep3.value.packageDetails![3].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              3 ||
          resultMainApplicantStep3.value.packageDetails![3].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              4 ||
          resultMainApplicantStep3.value.packageDetails![3].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              5) {
        yearOneToFiveList.add(resultMainApplicantStep3.value.packageDetails![3]
            .maximumLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![3]
                  .maximumLoanTenure!.mortgagePaymentList![i].year ==
              6 ||
          resultMainApplicantStep3.value.packageDetails![3].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              7 ||
          resultMainApplicantStep3.value.packageDetails![3].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              8 ||
          resultMainApplicantStep3.value.packageDetails![3].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              9 ||
          resultMainApplicantStep3.value.packageDetails![3].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              10) {
        yearSixToTenList.add(resultMainApplicantStep3.value.packageDetails![3]
            .maximumLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![3]
                  .maximumLoanTenure!.mortgagePaymentList![i].year ==
              11 ||
          resultMainApplicantStep3.value.packageDetails![3].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              12 ||
          resultMainApplicantStep3.value.packageDetails![3].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              13 ||
          resultMainApplicantStep3.value.packageDetails![3].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              14 ||
          resultMainApplicantStep3.value.packageDetails![3].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              15) {
        yearElevenToFifteenList.add(resultMainApplicantStep3.value
            .packageDetails![3].maximumLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![3]
                  .maximumLoanTenure!.mortgagePaymentList![i].year ==
              15 ||
          resultMainApplicantStep3.value.packageDetails![3].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              17 ||
          resultMainApplicantStep3.value.packageDetails![3].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              18 ||
          resultMainApplicantStep3.value.packageDetails![3].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              19 ||
          resultMainApplicantStep3.value.packageDetails![3].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              20) {
        sixteenToTwentyList.add(resultMainApplicantStep3.value
            .packageDetails![3].maximumLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![3]
                  .maximumLoanTenure!.mortgagePaymentList![i].year ==
              21 ||
          resultMainApplicantStep3.value.packageDetails![3].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              22 ||
          resultMainApplicantStep3.value.packageDetails![3].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              23 ||
          resultMainApplicantStep3.value.packageDetails![3].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              24 ||
          resultMainApplicantStep3.value.packageDetails![3].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              25) {
        twentyOneToTwnetyFiveList.add(resultMainApplicantStep3.value
            .packageDetails![3].maximumLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![3]
                  .maximumLoanTenure!.mortgagePaymentList![i].year ==
              26 ||
          resultMainApplicantStep3.value.packageDetails![3].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              27 ||
          resultMainApplicantStep3.value.packageDetails![3].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              28 ||
          resultMainApplicantStep3.value.packageDetails![3].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              29 ||
          resultMainApplicantStep3.value.packageDetails![3].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              30) {
        twentySixToThirtyList.add(resultMainApplicantStep3.value
            .packageDetails![3].maximumLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![3]
                  .maximumLoanTenure!.mortgagePaymentList![i].year ==
              31 ||
          resultMainApplicantStep3.value.packageDetails![3].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              32 ||
          resultMainApplicantStep3.value.packageDetails![3].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              33 ||
          resultMainApplicantStep3.value.packageDetails![3].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              34 ||
          resultMainApplicantStep3.value.packageDetails![3].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              35) {
        thirtyOneToThirtyFiveList.add(resultMainApplicantStep3.value
            .packageDetails![3].maximumLoanTenure!.mortgagePaymentList![i]);
      }
    }
  }

  mltMortgageRepaymentViewDataPackage4() {
    tableDataLengthMortgageRepaymentTableMltPackage1.value =
        resultMainApplicantStep3.value.packageDetails![4].maximumLoanTenure!
            .mortgagePaymentList!.length;

    for (int i = 0;
        i < tableDataLengthMortgageRepaymentTableMltPackage1.value;
        i++) {
      if (resultMainApplicantStep3.value.packageDetails![4].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              1 ||
          resultMainApplicantStep3.value.packageDetails![4].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              2 ||
          resultMainApplicantStep3.value.packageDetails![4].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              3 ||
          resultMainApplicantStep3.value.packageDetails![4].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              4 ||
          resultMainApplicantStep3.value.packageDetails![4].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              5) {
        yearOneToFiveList.add(resultMainApplicantStep3.value.packageDetails![4]
            .maximumLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![4]
                  .maximumLoanTenure!.mortgagePaymentList![i].year ==
              6 ||
          resultMainApplicantStep3.value.packageDetails![4].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              7 ||
          resultMainApplicantStep3.value.packageDetails![4].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              8 ||
          resultMainApplicantStep3.value.packageDetails![4].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              9 ||
          resultMainApplicantStep3.value.packageDetails![4].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              10) {
        yearSixToTenList.add(resultMainApplicantStep3.value.packageDetails![4]
            .maximumLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![4]
                  .maximumLoanTenure!.mortgagePaymentList![i].year ==
              11 ||
          resultMainApplicantStep3.value.packageDetails![4].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              12 ||
          resultMainApplicantStep3.value.packageDetails![4].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              13 ||
          resultMainApplicantStep3.value.packageDetails![4].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              14 ||
          resultMainApplicantStep3.value.packageDetails![4].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              15) {
        yearElevenToFifteenList.add(resultMainApplicantStep3.value
            .packageDetails![4].maximumLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![4]
                  .maximumLoanTenure!.mortgagePaymentList![i].year ==
              15 ||
          resultMainApplicantStep3.value.packageDetails![4].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              17 ||
          resultMainApplicantStep3.value.packageDetails![4].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              18 ||
          resultMainApplicantStep3.value.packageDetails![4].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              19 ||
          resultMainApplicantStep3.value.packageDetails![4].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              20) {
        sixteenToTwentyList.add(resultMainApplicantStep3.value
            .packageDetails![4].maximumLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![4]
                  .maximumLoanTenure!.mortgagePaymentList![i].year ==
              21 ||
          resultMainApplicantStep3.value.packageDetails![4].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              22 ||
          resultMainApplicantStep3.value.packageDetails![4].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              23 ||
          resultMainApplicantStep3.value.packageDetails![4].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              24 ||
          resultMainApplicantStep3.value.packageDetails![4].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              25) {
        twentyOneToTwnetyFiveList.add(resultMainApplicantStep3.value
            .packageDetails![4].maximumLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![4]
                  .maximumLoanTenure!.mortgagePaymentList![i].year ==
              26 ||
          resultMainApplicantStep3.value.packageDetails![4].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              27 ||
          resultMainApplicantStep3.value.packageDetails![4].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              28 ||
          resultMainApplicantStep3.value.packageDetails![4].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              29 ||
          resultMainApplicantStep3.value.packageDetails![4].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              30) {
        twentySixToThirtyList.add(resultMainApplicantStep3.value
            .packageDetails![4].maximumLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![4]
                  .maximumLoanTenure!.mortgagePaymentList![i].year ==
              31 ||
          resultMainApplicantStep3.value.packageDetails![4].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              32 ||
          resultMainApplicantStep3.value.packageDetails![4].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              33 ||
          resultMainApplicantStep3.value.packageDetails![4].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              34 ||
          resultMainApplicantStep3.value.packageDetails![4].maximumLoanTenure!
                  .mortgagePaymentList![i].year ==
              35) {
        thirtyOneToThirtyFiveList.add(resultMainApplicantStep3.value
            .packageDetails![4].maximumLoanTenure!.mortgagePaymentList![i]);
      }
    }
  }

  pltMortgageRepaymentViewDataPackage2() {
    tableDataLengthMortgageRepaymentTableMltPackage1.value =
        resultMainApplicantStep3.value.packageDetails![2].preferedLoanTenure!
            .mortgagePaymentList!.length;

    for (int i = 0;
        i < tableDataLengthMortgageRepaymentTableMltPackage1.value;
        i++) {
      if (resultMainApplicantStep3.value.packageDetails![2].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              1 ||
          resultMainApplicantStep3.value.packageDetails![2].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              2 ||
          resultMainApplicantStep3.value.packageDetails![2].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              3 ||
          resultMainApplicantStep3.value.packageDetails![2].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              4 ||
          resultMainApplicantStep3.value.packageDetails![2].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              5) {
        yearOneToFiveList.add(resultMainApplicantStep3.value.packageDetails![2]
            .preferedLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![2]
                  .preferedLoanTenure!.mortgagePaymentList![i].year ==
              6 ||
          resultMainApplicantStep3.value.packageDetails![2].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              7 ||
          resultMainApplicantStep3.value.packageDetails![2].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              8 ||
          resultMainApplicantStep3.value.packageDetails![2].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              9 ||
          resultMainApplicantStep3.value.packageDetails![2].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              10) {
        yearSixToTenList.add(resultMainApplicantStep3.value.packageDetails![2]
            .preferedLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![2]
                  .preferedLoanTenure!.mortgagePaymentList![i].year ==
              11 ||
          resultMainApplicantStep3.value.packageDetails![2].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              12 ||
          resultMainApplicantStep3.value.packageDetails![2].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              13 ||
          resultMainApplicantStep3.value.packageDetails![2].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              14 ||
          resultMainApplicantStep3.value.packageDetails![2].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              15) {
        yearElevenToFifteenList.add(resultMainApplicantStep3.value
            .packageDetails![2].preferedLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![2]
                  .preferedLoanTenure!.mortgagePaymentList![i].year ==
              15 ||
          resultMainApplicantStep3.value.packageDetails![2].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              17 ||
          resultMainApplicantStep3.value.packageDetails![2].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              18 ||
          resultMainApplicantStep3.value.packageDetails![2].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              19 ||
          resultMainApplicantStep3.value.packageDetails![2].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              20) {
        sixteenToTwentyList.add(resultMainApplicantStep3.value
            .packageDetails![2].preferedLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![2]
                  .preferedLoanTenure!.mortgagePaymentList![i].year ==
              21 ||
          resultMainApplicantStep3.value.packageDetails![2].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              22 ||
          resultMainApplicantStep3.value.packageDetails![2].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              23 ||
          resultMainApplicantStep3.value.packageDetails![2].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              24 ||
          resultMainApplicantStep3.value.packageDetails![2].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              25) {
        twentyOneToTwnetyFiveList.add(resultMainApplicantStep3.value
            .packageDetails![2].preferedLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![2]
                  .preferedLoanTenure!.mortgagePaymentList![i].year ==
              26 ||
          resultMainApplicantStep3.value.packageDetails![2].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              27 ||
          resultMainApplicantStep3.value.packageDetails![2].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              28 ||
          resultMainApplicantStep3.value.packageDetails![2].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              29 ||
          resultMainApplicantStep3.value.packageDetails![2].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              30) {
        twentySixToThirtyList.add(resultMainApplicantStep3.value
            .packageDetails![2].preferedLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![2]
                  .preferedLoanTenure!.mortgagePaymentList![i].year ==
              31 ||
          resultMainApplicantStep3.value.packageDetails![2].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              32 ||
          resultMainApplicantStep3.value.packageDetails![2].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              33 ||
          resultMainApplicantStep3.value.packageDetails![2].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              34 ||
          resultMainApplicantStep3.value.packageDetails![2].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              35) {
        thirtyOneToThirtyFiveList.add(resultMainApplicantStep3.value
            .packageDetails![2].preferedLoanTenure!.mortgagePaymentList![i]);
      }
    }
  }

  pltMortgageRepaymentViewDataPackage3() {
    tableDataLengthMortgageRepaymentTableMltPackage1.value =
        resultMainApplicantStep3.value.packageDetails![3].preferedLoanTenure!
            .mortgagePaymentList!.length;

    for (int i = 0;
        i < tableDataLengthMortgageRepaymentTableMltPackage1.value;
        i++) {
      if (resultMainApplicantStep3.value.packageDetails![3].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              1 ||
          resultMainApplicantStep3.value.packageDetails![3].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              2 ||
          resultMainApplicantStep3.value.packageDetails![3].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              3 ||
          resultMainApplicantStep3.value.packageDetails![3].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              4 ||
          resultMainApplicantStep3.value.packageDetails![3].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              5) {
        yearOneToFiveList.add(resultMainApplicantStep3.value.packageDetails![3]
            .preferedLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![3]
                  .preferedLoanTenure!.mortgagePaymentList![i].year ==
              6 ||
          resultMainApplicantStep3.value.packageDetails![3].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              7 ||
          resultMainApplicantStep3.value.packageDetails![3].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              8 ||
          resultMainApplicantStep3.value.packageDetails![3].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              9 ||
          resultMainApplicantStep3.value.packageDetails![3].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              10) {
        yearSixToTenList.add(resultMainApplicantStep3.value.packageDetails![3]
            .preferedLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![3]
                  .preferedLoanTenure!.mortgagePaymentList![i].year ==
              11 ||
          resultMainApplicantStep3.value.packageDetails![3].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              12 ||
          resultMainApplicantStep3.value.packageDetails![3].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              13 ||
          resultMainApplicantStep3.value.packageDetails![3].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              14 ||
          resultMainApplicantStep3.value.packageDetails![3].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              15) {
        yearElevenToFifteenList.add(resultMainApplicantStep3.value
            .packageDetails![3].preferedLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![3]
                  .preferedLoanTenure!.mortgagePaymentList![i].year ==
              15 ||
          resultMainApplicantStep3.value.packageDetails![3].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              17 ||
          resultMainApplicantStep3.value.packageDetails![3].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              18 ||
          resultMainApplicantStep3.value.packageDetails![3].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              19 ||
          resultMainApplicantStep3.value.packageDetails![3].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              20) {
        sixteenToTwentyList.add(resultMainApplicantStep3.value
            .packageDetails![3].preferedLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![3]
                  .preferedLoanTenure!.mortgagePaymentList![i].year ==
              21 ||
          resultMainApplicantStep3.value.packageDetails![3].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              22 ||
          resultMainApplicantStep3.value.packageDetails![3].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              23 ||
          resultMainApplicantStep3.value.packageDetails![3].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              24 ||
          resultMainApplicantStep3.value.packageDetails![3].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              25) {
        twentyOneToTwnetyFiveList.add(resultMainApplicantStep3.value
            .packageDetails![3].preferedLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![3]
                  .preferedLoanTenure!.mortgagePaymentList![i].year ==
              26 ||
          resultMainApplicantStep3.value.packageDetails![3].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              27 ||
          resultMainApplicantStep3.value.packageDetails![3].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              28 ||
          resultMainApplicantStep3.value.packageDetails![3].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              29 ||
          resultMainApplicantStep3.value.packageDetails![3].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              30) {
        twentySixToThirtyList.add(resultMainApplicantStep3.value
            .packageDetails![3].preferedLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![3]
                  .preferedLoanTenure!.mortgagePaymentList![i].year ==
              31 ||
          resultMainApplicantStep3.value.packageDetails![3].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              32 ||
          resultMainApplicantStep3.value.packageDetails![3].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              33 ||
          resultMainApplicantStep3.value.packageDetails![3].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              34 ||
          resultMainApplicantStep3.value.packageDetails![3].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              35) {
        thirtyOneToThirtyFiveList.add(resultMainApplicantStep3.value
            .packageDetails![3].preferedLoanTenure!.mortgagePaymentList![i]);
      }
    }
  }

  pltMortgageRepaymentViewDataPackage4() {
    tableDataLengthMortgageRepaymentTableMltPackage1.value =
        resultMainApplicantStep3.value.packageDetails![4].preferedLoanTenure!
            .mortgagePaymentList!.length;

    for (int i = 0;
        i < tableDataLengthMortgageRepaymentTableMltPackage1.value;
        i++) {
      if (resultMainApplicantStep3.value.packageDetails![4].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              1 ||
          resultMainApplicantStep3.value.packageDetails![4].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              2 ||
          resultMainApplicantStep3.value.packageDetails![4].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              3 ||
          resultMainApplicantStep3.value.packageDetails![4].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              4 ||
          resultMainApplicantStep3.value.packageDetails![4].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              5) {
        yearOneToFiveList.add(resultMainApplicantStep3.value.packageDetails![4]
            .preferedLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![4]
                  .preferedLoanTenure!.mortgagePaymentList![i].year ==
              6 ||
          resultMainApplicantStep3.value.packageDetails![4].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              7 ||
          resultMainApplicantStep3.value.packageDetails![4].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              8 ||
          resultMainApplicantStep3.value.packageDetails![4].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              9 ||
          resultMainApplicantStep3.value.packageDetails![4].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              10) {
        yearSixToTenList.add(resultMainApplicantStep3.value.packageDetails![4]
            .preferedLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![4]
                  .preferedLoanTenure!.mortgagePaymentList![i].year ==
              11 ||
          resultMainApplicantStep3.value.packageDetails![4].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              12 ||
          resultMainApplicantStep3.value.packageDetails![4].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              13 ||
          resultMainApplicantStep3.value.packageDetails![4].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              14 ||
          resultMainApplicantStep3.value.packageDetails![4].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              15) {
        yearElevenToFifteenList.add(resultMainApplicantStep3.value
            .packageDetails![4].preferedLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![4]
                  .preferedLoanTenure!.mortgagePaymentList![i].year ==
              15 ||
          resultMainApplicantStep3.value.packageDetails![4].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              17 ||
          resultMainApplicantStep3.value.packageDetails![4].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              18 ||
          resultMainApplicantStep3.value.packageDetails![4].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              19 ||
          resultMainApplicantStep3.value.packageDetails![4].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              20) {
        sixteenToTwentyList.add(resultMainApplicantStep3.value
            .packageDetails![4].preferedLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![4]
                  .preferedLoanTenure!.mortgagePaymentList![i].year ==
              21 ||
          resultMainApplicantStep3.value.packageDetails![4].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              22 ||
          resultMainApplicantStep3.value.packageDetails![4].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              23 ||
          resultMainApplicantStep3.value.packageDetails![4].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              24 ||
          resultMainApplicantStep3.value.packageDetails![4].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              25) {
        twentyOneToTwnetyFiveList.add(resultMainApplicantStep3.value
            .packageDetails![4].preferedLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![4]
                  .preferedLoanTenure!.mortgagePaymentList![i].year ==
              26 ||
          resultMainApplicantStep3.value.packageDetails![4].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              27 ||
          resultMainApplicantStep3.value.packageDetails![4].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              28 ||
          resultMainApplicantStep3.value.packageDetails![4].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              29 ||
          resultMainApplicantStep3.value.packageDetails![4].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              30) {
        twentySixToThirtyList.add(resultMainApplicantStep3.value
            .packageDetails![4].preferedLoanTenure!.mortgagePaymentList![i]);
      } else if (resultMainApplicantStep3.value.packageDetails![4]
                  .preferedLoanTenure!.mortgagePaymentList![i].year ==
              31 ||
          resultMainApplicantStep3.value.packageDetails![4].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              32 ||
          resultMainApplicantStep3.value.packageDetails![4].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              33 ||
          resultMainApplicantStep3.value.packageDetails![4].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              34 ||
          resultMainApplicantStep3.value.packageDetails![4].preferedLoanTenure!
                  .mortgagePaymentList![i].year ==
              35) {
        thirtyOneToThirtyFiveList.add(resultMainApplicantStep3.value
            .packageDetails![4].preferedLoanTenure!.mortgagePaymentList![i]);
      }
    }
  }
}
