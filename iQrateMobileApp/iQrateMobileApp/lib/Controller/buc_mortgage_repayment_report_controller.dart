import 'dart:convert';
import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:get/get.dart';

import '../Model/response_model.dart/BucMortgageRepaymentTableModel.dart';
import '../Model/response_model.dart/BucMortgareCompareModel.dart';

class BucMortgageRepaymentReportController extends GetxController {
  dynamic loadDetailsDataResult;
  var isLoanDetailCheckBoxEnable = true.obs;
  var isIProgressiveRepaymentScheduleCheckBoxEnable = true.obs;
  var isBucMortgageRepaymentCheckBoxEnable = true.obs;

  var arrayModelCompare = <RatePackageComparisonModel>[].obs;

  var dataSourceA = <BucMortgageRepaymentTableModel>[]; //for table
  var dataSourceB = <BucMortgageRepaymentTableModel>[]; //for table

  var dataSourceTable1 = <
      BucMortgageRepaymentTableModel>[]; //for table year 1- 5 , total max 60 item
  var dataSourceTable2 = <
      BucMortgageRepaymentTableModel>[]; //for table year 6- 10 , total max 60 item
  var dataSourceTable3 = <
      BucMortgageRepaymentTableModel>[]; //for table year 11-15 , total max 60 item
  var dataSourceTable4 = <
      BucMortgageRepaymentTableModel>[]; //for table year 16- 20 , total max 60 item
  var dataSourceTable5 = <
      BucMortgageRepaymentTableModel>[]; //for table year 21- 25 , total max 60 item
  var dataSourceTable6 = <
      BucMortgageRepaymentTableModel>[]; //for table year 26- 30 , total max 60 item
  var dataSourceTable7 = <
      BucMortgageRepaymentTableModel>[]; //for table year 31- 35 , total max 60 item
  var viewTable1 = true;

  var isCheckLoanDetails = false;
  var isRateComparison = false;
  var isDisbursementSchedule = false;
  var isProgressiveRepayment = false;
  var isPreferredRepayment = false;

  // "export_fields" = ["Loan Details","Rate Comparison","Disbursement Schedule","Progressive Repayment","Preferred Repayment"]

  clearData() {
    dataSourceA = [];
    dataSourceB = [];
    dataSourceTable1 = [];
    dataSourceTable2 = [];
    loadDetailsDataResult = null;
    dataSourceTable3 = [];
    dataSourceTable4 = [];
    dataSourceTable5 = [];
    dataSourceTable6 = [];
    dataSourceTable7 = [];
    viewTable1 = true;
  }

  clearDataTable() {
    dataSourceTable1 = [];
    dataSourceTable2 = [];
    dataSourceTable3 = [];
    dataSourceTable4 = [];
    dataSourceTable5 = [];
    dataSourceTable6 = [];
    dataSourceTable7 = [];
  }

  getDataTableModels() {
    viewTable1 ? getDataTableModels1() : getDataTableModels2();
  }

  getDataTableModels1() {
    debugPrint("getDataTableModels1");
    dataSourceA = [];
    dataSourceB = [];

    loadDetailsDataResult["IQrate Exclusive BUC Rate"].forEach((key, value) {
      var data = value;
      if (kDebugMode) {
        log("value of $key : ${jsonEncode(data)}");
      }
      if (data.isNotEmpty) {
        data.forEach((key, value) {
          if (key != "previous_total_principal_pay") {
            log("BucMortgageRepaymentTableModel value of $key : ${jsonEncode(value).toString()}");

            var model = BucMortgageRepaymentTableModel.fromJson(value);
            dataSourceA.add(model);
          }
        });
      }
    });

    if (kDebugMode) {
      print("dataSourceA have ${dataSourceA.length}");
    }
    //fill data for year 1 - 5
    for (var i = 0; i < 60 && i < dataSourceA.length; i++) {
      dataSourceTable1.add(dataSourceA[i]);
    }
    //
    //     //fill data for year 6 - 10
    for (var i = 60; i < 120 && i < dataSourceA.length; i++) {
      dataSourceTable2.add(dataSourceA[i]);
    }
    //     //fill data for year 11 - 15
    for (var i = 120; i < 180 && i < dataSourceA.length; i++) {
      dataSourceTable3.add(dataSourceA[i]);
    }
    //     //fill data for year 16 - 20
    for (var i = 180; i < 240 && i < dataSourceA.length; i++) {
      dataSourceTable4.add(dataSourceA[i]);
    }
    //fill data for year 21 - 25
    for (var i = 240; i < 300 && i < dataSourceA.length; i++) {
      dataSourceTable5.add(dataSourceA[i]);
    }
    //fill data for year 25 - 30
    for (var i = 300; i < 360 && i < dataSourceA.length; i++) {
      dataSourceTable6.add(dataSourceA[i]);
    }
    //fill data for year 31 - 35
    for (var i = 360; i < 420 && i < dataSourceA.length; i++) {
      dataSourceTable7.add(dataSourceA[i]);
    }
    if (kDebugMode) {
      print("dataSourceA lenght ${dataSourceA.length}");
    }
    if (kDebugMode) {
      print("dataSourceB lenght ${dataSourceB.length}");
    }

    if (kDebugMode) {
      print("dataSourceTable1 lenght ${dataSourceTable1.length}");
    }
    if (kDebugMode) {
      print("dataSourceTable2 lenght ${dataSourceTable2.length}");
    }
    if (kDebugMode) {
      print("dataSourceTable3 lenght ${dataSourceTable3.length}");
    }
    if (kDebugMode) {
      print("dataSourceTable4 lenght ${dataSourceTable4.length}");
    }
    if (kDebugMode) {
      print("dataSourceTable5 lenght ${dataSourceTable5.length}");
    }
    if (kDebugMode) {
      print("dataSourceTable6 lenght ${dataSourceTable6.length}");
    }
    if (kDebugMode) {
      print("dataSourceTable7 lenght ${dataSourceTable7.length}");
    }
  }

  getDataTableModels2() {
    debugPrint("getDataTableModels2");
    dataSourceA = [];
    dataSourceB = [];
    // var array = [];
    loadDetailsDataResult["Prefered BUC Rate"].forEach((key, value) {
      var data = value;
      if (data.isNotEmpty) {
        data.forEach((key, value) {
          if(key != "previous_total_principal_pay") {
            var model = BucMortgageRepaymentTableModel.fromJson(value);
            dataSourceB.add(model);
          }
        });
      }
    });
    //fill data for year 1 - 5
    for (var i = 0; i < 60 && i < dataSourceB.length; i++) {
      dataSourceTable1.add(dataSourceB[i]);
    }
    //
    //     //fill data for year 6 - 10
    for (var i = 60; i < 120 && i < dataSourceB.length; i++) {
      dataSourceTable2.add(dataSourceB[i]);
    }
    //     //fill data for year 11 - 15
    for (var i = 120; i < 180 && i < dataSourceB.length; i++) {
      dataSourceTable3.add(dataSourceB[i]);
    }
    //     //fill data for year 16 - 20
    for (var i = 180; i < 240 && i < dataSourceB.length; i++) {
      dataSourceTable4.add(dataSourceB[i]);
    }
    //fill data for year 21 - 25
    for (var i = 240; i < 300 && i < dataSourceB.length; i++) {
      dataSourceTable5.add(dataSourceB[i]);
    }
    //fill data for year 25 - 30
    for (var i = 300; i < 360 && i < dataSourceB.length; i++) {
      dataSourceTable6.add(dataSourceB[i]);
    }
    //fill data for year 31 - 35
    for (var i = 360; i < 420 && i < dataSourceB.length; i++) {
      dataSourceTable7.add(dataSourceB[i]);
    }
    // print("dataSourceB lenght ${dataSourceB.length}");
    if (kDebugMode) {
      print("dataSourceB lenght ${dataSourceB.length}");
    }

    if (kDebugMode) {
      print("dataSourceTable1 lenght ${dataSourceTable1.length}");
    }
    if (kDebugMode) {
      print("dataSourceTable2 lenght ${dataSourceTable2.length}");
    }
    if (kDebugMode) {
      print("dataSourceTable3 lenght ${dataSourceTable3.length}");
    }
    if (kDebugMode) {
      print("dataSourceTable4 lenght ${dataSourceTable4.length}");
    }
    if (kDebugMode) {
      print("dataSourceTable5 lenght ${dataSourceTable5.length}");
    }
    if (kDebugMode) {
      print("dataSourceTable6 lenght ${dataSourceTable6.length}");
    }
    if (kDebugMode) {
      print("dataSourceTable7 lenght ${dataSourceTable7.length}");
    }
  }
}
