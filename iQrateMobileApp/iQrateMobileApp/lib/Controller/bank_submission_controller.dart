import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Model/response_model.dart/bank_submission_banks_list_response_model.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';

class BankSubmissionController extends GetxController {
  // This is used in expanding a card (Only one card can be expanded at a time).
  var selectedIndex = RxInt(-1);
  RxBool isExpanded = false.obs;

  RxBool isSubmitted = false.obs;
  late RxDouble windowHeight;
  late RxDouble windowWidth;
  // late RxInt selectedIndex;
  int completedtakCounter = 0;

  Rx<BankSubmissionBanksListResponseModel>
      bankSubmissionBanksListResponseModel =
      BankSubmissionBanksListResponseModel().obs;

  RxBool isExpand = false.obs;
  RxBool isBankSubmissionDone = false.obs;

  void onTapReselectRates() {}

  void onTapSubmit() {
    isSubmitted.value = true;
  }

  // This is used for expanding the card
  void expandingCard(int index) {
    selectedIndex.value = index;
    checkIfExpanded(index);
  }

  // Reseting the value of the slectedIndex variable hence cosing the expansion section of the card
  void reset() {
    selectedIndex.value = -1;
  }

  // To check if the card is expanded or not
  bool checkIfExpanded(int index) {
    if (selectedIndex.value == index) {
      isExpanded.value = true;
      return isExpanded.value;
    } else {
      return isExpanded.value;
    }
  }

  // Get baknk list with tasks
  getBanks(String leadId) async {
    debugPrint(baseUrl + banksubmissionGetBanksUrl + leadId);
    try {
      Timer(const Duration(seconds: 0), () {
        Get.isDialogOpen ?? true
            ? const Offstage()
            : Get.dialog(
                const Center(
                  child: CircularProgressIndicator(),
                ),
                barrierDismissible: false);
      });
      var data = await CoreService()
          .getWithAuth(url: baseUrl + banksubmissionGetBanksUrl + leadId);
      log('*****************************');
      log(data.toString());
      log('*****************************');
      if (data != null) {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            getBanks(leadId);
          }
        } else {
          bankSubmissionBanksListResponseModel.value =
              BankSubmissionBanksListResponseModel.fromJson(data);

          // This forloop is to check if the banksubmission is completed or not
          for (int i = 0;
              i <
                  bankSubmissionBanksListResponseModel
                      .value.bankSubmissionOneBank!.length;
              i++) {
            if (bankSubmissionBanksListResponseModel
                        .value.bankSubmissionOneBank![i].emailToBankTask! !=
                    true ||
                bankSubmissionBanksListResponseModel
                        .value.bankSubmissionOneBank![i].emailToLeasTask! !=
                    true) {
              isBankSubmissionDone.value = false;
              break;
            } else {
              isBankSubmissionDone.value = true;
            }
          }
          if (Get.isDialogOpen ?? false) Get.back();
        }
      } else {
        if (Get.isDialogOpen ?? false) Get.back();
      }
    } catch (e) {
      if (Get.isDialogOpen ?? false) Get.back();
      debugPrint(e.toString());
    }
  }

  String getBankTaskCounterString(bool isEmailToLead, bool isEmailToBanker) {
    if (isEmailToLead && isEmailToBanker) {
      return '2/2';
    } else if (isEmailToLead && !isEmailToBanker) {
      return '1/2';
    } else if (!isEmailToLead && isEmailToBanker) {
      return '1/2';
    } else {
      return '0/2';
    }
  }
}
