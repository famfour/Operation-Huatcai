import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import '../Model/response_model.dart/BucMortgareCompareModel.dart';

class BucMortgageRepaymentCompareController extends GetxController {
  dynamic interestRateComparisonData;
  dynamic dataIQrateLowestBucPackage;
  //bankSubmissionOneBank
  var isExpanded = false.obs;
  // RxBool isExpanded = false.obs;
  RxBool isExpandedRates = true.obs;
  RxBool isExpandedKeyFeatures = true.obs;
  RxBool isExpandedBankSubsidy = true.obs;
  RxBool isExpandedBankPenalty = true.obs;

  var arrayModelCompare = <RatePackageComparisonModel>[].obs;

  getInterestRateComparisonModels() {
    debugPrint("getInterestRateComparisonModels");
    arrayModelCompare.clear();
    // debugPrint("getInterestRateComparisonModels Comparison ne ~~ ${interestRateComparisonData}");
    // debugPrint("getInterestRateComparisonModels ${loadDetailsDataResult}");
    // debugPrint("getInterestRateComparisonModels Loan details ${loadDetailsDataResult["Loan Details"]}");
    interestRateComparisonData.forEach((key, value) {
      var model = BucMortgareCompareModel.fromJson(value);
      var tatePackageComparisonModel =
          RatePackageComparisonModel(name: key, model: model);
      arrayModelCompare.add(tatePackageComparisonModel);
    });
    debugPrint("✅ arrayModelCompare have ${arrayModelCompare.length}");
  }
}
