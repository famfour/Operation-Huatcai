import 'package:get/get.dart';
import 'package:iqrate/Controller/acl_controller.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Service/url.dart';

class CalculatorsViewController extends GetxController {
  ACLController aclController = Get.find();
  onTapNewPurchase() {
    aclController.aclNavigationCheck(
      newPurchaseTotalApi,
      newPurchaseScreen,
    );
  }

  onTapRefinance() {
    aclController.aclNavigationCheck(
      refinanceTotalUrl,
      refinanceSavingsScreen,
    );
  }

  onTapMortgageRepayment() {
    aclController.aclNavigationCheck(
      mortgageCalculatorUrl,
      mortgageCalculatorScreen,
    );
  }

  onTapBUCMortageRepayment() {
    aclController.aclNavigationCheck(
      calculator_buc_mortgage_downpayment,
      bucMortgageRepaymentScreen,
    );
  }

  aclNavigationCheckBuyerStampDutyCalculator() {
    aclController.aclNavigationCheck(
      buyerStampDutyUrl,
      buyerStampDutyCalculatorScreen,
    );
  }

  aclNavigationCheckEquityLoanCalculator() {
    aclController.aclNavigationCheck(
      equityCalculatorUrl,
      equityCalculatorScreen,
    );
  }

  // onTapEquityLoan() {
  //   Get.toNamed(equityCalculatorScreen);
  // }

  aclNavigationCheckSellarStampDutynCalculator() {
    aclController.aclNavigationCheck(
      sellerStampDutyUrl,
      sellerStampDutyCalculatorScreen,
    );
  }
}
