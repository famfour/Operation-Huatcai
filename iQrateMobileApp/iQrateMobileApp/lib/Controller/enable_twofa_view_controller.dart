import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/login_controller.dart';
import 'package:iqrate/Controller/profile_controller.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Service/GetControllers.dart';
import 'package:iqrate/Service/core_services.dart';

import 'package:iqrate/Service/url.dart';

class Enable2FAViewController extends GetxController {
  late RxBool switchValue = false.obs;
  ProfileScreenController profileScreenController =
      Get.put(ProfileScreenController());

  @override
  void onInit() {
    // switchValue.value = profileScreenController
    //         .userData.value.isTwoFactorAuthenticationEnabled ??
    //     false;
    // switchValue.value = Get.arguments == null
    //     ? false
    //     : profileScreenController
    //         .userData.value.isTwoFactorAuthenticationEnabled!;
    switchValue.value = profileScreenController
                .userData.value.isTwoFactorAuthenticationEnabled ==
            null
        ? false
        : Get.arguments == null
            ? false
            : Get.arguments['isTwoFactorAuthEnabled'];
    log(switchValue.value.toString() + "*****************");
    super.onInit();
  }

  switchOnTap(value) async {
    log('message');
    switchValue.value = value;
    if (Get.arguments["isTwoFactorAuthEnabled"] != switchValue.value) {
      if (value == false) {
        Get.isDialogOpen ?? true
            ? const Offstage()
            : Get.dialog(
                const Center(
                  child: CircularProgressIndicator(),
                ),
                barrierDismissible: false);
        await CoreService()
            .putWithAuthNoBody(url: baseUrl + disable2FAUrl)
            .then((data) async {
          if (Get.isDialogOpen ?? false) Get.back();

          if (data != null) {
            if (data == 401) {
              bool success =
                  await CoreService().getNewAccessTokenWithRefreshToken();
              if (success) {
                switchOnTap(value);
              }
            } else {
              Get.back();

              profileScreenController.getProfileData();
              profileScreenController.userData.refresh();
              profileScreenController.profileDetails.refresh();

              Future.delayed(const Duration(milliseconds: 0), () {
                FocusManager.instance.primaryFocus!.unfocus();
                Fluttertoast.showToast(
                  timeInSecForIosWeb: 3,
                  msg: data['message'],
                  backgroundColor: Colors.green,
                  toastLength: Toast.LENGTH_LONG,
                  gravity: ToastGravity.BOTTOM,
                  textColor: Colors.white,
                  fontSize: 20.0,
                );
              });
            }
          } else {
            Future.delayed(const Duration(milliseconds: 0), () {
              FocusManager.instance.primaryFocus!.unfocus();
              Fluttertoast.showToast(
                timeInSecForIosWeb: 3,
                msg: "Some error occured",
                backgroundColor: Colors.red,
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.BOTTOM,
                textColor: Colors.white,
                fontSize: 20.0,
              );
            });
          }
        });
      }
    }
  }

  onPressEnable2fa() {
    SignInController signInController =
        GetControllers.shared.getSignInController();
    signInController.isAppleLogin2FAEnable = false;
    signInController.isGoogleLogin2FAEnable = false;
    signInController.redirectFromLogin.value = false;

    Get.toNamed(twoFactorQuestion);
  }
}
