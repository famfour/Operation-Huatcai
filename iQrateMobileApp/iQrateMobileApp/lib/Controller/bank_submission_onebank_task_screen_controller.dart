import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Model/response_model.dart/bank_submission_banks_list_response_model.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';

class BankSubmissionOneBankTaskScreenController extends GetxController {
  Rx<BankSubmissionBanksListResponseModel>
      bankSubmissionBanksListResponseModel =
      BankSubmissionBanksListResponseModel().obs;
  Rx<BankSubmissionOneBank> bankSubmissionOneBank = BankSubmissionOneBank().obs;
  int leadId = Get.arguments[1];
  int index = Get.arguments[2];

  @override
  void onInit() {
    updateBankTasksStatus(leadId.toString(), index);
    super.onInit();
  }

  updateBankTasksStatus(String leadId, int index) async {
    debugPrint(baseUrl + banksubmissionGetBanksUrl + leadId);
    try {
      Timer(const Duration(seconds: 0), () {
        Get.isDialogOpen ?? true
            ? const Offstage()
            : Get.dialog(
                const Center(
                  child: CircularProgressIndicator(),
                ),
                barrierDismissible: false);
      });
      var data = await CoreService()
          .getWithAuth(url: baseUrl + banksubmissionGetBanksUrl + leadId);
      if (data != null) {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            updateBankTasksStatus(leadId, index);
          }
        } else {
          bankSubmissionBanksListResponseModel.value =
              BankSubmissionBanksListResponseModel.fromJson(data);
          bankSubmissionOneBank.value = bankSubmissionBanksListResponseModel
              .value.bankSubmissionOneBank![index];
          if (Get.isDialogOpen ?? false) Get.back();
        }
      } else {
        if (Get.isDialogOpen ?? false) Get.back();
      }
    } catch (e) {
      if (Get.isDialogOpen ?? false) Get.back();
      debugPrint(e.toString());
    }
  }
}
