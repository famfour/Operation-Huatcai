import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Router/route_constants.dart';

class EmailAddressVerificationScreenController extends GetxController {
  late TextEditingController emailVerificationOtpController;

  @override
  void onInit() {
    emailVerificationOtpController = TextEditingController(); //initialize otp text field controller
    super.onInit();
  }

  onSubmit() async {
    Get.toNamed(newPasswordScreen); //navigate to new password screen
  }
}
