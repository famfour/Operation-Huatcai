import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Model/response_model.dart/seller_stamp_duty_response_model.dart';
import 'package:iqrate/Model/send_model.dart/seller_stamp_duty_send_model.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';

class SellerStampDutyCalculatorController extends GetxController {
  RxInt calculated = 0.obs;
  late TextEditingController sellingPriceController;
  late TextEditingController sellingDateController;
  RxString? maximumSellingPrice = "0".obs;

  @override
  void onInit() {
    sellingPriceController = TextEditingController();
    sellingDateController = TextEditingController();
    super.onInit();
  }

  clearFields() {
    sellingPriceController.clear();
    sellingDateController.clear();
  }

  calculate() {
    // final DateTime date = DateTime.tryParse(sellingDateController.text)!;
    // final DateFormat formatter = DateFormat('dd-MM-yyyy');
    // final String formatted = formatter.format(date);
    // debugPrint(formatted);
    // if (date.isAfter(DateTime.now())) {
    //   print('object');
    // } else {
    //   print('object11');
    // }
    // debugPrint('*****');
    // debugPrint(DateTime.now().day.toString());
    // debugPrint(sellingDateController.text.split('-').reversed.join('-'));
    // debugPrint(
    //     DateTime.tryParse(sellingDateController.text.replaceAll('-', '/'))
    //         .toString());

    if (sellingPriceController.text.isEmpty &&
        sellingDateController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "All fields are mandatory",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (sellingPriceController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter the selling price",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (sellingDateController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: 'Please enter the "Date of option excercised"',
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (DateTime.tryParse(
            sellingDateController.text.split('-').reversed.join('-'))!
        .isAfter(DateTime.now())) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Date cannot be in the future",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else {
      onPressCalculate();
    }
  }

  onPressCalculate() async {
    // if (calculated.value == 0) {
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);
    SellerStampDutyCalculatorSendModel sellerStampDutyCalculatorSendModel =
        SellerStampDutyCalculatorSendModel(
      sellingPrice: sellingPriceController.text.replaceAll(',', ''),
      date: sellingDateController.text,
    );
    var data = await CoreService().postWithAuthForCalculators(
        url: baseUrl + sellerStampDutyUrl,
        body: sellerStampDutyCalculatorSendModel.toJson());
    if (data == null) {
      if (Get.isDialogOpen ?? false) Get.back();
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          onPressCalculate();
        }
      } else {
        var result = SellerStampDutyResponseModel.fromJson(data);
        if (result.sellerStampDuty != null) {
          if (Get.isDialogOpen ?? false) Get.back();
          calculated.value = 1;

          debugPrint(result.sellerStampDuty.toString());
          maximumSellingPrice!.value = result.sellerStampDuty.toString();
        } else {
          if (Get.isDialogOpen ?? false) Get.back();
        }
      }
    }
    // } else {
    //   calculated.value = 0;
    //   clearFields();
    //   maximumSellingPrice!.value = "0";
    // }
  }
}
