// ignore_for_file: avoid_function_literals_in_foreach_calls

import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/generate_packages_controller.dart';
import 'package:iqrate/Model/response_model.dart/bank_forms_response_model.dart';

import 'package:iqrate/Model/response_model.dart/refinance_new_lead_package_response_model_v2.dart';
import 'package:iqrate/Service/GetControllers.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';
import 'package:iqrate/Utils/ItemCheckBoxModel.dart';
import '../Views/co_broke/LeadItemModel.dart';

class SelectedPackagesController extends GetxController {
  var coBrokerData = LeadItemModel(clients: []);
  var isCoBroker = false;
  var isExpand = false.obs;
  RxList selectedPackageListModel = <Fixed>[].obs;

  // * Integer value to show or not show filter or which filter to show
  var showFilterConditionalVariable =
      0.obs; //! 0; not show, 1, show filter package, 2 show export, 3: circle

  // List<int> selectedPackages = [];
  RxBool isGeneratedPackage = false.obs;
  RxList<int> selectedPackgesForGenerateckages = <int>[].obs;

  //! Fetched from rates view controller starts here
  Rx<BankFormsResponseModel> bankResponseModel = BankFormsResponseModel().obs;

  var is200k = false
      .obs; //* Used in checkbox value for if the user is willing to place a minimum of 200k fresh fund
  var is3year = false
      .obs; //*Used in checkBox value for intention to sell property within 3 years

  //* Selected bank
  var selectedBankDataSourceV2 = <
      ItemCheckBoxModel>[]; //* Selected banks list displayed in the UI in chips format
  var bankDataSourceV2 = <ItemCheckBoxModel>[];
  RxList<int> bankId = <int>[].obs; //* List of bank ids
  RxList<String> bankName = <String>[].obs; //* List of Bank names

  // * Selected property type
  var selectedProperTypeDataSourceV2 = <ItemCheckBoxModel>[];

  // * Loan amount
  var loanAmountController =
      TextEditingController(); //*Loan amount text editing controller

  //* Loan type
  List<String> loanTypeKeyDataSource = <String>[];
  RxInt loanTypeSelectedIndex = 0.obs;

  //* Rate category
  List<String> rateCategoryDataSource = ['Pending', 'Submitted'].obs;
  RxInt rateCategorySelectedIndex = 0.obs;

  //* Property status
  List<String> properStatusKeyDataSource = <String>[];
  RxInt properStatusSelectedIndex = 0.obs;

  //! Fetched from rates view controller ends here

  var leadID = "";

  GeneratePackagesController generatePackagesController =
      GetControllers.shared.getGeneratePackagesController();

  @override
  void onInit() {
    super.onInit();

    debugPrint("isCoBroker $isCoBroker");
  }

  Future<void> getSelectedPackages(String leadID) async {
    debugPrint("getPackages:");

    // Get.isDialogOpen ?? true
    //     ? const Offstage() //Used for avoiding the dialog to be open when the user is already on the next screen.
    //     : Get.dialog(
    //         const Center(
    //             child:
    //                 CircularProgressIndicator()), //Used for showing the loading dialog.
    //         barrierDismissible:
    //             false); //Used for preventing the user to dismiss the dialog.

    var data = await CoreService().getWithAuth(
        url: baseUrl +
            selectedPackagesUrl +
            leadID); //Send the otp to the server.

    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          getSelectedPackages(leadID);
        }
      } else {
        List<dynamic> result = data;

        selectedPackageListModel.clear();

        /*result.forEach((element) {
        lowestPackageListModel.add(Results.fromJson(element));x
      });*/

        for (int i = 0; i < result.length; i++) {
          selectedPackageListModel.add(Fixed.fromJson(result.elementAt(i)));
        }

        generatePackagesController.selectedPackgesForGenerateckages.clear();

        selectedPackageListModel.forEach((item) {
          generatePackagesController.selectedPackgesForGenerateckages
              .add(item.id!);
        });

        if (Get.isDialogOpen ?? false) Get.back();
      }
    }
  }

  Future<void> getSelectedPackagesWhenExapnded(String leadID) async {
    debugPrint("getPackages:");

    Get.isDialogOpen ?? true
        ? const Offstage() //Used for avoiding the dialog to be open when the user is already on the next screen.
        : Get.dialog(
            const Center(
                child:
                    CircularProgressIndicator()), //Used for showing the loading dialog.
            barrierDismissible:
                false); //Used for preventing the user to dismiss the dialog.

    var data = await CoreService().getWithAuth(
        url: baseUrl +
            selectedPackagesUrl +
            leadID); //Send the otp to the server.

    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          getSelectedPackages(leadID);
        }
      } else {
        List<dynamic> result = data;

        selectedPackageListModel.clear();

        /*result.forEach((element) {
        lowestPackageListModel.add(Results.fromJson(element));x
      });*/

        for (int i = 0; i < result.length; i++) {
          selectedPackageListModel.add(Fixed.fromJson(result.elementAt(i)));
        }

        generatePackagesController.selectedPackgesForGenerateckages.clear();

        selectedPackageListModel.forEach((item) {
          generatePackagesController.selectedPackgesForGenerateckages
              .add(item.id!);
        });

        if (Get.isDialogOpen ?? false) Get.back();
      }
    }
  }

  //* check generated package if completed or not
  Future<void> deleteGeneratedPackage(String packageID) async {
    debugPrint("deleteGeneratedPackage::: ");

    Get.isDialogOpen ?? true
        ? const Offstage() //Used for avoiding the dialog to be open when the user is already on the next screen.
        : Get.dialog(
            const Center(
                child:
                    CircularProgressIndicator()), //Used for showing the loading dialog.
            barrierDismissible:
                false); //Used for preventing the user to dismiss the dialog.

    var data = await CoreService().deleteWithAuth(
      url: baseUrl + leadPackageDeleteUrl + leadID + "/$packageID",
    ); //Send the otp to the server.

    if (Get.isDialogOpen ?? false) Get.back();

    if (data == null) {
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          deleteGeneratedPackage(packageID);
        }
      } else if (data == 204) {
        debugPrint('********************');
        getSelectedPackages(leadID);
      } else {
        getSelectedPackages(leadID);

        Future.delayed(const Duration(seconds: 5), () {
          if (Get.isDialogOpen ?? false) Get.back();
        });
      }
    }
  }
}
