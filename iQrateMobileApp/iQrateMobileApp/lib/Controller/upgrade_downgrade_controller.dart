import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/profile_controller.dart';
import 'package:iqrate/Model/response_model.dart/upgrade_downgrade_response_model.dart';
import 'package:iqrate/Model/send_model.dart/create_subscription_send_model.dart';
import 'package:iqrate/Router/route_constants.dart';

import '../Service/core_services.dart';
import '../Service/url.dart';

class UpgradeDowngradeController extends GetxController {
  ProfileScreenController profileScreenController = Get.find();
  onDetachPaymentButtonPressed(String planId, String planType) async {
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);
    /*UpgradeDowngradeSendModel model = UpgradeDowngradeSendModel(
      priceId: planId,
    );*/

    CreateSubscriptionSendModel model = CreateSubscriptionSendModel(
      customerId: profileScreenController.userData.value.stripeCustomerId,
      priceId: planId,
      collectionMethod: "charge_automatically",
      product: planType,
      promoCode: '',
    );

    var data = await CoreService().postWithAuth(
      url: baseUrl +
          upgradeDowngradeUrl +
          profileScreenController.userData.value.stripeCustomerId.toString(),
      body: model.toJson(),
    );
    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please try again later",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          onDetachPaymentButtonPressed(planId, planType);
        }
      } else {
        var result = UpgradeDowngradeResponseModel.fromJson(data);
        if (result.id != null) {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Subscription successfully changed",
              backgroundColor: Colors.green,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });

          Get.offAllNamed(loginScreen);
          //paymentDetailsViewController.clearPaymentDetailsFields();

        } else {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Please try again later",
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }
      }
    }
  }
}
