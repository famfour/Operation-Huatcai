// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:iqrate/Controller/profile_controller.dart';
// import 'package:iqrate/Model/send_model.dart/remove_payment_send_model.dart';
// import 'package:iqrate/Service/core_services.dart';
// import 'package:iqrate/Service/url.dart';

// class RemovePaymentMethodController extends GetxController {
//   final ProfileScreenController profileScreenController = Get.find();
//   removePaymentButtonPressed() async {
//     Get.isDialogOpen ?? true
//         ? const Offstage()
//         : Get.dialog(
//             const Center(
//               child: CircularProgressIndicator(),
//             ),
//             barrierDismissible: false);
//     RemovePaymentMethodSendModel model = RemovePaymentMethodSendModel(
//       customerId: profileScreenController.userData.value.stripeCustomerId,
//       paymentMethodId:
//           profileScreenController.userData.value.stripePaymentMethodId,
//     );
//     await CoreService()
//         .postWithAuth(url: baseUrl + removePaymentUrl, body: model.toJson());
//   }
// }
