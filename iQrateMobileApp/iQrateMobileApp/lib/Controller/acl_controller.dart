import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/profile_controller.dart';
import 'package:iqrate/Model/response_model.dart/profile_response_model.dart';

class ACLController extends GetxController {
  late ProfileScreenController profileScreenController;
  List accessType = [];
  @override
  void onInit() {
    profileScreenController = Get.find();
    super.onInit();
  }

  aclNavigationCheck(String url, String toNamedRoute) {
    debugPrint(url);
    for (var i = 0;
        i < profileScreenController.userData.value.aclData!.length;
        i++) {
      debugPrint(profileScreenController.userData.value.aclData![i].keys
          .toString()
          .contains(url)
          .toString());
      if (profileScreenController.userData.value.aclData![i].keys
              .toString()
              .contains(url) &&
          profileScreenController
                  .userData.value.aclData![i].values.first.permission ==
              Permission.ALLOW) {
        Get.toNamed(toNamedRoute);
        break;
      } else if (profileScreenController.userData.value.aclData![i].keys
              .toString()
              .contains(url) &&
          profileScreenController
                  .userData.value.aclData![i].values.first.permission !=
              Permission.ALLOW) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "This is a restricted feature, please upgrade your plan",
            backgroundColor: Colors.white,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.black,
            fontSize: 20.0,
          );
        });
        break;
      } else {
        continue;
      }
    }
  }

  aclPutMethodCheck(String url, Function functionToExecute) {
    for (var i = 0;
        i < profileScreenController.userData.value.aclData!.length;
        i++) {
      if (profileScreenController.userData.value.aclData![i].keys
              .toString()
              .contains(url) &&
          profileScreenController
                  .userData.value.aclData![i].values.first.permission ==
              Permission.ALLOW) {
        functionToExecute();
        break;
      } else if (profileScreenController.userData.value.aclData![i].keys
              .toString()
              .contains(url) &&
          profileScreenController
                  .userData.value.aclData![i].values.first.permission !=
              Permission.ALLOW) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "This is a restricted feature, please upgrade your plan",
            backgroundColor: Colors.white,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.black,
            fontSize: 20.0,
          );
        });
        break;
      } else {
        continue;
      }
    }
  }
}
