// ignore_for_file: avoid_print, prefer_typing_uninitialized_variables

import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/leads_view_controller.dart';
import 'package:iqrate/Model/response_model.dart/bank_forms_response_model.dart';
import 'package:iqrate/Model/response_model.dart/more_rates_response_model.dart';
import 'package:iqrate/Model/response_model.dart/refinance_new_lead_package_response_model_v2.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Service/GetControllers.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';
import 'package:iqrate/Utils/ItemCheckBoxModel.dart';
import 'package:logger/logger.dart';

// import 'package:multi_select_flutter/multi_select_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

import '../Model/response_model.dart/kv_response_model.dart';
import '../Model/response_model.dart/list_all_packages_response_model_v2.dart';
import '../Model/send_model.dart/filter_rate_package_sent_model.dart';

class RatesViewController extends GetxController {
  // This is used in expanding a card (Only one card can be expanded at a time).
  var selectedIndex = RxInt(-1);
  RxBool isExpanded = false.obs;
  RxBool isExpandedRates = true.obs;
  RxBool isExpandedKeyFeatures = true.obs;
  RxBool isExpandedBankSubsidy = true.obs;
  RxBool isExpandedBankPenalty = true.obs;

  var numberBankSelected = 0;

  //for Dropdown bank selected
  // List<OneBankForm> bankDataSource = [];
  // List<OneBankForm> bankDataSourceV2 = [];
  var bankDataSourceV2 = <ItemCheckBoxModel>[];
  var selectedBankDataSourceV2 = <ItemCheckBoxModel>[];

  RxList<String> bankName = <String>[].obs;

  // List<MultiSelectItem> = <MultiSelectItem>[].obs;

  // List<MultiSelectItem<OneBankForm>> _items;
  // var bankItems = <MultiSelectItem<OneBankForm>>[].obs;

  RxList<int> bankId = <int>[].obs;
  Rx<BankFormsResponseModel> bankResponseModel = BankFormsResponseModel().obs;

  // RxList<String> banksSelectedValues = <String>[].obs;
  // RxList<OneBankForm?> banksSelectedValues = <OneBankForm?>[].obs;
  // var selectedBank = <OneBankForm>[];
  // var selectedBank = <OneBankForm>[].obs;
  //for dropdown proerty type
  // RxList<String> properTyeNameDataSource = <String>[].obs;
  // var properTyeNameKeyDataSource = <String>[];
  // RxList<String> properTyeNameSelectedValues = <String>[].obs;

  var properTypeDataSourceV2 = <ItemCheckBoxModel>[];
  var selectedProperTypeDataSourceV2 = <ItemCheckBoxModel>[];

  getSelectedProperTypeDataSourceV2() {
    selectedProperTypeDataSourceV2.clear();
    for (var i = 0; i < properTypeDataSourceV2.length; i++) {
      var a = properTypeDataSourceV2[i];
      if (a.isChecked) {
        selectedProperTypeDataSourceV2.add(a);
      }
    }
  }

  getSelectedBankDataSourceV2() {
    selectedBankDataSourceV2.clear();
    for (var i = 0; i < bankDataSourceV2.length; i++) {
      var a = bankDataSourceV2[i];
      if (a.isChecked) {
        selectedBankDataSourceV2.add(a);
      }
    }
  }

//for dropdown loan type
  RxList<String> loanTypeDataSource = <String>[].obs;
  List<String> loanTypeKeyDataSource = <String>[];
  RxInt loanTypeSelectedIndex = 0.obs;

  // property_status
  RxList<String> properStatusDataSource = <String>[].obs;
  List<String> properStatusKeyDataSource = <String>[];
  RxInt properStatusSelectedIndex = 0.obs;

  //rate_category
  List<String> rateCategoryDataSource = ['Pending', 'Submitted'].obs;
  List<KvValueStoreResponseModel> kvStoreValues = <KvValueStoreResponseModel>[];
  RxInt rateCategorySelectedIndex = 0.obs;
  List rateTypeDataSource = [];

  var is200k = false.obs;
  var is3year = false.obs;

  var loanAmountController = TextEditingController();

  // var bankSelected = [];
  // This is used for expanding the card

  var moreRates = <Fixed>[].obs;

  // var listAllPackagesResponseModel = ListAllPackagesResponseModel().obs;
  var listData = <PackageModel>[].obs;
  var isLoadingAPI = false;
  var isWantToFilterApiCall = false;
  var next = "";
  var isLasted = false;

  var showFilter =
      0.obs; // 0; not show, 1, show filter package, 2 show export, 3: circle

  var selectedRateTypeDataSourceV2 = <ItemCheckBoxModel>[];
  var rateTypeDataSourceV2 = <ItemCheckBoxModel>[];

  var selectedLoanTypeDataSourceV2 = <ItemCheckBoxModel>[];
  var loanTypeDataSourceV2 = <ItemCheckBoxModel>[];

  var selectedPropertyStatusDataSourceV2 = <ItemCheckBoxModel>[];
  var propertyStatusDataSourceV2 = <ItemCheckBoxModel>[];

  @override
  void onInit() {
    /*Timer(const Duration(milliseconds: 1000), () {
      getMoreRates("770");
    });*/

    // getMoreRates("770");

    super.onInit();
    loadData();
  }

  loadData() async {
    await callAPIGetBankList();
    await callAPIGetKVStoreList();
    // resetFilter();
    await callAPIGetListAllPackages();

    getRateTypeDropdown();
    getLoanTypeDropdown();
    getPropertyStatusDropdown();
  }

  void expandingCard(int index) {
    selectedIndex.value = index;
    checkIfExpanded(index);
  }

  // Reseting the value of the slectedIndex variable hence cosing the expansion section of the card
  void reset() {
    selectedIndex.value = -1;
  }

  // To check if the card is expanded or not
  bool checkIfExpanded(int index) {
    if (selectedIndex.value == index) {
      isExpanded.value = true;
      return isExpanded.value;
    } else {
      return isExpanded.value;
    }
  }

  callAPiRefresh() {
    next = "";
    isLasted = false;
    showFilter.value = 0;
    // selectedBank.value = [];
    selectedBankDataSourceV2.clear();
    selectedProperTypeDataSourceV2.clear();
    // properTyeNameSelectedValues.value = [];
    rateCategorySelectedIndex.value = 0;
    properStatusSelectedIndex.value = 0;
    loanTypeSelectedIndex.value = 0;
    loanAmountController.text = "";
    listData.clear();
    properTypeDataSourceV2.clear();
    callAPIGetListAllPackages();
  }

  resetFilterDefault() {
    isWantToFilterApiCall = false;
    debugPrint("resetFilterDefault");
    // selectedBank.value = [];
    for (var i = 0; i < bankDataSourceV2.length; i++) {
      bankDataSourceV2[i].isChecked = false;
    }
    selectedBankDataSourceV2.clear();
    for (var i = 0; i < properTypeDataSourceV2.length; i++) {
      properTypeDataSourceV2[i].isChecked = false;
    }
    selectedProperTypeDataSourceV2.clear();

    for (var i = 0; i < propertyStatusDataSourceV2.length; i++) {
      properTypeDataSourceV2[i].isChecked = false;
    }
    selectedPropertyStatusDataSourceV2.clear();

    for (var i = 0; i < properTypeDataSourceV2.length; i++) {
      properTypeDataSourceV2[i].isChecked = false;
    }
    selectedProperTypeDataSourceV2.clear();

    for (var i = 0; i < rateTypeDataSourceV2.length; i++) {
      rateTypeDataSourceV2[i].isChecked = false;
    }
    selectedRateTypeDataSourceV2.clear();

    for (var i = 0; i < loanTypeDataSourceV2.length; i++) {
      loanTypeDataSourceV2[i].isChecked = false;
    }
    selectedLoanTypeDataSourceV2.clear();

    // properTyeNameSelectedValues.value = [];
    rateCategorySelectedIndex.value = 0;
    properStatusSelectedIndex.value = 0;
    loanTypeSelectedIndex.value = 0;
    loanAmountController.text = "";
    is200k.value = false;
    is3year.value = false;
    showFilter.value = 3; //show reset loading
    // showFilter.value = 1;
  }

  getBanksValues() async {
    var data = await CoreService().getWithAuth(url: baseUrl + getBanksUrl);
    if (data != null) {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          getBanksValues();
        }
      } else {
        bankResponseModel.value = BankFormsResponseModel.fromJson(data);
        bankResponseModel.value.results?.forEach((element) {
          if (element.name != null) {
            bankName.add(element.name!);
            bankId.add(element.id!);
          }
        });
      }
    }
  }

  Future<void> getMoreRates(String leadID) async {
    debugPrint("getMoreRates::: leadID:: " + leadID);

    Get.isDialogOpen ?? true
        ? const Offstage() //Used for avoiding the dialog to be open when the user is already on the next screen.
        : Get.dialog(
            const Center(
                child:
                    CircularProgressIndicator()), //Used for showing the loading dialog.
            barrierDismissible:
                false); //Used for preventing the user to dismiss the dialog.

    var data = await CoreService().getWithAuth(
        url: baseUrl + allRatesApi + leadID); //Send the otp to the server.

    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          getMoreRates(leadID);
        }
      } else {
        log("getMoreRates::: " + jsonEncode(data));

        var result = MoreRatesResponseModel.fromJson(data);

        moreRates.value = result.results!;

        if (Get.isDialogOpen ?? false) Get.back();

        // Get.to(() => const MoreRatesView());
        Get.toNamed(moreRatesScreen, arguments: [leadID]);
      }
    }
  }

  Future<void> callAPIGetListAllPackages() async {
    isLoadingAPI = true;
    debugPrint("callAPIGetListAllPackages:::");
    showFilter.value = 0; //off popup!

    var endPoint = baseUrl + customerBankPackages;
    if (next != "") endPoint = next;
    debugPrint("endPoint $endPoint");

    try {
      // var data = await CoreService()
      //     .getWithAuthV2(url: endPoint); //Send the otp to the server.

      //String query = "?page=1&page_size=50";
      String query = "";

      List<String> arrayBankSelect = getIDFromBankStringSelected();
      List<String> arrayRateTypeSelect = getIDSelectedRateTypeDataSourceV2();
      List<String> arrayLoanTypeSelect = getIDSelectedLoanTypeDataSourceV2();
      List<String> arrayPropertyStatusSelect =
          getIDSelectedPropertyStatusDataSourceV2();
      List<String> arrayPropertyTypeSelect =
          getArrayIDFromPropertyTypeSelected();

      print(arrayBankSelect);
      print("rateCategoryDataSource $rateCategoryDataSource");

      var data;

      if (isWantToFilterApiCall) {

        query = "?";

        if (arrayBankSelect.isNotEmpty) {
          String arrayValue = arrayBankSelect.join(",");
          query += "&bank_id=$arrayValue";
        }

        if (arrayRateTypeSelect.isNotEmpty) {
          String arrayValue = arrayRateTypeSelect.join(",");

          query += "&rate_type=$arrayValue";
        }

        if (arrayLoanTypeSelect.isNotEmpty) {
          String arrayValue = arrayLoanTypeSelect.join(",");

          query += "&loan_category=$arrayValue";
        }

        if (arrayPropertyStatusSelect.isNotEmpty) {
          String arrayValue = arrayPropertyStatusSelect.join(",");

          query += "&property_status=$arrayValue";
        }

        if (arrayPropertyTypeSelect.isNotEmpty) {
          String arrayValue = arrayPropertyTypeSelect.join(",");

          query += "&property_types=$arrayValue";
        }

        query += "&sell_property_in_3_years=${is3year.value}";
        query += "&has_200k=${is200k.value}";
        query += "&loan_amount=${loanAmountController.text}";
        //query += "&rate_category=${rateCategoryDataSource[rateCategorySelectedIndex.value].toLowerCase()}";

        /*var param = FilterRatePackageSentModel(
            bankId: arrayBankSelect,
            has200k: is200k.value,
            loanAmount: loanAmountController.text,
            loanCategory: getArrayIDFromLoanTypeSelected(),
            rateType :  getIDSelectedRateTypeDataSourceV2(),
            rateCategory: [
              rateCategoryDataSource[rateCategorySelectedIndex.value]
                  .toLowerCase()
            ],
            propertyTypes: getArrayIDFromPropertyTypeSelected(),
            propertyStatus: getArrayIDFromPropertyStatusSelected(),
            sellPropertyIn3Years: is3year.value,);

        Logger().d(param.toJson());*/

        data = await CoreService().getWithAuthV2(url: endPoint + query);
      } else {
        data = await CoreService().getWithAuthV2(url: endPoint + query);
      }

      isLoadingAPI = false;
      showFilter.value = 0; //off popup!

      if (data == null) {
        debugPrint("❌null callAPIGetListAllPackages}");
      } else {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            callAPIGetListAllPackages();
          }
        } else {
          var result = ListAllPackagesResponseModel.fromJson(data);
          // listAllPackagesResponseModel.value = result;
          //check page 1
          if (next == "") {
            print("is1st");
            listData.value = result.results ?? [];
          } else {
            print("not is1st");
            listData.addAll(result.results ?? []);
          }

          //check isLasted
          //isLasted = result.next == null ? true : false;

          next = result.next ?? "";

          if(next.isEmpty){
            isLasted = true;
          }else{
            isLasted = false;
          }

          print("isLasted $isLasted");
          print("next $next");
          // listData.value = listAllPackagesResponseModel.
          debugPrint("✅ok callAPIGetListAllPackages");
          // showFilter.value = 0;//off popup!
        }
      }
    } catch (e) {
      isLoadingAPI = false;
      debugPrint("something went wrong");
    }
  }

  getIDSelectedRateTypeDataSourceV2() {
    List<String> listIDFromRateTypeStringSelected = [];
    for (var i = 0; i < selectedRateTypeDataSourceV2.length; i++) {
      var selectedValues = selectedRateTypeDataSourceV2[i];
      listIDFromRateTypeStringSelected.add(selectedValues.value);
    }
    return listIDFromRateTypeStringSelected;
  }

  Future<void> callAPIGetListAllPackagesMoreRates() async {
    debugPrint("callAPIGetListAllPackages:::");
    showFilter.value = 0; //off popup!

    try {
      // var data = await CoreService()
      //     .getWithAuthV2(url: endPoint); //Send the otp to the server.

      var arrayBankSelect = getIDFromBankStringSelected();
      print(arrayBankSelect);
      print("rateCategoryDataSource $rateCategoryDataSource");
      var param = FilterRatePackageSentModel(
        bankId: arrayBankSelect,
        has200k: is200k.value,
        loanAmount: loanAmountController.text,
        loanCategory: getArrayIDFromLoanTypeSelected(),
        rateCategory: [
          rateCategoryDataSource[rateCategorySelectedIndex.value].toLowerCase()
        ],
        propertyTypes: getArrayIDFromPropertyTypeSelected(),
        propertyStatus: getArrayIDFromPropertyStatusSelected(),
        sellPropertyIn3Years: is3year.value,
      );
      Logger().d(param.toJson());
      var data = await CoreService().getWithAuthV2(
          url: baseUrl + customerBankPackages, query: param.toJson());
      // if (data != null) {
      //   var result = ListAllPackagesResponseModel.fromJson(data);
      //   listData.value = result.results ?? [];
      //   debugPrint("✅ok callAPIGetListAllPackages");
      //   showFilter.value = 0; //off popup!
      // }
      isLoadingAPI = false;
      showFilter.value = 0; //off popup!
      if (data == null) {
        debugPrint("❌null callAPIGetListAllPackages}");
      } else {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            callAPIGetListAllPackagesMoreRates();
          }
        } else {
          var result = MoreRatesResponseModel.fromJson(data);
          // listAllPackagesResponseModel.value = result;
          moreRates.value = result.results!;
        }
      }
    } catch (e) {
      isLoadingAPI = false;
      debugPrint("something went wrong");
    }
  }

  loadMore() async {
    if (showFilter.value == 0) {
      if (isLoadingAPI == false) {
        if (isLasted) {
          print("none load more ");
        } else {
          // print("isLastPage::::::::::::::::::::::$isLastPage");
          callAPIGetListAllPackages();
        }
      } else {
        print("data loading! not allow call!");
      }
    }
  }

  getBankData() {
    // bankDataSource = bankResponseModel.value.results ?? [];
    // bankResponseModel.value.results?.forEach((element) {
    //   if (element.name != null) {
    //     bankName.add(element.name!);
    //     bankId.add(element.id!);
    //   }
    // });
    bankDataSourceV2.clear();
    bankResponseModel.value.results?.forEach((element) {
      if (element.name != null) {
        var itemNew = ItemCheckBoxModel(
            name: element.name ?? "",
            isChecked: false,
            value: element.id!.toString());
        bankDataSourceV2.add(itemNew);
      }
    });
  }

  Future<void> callAPIGetBankList() async {
    debugPrint("callAPIGetBankList:::");
    var data = await CoreService().getWithAuthV2(
        url: baseUrl + getBanksUrl, query: {"page_size": "1000"});
    if (data != null) {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          callAPIGetBankList();
          getRateTypeDropdown();
        }
      } else {
        bankResponseModel.value = BankFormsResponseModel.fromJson(data);
        getBankData();
      }
    }
  }

  Future<void> callAPIExportPackage() async {
    debugPrint("callAPIExportPackage:::");
    var arrayBankSelect = getIDFromBankStringSelectedToExport();
    var param = {"packages": arrayBankSelect};
    if (arrayBankSelect.isNotEmpty) {
      Logger().d(param);
      var data = await CoreService()
          .postWithAuth(url: baseUrl + bankPackagesExportToPdf, body: param);
      if (data != null) {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            callAPIExportPackage();
          }
        } else {
          debugPrint("✅ok callAPIExportPackage $data");
          var url = data["url"];
          // ignore: deprecated_member_use
          await canLaunch(url) ? await launch(url) : debugPrint("error");
        }
      }
    }
  }

  void showDownloadProgress(received, total) {
    if (total != -1) {
      print((received / total * 100).toStringAsFixed(0) + '%');
    }
  }

  getCountItemSelected() {
    var a = 0;
    for (var i = 0; i < listData.length; i++) {
      var count = listData[i].isChecked ?? false ? 1 : 0;
      a += count;
    }
    return a;
  }

  Future<void> callAPIGetKVStoreList() async {
    debugPrint("callAPIGetKVStoreList:::");
    var data = await CoreService().getWithAuthV2(url: baseUrl + getKvValuesUrl);
    if (data != null) {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          callAPIGetKVStoreList();
        }
      } else {
        // rateCategoryList = [];
        // statusDataSource = [];
        rateCategoryDataSource = [];
        properStatusDataSource.value = [];
        // properTyeNameDataSource.value = [];
        properStatusKeyDataSource = [];
        List<dynamic> list = data;
        for (var element in list) {
          // statusDataSource = ['Active', 'Inactive'];//hard code because now APi dont have in KVI store
          //get rate cat

          //loan_category
          if (element["code"] == "loan_category") {
            element["value"].forEach((key, value) {
              print("add value");
              loanTypeDataSource.add(value);
              loanTypeKeyDataSource.add(key);
            });
          }

          //rate_category
          if (element["code"] == "rate_category") {
            element["value"].forEach((key, value) {
              rateCategoryDataSource.add(value);
            });
          }
          //rate type
          if (element["code"] == "rate_type") {
            element["value"].forEach((key, value) {
              rateTypeDataSource.add(value);
            });
          }
          if (element["code"] == "property_type") {
            element["value"].forEach((key, value) {
              print("add value");
              // properTyeNameDataSource.add(value);
              // properTyeNameKeyDataSource.add(key);
              //v2
              var item =
                  ItemCheckBoxModel(name: value, value: key, isChecked: false);
              properTypeDataSourceV2.add(item);
            });
          }
          //
          if (element["code"] == "property_status") {
            element["value"].forEach((key, value) {
              properStatusDataSource.add(value);
              properStatusKeyDataSource.add(key);
            });
          }
        }
        //property_type
      }
    }
  }

  // getIDFromBankStringSelected() {
  //   List<String> listIDFromBankStringSelected = [];
  //   for (var i = 0; i < selectedBank.length; i++) {
  //     var selectedValues = selectedBank[i];
  //     listIDFromBankStringSelected.add(selectedValues.id.toString());
  //   }
  //   return listIDFromBankStringSelected;
  // }

  getIDFromBankStringSelected() {
    List<String> listIDFromBankStringSelected = [];
    for (var i = 0; i < selectedBankDataSourceV2.length; i++) {
      var selectedValues = selectedBankDataSourceV2[i];
      listIDFromBankStringSelected.add(selectedValues.value);
    }
    return listIDFromBankStringSelected;
  }

  // //property_types
  getArrayIDFromPropertyStatusSelected() {
    return [properStatusKeyDataSource[properStatusSelectedIndex.value]];
  }

  getArrayIDFromLoanTypeSelected() {
    return [loanTypeKeyDataSource[loanTypeSelectedIndex.value]];
  }

  // getArrayIDFromPropertyTypeSelected() {
  //   List<String> listIDFromPropertyTypeSelected = [];
  //   // print("listIDFromPropertyTypeSelected.length ${listIDFromPropertyTypeSelected}");
  //   for (var i = 0; i < properTyeNameSelectedValues.length; i++) {
  //     var selectedValues = properTyeNameSelectedValues[i];
  //     for (var j = 0; j < properTyeNameDataSource.length; j++) {
  //       var found = properTyeNameDataSource[j];
  //       if (selectedValues == found) {
  //         listIDFromPropertyTypeSelected.add(properTyeNameKeyDataSource[j]);
  //       }
  //     }
  //   }
  //   return listIDFromPropertyTypeSelected;
  // }

  getArrayIDFromPropertyTypeSelected() {
    List<String> listIDFromPropertyTypeSelected = [];
    // print("listIDFromPropertyTypeSelected.length ${listIDFromPropertyTypeSelected}");
    for (var i = 0; i < selectedProperTypeDataSourceV2.length; i++) {
      var item = selectedProperTypeDataSourceV2[i];
      listIDFromPropertyTypeSelected.add(item.value);
    }
    return listIDFromPropertyTypeSelected;
  }

  List<String> getIDFromBankStringSelectedToExport() {
    List<String> listIDFromBankStringSelected = [];
    for (var i = 0; i < listData.length; i++) {
      var found = listData[i];
      if (found.isChecked ?? false) {
        listIDFromBankStringSelected.add(found.id.toString());
      }
    }
    return listIDFromBankStringSelected;
  }

//get property status key from list

  getSelectedRateTypeDataSourceV2() {
    selectedRateTypeDataSourceV2.clear();
    for (var i = 0; i < rateTypeDataSourceV2.length; i++) {
      var a = rateTypeDataSourceV2[i];
      if (a.isChecked) {
        selectedRateTypeDataSourceV2.add(a);
      }
    }
  }

  LeadsViewController leadsViewController =
      GetControllers.shared.getLeadsViewController();

  getRateTypeDropdown() {
    rateTypeDataSourceV2
        .clear(); // Clearing the banks list if has any banks added in it
    for (var element in leadsViewController.kvStoreValues) {
      if (element.code == "rate_type") {
        log('message');
        debugPrint("=====kvstore===== rate type" + element.value.toString());
        log('message');

        element.value.forEach((key, value) {
          var itemNew = ItemCheckBoxModel(
              name: value ?? "", isChecked: false, value: key ?? "");
          rateTypeDataSourceV2.add(itemNew);
          // dropDownPropertyTypeKey.add(key);
          //debugPrint("array_key" + key );
        });
      }
    }
  }

  getLoanTypeDropdown() {
    loanTypeDataSourceV2
        .clear(); // Clearing the banks list if has any banks added in it
    for (var element in leadsViewController.kvStoreValues) {
      if (element.code == "loan_category") {
        log('message');
        debugPrint("=====kvstore===== rate type" + element.value.toString());
        log('message');

        element.value.forEach((key, value) {
          var itemNew = ItemCheckBoxModel(
              name: value ?? "", isChecked: false, value: key ?? "");
          loanTypeDataSourceV2.add(itemNew);
          // dropDownPropertyTypeKey.add(key);
          //debugPrint("array_key" + key );
        });
      }
    }
  }

  getSelectedLoanTypeDataSourceV2() {
    selectedLoanTypeDataSourceV2.clear();
    for (var i = 0; i < loanTypeDataSourceV2.length; i++) {
      var a = loanTypeDataSourceV2[i];
      if (a.isChecked) {
        selectedLoanTypeDataSourceV2.add(a);
      }
    }
  }

  getIDSelectedLoanTypeDataSourceV2() {
    List<String> listIDFromRateTypeStringSelected = [];
    for (var i = 0; i < selectedLoanTypeDataSourceV2.length; i++) {
      var selectedValues = selectedLoanTypeDataSourceV2[i];
      listIDFromRateTypeStringSelected.add(selectedValues.value);
    }
    return listIDFromRateTypeStringSelected;
  }

  getPropertyStatusDropdown() {
    propertyStatusDataSourceV2
        .clear(); // Clearing the banks list if has any banks added in it
    for (var element in leadsViewController.kvStoreValues) {
      if (element.code == "property_status") {
        log('message');
        debugPrint("=====kvstore===== rate type" + element.value.toString());
        log('message');

        element.value.forEach((key, value) {
          var itemNew = ItemCheckBoxModel(
              name: value ?? "", isChecked: false, value: key ?? "");
          propertyStatusDataSourceV2.add(itemNew);
          // dropDownPropertyTypeKey.add(key);
          //debugPrint("array_key" + key );
        });
      }
    }
  }

  getSelectedPropertyStatusDataSourceV2() {
    selectedPropertyStatusDataSourceV2.clear();
    for (var i = 0; i < propertyStatusDataSourceV2.length; i++) {
      var a = propertyStatusDataSourceV2[i];
      if (a.isChecked) {
        selectedPropertyStatusDataSourceV2.add(a);
      }
    }
  }

  getIDSelectedPropertyStatusDataSourceV2() {
    List<String> listIDFromRateTypeStringSelected = [];
    for (var i = 0; i < selectedPropertyStatusDataSourceV2.length; i++) {
      var selectedValues = selectedPropertyStatusDataSourceV2[i];
      listIDFromRateTypeStringSelected.add(selectedValues.value);
    }
    return listIDFromRateTypeStringSelected;
  }
}
