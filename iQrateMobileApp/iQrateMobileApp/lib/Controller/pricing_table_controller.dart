import 'package:get/get.dart';
import 'package:iqrate/Router/route_constants.dart';

class PricingTableScreenController extends GetxController {
  var isAnnulPackageSelected = true.obs; // default annual package selected

  onToggle(bool toggleValue) async {
    isAnnulPackageSelected.value = toggleValue; //package change
  }

  onSubmit() {
    Get.toNamed(dashboard);
  }

  onSkip() {
    Get.toNamed(dashboard);
  }
}
