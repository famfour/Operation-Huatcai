// ignore_for_file: non_constant_identifier_names

import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/profile_controller.dart';
import 'package:iqrate/Controller/subscription_options_controller.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/container_styles.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Model/response_model.dart/bank_forms_response_model.dart';
import 'package:iqrate/Model/response_model.dart/create_lead_response_model.dart';
import 'package:iqrate/Model/response_model.dart/fetch_lead_list_response_model.dart';
import 'package:iqrate/Model/response_model.dart/kv_response_model.dart';
import 'package:iqrate/Model/send_model.dart/create_lead_client_send_model.dart';
import 'package:iqrate/Model/send_model.dart/create_lead_send_model.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';
import 'package:iqrate/Views/co_broke/LeadItemModel.dart';
import 'package:iqrate/Widgets/form_field_title.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/secondary_button.dart';

class LeadsViewController extends GetxController {
  late RxDouble windowHeight;
  late RxDouble windowWidth;
  late RxInt selectedIndex;
  RxBool changeToAllLeads = false.obs;
  String leadType = "others";
  FetchLeadListResponseModel fetchLeadListResponseModel =
      FetchLeadListResponseModel();
  RxList listOfLeads = [].obs;
  RxBool allAplicantPDPAApproved = true.obs;

  dynamic fetchLeadListResponse;
  var listOfLeadsV2 = <LeadItemModel>[].obs;
  RxInt errorPopUpCondition = 0.obs; // 0
  RxBool pdpaStatusForAllApplicant = true.obs;

  // var listData = <LeadItemModel>[];
  RxBool showFilter = false.obs;
  RxInt? coBrokeVlue = 0.obs;
  RxBool enableContinueButton = false.obs;
  List<String> coBrokeOptionList = [
    'Request Sent',
    'Request Received',
    'No Co Broke'
  ];
  RxList<String> slectedValues = <String>[].obs;
  Rx<BankFormsResponseModel> bankResponseModel = BankFormsResponseModel().obs;
  RxList<String> bankName = <String>[].obs;
  RxList<int> bankId = <int>[].obs;
  final RxList<String> leadStatus = <String>[
    'All',
    'Pending',
    'Won',
    'Submitted',
    'Rework',
    'Lost'
  ].obs; // Option 2
  RxString? selectedStatus = 'All'.obs;
  ScrollController scrollController = ScrollController();

  //create lead list
  TextEditingController emailController =
      TextEditingController(); //email text controller
  TextEditingController nameController =
      TextEditingController(); //name text controller
  TextEditingController phoneController =
      TextEditingController(); //phone text controller
  TextEditingController countryCodeController =
      TextEditingController(); //phone text controller

  TextEditingController countryCodeLabelController =
      TextEditingController(); //phone text controller

  TextEditingController dobController = TextEditingController(); //dob
  final formKey = GlobalKey<FormState>();

  TextEditingController filterLeadNameTextEditingController =
      TextEditingController(); //Filter section textediting controller for lead name

  ProfileScreenController profileScreenController =
      Get.put(ProfileScreenController());

  var isExpandItem = false.obs;

  List<KvValueStoreResponseModel> kvStoreValues = <KvValueStoreResponseModel>[];

  final SubscriptionOptionsController subscriptionOptionsController =
      Get.put(SubscriptionOptionsController());

  getListData(dynamic data) {
    var b = <LeadItemModel>[];
    var a = data['results'];
    if (a != null && a is List) {
      for (var i = 0; i < a.length; i++) {
        var c = LeadItemModel.fromJson(a[i]);
        b.add(c);
      }
    }
    return b;
  }

  @override
  onInit() {
    //subscriptionOptionsController.getPlanSummaryDetails();
    profileScreenController.getProfileData();

    selectedIndex = 0.obs;
    windowHeight = Get.height.obs;
    windowWidth = Get.width.obs;

    countryCodeController.text = "+65";
    countryCodeLabelController.text = "SG";
    scrollController.addListener(pagination);
    //create lead list
    emailController = TextEditingController(); //email text controller
    nameController = TextEditingController(); //name text controller
    phoneController = TextEditingController(); //phone text controller
    Timer(const Duration(seconds: 0), () {
      /*Get.isDialogOpen ?? true
          ? const Offstage()
          : Get.dialog(const Center(child: CircularProgressIndicator()),
              barrierDismissible: false);*/
      fetchLeads();
      getAndStoreKvValues();
      getBanksValues();
    });

    super.onInit();
  }

  clearFields() {
    emailController.clear();
    nameController.clear();
    phoneController.clear();
    // countryCodeController.text = '+65';
  }

  // Populate the lead form input fields for leadType yourself
  fillInputFieldsForYourselfLeadType() {
    emailController.text = profileScreenController.userData.value.email ??
        ''; //Set the login user email for leadtype ypurself
    nameController.text = profileScreenController.userData.value.fullName ??
        ''; // Set the login user name for the leadtype ypurself
    phoneController.text = profileScreenController.userData.value.mobile ??
        ''; //Set the login user phone number for leadtype yourself
    countryCodeController.text =
        '+${profileScreenController.userData.value.countryCode ?? 65}'; //Set the login user country code for leadtype yourself
    countryCodeLabelController.text =
        profileScreenController.userData.value.countryCodeLabel ??
            'SG'; //Set the login user country label for leadtype yourself
  }

  getAndStoreKvValues() async {
    /*Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
        const Center(
          child: CircularProgressIndicator(),
        ),
        barrierDismissible: false);*/
    var data = await CoreService().getWithAuth(url: baseUrl + getKvValuesUrl);
    /*if (Get.isDialogOpen ?? false) {
      Get.back();
    }*/
    if (data == null) {
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          getAndStoreKvValues();
        }
      } else {
        debugPrint(">>>>>>>>>>>>>>>>>>>>>>>>kv stores>>>>>>>>>>>>>>>>>>>>>>");

        List<dynamic> list = data;
        //debugPrint(">>>>>>kv stores jsonDecode length>>>>>>" + list.length.toString());
        //var result = KvValueStoreResponseModel.fromJson(data);
        for (var element in list) {
          //debugPrint(">>>>>>kv stores element.key>>>>>>" + element["key"].toString());
          kvStoreValues.add(KvValueStoreResponseModel(
              key: element["key"],
              code: element["code"],
              valueType: element["value_type"],
              value: element["value"]));
        }

        debugPrint(">>>>>>kv stores parse length>>>>>>" +
            kvStoreValues.length.toString());
      }
    }
  }

  getBanksValues() async {
    var data = await CoreService().getWithAuth(url: baseUrl + getBanksUrl);
    if (data != null) {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          getBanksValues();
        }
      } else {
        bankResponseModel.value = BankFormsResponseModel.fromJson(data);
        bankResponseModel.value.results?.forEach((element) {
          if (element.name != null) {
            bankName.add(element.name!);
            bankId.add(element.id!);
          }
        });
      }
    }
  }

  onTapCreateNowScreen() {
    selectedIndex.value = 1;
    windowHeight.value = windowHeight.value / 0.7;
    log();
  }

  onTapViewTutorial() {
    Get.toNamed(trainingScreen)!.then((value) {
      if (value == null) {
        enableContinueButton.value = true;
      }
    });
  }

  onTapContinueScreen() {
    selectedIndex.value = 1;
    //selectedIndex.value = 6;
    windowHeight.value = windowHeight.value / 0.8;
    log();
  }

  onTapApplyLoanOthersScreen() {
    selectedIndex.value = 3;
    windowHeight.value = windowHeight.value / 0.73;
    leadType = "others";
    log();
  }

  onTapApplyLoanSelfScreen() {
    selectedIndex.value = 8;
    leadType = "yourself";
    // clearFields();
    fillInputFieldsForYourselfLeadType();
  }

  onTapCoBrokeScreen() {
    selectedIndex.value = 7;
    leadType = "co_broke";
  }

  resetFilter() {
    filterLeadNameTextEditingController.clear();
    fetchLeads();
  }

  filterByLeadStatus(String leadStatus) async {
    listOfLeads.clear();
    listOfLeadsV2.clear();
    // fetchLeadListResponse = null;
    if (leadStatus == 'All') {
      fetchLeads();
    } else {
      debugPrint(baseUrl +
          leadListUrl +
          '?agent_applied=' +
          profileScreenController.userData.value.id.toString() +
          '&lead_status=$leadStatus');
      Get.isDialogOpen ?? true
          ? const Offstage()
          : Get.dialog(
              const Center(
                child: CircularProgressIndicator(),
              ),
              barrierDismissible: false);
      try {
        var data = await CoreService().getWithAuth(
            url: baseUrl +
                leadListUrl +
                '?agent_applied=' +
                profileScreenController.userData.value.id.toString() +
                '&lead_status=$leadStatus');
        if (data != null) {
          if (data == 401) {
            bool success =
                await CoreService().getNewAccessTokenWithRefreshToken();
            if (success) {
              filterByLeadStatus(leadStatus);
            }
          } else {
            fetchLeadListResponseModel =
                FetchLeadListResponseModel.fromJson(data);

            if (fetchLeadListResponseModel.count != 0) {
              if (fetchLeadListResponseModel.results!.isNotEmpty) {
                selectedIndex.value = 6;
                listOfLeads.addAll(fetchLeadListResponseModel.results!);
                listOfLeadsV2.addAll(getListLeadItem(data));
              }
              showFilter.value = false;
              Get.back();
            } else {
              // fetchLeadListResponseModel =
              //     FetchLeadListResponseModel.fromJson(data);
              //
              // listOfLeads.addAll(fetchLeadListResponseModel.results!);
              // listOfLeadsV2.addAll(data);
              showFilter.value = false;
              Get.back();
            }
          }
        }
      } catch (e) {
        debugPrint(e.toString());
      }
    }
  }

  getFilteredData(String leadName, String leadStatus) async {
    listOfLeads.clear();
    listOfLeadsV2.clear();

    debugPrint(baseUrl +
        leadListUrl +
        '?agent_applied=' +
        profileScreenController.userData.value.id.toString() +
        '&clients_name=$leadName' +
        '&lead_status=&co_broke=${coBrokeOptionList.elementAt(coBrokeVlue!.value).replaceAll(" ", "_").replaceAll("-", "_").toLowerCase()}');

    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);
    try {
      var data = await CoreService().getWithAuth(
          url: baseUrl +
              leadListUrl +
              '?agent_applied=' +
              profileScreenController.userData.value.id.toString() +
              '&clients_name=$leadName' +
              '&lead_status=&co_broke=${coBrokeOptionList.elementAt(coBrokeVlue!.value).replaceAll(" ", "_").toLowerCase()}');
      if (data != null) {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            getFilteredData(leadName, leadStatus);
          }
        } else {
          fetchLeadListResponseModel =
              FetchLeadListResponseModel.fromJson(data);
          if (fetchLeadListResponseModel.count != 0) {
            if (fetchLeadListResponseModel.results!.isNotEmpty) {
              selectedIndex.value = 6;
              listOfLeads.addAll(fetchLeadListResponseModel.results!);
              listOfLeadsV2.addAll(getListLeadItem(data));
            }
            showFilter.value = false;
            Get.back();
          } else {
            fetchLeadListResponseModel =
                FetchLeadListResponseModel.fromJson(data);
            listOfLeads.addAll(fetchLeadListResponseModel.results!);
            showFilter.value = false;
            Get.back();
          }
        }
      }
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  // onSignUpButtonTap() {
  //   if (emailController.text.isNotEmpty &&
  //       nameController.text.isNotEmpty &&
  //       phoneController.text.isNotEmpty) {
  //     debugPrint("submitted");
  //     onTapLeadCreateNowScreen();
  //   } else {
  //     Get.snackbar("Error", "Please fill all the fields",
  //         snackPosition: SnackPosition.TOP,
  //         backgroundColor: Colors.red,
  //         colorText: Colors.white);
  //   }
  // }

  //create lead
  onTapLeadCreateNowScreen() async {
    if (nameController.text.isEmpty &&
        phoneController.text.isEmpty &&
        emailController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter all mandatory fields",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return;
    }

    if (emailController.text.isNotEmpty &&
        nameController.text.isNotEmpty &&
        phoneController.text.isNotEmpty &&
        countryCodeController.text.isNotEmpty &&
        phoneController.text.length >= 8 &&
        emailController.text.isEmail) {
      if (fetchLeadListResponseModel.results != null) {
        /*switch (
            subscriptionOptionsController.subscriptionSummary.value.product) {
          case "Premium":
            if (leadList.value.results!.length >= 5) {
              Get.snackbar(
                  StringUtils.hasErrorTitle, "Lead creation limit exists!",
                  snackPosition: SnackPosition.TOP,
                  backgroundColor: Colors.red,
                  colorText: Colors.white);

              return;
            }
            break;

          case "Basic":
            if (leadList.value.results!.length >= 6) {
              Get.snackbar(
                  StringUtils.hasErrorTitle, "Lead creation limit exists!",
                  snackPosition: SnackPosition.TOP,
                  backgroundColor: Colors.red,
                  colorText: Colors.white);

              return;
            }
            break;
        }*/

        Get.isDialogOpen ?? true
            ? const Offstage()
            : Get.dialog(
                const Center(
                  child: CircularProgressIndicator(),
                ),
                barrierDismissible: false);

        await onCreateLead();
        log();
      } else {
        Get.isDialogOpen ?? true
            ? const Offstage()
            : Get.dialog(
                const Center(
                  child: CircularProgressIndicator(),
                ),
                barrierDismissible: false);

        await onCreateLead();
        log();
      }
    } else if (phoneController.text.length < 8) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter a valid mobile number",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (!emailController.text.isEmail) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter a valid Email",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    }
  }

  onTapBackPreLeadCreateScreen() {
    selectedIndex.value = 2;
    log();
  }

  onTapLeadCreateDoneScreen() async {
    selectedIndex.value = 6;
    fetchLeads();
    log();
  }

  onTapLeadPendingScreen() {
    selectedIndex.value = 6;
    log();
  }

  onTapManageLoan(Result data) {
    Get.toNamed(manageLoanScreen, arguments: data)!
        .then((value) => fetchLeads());
  }

  String drawerToken = "";
  String myDocumentID = "";
  String document_drawer_url = "";

  //debug info
  /*String drawerToken =
      "af1fb4f4-dd01-491f-9f3d-9cccb89e148a:1nsrUW:ic711N2xdFPHvqCkh6-x_g6lp-1dyolWxZ2PUjiio2k";
  String myDocumentID = "af1fb4f4-dd01-491f-9f3d-9cccb89e148a";*/

  onTapDocuments(Result data) {
    if (data.clients!.elementAt(0).pdpaStatus!) {
      myDocumentID = data.document_drawer_id!;
      document_drawer_url = data.document_drawer_url!;

      Get.toNamed(myDocumentsScreen,
          arguments: [data.document_drawer_id, data.document_drawer_url, data]);
    } else {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please verify PDPA first!",
          backgroundColor: Colors.white,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.black,
          fontSize: 20.0,
        );
      });
    }
  }

  var dropdownReasonType = ["Competitor"];
  var selectedReasonType = "Competitor".obs;

  void showMarkLeadLostAlert() {
    Get.defaultDialog(
        title: "",
        content: Container(
          padding: const EdgeInsets.only(left: 30, right: 30, bottom: 20),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 5.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Mark Lead as Lost?",
                        //textAlign: TextAlign.center,
                        style: TextStyles.leadsColorTextStyle,
                      ),
                      SizedBox(height: Get.height * 0.03),
                      Text(
                        "This step is irreversible and lead will no longer be editable. To confirm, please select reason:",
                        //textAlign: TextAlign.center,
                        style: TextStyles.leadsNormalTextStyle,
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: windowHeight * 0.02),
              const FormFieldTitle(title: 'Reason for Lost'),
              SizedBox(height: windowHeight * 0.01),
              Container(
                width: Get.width,
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
                decoration: BoxDecoration(
                    border: Border.all(
                        color: AppColors.formFieldBorderColor, width: 1.0),
                    borderRadius: const BorderRadius.all(Radius.circular(
                            10) //                 <--- border radius here
                        )),
                child: DropdownButtonHideUnderline(
                  child: DropdownButton(
                    hint: const Text('Select Type'),
                    // Not necessary for Option 1
                    value: selectedReasonType.value,
                    onChanged: (newValue) {
                      selectedReasonType.value = newValue.toString();
                    },
                    items: dropdownReasonType.map((location) {
                      return DropdownMenuItem(
                        child: Text(location),
                        value: location,
                      );
                    }).toList(),
                  ),
                ),
              ),
              SizedBox(height: Get.height * 0.04),
              PrimaryButton(
                windowHeight: Get.height,
                windowWidth: Get.width,
                buttonTitle: "Confirm",
                onPressed: () {},
              ),
              SizedBox(height: Get.height * 0.03),
              SecondaryButton(
                windowHeight: Get.height,
                windowWidth: Get.width,
                kGradientBoxDecoration:
                    ContainerStyles.kGradientBoxDecorationSecondaryButton,
                kInnerDecoration:
                    ContainerStyles.kInnerDecorationSecondaryButton,
                buttonTitle: 'Cancel',
                onPressed: () {
                  Get.back();
                },
              ),
            ],
          ),
        ));
  }

  // void showPopupCoBrokePdpaAppvored() {
  //   Get.defaultDialog(
  //       title: "",
  //       content: Container(
  //         padding: const EdgeInsets.only(left: 30, right: 30, bottom: 20),
  //         decoration: BoxDecoration(
  //           color: Colors.white,
  //           borderRadius: BorderRadius.circular(20),
  //         ),
  //         child: Column(
  //           crossAxisAlignment: CrossAxisAlignment.start,
  //           children: [
  //             SizedBox(
  //               child: Padding(
  //                 padding: const EdgeInsets.symmetric(horizontal: 5.0),
  //                 child: Column(
  //                   crossAxisAlignment: CrossAxisAlignment.start,
  //                   children: [
  //                     Text(
  //                       "Co-Broke",
  //                       //textAlign: TextAlign.center,
  //                       style: TextStyles.leadsColorTextStyle,
  //                     ),
  //                     SizedBox(height: Get.height * 0.03),
  //                     Text(
  //                       "IQrate partnered mortgage broker will close the deal for you. Referral fee will be co-shared at 50%.",
  //                       //textAlign: TextAlign.center,
  //                       style: TextStyles.leadsTextStyle,
  //                     ),
  //                   ],
  //                 ),
  //               ),
  //             ),
  //             SizedBox(height: Get.height * 0.04),
  //             PrimaryButton(
  //               windowHeight: Get.height,
  //               windowWidth: Get.width,
  //               buttonTitle: "Proceed",
  //               onPressed: () {},
  //             ),
  //             SizedBox(height: Get.height * 0.03),
  //             SecondaryButton(
  //               windowHeight: Get.height,
  //               windowWidth: Get.width,
  //               kGradientBoxDecoration:
  //                   ContainerStyles.kGradientBoxDecorationSecondaryButton,
  //               kInnerDecoration:
  //                   ContainerStyles.kInnerDecorationSecondaryButton,
  //               buttonTitle: 'Back',
  //               onPressed: () {
  //                 Get.back();
  //               },
  //             ),
  //           ],
  //         ),
  //       ));
  // }

  // void showPopupCoBrokePdpaNotAppvored() {
  //   Get.defaultDialog(
  //       title: "",
  //       content: Container(
  //         padding: const EdgeInsets.only(left: 30, right: 30, bottom: 20),
  //         decoration: BoxDecoration(
  //           color: Colors.white,
  //           borderRadius: BorderRadius.circular(20),
  //         ),
  //         child: Column(
  //           crossAxisAlignment: CrossAxisAlignment.start,
  //           children: [
  //             SizedBox(
  //               child: Padding(
  //                 padding: const EdgeInsets.symmetric(horizontal: 5.0),
  //                 child: Column(
  //                   crossAxisAlignment: CrossAxisAlignment.start,
  //                   children: [
  //                     Text(
  //                       "Co-Broke",
  //                       //textAlign: TextAlign.center,
  //                       style: TextStyles.leadsColorTextStyle,
  //                     ),
  //                     SizedBox(height: Get.height * 0.03),
  //                     Text(
  //                       "IQrate partnered mortgage broker will close the deal for you. Referral fee will be co-shared at 50%.",
  //                       //textAlign: TextAlign.center,
  //                       style: TextStyles.leadsTextStyle,
  //                     ),
  //                     SizedBox(height: Get.height * 0.03),
  //                     Row(
  //                       mainAxisAlignment: MainAxisAlignment.start,
  //                       crossAxisAlignment: CrossAxisAlignment.start,
  //                       children: [
  //                         const Icon(
  //                           Icons.report,
  //                           color: AppColors.kPrimaryColor,
  //                           size: 35,
  //                         ),
  //                         const SizedBox(
  //                           width: 10,
  //                         ),
  //                         Flexible(
  //                             child: Text(
  //                           "PDPA not approved. Please get lead to accept PDPA before proceeding.",
  //                           style: TextStyles.leadsTextStyle.copyWith(
  //                               color: AppColors.kPrimaryColor, fontSize: 14),
  //                           maxLines: 3,
  //                         ))
  //                       ],
  //                     )
  //                   ],
  //                 ),
  //               ),
  //             ),
  //             SizedBox(height: Get.height * 0.04),
  //             PrimaryButton(
  //               isDisable: true,
  //               windowHeight: Get.height,
  //               windowWidth: Get.width,
  //               buttonTitle: "Continue",
  //               onPressed: () {},
  //             ),
  //             SizedBox(height: Get.height * 0.03),
  //             SecondaryButton(
  //               windowHeight: Get.height,
  //               windowWidth: Get.width,
  //               kGradientBoxDecoration:
  //                   ContainerStyles.kGradientBoxDecorationSecondaryButton,
  //               kInnerDecoration:
  //                   ContainerStyles.kInnerDecorationSecondaryButton,
  //               buttonTitle: 'Back',
  //               onPressed: () {
  //                 Get.back();
  //               },
  //             ),
  //           ],
  //         ),
  //       ));
  // }

  // void showPopupCoBrokeUnable() {
  //   Get.defaultDialog(
  //       title: "",
  //       content: Container(
  //         padding: const EdgeInsets.only(left: 30, right: 30, bottom: 20),
  //         decoration: BoxDecoration(
  //           color: Colors.white,
  //           borderRadius: BorderRadius.circular(20),
  //         ),
  //         child: Column(
  //           crossAxisAlignment: CrossAxisAlignment.start,
  //           children: [
  //             SizedBox(
  //               child: Padding(
  //                 padding: const EdgeInsets.symmetric(horizontal: 5.0),
  //                 child: Column(
  //                   crossAxisAlignment: CrossAxisAlignment.start,
  //                   children: [
  //                     Text(
  //                       "Co-Broke",
  //                       //textAlign: TextAlign.center,
  //                       style: TextStyles.leadsColorTextStyle,
  //                     ),
  //                     SizedBox(height: Get.height * 0.03),
  //                     Text(
  //                       "IQrate partnered mortgage broker will close the deal for you. Referral fee will be co-shared at 50%.",
  //                       //textAlign: TextAlign.center,
  //                       style: TextStyles.leadsTextStyle,
  //                     ),
  //                     SizedBox(height: Get.height * 0.03),
  //                     Row(
  //                       mainAxisAlignment: MainAxisAlignment.start,
  //                       crossAxisAlignment: CrossAxisAlignment.start,
  //                       children: [
  //                         const Icon(
  //                           Icons.report,
  //                           color: AppColors.kPrimaryColor,
  //                           size: 35,
  //                         ),
  //                         const SizedBox(
  //                           width: 10,
  //                         ),
  //                         Flexible(
  //                             child: Text(
  //                           "Bank Submission has been initiated for lead. Unable to proceed with co-broke. Please complete your bank submission process",
  //                           style: TextStyles.leadsTextStyle.copyWith(
  //                               color: AppColors.kPrimaryColor, fontSize: 14),
  //                         ))
  //                       ],
  //                     )
  //                   ],
  //                 ),
  //               ),
  //             ),
  //             SizedBox(height: Get.height * 0.04),
  //             PrimaryButton(
  //               isDisable: true,
  //               windowHeight: Get.height,
  //               windowWidth: Get.width,
  //               buttonTitle: "Continue",
  //               onPressed: () {},
  //             ),
  //             SizedBox(height: Get.height * 0.03),
  //             SecondaryButton(
  //               windowHeight: Get.height,
  //               windowWidth: Get.width,
  //               kGradientBoxDecoration:
  //                   ContainerStyles.kGradientBoxDecorationSecondaryButton,
  //               kInnerDecoration:
  //                   ContainerStyles.kInnerDecorationSecondaryButton,
  //               buttonTitle: 'Back',
  //               onPressed: () {
  //                 Get.back();
  //               },
  //             ),
  //           ],
  //         ),
  //       ));
  // }

  void log() {
    debugPrint(selectedIndex.value.toString());
    debugPrint(windowHeight.value.toString());
    debugPrint(windowWidth.value.toString());
  }

  void onProceedToLeadCreation() {
    selectedIndex.value = 2;
    log();
    clearFields();
  }

  void onProceedToLeadCreationBack() {
    selectedIndex.value = 0;
    log();
  }

  void onTapApplyLoanForOthers() {
    selectedIndex.value = 4;
    log();
    clearFields();
  }

  void onTapApplyLoanForOthersBack() {
    selectedIndex.value = 2;
    log();
  }

  void onTapCoBrokeContinue() {
    selectedIndex.value = 4;
    log();
    clearFields();
  }

  void onTapCoBrokeContinueBack() {
    selectedIndex.value = 2;
    log();
  }

  void onTapLoanForOthersContinue() {
    selectedIndex.value = 4;
    log();
  }

  void onTapLoanForOthersContinueBack() {
    selectedIndex.value = 2;
    log();
  }

  onCreateLead() async {
    debugPrint("data.toString()>>>>>");

    CreateLeadSendModel model = CreateLeadSendModel(
      leadType: leadType,
      agentApplied: profileScreenController.userData.value.id,
    );

    var data = await CoreService().postWithAuth(
      url: baseUrl + leadCreateUrl,
      body: model.toJson(),
    );

    debugPrint(data.toString());

    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          onCreateLead();
        }
      } else {
        var result = CreateLeadResponseModel.fromJson(data);
        if (result.id != null) {
          leadClientCreate(result.id);

          listOfLeads.refresh();
        } else {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Lead creation failed",
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }
      }
    }
  }

  leadClientCreate(id) async {
    CreateLeadClientSendModel model = CreateLeadClientSendModel(
      lead: id,
      name: nameController.text,
      email: emailController.text,
      phoneNumber: phoneController.text,
      mainApplicant: true,
      nationality: "Singapore Citizens",
      countryCode: countryCodeController.text.replaceAll("+", ""),
    );

    var data = await CoreService().postWithAuth(
      url: baseUrl + leadClientCreateUrl,
      body: model.toJson(),
    );
    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          leadClientCreate(id);
        }
      } else {
        var result = CreateLeadResponseModel.fromJson(data);
        if (result.id != null) {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Lead successfully created",
              backgroundColor: Colors.green,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
          selectedIndex.value = 5;
          clearFields();
          //sendPDPAApproval(result.id.toString());
        } else {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Lead client creation failed",
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }
      }
    }
  }

  fetchLeads() async {
    listOfLeads.clear();
    listOfLeadsV2.clear();
    var data = await CoreService().getWithAuth(
        url: baseUrl +
            leadListUrl +
            "?agent_applied=" +
            profileScreenController.userData.value.id.toString() +
            '&page_size=1000');
    if (data == null) {
      if (Get.isDialogOpen ?? false) Get.back();
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          fetchLeads();
        }
      } else {
        fetchLeadListResponseModel = FetchLeadListResponseModel.fromJson(data);
        if (fetchLeadListResponseModel.count != 0) {
          if (fetchLeadListResponseModel.results!.isNotEmpty) {
            selectedIndex.value = 6;
            listOfLeads.addAll(fetchLeadListResponseModel.results!);
            listOfLeadsV2.addAll(getListLeadItem(data));
          }
          if (Get.isDialogOpen ?? false) Get.back();
        } else {
          if (Get.isDialogOpen ?? false) Get.back();
        }
      }
    }
  }

  getListLeadItem(dynamic data) {
    var a = <LeadItemModel>[];
    var jsonArray = data['results'];
    if (data != null && jsonArray.isNotEmpty) {
      for (var i = 0; i < jsonArray.length; i++) {
        var json = jsonArray[i];
        var b = LeadItemModel.fromJson(json);
        a.add(b);
      }
    }
    if (kDebugMode) {
      print("a have ${a.length}");
    }
    return a;
  }

  fetchMoreLeads() async {
    try {
      if (fetchLeadListResponseModel.next != null) {
        // Get.isDialogOpen ?? true
        //     ? const Offstage()
        //     : Get.dialog(
        //         const Center(
        //           child: CircularProgressIndicator(),
        //         ),
        //         barrierDismissible: false);
        var data = await CoreService().getWithAuth(
          url: fetchLeadListResponseModel.next,
        );
        if (Get.isDialogOpen ?? false) Get.back();
        if (data == null) {
          if (Get.isDialogOpen ?? false) Get.back();
        } else {
          if (data == 401) {
            bool success =
                await CoreService().getNewAccessTokenWithRefreshToken();
            if (success) {
              fetchMoreLeads();
            }
          } else {
            fetchLeadListResponseModel =
                FetchLeadListResponseModel.fromJson(data);
            if (fetchLeadListResponseModel.count != 0) {
              if (fetchLeadListResponseModel.results!.isNotEmpty) {
                selectedIndex.value = 6;
                listOfLeads.addAll(fetchLeadListResponseModel.results!);
                listOfLeadsV2.addAll(data);
              }
              if (Get.isDialogOpen ?? false) Get.back();
            } else {
              if (Get.isDialogOpen ?? false) Get.back();
            }
          }
        }
      }
    } catch (e) {
      if (Get.isDialogOpen ?? false) Get.back();
      debugPrint(e.toString());
    }
  }

  pagination() {
    if ((scrollController.position.pixels ==
        scrollController.position.maxScrollExtent)) {
      //add api for load the more data according to new page
      fetchMoreLeads();
    }
  }

  sendPDPAApproval(String clientID) async {
    var data = await CoreService().getWithAuth(
      url: baseUrl + pdpaTokenGenerateUrl + clientID,
    );
    if (data != null) {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          sendPDPAApproval(clientID);
        }
      }
      debugPrint(data.toString());
    } else {
      debugPrint(data.toString());
    }
  }

  applyForCoBroke(int leadId) async {
    await CoreService().postWithAuth(
        url: baseUrl + requestForCoBrokeUrl + leadId.toString(),
        body: {}).then((value) async {
      // debugPrint('**********************');
      // debugPrint(value.toString());
      // debugPrint('**********************');
      if (value == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          applyForCoBroke(leadId);
        }
      } else {
        if (value['message'] == 'Co-Broke request sent') {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: value['message'],
              backgroundColor: Colors.green,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }
        fetchLeads();
      }
    });
  }

  // Future<Result> getOneLeadDetails(String leadId) async {

  //   var data =
  //       await CoreService().getWithAuth(url: baseUrl + oneLeadUrl) + leadId;
  //   if (data == null) {
  //   } else {
  //     var result = Result.fromJson(data);
  //   }
  //   return result;
  // }

  bool checkIfPDPACompletedForAll(List<Client>? clients) {
    if (listOfLeads.isNotEmpty) {
      for (int i = 0; i < clients!.length; i++) {
        if (!clients[i].pdpaStatus!) {
          pdpaStatusForAllApplicant.value = false;
        } else {
          pdpaStatusForAllApplicant.value = true;
        }
      }
    }
    return pdpaStatusForAllApplicant.value;
  }
}
