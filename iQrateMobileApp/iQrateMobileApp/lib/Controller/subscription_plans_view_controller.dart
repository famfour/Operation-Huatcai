import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
// import 'package:iqrate/Controller/profile_controller.dart';
import 'package:iqrate/Model/response_model.dart/subscription_plans_response_model.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Service/core_services.dart';
// import 'package:iqrate/Service/string_utils.dart';
import 'package:iqrate/Service/url.dart';
import 'package:iqrate/Utils/config.dart';

class SubscriptionPlansViewController extends GetxController {
  RxInt selectedValue =
      0.obs; //Plan type(Basic , Premium, Premium+ selected by user)
  RxInt finalSubscriptionTermSelected = 0.obs;
  RxString planTermPriceDaily = 'Enjoy Basic Plan for Free'.obs;
  RxString planTermPriceMonthly = '0'.obs;
  RxString planTermPriceYearly = '0'.obs;
  var arguments = Get.arguments;
  RxInt subscribedPlan = 0.obs;
  RxString subscribedPlanAmount = ''.obs;

  late RxBool checkedValue = true.obs;

  late SubscriptionPlansModel subscriptionPlansModel;
  Map<String, dynamic>? paymentIntentData;
  // final ProfileScreenController profileScreenController = Get.find();

  @override
  void onInit() {
    Timer(const Duration(seconds: 0), () {
      Get.isDialogOpen ?? true
          ? const Offstage()
          : Get.dialog(const Center(child: CircularProgressIndicator()),
              barrierDismissible: false);
      getPlanDetails();
    });
    // getPlanDetails();
    super.onInit();
  }

  String getPlanType() {
    if (selectedValue.value == 0) {
      return 'Basic';
    } else if (selectedValue.value == 1) {
      return 'Premium';
    } else if (selectedValue.value == 2) {
      return 'Premium+';
    } else {
      return 'Premium+';
    }
  }

  String getPlanId() {
    if (selectedValue.value == 0) {
      return subscriptionPlansModel
          .subscriptionPlans[2].plan[1].planId //basic plan id
          .toString();
    } else if (selectedValue.value == 1 &&
        finalSubscriptionTermSelected.value == 1) {
      return subscriptionPlansModel
          .subscriptionPlans[1].plan[1].planId // Premiuim Monthly id
          .toString();
    } else if (selectedValue.value == 1 &&
        finalSubscriptionTermSelected.value == 2) {
      return subscriptionPlansModel
          .subscriptionPlans[1].plan[0].planId // premium yearly id
          .toString();
    } else if (selectedValue.value == 2 &&
        finalSubscriptionTermSelected.value == 1) {
      return subscriptionPlansModel
          .subscriptionPlans[0].plan[1].planId // premium+ month id
          .toString();
    } else if (selectedValue.value == 2 &&
        finalSubscriptionTermSelected.value == 2) {
      return subscriptionPlansModel
          .subscriptionPlans[0].plan[0].planId // Premium+ yearly id
          .toString();
    } else {
      return subscriptionPlansModel.subscriptionPlans[2].plan[0].planId
          .toString();
    }
  }

  String getPlanTerm() {
    if (finalSubscriptionTermSelected.value == 1) {
      return 'Monthly';
    } else if (finalSubscriptionTermSelected.value == 2) {
      return 'Yearly';
    } else {
      return 'Monthly';
    }
  }

  getPlanDetails() async {
    subscribedPlan.value = arguments[0];
    selectedValue.value = arguments[0];
    // debugPrint('************');
    // debugPrint(selectedValue.value.toString());
    // debugPrint('************');
    subscribedPlanAmount.value = arguments[1];
    // debugPrint(subscribedPlanAmount.value.toString());
    // debugPrint(selectedValue.value.toString());
    // debugPrint((int.parse(subscribedPlanAmount.value) / 100).toString() + '0');
    // debugPrint('************');

    var data = await CoreService()
        .getWithAuth(url: baseUrl + getsubscriptionPlanDetails);
    if (Get.isDialogOpen ?? false) Get.back();
    if (data != null) {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          getPlanDetails();
        }
      } else {
        subscriptionPlansModel = SubscriptionPlansModel.fromJson(data);
        planTermPriceDaily.value = 'Enjoy Basic Plan for Free';
        planTermPriceMonthly.value =
            '\$ ${AppConfig.extraDecimalAdd(subscriptionPlansModel.subscriptionPlans[0].plan[1].amount.toString())} per Month';
        planTermPriceYearly.value =
            '\$ ${AppConfig.extraDecimalAdd(subscriptionPlansModel.subscriptionPlans[0].plan[0].amount.toString())} per Year';
        onTapUpdatePlan(selectedValue.value);
      }
    } else {
      Get.back();
      // Get.snackbar(StringUtils.error, "${data.message},",
      //     snackPosition: SnackPosition.TOP);
    }
  }

  onTapUpdatePlan(int selectedPlan) {
    selectedValue.value = selectedPlan;
    if (selectedPlan == 0) {
      for (int i = 0;
          i < subscriptionPlansModel.subscriptionPlans.length;
          i++) {
        if (subscriptionPlansModel.subscriptionPlans[i].product == "Basic") {
          planTermPriceDaily.value = 'Enjoy Basic Plan for Free';
          // debugPrint(
          //     '***************************************************************');
          // debugPrint(subscriptionPlansModel.subscriptionPlans[0].plan[2].amount
          //     .toString());
          // planTermPriceMonthly.value =
          //     '\$ ${subscriptionPlansModel.subscriptionPlans[0].plan[2].amount} per Month';
          // planTermPriceYearly.value =
          //     '\$ ${subscriptionPlansModel.subscriptionPlans[i].plan[1].amount} per Year';
        }
      }
    } else if (selectedPlan == 1) {
      for (int i = 0;
          i < subscriptionPlansModel.subscriptionPlans.length;
          i++) {
        if (subscriptionPlansModel.subscriptionPlans[i].product == "Premium") {
          // planTermPriceDaily.value =
          //     '\$ ${AppConfig.extraDecimalAdd(subscriptionPlansModel.subscriptionPlans[i].plan[0].amount.toString())} per Day';
          planTermPriceMonthly.value =
              '\$ ${AppConfig.extraDecimalAdd(subscriptionPlansModel.subscriptionPlans[i].plan[1].amount.toString())} per Month'; // plan[3] has the $200 price
          planTermPriceYearly.value =
              '\$ ${AppConfig.extraDecimalAdd(subscriptionPlansModel.subscriptionPlans[i].plan[0].amount.toString())} per Year'; // plan[2] has the $2400 price
        }
      }
    } else {
      for (int i = 0;
          i < subscriptionPlansModel.subscriptionPlans.length;
          i++) {
        if (subscriptionPlansModel.subscriptionPlans[i].product == "Premium+") {
          // planTermPriceDaily.value =
          //     '\$ ${AppConfig.extraDecimalAdd(subscriptionPlansModel.subscriptionPlans[i].plan[0].amount.toString())} per Day';
          planTermPriceMonthly.value =
              '\$ ${AppConfig.extraDecimalAdd(subscriptionPlansModel.subscriptionPlans[i].plan[1].amount.toString())} per Month';
          planTermPriceYearly.value =
              '\$ ${AppConfig.extraDecimalAdd(subscriptionPlansModel.subscriptionPlans[i].plan[0].amount.toString())} per Year';
        }
      }
    }
  }

  String getAmount() {
    if (selectedValue.value == 0) {
      return '0';
    } else if (selectedValue.value == 1 &&
        finalSubscriptionTermSelected.value == 1) {
      return subscriptionPlansModel
          .subscriptionPlans[1].plan[1].amount //Premium Monthly
          .toString();
    } else if (selectedValue.value == 1 &&
        finalSubscriptionTermSelected.value == 2) {
      return subscriptionPlansModel
          .subscriptionPlans[1].plan[0].amount // Premium Yearly
          .toString();
    } else if (selectedValue.value == 2 &&
        finalSubscriptionTermSelected.value == 1) {
      return subscriptionPlansModel
          .subscriptionPlans[0].plan[1].amount //premium+ montly
          .toString();
    } else if (selectedValue.value == 2 &&
        finalSubscriptionTermSelected.value == 2) {
      return subscriptionPlansModel
          .subscriptionPlans[0].plan[0].amount //premium+ yearly
          .toString();
    } else {
      return subscriptionPlansModel
          .subscriptionPlans[2].plan[1].amount //Basic montly
          .toString();
    }
  }

  subscriptionTermSelection(int selectedSubscriptionTerm) {
    finalSubscriptionTermSelected.value = selectedSubscriptionTerm;
  }

  onTapSubscribeNow({
    required String amount,
    required String planTerm,
    required String planType,
    required String planId,
  }) async {
    debugPrint("=======================");
    debugPrint("planType::" + planType.toString().toLowerCase());
    debugPrint("amount::" + amount);
    debugPrint("planTerm::" + planTerm);
    debugPrint("planId::" + planId);
    debugPrint("=======================");
    // if (planType.toString().toLowerCase() == 'basic') {
    //   Get.isDialogOpen ?? true
    //       ? const Offstage()
    //       : Get.dialog(
    //           const Center(
    //             child: CircularProgressIndicator(),
    //           ),
    //           barrierDismissible: false);
    //   var data = await CoreService()
    //       .postWithAuth(url: baseUrl + createSubscriptionUrl, body: {
    //     "customer_id": profileScreenController.userData.value.stripeCustomerId,
    //     "price_id": "price_1L2ZuSFSgVYpzNjn3DenTtpu",
    //     "collection_method": "charge_automatically",
    //     "product": "basic"
    //   });
    //   if (data == null) {
    //     if (Get.isDialogOpen ?? false) Get.back();
    //   } else {
    //     if (Get.isDialogOpen ?? false) Get.back();
    //     Get.offAllNamed(dashboard);
    //     Get.snackbar(
    //       StringUtils.success,
    //       'You have been upgraded',
    //       colorText: Colors.white,
    //       duration: const Duration(seconds: 3),
    //       backgroundColor: Colors.green,
    //     );
    //   }
    // } else {
    Get.toNamed(paymentDetailsScreen, arguments: {
      "amount": amount,
      "planTerm": planTerm,
      "planType": planType.toString().toLowerCase(),
      "planId": planId
    });
    // }
  }
}
