import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/profile_controller.dart';
import 'package:iqrate/Model/response_model.dart/contact_us_response_model.dart';
import 'package:iqrate/Model/send_model.dart/contact_us_send_model.dart';

import '../Service/core_services.dart';

import '../Service/url.dart';

class ContactUsContoller extends GetxController {
  late TextEditingController nameController;
  late TextEditingController emailController;
  late TextEditingController messageController;
  final ProfileScreenController profileScreenController = Get.find();

  @override
  void onInit() {
    nameController = TextEditingController(
        text: profileScreenController.userData.value.fullName);
    emailController = TextEditingController(
        text: profileScreenController.userData.value.email);
    messageController = TextEditingController();
    super.onInit();
  }

  clearFields() {
    messageController.clear();
  }

  onSubmit() {
    if (nameController.text.isEmpty &&
        emailController.text.isEmpty &&
        messageController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "All fileds are mandatory",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (nameController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Your name is required",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (emailController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Your email is required",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (messageController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Your message is required",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (messageController.text.trim().isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Your message is required",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else {
      onMessageSubmit();
    }
  }

  onMessageSubmit() async {
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);
    ContactUsSendModel contactUsSendModel = ContactUsSendModel(
      name: nameController.text,
      email: emailController.text,
      message: messageController.text,
    );
    var data = await CoreService().putWithAuth(
        url: baseUrl + contactUsUrl, body: contactUsSendModel.toJson());
    if (data == null) {
      if (Get.isDialogOpen ?? false) Get.back();
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          onMessageSubmit();
        }
      } else {
        var result = ContactUsResponseModel.fromJson(data);
        if (result.data != null) {
          if (Get.isDialogOpen ?? false) Get.back();
          clearFields();

          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg:
                  "We have received your message and will get in touch with you shortly",
              backgroundColor: Colors.green,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        } else {
          if (Get.isDialogOpen ?? false) Get.back();

          // Get.snackbar(StringUtils.error, StringUtils.error,
          //     snackPosition: SnackPosition.TOP, backgroundColor: Colors.white);
        }
      }
    }
  }
}
