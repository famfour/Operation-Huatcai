import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:iqrate/Model/response_model.dart/MortgageRepaymentTableModel.dart';

import 'dart:convert';

InterestRateComparisonModel interestRateComparisonModelFromJson(String str) =>
    InterestRateComparisonModel.fromJson(json.decode(str));

// String interestRateComparisonModelJson(InterestRateComparisonModel data) => json.encode(data.toJson());

class InterestRateComparisonModel {
  InterestRateComparisonModel({
    this.monthlyInstallment = "",
    this.totalPayment = "",
    this.totalPrincipal = '',
    this.totalInterest = "",
  });

  dynamic monthlyInstallment;
  dynamic totalPayment;
  dynamic totalPrincipal;
  dynamic totalInterest;

  factory InterestRateComparisonModel.fromJson(Map<String, dynamic> json) =>
      InterestRateComparisonModel(
        monthlyInstallment: json["Monthly Installment"],
        totalPayment: json["Total Payment"],
        totalPrincipal: json["Total Principal"],
        totalInterest: json["Total Interest"],
      );

  Map<String, dynamic> toJson() => {
        "Monthly Installment": monthlyInstallment,
        "Total Payment": totalPayment,
        "Total Principal": totalPrincipal,
        "Total Interest": totalInterest,
      };
}

// List<Map<String, dynamic>> getArrayContentLoanDetails() {
//   List<Map<String, dynamic>> array = [];
//   controller.loadDetailsDataResult["Loan Details"].forEach((key, value) {
//     // print("add value");
//     loanDetailsItem(key, value);
//     var a = {"name": key, "value": value};
//     array.add(a);
//   });
//   return array;
// }

// Widget buildListContentLoanDetails(dynamic array) => Column(
//       mainAxisAlignment: MainAxisAlignment.start,
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: [
//         for (var i = 0; i < array.length; i++)
//           loanDetailsItem(array[i]["name"], array[i]["value"])
//       ],
//     );
class MortgageRepaymentReportController extends GetxController {
  dynamic loadDetailsDataResult;
  // var isSummeryCheckBoxEnable = true.obs;
  // var isInterestRateCheckBoxEnable = true.obs;
  // var isMortgageRepaymentCheckBoxEnable = true.obs;
  dynamic modelCompare1;
  dynamic modelCompare2;

  var dataSourceA = <MortgageRepaymentTableModel>[]; //for table
  var dataSourceB = <MortgageRepaymentTableModel>[]; //for table

  var dataSourceTable1 =
      <MortgageRepaymentTableModel>[]; //for table year 1- 5 , total max 60 item
  var dataSourceTable2 = <
      MortgageRepaymentTableModel>[]; //for table year 6- 10 , total max 60 item
  var dataSourceTable3 = <
      MortgageRepaymentTableModel>[]; //for table year 11-15 , total max 60 item
  var dataSourceTable4 = <
      MortgageRepaymentTableModel>[]; //for table year 16- 20 , total max 60 item
  var dataSourceTable5 = <
      MortgageRepaymentTableModel>[]; //for table year 21- 25 , total max 60 item
  var dataSourceTable6 = <
      MortgageRepaymentTableModel>[]; //for table year 26- 30 , total max 60 item

  var dataSourceTable7 = <
      MortgageRepaymentTableModel>[];//for table year 31- 35 , total max 60 item


  var viewTable1 = true;
  getInterestRateComparisonModels() {
    loadDetailsDataResult["Interest Rate Comparison"].forEach((key, value) {
      if (key == "Interest Rate 1") {
        modelCompare1 = InterestRateComparisonModel.fromJson(value);
      }
      if (key == "Interest Rate 2") {
        modelCompare2 = InterestRateComparisonModel.fromJson(value);
      }
    });
  }

  var isCheckLoanDetails = false;
  var isInterestRateComparison = false;
  var isMortgageRepayment1 = false;
  var isMortgageRepayment2 = false;

  getDataTableModels() {

    clearDataTable();
    loadDetailsDataResult["Mortgage Repayment"].forEach((key, value) {
      if (viewTable1) {
        if (key == "Mortgage list for 1") {
          var jsonMortgageListFor1 = value;
          jsonMortgageListFor1.forEach((key, value) {
            // print(value["cum_interest_paid"]);
            var model = MortgageRepaymentTableModel.fromJson(value);
            // print(model.cumInterestPaid);
            dataSourceA.add(model);
          });
          for (var i = 0; i < 60 && i < dataSourceA.length; i++) {
            dataSourceTable1.add(dataSourceA[i]);
          }

          //fill data for year 6 - 10
          for (var i = 60; i < 120 && i < dataSourceA.length; i++) {
            dataSourceTable2.add(dataSourceA[i]);
          }
          if (kDebugMode) {
            print("dataSourceA lenght ${dataSourceA.length}");
          }
          if (kDebugMode) {
            print("dataSourceTable1 lenght ${dataSourceTable1.length}");
          }
          //fill data for year 11 - 15
          for (var i = 120; i < 180 && i < dataSourceA.length; i++) {
            dataSourceTable3.add(dataSourceA[i]);
          }
          if (kDebugMode) {
            print("dataSourceTable2 lenght ${dataSourceTable2.length}");
          }
          //fill data for year 16 - 20
          for (var i = 180; i < 240 && i < dataSourceA.length; i++) {
            dataSourceTable4.add(dataSourceA[i]);
          }
          //fill data for year 21 - 25
          for (var i = 240; i < 300 && i < dataSourceA.length; i++) {
            dataSourceTable5.add(dataSourceA[i]);
          }
          //fill data for year 25 - 30
          for (var i = 300; i < 360 && i < dataSourceA.length; i++) {
            dataSourceTable6.add(dataSourceA[i]);
          }

          //fill data for year 31 - 35
          for (var i = 360; i < 420 && i < dataSourceA.length; i++) {
            dataSourceTable7.add(dataSourceA[i]);
          }


        }
        //fill data for year 1 - 5

      } else {
        if (key == "Mortgage list for 2") {
          var jsonMortgageListFor2 = value;
          jsonMortgageListFor2.forEach((key, value) {
            // print(value["cum_interest_paid"]);
            var model = MortgageRepaymentTableModel.fromJson(value);
            // print(model.cumInterestPaid);
            dataSourceB.add(model);
          });
          for (var i = 0; i < 60 && i < dataSourceB.length; i++) {
            dataSourceTable1.add(dataSourceB[i]);
          }
          //fill data for year 6 - 10
          for (var i = 60; i < 120 && i < dataSourceB.length; i++) {
            dataSourceTable2.add(dataSourceB[i]);
          }
          //fill data for year 11 - 15
          for (var i = 120; i < 180 && i < dataSourceB.length; i++) {
            dataSourceTable3.add(dataSourceB[i]);
          }
          //fill data for year 16 - 20
          for (var i = 180; i < 240 && i < dataSourceB.length; i++) {
            dataSourceTable4.add(dataSourceB[i]);
          }
          //fill data for year 21 - 25
          for (var i = 240; i < 300 && i < dataSourceB.length; i++) {
            dataSourceTable5.add(dataSourceB[i]);
          }
          //fill data for year 25 - 30
          for (var i = 300; i < 360 && i < dataSourceB.length; i++) {
            dataSourceTable6.add(dataSourceB[i]);
          }
          //fill data for year 31 - 35
          for (var i = 360; i < 420 && i < dataSourceB.length; i++) {
            dataSourceTable7.add(dataSourceA[i]);
          }
        }
      }
    });

  }

  InterestRateComparisonModel getModelCompare1() {
    return modelCompare1;
  }

  InterestRateComparisonModel getModelCompare2() {
    return modelCompare2;
  }

  clearData() {
    modelCompare1 = null;
    modelCompare2 = null;

    loadDetailsDataResult = null;
    clearDataTable();
  }


  clearDataTable() {
    dataSourceA = [];
    dataSourceB = [];
    dataSourceTable1 = [];
    dataSourceTable2 = [];
    dataSourceTable3 = [];
    dataSourceTable4 = [];
    dataSourceTable5 = [];
    dataSourceTable6 = [];
    dataSourceTable7 = [];

  }
}
