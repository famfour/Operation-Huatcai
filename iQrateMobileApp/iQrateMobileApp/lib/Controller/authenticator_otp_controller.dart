import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/enable_twofa_view_controller.dart';
import 'package:iqrate/Controller/login_controller.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Service/core_services.dart';

import 'package:iqrate/Service/url.dart';

import '../Model/send_model.dart/enable_mfa_send_model.dart';

class AuthenticatorOtpController extends GetxController {
  SignInController signInController = Get.put(SignInController());
  Enable2FAViewController enable2faViewController =
      Get.put(Enable2FAViewController());

  TextEditingController authenticatorOtpController =
      TextEditingController(); //Initalizing the controller before the UI is built.

  // @override
  // void onInit() {
  //   super.onInit();
  // }

  onSubmitButtonTap() {
    if (authenticatorOtpController.text.isEmpty) {
      //Check if the otp is empty
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter the OTP",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (authenticatorOtpController.text.length < 6) {
      //Check if the otp is less than 6 characters
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please check the OTP",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else {
      //If the otp is not empty and is 6 characters long call the onSubmit method which sends the OTP and checks if the OTP is correct and moves to the next screen
      if (signInController.redirectFromLogin.value) {
        debugPrint("Submit button tapped from normal login");
        signInController.submitMFALogin(authenticatorOtpController);
      } else if (signInController.isGoogleLogin2FAEnable) {
        debugPrint("Submit button tapped from google login");
        signInController.handleGoogleLogin(authenticatorOtpController.text);
      } else if (signInController.isAppleLogin2FAEnable) {
        debugPrint("Submit button tapped from apple login");
        signInController.handleAppleLogin(authenticatorOtpController.text);
        //signInController.submitMFALogin(authenticatorOtpController.text);
      } else {
        debugPrint("Submit button tapped");
        check2FAStatus();
      }
    }
  }

  check2FAStatus() async {
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);

    EnableTwoMfaSendModel enableTwoMfaSendModel = EnableTwoMfaSendModel();

    var data = await CoreService().putWithAuth(
        url: baseUrl + check2FAstatusUrl + authenticatorOtpController.text,
        body: enableTwoMfaSendModel.toJson());
    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
      authenticatorOtpController.clear();
    } else {
      if (data == 401) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Wrong authorization code",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
        /* bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          check2FAStatus();
        }*/

        authenticatorOtpController.clear();
      } else {
        authenticatorOtpController.clear();
        debugPrint("verified");
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: data['message'],
            backgroundColor: Colors.green,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
        Get.toNamed(loginScreen);
      }
    }
  }

  clearRedirectionFromLogin() {
    signInController.redirectFromLogin.value = false;
  }

  void onCancel() {
    //enable2faViewController.switchValue.value = false;
    //Get.toNamed(dashboard);
    Get.back();
  }
}
