import 'dart:developer';

import 'package:get/get.dart';
import 'package:iqrate/Model/response_model.dart/subscription_summary_details_response.model.dart';
import 'package:iqrate/Router/route_constants.dart';

import '../Service/core_services.dart';
import '../Service/url.dart';

class SubscriptionOptionsController extends GetxController {
  Rx<SubscriptionSummaryDetailsResponseModel> subscriptionSummary =
      SubscriptionSummaryDetailsResponseModel().obs;
  @override
  onInit() {
    /* Timer(const Duration(seconds: 0), () {
      Get.isDialogOpen ?? true
          ? const Offstage()
          : Get.dialog(const Center(child: CircularProgressIndicator()),
              barrierDismissible: false);*/
    getPlanSummaryDetails();
    //});
    super.onInit();
  }

  getPlanSummaryDetails() async {
    var data = await CoreService()
        .getWithAuth(url: baseUrl + getSubscriptionDetailsUrl);
    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
    } else {
      var result = SubscriptionSummaryDetailsResponseModel.fromJson(data);
      log(result.toString());
      if (result.product != null) {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            getPlanSummaryDetails();
          }
        } else {
          // debugPrint('********************');
          // debugPrint(result.product);
          // debugPrint(result.interval);
          // debugPrint(result.intervalCount.toString());
          // debugPrint(result.currentPlanAmount.toString());
          // debugPrint(result.currency);
          // debugPrint(result.currentPeriodEnd);
          // debugPrint('********************');

          subscriptionSummary.value.product = result.product;
          subscriptionSummary.value.interval = result.interval;
          subscriptionSummary.value.intervalCount = result.intervalCount;
          subscriptionSummary.value.currentPlanAmount =
              result.currentPlanAmount;
          subscriptionSummary.value.currency = result.currency;
          subscriptionSummary.value.currentPeriodEnd = result.currentPeriodEnd;
          subscriptionSummary.refresh();
        }
      }
    }
  }

  onTapChangePlan() {
    if (subscriptionSummary.value.product == 'Premium+') {
      Get.toNamed(subscriptionPlansScreen, arguments: [
        2,
        subscriptionSummary.value.currentPlanAmount.toString()
      ]);
    } else if (subscriptionSummary.value.product == 'Premium') {
      Get.toNamed(subscriptionPlansScreen, arguments: [
        1,
        subscriptionSummary.value.currentPlanAmount.toString()
      ]);
    } else {
      Get.toNamed(subscriptionPlansScreen, arguments: [
        0,
        subscriptionSummary.value.currentPlanAmount.toString()
      ]);
    }
  }

  onTapManageCards() {
    Get.toNamed(savedCardsViewScreen);
  }
}
