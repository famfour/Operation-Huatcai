import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/profile_controller.dart';
import 'package:iqrate/Model/response_model.dart/detach_payment_response_model.dart';
import 'package:iqrate/Model/send_model.dart/detach_payment_send_model.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';

class DetachPaymentController extends GetxController {
  ProfileScreenController profileScreenController = Get.find();

  onDetachPaymentButtonPressed(String stripePaymentMethodId) async {
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);

    DetachPaymentSendModel model = DetachPaymentSendModel(
      paymentMethodId: stripePaymentMethodId,
      customerId: profileScreenController.userData.value.stripeCustomerId,
    );
    var data = await CoreService().postWithAuth(
      url: baseUrl + detachPaymentMethod,
      body: model.toJson(),
    );

    if (Get.isDialogOpen ?? false) Get.back();

    if (data == null) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Something went wrong, please try again later",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          onDetachPaymentButtonPressed(stripePaymentMethodId);
        }
      } else {
        var result = DetachPaymentResponseModel.fromJson(data);
        if (result.customer == null) {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Card removed successfully",
              backgroundColor: Colors.green,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        } else {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Something went wrong, please try again later",
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }
      }
    }
  }
}
