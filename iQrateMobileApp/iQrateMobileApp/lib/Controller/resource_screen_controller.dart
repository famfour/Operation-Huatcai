import 'package:get/get.dart';
import 'package:iqrate/Controller/acl_controller.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Service/url.dart';

class ResourceScreenController extends GetxController {
  ACLController aclController = Get.find();

  checkLawfirmNavigation() {
    aclController.aclNavigationCheck(lawFirmsListUrls, lawFirmListingScreen);
  }

  checkBankFormsNavigation() {
    aclController.aclNavigationCheck('customer/bank/banks', bankFormsScreen);
  }
}
