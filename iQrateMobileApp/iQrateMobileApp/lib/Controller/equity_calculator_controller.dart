import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Model/response_model.dart/eqity_calculator_response_model.dart';
import 'package:iqrate/Model/send_model.dart/equity_calculator_send_model.dart';
import 'package:iqrate/Service/url.dart';

import '../Service/core_services.dart';

class EquityCalculatorController extends GetxController {
  late TextEditingController totalNumberOfHousingLoan;
  late TextEditingController currentValuationController;
  late TextEditingController totalCPFController;
  late TextEditingController currentOutStandingLoanController;
  RxString? maximumEquityLoan = "0".obs;
  RxInt calculated = 0.obs;
  late RxInt numberOfHousingLoan;

  List<String> dropdownLongerLoanTenure = ['No', 'Yes'];
  String? selectedPropertyPlan = "Yes";

  @override
  void onInit() {
    totalNumberOfHousingLoan = TextEditingController();
    currentValuationController = TextEditingController();
    totalCPFController = TextEditingController();
    currentOutStandingLoanController = TextEditingController();
    numberOfHousingLoan = 0.obs;
    super.onInit();
  }

  clearFields() {
    totalNumberOfHousingLoan.clear();
    currentValuationController.clear();
    totalCPFController.clear();
    currentOutStandingLoanController.clear();
  }

  calculate() {
    if (totalNumberOfHousingLoan.text.isEmpty &&
        currentValuationController.text.isEmpty &&
        totalCPFController.text.isEmpty &&
        currentOutStandingLoanController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "All fileds are mandatory",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (totalNumberOfHousingLoan.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter the total number of existing housing loan",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (currentValuationController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter the current valuation",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (totalCPFController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter the total CPF",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (currentOutStandingLoanController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter the current outstanding loan",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else {
      onPressCalculate();
    }
  }

  onPressCalculate() async {
    // if (calculated.value == 0) {
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);
    EquityCalculatorSendModel equityCalculatorSendModel =
        EquityCalculatorSendModel(
      existingHousingLoans: numberOfHousingLoan.value,
      currentValuation: currentValuationController.text.replaceAll(',', ''),
      totalCpf: totalCPFController.text.replaceAll(',', ''),
      outstandingLoan:
          currentOutStandingLoanController.text.replaceAll(',', ''),
      loanTagged: numberOfHousingLoan.value == 1
          ? selectedPropertyPlan!.toLowerCase()
          : "",
    );
    var data = await CoreService().postWithAuth(
        url: baseUrl + equityCalculatorUrl,
        body: equityCalculatorSendModel.toJson());
    if (data == null) {
      if (Get.isDialogOpen ?? false) Get.back();
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          onPressCalculate();
        }
      } else {
        var result = EquityCalculatorResponseModel.fromJson(data);
        if (result.maximumEquityLoan != null) {
          if (Get.isDialogOpen ?? false) Get.back();
          calculated.value = 1;

          debugPrint(result.maximumEquityLoan.toString());
          maximumEquityLoan!.value = result.maximumEquityLoan.toString();
          // debugPrint(maximumEquityLoan);
          // Get.snackbar(
          //   StringUtils.success,
          //   "Successful",
          //   snackPosition: SnackPosition.TOP,
          //   backgroundColor: Colors.green,
          //   colorText: Colors.white,
          //   duration: const Duration(seconds: 3),
          // );
        } else {
          if (Get.isDialogOpen ?? false) Get.back();

          // Get.snackbar(StringUtils.error, StringUtils.error,
          //     snackPosition: SnackPosition.TOP, backgroundColor: Colors.white);
        }
      }
    }
    // } else {
    //   calculated.value = 0;
    //   clearFields();
    //   maximumEquityLoan!.value = "0";
    //   numberOfHousingLoan.value = 1;
    // }
  }
}
