import 'dart:async';
import 'dart:io';
import 'dart:isolate';
import 'dart:ui';

import 'package:android_path_provider/android_path_provider.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Model/response_model.dart/bank_forms_response_model.dart';
import 'package:open_filex/open_filex.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

import '../Service/core_services.dart';
import '../Service/url.dart';

class BankFormsViewController extends GetxController {
  Rx<BankFormsResponseModel> bankFormsResponseModel =
      BankFormsResponseModel().obs;
  ScrollController scrollController = ScrollController();
  RxList<OneBankForm> bankForms = <OneBankForm>[].obs;
  int fileIndex = 0;
  RxBool showFormsList = false.obs;
  @override
  void onInit() {
    getPermission();
    Timer(const Duration(seconds: 0), () {
      Get.isDialogOpen ?? true
          ? const Offstage()
          : Get.dialog(const Center(child: CircularProgressIndicator()),
              barrierDismissible: false);
      getBankForms();
    });
    prepareSaveDir();
    scrollController.addListener(pagination);
    FlutterDownloader.registerCallback(downloadCallback);

    super.onInit();
  }

  getBankForms() async {
    var data =
        await CoreService().getWithAuth(url: baseUrl + resourcesBankFormsUrls);
    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          getBankForms();
        }
      } else {
        bankFormsResponseModel.value = BankFormsResponseModel.fromJson(data);
        var arrayValid = <OneBankForm>[];
        for (var i = 0; i < bankFormsResponseModel.value.results!.length; i++) {
          var model = bankFormsResponseModel.value.results![i];
          if (model.bankFormUrls != null) {
            arrayValid.add(model);
          }
        }
        bankForms.addAll(arrayValid);
        sortListAlphabatically(bankForms);
      }
    }
  }

  getMoreBankForms() async {
    if (bankFormsResponseModel.value.next != null) {
      Get.isDialogOpen ?? true
          ? const Offstage()
          : Get.dialog(const Center(child: CircularProgressIndicator()),
              barrierDismissible: false);
      var data = await CoreService()
          .getWithAuth(url: bankFormsResponseModel.value.next!);
      if (Get.isDialogOpen ?? false) Get.back();
      if (data == null) {
      } else {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            getMoreBankForms();
          }
        } else {
          bankFormsResponseModel.value = BankFormsResponseModel.fromJson(data);
          var arrayValid = <OneBankForm>[];
          for (var i = 0;
              i < bankFormsResponseModel.value.results!.length;
              i++) {
            var model = bankFormsResponseModel.value.results![i];
            if (model.bankFormUrls != null) {
              arrayValid.add(model);
            }
          }
          // bankForms.addAll(bankFormsResponseModel.value.results!);
          bankForms.addAll(arrayValid);
          sortListAlphabatically(bankForms);
        }
      }
    }
  }

  sortListAlphabatically(List<OneBankForm>? results) {
    results?.sort((a, b) => a.name!.compareTo(b.name!));
  }

  void getPermission() async {
    await Permission.storage.request();
  }

  onDownloadPdfButtonTap(String url) {
    openFile(
      url: url,
      fileName: "Invoice ${url.split('/').last}.pdf",
    );
  }

  Future openFile({String? url, String? fileName}) async {
    debugPrint('url' + url!);

    final file = await downloadFile(url, fileName!);
    if (file == null) return;

    debugPrint('Path: ${file.path}');

    OpenFilex.open(file.path);
  }

  // ignore: body_might_complete_normally_nullable
  Future<File?> downloadFile(String url, String name) async {
    Future.delayed(const Duration(milliseconds: 0), () {
      FocusManager.instance.primaryFocus!.unfocus();
      Fluttertoast.showToast(
        timeInSecForIosWeb: 3,
        msg: "Download Started",
        backgroundColor: Colors.green,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        textColor: Colors.white,
        fontSize: 20.0,
      );
    });
    final taskId = await FlutterDownloader.enqueue(
        url: url,
        savedDir: _localPath,
        showNotification:
            true, // show download progress in status bar (for Android)
        openFileFromNotification:
            true, // click on notification to open downloaded file (for Android)
        saveInPublicStorage: true);

    Get.snackbar("Downloaded", "Download completed",
        backgroundColor: Colors.green,
        colorText: Colors.white,
        mainButton: TextButton(
            onPressed: () async {
              await FlutterDownloader.open(taskId: taskId!);
            },
            child: const Text(
              "Open",
              style: TextStyle(color: Colors.white),
            )),
        duration: const Duration(seconds: 7));
  }

  late String _localPath;

  Future<void> prepareSaveDir() async {
    _localPath = (await _findLocalPath())!;
    final savedDir = Directory(_localPath);
    bool hasExisted = await savedDir.exists();
    if (!hasExisted) {
      savedDir.create();
    }
  }

  Future<String?> _findLocalPath() async {
    String? externalStorageDirPath;
    if (Platform.isAndroid) {
      try {
        externalStorageDirPath = await AndroidPathProvider.downloadsPath;
      } catch (e) {
        final directory = await getExternalStorageDirectory();
        externalStorageDirPath = directory?.path;
      }
    } else if (Platform.isIOS) {
      externalStorageDirPath =
          (await getApplicationDocumentsDirectory()).absolute.path;
    }
    return externalStorageDirPath;
  }

  static void downloadCallback(
      String id, DownloadTaskStatus status, int progress) {
    if (kDebugMode) {
      print(
          'Background Isolate Callback: task ($id) is in status ($status) and process ($progress)');
    }

    final SendPort send =
        IsolateNameServer.lookupPortByName('downloader_send_port')!;
    send.send([id, status, progress]);
  }

  pagination() {
    if ((scrollController.position.pixels ==
        scrollController.position.maxScrollExtent)) {
      //add api for load the more data according to new page
      getMoreBankForms();
    }
  }
}
