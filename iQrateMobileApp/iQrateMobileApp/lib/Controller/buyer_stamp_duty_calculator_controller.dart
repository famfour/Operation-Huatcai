import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Model/response_model.dart/buyer_stamp_duty_response_model.dart';
import 'package:iqrate/Model/send_model.dart/buyer_stamp_duty_send_model.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';

class BuyerStampDutyCalculatorController extends GetxController {
  late int numberOfApplicants;
  late TextEditingController purchasePriceContoller;
  late RxString numberOfProperties;
  late RxString nationality;
  late RxString numberOfPropertiesApplicant2;
  late RxString nationalityApplicant2;
  late RxString numberOfPropertiesApplicant3;
  late RxString nationalityApplicant3;
  late RxString numberOfPropertiesApplicant4;
  late RxString nationalityApplicant4;
  RxBool isCalculated = false.obs;
  RxInt calculated = 0.obs;
  RxString? buyerStampDuty = "0".obs;
  RxString? additionalBuyerStampDuty = "0".obs;
  RxString? totalBuyerStampDuty = "0".obs;

  @override
  void onInit() {
    numberOfApplicants = 1;
    purchasePriceContoller = TextEditingController();
    numberOfProperties = "0".obs;
    nationality = "Singapore Citizens".obs;
    numberOfPropertiesApplicant2 = "0".obs;
    nationalityApplicant2 = "Singapore Citizens".obs;
    numberOfPropertiesApplicant3 = "0".obs;
    nationalityApplicant3 = "Singapore Citizens".obs;
    numberOfPropertiesApplicant4 = "0".obs;
    nationalityApplicant4 = "Singapore Citizens".obs;
    super.onInit();
  }

  clearFields() {
    purchasePriceContoller.clear();
    numberOfProperties.value = "0";
    nationality.value = "Singapore Citizens";
    numberOfPropertiesApplicant2.value = "0";
    nationalityApplicant2.value = "Singapore Citizens";
    numberOfPropertiesApplicant3.value = "0";
    nationalityApplicant3.value = "Singapore Citizens";
    numberOfPropertiesApplicant4.value = "0";
    nationalityApplicant4.value = "Singapore Citizens";
  }

  onPressContinueForNumberOfApplicants() {
    Get.toNamed(buyerStampDutyCalculatorScreen2);
    clearFields();
    buyerStampDuty!.value = "0";
    additionalBuyerStampDuty!.value = "0";
    totalBuyerStampDuty!.value = "0";
    calculated.value = 0;
  }

  calculate() {
    if (purchasePriceContoller.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter the purchase price",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else {
      onPressCalculate();
    }
  }

  onPressCalculate() async {
    // if (calculated.value == 0) {
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);
    calculated.value = 1;
    BuyerStampDutySendModel buyerStampDutySendModel = BuyerStampDutySendModel(
      purchasePrice:
          double.tryParse(purchasePriceContoller.text.replaceAll(',', '')),
      loanApplicant: numberOfApplicants,
      applicant1Property: int.tryParse(numberOfProperties.value),
      applicant1Nationality: nationality.value,
      applicant2Property: int.tryParse(numberOfPropertiesApplicant2.value),
      applicant2Nationality: nationalityApplicant2.value,
      applicant3Property: int.tryParse(numberOfPropertiesApplicant3.value),
      applicant3Nationality: nationalityApplicant3.value,
      applicant4Property: int.tryParse(numberOfPropertiesApplicant4.value),
      applicant4Nationality: nationalityApplicant4.value,
    );
    var data = await CoreService().postWithAuth(
        url: baseUrl + buyerStampDutyUrl,
        body: buyerStampDutySendModel.toJson());
    if (data == null) {
      if (Get.isDialogOpen ?? false) Get.back();
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          onPressCalculate();
        }
      } else {
        var result = BuyerStampDutyResponseModel.fromJson(data);
        if (result.buyerStampDuty != null) {
          if (Get.isDialogOpen ?? false) Get.back();
          calculated.value = 1;
          buyerStampDuty!.value = result.buyerStampDuty.toString();
          additionalBuyerStampDuty!.value =
              result.additionalBuyerStampDuty.toString();
          totalBuyerStampDuty!.value = result.totalBuyerStampDuty.toString();
          // Get.snackbar(
          //   StringUtils.success,
          //   "Successful",
          //   snackPosition: SnackPosition.TOP,
          //   backgroundColor: Colors.green,
          //   colorText: Colors.white,
          //   duration: const Duration(seconds: 3),
          // );
          isCalculated.value = true;
        } else {
          if (Get.isDialogOpen ?? false) Get.back();
        }
      }
    }
    // } else {
    //   calculated.value = 0;
    //   clearFields();
    //   buyerStampDuty!.value = "0";
    //   additionalBuyerStampDuty!.value = "0";
    //   totalBuyerStampDuty!.value = "0";
    // }
  }
}
