import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/authenticator_otp_controller.dart';
import 'package:iqrate/Controller/forget_password_controller.dart';
import 'package:iqrate/Model/response_model.dart/update_password_response_model.dart';
import 'package:iqrate/Model/send_model.dart/update_password_send_model.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';

class NewPasswordController extends GetxController {
  final ForgetPasswordController forgetPasswordController =
      Get.put(ForgetPasswordController());
  final AuthenticatorOtpController authenticatorOtpController =
      Get.put(AuthenticatorOtpController());
  late TextEditingController passwordTextController;
  late TextEditingController confirmPasswordTextController;
  final formKey =
      GlobalKey<FormState>(); // this key manage to form submit validation

  @override
  void onInit() {
    passwordTextController =
        TextEditingController(); //initialize password text field controller
    confirmPasswordTextController =
        TextEditingController(); //initialize confirm password text field controller
    super.onInit();
  }

  onSubmit() {
    if (passwordTextController.text.isNotEmpty &&
        confirmPasswordTextController.text.isNotEmpty) {
      if (passwordTextController.text == confirmPasswordTextController.text) {
        onSubmitNewPassword();
      }
    }
  }

  onSubmitNewPassword() async {
    debugPrint(
        updatePasswordUrl + forgetPasswordController.emailTextController.text);
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);

    UpdatePasswordSendModel updatePasswordSendModel = UpdatePasswordSendModel(
      password: passwordTextController.text,
      otp: authenticatorOtpController.authenticatorOtpController.text,
    );
    var data = await CoreService().putWithoutAuth(
        url: updatePasswordUrl +
            forgetPasswordController.emailTextController.text,
        body: updatePasswordSendModel.toJson());
    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Something went wrong, please try after sometime",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else {
      var result = UpdatePasswordResponseModel.fromJson(data);
      if (result.message != null) {
        debugPrint("hello");
        Get.toNamed(loginScreen);

        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Updated Successfully",
            backgroundColor: Colors.green,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      } else {}
    }
  }
}
