import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:iqrate/Controller/login_controller.dart';
import 'package:iqrate/Controller/shared_prefs_controller.dart';
import 'package:iqrate/DeviceManager/hive_string.dart';
import 'package:iqrate/Router/route_constants.dart';

class MoreViewController extends GetxController {
  late Box hive = Hive.box(HiveString.hiveName);

  onTapProfile() {
    Get.toNamed(profileScreen);
  }

  onTapSubscription() {
    // Get.toNamed(subscriptionPlansScreen);
    Get.toNamed(subscriptionMenusScreen);
  }

  onTapTraining() {
    Get.toNamed(trainingScreen);
  }

  onTapResources() {
    Get.toNamed(resourseScreen);
  }

  onTapFAQ() {
    Get.toNamed(faqScreen);
  }

  onTapContactUs() {
    Get.toNamed(contactUs);
  }

  onTapLogout() async {
    Get.find<SharedPrefController>().prefs.setInt("counter", 0);
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);

    hive.deleteAll([
      HiveString.token,
      HiveString.userId,
      HiveString.refreshToken,
    ]);

    Get.offAllNamed(loginScreen);

    Future.delayed(const Duration(milliseconds: 0), () {
      FocusManager.instance.primaryFocus!.unfocus();
      Fluttertoast.showToast(
        timeInSecForIosWeb: 3,
        msg: "Logout Successful",
        backgroundColor: Colors.green,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        textColor: Colors.white,
        fontSize: 20.0,
      );
    });

    Get.find<SignInController>().isGoogleLogin2FAEnable = false;
    Get.find<SignInController>().isAppleLogin2FAEnable = false;
  }
}
