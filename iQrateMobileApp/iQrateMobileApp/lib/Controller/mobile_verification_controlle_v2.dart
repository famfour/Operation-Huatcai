// ignore_for_file: avoid_print

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/shared_prefs_controller.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';

import '../Service/GetControllers.dart';

class MobileVerificationControllerV2 extends GetxController {
  // final SignUpController signUpController = Get.put(
  //     SignUpController()); //Signup controller is used here in for getting the entered mobile number
  // final SignInController signInController = Get.put(
  //     SignInController()); //Signup controller is used here in for getting the entered mobile number
  // final ProfileScreenController profileScreenController =
  //     Get.put(ProfileScreenController());
  late TextEditingController mobileOtpController; // Controller for mobile otp
  final SharedPrefController sharedPrefController = Get.find();

  var totalCountdownSeconds = 60;
  var countDownSecond = 0.obs;
  late Timer timer;

  var countTimes = 0;
  var enterWrongOtpCount = 0;

  var isResendButtonDisable = false.obs;
  var isResendButtonHide = false.obs;

  @override
  void onInit() {
    mobileOtpController =
        TextEditingController(); // Initialize controller before the screen loads

    resendSMSCodeTimer();

    //signUpController.phoneController.text = "919962679318"; //demo number

    super.onInit();
  }
  //
  // onSubmitButtonTap() {
  //   if (mobileOtpController.text.isEmpty) {
  //     //Check if the otp is empty
  //     Get.snackbar(
  //       StringUtils.error,
  //       "Please enter the OTP",
  //       backgroundColor: Colors.red,
  //       colorText: Colors.white,
  //       duration: const Duration(seconds: 3),
  //     );
  //   } else if (mobileOtpController.text.length < 6) {
  //     //Check if the otp is less than 6 characters
  //     Get.snackbar(
  //       StringUtils.error,
  //       "Please check the OTP",
  //       backgroundColor: Colors.red,
  //       colorText: Colors.white,
  //       duration: const Duration(seconds: 3),
  //     );
  //   } else {
  //     //If the otp is not empty and is 6 characters long call the onSubmit method which sends the OTP and checks if the OTP is correct and moves to the next screen
  //     //debugPrint("Submit button tapped");
  //     if (signUpController.isRedirectFromSignupScreen) {
  //       debugPrint("Submit button tapped for Signup phone Verification");
  //       signUpMobileVerification();
  //     } else if (signInController.isRedirectFromSignInScreen) {
  //       //signInMobileVerification();
  //       debugPrint("Submit button tapped for google login phone Verification");
  //       profileScreenController.phoneVerificationToken =
  //           signInController.phoneVerificationToken;
  //       onSubmitOtpPhoneVerification(mobileOtpController.text);
  //     } else {
  //       debugPrint(">>>>>>>>>mobile number update phone Verification");
  //       profileScreenController
  //           .onSubmitOtpPhoneVerification(mobileOtpController.text);
  //     }
  //   }
  // }
  //
  // signUpMobileVerification() async {
  //   debugPrint(
  //       "Sms token" + signUpController.signUpData.value.smsToken.toString());
  //   Get.isDialogOpen ?? true
  //       ? const Offstage() //Used for avoiding the dialog to be open when the user is already on the next screen.
  //       : Get.dialog(
  //           const Center(
  //               child:
  //                   CircularProgressIndicator()), //Used for showing the loading dialog.
  //           barrierDismissible:
  //               false); //Used for preventing the user to dismiss the dialog.
  //   MobileVerificationSendModel mobileVerificationSendModel =
  //       MobileVerificationSendModel(
  //     verification_code: mobileOtpController.text,
  //     verification_token: signUpController.signUpData.value.smsToken,
  //     type: "Mobile",
  //   ); //Create a model for sending the otp to the server.
  //   //
  //   //The entered url is base+mobileVerificationUrl+the email address of the user.
  //   var data = await CoreService().putWithoutAuthMobileVerification(
  //       url: baseUrl +
  //           mobileVerificationUrl +
  //           signUpController.countryCodeController.text.replaceAll("+", "") +
  //           signUpController.phoneController.text,
  //       body:
  //           mobileVerificationSendModel.toJson()); //Send the otp to the server.
  //   if (Get.isDialogOpen ?? false) Get.back();
  //   if (data == null) {
  //     enterWrongOtpCount++;
  //
  //     if (isResendButtonHide.value || enterWrongOtpCount >= 3) {
  //       Get.offAndToNamed(signUp);
  //       Get.snackbar(
  //         StringUtils.error,
  //         "Account has been deleted. please try again",
  //         snackPosition: SnackPosition.TOP,
  //         backgroundColor: Colors.red,
  //         duration: const Duration(seconds: 3),
  //       );
  //     } else {
  //       //Get.snackbar(StringUtils.error, data["errMessage"].toString(), snackPosition: SnackPosition.TOP, backgroundColor: Colors.white);
  //     }
  //   } else {
  //     //if the status code is 400, the server sends an error, which is displayed via the snackbar.
  //     //If the status code is 200, the server sends a success message, which is displayed via the snackbar. and the data is mapped in the response model. The control then is moved to the next screen which is the Google Authenticator Screen.
  //     var result = MobileVerificationResponseModel.fromJson(data);
  //     debugPrint("Message:" + result.message.toString());
  //
  //     closeCountdownTimer();
  //     Get.toNamed(loginScreen);
  //     Get.snackbar(
  //       StringUtils.success,
  //       result.message.toString(),
  //       snackPosition: SnackPosition.TOP,
  //       backgroundColor: Colors.green,
  //       colorText: Colors.white,
  //       duration: const Duration(seconds: 3),
  //     );
  //   }
  // }

  // onSubmitOtpPhoneVerification(code) async {
  //   debugPrint("onSubmitOtpPhoneVerification::: ");
  //
  //   Get.isDialogOpen ?? true
  //       ? const Offstage() //Used for avoiding the dialog to be open when the user is already on the next screen.
  //       : Get.dialog(
  //           const Center(
  //               child:
  //                   CircularProgressIndicator()), //Used for showing the loading dialog.
  //           barrierDismissible:
  //               false); //Used for preventing the user to dismiss the dialog.
  //   MobileVerificationSendModel mobileVerificationSendModel =
  //       MobileVerificationSendModel(
  //     verification_code: code,
  //     verification_token: profileScreenController.phoneVerificationToken,
  //     type: "Mobile",
  //   ); //Create a model for sending the otp to the server.
  //   //
  //   //The entered url is base+emailverifyurl+the email address of the user.
  //   var data = await CoreService().putWithAuth(
  //       url: baseUrl + verifyWithCodeUrl,
  //       body:
  //           mobileVerificationSendModel.toJson()); //Send the otp to the server.
  //   if (data == null) {
  //     enterWrongOtpCount++;
  //
  //     if (isResendButtonHide.value || enterWrongOtpCount >= 3) {
  //       Get.offAndToNamed(signUp);
  //       Get.snackbar(
  //         StringUtils.error,
  //         "Account has been deleted. please try again",
  //         snackPosition: SnackPosition.TOP,
  //         backgroundColor: Colors.red,
  //         colorText: Colors.white,
  //         duration: const Duration(seconds: 3),
  //       );
  //     }
  //
  //     Timer(const Duration(seconds: 6), () {
  //       debugPrint("onSubmitOtpPhoneVerification::: null");
  //       if (Get.isDialogOpen ?? false) Get.back();
  //     });
  //   } else {
  //     if (Get.isDialogOpen ?? false) Get.back();
  //     //if the status code is 400, the server sends an error, which is displayed via the snackbar.
  //     // Get.snackbar(StringUtils.success, data["message"].toString(),
  //     //     backgroundColor: Colors.green, colorText: Colors.white);
  //     if (data["resultcode"] == 400) {
  //       Get.snackbar(
  //         StringUtils.error,
  //         data["errMessage"].toString(),
  //         snackPosition: SnackPosition.TOP,
  //         backgroundColor: Colors.red,
  //         colorText: Colors.white,
  //         duration: const Duration(seconds: 3),
  //       );
  //     } else {
  //       closeCountdownTimer();
  //
  //       debugPrint("kv store values");
  //       //signInController.getAndStoreKvValues();
  //
  //       await profileScreenController.getProfileData();
  //
  //       await signInController.registerWithStripe();
  //
  //       signInController.navigationCheckAfterLogin(true);
  //
  //       sharedPrefController.prefs.setBool('isNewUser',
  //           false); // This is to set false so that the app understands that, the user loggedin is not the user's first time
  //       Get.snackbar(
  //         StringUtils.success,
  //         "Login Successful",
  //         backgroundColor: Colors.green,
  //         colorText: Colors.white,
  //         duration: const Duration(seconds: 3),
  //       );
  //     }
  //   }
  // }

  resendSMSCodeTimer() {
    isResendButtonDisable.value = true; //resend button disable

    debugPrint('countDownSecond::${countDownSecond.value}');
    debugPrint('countTimes::$countTimes');

    if (countTimes == 3) {
      isResendButtonHide.value = true;
      return;
    }

    countDownSecond.value = totalCountdownSeconds; //initial total seconds

    // Start the periodic timer which prints remaining seconds after every 1 seconds
    timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      --countDownSecond.value; //countDownSecond decrement

      debugPrint('second::${countDownSecond.value}');

      if (countDownSecond.value == 0) {
        isResendButtonDisable.value = false; //resend button enable

        countTimes++;
        closeCountdownTimer();

        switch (countTimes) {
          case 1:
          case 2:
            totalCountdownSeconds = 60;
            break;
          default:
            totalCountdownSeconds = 0;
        }
      }
    });
  }

  closeCountdownTimer() {
    //close resend timer when verify successful
    timer.cancel();
  }
  //
  // apiCallResendSMSCode() async {
  //   // if (!signUpController.isRedirectFromSignupScreen &&
  //   //     !signInController.isRedirectFromSignInScreen) {
  //   //   await profileScreenController.onUpdatePhone(true);
  //   //   resendSMSCodeTimer();
  //   //   return;
  //   // }
  //   //
  //   // String phone = "", code = "";
  //   //
  //   // if (signUpController.isRedirectFromSignupScreen) {
  //   //   code = signUpController.countryCodeController.text.replaceAll("+", "");
  //   //   phone = signUpController.phoneController.text;
  //   // }
  //   //
  //   // if (signInController.isRedirectFromSignInScreen) {
  //   //   code = signInController.countryCodeController.text.replaceAll("+", "");
  //   //   phone = signInController.phoneController.text;
  //   //
  //   //   await signInController.onSubmitGoogleSignUpMobDob(true);
  //   //   resendSMSCodeTimer();
  //   //
  //   //   return;
  //   // }
  //
  //   //The entered url is base+resendEmailCodeUrl+the email address of the user.
  //   var data = await CoreService().putWithoutAuth(
  //       url: baseUrl +
  //           resendSmsCodeUrl +
  //           code +
  //           phone); //Send the otp to the server.
  //   if (Get.isDialogOpen ?? false) Get.back();
  //   if (data == null) {
  //   } else {
  //     //if the status code is 400, the server sends an error, which is displayed via the snackbar.
  //     if (data["resultcode"] == 400) {
  //       resendSMSCodeTimer();
  //
  //       Get.snackbar(
  //         StringUtils.error,
  //         data["errMessage"].toString(),
  //         snackPosition: SnackPosition.TOP,
  //         backgroundColor: Colors.red,
  //         colorText: Colors.white,
  //         duration: const Duration(seconds: 3),
  //       );
  //     } else {
  //       //If the status code is 200, the server sends a success message, which is displayed via the snackbar
  //       //if (data["resultcode"] == 200) {
  //
  //       var result = MobileVerificationResponseModel.fromJson(data);
  //       resendSMSCodeTimer();
  //
  //       Get.snackbar(
  //         StringUtils.success,
  //         result.message!,
  //         snackPosition: SnackPosition.TOP,
  //         backgroundColor: Colors.green,
  //         colorText: Colors.white,
  //         duration: const Duration(seconds: 3),
  //       );
  //       // }
  //     }
  //   }
  // }

  apiCallResendSMSCodeNotVerify() async {
    var numberPhone = GetControllers.shared
            .getProfileScreenController()
            .userData
            .value
            .wholeNumber ??
        "";
    var endPoint = baseUrl + resendSmsCodeUrl + numberPhone;
    print("endPoint : $endPoint");
    var data = await CoreService()
        .putWithoutAuth(url: endPoint); //Send the otp to the server.
    if (data == null) {
    } else {
      //if the status code is 400, the server sends an error, which is displayed via the snackbar.
      if (data["resultcode"] == 400) {
        resendSMSCodeTimer();
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: data["message"].toString(),
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      } else {
        //success then get profile again and go back
        print(data.toString());
        await GetControllers.shared
            .getProfileScreenController()
            .getProfileData();
      }
    }
  }
}
