import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/detach_payment_controller.dart';
import 'package:iqrate/Controller/profile_controller.dart';
import 'package:iqrate/Model/response_model.dart/default_payment_method_response_model.dart';
import 'package:iqrate/Model/response_model.dart/card_list_response_model2.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';

class SavedCardsController extends GetxController {
  late RxBool switchValue = false.obs;

  late TextEditingController nameOnCardTextEditingController;
  late TextEditingController cardNumberTextEditingController;
  late TextEditingController expirationMonthTextEditingController;
  late TextEditingController expirationYearTextEditingController;
  late TextEditingController cvcTextEditingController;
  late TextEditingController countryTextEditingController;
  late TextEditingController postalCodeController;

  late RxBool checkedValue = true.obs;
  final ProfileScreenController profileScreenController =
      Get.put(ProfileScreenController());
  final DetachPaymentController detachPaymentController =
      Get.put(DetachPaymentController());

  Rx<CardDetails> cardDetails = CardDetails().obs;

  Rx<CardListResponseModel> cardListResponseModel = CardListResponseModel().obs;
  Rx<DefaultPaymentMethodResponseModel> defaultPaymentMethodResponseModel =
      DefaultPaymentMethodResponseModel().obs;

  @override
  void onInit() {
    getSavedCards();

    nameOnCardTextEditingController = TextEditingController();
    cardNumberTextEditingController = TextEditingController();
    expirationMonthTextEditingController = TextEditingController();
    expirationYearTextEditingController = TextEditingController();
    cvcTextEditingController = TextEditingController();
    countryTextEditingController = TextEditingController();
    postalCodeController = TextEditingController();

    getDefaultPaymentMethod();

    //debugAddCard();

    super.onInit();
  }

  debugAddCard() {
    if (kDebugMode) {
      nameOnCardTextEditingController.text = "Shohel";
      cardNumberTextEditingController.text = "4242 4242 4242 4242";
      cvcTextEditingController.text = "879";
      expirationMonthTextEditingController.text = "10";
      expirationYearTextEditingController.text = "23";
      postalCodeController.text = "555555";
    }
  }

  Future<bool> clearCardInfo() async {
    nameOnCardTextEditingController.clear();
    cardNumberTextEditingController.clear();
    cvcTextEditingController.clear();
    expirationMonthTextEditingController.clear();
    expirationYearTextEditingController.clear();
    postalCodeController.clear();
    return true;
  }

  switchOnTap(String value, bool newValue) async {
    //switchValue.value = value;
    switchValue.toggle();
    if (newValue) {
      setPaymentMethod(value);
    }
  }

  addNewCard() async {
    Get.toNamed(addNewCardScreen);
  }

  Future<void> onTapSave(bool isUpdate) async {
    cardDetails.value = cardDetails.value.copyWith(
      expirationMonth: int.tryParse(expirationMonthTextEditingController.text),
      expirationYear: int.tryParse(expirationYearTextEditingController.text),
      number: cardNumberTextEditingController.text.replaceAll(' ', ''),
      cvc: cvcTextEditingController.text,
    );

    if (cardDetails.value.number == null ||
        cardDetails.value.expirationYear == null ||
        //cardDetails.value.cvc == null ||
        cardDetails.value.expirationMonth == null ||
        nameOnCardTextEditingController.text.trim().isEmpty) {
      debugPrint(">>>>>>>>>>" + cardDetails.value.toString());

      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter valid card details",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return;
    } else {
      Get.isDialogOpen ?? true
          ? const Offstage()
          : Get.dialog(
              const Center(
                child: CircularProgressIndicator(),
              ),
              barrierDismissible: false);
      try {
        await Stripe.instance.dangerouslyUpdateCardDetails(cardDetails.value);
        final billingDetails = BillingDetails(
          email: null,
          phone: null,
          name: nameOnCardTextEditingController.text,
          address: const Address(
              city: null,
              country: null,
              line1: null,
              line2: null,
              state: null,
              postalCode: null),
        );
        await Stripe.instance
            .createPaymentMethod(
          // PaymentMethodParams.card(billingDetails: billingDetails),
          PaymentMethodParams.card(
              paymentMethodData:
                  PaymentMethodData(billingDetails: billingDetails)),
        )
            .then((PaymentMethod tokenData) {
          if (profileScreenController.userData.value.stripeCustomerId != null) {
            debugPrint('This is toked ID: ${tokenData.id}');
            createPaymentMethod(body: {
              "type": "card",
              "customer_id":
                  profileScreenController.userData.value.stripeCustomerId,
              "payment_method_token": tokenData.id
            }, isUpdate: isUpdate);
          } else {
            // When the customer id is null
            Get.back();
            Future.delayed(const Duration(milliseconds: 0), () {
              FocusManager.instance.primaryFocus!.unfocus();
              Fluttertoast.showToast(
                timeInSecForIosWeb: 3,
                msg: "Please try again later",
                backgroundColor: Colors.red,
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.BOTTOM,
                textColor: Colors.white,
                fontSize: 20.0,
              );
            });
          }
        });
        return;
      } on Exception catch (e) {
        if (Get.isDialogOpen ?? false) Get.back();
        if (e is StripeException) {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: e.error.localizedMessage.toString(),
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        } else {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: e.toString(),
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }
      }
    }
  }

  createPaymentMethod(
      {required Map<String, dynamic> body, required bool isUpdate}) async {
    String apiUrl = "";

    if (isUpdate) {
      apiUrl = baseUrl + updatePaymentMethodUrl + paymentIDForUpdate;
    } else {
      apiUrl = baseUrl + createPaymentMethodUrl;
    }

    debugPrint(apiUrl);

    try {
      await CoreService()
          .postWithAuth(url: apiUrl, body: body)
          .then((value) async {
        if (value == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            createPaymentMethod(body: body, isUpdate: isUpdate);
          }
        } else {
          debugPrint(value.toString());
          getSavedCards();
          if (Get.isDialogOpen ?? false) Get.back();
          Get.back();
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: isUpdate
                  ? "Card updated successfully"
                  : "Card added successfully",
              backgroundColor: Colors.green,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });

          clearCardInfo();
        }
      });
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  setPaymentMethod(String value) async {
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);

    debugPrint(baseUrl + setPaymentMethodUrl);
    try {
      await CoreService()
          .postWithAuth(url: baseUrl + setPaymentMethodUrl, body: {
        "customer_id": profileScreenController.userData.value.stripeCustomerId,
        "payment_method_id": value
      }).then((value) async {
        if (value == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            setPaymentMethod(value);
          }
        } else {
          debugPrint(value.toString());
          await getDefaultPaymentMethod();
          getSavedCards();
          if (Get.isDialogOpen ?? false) Get.back();
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Update successful",
              backgroundColor: Colors.green,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }
      });
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  getSavedCards() async {
    debugPrint(
        ">>>>>>>cardListResponseModel>>>>" + baseUrl + savedPaymentMethodUrl);

    try {
      await CoreService()
          .getWithAuth(url: baseUrl + savedPaymentMethodUrl)
          .then((value) async {
        if (value == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            getSavedCards();
          }
        } else {
          //debugPrint(value.toString());

          cardListResponseModel.value = CardListResponseModel.fromJson(value);

          debugPrint(cardListResponseModel.value.data!.length.toString() +
              ">>>>>>>>>cardListResponseModel>>>>>>>>");

          //if (Get.isDialogOpen ?? false) Get.back();
          //Get.snackbar(StringUtils.success, "Saved card fetched successfully", snackPosition: SnackPosition.TOP, backgroundColor: Colors.green);
        }
      });
    } catch (e) {
      debugPrint(">>>cardListResponseModel error::: >>>>>" + e.toString());
    }
  }

  getDefaultPaymentMethod() async {
    debugPrint(baseUrl + getDefaultPaymentMethodUrl);
    try {
      await CoreService()
          .getWithAuth(
              url: baseUrl +
                  getDefaultPaymentMethodUrl +
                  profileScreenController.userData.value.stripeCustomerId!)
          .then((value) async {
        if (value == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            getDefaultPaymentMethod();
          }
        } else {
          defaultPaymentMethodResponseModel.value =
              DefaultPaymentMethodResponseModel.fromJson(value);

          debugPrint(
              defaultPaymentMethodResponseModel.value.toJson().toString() +
                  ">>>>>>");

          //if (Get.isDialogOpen ?? false) Get.back();
          // Get.snackbar(StringUtils.success, "Default payment method fetched successfully", snackPosition: SnackPosition.TOP, backgroundColor: Colors.green);
        }
      });
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  String paymentIDForUpdate = "";

  void onEdit(DataModel data) {
    Get.toNamed(editCardScreen, arguments: data);
    paymentIDForUpdate = data.id!;
    nameOnCardTextEditingController.text = data.billingDetails!.name!;
    cardNumberTextEditingController.text = "4242 4242 4242 4242";
    cvcTextEditingController.text = "879";
    expirationMonthTextEditingController.text = data.card!.expMonth.toString();
    expirationYearTextEditingController.text = data.card!.expYear.toString();
    postalCodeController.text = "555555";
  }

  Future<void> onUpdate() async {
    await onTapSave(true);
  }

  Future<void> onTapDelete(DataModel data) async {
    await detachPaymentController.onDetachPaymentButtonPressed(data.id!);

    getSavedCards();
  }
}
