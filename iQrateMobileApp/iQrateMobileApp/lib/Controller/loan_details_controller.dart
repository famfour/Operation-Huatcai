import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:iqrate/Controller/generate_packages_controller.dart';
import 'package:iqrate/Controller/leads_view_controller.dart';
import 'package:iqrate/Model/response_model.dart/bank_forms_response_model.dart';
import 'package:iqrate/Model/response_model.dart/fetch_lead_list_response_model.dart';
import 'package:iqrate/Model/response_model.dart/kv_response_model.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';

class LoanDetailsController extends GetxController {
  late RxDouble windowHeight;
  late RxDouble windowWidth;
  late RxInt selectedIndex;
  late RxBool isCompleted = false.obs;
  //create lead list
  TextEditingController amountController =
      TextEditingController(); //amount text controller

  TextEditingController currentInterestRateController =
      TextEditingController(); //current Interest text controller

  int precision = 2;

  // MoneyMaskedTextController maskedTextController = MoneyMaskedTextController(
  //     decimalSeparator: '.', thousandSeparator: ',', precision: 2);

  final formKey = GlobalKey<FormState>();

  var isExpand = false.obs;
  var is3Years = false.obs;
  var is200K = false.obs;

  //String? defaultLoanType = 'New Purchase(With Option to Purchase)';
  String? defaultLoanType = 'new_purchase_with_option_to_purchase';
  String? defaultLoanType2 = 'new_purchase_approval_in_principle';
  List<String> loanTypes = [];
  List<String> loanTypeValues = [];

  var selectedLoanType = "new_purchase_with_option_to_purchase";
  BankFormsResponseModel bankFormsResponseModel = BankFormsResponseModel();
  List<OneBankForm> existingBanks = <OneBankForm>[];
  int? selectedExistingBank;

  KvValueStoreResponseModel? selectedKVStore;

  int? leadID;
  var isLoanDetailsFilled = false.obs;

  late Result leadData;

  LeadsViewController leadsViewController = Get.find();
  GeneratePackagesController generatePackagesController = Get.find();

  @override
  onInit() {
    selectedIndex = 0.obs;
    windowHeight = Get.height.obs;
    windowWidth = Get.width.obs;

    //create lead list
    amountController = TextEditingController(); //email text controller
    currentInterestRateController =
        TextEditingController(); // current Interest text controller

    setLoanTypeDropdown();
    setExistingBanksDropdown();

    super.onInit();
  }

  setLoanTypeDropdown() {
    for (var element in leadsViewController.kvStoreValues) {
      if (element.code == "loan_category") {
        element.value.forEach((key, value) {
          loanTypes.add(key);
          loanTypeValues.add(value);
          // debugPrint("array_key:: " + value);
        });
      }
    }
  }

  setExistingBanksDropdown() async {
    try {
      var data = await CoreService()
          .getWithAuth(url: baseUrl + resourcesBankFormsUrls);
      if (data == null) {
      } else {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            setExistingBanksDropdown();
          }
        } else {
          bankFormsResponseModel = BankFormsResponseModel.fromJson(data);
          for (int i = 0; i < bankFormsResponseModel.results!.length; i++) {
            existingBanks.add(bankFormsResponseModel.results![i]);
          }
          if (bankFormsResponseModel.next != null) {
            getMoreBanks(bankFormsResponseModel.next!);
          }
        }
      }
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  getMoreBanks(url) async {
    var data = await CoreService().getWithAuth(url: url);
    bankFormsResponseModel = BankFormsResponseModel.fromJson(data);
    for (int i = 0; i < bankFormsResponseModel.results!.length; i++) {
      existingBanks.add(bankFormsResponseModel.results![i]);
    }
    if (bankFormsResponseModel.next != null) {
      getMoreBanks(bankFormsResponseModel.next!);
    }
  }

  Future<void> onTapSave() async {
    debugPrint("onTapSave==");
    if (selectedLoanType != defaultLoanType &&
        selectedLoanType != defaultLoanType2) {
      if (amountController.text.isEmpty ||
          selectedLoanType.isEmpty ||
          currentInterestRateController.text.isEmpty ||
          selectedExistingBank == null) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Please fill all mandatory fields",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
        return;
      }
    } else if (selectedLoanType == defaultLoanType ||
        selectedLoanType == defaultLoanType2) {
      if (amountController.text.isEmpty || selectedLoanType.isEmpty) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Please fill all mandatory fields",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
        return;
      }
    }

    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);

    var data = await CoreService()
        .putWithAuth(url: baseUrl + loanDetailsUrl + leadID.toString(), body: {
      "agent_applied": leadData.agentApplied,
      "lead_type": leadData.leadType,
      "loan_category": selectedLoanType,
      //"co_broke_agent": "03ff3716-ec15-2cd6-b4f9-c16f6614231c",
      "sell_within_three_years": is3Years.value,
      "has_200k_down": is200K.value,
      "loan_amount": amountController.text != ""
          ? double.parse(amountController.text.replaceAll(",", ""))
          : "0",
      "outstanding_loan_amount":
          double.parse(amountController.text.replaceAll(",", "")),
      "preferred_loan_amount":
          double.parse(amountController.text.replaceAll(",", "")),
      "existing_bank": selectedExistingBank,
      "current_interest_rate": currentInterestRateController.text != ""
          ? double.parse(currentInterestRateController.text)
          : "0",
    });

    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          onTapSave();
        }
      } else {
        leadsViewController.fetchLeads();

        //var result = LoanDetailsResponseModel.fromJson(data);

        isLoanDetailsFilled.value = true;
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Loan details successfully updated",
            backgroundColor: Colors.green,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
        //loadDetailsID = result.id.toString();

        // amountController.text = data["loan_amount"].toString() == "null"
        //     ? ""
        //     : oCcy.format(data["loan_amount"]);

        generatePackagesController.getLowestPackages(leadID.toString());
      }
    }
  }

  final oCcy = NumberFormat("#,##0.00", "en_US");

  void onTapAddJoint() {}

  void onTapSendPDPAApproval() {}
}
