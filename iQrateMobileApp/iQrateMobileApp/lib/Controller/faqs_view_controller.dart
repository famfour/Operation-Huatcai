// ignore_for_file: non_constant_identifier_names

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Model/response_model.dart/faq_response_model.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';

class FaqsController extends GetxController {
  Rx<FaqResponseModel> faqResponseModel = FaqResponseModel().obs;
  late TextEditingController searchTextEditingController;

  @override
  void onInit() {
    searchTextEditingController = TextEditingController();
    getFaqs();
    super.onInit();
  }

  getFaqs() async {
    debugPrint(strapiUrl + faqUrl);
    Timer(const Duration(seconds: 0), () {
      Get.isDialogOpen ?? true
          ? const Offstage()
          : Get.dialog(const Center(child: CircularProgressIndicator()),
              barrierDismissible: false);
    });
    var data = await CoreService().getWithoutAuth(
      url: strapiUrl + faqUrl,
    );
    if (data != null) {
      faqResponseModel.value = FaqResponseModel.fromJson(data);
      sortByDisplayOrder(faqResponseModel.value.data!);
      if (Get.isDialogOpen ?? false) Get.back();
    } else {
      if (Get.isDialogOpen ?? false) Get.back();

      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: data["message"],
          backgroundColor: Colors.green,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    }
  }

  sortByDisplayOrder(List<OneFaqModel> results) {
    results.sort((a, b) => a.displayOrder!.compareTo(b.displayOrder!));
  }

  searchFaq(String searchKeyword) async {
    debugPrint(strapiUrl + searchFaqUrl + searchKeyword);
    Timer(const Duration(seconds: 0), () {
      Get.isDialogOpen ?? true
          ? const Offstage()
          : Get.dialog(const Center(child: CircularProgressIndicator()),
              barrierDismissible: false);
    });
    var data = await CoreService().getWithoutAuth(
      url: strapiUrl + searchFaqUrl + searchKeyword,
    );
    if (data != null) {
      faqResponseModel.value = FaqResponseModel.fromJson(data);
      if (Get.isDialogOpen ?? false) Get.back();
    } else {
      if (Get.isDialogOpen ?? false) Get.back();

      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: data["message"],
          backgroundColor: Colors.green,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    }
  }
}
