// ignore_for_file: unused_local_variable, prefer_typing_uninitialized_variables
import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/leads_view_controller.dart';
import 'package:iqrate/Controller/profile_controller.dart';
import 'package:iqrate/DeviceManager/container_styles.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Model/response_model.dart/create_lead_response_model.dart';
import 'package:iqrate/Model/response_model.dart/pdpa_token_response.dart';
import 'package:iqrate/Model/response_model.dart/update_client_details_response_model.dart';
import 'package:iqrate/Model/send_model.dart/create_lead_client_send_model.dart';
import 'package:iqrate/Model/send_model.dart/verify_pdpa_send_model.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/secondary_button.dart';

import '../Model/response_model.dart/fetch_lead_list_response_model.dart';
import '../Model/response_model.dart/verify_pdpa_response_model.dart';

class LeadProfileController extends GetxController {
  late RxDouble windowHeight;
  late RxDouble windowWidth;
  late RxInt selectedIndex;
  double windowHeightC = Get.height;
  double windowWidthC = Get.width;
  Result?
      leadDataFromView; //this value is initialized with the lead data passed in the constructor of the view file
  RxBool pdpaStatusForAllApplicant = false.obs;

  //lead profile
  TextEditingController emailController =
      TextEditingController(); //email text controller
  TextEditingController nameController =
      TextEditingController(); //name text controller
  TextEditingController phoneController =
      TextEditingController(); //phone text controller
  TextEditingController countryCodeController =
      TextEditingController(); //phone code text controller
  TextEditingController annualIncomeController =
      TextEditingController(); //annual Income text controller
  TextEditingController dobController = TextEditingController(); //dob
  TextEditingController nationalityController =
      TextEditingController(); //nationality text Controller

  //lead joint profile
  TextEditingController emailJointController =
      TextEditingController(); //email Joint text controller
  TextEditingController nameJointController =
      TextEditingController(); //name text controller
  TextEditingController phoneJointController =
      TextEditingController(); //phone text controller
  TextEditingController countryCodeJointController =
      TextEditingController(); //phone code text controller
  TextEditingController annualIncomeJointController =
      TextEditingController(); //annual Income text controller
  TextEditingController dobJointController = TextEditingController(); //dob
  TextEditingController nationalityJointController =
      TextEditingController(); //nationality joint text Controller

  //lead joint profile 2
  TextEditingController emailJointController2 =
      TextEditingController(); //email Joint text controller
  TextEditingController nameJointController2 =
      TextEditingController(); //name text controller
  TextEditingController phoneJointController2 =
      TextEditingController(); //phone text controller
  TextEditingController countryCodeJointController2 =
      TextEditingController(); //phone code text controller
  TextEditingController annualIncomeJointController2 =
      TextEditingController(); //annual Income text controller
  TextEditingController dobJointController2 = TextEditingController(); //dob
  TextEditingController nationalityJointController2 =
      TextEditingController(); //nationality joint text Controller
  TextEditingController verificationCodeController1 = TextEditingController();
  TextEditingController verificationCodeController2 = TextEditingController();

  //lead joint profile 3
  TextEditingController emailJointController3 =
      TextEditingController(); //email Joint text controller
  TextEditingController nameJointController3 =
      TextEditingController(); //name text controller
  TextEditingController phoneJointController3 =
      TextEditingController(); //phone text controller
  TextEditingController countryCodeJointController3 =
      TextEditingController(); //phone code text controller
  TextEditingController annualIncomeJointController3 =
      TextEditingController(); //annual Income text controller
  TextEditingController dobJointController3 = TextEditingController(); //dob
  TextEditingController nationalityJointController3 =
      TextEditingController(); //nationality joint text Controller

  final formKey = GlobalKey<FormState>();

  var isExpand = false.obs;
  var isShowJointAccount = false.obs;

  var isExpand2 = false.obs;
  var isShowJointAccount2 = false.obs;
  var isShowJointAccount3 = false.obs;

  ProfileScreenController profileScreenController = Get.find();

  int? leadID;
  String? documentsDrawerID;

  RxBool leadProfileCompleted =
      false.obs; //This is used to show the green check or show the red uncheck.

  String mainClientID = "";
  RxBool clientPDPAStatus = false.obs;

  var jointClientID1 = "".obs;
  RxBool jointClient1PDPAStatus = false.obs;

  var jointClientID2 = "".obs;
  RxBool jointClient2PDPAStatus = false.obs;

  var jointClientID3 = "".obs;
  RxBool jointClient3PDPAStatus = false.obs;

  LeadsViewController leadsViewController = Get.find();

  List dropdownForLeadCountries = [];

  //main account
  var totalCountdownSeconds = 60;
  var countDownSecond = 0.obs;
  Timer? timer;
  var countTimes = 0;
  var isResendButtonDisable = false.obs;
  var isResendButtonHide = true.obs;

  //joint 1
  var totalCountdownSeconds1 = 60;
  var countDownSecond1 = 0.obs;
  Timer? timer1;
  var countTimes1 = 0;
  var isResendButtonDisable1 = false.obs;
  var isResendButtonHide1 = true.obs;

  //joint 2
  var totalCountdownSeconds2 = 60;
  var countDownSecond2 = 0.obs;
  Timer? timer2;
  var countTimes2 = 0;
  var isResendButtonDisable2 = false.obs;
  var isResendButtonHide2 = true.obs;

  //joint 3
  var totalCountdownSeconds3 = 60;
  var countDownSecond3 = 0.obs;
  Timer? timer3;
  var countTimes3 = 0;
  var isResendButtonDisable3 = false.obs;
  var isResendButtonHide3 = true.obs;

  @override
  onInit() {
    selectedIndex = 0.obs;
    windowHeight = Get.height.obs;
    windowWidth = Get.width.obs;

    countryCodeController.text = "+65";
    countryCodeJointController.text = "+65";
    countryCodeJointController2.text = "+65";
    countryCodeJointController3.text = "+65";

    setNationalityDropdown();

    super.onInit();
  }

  resendSMSCodeTimer() {
    isResendButtonDisable.value = true; //resend button disable
    isResendButtonHide.value = false; //resend button disable

    debugPrint('countDownSecond::${countDownSecond.value}');
    debugPrint('countTimes::$countTimes');

    if (countTimes == 3) {
      isResendButtonDisable.value = true; //resend button enable
      isResendButtonHide.value = true;
      return;
    }

    countDownSecond.value = totalCountdownSeconds; //initial total seconds

    // Start the periodic timer which prints remaining seconds after every 1 seconds
    timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      --countDownSecond.value; //countDownSecond decrement

      debugPrint('second::${countDownSecond.value}');

      if (countDownSecond.value == 0) {
        isResendButtonDisable.value = false; //resend button enable
        isResendButtonHide.value = true; //resend button enable

        countTimes++;
        closeCountdownTimer();

        switch (countTimes) {
          case 1:
          case 2:
            totalCountdownSeconds = 60;
            break;
          default:
            totalCountdownSeconds = 0;
        }
      }
    });
  }

  resendSMSCodeTimer1() {
    isResendButtonDisable1.value = true; //resend button disable
    isResendButtonHide1.value = false; //resend button disable

    debugPrint('countDownSecond::${countDownSecond1.value}');
    debugPrint('countTimes::$countTimes1');

    if (countTimes == 3) {
      isResendButtonDisable1.value = true; //resend button enable
      isResendButtonHide1.value = true;
      return;
    }

    countDownSecond1.value = totalCountdownSeconds1; //initial total seconds

    // Start the periodic timer which prints remaining seconds after every 1 seconds
    timer1 = Timer.periodic(const Duration(seconds: 1), (timer) {
      --countDownSecond1.value; //countDownSecond decrement

      debugPrint('second::${countDownSecond1.value}');

      if (countDownSecond1.value == 0) {
        isResendButtonDisable1.value = false; //resend button enable
        isResendButtonHide1.value = true; //resend button enable

        countTimes1++;
        closeCountdownTimer1();

        switch (countTimes1) {
          case 1:
          case 2:
            totalCountdownSeconds1 = 60;
            break;
          default:
            totalCountdownSeconds1 = 0;
        }
      }
    });
  }

  resendSMSCodeTimer2() {
    isResendButtonDisable2.value = true; //resend button disable
    isResendButtonHide2.value = false; //resend button disable

    debugPrint('countDownSecond::${countDownSecond2.value}');
    debugPrint('countTimes::$countTimes2');

    if (countTimes2 == 3) {
      isResendButtonDisable2.value = true; //resend button enable
      isResendButtonHide2.value = true;
      return;
    }

    countDownSecond2.value = totalCountdownSeconds2; //initial total seconds

    // Start the periodic timer which prints remaining seconds after every 1 seconds
    timer2 = Timer.periodic(const Duration(seconds: 1), (timer) {
      --countDownSecond2.value; //countDownSecond decrement

      debugPrint('second::${countDownSecond2.value}');

      if (countDownSecond2.value == 0) {
        isResendButtonDisable2.value = false; //resend button enable
        isResendButtonHide2.value = true; //resend button enable

        countTimes2++;
        closeCountdownTimer2();

        switch (countTimes2) {
          case 1:
          case 2:
            totalCountdownSeconds2 = 60;
            break;
          default:
            totalCountdownSeconds2 = 0;
        }
      }
    });
  }

  resendSMSCodeTimer3() {
    isResendButtonDisable3.value = true; //resend button disable
    isResendButtonHide3.value = false; //resend button disable

    debugPrint('countDownSecond::${countDownSecond3.value}');
    debugPrint('countTimes::$countTimes3');

    if (countTimes3 == 3) {
      isResendButtonDisable3.value = true; //resend button enable
      isResendButtonHide3.value = true;
      return;
    }

    countDownSecond3.value = totalCountdownSeconds3; //initial total seconds

    // Start the periodic timer which prints remaining seconds after every 1 seconds
    timer3 = Timer.periodic(const Duration(seconds: 1), (timer) {
      --countDownSecond3.value; //countDownSecond decrement

      debugPrint('second::${countDownSecond3.value}');

      if (countDownSecond3.value == 0) {
        isResendButtonDisable3.value = false; //resend button enable
        isResendButtonHide3.value = true; //resend button enable

        countTimes3++;
        closeCountdownTimer3();

        switch (countTimes) {
          case 1:
          case 2:
            totalCountdownSeconds3 = 60;
            break;
          default:
            totalCountdownSeconds3 = 0;
        }
      }
    });
  }

  closeCountdownTimer() {
    //close resend timer when verify successful

    if (timer == null) {
      return;
    }

    isResendButtonDisable.value = false; //resend button enable
    isResendButtonHide.value = true; //resend button enable

    timer?.cancel();
  }

  closeCountdownTimer1() {
    //close resend timer when verify successful

    if (timer1 == null) {
      return;
    }

    isResendButtonDisable1.value = false; //resend button enable
    isResendButtonHide1.value = true; //resend button enable

    timer1?.cancel();
  }

  closeCountdownTimer2() {
    //close resend timer when verify successful

    if (timer2 == null) {
      return;
    }

    isResendButtonDisable2.value = false; //resend button enable
    isResendButtonHide2.value = true; //resend button enable

    timer2?.cancel();
  }

  closeCountdownTimer3() {
    //close resend timer when verify successful

    if (timer3 == null) {
      return;
    }

    isResendButtonDisable3.value = false; //resend button enable
    isResendButtonHide3.value = true; //resend button enable

    timer3?.cancel();
  }

  setNationalityDropdown() {
    debugPrint("=====setNationalityDropdown=====" +
        leadsViewController.kvStoreValues.length.toString());

    for (var element in leadsViewController.kvStoreValues) {
      //debugPrint("=====kvstore====="+element.toJson().toString());
      if (element.code == "nationality") {
        debugPrint("=====kvstore===== nationality" + element.value.toString());

        element.value.forEach((value) {
          dropdownForLeadCountries.add(value);
          debugPrint("array_key" + value);
        });
      }
    }
  }

  onTapSaveMainClient(String clientID) async {
    /*if (!clientPDPAStatus.value) {
      Get.snackbar(
        "Error",
        "PDPA Needs to be approved",
        snackPosition: SnackPosition.TOP,
        backgroundColor: Colors.red,
        colorText: Colors.white,
        borderRadius: 10,
      );
      return;
    }*/

    if (emailController.text.isEmpty &&
        nameController.text.isEmpty &&
        phoneController.text.isEmpty &&
        // annualIncomeController.text.isEmpty &&
        //dobController.text.isEmpty &&
        nationalityController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter all mandatory fields",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (emailController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter email",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (nameController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter name",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (phoneController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter mobile number",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (phoneController.text.length < 8) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter a valid number",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else {
      CreateLeadClientSendModel model;

      if (!clientPDPAStatus.value) {
        model = CreateLeadClientSendModel(
          lead: leadID,
          name: nameController.text,
          email: emailController.text,
          countryCode:
              removePlusSymbolForCountryCode(countryCodeController.text),
          phoneNumber: phoneController.text,
          mainApplicant: true,
        );
      } else {
        if (dobController.text.isEmpty ||
            nationalityController.text.isEmpty ||
            annualIncomeController.text.isEmpty) {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Please fill all fields",
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
          return;
        }

        model = CreateLeadClientSendModel(
          lead: leadID,
          name: nameController.text,
          email: emailController.text,
          countryCode:
              removePlusSymbolForCountryCode(countryCodeController.text),
          phoneNumber: phoneController.text,
          mainApplicant: true,
          nationality: nationalityController.text,
          annualIncome: annualIncomeController.text.isEmpty
              ? 0.0
              : double.parse(annualIncomeController.text.replaceAll(',', '')),
          dob: dobController.text.isEmpty
              ? ""
              : dobController.text.split('-').reversed.join('-'),
        );
      }

      Get.isDialogOpen ?? true
          ? const Offstage()
          : Get.dialog(
              const Center(
                child: CircularProgressIndicator(),
              ),
              barrierDismissible: false);

      var data = await CoreService().putWithAuth(
          url: baseUrl + updateLeadClientUrl + clientID, body: model.toJson());
      if (Get.isDialogOpen ?? false) Get.back();
      if (data == null) {
      } else {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            onTapSaveMainClient(clientID);
          }
        } else {
          var result = UpdateClientDetailsResponseModel.fromJson(data);

          leadsViewController.fetchLeads();
          // Function called to check the done status om the tab heading (If the lead profile is completed then green tick if not then red un-tick)
          checkTheCompletedStatus(
              pdpaStatus: result.pdpaStatus ?? false,
              dob: result.dob.toString(),
              nationality: result.nationality ?? '',
              annualIncome: result.annualIncome.toString());
          clientPDPAStatus.value = result.pdpaStatus!;

          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Lead updated successfully",
              backgroundColor: Colors.green,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
          //}

          /* if (!clientPDPAStatus.value && !isResendButtonDisable.value) {
            onTapSendPDPAApproval(mainClientID);
          }*/
        }
      }
    }

    checkIfPDPACompletedForAll();
  }

// Function used to check the dome status of the lead profile tab
  checkTheCompletedStatus(
      {required bool pdpaStatus,
      required String dob,
      required String nationality,
      required String annualIncome}) {
    if (pdpaStatus &&
        dob.isNotEmpty &&
        nationality.isNotEmpty &&
        annualIncome.isNotEmpty &&
        dob != 'null' &&
        nationality != 'null' &&
        annualIncome != 'null') {
      leadProfileCompleted.value = true;
    } else {
      leadProfileCompleted.value = false;
    }
  }

  removePlusSymbolForCountryCode(String code) {
    return code.replaceAll("+", "");
  }

  void onTapAddJoint() {
    isShowJointAccount.value = true;
  }

  void onTapCancelAddJoint() {
    isShowJointAccount.value = false;
  }

  Future<void> onTapDeleteJointAccount() async {
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);

    var data = await CoreService().deleteWithAuth(
        url: baseUrl + updateLeadClientUrl + jointClientID1.value);
    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
      leadsViewController.fetchLeads();

      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Joint applicant deleted successfully",
          backgroundColor: Colors.green,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      clearAddJointFields();
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          onTapDeleteJointAccount();
        }
      } else {
        leadsViewController.fetchLeads();

        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Joint applicant deleted successfully",
            backgroundColor: Colors.green,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
        clearAddJointFields();
      }
    }

    onTapCancelAddJoint();

    checkIfPDPACompletedForAll();
    //Get.back();
  }

  void onTapAddJoint2() {
    isShowJointAccount2.value = true;
  }

  void onTapAddJoint3() {
    isShowJointAccount3.value = true;
  }

  void onTapCancelAddJoint2() {
    isShowJointAccount2.value = false;
  }

  Future<void> onTapDeleteJointAccount2() async {
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);

    var data = await CoreService().deleteWithAuth(
        url: baseUrl + updateLeadClientUrl + jointClientID2.value);
    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
      leadsViewController.fetchLeads();

      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Joint applicant deleted successfully",
          backgroundColor: Colors.green,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      onTapCancelAddJoint2();

      clearAddJointFields2();
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          onTapDeleteJointAccount2();
        }
      } else {
        leadsViewController.fetchLeads();

        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Joint applicant deleted successfully",
            backgroundColor: Colors.green,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      }
    }

    onTapCancelAddJoint2();
    clearAddJointFields2();
    checkIfPDPACompletedForAll();
  }

  void onTapCancelAddJoint3() {
    isShowJointAccount3.value = false;
  }

  Future<void> onTapDeleteJointAccount3() async {
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);

    var data = await CoreService().deleteWithAuth(
        url: baseUrl + updateLeadClientUrl + jointClientID3.value);
    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
      leadsViewController.fetchLeads();

      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Joint applicant deleted successfully",
          backgroundColor: Colors.green,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      onTapCancelAddJoint3();

      clearAddJointFields3();
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          onTapDeleteJointAccount3();
        }
      } else {
        leadsViewController.fetchLeads();

        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Joint applicant deleted successfully",
            backgroundColor: Colors.green,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      }
    }

    onTapCancelAddJoint3();

    clearAddJointFields3();
    checkIfPDPACompletedForAll();
  }

  Future<void> onTapSendPDPAApproval(String clientID) async {
    //showPDPAApprovalAlert(false, clientID);
    var isSuccess = await resendPDPAApproval(clientID);
    if (isSuccess) {
      resendSMSCodeTimer();
    }
  }

  Future<void> onTapSendPDPAApproval1(String clientID) async {
    //showPDPAApprovalAlert(false, clientID);
    var isSuccess = await resendPDPAApproval(clientID);
    if (isSuccess) {
      resendSMSCodeTimer1();
    }
  }

  Future<void> onTapSendPDPAApproval2(String clientID) async {
    //showPDPAApprovalAlert(false, clientID);
    var isSuccess = await resendPDPAApproval(clientID);
    if (isSuccess) {
      resendSMSCodeTimer2();
    }
  }

  Future<void> onTapSendPDPAApproval3(String clientID) async {
    //showPDPAApprovalAlert(false, clientID);
    var isSuccess = await resendPDPAApproval(clientID);
    if (isSuccess) {
      resendSMSCodeTimer3();
    }
  }

  Future<void> onTapAddJointSave() async {
    if (emailJointController.text.isEmpty ||
        nameJointController.text.isEmpty ||
        phoneJointController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter all mandatory fields",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return;
    } else if (phoneJointController.text.length < 8) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter a valid number",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else {
      CreateLeadClientSendModel model;

      if (jointClientID1.isEmpty) {
        model = CreateLeadClientSendModel(
          lead: leadID,
          name: nameJointController.text,
          email: emailJointController.text,
          phoneNumber: phoneJointController.text,
          countryCode:
              removePlusSymbolForCountryCode(countryCodeJointController.text),
          mainApplicant: false,
        );
      } else {
        if (!jointClient1PDPAStatus.value) {
          model = CreateLeadClientSendModel(
            lead: leadID,
            name: nameJointController.text,
            email: emailJointController.text,
            phoneNumber: phoneJointController.text,
            countryCode:
                removePlusSymbolForCountryCode(countryCodeJointController.text),
            mainApplicant: false,
          );
        } else {
          if (jointClientID1.isNotEmpty &&
              (dobJointController.text.isEmpty ||
                  nationalityJointController.text.isEmpty ||
                  annualIncomeJointController.text.isEmpty)) {
            Future.delayed(const Duration(milliseconds: 0), () {
              FocusManager.instance.primaryFocus!.unfocus();
              Fluttertoast.showToast(
                timeInSecForIosWeb: 3,
                msg: "Please enter all mandatory fields",
                backgroundColor: Colors.red,
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.BOTTOM,
                textColor: Colors.white,
                fontSize: 20.0,
              );
            });
            return;
          }

          model = CreateLeadClientSendModel(
            lead: leadID,
            name: nameJointController.text,
            email: emailJointController.text,
            phoneNumber: phoneJointController.text,
            countryCode:
                removePlusSymbolForCountryCode(countryCodeJointController.text),
            mainApplicant: false,
            nationality: nationalityJointController.text,
            annualIncome: double.parse(
                annualIncomeJointController.text.replaceAll(',', '')),
            dob: dobJointController.text.split('-').reversed.join('-'),
          );
        }
      }

      Get.isDialogOpen ?? true
          ? const Offstage()
          : Get.dialog(
              const Center(
                child: CircularProgressIndicator(),
              ),
              barrierDismissible: false);

      var data;

      if (jointClientID1.value.isEmpty) {
        data = await CoreService().postWithAuth(
          url: baseUrl + leadClientCreateUrl,
          body: model.toJson(),
        );
      } else {
        data = await CoreService().putWithAuth(
          url: baseUrl + updateLeadClientUrl + jointClientID1.value,
          body: model.toJson(),
        );
      }

      if (Get.isDialogOpen ?? false) Get.back();
      if (data == null) {
      } else {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            onTapAddJointSave();
          }
        } else {
          leadsViewController.fetchLeads();

          var result = CreateLeadResponseModel.fromJson(data);

          log(data.toString());

          if (result.id != null) {
            Future.delayed(const Duration(milliseconds: 0), () {
              FocusManager.instance.primaryFocus!.unfocus();
              Fluttertoast.showToast(
                timeInSecForIosWeb: 3,
                msg: jointClientID1.value.isEmpty
                    ? "Joint applicant created Successfully"
                    : "Joint applicant updated Successfully",
                backgroundColor: Colors.green,
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.BOTTOM,
                textColor: Colors.white,
                fontSize: 20.0,
              );
            });
            jointClient1PDPAStatus.value = data['pdpa_status'];

            /*if (jointClientID1.value.isEmpty || !jointClient1PDPAStatus.value) {
              onTapSendPDPAApproval1(result.id.toString());
            }*/

            if (jointClientID1.value.isNotEmpty &&
                !jointClient1PDPAStatus.value &&
                !isResendButtonDisable1.value) {
              onTapSendPDPAApproval1(jointClientID1.value);
            }

            jointClientID1.value = result.id.toString();
          } else {
            Future.delayed(const Duration(milliseconds: 0), () {
              FocusManager.instance.primaryFocus!.unfocus();
              Fluttertoast.showToast(
                timeInSecForIosWeb: 3,
                msg: "Creating joint applicant failed",
                backgroundColor: Colors.red,
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.BOTTOM,
                textColor: Colors.white,
                fontSize: 20.0,
              );
            });
          }
        }
      }
    }
    checkIfPDPACompletedForAll();
  }

  clearAddJointFields() {
    nameJointController.clear();
    emailJointController.clear();
    phoneJointController.clear();
    countryCodeJointController.clear();
    nationalityJointController.clear();
    annualIncomeJointController.clear();
    dobJointController.clear();
    jointClientID1.value = "";
    jointClient1PDPAStatus.value = false;
  }

  Future<void> onTapAddJointSave2() async {
    if (emailJointController2.text.isEmpty ||
        nameJointController2.text.isEmpty ||
        phoneJointController2.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter all mandatory fields",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return;
    } else if (phoneJointController2.text.length < 8) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter a valid number",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else {
      CreateLeadClientSendModel model;

      if (jointClientID2.isEmpty) {
        model = CreateLeadClientSendModel(
            lead: leadID,
            name: nameJointController2.text,
            email: emailJointController2.text,
            phoneNumber: phoneJointController2.text,
            countryCode: removePlusSymbolForCountryCode(
                countryCodeJointController2.text),
            mainApplicant: false);
      } else {
        if (!jointClient2PDPAStatus.value) {
          model = CreateLeadClientSendModel(
            lead: leadID,
            name: nameJointController2.text,
            email: emailJointController2.text,
            phoneNumber: phoneJointController2.text,
            countryCode: removePlusSymbolForCountryCode(
                countryCodeJointController2.text),
            mainApplicant: false,
            nationality: nationalityJointController2.text,
          );
        } else {
          if (jointClientID2.isNotEmpty &&
              (dobJointController2.text.isEmpty ||
                  nationalityJointController2.text.isEmpty ||
                  annualIncomeJointController2.text.isEmpty)) {
            Future.delayed(const Duration(milliseconds: 0), () {
              FocusManager.instance.primaryFocus!.unfocus();
              Fluttertoast.showToast(
                timeInSecForIosWeb: 3,
                msg: "Please enter all mandatory fields",
                backgroundColor: Colors.red,
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.BOTTOM,
                textColor: Colors.white,
                fontSize: 20.0,
              );
            });
            return;
          }

          model = CreateLeadClientSendModel(
            lead: leadID,
            name: nameJointController2.text,
            email: emailJointController2.text,
            phoneNumber: phoneJointController2.text,
            countryCode: removePlusSymbolForCountryCode(
                countryCodeJointController2.text),
            mainApplicant: false,
            nationality: nationalityJointController2.text,
            annualIncome: double.parse(
                annualIncomeJointController2.text.replaceAll(',', '')),
            dob: dobJointController2.text.split('-').reversed.join('-'),
          );
        }
      }

      Get.isDialogOpen ?? true
          ? const Offstage()
          : Get.dialog(
              const Center(
                child: CircularProgressIndicator(),
              ),
              barrierDismissible: false);

      var data;

      if (jointClientID2.value.isEmpty) {
        data = await CoreService().postWithAuth(
          url: baseUrl + leadClientCreateUrl,
          body: model.toJson(),
        );
      } else {
        data = await CoreService().putWithAuth(
          url: baseUrl + updateLeadClientUrl + jointClientID2.value,
          body: model.toJson(),
        );
      }

      if (Get.isDialogOpen ?? false) Get.back();
      if (data == null) {
      } else {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            onTapAddJointSave2();
          }
        } else {
          leadsViewController.fetchLeads();

          var result = CreateLeadResponseModel.fromJson(data);
          if (result.id != null) {
            Future.delayed(const Duration(milliseconds: 0), () {
              FocusManager.instance.primaryFocus!.unfocus();
              Fluttertoast.showToast(
                timeInSecForIosWeb: 3,
                msg: jointClientID2.value.isEmpty
                    ? "Joint account details added Successfully"
                    : "Joint account details updated Successfully",
                backgroundColor: Colors.green,
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.BOTTOM,
                textColor: Colors.white,
                fontSize: 20.0,
              );
            });
            jointClient2PDPAStatus.value = data['pdpa_status'];

            if (jointClientID2.value.isEmpty || !jointClient2PDPAStatus.value) {
              onTapSendPDPAApproval2(result.id.toString());
              if (Get.isDialogOpen ?? false) Get.back();
              if (data == null) {
              } else {
                leadsViewController.fetchLeads();

                var result = CreateLeadResponseModel.fromJson(data);
                if (result.id != null) {
                  Future.delayed(const Duration(milliseconds: 0), () {
                    FocusManager.instance.primaryFocus!.unfocus();
                    Fluttertoast.showToast(
                      timeInSecForIosWeb: 3,
                      msg: jointClientID2.value.isEmpty
                          ? "Joint applicant created Successfully"
                          : "Joint applicant updated Successfully",
                      backgroundColor: Colors.green,
                      toastLength: Toast.LENGTH_LONG,
                      gravity: ToastGravity.BOTTOM,
                      textColor: Colors.white,
                      fontSize: 20.0,
                    );
                  });
                  jointClient2PDPAStatus.value = data['pdpa_status'];

                  /*if (jointClientID2.value.isEmpty ||
                      !jointClient2PDPAStatus.value) {
                    onTapSendPDPAApproval2(result.id.toString());
                  }*/

                  if (jointClientID2.value.isNotEmpty &&
                      !jointClient2PDPAStatus.value &&
                      !isResendButtonDisable2.value) {
                    onTapSendPDPAApproval2(jointClientID2.value);
                  }

                  jointClientID2.value = result.id.toString();
                } else {
                  Future.delayed(const Duration(milliseconds: 0), () {
                    FocusManager.instance.primaryFocus!.unfocus();
                    Fluttertoast.showToast(
                      timeInSecForIosWeb: 3,
                      msg: "Adding of joint applicant failed",
                      backgroundColor: Colors.red,
                      toastLength: Toast.LENGTH_LONG,
                      gravity: ToastGravity.BOTTOM,
                      textColor: Colors.white,
                      fontSize: 20.0,
                    );
                  });
                }

                jointClientID2.value = result.id.toString();
              }
            } /*else {
              Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
                Fluttertoast.showToast(
                  timeInSecForIosWeb: 3,
                  msg: "Adding of joint applicant failed",
                  backgroundColor: Colors.red,
                  toastLength: Toast.LENGTH_LONG,
                  gravity: ToastGravity.BOTTOM,
                  textColor: Colors.white,
                  fontSize: 20.0,
                );
              });
            }*/
          }
        }
      }
    }
    checkIfPDPACompletedForAll();
  }

  clearAddJointFields2() {
    nameJointController2.clear();
    emailJointController2.clear();
    phoneJointController2.clear();
    countryCodeJointController2.clear();
    nationalityJointController2.clear();
    annualIncomeJointController2.clear();
    dobJointController2.clear();
    jointClientID2.value = "";

    jointClient2PDPAStatus.value = false;
  }

  Future<void> onTapAddJointSave3() async {
    if (emailJointController3.text.isEmpty ||
        nameJointController3.text.isEmpty ||
        phoneJointController3.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter all mandatory fields",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return;
    } else if (phoneJointController3.text.length < 8) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter a valid number",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else {
      CreateLeadClientSendModel model;

      if (jointClientID3.isEmpty) {
        model = CreateLeadClientSendModel(
            lead: leadID,
            name: nameJointController3.text,
            email: emailJointController3.text,
            phoneNumber: phoneJointController3.text,
            countryCode: removePlusSymbolForCountryCode(
                countryCodeJointController3.text),
            mainApplicant: false);
      } else {
        if (!jointClient3PDPAStatus.value) {
          model = CreateLeadClientSendModel(
            lead: leadID,
            name: nameJointController3.text,
            email: emailJointController3.text,
            phoneNumber: phoneJointController3.text,
            countryCode: removePlusSymbolForCountryCode(
                countryCodeJointController3.text),
            mainApplicant: false,
          );
        } else {
          if (jointClientID3.isNotEmpty &&
              (dobJointController3.text.isEmpty ||
                  nationalityJointController3.text.isEmpty ||
                  annualIncomeJointController3.text.isEmpty)) {
            Future.delayed(const Duration(milliseconds: 0), () {
              FocusManager.instance.primaryFocus!.unfocus();
              Fluttertoast.showToast(
                timeInSecForIosWeb: 3,
                msg: "Please enter all mandatory fields",
                backgroundColor: Colors.red,
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.BOTTOM,
                textColor: Colors.white,
                fontSize: 20.0,
              );
            });
            return;
          }

          model = CreateLeadClientSendModel(
            lead: leadID,
            name: nameJointController3.text,
            email: emailJointController3.text,
            phoneNumber: phoneJointController3.text,
            countryCode: removePlusSymbolForCountryCode(
                countryCodeJointController3.text),
            mainApplicant: false,
            nationality: nationalityJointController3.text,
            annualIncome: double.parse(
                annualIncomeJointController3.text.replaceAll(',', '')),
            dob: dobJointController3.text.split('-').reversed.join('-'),
          );
        }
      }

      Get.isDialogOpen ?? true
          ? const Offstage()
          : Get.dialog(
              const Center(
                child: CircularProgressIndicator(),
              ),
              barrierDismissible: false);

      var data;

      if (jointClientID3.value.isEmpty) {
        data = await CoreService().postWithAuth(
          url: baseUrl + leadClientCreateUrl,
          body: model.toJson(),
        );
      } else {
        data = await CoreService().putWithAuth(
          url: baseUrl + updateLeadClientUrl + jointClientID3.value,
          body: model.toJson(),
        );
      }

      if (Get.isDialogOpen ?? false) Get.back();
      if (data == null) {
      } else {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            onTapAddJointSave3();
          }
        } else {
          leadsViewController.fetchLeads();

          var result = CreateLeadResponseModel.fromJson(data);
          if (result.id != null) {
            Future.delayed(const Duration(milliseconds: 0), () {
              FocusManager.instance.primaryFocus!.unfocus();
              Fluttertoast.showToast(
                timeInSecForIosWeb: 3,
                msg: jointClientID3.value.isEmpty
                    ? "Joint account details Successfully"
                    : "Joint account details updated Successfully",
                backgroundColor: Colors.green,
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.BOTTOM,
                textColor: Colors.white,
                fontSize: 20.0,
              );
            });
            jointClient3PDPAStatus.value = data['pdpa_status'];

            if (jointClientID3.value.isEmpty || !jointClient3PDPAStatus.value) {
              onTapSendPDPAApproval3(result.id.toString());
              if (Get.isDialogOpen ?? false) Get.back();
              if (data == null) {
              } else {
                leadsViewController.fetchLeads();

                var result = CreateLeadResponseModel.fromJson(data);
                if (result.id != null) {
                  Future.delayed(const Duration(milliseconds: 0), () {
                    FocusManager.instance.primaryFocus!.unfocus();
                    Fluttertoast.showToast(
                      timeInSecForIosWeb: 3,
                      msg: jointClientID3.value.isEmpty
                          ? "Joint applicant created Successfully"
                          : "Joint applicant updated Successfully",
                      backgroundColor: Colors.green,
                      toastLength: Toast.LENGTH_LONG,
                      gravity: ToastGravity.BOTTOM,
                      textColor: Colors.white,
                      fontSize: 20.0,
                    );
                  });
                  jointClient3PDPAStatus.value = data['pdpa_status'];

                  /*if (jointClientID3.value.isEmpty ||
                      !jointClient3PDPAStatus.value) {
                    onTapSendPDPAApproval3(result.id.toString());
                  }*/

                  if (jointClientID3.value.isNotEmpty &&
                      !jointClient3PDPAStatus.value &&
                      !isResendButtonDisable3.value) {
                    onTapSendPDPAApproval3(jointClientID3.value);
                  }

                  jointClientID3.value = result.id.toString();
                } else {
                  Future.delayed(const Duration(milliseconds: 0), () {
                    FocusManager.instance.primaryFocus!.unfocus();
                    Fluttertoast.showToast(
                      timeInSecForIosWeb: 3,
                      msg: "Adding of joint applicant failed",
                      backgroundColor: Colors.red,
                      toastLength: Toast.LENGTH_LONG,
                      gravity: ToastGravity.BOTTOM,
                      textColor: Colors.white,
                      fontSize: 20.0,
                    );
                  });
                }

                jointClientID3.value = result.id.toString();
              }
            } /*else {
              Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
                Fluttertoast.showToast(
                  timeInSecForIosWeb: 3,
                  msg: "Adding of joint applicant failed",
                  backgroundColor: Colors.red,
                  toastLength: Toast.LENGTH_LONG,
                  gravity: ToastGravity.BOTTOM,
                  textColor: Colors.white,
                  fontSize: 20.0,
                );
              });
            }*/
          }
        }
      }
    }
    checkIfPDPACompletedForAll();
  }

  clearAddJointFields3() {
    nameJointController3.clear();
    emailJointController3.clear();
    phoneJointController3.clear();
    countryCodeJointController3.clear();
    nationalityJointController3.clear();
    annualIncomeJointController3.clear();
    dobJointController3.clear();
    jointClientID3.value = "";
    jointClient3PDPAStatus.value = false;
  }

  void onTapSendPDPAApprovalAddJoint() {
    showPDPAApprovalAlert(true, "");
  }

  void resendPDPAApprovalForJointAccount() {}

  Future<bool> resendPDPAApproval(String clientID) async {
    /*Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
        const Center(
          child: CircularProgressIndicator(),
        ),
        barrierDismissible: false);*/

    var data = await CoreService().getWithAuth(
      url: baseUrl + pdpaTokenGenerateUrl + clientID,
    );

    //if (Get.isDialogOpen ?? false) Get.back();

    if (data == null) {
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          resendPDPAApproval(clientID);
        }
      } else {
        var result = PdpaTokenResponse.fromJson(data);
        if (result.message != null) {
          //Get.back();
          // Get.defaultDialog(
          //   title: "",
          //   content: SizedBox(
          //     height: windowHeightC * 0.4,
          //     width: windowWidthC * 0.9,
          //     child: SingleChildScrollView(
          //       child: Padding(
          //         padding:
          //             const EdgeInsets.symmetric(vertical: 10.0, horizontal: 18),
          //         child: Column(
          //           mainAxisAlignment: MainAxisAlignment.start,
          //           crossAxisAlignment: CrossAxisAlignment.center,
          //           children: [
          //             SizedBox(
          //               height: windowHeightC * 0.01,
          //             ),
          //             Text(
          //               "A verification code has been send to your mobile number. Please enter the code to verify your PDPA .",
          //               style: TextStyle(
          //                 color: Colors.black,
          //                 fontSize: FontSize.s15,
          //                 fontWeight: FontWeight.w500,
          //               ),
          //               textAlign: TextAlign.center,
          //             ),
          //             SizedBox(
          //               height: windowHeightC * 0.01,
          //             ),
          //             Column(
          //               children: [
          //                 SizedBox(height: windowHeightC * 0.01),
          //                 const Align(
          //                   alignment: Alignment.centerLeft,
          //                   child: FormFieldTitle(title: 'Verification Code*'),
          //                 ),
          //                 SizedBox(height: windowHeightC * 0.01),
          //                 Row(
          //                   children: [
          //                     Expanded(
          //                       flex: 1,
          //                       child: RequireTextField(
          //                         maxLength: 3,
          //                         type: Type.text,
          //                         controller: verificationCodeController1,
          //                       ),
          //                     ),
          //                     Text(
          //                       '  -  ',
          //                       style: TextStyle(
          //                           color: Colors.black, fontSize: FontSize.s20),
          //                     ),
          //                     Expanded(
          //                       flex: 3,
          //                       child: RequireTextField(
          //                         maxLength: 8,
          //                         type: Type.pdpa,
          //                         controller: verificationCodeController2,
          //                       ),
          //                     ),
          //                   ],
          //                 ),
          //               ],
          //             ),
          //             SizedBox(height: windowHeightC * 0.05),
          //             PrimaryButton(
          //               windowHeight: windowHeightC,
          //               windowWidth: windowWidthC,
          //               buttonTitle: "Verify",
          //               onPressed: () {
          //                 verifyPdpaToken();
          //               },
          //             )
          //           ],
          //         ),
          //       ),
          //     ),
          //   ),
          // );
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Please check your messages to proceed further",
              backgroundColor: Colors.green,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });

          return Future.value(true);
        } else {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg:
                  "We are unable to process the request now. Please try again later",
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });

          return Future.value(false);
        }
      }
    }
    checkIfPDPACompletedForAll();
    return Future.value(false);
  }

  Future<void> verifyPDPA(String token) async {
    PdpaTokenSend model = PdpaTokenSend(token: token);

    var data = await CoreService()
        .postWithAuth(url: baseUrl + validatePdpaUrl, body: model.toJson());
    //if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          verifyPDPA(token);
        }
      } else {
        var result = PdpaTokenResponse.fromJson(data);
        if (result.message != null) {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "PDPA Approved",
              backgroundColor: Colors.green,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        } else {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "PDPA Verification failed",
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }
      }
    }
  }

  void showPDPAApprovalAlert(bool isPDPAJointAccount, String clientID) {
    Get.defaultDialog(
        title: "",
        content: Container(
          padding: const EdgeInsets.only(left: 30, right: 30, bottom: 20),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 5.0),
                  child: Text(
                    "All applicants will need to accept the PDPA via the link attached in the SMS.",
                    //textAlign: TextAlign.center,
                    style: TextStyles.leadsNormalTextStyle,
                  ),
                ),
              ),
              SizedBox(height: Get.height * 0.04),
              PrimaryButton(
                windowHeight: Get.height,
                windowWidth: Get.width,
                buttonTitle: "Resend PDPA Approval",
                onPressed: () {
                  if (isPDPAJointAccount) {
                    resendPDPAApprovalForJointAccount();
                  } else {
                    resendPDPAApproval(clientID);
                  }
                },
              ),
              SizedBox(height: Get.height * 0.05),
              SecondaryButton(
                windowHeight: Get.height,
                windowWidth: Get.width,
                kGradientBoxDecoration:
                    ContainerStyles.kGradientBoxDecorationSecondaryButton,
                kInnerDecoration:
                    ContainerStyles.kInnerDecorationSecondaryButton,
                buttonTitle: 'Cancel',
                onPressed: () {
                  Get.back();
                },
              ),
            ],
          ),
        ));
    checkIfPDPACompletedForAll();
  }

  Future<void> onTapPDPARefresh(String clientID) async {
    if (clientID.isEmpty || clientPDPAStatus.value) {
      return;
    }

    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);

    await leadsViewController.fetchLeads();
    if (Get.isDialogOpen ?? false) Get.back();

    leadsViewController.fetchLeadListResponseModel.results?.forEach((lead) {
      if (lead.id == leadID) {
        debugPrint("==========lead.id==========" +
            lead.id.toString() +
            "==========leadID========" +
            leadID.toString());
        lead.clients?.forEach((client) {
          if (client.id.toString() == clientID) {
            clientPDPAStatus.value = client.pdpaStatus!;
            debugPrint("==========client.id==========" +
                client.id.toString() +
                "==================");
            debugPrint("==========clientPDPAStatus==========" +
                client.pdpaStatus.toString() +
                "==================");
          }
        });
      }
      if (Get.isDialogOpen ?? false) Get.back();
    });

    if (Get.isDialogOpen ?? false) Get.back();

    Timer(const Duration(seconds: 3), () {
      if (Get.isDialogOpen ?? false) Get.back();
    });

    checkIfPDPACompletedForAll();
  }

  Future<void> onTapPDPARefresh1(String clientID) async {
    if (clientID.isEmpty || jointClient1PDPAStatus.value) {
      return;
    }

    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);

    await leadsViewController.fetchLeads();

    leadsViewController.fetchLeadListResponseModel.results?.forEach((lead) {
      if (lead.id == leadID) {
        debugPrint("==========lead.id==========" +
            lead.id.toString() +
            "==========leadID========" +
            leadID.toString());
        lead.clients?.forEach((client) {
          if (client.id.toString() == clientID) {
            jointClient1PDPAStatus.value = client.pdpaStatus!;
            debugPrint("==========client.id==========" +
                client.id.toString() +
                "==================");
            debugPrint("==========clientPDPAStatus==========" +
                client.pdpaStatus.toString() +
                "==================");
          }
        });
      }
    });

    if (Get.isDialogOpen ?? false) Get.back();

    Timer(const Duration(seconds: 3), () {
      if (Get.isDialogOpen ?? false) Get.back();
    });
    checkIfPDPACompletedForAll();
  }

  Future<void> onTapPDPARefresh2(String clientID) async {
    if (clientID.isEmpty || jointClient2PDPAStatus.value) {
      return;
    }

    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);

    await leadsViewController.fetchLeads();

    leadsViewController.fetchLeadListResponseModel.results?.forEach((lead) {
      if (lead.id == leadID) {
        debugPrint("==========lead.id==========" +
            lead.id.toString() +
            "==========leadID========" +
            leadID.toString());
        lead.clients?.forEach((client) {
          if (client.id.toString() == clientID) {
            jointClient2PDPAStatus.value = client.pdpaStatus!;
            debugPrint("==========client.id==========" +
                client.id.toString() +
                "==================");
            debugPrint("==========clientPDPAStatus==========" +
                client.pdpaStatus.toString() +
                "==================");
          }
        });
      }
    });

    if (Get.isDialogOpen ?? false) Get.back();

    Timer(const Duration(seconds: 3), () {
      if (Get.isDialogOpen ?? false) Get.back();
    });

    checkIfPDPACompletedForAll();
  }

  Future<void> onTapPDPARefresh3(String clientID) async {
    if (clientID.isEmpty || jointClient3PDPAStatus.value) {
      return;
    }

    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);

    await leadsViewController.fetchLeads();

    leadsViewController.fetchLeadListResponseModel.results?.forEach((lead) {
      if (lead.id == leadID) {
        debugPrint("==========lead.id==========" +
            lead.id.toString() +
            "==========leadID========" +
            leadID.toString());
        lead.clients?.forEach((client) {
          if (client.id.toString() == clientID) {
            jointClient3PDPAStatus.value = client.pdpaStatus!;
            debugPrint("==========client.id==========" +
                client.id.toString() +
                "==================");
            debugPrint("==========clientPDPAStatus==========" +
                client.pdpaStatus.toString() +
                "==================");
          }
        });
      }
    });

    if (Get.isDialogOpen ?? false) Get.back();

    Timer(const Duration(seconds: 3), () {
      if (Get.isDialogOpen ?? false) Get.back();
    });

    checkIfPDPACompletedForAll();
  }

  @override
  void onClose() {
    closeCountdownTimer();
    closeCountdownTimer1();
    closeCountdownTimer2();
    closeCountdownTimer3();
    debugPrint("============onClose lead profile controller");
    super.onClose();
  }

  verifyPdpaToken() {
    if (verificationCodeController1.text.isEmpty &&
        verificationCodeController2.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter verification code",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else {
      onVerifyPdpaTokenButtonPress();
    }
  }

  onVerifyPdpaTokenButtonPress() async {
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);

    VerifyPdpaSendModel verifyPdpaSendModel = VerifyPdpaSendModel(
      token: verificationCodeController1.text +
          "-" +
          verificationCodeController2.text,
    );

    var data = await CoreService().postWithAuth(
      url: baseUrl + validatePdpaUrl,
      body: verifyPdpaSendModel.toJson(),
    );
    if (data == null) {
      if (Get.isDialogOpen ?? false) Get.back();
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          onVerifyPdpaTokenButtonPress();
        }
      } else {
        var result = VerifyPdpaResponseModel.fromJson(data);
        if (result.status == true) {
          if (Get.isDialogOpen ?? false) Get.back();

          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: result.message!,
              backgroundColor: Colors.green,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });

          clearTokenField();
          closeCountdownTimer();
          closeCountdownTimer1();
          closeCountdownTimer2();
          closeCountdownTimer3();
        } else {
          if (Get.isDialogOpen ?? false) Get.back();

          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: result.message!,
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }
      }
    }
  }

  clearTokenField() {
    verificationCodeController1.clear();
    verificationCodeController2.clear();
  }

  // Function to check if the PDPA has been completed for the main and the joint applicants
  bool checkIfPDPACompletedForAll() {
    pdpaStatusForAllApplicant.value = true;

    /*if (leadDataFromView != null) {
      for (int i = 0; i < leadDataFromView!.clients!.length; i++) {
        if (!leadDataFromView!.clients![i].pdpaStatus!) {
          pdpaStatusForAllApplicant.value = false;
        }
      }
    }
*/
    if (!clientPDPAStatus.value) {
      pdpaStatusForAllApplicant.value = false;
    }

    if (jointClientID1.value.isNotEmpty && !jointClient1PDPAStatus.value) {
      pdpaStatusForAllApplicant.value = false;
    }

    if (jointClientID2.value.isNotEmpty && !jointClient2PDPAStatus.value) {
      pdpaStatusForAllApplicant.value = false;
    }

    if (jointClientID3.value.isNotEmpty && !jointClient3PDPAStatus.value) {
      pdpaStatusForAllApplicant.value = false;
    }

    return pdpaStatusForAllApplicant.value;
  }
}
