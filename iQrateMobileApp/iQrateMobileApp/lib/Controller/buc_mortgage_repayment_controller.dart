// ignore_for_file: non_constant_identifier_names

import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:isolate';
import 'dart:ui';

import 'package:android_path_provider/android_path_provider.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:iqrate/Model/response_model.dart/buc_mortgage_pdf_response_model.dart';
import 'package:iqrate/Provider/network_provider.dart';
import 'package:iqrate/Service/url.dart';
import 'package:iqrate/Views/calculator/your_preferred_interest_rate.dart';
import 'package:logger/logger.dart';
import 'package:open_filex/open_filex.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

import '../DeviceManager/hive_string.dart';
import '../Model/header_model.dart';
import '../Model/response_model.dart/buc_progressive_repayment_schedule_model_sent.dart';
import '../Service/GetControllers.dart';
import '../Utils/text_formatter.dart';

import 'package:http/http.dart' as http;

class BucMortgageRepaymentController extends GetxController {
  var resultDownPayment = "".obs;
  var resultLoanAmount = "".obs;
  dynamic disbursement_schedule;
  var nameController = TextEditingController();
  var propertyPriceController = TextEditingController();
  var loanToValueController = TextEditingController();
  var loanTenureController = TextEditingController();
  var cpfAvailableController = TextEditingController(text: "0");
  var isCalculated = false.obs;

  TextEditingController textEditingController0 =
      TextEditingController(); //optionToPurchaseController
  TextEditingController textEditingController1 =
      TextEditingController(); //downPaymentController
  TextEditingController textEditingController2 =
      TextEditingController(); //foundationController
  TextEditingController textEditingController3 =
      TextEditingController(); //concreteFrameworkController
  TextEditingController textEditingController4 =
      TextEditingController(); //wallsController
  TextEditingController textEditingController5 =
      TextEditingController(); //ceilingController
  TextEditingController textEditingController6 =
      TextEditingController(); //doorsAndWindowsController
  TextEditingController textEditingController7 =
      TextEditingController(); //carParkController
  TextEditingController textEditingController8 =
      TextEditingController(); //temporaryOccupationPermitController
  TextEditingController textEditingController9 =
      TextEditingController(); //certificateOfSatuCompController

  RxList<TextEditingController> textEditingControllerList =
      <TextEditingController>[].obs;

  addingTextEditingControllerToList() {
    textEditingControllerList.add(textEditingController0);
    textEditingControllerList.add(textEditingController1);
    textEditingControllerList.add(textEditingController2);
    textEditingControllerList.add(textEditingController3);
    textEditingControllerList.add(textEditingController4);
    textEditingControllerList.add(textEditingController5);
    textEditingControllerList.add(textEditingController6);
    textEditingControllerList.add(textEditingController7);
    textEditingControllerList.add(textEditingController8);
    textEditingControllerList.add(textEditingController9);
  }

  var arrayYourPreferredInterestRateWidget =
      <YourPreferredInterestRateWidget>[];
  final Box hive = Hive.box(HiveString.hiveName);

  Rx<BucMortgagePdfResponseModel> bucMortgagePdfResponseModel =
      BucMortgagePdfResponseModel().obs;

  var prefered_rate_enable = false;

  var dropdown1 = "1".obs;
  var dropdown2 = "1".obs;
  var dropdown3 = "1".obs;
  var dropdown4 = "1".obs;
  var dropdown5 = "1".obs;
  var dropdown6 = "1".obs;
  var dropdown7 = "1".obs;
  var dropdown8 = "1".obs;

  @override
  void onInit() {
    getPermission();

    _prepareSaveDir();
    if (kDebugMode) {
      debugData();
    }
    super.onInit();
  }

  debugData() {
    nameController.text = "Rishabh";
    propertyPriceController.text = "1500000";
    loanToValueController.text = "55";
    loanTenureController.text = "20";
  }

  var arrayBucProgressiveRepaymentScheduleModelSent =
      <BucProgressiveRepaymentScheduleModelSent>[];
  // var arrayDisbursementStageController = <TextEditingController>[];
  var errorNotAllowZero = "0 is not acceptable";
  var notInRange = "1-100 acceptable";

  resetDataArrayBucProgressiveRepaymentScheduleModelSent() {
    arrayBucProgressiveRepaymentScheduleModelSent.clear();
    var a1 = BucProgressiveRepaymentScheduleModelSent(
        month: "6",
        disbursementName: "Completion of Foundation",
        percentage: "");
    var a2 = BucProgressiveRepaymentScheduleModelSent(
        month: "6",
        disbursementName: "Reinforcement Concrete",
        percentage: "1");
    var a3 = BucProgressiveRepaymentScheduleModelSent(
        month: "6", disbursementName: "Brick Walls", percentage: "1");
    var a4 = BucProgressiveRepaymentScheduleModelSent(
        month: "6", disbursementName: "Roofing / Ceiling", percentage: "1");
    var a5 = BucProgressiveRepaymentScheduleModelSent(
        month: "6", disbursementName: "Electrical Wiring", percentage: "1");
    var a6 = BucProgressiveRepaymentScheduleModelSent(
        month: "6",
        disbursementName: "Car Park, Roads, Drains",
        percentage: "1");
    var a7 = BucProgressiveRepaymentScheduleModelSent(
        month: "6",
        disbursementName: "Temporary Occupation Permit (TOP)",
        percentage: "1");
    var a8 = BucProgressiveRepaymentScheduleModelSent(
        month: "6",
        disbursementName: "Certificate of Statutory Completion(CSC)",
        percentage: "1");
    arrayBucProgressiveRepaymentScheduleModelSent.add(a1);
    arrayBucProgressiveRepaymentScheduleModelSent.add(a2);
    arrayBucProgressiveRepaymentScheduleModelSent.add(a3);
    arrayBucProgressiveRepaymentScheduleModelSent.add(a4);
    arrayBucProgressiveRepaymentScheduleModelSent.add(a5);
    arrayBucProgressiveRepaymentScheduleModelSent.add(a6);
    arrayBucProgressiveRepaymentScheduleModelSent.add(a7);
    arrayBucProgressiveRepaymentScheduleModelSent.add(a8);
  }

  bool validateTextFieldDownPayment() {
    if (loanToValueController.text.isEmpty) {
      GetControllers.shared.showAlertPopupOneButtonWithCallBack(
          content: "Please fill all the fields", callback: () {});
      return false;
    }

    if (loanTenureController.text.isEmpty) {
      GetControllers.shared.showAlertPopupOneButtonWithCallBack(
          content: "Please fill all the fields", callback: () {});
      return false;
    }

    if (double.parse(getAmountString(propertyPriceController.text)) < 1) {
      GetControllers.shared.showAlertPopupOneButtonWithCallBack(
          content: "Please fill all the fields", callback: () {});
      return false;
    }

    return true;
  }

  bool validateTextFieldB4Continue() {
    if (validateTextFieldDownPayment() == true) {
      if (nameController.text.isEmpty) {
        GetControllers.shared.showAlertPopupOneButtonWithCallBack(
            content: "Please fill all the fields", callback: () {});
        return false;
      }

      if (arrayYourPreferredInterestRateWidget.isNotEmpty) {
        debugPrint("have data ");
        for (var i = 0; i < arrayYourPreferredInterestRateWidget.length; i++) {
          var a =
              arrayYourPreferredInterestRateWidget[i].year1TextController.text;
          var b =
              arrayYourPreferredInterestRateWidget[i].year2TextController.text;
          var c =
              arrayYourPreferredInterestRateWidget[i].year3TextController.text;
          var d =
              arrayYourPreferredInterestRateWidget[i].year4TextController.text;
          var e =
              arrayYourPreferredInterestRateWidget[i].year5TextController.text;
          debugPrint("have data a $a");
          if (a.isEmpty || double.parse(a) > 100 || double.parse(a) < 1) {
            debugPrint("have data error");
            GetControllers.shared.showAlertPopupOneButtonWithCallBack(
                content: "Year 1  The value should be between 0.1 and 100",
                callback: () {});
            return false;
          }
          if (b.isEmpty || double.parse(b) > 100 || double.parse(b) < 1) {
            debugPrint("have data error");
            GetControllers.shared.showAlertPopupOneButtonWithCallBack(
                content: "Year 2  The value should be between 0.1 and 100",
                callback: () {});
            return false;
          }
          if (c.isEmpty || double.parse(c) > 100 || double.parse(c) < 1) {
            debugPrint("have data error");
            GetControllers.shared.showAlertPopupOneButtonWithCallBack(
                content: "Year 3  The value should be between 0.1 and 100",
                callback: () {});
            return false;
          }
          if (d.isEmpty || double.parse(d) > 100 || double.parse(d) < 1) {
            debugPrint("have data error");
            GetControllers.shared.showAlertPopupOneButtonWithCallBack(
                content: "Year 4  The value should be between 0.1 and 100",
                callback: () {});
            return false;
          }
          if (e.isEmpty || double.parse(e) > 100 || double.parse(e) < 1) {
            debugPrint("have data error");
            GetControllers.shared.showAlertPopupOneButtonWithCallBack(
                content: "Year 5  The value should be between 0.1 and 100",
                callback: () {});
            return false;
          }
        }
      }

      return true;
    }
    return false;
  }

  bool validateTextFieldB4GenerateReport() {
    return true; // always here becuse previous screen is validate already
  }

  reset() {
    propertyPriceController.clear();
    loanToValueController.clear();
    loanTenureController.clear();
    resultDownPayment.value = "";
    resultLoanAmount.value = "";
    isCalculated.value = false;
    cpfAvailableController.clear();
    nameController.clear();
    arrayYourPreferredInterestRateWidget.clear();
  }

  callAPICalculatorBucMortgageDownpayment(BuildContext context) async {
    _callAPICalculatorBucMortgageDownpayment(); // fucntion call API need check token!
  }

  _callAPICalculatorBucMortgageDownpayment() async {
    debugPrint("start callAPICalculatorBucMortgageDownpayment");
    GetControllers.shared.showLoading();
    Map<String, dynamic> bodyJson = {
      "token": GetControllers.shared.getToken(),
      // step 1 only need 2 param!!
      "property_price": getAmountString(propertyPriceController.text),
      // "ltv": loanToValueController.text,
      "ltv": double.parse(loanToValueController.text),
      "loan_tenure": double.parse(loanTenureController.text),

      "cpf_available_amount": cpfAvailableController.text.isNotEmpty
          ? double.parse(getAmountString(cpfAvailableController.text))
          : 0,
    };
    Logger().d(bodyJson);
    var endpoint = baseUrl + calculator_buc_mortgage_downpayment;
    var token = GetControllers.shared.getToken();
    var data = await NetworkProvider().callAPIPost(
        token: token,
        url: endpoint,
        data: bodyJson); //Send the otp to the server.
    GetControllers.shared.hideLoading();
    if (NetworkProvider().isSuccessAPI(data)) {
      isCalculated.value = true;
      resultDownPayment.value = (data["Down Payment"]).toString();
      resultLoanAmount.value = (data["Loan Amount"]).toString();
      disbursement_schedule = data["disbursement_schedule"];
      //disbursement_schedule

      textEditingController0.text = disbursement_schedule[0]['month'] != null
          ? disbursement_schedule[0]['month'].toString()
          : "6";
      textEditingController1.text = disbursement_schedule[1]['month'] != null
          ? disbursement_schedule[1]['month'].toString()
          : "6";
      textEditingController2.text = disbursement_schedule[2]['month'] != null
          ? disbursement_schedule[2]['month'].toString()
          : "6";
      textEditingController3.text = disbursement_schedule[3]['month'] != null
          ? disbursement_schedule[3]['month'].toString()
          : "6";
      textEditingController4.text = disbursement_schedule[4]['month'] != null
          ? disbursement_schedule[4]['month'].toString()
          : "6";
      textEditingController5.text = disbursement_schedule[5]['month'] != null
          ? disbursement_schedule[5]['month'].toString()
          : "6";
      textEditingController6.text = disbursement_schedule[6]['momth'] != null
          ? disbursement_schedule[6]['momth'].toString()
          : "6";
      textEditingController7.text = disbursement_schedule[7]['momth'] != null
          ? disbursement_schedule[7]['momth'].toString()
          : "6";
      textEditingController8.text = disbursement_schedule[8]['month'] != null
          ? disbursement_schedule[8]['month'].toString()
          : "6";
      textEditingController9.text = disbursement_schedule[9]['month'] != null
          ? disbursement_schedule[9]['month'].toString()
          : "6";
      addingTextEditingControllerToList();
      debugPrint("✅ OK callAPICalculatorBucMortgageDownpayment");
      //data
      Logger().d(data);
    } else {
      debugPrint("❌  mortgageCalculatorUrl");
      Logger().d(bodyJson);
      GetControllers.shared.showAlertPopupOneButtonWithCallBack(
          content: NetworkProvider().getErrorMessage(
            data['message'],
          ),
          callback: () {});
    }
  }

  callAPICalculatorBucMortgageReport(
      BuildContext context, Function(dynamic) callback) async {
    _callAPICalculatorBucMortgageReport(
        callback); // fucntion call API need check token!
  }

  _callAPICalculatorBucMortgageReport(Function(dynamic) callback) async {
    debugPrint("start callAPICalculatorBucMortgageReport");
    GetControllers.shared.showLoading();
    // if (va() == false) return;
    prefered_rate_enable = arrayYourPreferredInterestRateWidget.isNotEmpty;
    Map<String, dynamic> bodyJson = {
      "token": hive.get(HiveString.token),
      // step 1 only need 2 param!!
      "name": nameController.text, //1
      "cpf_available_amount": cpfAvailableController.text.isNotEmpty
          ? double.parse(getAmountString(cpfAvailableController.text))
          : 0,
      "cpf_available": (cpfAvailableController.text.isEmpty) ? "no" : "yes", //3
      "downpayment": getAmountString(resultDownPayment.value), //4
      "loan_amount": getAmountString(resultLoanAmount.value), //5
      // "downpayment": double.parse(getAmountString(resultDownPayment.value)),//4
      // "loan_amount": double.parse(getAmountString(resultLoanAmount.value)),//5
      "loan_tenure": double.parse(loanTenureController.text), //6
      "prefered_rate_enable":
          arrayYourPreferredInterestRateWidget.isNotEmpty ? "yes" : "no", //7
      "prefered_rate_year1": arrayYourPreferredInterestRateWidget.isNotEmpty //8
          ? arrayYourPreferredInterestRateWidget[0].year1TextController.text
          : null,
      "prefered_rate_year2": arrayYourPreferredInterestRateWidget.isNotEmpty //9
          ? arrayYourPreferredInterestRateWidget[0].year2TextController.text
          : null,
      "prefered_rate_year3":
          arrayYourPreferredInterestRateWidget.isNotEmpty //10
              ? arrayYourPreferredInterestRateWidget[0].year3TextController.text
              : null,
      "prefered_rate_year4":
          arrayYourPreferredInterestRateWidget.isNotEmpty //11
              ? arrayYourPreferredInterestRateWidget[0].year4TextController.text
              : null,
      "prefered_rate_year5":
          arrayYourPreferredInterestRateWidget.isNotEmpty //12
              ? arrayYourPreferredInterestRateWidget[0].year5TextController.text
              : null,
      "property_price": getAmountString(propertyPriceController.text), //13
      "ltv": loanToValueController.text, //14
      "disbursement_month": {
        "Option To Purchase": int.tryParse(textEditingController0.text),
        "Downpayment": int.tryParse(textEditingController1.text),
        "Foundation": int.tryParse(textEditingController2.text),
        "Concrete Framework": int.tryParse(textEditingController3.text),
        "Walls": int.tryParse(textEditingController4.text),
        "Ceiling": int.tryParse(textEditingController5.text),
        "Doors and Windows": int.tryParse(textEditingController6.text),
        "Car Park": int.tryParse(textEditingController7.text),
        "Temporary Occupation Permit":
            int.tryParse(textEditingController8.text),
        "Certificate of Statutory Completion":
            int.tryParse(textEditingController9.text),
      }
    };

    Logger().d(bodyJson);
    var token = GetControllers.shared.getToken();
    var endpoint = baseUrl + calculator_buc_mortgage_report;
    var data = await NetworkProvider().callAPIPost(
        token: token,
        url: endpoint,
        data: bodyJson); //Send the otp to the server.
    GetControllers.shared.hideLoading();
    if (NetworkProvider().isSuccessAPI(data)) {
      debugPrint("✅ OK callAPICalculatorBucMortgageReport");
      Logger().d(data);
      callback(data);
    } else {
      debugPrint("❌  mortgageCalculatorUrl");
      GetControllers.shared.showAlertPopupOneButtonWithCallBack(
          content: NetworkProvider().getErrorMessage(data), callback: () {});
    }
  }

  getExportField() {
    var a = <String>[];
    var b = GetControllers.shared.getBucMortgageRepaymentReportController();
    if (b.isCheckLoanDetails) {
      a.add("Loan Details");
    }
    if (b.isRateComparison) {
      a.add("Rate Comparison");
    }
    if (b.isDisbursementSchedule) {
      a.add("Disbursement Schedule");
    }
    if (b.isProgressiveRepayment) {
      a.add("Progressive Repayment");
    }
    if (b.isPreferredRepayment) {
      a.add("Preferred Repayment");
    }
    return a;
  }

  // callAPIDownloadBUCReport(Function(dynamic) callback) async {
  //   debugPrint("start callAPIDownloadBUCReport");
  //   GetControllers.shared.showLoading();
  //   prefered_rate_enable = arrayYourPreferredInterestRateWidget.isNotEmpty;
  //   Map<String, dynamic> bodyJson = {
  //     // "export_fields" = ["Loan Details","Rate Comparison","Disbursement Schedule","Progressive Repayment","Preferred Repayment"]
  //     "export_fields": getExportField(),
  //     "token": hive.get(HiveString.token),
  //     "name": nameController.text, //1
  //     "cpf_available_amount": cpfAvailableController.text.isNotEmpty
  //         ? double.parse(getAmountString(cpfAvailableController.text))
  //         : 0,
  //     "cpf_available": (cpfAvailableController.text.isEmpty) ? "no" : "yes", //3
  //     "downpayment": getAmountString(resultDownPayment.value), //4
  //     "loan_amount": getAmountString(resultLoanAmount.value), //5
  //     "loan_tenure": double.parse(loanTenureController.text), //6
  //     "prefered_rate_enable":
  //         arrayYourPreferredInterestRateWidget.isNotEmpty ? "yes" : "no", //7
  //     "prefered_rate_year1": arrayYourPreferredInterestRateWidget.isNotEmpty //8
  //         ? arrayYourPreferredInterestRateWidget[0].year1TextController.text
  //         : null,
  //     "prefered_rate_year2": arrayYourPreferredInterestRateWidget.isNotEmpty //9
  //         ? arrayYourPreferredInterestRateWidget[0].year1TextController.text
  //         : null,
  //     "prefered_rate_year3":
  //         arrayYourPreferredInterestRateWidget.isNotEmpty //10
  //             ? arrayYourPreferredInterestRateWidget[0].year1TextController.text
  //             : null,
  //     "prefered_rate_year4":
  //         arrayYourPreferredInterestRateWidget.isNotEmpty //11
  //             ? arrayYourPreferredInterestRateWidget[0].year1TextController.text
  //             : null,
  //     "prefered_rate_year5":
  //         arrayYourPreferredInterestRateWidget.isNotEmpty //12
  //             ? arrayYourPreferredInterestRateWidget[0].year1TextController.text
  //             : null,
  //     "property_price": getAmountString(propertyPriceController.text), //13
  //     "ltv": loanToValueController.text, //14
  //   };
  //   Logger().d(bodyJson);
  //   var token = GetControllers.shared.getToken();
  //   var data = await NetworkProvider().callAPIPost(
  //       token: token,
  //       url: baseUrl + 'user/api/calculator/buc-mortgage-report-pdf',
  //       data: bodyJson); //Send the otp to the server.
  //   GetControllers.shared.hideLoading();
  //   if (NetworkProvider().isSuccessAPI(data)) {
  //     debugPrint("✅ OK mortgageCalculatorUrlf");
  //     Logger().d(data);
  //     callback(data);
  //   } else {
  //     debugPrint("❌  mortgageCalculatorUrl");
  //     GetControllers.shared.showAlertPopupOneButtonWithCallBack(
  //         content: NetworkProvider().getErrorMessage(data), callback: () {});
  //   }
  // }

  callAPIDownloadBUCReport() async {
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);

    debugPrint("start callAPIDownloadBUCReport");
    // GetControllers.shared.showLoading();
    // prefered_rate_enable = arrayYourPreferredInterestRateWidget.isNotEmpty;
    Map<String, dynamic> bodyJson = {
      // "export_fields" = ["Loan Details","Rate Comparison","Disbursement Schedule","Progressive Repayment","Preferred Repayment"]
      "export_fields": getExportField(),
      "token": hive.get(HiveString.token),
      "name": nameController.text, //1
      "cpf_available_amount": cpfAvailableController.text.isNotEmpty
          ? double.parse(getAmountString(cpfAvailableController.text))
          : 0,
      "cpf_available": (cpfAvailableController.text.isEmpty) ? "no" : "yes", //3
      "downpayment": getAmountString(resultDownPayment.value), //4
      "loan_amount": getAmountString(resultLoanAmount.value), //5
      "loan_tenure": double.parse(loanTenureController.text), //6
      "prefered_rate_enable":
          arrayYourPreferredInterestRateWidget.isNotEmpty ? "yes" : "no", //7
      "prefered_rate_year1": arrayYourPreferredInterestRateWidget.isNotEmpty //8
          ? arrayYourPreferredInterestRateWidget[0].year1TextController.text
          : null,
      "prefered_rate_year2": arrayYourPreferredInterestRateWidget.isNotEmpty //9
          ? arrayYourPreferredInterestRateWidget[0].year1TextController.text
          : null,
      "prefered_rate_year3":
          arrayYourPreferredInterestRateWidget.isNotEmpty //10
              ? arrayYourPreferredInterestRateWidget[0].year1TextController.text
              : null,
      "prefered_rate_year4":
          arrayYourPreferredInterestRateWidget.isNotEmpty //11
              ? arrayYourPreferredInterestRateWidget[0].year1TextController.text
              : null,
      "prefered_rate_year5":
          arrayYourPreferredInterestRateWidget.isNotEmpty //12
              ? arrayYourPreferredInterestRateWidget[0].year1TextController.text
              : null,
      "property_price": getAmountString(propertyPriceController.text), //13
      "ltv": loanToValueController.text, //14
      "disbursement_month": {
        "Option To Purchase": int.tryParse(textEditingController0.text),
        "Downpayment": int.tryParse(textEditingController1.text),
        "Foundation": int.tryParse(textEditingController2.text),
        "Concrete Framework": int.tryParse(textEditingController3.text),
        "Walls": int.tryParse(textEditingController4.text),
        "Ceiling": int.tryParse(textEditingController5.text),
        "Doors and Windows": int.tryParse(textEditingController6.text),
        "Car Park": int.tryParse(textEditingController7.text),
        "Temporary Occupation Permit":
            int.tryParse(textEditingController8.text),
        "Certificate of Statutory Completion":
            int.tryParse(textEditingController9.text),
      }
    };

    log(bodyJson.toString());

    HeaderModel headerModel =
        HeaderModel(authorization: "Bearer ${hive.get(HiveString.token)}");
    http.Response data = await http
        .post(
            Uri.parse(baseUrl + "user/api/calculator/buc-mortgage-report-pdf"),
            body: jsonEncode(bodyJson),
            headers: headerModel.toHeader())
        .timeout(const Duration(seconds: 50));

    log("response code::: " + data.statusCode.toString());
    log("response::: " + data.body.toString());

    if (Get.isDialogOpen ?? false) Get.back();
    if (data.body.isEmpty) {
      if (Get.isDialogOpen ?? false) Get.back();
    } else {
      if (data.body.contains('message')) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: data.body.contains('message')
                ? jsonDecode(data.body)['message']
                : "Something went wrong",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      } else {
        bucMortgagePdfResponseModel.value =
            BucMortgagePdfResponseModel.fromJson(jsonDecode(data.body));

        onDownloadPdfButtonTap(
          bucMortgagePdfResponseModel.value.url.toString(),
          ("${bucMortgagePdfResponseModel.value.url}.pdf"),
        );
      }
      if (Get.isDialogOpen ?? false) Get.back();
    }
    if (Get.isDialogOpen ?? false) Get.back();
  }

  void getPermission() async {
    await Permission.storage.request();
  }

  onDownloadPdfButtonTap(String url, String filename) {
    openFile(
      //url: invoices.value.invoicePdf,
      // url: invoices.value.invoicePdf.toString(),
      // fileName: "Invoice ${invoices.value.number}.pdf",
      url: url,
      fileName: filename,
    );
  }

  Future openFile({String? url, String? fileName}) async {
    debugPrint('url' + url!);

    final file = await downloadFile(url, fileName!);
    if (file == null) return;

    debugPrint('Path: ${file.path}');

    OpenFilex.open(file.path);
  }

// ignore: body_might_complete_normally_nullable
  Future<File?> downloadFile(String url, String name) async {
    Future.delayed(const Duration(milliseconds: 0), () {
      FocusManager.instance.primaryFocus!.unfocus();
      Fluttertoast.showToast(
        timeInSecForIosWeb: 3,
        msg: "Download Started",
        backgroundColor: Colors.green,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        textColor: Colors.white,
        fontSize: 20.0,
      );
    });
    final taskId = await FlutterDownloader.enqueue(
        url: url,
        savedDir: _localPath,
        showNotification: true,
        // show download progress in status bar (for Android)
        openFileFromNotification: true,
        // click on notification to open downloaded file (for Android)
        saveInPublicStorage: true);

    Get.snackbar("Downloaded", "Download completed",
        backgroundColor: Colors.green,
        colorText: Colors.white,
        mainButton: TextButton(
            onPressed: () async {
              debugPrint(">>>>>>>>");
              await FlutterDownloader.open(taskId: taskId!);
            },
            child: const Text(
              "Open",
              style: TextStyle(color: Colors.white),
            )),
        duration: const Duration(seconds: 7));
  }

  late String _localPath;

  Future<void> _prepareSaveDir() async {
    _localPath = (await _findLocalPath())!;
    final savedDir = Directory(_localPath);
    bool hasExisted = await savedDir.exists();
    if (!hasExisted) {
      savedDir.create();
    }
  }

  Future<String?> _findLocalPath() async {
    String? externalStorageDirPath;
    if (Platform.isAndroid) {
      try {
        externalStorageDirPath = await AndroidPathProvider.downloadsPath;
      } catch (e) {
        final directory = await getExternalStorageDirectory();
        externalStorageDirPath = directory?.path;
      }
    } else if (Platform.isIOS) {
      externalStorageDirPath =
          (await getApplicationDocumentsDirectory()).absolute.path;
    }
    return externalStorageDirPath;
  }

  static void downloadCallback(
      String id, DownloadTaskStatus status, int progress) {
    if (kDebugMode) {
      print(
          'Background Isolate Callback: task ($id) is in status ($status) and process ($progress)');
    }

    final SendPort send =
        IsolateNameServer.lookupPortByName('downloader_send_port')!;
    send.send([id, status, progress]);
  }
}
