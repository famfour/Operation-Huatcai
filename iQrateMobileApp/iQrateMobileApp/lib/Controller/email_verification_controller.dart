import 'dart:async';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/sign_up_controller.dart';
import 'package:iqrate/Model/response_model.dart/email_verification_response_model.dart';
import 'package:iqrate/Model/response_model.dart/resend_email_otp_response_model.dart';
import 'package:iqrate/Model/send_model.dart/email_verification_send_model.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Service/core_services.dart';

import 'package:iqrate/Service/url.dart';

class EmailVerificationController extends GetxController {
  final SignUpController signUpController = Get
      .find(); //We are using the sign up controller to get the email address of the user
  late TextEditingController
      emailVerificationOtpController; //Controller for email verification otp

  var totalCountdownSeconds = 60;
  var countDownSecond = 0.obs;
  late Timer timer;

  var countTimes = 0;
  var isResendButtonDisable = false.obs;

  @override
  void onInit() {
    emailVerificationOtpController =
        TextEditingController(); //Initialize controller before the screen loads

    resendEmailCodeTimer();

    super.onInit();
  }

  onSubmitButtonTap() {
    if (emailVerificationOtpController.text.isEmpty) {
      //Check if the otp is empty

      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter the OTP",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (emailVerificationOtpController.text.length < 6) {
      //Check if the otp is less than 6 characters

      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please check the OTP",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else {
      //If the otp is not empty and is 6 characters long call the onSubmit method which sends the OTP and checks if the OTP is correct and moves to the next screen
      debugPrint("Submit button tapped");
      onSubmit();
    }
  }

  onSubmit() async {
    Get.isDialogOpen ?? true
        ? const Offstage() //Used for avoiding the dialog to be open when the user is already on the next screen.
        : Get.dialog(
            const Center(
                child:
                    CircularProgressIndicator()), //Used for showing the loading dialog.
            barrierDismissible:
                false); //Used for preventing the user to dismiss the dialog.
    EmailVerificationSendModel emailVerificationSendModel =
        EmailVerificationSendModel(
      verification_code: emailVerificationOtpController.text,
    ); //Create a model for sending the otp to the server.
    //
    //The entered url is base+emailverifyurl+the email address of the user.
    var data = await CoreService().putWithoutAuth(
        url: baseUrl +
            emailVerificationUrl +
            signUpController.emailController.text,
        body:
            emailVerificationSendModel.toJson()); //Send the otp to the server.
    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
    } else {
      //if the status code is 400, the server sends an error, which is displayed via the snackbar.
      if (data["resultcode"] == 400) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: data["errMessage"],
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      } else {
        //If the status code is 200, the server sends a success message, which is displayed via the snackbar. and the data is mapped in the response model. The control then is moved to the next screen which is the mobile verification screen.
        var result = EmailVerificationResponseModel.fromJson(data);
        if (result.message == "Email has been verified") {
          closeCountdownTimer();

          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "OTP Verified Successfully",
              backgroundColor: Colors.green,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
          Get.toNamed(
              mobileVerification); //Used for navigating to the next screen.
        } else {
          // Get.snackbar(StringUtils.error, data["errMessage"].toString(),
          //     snackPosition: SnackPosition.TOP, backgroundColor: Colors.white);
        }
      }
    }
  }

  resendEmailCodeTimer() {
    isResendButtonDisable.value = true; //resend button disable

    debugPrint('countDownSecond::${countDownSecond.value}');
    debugPrint('countTimes::$countTimes');

    if (countTimes == 3) {
      return;
    }

    countDownSecond.value = totalCountdownSeconds; //initial total seconds

    // Start the periodic timer which prints remaining seconds after every 1 seconds
    timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      --countDownSecond.value; //countDownSecond decrement

      debugPrint('second::${countDownSecond.value}');

      if (countDownSecond.value == 0) {
        isResendButtonDisable.value = false; //resend button enable

        countTimes++;
        closeCountdownTimer();

        switch (countTimes) {
          case 1:
          case 2:
            totalCountdownSeconds = 60;
            break;
          default:
            totalCountdownSeconds = 0;
        }
      }
    });
  }

  closeCountdownTimer() {
    //close resend timer when verify successful
    timer.cancel();
  }

  apiCallResendEmailCode() async {
    Get.isDialogOpen ?? true
        ? const Offstage() //Used for avoiding the dialog to be open when the user is already on the next screen.
        : Get.dialog(
            const Center(
                child:
                    CircularProgressIndicator()), //Used for showing the loading dialog.
            barrierDismissible:
                false); //Used for preventing the user to dismiss the dialog.

    EmailVerificationSendModel emailVerificationSendModel =
        EmailVerificationSendModel(
      verification_code: emailVerificationOtpController.text,
    ); //Create a model for resending the otp to the server.

    //The entered url is base+resendEmailCodeUrl+the email address of the user.
    var data = await CoreService().putWithoutAuth(
        url: baseUrl +
            resendEmailCodeUrl +
            signUpController.emailController.text,
        body:
            emailVerificationSendModel.toJson()); //Send the otp to the server.
    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
    } else {
      //if the status code is 400, the server sends an error, which is displayed via the snackbar.
      if (data["resultcode"] == 400) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: data["errMessage"],
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
        resendEmailCodeTimer();
      } else {
        //If the status code is 200, the server sends a success message, which is displayed via the snackbar. and the data is mapped in the response model.
        var result = ResendEmailOtpResponseModel.fromJson(data);
        resendEmailCodeTimer();
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: result.message!,
            backgroundColor: Colors.green,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      }
    }
  }
}
