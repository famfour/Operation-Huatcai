// ignore_for_file: prefer_typing_uninitialized_variables

import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:isolate';
import 'dart:ui';

import 'package:android_path_provider/android_path_provider.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:iqrate/Controller/leads_view_controller.dart';
import 'package:iqrate/Model/header_model.dart';
import 'package:iqrate/Model/response_model.dart/fetch_lead_list_response_model.dart';
import 'package:iqrate/Model/response_model.dart/new_purchase_report_response_model.dart';
import 'package:iqrate/Model/response_model.dart/new_purchase_total_response_model.dart';
import 'package:iqrate/Model/send_model.dart/new_purchase_get_pledge_amount_send_model.dart';
import 'package:iqrate/Model/send_model.dart/new_purchase_report_send_model.dart';
import 'package:iqrate/Model/send_model.dart/new_purchase_send_model.dart';
import 'package:iqrate/Model/send_model.dart/new_purchase_total_send_model.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';
import 'package:iqrate/Utils/config.dart';
import 'package:open_filex/open_filex.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

import '../DeviceManager/hive_string.dart';
import '../Model/response_model.dart/new_purchase_report_pdf_response_model.dart';
import '../Model/response_model.dart/new_purchase_response_model.dart';
import '../Model/send_model.dart/new_purchase_get_unpledge_amount_send_model.dart';
import '../Model/send_model.dart/new_purchase_report_pdf_send_model.dart';
import 'package:http/http.dart' as http;

//!API 1 --> new-purchase-total
//!API 2 --> new-purchase
//!API 3 --> new-purchase-report

class NewPurchaseExistingLeadController extends GetxController {
  late List<Client>
      leadDetails; //! Stores the lead data value, used for accessing the name of applicants and the length of the list. {Search for leadlist.length to access the value}

  var totalMonthlyIncomeMainApplicant =
      0.0.obs; //! Stores the total monthly income of the main applicant.
  var totalFinancialCommitmentsMainApplicant =
      0.0.obs; //! Stores the total financial commitments of the main applicant.

  var totalMonthlyIncomeJointApplicant1 =
      0.0.obs; //! Stores the total monthly income of the joint applicant 1.
  var totalFinancialCommitmentsJointApplicant1 = 0.0
      .obs; //! Stores the total financial commitments of the joint applicant 1.

  var totalMonthlyIncomeJointApplicant2 =
      0.0.obs; //! Stores the total monthly income of the joint applicant 2.
  var totalFinancialCommitmentsJointApplicant2 = 0.0
      .obs; //! Stores the total financial commitments of the joint applicant 2.

  var totalMonthlyIncomeJointApplicant3 =
      0.0.obs; //! Stores the total monthly income of the joint applicant 3.
  var totalFinancialCommitmentsJointApplicant3 = 0.0
      .obs; //! Stores the total financial commitments of the joint applicant 3.

  //*This is the data for main applicant. This is reponse from 2nd API
  var loanToValueMainApplicant = ""
      .obs; //! Stores the loan to value of the main applicant. This is a response model value from the 2nd API.
  var maximumLoanTenureMainApplicant = ""
      .obs; //! Stores the maximum loan tenure of the main applicant. This is a response model value from the 2nd API.
  var buyerStampDutyMainApplicant = ""
      .obs; //! Stores the buyer stamp duty of the main applicant. This is a response model value from the 2nd API.
  var additionalBuyerStampDutyMainApplicant = ""
      .obs; //! Stores the additional buyer stamp duty of the main applicant. This is a response model value from the 2nd API.
  var legalFeesMainApplicant = ""
      .obs; //! Stores the legal fees of the main applicant. This is a response model value from the 2nd API.
  var valuationFeesMainApplicant = ""
      .obs; //! Stores the valuation fees of the main applicant. This is a response model value from the 2nd API.
  var maximumQualifiedLoanMainApplicant = ""
      .obs; //! Stores the maximum qualified loan of the main applicant. This is a response model value from the 2nd API.
  var maximumPropertyPriceMainApplicant = ""
      .obs; //! Stores the maximum property price of the main applicant. This is a response model value from the 2nd API.
  var msrMainApplicant = 0
      .obs; //! Stores the msr of the main applicant. This is a response model value from the 2nd API.
  var tdsrMainApplicant = 0
      .obs; //! Stores the tdsr of the main applicant. This is a response model value from the 2nd API.
  var fullFludgeAmountMainApplicant = ""
      .obs; //! Stores the full fludge amount of the main applicant. This is a response model value from the 2nd API.
  var fullUnfludgeAmountMainApplicant = ""
      .obs; //! Stores the full unfludge amount of the main applicant. This is a response model value from the 2nd API.
  var loanTenureMainApplicant = 0
      .obs; //! Stores the loan tenure of the main applicant. This is a response model value from the 2nd API.

  int fludgeAmountMainApplicant = 0;

  int unfludgeAmountMainApplicant = 0;

  int fludgeAmountMainApplicantPlt = 0;

  int unfludgeAmountMainApplicantPlt = 0;

  Rx<NewPurchaseResponseModel> resultMainApplicantStep2 =
      NewPurchaseResponseModel().obs;

  Rx<NewPurchaseReportResponseModel> resultMainApplicantStep3 =
      NewPurchaseReportResponseModel().obs;

  Rx<NewPurchaseReportPdfResponseModel> resultMainApplicantStep4 =
      NewPurchaseReportPdfResponseModel().obs;

  Rx<NewPurchaseTotalResponseModel> resultMainApplicant =
      NewPurchaseTotalResponseModel().obs;

  var maximumTenureBasedAffordabilty = [];

  double ltvValueToShowAndHideTheLongerLoanTenureCondition = 0.0;
  RxBool showLongerLoanTenureCondition = true.obs;

  RxString titleForMortgageTableScreen = "".obs;
  int propertyTypeForReportsAndPDF = 0;

  @override
  onInit() {
    leadDetails = Get.arguments;

    debugPrint("============lead size======" + leadDetails.length.toString());

    annualIncomeControllerMainApplicant = TextEditingController();
    annualIncomeControllerJointApplicant1 = TextEditingController();
    annualIncomeControllerJointApplicant2 = TextEditingController();
    annualIncomeControllerJointApplicant3 = TextEditingController();

    //If there are no joint applicants add only the main applicant
    if (leadDetails.length == 1) {
      emailControllerMainApplicant =
          TextEditingController(text: leadDetails[0].email ?? '');
      nameControllerMainApplicant =
          TextEditingController(text: leadDetails[0].name ?? '');
      countryCodeControllerMainApplicant = TextEditingController(
          text: leadDetails[0].countryCode.toString().isEmpty
              ? ''
              : "+" + leadDetails[0].countryCode.toString());

      //debugPrint("===========countryCodeControllerMainApplicant===="+leadDetails[0].countryCode.toString());

      phoneControllerMainApplicant = TextEditingController(
          text: leadDetails[0].phoneNumber.toString().isEmpty
              ? ''
              : leadDetails[0].phoneNumber.toString());
      dobControllerMainApplicant = TextEditingController(
          text: leadDetails[0].dob == null
              ? ''
              : AppConfig.getDateFormat(leadDetails[0].dob));
      nationalityControllerMainApplicant =
          TextEditingController(text: leadDetails[0].nationality ?? '');

      annualIncomeControllerMainApplicant = TextEditingController(
          text: leadDetails[0].annualIncome == null
              ? "0"
              : leadDetails[0].annualIncome.toString().replaceAll(".0", ""));
    }
    //if there is 1 joint applicant, add details for the main applicant and the 1st joint applicant
    else if (leadDetails.length == 2) {
      emailControllerMainApplicant =
          TextEditingController(text: leadDetails[0].email ?? '');
      nameControllerMainApplicant =
          TextEditingController(text: leadDetails[0].name ?? '');
      countryCodeControllerMainApplicant = TextEditingController(
          text: leadDetails[0].countryCode.toString().isEmpty
              ? ''
              : "+" + leadDetails[0].countryCode.toString());
      phoneControllerMainApplicant = TextEditingController(
          text: leadDetails[0].phoneNumber.toString().isEmpty
              ? ''
              : leadDetails[0].phoneNumber.toString());
      dobControllerMainApplicant =
          TextEditingController(text: leadDetails[0].dob ?? '');
      nationalityControllerMainApplicant =
          TextEditingController(text: leadDetails[0].nationality ?? '');
      emailControllerJointApplicant1 =
          TextEditingController(text: leadDetails[1].email ?? '');
      nameControllerJointApplicant1 =
          TextEditingController(text: leadDetails[1].name ?? '');

      countryCodeControllerJointApplicant1 = TextEditingController(
          text: leadDetails[1].countryCode.toString().isEmpty
              ? ''
              : leadDetails[1].countryCode.toString());
      phoneControllerJointApplicant1 = TextEditingController(
          text: leadDetails[1].phoneNumber.toString().isEmpty
              ? ''
              : leadDetails[1].phoneNumber.toString());
      dobControllerJointApplicant1 =
          TextEditingController(text: leadDetails[1].dob ?? '');
      nationalityControllerJointApplicant1 =
          TextEditingController(text: leadDetails[1].nationality ?? '');

      annualIncomeControllerJointApplicant1 = TextEditingController(
          text: leadDetails[1].annualIncome == null
              ? "0"
              : leadDetails[1].annualIncome.toString().replaceAll(".0", ""));
    }
    //if there are 2 joint applicants, add details for the main applicant and the 2 joint applicants
    else if (leadDetails.length == 3) {
      emailControllerMainApplicant =
          TextEditingController(text: leadDetails[0].email ?? '');
      nameControllerMainApplicant =
          TextEditingController(text: leadDetails[0].name ?? '');
      countryCodeControllerMainApplicant = TextEditingController(
          text: leadDetails[0].countryCode.toString().isEmpty
              ? ''
              : "+" + leadDetails[0].countryCode.toString());
      phoneControllerMainApplicant = TextEditingController(
          text: leadDetails[0].phoneNumber.toString().isEmpty
              ? ''
              : leadDetails[0].phoneNumber.toString());
      dobControllerMainApplicant =
          TextEditingController(text: leadDetails[0].dob ?? '');
      nationalityControllerMainApplicant =
          TextEditingController(text: leadDetails[0].nationality ?? '');
      emailControllerJointApplicant1 =
          TextEditingController(text: leadDetails[1].email ?? '');
      nameControllerJointApplicant1 =
          TextEditingController(text: leadDetails[1].name ?? '');

      countryCodeControllerJointApplicant1 = TextEditingController(
          text: leadDetails[1].countryCode.toString().isEmpty
              ? ''
              : leadDetails[1].countryCode.toString());
      phoneControllerJointApplicant1 = TextEditingController(
          text: leadDetails[1].phoneNumber.toString().isEmpty
              ? ''
              : leadDetails[1].phoneNumber.toString());
      dobControllerJointApplicant1 =
          TextEditingController(text: leadDetails[1].dob ?? '');
      nationalityControllerJointApplicant1 =
          TextEditingController(text: leadDetails[1].nationality ?? '');
      emailControllerJointApplicant2 =
          TextEditingController(text: leadDetails[2].email ?? '');
      nameControllerJointApplicant2 =
          TextEditingController(text: leadDetails[2].name ?? '');
      countryCodeControllerJointApplicant2 = TextEditingController(
          text: leadDetails[2].countryCode.toString().isEmpty
              ? ''
              : leadDetails[2].countryCode.toString());
      phoneControllerJointApplicant2 = TextEditingController(
          text: leadDetails[2].phoneNumber.toString().isEmpty
              ? ''
              : leadDetails[2].phoneNumber.toString());
      dobControllerJointApplicant2 =
          TextEditingController(text: leadDetails[2].dob ?? '');
      nationalityControllerJointApplicant2 =
          TextEditingController(text: leadDetails[2].nationality ?? '');

      annualIncomeControllerJointApplicant2 = TextEditingController(
          text: leadDetails[2].annualIncome == null
              ? "0"
              : leadDetails[2].annualIncome.toString().replaceAll(".0", ""));
    }
    //if there are 3 joint applicants, add details for the main applicant and the 3 joint applicants
    else if (leadDetails.length == 4) {
      emailControllerMainApplicant =
          TextEditingController(text: leadDetails[0].email ?? '');
      nameControllerMainApplicant =
          TextEditingController(text: leadDetails[0].name ?? '');
      countryCodeControllerMainApplicant = TextEditingController(
          text: leadDetails[0].countryCode.toString().isEmpty
              ? ''
              : leadDetails[0].countryCode.toString());
      phoneControllerMainApplicant = TextEditingController(
          text: leadDetails[0].phoneNumber.toString().isEmpty
              ? ''
              : leadDetails[0].phoneNumber.toString());
      dobControllerMainApplicant =
          TextEditingController(text: leadDetails[0].dob ?? '');
      nationalityControllerMainApplicant =
          TextEditingController(text: leadDetails[0].nationality ?? '');
      emailControllerJointApplicant1 =
          TextEditingController(text: leadDetails[1].email ?? '');
      nameControllerJointApplicant1 =
          TextEditingController(text: leadDetails[1].name ?? '');

      countryCodeControllerJointApplicant1 = TextEditingController(
          text: leadDetails[1].countryCode.toString().isEmpty
              ? ''
              : "+" + leadDetails[1].countryCode.toString());
      phoneControllerJointApplicant1 = TextEditingController(
          text: leadDetails[1].phoneNumber.toString().isEmpty
              ? ''
              : leadDetails[1].phoneNumber.toString());
      dobControllerJointApplicant1 =
          TextEditingController(text: leadDetails[1].dob ?? '');
      nationalityControllerJointApplicant1 =
          TextEditingController(text: leadDetails[1].nationality ?? '');
      emailControllerJointApplicant2 =
          TextEditingController(text: leadDetails[2].email ?? '');
      nameControllerJointApplicant2 =
          TextEditingController(text: leadDetails[2].name ?? '');
      countryCodeControllerJointApplicant2 = TextEditingController(
          text: leadDetails[2].countryCode.toString().isEmpty
              ? ''
              : leadDetails[2].countryCode.toString());
      phoneControllerJointApplicant2 = TextEditingController(
          text: leadDetails[2].phoneNumber.toString().isEmpty
              ? ''
              : leadDetails[2].phoneNumber.toString());
      dobControllerJointApplicant2 =
          TextEditingController(text: leadDetails[2].dob ?? '');
      nationalityControllerJointApplicant2 =
          TextEditingController(text: leadDetails[2].nationality ?? '');
      emailControllerJointApplicant3 =
          TextEditingController(text: leadDetails[3].email ?? '');

      nameControllerJointApplicant3 =
          TextEditingController(text: leadDetails[3].name ?? '');

      countryCodeControllerJointApplicant3 = TextEditingController(
          text: leadDetails[3].countryCode.toString().isEmpty
              ? ''
              : leadDetails[3].countryCode.toString());

      phoneControllerJointApplicant3 = TextEditingController(
          text: leadDetails[3].phoneNumber.toString().isEmpty
              ? ''
              : leadDetails[3].phoneNumber.toString());

      dobControllerJointApplicant3 =
          TextEditingController(text: leadDetails[3].dob ?? '');

      nationalityControllerJointApplicant3 =
          TextEditingController(text: leadDetails[3].nationality ?? '');

      annualIncomeControllerJointApplicant3 = TextEditingController(
          text: leadDetails[3].annualIncome == null
              ? "0"
              : leadDetails[3].annualIncome.toString().replaceAll(".0", ""));
    }

    //initializing annual income controllers for all applicants
    /*annualIncomeControllerMainApplicant = TextEditingController();
    annualIncomeControllerJointApplicant1 = TextEditingController();
    annualIncomeControllerJointApplicant2 = TextEditingController();
    annualIncomeControllerJointApplicant3 = TextEditingController();*/

    //initializing monthly rental income controllers for all applicants
    monthlyRentalIncomeControllerMainApplicant =
        TextEditingController(text: '0');
    monthlyRentalIncomeControllerJointApplicant1 =
        TextEditingController(text: '0');
    monthlyRentalIncomeControllerJointApplicant2 =
        TextEditingController(text: '0');
    monthlyRentalIncomeControllerJointApplicant3 =
        TextEditingController(text: '0');

    //initializing commercial property loan company controllers for all applicants
    commercialPropertyLoanCompanyControllerMainApplicant =
        TextEditingController(text: '0');
    commercialPropertyLoanCompanyControllerJointApplicant1 =
        TextEditingController(text: '0');
    commercialPropertyLoanCompanyControllerJointApplicant2 =
        TextEditingController(text: '0');
    commercialPropertyLoanCompanyControllerJointApplicant3 =
        TextEditingController(text: '0');

    //initializing commercial property personal loan amount controllers for all applicants
    commercialPropertyLoanPersonalControllerMainApplicant =
        TextEditingController(text: '0');
    commercialPropertyLoanPersonalControllerJointApplicant1 =
        TextEditingController(text: '0');
    commercialPropertyLoanPersonalControllerJointApplicant2 =
        TextEditingController(text: '0');
    commercialPropertyLoanPersonalControllerJointApplicant3 =
        TextEditingController(text: '0');

    //initializing guarantor loan amount controllers for all applicants
    guarantorLoanAmountControllerMainApplicant =
        TextEditingController(text: '0');
    guarantorLoanAmountControllerJointApplicant1 =
        TextEditingController(text: '0');
    guarantorLoanAmountControllerJointApplicant2 =
        TextEditingController(text: '0');
    guarantorLoanAmountControllerJointApplicant3 =
        TextEditingController(text: '0');

    //initializing card repayment amount controllers for all applicants
    cardRepaymentAmountControllerMainApplicant =
        TextEditingController(text: '0');
    cardRepaymentAmountControllerJointApplicant1 =
        TextEditingController(text: '0');
    cardRepaymentAmountControllerJointApplicant2 =
        TextEditingController(text: '0');
    cardRepaymentAmountControllerJointApplicant3 =
        TextEditingController(text: '0');

    //initializing car loan installment amount controllers for all applicants
    carLoansInstallmentControllerMainApplicant =
        TextEditingController(text: '0');
    carLoansInstallmentControllerJointApplicant1 =
        TextEditingController(text: '0');
    carLoansInstallmentControllerJointApplicant2 =
        TextEditingController(text: '0');
    carLoansInstallmentControllerJointApplicant3 =
        TextEditingController(text: '0');

    //initializing housing loan installment amount controllers for all applicants
    housingLoansInstallmentControllerMainApplicant =
        TextEditingController(text: '0');
    housingLoansInstallmentControllerJointApplicant1 =
        TextEditingController(text: '0');
    housingLoansInstallmentControllerJointApplicant2 =
        TextEditingController(text: '0');
    housingLoansInstallmentControllerJointApplicant3 =
        TextEditingController(text: '0');

    //initializing personal, education, renovation loan amount controllers for all applicants
    personalEducationRenovationControllerMainApplicant =
        TextEditingController(text: '0');
    personalEducationRenovationControllerJointApplicant1 =
        TextEditingController(text: '0');
    personalEducationRenovationControllerJointApplicant2 =
        TextEditingController(text: '0');
    personalEducationRenovationControllerJointApplicant3 =
        TextEditingController(text: '0');

    loanAmountControllerMainApplicant = TextEditingController(text: '0');

    propertyPriceControllerMainApplicant = TextEditingController(text: '0');

    loanTenureYearControllerMainApplicant = TextEditingController(text: '0');

    otherFeesControllerMainApplicant = TextEditingController(text: '0');

    preferredUnpledgeAmountMltControllerMainApplicant =
        TextEditingController(text: '0');

    preferredPledgeAmountPltControllerMainApplicant =
        TextEditingController(text: '0');

    preferredUnpledgeAmountPltControllerMainApplicant =
        TextEditingController(text: '0');

    preferredPledgeAmountMltControllerMainApplicant =
        TextEditingController(text: '0');

    getPermission();

    _prepareSaveDir();

    FlutterDownloader.registerCallback(downloadCallback);

    addBasicDataToList();

    setLoanTypeDropdown();

    super.onInit();
  }

  LeadsViewController leadsViewController = Get.find();

  List<String> loanTypes = [];

  var selectedLoanTypeMainApplicant =
      "new_purchase_with_option_to_purchase"; //!default value for loan type dropdown for main applicant

  //* adding value for loan type drop down
  setLoanTypeDropdown() {
    for (var element in leadsViewController.kvStoreValues) {
      if (element.code == "loan_category") {
        element.value.forEach((key, value) {
          loanTypes.add(key);
          // debugPrint("array_key:: " + value);
        });
      }
    }
  }

  TextEditingController leadNameController =
      TextEditingController(); //name text controller

  //existing lead calculator
  late TextEditingController
      emailControllerMainApplicant; //email text controller
  late TextEditingController
      emailControllerJointApplicant1; //email text controller for joint applicant 1
  late TextEditingController
      emailControllerJointApplicant2; //email text controller for joint applicant 2
  late TextEditingController
      emailControllerJointApplicant3; //email text controller for joint applicant 3

  late TextEditingController nameControllerMainApplicant; //name text controller
  late TextEditingController
      nameControllerJointApplicant1; //name text controller for joint applicant 1
  late TextEditingController
      nameControllerJointApplicant2; //name text controller for joint applicant 2
  late TextEditingController
      nameControllerJointApplicant3; //name text controller for joint applicant 3

  late TextEditingController
      phoneControllerMainApplicant; //phone text controller
  late TextEditingController
      phoneControllerJointApplicant1; //phone text controller for joint applicant 1
  late TextEditingController
      phoneControllerJointApplicant2; //phone text controller for joint applicant 2
  late TextEditingController
      phoneControllerJointApplicant3; //phone text controller for joint applicant 3

  late TextEditingController
      countryCodeControllerMainApplicant; //phone code text controller
  late TextEditingController
      countryCodeControllerJointApplicant1; //phone code text controller for joint applicant 1
  late TextEditingController
      countryCodeControllerJointApplicant2; //phone code text controller for joint applicant 2
  late TextEditingController
      countryCodeControllerJointApplicant3; //phone code text controller for joint applicant 3

  TextEditingController monthlyFixedIncomeControllerMainApplicant =
      TextEditingController(text: '0'); //annual Income text controller
  TextEditingController monthlyFixedIncomeControllerJointApplicant1 =
      TextEditingController(
          text: '0'); //annual Income text controller for joint applicant 1
  TextEditingController monthlyFixedIncomeControllerJointApplicant2 =
      TextEditingController(
          text: '0'); //annual Income text controller for joint applicant 2
  TextEditingController monthlyFixedIncomeControllerJointApplicant3 =
      TextEditingController(
          text: '0'); //annual Income text controller for joint applicant 3

  late TextEditingController dobControllerMainApplicant; //dob
  late TextEditingController
      dobControllerJointApplicant1; //dob for joint applicant 1
  late TextEditingController
      dobControllerJointApplicant2; //dob for joint applicant 2
  late TextEditingController
      dobControllerJointApplicant3; //dob for joint applicant 3

  late TextEditingController
      nationalityControllerMainApplicant; //nationality for main applicant
  late TextEditingController
      nationalityControllerJointApplicant1; //nationality for joint applicant 1
  late TextEditingController
      nationalityControllerJointApplicant2; //nationality for joint applicant 2
  late TextEditingController
      nationalityControllerJointApplicant3; //nationality for joint applicant 3

  late TextEditingController
      annualIncomeControllerMainApplicant; //annual income controller main applicant
  late TextEditingController
      annualIncomeControllerJointApplicant1; //annual income controller joint applicant 1
  late TextEditingController
      annualIncomeControllerJointApplicant2; //annual income controller joint applicant 2
  late TextEditingController
      annualIncomeControllerJointApplicant3; //annual income controller joint applicant 3

  late TextEditingController
      monthlyRentalIncomeControllerMainApplicant; //monthly rental income controller main applicant
  late TextEditingController
      monthlyRentalIncomeControllerJointApplicant1; //monthly rental income controller joint applicant 1
  late TextEditingController
      monthlyRentalIncomeControllerJointApplicant2; //monthly rental income controller joint applicant 2
  late TextEditingController
      monthlyRentalIncomeControllerJointApplicant3; //monthly rental income controller joint applicant 3

  late TextEditingController
      commercialPropertyLoanCompanyControllerMainApplicant; //commercial property loan company controller main applicant
  late TextEditingController
      commercialPropertyLoanCompanyControllerJointApplicant1; //commercial property loan company controller joint applicant 1
  late TextEditingController
      commercialPropertyLoanCompanyControllerJointApplicant2; //commercial property loan company controller joint applicant 2
  late TextEditingController
      commercialPropertyLoanCompanyControllerJointApplicant3; //commercial property loan company controller joint applicant 3

  late TextEditingController
      commercialPropertyLoanPersonalControllerMainApplicant;
  late TextEditingController
      commercialPropertyLoanPersonalControllerJointApplicant1;
  late TextEditingController
      commercialPropertyLoanPersonalControllerJointApplicant2;
  late TextEditingController
      commercialPropertyLoanPersonalControllerJointApplicant3;

  late TextEditingController guarantorLoanAmountControllerMainApplicant;
  late TextEditingController guarantorLoanAmountControllerJointApplicant1;
  late TextEditingController guarantorLoanAmountControllerJointApplicant2;
  late TextEditingController guarantorLoanAmountControllerJointApplicant3;

  late TextEditingController cardRepaymentAmountControllerMainApplicant;
  late TextEditingController cardRepaymentAmountControllerJointApplicant1;
  late TextEditingController cardRepaymentAmountControllerJointApplicant2;
  late TextEditingController cardRepaymentAmountControllerJointApplicant3;

  late TextEditingController carLoansInstallmentControllerMainApplicant;
  late TextEditingController carLoansInstallmentControllerJointApplicant1;
  late TextEditingController carLoansInstallmentControllerJointApplicant2;
  late TextEditingController carLoansInstallmentControllerJointApplicant3;

  late TextEditingController housingLoansInstallmentControllerMainApplicant;
  late TextEditingController housingLoansInstallmentControllerJointApplicant1;
  late TextEditingController housingLoansInstallmentControllerJointApplicant2;
  late TextEditingController housingLoansInstallmentControllerJointApplicant3;

  late TextEditingController personalEducationRenovationControllerMainApplicant;
  late TextEditingController
      personalEducationRenovationControllerJointApplicant1;
  late TextEditingController
      personalEducationRenovationControllerJointApplicant2;
  late TextEditingController
      personalEducationRenovationControllerJointApplicant3;

  late TextEditingController loanTenureYearControllerMainApplicant;

  late TextEditingController loanAmountControllerMainApplicant;

  late TextEditingController propertyPriceControllerMainApplicant;

  late TextEditingController otherFeesControllerMainApplicant;

  late TextEditingController preferredPledgeAmountMltControllerMainApplicant;

  late TextEditingController preferredPledgeAmountPltControllerMainApplicant;

  late TextEditingController preferredUnpledgeAmountMltControllerMainApplicant;

  late TextEditingController preferredUnpledgeAmountPltControllerMainApplicant;

  var isCalculated = false.obs;
  var newPurchaseCalcResponseModel = NewPurchaseResponseModel().obs;

  List<String> dropdownLoanApplicant = [
    '1',
    '2',
    '3',
    '4',
  ];

  List<String> leadNamesForDropdown = [];
  String? selectedApplicant = '1';

  List<String> dropdownSelectedProperties = ['0', '1', '2 or more'];
  String? selectedPropertiesMainApplicant = '0';
  String? selectedPropertiesJointApplicant1 = '0';
  String? selectedPropertiesJointApplicant2 = '0';
  String? selectedPropertiesJointApplicant3 = '0';

  List<String> dropdownSelectedHousingLoan = ['0', '1', '2 or more'];
  RxString? selectedHousingLoanMainApplicant = '0'.obs;
  RxString? selectedHousingLoanJointApplicant1 = '0'.obs;
  RxString? selectedHousingLoanJointApplicant2 = '0'.obs;
  RxString? selectedHousingLoanJointApplicant3 = '0'.obs;

  List<String> dropdownEmploymentType = [
    'Salaried',
    'Self-Employed',
    'Unemployed (Homemaker / Retiree / Student)'
  ];
  String? selectedEmploymentTypeMainApplicant = "Salaried";
  String? selectedEmploymentTypeJointApplicant1 = "Salaried";
  String? selectedEmploymentTypeJointApplicant2 = "Salaried";
  String? selectedEmploymentTypeJointApplicant3 = "Salaried";

  List<String> dropdownLongerLoanTenure = ['No', 'Yes'];
  RxString selectedLongerLoanTenureMainApplicant = "No".obs;

  List<String> dropdownPropertyType = [
    'HDB',
    'Condominium/ Apartment',
    'EC (Resale/ out of MOP)',
    'EC (From Developer/ within MOP)',
    'Strata Housing/ Townhouses',
    'Landed'
  ];

  String? selectedPropertyTypeMainApplicant = "HDB";

  List<String> dropdownPropertyStatus = [
    'Complete',
    'Under Construction (TOP to be obtained within 2 years)',
    'Under Construction (TOP to be obtained more than 2 years)'
  ];
  String? selectedPropertyStatusMainApplicant = "Complete";

  var step = 1.obs;
  var summaryCheckbox = false.obs;
  var loanEligibiltyMltCheckBox = false.obs;
  var loanEligibiltyPltCheckBox = false.obs;
  var loanEligibiltyLoanPackageMltCheckBox = false.obs;
  var loanEligibiltyLoanPackagePltCheckBox = false.obs;
  var loanEligiblityMortgageRepaymentMltCheckBox = false.obs;
  var loanEligiblityMortgageRepaymentPltCheckBox = false.obs;

  //Data from screen 1
  List nameList = [];
  List phoneList = [];
  List countryCodeList = [];
  List emailAddressList = [];
  List dobList = [];
  List nationalityList = [];
  List employementTypeList = [];
  List monthlyFixedIncomeList = [];
  List annualIncomeList = [];
  List monthlyRentalIncomeList = [];
  List numberOfOwnedPropertiesList = [];
  List numberOfHousingLoanLoansList = [];
  List totalMonthlyInstallmentForCurrentHousingLoanList = [];
  List totalMonthlyInstallmentForCarLoanList = [];
  List totalMonthlyInstallmentForPersonalEducationRenovationList = [];
  List minimumMonthlyCreditCardRepaymentAmountList = [];
  List totalMonthlyGuarantorInstallmentList = [];
  List totalMonthlyCommercialPropertyCompanyLoanList = [];
  List totalMonthlyCommercialPropertyLoanPersonalList = [];

  void onCancel() {
    isCalculated.value = false;
    Get.back();
  }

  addBasicDataToList() {
    for (int i = 0; i < leadDetails.length; i++) {
      nameList.add(leadDetails[i].name);
      phoneList.add(leadDetails[i].phoneNumber);
      countryCodeList.add(leadDetails[i].countryCode);
      emailAddressList.add(leadDetails[i].email);
      dobList.add(leadDetails[i].dob);
      nationalityList.add(leadDetails[i].nationality);
    }
  }

  addDetailsToListForMainApplicantScreen1() {
    totalMonthlyCommercialPropertyLoanPersonalList.add(int.tryParse(
        AppConfig.getPlainAmount(
            commercialPropertyLoanPersonalControllerMainApplicant.text)));
    totalMonthlyGuarantorInstallmentList.add(int.tryParse(
        AppConfig.getPlainAmount(
            guarantorLoanAmountControllerMainApplicant.text)));
    totalMonthlyCommercialPropertyCompanyLoanList.add(int.tryParse(
        AppConfig.getPlainAmount(
            commercialPropertyLoanCompanyControllerMainApplicant.text)));
    totalMonthlyInstallmentForCurrentHousingLoanList.add(int.tryParse(
        AppConfig.getPlainAmount(
            housingLoansInstallmentControllerMainApplicant.text)));
    totalMonthlyInstallmentForPersonalEducationRenovationList.add(int.tryParse(
        AppConfig.getPlainAmount(
            personalEducationRenovationControllerMainApplicant.text)));
    minimumMonthlyCreditCardRepaymentAmountList.add(int.tryParse(
        AppConfig.getPlainAmount(
            cardRepaymentAmountControllerMainApplicant.text)));
    annualIncomeList.add(int.tryParse(
        AppConfig.getPlainAmount(annualIncomeControllerMainApplicant.text)));
    monthlyFixedIncomeList.add(int.tryParse(AppConfig.getPlainAmount(
        monthlyFixedIncomeControllerMainApplicant.text)));
    monthlyRentalIncomeList.add(int.tryParse(AppConfig.getPlainAmount(
        monthlyRentalIncomeControllerMainApplicant.text)));
    totalMonthlyInstallmentForCarLoanList.add(int.tryParse(
        AppConfig.getPlainAmount(
            carLoansInstallmentControllerMainApplicant.text)));
    employementTypeList.add(selectedEmploymentTypeMainApplicant);
    numberOfHousingLoanLoansList.add(int.tryParse(
        selectedHousingLoanMainApplicant!.value == "2 or more"
            ? "2"
            : selectedHousingLoanMainApplicant!.value));
    numberOfOwnedPropertiesList.add(int.tryParse(
        selectedPropertiesMainApplicant! == '2 or more'
            ? '2'
            : selectedPropertiesMainApplicant!));
    dobList.add(dobControllerMainApplicant.text);
  }

  addDetailsToListJointApplicant1Screen1() {
    addDetailsToListForMainApplicantScreen1();
    totalMonthlyCommercialPropertyLoanPersonalList.add(int.tryParse(
        AppConfig.getPlainAmount(
            commercialPropertyLoanPersonalControllerJointApplicant1.text)));
    totalMonthlyGuarantorInstallmentList.add(int.tryParse(
        AppConfig.getPlainAmount(
            guarantorLoanAmountControllerJointApplicant1.text)));
    totalMonthlyCommercialPropertyCompanyLoanList.add(int.tryParse(
        AppConfig.getPlainAmount(
            commercialPropertyLoanCompanyControllerJointApplicant1.text)));
    totalMonthlyInstallmentForCurrentHousingLoanList.add(int.tryParse(
        AppConfig.getPlainAmount(
            housingLoansInstallmentControllerJointApplicant1.text)));
    totalMonthlyInstallmentForPersonalEducationRenovationList.add(int.tryParse(
        AppConfig.getPlainAmount(
            personalEducationRenovationControllerJointApplicant1.text)));
    minimumMonthlyCreditCardRepaymentAmountList.add(int.tryParse(
        AppConfig.getPlainAmount(
            cardRepaymentAmountControllerJointApplicant1.text)));
    annualIncomeList.add(int.tryParse(
        AppConfig.getPlainAmount(annualIncomeControllerJointApplicant1.text)));
    monthlyFixedIncomeList.add(int.tryParse(AppConfig.getPlainAmount(
        monthlyFixedIncomeControllerJointApplicant1.text)));
    monthlyRentalIncomeList.add(int.tryParse(AppConfig.getPlainAmount(
        monthlyRentalIncomeControllerJointApplicant1.text)));
    totalMonthlyInstallmentForCarLoanList.add(int.tryParse(
        AppConfig.getPlainAmount(
            carLoansInstallmentControllerJointApplicant1.text)));
    employementTypeList.add(selectedEmploymentTypeJointApplicant1);
    numberOfHousingLoanLoansList.add(int.tryParse(
        selectedHousingLoanJointApplicant1!.value == "2 or more"
            ? "2"
            : selectedHousingLoanMainApplicant!.value));
    numberOfOwnedPropertiesList.add(int.tryParse(
        selectedPropertiesJointApplicant1! == '2 or more'
            ? '2'
            : selectedPropertiesJointApplicant1!));
    dobList.add(dobControllerJointApplicant1.text);
  }

  addDetailsToListJointApplicant2Screen1() {
    addDetailsToListJointApplicant1Screen1();
    totalMonthlyCommercialPropertyLoanPersonalList.add(int.tryParse(
        AppConfig.getPlainAmount(
            commercialPropertyLoanPersonalControllerJointApplicant2.text)));
    totalMonthlyGuarantorInstallmentList.add(int.tryParse(
        AppConfig.getPlainAmount(
            guarantorLoanAmountControllerJointApplicant2.text)));
    totalMonthlyCommercialPropertyCompanyLoanList.add(int.tryParse(
        AppConfig.getPlainAmount(
            commercialPropertyLoanCompanyControllerJointApplicant2.text)));
    totalMonthlyInstallmentForCurrentHousingLoanList.add(int.tryParse(
        AppConfig.getPlainAmount(
            housingLoansInstallmentControllerJointApplicant2.text)));
    totalMonthlyInstallmentForPersonalEducationRenovationList.add(int.tryParse(
        AppConfig.getPlainAmount(
            personalEducationRenovationControllerJointApplicant2.text)));
    minimumMonthlyCreditCardRepaymentAmountList.add(int.tryParse(
        AppConfig.getPlainAmount(
            cardRepaymentAmountControllerJointApplicant2.text)));
    annualIncomeList.add(int.tryParse(
        AppConfig.getPlainAmount(annualIncomeControllerJointApplicant2.text)));
    monthlyFixedIncomeList
        .add(int.tryParse(monthlyFixedIncomeControllerJointApplicant2.text));
    monthlyRentalIncomeList.add(int.tryParse(AppConfig.getPlainAmount(
        monthlyRentalIncomeControllerJointApplicant2.text)));
    totalMonthlyInstallmentForCarLoanList.add(int.tryParse(
        AppConfig.getPlainAmount(
            carLoansInstallmentControllerJointApplicant2.text)));
    employementTypeList.add(selectedEmploymentTypeJointApplicant2);
    numberOfHousingLoanLoansList.add(int.tryParse(
        selectedHousingLoanJointApplicant2!.value == "2 or more"
            ? "2"
            : selectedHousingLoanMainApplicant!.value));
    numberOfOwnedPropertiesList.add(int.tryParse(
        selectedPropertiesJointApplicant2! == '2 or more'
            ? '2'
            : selectedPropertiesJointApplicant2!));

    dobList.add(dobControllerJointApplicant2.text);
  }

  addDetailsToListJointApplicant3Screen1() {
    addDetailsToListJointApplicant2Screen1();
    totalMonthlyCommercialPropertyLoanPersonalList.add(int.tryParse(
        AppConfig.getPlainAmount(
            commercialPropertyLoanPersonalControllerJointApplicant3.text)));
    totalMonthlyGuarantorInstallmentList.add(int.tryParse(
        AppConfig.getPlainAmount(
            guarantorLoanAmountControllerJointApplicant3.text)));
    totalMonthlyCommercialPropertyCompanyLoanList.add(int.tryParse(
        AppConfig.getPlainAmount(
            commercialPropertyLoanCompanyControllerJointApplicant3.text)));
    totalMonthlyInstallmentForCurrentHousingLoanList.add(int.tryParse(
        AppConfig.getPlainAmount(
            housingLoansInstallmentControllerJointApplicant3.text)));
    totalMonthlyInstallmentForPersonalEducationRenovationList.add(int.tryParse(
        AppConfig.getPlainAmount(
            personalEducationRenovationControllerJointApplicant3.text)));
    minimumMonthlyCreditCardRepaymentAmountList.add(int.tryParse(
        AppConfig.getPlainAmount(
            cardRepaymentAmountControllerJointApplicant3.text)));
    annualIncomeList.add(int.tryParse(
        AppConfig.getPlainAmount(annualIncomeControllerJointApplicant3.text)));
    monthlyFixedIncomeList
        .add(int.tryParse(monthlyFixedIncomeControllerJointApplicant3.text));
    monthlyRentalIncomeList.add(int.tryParse(AppConfig.getPlainAmount(
        monthlyRentalIncomeControllerJointApplicant3.text)));
    totalMonthlyInstallmentForCarLoanList.add(int.tryParse(
        AppConfig.getPlainAmount(
            carLoansInstallmentControllerJointApplicant3.text)));
    employementTypeList.add(selectedEmploymentTypeJointApplicant3);
    numberOfHousingLoanLoansList.add(int.tryParse(
        selectedHousingLoanJointApplicant3!.value == "2 or more"
            ? "2"
            : selectedHousingLoanMainApplicant!.value));
    numberOfOwnedPropertiesList.add(int.tryParse(
        selectedPropertiesJointApplicant3! == '2 or more'
            ? '2'
            : selectedPropertiesJointApplicant3!));
    dobList.add(dobControllerJointApplicant3.text);
  }

  clearAllListForCalculationOfStep1() {
    // ! Clear all the lists before calculating again with different values
    isCalculated.value = false;
    nameList.clear();
    phoneList.clear();
    countryCodeList.clear();
    emailAddressList.clear();
    dobList.clear();
    nationalityList.clear();
    employementTypeList.clear();
    monthlyFixedIncomeList.clear();
    annualIncomeList.clear();
    monthlyRentalIncomeList.clear();
    numberOfOwnedPropertiesList.clear();
    numberOfHousingLoanLoansList.clear();
    totalMonthlyInstallmentForCurrentHousingLoanList.clear();
    totalMonthlyInstallmentForCarLoanList.clear();
    totalMonthlyInstallmentForPersonalEducationRenovationList.clear();
    minimumMonthlyCreditCardRepaymentAmountList.clear();
    totalMonthlyGuarantorInstallmentList.clear();
    totalMonthlyCommercialPropertyCompanyLoanList.clear();
    totalMonthlyCommercialPropertyLoanPersonalList.clear();
  }

  onTapCalculateStep1Check() {
    addBasicDataToList();
    clearAllListForCalculationOfStep1();
    if (leadDetails.length == 1) {
      if (nameControllerMainApplicant.text.isNotEmpty &&
          phoneControllerMainApplicant.text.isNotEmpty &&
          countryCodeControllerMainApplicant.text.isNotEmpty &&
          emailControllerMainApplicant.text.isNotEmpty &&
          dobControllerMainApplicant.text.isNotEmpty &&
          nationalityControllerMainApplicant.text.isNotEmpty &&
          monthlyFixedIncomeControllerMainApplicant.text.isNotEmpty &&
          annualIncomeControllerMainApplicant.text.isNotEmpty &&
          monthlyRentalIncomeControllerMainApplicant.text.isNotEmpty &&
          housingLoansInstallmentControllerMainApplicant.text.isNotEmpty &&
          carLoansInstallmentControllerMainApplicant.text.isNotEmpty &&
          personalEducationRenovationControllerMainApplicant.text.isNotEmpty &&
          cardRepaymentAmountControllerMainApplicant.text.isNotEmpty &&
          guarantorLoanAmountControllerMainApplicant.text.isNotEmpty &&
          commercialPropertyLoanCompanyControllerMainApplicant
              .text.isNotEmpty &&
          commercialPropertyLoanPersonalControllerMainApplicant
              .text.isNotEmpty) {
        if (selectedEmploymentTypeMainApplicant == 'Salaried' &&
            monthlyFixedIncomeControllerMainApplicant.text
                    .replaceAll('\$', '')
                    .replaceAll(',', '') ==
                '0') {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Monthly fixed income should be greater than 0",
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        } else if (selectedEmploymentTypeMainApplicant == 'Self-Employed' &&
            annualIncomeControllerMainApplicant.text
                    .replaceAll('\$', '')
                    .replaceAll(',', '') ==
                '0') {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Annual income should be greater than 0",
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        } else {
          // if (int.tryParse(
          //       monthlyFixedIncomeControllerMainApplicant.text
          //           .replaceAll('\$', '')
          //           .replaceAll(',', ''),
          //     )! >
          //     0) {
          onTapCalculateStep1();
          // } else {
          // Get.snackbar(
          //   'Sorry!',
          //   'Monthly fixed income should be greater than 0',
          //   backgroundColor: Colors.red,
          //   colorText: Colors.white,
          //   snackPosition: SnackPosition.TOP,
          //   duration: const Duration(seconds: 3),
          // );
          // }
        }
      } else {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "All fields are mandatory",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      }
    } else if (leadDetails.length == 2) {
      if (nameControllerMainApplicant.text.isNotEmpty &&
          phoneControllerMainApplicant.text.isNotEmpty &&
          countryCodeControllerMainApplicant.text.isNotEmpty &&
          emailControllerMainApplicant.text.isNotEmpty &&
          dobControllerMainApplicant.text.isNotEmpty &&
          nationalityControllerMainApplicant.text.isNotEmpty &&
          monthlyFixedIncomeControllerMainApplicant.text.isNotEmpty &&
          annualIncomeControllerMainApplicant.text.isNotEmpty &&
          monthlyRentalIncomeControllerMainApplicant.text.isNotEmpty &&
          housingLoansInstallmentControllerMainApplicant.text.isNotEmpty &&
          carLoansInstallmentControllerMainApplicant.text.isNotEmpty &&
          personalEducationRenovationControllerMainApplicant.text.isNotEmpty &&
          cardRepaymentAmountControllerMainApplicant.text.isNotEmpty &&
          guarantorLoanAmountControllerMainApplicant.text.isNotEmpty &&
          commercialPropertyLoanCompanyControllerMainApplicant
              .text.isNotEmpty &&
          commercialPropertyLoanPersonalControllerMainApplicant
              .text.isNotEmpty &&
          nameControllerJointApplicant1.text.isNotEmpty &&
          phoneControllerJointApplicant1.text.isNotEmpty &&
          countryCodeControllerJointApplicant1.text.isNotEmpty &&
          emailControllerJointApplicant1.text.isNotEmpty &&
          dobControllerJointApplicant1.text.isNotEmpty &&
          nationalityControllerJointApplicant1.text.isNotEmpty &&
          monthlyFixedIncomeControllerJointApplicant1.text.isNotEmpty &&
          annualIncomeControllerJointApplicant1.text.isNotEmpty &&
          monthlyRentalIncomeControllerJointApplicant1.text.isNotEmpty &&
          housingLoansInstallmentControllerJointApplicant1.text.isNotEmpty &&
          carLoansInstallmentControllerJointApplicant1.text.isNotEmpty &&
          personalEducationRenovationControllerJointApplicant1
              .text.isNotEmpty &&
          cardRepaymentAmountControllerJointApplicant1.text.isNotEmpty &&
          guarantorLoanAmountControllerJointApplicant1.text.isNotEmpty &&
          commercialPropertyLoanCompanyControllerJointApplicant1
              .text.isNotEmpty &&
          commercialPropertyLoanPersonalControllerJointApplicant1
              .text.isNotEmpty) {
        if ((selectedEmploymentTypeMainApplicant == 'Salaried' &&
                monthlyFixedIncomeControllerMainApplicant.text
                        .replaceAll('\$', '')
                        .replaceAll(',', '') ==
                    '0') ||
            (selectedEmploymentTypeJointApplicant1 == 'Salaried' &&
                monthlyFixedIncomeControllerJointApplicant1.text
                        .replaceAll('\$', '')
                        .replaceAll(',', '') ==
                    '0')) {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Monthly fixed income should be greater than 0",
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        } else if ((selectedEmploymentTypeMainApplicant == 'Self-Employed' &&
                annualIncomeControllerMainApplicant.text
                        .replaceAll('\$', '')
                        .replaceAll(',', '') ==
                    '0') ||
            (selectedEmploymentTypeJointApplicant1 == 'Self-Employed' &&
                annualIncomeControllerJointApplicant1.text
                        .replaceAll('\$', '')
                        .replaceAll(',', '') ==
                    '0')) {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Annual income should be greater than 0",
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        } else {
          // if (int.tryParse(
          //           monthlyFixedIncomeControllerMainApplicant.text
          //               .replaceAll('\$', '')
          //               .replaceAll(',', ''),
          //         )! >
          //         0 &&
          //     int.tryParse(
          //           monthlyFixedIncomeControllerJointApplicant1.text
          //               .replaceAll('\$', '')
          //               .replaceAll(',', ''),
          //         )! >
          //         0) {
          onTapCalculateStep1();
          // } else {
          //   Get.snackbar(
          //     'Sorry!',
          //     'Monthly fixed income should be greater than 0',
          //     backgroundColor: Colors.red,
          //     colorText: Colors.white,
          //     snackPosition: SnackPosition.TOP,
          //     duration: const Duration(seconds: 3),
          //   );
          // }
        }
      } else {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "All fields are mandatory",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      }
    } else if (leadDetails.length == 3) {
      if (nameControllerMainApplicant.text.isNotEmpty &&
          phoneControllerMainApplicant.text.isNotEmpty &&
          countryCodeControllerMainApplicant.text.isNotEmpty &&
          emailControllerMainApplicant.text.isNotEmpty &&
          dobControllerMainApplicant.text.isNotEmpty &&
          nationalityControllerMainApplicant.text.isNotEmpty &&
          monthlyFixedIncomeControllerMainApplicant.text.isNotEmpty &&
          annualIncomeControllerMainApplicant.text.isNotEmpty &&
          monthlyRentalIncomeControllerMainApplicant.text.isNotEmpty &&
          housingLoansInstallmentControllerMainApplicant.text.isNotEmpty &&
          carLoansInstallmentControllerMainApplicant.text.isNotEmpty &&
          personalEducationRenovationControllerMainApplicant.text.isNotEmpty &&
          cardRepaymentAmountControllerMainApplicant.text.isNotEmpty &&
          guarantorLoanAmountControllerMainApplicant.text.isNotEmpty &&
          commercialPropertyLoanCompanyControllerMainApplicant
              .text.isNotEmpty &&
          commercialPropertyLoanPersonalControllerMainApplicant
              .text.isNotEmpty &&
          nameControllerJointApplicant1.text.isNotEmpty &&
          phoneControllerJointApplicant1.text.isNotEmpty &&
          countryCodeControllerJointApplicant1.text.isNotEmpty &&
          emailControllerJointApplicant1.text.isNotEmpty &&
          dobControllerJointApplicant1.text.isNotEmpty &&
          nationalityControllerJointApplicant1.text.isNotEmpty &&
          monthlyFixedIncomeControllerJointApplicant1.text.isNotEmpty &&
          annualIncomeControllerJointApplicant1.text.isNotEmpty &&
          monthlyRentalIncomeControllerJointApplicant1.text.isNotEmpty &&
          housingLoansInstallmentControllerJointApplicant1.text.isNotEmpty &&
          carLoansInstallmentControllerJointApplicant1.text.isNotEmpty &&
          personalEducationRenovationControllerJointApplicant1
              .text.isNotEmpty &&
          cardRepaymentAmountControllerJointApplicant1.text.isNotEmpty &&
          guarantorLoanAmountControllerJointApplicant1.text.isNotEmpty &&
          commercialPropertyLoanCompanyControllerJointApplicant1
              .text.isNotEmpty &&
          commercialPropertyLoanPersonalControllerJointApplicant1
              .text.isNotEmpty &&
          nameControllerJointApplicant2.text.isNotEmpty &&
          phoneControllerJointApplicant2.text.isNotEmpty &&
          countryCodeControllerJointApplicant2.text.isNotEmpty &&
          emailControllerJointApplicant2.text.isNotEmpty &&
          dobControllerJointApplicant2.text.isNotEmpty &&
          nationalityControllerJointApplicant2.text.isNotEmpty &&
          monthlyFixedIncomeControllerJointApplicant2.text.isNotEmpty &&
          annualIncomeControllerJointApplicant2.text.isNotEmpty &&
          monthlyRentalIncomeControllerJointApplicant2.text.isNotEmpty &&
          housingLoansInstallmentControllerJointApplicant2.text.isNotEmpty &&
          carLoansInstallmentControllerJointApplicant2.text.isNotEmpty &&
          personalEducationRenovationControllerJointApplicant2
              .text.isNotEmpty &&
          cardRepaymentAmountControllerJointApplicant2.text.isNotEmpty &&
          guarantorLoanAmountControllerJointApplicant2.text.isNotEmpty &&
          commercialPropertyLoanCompanyControllerJointApplicant2
              .text.isNotEmpty &&
          commercialPropertyLoanPersonalControllerJointApplicant2
              .text.isNotEmpty) {
        if ((selectedEmploymentTypeMainApplicant == 'Salaried' &&
                monthlyFixedIncomeControllerMainApplicant.text
                        .replaceAll('\$', '')
                        .replaceAll(',', '') ==
                    '0') ||
            (selectedEmploymentTypeJointApplicant1 == 'Salaried' &&
                monthlyFixedIncomeControllerJointApplicant1.text
                        .replaceAll('\$', '')
                        .replaceAll(',', '') ==
                    '0') ||
            (selectedEmploymentTypeJointApplicant2 == 'Salaried' &&
                monthlyFixedIncomeControllerJointApplicant2.text
                        .replaceAll('\$', '')
                        .replaceAll(',', '') ==
                    '0')) {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Monthly fixed income should be greater than 0",
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        } else if ((selectedEmploymentTypeMainApplicant == 'Self-Employed' &&
                annualIncomeControllerMainApplicant.text
                        .replaceAll('\$', '')
                        .replaceAll(',', '') ==
                    '0') ||
            (selectedEmploymentTypeJointApplicant1 == 'Self-Employed' &&
                annualIncomeControllerJointApplicant1.text
                        .replaceAll('\$', '')
                        .replaceAll(',', '') ==
                    '0') ||
            (selectedEmploymentTypeJointApplicant2 == 'Self-Employed' &&
                annualIncomeControllerJointApplicant2.text
                        .replaceAll('\$', '')
                        .replaceAll(',', '') ==
                    '0')) {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Annual income should be greater than 0",
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        } else {
          // if (int.tryParse(
          //         monthlyFixedIncomeControllerMainApplicant.text
          //             .replaceAll('\$', '')
          //             .replaceAll(',', ''),
          //       )! >
          //       0 &&
          //   int.tryParse(
          //         monthlyFixedIncomeControllerJointApplicant1.text
          //             .replaceAll('\$', '')
          //             .replaceAll(',', ''),
          //       )! >
          //       0 &&
          //   int.tryParse(
          //         monthlyFixedIncomeControllerJointApplicant2.text
          //             .replaceAll('\$', '')
          //             .replaceAll(',', ''),
          //       )! >
          //       0) {
          onTapCalculateStep1();
          // } else {
          //   Get.snackbar(
          //     'Sorry!',
          //     'Monthly fixed income and annual income should be greater than 0',
          //     backgroundColor: Colors.red,
          //     colorText: Colors.white,
          //     snackPosition: SnackPosition.TOP,
          //     duration: const Duration(seconds: 3),
          //   );
          // }
        }
      } else {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "All fields are mandatory",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      }
    } else if (leadDetails.length == 4) {
      if (nameControllerMainApplicant.text.isNotEmpty &&
          phoneControllerMainApplicant.text.isNotEmpty &&
          countryCodeControllerMainApplicant.text.isNotEmpty &&
          emailControllerMainApplicant.text.isNotEmpty &&
          dobControllerMainApplicant.text.isNotEmpty &&
          nationalityControllerMainApplicant.text.isNotEmpty &&
          monthlyFixedIncomeControllerMainApplicant.text.isNotEmpty &&
          annualIncomeControllerMainApplicant.text.isNotEmpty &&
          monthlyRentalIncomeControllerMainApplicant.text.isNotEmpty &&
          housingLoansInstallmentControllerMainApplicant.text.isNotEmpty &&
          carLoansInstallmentControllerMainApplicant.text.isNotEmpty &&
          personalEducationRenovationControllerMainApplicant.text.isNotEmpty &&
          cardRepaymentAmountControllerMainApplicant.text.isNotEmpty &&
          guarantorLoanAmountControllerMainApplicant.text.isNotEmpty &&
          commercialPropertyLoanCompanyControllerMainApplicant
              .text.isNotEmpty &&
          commercialPropertyLoanPersonalControllerMainApplicant
              .text.isNotEmpty &&
          nameControllerJointApplicant1.text.isNotEmpty &&
          phoneControllerJointApplicant1.text.isNotEmpty &&
          countryCodeControllerJointApplicant1.text.isNotEmpty &&
          emailControllerJointApplicant1.text.isNotEmpty &&
          dobControllerJointApplicant1.text.isNotEmpty &&
          nationalityControllerJointApplicant1.text.isNotEmpty &&
          monthlyFixedIncomeControllerJointApplicant1.text.isNotEmpty &&
          annualIncomeControllerJointApplicant1.text.isNotEmpty &&
          monthlyRentalIncomeControllerJointApplicant1.text.isNotEmpty &&
          housingLoansInstallmentControllerJointApplicant1.text.isNotEmpty &&
          carLoansInstallmentControllerJointApplicant1.text.isNotEmpty &&
          personalEducationRenovationControllerJointApplicant1
              .text.isNotEmpty &&
          cardRepaymentAmountControllerJointApplicant1.text.isNotEmpty &&
          guarantorLoanAmountControllerJointApplicant1.text.isNotEmpty &&
          commercialPropertyLoanCompanyControllerJointApplicant1
              .text.isNotEmpty &&
          commercialPropertyLoanPersonalControllerJointApplicant1
              .text.isNotEmpty &&
          nameControllerJointApplicant2.text.isNotEmpty &&
          phoneControllerJointApplicant2.text.isNotEmpty &&
          countryCodeControllerJointApplicant2.text.isNotEmpty &&
          emailControllerJointApplicant2.text.isNotEmpty &&
          dobControllerJointApplicant2.text.isNotEmpty &&
          nationalityControllerJointApplicant2.text.isNotEmpty &&
          monthlyFixedIncomeControllerJointApplicant2.text.isNotEmpty &&
          AppConfig.getPlainAmount(annualIncomeControllerJointApplicant2.text)
              .isNotEmpty &&
          monthlyRentalIncomeControllerJointApplicant2.text.isNotEmpty &&
          housingLoansInstallmentControllerJointApplicant2.text.isNotEmpty &&
          carLoansInstallmentControllerJointApplicant2.text.isNotEmpty &&
          personalEducationRenovationControllerJointApplicant2
              .text.isNotEmpty &&
          cardRepaymentAmountControllerJointApplicant2.text.isNotEmpty &&
          guarantorLoanAmountControllerJointApplicant2.text.isNotEmpty &&
          commercialPropertyLoanCompanyControllerJointApplicant2
              .text.isNotEmpty &&
          commercialPropertyLoanPersonalControllerJointApplicant2
              .text.isNotEmpty &&
          nameControllerJointApplicant3.text.isNotEmpty &&
          phoneControllerJointApplicant3.text.isNotEmpty &&
          countryCodeControllerJointApplicant3.text.isNotEmpty &&
          emailControllerJointApplicant3.text.isNotEmpty &&
          dobControllerJointApplicant3.text.isNotEmpty &&
          nationalityControllerJointApplicant3.text.isNotEmpty &&
          monthlyFixedIncomeControllerJointApplicant3.text.isNotEmpty &&
          annualIncomeControllerJointApplicant3.text.isNotEmpty &&
          monthlyRentalIncomeControllerJointApplicant3.text.isNotEmpty &&
          housingLoansInstallmentControllerJointApplicant3.text.isNotEmpty &&
          carLoansInstallmentControllerJointApplicant3.text.isNotEmpty &&
          personalEducationRenovationControllerJointApplicant3
              .text.isNotEmpty &&
          cardRepaymentAmountControllerJointApplicant3.text.isNotEmpty &&
          guarantorLoanAmountControllerJointApplicant3.text.isNotEmpty &&
          commercialPropertyLoanCompanyControllerJointApplicant3
              .text.isNotEmpty &&
          commercialPropertyLoanPersonalControllerJointApplicant3
              .text.isNotEmpty) {
        if ((selectedEmploymentTypeMainApplicant == 'Salaried' &&
                monthlyFixedIncomeControllerMainApplicant.text
                        .replaceAll('\$', '')
                        .replaceAll(',', '') ==
                    '0') ||
            (selectedEmploymentTypeJointApplicant1 == 'Salaried' &&
                monthlyFixedIncomeControllerJointApplicant1.text
                        .replaceAll('\$', '')
                        .replaceAll(',', '') ==
                    '0') ||
            (selectedEmploymentTypeJointApplicant2 == 'Salaried' &&
                monthlyFixedIncomeControllerJointApplicant2.text
                        .replaceAll('\$', '')
                        .replaceAll(',', '') ==
                    '0') ||
            (selectedEmploymentTypeJointApplicant2 == 'Salaried' &&
                monthlyFixedIncomeControllerJointApplicant2.text
                        .replaceAll('\$', '')
                        .replaceAll(',', '') ==
                    '0') ||
            (selectedEmploymentTypeJointApplicant3 == 'Salaried' &&
                monthlyFixedIncomeControllerJointApplicant3.text
                        .replaceAll('\$', '')
                        .replaceAll(',', '') ==
                    '0')) {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Monthly fixed income should be greater than 0",
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        } else if ((selectedEmploymentTypeMainApplicant == 'Self-Employed' &&
                annualIncomeControllerMainApplicant.text
                        .replaceAll('\$', '')
                        .replaceAll(',', '') ==
                    '0') ||
            (selectedEmploymentTypeJointApplicant1 == 'Self-Employed' &&
                annualIncomeControllerJointApplicant1.text
                        .replaceAll('\$', '')
                        .replaceAll(',', '') ==
                    '0') ||
            (selectedEmploymentTypeJointApplicant2 == 'Self-Employed' &&
                annualIncomeControllerJointApplicant2.text
                        .replaceAll('\$', '')
                        .replaceAll(',', '') ==
                    '0') ||
            (selectedEmploymentTypeJointApplicant3 == 'Self-Employed' &&
                annualIncomeControllerJointApplicant3.text
                        .replaceAll('\$', '')
                        .replaceAll(',', '') ==
                    '0')) {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Annual income should be greater than 0",
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        } else {
          // if (int.tryParse(
          //         monthlyFixedIncomeControllerMainApplicant.text
          //             .replaceAll('\$', '')
          //             .replaceAll(',', ''),
          //       )! >
          //       0 &&
          //   int.tryParse(
          //         monthlyFixedIncomeControllerJointApplicant1.text
          //             .replaceAll('\$', '')
          //             .replaceAll(',', ''),
          //       )! >
          //       0 &&
          //   int.tryParse(
          //         monthlyFixedIncomeControllerJointApplicant2.text
          //             .replaceAll('\$', '')
          //             .replaceAll(',', ''),
          //       )! >
          //       0 &&
          //   int.tryParse(
          //         monthlyFixedIncomeControllerJointApplicant3.text
          //             .replaceAll('\$', '')
          //             .replaceAll(',', ''),
          //       )! >
          //       0) {
          onTapCalculateStep1();
          // } else {
          //   Get.snackbar(
          //     'Sorry!',
          //     'Monthly fixed income and annual income should be greater than 0',
          //     backgroundColor: Colors.red,
          //     colorText: Colors.white,
          //     snackPosition: SnackPosition.TOP,
          //     duration: const Duration(seconds: 3),
          //   );
          // }
        }
      } else {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "All fields are mandatory",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      }
    }
  }

  onTapCalculateStep1() async {
    final Box hive = Hive.box(HiveString.hiveName);

    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);

    if (leadDetails.length == 1) {
      addDetailsToListForMainApplicantScreen1();
    } else if (leadDetails.length == 2) {
      addDetailsToListJointApplicant1Screen1();
    } else if (leadDetails.length == 3) {
      addDetailsToListJointApplicant2Screen1();
    } else if (leadDetails.length == 4) {
      addDetailsToListJointApplicant3Screen1();
    }

    NewPurchaseTotalSendModel newPurchaseTotalSendModel =
        NewPurchaseTotalSendModel(
      numberOfLoanApplicants: leadDetails.length,
      totalPropertyInstallment: totalMonthlyCommercialPropertyLoanPersonalList,
      totalGurantorInstallment: totalMonthlyGuarantorInstallmentList,
      totalPropertyCompanyInstallment:
          totalMonthlyCommercialPropertyCompanyLoanList,
      totalHouseInstallment: totalMonthlyInstallmentForCurrentHousingLoanList,
      totalPersonalInstallment:
          totalMonthlyInstallmentForPersonalEducationRenovationList,
      totalCreditInstallment: minimumMonthlyCreditCardRepaymentAmountList,
      annualIncome: annualIncomeList,
      monthlyFixedIncome: monthlyFixedIncomeList,
      monthlyRentalIncome: monthlyRentalIncomeList,
      employementType: employementTypeList,
      totalCarInstallment: totalMonthlyInstallmentForCarLoanList,
      token: hive.get(HiveString.token.toString()),
    );

    var data = await CoreService().postWithAuth(
        url: baseUrl + newPurchaseTotalApi,
        body: newPurchaseTotalSendModel.toJson());

    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
      if (Get.isDialogOpen ?? false) Get.back();
      clearAllListForCalculationOfStep1();
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          onTapCalculateStep1();
        }
      } else {
        isCalculated.value = true;
        resultMainApplicant.value =
            NewPurchaseTotalResponseModel.fromJson(data);
        if (resultMainApplicant.value.totalMonthlyIncome != null) {
          totalMonthlyIncomeMainApplicant.value =
              resultMainApplicant.value.totalMonthlyIncome!.toDouble();
          totalFinancialCommitmentsMainApplicant.value =
              resultMainApplicant.value.totalFinancialCommitments!.toDouble();
        }
      }
    }
  }

  onTapCalculateStep2Check() {
    if (propertyPriceControllerMainApplicant.text == "0") {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Property price cannot be 0",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return;
    }

    // if (loanTenureYearControllerMainApplicant.text == "0") {
    //   Get.snackbar(
    //     'Sorry!',
    //     "Loan Tenure Year can't be 0",
    //     snackPosition: SnackPosition.TOP,
    //     backgroundColor: Colors.red,
    //     colorText: Colors.white,
    //     duration: const Duration(seconds: 3),
    //   );
    //   return;
    // }

    if (propertyPriceControllerMainApplicant.text.isNotEmpty &&
        loanAmountControllerMainApplicant.text.isNotEmpty &&
        loanTenureYearControllerMainApplicant.text.isNotEmpty) {
      if (selectedLongerLoanTenureMainApplicant.value == "No") {
        if (int.tryParse(loanTenureYearControllerMainApplicant.text)! <= 25 &&
            selectedPropertyTypeMainApplicant == 'HDB') {
          onTapCalculateStep2(); //If PropertyType is HDB and preferred loan tenure is less than equal to 25
        } else if (int.tryParse(loanTenureYearControllerMainApplicant.text)! <=
                30 &&
            selectedPropertyTypeMainApplicant != 'HDB') {
          onTapCalculateStep2(); //If PropertyType is not HDB and preferred loan tenure is less than equal to 30
        } else if (int.tryParse(loanTenureYearControllerMainApplicant.text)! >
                25 &&
            selectedPropertyTypeMainApplicant == 'HDB') {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Loan tenure cannot be greater than 25 years",
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        } else {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Loan tenure cannot be more than 30 years",
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }
      } else {
        // This is Yes condition for longer loan tenure
        if (int.tryParse(loanTenureYearControllerMainApplicant.text)! <= 30 &&
            selectedPropertyTypeMainApplicant == 'HDB') {
          onTapCalculateStep2(); //If PropertyType is HDB and preferred loan tenure is less than equal to 30
        } else if (int.tryParse(loanTenureYearControllerMainApplicant.text)! <=
                35 &&
            selectedPropertyTypeMainApplicant != 'HDB') {
          onTapCalculateStep2(); //If PropertyType is NOT HDB and preferred loan tenure is less than equal to 35
        } else if (int.tryParse(loanTenureYearControllerMainApplicant.text)! >
                30 &&
            selectedPropertyTypeMainApplicant == 'HDB') {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Loan tenure cannot be more than 30 years",
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        } else {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Loan tenure cannot be more than 35 years",
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }
      }
    } else {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "All fields are mandatory",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    }
  }

  int indexOfPropertyTypeDropDown = 0;

  onTapCalculateStep2() async {
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);

    int index = dropdownPropertyType
        .indexWhere((element) => element == selectedPropertyTypeMainApplicant);
    int propertyTypeValue = index++;
    propertyTypeForReportsAndPDF = propertyTypeValue;

    debugPrint('************');
    debugPrint(dobList.toString());
    debugPrint(numberOfOwnedPropertiesList.toString());
    debugPrint('************');

    NewPurchaseCalculatorSendModel newPurchaseCalculatorSendModel =
        NewPurchaseCalculatorSendModel(
            propertyPrice: int.tryParse(AppConfig.getPlainAmount(
                propertyPriceControllerMainApplicant.text)),
            loanAmount: int.tryParse(AppConfig.getPlainAmount(
                loanAmountControllerMainApplicant.text)),
            dob: dobControllerMainApplicant.text,
            extendedLoanTenure: selectedLongerLoanTenureMainApplicant.value
                .toString()
                .toLowerCase(),
            propertyType: propertyTypeValue + 1,
            noOfLoanApplicants: leadDetails.length,
            monthlyFixedIncome: monthlyFixedIncomeList,
            totalHouseInstallment:
                totalMonthlyInstallmentForCurrentHousingLoanList,
            totalCarInstallment: totalMonthlyInstallmentForCarLoanList,
            totalPersonalInstallment:
                totalMonthlyInstallmentForPersonalEducationRenovationList,
            totalCreditInstallment: minimumMonthlyCreditCardRepaymentAmountList,
            totalPropertyInstallment:
                totalMonthlyCommercialPropertyLoanPersonalList,
            totalPropertyCompanyInstallment:
                totalMonthlyCommercialPropertyCompanyLoanList,
            annualIncome: annualIncomeList,
            monthlyRentalIncome: monthlyRentalIncomeList,
            totalGuarantorInstallment: totalMonthlyGuarantorInstallmentList,
            numberOfHousingLoan: numberOfHousingLoanLoansList,
            numberOfOwnProperties: numberOfOwnedPropertiesList,
            nationality: nationalityControllerMainApplicant.text,
            preferredLoanTenure:
                int.tryParse(loanTenureYearControllerMainApplicant.text),
            loanCategory: selectedLoanTypeMainApplicant,
            dobList: dobList,
            employemetTypeList: employementTypeList);

    var data = await CoreService().postWithAuth(
      url: baseUrl + newPurchaseCalcApi,
      body: newPurchaseCalculatorSendModel.toJson(),
    );

    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
      if (Get.isDialogOpen ?? false) Get.back();
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          onTapCalculateStep2();
        }
      } else {
        if (data["message"] == null) {
          resultMainApplicantStep2.value =
              NewPurchaseResponseModel.fromJson(data);

          isCalculated.value = true;

          getPledgeAmountMlt();
          resultMainApplicantStep2.value.preferedTenureBasedAffordability !=
                  null
              ? getPledgeAmountPlt()
              : null;
          getUnpledgeAmountMlt();
          resultMainApplicantStep2.value.preferedTenureBasedAffordability !=
                  null
              ? getUnpledgeAmountPlt()
              : null;
        } else {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: data["message"][0],
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }
      }
    }
  }

  RxString pledgeAmountMlt = ''.obs;
  RxString pledgeAmountPlt = ''.obs;
  RxString unpledgeAmountMlt = ''.obs;
  RxString unpledgeAmountPlt = ''.obs;

  RxString pledgeAmountMltCalculated = ''.obs;
  RxString pledgeAmountPltCalculated = ''.obs;
  RxString unpledgeAmountMltCalculated = ''.obs;
  RxString unpledgeAmountPltCalculated = ''.obs;

  getPledgeAmountMlt() async {
    // Get.isDialogOpen ?? true
    //     ? const Offstage()
    //     : Get.dialog(
    //         const Center(
    //           child: CircularProgressIndicator(),
    //         ),
    //         barrierDismissible: false);
    // debugPrint("***********");
    // debugPrint(resultMainApplicantStep2
    //     .value.maximumTenureBasedAffordability!.maximumQualifiedLoan
    //     .toString()
    //     .replaceAll(',', '')
    //     .replaceAll('%', '')
    //     .replaceAll('\$', ''));
    // debugPrint("***********");
    int index = dropdownPropertyType
        .indexWhere((element) => element == selectedPropertyTypeMainApplicant);
    int propertyTypeValue = index++;
    NewPurchaseGetPledgeAmountSendModel newPurchaseGetPledgeAmountSendModel =
        NewPurchaseGetPledgeAmountSendModel(
            loanAmount: double.tryParse(loanAmountControllerMainApplicant
                    .text.removeAllWhitespace
                    .replaceAll(',', '')
                // resultMainApplicantStep2
                //     .value.maximumTenureBasedAffordability!.maximumQualifiedLoan
                //     .toString()
                //     .replaceAll(',', '')
                //     .replaceAll('%', '')
                //     .replaceAll('\$', ''),
                ),
            preferredUnfludgeAmount:
                preferredUnpledgeAmountMltControllerMainApplicant.text.isEmpty
                    ? 0
                    : int.tryParse(
                        preferredUnpledgeAmountMltControllerMainApplicant.text,
                      ),
            totalMonthlyIncome: double.tryParse(resultMainApplicant
                .value.totalMonthlyIncome
                .toString()
                .replaceAll(',', '')
                .replaceAll('%', '')
                .replaceAll('\$', '')),
            loanTenure: int.tryParse(
              resultMainApplicantStep2
                  .value.maximumTenureBasedAffordability!.loanTenure
                  .toString()
                  .replaceAll(',', '')
                  .replaceAll('%', '')
                  .replaceAll('\$', '')
                  .replaceAll('years', ''),
            ),
            totalFinancialCommitments: double.tryParse(
              resultMainApplicant.value.totalFinancialCommitments
                  .toString()
                  .replaceAll(',', '')
                  .replaceAll('%', '')
                  .replaceAll('\$', ''),
            ),
            propertyType: propertyTypeValue + 1);

    var data = await CoreService().postWithAuth(
      url: baseUrl + newPurchaseGetPledgeAmountApi,
      body: newPurchaseGetPledgeAmountSendModel.toJson(),
    );

    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
      if (Get.isDialogOpen ?? false) Get.back();
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          getPledgeAmountMlt();
        }
      } else {
        if (data["message"] == null) {
          debugPrint(pledgeAmountMlt.value.toString());
          // pledgeAmountMlt = data["fludge_amount"];
          pledgeAmountMltCalculated.value = data["fludge_amount"];
          // pledgeAmountMltCalculated.value = double.tryParse(
          //     data["fludge_amount"].toString().replaceAll(',', ''))!;
          pledgeAmountMlt.value = pledgeAmountMltCalculated
              .value; // Assigning value to show in the UI
          debugPrint(pledgeAmountMltCalculated.toString());
        } else {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: data["message"],
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }
      }
    }
  }

  getUnpledgeAmountMlt() async {
    // Get.isDialogOpen ?? true
    //     ? const Offstage()
    //     : Get.dialog(
    //         const Center(
    //           child: CircularProgressIndicator(),
    //         ),
    //         barrierDismissible: false);
    int index = dropdownPropertyType
        .indexWhere((element) => element == selectedPropertyTypeMainApplicant);
    int propertyTypeValue = index++;
    NewPurchaseGetUnpledgeAmountSendModel
        newPurchaseGetUnpledgeAmountSendModel =
        NewPurchaseGetUnpledgeAmountSendModel(
            loanAmount: double.tryParse(loanAmountControllerMainApplicant
                    .text.removeAllWhitespace
                    .replaceAll(',', '')
                // resultMainApplicantStep2
                //     .value.maximumTenureBasedAffordability!.maximumQualifiedLoan
                //     .toString()
                //     .replaceAll(',', '')
                //     .replaceAll('%', '')
                //     .replaceAll('\$', '')
                //     .replaceAll('%', ''),
                ),
            preferredfludgeAmount:
                preferredPledgeAmountMltControllerMainApplicant.text.isEmpty
                    ? 0
                    : double.tryParse(
                        preferredPledgeAmountMltControllerMainApplicant.text),
            totalMonthlyIncome: double.tryParse(
              resultMainApplicant.value.totalMonthlyIncome
                  .toString()
                  .replaceAll(',', '')
                  .replaceAll('%', '')
                  .replaceAll('\$', '')
                  .replaceAll('%', ''),
            ),
            loanTenure: int.tryParse(
              resultMainApplicantStep2
                  .value.maximumTenureBasedAffordability!.loanTenure
                  .toString()
                  .replaceAll(',', '')
                  .replaceAll('%', '')
                  .replaceAll('\$', '')
                  .replaceAll('%', '')
                  .replaceAll('years', ''),
            ),
            totalFinancialCommitments: double.tryParse(
              resultMainApplicant.value.totalFinancialCommitments
                  .toString()
                  .replaceAll(',', '')
                  .replaceAll('%', '')
                  .replaceAll('\$', '')
                  .replaceAll('%', ''),
            ),
            propertyType: propertyTypeValue + 1);

    var data = await CoreService().postWithAuth(
      url: baseUrl + newPurchaseGetunPledgeAmountApi,
      body: newPurchaseGetUnpledgeAmountSendModel.toJson(),
    );

    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
      if (Get.isDialogOpen ?? false) Get.back();
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          getUnpledgeAmountMlt();
        }
      } else {
        if (data["message"] == null) {
          unpledgeAmountMltCalculated.value = data["unfludge_amount"];
          // unpledgeAmountMltCalculated.value = double.tryParse(
          //     data["unfludge_amount"].toString().replaceAll(',', ''))!;
          unpledgeAmountMlt.value = unpledgeAmountMltCalculated
              .value; // Assigning value to show in the UI
          debugPrint(unpledgeAmountMltCalculated.toString());
        } else {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: data["message"],
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }
      }
    }
  }

  getPledgeAmountPlt() async {
    // Get.isDialogOpen ?? true
    //     ? const Offstage()
    //     : Get.dialog(
    //         const Center(
    //           child: CircularProgressIndicator(),
    //         ),
    //         barrierDismissible: false);
    int index = dropdownPropertyType
        .indexWhere((element) => element == selectedPropertyTypeMainApplicant);
    int propertyTypeValue = index++;
    NewPurchaseGetPledgeAmountSendModel newPurchaseGetPledgeAmountSendModel =
        NewPurchaseGetPledgeAmountSendModel(
            loanAmount: double.tryParse(loanAmountControllerMainApplicant
                    .text.removeAllWhitespace
                    .replaceAll(',', '')
                // resultMainApplicantStep2
                //     .value.preferedTenureBasedAffordability!.maximumQualifiedLoan
                //     .toString()
                //     .replaceAll(',', '')
                //     .replaceAll('%', '')
                //     .replaceAll('\$', '')
                //     .replaceAll('%', ''),
                ),
            preferredUnfludgeAmount:
                preferredUnpledgeAmountPltControllerMainApplicant.text.isEmpty
                    ? 0
                    : int.tryParse(
                        preferredUnpledgeAmountPltControllerMainApplicant.text),
            totalMonthlyIncome: double.tryParse(
              totalMonthlyIncomeMainApplicant
                  .toString()
                  .replaceAll(',', '')
                  .replaceAll('%', '')
                  .replaceAll('\$', '')
                  .replaceAll('%', ''),
            ),
            loanTenure: int.tryParse(
              resultMainApplicantStep2
                  .value.preferedTenureBasedAffordability!.loanTenure
                  .toString()
                  .replaceAll(',', '')
                  .replaceAll('%', '')
                  .replaceAll('\$', '')
                  .replaceAll('%', '')
                  .replaceAll('years', ''),
            ),
            totalFinancialCommitments: double.tryParse(
              totalFinancialCommitmentsMainApplicant
                  .toString()
                  .replaceAll(',', '')
                  .replaceAll('%', '')
                  .replaceAll('\$', '')
                  .replaceAll('%', ''),
            ),
            propertyType: propertyTypeValue + 1);

    var data = await CoreService().postWithAuth(
      url: baseUrl + newPurchaseGetPledgeAmountApi,
      body: newPurchaseGetPledgeAmountSendModel.toJson(),
    );

    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
      if (Get.isDialogOpen ?? false) Get.back();
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          getPledgeAmountPlt();
        }
      } else {
        if (data["message"] == null) {
          pledgeAmountPltCalculated.value = data["fludge_amount"];
          // pledgeAmountPltCalculated.value = double.tryParse(
          //     data["fludge_amount"].toString().replaceAll(',', ''))!;
          pledgeAmountPlt.value = pledgeAmountPltCalculated
              .value; // Calculated value is assigned to the variable responsible to display in UI
          debugPrint(pledgeAmountPltCalculated.toString());
        } else {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: data["message"],
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }
      }
    }
  }

  getUnpledgeAmountPlt() async {
    // Get.isDialogOpen ?? true
    //     ? const Offstage()
    //     : Get.dialog(
    //         const Center(
    //           child: CircularProgressIndicator(),
    //         ),
    //         barrierDismissible: false);
    int index = dropdownPropertyType
        .indexWhere((element) => element == selectedPropertyTypeMainApplicant);
    int propertyTypeValue = index++;
    NewPurchaseGetUnpledgeAmountSendModel
        newPurchaseGetUnpledgeAmountSendModel =
        NewPurchaseGetUnpledgeAmountSendModel(
            loanAmount: double.tryParse(loanAmountControllerMainApplicant
                    .text.removeAllWhitespace
                    .replaceAll(',', '')
                // resultMainApplicantStep2
                //     .value.preferedTenureBasedAffordability!.maximumQualifiedLoan
                //     .toString()
                //     .replaceAll(',', '')
                //     .replaceAll('%', '')
                //     .replaceAll('\$', '')
                //     .replaceAll('%', ''),
                ),
            preferredfludgeAmount:
                preferredPledgeAmountPltControllerMainApplicant.text.isEmpty
                    ? 0
                    : double.tryParse(
                        preferredPledgeAmountPltControllerMainApplicant.text),
            totalMonthlyIncome: double.tryParse(
              totalMonthlyIncomeMainApplicant
                  .toString()
                  .replaceAll(',', '')
                  .replaceAll('%', '')
                  .replaceAll('\$', '')
                  .replaceAll('%', ''),
            ),
            loanTenure: int.tryParse(
              resultMainApplicantStep2
                  .value.preferedTenureBasedAffordability!.loanTenure
                  .toString()
                  .replaceAll(',', '')
                  .replaceAll('%', '')
                  .replaceAll('\$', '')
                  .replaceAll('%', '')
                  .replaceAll('years', ''),
            ),
            totalFinancialCommitments: double.tryParse(
              totalFinancialCommitmentsMainApplicant
                  .toString()
                  .replaceAll(',', '')
                  .replaceAll('%', '')
                  .replaceAll('\$', '')
                  .replaceAll('%', ''),
            ),
            propertyType: propertyTypeValue + 1);

    var data = await CoreService().postWithAuth(
      url: baseUrl + newPurchaseGetunPledgeAmountApi,
      body: newPurchaseGetUnpledgeAmountSendModel.toJson(),
    );

    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
      if (Get.isDialogOpen ?? false) Get.back();
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          getUnpledgeAmountPlt();
        }
      } else {
        if (data["message"] == null) {
          unpledgeAmountPltCalculated.value = data["unfludge_amount"];
          // unpledgeAmountPltCalculated.value = double.tryParse(
          //     data["unfludge_amount"].toString().replaceAll(',', ''))!;
          unpledgeAmountPlt.value = unpledgeAmountPltCalculated
              .value; // Calculated value is assigned to the variable responsible to display in UI
          debugPrint(pledgeAmountPltCalculated.toString());
        } else {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: data["message"],
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }
      }
    }
  }

  onTapContinueToReportsCheck() {
    if (otherFeesControllerMainApplicant.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Other fees cannot be empty",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else {
      onTapContinueToReports();
    }
  }

  onTapContinueToReports() async {
    final Box hive = Hive.box(HiveString.hiveName);
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);

    NewPurchaseReportSendModel newPurchaseReportSendModel =
        NewPurchaseReportSendModel(
      numberOfLoanApplicants: leadDetails.length,
      name: nameControllerMainApplicant.text,
      propertyPrice: int.tryParse(
          AppConfig.getPlainAmount(propertyPriceControllerMainApplicant.text)),
      numberOfHousingLoan:
          selectedHousingLoanMainApplicant!.value == '2 or more'
              ? 2
              : int.tryParse(selectedHousingLoanMainApplicant!.value),
      ltv: double.tryParse(resultMainApplicantStep2.value.loanToValue
          .toString()
          .trim()
          .replaceAll('%', '')
          .replaceAll(',', '')),
      loanAmount: int.tryParse(
          AppConfig.getPlainAmount(loanAmountControllerMainApplicant.text)),
      buyerStampDuty: int.tryParse(resultMainApplicantStep2.value.buyerStampDuty
          .toString()
          .trim()
          .replaceAll('%', '')
          .replaceAll(',', '')),
      additionalBuyerStampDuty: int.tryParse(resultMainApplicantStep2
          .value.additionalBuyerStampDuty
          .toString()
          .trim()
          .replaceAll('%', '')
          .replaceAll(',', '')),
      legalFees: int.tryParse(resultMainApplicantStep2.value.legalFees
          .toString()
          .trim()
          .replaceAll('%', '')
          .replaceAll(',', '')),
      valuationFees: int.tryParse(resultMainApplicantStep2.value.valuationFees
          .toString()
          .trim()
          .replaceAll('%', '')
          .replaceAll(',', '')),
      otherFees: int.tryParse(otherFeesControllerMainApplicant.text),
      extendLoanTenure:
          selectedLongerLoanTenureMainApplicant.value.toString().toLowerCase(),
      maximumLoanTenure: int.tryParse(resultMainApplicantStep2
          .value.maximumLoanTenure
          .toString()
          .trim()
          .replaceAll('%', '')
          .replaceAll(',', '')
          .replaceAll('years', '')),
      maximumLoanQualifiedMtl: double.tryParse(resultMainApplicantStep2
          .value.maximumTenureBasedAffordability!.maximumQualifiedLoan
          .toString()
          .trim()
          .replaceAll('%', '')
          .replaceAll(',', '')),
      maximumPriceMlt: double.tryParse(resultMainApplicantStep2
          .value.maximumTenureBasedAffordability!.maximumPropertyPrice
          .toString()
          .trim()
          .replaceAll('%', '')
          .replaceAll(',', '')),
      msrMtl: double.tryParse(resultMainApplicantStep2
          .value.maximumTenureBasedAffordability!.msr
          .toString()
          .trim()
          .replaceAll('%', '')
          .replaceAll(',', '')),
      tdsrMtl: double.tryParse(resultMainApplicantStep2
          .value.maximumTenureBasedAffordability!.tdsr
          .toString()
          .trim()
          .replaceAll('%', '')
          .replaceAll(',', '')),
      fullPludgeAmountMlt: int.tryParse(resultMainApplicantStep2
          .value.maximumTenureBasedAffordability!.fullFludgeAmount
          .toString()
          .trim()
          .replaceAll('%', '')
          .replaceAll(',', '')),
      fullUnPludgeAmountMlt: int.tryParse(resultMainApplicantStep2
          .value.maximumTenureBasedAffordability!.fullUnFludgeAmount
          .toString()
          .trim()
          .replaceAll('%', '')
          .replaceAll(',', '')),
      preferPludgeMlt: int.tryParse(
          preferredPledgeAmountMltControllerMainApplicant
              .text), //From form field prefered pludge amount
      calculatiomnUnpludgeMlt:
          unfludgeAmountMainApplicant, //Calculation pledegd mlt container value
      preferUnpludgeMlt: int.tryParse(
          preferredUnpledgeAmountMltControllerMainApplicant
              .text), //From form field prefered unpledge amount
      calculationPludgeMlt:
          fludgeAmountMainApplicant, //Calculation unpledged mlt container value
      preferredLoanTenure: int.tryParse(loanTenureYearControllerMainApplicant
          .text), //form field preferred loan tenure
      maximumLoanQualifiedPlt: double.tryParse(
          resultMainApplicantStep2.value.preferedTenureBasedAffordability ==
                  null
              ? '0'
              : resultMainApplicantStep2
                  .value.preferedTenureBasedAffordability!.maximumQualifiedLoan
                  .toString()
                  .trim()
                  .replaceAll('%', '')
                  .replaceAll(',', '')), //maximum qualified loan plt value
      maximumPricePlt: double.tryParse(
          resultMainApplicantStep2.value.preferedTenureBasedAffordability ==
                  null
              ? '0'
              : resultMainApplicantStep2
                  .value.preferedTenureBasedAffordability!.maximumPropertyPrice
                  .toString()
                  .trim()
                  .replaceAll('%', '')
                  .replaceAll(',', '')), //maximum property price plt value
      msrPlt: double.tryParse(resultMainApplicantStep2
                  .value.preferedTenureBasedAffordability ==
              null
          ? '0'
          : resultMainApplicantStep2.value.preferedTenureBasedAffordability!.msr
              .toString()
              .trim()
              .replaceAll('%', '')
              .replaceAll(',', '')), //msr plt value
      tdsrPlt: double.tryParse(
          resultMainApplicantStep2.value.preferedTenureBasedAffordability ==
                  null
              ? '0'
              : resultMainApplicantStep2
                  .value.preferedTenureBasedAffordability!.tdsr
                  .toString()
                  .trim()
                  .replaceAll('%', '')
                  .replaceAll(',', '')), //tdsr plt value
      fullPludgeAmountPlt: int.tryParse(
          resultMainApplicantStep2.value.preferedTenureBasedAffordability ==
                  null
              ? '0'
              : resultMainApplicantStep2
                  .value.preferedTenureBasedAffordability!.fullFludgeAmount
                  .toString()
                  .trim()
                  .replaceAll('%', '')
                  .replaceAll(',', '')), //full pludge amount plt value
      fullUnPludgeAmountPlt: int.tryParse(
          resultMainApplicantStep2.value.preferedTenureBasedAffordability ==
                  null
              ? '0'
              : resultMainApplicantStep2
                  .value.preferedTenureBasedAffordability!.fullUnFludgeAmount
                  .toString()
                  .trim()
                  .replaceAll('%', '')
                  .replaceAll(',', '')), //full unpledge amount plt value
      preferPludgePlt: int.tryParse(
          preferredPledgeAmountPltControllerMainApplicant
              .text), //From form field prefered pludge amount plt value
      calculatiomnUnpludgePlt:
          unfludgeAmountMainApplicantPlt, //Calculation pledged plt container value
      preferUnpludgePlt: int.tryParse(
          preferredUnpledgeAmountPltControllerMainApplicant
              .text), //From form field prefered unpledge amount plt value
      calculationPludgePlt:
          fludgeAmountMainApplicantPlt, //Calculation unpledged plt container value
      monthlyInstallmentMlt: double.tryParse(resultMainApplicantStep2
          .value.maximumTenureBasedAffordability!.monthlyInstallment
          .toString()
          .trim()
          .replaceAll(',', '')),
      monthlyInstallmentPlt: double.tryParse(
          resultMainApplicantStep2.value.preferedTenureBasedAffordability ==
                  null
              ? '0'
              : resultMainApplicantStep2
                  .value.preferedTenureBasedAffordability!.monthlyInstallment
                  .toString()
                  .trim()
                  .replaceAll(',', '')),
      propertyType: propertyTypeForReportsAndPDF + 1,
      token: hive.get(HiveString.token.toString()),
    );
    var data = await CoreService().postWithAuth(
      url: baseUrl + newPurchaseReportApi,
      body: newPurchaseReportSendModel.toJson(),
    );

    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
      if (Get.isDialogOpen ?? false) Get.back();
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          onTapContinueToReports();
        }
      } else {
        resultMainApplicantStep3.value =
            NewPurchaseReportResponseModel.fromJson(data);
      }
    }
  }

  List exportFieldsCheckBoxesList = [];

  onTapDownloadReport() async {
    final Box hive = Hive.box(HiveString.hiveName);
    log(
      "Max loan mlt " +
          double.tryParse(
            resultMainApplicantStep2
                .value.maximumTenureBasedAffordability!.maximumQualifiedLoan
                .toString()
                .trim()
                .replaceAll('%', '')
                .replaceAll(',', ''),
          ).toString(),
    );
    log('Max Loan PLT');
    log(resultMainApplicantStep2
        .value.preferedTenureBasedAffordability!.maximumQualifiedLoan
        .toString()
        .trim()
        .replaceAll('%', '')
        .replaceAll(',', '')
        .replaceAll('\$', ''));
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);
    NewPurchaseReportPdfSendModel newPurchaseReportPdfSendModel =
        NewPurchaseReportPdfSendModel(
            numberOfLoanApplicants: leadDetails.length,
            name: nameControllerMainApplicant.text,
            propertyPrice: int.tryParse(AppConfig.getPlainAmount(
                propertyPriceControllerMainApplicant.text)),
            numberOfHousingLoan:
                int.tryParse(selectedHousingLoanMainApplicant!.value),
            ltv: double.tryParse(resultMainApplicantStep2.value.loanToValue
                .toString()
                .trim()
                .replaceAll('%', '')
                .replaceAll(',', '')),
            loanAmount: int.tryParse(AppConfig.getPlainAmount(
                loanAmountControllerMainApplicant.text)),
            buyerStampDuty: int.tryParse(resultMainApplicantStep2
                .value.buyerStampDuty
                .toString()
                .trim()
                .replaceAll('%', '')
                .replaceAll(',', '')),
            additionalBuyerStampDuty: int.tryParse(
                resultMainApplicantStep2.value.additionalBuyerStampDuty.toString().trim().replaceAll('%', '').replaceAll(',', '')),
            legalFees: int.tryParse(resultMainApplicantStep2.value.legalFees.toString().trim().replaceAll('%', '').replaceAll(',', '')),
            valuationFees: int.tryParse(resultMainApplicantStep2.value.valuationFees.toString().trim().replaceAll('%', '').replaceAll(',', '')),
            otherFees: int.tryParse(otherFeesControllerMainApplicant.text),
            extendLoanTenure: selectedLongerLoanTenureMainApplicant.value.toString().toLowerCase(),
            maximumLoanTenure: int.tryParse(resultMainApplicantStep2.value.maximumLoanTenure.toString().trim().replaceAll('%', '').replaceAll(',', '').replaceAll('years', '')),
            maximumLoanQualifiedMtl: double.tryParse(
              resultMainApplicantStep2
                  .value.maximumTenureBasedAffordability!.maximumQualifiedLoan
                  .toString()
                  .trim()
                  .replaceAll('%', '')
                  .replaceAll(',', '')
                  .toString(),
            ),
            maximumPriceMlt: double.tryParse(
              resultMainApplicantStep2
                  .value.maximumTenureBasedAffordability!.maximumPropertyPrice
                  .toString()
                  .trim()
                  .replaceAll('%', '')
                  .replaceAll(',', '')
                  .toString(),
            ),
            msrMtl: double.tryParse(resultMainApplicantStep2.value.maximumTenureBasedAffordability!.msr.toString().trim().replaceAll('%', '').replaceAll(',', '')),
            tdsrMtl: double.tryParse(resultMainApplicantStep2.value.maximumTenureBasedAffordability!.tdsr.toString().trim().replaceAll('%', '').replaceAll(',', '')),
            fullPludgeAmountMlt: int.tryParse(resultMainApplicantStep2.value.maximumTenureBasedAffordability!.fullFludgeAmount.toString().trim().replaceAll('%', '').replaceAll(',', '')),
            fullUnPludgeAmountMlt: int.tryParse(resultMainApplicantStep2.value.maximumTenureBasedAffordability!.fullUnFludgeAmount.toString().trim().replaceAll('%', '').replaceAll(',', '')),
            preferPludgeMlt: int.tryParse(preferredPledgeAmountMltControllerMainApplicant.text), //From form field prefered pludge amount
            calculatiomnUnpludgeMlt: unfludgeAmountMainApplicant, //Calculation pledegd mlt container value
            preferUnpludgeMlt: int.tryParse(preferredUnpledgeAmountMltControllerMainApplicant.text), //From form field prefered unpledge amount
            calculationPludgeMlt: fludgeAmountMainApplicant, //Calculation unpledged mlt container value
            preferredLoanTenure: int.tryParse(loanTenureYearControllerMainApplicant.text), //form field preferred loan tenure
            maximumLoanQualifiedPlt: double.tryParse(
              resultMainApplicantStep2.value.preferedTenureBasedAffordability ==
                      null
                  ? '0'
                  : resultMainApplicantStep2.value
                      .preferedTenureBasedAffordability!.maximumQualifiedLoan
                      .toString()
                      .trim()
                      .replaceAll('%', '')
                      .replaceAll(',', '')
                      .replaceAll('\$', ''),
            ), //maximum qualified loan plt value
            maximumPricePlt: double.tryParse(
              resultMainApplicantStep2.value.preferedTenureBasedAffordability ==
                      null
                  ? '0'
                  : resultMainApplicantStep2.value
                      .preferedTenureBasedAffordability!.maximumPropertyPrice
                      .toString()
                      .trim()
                      .replaceAll('%', '')
                      .replaceAll(',', '')
                      .replaceAll('\$', ''),
            ), //maximum property price plt value
            msrPlt: double.tryParse(resultMainApplicantStep2.value.preferedTenureBasedAffordability == null ? '0' : resultMainApplicantStep2.value.preferedTenureBasedAffordability!.msr.toString().trim().replaceAll('%', '').replaceAll(',', '')), //msr plt value
            tdsrPlt: double.tryParse(resultMainApplicantStep2.value.preferedTenureBasedAffordability == null ? '0' : resultMainApplicantStep2.value.preferedTenureBasedAffordability!.tdsr.toString().trim().replaceAll('%', '').replaceAll(',', '')), //tdsr plt value
            fullPludgeAmountPlt: int.tryParse(resultMainApplicantStep2.value.preferedTenureBasedAffordability == null ? '0' : resultMainApplicantStep2.value.preferedTenureBasedAffordability!.fullFludgeAmount.toString().trim().replaceAll('%', '').replaceAll(',', '')), //full pludge amount plt value
            fullUnPludgeAmountPlt: int.tryParse(resultMainApplicantStep2.value.preferedTenureBasedAffordability == null ? '0' : resultMainApplicantStep2.value.preferedTenureBasedAffordability!.fullUnFludgeAmount.toString().trim().replaceAll('%', '').replaceAll(',', '')), //full unpledge amount plt value
            preferPludgePlt: int.tryParse(preferredPledgeAmountPltControllerMainApplicant.text), //From form field prefered pludge amount plt value
            calculatiomnUnpludgePlt: unfludgeAmountMainApplicantPlt, //Calculation pledged plt container value
            preferUnpludgePlt: int.tryParse(preferredUnpledgeAmountPltControllerMainApplicant.text), //From form field prefered unpledge amount plt value
            calculationPludgePlt: fludgeAmountMainApplicantPlt, //Calculation unpledged plt container value
            token: hive.get(HiveString.token.toString()),
            exportFields: exportFieldsCheckBoxesList,
            monthlyInstallmentMlt: double.tryParse(resultMainApplicantStep2.value.maximumTenureBasedAffordability == null ? '0' : resultMainApplicantStep2.value.maximumTenureBasedAffordability!.monthlyInstallment.toString().trim().replaceAll(',', '')),
            monthlyInstallmentPlt: double.tryParse(resultMainApplicantStep2.value.preferedTenureBasedAffordability == null ? '0' : resultMainApplicantStep2.value.preferedTenureBasedAffordability!.monthlyInstallment.toString().trim().replaceAll(',', '')),
            propertyType: propertyTypeForReportsAndPDF + 1);
    //! Header model used to pass the pass the acess token we are accessing this here cause we are explicitly using the http package here instead of the coreService
    log(jsonEncode(newPurchaseReportPdfSendModel).toString());
    HeaderModel headerModel =
        HeaderModel(authorization: "Bearer ${hive.get(HiveString.token)}");
// ! Using http cause of the Timout issue which is not working with the getConnect that comes with Getx
    http.Response data = await http
        .post(Uri.parse(baseUrl + newPurchaseExportReportApi),
            body: jsonEncode(newPurchaseReportPdfSendModel.toJson()),
            headers: headerModel.toHeader())
        .timeout(const Duration(seconds: 200));
    log(data.body.toString());
    if (Get.isDialogOpen ?? false) Get.back();
    if (data.body.isEmpty) {
      if (Get.isDialogOpen ?? false) Get.back();
    } else {
      if (data.body.contains('message')) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: data.body.contains('message')
                ? jsonDecode(data.body)['message']
                : "Something went wrong",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      } else {
        resultMainApplicantStep4.value =
            NewPurchaseReportPdfResponseModel.fromJson(jsonDecode(data.body));

        onDownloadPdfButtonTap(
          resultMainApplicantStep4.value.url.toString(),
          ("${resultMainApplicantStep4.value.url}.pdf"),
        );
        if (Get.isDialogOpen ?? false) Get.back();
      }
      if (Get.isDialogOpen ?? false) Get.back();
    }
  }

  void getPermission() async {
    await Permission.storage.request();
  }

  onDownloadPdfButtonTap(String url, String filename) {
    openFile(
      //url: invoices.value.invoicePdf,
      // url: invoices.value.invoicePdf.toString(),
      // fileName: "Invoice ${invoices.value.number}.pdf",
      url: url,
      fileName: filename,
    );
  }

  Future openFile({String? url, String? fileName}) async {
    debugPrint('url' + url!);

    final file = await downloadFile(url, fileName!);
    if (file == null) return;

    debugPrint('Path: ${file.path}');

    OpenFilex.open(file.path);
  }

  // ignore: body_might_complete_normally_nullable
  Future<File?> downloadFile(String url, String name) async {
    Future.delayed(const Duration(milliseconds: 0), () {
      FocusManager.instance.primaryFocus!.unfocus();
      Fluttertoast.showToast(
        timeInSecForIosWeb: 3,
        msg: "Downloading",
        backgroundColor: Colors.green,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        textColor: Colors.white,
        fontSize: 20.0,
      );
    });

    final taskId = await FlutterDownloader.enqueue(
        url: url,
        savedDir: _localPath,
        showNotification:
            true, // show download progress in status bar (for Android)
        openFileFromNotification:
            true, // click on notification to open downloaded file (for Android)
        saveInPublicStorage: true);

    Get.snackbar("Downloaded", "Download completed",
        backgroundColor: Colors.green,
        colorText: Colors.white,
        mainButton: TextButton(
            onPressed: () async {
              debugPrint(">>>>>>>>");
              await FlutterDownloader.open(taskId: taskId!);
            },
            child: const Text(
              "Open",
              style: TextStyle(color: Colors.white),
            )),
        duration: const Duration(seconds: 7));
  }

  late String _localPath;

  Future<void> _prepareSaveDir() async {
    _localPath = (await _findLocalPath())!;
    final savedDir = Directory(_localPath);
    bool hasExisted = await savedDir.exists();
    if (!hasExisted) {
      savedDir.create();
    }
  }

  Future<String?> _findLocalPath() async {
    String? externalStorageDirPath;
    if (Platform.isAndroid) {
      try {
        externalStorageDirPath = await AndroidPathProvider.downloadsPath;
      } catch (e) {
        final directory = await getExternalStorageDirectory();
        externalStorageDirPath = directory?.path;
      }
    } else if (Platform.isIOS) {
      externalStorageDirPath =
          (await getApplicationDocumentsDirectory()).absolute.path;
    }
    return externalStorageDirPath;
  }

  static void downloadCallback(
      String id, DownloadTaskStatus status, int progress) {
    if (kDebugMode) {
      print(
          'Background Isolate Callback: task ($id) is in status ($status) and process ($progress)');
    }

    final SendPort send =
        IsolateNameServer.lookupPortByName('downloader_send_port')!;
    send.send([id, status, progress]);
  }

  onTapMltLoanPackage() {
    Get.toNamed(newPurchaseExistingLeadMltLoanPackage);
  }

  RxInt tableDataLengthMortgageRepaymentTableMltLowestFloating = 0.obs;
  var yearOneToFiveList = [];
  var yearSixToTenList = [];
  var yearElevenToFifteenList = [];
  var sixteenToTwentyList = [];
  var twentyOneToTwnetyFiveList = [];
  var twentySixToThirtyList = [];
  var thirtyOneToThirtyFiveList = [];

  onTapMltMortgageRepaymentTable() {
    titleForMortgageTableScreen.value =
        'Lowest Floating Based On Maximum Loan Tenure';
    yearOneToFiveList.clear();
    yearSixToTenList.clear();
    yearElevenToFifteenList.clear();
    sixteenToTwentyList.clear();
    twentyOneToTwnetyFiveList.clear();
    twentySixToThirtyList.clear();
    thirtyOneToThirtyFiveList.clear();
    Get.toNamed(newPurchaseExistingLeadMltMortgageRepayment);
    mltMortgageRepaymentTableViewDataLowestFloating();
  }

  mltMortgageRepaymentTableViewDataLowestFloating() {
    tableDataLengthMortgageRepaymentTableMltLowestFloating.value =
        resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
            .lowestFloating!.mortgageRepaymentTable!.length;
    for (int i = 0;
        i < tableDataLengthMortgageRepaymentTableMltLowestFloating.value;
        i++) {
      if (resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              1 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              2 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              3 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              4 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              5) {
        yearOneToFiveList.add(resultMainApplicantStep3
            .value
            .loanPackageMaximumLoanTenureBased!
            .lowestFloating!
            .mortgageRepaymentTable![i]);
      } else if (resultMainApplicantStep3
                  .value
                  .loanPackageMaximumLoanTenureBased!
                  .lowestFloating!
                  .mortgageRepaymentTable![i]
                  .year ==
              6 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              7 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              8 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              9 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              10) {
        yearSixToTenList.add(resultMainApplicantStep3
            .value
            .loanPackageMaximumLoanTenureBased!
            .lowestFloating!
            .mortgageRepaymentTable![i]);
      } else if (resultMainApplicantStep3
                  .value
                  .loanPackageMaximumLoanTenureBased!
                  .lowestFloating!
                  .mortgageRepaymentTable![i]
                  .year ==
              11 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              12 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              13 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              14 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              15) {
        yearElevenToFifteenList.add(resultMainApplicantStep3
            .value
            .loanPackageMaximumLoanTenureBased!
            .lowestFloating!
            .mortgageRepaymentTable![i]);
      } else if (resultMainApplicantStep3
                  .value
                  .loanPackageMaximumLoanTenureBased!
                  .lowestFloating!
                  .mortgageRepaymentTable![i]
                  .year ==
              16 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              17 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              18 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              19 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              20) {
        sixteenToTwentyList.add(resultMainApplicantStep3
            .value
            .loanPackageMaximumLoanTenureBased!
            .lowestFloating!
            .mortgageRepaymentTable![i]);
      } else if (resultMainApplicantStep3
                  .value
                  .loanPackageMaximumLoanTenureBased!
                  .lowestFloating!
                  .mortgageRepaymentTable![i]
                  .year ==
              21 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              22 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              23 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              24 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              25) {
        twentyOneToTwnetyFiveList.add(resultMainApplicantStep3
            .value
            .loanPackageMaximumLoanTenureBased!
            .lowestFloating!
            .mortgageRepaymentTable![i]);
      } else if (resultMainApplicantStep3
                  .value
                  .loanPackageMaximumLoanTenureBased!
                  .lowestFloating!
                  .mortgageRepaymentTable![i]
                  .year ==
              26 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              27 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              28 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              29 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              30) {
        twentySixToThirtyList.add(resultMainApplicantStep3
            .value
            .loanPackageMaximumLoanTenureBased!
            .lowestFloating!
            .mortgageRepaymentTable![i]);
      } else if (resultMainApplicantStep3
                  .value
                  .loanPackageMaximumLoanTenureBased!
                  .lowestFloating!
                  .mortgageRepaymentTable![i]
                  .year ==
              31 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              32 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              33 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              34 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              35) {
        thirtyOneToThirtyFiveList.add(resultMainApplicantStep3
            .value
            .loanPackageMaximumLoanTenureBased!
            .lowestFloating!
            .mortgageRepaymentTable![i]);
      }
    }
  }

  onTapMltMortgageRepaymentTableLowestFixed() {
    titleForMortgageTableScreen.value =
        'Lowest Fixed Based On Maximum Loan Tenure';
    yearOneToFiveList.clear();
    yearSixToTenList.clear();
    yearElevenToFifteenList.clear();
    sixteenToTwentyList.clear();
    twentyOneToTwnetyFiveList.clear();
    twentySixToThirtyList.clear();
    thirtyOneToThirtyFiveList.clear();
    Get.toNamed(newPurchaseExistingLeadMltMortgageRepayment);
    mltMortgageRepaymentTableViewDataLowestFixed();
  }

  mltMortgageRepaymentTableViewDataLowestFixed() {
    tableDataLengthMortgageRepaymentTableMltLowestFloating.value =
        resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
            .lowestFixed!.mortgageRepaymentTable!.length;
    for (int i = 0;
        i < tableDataLengthMortgageRepaymentTableMltLowestFloating.value;
        i++) {
      if (resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              1 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              2 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              3 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              4 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              5) {
        yearOneToFiveList.add(resultMainApplicantStep3
            .value
            .loanPackageMaximumLoanTenureBased!
            .lowestFixed!
            .mortgageRepaymentTable![i]);
      } else if (resultMainApplicantStep3
                  .value
                  .loanPackageMaximumLoanTenureBased!
                  .lowestFixed!
                  .mortgageRepaymentTable![i]
                  .year ==
              6 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              7 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              8 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              9 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              10) {
        yearSixToTenList.add(resultMainApplicantStep3
            .value
            .loanPackageMaximumLoanTenureBased!
            .lowestFixed!
            .mortgageRepaymentTable![i]);
      } else if (resultMainApplicantStep3
                  .value
                  .loanPackageMaximumLoanTenureBased!
                  .lowestFixed!
                  .mortgageRepaymentTable![i]
                  .year ==
              11 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              12 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              13 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              14 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              15) {
        yearElevenToFifteenList.add(resultMainApplicantStep3
            .value
            .loanPackageMaximumLoanTenureBased!
            .lowestFixed!
            .mortgageRepaymentTable![i]);
      } else if (resultMainApplicantStep3
                  .value
                  .loanPackageMaximumLoanTenureBased!
                  .lowestFixed!
                  .mortgageRepaymentTable![i]
                  .year ==
              16 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              17 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              18 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              19 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              20) {
        sixteenToTwentyList.add(resultMainApplicantStep3
            .value
            .loanPackageMaximumLoanTenureBased!
            .lowestFixed!
            .mortgageRepaymentTable![i]);
      } else if (resultMainApplicantStep3
                  .value
                  .loanPackageMaximumLoanTenureBased!
                  .lowestFixed!
                  .mortgageRepaymentTable![i]
                  .year ==
              21 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              22 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              23 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              24 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              25) {
        twentyOneToTwnetyFiveList.add(resultMainApplicantStep3
            .value
            .loanPackageMaximumLoanTenureBased!
            .lowestFixed!
            .mortgageRepaymentTable![i]);
      } else if (resultMainApplicantStep3
                  .value
                  .loanPackageMaximumLoanTenureBased!
                  .lowestFixed!
                  .mortgageRepaymentTable![i]
                  .year ==
              26 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              27 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              28 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              29 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              30) {
        twentySixToThirtyList.add(resultMainApplicantStep3
            .value
            .loanPackageMaximumLoanTenureBased!
            .lowestFixed!
            .mortgageRepaymentTable![i]);
      } else if (resultMainApplicantStep3
                  .value
                  .loanPackageMaximumLoanTenureBased!
                  .lowestFixed!
                  .mortgageRepaymentTable![i]
                  .year ==
              31 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              32 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              33 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              34 ||
          resultMainApplicantStep3.value.loanPackageMaximumLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              35) {
        thirtyOneToThirtyFiveList.add(resultMainApplicantStep3
            .value
            .loanPackageMaximumLoanTenureBased!
            .lowestFixed!
            .mortgageRepaymentTable![i]);
      }
    }
  }

  onTapPltLoanPackage() {
    Get.toNamed(newPurchaseExistingLeadPltLoanPackage);
  }

  RxInt tableDataLengthMortgageRepaymentTablePltLowestFloating = 0.obs;
  var yearOneToFiveListPlt = [];
  var yearSixToTenListPlt = [];
  var yearElevenToFifteenListPlt = [];
  var sixteenToTwentyListPlt = [];
  var twentyOneToTwnetyFiveListPlt = [];
  var twentySixToThirtyListPlt = [];
  var thirtyOneToThirtyFiveListPlt = [];

  onTapPltMortgageRepaymentTable() {
    titleForMortgageTableScreen.value =
        'Lowest Floating Based On Preferred Loan Tenure';
    yearOneToFiveListPlt.clear();
    yearSixToTenListPlt.clear();
    yearElevenToFifteenListPlt.clear();
    sixteenToTwentyListPlt.clear();
    twentyOneToTwnetyFiveListPlt.clear();
    twentySixToThirtyListPlt.clear();
    thirtyOneToThirtyFiveListPlt.clear();
    Get.toNamed(newPurchaseExistingLeadPltMortgageRepayment);
    pltMortgageRepaymentTableViewData();
  }

  pltMortgageRepaymentTableViewData() {
    tableDataLengthMortgageRepaymentTablePltLowestFloating.value =
        resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
            .lowestFloating!.mortgageRepaymentTable!.length;
    for (int i = 0;
        i < tableDataLengthMortgageRepaymentTablePltLowestFloating.value;
        i++) {
      if (resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              1 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              2 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              3 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              4 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              5) {
        yearOneToFiveListPlt.add(resultMainApplicantStep3
            .value
            .loanPackagePreferredLoanTenureBased!
            .lowestFloating!
            .mortgageRepaymentTable![i]);
      } else if (resultMainApplicantStep3
                  .value
                  .loanPackagePreferredLoanTenureBased!
                  .lowestFloating!
                  .mortgageRepaymentTable![i]
                  .year ==
              6 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              7 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              8 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              9 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              10) {
        yearSixToTenListPlt.add(resultMainApplicantStep3
            .value
            .loanPackagePreferredLoanTenureBased!
            .lowestFloating!
            .mortgageRepaymentTable![i]);
      } else if (resultMainApplicantStep3
                  .value
                  .loanPackagePreferredLoanTenureBased!
                  .lowestFloating!
                  .mortgageRepaymentTable![i]
                  .year ==
              11 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              12 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              13 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              14 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              15) {
        yearElevenToFifteenListPlt.add(resultMainApplicantStep3
            .value
            .loanPackagePreferredLoanTenureBased!
            .lowestFloating!
            .mortgageRepaymentTable![i]);
      } else if (resultMainApplicantStep3
                  .value
                  .loanPackagePreferredLoanTenureBased!
                  .lowestFloating!
                  .mortgageRepaymentTable![i]
                  .year ==
              16 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              17 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              18 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              19 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              20) {
        sixteenToTwentyListPlt.add(resultMainApplicantStep3
            .value
            .loanPackagePreferredLoanTenureBased!
            .lowestFloating!
            .mortgageRepaymentTable![i]);
      } else if (resultMainApplicantStep3
                  .value
                  .loanPackagePreferredLoanTenureBased!
                  .lowestFloating!
                  .mortgageRepaymentTable![i]
                  .year ==
              21 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              22 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              23 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              24 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              25) {
        twentyOneToTwnetyFiveListPlt.add(resultMainApplicantStep3
            .value
            .loanPackagePreferredLoanTenureBased!
            .lowestFloating!
            .mortgageRepaymentTable![i]);
      } else if (resultMainApplicantStep3
                  .value
                  .loanPackagePreferredLoanTenureBased!
                  .lowestFloating!
                  .mortgageRepaymentTable![i]
                  .year ==
              26 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              27 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              28 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              29 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              30) {
        twentySixToThirtyListPlt.add(resultMainApplicantStep3
            .value
            .loanPackagePreferredLoanTenureBased!
            .lowestFloating!
            .mortgageRepaymentTable![i]);
      } else if (resultMainApplicantStep3
                  .value
                  .loanPackagePreferredLoanTenureBased!
                  .lowestFloating!
                  .mortgageRepaymentTable![i]
                  .year ==
              31 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              32 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              33 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              34 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFloating!.mortgageRepaymentTable![i].year ==
              35) {
        thirtyOneToThirtyFiveListPlt.add(resultMainApplicantStep3
            .value
            .loanPackagePreferredLoanTenureBased!
            .lowestFloating!
            .mortgageRepaymentTable![i]);
      }
    }
  }

  onTapPltMortgageRepaymentTableLowestFixed() {
    titleForMortgageTableScreen.value =
        'Lowest Fixed Based On Preferred Loan Tenure';
    yearOneToFiveListPlt.clear();
    yearSixToTenListPlt.clear();
    yearElevenToFifteenListPlt.clear();
    sixteenToTwentyListPlt.clear();
    twentyOneToTwnetyFiveListPlt.clear();
    twentySixToThirtyListPlt.clear();
    thirtyOneToThirtyFiveListPlt.clear();
    Get.toNamed(newPurchaseExistingLeadPltMortgageRepayment);
    pltMortgageRepaymentTableViewDataLowestFixed();
  }

  pltMortgageRepaymentTableViewDataLowestFixed() {
    tableDataLengthMortgageRepaymentTablePltLowestFloating.value =
        resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
            .lowestFixed!.mortgageRepaymentTable!.length;
    for (int i = 0;
        i < tableDataLengthMortgageRepaymentTablePltLowestFloating.value;
        i++) {
      if (resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              1 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              2 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              3 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              4 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              5) {
        yearOneToFiveListPlt.add(resultMainApplicantStep3
            .value
            .loanPackagePreferredLoanTenureBased!
            .lowestFixed!
            .mortgageRepaymentTable![i]);
      } else if (resultMainApplicantStep3
                  .value
                  .loanPackagePreferredLoanTenureBased!
                  .lowestFixed!
                  .mortgageRepaymentTable![i]
                  .year ==
              6 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              7 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              8 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              9 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              10) {
        yearSixToTenListPlt.add(resultMainApplicantStep3
            .value
            .loanPackagePreferredLoanTenureBased!
            .lowestFixed!
            .mortgageRepaymentTable![i]);
      } else if (resultMainApplicantStep3
                  .value
                  .loanPackagePreferredLoanTenureBased!
                  .lowestFixed!
                  .mortgageRepaymentTable![i]
                  .year ==
              11 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              12 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              13 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              14 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              15) {
        yearElevenToFifteenListPlt.add(resultMainApplicantStep3
            .value
            .loanPackagePreferredLoanTenureBased!
            .lowestFixed!
            .mortgageRepaymentTable![i]);
      } else if (resultMainApplicantStep3
                  .value
                  .loanPackagePreferredLoanTenureBased!
                  .lowestFixed!
                  .mortgageRepaymentTable![i]
                  .year ==
              16 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              17 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              18 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              19 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              20) {
        sixteenToTwentyListPlt.add(resultMainApplicantStep3
            .value
            .loanPackagePreferredLoanTenureBased!
            .lowestFixed!
            .mortgageRepaymentTable![i]);
      } else if (resultMainApplicantStep3
                  .value
                  .loanPackagePreferredLoanTenureBased!
                  .lowestFixed!
                  .mortgageRepaymentTable![i]
                  .year ==
              21 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              22 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              23 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              24 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              25) {
        twentyOneToTwnetyFiveListPlt.add(resultMainApplicantStep3
            .value
            .loanPackagePreferredLoanTenureBased!
            .lowestFixed!
            .mortgageRepaymentTable![i]);
      } else if (resultMainApplicantStep3
                  .value
                  .loanPackagePreferredLoanTenureBased!
                  .lowestFixed!
                  .mortgageRepaymentTable![i]
                  .year ==
              26 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              27 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              28 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              29 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              30) {
        twentySixToThirtyListPlt.add(resultMainApplicantStep3
            .value
            .loanPackagePreferredLoanTenureBased!
            .lowestFixed!
            .mortgageRepaymentTable![i]);
      } else if (resultMainApplicantStep3
                  .value
                  .loanPackagePreferredLoanTenureBased!
                  .lowestFixed!
                  .mortgageRepaymentTable![i]
                  .year ==
              31 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              32 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              33 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              34 ||
          resultMainApplicantStep3.value.loanPackagePreferredLoanTenureBased!
                  .lowestFixed!.mortgageRepaymentTable![i].year ==
              35) {
        thirtyOneToThirtyFiveListPlt.add(resultMainApplicantStep3
            .value
            .loanPackagePreferredLoanTenureBased!
            .lowestFixed!
            .mortgageRepaymentTable![i]);
      }
    }
  }

  checkLTVvalueToHideShowLongerTenureLoanCondition(
      {required double loanAmount,
      required double propertyPrice,
      required int totalNumberOfHousingLoan}) {
    debugPrint('****************');
    debugPrint(totalNumberOfHousingLoan.toString());
    debugPrint('****************');
    ltvValueToShowAndHideTheLongerLoanTenureCondition =
        (loanAmount / propertyPrice) * 100; //! calculating the LTV percentage
    if (totalNumberOfHousingLoan == 0) {
      debugPrint('HAHAHAHAHAA');
      // var tempLtv = loanAmount / propertyPrice;
      // var tempLoanAmount = (tempLtv / 100) * propertyPrice;
      // debugPrint('**********');
      // debugPrint(ltvValueToShowAndHideTheLongerLoanTenureCondition.toString());
      // debugPrint('**********');
      // debugPrint(double.tryParse(ltvValueToShowAndHideTheLongerLoanTenureCondition
      //         .toStringAsFixed(9))
      //     .toString());
      // debugPrint(tempLtv.toString());
      // debugPrint(tempLoanAmount.toString());
      // debugPrint(loanAmount.toString());
      // if (tempLoanAmount <= loanAmount) {
      //   showLongerLoanTenureCondition.value = true;
      //   selectedLongerLoanTenureMainApplicant.value = 'No';
      //   loanTenureYearControllerMainApplicant.text = '0';
      // } else {
      //   showLongerLoanTenureCondition.value = false;
      //   selectedLongerLoanTenureMainApplicant.value = 'No';
      //   loanTenureYearControllerMainApplicant.text = '0';
      // }
      if (double.tryParse(ltvValueToShowAndHideTheLongerLoanTenureCondition
                  .toStringAsFixed(9))! <=
              55.0
          // &&selectedPropertyTypeMainApplicant == 'HDB'
          ) {
        showLongerLoanTenureCondition.value = true;
        selectedLongerLoanTenureMainApplicant.value = 'No';
        loanTenureYearControllerMainApplicant.text = '0';
      }
      // else if (ltvValueToShowAndHideTheLongerLoanTenureCondition
      //             .roundToDouble() <=
      //         55.0
      //     //  && selectedPropertyTypeMainApplicant ==
      //     // 'EC (From Developer/ within MOP)'
      //     ) {
      //   showLongerLoanTenureCondition.value = true;
      //   selectedLongerLoanTenureMainApplicant.value = 'No';
      //   loanTenureYearControllerMainApplicant.text = '0';
      // }
      // else if (selectedPropertyTypeMainApplicant != 'HDB' &&
      //     selectedPropertyTypeMainApplicant !=
      //         'EC (From Developer/ within MOP)') {
      //   showLongerLoanTenureCondition.value = true;
      // }
      else {
        showLongerLoanTenureCondition.value = false;
        selectedLongerLoanTenureMainApplicant.value = 'No';
        loanTenureYearControllerMainApplicant.text = '0';
      }
    } else if (totalNumberOfHousingLoan == 1) {
      debugPrint('HIHIHIHIHIHIH');

      if (double.tryParse(ltvValueToShowAndHideTheLongerLoanTenureCondition
                  .toStringAsFixed(9))! <=
              25.0
          // &&selectedPropertyTypeMainApplicant == 'HDB'
          ) {
        showLongerLoanTenureCondition.value = true;
        selectedLongerLoanTenureMainApplicant.value = 'No';
        loanTenureYearControllerMainApplicant.text = '0';
      }
      // else if (ltvValueToShowAndHideTheLongerLoanTenureCondition
      //             .roundToDouble() <=
      //         55.0
      //     //  && selectedPropertyTypeMainApplicant ==
      //     // 'EC (From Developer/ within MOP)'
      //     ) {
      //   showLongerLoanTenureCondition.value = true;
      //   selectedLongerLoanTenureMainApplicant.value = 'No';
      //   loanTenureYearControllerMainApplicant.text = '0';
      // }
      // else if (selectedPropertyTypeMainApplicant != 'HDB' &&
      //     selectedPropertyTypeMainApplicant !=
      //         'EC (From Developer/ within MOP)') {
      //   showLongerLoanTenureCondition.value = true;
      // }
      else {
        showLongerLoanTenureCondition.value = false;
        selectedLongerLoanTenureMainApplicant.value = 'No';
        loanTenureYearControllerMainApplicant.text = '0';
      }
    } else {
      debugPrint('HOOHOHOHOHOHOH');

      if (double.tryParse(ltvValueToShowAndHideTheLongerLoanTenureCondition
                  .toStringAsFixed(9))! <=
              15.0
          // &&selectedPropertyTypeMainApplicant == 'HDB'
          ) {
        showLongerLoanTenureCondition.value = true;
        selectedLongerLoanTenureMainApplicant.value = 'No';
        loanTenureYearControllerMainApplicant.text = '0';
      }
      // else if (ltvValueToShowAndHideTheLongerLoanTenureCondition
      //             .roundToDouble() <=
      //         55.0
      //     //  && selectedPropertyTypeMainApplicant ==
      //     // 'EC (From Developer/ within MOP)'
      //     ) {
      //   showLongerLoanTenureCondition.value = true;
      //   selectedLongerLoanTenureMainApplicant.value = 'No';
      //   loanTenureYearControllerMainApplicant.text = '0';
      // }
      // else if (selectedPropertyTypeMainApplicant != 'HDB' &&
      //     selectedPropertyTypeMainApplicant !=
      //         'EC (From Developer/ within MOP)') {
      //   showLongerLoanTenureCondition.value = true;
      // }
      else {
        showLongerLoanTenureCondition.value = false;
        selectedLongerLoanTenureMainApplicant.value = 'No';
        loanTenureYearControllerMainApplicant.text = '0';
      }
    }
  }

  int calculateTotalHousingLoan(String mainApplicant, String jointApplicant1,
      String jointApplicant2, String jointApplicant3) {
    int totalNumberOfHousingLoan = 0;
    totalNumberOfHousingLoan = int.tryParse(
            mainApplicant != '2 or more' ? mainApplicant : '2')! +
        int.tryParse(jointApplicant1 != '2 or more' ? jointApplicant1 : '2')! +
        int.tryParse(jointApplicant2 != '2 or more' ? jointApplicant2 : '2')! +
        int.tryParse(jointApplicant3 != '2 or more' ? jointApplicant3 : '2')!;
    debugPrint(totalNumberOfHousingLoan.toString());
    return totalNumberOfHousingLoan;
  }
}
