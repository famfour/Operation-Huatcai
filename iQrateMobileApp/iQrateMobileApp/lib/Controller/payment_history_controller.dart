import 'dart:async';

import 'dart:io';
import 'dart:isolate';
import 'dart:ui';

import 'package:android_path_provider/android_path_provider.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/profile_controller.dart';
import 'package:iqrate/Model/response_model.dart/get_invoices_response_model.dart';

import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';
import 'package:open_filex/open_filex.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

class PaymentHistoryController extends GetxController {
  Rx<GetInvoicesResponseModel> invoices = GetInvoicesResponseModel().obs;
  var invoicesData = GetInvoicesResponseModel().obs;
  late ProfileScreenController profileScreenController;
  RxList invoicesList = RxList();
  @override
  void onInit() {
    profileScreenController = Get.find();

    Timer(const Duration(seconds: 0), () {
      Get.isDialogOpen ?? true
          ? const Offstage()
          : Get.dialog(const Center(child: CircularProgressIndicator()),
              barrierDismissible: false);
      getPaymentHistory();
    });
    // getPaymentHistory();
    getPermission();

    _prepareSaveDir();

    FlutterDownloader.registerCallback(downloadCallback);

    super.onInit();
  }

  getPaymentHistory() async {
    var data = await CoreService().getWithAuth(
        url: baseUrl +
            getInvoicesUrl +
            profileScreenController.userData.value.stripeCustomerId.toString());
    if (Get.isDialogOpen ?? false) {
      Get.back();
    }
    if (data == null) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "No data",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          getPaymentHistory();
        }
      } else {
        for (int i = 0; i < data['data'].length; i++) {
          invoicesList.add(data['data'][i]);
        }
        var result = GetInvoicesResponseModel.fromJson(data['data'][0]);
        if (result.id != null) {
          invoices.value.id = result.id;
          invoices.value.amountPaid = result.amountPaid;
          invoices.value.amountRemaining = result.amountRemaining;
          invoices.value.number = result.number;
          invoices.value.status = result.status;
          invoices.value.paid = result.paid;
          invoices.value.invoicePdf = result.invoicePdf;
          invoices.value.paidAt = result.paidAt;
          invoices.refresh();
          invoicesData.refresh();
          invoicesList.refresh();
        } else {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Some error",
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }
      }
    }
  }

  void getPermission() async {
    await Permission.storage.request();
  }

  onDownloadPdfButtonTap(String url, String filename) {
    openFile(
      //url: invoices.value.invoicePdf,
      // url: invoices.value.invoicePdf.toString(),
      // fileName: "Invoice ${invoices.value.number}.pdf",
      url: url,
      fileName: filename,
    );
  }

  Future openFile({String? url, String? fileName}) async {
    debugPrint('url' + url!);

    final file = await downloadFile(url, fileName!);
    if (file == null) return;

    debugPrint('Path: ${file.path}');

    OpenFilex.open(file.path);
  }

  // ignore: body_might_complete_normally_nullable
  Future<File?> downloadFile(String url, String name) async {
    Future.delayed(const Duration(milliseconds: 0), () {
      FocusManager.instance.primaryFocus!.unfocus();
      Fluttertoast.showToast(
        timeInSecForIosWeb: 3,
        msg: "Downloading",
        backgroundColor: Colors.green,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        textColor: Colors.white,
        fontSize: 20.0,
      );
    });
    final taskId = await FlutterDownloader.enqueue(
        url: url,
        savedDir: _localPath,
        showNotification:
            true, // show download progress in status bar (for Android)
        openFileFromNotification:
            true, // click on notification to open downloaded file (for Android)
        saveInPublicStorage: true);

    Get.snackbar("Downloaded", "Download completed",
        backgroundColor: Colors.green,
        colorText: Colors.white,
        mainButton: TextButton(
            onPressed: () async {
              debugPrint(">>>>>>>>");
              await FlutterDownloader.open(taskId: taskId!);
            },
            child: const Text(
              "Open",
              style: TextStyle(color: Colors.white),
            )),
        duration: const Duration(seconds: 7));
  }

  late String _localPath;

  Future<void> _prepareSaveDir() async {
    _localPath = (await _findLocalPath())!;
    final savedDir = Directory(_localPath);
    bool hasExisted = await savedDir.exists();
    if (!hasExisted) {
      savedDir.create();
    }
  }

  Future<String?> _findLocalPath() async {
    String? externalStorageDirPath;
    if (Platform.isAndroid) {
      try {
        externalStorageDirPath = await AndroidPathProvider.downloadsPath;
      } catch (e) {
        final directory = await getExternalStorageDirectory();
        externalStorageDirPath = directory?.path;
      }
    } else if (Platform.isIOS) {
      externalStorageDirPath =
          (await getApplicationDocumentsDirectory()).absolute.path;
    }
    return externalStorageDirPath;
  }

  static void downloadCallback(
      String id, DownloadTaskStatus status, int progress) {
    if (kDebugMode) {
      print(
          'Background Isolate Callback: task ($id) is in status ($status) and process ($progress)');
    }

    final SendPort send =
        IsolateNameServer.lookupPortByName('downloader_send_port')!;
    send.send([id, status, progress]);
  }
}
