// ignore_for_file: unused_local_variable

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Provider/network_provider.dart';
import 'package:iqrate/Service/GetControllers.dart';
import 'package:iqrate/Service/url.dart';
import 'package:iqrate/Views/calculator/your_preferred_interest_rate.dart';
import 'package:logger/logger.dart';

import '../Utils/text_formatter.dart';

class CalculatorMortgageRepaymentController extends GetxController {
  var nameController = TextEditingController();
  var loanAmountController = TextEditingController();
  var loanTenureController = TextEditingController();
  var arrayYourPreferredInterestRateWidget =
      <YourPreferredInterestRateWidget>[];

  // final Box hive = Hive.box(HiveString.hiveName);
  bool validateTextField() {
    var errorNotAllowZero = "0 is not acceptable";
    var notInRange = "1-100 acceptable";
    var notAllowEmpty = "Please fill all the fields";

    if (nameController.text.isEmpty) {
      GetControllers.shared.showAlertPopupOneButtonWithCallBack(
          content: "Please fill all the fields", callback: () {});
      return false;
    }
    if (loanAmountController.text.isEmpty) {
      GetControllers.shared.showAlertPopupOneButtonWithCallBack(
          content: "Please fill all the fields", callback: () {});
      return false;
    }

    if (loanTenureController.text.isEmpty) {
      GetControllers.shared.showAlertPopupOneButtonWithCallBack(
          content: "Please fill all the fields", callback: () {});
      return false;
    }

    if (double.parse(loanTenureController.text) < 1 ||
        double.parse(loanTenureController.text) > 30) {
      GetControllers.shared.showAlertPopupOneButtonWithCallBack(
          content: "Loan Tenure in 1 - 30 years", callback: () {});
      return false;
    }

    if (arrayYourPreferredInterestRateWidget[0]
        .year1TextController
        .text
        .isEmpty) {
      GetControllers.shared.showAlertPopupOneButtonWithCallBack(
          content: notAllowEmpty, callback: () {});
      return false;
    }

    if (double.parse(arrayYourPreferredInterestRateWidget[0]
                .year1TextController
                .text) <
            1 ||
        double.parse(arrayYourPreferredInterestRateWidget[0]
                .year1TextController
                .text) >
            100) {
      GetControllers.shared.showAlertPopupOneButtonWithCallBack(
          content: notAllowEmpty, callback: () {});
      return false;
    }

    if (arrayYourPreferredInterestRateWidget[0]
        .year2TextController
        .text
        .isEmpty) {
      GetControllers.shared.showAlertPopupOneButtonWithCallBack(
          content: notAllowEmpty, callback: () {});
      return false;
    }
    if (double.parse(arrayYourPreferredInterestRateWidget[0]
                .year2TextController
                .text) <
            1 ||
        double.parse(arrayYourPreferredInterestRateWidget[0]
                .year2TextController
                .text) >
            100) {
      GetControllers.shared.showAlertPopupOneButtonWithCallBack(
          content: notAllowEmpty, callback: () {});
      return false;
    }

    if (arrayYourPreferredInterestRateWidget[0]
        .year3TextController
        .text
        .isEmpty) {
      GetControllers.shared.showAlertPopupOneButtonWithCallBack(
          content: notAllowEmpty, callback: () {});
      return false;
    }

    if (double.parse(arrayYourPreferredInterestRateWidget[0]
                .year3TextController
                .text) <
            1 ||
        double.parse(arrayYourPreferredInterestRateWidget[0]
                .year3TextController
                .text) >
            100) {
      GetControllers.shared.showAlertPopupOneButtonWithCallBack(
          content: notAllowEmpty, callback: () {});
      return false;
    }

    if (arrayYourPreferredInterestRateWidget[0]
        .year4TextController
        .text
        .isEmpty) {
      GetControllers.shared.showAlertPopupOneButtonWithCallBack(
          content: notAllowEmpty, callback: () {});
      return false;
    }

    if (double.parse(arrayYourPreferredInterestRateWidget[0]
                .year4TextController
                .text) <
            1 ||
        double.parse(arrayYourPreferredInterestRateWidget[0]
                .year4TextController
                .text) >
            100) {
      GetControllers.shared.showAlertPopupOneButtonWithCallBack(
          content: notInRange, callback: () {});
      return false;
    }
    if (arrayYourPreferredInterestRateWidget[0]
        .year5TextController
        .text
        .isEmpty) {
      GetControllers.shared.showAlertPopupOneButtonWithCallBack(
          content: notAllowEmpty, callback: () {});
      return false;
    }

    if (double.parse(arrayYourPreferredInterestRateWidget[0]
                .year5TextController
                .text) <
            1 ||
        double.parse(arrayYourPreferredInterestRateWidget[0]
                .year5TextController
                .text) >
            100) {
      GetControllers.shared.showAlertPopupOneButtonWithCallBack(
          content: notInRange, callback: () {});
      return false;
    }
    if (arrayYourPreferredInterestRateWidget.length > 1) {
      if (arrayYourPreferredInterestRateWidget[1]
          .year1TextController
          .text
          .isEmpty) {
        GetControllers.shared.showAlertPopupOneButtonWithCallBack(
            content: notAllowEmpty, callback: () {});
        return false;
      }
      if (double.parse(arrayYourPreferredInterestRateWidget[1]
                  .year1TextController
                  .text) <
              1 ||
          double.parse(arrayYourPreferredInterestRateWidget[1]
                  .year1TextController
                  .text) >
              100) {
        GetControllers.shared.showAlertPopupOneButtonWithCallBack(
            content: notInRange, callback: () {});
        return false;
      }

      if (arrayYourPreferredInterestRateWidget[1]
          .year2TextController
          .text
          .isEmpty) {
        GetControllers.shared.showAlertPopupOneButtonWithCallBack(
            content: notAllowEmpty, callback: () {});
        return false;
      }
      if (double.parse(arrayYourPreferredInterestRateWidget[1]
                  .year2TextController
                  .text) <
              1 ||
          double.parse(arrayYourPreferredInterestRateWidget[1]
                  .year2TextController
                  .text) >
              100) {
        GetControllers.shared.showAlertPopupOneButtonWithCallBack(
            content: notInRange, callback: () {});
        return false;
      }
      if (arrayYourPreferredInterestRateWidget[1]
          .year3TextController
          .text
          .isEmpty) {
        GetControllers.shared.showAlertPopupOneButtonWithCallBack(
            content: notAllowEmpty, callback: () {});
        return false;
      }
      if (double.parse(arrayYourPreferredInterestRateWidget[1]
                  .year3TextController
                  .text) <
              1 ||
          double.parse(arrayYourPreferredInterestRateWidget[1]
                  .year3TextController
                  .text) >
              100) {
        GetControllers.shared.showAlertPopupOneButtonWithCallBack(
            content: notInRange, callback: () {});
        return false;
      }

      if (arrayYourPreferredInterestRateWidget[1]
          .year4TextController
          .text
          .isEmpty) {
        GetControllers.shared.showAlertPopupOneButtonWithCallBack(
            content: notAllowEmpty, callback: () {});
        return false;
      }
      if (double.parse(arrayYourPreferredInterestRateWidget[1]
                  .year4TextController
                  .text) <
              1 ||
          double.parse(arrayYourPreferredInterestRateWidget[1]
                  .year4TextController
                  .text) >
              100) {
        GetControllers.shared.showAlertPopupOneButtonWithCallBack(
            content: notInRange, callback: () {});
        return false;
      }

      if (arrayYourPreferredInterestRateWidget[1]
          .year5TextController
          .text
          .isEmpty) {
        GetControllers.shared.showAlertPopupOneButtonWithCallBack(
            content: notAllowEmpty, callback: () {});
        return false;
      }

      if (double.parse(arrayYourPreferredInterestRateWidget[1]
                  .year5TextController
                  .text) <
              1 ||
          double.parse(arrayYourPreferredInterestRateWidget[1]
                  .year5TextController
                  .text) >
              100) {
        GetControllers.shared.showAlertPopupOneButtonWithCallBack(
            content: notInRange, callback: () {});
        return false;
      }
    }

    return true;
  }

  callAPICalculatorMortgageRepayment(
      BuildContext context, Function(dynamic) callback) async {
    _callAPICalculatorMortgageRepayment(
        callback); // fucntion call API need check token!
  }

  _callAPICalculatorMortgageRepayment(Function(dynamic) callback) async {
    debugPrint("start callAPICalculatorMortgageRepayment");
    //check expire token
    //
    // await GetControllers.shared.checkRefreshToken();
    //clear data 1st
    GetControllers.shared.getMortgageRepaymentReportController().clearData();

    if (validateTextField() == false) return;
    GetControllers.shared.showLoading();
    Map<String, dynamic> bodyJson = {
      "token": GetControllers.shared.getToken(),
      "name": nameController.text,
      "loan_amount": double.parse(getAmountString(loanAmountController
          .text)), //getAmountString(loanAmountController.text),
      "prefered_rate1_year1":
          arrayYourPreferredInterestRateWidget[0].year1TextController.text,
      "prefered_rate1_year2":
          arrayYourPreferredInterestRateWidget[0].year2TextController.text,
      "prefered_rate1_year3":
          arrayYourPreferredInterestRateWidget[0].year3TextController.text,
      "prefered_rate1_year4":
          arrayYourPreferredInterestRateWidget[0].year4TextController.text,
      "prefered_rate1_year5":
          arrayYourPreferredInterestRateWidget[0].year5TextController.text,
      "prefered_rate2_year1": arrayYourPreferredInterestRateWidget.length == 2
          ? arrayYourPreferredInterestRateWidget[1].year1TextController.text
          : null,
      "prefered_rate2_year2": arrayYourPreferredInterestRateWidget.length == 2
          ? arrayYourPreferredInterestRateWidget[1].year2TextController.text
          : null,
      "prefered_rate2_year3": arrayYourPreferredInterestRateWidget.length == 2
          ? arrayYourPreferredInterestRateWidget[1].year3TextController.text
          : null,
      "prefered_rate2_year4": arrayYourPreferredInterestRateWidget.length == 2
          ? arrayYourPreferredInterestRateWidget[1].year4TextController.text
          : null,
      "prefered_rate2_year5": arrayYourPreferredInterestRateWidget.length == 2
          ? arrayYourPreferredInterestRateWidget[1].year5TextController.text
          : null,
      "loan_tenure": loanTenureController.text,
      "prefered_rate2_enable":
          arrayYourPreferredInterestRateWidget.length == 2 ? "yes" : "no"
    };
    Logger().d(bodyJson);
    var token = GetControllers.shared.getToken();
    var data = await NetworkProvider().callAPIPost(
        token: token,
        url: baseUrl + mortgageCalculatorUrl,
        data: bodyJson); //Send the otp to the server.
    GetControllers.shared.hideLoading();
    if (NetworkProvider().isSuccessAPI(data)) {
      debugPrint("✅ OK mortgageCalculatorUrlf");
      Logger().d(data);
      callback(data);
    } else {
      debugPrint("❌  mortgageCalculatorUrl");
      GetControllers.shared.showAlertPopupOneButtonWithCallBack(
          content: NetworkProvider().getErrorMessage(data), callback: () {});
    }
  }
  //
  // _callAPICalculatorMortgageRepayment(Function(dynamic) callback) async {
  //   debugPrint("start callAPICalculatorMortgageRepayment");
  //   //check expire token
  //   //
  //   // await GetControllers.shared.checkRefreshToken();
  //   //clear data 1st
  //   GetControllers.shared.getMortgageRepaymentReportController().clearData();
  //
  //   if (validateTextField() == false) return;
  //   GetControllers.shared.showLoading();
  //   Map<String, dynamic> bodyJson = {
  //     "token": GetControllers.shared.getToken(),
  //     "name": nameController.text,
  //     "loan_amount": double.parse(getAmountString(loanAmountController
  //         .text)), //getAmountString(loanAmountController.text),
  //     "prefered_rate1_year1":
  //     arrayYourPreferredInterestRateWidget[0].year1TextController.text,
  //     "prefered_rate1_year2":
  //     arrayYourPreferredInterestRateWidget[0].year2TextController.text,
  //     "prefered_rate1_year3":
  //     arrayYourPreferredInterestRateWidget[0].year3TextController.text,
  //     "prefered_rate1_year4":
  //     arrayYourPreferredInterestRateWidget[0].year4TextController.text,
  //     "prefered_rate1_year5":
  //     arrayYourPreferredInterestRateWidget[0].year5TextController.text,
  //     "prefered_rate2_year1": arrayYourPreferredInterestRateWidget.length == 2
  //         ? arrayYourPreferredInterestRateWidget[1].year1TextController.text
  //         : null,
  //     "prefered_rate2_year2": arrayYourPreferredInterestRateWidget.length == 2
  //         ? arrayYourPreferredInterestRateWidget[1].year2TextController.text
  //         : null,
  //     "prefered_rate2_year3": arrayYourPreferredInterestRateWidget.length == 2
  //         ? arrayYourPreferredInterestRateWidget[1].year3TextController.text
  //         : null,
  //     "prefered_rate2_year4": arrayYourPreferredInterestRateWidget.length == 2
  //         ? arrayYourPreferredInterestRateWidget[1].year4TextController.text
  //         : null,
  //     "prefered_rate2_year5": arrayYourPreferredInterestRateWidget.length == 2
  //         ? arrayYourPreferredInterestRateWidget[1].year5TextController.text
  //         : null,
  //     "loan_tenure": loanTenureController.text,
  //     // "prefered_loan_tenure": "4",
  //     // "existing_bank": "4",
  //     // "existing_bank_interest": "3.7",
  //     "prefered_rate2_enable":
  //     arrayYourPreferredInterestRateWidget.length == 2 ? "yes" : "no"
  //   };
  //   Logger().d(bodyJson);
  //
  //   var data = await CoreService().postWithAuth(
  //       url: baseUrl + mortgageCalculatorUrl,
  //       body: bodyJson); //Send the otp to the server.
  //   GetControllers.shared.hideLoading();
  //   // if (Get.isDialogOpen ?? false) Get.back();
  //   if (data == null) {
  //     // if (Get.isDialogOpen ?? false) Get.back();
  //     // Get.snackbar(
  //     //   StringUtils.error,
  //     //   "Something went wrong1!",
  //     //   snackPosition: SnackPosition.TOP,
  //     //   backgroundColor: Colors.red,
  //     //   colorText: Colors.white,
  //     //   duration: const Duration(seconds: 3),
  //     // );
  //
  //     GetControllers.shared.showAlertPopupOneButtonWithCallBack(
  //         content: "Something went wrong!", callback: () {});
  //   } else {
  //     debugPrint("✅ OK callAPICalculatorMortgageRepayment");
  //     Logger().d(data);
  //     if (data["statusCode"] == 400 || data["statusCode"] == 401) {
  //       // if (Get.isDialogOpen ?? false) Get.back();
  //       // Get.snackbar(
  //       //   StringUtils.error,
  //       //   // data["message"].first ?? "Something went wrong!",
  //       //   GetControllers.shared.getErrorMessage(data),
  //       //   snackPosition: SnackPosition.TOP,
  //       //   backgroundColor: Colors.red,
  //       //   colorText: Colors.white,
  //       //   duration: const Duration(seconds: 3),
  //       // );
  //       GetControllers.shared.showAlertPopupOneButtonWithCallBack(
  //           content: GetControllers.shared.getErrorMessage(data),
  //           callback: () {});
  //     } else {
  //       callback(data);
  //     }
  //   }
  // }

  // var isCheckLoanDetails = true;
  // var isInterestRateComparison = true;
  // var isMortgageRepayment1 = true;
  // var isMortgageRepayment2 = true;
  List<String> getExportField() {
    var a = <String>[];
    var b = GetControllers.shared.getMortgageRepaymentReportController();
    if (b.isCheckLoanDetails) {
      a.add("Loan Details");
    }
    if (b.isInterestRateComparison) {
      a.add("Interest Rate Comparison");
    }
    if (b.isMortgageRepayment1) {
      a.add("Mortgage Repayment 1");
    }
    if (b.isMortgageRepayment2) {
      a.add("Mortgage Repayment 2");
    }
    return a;
  }
  //
  // callAPIReportCalculatorMortgageRepayment(Function(dynamic) callback) async {
  //   debugPrint("start callAPIReportCalculatorMortgageRepayment");
  //   //Mortgage-Repayment-pdf "export_fields" = ["Loan Details","Interest Rate Comparison","Mortgage Repayment 1","Mortgage Repayment 2"]
  //   //"export_fields" = ["Loan Details","Rate Comparison","Disbursement Schedule","Progressive Repayment","Preferred Repayment"]
  //
  //   GetControllers.shared.showLoading();
  //   Map<String, dynamic> bodyJson = {
  //     "export_fields" : getExportField(),
  //     "token" : GetControllers.shared.getToken(),
  //     "name": nameController.text,
  //     "loan_amount": getAmountString(loanAmountController.text),
  //     "prefered_rate1_year1":
  //     arrayYourPreferredInterestRateWidget[0].year1TextController.text,
  //     "prefered_rate1_year2":
  //     arrayYourPreferredInterestRateWidget[0].year2TextController.text,
  //     "prefered_rate1_year3":
  //     arrayYourPreferredInterestRateWidget[0].year3TextController.text,
  //     "prefered_rate1_year4":
  //     arrayYourPreferredInterestRateWidget[0].year4TextController.text,
  //     "prefered_rate1_year5":
  //     arrayYourPreferredInterestRateWidget[0].year5TextController.text,
  //     "prefered_rate2_year1": arrayYourPreferredInterestRateWidget.length == 2
  //         ? arrayYourPreferredInterestRateWidget[1].year1TextController.text
  //         : null,
  //     "prefered_rate2_year2": arrayYourPreferredInterestRateWidget.length == 2
  //         ? arrayYourPreferredInterestRateWidget[1].year2TextController.text
  //         : null,
  //     "prefered_rate2_year3": arrayYourPreferredInterestRateWidget.length == 2
  //         ? arrayYourPreferredInterestRateWidget[1].year3TextController.text
  //         : null,
  //     "prefered_rate2_year4": arrayYourPreferredInterestRateWidget.length == 2
  //         ? arrayYourPreferredInterestRateWidget[1].year4TextController.text
  //         : null,
  //     "prefered_rate2_year5": arrayYourPreferredInterestRateWidget.length == 2
  //         ? arrayYourPreferredInterestRateWidget[1].year5TextController.text
  //         : null,
  //     "loan_tenure": loanTenureController.text,
  //     // "prefered_loan_tenure": "4",
  //     // "existing_bank": "4",
  //     // "existing_bank_interest": "3.7",
  //     "prefered_rate2_enable":
  //     arrayYourPreferredInterestRateWidget.length == 2 ? "yes" : "no"
  //   };
  //   Logger().d(bodyJson);
  //
  //   var data = await CoreService().postWithAuth(
  //       url: baseUrl + 'user/api/calculator/mortgage-repayment-pdf',
  //       body: bodyJson); //Send the otp to the server.
  //   GetControllers.shared.hideLoading();
  //   // if (Get.isDialogOpen ?? false) Get.back();
  //   if (data == null) {
  //     Get.snackbar(
  //       StringUtils.error,
  //       "Something went wrong!",
  //       snackPosition: SnackPosition.TOP,
  //       backgroundColor: Colors.red,
  //       colorText: Colors.white,
  //       duration: const Duration(seconds: 3),
  //     );
  //   } else {
  //     debugPrint("✅ OK callAPICalculatorMortgageRepayment");
  //     Logger().d(data);
  //     callback(data);
  //   }
  // }

  callAPIReportCalculatorMortgageRepayment(Function(dynamic) callback) async {
    if(getExportField().isEmpty){
      Fluttertoast.showToast(
        timeInSecForIosWeb: 3,
        msg: "Please select the option first then download report",
        backgroundColor: Colors.red,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        textColor: Colors.white,
        fontSize: 20.0,
      );
      return;
    }

    debugPrint("start callAPIReportCalculatorMortgageRepayment v2");
    //Mortgage-Repayment-pdf "export_fields" = ["Loan Details","Interest Rate Comparison","Mortgage Repayment 1","Mortgage Repayment 2"]
    //"export_fields" = ["Loan Details","Rate Comparison","Disbursement Schedule","Progressive Repayment","Preferred Repayment"]

    GetControllers.shared.showLoading();
    Map<String, dynamic> bodyJson = {
      "export_fields": getExportField(),
      "token": GetControllers.shared.getToken(),
      "name": nameController.text,
      "loan_amount": getAmountString(loanAmountController.text),
      "prefered_rate1_year1":
          arrayYourPreferredInterestRateWidget[0].year1TextController.text,
      "prefered_rate1_year2":
          arrayYourPreferredInterestRateWidget[0].year2TextController.text,
      "prefered_rate1_year3":
          arrayYourPreferredInterestRateWidget[0].year3TextController.text,
      "prefered_rate1_year4":
          arrayYourPreferredInterestRateWidget[0].year4TextController.text,
      "prefered_rate1_year5":
          arrayYourPreferredInterestRateWidget[0].year5TextController.text,
      "prefered_rate2_year1": arrayYourPreferredInterestRateWidget.length == 2
          ? arrayYourPreferredInterestRateWidget[1].year1TextController.text
          : null,
      "prefered_rate2_year2": arrayYourPreferredInterestRateWidget.length == 2
          ? arrayYourPreferredInterestRateWidget[1].year2TextController.text
          : null,
      "prefered_rate2_year3": arrayYourPreferredInterestRateWidget.length == 2
          ? arrayYourPreferredInterestRateWidget[1].year3TextController.text
          : null,
      "prefered_rate2_year4": arrayYourPreferredInterestRateWidget.length == 2
          ? arrayYourPreferredInterestRateWidget[1].year4TextController.text
          : null,
      "prefered_rate2_year5": arrayYourPreferredInterestRateWidget.length == 2
          ? arrayYourPreferredInterestRateWidget[1].year5TextController.text
          : null,
      "loan_tenure": loanTenureController.text,
      "prefered_rate2_enable":
          arrayYourPreferredInterestRateWidget.length == 2 ? "yes" : "no"
    };
    Logger().d(bodyJson);
    var token = GetControllers.shared.getToken();
    var data = await NetworkProvider().callAPIPost(
        token: token,
        url: baseUrl + 'user/api/calculator/mortgage-repayment-pdf',
        data: bodyJson); //Send the otp to the server.
    GetControllers.shared.hideLoading();
    if (NetworkProvider().isSuccessAPI(data)) {
      debugPrint("✅ OK user/api/calculator/mortgage-repayment-pdf");
      //Logger().d(data);
      callback(data);
    } else {
      debugPrint("❌  callAPICalculatorMortgageRepayment");
      GetControllers.shared.showAlertPopupOneButtonWithCallBack(
          content: NetworkProvider().getErrorMessage(data), callback: () {});
    }
  }
}
