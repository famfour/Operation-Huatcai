// ignore_for_file: unused_field, prefer_final_fields, unused_local_variable, non_constant_identifier_names, deprecated_member_use

import 'dart:async';
import 'dart:io';
import 'dart:isolate';
import 'dart:ui';

import 'package:android_path_provider/android_path_provider.dart';
import 'package:clipboard/clipboard.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:iqrate/Controller/leads_view_controller.dart';
import 'package:iqrate/Controller/profile_controller.dart';
import 'package:iqrate/DeviceManager/container_styles.dart';
import 'package:iqrate/DeviceManager/screen_constants.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Model/response_model.dart/documents_downlaod_response_model.dart';
import 'package:iqrate/Model/response_model.dart/documents_drawer_response_model.dart';
import 'package:iqrate/Model/response_model.dart/fetch_lead_list_response_model.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';
import 'package:iqrate/Widgets/form_field_title.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';
import 'package:iqrate/Widgets/secondary_button.dart';
import 'package:open_filex/open_filex.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:url_launcher/url_launcher.dart';

class MyDocumentsController extends GetxController {
  final ProfileScreenController profileScreenController =
      Get.put(ProfileScreenController());

  late TextEditingController mobileOtpController; // Controller for mobile otp
  late TextEditingController fileNameController; // Controller for mobile otp

  final LeadsViewController leadsViewController = Get.find();
  Result leadData = Get.arguments[
      2]; //Get the lead data passed in the argument in the previous screen

  var bankForms = <Documents>[].obs;
  var nRICs = <Documents>[].obs;
  var oTPOptiontoPurchase = <Documents>[].obs;
  var iRASs = <Documents>[].obs;
  var cPFs = <Documents>[].obs;
  var hDBs = <Documents>[].obs;
  var payslips = <Documents>[].obs;
  var valuationReports = <Documents>[].obs;
  var loanStatements = <Documents>[].obs;
  var tenancyAgreement = <Documents>[].obs;
  var others = <Documents>[].obs;
  var lOs = <Documents>[].obs;
  var calculatorReports = <Documents>[].obs;
  var fileName = "".obs;
  var fileSize = "".obs;

  @override
  void onInit() {
    mobileOtpController =
        TextEditingController(); // Initialize controller before the screen loads
    fileNameController =
        TextEditingController(); // Initialize controller before the screen loads

    _bindBackgroundIsolate();

    Timer(const Duration(milliseconds: 1000), () {
      //FlutterDownloader.registerCallback(downloadCallback);
    });

    prepareSaveDir();

    super.onInit();
  }

  late bool _isLoading;
  late bool _permissionReady;
  late String _localPath;
  ReceivePort _port = ReceivePort();

  void _bindBackgroundIsolate() {
    bool isSuccess = IsolateNameServer.registerPortWithName(
        _port.sendPort, 'downloader_send_port');
    if (!isSuccess) {
      _unbindBackgroundIsolate();
      _bindBackgroundIsolate();
      return;
    }
    _port.listen((dynamic data) {
      String? id = data[0];
      DownloadTaskStatus? status = data[1];
      int? progress = data[2];
    });
  }

  void _unbindBackgroundIsolate() {
    IsolateNameServer.removePortNameMapping('downloader_send_port');
  }

  onTapOTPSubmit() {
    //onTapUploadPopup();

    if (mobileOtpController.text.isEmpty) {
      //Check if the otp is empty
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter the OTP",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (mobileOtpController.text.length < 6) {
      //Check if the otp is less than 6 characters
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "OTP should be minimum 6 characters",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else {
      //If the otp is not empty and is 6 characters long call the onSubmit method which sends the OTP and checks if the OTP is correct and moves to the next screen
      debugPrint("Submit button tapped for phone Verification");
      mobileVerification();
    }
  }

  mobileVerification() async {
    Get.isDialogOpen ?? true
        ? const Offstage() //Used for avoiding the dialog to be open when the user is already on the next screen.
        : Get.dialog(
            const Center(
                child:
                    CircularProgressIndicator()), //Used for showing the loading dialog.
            barrierDismissible:
                false); //Used for preventing the user to dismiss the dialog.

    var data = await CoreService()
        .postWithAuth(url: baseUrl + drawerValidOtpUrl, body: {
      "drawer": leadsViewController.myDocumentID,
      "otp": mobileOtpController.text
      //"otp": "S86M9PZ"
    }); //Send the otp to the server.

    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          mobileVerification();
        }
      } else {
//Get.snackbar(StringUtils.success, result.message.toString(), snackPosition: SnackPosition.TOP, backgroundColor: Colors.white);
        leadsViewController.drawerToken = data["token"];
        Get.offAndToNamed(myDocumentsScreen);
      }
    }
  }

  sendOTP() async {
    Get.isDialogOpen ?? true
        ? const Offstage() //Used for avoiding the dialog to be open when the user is already on the next screen.
        : Get.dialog(
            const Center(
                child:
                    CircularProgressIndicator()), //Used for showing the loading dialog.
            barrierDismissible:
                false); //Used for preventing the user to dismiss the dialog.

    var data = await CoreService().postWithAuth(
        url: baseUrl + drawerGetOtpUrl,
        body: {
          "drawer": leadsViewController.myDocumentID
        }); //Send the otp to the server.

    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          sendOTP();
        }
      } else {
        //Get.snackbar(StringUtils.success, result.message.toString(), snackPosition: SnackPosition.TOP, backgroundColor: Colors.white);
      }
    }
  }

  Future<void> onTapSendOTP() async {
    await sendOTP();
    Get.offAndToNamed(documentMobileVerification);
  }

  double windowHeightC = Get.height;
  double windowWidthC = Get.width;

  void onTapUploadPopup(doc_type) {
    debugPrint("onTapUploadPopup");
    fileNameController.clear();
    Get.defaultDialog(
      onWillPop: () async {
        return false;
      },
      barrierDismissible: false,
      title: "",
      content: SizedBox(
        height: windowHeightC * 0.55,
        width: windowWidthC * 0.9,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 0.0, horizontal: 18),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  children: [
                    Text(
                      "Add Documents",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: FontSize.s20,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: windowHeightC * 0.03),
                    const FormFieldTitle(title: 'File Name*'),
                    SizedBox(height: windowHeightC * 0.01),
                    RequireTextField(
                      type: Type.optional,
                      labelText: "Enter File Name",
                      controller: fileNameController,
                      key: const Key("File Name"),
                    ),
                  ],
                ),
                SizedBox(height: windowHeightC * 0.03),
                const FormFieldTitle(title: 'Attach Documents*'),
                SizedBox(height: windowHeightC * 0.01),
                Obx(() => InkWell(
                    onTap: () {
                      onTapAddAttachment();
                    },
                    child: images.value.path.isEmpty
                        ? Image.asset("assets/images/upload_document.png")
                        : Stack(
                            alignment: Alignment.center,
                            children: [
                              Image.asset("assets/images/upload_border.png"),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 7.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SizedBox(height: windowHeightC * 0.005),
                                    FormFieldTitle(title: fileName.value),
                                    SizedBox(height: windowHeightC * 0.005),
                                    Text(
                                      "(" + fileSize.value + ")",
                                      style: TextStyles
                                          .planTermSelectionPriceSavingTextStyle,
                                    ),
                                    SizedBox(height: windowHeightC * 0.002),
                                    Text(
                                      "Change",
                                      style: TextStyles.pdpaSendtitleStyle1,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ))),
                SizedBox(height: windowHeightC * 0.01),
                SizedBox(height: windowHeightC * 0.03),
                PrimaryButton(
                  windowHeight: windowHeightC,
                  windowWidth: windowWidthC,
                  buttonTitle: "Upload",
                  onPressed: () {
                    fileUpload(doc_type);
                  },
                ),
                SizedBox(height: windowHeightC * 0.03),
                SecondaryButton(
                  kGradientBoxDecoration:
                      ContainerStyles.kGradientBoxDecorationSecondaryButton,
                  kInnerDecoration:
                      ContainerStyles.kInnerDecorationSecondaryButton,
                  windowHeight: windowHeightC,
                  windowWidth: windowWidthC,
                  buttonTitle: "Cancel",
                  onPressed: () {
                    onPressBack();

                    Timer(const Duration(seconds: 1), () {
                      clearField();
                    });
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void onTapPopUpUpdate(Documents document) {
    debugPrint("=======onTapPopUpUpdate========");
    fileNameController.text = document.filename!;

    Get.defaultDialog(
      title: "",
      content: SizedBox(
        height: windowHeightC * 0.4,
        width: windowWidthC * 0.9,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 0.0, horizontal: 18),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  children: [
                    Text(
                      "Please enter a new name for this file",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: FontSize.s20,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: windowHeightC * 0.05),
                    RequireTextField(
                      type: Type.optional,
                      labelText: "File Name",
                      controller: fileNameController,
                      key: const Key("File Name"),
                    ),
                  ],
                ),
                SizedBox(height: windowHeightC * 0.05),
                PrimaryButton(
                  windowHeight: windowHeightC,
                  windowWidth: windowWidthC,
                  buttonTitle: "Save",
                  onPressed: () {
                    onTapUpdate(document, fileNameController.text.trim());
                  },
                ),
                SizedBox(height: windowHeightC * 0.03),
                SecondaryButton(
                  kGradientBoxDecoration:
                      ContainerStyles.kGradientBoxDecorationSecondaryButton,
                  kInnerDecoration:
                      ContainerStyles.kInnerDecorationSecondaryButton,
                  windowHeight: windowHeightC,
                  windowWidth: windowWidthC,
                  buttonTitle: "Cancel",
                  onPressed: () {
                    onPressBack();
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void onTapPopUpDelete(String? id) {
    debugPrint("=======popUpDelete========");

    Get.defaultDialog(
      title: "",
      content: SizedBox(
        height: windowHeightC * 0.28,
        width: windowWidthC * 0.9,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 0.0, horizontal: 18),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  children: [
                    Text(
                      "Are you sure you want to delete this document?",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: FontSize.s20,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: windowHeightC * 0.05),
                PrimaryButton(
                  windowHeight: windowHeightC,
                  windowWidth: windowWidthC,
                  buttonTitle: "Delete Now",
                  onPressed: () {
                    onPressBack();
                    onTapDeleteButton(id);
                  },
                ),
                SizedBox(height: windowHeightC * 0.03),
                SecondaryButton(
                  kGradientBoxDecoration:
                      ContainerStyles.kGradientBoxDecorationSecondaryButton,
                  kInnerDecoration:
                      ContainerStyles.kInnerDecorationSecondaryButton,
                  windowHeight: windowHeightC,
                  windowWidth: windowWidthC,
                  buttonTitle: "Cancel",
                  onPressed: () {
                    onPressBack();
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void onTapUpload() {
    debugPrint("onTapUpload");
  }

  Future<void> onTapUpdate(Documents document, String fileName) async {
    debugPrint("onTapUpdate");

    if (fileName.trim().isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter filename",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return;
    }

    Get.isDialogOpen ?? true
        ? const Offstage() //Used for avoiding the dialog to be open when the user is already on the next screen.
        : Get.dialog(
            const Center(
                child:
                    CircularProgressIndicator()), //Used for showing the loading dialog.
            barrierDismissible:
                false); //Used for preventing the user to dismiss the dialog.

    var data = await CoreService().putWithAuth(
        url: baseUrl + drawerUpdateFileUrl + document.id!,
        body: {"filename": fileName}); //Send the otp to the server.

    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          onTapUpdate(document, fileName);
        }
      } else {
        clearField();
        getDocuments();
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "File name successfully updated",
            backgroundColor: Colors.green,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      }
    }
  }

  Future<void> onTapDeleteButton(String? id) async {
    Timer(const Duration(seconds: 1), () {
      if (Get.isDialogOpen ?? false) Get.back();
    });

    debugPrint("onTapDeleteButton");

    Get.isDialogOpen ?? true
        ? const Offstage() //Used for avoiding the dialog to be open when the user is already on the next screen.
        : Get.dialog(
            const Center(
                child:
                    CircularProgressIndicator()), //Used for showing the loading dialog.
            barrierDismissible:
                false); //Used for preventing the user to dismiss the dialog.

    var data = await CoreService().deleteWithAuth(
        url: baseUrl + drawerDeleteFileUrl + id!); //Send the otp to the server.

    if (Get.isDialogOpen ?? false) Get.back();

    if (data == null) {
      getDocuments();

      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Successfully deleted",
          backgroundColor: Colors.green,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          onTapDeleteButton(id);
        }
      }
    }
  }

  Future<void> onTapDownloadButton(String id) async {
    debugPrint("onTapDownloadButton");

    Get.isDialogOpen ?? true
        ? const Offstage() //Used for avoiding the dialog to be open when the user is already on the next screen.
        : Get.dialog(
            const Center(
                child:
                    CircularProgressIndicator()), //Used for showing the loading dialog.
            barrierDismissible:
                false); //Used for preventing the user to dismiss the dialog.

    var data = await CoreService().getWithAuth(
        url:
            baseUrl + drawerDownloadFileUrl + id); //Send the otp to the server.

    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          onTapDownloadButton(id);
        }
      } else {
        var result = DocumentsDownlaodResponseModel.fromJson(data);
        getPermission();

        launchUrl(result.url!);
        //downloadFile(result.url!, result.filename!);

        if (Get.isDialogOpen ?? false) Get.back();
      }
    }
  }

  Future<void> share(String url) async {
    await FlutterShare.share(
        title: 'Document Upload',
        text: 'Use this link for document upload',
        linkUrl: url,
        chooserTitle: 'Document Upload');
  }

  void launchUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Invalid download link",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    }
  }

  Future getPermission() async {
    var status = await Permission.storage.status;
    if (status.isDenied) {
      debugPrint("=======storage denied=======");
      await Permission.storage.request();
      // We didn't ask for permission yet or the permission has been denied before but not permanently.
    }
  }

  onDownloadPdfButtonTap(String url) {
    openFile(
      url: url,
      fileName: "Invoice ${url.split('/').last}.pdf",
    );
  }

  Future openFile({String? url, String? fileName}) async {
    debugPrint('url' + url!);

    final file = await downloadFile(url, fileName!);
    if (file == null) return;

    debugPrint('Path: ${file.path}');

    OpenFilex.open(file.path);
  }

  // ignore: body_might_complete_normally_nullable
  Future<File?> downloadFile(String url, String name) async {
    Future.delayed(const Duration(milliseconds: 0), () {
      FocusManager.instance.primaryFocus!.unfocus();
      Fluttertoast.showToast(
        timeInSecForIosWeb: 3,
        msg: "Downlaod Started",
        backgroundColor: Colors.green,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        textColor: Colors.white,
        fontSize: 20.0,
      );
    });

    final taskId = await FlutterDownloader.enqueue(
        url: url,
        savedDir: _localPath,
        showNotification: true,
        // show download progress in status bar (for Android)
        openFileFromNotification: true,
        // click on notification to open downloaded file (for Android)
        saveInPublicStorage: false);

    Get.snackbar("Downloaded", "Download completed",
        mainButton: TextButton(
            onPressed: () async {
              debugPrint(">>>>>>>>");
              await FlutterDownloader.open(taskId: taskId!);
            },
            child: const Text("Open")),
        duration: const Duration(seconds: 7));
  }

  Future<void> getDocuments() async {
    debugPrint("onTapDownloadButton");

    var data = await CoreService().getWithAuth(
        url: baseUrl +
            drawerLoginWithOTPUrl +
            leadsViewController.myDocumentID); //Send the otp to the server.

    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          getDocuments();
        }
      } else {
        DocumentsDrawerResponseModel results =
            DocumentsDrawerResponseModel.fromJson(data);

        setData(results);

        //Get.snackbar(StringUtils.success, "Documents fetch successfully", snackPosition: SnackPosition.TOP, backgroundColor: Colors.white);
      }
    }
  }

  setData(DocumentsDrawerResponseModel results) {
    bankForms.clear();
    nRICs.clear();
    oTPOptiontoPurchase.clear();
    iRASs.clear();
    cPFs.clear();
    hDBs.clear();
    payslips.clear();
    valuationReports.clear();
    loanStatements.clear();
    tenancyAgreement.clear();
    others.clear();
    lOs.clear();
    calculatorReports.clear();

    for (var item in results.documents!) {
      if (item.docType == "Bank Forms") {
        bankForms.add(item);
      }

      if (item.docType == "NRIC") {
        nRICs.add(item);
      }

      if (item.docType == "Option to Purchase") {
        oTPOptiontoPurchase.add(item);
      }

      if (item.docType == "IRAS Statements") {
        iRASs.add(item);
      }

      if (item.docType == "CPF Statements") {
        cPFs.add(item);
      }

      if (item.docType == "HDB Statements") {
        hDBs.add(item);
      }

      if (item.docType == "Payslips") {
        payslips.add(item);
      }

      if (item.docType == "Valuation Reports") {
        valuationReports.add(item);
      }

      if (item.docType == "Outstanding Loan Statements") {
        loanStatements.add(item);
      }

      if (item.docType == "Tenancy Agreement") {
        tenancyAgreement.add(item);
      }

      if (item.docType == "Others") {
        others.add(item);
      }

      if (item.docType == "Letter Of Offer") {
        lOs.add(item);
      }

      if (item.docType == "Calculator Reports") {
        calculatorReports.add(item);
      }
    }
  }

  void onTapAddAttachment() {
    debugPrint("onTapUpload");
    getImageFile(ImageSource.gallery);
  }

  //final picker = ImagePicker();
  Rx<File> images = File("").obs;

  Future getImageFile(ImageSource source) async {
    await getPermission();

    /*final pickedFile = await picker.pickImage(
      source: source,
      imageQuality: 30,
    );
    if (pickedFile != null) {
      debugPrint(pickedFile.path);
      images.value = File(pickedFile.path);
    } else {
      debugPrint('No image selected.');
    }
*/

    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: [
        'jpg',
        'pdf',
        'doc',
        'DOCX' "png",
        "jpeg",
        "gif",
        "tiff"
      ],
    );

    if (result == null) {
      return;
    }

    if (result.files.isNotEmpty) {
      debugPrint("file size::: " + result.files.elementAt(0).size.toString());

      double size = result.files.elementAt(0).size / 1024 / 1024;

      debugPrint("file size mb::: " + size.toString());

      fileSize.value = size.toStringAsFixed(2) + " MB";
      fileName.value = result.files.elementAt(0).name;

      if (size > 3) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "File size too large. Please upload file within 3 MB",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
        return;
      }

      debugPrint('file path:: ' + result.files.elementAt(0).path!);

      images.value = File(result.files.elementAt(0).path!);
    }
  }

  /*==================fileUpload====================*/
  fileUpload(String doc_type) async {
    if (fileNameController.text.trim().isEmpty || images.value.path.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter all mandatory fields",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return;
    }

    onPressBack();

    Timer(const Duration(milliseconds: 500), () async {
      Get.dialog(const Center(child: CircularProgressIndicator()),
          barrierDismissible: false);

      var data = await CoreService().postFileWithAuthDocumentDrawer(
          upload: images.value.readAsBytesSync(),
          filename: images.value.path.split("/").last,
          url: baseUrl + drawerUploadFileUrl,
          id: leadsViewController.myDocumentID,
          nameField: fileNameController.text.trim(),
          doc_type: doc_type);
      if (data == null) {
        if (Get.isDialogOpen ?? false) Get.back();
      } else {
        if (Get.isDialogOpen ?? false) Get.back();
        var result = data;
        if (result != null) {
          if (data == 401) {
            bool success =
                await CoreService().getNewAccessTokenWithRefreshToken();
            if (success) {
              fileUpload(doc_type);
            }
          } else {
            clearField();
            Future.delayed(const Duration(milliseconds: 0), () {
              FocusManager.instance.primaryFocus!.unfocus();
              Fluttertoast.showToast(
                timeInSecForIosWeb: 3,
                msg: "Successfully Uploaded",
                backgroundColor: Colors.green,
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.BOTTOM,
                textColor: Colors.white,
                fontSize: 20.0,
              );
            });
            getDocuments();
          }
        } else {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: result.message!,
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }
      }
    });
  }

  clearField() {
    fileNameController.clear();
    images.value = File("");
  }

  onPressBack() {
    Get.back();
  }

  //===================downlaod=============
  Future<void> prepareSaveDir() async {
    _localPath = (await _findLocalPath())!;
    final savedDir = Directory(_localPath);
    bool hasExisted = await savedDir.exists();
    if (!hasExisted) {
      savedDir.create();
    }
  }

  Future<String?> _findLocalPath() async {
    String? externalStorageDirPath;
    if (Platform.isAndroid) {
      try {
        externalStorageDirPath = await AndroidPathProvider.downloadsPath;
      } catch (e) {
        final directory = await getExternalStorageDirectory();
        externalStorageDirPath = directory?.path;
      }
    } else if (Platform.isIOS) {
      externalStorageDirPath =
          (await getApplicationDocumentsDirectory()).absolute.path;
    }
    return externalStorageDirPath;
  }

  void downloadCallback(String id, DownloadTaskStatus status, int progress) {
    if (kDebugMode) {
      print(
          'Background Isolate Callback: task ($id) is in status ($status) and process ($progress)');
    }

    final SendPort send =
        IsolateNameServer.lookupPortByName('downloader_send_port')!;
    send.send([id, status, progress]);
  }

  void copy(String document_drawer_url) {
    FlutterClipboard.copy(document_drawer_url).then(
      (value) => Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Successfully copied",
          backgroundColor: Colors.green,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      }),
    );
  }
}
