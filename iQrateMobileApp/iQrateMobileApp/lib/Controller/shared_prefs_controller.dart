import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefController extends GetxController {
  // ignore: prefer_typing_uninitialized_variables
  var prefs;
  @override
  void onInit() {
    initializeSharePrefs();
    super.onInit();
  }

  initializeSharePrefs() async {
    prefs = await SharedPreferences.getInstance();
    await prefs.setInt('counter', 0);
    await prefs.setBool('firstLogin', false);
  }
}
