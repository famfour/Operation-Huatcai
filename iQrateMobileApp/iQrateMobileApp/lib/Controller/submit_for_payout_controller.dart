// ignore_for_file: prefer_typing_uninitialized_variables, invalid_use_of_protected_member, unused_field, deprecated_member_use

import 'dart:async';
import 'dart:developer';
import 'dart:io';
import 'dart:isolate';
import 'dart:ui';

import 'package:android_path_provider/android_path_provider.dart';
import 'package:clipboard/clipboard.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:iqrate/Controller/leads_view_controller.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/container_styles.dart';
import 'package:iqrate/DeviceManager/screen_constants.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Model/response_model.dart/ResponseBankers.dart';
import 'package:iqrate/Model/response_model.dart/bank_forms_response_model.dart';
import 'package:iqrate/Model/response_model.dart/bank_submission_lawfirms_list_response_model.dart';
import 'package:iqrate/Model/response_model.dart/banker_model.dart';
import 'package:iqrate/Model/response_model.dart/documents_downlaod_response_model.dart';
import 'package:iqrate/Model/response_model.dart/documents_drawer_response_model.dart';
import 'package:iqrate/Model/response_model.dart/response_payout_model.dart';
import 'package:iqrate/Model/send_model.dart/send_create_payout_v2.dart';
import 'package:iqrate/Service/GetControllers.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';
import 'package:iqrate/Utils/ItemCheckBoxModel.dart';
import 'package:iqrate/Utils/config.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';
import 'package:iqrate/Widgets/secondary_button.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:url_launcher/url_launcher.dart';

import '../Widgets/form_field_title.dart';

class SubmitForPayoutViewController extends GetxController {
  TextEditingController legalFeesController =
      TextEditingController(); //legal Fees text controller
  TextEditingController referralFeesController = TextEditingController(
      text: "0.15"); //referral Fees Income text controller
  TextEditingController referenceNumberController =
      TextEditingController(); //reference Number text controller
  TextEditingController loanAcceptanceDateController =
      TextEditingController(); //loan Acceptance Date
  TextEditingController fileNameController =
      TextEditingController(); //loan Acceptance Date

  final formKey = GlobalKey<FormState>();

  var isExpand = false.obs;
  var isShowJointAccount = false.obs;

  List<OneBankForm> banks = <OneBankForm>[];
  OneBankForm? selectedBank;

  final List<BankerModel> bankers = <BankerModel>[].obs;
  BankerModel? selectedBanker;

  RxList<LawFirmModel> lawFirmList = <LawFirmModel>[].obs;
  LawFirmModel? selectedLawFirm;

  RxList<Documents> documents = <Documents>[].obs;
  final List<String> lockInPeriods = [];
  final List<String> clawbackPeriods = [];

  var rateTypeDataSourceV2 = <ItemCheckBoxModel>[];
  ItemCheckBoxModel? selectedRateType1;

  //loan 1
  TextEditingController loanAmountController1 =
      TextEditingController(); //loan Amount text controller

  RxString selectedLockInPeriod1 = '2'.obs;
  RxString selectedClawbackPeriod1 = '0'.obs;

  TextEditingController cashRebateController1 =
      TextEditingController(); //Cash Rebate text controller
  TextEditingController nextReviewDateController1 =
      TextEditingController(); //next Review Date text controller

  //loan 2
  TextEditingController loanAmountController2 =
      TextEditingController(); //loan Amount text controller
  ItemCheckBoxModel? selectedRateType2;
  var selectedLockInPeriod2 = '2'.obs;
  var selectedClawbackPeriod2 = '2'.obs;
  TextEditingController cashRebateController2 =
      TextEditingController(); //Cash Rebate text controller
  TextEditingController nextReviewDateController2 =
      TextEditingController(); //next Review Date text controller

  //loan 3
  TextEditingController loanAmountController3 =
      TextEditingController(); //loan Amount text controller
  ItemCheckBoxModel? selectedRateType3;
  var selectedLockInPeriod3 = '2'.obs;
  var selectedClawbackPeriod3 = '2'.obs;
  TextEditingController cashRebateController3 =
      TextEditingController(); //Cash Rebate text controller
  TextEditingController nextReviewDateController3 =
      TextEditingController(); //next Review Date text controller

  //loan 4
  TextEditingController loanAmountController4 =
      TextEditingController(); //loan Amount text controller
  ItemCheckBoxModel? selectedRateType4;
  var selectedLockInPeriod4 = '2'.obs;
  var selectedClawbackPeriod4 = '2'.obs;
  TextEditingController cashRebateController4 =
      TextEditingController(); //Cash Rebate text controller
  TextEditingController nextReviewDateController4 =
      TextEditingController(); //next Review Date text controller

  //loan 5
  TextEditingController loanAmountController5 =
      TextEditingController(); //loan Amount text controller
  ItemCheckBoxModel? selectedRateType5;
  var selectedLockInPeriod5 = '2'.obs;
  var selectedClawbackPeriod5 = '2'.obs;
  TextEditingController cashRebateController5 =
      TextEditingController(); //Cash Rebate text controller
  TextEditingController nextReviewDateController5 = TextEditingController();

  int? leadID;
  String? documentID;

  //BankSubmissionDocumentDrawerScreenController bankSubmissionDocumentDrawerScreenController = GetControllers.shared.getBankSubmissionDocumentDrawerScreenController();

  ResponsePayoutModel? responsePayoutModel;

  var isSubmitPayoutApproved = false.obs;

  @override
  onInit() {
    setExistingBanksDropdown();
    //setBankersDropdown();

    //demoData();

    referralFeesController.text = "0.15";
    super.onInit();
  }

  demoData() {
    if (kDebugMode) {
      loanAcceptanceDateController.text = "2022-09-09";
      referenceNumberController.text = "123432";
      referralFeesController.text = "123432";
      loanAmountController1.text = "123433432";
      cashRebateController1.text = "123432";
      cashRebateController1.text = "123432";
      nextReviewDateController1.text = "2022-09-09";
      legalFeesController.text = "123";
    }
  }

  void onTapSubmit() {
    if (selectedBank == null &&
        selectedBanker == null &&
        loanAcceptanceDateController.text.isEmpty &&
        referenceNumberController.text.isEmpty &&
        referralFeesController.text.isEmpty &&
        selectedLawFirm!.id.toString().isEmpty &&
        legalFeesController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "All fields are mandatory",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (selectedBank == null) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please select the bank",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (selectedBanker == null) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please select the banker",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (loanAcceptanceDateController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please select loan acceptance date",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (referenceNumberController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter reference number",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (selectedLawFirm == null) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please select your law firm",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (legalFeesController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter legal fees",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else {
      createAndUpdatePayout();
    }
  }

  createAndUpdatePayout() async {
    if (loanAmountController1.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter loan 1 amount",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });

      return;
    }

    if (cashRebateController1.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter loan 1 cash rebate",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });

      return;
    }

    if (nextReviewDateController1.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter loan 1 next review date",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });

      return;
    }

    List<LoansItem> loans = <LoansItem>[];
    //selectedLawFirm!.id!
    loans.add(LoansItem(
        rates: [],
        rateType: selectedRateType1!.value,
        lockInPeriod: selectedLockInPeriod1.value,
        loanAmount: double.parse(
          loanAmountController1.text
              .toString()
              .replaceAll('\$', '')
              .replaceAll(',', ''),
        ),
        cashRebateSubsidy: double.parse(
          cashRebateController1.text
              .toString()
              .replaceAll('\$', '')
              .replaceAll(',', ''),
        ),
        nextReviewDate:
            AppConfig.getDateFormat2(nextReviewDateController1.text),
        clawbackPeriod: int.parse(selectedClawbackPeriod1.value)));

    if (isShowLoanNumber2.value) {
      if (loanAmountController2.text.isEmpty) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Please enter loan 1 amount",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });

        return;
      }

      if (cashRebateController2.text.isEmpty) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Please enter loan 1 cash rebate",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });

        return;
      }

      if (nextReviewDateController2.text.isEmpty) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Please enter loan 1 next review date",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });

        return;
      }

      loans.add(LoansItem(
          rates: [],
          rateType: selectedRateType2!.value,
          lockInPeriod: selectedLockInPeriod2.value,
          loanAmount: double.parse(
            loanAmountController2.text
                .toString()
                .replaceAll('\$', '')
                .replaceAll(',', ''),
          ),
          cashRebateSubsidy: double.parse(
            cashRebateController2.text
                .toString()
                .replaceAll('\$', '')
                .replaceAll(',', ''),
          ),
          nextReviewDate:
              AppConfig.getDateFormat2(nextReviewDateController2.text),
          clawbackPeriod: int.parse(selectedClawbackPeriod2.value)));
    }

    if (isShowLoanNumber3.value) {
      if (loanAmountController3.text.isEmpty) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Please enter loan 1 amount",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });

        return;
      }

      if (cashRebateController3.text.isEmpty) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Please enter loan 1 cash rebate",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });

        return;
      }

      if (nextReviewDateController3.text.isEmpty) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Please enter loan 1 next review date",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });

        return;
      }

      loans.add(LoansItem(
          rates: [],
          rateType: selectedRateType3!.value,
          lockInPeriod: selectedLockInPeriod3.value,
          loanAmount: double.parse(
            loanAmountController3.text
                .toString()
                .replaceAll('\$', '')
                .replaceAll(',', ''),
          ),
          cashRebateSubsidy: double.parse(
            cashRebateController3.text
                .toString()
                .replaceAll('\$', '')
                .replaceAll(',', ''),
          ),
          nextReviewDate:
              AppConfig.getDateFormat2(nextReviewDateController3.text),
          clawbackPeriod: int.parse(selectedClawbackPeriod3.value)));
    }

    if (isShowLoanNumber4.value) {
      if (loanAmountController4.text.isEmpty) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Please enter loan 1 amount",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });

        return;
      }

      if (cashRebateController4.text.isEmpty) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Please enter loan 1 cash rebate",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });

        return;
      }

      if (nextReviewDateController4.text.isEmpty) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Please enter loan 1 next review date",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });

        return;
      }

      loans.add(LoansItem(
          rates: [],
          rateType: selectedRateType4!.value,
          lockInPeriod: selectedLockInPeriod4.value,
          loanAmount: double.parse(
            loanAmountController4.text
                .toString()
                .replaceAll('\$', '')
                .replaceAll(',', ''),
          ),
          cashRebateSubsidy: double.parse(
            cashRebateController4.text
                .toString()
                .replaceAll('\$', '')
                .replaceAll(',', ''),
          ),
          nextReviewDate:
              AppConfig.getDateFormat2(nextReviewDateController4.text),
          clawbackPeriod: int.parse(selectedClawbackPeriod4.value)));
    }

    if (isShowLoanNumber5.value) {
      if (loanAmountController5.text.isEmpty) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Please enter loan 1 amount",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });

        return;
      }

      if (cashRebateController5.text.isEmpty) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Please enter loan 1 cash rebate",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });

        return;
      }

      if (nextReviewDateController5.text.isEmpty) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Please enter loan 1 next review date",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });

        return;
      }

      loans.add(LoansItem(
          rates: [],
          rateType: selectedRateType5!.value,
          lockInPeriod: selectedLockInPeriod5.value,
          loanAmount: double.parse(
            loanAmountController5.text.replaceAll('\$', '').replaceAll(',', ''),
          ),
          cashRebateSubsidy: double.parse(
            cashRebateController5.text
                .toString()
                .replaceAll('\$', '')
                .replaceAll(',', ''),
          ),
          nextReviewDate: nextReviewDateController5.text,
          clawbackPeriod: int.parse(selectedClawbackPeriod5.value)));
    }

    if (responsePayoutModel != null &&
        responsePayoutModel!.id > 0 &&
        responsePayoutModel!.status == "uneditable") {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "You can no longer update the payout details",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });

      return;
    }

    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);

    var sendModel = SendCreatePayoutV2(
      lead: leadID!,
      bank: selectedBank!.id!,
      banker: selectedBanker!.id!,
      lawFirm: selectedLawFirm!.id!,
      //lawFirm: 1,
      loans: loans,
      loanAcceptanceDate: loanAcceptanceDateController.text,
      loReferenceNumber: referenceNumberController.text,
      bankReferralFee: double.tryParse(referralFeesController.text
          .toString()
          .replaceAll(',', '')
          .replaceAll('\$', ''))!,
      //ownLawFirmName: selectedLawFirm!.name!,
      ownLawFirmName: selectedLawFirm!.name!,
      legalFee: double.parse(
        legalFeesController.text
            .toString()
            .replaceAll('\$', '')
            .replaceAll(',', ''),
      ),
    );

    // log(sendModel.toJson().toString());

    var data;

    if (responsePayoutModel != null && responsePayoutModel!.id > 0) {
      //update
      data = await CoreService().putWithAuth(
          url: baseUrl + updatePayoutUrl + responsePayoutModel!.id.toString(),
          body: sendModel.toJson());
    } else {
      //createw
      data = await CoreService().postWithAuth(
          url: baseUrl + createPayoutUrl, body: sendModel.toJson());
    }

    try {
      if (Get.isDialogOpen ?? false) Get.back();

      if (data == null) {
      } else {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            createAndUpdatePayout();
          }
        } else {
          successPopUp();
        }
      }
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  getDetailsPayout() async {
    try {
      var data = await CoreService()
          .getWithAuth(url: baseUrl + detailsPayoutUrl + leadID.toString());
      if (data == null) {
      } else {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            getDetailsPayout();
          }
        } else {
          responsePayoutModel = ResponsePayoutModel.fromJson(data);

          if (responsePayoutModel != null && responsePayoutModel!.id > 0) {
            payoutDataSet();
          } else {
            setExistingBanksDropdown();
          }
        }
      }
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  payoutDataSet() {
    if (responsePayoutModel!.bank.forms.isNotEmpty) {
      isSubmitPayoutApproved.value = true;
    }

    /* if (responsePayoutModel!.status == "approved") {
      isSubmitPayoutApproved.value = true;
    }*/

    documents.value = responsePayoutModel!.documents;

    debugPrint("documents.length set:: " + documents.value.length.toString());

    /*OneBankForm bankForm = banks
        .firstWhere((element) => element.id == responsePayoutModel!.bank.id);

    selectedBank = bankForm;


    BankerModel bankerModel = bankers
        .firstWhere((element) => element.id == responsePayoutModel!.banker.id);

    selectedBanker = bankerModel;
    */

    loanAcceptanceDateController.text = responsePayoutModel!.loanAcceptanceDate;
    referenceNumberController.text = responsePayoutModel!.loReferenceNumber;
    referralFeesController.text =
        responsePayoutModel!.bankReferralFee.toString();

    legalFeesController.text = responsePayoutModel!.legalFee.toString();

    if (responsePayoutModel!.loans.isNotEmpty) {
      LoansItem loansItem = responsePayoutModel!.loans.first;

      loanAmountController1.text = loansItem.loanAmount.toString();
      cashRebateController1.text = loansItem.cashRebateSubsidy.toString();
      nextReviewDateController1.text = loansItem.nextReviewDate.toString();
      selectedLockInPeriod1.value = loansItem.lockInPeriod.toString();
      selectedClawbackPeriod1.value = loansItem.clawbackPeriod.toString();

      ItemCheckBoxModel rateModel = rateTypeDataSourceV2.firstWhere(
          (element) => element.name == loansItem.rateType.toString());
      selectedRateType1 = rateModel;
    }

    if (responsePayoutModel!.loans.length > 1) {
      isShowLoanNumber2.value = true;
      LoansItem loansItem = responsePayoutModel!.loans.elementAt(1);

      loanAmountController2.text = loansItem.loanAmount.toString();
      cashRebateController2.text = loansItem.cashRebateSubsidy.toString();
      nextReviewDateController2.text = loansItem.nextReviewDate.toString();
      selectedLockInPeriod2.value = loansItem.lockInPeriod.toString();
      selectedClawbackPeriod2.value = loansItem.clawbackPeriod.toString();

      ItemCheckBoxModel rateModel = rateTypeDataSourceV2.firstWhere(
          (element) => element.name == loansItem.rateType.toString());
      selectedRateType2 = rateModel;
    }

    if (responsePayoutModel!.loans.length > 2) {
      isShowLoanNumber3.value = true;
      LoansItem loansItem = responsePayoutModel!.loans.elementAt(2);

      loanAmountController3.text = loansItem.loanAmount.toString();
      cashRebateController3.text = loansItem.cashRebateSubsidy.toString();
      nextReviewDateController3.text = loansItem.nextReviewDate.toString();
      selectedLockInPeriod3.value = loansItem.lockInPeriod.toString();
      selectedClawbackPeriod3.value = loansItem.clawbackPeriod.toString();

      ItemCheckBoxModel rateModel = rateTypeDataSourceV2.firstWhere(
          (element) => element.name == loansItem.rateType.toString());
      selectedRateType3 = rateModel;
    }

    if (responsePayoutModel!.loans.length > 3) {
      isShowLoanNumber4.value = true;
      LoansItem loansItem = responsePayoutModel!.loans.elementAt(3);

      loanAmountController4.text = loansItem.loanAmount.toString();
      cashRebateController4.text = loansItem.cashRebateSubsidy.toString();
      nextReviewDateController4.text = loansItem.nextReviewDate.toString();
      selectedLockInPeriod4.value = loansItem.lockInPeriod.toString();
      selectedClawbackPeriod4.value = loansItem.clawbackPeriod.toString();

      ItemCheckBoxModel rateModel = rateTypeDataSourceV2.firstWhere(
          (element) => element.name == loansItem.rateType.toString());
      selectedRateType4 = rateModel;
    }

    if (responsePayoutModel!.loans.length > 4) {
      isShowLoanNumber5.value = true;
      LoansItem loansItem = responsePayoutModel!.loans.elementAt(4);

      loanAmountController5.text = loansItem.loanAmount.toString();
      cashRebateController5.text = loansItem.cashRebateSubsidy.toString();
      nextReviewDateController5.text = loansItem.nextReviewDate.toString();
      selectedLockInPeriod5.value = loansItem.lockInPeriod.toString();
      selectedClawbackPeriod5.value = loansItem.clawbackPeriod.toString();

      ItemCheckBoxModel rateModel = rateTypeDataSourceV2.firstWhere(
          (element) => element.name == loansItem.rateType.toString());
      selectedRateType5 = rateModel;
    }
  }

  Future<void> getLoDocuments() async {
    // debugPrint("onTapDownloadButton");

    var data = await CoreService().getWithAuth(
        url: baseUrl +
            drawerLoginWithOTPUrl +
            documentID!); //Send the otp to the server.

    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          getLoDocuments();
        }
      } else {
        DocumentsDrawerResponseModel results =
            DocumentsDrawerResponseModel.fromJson(data);

        setDocumentsData(results);
      }
    }
  }

  setDocumentsData(DocumentsDrawerResponseModel results) {
    documents.clear();

    for (var item in results.documents!) {
      if (item.docType == "Letter Of Offer") {
        documents.add(item);
      }
    }
  }

  LeadsViewController leadsViewController =
      GetControllers.shared.getLeadsViewController();

  getRateTypeDropdown() {
    rateTypeDataSourceV2
        .clear(); // Clearing the banks list if has any banks added in it
    for (var element in leadsViewController.kvStoreValues) {
      if (element.code == "rate_type") {
        log('message');
        debugPrint("=====kvstore===== rate type" + element.value.toString());
        log('message');

        element.value.forEach((key, value) {
          var itemNew = ItemCheckBoxModel(
              name: value ?? "", isChecked: false, value: key ?? "");
          rateTypeDataSourceV2.add(itemNew);
          // dropDownPropertyTypeKey.add(key);
          //debugPrint("array_key" + key );
        });

        selectedRateType1 = rateTypeDataSourceV2.first;
        selectedRateType2 = rateTypeDataSourceV2.first;
        selectedRateType3 = rateTypeDataSourceV2.first;
        selectedRateType4 = rateTypeDataSourceV2.first;
        selectedRateType5 = rateTypeDataSourceV2.first;
      }

      if (element.code == "cash_rebate_subsidy_clawback_period_years") {
        element.value.forEach((value) {
          debugPrint("cash_rebate_subsidy_clawback_period_years:: " +
              value.toString());
          clawbackPeriods.add(value.toString());
        });

        selectedClawbackPeriod1.value = clawbackPeriods.first;
        selectedClawbackPeriod2.value = clawbackPeriods.first;
        selectedClawbackPeriod3.value = clawbackPeriods.first;
        selectedClawbackPeriod4.value = clawbackPeriods.first;
        selectedClawbackPeriod5.value = clawbackPeriods.first;
      }

      if (element.code == "lock_in_period_years") {
        element.value.forEach((value) {
          debugPrint("lock_in_period_years:: " + value.toString());
          lockInPeriods.add(value.toString());
        });

        selectedLockInPeriod1.value = lockInPeriods.first;
        selectedLockInPeriod2.value = lockInPeriods.first;
        selectedLockInPeriod3.value = lockInPeriods.first;
        selectedLockInPeriod4.value = lockInPeriods.first;
        selectedLockInPeriod5.value = lockInPeriods.first;
      }
    }
  }

  getLawfirmList() async {
    lawFirmList.clear();
    selectedLawFirm = null;
    debugPrint(baseUrl + lawFirmPayoutUrl + leadID.toString());
    try {
      Timer(const Duration(seconds: 0), () {
        Get.isDialogOpen ?? true
            ? const Offstage()
            : Get.dialog(
                const Center(
                  child: CircularProgressIndicator(),
                ),
                barrierDismissible: false);
      });
      var data = await CoreService()
          .getWithAuth(url: baseUrl + lawFirmPayoutUrl + leadID.toString());
      if (data != null) {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            getLawfirmList();
          }
        } else {
          var result = LawfirmListResponseModel.fromJson(data);
          lawFirmList.value = result.lawFirmList!;
          debugPrint("LawfirmList::: " + lawFirmList.value.length.toString());

          if (lawFirmList.isNotEmpty) {
            selectedLawFirm = lawFirmList.first;
          }

          if (Get.isDialogOpen ?? false) Get.back();
        }
      } else {
        if (Get.isDialogOpen ?? false) Get.back();
      }
    } catch (e) {
      if (Get.isDialogOpen ?? false) Get.back();
      debugPrint(e.toString());
    }
  }

  getBankID() {
    //dropdownPropertyType.for

    OneBankForm item =
        banks.firstWhere((element) => element.name == selectedBank!.name);
    debugPrint("====dropdownPropertyType====" + item.id.toString());
    return item.id.toString();
  }

  setExistingBanksDropdown() async {
    try {
      var data = await CoreService()
          .getWithAuth(url: baseUrl + resourcesBankFormsUrls);
      if (data == null) {
      } else {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            setExistingBanksDropdown();
          }
        } else {
          var bankFormsResponseModel = BankFormsResponseModel.fromJson(data);
          for (int i = 0; i < bankFormsResponseModel.results!.length; i++) {
            banks.add(bankFormsResponseModel.results![i]);
          }
        }
      }
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  setBankersDropdown(String id) async {
    bankers.clear();
    selectedBanker = null;
    try {
      var data =
          await CoreService().getWithAuth(url: baseUrl + bankersUrl + id);
      if (data == null) {
      } else {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            setBankersDropdown(id);
          }
        } else {
          var bankFormsResponseModel = ResponseBankers.fromJson(data);
          for (int i = 0; i < bankFormsResponseModel.results!.length; i++) {
            bankers.add(bankFormsResponseModel.results![i]);
          }
        }
      }
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  var loanNumber = 1.obs;
  var isShowLoanNumber2 = false.obs;
  var isShowLoanNumber3 = false.obs;
  var isShowLoanNumber4 = false.obs;
  var isShowLoanNumber5 = false.obs;

  void onTapAddLoan() {
    if (loanNumber.value < 5) {
      loanNumber.value++;
    }

    if (loanNumber.value > 1) {
      isShowLoanNumber2.value = true;
    }

    if (loanNumber.value > 2) {
      isShowLoanNumber3.value = true;
    }

    if (loanNumber.value > 3) {
      isShowLoanNumber4.value = true;
    }

    if (loanNumber.value > 4) {
      isShowLoanNumber5.value = true;
    }
  }

  void onTapDeleteLoan(String number) {
    if (number == "2") {
      isShowLoanNumber2.value = false;
    } else if (number == "3") {
      isShowLoanNumber3.value = false;
    } else if (number == "4") {
      isShowLoanNumber4.value = false;
    } else if (number == "5") {
      isShowLoanNumber5.value = false;
    }

    loanNumber.value--;

    FocusManager.instance.primaryFocus!.unfocus();
    Fluttertoast.showToast(
      timeInSecForIosWeb: 3,
      msg: "Successfully Deleted",
      backgroundColor: Colors.green,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
      textColor: Colors.white,
      fontSize: 20.0,
    );
  }

  double windowHeightC = Get.height;
  double windowWidthC = Get.width;

  void successPopUp() {
    debugPrint("=======successPopUp========");

    Get.defaultDialog(
      title: "",
      content: Flexible(
        child: SizedBox(
          //height: Get.height * 0.75,
          width: Get.width * 0.95,
          child: SingleChildScrollView(
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 0.0, horizontal: 18),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        "assets/images/success-payout.png",
                        width: 70,
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Text(
                        //"Congratulations ! \n Your Payout is on the way.",
                        "Congratulations ! \n Your Payout is on the way.",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: FontSize.s20,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Container(
                      //height: windowHeight * 0.08,
                      //width: windowWidth * 0.9,
                      padding: const EdgeInsets.all(20),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: AppColors.greyF4F4F4,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: const [
                          Text(
                            "*Note :",
                            style: TextStyle(color: Colors.black),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            "Please remind your lead in the event of bank’s verification, it must be mentioned that IQrate is their mortgage broker. \n\nIf IQrate is not mentioned as their mortgage broker, the bank will not pay out the referral fee.",
                            style: TextStyle(color: Colors.black54),
                          ),
                        ],
                      )),
                  SizedBox(height: windowHeightC * 0.05),
                  PrimaryButton(
                    windowHeight: windowHeightC,
                    windowWidth: windowWidthC,
                    buttonTitle: "Close",
                    onPressed: () {
                      Get.back();
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  var fileName = "".obs;
  var fileSize = "".obs;

  void onTapUploadPopup(docType) {
    debugPrint("onTapUploadPopup");

    Get.defaultDialog(
      onWillPop: () async {
        return false;
      },
      barrierDismissible: false,
      title: "",
      content: SizedBox(
        height: windowHeightC * 0.55,
        width: windowWidthC * 0.9,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 0.0, horizontal: 18),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  children: [
                    Text(
                      "Add Documents",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: FontSize.s20,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: windowHeightC * 0.03),
                    const FormFieldTitle(title: 'File Name*'),
                    SizedBox(height: windowHeightC * 0.01),
                    RequireTextField(
                      type: Type.optional,
                      labelText: "Enter File Name",
                      controller: fileNameController,
                      key: const Key("File Name"),
                    ),
                  ],
                ),
                SizedBox(height: windowHeightC * 0.03),
                const FormFieldTitle(title: 'Attach Documents*'),
                SizedBox(height: windowHeightC * 0.01),
                Obx(() => InkWell(
                    onTap: () {
                      onTapAddAttachment();
                    },
                    child: images.value.path.isEmpty
                        ? Image.asset("assets/images/upload_document.png")
                        : Stack(
                            alignment: Alignment.center,
                            children: [
                              Image.asset("assets/images/upload_border.png"),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 7.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SizedBox(height: windowHeightC * 0.005),
                                    FormFieldTitle(title: fileName.value),
                                    SizedBox(height: windowHeightC * 0.005),
                                    Text(
                                      "(" + fileSize.value + ")",
                                      style: TextStyles
                                          .planTermSelectionPriceSavingTextStyle,
                                    ),
                                    SizedBox(height: windowHeightC * 0.002),
                                    Text(
                                      "Change",
                                      style: TextStyles.pdpaSendtitleStyle1,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ))),
                SizedBox(height: windowHeightC * 0.01),
                SizedBox(height: windowHeightC * 0.03),
                PrimaryButton(
                  windowHeight: windowHeightC,
                  windowWidth: windowWidthC,
                  buttonTitle: "Upload",
                  onPressed: () {
                    fileUpload(docType);
                  },
                ),
                SizedBox(height: windowHeightC * 0.03),
                SecondaryButton(
                  kGradientBoxDecoration:
                      ContainerStyles.kGradientBoxDecorationSecondaryButton,
                  kInnerDecoration:
                      ContainerStyles.kInnerDecorationSecondaryButton,
                  windowHeight: windowHeightC,
                  windowWidth: windowWidthC,
                  buttonTitle: "Cancel",
                  onPressed: () {
                    onPressBack();

                    Timer(const Duration(seconds: 1), () {
                      clearField();
                    });
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void onTapAddAttachment() {
    debugPrint("onTapUpload");
    getImageFile(ImageSource.gallery);
  }

  //final picker = ImagePicker();
  Rx<File> images = File("").obs;

  Future getImageFile(ImageSource source) async {
    await getPermission();

    /*final pickedFile = await picker.pickImage(
      source: source,
      imageQuality: 30,
    );
    if (pickedFile != null) {
      debugPrint(pickedFile.path);
      images.value = File(pickedFile.path);
    } else {
      debugPrint('No image selected.');
    }
*/

    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: [
        'jpg',
        'pdf',
        'doc',
        'DOCX' "png",
        "jpeg",
        "gif",
        "tiff"
      ],
    );

    if (result == null) {
      return;
    }

    if (result.files.isNotEmpty) {
      debugPrint("file size::: " + result.files.elementAt(0).size.toString());

      double size = result.files.elementAt(0).size / 1024 / 1024;

      debugPrint("file size mb::: " + size.toString());

      fileSize.value = size.toStringAsFixed(2) + " MB";
      fileName.value = result.files.elementAt(0).name;

      if (size > 3) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "File size too large. Please upload within 3MB,",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
        return;
      }

      debugPrint('file path:: ' + result.files.elementAt(0).path!);

      images.value = File(result.files.elementAt(0).path!);
    }
  }

  /*==================fileUpload====================*/
  fileUpload(String docType) async {
    if (fileNameController.text.isEmpty || images.value.path.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "All fileds are mandatory",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return;
    }

    onPressBack();
    try {
      Timer(const Duration(milliseconds: 500), () async {
        Get.dialog(const Center(child: CircularProgressIndicator()),
            barrierDismissible: false);
        var data = await CoreService().postFileWithAuthDocumentDrawer(
            upload: images.value.readAsBytesSync(),
            filename: images.value.path.split("/").last,
            url: baseUrl + drawerUploadFileUrl,
            id: documentID!,
            nameField: fileNameController.text,
            doc_type: docType);
        if (data == null) {
          if (Get.isDialogOpen ?? false) Get.back();
        } else {
          if (data == 401) {
            bool success =
                await CoreService().getNewAccessTokenWithRefreshToken();
            if (success) {
              fileUpload(docType);
            }
          } else {
            if (Get.isDialogOpen ?? false) Get.back();
            var result = data;
            if (result != null) {
              clearField();
              getDetailsPayout();
              getLoDocuments();
              Future.delayed(const Duration(milliseconds: 0), () {
                FocusManager.instance.primaryFocus!.unfocus();
                Fluttertoast.showToast(
                  timeInSecForIosWeb: 3,
                  msg: "Successfully Uploaded",
                  backgroundColor: Colors.green,
                  toastLength: Toast.LENGTH_LONG,
                  gravity: ToastGravity.BOTTOM,
                  textColor: Colors.white,
                  fontSize: 20.0,
                );
              });
              //getDocuments(documentIdFromArgument);
            } else {
              Future.delayed(const Duration(milliseconds: 0), () {
                FocusManager.instance.primaryFocus!.unfocus();
                Fluttertoast.showToast(
                  timeInSecForIosWeb: 3,
                  msg: result.message!,
                  backgroundColor: Colors.red,
                  toastLength: Toast.LENGTH_LONG,
                  gravity: ToastGravity.BOTTOM,
                  textColor: Colors.white,
                  fontSize: 20.0,
                );
              });
            }
          }
        }
      });
    } catch (e) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please try again later",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      if (Get.isDialogOpen ?? false) Get.back();
    }
  }

  clearField() {
    fileNameController.clear();
    images.value = File("");
  }

  onPressBack() {
    Get.back();
  }

  Future getPermission() async {
    var status = await Permission.storage.status;
    if (status.isDenied) {
      debugPrint("=======storage denied=======");
      await Permission.storage.request();
      // We didn't ask for permission yet or the permission has been denied before but not permanently.
    }
  }

  Future<void> onTapDownloadButton(String id) async {
    debugPrint("onTapDownloadButton");

    Get.isDialogOpen ?? true
        ? const Offstage() //Used for avoiding the dialog to be open when the user is already on the next screen.
        : Get.dialog(
            const Center(
                child:
                    CircularProgressIndicator()), //Used for showing the loading dialog.
            barrierDismissible:
                false); //Used for preventing the user to dismiss the dialog.

    var data = await CoreService().getWithAuth(
        url:
            baseUrl + drawerDownloadFileUrl + id); //Send the otp to the server.

    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          onTapDownloadButton(id);
        }
      } else {
        var result = DocumentsDownlaodResponseModel.fromJson(data);
        getPermission();

        launchUrl(result.url!);
        //downloadFile(result.url!, result.filename!);

        if (Get.isDialogOpen ?? false) Get.back();
      }
    }
  }

  void launchUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Invalid Download Link",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    }
  }

  void onTapPopUpUpdate(Documents document) {
    debugPrint("=======onTapPopUpUpdate========");
    fileNameController.text = document.filename!;

    Get.defaultDialog(
      title: "",
      content: SizedBox(
        height: windowHeightC * 0.45,
        width: windowWidthC * 0.9,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 0.0, horizontal: 18),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  children: [
                    Text(
                      "Please enter a new name for this file",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: FontSize.s20,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: windowHeightC * 0.05),
                    RequireTextField(
                      type: Type.optional,
                      labelText: "File Name",
                      controller: fileNameController,
                      key: const Key("File Name"),
                    ),
                  ],
                ),
                SizedBox(height: windowHeightC * 0.05),
                PrimaryButton(
                  windowHeight: windowHeightC,
                  windowWidth: windowWidthC,
                  buttonTitle: "Save",
                  onPressed: () {
                    onTapUpdate(document, fileNameController.text);
                  },
                ),
                SizedBox(height: windowHeightC * 0.03),
                SecondaryButton(
                  kGradientBoxDecoration:
                      ContainerStyles.kGradientBoxDecorationSecondaryButton,
                  kInnerDecoration:
                      ContainerStyles.kInnerDecorationSecondaryButton,
                  windowHeight: windowHeightC,
                  windowWidth: windowWidthC,
                  buttonTitle: "Cancel",
                  onPressed: () {
                    onPressBack();
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> onTapUpdate(Documents document, String fileName) async {
    debugPrint("onTapUpdate");

    if (fileName.trim().isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter file name!",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return;
    }

    Get.isDialogOpen ?? true
        ? const Offstage() //Used for avoiding the dialog to be open when the user is already on the next screen.
        : Get.dialog(
            const Center(
                child:
                    CircularProgressIndicator()), //Used for showing the loading dialog.
            barrierDismissible:
                false); //Used for preventing the user to dismiss the dialog.

    var data = await CoreService().putWithAuth(
        url: baseUrl + drawerUpdateFileUrl + document.id!,
        body: {"filename": fileName}); //Send the otp to the server.

    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          onTapUpdate(document, fileName);
        }
      } else {
        clearField();

        getDetailsPayout();
        getLoDocuments();

        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Successfully Updated",
            backgroundColor: Colors.green,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      }
    }
  }

  void onTapPopUpDelete(String? id) {
    debugPrint("=======popUpDelete========");

    Get.defaultDialog(
      title: "",
      content: SizedBox(
        height: windowHeightC * 0.32,
        width: windowWidthC * 0.9,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 0.0, horizontal: 18),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  children: [
                    Text(
                      "Are you sure you want to delete this document?",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: FontSize.s20,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: windowHeightC * 0.05),
                PrimaryButton(
                  windowHeight: windowHeightC,
                  windowWidth: windowWidthC,
                  buttonTitle: "Delete Now",
                  onPressed: () {
                    onPressBack();
                    onTapDeleteButton(id);
                  },
                ),
                SizedBox(height: windowHeightC * 0.03),
                SecondaryButton(
                  kGradientBoxDecoration:
                      ContainerStyles.kGradientBoxDecorationSecondaryButton,
                  kInnerDecoration:
                      ContainerStyles.kInnerDecorationSecondaryButton,
                  windowHeight: windowHeightC,
                  windowWidth: windowWidthC,
                  buttonTitle: "Cancel",
                  onPressed: () {
                    onPressBack();
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> onTapDeleteButton(String? id) async {
    Timer(const Duration(seconds: 1), () {
      if (Get.isDialogOpen ?? false) Get.back();
    });

    debugPrint("onTapDeleteButton");

    Get.isDialogOpen ?? true
        ? const Offstage() //Used for avoiding the dialog to be open when the user is already on the next screen.
        : Get.dialog(
            const Center(
                child:
                    CircularProgressIndicator()), //Used for showing the loading dialog.
            barrierDismissible:
                false); //Used for preventing the user to dismiss the dialog.

    var data = await CoreService().deleteWithAuth(
        url: baseUrl + drawerDeleteFileUrl + id!); //Send the otp to the server.

    if (Get.isDialogOpen ?? false) Get.back();

    if (data == null) {
      getDetailsPayout();
      getLoDocuments();

      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Successfully Deleted",
          backgroundColor: Colors.green,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          onTapDeleteButton(id);
        }
      } else {
        getDetailsPayout();
      }
    }
  }

//===================downlaod=============

  late bool _permissionReady;
  late String _localPath;
  final ReceivePort _port = ReceivePort();

  Future<void> prepareSaveDir() async {
    _localPath = (await _findLocalPath())!;
    final savedDir = Directory(_localPath);
    bool hasExisted = await savedDir.exists();
    if (!hasExisted) {
      savedDir.create();
    }
  }

  Future<String?> _findLocalPath() async {
    String? externalStorageDirPath;
    if (Platform.isAndroid) {
      try {
        externalStorageDirPath = await AndroidPathProvider.downloadsPath;
      } catch (e) {
        final directory = await getExternalStorageDirectory();
        externalStorageDirPath = directory?.path;
      }
    } else if (Platform.isIOS) {
      externalStorageDirPath =
          (await getApplicationDocumentsDirectory()).absolute.path;
    }
    return externalStorageDirPath;
  }

  void downloadCallback(String id, DownloadTaskStatus status, int progress) {
    if (kDebugMode) {
      print(
          'Background Isolate Callback: task ($id) is in status ($status) and process ($progress)');
    }

    final SendPort send =
        IsolateNameServer.lookupPortByName('downloader_send_port')!;
    send.send([id, status, progress]);
  }

  void copy(String documentDrawerUrl) {
    FlutterClipboard.copy(documentDrawerUrl).then(
      (value) => Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Successfully Copied",
          backgroundColor: Colors.green,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      }),
    );
  }
}
