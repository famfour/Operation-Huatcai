import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:iqrate/Model/response_model.dart/training_view_response_model.dart';

class TrainingCategoryDetailsViewController extends GetxController {
  var arguments = Get.arguments;
  TrainingViewResponseModel trainingViewResponseModel =
      TrainingViewResponseModel();

  @override
  void onInit() {
    trainingViewResponseModel = arguments[0];
    super.onInit();
  }

  String dateFormatter(String date) {
    final DateTime now = DateTime.parse(date);
    final DateFormat formatter = DateFormat('dd-MM-yyyy');
    final String formatted = formatter.format(now);
    return formatted;
  }
}
