// ignore_for_file: unused_local_variable, unnecessary_null_comparison

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:iqrate/DeviceManager/strings.dart';
import 'package:iqrate/Model/response_model.dart/signup_response_model.dart';
import 'package:iqrate/Model/send_model.dart/apple_login_send_model.dart';
import 'package:iqrate/Model/send_model.dart/twofa_user_send_model.dart';
import 'package:iqrate/Model/send_model.dart/google_auth_send_model.dart';
import 'package:iqrate/Model/send_model.dart/signup_send_model.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';
import 'package:iqrate/Utils/config.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

class SignUpController extends GetxController {
  Rx<SignUpResponseModel> signUpData = SignUpResponseModel().obs;
  TextEditingController emailController =
      TextEditingController(); //email text controller
  TextEditingController passwordController =
      TextEditingController(); //password text controller
  TextEditingController nameController =
      TextEditingController(); //name text controller
  TextEditingController dobController =
      TextEditingController(); //dob text controller
  TextEditingController phoneController =
      TextEditingController(); //phone text controller
  TextEditingController countryCodeController =
      TextEditingController(); //phone text controller
  TextEditingController countryCodeLabelController =
      TextEditingController(); //phone text controller

  TextEditingController referralCodeController = TextEditingController();

  GoogleSignIn googleSignIn = GoogleSignIn(scopes: ['email']);

  final formKey = GlobalKey<FormState>();

  bool isRedirectFromSignupScreen = false;
  bool isMobileVerificationFromProfileScreen = false;
  @override
  void onInit() {
    //Everything is initailized before the UI completely built. This is to avoid crashes later.
    nameController = TextEditingController();
    emailController = TextEditingController();
    passwordController = TextEditingController();
    phoneController = TextEditingController();
    dobController = TextEditingController();
    referralCodeController = TextEditingController();

    /*countryCodeController.text = "+65";
    countryCodeLabelController.text = "SG";*/

    debugSignUp();
    super.onInit();
  }

  onSignUpButtonTap() {
    if (emailController.text.isEmpty &&
        passwordController.text.isEmpty &&
        nameController.text.isEmpty &&
        phoneController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "All fields are mandatory",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (emailController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter a valid email",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (passwordController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter a valid password",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (!AppConfig.validatePassword(passwordController.text)) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: AppLabels.passwordValidation2,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (!AppConfig.validatePassword2(passwordController.text)) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: AppLabels.passwordValidation3,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (nameController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter a valid name",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (phoneController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter a valid mobile number",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (dobController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter a valid DOB",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (passwordController.text.length < 8) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Password must be a minimum of 8 characters",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else {
      submitSignUp();
    }
  }

  /*debugSignUp() {
    if (kDebugMode) {
      countryCodeController.text = "+91";
      emailController.text = "rishabh@digitalprizm.net";
      passwordController.text = "password1";
      nameController.text = "Rishabh Sethia";
      phoneController.text = "8017156043";
      dobController.text = "20-06-1996";
      countryCodeLabelController.text = "IN";
    }
  }*/

  debugSignUp() {
    if (kDebugMode) {
      emailController.text = "shohel01716@gmail.com";
      passwordController.text = "Password1";
      nameController.text = "Shohel";
      phoneController.text = "1844476978";
      dobController.text = "01-01-1990";
      countryCodeController.text = "+880";
      countryCodeLabelController.text = "BD";
    }
  }

  /*debugSignUp() {
    if (kDebugMode) {
      countryCodeController.text = "+91";
      emailController.text = "sethia.rishabh007@gmail.com";
      passwordController.text = "password";
      nameController.text = "Rishabh";
      phoneController.text = "8017156043";
      dobController.text = "20-06-1996";
      countryCodeLabelController.text = "IN";
    }
  }*/

  // debugSignUp() {
  //   if (kDebugMode) {
  //     emailController.text = "sunil@digitalprizm.net";
  //     passwordController.text = "password";
  //     nameController.text = "Sunil";
  //     phoneController.text = "8105956342";
  //     dobController.text = "15-06-1990";
  //     countryCodeController.text = "+91";
  //     countryCodeLabelController.text = "IN";
  //   }
  // }

  // submitSignUp() {
  //   Get.toNamed(emailVerification);
  // }

  late TwoFAuserSendModel twoFAuserSendModel;

  submitSignUp() async {
    //String phoneCode = countryCodeController.text.replaceAll("+", "");
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);
    SignUpSendModel model = SignUpSendModel(
      full_name: nameController.text,
      password: passwordController.text,
      email: emailController.text,
      mobile: phoneController.text,
      dob: dobController.text,
      agreed_term: 1,
      country_code: countryCodeController.text.replaceAll("+", "").toString(),
      country_code_label: countryCodeLabelController.text,
      referral_code: referralCodeController.text,
    );

    var data = await CoreService()
        .postWithoutAuth(url: baseUrl + registerUrl, body: model.toJson());

    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
      // Get.snackbar(StringUtils.error, "User already exists",
      //     snackPosition: SnackPosition.TOP, backgroundColor: Colors.white);
    } else {
      var result = SignUpResponseModel.fromJson(data);

      // twoFAuserSendModel = TwoFAuserSendModel.fromJson(data);

      if (result.id != null) {
        isRedirectFromSignupScreen = true;

        signUpData.value.smsToken = result.smsToken;
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Please verify your mobile by entering the OTP",
            backgroundColor: Colors.white,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.black,
            fontSize: 20.0,
          );
        });
        // Get.back();
        // Get.offAllNamed(verification, arguments: [phoneTextController.text]);
        Get.toNamed(mobileVerification);
      } else {
        // Get.snackbar(StringUtils.error, data["errMessage"].toString(),
        //     snackPosition: SnackPosition.TOP, backgroundColor: Colors.white);
      }
    }
  }

  handleGoogleSignUp() {
    try {
      googleSignIn.signIn().then((acc) async {
        if (acc != null) {
          GoogleSignInAuthentication auth = await acc.authentication;
          GoogleAuthSendModel model = GoogleAuthSendModel(
              accessToken: auth.accessToken.toString(),
              idToken: auth.idToken.toString(),
              type: 'Register');
          var data = await CoreService()
              .postWithoutAuth(url: baseUrl + googleAuth, body: model.toJson());
          if (data == null) {
            /*Get.snackbar(StringUtils.error, 'User already exist',
                snackPosition: SnackPosition.TOP,
                backgroundColor: Colors.red,
                colorText: Colors.white);*/
          } else {
            var result = SignUpResponseModel.fromJson(data);
            if (result.id != null) {
              Future.delayed(const Duration(milliseconds: 0), () {
                FocusManager.instance.primaryFocus!.unfocus();
                Fluttertoast.showToast(
                  timeInSecForIosWeb: 3,
                  msg: "Registration Successful",
                  backgroundColor: Colors.green,
                  toastLength: Toast.LENGTH_LONG,
                  gravity: ToastGravity.BOTTOM,
                  textColor: Colors.white,
                  fontSize: 20.0,
                );
              });

              Get.toNamed(successScreen);
            } else {}
          }
        } else {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Error",
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }
      });
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  handleAppleSignUp(String twoFactorCode) async {
    final credential = await SignInWithApple.getAppleIDCredential(
      scopes: [
        AppleIDAuthorizationScopes.email,
        AppleIDAuthorizationScopes.fullName,
      ],
      state: 'Register',
    );

    /*if (kDebugMode) {
      print("Apple login credential:: "+credential.toString());
      print("Apple login authorizationCode:: "+credential.authorizationCode);
      log("Apple login identityToken:: "+credential.identityToken.toString());
      print("Apple login userIdentifier:: "+credential.userIdentifier.toString());
      print("Apple login email:: "+credential.email.toString());
      print("Apple login givenName:: "+credential.givenName.toString());
      print("Apple login state:: "+credential.state.toString());
    }*/

    try {
      if (credential != null) {
        if (credential.givenName == null) {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg:
                  "Name can't be empty. Please check settings under apps using Apple ID",
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
          return;
        }
        AppleLoginSendModel model = AppleLoginSendModel(
            identityToken: credential.identityToken,
            authorizationCode: credential.authorizationCode,
            user: credential.userIdentifier,
            givenName: credential.givenName,
            type: 'Register');
        var data = await CoreService()
            .postWithoutAuth(url: baseUrl + appleAuth, body: model.toJson());
        if (data == null) {
          /*Get.snackbar(StringUtils.error, 'User already exist',
                snackPosition: SnackPosition.TOP,
                backgroundColor: Colors.red,
                colorText: Colors.white);*/
        } else {
          var result = SignUpResponseModel.fromJson(data);
          if (result.id != null) {
            Future.delayed(const Duration(milliseconds: 0), () {
              FocusManager.instance.primaryFocus!.unfocus();
              Fluttertoast.showToast(
                timeInSecForIosWeb: 3,
                msg: "Registration Successful",
                backgroundColor: Colors.green,
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.BOTTOM,
                textColor: Colors.white,
                fontSize: 20.0,
              );
            });

            Get.toNamed(successScreen);
          } else {}
        }
      } else {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Error!",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      }
    } catch (e) {
      debugPrint(e.toString());
    }
  }
}
