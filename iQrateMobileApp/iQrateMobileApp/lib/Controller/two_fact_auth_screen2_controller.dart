import 'dart:async';

import 'package:clipboard/clipboard.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Model/response_model.dart/enable_two_mfa_response_model.dart';
import 'package:iqrate/Model/send_model.dart/enable_mfa_send_model.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';

class TwoFactAuthScreenTwoController extends GetxController {
  Rx<EnableTwoMfaResponseModel> enableTwoMfaResponseModel =
      EnableTwoMfaResponseModel().obs;

  onSubmit() async {
    /*Get.toNamed(authenticatorOtp);
    return;*/

    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);
    EnableTwoMfaSendModel enableTwoMfaSendModel = EnableTwoMfaSendModel();
    var data = await CoreService().postWithAuth(
        url: baseUrl + twoMfaUrl, body: enableTwoMfaSendModel.toJson());
    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
      debugPrint("twoMfaUrl null");
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          onSubmit();
        }
      } else {
        var result = EnableTwoMfaResponseModel.fromJson(data);
        if (result.secret != null) {
          enableTwoMfaResponseModel.value = result;
          enableTwoMfaResponseModel.value.secret = result.secret;
          enableTwoMfaResponseModel.value.otpauthUrl = result.otpauthUrl;
          Get.toNamed(qrCodeScanner);
        }
      }
    }

    Timer(const Duration(seconds: 5), () {
      if (Get.isDialogOpen ?? false) Get.back();
    });
  }

  onCancel() {
    Get.back();
  }

  void copyCode() {
    FlutterClipboard.copy(enableTwoMfaResponseModel.value.secret.toString())
        .then((value) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Copied to clipboard",
          backgroundColor: Colors.green,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    });
  }
}
