import 'package:get/get.dart';
import 'package:iqrate/Router/route_constants.dart';

class InitialScreenController extends GetxController {
  void signUpButtonController() {
    Get.toNamed(intro); //go to sign up page
  }

  void loginButtonController() {
    Get.toNamed(loginScreen); //go to login page
  }
}
