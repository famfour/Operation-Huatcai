import 'dart:async';
import 'package:get/get.dart';
import 'package:iqrate/Router/route_constants.dart';

class BankSubmissionDocumentDrawerCompressionScreenController
    extends GetxController {
  @override
  void onInit() {
    Future.delayed(
        const Duration(seconds: 3),
        () => Get.offNamed(bankSubmissionEmailBankerScreen, arguments: [
              Get.arguments[1], //BankId
              Get.arguments[2], //LeadID
              Get.arguments[0], // DocumentList,
            ]));
    super.onInit();
  }
}
