// ignore_for_file: deprecated_member_use

import 'dart:developer';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Service/url.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';

class ContinueWithGoogleButtonController extends GetxController {
  // ignore: prefer_typing_uninitialized_variables
  var _controller;

  @override
  void onInit() {
    super.onInit();
    if (Platform.isAndroid) {
      WebView.platform = SurfaceAndroidWebView();
    }
    log('onInit');
  }

  onPressedWebView() async {
    debugPrint("hhh");
    WebView(
      initialUrl: baseUrl + registerWithGoogleUrl,
      javascriptMode: JavascriptMode.unrestricted,
      onWebViewCreated: (contoller) {
        _controller = contoller;
      },
      onProgress: (int progress) {
        debugPrint('WebView is loading (progress : $progress%)');
      },
      onPageFinished: (_) {
        readJS();
      },
    );
  }

  onPressedSignUp() async {
    const urlString = baseUrl + registerWithGoogleUrl;
    await canLaunch(urlString)
        ? await launch(
            urlString,
            forceWebView: true,
            enableJavaScript: true,
          )
        : throw 'Could not launch $urlString';
  }

  onPressedLogin() async {
    const urlString = baseUrl + signInWithGoogleUrl;
    await canLaunch(urlString)
        ? await launch(
            urlString,
            forceWebView: true,
            enableJavaScript: true,
          )
        : throw 'Could not launch $urlString';
  }

  void readJS() async {
    String html = await _controller.evaluateJavascript(
        "window.document.getElementsByTagName('html')[0].outerHTML;");
    debugPrint(html);
  }
}
