import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Model/response_model.dart/lawfirm_listing_response_model.dart';

class LawFirmListingDetailsViewController extends GetxController {
  var arguments = Get.arguments;

  RxList<OneProduct> productList = <OneProduct>[].obs;
  Rx<OneResult> oneProduct = OneResult().obs;

  @override
  void onInit() {
    productList.value = arguments[0];
    oneProduct.value = arguments[1];
    debugPrint(productList.length.toString());
    super.onInit();
  }
}
