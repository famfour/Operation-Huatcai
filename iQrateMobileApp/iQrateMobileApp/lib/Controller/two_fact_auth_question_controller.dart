import 'package:get/get.dart';
import 'package:iqrate/Router/route_constants.dart';

class TwoFactAuthQuestionController extends GetxController {
  onSubmit() {
    Get.toNamed(twoFactorScreenTwo);
  }

  onCancel() {
    Get.offAllNamed(dashboard);
  }
}
