import 'package:get/get.dart';
import 'package:iqrate/Router/route_constants.dart';

class SuccessViewController extends GetxController {
  onTapPrimaryButton() {
    Get.toNamed(loginScreen);
  }
}
