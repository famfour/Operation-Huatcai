import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Model/response_model.dart/forgot_password_response_model.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';

class ForgetPasswordController extends GetxController {
  late TextEditingController emailTextController;
  final formKey =
      GlobalKey<FormState>(); // this key manage form submit validation
  RxInt resendCount = 0.obs;

  @override
  void onInit() {
    emailTextController =
        TextEditingController(); //initialize email text field controller
    super.onInit();
  }

  onSubmitButtonTap() {
    if (emailTextController.text.isNotEmpty &&
        emailTextController.text.isEmail) {
      onSubmit();
    } else if (!emailTextController.text.isEmail) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter a valid email",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter an email id",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    }
  }

  onSubmit() async {
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);

    var data = await CoreService().getWithoutAuth(
        url: baseUrl +
            sendResetPasswordUrl +
            emailTextController.text.toString().toLowerCase());
    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
    } else {
      var result = ForgotPasswordResponseModel.fromJson(data);
      // Get.back();
      resendCount++;

      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: result.message.toString() +
              " " +
              emailTextController.text.toString().toLowerCase(),
          backgroundColor: Colors.green,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    }
  }
}
