import 'package:get/get.dart';
import 'package:iqrate/Provider/login_provider.dart';

import '../Model/response_model.dart/login_response_model.dart';
import '../Model/send_model.dart/login_send_model.dart';

class LoginControllerV2 extends GetxController {
  Future<LoginResponseModel> callAPILogin(
      {required String endpoint, required LoginSend loginSend}) async {
    var data = await LoginProvider()
        .postWithoutAuth(url: endpoint, body: loginSend.toJson());
    var loginResponseModel = LoginResponseModel.fromJson(data);
    return loginResponseModel;
  }
}
