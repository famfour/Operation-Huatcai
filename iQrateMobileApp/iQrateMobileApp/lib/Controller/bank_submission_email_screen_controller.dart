// ignore_for_file: prefer_typing_uninitialized_variables

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Model/email_model.dart';
import 'package:iqrate/Model/response_model.dart/bank_submission_email_to_lead_response_model.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';

class BankSubmissionEmailScreenController extends GetxController {
  late TextEditingController ccTextEditingController;
  late TextEditingController remarksTextEditingController;
  Rx<BankSubmissionEmailToLeadResponseModel>
      bankSubmissionEmailToLeadResponseModel =
      BankSubmissionEmailToLeadResponseModel().obs;
  RxList<EmailModel> emailList = <EmailModel>[].obs;
  RxList<String> ccEmailList = <String>[].obs;
  List toRemoveEmail = [];
  late RxString emailContent;
  int bankId = Get.arguments[0];
  int leadId = Get.arguments[1];
  RxBool showPackageExpiredError = false.obs;
  RxString errorBodyToDisplay =
      ''.obs; //Need to display the error message from the Server to the UI

  @override
  void onInit() {
    ccTextEditingController = TextEditingController();
    remarksTextEditingController = TextEditingController();
    emailContent = ''.obs;
    getEmailBody(bankId: bankId.toString(), leadId: leadId.toString());
    super.onInit();
  }

  getEmailBody({required String bankId, required String leadId}) async {
    debugPrint(baseUrl + emailToLeadEmailBodyUrl + leadId + '/' + bankId);
    try {
      Timer(const Duration(seconds: 0), () {
        Get.isDialogOpen ?? true
            ? const Offstage()
            : Get.dialog(
                const Center(
                  child: CircularProgressIndicator(),
                ),
                barrierDismissible: false);
      });
      var data = await CoreService().getWithAuth(
          url: baseUrl + emailToLeadEmailBodyUrl + leadId + '/' + bankId);
      if (data != null) {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            getEmailBody(bankId: bankId, leadId: leadId);
          }
        } else if (data == 403) {
          showPackageExpiredError.value = true;
        } else {
          bankSubmissionEmailToLeadResponseModel.value =
              BankSubmissionEmailToLeadResponseModel.fromJson(data);
          // emailContent.value = bankSubmissionEmailToLeadResponseModel
          //     .value.content!
          //     .replaceAll(r'$', r'\$');
          if (Get.isDialogOpen ?? false) Get.back();
        }
      } else {
        if (Get.isDialogOpen ?? false) Get.back();
      }

      if (Get.isDialogOpen ?? false) Get.back();
    } catch (e) {
      if (Get.isDialogOpen ?? false) Get.back();
      debugPrint(e.toString());
    }
  }

  sendEmailToLead() async {
    debugPrint(baseUrl +
        emailToLeadEmailBodyUrl +
        leadId.toString() +
        '/' +
        bankId.toString());
    try {
      Timer(const Duration(seconds: 0), () {
        Get.isDialogOpen ?? true
            ? const Offstage()
            : Get.dialog(
                const Center(
                  child: CircularProgressIndicator(),
                ),
                barrierDismissible: false);
      });
      var data = await CoreService().postWithAuth(
          url: baseUrl +
              emailToLeadEmailBodyUrl +
              leadId.toString() +
              '/' +
              bankId.toString(),
          body: {
            "cc_emails": ccEmailList,
            "remarks": remarksTextEditingController.text
          });
      if (data != null) {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            sendEmailToLead();
          }
        } else {
          if (Get.isDialogOpen ?? false) Get.back();
          Get.offNamed(bankSubmissionEmailSentScreen, arguments: [
            leadId,
            bankId,
            bankSubmissionEmailToLeadResponseModel
          ]);
        }
      } else {
        if (Get.isDialogOpen ?? false) Get.back();
      }
    } catch (e) {
      if (Get.isDialogOpen ?? false) Get.back();
      debugPrint(e.toString());
    }

    if(Get.isDialogOpen == true){
      Get.back();
    }

  }

  Iterable<Widget> get emailWidgets {
    return emailList.map((EmailModel email) {
      return Padding(
        padding: const EdgeInsets.all(4.0),
        child: Chip(
          label: Text(email.name),
          onDeleted: () {
            emailList.removeWhere((EmailModel entry) {
              return entry.email == email.email;
            });
            for (var element in ccEmailList) {
              if (element == email.email) {
                // ccEmailList.remove(element);
                toRemoveEmail.add(element);
              }
            }
            ccEmailList.removeWhere((e) => toRemoveEmail.contains(e));
          },
        ),
      );
    });
  }
}
