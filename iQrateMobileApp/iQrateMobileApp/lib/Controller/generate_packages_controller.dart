// ignore_for_file: avoid_function_literals_in_foreach_calls, unused_local_variable

import 'dart:async';
import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/rates_view_controller.dart';
import 'package:iqrate/Controller/selected_packages_controller.dart';
import 'package:iqrate/Controller/subscription_options_controller.dart';
import 'package:iqrate/Model/response_model.dart/bank_forms_response_model.dart';
import 'package:iqrate/Model/response_model.dart/fetch_lead_list_response_model.dart';
import 'package:iqrate/Model/response_model.dart/more_rates_response_model.dart';
import 'package:iqrate/Model/response_model.dart/refinance_new_lead_package_response_model_v2.dart';
import 'package:iqrate/Model/send_model.dart/GeneratePackageSendModel.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';
import 'package:iqrate/Utils/ItemCheckBoxModel.dart';
import 'package:logger/logger.dart';
import 'package:url_launcher/url_launcher.dart';

import '../Views/co_broke/LeadItemModel.dart';
import 'leads_view_controller.dart';

class GeneratePackagesController extends GetxController {
  var coBrokerData = LeadItemModel(clients: []);
  var isCoBroker = false;
  RxString coBrokeStatus = ''.obs;
  var isExpand = false.obs;
  var lowestPackageListModel = <Fixed>[].obs;
  var floatingPackageListModel = <Fixed>[].obs;

  // * Integer value to show or not show filter or which filter to show
  var showFilterConditionalVariable =
      0.obs; //! 0; not show, 1, show filter package, 2 show export, 3: circle

  // List<int> selectedPackages = [];
  RxBool isGeneratedPackage = false.obs;
  RxList<int> selectedPackgesForGenerateckages = <int>[].obs;

  //! Fetched from rates view controller starts here
  Rx<BankFormsResponseModel> bankResponseModel = BankFormsResponseModel().obs;

  var is200k = false
      .obs; //* Used in checkbox value for if the user is willing to place a minimum of 200k fresh fund
  var is3year = false
      .obs; //*Used in checkBox value for intention to sell property within 3 years

  //* Selected bank
  var selectedBankDataSourceV2 = <
      ItemCheckBoxModel>[]; //* Selected banks list displayed in the UI in chips format
  var bankDataSourceV2 = <ItemCheckBoxModel>[];
  RxList<int> bankId = <int>[].obs; //* List of bank ids
  RxList<String> bankName = <String>[].obs; //* List of Bank names

  var selectedRateTypeDataSourceV2 = <ItemCheckBoxModel>[];
  var rateTypeDataSourceV2 = <ItemCheckBoxModel>[];

  // * Selected property type
  var selectedProperTypeDataSourceV2 = <ItemCheckBoxModel>[];

  // * Loan amount
  var loanAmountController =
      TextEditingController(); //*Loan amount text editing controller

  //* Loan type
  List<String> loanTypeKeyDataSource = <String>[];
  RxInt loanTypeSelectedIndex = 0.obs;

  //* Rate category
  List<String> rateCategoryDataSource = ['Pending', 'Submitted'].obs;
  RxInt rateCategorySelectedIndex = 0.obs;
  List rateTypeDataSource = [];

  //* Property status
  List<String> properStatusKeyDataSource = <String>[];
  RxInt properStatusSelectedIndex = 0.obs;

  //! Fetched from rates view controller ends here

  @override
  void onInit() {
    super.onInit();
    loadData();

    debugPrint("isCoBroker $isCoBroker");
  }

  loadData() async {
    await callAPIGetKVStoreList();

    await callAPIGetBankList(); //* This function is used to get the list of banks and we show it in the filter multi-select dropdown
    // await callAPIGetKVStoreList();
    // resetFilter();
    // await callAPIGetListAllPackages();
  }

  Future<void> getLowestPackages(String leadID) async {
    debugPrint("getPackages::: ");
    lowestPackageListModel.clear();
    floatingPackageListModel.clear();

    /*Get.isDialogOpen ?? true
        ? const Offstage() //Used for avoiding the dialog to be open when the user is already on the next screen.
        : Get.dialog(
        const Center(
            child:
            CircularProgressIndicator()), //Used for showing the loading dialog.
        barrierDismissible:
        false); //Used for preventing the user to dismiss the dialog.*/

    var data = await CoreService().getWithAuth(
        url: baseUrl +
            getLowestPackagesApi +
            leadID); //Send the otp to the server.

    if (Get.isDialogOpen ?? false) Get.back();

    if (data == null) {
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          getLowestPackages(leadID);
        }
      } else if (data == 404) {
        debugPrint(data.toString());
      } else {
        if (data.toString() != "{fixed: null, floating: null}") {
          var result = GeneratePackageResponseModelV2.fromJson(data);

          lowestPackageListModel.addAll(result.fixed!);

          floatingPackageListModel.addAll(result.floating!);
        }

        if (Get.isDialogOpen ?? false) Get.back();
      }
    }
  }

  bool selectedPackages(int selected) {
    debugPrint("selectedPackgesForGenerateckages size:: " +
        selectedPackgesForGenerateckages.length.toString());

    int index = selectedPackgesForGenerateckages
        .indexWhere((element) => element == selected);

    debugPrint("selectedPackgesForGenerateckages index:: " + index.toString());

    if (index >= 0) {
      return true;
    }

    return false;
  }

  Future<void> generatePackage(String leadID, bool isShowLoading) async {
    debugPrint("getPackages::: ");
    isGeneratedPackage.value = false;

    if (selectedPackgesForGenerateckages.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please select packages",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return;
    }

    if (isShowLoading) {
      Get.isDialogOpen ?? true
          ? const Offstage() //Used for avoiding the dialog to be open when the user is already on the next screen.
          : Get.dialog(const Center(child: CircularProgressIndicator()),
              //Used for showing the loading dialog.
              barrierDismissible:
                  false); //Used for preventing the user to dismiss the dialog.

    }

    var model = GeneratePackageSendModel(
        lead: int.parse(leadID), packages: selectedPackgesForGenerateckages);

    var data = await CoreService().postWithAuth(
        url: baseUrl + generatePackagesApi,
        body: model.toJson()); //Send the otp to the server.

    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          generatePackage(leadID, isShowLoading);
        }
      } else {
        /*var result = Results.fromJson(data);

      generatesPackage.value = result;*/

        //check generated package
        if (data['packages'] != null && data['packages'].length > 0) {
          isGeneratedPackage.value = true;
          Get.find<SelectedPackagesController>().getSelectedPackages(leadID);
        }
        if (isShowLoading) {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Generated Successfully",
              backgroundColor: Colors.green,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }

        if (Get.isDialogOpen ?? false) Get.back();
      }
    }

    Timer(const Duration(seconds: 6), () {
      if (Get.isDialogOpen ?? false) Get.back();
    });
  }

  generatePackagesFromMorePackagesScreen(String leadID) async {
    isGeneratedPackage.value = false;

    try {
      if (selectedPackgesForGenerateckages.isEmpty) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Please select packages",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      } else {
        // generate body to pass to the API
        var model = GeneratePackageSendModel(
            lead: int.parse(leadID),
            packages: selectedPackgesForGenerateckages);

        // API call to generate packages
        var data = await CoreService().postWithAuth(
            url: baseUrl + generatePackagesApi,
            body: model.toJson()); //Send the otp to the server.
        if (data != null) {
          if (data == 401) {
            bool success =
                await CoreService().getNewAccessTokenWithRefreshToken();
            if (success) {
              generatePackagesFromMorePackagesScreen(leadID);
            }
          } else {
            Get.back();
            if (data['packages'] != null && data['packages'].length > 0) {
              isGeneratedPackage.value = true;
              Get.find<SelectedPackagesController>()
                  .getSelectedPackages(leadID);
            }
            isExpand.value = false;

            Future.delayed(const Duration(milliseconds: 0), () {
              FocusManager.instance.primaryFocus!.unfocus();
              Fluttertoast.showToast(
                timeInSecForIosWeb: 3,
                msg: "Successfully generated packages",
                backgroundColor: Colors.green,
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.BOTTOM,
                textColor: Colors.white,
                fontSize: 20.0,
              );
            });
          }
        } else {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Failed to generate packages",
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }
      }
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  //* check generated package if completed or not
  Future<void> checkGeneratedPackage(String leadID, Result leadData) async {
    isGeneratedPackage.value = false;

    // If the leadtype is Yourself then we do not need to check if package is generated or not
    if (leadData.leadType != 'yourself' && leadData.leadType != 'co_broke') {
      debugPrint("checkGeneratedPackage::: ");

      //demo package
      selectedPackgesForGenerateckages.clear();

      var model = GeneratePackageSendModel(
          lead: int.parse(leadID), packages: selectedPackgesForGenerateckages);

      var data = await CoreService().postWithAuth(
          url: baseUrl + generatePackagesApi,
          body: model.toJson()); //Send the otp to the server.

      if (data == null) {
      } else {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            checkGeneratedPackage(leadID, leadData);
          }
        } else {
          if (data['packages'] != null && data['packages'].length > 0) {
            isGeneratedPackage.value = true;
          }
        }
      }
    }
  }

  //* Function to call API which is used to export the selected packages
  Future<void> callAPIExportPackage() async {
    debugPrint("callAPIExportPackage:::");
    var arrayBankSelect = getIDFromBankStringSelectedToExport();
    var param = {"packages": arrayBankSelect};
    if (arrayBankSelect.isNotEmpty) {
      Logger().d(param);
      var data = await CoreService()
          .postWithAuth(url: baseUrl + bankPackagesExportToPdf, body: param);
      if (data != null) {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            callAPIExportPackage();
          }
        } else {
          showFilterConditionalVariable.value = 0;
          debugPrint("✅ok callAPIExportPackage $data");
          var url = data["url"];
          // ignore: deprecated_member_use
          await canLaunch(url) ? await launch(url) : debugPrint("error");
        }
      }
    } else {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please select packages to export",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    }
  }

  //* get the IDs of bank we need to export
  List<String> getIDFromBankStringSelectedToExport() {
    List<String> listIDFromBankStringSelected = [];
    for (var i = 0; i < selectedPackgesForGenerateckages.length; i++) {
      listIDFromBankStringSelected
          .add(selectedPackgesForGenerateckages[i].toString());
    }
    return listIDFromBankStringSelected;
  }

  //* This function is used to show the selected bank from the drop down in chip format
  getSelectedBankDataSourceV2() {
    selectedBankDataSourceV2.clear();
    for (var i = 0; i < bankDataSourceV2.length; i++) {
      var a = bankDataSourceV2[i];
      if (a.isChecked) {
        selectedBankDataSourceV2.add(a);
      }
    }
  }

  getSelectedRateTypeDataSourceV2() {
    selectedRateTypeDataSourceV2.clear();
    for (var i = 0; i < rateTypeDataSourceV2.length; i++) {
      var a = rateTypeDataSourceV2[i];
      if (a.isChecked) {
        selectedRateTypeDataSourceV2.add(a);
      }
    }
  }

  getBanksValues() async {
    var data = await CoreService().getWithAuth(url: baseUrl + getBanksUrl);
    if (data != null) {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          getBanksValues();
        }
      } else {
        bankResponseModel.value = BankFormsResponseModel.fromJson(data);
        bankResponseModel.value.results?.forEach((element) {
          if (element.name != null) {
            bankName.add(element.name!);
            bankId.add(element.id!);
          }
        });
      }
    }
  }

//* This function is used to get the list of banks and we show it in the filter multi-select dropdown
  Future<void> callAPIGetBankList() async {
    debugPrint("more rates callAPIGetBankList:::");
    var data = await CoreService().getWithAuthV2(
        url: baseUrl + getBanksUrl, query: {"page_size": "1000"});
    if (data != null) {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          callAPIGetBankList();
        }
      } else {
        bankResponseModel.value = BankFormsResponseModel.fromJson(data);
        getBankData();
        // getRateTypeData();
        getRateTypeDropdown();
      }
    } else {
      debugPrint("more rates bank empty:::" + data.toString());
    }
  }

  getBankData() {
    bankDataSourceV2
        .clear(); // Clearing the banks list if has any banks added in it
    bankResponseModel.value.results?.forEach((element) {
      if (element.name != null) {
        var itemNew = ItemCheckBoxModel(
            name: element.name ?? "",
            isChecked: false,
            value: element.id!.toString());
        bankDataSourceV2.add(itemNew);
      }
    });
  }

  LeadsViewController leadsViewController = Get.find();

  getRateTypeDropdown() {
    rateTypeDataSourceV2
        .clear(); // Clearing the banks list if has any banks added in it
    for (var element in leadsViewController.kvStoreValues) {
      if (element.code == "rate_type") {
        log('message');
        debugPrint("=====kvstore===== rate type" + element.value.toString());
        log('message');

        element.value.forEach((key, value) {
          var itemNew = ItemCheckBoxModel(
              name: value ?? "", isChecked: false, value: key ?? "");
          rateTypeDataSourceV2.add(itemNew);
          // dropDownPropertyTypeKey.add(key);
          //debugPrint("array_key" + key );
        });
      }
    }
  }

  //* Reseting the filter values
  resetFilterDefault() {
    debugPrint("resetFilterDefault");
    // selectedBank.value = [];
    selectedBankDataSourceV2.clear();
    selectedProperTypeDataSourceV2.clear();
    selectedRateTypeDataSourceV2.clear();
    // properTyeNameSelectedValues.value = [];
    rateCategorySelectedIndex.value = 0;
    properStatusSelectedIndex.value = 0;
    loanTypeSelectedIndex.value = 0;
    loanAmountController.text = "";
    is200k.value = false;
    is3year.value = false;
    showFilterConditionalVariable.value = 3; //show reset loading
    // showFilter.value = 1;
  }

  Future<void> callAPIGetKVStoreList() async {
    debugPrint("callAPIGetKVStoreList:::");
    var data = await CoreService().getWithAuthV2(url: baseUrl + getKvValuesUrl);
    if (data != null) {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          callAPIGetKVStoreList();
        }
      } else {
        // rateCategoryList = [];
        // statusDataSource = [];
        rateCategoryDataSource = [];
        //properStatusDataSource.value = [];
        // properTyeNameDataSource.value = [];
        properStatusKeyDataSource = [];
        List<dynamic> list = data;
        for (var element in list) {
          // statusDataSource = ['Active', 'Inactive'];//hard code because now APi dont have in KVI store
          //get rate cat

          //loan_category
          if (element["code"] == "loan_category") {
            element["value"].forEach((key, value) {
              if (kDebugMode) {
                print("add value");
              }
              //loanTypeDataSource.add(value);
              loanTypeKeyDataSource.add(key);
            });
          }

          //rate_category
          if (element["code"] == "rate_category") {
            element["value"].forEach((key, value) {
              rateCategoryDataSource.add(value);
            });
          }
          //rate type
          if (element["code"] == "rate_type") {
            element["value"].forEach((key, value) {
              rateTypeDataSource.add(value);
            });
          }
          if (element["code"] == "property_type") {
            element["value"].forEach((key, value) {
              if (kDebugMode) {
                print("add value");
              }
              // properTyeNameDataSource.add(value);
              // properTyeNameKeyDataSource.add(key);
              //v2
              var item =
                  ItemCheckBoxModel(name: value, value: key, isChecked: false);
              //properTypeDataSourceV2.add(item);
            });
          }
          //
          if (element["code"] == "property_status") {
            element["value"].forEach((key, value) {
              //properStatusDataSource.add(value);
              properStatusKeyDataSource.add(key);
            });
          }
        }
        //property_type
      }
    }
  }

  RatesViewController ratesViewController = Get.find();

  var leadID = "";

  Future<void> callAPIGetListAllPackages() async {
    // isLoadingAPI = true;
    debugPrint("callAPIGetListAllPackages:::");
    showFilterConditionalVariable.value = 0; //off popup!
    // var endPoint = baseUrl + customerBankPackages;
    // if (next != "") endPoint = next;
    // debugPrint("endPoint $endPoint");

    try {
      List<String> arrayBankSelect = getIDFromBankStringSelected();
      var arrayRateTypeSelect = getIDSelectedRateTypeDataSourceV2();
      if (kDebugMode) {
        print(arrayBankSelect);
        print(arrayRateTypeSelect);
      }

      if (kDebugMode) {
        print("rateCategoryDataSource $rateCategoryDataSource");
      }
      if (kDebugMode) {
        print("loanTypeKeyDataSource $loanTypeKeyDataSource");
      }

      String query = "?page=1&page_size=100";

      if (Get.find<SubscriptionOptionsController>()
              .subscriptionSummary
              .value
              .product
              .toString() ==
          "Basic") {}

      arrayBankSelect.forEach((element) {
        query += "&bank=$element";
      });

      arrayRateTypeSelect.forEach((element) {
        query += "&rate_type=$element";
      });

      // Logger().d("Query:: " + param.toString());

      var data = await CoreService()
          .getWithAuthV2(url: baseUrl + moreRatesApi + leadID + query);

      showFilterConditionalVariable.value = 0; //off popup!
      if (data == null) {
        debugPrint("❌null callAPIGetListAllPackages}");
      } else {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            callAPIGetListAllPackages();
          }
        } else {
          var result = MoreRatesResponseModel.fromJson(data);

          ratesViewController.moreRates.value = result.results!;

          debugPrint("✅ok callAPIGetListAllPackages");

          // showFilter.value = 0;//off popup!
        }
      }
    } catch (e) {
      // isLoadingAPI = false;
      debugPrint("something went wrong:: " + e.toString());
    }
  }

  List<String> getIDFromBankStringSelected() {
    List<String> listIDFromBankStringSelected = [];
    for (var i = 0; i < selectedBankDataSourceV2.length; i++) {
      var selectedValues = selectedBankDataSourceV2[i];
      listIDFromBankStringSelected.add(selectedValues.value);
    }
    return listIDFromBankStringSelected;
  }

  getIDSelectedRateTypeDataSourceV2() {
    List<String> listIDFromRateTypeStringSelected = [];
    for (var i = 0; i < selectedRateTypeDataSourceV2.length; i++) {
      var selectedValues = selectedRateTypeDataSourceV2[i];
      listIDFromRateTypeStringSelected.add(selectedValues.value);
    }
    return listIDFromRateTypeStringSelected;
  }

  getArrayIDFromLoanTypeSelected() {
    return [loanTypeKeyDataSource[loanTypeSelectedIndex.value]];
  }

  getArrayIDFromPropertyTypeSelected() {
    List<String> listIDFromPropertyTypeSelected = [];
    // print("listIDFromPropertyTypeSelected.length ${listIDFromPropertyTypeSelected}");
    for (var i = 0; i < selectedProperTypeDataSourceV2.length; i++) {
      var item = selectedProperTypeDataSourceV2[i];
      listIDFromPropertyTypeSelected.add(item.value);
    }
    return listIDFromPropertyTypeSelected;
  }

  getArrayIDFromPropertyStatusSelected() {
    return [properStatusKeyDataSource[properStatusSelectedIndex.value]];
  }

  //* Function to call API which is used to export the selected packages
  Future<void> callAPIExportGeneratePackage() async {
    debugPrint("callAPIExportPackage:::");
    var arrayBankSelect = getIDFromBankStringSelectedToExport2();
    var param = {"packages": arrayBankSelect};
    if (arrayBankSelect.isNotEmpty) {
      Logger().d(param);
      var data = await CoreService()
          .postWithAuth(url: baseUrl + bankPackagesExportToPdf, body: param);
      if (data != null) {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            callAPIExportGeneratePackage();
          }
        } else {
          debugPrint("✅ok callAPIExportPackage $data");
          var url = data["url"];
          // ignore: deprecated_member_use
          await canLaunch(url) ? await launch(url) : debugPrint("error");
        }
      }
    } else {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please select packages to export",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    }
  }

  //* get the IDs of bank we need to export
  List<String> getIDFromBankStringSelectedToExport2() {
    List<String> listIDFromBankStringSelected = [];
    for (var i = 0; i < selectedPackgesForGenerateckages.length; i++) {
      listIDFromBankStringSelected
          .add(selectedPackgesForGenerateckages[i].toString());
    }
    return listIDFromBankStringSelected;
  }
}
