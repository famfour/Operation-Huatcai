// ignore_for_file: prefer_typing_uninitialized_variables

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Model/email_model.dart';
import 'package:iqrate/Model/response_model.dart/bank_submission_email_to_banker_reseponse_model.dart';
import 'package:iqrate/Model/response_model.dart/bank_submission_lawfirms_list_response_model.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';

class BankSubmissionEmailBankerScreenController extends GetxController {
  late TextEditingController ccTextEditingController;
  late TextEditingController remarksTextEditingController;
  RxList<EmailModel> emailList = <EmailModel>[].obs;
  Rx<BankSubmissionEmailToBankerResponseModel>
      bankSubmissionEmailToBankerResponseModel =
      BankSubmissionEmailToBankerResponseModel().obs;
  Rx<LawfirmListResponseModel> bankSubmissionLawfirmListResponseModel =
      LawfirmListResponseModel().obs;
  RxList<String> ccEmailList = <String>[].obs;
  List toRemoveEmail = [];
  int bankId = Get.arguments[0];
  int leadId = Get.arguments[1];
  List<String> documentsList = Get.arguments[2] ?? [];
  int? selectedExistingLawfirm;
  RxBool showPackageExpiredError = false.obs;

  @override
  void onInit() {
    ccTextEditingController = TextEditingController();
    remarksTextEditingController = TextEditingController();
    getLawfirmList();
    getEmailBody(bankId: bankId.toString(), leadId: leadId.toString());
    super.onInit();
  }

  getEmailBody({required String bankId, required String leadId}) async {
    debugPrint(baseUrl + emailToBankerEmailBodyUrl + leadId + '/' + bankId);
    try {
      Timer(const Duration(seconds: 0), () {
        Get.isDialogOpen ?? true
            ? const Offstage()
            : Get.dialog(
                const Center(
                  child: CircularProgressIndicator(),
                ),
                barrierDismissible: false);
      });
      var data = await CoreService().getWithAuth(
          url: baseUrl + emailToBankerEmailBodyUrl + leadId + '/' + bankId);
      if (data != null) {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            getEmailBody(bankId: bankId.toString(), leadId: leadId.toString());
          }
        } else if (data == 403) {
          showPackageExpiredError.value = true;
        } else {
          bankSubmissionEmailToBankerResponseModel.value =
              BankSubmissionEmailToBankerResponseModel.fromJson(data);
          debugPrint(bankSubmissionEmailToBankerResponseModel.toString());
          if (Get.isDialogOpen ?? false) Get.back();
        }
      } else {
        if (Get.isDialogOpen ?? false) Get.back();
      }
    } catch (e) {
      if (Get.isDialogOpen ?? false) Get.back();
      debugPrint(e.toString());
    }
  }

  getLawfirmList() async {
    debugPrint(baseUrl + bankSubmissionGetLawfirmsUrl + leadId.toString());
    try {
      Timer(const Duration(seconds: 0), () {
        Get.isDialogOpen ?? true
            ? const Offstage()
            : Get.dialog(
                const Center(
                  child: CircularProgressIndicator(),
                ),
                barrierDismissible: false);
      });
      var data = await CoreService().getWithAuth(
          url: baseUrl + bankSubmissionGetLawfirmsUrl + leadId.toString());
      if (data != null) {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            getLawfirmList();
          }
        } else {
          bankSubmissionLawfirmListResponseModel.value =
              LawfirmListResponseModel.fromJson(data);
          debugPrint(bankSubmissionLawfirmListResponseModel
              .value.lawFirmList!.length
              .toString());
          if (Get.isDialogOpen ?? false) Get.back();
        }
      } else {
        if (Get.isDialogOpen ?? false) Get.back();
      }
    } catch (e) {
      if (Get.isDialogOpen ?? false) Get.back();
      debugPrint(e.toString());
    }
  }

  sendEmailToBanker() async {
    // debugPrint(selectedExistingLawfirm.toString());
    // debugPrint(ccEmailList.toString());
    // debugPrint(remarksTextEditingController.text.toString());
    // debugPrint(documentsList.toString());

    debugPrint(baseUrl +
        emailToBankerEmailBodyUrl +
        leadId.toString() +
        '/' +
        bankId.toString());
    try {
      Timer(const Duration(seconds: 0), () {
        Get.isDialogOpen ?? true
            ? const Offstage()
            : Get.dialog(
                const Center(
                  child: CircularProgressIndicator(),
                ),
                barrierDismissible: false);
      });
      var data = await CoreService().postWithAuth(
          url: baseUrl +
              emailToBankerEmailBodyUrl +
              leadId.toString() +
              '/' +
              bankId.toString(),
          body: {
            "law_firm": selectedExistingLawfirm,
            // "cc_emails": ccEmailList,
            "remarks": remarksTextEditingController.text,
            "documents": documentsList
          });
      if (data != null) {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            sendEmailToBanker();
          }
        } else {
          if (Get.isDialogOpen ?? false) Get.back();
          Get.offNamed(bankSubmissionEmailSentBankerScreen, arguments: [
            leadId,
            bankId,
            bankSubmissionEmailToBankerResponseModel
          ]);
        }
      } else {
        if (Get.isDialogOpen ?? false) Get.back();
      }
    } catch (e) {
      if (Get.isDialogOpen ?? false) Get.back();
      debugPrint(e.toString());
    }
  }

  Iterable<Widget> get emailWidgets {
    return emailList.map((EmailModel email) {
      return Padding(
        padding: const EdgeInsets.all(4.0),
        child: Chip(
          label: Text(email.name),
          onDeleted: () {
            emailList.removeWhere((EmailModel entry) {
              return entry.email == email.email;
            });
            for (var element in ccEmailList) {
              if (element == email.email) {
                toRemoveEmail.add(element);
              }
            }
            ccEmailList.removeWhere((e) => toRemoveEmail.contains(e));
          },
        ),
      );
    });
  }
}
