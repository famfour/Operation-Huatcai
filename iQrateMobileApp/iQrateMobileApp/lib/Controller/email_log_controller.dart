import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Model/response_model.dart/email_log_response_model.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';

import '../Views/co_broke/LeadItemModel.dart';

class EmailLogController extends GetxController {
  var coBrokerData = LeadItemModel(clients: []);
  var isCoBroker = false;
  var isExpand = false.obs;
  var emailLogListModelAll = <EmailLogResponseModel>[].obs;
  var emailLogListModelLead = <EmailLogResponseModel>[].obs;
  var emailLogListModelBanker = <EmailLogResponseModel>[].obs;
  var emailLogListModelLawFirm = <EmailLogResponseModel>[].obs;

  RxBool isEmailLogShown = false.obs;
  RxList<int> emailLogContent = <int>[].obs;

  var leadID = "";

  Future<void> getEmailLogs(String leadId) async {
    debugPrint("Get Email Logs");

    var data =
        await CoreService().getWithAuth(url: baseUrl + emailLogUrl + leadId);
    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
      if (Get.isDialogOpen ?? false) Get.back();
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          getEmailLogs(leadId);
        }
      } else {
        List<dynamic> result = data;

        emailLogListModelAll.clear();
        emailLogListModelLead.clear();
        emailLogListModelBanker.clear();
        emailLogListModelLawFirm.clear();

        for (int i = 0; i < result.length; i++) {
          emailLogListModelAll
              .add(EmailLogResponseModel.fromJson(result.elementAt(i)));
        }

        for (var item in emailLogListModelAll) {
          if (item.emailType.toString() == "lead") {
            emailLogListModelLead.add(item);
          }

          if (item.emailType.toString() == "banker") {
            emailLogListModelBanker.add(item);
          }

          if (item.emailType.toString() == "law_firm") {
            emailLogListModelLawFirm.add(item);
          }
        }

        if (Get.isDialogOpen ?? false) Get.back();
      }
    }
  }

  Future<void> getEmailLogsWhenExpanded(String leadId) async {
    debugPrint("Get Email Logs");

    /*  Get.isDialogOpen ?? true
        ? const Offstage() //Used for avoiding the dialog to be open when the user is already on the next screen.
        : Get.dialog(
            const Center(
                child:
                    CircularProgressIndicator()), //Used for showing the loading dialog.
            barrierDismissible:
                false); //Used for preventing the user to dismiss the dialog.*/

    var data =
        await CoreService().getWithAuth(url: baseUrl + emailLogUrl + leadId);
    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
      if (Get.isDialogOpen ?? false) Get.back();
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          getEmailLogs(leadId);
        }
      } else {
        List<dynamic> result = data;

        emailLogListModelAll.clear();
        emailLogListModelLead.clear();
        emailLogListModelBanker.clear();
        emailLogListModelLawFirm.clear();

        for (int i = 0; i < result.length; i++) {
          emailLogListModelAll
              .add(EmailLogResponseModel.fromJson(result.elementAt(i)));
        }

        for (var item in emailLogListModelAll) {
          if (item.emailType.toString() == "lead") {
            emailLogListModelLead.add(item);
          }

          if (item.emailType.toString() == "banker") {
            emailLogListModelBanker.add(item);
          }

          if (item.emailType.toString() == "law_firm") {
            emailLogListModelLawFirm.add(item);
          }
        }

        if (Get.isDialogOpen ?? false) Get.back();
      }
    }
  }
}
