// ignore_for_file: unused_local_variable, avoid_function_literals_in_foreach_calls

import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:iqrate/Controller/login_controller.dart';
import 'package:iqrate/Controller/shared_prefs_controller.dart';
import 'package:iqrate/Controller/sign_up_controller.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/container_styles.dart';
import 'package:iqrate/DeviceManager/hive_string.dart';
import 'package:iqrate/DeviceManager/screen_constants.dart';
import 'package:iqrate/DeviceManager/strings.dart';
import 'package:iqrate/Model/response_model.dart/bank_forms_response_model.dart';
import 'package:iqrate/Model/response_model.dart/change_email_response_model.dart';
import 'package:iqrate/Model/response_model.dart/change_password_response_model.dart';
import 'package:iqrate/Model/response_model.dart/change_phone_response_model.dart';
import 'package:iqrate/Model/response_model.dart/delete_profile_image_response_model.dart';
import 'package:iqrate/Model/response_model.dart/email_verification_response_model.dart';
import 'package:iqrate/Model/response_model.dart/mobile_verification_response_model.dart';
import 'package:iqrate/Model/response_model.dart/profile_response_model.dart';
import 'package:iqrate/Model/response_model.dart/resend_email_code_response_model.dart';
import 'package:iqrate/Model/send_model.dart/change_email_send_model.dart';
import 'package:iqrate/Model/send_model.dart/change_password_response_model.dart';
import 'package:iqrate/Model/send_model.dart/change_phone_send_model.dart';
import 'package:iqrate/Model/send_model.dart/email_verification_send_model.dart';
import 'package:iqrate/Model/send_model.dart/mobile_verification_send_model.dart';
import 'package:iqrate/Model/send_model.dart/update_bank_account_info_send_model.dart';
import 'package:iqrate/Model/send_model.dart/update_profile_send_model.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/string_utils.dart';
import 'package:iqrate/Service/url.dart';
import 'package:image_picker/image_picker.dart';
import 'package:iqrate/Utils/config.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/secondary_button.dart';

class ProfileScreenController extends GetxController {
  var profileDetails = Data().obs;
  // var bankData = Banker().obs;
  Rx<ProfileResponseModel> userData = ProfileResponseModel().obs;
  // Rx<Banker> banker = Banker().obs;
  late TextEditingController nameController;
  late TextEditingController dateOfBirthController;
  late TextEditingController phoneController;
  late TextEditingController emailController;
  late TextEditingController oldPasswordController;
  late TextEditingController newPasswordController;
  late TextEditingController confirmPasswordController;
  late TextEditingController bankNameController;
  late TextEditingController accountNumberController;
  late TextEditingController otpController;
  late TextEditingController countryCodeController;
  late TextEditingController
      countryCodeLabelController; //phone code label text controller
  final picker = ImagePicker();
  Rx<File> images = File("").obs;

  final formKey =
      GlobalKey<FormState>(); // this key manage to form submit validation

  var isProfileExpanded = false.obs;
  var isNumberExpanded = false.obs;
  var isEmailExpanded = false.obs;
  var isPasswordExpanded = false.obs;
  var isBankExpanded = false.obs;

  @override
  Future<void> onInit() async {
    await getBankForms();

    nameController = TextEditingController();
    dateOfBirthController = TextEditingController();
    phoneController = TextEditingController();
    emailController =
        TextEditingController(); //initialize email text field controller
    oldPasswordController =
        TextEditingController(); //initialize password text field controller
    newPasswordController = TextEditingController();
    confirmPasswordController = TextEditingController();
    bankNameController = TextEditingController();
    accountNumberController = TextEditingController();
    otpController = TextEditingController();
    countryCodeController = TextEditingController();
    countryCodeLabelController = TextEditingController();

    getProfileData();

    // Timer(const Duration(seconds: 0), () {
    //   Get.isDialogOpen ?? true
    //       ? const Offstage()
    //       : Get.dialog(const Center(child: CircularProgressIndicator()),
    //           barrierDismissible: false);
    //   getProfileData();
    // });
    // getProfileData();

    super.onInit();
  }

  onCloseIconPress(bool isFromGoogleAuth, bool isMobileVerified) {
    if (isFromGoogleAuth && isMobileVerified) {
      Get.toNamed(dashboard);
    } else if (isFromGoogleAuth && !isMobileVerified) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please update mobile number",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else {
      Get.back();
    }
  }

  getProfileDataOnEnteringProfilePage() {
    debugPrint(baseUrl + getUserProfileUrl);
    Timer(const Duration(seconds: 0), () async {
      Get.isDialogOpen ?? true
          ? const Offstage()
          : Get.dialog(const Center(child: CircularProgressIndicator()),
              barrierDismissible: false);
      var data =
          await CoreService().getWithAuth(url: baseUrl + getUserProfileUrl);
      if (data == null) {
        if (Get.isDialogOpen ?? false) Get.back();
        return;
      } else {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            getProfileDataOnEnteringProfilePage();
          }
        } else {
          if (Get.isDialogOpen ?? false) Get.back();

          var result = ProfileResponseModel.fromJson(data);
          userData.value = result;
          if (result.status == 1) {
            nameController.text = result.fullName.toString();
            countryCodeLabelController.text =
                result.countryCodeLabel.toString();
            dateOfBirthController.text = result.dob!;
            if (result.bankers!.isNotEmpty) {
              //selectedBank = result.bankers![0]["bank_name"] ?? "";

              if (banks.contains(result.bankers![0]["bank_name"])) {
                selectedBank = result.bankers![0]["bank_name"] ?? "";

                debugPrint(
                    ">>>>>>>>>>>mybank: " + result.bankers![0]["bank_name"]);
              }

              accountNumberController.text =
                  result.bankers![0]["bank_ac_number"] ?? "";
            }
            phoneController.text = result.mobile.toString() == '1234123456'
                ? ''
                : result.mobile.toString();

            countryCodeController.text = "+" + result.countryCode.toString();
            emailController.text = result.email.toString();
            userData.value.isEmailVerified = result.isEmailVerified;
            userData.value.isMobileVerified = result.isMobileVerified;
            userData.value.email = result.email;
            userData.value.fullName = result.fullName;
            userData.value.phone = result.phone;
            userData.value.mobile = result.mobile;
            userData.value.id = result.id;
            userData.value.stripeCustomerId = result.stripeCustomerId;
            userData.value.isTwoFactorAuthenticationEnabled =
                result.isTwoFactorAuthenticationEnabled;
            userData.value.stripeCustomerId = result.stripeCustomerId;
            userData.value.stripePaymentMethodId = result.stripePaymentMethodId;
            userData.value.stripeSubscriptionId = result.stripeSubscriptionId;
            userData.value.membershipType = result.membershipType;
            userData.value.registerType = result.registerType;
            if (result.photo != null) {
              userData.value.photo = result.photo;
            }
            if (Get.isDialogOpen ?? false) Get.back();

            userData.refresh();
            profileDetails.refresh();
            if (Get.isDialogOpen ?? false) Get.back();
            return;
          } else {
            if (Get.isDialogOpen ?? false) Get.back();
            return;
          }
        }
      }
    });

    Timer(const Duration(seconds: 5), () {
      if (Get.isDialogOpen ?? false) Get.back();
    });
  }

// ignore: todo
// TODO: Need to check this
  Future<ProfileResponseModel> getProfileData() async {
    debugPrint("=============Call getProfileData()=============");
    //initialized
    nameController = TextEditingController();
    dateOfBirthController = TextEditingController();
    phoneController = TextEditingController();
    emailController =
        TextEditingController(); //initialize email text field controller
    oldPasswordController =
        TextEditingController(); //initialize password text field controller
    newPasswordController = TextEditingController();
    confirmPasswordController = TextEditingController();
    bankNameController = TextEditingController();
    accountNumberController = TextEditingController();
    otpController = TextEditingController();
    countryCodeController = TextEditingController();
    countryCodeLabelController = TextEditingController();

    var data =
        await CoreService().getWithAuth(url: baseUrl + getUserProfileUrl);
    if (data == null) {
      return ProfileResponseModel();
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          getProfileData();
        }
      } else {
        var result = ProfileResponseModel.fromJson(data);
        userData.value = result;
        if (result.status == 1) {
          nameController.text = result.fullName.toString();
          countryCodeLabelController.text = result.countryCodeLabel.toString();
          // debugPrint("*************");
          // debugPrint(countryCodeLabelController.text);
          // debugPrint("*************");

          /*dateOfBirthController.text = result.dob!.day.toString() +
            "-" +
            result.dob!.month.toString() +
            "-" +
            result.dob!.year.toString();*/

          dateOfBirthController.text = result.dob!;
          if (result.bankers!.isNotEmpty) {
            //selectedBank = result.bankers![0]["bank_name"] ?? "";
            accountNumberController.text =
                result.bankers![0]["bank_ac_number"] ?? "";

            //selectedBank!.name = bankNameController.text;
            //selectedBank!.name = "Islamic Bank of Asia";

            if (banks.contains(result.bankers![0]["bank_name"])) {
              selectedBank = result.bankers![0]["bank_name"] ?? "";

              debugPrint(
                  ">>>>>>>>>>>mybank: " + result.bankers![0]["bank_name"]);
            }
          }
          phoneController.text = result.mobile.toString() == '1234123456'
              ? ''
              : result.mobile.toString();

          countryCodeController.text = "+" + result.countryCode.toString();
          emailController.text = result.email.toString();
          userData.value.isEmailVerified = result.isEmailVerified;
          userData.value.isMobileVerified = result.isMobileVerified;
          userData.value.email = result.email;
          userData.value.fullName = result.fullName;
          userData.value.phone = result.phone;
          userData.value.mobile = result.mobile;
          userData.value.id = result.id;
          userData.value.stripeCustomerId = result.stripeCustomerId;
          userData.value.isTwoFactorAuthenticationEnabled =
              result.isTwoFactorAuthenticationEnabled;
          userData.value.stripeCustomerId = result.stripeCustomerId;
          userData.value.stripePaymentMethodId = result.stripePaymentMethodId;
          userData.value.stripeSubscriptionId = result.stripeSubscriptionId;
          userData.value.membershipType = result.membershipType;
          userData.value.registerType = result.registerType;

          // userData.value.bankers![0]["bank_name"] =
          //     result.bankers![0]["bank_name"];
          // userData.value.bankers![0]["bank_ac_number"] =
          //     result.bankers![0]["bank_ac_number"];
          if (result.photo != null) {
            userData.value.photo = result.photo;
          }
          userData.refresh();
          profileDetails.refresh();
          return result;
        } else {
          return result;
          // Get.snackbar(StringUtils.error, "${data.message},",
          //     snackPosition: SnackPosition.TOP);
        }
      }
      return ProfileResponseModel();
    }
  }

  onUpdatePhoneButtonPressed() async {
    if (phoneController.text.trim() == userData.value.mobile.toString() &&
        userData.value.isMobileVerified == 1) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter a different number",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return;
    }

    if (phoneController.text.trim().isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter your phone number",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return;
    }

    if (phoneController.text.trim().isNotEmpty &&
        phoneController.text.isNumericOnly) {
      if (userData.value.isMobileVerified == 1) {
        onUpdatePhone(false);
      } else {
        Get.put(SignUpController()).isRedirectFromSignupScreen = true;
        Get.put(SignUpController()).phoneController.text = phoneController.text;
        Get.put(SignUpController()).countryCodeController.text =
            countryCodeController.text;
        Get.put(SignUpController()).countryCodeLabelController.text =
            countryCodeLabelController.text;

        await apiCallResendSMSCode();
        Get.toNamed(mobileVerification);
      }
    }
  }

  onUpdateProfileButtonPressed() {
    try {
      if (nameController.text.isNotEmpty &&
          dateOfBirthController.text.isNotEmpty &&
          nameController.text != ' ') {
        onUpdateProfileData();
      } else {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "All fields are mandatory",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      }
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  // onUpdateDobButtonPressed() {
  //   if (dateOfBirthController.text.isNotEmpty) {
  //     onUpdateProfileData();
  //     Get.focusScope!.unfocus();
  //   }
  // }

  onUpdateProfileData() async {
    try {
      Get.isDialogOpen ?? true
          ? const Offstage()
          : Get.dialog(
              const Center(
                child: CircularProgressIndicator(),
              ),
              barrierDismissible: false);
      UpdateProfileSendModel model = UpdateProfileSendModel(
        name: nameController.text,
        dob: dateOfBirthController.text,
      );
      await CoreService()
          .putWithAuth(url: baseUrl + updateNameDobGoogle, body: model.toJson())
          .then((value) async {
        if (value != null) {
          if (value == 401) {
            bool success =
                await CoreService().getNewAccessTokenWithRefreshToken();
            if (success) {
              onUpdateProfileData();
            }
          } else {
            Get.back();
            Future.delayed(const Duration(milliseconds: 0), () {
              FocusManager.instance.primaryFocus!.unfocus();
              Fluttertoast.showToast(
                timeInSecForIosWeb: 3,
                msg: value["message"],
                backgroundColor: Colors.green,
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.BOTTOM,
                textColor: Colors.white,
                fontSize: 20.0,
              );
            });
          }
        } else {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Oops!, some error occurred",
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }
      });
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  onUpdateEmailButtonPressed() {
    if (emailController.text.isNotEmpty) {
      onUpdateEmail();
    }
  }

  var phoneVerificationToken = "";
  onUpdatePhone(bool isResendCode) async {
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);
    ChangeMobileSendModel model = ChangeMobileSendModel(
      country_code: removePlusSymbolCountryCode(countryCodeController.text),
      country_code_label: countryCodeLabelController.text,
      mobile: phoneController.text.toString(),
    );
    var data = await CoreService().putWithAuth(
      url: baseUrl + changeMobileUrl,
      body: model.toJson(),
    );
    if (Get.isDialogOpen ?? false) Get.back();

    if (data == null) {
      if (Get.isDialogOpen ?? false) Get.back();
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          onUpdatePhone(isResendCode);
        }
      } else {
        var result = ChangePhoneResponseModel.fromJson(data);
        phoneVerificationToken = result.verification_token!;

        if (!isResendCode) {
          Get.put(SignUpController()).isRedirectFromSignupScreen = false;
          Get.toNamed(mobileVerification);
        }
      }
    }
  }

  //resend api call for phone verification only

  apiCallResendSMSCode() async {
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);
    UpdateProfileSendModel model = UpdateProfileSendModel(
      name: nameController.text,
      dob: dateOfBirthController.text,
    );

    String phone = "", code = "";
    code = countryCodeController.text.replaceAll("+", "");
    phone = phoneController.text;

    //The entered url is base+resendEmailCodeUrl+the email address of the user.
    var data = await CoreService().putWithoutAuth(
        url: baseUrl +
            resendSmsCodeUrl +
            code +
            phone); //Send the otp to the server.
    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
    } else {
      //if the status code is 400, the server sends an error, which is displayed via the snackbar.
      if (data["resultcode"] == 400) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: data["errMessage"].toString(),
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      } else {
        //If the status code is 200, the server sends a success message, which is displayed via the snackbar
        //if (data["resultcode"] == 200) {

        //var result = MobileVerificationResponseModel.fromJson(data);
        var result = ChangePhoneResponseModel.fromJson(data);
        phoneVerificationToken = result.verification_token!;
        Get.put(SignUpController()).isMobileVerificationFromProfileScreen =
            true;

        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: result.message!,
            backgroundColor: Colors.green,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
        // }
      }

      if (Get.isDialogOpen ?? false) Get.back();
    }
  }

  onUpdateEmail() async {
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);
    ChangeEmailSendModel model = ChangeEmailSendModel(
      email: emailController.text.toString().toLowerCase(),
    );
    var data = await CoreService().putWithAuth(
      url: baseUrl + changeEmailUrl,
      body: model.toJson(),
    );
    if (Get.isDialogOpen ?? false) {
      Get.back();
    }
    if (data == null) {
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          onUpdateEmail();
        }
      } else {
        var result = ChangeEmailResponseModel.fromJson(data);
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Verification link sent",
            backgroundColor: Colors.green,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      }
    }
  }

  addPlusSymbolCountryCode(String code) {
    return "+" + code;
  }

  removePlusSymbolCountryCode(String code) {
    return code.replaceAll("+", "");
  }

  onSubmitOtpPhoneVerification(code) async {
    Get.isDialogOpen ?? true
        ? const Offstage() //Used for avoiding the dialog to be open when the user is already on the next screen.
        : Get.dialog(
            const Center(
                child:
                    CircularProgressIndicator()), //Used for showing the loading dialog.
            barrierDismissible:
                false); //Used for preventing the user to dismiss the dialog.
    MobileVerificationSendModel mobileVerificationSendModel =
        MobileVerificationSendModel(
      verification_code: code,
      verification_token: phoneVerificationToken,
      type: "Mobile",
    ); //Create a model for sending the otp to the server.
    //
    //The entered url is base+emailverifyurl+the email address of the user.
    var data = await CoreService().putWithAuth(
        url: baseUrl + verifyWithCodeUrl,
        body:
            mobileVerificationSendModel.toJson()); //Send the otp to the server.

    if (Get.isDialogOpen ?? false) Get.back();

    if (data != null) {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          onSubmitOtpPhoneVerification(code);
        }
      } else {
        //If the status code is 200, the server sends a success message, which is displayed via the snackbar. and the data is mapped in the response model. The control then is moved to the next screen which is the mobile verification screen.
        var result = MobileVerificationResponseModel.fromJson(data);

        //Get.offAndToNamed(loginScreen); //Used for navigating to the next screen.
        Get.back();
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: data["message"].toString(),
            backgroundColor: Colors.green,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
        getProfileData();
      }
    }
  }

  onSubmitOtpEmailVerification() async {
    Get.isDialogOpen ?? true
        ? const Offstage() //Used for avoiding the dialog to be open when the user is already on the next screen.
        : Get.dialog(
            const Center(
                child:
                    CircularProgressIndicator()), //Used for showing the loading dialog.
            barrierDismissible:
                false); //Used for preventing the user to dismiss the dialog.
    EmailVerificationSendModel emailVerificationSendModel =
        EmailVerificationSendModel(
      verification_code: otpController.text,
    ); //Create a model for sending the otp to the server.
    //
    //The entered url is base+emailverifyurl+the email address of the user.
    var data = await CoreService().putWithoutAuth(
        url: baseUrl + emailVerificationUrl + emailController.text,
        body:
            emailVerificationSendModel.toJson()); //Send the otp to the server.
    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
    } else {
      //if the status code is 400, the server sends an error, which is displayed via the snackbar.
      if (data["resultcode"] == 400) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: data["errMessage"].toString(),
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      } else {
        //If the status code is 200, the server sends a success message, which is displayed via the snackbar. and the data is mapped in the response model. The control then is moved to the next screen which is the mobile verification screen.
        var result = EmailVerificationResponseModel.fromJson(data);
        if (result.message == "Email has been verified") {
          Get.back(); //Used for navigating to the next screen.
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "OTP verified",
              backgroundColor: Colors.green,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        } else {
          // Get.snackbar(StringUtils.error, data["errMessage"].toString(),
          //     snackPosition: SnackPosition.TOP, backgroundColor: Colors.white);
        }
      }
    }
  }

  void toAccountInformation() {
    showImagePickerOption();
  }

  showImagePickerOption() {
    Get.bottomSheet(Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
            topRight: Radius.circular(ScreenConstant.sizeLarge),
            topLeft: Radius.circular(ScreenConstant.sizeLarge)),
        color: Colors.white,
      ),
      child: Wrap(
        children: <Widget>[
          ListTile(
            leading: const Icon(Icons.camera, color: AppColors.background),
            title: const Text("Camera"),
            onTap: () {
              Get.back();
              getImage(ImageSource.camera);
              //   onQuote(index);
            },
          ),
          ListTile(
            leading: const Icon(Icons.photo, color: AppColors.background),
            title: const Text(
              "Gallery",
            ),
            onTap: () {
              Get.back();
              getImage(ImageSource.gallery);
            },
          ),
          userData.value.photo == null ||
                  userData.value.photo.toString().isEmpty
              ? const SizedBox()
              : ListTile(
                  leading:
                      const Icon(Icons.delete, color: AppColors.background),
                  title: const Text(
                    "Delete",
                  ),
                  onTap: () {
                    Get.back();
                    onDeleteImage();
                  },
                ),
        ],
      ),
    ));
  }

  Future getImage(ImageSource source) async {
    final pickedFile = await picker.pickImage(
      source: source,
      imageQuality: 30,
    );
    if (pickedFile != null) {
      debugPrint(pickedFile.path);
      images.value = File(pickedFile.path);
      profileImageUpload();
    } else {
      debugPrint('No image selected.');
    }
  }

  /*==================profile image upload====================*/
  profileImageUpload() async {
    Get.dialog(const Center(child: CircularProgressIndicator()),
        barrierDismissible: false);
    var data = await CoreService().putFileWithAuth(
        upload: images.value.readAsBytesSync(),
        filename: images.value.path.split("/").last,
        url: baseUrl + updateImageUrl,
        key: 'upload');
    if (data == null) {
      Get.back();
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          profileImageUpload();
        }
      } else {
        Get.back();
        var result = data;
        if (result != null) {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Uploaded",
              backgroundColor: Colors.green,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
          getProfileData();
          userData.refresh();
          profileDetails.refresh();
        } else {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: result.message!,
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }
      }
    }
  }

  onDeleteImage() async {
    Get.dialog(const Center(child: CircularProgressIndicator()),
        barrierDismissible: false);
    var data = await CoreService()
        .patchWithAuthImageDelete(url: baseUrl + deleteImageUrl, body: null);
    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Something went wrong",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          onDeleteImage();
        }
      } else {
        Get.back();
        var result = DeleteProfileImageResponseModel.fromJson(data);
        if (result.data!.id != null) {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Deleted Successfully",
              backgroundColor: Colors.green,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
          userData.refresh();
          profileDetails.refresh();
          getProfileData();
        } else {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Something went wrong",
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }
      }
    }
  }

  onUpdateBankDetailsButtonPress() {
    try {
      if (selectedBank == null && accountNumberController.text.isEmpty) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Please enter bank details",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      } else if (selectedBank == null || selectedBank!.isEmpty) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Please select your bank",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      } else if (accountNumberController.text.isEmpty) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Please enter your account number",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      } else if (selectedBank!.trim().isEmpty ||
          accountNumberController.text.trim().isEmpty) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Please enter bank details",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      } else if (accountNumberController.text.isNumericOnly == false) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Account number invalid (Numeric only)",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      } else {
        updateBankDetails();
      }
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  updateBankDetails() async {
    try {
      Get.isDialogOpen ?? true
          ? const Offstage()
          : Get.dialog(
              const Center(
                child: CircularProgressIndicator(),
              ),
              barrierDismissible: false);
      UpdateBankAccountInformationSendModel model =
          UpdateBankAccountInformationSendModel(
        bankName: selectedBank!,
        accountNumber: accountNumberController.text.trim(),
      );
      await CoreService()
          .putWithAuth(url: baseUrl + updateBankAccInfo, body: model.toJson())
          .then((value) async {
        if (value != null) {
          if (value == 401) {
            bool success =
                await CoreService().getNewAccessTokenWithRefreshToken();
            if (success) {
              updateBankDetails();
            }
          } else {
            Get.back();
            Future.delayed(const Duration(milliseconds: 0), () {
              FocusManager.instance.primaryFocus!.unfocus();
              Fluttertoast.showToast(
                timeInSecForIosWeb: 3,
                msg: "Account added successfully",
                backgroundColor: Colors.green,
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.BOTTOM,
                textColor: Colors.white,
                fontSize: 20.0,
              );
            });
          }
        } else {
          Get.back();
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Oops! Some error occurred",
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }
      });
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  clearPasswordField() {
    oldPasswordController.clear();
    newPasswordController.clear();
    confirmPasswordController.clear();
  }

  onChangePasswordButtonTap() {
    if (oldPasswordController.text.isEmpty &&
        newPasswordController.text.isEmpty &&
        confirmPasswordController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "All fields are mandatory",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (oldPasswordController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter your current password",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (newPasswordController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter your new password",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (confirmPasswordController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please confirm your passowrd",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (!AppConfig.validatePassword(oldPasswordController.text) ||
        !AppConfig.validatePassword(newPasswordController.text) ||
        !AppConfig.validatePassword(confirmPasswordController.text)) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: AppLabels.passwordValidation2,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });

      return;
    } else if (!AppConfig.validatePassword2(oldPasswordController.text) ||
        !AppConfig.validatePassword2(newPasswordController.text) ||
        !AppConfig.validatePassword2(confirmPasswordController.text)) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: AppLabels.passwordValidation3,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });

      return;
    } else {
      if (oldPasswordController.text == newPasswordController.text) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Old and new password cannot be the same",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      } else if (newPasswordController.text != confirmPasswordController.text) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "New and confirm password mismatch",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      } else if (newPasswordController.text.length < 8 &&
          confirmPasswordController.text.length < 8) {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Password should be 8 or more characters",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      } else {
        changePassword();
      }
    }
  }

  changePassword() async {
    Get.isDialogOpen ?? true
        ? const Offstage() //Used for avoiding the dialog to be open when the user is already on the next screen.
        : Get.dialog(
            const Center(
                child:
                    CircularProgressIndicator()), //Used for showing the loading dialog.
            barrierDismissible:
                false); //Used for preventing the user to dismiss the dialog.
    ChangePasswordSendModel model = ChangePasswordSendModel(
      oldPassword: oldPasswordController.text,
      newPassword: newPasswordController.text,
      confirmPassword: confirmPasswordController.text,
    );

    var data = await CoreService().putWithAuth(
      url: baseUrl + changePasswordUrl,
      body: model.toJson(),
    );
    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          changePassword();
        }
      } else {
        var result = ChangePasswordResponseModel.fromJson(data);
        if (result.message!.isNotEmpty) {
          Get.back();
          // clearPasswordField();
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: result.message.toString(),
              backgroundColor: Colors.green,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        } else {
          Get.back();
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Some error occurred",
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }
      }
    }

    Timer(const Duration(seconds: 7), () {
      if (Get.isDialogOpen ?? false) Get.back();
    });
  }

  Rx<BankFormsResponseModel> bankFormsResponseModel =
      BankFormsResponseModel().obs;

  //String? selectedBank = "Islamic Bank of Asia";
  String? selectedBank;

  var banks = <String>[].obs;

  getBankForms() async {
    var data =
        await CoreService().getWithAuth(url: baseUrl + resourcesBankFormsUrls);
    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          getBankForms();
        }
      } else {
        debugPrint("getBankForms:: " + data.toString());
        bankFormsResponseModel.value = BankFormsResponseModel.fromJson(data);
        for (int i = 0; i < bankFormsResponseModel.value.results!.length; i++) {
          banks.add(bankFormsResponseModel.value.results![i].name!);
        }
        if (bankFormsResponseModel.value.next != null) {
          getMoreBanks(bankFormsResponseModel.value.next!);
        }
        // bankFormsResponseModel.value.results!.forEach((element) {
        //   banks.add(element.name!);
        // });
      }
    }
  }

  getMoreBanks(url) async {
    var data = await CoreService().getWithAuth(url: url);
    bankFormsResponseModel.value = BankFormsResponseModel.fromJson(data);
    for (int i = 0; i < bankFormsResponseModel.value.results!.length; i++) {
      banks.add(bankFormsResponseModel.value.results![i].name!);
    }
    if (bankFormsResponseModel.value.next != null) {
      getMoreBanks(bankFormsResponseModel.value.next!);
    }
  }

  double windowHeightC = Get.height;
  double windowWidthC = Get.width;

  deleteMyAccountDialog() async {
    /*Get.defaultDialog(title: StringUtils.attention, content: const Text("Please note that once you delete your account, you cannot restore and will have no access to all the data in your account. You will not be able to sign up a new account with the same email id and mobile number."), cancel: const Text("Cancel"),onCancel:(){Get.back();},confirm: const Text("Confirm"), onConfirm: (){
      deleteMyAccountApiCall();
    });*/

    Get.defaultDialog(
      title: "",
      content: SizedBox(
        //height: windowHeightC * 0.28,
        width: windowWidthC * 0.9,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 0.0, horizontal: 18),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  children: [
                    Text(
                      StringUtils.attention,
                      style: TextStyle(
                        color: Colors.red,
                        fontSize: FontSize.s26,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Please note that once you delete your account, you cannot restore and will have no access to all the data in your account. You will not be able to sign up a new account with the same email id and mobile number.",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: FontSize.s18,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: windowHeightC * 0.05),
                PrimaryButton(
                  windowHeight: windowHeightC,
                  windowWidth: windowWidthC,
                  buttonTitle: "Confirm",
                  onPressed: () {
                    deleteMyAccountApiCall();
                  },
                ),
                SizedBox(height: windowHeightC * 0.03),
                SecondaryButton(
                  kGradientBoxDecoration:
                      ContainerStyles.kGradientBoxDecorationSecondaryButton,
                  kInnerDecoration:
                      ContainerStyles.kInnerDecorationSecondaryButton,
                  windowHeight: windowHeightC,
                  windowWidth: windowWidthC,
                  buttonTitle: "Cancel",
                  onPressed: () {
                    Get.back();
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  deleteMyAccountApiCall() async {
    Get.isDialogOpen ?? true
        ? const Offstage() //Used for avoiding the dialog to be open when the user is already on the next screen.
        : Get.dialog(
            const Center(
                child:
                    CircularProgressIndicator()), //Used for showing the loading dialog.
            barrierDismissible:
                false); //Used for preventing the user to dismiss the dialog.

    var data = await CoreService().patchWithAuthImageDelete(
      url: baseUrl + deleteMyAccountUrl,
    );
    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          deleteMyAccountApiCall();
        }
      } else {
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Account successfully deleted",
            backgroundColor: Colors.green,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
        clearUserInfo();
      }
    }

    Timer(const Duration(seconds: 7), () {
      if (Get.isDialogOpen ?? false) Get.back();
    });
  }

  late Box hive = Hive.box(HiveString.hiveName);

  clearUserInfo() async {
    Get.find<SharedPrefController>().prefs.setInt("counter", 0);
    hive.deleteAll([
      HiveString.token,
      HiveString.userId,
      HiveString.refreshToken,
    ]);

    Get.offAllNamed(loginScreen);

    Get.find<SignInController>().isGoogleLogin2FAEnable = false;
    Get.find<SignInController>().isAppleLogin2FAEnable = false;
  }
}
