// ignore_for_file: non_constant_identifier_names, invalid_use_of_protected_member

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Model/response_model.dart/ResponseEmailLawFirmTemplate.dart';
import 'package:iqrate/Model/response_model.dart/bank_submission_lawfirms_list_response_model.dart';
import 'package:iqrate/Model/response_model.dart/documents_drawer_response_model.dart';
import 'package:iqrate/Model/send_model.dart/SendLawFirmSubmit.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';
import 'package:iqrate/Views/law_firm_submission_document_drawer_screen.dart';

class LawFirmSubmissionController extends GetxController {
  late RxDouble windowHeight;
  late RxDouble windowWidth;
  late RxInt selectedIndex;

  TextEditingController remarksController =
      TextEditingController(); //remarks Interest text controller

  final formKey = GlobalKey<FormState>();

  var isExpand = false.obs;
  var isChecked = true.obs;

  String? leadID = "";
  String? documentsDrawerID = "";

  TextEditingController legalFeesController = TextEditingController();
  TextEditingController lawFirmEmailController = TextEditingController();

  var lawFirmEmailTemplate = ResponseEmailLawFirmTemplate().obs;

  RxList<LawFirmModel> lawFirmList = <LawFirmModel>[].obs;
  LawFirmModel? selectedLawFirm;

  RxList<Documents> documentUploadList = <Documents>[].obs;
  List<String> documentUploadIdList = [];

  @override
  onInit() {
    selectedIndex = 0.obs;
    windowHeight = Get.height.obs;
    windowWidth = Get.width.obs;

    remarksController =
        TextEditingController(); // current Interest text controller

    super.onInit();
  }

  onTapSubmit() {
    if (selectedLawFirm == null &&
        lawFirmEmailController.text.isEmpty &&
        legalFeesController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "All fields are mandatory",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (selectedLawFirm == null) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please select the law firm",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else if (legalFeesController.text.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter legal fee",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else {
      postLawFirmData();
    }
  }

  postLawFirmData() async {
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);

    var sendModel = SendLawFirmSubmit(
        ownLawFirmEmail: lawFirmEmailController.text,
        lawFirm: selectedLawFirm!.id!,
        ownLawFirmName: selectedLawFirm!.name!,
        remarks: remarksController.text,
        documentIds: documentUploadIdList);

    var data = await CoreService().postWithAuth(
        url: baseUrl + emailLawFirmPayoutUrl + leadID!,
        body: sendModel.toJson());

    try {
      if (Get.isDialogOpen ?? false) Get.back();

      if (data == null) {
      } else {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            postLawFirmData();
          }
        } else {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: data['detail'],
              backgroundColor: Colors.green,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }
      }
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  getLawFirmList() async {
    getEmailTemplate();

    debugPrint(baseUrl + lawFirmPayoutUrl + leadID.toString());

    try {
      Timer(const Duration(seconds: 0), () {
        Get.isDialogOpen ?? true
            ? const Offstage()
            : Get.dialog(
                const Center(
                  child: CircularProgressIndicator(),
                ),
                barrierDismissible: false);
      });
      var data = await CoreService()
          .getWithAuth(url: baseUrl + lawFirmPayoutUrl + leadID.toString());
      if (data != null) {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            getLawFirmList();
          }
        } else {
          var result = LawfirmListResponseModel.fromJson(data);
          lawFirmList.value = result.lawFirmList!;
          debugPrint("LawfirmList::: " + lawFirmList.value.length.toString());

          if (Get.isDialogOpen ?? false) Get.back();
        }
      } else {
        if (Get.isDialogOpen ?? false) Get.back();
      }
    } catch (e) {
      if (Get.isDialogOpen ?? false) Get.back();
      debugPrint(e.toString());
    }
  }

  getEmailTemplate() async {
    debugPrint(baseUrl + emailLawFirmPayoutUrl + leadID.toString());
    try {
      Timer(const Duration(seconds: 0), () {
        Get.isDialogOpen ?? true
            ? const Offstage()
            : Get.dialog(
                const Center(
                  child: CircularProgressIndicator(),
                ),
                barrierDismissible: false);
      });
      var data = await CoreService().getWithAuth(
          url: baseUrl + emailLawFirmPayoutUrl + leadID.toString());
      if (data != null) {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            getLawFirmList();
          }
        } else {
          var result = ResponseEmailLawFirmTemplate.fromJson(data);
          lawFirmEmailTemplate.value = result;

          if (Get.isDialogOpen ?? false) Get.back();
        }
      } else {
        if (Get.isDialogOpen ?? false) Get.back();
      }
    } catch (e) {
      if (Get.isDialogOpen ?? false) Get.back();
      debugPrint(e.toString());
    }
  }

  void onTapAddJoint() {}

  void onTapSendPDPAApproval() {}

  void addDocumentsScreen() {
    Get.to(const LawFirmSubmissionDocumentDrawerScreen(),
        arguments: ["", "", documentsDrawerID]);
  }
}
