import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/profile_controller.dart';
import 'package:iqrate/Controller/subscription_plans_view_controller.dart';
import 'package:iqrate/Controller/upgrade_downgrade_controller.dart';
import 'package:iqrate/Model/response_model.dart/create_subscription_response_model.dart';
import 'package:iqrate/Model/response_model.dart/upgrade_downgrade_response_model.dart';
import 'package:iqrate/Model/send_model.dart/create_subscription_send_model.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';

class PaymentDetailsViewController extends GetxController {
  late Rx<String> amount = ''.obs;
  late Rx<String> planTerm = ''.obs;
  late Rx<String> planType = ''.obs;
  late Rx<String> planId = ''.obs;
  late Rx<String> planExpiration = ''.obs;
  late TextEditingController nameOnCardTextEditingController;
  late TextEditingController cardNumberTextEditingController;
  late TextEditingController expirationMonthTextEditingController;
  late TextEditingController expirationYearTextEditingController;
  late TextEditingController cvcTextEditingController;
  late TextEditingController countryTextEditingController;
  late TextEditingController postalCodeController;
  Rx<CardDetails> cardDetails = CardDetails().obs;
  RxString? stripePaymentMethodId = ''.obs;

  late RxBool checkedValue = true.obs;

  clearPaymentDetailsFields() {
    nameOnCardTextEditingController.clear();
    cardNumberTextEditingController.clear();
    expirationMonthTextEditingController.clear();
    expirationYearTextEditingController.clear();
    cvcTextEditingController.clear();
    countryTextEditingController.clear();
    postalCodeController.clear();
  }

  final UpgradeDowngradeController upgradeDowngradeController =
      Get.put(UpgradeDowngradeController());

  final ProfileScreenController profileScreenController =
      Get.put(ProfileScreenController());
  final SubscriptionPlansViewController subscriptionPlansViewController =
      Get.find();

  @override
  void onInit() {
    getParameters();
    nameOnCardTextEditingController = TextEditingController();
    cardNumberTextEditingController = TextEditingController();
    expirationMonthTextEditingController = TextEditingController();
    expirationYearTextEditingController = TextEditingController();
    cvcTextEditingController = TextEditingController();
    countryTextEditingController = TextEditingController();
    postalCodeController = TextEditingController();
    //debugCreateSub();
    clearPaymentDetailsFields();
    stripePaymentMethodId?.value =
        profileScreenController.userData.value.stripePaymentMethodId ?? '';
    super.onInit();
  }

  debugCreateSub() {
    if (kDebugMode) {
      print(">>>>>>>>>>>>>>>debug card>>>");
      nameOnCardTextEditingController.text = "Test";
      cardNumberTextEditingController.text = "4242424242424242";
      cvcTextEditingController.text = "879";
      expirationMonthTextEditingController.text = "10";
      expirationYearTextEditingController.text = "23";
      postalCodeController.text = "555555";
    }
  }

  getPlanExiprationDuration(Rx<String> planTerm) {
    if (planTerm.value == 'Daily') {
      DateTime now = DateTime.now().add(const Duration(days: 1));
      planExpiration.value =
          'Plan will end on ${now.day.toString().padLeft(2, '0')}/${now.month.toString().padLeft(2, '0')}/${now.year.toString()}';
    } else if (planTerm.value == 'Monthly') {
      DateTime now = DateTime.now().add(const Duration(days: 30));
      planExpiration.value =
          'Plan will end on ${now.day.toString().padLeft(2, '0')}/${now.month.toString().padLeft(2, '0')}/${now.year.toString()}';
    } else {
      DateTime now = DateTime.now().add(const Duration(days: 365));
      planExpiration.value =
          'Plan will end on ${now.day.toString().padLeft(2, '0')}/${now.month.toString().padLeft(2, '0')}/${now.year.toString()}';
    }
  }

  getParameters() async {
    Map<String, dynamic> popedScreenData = await Get.arguments;
    debugPrint(popedScreenData.toString());
    amount.value = Get.arguments["amount"];
    planTerm.value = Get.arguments["planTerm"];
    planType.value = Get.arguments["planType"];
    planId.value = Get.arguments["planId"];

    getPlanExiprationDuration(planTerm);
  }

  Future<void> onTapSubmit(
      card, String nameOnCard, String country, String postalCode) async {
    if (card.value.number == null ||
        card.value.expirationYear == null ||
        card.value.cvc == null ||
        card.value.expirationMonth == null ||
        card.value.number.length != 16) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter valid card details",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return;
    } else {
      debugPrint('****!!!!!!!!');
      debugPrint(">>>>>>>>>>" + card.value.toString());

      Get.isDialogOpen ?? true
          ? const Offstage()
          : Get.dialog(
              const Center(
                child: CircularProgressIndicator(),
              ),
              barrierDismissible: false);
      try {
        await Stripe.instance.dangerouslyUpdateCardDetails(card.value);
        final billingDetails = BillingDetails(
          email: null,
          phone: null,
          name: nameOnCard,
          address: Address(
              city: null,
              country: null,
              line1: null,
              line2: null,
              state: null,
              postalCode: postalCode),
        );
        await Stripe.instance
            .createPaymentMethod(
          //  PaymentMethodParams.card(billingDetails: billingDetails),
          PaymentMethodParams.card(
              paymentMethodData:
                  PaymentMethodData(billingDetails: billingDetails)),
        )
            .then((PaymentMethod tokenData) {
          if (profileScreenController.userData.value.stripeCustomerId != null) {
            createPaymentMethod(body: {
              "type": "card",
              "customer_id":
                  profileScreenController.userData.value.stripeCustomerId,
              "payment_method_token": tokenData.id
            });
          } else {
            // When the customer id is null
            Get.back();
            Future.delayed(const Duration(milliseconds: 0), () {
              FocusManager.instance.primaryFocus!.unfocus();
              Fluttertoast.showToast(
                timeInSecForIosWeb: 3,
                msg: "Please try again later",
                backgroundColor: Colors.red,
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.BOTTOM,
                textColor: Colors.white,
                fontSize: 20.0,
              );
            });
          }
        });
        return;
      } catch (e) {
        debugPrint('Error: $e');
        Get.back();
        Future.delayed(const Duration(milliseconds: 0), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Card information incorrect",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
      }
    }
  }

  createPaymentMethod({required Map<String, dynamic> body}) async {
    debugPrint(baseUrl + createPaymentMethodUrl);
    try {
      await CoreService()
          .postWithAuth(url: baseUrl + createPaymentMethodUrl, body: body)
          .then((value) async {
        debugPrint(value.toString());
        //createSubscription();
        if (value == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            createPaymentMethod(body: body);
          }
        } else {
          onDetachPaymentButtonPressed(
            planId.value,
            planType.value,
          );
        }
      });
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  createSubscription() async {
    debugPrint(">>>>>user current membership>>>>" +
        profileScreenController.userData.value.membershipType.toString());

    /*if(profileScreenController.userData.value.membershipType.toString().isNotEmpty){
      await upgradeDowngradeController.onDetachPaymentButtonPressed(planId.value, planType.value, );
      profileScreenController.getProfileData();
      clearPaymentDetailsFields();
      return;
    }*/

    CreateSubscriptionSendModel model = CreateSubscriptionSendModel(
      customerId: profileScreenController.userData.value.stripeCustomerId,
      priceId: planId.value,
      collectionMethod: "charge_automatically",
      product: planType.value,
      promoCode: '',
    );
    var data = await CoreService().postWithAuth(
      url: baseUrl + createSubscriptionUrl,
      body: model.toJson(),
    );
    if (data == null) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please try again later",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          createSubscription();
        }
      } else {
        Get.back();
        var result = CreateSubscriptionResponseModel.fromJson(data);
        if (result.id != null) {
          Get.offAllNamed(loginScreen);
          if (Get.isDialogOpen ?? false) Get.back();
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "You have been upgraded, please relogin to continue",
              backgroundColor: Colors.green,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
          profileScreenController.getProfileData();
          clearPaymentDetailsFields();
        } else {
          var result = CreateSubscriptionResponseModel.fromJson(data);
          if (result.id != null) {
            Future.delayed(const Duration(milliseconds: 0), () {
              FocusManager.instance.primaryFocus!.unfocus();
              Fluttertoast.showToast(
                timeInSecForIosWeb: 3,
                msg: data["message"],
                backgroundColor: Colors.red,
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.BOTTOM,
                textColor: Colors.white,
                fontSize: 20.0,
              );
            });
          } else {
            Future.delayed(const Duration(milliseconds: 0), () {
              FocusManager.instance.primaryFocus!.unfocus();
              Fluttertoast.showToast(
                timeInSecForIosWeb: 3,
                msg: "Please try again later",
                backgroundColor: Colors.red,
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.BOTTOM,
                textColor: Colors.white,
                fontSize: 20.0,
              );
            });
          }
        }
      }
    }
  }

  onDetachPaymentButtonPressed(String planId, String planType) async {
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);
    /*UpgradeDowngradeSendModel model = UpgradeDowngradeSendModel(
      priceId: planId,
    );*/

    CreateSubscriptionSendModel model = CreateSubscriptionSendModel(
      customerId: profileScreenController.userData.value.stripeCustomerId,
      priceId: planId,
      collectionMethod: "charge_automatically",
      product: planType,
      promoCode: '',
    );

    var data = await CoreService().postWithAuth(
      url: baseUrl +
          upgradeDowngradeUrl +
          profileScreenController.userData.value.stripeCustomerId.toString(),
      body: model.toJson(),
    );
    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please try again later",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          onDetachPaymentButtonPressed(planId, planType);
        }
      } else {
        if (data.toString().contains("SUBSCRIPTION_NOT_FOUND")) {
          createSubscription();
          return;
        }

        var result = UpgradeDowngradeResponseModel.fromJson(data);
        if (result.id != null) {
          profileScreenController.getProfileData();
          clearPaymentDetailsFields();
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg:
                  "Your subscription has been successfully changed, please relogin to continue",
              backgroundColor: Colors.green,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });

          Get.offAllNamed(loginScreen);
          //paymentDetailsViewController.clearPaymentDetailsFields();

        } else {
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Please try again later",
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }
      }
    }
  }

  upgradeDowngradeSubscriptionFunction() {
    upgradeDowngradeController.onDetachPaymentButtonPressed(
        planId.value, planType.value);
  }
}
