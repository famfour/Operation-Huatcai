import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Model/response_model.dart/training_view_response_model.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';

class TrainingViewController extends GetxController {
  var arguments = Get.arguments;
  Rx<TrainingViewResponse> trainingViewResponse = TrainingViewResponse().obs;
  late TextEditingController userGuideSearchTextEditingController;
  late TextEditingController basicModulesSearchEditingController;
  late TextEditingController applicationDemoSearchEditingController;

  @override
  void onInit() {
    userGuideSearchTextEditingController = TextEditingController();
    basicModulesSearchEditingController = TextEditingController();
    applicationDemoSearchEditingController = TextEditingController();
    Timer(const Duration(seconds: 0), () {
      Get.isDialogOpen ?? true
          ? const Offstage()
          : Get.dialog(const Center(child: CircularProgressIndicator()),
              barrierDismissible: false);
      getTrainigData(trainingModule: arguments[0]);
    });
    super.onInit();
  }

  getTrainigData({required String trainingModule}) async {
    debugPrint(strapiUrl + trainingCatagoryUrl + trainingModule);
    var data = await CoreService()
        .getWithoutAuth(url: strapiUrl + trainingCatagoryUrl + trainingModule);

    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
      if (Get.isDialogOpen ?? false) Get.back();
    } else {
      trainingViewResponse.value = TrainingViewResponse.fromJson(data);
      // debugPrint("*****");
      // debugPrint(trainingViewResponse
      //     .value.value![2].media!.formats!.thumbnail!.url
      //     .toString());
      // debugPrint("*****");
      sortByDisplayOrder(trainingViewResponse.value.value!);
      if (Get.isDialogOpen ?? false) Get.back();
    }
  }

  sortByDisplayOrder(List<TrainingViewResponseModel> results) {
    results.sort((a, b) => a.displayOrder!.compareTo(b.displayOrder!));
  }

  searchBasicModules(String query) async {
    debugPrint(strapiUrl + trainingCatagorySearchUrl + query);
    var data = await CoreService()
        .getWithoutAuth(url: strapiUrl + trainingCatagorySearchUrl + query);

    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
      if (Get.isDialogOpen ?? false) Get.back();
    } else {
      trainingViewResponse.value = TrainingViewResponse.fromJson(data);
      if (Get.isDialogOpen ?? false) Get.back();
    }
  }

  searchApplicationDemo(String query) async {
    debugPrint(strapiUrl + trainingCatagorySearchUrl + query);
    var data = await CoreService()
        .getWithoutAuth(url: strapiUrl + trainingCatagorySearchUrl + query);

    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
      if (Get.isDialogOpen ?? false) Get.back();
    } else {
      trainingViewResponse.value = TrainingViewResponse.fromJson(data);
      if (Get.isDialogOpen ?? false) Get.back();
    }
  }

  searchUserGuide(String query) async {
    debugPrint(strapiUrl + trainingCatagorySearchUrl + query);
    var data = await CoreService()
        .getWithoutAuth(url: strapiUrl + trainingCatagorySearchUrl + query);

    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
      if (Get.isDialogOpen ?? false) Get.back();
    } else {
      trainingViewResponse.value = TrainingViewResponse.fromJson(data);
      if (Get.isDialogOpen ?? false) Get.back();
    }
  }

  navigateToRequiredDetailedPage(oneTrainingCatagoryDetail) {
    Get.toNamed(trainingCategoryDetailsScreenUrl,
        arguments: [oneTrainingCatagoryDetail]);
  }
}
