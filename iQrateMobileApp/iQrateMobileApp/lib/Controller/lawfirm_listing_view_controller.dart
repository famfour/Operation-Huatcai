import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Model/response_model.dart/lawfirm_listing_response_model.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';

class LawfirmListingViewController extends GetxController {
  Rx<LawFirmListingsResponseModel> lawFirmListsResponseModel =
      LawFirmListingsResponseModel().obs;
  late ScrollController scrollController = ScrollController();
  RxList<OneResult> lawfirmList = <OneResult>[].obs;

  @override
  void onInit() {
    Timer(const Duration(seconds: 0), () {
      Get.isDialogOpen ?? true
          ? const Offstage()
          : Get.dialog(const Center(child: CircularProgressIndicator()),
              barrierDismissible: false);
      getLawFirmLists();
    });
    scrollController.addListener(pagination);
    super.onInit();
  }

  getLawFirmLists() async {
    debugPrint(baseUrl + lawFirmsListUrls);
    var data = await CoreService().getWithAuth(url: baseUrl + lawFirmsListUrls);
    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          getLawFirmLists();
        }
      } else {
        lawFirmListsResponseModel.value =
            LawFirmListingsResponseModel.fromJson(data);
        lawfirmList.addAll(lawFirmListsResponseModel.value.result!);
        sortByPriorityOrder(lawfirmList);
      }
    }
  }

  getMoreLawFirmLists() async {
    debugPrint(lawFirmListsResponseModel.value.next);
    if (lawFirmListsResponseModel.value.next != null) {
      Get.isDialogOpen ?? true
          ? const Offstage()
          : Get.dialog(const Center(child: CircularProgressIndicator()),
              barrierDismissible: false);
      var data = await CoreService()
          .getWithAuth(url: lawFirmListsResponseModel.value.next!);
      if (data != null) {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            getMoreLawFirmLists();
          }
        } else {
          lawFirmListsResponseModel.value =
              LawFirmListingsResponseModel.fromJson(data);
          lawfirmList.addAll(lawFirmListsResponseModel.value.result!);
          sortByPriorityOrder(lawfirmList);
          Get.back();
        }
      } else {
        Get.back();
      }
    }
  }

  sortByPriorityOrder(List<OneResult>? results) {
    results?.sort((a, b) => a.priority!.compareTo(b.priority!));
  }

  onTapOneLawFirm(
      {required List<OneProduct> productList, required OneResult oneResult}) {
    Get.toNamed(lawFirmProductDetailsScreen,
        arguments: [productList, oneResult]);
  }

  pagination() {
    if ((scrollController.position.pixels ==
        scrollController.position.maxScrollExtent)) {
      //add api for load the more data according to new page
      getMoreLawFirmLists();
    }
  }
}
