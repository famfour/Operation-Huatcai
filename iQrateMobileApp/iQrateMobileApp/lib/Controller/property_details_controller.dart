// ignore_for_file: prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:iqrate/Controller/generate_packages_controller.dart';
import 'package:iqrate/Controller/leads_view_controller.dart';
import 'package:iqrate/Model/response_model.dart/address_with_postalcode_response_model.dart';
import 'package:iqrate/Model/response_model.dart/property_details_response_model.dart';
import 'package:iqrate/Model/response_model.dart/kv_response_model.dart';
import 'package:iqrate/Model/send_model.dart/property_details_send_model.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';

class PropertyDetailsController extends GetxController {
  late RxDouble windowHeight;
  late RxDouble windowWidth;
  late RxInt selectedIndex;

  TextEditingController propertyPurchasePriceController =
      TextEditingController(); //property Purchase Price text controller
  TextEditingController countryController =
      TextEditingController(); //country text controller
  TextEditingController postCodeController =
      TextEditingController(); //postCode text controller
  TextEditingController streetNameController =
      TextEditingController(); //streetName text controller
  TextEditingController unitNoController =
      TextEditingController(); //unitNo text controller

  TextEditingController projectNameController =
      TextEditingController(); //project Name text controller

  final formKey = GlobalKey<FormState>();

  var isExpand = false.obs;

  String? selectedPropertyType;
  KvValueStoreResponseModel? selectedKVStore;
  List<String> dropdownPropertyStatusType = []; //values for property status
  List<String> dropdownPropertyStatusTypeKey = []; //Keys for property status
  String propertyStatusTypeToPassInAPI = ''; //Value to pass in API
  List<String> dropdownPropertyType = []; //Values for property type
  List<String> dropDownPropertyTypeKey = []; //Keys for propertyType
  String propertyTypeValueToPassInAPI = ''; //Value to pass in API

  String? selectedPropertyStatusType;

  int? leadID;
  var propertyID = "".obs;

  LeadsViewController leadsViewController = Get.find();
  GeneratePackagesController generatePackagesController = Get.find();

  @override
  onInit() {
    setStatusDropdown();
    setPropertyTypeDropdown();

    super.onInit();
  }

  setStatusDropdown() {
    for (var element in leadsViewController.kvStoreValues) {
      if (element.code == "property_status") {
        // debugPrint("=====kvstore=====" + element.value.toString());
        element.value.forEach((key, value) {
          dropdownPropertyStatusType.add(value);
          dropdownPropertyStatusTypeKey.add(key);
          // debugPrint("array_key" + key);
        });
      }
    }
  }

  setPropertyTypeDropdown() {
    for (var element in leadsViewController.kvStoreValues) {
      if (element.code == "property_type") {
        //debugPrint("=====kvstore====="+element.value.toString());
        element.value.forEach((key, value) {
          dropdownPropertyType.add(value);
          dropDownPropertyTypeKey.add(key);
          //debugPrint("array_key" + key );
        });
      }
    }
  }

  Future<void> onTapSave() async {
    debugPrint("onTapSave==");

    if (
        // propertyPurchasePriceController.text.isEmpty ||
        //   postCodeController.text.isEmpty ||
        //unitNoController.text.isEmpty ||
        // streetNameController.text.isEmpty ||
        propertyTypeValueToPassInAPI.isEmpty ||
            propertyStatusTypeToPassInAPI.isEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please enter all mandatory fields",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    }
    // else if (propertyPurchasePriceController.text.isEmpty) {
    //   Get.snackbar("Error", "Please fill the property purchase price",
    //       snackPosition: SnackPosition.TOP,
    //       backgroundColor: Colors.red,
    //       colorText: Colors.white);
    // } else if (postCodeController.text.isEmpty) {
    //   Get.snackbar("Error", "Postal Code is required",
    //       snackPosition: SnackPosition.TOP,
    //       backgroundColor: Colors.red,
    //       colorText: Colors.white);
    // } else if (streetNameController.text.isEmpty) {
    //   Get.snackbar("Error", "Street Name is required",
    //       snackPosition: SnackPosition.TOP,
    //       backgroundColor: Colors.red,
    //       colorText: Colors.white);
    // }
    /*else if (unitNoController.text.isEmpty) {
      Get.snackbar("Error", "Unit No is required",
          snackPosition: SnackPosition.TOP,
          backgroundColor: Colors.red,
          colorText: Colors.white);
    }*/
    else if (postCodeController.text.isNotEmpty &&
        postCodeController.text.length < 6) {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Invalid postal code",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    } else {
      Get.isDialogOpen ?? true
          ? const Offstage()
          : Get.dialog(
              const Center(
                child: CircularProgressIndicator(),
              ),
              barrierDismissible: false);

      PropertyDetailsSendModel model = PropertyDetailsSendModel(
        lead: leadID,
        postalCode: postCodeController.text,
        propertyStatus: propertyStatusTypeToPassInAPI,
        propertyType: propertyTypeValueToPassInAPI,
        purchasePrice: propertyPurchasePriceController.text.isNotEmpty
            ? double.parse(propertyPurchasePriceController.text
                .toString()
                .replaceAll(',', ''))
            : null,
        streetName: streetNameController.text,
        unitNumber: unitNoController.text,
        projectName: projectNameController.text,
      );

      var data;

      if (propertyID.value.isEmpty) {
        data = await CoreService().postWithAuth(
          url: baseUrl + createPropertyUrl,
          body: model.toJson(),
        );
      } else {
        data = await CoreService().putWithAuth(
          url: baseUrl + updatePropertyUrl + propertyID.value,
          body: model.toJson(),
        );
      }

      if (Get.isDialogOpen ?? false) Get.back();
      if (data == null) {
      } else {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            onTapSave();
          }
        } else {
          leadsViewController.fetchLeads();

          var result = PropertyDetailsResponseModel.fromJson(data);

          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: propertyID.value.isEmpty
                  ? "Property added Successfully"
                  : "Property updated Successfully",
              backgroundColor: Colors.green,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
          propertyID.value = result.id.toString();

          generatePackagesController.getLowestPackages(leadID.toString());

          propertyPurchasePriceController.text =
              data['purchase_price'].toString() != 'null'
                  ? oCcy.format(data['purchase_price'])
                  : "";
        }
      }
    }
  }

  final oCcy = NumberFormat("#,##0.00", "en_US");

  void onTapGeneratePackage() {
    debugPrint("onTapSave==");
    Get.toNamed(loanPackageScreen);
  }

  getAddressWithPostalCode(String pinCode) async {
    debugPrint(addressWithPostalCodeUrl);
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);
    var data = await CoreService()
        .getWithoutAuth(url: addressWithPostalCodeUrl + pinCode);
    if (Get.isDialogOpen == true) Get.back();
    if (data == null) {
      if (Get.isDialogOpen ?? false) Get.back();
    } else {
      var result = AddressWithPostalCodeResponseModel.fromJson(data);
      if (result.results != null) {
        streetNameController.text =
            '${result.results![0].blkNo}, ${result.results![0].roadName}';
        projectNameController.text = '${result.results![0].building}';
        if (Get.isDialogOpen ?? false) Get.back();
      } else {
        if (Get.isDialogOpen == true) Get.back();
      }
    }
  }
}
