import 'package:get/get.dart';
import 'package:iqrate/Router/route_constants.dart';

class LandingController extends GetxController {
  void toLanding() async {
    Get.offAndToNamed(
        initialScreen); //Auto navigates to the initial screen after the splash screen is displayed
  }
}
