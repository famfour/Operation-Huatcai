// ignore_for_file: prefer_typing_uninitialized_variables, unused_local_variable

import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:hive/hive.dart';
import 'package:iqrate/Controller/profile_controller.dart';
import 'package:iqrate/Controller/shared_prefs_controller.dart';
import 'package:iqrate/Controller/sign_up_controller.dart';
import 'package:iqrate/DeviceManager/hive_string.dart';
import 'package:iqrate/Model/response_model.dart/check_email_verified_response_model.dart';
import 'package:iqrate/Model/response_model.dart/customer_register_strpie_response_model.dart';
import 'package:iqrate/Model/response_model.dart/login_response_model.dart';
import 'package:iqrate/Model/response_model.dart/profile_response_model.dart';
import 'package:iqrate/Model/response_model.dart/resend_email_otp_response_model.dart';
import 'package:iqrate/Model/send_model.dart/apple_login_send_model.dart';
import 'package:iqrate/Model/send_model.dart/email_verification_send_model.dart';
import 'package:iqrate/Model/send_model.dart/google_auth_send_model.dart';
import 'package:iqrate/Model/send_model.dart/google_dob_send_model.dart';
import 'package:iqrate/Model/send_model.dart/login_send_model.dart';
import 'package:iqrate/Model/send_model.dart/register_strip_send_model.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Service/GetControllers.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/string_utils.dart';
import 'package:iqrate/Service/url.dart';
import 'package:iqrate/Widgets/form_field_title.dart';
import 'package:iqrate/Widgets/require_text_field.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

import '../DeviceManager/screen_constants.dart';
import '../Model/response_model.dart/change_phone_response_model.dart';
import '../Model/send_model.dart/update_profile_send_model.dart';
import '../Widgets/primary_button.dart';

class SignInController extends GetxController {
  late ProfileScreenController profileScreenController =
      Get.put(ProfileScreenController());
  late TextEditingController emailTextController;
  late TextEditingController emailVerifyTextController;
  late TextEditingController passwordTextController;
  late TextEditingController phoneController;
  late TextEditingController dobController;
  late TextEditingController referralCodeController;
  TextEditingController countryCodeController = TextEditingController();
  TextEditingController countryCodeLabelController =
      TextEditingController(); //phone code label text controller
  final SharedPrefController sharedPrefController = Get.find();
  late TextEditingController resendEmailController;
  final formKey =
      GlobalKey<FormState>(); // this key manage to form submit validation
  GoogleSignIn googleSignIn = GoogleSignIn(scopes: ['email']);

  late Box hive = Hive.box(HiveString.hiveName);
  var mediaQuery = Get.mediaQuery;

  RxBool redirectFromLogin = false.obs;
  bool isNewUser = true;
  double windowHeightC = Get.height;
  double windowWidthC = Get.width;

  bool isRedirectFromSignInScreen = false;
  bool isGoogleLogin2FAEnable = false;
  bool isAppleLogin2FAEnable = false;

  @override
  void onInit() {
    emailTextController =
        TextEditingController(); //initialize email text field controller
    emailVerifyTextController =
        TextEditingController(); //initialize email text field controller
    passwordTextController =
        TextEditingController(); //initialize password text field controller
    phoneController =
        TextEditingController(); //initialize phone text field controller
    dobController =
        TextEditingController(); //initialize dob text field controller
    referralCodeController =
        TextEditingController(); //initialize referral text field controller
    countryCodeController.text =
        "+65"; //initialize country code text field controller
    countryCodeLabelController.text = "SG";
    resendEmailController = TextEditingController();
    //only for debug login when we go to app can use it
    debugLogin();
    //debugGoogleLogin();
    isNewUser = sharedPrefController.prefs.getBool('isNewUser') ?? true;
    super.onInit();
  }

  debugGoogleLogin() {
    if (kDebugMode) {
      phoneController.text = "8017156043";
      dobController.text = "01/01/1990";
      countryCodeController.text = "+91";

      //phoneController.text = "1928292779";
      phoneController.text = "1844476978";
      dobController.text = "01-01-1990";
      countryCodeController.text = "+880";
    }
  }

  // debugLogin() {
  //   if (kDebugMode) {
  //     emailTextController.text = "phung@yopmail.com";
  //     // emailTextController.text = "appdeveloper.shohel@gmail.com";
  //     //  passwordTextController.text = "password1";
  //     //emailTextController.text = "abyss1x@gmail.com";
  //     passwordTextController.text = "aA12345678";
  //     // passwordTextController.text = "aA12345678";
  //   }
  // }

  debugLogin() {
    if (kDebugMode) {
      // emailTextController.text = "aiswaryatest3@gmail.com";
      // passwordTextController.text = "Password2";
      //emailTextController.text = "phung@yopmail.com";
      //emailTextController.text = "aiswaryatest1@gmail.com";

      //emailTextController.text = "aiswaryatest1@gmail.com";
      // emailTextController.text = "aiswaryatest3@gmail.com";

      //emailTextController.text = "shohel01716@gmail.com";
      // emailTextController.text = "aiswaryatest3@gmail.com";
      // emailTextController.text = "developer.shohel@gmail.com";
      emailTextController.text = "rishabh@digitalprizm.net";
      passwordTextController.text = "Password1";
      //emailTextController.text = "abyss1x@gmail.com";
      //passwordTextController.text = "aA12345678";
      //passwordTextController.text = "aA12345678";
      // emailTextController.text = "harikumar@digitalprizm.net";
      // passwordTextController.text = "IQrateHar1";

      // emailTextController.text = "jessy@digitalprizm.net";
      // passwordTextController.text = "Password1";
    }
  }
  /*debugLogin() {
    if (kDebugMode) {
      // emailTextController.text = "sethia.rishabh007@gmail.com";
      // passwordTextController.text = "password";
      emailTextController.text = "phung@yopmail.com";
      passwordTextController.text = "aA12345678";
    }
  }*/

  onSignIn() {
    if (emailTextController.text.isEmpty &&
        passwordTextController.text.isEmpty) {
      FocusManager.instance.primaryFocus!.unfocus();
      Fluttertoast.showToast(
        timeInSecForIosWeb: 3,
        msg: "All fields are required",
        backgroundColor: Colors.red,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        textColor: Colors.white,
        fontSize: 20.0,
      );
    } else if (emailTextController.text.isEmpty) {
      FocusManager.instance.primaryFocus!.unfocus();
      Fluttertoast.showToast(
        timeInSecForIosWeb: 3,
        msg: "Please enter a valid email",
        backgroundColor: Colors.red,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        textColor: Colors.white,
        fontSize: 20.0,
      );
    } else if (passwordTextController.text.isEmpty) {
      FocusManager.instance.primaryFocus!.unfocus();
      Fluttertoast.showToast(
        timeInSecForIosWeb: 3,
        msg: "Please enter a valid password",
        backgroundColor: Colors.red,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        textColor: Colors.white,
        fontSize: 20.0,
      );
    } else {
      checkEmailVerified();
    }
  }

  submitLogin() async {
    debugPrint(baseUrl + loginUrl);
    // Get.isDialogOpen ?? true
    //     ? const Offstage()
    //     : Get.dialog(
    //         const Center(
    //           child: CircularProgressIndicator(),
    //         ),
    //         barrierDismissible: false);
    GetControllers.shared.showLoading();
    LoginSend loginSend = LoginSend(
      email: emailTextController.text,
      password: passwordTextController.text,
    );
    var data = await CoreService().postWithoutAuthForLogin(
        url: baseUrl + loginUrl, body: loginSend.toJson());
    GetControllers.shared.hideLoading();
    if (data == null) {
      // if (Get.isDialogOpen ?? false) Get.back();
      //Get.toNamed(authenticatorOtp);
    } else {
      var result = LoginResponseModel.fromJson(data);
      if (result.accessToken != null) {
        // debugPrint(result.accessToken);
        await hive.put(HiveString.token, result.accessToken);
        await hive.put(HiveString.refreshToken, result.refreshToken);
        //profileScreenController = Get.put(ProfileScreenController());
        //getAndStoreKvValues();

        // await profileScreenController.getProfileData();
        await GetControllers.shared
            .getProfileScreenController()
            .getProfileData();
        await registerWithStripe();
        navigationCheckAfterLogin(false);

        sharedPrefController.prefs.setBool('isNewUser',
            false); // This is to set false so that the app understands that, the user loggedin is not the user's first time

      } else {}
    }

    // if (Get.isDialogOpen ?? false) Get.back();
  }

  apiCallResendEmailCode() async {
    if (resendEmailController.text.isEmpty) {
      FocusManager.instance.primaryFocus!.unfocus();
      Fluttertoast.showToast(
        timeInSecForIosWeb: 3,
        msg: "Please enter email address",
        backgroundColor: Colors.red,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        textColor: Colors.white,
        fontSize: 20.0,
      );

      return;
    }

    if (!resendEmailController.text.isEmail) {
      FocusManager.instance.primaryFocus!.unfocus();
      Fluttertoast.showToast(
        timeInSecForIosWeb: 3,
        msg: "Please enter a valid email",
        backgroundColor: Colors.red,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        textColor: Colors.white,
        fontSize: 20.0,
      );

      return;
    }

    Get.isDialogOpen ?? true
        ? const Offstage() //Used for avoiding the dialog to be open when the user is already on the next screen.
        : Get.dialog(
            const Center(
                child:
                    CircularProgressIndicator()), //Used for showing the loading dialog.
            barrierDismissible:
                false); //Used for preventing the user to dismiss the dialog.

    EmailVerificationSendModel emailVerificationSendModel =
        EmailVerificationSendModel(
      verification_code: resendEmailController.text,
    ); //Create a model for resending the otp to the server.

    //The entered url is base+resendEmailCodeUrl+the email address of the user.
    var data = await CoreService().putWithoutAuth(
        url: baseUrl + resendEmailCodeUrl + resendEmailController.text,
        body:
            emailVerificationSendModel.toJson()); //Send the otp to the server.
    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
    } else {
      //if the status code is 400, the server sends an error, which is displayed via the.
      if (data["resultcode"] == 400) {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: data["errMessage"].toString(),
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      } else {
        var result = ResendEmailOtpResponseModel.fromJson(data);

        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: result.message!,
          backgroundColor: Colors.green,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      }
    }
  }

  navigationCheckAfterLogin(bool isGoogleLogin) {
    if (profileScreenController
                .userData.value.isTwoFactorAuthenticationEnabled !=
            false &&
        !isNewUser) {
      // !
      debugPrint(profileScreenController
          .userData.value.isTwoFactorAuthenticationEnabled
          .toString());
      debugPrint(isNewUser.toString());
      Get.offAndToNamed(dashboard);
      if (Get.isDialogOpen ?? false) Get.back();
      if (sharedPrefController.prefs.getInt("counter") == 0) {
        sharedPrefController.prefs.setInt("counter", 1);
      }
    } else if (profileScreenController
                .userData.value.isTwoFactorAuthenticationEnabled !=
            false &&
        isNewUser) {
      // !
      debugPrint(profileScreenController
          .userData.value.isTwoFactorAuthenticationEnabled
          .toString());
      debugPrint(isNewUser.toString());
      Get.toNamed(subscriptionPlansScreen);
    } else {
      // !
      debugPrint(profileScreenController
          .userData.value.isTwoFactorAuthenticationEnabled
          .toString());

      debugPrint(isNewUser.toString());

      if (isGoogleLogin) {
        Get.toNamed(dashboard);
        if (Get.isDialogOpen ?? false) Get.back();
        return;
      }

      Get.toNamed(twoFactorQuestion);
    }
  }

  var credential;
  handleAppleLogin(String twoFactorCode) async {
    try {
      var jsonBody;

      if (isAppleLogin2FAEnable) {
        AppleLoginSendModel model = AppleLoginSendModel(
            identityToken: credential.identityToken,
            authorizationCode: credential.authorizationCode,
            user: credential.userIdentifier,
            type: 'Login',
            twoFactorAuthenticationCode: twoFactorCode);
        jsonBody = model.toJson();
      } else {
        credential = await SignInWithApple.getAppleIDCredential(
          scopes: [
            AppleIDAuthorizationScopes.email,
            AppleIDAuthorizationScopes.fullName,
          ],
          state: 'Login',
        );

        if (kDebugMode) {
          print("Apple login credential:: " + credential.toString());
          print("Apple login authorizationCode:: " +
              credential.authorizationCode);
          log("Apple login identityToken:: " +
              credential.identityToken.toString());
          print("Apple login userIdentifier:: " +
              credential.userIdentifier.toString());
          print("Apple login email:: " + credential.email.toString());
          print("Apple login givenName:: " + credential.givenName.toString());
          print("Apple login state:: " + credential.state.toString());
        }

        if (credential == null) {
          debugPrint('Apple login Process Aborted!');

          return;
        }

        AppleLoginSendModel model = AppleLoginSendModel(
            identityToken: credential.identityToken,
            authorizationCode: credential.authorizationCode,
            user: credential.userIdentifier,
            type: 'Login');

        jsonBody = model.toJson();
      }

      Get.isDialogOpen ?? true
          ? const Offstage()
          : Get.dialog(
              const Center(
                child: CircularProgressIndicator(),
              ),
              barrierDismissible: false);
      var data = await CoreService()
          .postWithoutAuthAppleLogin(url: baseUrl + appleAuth, body: jsonBody);
      if (data == null) {
        Get.back();
      } else {
        var result = LoginResponseModel.fromJson(data);
        if (result.accessToken != null) {
          debugPrint("handleAppleLogin:: " + result.toJson().toString());
          hive.put(HiveString.token, result.accessToken);
          hive.put(HiveString.refreshToken, result.refreshToken);
          var data =
              await CoreService().getWithAuth(url: baseUrl + getUserProfileUrl);
          if (data == null) {
          } else {
            Get.back();
            var result = ProfileResponseModel.fromJson(data);
            if (result.isMobileVerified == 0) {
              // Get.toNamed(profileScreen, parameters: {'from': 'GoogleAuth'});
              Get.defaultDialog(
                title: "",
                content: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 0.0, horizontal: 5),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Column(
                          children: [
                            Text(
                              "Finish Signing Up\n",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: FontSize.s20,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            Text(
                              "Enter Your mobile number to get a\nverifcation code",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: const Color(0XFF87898C),
                                fontSize: FontSize.s14,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: windowHeightC * 0.01),
                        Column(
                          children: [
                            SizedBox(height: windowHeightC * 0.01),
                            const Align(
                              alignment: Alignment.centerLeft,
                              child: FormFieldTitle(title: 'Mobile Number*'),
                            ),
                            SizedBox(height: windowHeightC * 0.01),
                            RequireTextField(
                              type: Type.dropdownCountryCodeWithPhone,
                              controller: phoneController,
                              countryCodeLabelController:
                                  countryCodeLabelController,
                              countryCodeController: countryCodeController,
                            ),
                            SizedBox(height: windowHeightC * 0.01),
                            const Align(
                              alignment: Alignment.centerLeft,
                              child: FormFieldTitle(title: 'Date Of Birth*'),
                            ),
                            SizedBox(height: windowHeightC * 0.01),
                            RequireTextField(
                              type: Type.dob,
                              controller: dobController,
                            ),
                            SizedBox(height: windowHeightC * 0.03),
                            const Align(
                                alignment: Alignment.centerLeft,
                                child: FormFieldTitle(
                                    title: 'Referral Code (Optional)')),
                            SizedBox(height: windowHeightC * 0.01),
                            RequireTextField(
                              type: Type.optional,
                              labelText: "Enter referral code",
                              controller: referralCodeController,
                              key: const Key("code"),
                            ),
                          ],
                        ),
                        SizedBox(height: windowHeightC * 0.05),
                        PrimaryButton(
                          windowHeight: windowHeightC,
                          windowWidth: windowWidthC,
                          buttonTitle: "Continue",
                          onPressed: () {
                            onContinueButtonGoogleSignUpTap();
                          },
                        )
                      ],
                    ),
                  ),
                ),
              );
            } else if (result.isMobileVerified == 1) {
              profileScreenController = Get.put(ProfileScreenController());
              debugPrint("kv store values");
              //getAndStoreKvValues();

              await profileScreenController.getProfileData();
              await registerWithStripe();

              navigationCheckAfterLogin(true);

              sharedPrefController.prefs.setBool('isNewUser',
                  false); // This is to set false so that the app understands that, the user loggedin is not the user's first time

              FocusManager.instance.primaryFocus!.unfocus();
              Fluttertoast.showToast(
                timeInSecForIosWeb: 3,
                msg: "Login Successful",
                backgroundColor: Colors.green,
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.BOTTOM,
                textColor: Colors.white,
                fontSize: 20.0,
              );
            }
          }
        } else {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: StringUtils.error,
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        }
      }
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  handleGoogleLogin(String twoFactorCode) {
    try {
      googleSignIn.signIn().then((acc) async {
        GoogleSignInAuthentication? auth = await acc?.authentication;
        if (auth == null) {
          debugPrint('Process Aborted!');
        } else {
          emailTextController.text = acc!.email;
          log(auth.accessToken.toString()); //! Print accesstoken
          log(auth.idToken.toString()); //! Print idToken

          var jsonBody;

          if (isGoogleLogin2FAEnable) {
            GoogleAuth2FASendModel model = GoogleAuth2FASendModel(
                accessToken: auth.accessToken.toString(),
                idToken: auth.idToken.toString(),
                type: 'Login',
                twoFactorAuthenticationCode: twoFactorCode);
            jsonBody = model.toJson();
          } else {
            GoogleAuthSendModel model = GoogleAuthSendModel(
                accessToken: auth.accessToken.toString(),
                idToken: auth.idToken.toString(),
                type: 'Login');
            jsonBody = model.toJson();
            log(jsonBody.toString());
          }

          Get.isDialogOpen ?? true
              ? const Offstage()
              : Get.dialog(
                  const Center(
                    child: CircularProgressIndicator(),
                  ),
                  barrierDismissible: false);
          var data = await CoreService().postWithoutAuthGoogleLogin(
              url: baseUrl + googleAuth, body: jsonBody);
          if (data == null) {
            Get.back();
          } else {
            var result = LoginResponseModel.fromJson(data);
            if (result.accessToken != null) {
              debugPrint("handleGoogleLogin:: " + result.toJson().toString());
              hive.put(HiveString.token, result.accessToken);
              hive.put(HiveString.refreshToken, result.refreshToken);
              var data = await CoreService()
                  .getWithAuth(url: baseUrl + getUserProfileUrl);
              if (data == null) {
              } else {
                Get.back();
                var result = ProfileResponseModel.fromJson(data);
                if (result.isMobileVerified == 0) {
                  // Get.toNamed(profileScreen, parameters: {'from': 'GoogleAuth'});
                  Get.defaultDialog(
                    title: "",
                    content: SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 0.0, horizontal: 5),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Column(
                              children: [
                                Text(
                                  "Finish Signing Up\n",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: FontSize.s20,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                                Text(
                                  "Enter Your mobile number to get a\nverifcation code",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: const Color(0XFF87898C),
                                    fontSize: FontSize.s14,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: windowHeightC * 0.01),
                            Column(
                              children: [
                                SizedBox(height: windowHeightC * 0.01),
                                const Align(
                                  alignment: Alignment.centerLeft,
                                  child:
                                      FormFieldTitle(title: 'Mobile Number*'),
                                ),
                                SizedBox(height: windowHeightC * 0.01),
                                RequireTextField(
                                  type: Type.dropdownCountryCodeWithPhone,
                                  controller: phoneController,
                                  countryCodeLabelController:
                                      countryCodeLabelController,
                                  countryCodeController: countryCodeController,
                                ),
                                SizedBox(height: windowHeightC * 0.01),
                                const Align(
                                  alignment: Alignment.centerLeft,
                                  child:
                                      FormFieldTitle(title: 'Date Of Birth*'),
                                ),
                                SizedBox(height: windowHeightC * 0.01),
                                RequireTextField(
                                  type: Type.dob,
                                  controller: dobController,
                                ),
                                SizedBox(height: windowHeightC * 0.03),
                                const Align(
                                    alignment: Alignment.centerLeft,
                                    child: FormFieldTitle(
                                        title: 'Referral Code (Optional)')),
                                SizedBox(height: windowHeightC * 0.01),
                                RequireTextField(
                                  type: Type.optional,
                                  labelText: "Enter referral code",
                                  controller: referralCodeController,
                                  key: const Key("code"),
                                ),
                              ],
                            ),
                            SizedBox(height: windowHeightC * 0.05),
                            PrimaryButton(
                              windowHeight: windowHeightC,
                              windowWidth: windowWidthC,
                              buttonTitle: "Continue",
                              onPressed: () {
                                onContinueButtonGoogleSignUpTap();
                              },
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                } else if (result.isMobileVerified == 1) {
                  profileScreenController = Get.put(ProfileScreenController());
                  debugPrint("kv store values");
                  //getAndStoreKvValues();

                  await profileScreenController.getProfileData();
                  await registerWithStripe();

                  navigationCheckAfterLogin(true);

                  sharedPrefController.prefs.setBool('isNewUser',
                      false); // This is to set false so that the app understands that, the user loggedin is not the user's first time

                  FocusManager.instance.primaryFocus!.unfocus();
                  Fluttertoast.showToast(
                    timeInSecForIosWeb: 3,
                    msg: "Login Successful",
                    backgroundColor: Colors.green,
                    toastLength: Toast.LENGTH_LONG,
                    gravity: ToastGravity.BOTTOM,
                    textColor: Colors.white,
                    fontSize: 20.0,
                  );
                }
              }
            } else {
              FocusManager.instance.primaryFocus!.unfocus();
              Fluttertoast.showToast(
                timeInSecForIosWeb: 3,
                msg: StringUtils.error,
                backgroundColor: Colors.red,
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.BOTTOM,
                textColor: Colors.white,
                fontSize: 20.0,
              );
            }
          }
        }
      });
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  checkEmailVerified() async {
    // debugPrint(baseUrl + checkEmailVerifiedUrl + emailTextController.text);
    // Get.isDialogOpen ?? true
    //     ? const Offstage()
    //     : Get.dialog(
    //         const Center(
    //           child: CircularProgressIndicator(),
    //         ),
    //         barrierDismissible: false);

    var data = await CoreService().getWithoutAuth(
        url: baseUrl + checkEmailVerifiedUrl + emailTextController.text);
    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
    } else {
      var result = CheckEmailVerifiedResponseModel.fromJson(data);
      if (result.isEmailVerified == 0) {
        debugPrint(result.isEmailVerified.toString());

        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Please verify your email to continue logging in",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );

        // if (Get.isDialogOpen ?? false) Get.back();
      } else {
        submitLogin();
      }
    }
  }

  submitMFALogin(TextEditingController twoFactorAuthenticationCode) async {
    if (twoFactorAuthenticationCode.text.isEmpty) {
      FocusManager.instance.primaryFocus!.unfocus();
      Fluttertoast.showToast(
        timeInSecForIosWeb: 3,
        msg: "Please enter OTP code",
        backgroundColor: Colors.red,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        textColor: Colors.white,
        fontSize: 20.0,
      );

      return;
    }

    debugPrint(baseUrl + loginUrl);
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);
    LoginSend loginSend = LoginSend(
      email: emailTextController.text,
      password: passwordTextController.text,
      twoFactorAuthenticationCode: twoFactorAuthenticationCode.text,
    );
    var data = await CoreService().postWithoutAuth2faVerification(
        url: baseUrl + loginUrl, body: loginSend.toJson());
    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
      twoFactorAuthenticationCode.clear();
    } else {
      twoFactorAuthenticationCode.clear();
      var result = LoginResponseModel.fromJson(data);
      if (result.accessToken != null) {
        debugPrint(result.accessToken);
        hive.put(HiveString.token, result.accessToken);
        hive.put(HiveString.refreshToken, result.refreshToken);

        await profileScreenController.getProfileData();
        registerWithStripe();

        Get.toNamed(dashboard);

        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "Login Successful",
          backgroundColor: Colors.green,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      } else {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.error,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      }
    }
  }

  registerWithStripe() async {
    if (profileScreenController.userData.value.stripeCustomerId == null) {
      RegisterStripeSendModel model = RegisterStripeSendModel(
        email: profileScreenController.userData.value.email.toString(),
        phone: profileScreenController.userData.value.mobile.toString(),
        user_id: profileScreenController.userData.value.id.toString(),
        name: profileScreenController.userData.value.fullName.toString(),
      );
      var data = await CoreService().postWithAuthRaw(
        url: baseUrl + registerWithStripeUrl,
        body: model.toJson(),
      );
      // if (Get.isDialogOpen ?? false) Get.back();

      debugPrint("registerWithStripe::: " + data.toString());

      if (data == null) {
      } else {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            registerWithStripe();
          }
        } else {
          var result = CustomerRegisterStripeResponseModel.fromJson(data);

          if (result.stripeId != null) {
            await profileScreenController.getProfileData().then((value) async {
              await subscribeToBasicPlan(value);
            });
          } else {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Please try again",
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          }
        }
      }
    } else {
      debugPrint("Already registered With Stripe:: stripeCustomerId:: " +
          profileScreenController.userData.value.stripeCustomerId!);
      if (Get.isDialogOpen ?? false) Get.back();
    }
  }

  subscribeToBasicPlan(ProfileResponseModel profileResponseModel) async {
    if (profileResponseModel.membershipType == 'basic') {
      debugPrint(baseUrl + createSubscriptionUrl);
      /*Get.isDialogOpen ?? true
          ? const Offstage()
          : Get.dialog(
              const Center(
                child: CircularProgressIndicator(),
              ),
              barrierDismissible: false);*/

      var data = await CoreService()
          .postWithAuth(url: baseUrl + createSubscriptionUrl, body: {
        "customer_id": profileScreenController.userData.value.stripeCustomerId,
        "price_id": "price_1L2ZuSFSgVYpzNjn3DenTtpu",
        "collection_method": "charge_automatically",
        "product": "basic"
      });

      if (data == null) {
        if (Get.isDialogOpen ?? false) Get.back();
      } else {
        if (data == 401) {
          bool success =
              await CoreService().getNewAccessTokenWithRefreshToken();
          if (success) {
            subscribeToBasicPlan(profileResponseModel);
          }
        } else {
          if (Get.isDialogOpen ?? false) Get.back();
        }
      }
    }
  }

  onContinueButtonGoogleSignUpTap() {
    profileScreenController = Get.put(ProfileScreenController());
    if (phoneController.text.isEmpty && dobController.text.isEmpty) {
      FocusManager.instance.primaryFocus!.unfocus();
      Fluttertoast.showToast(
        timeInSecForIosWeb: 3,
        msg: "Please enter mobile number and date of birth",
        backgroundColor: Colors.red,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        textColor: Colors.white,
        fontSize: 20.0,
      );
    } else if (phoneController.text.isEmpty) {
      FocusManager.instance.primaryFocus!.unfocus();
      Fluttertoast.showToast(
        timeInSecForIosWeb: 3,
        msg: "Please enter mobile number",
        backgroundColor: Colors.red,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        textColor: Colors.white,
        fontSize: 20.0,
      );
    } else if (dobController.text.isEmpty) {
      FocusManager.instance.primaryFocus!.unfocus();
      Fluttertoast.showToast(
        timeInSecForIosWeb: 3,
        msg: "Please enter date of birth",
        backgroundColor: Colors.red,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        textColor: Colors.white,
        fontSize: 20.0,
      );
    } else {
      onSubmitGoogleSignUpMobDob(false);
    }
  }

  removePlusSymbolCountryCode(String code) {
    return code.replaceAll("+", "");
  }

  var phoneVerificationToken = "";

  onSubmitGoogleSignUpMobDob(bool isResendCode) async {
    if (!isResendCode) {
      var data = await updateReferralCode();

      debugPrint("updateReferralCode result:: " + data.toString());

      if (data == false) {
        return;
      }

      await onUpdateDob();
    }

    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);
    GoogleDobSendModel model = GoogleDobSendModel(
      country_code: removePlusSymbolCountryCode(countryCodeController.text),
      mobile: phoneController.text.toString(),
      country_code_label: countryCodeLabelController.text,
      //referral_code: referralCodeController.text,
    );
    var data = await CoreService().putWithAuth(
      url: baseUrl + changeMobileUrl,
      body: model.toJson(),
    );
    if (Get.isDialogOpen ?? false) {
      Get.back();
    }
    if (data == null) {
    } else {
      if (data == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          onSubmitGoogleSignUpMobDob(isResendCode);
        }
      } else {
        var result = ChangePhoneResponseModel.fromJson(data);
        phoneVerificationToken = result.verification_token!;

        if (isResendCode) {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "OTP resend successful",
            backgroundColor: Colors.green,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        }

        if (!isResendCode) {
          Get.put(SignUpController()).isRedirectFromSignupScreen = false;
          isRedirectFromSignInScreen = true;
          Get.toNamed(mobileVerification);
        }
      }
    }
  }

  onUpdateDob() async {
    try {
      Get.isDialogOpen ?? true
          ? const Offstage()
          : Get.dialog(
              const Center(
                child: CircularProgressIndicator(),
              ),
              barrierDismissible: false);
      UpdateProfileSendModel model = UpdateProfileSendModel(
        //name: "Rasel",
        mobile: phoneController.text,
        country_code: countryCodeController.text,
        dob: dobController.text,
      );
      await CoreService()
          .putWithAuth(url: baseUrl + updateNameDobGoogle, body: model.toJson())
          .then((value) async {
        if (value != null) {
          if (value == 401) {
            bool success =
                await CoreService().getNewAccessTokenWithRefreshToken();
            if (success) {
              onUpdateDob();
            }
          } else {
            // Get.back();
            // debugPrint('***********');
          }
        } else {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: "Oops! Some error occured",
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        }
      });
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  Future updateReferralCode() async {
    if (referralCodeController.text.isEmpty) {
      return true;
    }

    debugPrint(baseUrl + updateReferalCodeUrl + emailTextController.text);

    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);

    var value = await CoreService().putWithAuth(
      url: baseUrl + updateReferalCodeUrl + emailTextController.text,
      body: {
        "referal_code": referralCodeController.text,
      },
    );

    if (value != null) {
      if (value == 401) {
        bool success = await CoreService().getNewAccessTokenWithRefreshToken();
        if (success) {
          updateReferralCode();
        }
      } else {
        //Get.back();
        return true;
      }
    } else {
      return false;
    }
  }
}
