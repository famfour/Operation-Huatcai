import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/leads_view_controller.dart';
import 'package:iqrate/Router/route_constants.dart';

class NewPurchaseController extends GetxController {
  late String selectedValue;

  late LeadsViewController leadsViewController;

  var isCalculated = false.obs;

  int leadLength = 0;

  List<String> dropdownLoanApplicant = [
    '1',
    '2',
    '3',
    '4',
  ];

  List<String> leadNamesForDropdown = [];
  String? selectedApplicant = '1';

  String? selectedProperties = '1';
  String? selectedHousingLoad = '1';

  List<String> dropdownEmploymentType = ['Salaried'];
  String? selectedEmploymentType = "Salaried";

  List<String> dropdownLongerLoanTenure = ['No'];
  String? selectedLongerLoanTenure = "No";

  List<String> dropdownPropertyType = ['HDB'];
  String? selectedPropertyType = "HDB";

  List<String> dropdownPropertyStatus = ['Complete'];
  String? selectedPropertyStatus = "Complete";

  var step = 1.obs;
  var isSummeryCheckBoxEnable = true.obs;

  @override
  void onInit() {
    Timer(const Duration(seconds: 0), () {
      Get.isDialogOpen ?? true
          ? const Offstage()
          : Get.dialog(const Center(child: CircularProgressIndicator()),
              barrierDismissible: false);
      leadsViewController = Get.put(LeadsViewController());
      leadLength =
          leadsViewController.fetchLeadListResponseModel.results!.length;
      debugPrint(leadLength.toString());
      for (int i = 0; i < leadLength; i++) {
        if (leadsViewController
            .fetchLeadListResponseModel.results![i].clients!.isNotEmpty) {
          leadNamesForDropdown.add(leadsViewController
              .fetchLeadListResponseModel.results![i].clients![0].name
              .toString());
          selectedValue = leadsViewController
              .fetchLeadListResponseModel.results![0].clients![0].name
              .toString();
          log(selectedValue);
          if (Get.isDialogOpen ?? false) Get.back();
        }
        if (Get.isDialogOpen ?? false) Get.back();
      }
      if (Get.isDialogOpen ?? false) Get.back();
    });
    if (Get.isDialogOpen ?? false) Get.back();

    super.onInit();
  }

  void onTapCalculateExistingLead() {
    if (leadNamesForDropdown.isNotEmpty) {
      Get.toNamed(newPurchaseForExistingLeadScreen);
    } else {
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: "You do not have any existing leads for which you can calculate",
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
    }
  }

  void onTapCalculateNewLead() {
    Get.toNamed(newPurchaseForNewLeadScreen, arguments: selectedApplicant);
  }

  void onCancel() {
    isCalculated.value = false;
    Get.back();
  }

  void onSubmitButtonTap() {
    Get.toNamed(newPurchaseNewLeadCalc,
        arguments: int.parse(selectedApplicant!));
  }

  void onTapSubmitExitingLead() {
    debugPrint(leadsViewController.fetchLeadListResponseModel
        .results![leadNamesForDropdown.indexOf(selectedValue)].clients![0].lead
        .toString());

    Get.toNamed(existingLeadCalculatorScreen,
        arguments: leadsViewController.fetchLeadListResponseModel
            .results![leadNamesForDropdown.indexOf(selectedValue)].clients);
  }
}
