import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Router/route_constants.dart';

class IntroController extends GetxController {
  final PageController controller = PageController(initialPage: 0);

  void toSignUp() {
    Get.offAndToNamed(signUp); // Navigate to the SignUp page
  }
}
