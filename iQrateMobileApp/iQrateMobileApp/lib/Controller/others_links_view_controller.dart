import 'dart:async';
import 'dart:io';
import 'dart:isolate';
import 'dart:ui';

import 'package:android_path_provider/android_path_provider.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Model/response_model.dart/important_links_response_model.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';
import 'package:open_filex/open_filex.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:url_launcher/url_launcher_string.dart';

class OthersLinkViewController extends GetxController {
  Rx<ResourcesLinksResponseModel> othersLinkResponseModel =
      ResourcesLinksResponseModel().obs;
  late String _localPath;
  @override
  void onInit() {
    getPermission();
    Timer(const Duration(seconds: 0), () {
      Get.isDialogOpen ?? true
          ? const Offstage()
          : Get.dialog(const Center(child: CircularProgressIndicator()),
              barrierDismissible: false);
      getOthersLinks();
    });
    prepareSaveDir();
    super.onInit();
  }

  getOthersLinks() async {
    debugPrint(strapiUrl + resourcesCategoryUrl + 'Others');
    var data = await CoreService()
        .getWithoutAuth(url: strapiUrl + resourcesCategoryUrl + 'Others');

    if (Get.isDialogOpen ?? false) Get.back();
    if (data == null) {
      if (Get.isDialogOpen ?? false) Get.back();
    } else {
      othersLinkResponseModel.value =
          ResourcesLinksResponseModel.fromJson(data);
      sortByDisplayOrder(othersLinkResponseModel.value.data);
      if (Get.isDialogOpen ?? false) Get.back();
    }
  }

  sortByDisplayOrder(List<OneResponse>? results) {
    try {
      results?.sort((a, b) => a.displayOrder!.compareTo(b.displayOrder!));
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  launchUrl(url) async {
    try {
      if (await canLaunchUrlString(url)) {
        await launchUrlString(url);
      } else {
        throw "Could not launch $url";
      }
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  onDownloadPdfButtonTap(String url) {
    openFile(
      url: url,
      fileName: "Invoice ${url.split('/').last}.pdf",
    );
  }

  void getPermission() async {
    await Permission.storage.request();
  }

  Future<void> prepareSaveDir() async {
    _localPath = (await _findLocalPath())!;
    final savedDir = Directory(_localPath);
    bool hasExisted = await savedDir.exists();
    if (!hasExisted) {
      savedDir.create();
    }
  }

  Future<String?> _findLocalPath() async {
    String? externalStorageDirPath;
    if (Platform.isAndroid) {
      try {
        externalStorageDirPath = await AndroidPathProvider.downloadsPath;
      } catch (e) {
        final directory = await getExternalStorageDirectory();
        externalStorageDirPath = directory?.path;
      }
    } else if (Platform.isIOS) {
      externalStorageDirPath =
          (await getApplicationDocumentsDirectory()).absolute.path;
    }
    return externalStorageDirPath;
  }

  Future openFile({String? url, String? fileName}) async {
    debugPrint('url' + url!);

    final file = await downloadFile(url, fileName!);
    if (file == null) return;

    debugPrint('Path: ${file.path}');

    OpenFilex.open(file.path);
  }

  // ignore: body_might_complete_normally_nullable
  Future<File?> downloadFile(String url, String name) async {
    Get.snackbar("Downloading", "Download Started",
        backgroundColor: Colors.green, colorText: Colors.white);
    Future.delayed(const Duration(milliseconds: 0), () {
      FocusManager.instance.primaryFocus!.unfocus();
      Fluttertoast.showToast(
        timeInSecForIosWeb: 3,
        msg: "Downloading",
        backgroundColor: Colors.green,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        textColor: Colors.white,
        fontSize: 20.0,
      );
    });
    final taskId = await FlutterDownloader.enqueue(
        url: url,
        savedDir: _localPath,
        showNotification:
            true, // show download progress in status bar (for Android)
        openFileFromNotification:
            true, // click on notification to open downloaded file (for Android)
        saveInPublicStorage: true);

    Get.snackbar("Downloaded", "Download completed",
        backgroundColor: Colors.green,
        colorText: Colors.white,
        mainButton: TextButton(
            onPressed: () async {
              await FlutterDownloader.open(taskId: taskId!);
            },
            child: const Text(
              "Open",
              style: TextStyle(color: Colors.white),
            )),
        duration: const Duration(seconds: 7));
  }

  static void downloadCallback(
      String id, DownloadTaskStatus status, int progress) {
    if (kDebugMode) {
      print(
          'Background Isolate Callback: task ($id) is in status ($status) and process ($progress)');
    }

    final SendPort send =
        IsolateNameServer.lookupPortByName('downloader_send_port')!;
    send.send([id, status, progress]);
  }
}
