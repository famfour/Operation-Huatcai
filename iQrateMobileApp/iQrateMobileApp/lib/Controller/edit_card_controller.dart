import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Router/route_constants.dart';

class EditCardController extends GetxController {

  late TextEditingController nameOnCardTextEditingController;
  late TextEditingController cardNumberTextEditingController;
  late TextEditingController expirationMonthTextEditingController;
  late TextEditingController expirationYearTextEditingController;
  late TextEditingController cvcTextEditingController;
  late TextEditingController countryTextEditingController;
  late TextEditingController postalCodeController;

  late RxBool checkedValue = true.obs;

  @override
  void onInit() {
    nameOnCardTextEditingController = TextEditingController();
    cardNumberTextEditingController = TextEditingController();
    expirationMonthTextEditingController = TextEditingController();
    expirationYearTextEditingController = TextEditingController();
    cvcTextEditingController = TextEditingController();
    countryTextEditingController = TextEditingController();
    postalCodeController = TextEditingController();
    super.onInit();
  }


  addNewCard() async {
    Get.toNamed(addNewCardScreen);
  }

  void onSave() {

  }

}
