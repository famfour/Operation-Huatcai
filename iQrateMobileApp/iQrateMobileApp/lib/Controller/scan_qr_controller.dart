// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/enable_twofa_view_controller.dart';
import 'package:iqrate/Controller/two_fact_auth_screen2_controller.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:url_launcher/url_launcher.dart';

class ScanQrController extends GetxController {
  TwoFactAuthScreenTwoController twoFactAuthScreenTwoController = Get.find();
  Enable2FAViewController enable2faViewController =
      Get.put(Enable2FAViewController());

  @override
  onInit() {
    getQrCode();
    super.onInit();
  }

  getQrCode() async {
    Get.isDialogOpen ?? true
        ? const Offstage()
        : Get.dialog(
            const Center(
              child: CircularProgressIndicator(),
            ),
            barrierDismissible: false);
    if (Get.isDialogOpen ?? false) Get.back();

    // var data = await CoreService().postWithAuth(url: baseUrl + twoMfaUrl);
  }

  onSubmit() {
    _launchURL(twoFactAuthScreenTwoController
        .enableTwoMfaResponseModel.value.otpauthUrl);
    Get.toNamed(authenticatorOtp);
  }

  onCancel() {
    enable2faViewController.switchValue.value = false;
    Get.toNamed(dashboard);
  }

  void _launchURL(_url) async {
    if (!await launch(_url)) throw 'Could not launch $_url';
  }
}
