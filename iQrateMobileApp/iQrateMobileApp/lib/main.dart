import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:iqrate/Controller/shared_prefs_controller.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'DeviceManager/hive_string.dart';
import 'Router/nav_router.dart';
import 'lang/localization_service.dart';

void main() async {
  WidgetsFlutterBinding
      .ensureInitialized(); // This helps in using of widgets before the app has been initialized

  Stripe.publishableKey =
      'pk_test_51KThJAFSgVYpzNjnMmY8XWelfoK65q41V0kE2xm48db3QbbjeWqJ1WEap6Z8S3Qi0nAOr88zq4t3n0HvYGOhAHnr00RpQdPAN0';
  await Hive
      .initFlutter(); //Hive is used for storing of strings like user name and user ids etc.
  await Hive.openBox(HiveString.hiveName);
  // ignore: unused_local_variable
  final SharedPrefController sharedPrefController =
      Get.put(SharedPrefController());

  await FlutterDownloader.initialize(
      debug: true // optional: set false to disable printing logs to console
      );

  FlutterDownloader.registerCallback(TestClass.callback);

  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]).then((_) {
    runApp(const IQrate());
  });
}

class TestClass {
  static void callback(String id, DownloadTaskStatus status, int progress) {}
}

class IQrate extends StatelessWidget {
  const IQrate({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    //return GetBuilder<TwoFactAuthQuestionController>(builder: (_){
    return GetMaterialApp(
      //Get material app is used to enable the use of get as the state management package
      builder: (context, widget) => ResponsiveWrapper.builder(
          //This is used to make the app responsive. The breakpoints are what define when the app is supposed to change its state. This is particularly useful if the same app is being used for tabs, different sizes of mobile phones as well as the web.
          BouncingScrollWrapper.builder(context, widget!),
          maxWidth: 2460,
          minWidth: 450,
          defaultScale: true,
          breakpoints: const [
            ResponsiveBreakpoint.resize(450, name: MOBILE),
            ResponsiveBreakpoint.autoScale(800, name: TABLET),
            ResponsiveBreakpoint.autoScale(1000, name: TABLET),
            ResponsiveBreakpoint.resize(1200, name: DESKTOP),
            ResponsiveBreakpoint.autoScale(2460, name: "4K"),
          ],
          background: Container(color: const Color(0xFFF5F5F5))),
      debugShowCheckedModeBanner: false,
      initialRoute: initialScreen, //The 1st route when the app is launched.
      // home: Testing(),
      // initialRoute: myDocumentsScreen, //The 1st route when the app is launched.
      // initialRoute: initialScreen, //The 1st route when the app is launched is initail screen.
      locale: LocalizationService
          .locale, //Currently only english US. This is used for using the app with different languages.
      fallbackLocale: LocalizationService.fallbackLocale,
      translations: LocalizationService(),
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: LocalizationService.locales,
      defaultTransition: Transition.native,
      getPages: NavRouter
          .generateRoute, //For navigating to different pages and generating the routes.
      title: 'iQrate',
      theme: ThemeData(
        primarySwatch: Colors.red,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
    //});
  }
}
