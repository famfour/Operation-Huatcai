import 'package:flutter/material.dart';

enum KeyStore {
  lang,
  email,
  phone,
  isForget,
  phoneOrEmail,
  isSetPassword,
  authId,
  formationId,
  locationId,
  job,
  jobId
}

class ScopeStorage {
  var scopeData = <KeyStore, Object>{};

  void saveData({KeyStore? store, Object? object}) {
    scopeData[store!] = object!;
    debugPrint('Successfully saved');
  }

  void deleteData({KeyStore? scope}) {
    scopeData.remove(scope);
    debugPrint('deleteData Success');
  }

  Object? getData({KeyStore? store}) {
    debugPrint('getData: ${scopeData[store]}');
    return scopeData[store];
  }

  void updateDataWhileNotPresent({KeyStore? store, Object? object}) {
    scopeData.putIfAbsent(store!, () => object!);
    debugPrint('updateDataWhileNotPresent Success');
  }

  void replaceData({KeyStore? store, Object? object}) {
    scopeData.update(store!, (e) => object!);
    debugPrint('replaceData Success');
  }

  void clear() {
    scopeData.clear();
    debugPrint('clear Success');
  }
}
