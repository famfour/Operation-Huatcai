// ignore_for_file: constant_identifier_names

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:iqrate/Model/response_model.dart/kv_response_model.dart';
import 'package:path_provider/path_provider.dart';
import 'vm.dart';

class Keys {
  static const String LOGGEDIN = "LOGGEDIN";
  static const String UUID = "UUID";
  static const String UNAME = "UNAME";
  static const String UIMAGE = "UIMAGE";
  static const String PHONE = "PHONE";
  static const String FBNAME = "FBNAME";
  static const String FBEMAIL = "FBEMAIL";
  static const String EMAIL = "EMAIL";
  static const String FBPHOTOURL = "FBPHOTOURL";
  static const String LOGINTYPE = "LOGINTYPE";
  static const String APPLEIDNAME = "APPLEIDNAME";
  static const String APPLEIDEMAIL = "APPLEIDEMAIL";
  static const String APPLEIDPHOTO = "APPLEIDPHOTO";
}

class HiveStore {
  //Singleton Class
  static final HiveStore _default = HiveStore._internal();
  static Box? defBox;

  factory HiveStore() {
    return _default;
  }

  HiveStore._internal();

  static getInstance() {
    return _default;
  }

  initBox() async {
    defBox = await openBox();
  }

  //Object Storage
  put(String key, Object value) async {
    defBox!.put(key, value);
    debugPrint("HiveStored : Key:$key, Value:$value");
  }

  get(String key) {
    debugPrint("Box is Open? ${defBox!.isOpen}");
    debugPrint("HiveRetrive : Key:$key, Value:${defBox!.get(key)}");
    return defBox!.get(key);
  }

  clear() {
    defBox!.clear();
  }

  delete(String key) async {
    defBox!.delete(key);
  }

  Future openBox() async {
    if (!isBrowser) {
      var dir = await getApplicationDocumentsDirectory();
      Hive.init(dir.path);
    }
    return await Hive.openBox('Store');
  }

  static void saveKvValues(KvValueStoreResponseModel result) {}
}
