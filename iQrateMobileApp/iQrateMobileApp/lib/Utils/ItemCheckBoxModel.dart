// ignore_for_file: file_names, unnecessary_this

import 'package:iqrate/Model/response_model.dart/safe_convert.dart';

class ItemCheckBoxModel {
  String name = "";
  String value = "";
  bool isChecked = false;

  ItemCheckBoxModel({
    required this.name,
    required this.value,
    required this.isChecked,
  });

  ItemCheckBoxModel.fromJson(Map<String, dynamic> json)
      : name = SafeManager.parseString(json, 'name'),
        value = SafeManager.parseString(json, 'value'),
        isChecked = SafeManager.parseBoolean(json, 'isChecked');

  Map<String, dynamic> toJson() => {
        'name': this.name,
        'value': this.value,
        'isChecked': this.isChecked,
      };
}
