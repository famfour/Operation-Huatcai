// ignore_for_file: must_be_immutable

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Utils/ItemCheckBoxModel.dart';
import 'package:iqrate/Utils/testing.dart';

class MultiSelect extends StatefulWidget {
  final String titleDropdown;
  final Function(List<ItemCheckBoxModel>) didChoose;
  List<ItemCheckBoxModel> dataSourceItemOutSide;
  MultiSelect(
      {Key? key,
      required this.dataSourceItemOutSide,
      required this.didChoose,
      required this.titleDropdown})
      : super(key: key);
  @override
  State<StatefulWidget> createState() => _MultiSelectState();
}

class _MultiSelectState extends State<MultiSelect> {
  var dataSourceItem = <ItemCheckBoxModel>[];
  // var dataSourceCheck = <ItemCheckBoxModel>[];

  // var dataSourceCheck = <ItemCheckBoxModel>[];
  final _filter = TextEditingController();
  String _searchText = "";
  var filteredDataSourceCheck = <ItemCheckBoxModel>[];

  setupSearchListener() {
    _filter.addListener(() {
      if (_filter.text.isEmpty) {
        setState(() {
          _searchText = "";
          filteredDataSourceCheck = dataSourceItem;
        });
      } else {
        setState(() {
          _searchText = _filter.text;
          if (kDebugMode) {
            print(_searchText);
          }
        });
      }
    });
  }

  @override
  void dispose() {
    _filter.dispose();
    super.dispose();
  }

  countTrue(List<ItemCheckBoxModel> datasource) {
    var count = 0;
    for (var i = 0; i < datasource.length; i++) {
      if (datasource[i].isChecked) count += 1;
    }
    if (kDebugMode) {
      print("count true $count");
    }
  }

  @override
  void initState() {
    super.initState();
    if (kDebugMode) {
      print("initState MultiSelect run init");
    }
    dataSourceItem.clear();
    filteredDataSourceCheck.clear();
    setupSearchListener();
    // for(var item in widget.dataSourceItemOutSide){
    //   ItemCheckBoxModel newItem =  ItemCheckBoxModel(
    //       name: item.name,
    //       value: item.value,
    //       isChecked: item.isChecked
    //   );
    //   dataSourceItem.add(newItem);
    //   dataSourceCheck.add(newItem);
    // }
    for (var i = 0; i < widget.dataSourceItemOutSide.length; i++) {
      var item = widget.dataSourceItemOutSide[i];
      ItemCheckBoxModel newItem = ItemCheckBoxModel(
          name: item.name, value: item.value, isChecked: item.isChecked);
      dataSourceItem.add(newItem);
      filteredDataSourceCheck.add(newItem);
    }
    //
    // for (var i = 0; i < widget.dataSourceItemOutSide.length; i++) {
    //   var link = widget.dataSourceItemOutSide[i];
    //   dataSourceOld.add(link);
    // }
    if (kDebugMode) {
      print("dataSourceItem after init from widget");
    }
    countTrue(dataSourceItem);
  }

  void _itemChange(String itemValue, bool isSelected, int index) {
    // dataSourceCheck[index].isChecked = isSelected;
    // setState(() {});
    filteredDataSourceCheck[index].isChecked = isSelected;
    setState(() {});
  }
  // this function is called when the Cancel button is pressed
  // void _cancel() {
  //   // dataSourceCheck = dataSourceItem;
  //   widget.didChoose(dataSourceItem);
  //   countTrue(dataSourceItem);
  //   print("_submcancelled");
  //   setState(() {
  //
  //   });
  //   Navigator.pop(context);
  // }

// this function is called when the Submit button is tapped
//   void _submit() {
//     print("_submit clicked newlist have ${dataSourceItem.length}");
//     dataSourceItem = dataSourceCheck;
//     widget.didChoose(dataSourceCheck);
//     // getTestingLogic().dataSourceItem = dataSourceItem;
//     countTrue(dataSourceItem);
//     Navigator.pop(context);
//   }

  TestingLogic getTestingLogic() {
    if (!Get.isRegistered<TestingLogic>()) {
      Get.put(TestingLogic());
    }
    return Get.find<TestingLogic>();
  }

  void _cancel() {
    // dataSourceCheck = dataSourceItem;
    // Navigator.pop(context);
    _filter.clear();
    filteredDataSourceCheck = dataSourceItem;
    Navigator.pop(context);
  }

  void _submit() {
    // // countTrue(dataSourceItem);
    // dataSourceItem = dataSourceCheck;
    // // getTestingLogic().dataSourceItem = dataSourceItem;
    // widget.didChoose(dataSourceItem);
    // Navigator.pop(context);
    _filter.clear();
    dataSourceItem = filteredDataSourceCheck;
    widget.didChoose(dataSourceItem);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    if (kDebugMode) {
      print("_MultiSelectState BuildContext clicked");
    }
    if (_searchText.isNotEmpty) {
      var tempList = <ItemCheckBoxModel>[];
      for (var item in filteredDataSourceCheck) {
        if (item.name.toLowerCase().contains(_searchText.toLowerCase())) {
          tempList.add(item);
        }
      }
      filteredDataSourceCheck = tempList;
    }

    return AlertDialog(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10))),
      // scrollable : true,//bo vao tach luon!
      // title: Text(widget.titleDropdown),
      title: TextField(
        controller: _filter,
        decoration: InputDecoration(
          prefixIcon: const Icon(Icons.search),
          hintText: 'Search',
          suffixIcon: IconButton(
            iconSize: 14,
            onPressed: _filter.clear,
            icon: const Icon(Icons.clear),
          ),
        ),
      ),
      content: SizedBox(
        width: double.maxFinite,
        // height: double.maxFinite,
        child: ListView.builder(
            shrinkWrap: true,
            itemCount: filteredDataSourceCheck.length,
            addAutomaticKeepAlives: true,
            itemBuilder: (context, index) {
              return CheckboxListTile(
                  value: (filteredDataSourceCheck[index].isChecked),
                  title: Text(filteredDataSourceCheck[index].name),
                  controlAffinity: ListTileControlAffinity.leading,
                  onChanged: (isChecked) {
                    _itemChange(
                        filteredDataSourceCheck[index].name, isChecked!, index);
                  });
            }),
      ),
      actions: [
        TextButton(
          child: const Text('Cancel'),
          onPressed: _cancel,
        ),
        TextButton(
          child: const Text('Confirm'),
          onPressed: _submit,
        ),
        // ElevatedButton(
        //   child: const Text('Submit'),
        //   onPressed: _submit,
        // ),
      ],
    );
  }
}
