// ignore_for_file: prefer_adjacent_string_concatenation

import 'package:intl/intl.dart';

class AppConfig {
  static String defaultCountryCode = "+65";
  static String defaultCountryLabel = "SG";
  static String blankImage =
      "https://app.iqrate.io/assets/images/logo.png";

  static bool validatePassword(String value) {
    RegExp regExp1 = RegExp(r'^(?=.*[a-zA-Z])(?=.*[*".!@#\$%^&(){}:;<>,.\' +
        r"'?/~_+-=])(?=.*[0-9]).{8,30}");

    if (!regExp1.hasMatch(value)) {
      return false;
    } else {
      return true;
    }
  }

  static bool validatePassword2(String value) {
    RegExp regExp2 = RegExp(r'^(?=.*[A-Z])');

    if (!regExp2.hasMatch(value)) {
      return false;
    } else {
      return true;
    }
  }

  static String extraDecimalAdd(String amount) {
    if (amount.isNotEmpty && !amount.trim().contains(".")) {
      double intAmount = (int.parse(amount)) / 100;
      return intAmount.toStringAsFixed(2).toString();
    }
    return amount;
  }

  static String extraDecimalAdd2Digit(String amount) {
    if (amount.isNotEmpty && !amount.trim().contains(".")) {
      return amount+".00";
    }
    return amount;
  }

  static String getFormattedAmount(String s) {
    const _locale = 'en';
    String _formatNumber =
    NumberFormat.decimalPattern(_locale).format(double.parse(s)).toString();

    return _formatNumber;
  }

  static String getDateFormat(String date) {
    DateFormat dateFormat = DateFormat("yyyy-MM-dd");
    DateTime dateTime = dateFormat.parse(date);

    DateFormat dateFormat2 = DateFormat("dd-MM-yyyy");
    String formattedDate = dateFormat2.format(dateTime);
    return formattedDate;
  }

  static String getDateFormat2(String date) {
    DateFormat dateFormat = DateFormat("dd-MM-yyyy");
    DateTime dateTime = dateFormat.parse(date);

    DateFormat dateFormat2 = DateFormat("yyyy-MM-dd");
    String formattedDate = dateFormat2.format(dateTime);
    return formattedDate;
  }

  static String getDateFormatForEmailLog(String date) {
    DateFormat dateFormat = DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    DateTime dateTime = dateFormat.parse(date);

    DateFormat dateFormat2 = DateFormat("dd MMMM yyyy");
    String formattedDate = dateFormat2.format(dateTime);
    return formattedDate;
  }

  static String getDateTimeFormatForEmailLog(String date) {
    DateFormat dateFormat = DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    DateTime dateTime = dateFormat.parse(date);

    DateFormat dateFormat2 = DateFormat("dd/MM/yyyy - hh:mma");

    var utcDate = dateFormat2.format(DateTime.parse(dateTime.toString()));
    var localDate = dateFormat2.parse(utcDate, true).toLocal().toString();
    String createdDate = dateFormat2.format(DateTime.parse(localDate));

    return createdDate;
  }

  static String getPlainAmount(String amount) {
    return amount != "" ? amount.replaceAll(",", "") : "0";
  }

  static double getDoubleAmount(String amount) {
    return double.parse(getPlainAmount(amount));
  }
}
