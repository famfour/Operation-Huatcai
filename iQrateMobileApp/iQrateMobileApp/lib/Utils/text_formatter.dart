import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

class CardNumberFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue oldValue,
    TextEditingValue newValue,
  ) {
    var inputText = newValue.text;

    if (newValue.selection.baseOffset == 0) {
      return newValue;
    }

    var bufferString = StringBuffer();
    for (int i = 0; i < inputText.length; i++) {
      bufferString.write(inputText[i]);
      var nonZeroIndexValue = i + 1;
      if (nonZeroIndexValue % 4 == 0 && nonZeroIndexValue != inputText.length) {
        bufferString.write(' ');
      }
    }

    var string = bufferString.toString();
    return newValue.copyWith(
      text: string,
      selection: TextSelection.collapsed(
        offset: string.length,
      ),
    );
  }
}

getAmountString(String string) {
  var temp = string.replaceAll(",", "");
  return temp.toString();
}
convertToCurrency(dynamic data){
  if (data == "") {
    return "";
  }
  if (data is String && data != "") {
    var aa = getAmountString(data);
    var a = double.parse(aa);
    final oCcy = NumberFormat("#,##0.00", "en_US");
    return "\$" + oCcy.format(a);
  } else {
    // print("this case is not string");
    final oCcy = NumberFormat("#,##0.00", "en_US");
    return "\$" + oCcy.format(data);
  }
}