// ignore_for_file: unnecessary_null_comparison, prefer_typing_uninitialized_variables, prefer_final_fields, unused_local_variable, prefer_adjacent_string_concatenation, avoid_types_as_parameter_names, non_constant_identifier_names

import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/screen_constants.dart';
import 'package:iqrate/DeviceManager/strings.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Service/string_utils.dart';
import 'package:iqrate/Utils/config.dart';
import 'package:iqrate/Widgets/box_entry_text_field.dart';
import 'package:iqrate/Widgets/box_entry_text_field_popup_menu.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/otp_field_style.dart';
import 'package:otp_text_field/style.dart';

// List for all TypeRequireTextFieldV2s of form fields that could be used throughout the app. This file contains all TypeRequireTextFieldV2s of form fields along with the validations that are needed.
enum TypeRequireTextFieldV2 {
  numberInt,
  percent,
  money,
  number,
  emailOrPhone,
  passWord,
  passWordWithClear,
  firstName,
  email,
  emailWithClear,
  otp,
  otpV2,
  search,
  searchLeft,
  searchWithActionButton,
  lastName,
  phone,
  text,
  pdpa,
  fullname,
  fullnameWithClear,
  customText,
  optional,
  optionalWithClear,
  optionalTextArea,
  textArea,
  date,
  dob,
  dropdownForLeadCountry,
  dropdownCountryCodeWithPhone,
  dropdownCountryCodeWithPhoneClear,
  dropDownCountry,
  otpPopUpMenu,
  expiry
}

// ignore: must_be_immutable
class RequireTextFieldV2 extends StatefulWidget {
  final TextEditingController
      controller; //Global controller variable is declared so that any controller can be passed to it.
  final TypeRequireTextFieldV2
      type; //Form field TypeRequireTextFieldV2. Used to determine what validations should be used by the field.
  final caption;
  var errorFree;
  FocusNode?
      myFocusNode; //Which node is being currently focused. This is used for OTP and other OTP like validations
  final hintText; //Hints within the form field
  int maxNumber;
  final mobileNumber;
  final readWriteStatus;
  final onTap; //What happens when the form field is tapped upon.
  final labelText; //Label Text for the form field
  final autoValidate; //Validates as the user TypeRequireTextFieldV2s the email or password or whatever validation is required.
  final autoSubmit; // Submits the form if all fields are filled up correctly. Preferred to be false, so that the user can check if any mistake are made from his/her end.
  final verified; //Checks if fields are verified.
  final suggestions; //Auto-complete suggestions.
  final autoKey;
  final isPast;
  final onSubmit; //On submit function is used when the form is submitted.
  final onSubmitSearch; //On submit function is used when the form is submitted.
  final maxLength; //Maximum length on a textField.
  final TextEditingController? countryCodeController;
  final TextEditingController? countryCodeLabelController;
  final onChanged;
  final dateInputFormatter;
  final numberInputFormatter;
  final textInputAction;
  final String dateFormat;
  String dropdownForLeadCountryDefaultValue;
  bool readOnly;
  String? Function(String?)? validator;
  final OtpFieldController? otpFieldController;
  // final formState
  // final formKey = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey;

  RequireTextFieldV2({
    Key? key,
    this.type = TypeRequireTextFieldV2.text,
    required this.controller,
    this.onSubmit,
    this.maxNumber = 0,
    this.countryCodeController,
    this.countryCodeLabelController,
    this.caption,
    this.errorFree,
    this.myFocusNode,
    this.hintText,
    this.mobileNumber,
    this.readWriteStatus,
    this.onTap,
    this.labelText,
    this.autoValidate,
    this.autoSubmit,
    this.verified = false,
    this.suggestions,
    this.autoKey,
    this.isPast = false,
    this.maxLength,
    this.onChanged,
    this.dateInputFormatter,
    this.numberInputFormatter,
    this.textInputAction,
    this.dateFormat = "dd-MM-yyyy",
    this.dropdownForLeadCountryDefaultValue = "Singapore Citizens",
    this.readOnly = false,
    this.onSubmitSearch,
    this.validator,
    this.otpFieldController,
    required this.formKey,
  }) : super(key: key);

  @override
  _RequireTextFieldV2State createState() => _RequireTextFieldV2State();
}

class _RequireTextFieldV2State extends State<RequireTextFieldV2> {
  late TypeRequireTextFieldV2 typeRequireTextFieldV2;

  var _value;

  bool _errorFree = true;

  bool validate = false;

  bool? readOnlyEmail;

  get errorFree => _errorFree;

  // TypeRequireTextFieldV2 get _typeRequireTextFieldV2 => widget.typeRequireTextFieldV2;

  var searchTextField;

  TextEditingController get _thisController => widget.controller;
  late GlobalKey<NavigatorState> navigatorKey;
  bool indicator = true; //indicates if the field is selected or not

  FocusNode _focus = FocusNode();

  //initialize everything before the page loads so that it doesn't throw an error later.
  @override
  void initState() {
    super.initState();
    typeRequireTextFieldV2 = widget.type;
    navigatorKey = GlobalKey<
        NavigatorState>(); //navigator key for validations and checking
    //if the controller is null(has no data) do nothing else start validating as the user TypeRequireTextFieldV2s.
    if (widget.controller == null) {
    } else {
      widget.controller.addListener(_handleControllerChanged);
    }
    if (widget.mobileNumber != null) {
      _thisController.text = widget.mobileNumber;
    }
  }

  //What needs to be done when the controller finds the changing data
  void _handleControllerChanged() {
    if (_thisController.text != _value || _value.trim().isNotEmpty) {
      didChange(_thisController.text);
    }
  }

  //As and when the data changes, the value starts getting updated.
  void didChange(var value) {
    setState(() {
      _value = value;
    });
  }

  @override
  void didUpdateWidget(RequireTextFieldV2 oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.controller != oldWidget.controller) {
      oldWidget.controller.removeListener(_handleControllerChanged);
      widget.controller.addListener(_handleControllerChanged);

      if (widget.controller == null) {
        if (widget.controller != null) {
          _thisController.text = widget.controller.text;
          //if (oldWidget.controller == null) _controller = null;
        }
      }
    }
  }

  //Disposing the listener to avoid taking up extra memory.
  @override
  void dispose() {
    widget.controller.removeListener(_handleControllerChanged);
    super.dispose();
  }

  String dropdownValueCountry = 'Singapore';
  String dropdownForLeadCountryDefaultValue = 'Singapore Citizens';
  String dropdownInitialValue = '+65';

  @override
  Widget build(BuildContext context) {
    switch (typeRequireTextFieldV2) {
      case TypeRequireTextFieldV2.numberInt:
        {
          return Form(
            key: widget.formKey,
            child: TextFormField(
              onChanged: (value) {
                widget.formKey.currentState!.validate();
              },
              // The validator receives the text that the user has entered.

              controller: _thisController,
              //Controller for the form field
              // keyboardType: TextInputType.number,
              keyboardType:
                  const TextInputType.numberWithOptions(decimal: false),
              //Keyboard TypeRequireTextFieldV2 for the form field
              autofocus: false,
              validator: widget.validator,
              //Autofocus for the form field
              maxLines: 1,
              //Maximum lines for the form field
              autovalidateMode: AutovalidateMode.onUserInteraction,
              // inputFormatters: widget.numberInputFormatter,
              inputFormatters: <TextInputFormatter>[
                FilteringTextInputFormatter.allow(RegExp("[0-9]")),
              ],
              //Autovalidate mode for the form field
              // validator: validatePassword, //Validator for the form field
              obscureText: false,
              //Obscure text for the form field
              style: TextStyles.lowerSubTitle,
              //Style for the form field
              maxLength: widget.maxLength,
              decoration: InputDecoration(
                //Decoration for the form field starts here
                hintText: widget.hintText,
                //Hint text for the form field
                hintStyle: TextStyles.lowerSubTitleHintText,
                //Hint style for the form field
                errorMaxLines: 2,
                //Maximum lines for the error text
                labelStyle: TextStyles.lowerSubTitle,
                //Label style for the form field
                // suffix: const Text("%"),
                suffixStyle: TextStyles.lowerSubTitle,
                enabledBorder: const OutlineInputBorder(
                  borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                  borderRadius: BorderRadius.all(
                    Radius.circular(10.0),
                  ),
                ),
                focusedBorder: const OutlineInputBorder(
                  borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                  borderRadius: BorderRadius.all(
                    Radius.circular(10.0),
                  ),
                ),
                border: const OutlineInputBorder(
                  borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                  borderRadius: BorderRadius.all(
                    Radius.circular(10.0),
                  ),
                ),
                filled: true,
                //Filled for the form field
                fillColor: AppColors.white, //Fill color for the form field
              ),
            ),
          );
        }
      //percent
      case TypeRequireTextFieldV2.percent:
        {
          return Form(
            key: widget.formKey,
            child: TextFormField(
              onChanged: (value) {
                widget.formKey.currentState!.validate();
              },
              // The validator receives the text that the user has entered.
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Required!';
                } else if (int.parse(value) > 100) {
                  return 'Max is 100!';
                }
                return null;
              },
              controller: _thisController,
              //Controller for the form field
              keyboardType:
                  const TextInputType.numberWithOptions(decimal: true),
              //Keyboard TypeRequireTextFieldV2 for the form field
              autofocus: false,
              //Autofocus for the form field
              maxLines: 1,
              //Maximum lines for the form field
              autovalidateMode: AutovalidateMode.onUserInteraction,
              inputFormatters: widget.numberInputFormatter,
              //Autovalidate mode for the form field
              // validator: validatePassword, //Validator for the form field
              obscureText: false,
              //Obscure text for the form field
              style: TextStyles.lowerSubTitle,
              //Style for the form field
              maxLength: widget.maxLength,
              decoration: InputDecoration(
                //Decoration for the form field starts here
                hintText: widget.hintText,
                //Hint text for the form field
                hintStyle: TextStyles.lowerSubTitleHintText,
                //Hint style for the form field
                errorMaxLines: 2,
                //Maximum lines for the error text
                labelStyle: TextStyles.lowerSubTitle,
                //Label style for the form field
                suffix: const Text("%"),
                suffixStyle: TextStyles.lowerSubTitle,
                enabledBorder: const OutlineInputBorder(
                  borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                  borderRadius: BorderRadius.all(
                    Radius.circular(10.0),
                  ),
                ),
                focusedBorder: const OutlineInputBorder(
                  borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                  borderRadius: BorderRadius.all(
                    Radius.circular(10.0),
                  ),
                ),
                border: const OutlineInputBorder(
                  borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                  borderRadius: BorderRadius.all(
                    Radius.circular(10.0),
                  ),
                ),
                filled: true,
                //Filled for the form field
                fillColor: AppColors.white, //Fill color for the form field
              ),
            ),
          );
        }
      // money
      case TypeRequireTextFieldV2.money:
        {
          return TextFormField(
            controller: _thisController,
            //Controller for the form field
            keyboardType: TextInputType.number,
            //Keyboard TypeRequireTextFieldV2 for the form field
            autofocus: false,
            //Autofocus for the form field
            maxLines: 1,
            //Maximum lines for the form field
            autovalidateMode: AutovalidateMode.onUserInteraction,
            // inputFormatters: widget.numberInputFormatter,
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.allow(RegExp("[0-9]")),
            ],
            //Autovalidate mode for the form field
            // validator: validatePassword, //Validator for the form field
            obscureText: false,
            //Obscure text for the form field
            style: TextStyles.lowerSubTitle,
            //Style for the form field
            validator: widget.validator,
            maxLength: widget.maxLength,
            onChanged: widget.onChanged,
            decoration: InputDecoration(
              // counter: Container(),
              //Decoration for the form field starts here
              hintText: widget.hintText,
              //Hint text for the form field
              hintStyle: TextStyles.lowerSubTitleHintText,
              //Hint style for the form field
              errorMaxLines: 2,
              //Maximum lines for the error text
              labelStyle: TextStyles.lowerSubTitle,
              //Label style for the form field
              // prefix: const Text("\$ "),
              prefix: _thisController.text.isNotEmpty
                  ? const Text('\$')
                  : const SizedBox(),
              prefixStyle: TextStyles.lowerSubTitle,
              enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              border: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              filled: true,
              //Filled for the form field
              fillColor: AppColors.white, //Fill color for the form field
            ),
          );
        }
      // Number only input
      case TypeRequireTextFieldV2.number:
        {
          return TextFormField(
            controller: _thisController,
            //Controller for the form field
            keyboardType: const TextInputType.numberWithOptions(decimal: true),
            //Keyboard TypeRequireTextFieldV2 for the form field
            autofocus: false,
            //Autofocus for the form field
            maxLines: 1,
            //Maximum lines for the form field
            autovalidateMode: AutovalidateMode.onUserInteraction,
            inputFormatters: widget.numberInputFormatter,
            //Autovalidate mode for the form field
            validator: widget.validator, //Validator for the form field
            obscureText: false,
            //Obscure text for the form field
            style: TextStyles.lowerSubTitle,
            //Style for the form field
            // validator: validateForNumber,

            maxLength: widget.maxLength,
            // onChanged: widget.onChanged,
            onChanged: (value) {
              widget.formKey.currentState?.validate();
            },
            decoration: InputDecoration(
              // counter: Container(),
              //Decoration for the form field starts here
              hintText: widget.labelText,
              //Hint text for the form field
              hintStyle: TextStyles.lowerSubTitleHintText,
              //Hint style for the form field
              errorMaxLines: 2,
              //Maximum lines for the error text
              labelStyle: TextStyles.lowerSubTitle,
              //Label style for the form field
              // suffixIcon: IconButton(
              //   //Suffix icon for the form field
              //   icon: Icon(
              //     indicator
              //         ? Icons
              //             .visibility_off_outlined //If the indicator is true, then the icon is the eye closed icon
              //         : Icons
              //             .visibility_outlined, //If the indicator is false, then the icon is the eye open icon
              //     color: AppColors.grey625F5A,
              //   ),
              //   onPressed: () {
              //     setState(() {
              //       indicator = !indicator; //Toggle the indicator
              //     });
              //   },
              // ),
              //labelText: widget.labelText,
              enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              border: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              filled: true,
              //Filled for the form field
              fillColor: AppColors.white, //Fill color for the form field
            ),
          );
        }
      //Password form field
      case TypeRequireTextFieldV2.passWord:
        {
          return TextFormField(
            controller: _thisController,
            //Controller for the form field
            keyboardType: TextInputType.text,
            //Keyboard TypeRequireTextFieldV2 for the form field
            autofocus: false,
            //Autofocus for the form field
            maxLines: 1,
            //Maximum lines for the form field
            autovalidateMode: AutovalidateMode.onUserInteraction,
            //Autovalidate mode for the form field
            validator: validatePassword,
            //Validator for the form field
            obscureText: indicator,
            //Obscure text for the form field
            style: TextStyles.lowerSubTitle,
            //inputFormatters: [ FilteringTextInputFormatter.allow(RegExp("[a-zA-Z]")), ],
            //Style for the form field
            decoration: InputDecoration(
              //Decoration for the form field starts here
              hintText: widget.labelText,
              //Hint text for the form field
              hintStyle: TextStyles.lowerSubTitleHintText,
              //Hint style for the form field
              errorMaxLines: 2,
              //Maximum lines for the error text
              labelStyle: TextStyles.lowerSubTitle,
              //Label style for the form field
              suffixIcon: IconButton(
                //Suffix icon for the form field
                icon: Icon(
                  indicator
                      ? Icons
                          .visibility_off_outlined //If the indicator is true, then the icon is the eye closed icon
                      : Icons.visibility_outlined,
                  //If the indicator is false, then the icon is the eye open icon
                  color: AppColors.grey625F5A,
                ),
                onPressed: () {
                  setState(() {
                    indicator = !indicator; //Toggle the indicator
                  });
                },
              ),
              //labelText: widget.labelText,
              enabledBorder: const OutlineInputBorder(
                //Enabled border for the form field
                borderSide: BorderSide(
                    color: AppColors.formFieldBorderColor, width: 2.0),
                borderRadius: BorderRadius.all(
                  //Border radius for the form field
                  Radius.circular(10.0),
                ),
              ),
              focusedBorder: const OutlineInputBorder(
                //Focused border for the form field
                borderSide: BorderSide(
                    color: AppColors.formFieldBorderColor, width: 2.0),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              border: const OutlineInputBorder(
                //Border for the form field
                borderSide: BorderSide(
                    color: AppColors.formFieldBorderColor, width: 2.0),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              filled: true,
              //Filled for the form field
              fillColor: AppColors.white, //Fill color for the form field
            ),
          );
        } //Password form field
      case TypeRequireTextFieldV2.passWordWithClear:
        {
          return TextFormField(
            controller: _thisController,
            //Controller for the form field
            keyboardType: TextInputType.text,
            //Keyboard TypeRequireTextFieldV2 for the form field
            autofocus: false,
            focusNode: _focus,
            //Autofocus for the form field
            maxLines: 1,
            //Maximum lines for the form field
            autovalidateMode: AutovalidateMode.onUserInteraction,
            //Autovalidate mode for the form field
            validator: validatePassword,
            //Validator for the form field
            obscureText: indicator,
            //Obscure text for the form field
            style: TextStyles.lowerSubTitle,
            //inputFormatters: [ FilteringTextInputFormatter.allow(RegExp("[a-zA-Z]")), ],
            //Style for the form field
            decoration: InputDecoration(
              //Decoration for the form field starts here
              hintText: widget.labelText,
              //Hint text for the form field
              hintStyle: TextStyles.lowerSubTitleHintText,
              //Hint style for the form field
              errorMaxLines: 2,
              //Maximum lines for the error text
              labelStyle: TextStyles.lowerSubTitle,
              //Label style for the form field
              suffixIcon: SizedBox(
                width: 100,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    widget.controller.text.isNotEmpty && _focus.hasFocus
                        ? IconButton(
                            icon:
                                const Icon(Icons.clear, color: Colors.black54),
                            onPressed: () {
                              if (mounted) {
                                setState(() {
                                  widget.controller.clear();
                                });
                              }
                            },
                          )
                        : const SizedBox(),
                    IconButton(
                      //Suffix icon for the form field
                      icon: Icon(
                        indicator
                            ? Icons
                                .visibility_off_outlined //If the indicator is true, then the icon is the eye closed icon
                            : Icons.visibility_outlined,
                        //If the indicator is false, then the icon is the eye open icon
                        color: AppColors.grey625F5A,
                      ),
                      onPressed: () {
                        setState(() {
                          indicator = !indicator; //Toggle the indicator
                        });
                      },
                    ),
                  ],
                ),
              ),
              //labelText: widget.labelText,
              enabledBorder: const OutlineInputBorder(
                //Enabled border for the form field
                borderSide: BorderSide(
                    color: AppColors.formFieldBorderColor, width: 2.0),
                borderRadius: BorderRadius.all(
                  //Border radius for the form field
                  Radius.circular(10.0),
                ),
              ),
              focusedBorder: const OutlineInputBorder(
                //Focused border for the form field
                borderSide: BorderSide(
                    color: AppColors.formFieldBorderColor, width: 2.0),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              border: const OutlineInputBorder(
                //Border for the form field
                borderSide: BorderSide(
                    color: AppColors.formFieldBorderColor, width: 2.0),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              filled: true,
              //Filled for the form field
              fillColor: AppColors.white, //Fill color for the form field
            ),
          );
        }

      //email or phone form field
      case TypeRequireTextFieldV2.emailOrPhone:
        {
          return TextFormField(
            controller: _thisController,
            keyboardType: TextInputType.text,
            autofocus: false,
            maxLines: 1,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            style: TextStyles.lowerSubTitle,
            validator: validateEmailAndMobile,
            decoration: InputDecoration(
              //errorText: _value != null ? validateEmailAndMobile(_value) : null,
              /*suffixIcon: Icon(
                Icons.done_rounded,
                color: AppColors.iconColor,
              ),*/
              hintText: widget.labelText,
              hintStyle: TextStyles.lowerSubTitleHintText,

              labelStyle: TextStyles.lowerSubTitle,
              // contentPadding: ScreenConstant.spacingAllLarge,
              enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              border: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              filled: true,
              fillColor: AppColors.white,
            ),
          );
        }

      //name form field
      case TypeRequireTextFieldV2.firstName:
        {
          return TextFormField(
            controller: _thisController,
            keyboardType: TextInputType.text,
            autofocus: false,
            maxLines: 1,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            style: TextStyles.lowerSubTitle,
            validator: validateName,
            decoration: InputDecoration(
              //errorText: _value != null ? validateEmailAndMobile(_value) : null,
              labelStyle: TextStyles.lowerSubTitle,
              // contentPadding: ScreenConstant.spacingAllLarge,
              hintText: widget.labelText,
              hintStyle: TextStyles.lowerSubTitleHintText,

              enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              border: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              filled: true,
              fillColor: AppColors.white,
            ),
          );
        }

      case TypeRequireTextFieldV2.dob:
        {
          return GestureDetector(
            onTap: () {
              debugPrint("calender");
              _showDOBPicker(Get.context, TypeRequireTextFieldV2.dob);
            },
            child: Container(
                height: 52,
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
                decoration: BoxDecoration(
                    border: Border.all(
                        color: AppColors.formFieldBorderColor, width: 1.0),
                    borderRadius: const BorderRadius.all(Radius.circular(
                            10) //                 <--- border radius here
                        )),
                child: Row(
                  children: [
                    Text(_thisController.text),
                    const Spacer(),
                    const Icon(
                      Icons.calendar_today_outlined,
                      color: AppColors.grey625F5A,
                    ),
                  ],
                )
                /*TextFormField(
                controller: _thisController,
                keyboardType: TextInputType.text,
                autofocus: false,
                maxLines: 1,
                readOnly: true,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                style: TextStyles.lowerSubTitle,
                validator: validateDOB,
                enabled: true,
                decoration: InputDecoration(
                  //errorText: _value != null ? validateEmailAndMobile(_value) : null,
                  labelStyle: TextStyles.lowerSubTitle,
                  contentPadding: ScreenConstant.spacingAllLarge,
                  hintText: widget.labelText,
                  hintStyle: TextStyles.lowerSubTitleHintText,
                  suffixIcon: Icon(
                      Icons.calendar_today_outlined,
                      color: AppColors.grey625F5A,
                  ),
                  enabledBorder: const OutlineInputBorder(
                    borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                    borderRadius: BorderRadius.all(
                      Radius.circular(10.0),
                    ),
                  ),
                  focusedBorder: const OutlineInputBorder(
                    borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                    borderRadius: BorderRadius.all(
                      Radius.circular(10.0),
                    ),
                  ),
                  border: const OutlineInputBorder(
                    borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                    borderRadius: BorderRadius.all(
                      Radius.circular(10.0),
                    ),
                  ),
                  filled: true,
                  fillColor: AppColors.white,
                ),
              ),*/
                ),
          );
        }

      case TypeRequireTextFieldV2.date:
        {
          return TextFormField(
            controller: _thisController,
            keyboardType: TextInputType.text,
            autofocus: false,
            maxLines: 1,
            readOnly: true,
            // autovalidateMode: AutovalidateMode.onUserInteraction,
            style: TextStyles.lowerSubTitle,
            // validator: validateDate,
            enabled: true,
            decoration: InputDecoration(
              //errorText: _value != null ? validateEmailAndMobile(_value) : null,
              labelStyle: TextStyles.lowerSubTitle,
              // contentPadding: ScreenConstant.spacingAllLarge,
              hintText: widget.labelText,
              hintStyle: TextStyles.lowerSubTitleHintText,
              suffixIcon: IconButton(
                //Suffix icon for the form field
                icon: const Icon(
                  Icons.calendar_today_outlined,
                  color: AppColors.grey625F5A,
                ),
                onPressed: () {
                  debugPrint("calender");
                  _showDatePicker(Get.context);
                },
              ),
              enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              border: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              filled: true,
              fillColor: AppColors.white,
            ),
          );
        }
      case TypeRequireTextFieldV2.dropDownCountry:
        return Container(
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
          decoration: BoxDecoration(
              border:
                  Border.all(color: AppColors.formFieldBorderColor, width: 1.0),
              borderRadius: const BorderRadius.all(
                  Radius.circular(10) //                 <--- border radius here
                  )),
          child: DropdownButton<String>(
            isExpanded: true,
            value: dropdownValueCountry,
            icon: const Icon(Icons.keyboard_arrow_down_sharp),
            elevation: 16,
            //style: const TextStyle(color: Colors.deepPurple),
            underline: Container(
              height: 0,
              // color: Colors.deepPurpleAccent,
            ),
            onChanged: (String? newValue) {
              setState(() {
                dropdownValueCountry = newValue!;
              });
            },
            items: StringUtils.countriesList
                .map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(
                  value,
                  style: TextStyles.lowerSubTitle,
                ),
              );
            }).toList(),
          ),
        );

      case TypeRequireTextFieldV2.dropdownForLeadCountry:
        if (widget.dropdownForLeadCountryDefaultValue.isEmpty) {
          widget.dropdownForLeadCountryDefaultValue =
              dropdownForLeadCountryDefaultValue;
        }

        widget.controller.text = widget.dropdownForLeadCountryDefaultValue;

        return Container(
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
          decoration: BoxDecoration(
              border:
                  Border.all(color: AppColors.formFieldBorderColor, width: 1.0),
              borderRadius: const BorderRadius.all(
                  Radius.circular(10) //                 <--- border radius here
                  )),
          child: DropdownButton<String>(
            isExpanded: true,
            value: widget.dropdownForLeadCountryDefaultValue,
            icon: const Icon(Icons.keyboard_arrow_down_sharp),
            elevation: 16,
            //style: const TextStyle(color: Colors.deepPurple),
            underline: Container(
              height: 0,
              // color: Colors.deepPurpleAccent,
            ),
            onChanged: (String? newValue) {
              setState(() {
                widget.dropdownForLeadCountryDefaultValue = newValue!;
                widget.controller.text = newValue;
              });
            },
            items: <String>[
              "Singapore Citizens",
              "Singapore Permanent Residents",
              "Iceland Citizens / Permanent Residents",
              "Liechtenstein Citizens / Permanent Residents",
              "Norway Citizens / Permanent Residents",
              "Switzerland Citizens / Permanent Residents",
              "USA Citizens",
              "Others"
            ]
                //items: <String>['Singapore Permanent Residents', 'Indian Permanent Residents']
                .map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(
                  value,
                  style: TextStyles.lowerSubTitle,
                ),
              );
            }).toList(),
          ),
        );

      case TypeRequireTextFieldV2.dropdownCountryCodeWithPhone:
        if (widget.countryCodeLabelController == null ||
            widget.countryCodeLabelController!.text.isEmpty) {
          widget.countryCodeLabelController?.text =
              AppConfig.defaultCountryLabel;
        }

        if (widget.countryCodeController == null ||
            widget.countryCodeController!.text.isEmpty) {
          widget.countryCodeController?.text = AppConfig.defaultCountryCode;
        }

        return SizedBox(
          height: 75,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                flex: 1,
                child: Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
                  margin: const EdgeInsets.only(bottom: 20),
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: AppColors.formFieldBorderColor, width: 1.0),
                      borderRadius: const BorderRadius.all(Radius.circular(
                              10) //                 <--- border radius here
                          )),
                  child: SizedBox(
                    height: 45,
                    child: CountryCodePicker(
                      padding: EdgeInsets.zero,
                      textStyle: TextStyles.lowerSubTitle,
                      showFlag: false,
                      showFlagDialog: true,
                      alignLeft: true,
                      onChanged: (code) {
                        debugPrint(">>>>>>>>>>" + code.dialCode.toString());
                        debugPrint(">>>>>>>>>>" + code.code.toString());
                        setState(() {
                          widget.countryCodeController?.text =
                              code.dialCode.toString();
                          if (widget.countryCodeLabelController != null) {
                            widget.countryCodeLabelController?.text =
                                code.code.toString();
                          }
                        });
                      },
                      // Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
                      initialSelection: widget.countryCodeController?.text,
                    ),
                  ),
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(bottom: 20.0, right: 5, left: 5),
                child: Text("-", style: TextStyle(fontSize: 25)),
              ),
              Expanded(
                flex: 3,
                child: TextFormField(
                  controller: _thisController,
                  keyboardType: TextInputType.number,
                  autofocus: false,
                  readOnly: widget.readOnly,
                  maxLines: 1,
                  maxLength: 12,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  style: TextStyles.lowerSubTitle,
                  validator: validateMobile,
                  decoration: InputDecoration(
                    //errorText: _value != null ? validateEmailAndMobile(_value) : null,
                    labelStyle: TextStyles.lowerSubTitle,
                    // contentPadding: ScreenConstant.spacingAllLarge,
                    hintText: widget.labelText,
                    hintStyle: TextStyles.lowerSubTitleHintText,
                    enabledBorder: const OutlineInputBorder(
                      borderSide:
                          BorderSide(color: AppColors.formFieldBorderColor),
                      borderRadius: BorderRadius.all(
                        Radius.circular(10.0),
                      ),
                    ),
                    focusedBorder: const OutlineInputBorder(
                      borderSide:
                          BorderSide(color: AppColors.formFieldBorderColor),
                      borderRadius: BorderRadius.all(
                        Radius.circular(10.0),
                      ),
                    ),
                    border: const OutlineInputBorder(
                      borderSide:
                          BorderSide(color: AppColors.formFieldBorderColor),
                      borderRadius: BorderRadius.all(
                        Radius.circular(10.0),
                      ),
                    ),
                    filled: true,
                    fillColor: AppColors.white,
                  ),
                ),
              ),
            ],
          ),
        );

      case TypeRequireTextFieldV2.dropdownCountryCodeWithPhoneClear:
        if (widget.countryCodeLabelController == null ||
            widget.countryCodeLabelController!.text.isEmpty) {
          widget.countryCodeLabelController?.text =
              AppConfig.defaultCountryLabel;
        }

        if (widget.countryCodeController == null ||
            widget.countryCodeController!.text.isEmpty) {
          widget.countryCodeController?.text = AppConfig.defaultCountryCode;
        }

        return SizedBox(
          height: 75,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                flex: 1,
                child: Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
                  margin: const EdgeInsets.only(bottom: 20),
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: AppColors.formFieldBorderColor, width: 1.0),
                      borderRadius: const BorderRadius.all(Radius.circular(
                              10) //                 <--- border radius here
                          )),
                  child: SizedBox(
                    height: 45,
                    child: CountryCodePicker(
                      padding: EdgeInsets.zero,
                      textStyle: TextStyles.lowerSubTitle,
                      showFlag: false,
                      showFlagDialog: true,
                      alignLeft: true,
                      onChanged: (code) {
                        debugPrint(">>>>>>>>>>" + code.dialCode.toString());
                        debugPrint(">>>>>>>>>>" + code.code.toString());
                        setState(() {
                          widget.countryCodeController?.text =
                              code.dialCode.toString();
                          if (widget.countryCodeLabelController != null) {
                            widget.countryCodeLabelController?.text =
                                code.code.toString();
                          }
                        });
                      },
                      // Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
                      initialSelection: widget.countryCodeController?.text,
                    ),
                  ),
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(bottom: 20.0, right: 5, left: 5),
                child: Text("-", style: TextStyle(fontSize: 25)),
              ),
              Expanded(
                flex: 3,
                child: TextFormField(
                  controller: _thisController,
                  keyboardType: TextInputType.number,
                  autofocus: false,
                  focusNode: _focus,
                  readOnly: widget.readOnly,
                  maxLines: 1,
                  maxLength: 12,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  style: TextStyles.lowerSubTitle,
                  validator: validateMobile,
                  decoration: InputDecoration(
                    suffixIcon: (widget.controller.text.isNotEmpty &&
                            _focus.hasFocus)
                        ? IconButton(
                            icon:
                                const Icon(Icons.clear, color: Colors.black54),
                            onPressed: () {
                              if (mounted) {
                                setState(() {
                                  widget.controller.clear();
                                });
                              }
                            },
                          )
                        : null,
                    //errorText: _value != null ? validateEmailAndMobile(_value) : null,
                    labelStyle: TextStyles.lowerSubTitle,
                    // contentPadding: ScreenConstant.spacingAllLarge,
                    hintText: widget.labelText,
                    hintStyle: TextStyles.lowerSubTitleHintText,
                    enabledBorder: const OutlineInputBorder(
                      borderSide:
                          BorderSide(color: AppColors.formFieldBorderColor),
                      borderRadius: BorderRadius.all(
                        Radius.circular(10.0),
                      ),
                    ),
                    focusedBorder: const OutlineInputBorder(
                      borderSide:
                          BorderSide(color: AppColors.formFieldBorderColor),
                      borderRadius: BorderRadius.all(
                        Radius.circular(10.0),
                      ),
                    ),
                    border: const OutlineInputBorder(
                      borderSide:
                          BorderSide(color: AppColors.formFieldBorderColor),
                      borderRadius: BorderRadius.all(
                        Radius.circular(10.0),
                      ),
                    ),
                    filled: true,
                    fillColor: AppColors.white,
                  ),
                ),
              ),
            ],
          ),
        );

      case TypeRequireTextFieldV2.fullname:
        {
          return TextFormField(
            controller: _thisController,
            keyboardType: TextInputType.text,
            autofocus: false,
            maxLines: 1,
            readOnly: widget.readOnly,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            style: TextStyles.lowerSubTitle,
            // validator: validateName,
            decoration: InputDecoration(
              //errorText: _value != null ? validateEmailAndMobile(_value) : null,
              labelStyle: TextStyles.lowerSubTitle,
              // contentPadding: ScreenConstant.spacingAllLarge,
              hintText: widget.labelText,
              hintStyle: TextStyles.lowerSubTitleHintText,
              enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              border: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              filled: true,
              fillColor: AppColors.white,
            ),
          );
        }

      case TypeRequireTextFieldV2.fullnameWithClear:
        {
          return TextFormField(
            controller: _thisController,
            keyboardType: TextInputType.text,
            autofocus: false,
            focusNode: _focus,
            maxLines: 1,
            readOnly: widget.readOnly,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            style: TextStyles.lowerSubTitle,
            validator: validateName,
            decoration: InputDecoration(
              suffixIcon: (widget.controller.text.isNotEmpty && _focus.hasFocus)
                  ? IconButton(
                      icon: const Icon(Icons.clear, color: Colors.black54),
                      onPressed: () {
                        if (mounted) {
                          setState(() {
                            widget.controller.clear();
                          });
                        }
                      },
                    )
                  : null,
              //errorText: _value != null ? validateEmailAndMobile(_value) : null,
              labelStyle: TextStyles.lowerSubTitle,
              // contentPadding: ScreenConstant.spacingAllLarge,
              hintText: widget.labelText,
              hintStyle: TextStyles.lowerSubTitleHintText,
              enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              border: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              filled: true,
              fillColor: AppColors.white,
            ),
          );
        }

      case TypeRequireTextFieldV2.text:
        {
          return TextFormField(
            controller: _thisController,
            keyboardType: TextInputType.text,
            autofocus: false,
            maxLines: 1,
            maxLength: widget.maxLength,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            style: TextStyles.lowerSubTitle,
            validator: validateText,
            decoration: InputDecoration(
              //errorText: _value != null ? validateEmailAndMobile(_value) : null,
              labelStyle: TextStyles.lowerSubTitle,
              // contentPadding: ScreenConstant.spacingAllLarge,
              hintText: widget.labelText,
              hintStyle: TextStyles.lowerSubTitleHintText,
              enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              border: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              filled: true,
              fillColor: AppColors.white,
            ),
          );
        }

      case TypeRequireTextFieldV2.pdpa:
        {
          return TextFormField(
            controller: _thisController,
            keyboardType: TextInputType.text,
            autofocus: false,
            maxLines: 1,
            maxLength: widget.maxLength,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            style: TextStyles.lowerSubTitle,
            //validator: validateText,
            decoration: InputDecoration(
              //errorText: _value != null ? validateEmailAndMobile(_value) : null,
              labelStyle: TextStyles.lowerSubTitle,
              // contentPadding: ScreenConstant.spacingAllLarge,
              hintText: widget.labelText,
              hintStyle: TextStyles.lowerSubTitleHintText,
              enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              border: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              filled: true,
              fillColor: AppColors.white,
            ),
          );
        }

      case TypeRequireTextFieldV2.customText:
        {
          return TextFormField(
            controller: _thisController,
            keyboardType: TextInputType.text,
            autofocus: false,
            maxLines: 1,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            style: TextStyles.lowerSubTitle,
            validator: validateLDefault,
            decoration: InputDecoration(
              //errorText: _value != null ? validateEmailAndMobile(_value) : null,
              labelStyle: TextStyles.lowerSubTitle,
              // contentPadding: ScreenConstant.spacingAllLarge,
              hintText: widget.labelText,
              hintStyle: TextStyles.lowerSubTitleHintText,
              enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              border: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              filled: true,
              fillColor: AppColors.white,
            ),
          );
        }

      case TypeRequireTextFieldV2.optional:
        {
          return TextFormField(
            controller: _thisController,
            keyboardType: TextInputType.text,
            autofocus: false,
            maxLines: 1,
            //autovalidateMode: AutovalidateMode.onUserInteraction,
            style: TextStyles.lowerSubTitle,
            //validator: validateLDefault,
            decoration: InputDecoration(
              //errorText: _value != null ? validateEmailAndMobile(_value) : null,
              labelStyle: TextStyles.lowerSubTitle,
              // contentPadding: ScreenConstant.spacingAllLarge,
              hintText: widget.labelText,
              hintStyle: TextStyles.lowerSubTitleHintText,
              enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              border: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              filled: true,
              fillColor: AppColors.white,
            ),
            onChanged: widget.onChanged,
          );
        }

      case TypeRequireTextFieldV2.optionalWithClear:
        {
          return TextFormField(
            controller: _thisController,
            keyboardType: TextInputType.text,
            autofocus: false,
            focusNode: _focus,
            maxLines: 1,
            //autovalidateMode: AutovalidateMode.onUserInteraction,
            style: TextStyles.lowerSubTitle,
            //validator: validateLDefault,
            decoration: InputDecoration(
              suffixIcon: (widget.controller.text.isNotEmpty && _focus.hasFocus)
                  ? IconButton(
                      icon: const Icon(Icons.clear, color: Colors.black54),
                      onPressed: () {
                        if (mounted) {
                          setState(() {
                            widget.controller.clear();
                          });
                        }
                      },
                    )
                  : null,
              //errorText: _value != null ? validateEmailAndMobile(_value) : null,
              labelStyle: TextStyles.lowerSubTitle,
              // contentPadding: ScreenConstant.spacingAllLarge,
              hintText: widget.labelText,
              hintStyle: TextStyles.lowerSubTitleHintText,
              enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              border: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              filled: true,
              fillColor: AppColors.white,
            ),
            onChanged: widget.onChanged,
          );
        }
      case TypeRequireTextFieldV2.optionalTextArea:
        {
          return TextFormField(
            controller: _thisController,
            keyboardType: TextInputType.multiline,
            autofocus: false,
            maxLines: 5,
            //autovalidateMode: AutovalidateMode.onUserInteraction,
            style: TextStyles.lowerSubTitle,
            //validator: validateLDefault,
            decoration: InputDecoration(
              //errorText: _value != null ? validateEmailAndMobile(_value) : null,
              labelStyle: TextStyles.lowerSubTitle,
              // contentPadding: ScreenConstant.spacingAllLarge,
              hintText: widget.labelText,
              hintStyle: TextStyles.lowerSubTitleHintText,
              enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              border: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              filled: true,
              fillColor: AppColors.white,
            ),
          );
        }

      case TypeRequireTextFieldV2.textArea:
        {
          return TextFormField(
            controller: _thisController,
            keyboardType: TextInputType.multiline,
            autofocus: false,
            minLines: 6,
            maxLines: null,
            // autovalidateMode: AutovalidateMode.onUserInteraction,
            style: TextStyles.lowerSubTitle,
            validator: validateLDefault,
            decoration: InputDecoration(
              //errorText: _value != null ? validateEmailAndMobile(_value) : null,
              labelStyle: TextStyles.lowerSubTitle,
              // contentPadding: ScreenConstant.spacingAllLarge,
              hintText: widget.labelText,
              hintStyle: TextStyles.lowerSubTitleHintText,
              enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              border: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              filled: true,
              fillColor: AppColors.white,
            ),
          );
        }

      //last name form field
      case TypeRequireTextFieldV2.lastName:
        {
          return TextFormField(
            controller: _thisController,
            keyboardType: TextInputType.text,
            autofocus: false,
            maxLines: 1,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            style: TextStyles.lowerSubTitle,
            validator: validateLName,
            decoration: InputDecoration(
              //errorText: _value != null ? validateEmailAndMobile(_value) : null,
              labelStyle: TextStyles.lowerSubTitle,
              //labelText: widget.labelText,
              // contentPadding: ScreenConstant.spacingAllLarge,
              hintText: widget.labelText,
              hintStyle: TextStyles.lowerSubTitleHintText,

              enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              border: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              filled: true,
              fillColor: AppColors.white,
            ),
          );
        }

      //email form field
      case TypeRequireTextFieldV2.email:
        {
          return TextFormField(
            controller: _thisController,
            keyboardType: TextInputType.text,
            autofocus: false,
            maxLines: 1,
            readOnly: widget.readOnly,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            style: TextStyles.lowerSubTitle,
            validator: validateEmail,
            decoration: InputDecoration(
              //errorText: _value != null ? validateEmailAndMobile(_value) : null,
              labelStyle: TextStyles.lowerSubTitle,
              // contentPadding: ScreenConstant.spacingAllLarge,
              hintText: widget.labelText,
              hintStyle: TextStyles.lowerSubTitleHintText,
              enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(
                  color: AppColors.formFieldBorderColor,
                ),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(
                  color: AppColors.formFieldBorderColor,
                ),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              border: const OutlineInputBorder(
                borderSide: BorderSide(
                  color: AppColors.formFieldBorderColor,
                ),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              filled: true,
              fillColor: AppColors.white,
            ),
          );
        }
//email form field
      case TypeRequireTextFieldV2.emailWithClear:
        {
          return TextFormField(
            controller: _thisController,
            keyboardType: TextInputType.text,
            autofocus: false,
            focusNode: _focus,
            maxLines: 1,
            readOnly: widget.readOnly,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            style: TextStyles.lowerSubTitle,
            validator: validateEmail,
            decoration: InputDecoration(
              suffixIcon: (widget.controller.text.isNotEmpty && _focus.hasFocus)
                  ? IconButton(
                      icon: const Icon(Icons.clear, color: Colors.black54),
                      onPressed: () {
                        if (mounted) {
                          setState(() {
                            widget.controller.clear();
                          });
                        }
                      },
                    )
                  : null,
              //errorText: _value != null ? validateEmailAndMobile(_value) : null,
              labelStyle: TextStyles.lowerSubTitle,
              // contentPadding: ScreenConstant.spacingAllLarge,
              hintText: widget.labelText,
              hintStyle: TextStyles.lowerSubTitleHintText,
              enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(
                  color: AppColors.formFieldBorderColor,
                ),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(
                  color: AppColors.formFieldBorderColor,
                ),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              border: const OutlineInputBorder(
                borderSide: BorderSide(
                  color: AppColors.formFieldBorderColor,
                ),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              filled: true,
              fillColor: AppColors.white,
            ),
          );
        }

      //phone form field
      case TypeRequireTextFieldV2.phone:
        {
          return TextFormField(
            controller: _thisController,
            keyboardType: TextInputType.text,
            autofocus: false,
            maxLines: 1,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            style: TextStyles.lowerSubTitle,
            validator: validateMobile,
            decoration: InputDecoration(
              //errorText: _value != null ? validateEmailAndMobile(_value) : null,
              labelStyle: TextStyles.lowerSubTitle,
              // contentPadding: ScreenConstant.spacingAllLarge,
              hintText: widget.labelText,
              hintStyle: TextStyles.lowerSubTitleHintText,

              enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              border: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              filled: true,
              fillColor: AppColors.white,
            ),
          );
        }

      //otp boxes
      case TypeRequireTextFieldV2.otpV2:
        {
          return OTPTextField(
              controller: widget.otpFieldController,
              otpFieldStyle: OtpFieldStyle(focusBorderColor: Colors.deepOrange),
              length: 6,
              width: MediaQuery.of(context).size.width,
              textFieldAlignment: MainAxisAlignment.spaceAround,
              fieldWidth: 50,
              fieldStyle: FieldStyle.box,
              outlineBorderRadius: 5,
              style: const TextStyle(fontSize: 17),
              onChanged: (pin) {
                debugPrint("Changed: " + pin);
                _thisController.text = pin;
              },
              onCompleted: (pin) {
                debugPrint("Completed: " + pin);
              });
        }

      case TypeRequireTextFieldV2.otp:
        {
          return BoxEntryTextField(
            onSubmit: (String pin) {
              setState(() {
                _thisController.text = pin;
              });
            }, // end onSubmit
          );
        }

      case TypeRequireTextFieldV2.otpPopUpMenu:
        {
          return BoxEntryTextFieldPopUpMenu(
            onSubmit: (String pin) {
              setState(() {
                _thisController.text = pin;
              });
            }, // end onSubmit
          );
        }

      // case TypeRequireTextFieldV2.authenticatorOtp:
      //   {
      //     return AuthenticatorOtpTextField(
      //       onSubmitOtp: (String pin) {
      //         setState(() {
      //           _thisController.text = pin;
      //         });
      //       }, // end onSubmit
      //     );
      //   }
      case TypeRequireTextFieldV2.search:
        {
          return TextFormField(
            controller: _thisController,
            keyboardType: TextInputType.text,
            autofocus: false,
            maxLines: 1,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            style: TextStyle(fontSize: FontSize.s12, color: Colors.black),
            //validator: validateEmailAndMobile,
            decoration: InputDecoration(
              focusedBorder: const OutlineInputBorder(
                gapPadding: 20,
                borderRadius: BorderRadius.all(Radius.circular(10)),
                borderSide: BorderSide(width: 2, color: Color(0xFFCDD5D4)),
              ),
              disabledBorder: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                borderSide: BorderSide(width: 2, color: Color(0xFFCDD5D4)),
              ),
              enabledBorder: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                borderSide: BorderSide(width: 2, color: Color(0xFFCDD5D4)),
              ),
              border: const OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  borderSide: BorderSide(
                    width: 1,
                  )),
              errorBorder: const OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  borderSide: BorderSide(width: 1, color: Colors.redAccent)),
              focusedErrorBorder: const OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  borderSide: BorderSide(width: 1, color: Colors.redAccent)),
              //errorText: _value != null ? validateEmailAndMobile(_value) : null,
              suffixIcon: InkWell(
                onTap: widget.onSubmitSearch,
                child: const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Icon(
                    Icons.search,
                    color: Color(0xFF262A2B),
                  ),
                ),
              ),
              hintStyle: TextStyle(
                  fontSize: FontSize.s12, color: const Color(0xFFA8A8A8)),
              // contentPadding: ScreenConstant.spacingAllSmall,
              hintText: widget.labelText,
              filled: true,
              // fillColor: const Color(0xFFF9F9F9),
              fillColor: Colors.white,
            ),
            onFieldSubmitted: widget.onSubmit,
          );
        }

      case TypeRequireTextFieldV2.searchLeft:
        {
          return TextFormField(
            controller: _thisController,
            keyboardType: TextInputType.text,
            autofocus: true,
            maxLines: 1,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            style: TextStyle(fontSize: FontSize.s12, color: Colors.black),
            //validator: validateEmailAndMobile,
            decoration: InputDecoration(
              focusedBorder: const OutlineInputBorder(
                gapPadding: 20,
                borderRadius: BorderRadius.all(Radius.circular(10)),
                borderSide: BorderSide(width: 2, color: Color(0xFFCDD5D4)),
              ),
              disabledBorder: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                borderSide: BorderSide(width: 2, color: Color(0xFFCDD5D4)),
              ),
              enabledBorder: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                borderSide: BorderSide(width: 2, color: Color(0xFFCDD5D4)),
              ),
              border: const OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  borderSide: BorderSide(
                    width: 1,
                  )),
              errorBorder: const OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  borderSide: BorderSide(width: 1, color: Colors.redAccent)),
              focusedErrorBorder: const OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  borderSide: BorderSide(width: 1, color: Colors.redAccent)),
              //errorText: _value != null ? validateEmailAndMobile(_value) : null,
              suffixIcon: const Icon(
                Icons.keyboard_arrow_down_outlined,
                color: Colors.black45,
              ),
              prefixIcon: const Icon(
                Icons.search,
                color: Colors.black45,
              ),
              hintStyle: TextStyle(
                  fontSize: FontSize.s12, color: const Color(0xFFA8A8A8)),
              // contentPadding: ScreenConstant.spacingAllSmall,
              hintText: widget.labelText,
              filled: true,
              // fillColor: const Color(0xFFF9F9F9),
              fillColor: Colors.white,
            ),
            onFieldSubmitted: widget.onSubmit,
          );
        }
      case TypeRequireTextFieldV2.expiry:
        {
          return TextFormField(
            controller: _thisController,
            //Controller for the form field
            keyboardType: TextInputType.number,
            textInputAction: widget.textInputAction,
            //Keyboard TypeRequireTextFieldV2 for the form field
            autofocus: false,
            //Autofocus for the form field
            maxLines: 1,
            //Maximum lines for the form field
            autovalidateMode: AutovalidateMode.onUserInteraction,
            //Autovalidate mode for the form field
            validator: validateExpiry,
            //Validator for the form field
            obscureText: false,
            //Obscure text for the form field
            style: TextStyles.lowerSubTitle,
            //Style for the form field
            maxLength: widget.maxLength,
            onChanged: widget.onChanged,
            decoration: InputDecoration(
              errorBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
              ),
              focusedErrorBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
              ),
              errorStyle: const TextStyle(
                height: 0,
              ),
              counter: Container(),
              //Decoration for the form field starts here
              hintText: widget.labelText,
              //Hint text for the form field
              hintStyle: TextStyles.lowerSubTitleHintText,
              //Hint style for the form field
              errorMaxLines: 2,
              //Maximum lines for the error text
              labelStyle: TextStyles.lowerSubTitle,
              //Label style for the form field
              // suffixIcon: IconButton(
              //   //Suffix icon for the form field
              //   icon: Icon(
              //     indicator
              //         ? Icons
              //             .visibility_off_outlined //If the indicator is true, then the icon is the eye closed icon
              //         : Icons
              //             .visibility_outlined, //If the indicator is false, then the icon is the eye open icon
              //     color: AppColors.grey625F5A,
              //   ),
              //   onPressed: () {
              //     setState(() {
              //       indicator = !indicator; //Toggle the indicator
              //     });
              //   },
              // ),
              //labelText: widget.labelText,
              enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              border: const OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.formFieldBorderColor),
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              filled: true,
              //Filled for the form field
              fillColor: AppColors.white, //Fill color for the form field
            ),
          );
        }
      default:
        {
          return TextFormField(
            controller: _thisController,
            keyboardType: TextInputType.text,
            autofocus: false,
            maxLines: 1,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            style: TextStyles.subTitle,
            //validator: validateEmailAndMobile,
            decoration: InputDecoration(
              //errorText: _value != null ? validateEmailAndMobile(_value) : null,
              labelStyle: TextStyles.lowerSubTitle,
              // contentPadding: ScreenConstant.spacingAllLarge,
              labelText: widget.labelText,
              border: const OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              filled: true,
              fillColor: AppColors.borderColor,
            ),
          );
        }
    }
  }

  // Show the modal that contains the CupertinoDatePicker
  void _showDatePicker(context) {
    // showCupertinoModalPopup is a built-in function of the cupertino library

    var initDate = DateTime.now();
    var selectedDate = initDate;
    showCupertinoModalPopup(
        //barrierDismissible: false,
        context: context,
        builder: (_) => Container(
              height: 300,
              color: const Color.fromARGB(255, 255, 255, 255),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  GestureDetector(
                    onTap: () {
                      Get.back();
                      _thisController.text =
                          DateFormat(widget.dateFormat).format(selectedDate);
                      setState(() {});
                    },
                    child: const Padding(
                      padding: EdgeInsets.only(top: 15.0, right: 15.0),
                      child: Icon(
                        Icons.done,
                        size: 30,
                      ),
                    ),
                  ),
                  const Spacer(),
                  SizedBox(
                    height: 254,
                    child: CupertinoDatePicker(
                        mode: CupertinoDatePickerMode.date,
                        initialDateTime: initDate,
                        //maximumDate: initDate,
                        onDateTimeChanged: (date) {
                          selectedDate = date;

                          _thisController.text =
                              DateFormat(widget.dateFormat).format(date);

                          setState(() {});
                        }),
                  ),
                ],
              ),
            ));
  }

  // Show the modal that contains the CupertinoDatePicker
  void _showDOBPicker(context, TypeRequireTextFieldV2) {
    // showCupertinoModalPopup is a built-in function of the cupertino library

    var initDate = DateTime.now().subtract(const Duration(days: 7665));
    var maxYear = initDate.year;
    var selectedDate = initDate;
    showCupertinoModalPopup(
        //barrierDismissible: false,
        context: context,
        builder: (_) => Container(
              height: 300,
              color: const Color.fromARGB(255, 255, 255, 255),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  GestureDetector(
                    onTap: () {
                      Get.back();
                      if (TypeRequireTextFieldV2 ==
                          TypeRequireTextFieldV2.dob) {
                        var dateNow = DateTime.now();
                        var diff = dateNow.difference(selectedDate);
                        var year = ((diff.inDays) / 365).round();

                        //if (year > 20) {
                        _thisController.text =
                            DateFormat(widget.dateFormat).format(selectedDate);
                        setState(() {});
                        /*} else {
                          Get.snackbar("Invalid DOB",
                              "You must be 21 year old and above to access iqrate services. ",
                              backgroundColor: Colors.red,
                              colorText: Colors.white);
                        }*/
                      } else {
                        _thisController.text =
                            DateFormat(widget.dateFormat).format(selectedDate);
                        setState(() {});
                      }
                    },
                    child: const Padding(
                      padding: EdgeInsets.only(top: 15.0, right: 15.0),
                      child: Icon(
                        Icons.done,
                        size: 30,
                      ),
                    ),
                  ),
                  const Spacer(),
                  SizedBox(
                    height: 254,
                    child: CupertinoDatePicker(
                        mode: CupertinoDatePickerMode.date,
                        initialDateTime: initDate,
                        maximumDate: initDate,
                        onDateTimeChanged: (date) {
                          selectedDate = date;
                          if (TypeRequireTextFieldV2 ==
                              TypeRequireTextFieldV2.date) {
                            // setState(() {
                            _thisController.text =
                                DateFormat('yyyy-MM-dd').format(date);
                            //  });
                          }

                          setState(() {});
                        }),
                  ),
                ],
              ),
            ));
  }

  //validations for email and password. If the email is not correct, it will keep on auto validating, if the password is less than 6 characters, it will throw an error. Other validations can be written later on as per the requirements.
  bool validateStructure(String value) {
    String? pattern;

    RegExp regExp1 = RegExp(r'^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$');
    switch (typeRequireTextFieldV2) {
      case TypeRequireTextFieldV2.passWord:
        {
          if (value != null) {
            if (value.trim().isNotEmpty) {
              if (value.length >= 8 &&
                  value.length <= 25 &&
                  regExp1.hasMatch(value)) {
                widget.errorFree = true;
                return true;
              } else {
                widget.errorFree = false;
                return false;
              }
            } else {
              widget.errorFree = true;
              return true;
            }
          } else {
            widget.errorFree = true;
            return true;
          }
          /*r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';*/
        }
      default:
        {
          //statements;
        }
        break;
    }

    RegExp regExp = RegExp(pattern!);
    if (value != null) {
      if (value.trim().isNotEmpty) {
        if (regExp.hasMatch(value)) {
          widget.errorFree = true;
          return true;
        } else {
          widget.errorFree = false;
          return false;
        }
      } else {
        widget.errorFree = true;
        return true;
      }
    } else {
      widget.errorFree = true;
      return true;
    }
  }

  void toggle() {
    setState(() {
      indicator = !indicator;
    });
  }

  String? validateSignInPassword(String value) {
    String pattern =
        r"(^(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*?&.*':;€#])[A-Za-z\d@$!%*?&.*':;€#]{8,}$)";
    RegExp regExp = RegExp(pattern);
    if (!regExp.hasMatch(value)) {
      return AppLabels.signInPasswordValidation;
    }
    return null;
  }

  String? validateExpiry(String? value) {
    String pattern = r" = ^(0[1-9]|1[0-2])\/?(([0-9]{4}|[0-9]{2})$)";

    RegExp regExp = RegExp(pattern);
    if (!regExp.hasMatch(value!)) {
      return AppLabels.expiryValidation;
    }
    return null;
  }

  String? validateEmailAndMobile(String? value) {
    if (value!.length > 3) {
      if (isNumeric(value)) {
        return validateMobile(value);
      } else {
        return validateEmail(value);
      }
    } else {
      validate = true;
      return AppLabels.emailOrRegMobile;
    }
  }

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    try {
      return double.parse(s) != null;
    } catch (e) {
      // print("Error: $e");

      return false;
    }
  }

  String? validateMobile(String? value) {
    String pattern = r'(^[0-9]*$)';
    RegExp regExp = RegExp(pattern);
    if (value!.isEmpty) {
      return AppLabels.mobileRequired;
    } else if (value.length < 8) {
      return AppLabels.mobile9Digits;
    } else if (value.length > 12) {
      return AppLabels.mobile12Digits;
    } else if (!regExp.hasMatch(value)) {
      return AppLabels.mobileDigits;
    }
    return null;
  }

  String? validateEmail(String? value) {
    String pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = RegExp(pattern);
    if (value!.isEmpty) {
      return AppLabels.emailRequired;
    } else if (!regExp.hasMatch(value)) {
      return AppLabels.invalidEmail;
    } else {
      return null;
    }
  }

  String? validateFName(String? value) {
    if (value!.isEmpty) {
      return AppLabels.fNameRequired;
    } else {
      return null;
    }
  }

  String? validateName(String? value) {
    if (value!.isEmpty) {
      return AppLabels.nameRequired;
    } else {
      return null;
    }
  }

  String? validateText(String? value) {
    if (value!.isEmpty) {
      return AppLabels.defaultValidation;
    } else {
      return null;
    }
  }

  String? validateDOB(String? value) {
    if (value!.isEmpty) {
      return AppLabels.dobValidation;
    } else {
      return null;
    }
  }

  String? validateDate(String? value) {
    if (value!.isEmpty) {
      return AppLabels.dobValidation;
    } else {
      return null;
    }
  }

  String? validateLName(String? value) {
    if (value!.isEmpty) {
      return AppLabels.lNameRequired;
    } else {
      return null;
    }
  }

  String? validateLDefault(String? value) {
    if (value!.isEmpty) {
      return AppLabels.defaultValidation;
    } else {
      return null;
    }
  }

  String? validatePassword(String? value) {
    /*String pattern =
        r"(^(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*?&.*':;€#])[A-Za-z\d@$!%*?&.*':;€#]{8,}$)";
    // ignore: unused_local_variable
    RegExp regExp = RegExp(pattern);*/

    //old
    //RegExp regExp1 = RegExp(r'^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$');
    RegExp regExp1 = RegExp(r'^(?=.*[a-zA-Z])(?=.*[*".!@#\$%^&(){}:;<>,.\' +
        r"'?/~_+-=])(?=.*[0-9]).{8,30}");
    RegExp regExp2 = RegExp(r'^(?=.*[A-Z])');

    if (value!.length < 8) {
      return AppLabels.passwordValidation;
    } else if (!regExp1.hasMatch(value)) {
      return AppLabels.passwordValidation2;
    } else if (!regExp2.hasMatch(value)) {
      return AppLabels.passwordValidation3;
    }
    return null;
  }
}
