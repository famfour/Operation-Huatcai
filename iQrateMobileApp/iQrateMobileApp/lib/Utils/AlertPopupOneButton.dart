// ignore_for_file: file_names

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class AlertPopupOneButton extends StatelessWidget {
  final Function() callbackAfterPressOK;
  final String title;
  final String content;

  const AlertPopupOneButton(
      {Key? key,
      required this.callbackAfterPressOK,
      required this.title,
      required this.content})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoAlertDialog(
      title: Text(title),
      content: Text(content),
      actions: [
        CupertinoDialogAction(
          isDefaultAction: true,
          child: const Text("OK"),
          onPressed: () {
            Get.back();
            callbackAfterPressOK();
          },
        ),
      ],
    );
  }
}
