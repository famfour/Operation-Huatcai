// ignore_for_file: unused_field

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/Model/response_model.dart/login_response_model.dart';
import 'package:iqrate/Model/send_model.dart/login_send_model.dart';
import 'package:iqrate/Service/GetControllers.dart';
import 'package:iqrate/Service/core_services.dart';
import 'package:iqrate/Service/url.dart';
import 'package:iqrate/Utils/ItemCheckBoxModel.dart';
import 'package:iqrate/Utils/multi_checkbox_custom.dart';

class TestingLogic extends GetxController {
  final List<String> _items = [
    'Flutter',
    'Node.js',
    'React Native',
    'Java',
    'Docker',
    'MySQL',
  ];
  var oldDataSourceItem = <ItemCheckBoxModel>[];
  var dataSourceItem = <ItemCheckBoxModel>[];

  loadData() {
    dataSourceItem.clear();
    for (var i = 0; i < _items.length; i++) {
      var aa = _items[i];
      dataSourceItem.add(
          ItemCheckBoxModel(name: aa, value: i.toString(), isChecked: false));
    }

    // oldDataSourceItem.clear();
    // for (var i = 0; i < _items.length; i++) {
    //   var aa = _items[i];
    //   oldDataSourceItem.add(ItemCheckBoxModel(name: aa, value: i.toString(), isChecked: false));
    // }
  }
}

class Testing extends StatefulWidget {
  const Testing({Key? key}) : super(key: key);

  @override
  _TestingState createState() => _TestingState();
}

class Animal {
  final int id;
  final String name;

  Animal({
    required this.id,
    required this.name,
  });
}

class _TestingState extends State<Testing> {
  var needRefresh = false;
  var loginStatus = false;
  var textController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  TestingLogic getTestingLogic() {
    if (!Get.isRegistered<TestingLogic>()) {
      Get.put(TestingLogic());
    }
    return Get.find<TestingLogic>();
  }

  final List<Animal> _animals = [
    Animal(id: 1, name: "Lion"),
    Animal(id: 2, name: "Flamingo"),
    Animal(id: 3, name: "Hippo"),
  ];

  final List<Animal> initAnimals = [
    Animal(id: 1, name: "Lion"),
  ];
  //
  // List<Animal> _selectedAnimals = [];
  // // List<Animal> _selectedAnimals2 = [];
  // List<String> _selectedItems = [];
  @override
  void initState() {
    super.initState();
    getTestingLogic().loadData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Test'),
        ),
        body: bodyWidget());
  }
  //
  // void _showMultiSelectDialog(BuildContext context) async {
  //   await showDialog(
  //     context: context,
  //     builder: (ctx) {
  //       return MultiSelectDialog(
  //         searchable: true,
  //         items: _animals.map((e) => MultiSelectItem(e, e.name)).toList(),
  //         initialValue: _animals,
  //         selectedColor: AppColors.kPrimaryColor,
  //
  //         onConfirm: (values) {
  //           // _animals = values as List<Animal>;
  //           // _selectedAnimals2 = values as List<Animal>;
  //         },
  //       );
  //     },
  //   );
  // }

  void _showMultiSelect() async {
    // getTestingLogic().oldDataSourceItem = getTestingLogic().dataSourceItem;//clone backup
    if (kDebugMode) {
      print("backup");
    }
    // var newArray = <ItemCheckBoxModel>[];
    // // getTestingLogic().oldDataSourceItem.clear();
    // for (var i = 0; i < getTestingLogic().dataSourceItem.length; i++) {
    //   var link = getTestingLogic().dataSourceItem[i];
    //   newArray.add(link);
    //   // getTestingLogic().oldDataSourceItem.add(link);
    // }

    countTrue(getTestingLogic().oldDataSourceItem);
    if (kDebugMode) {
      print("current");
    }
    countTrue(getTestingLogic().dataSourceItem);
    await showDialog(
      context: context,
      builder: (BuildContext context) {
        // final TestingLogic controller = Get.put(TestingLogic());
        return MultiSelect(
          titleDropdown: "hhih",
          dataSourceItemOutSide: getTestingLogic().dataSourceItem,
          didChoose: (newlList) {
            getTestingLogic().dataSourceItem = newlList;
            // countTrue(newlList);
          },
        );
      },
    );
    // print("newList ${newList}");
    // getTestingLogic().dataSourceItem = newList!;
    //
    // if (newList != null) {
    //   getTestingLogic().dataSourceItem = newList;
    //   countTrue(getTestingLogic().dataSourceItem);
    //   // for (var i = 0; i < getTestingLogic().dataSourceItem.length; i++) {
    //   //   Logger().d(getTestingLogic().dataSourceItem[i].toJson());
    //   // }
    // }
    //
    //
    // final List<String>? results = await showDialog(
    //   context: context,
    //   builder: (BuildContext context) {
    //     // final TestingLogic controller = Get.put(TestingLogic());
    //     return MultiSelect(dataSourceItem : getTestingLogic().dataSourceItem, newListChanged: (newList) {
    //       getTestingLogic().dataSourceItem.value = newList;
    //     },);
    //   },
    // );
    //
    // // Update UI
    // if (results != null) {
    //   setState(() {
    //     _selectedItems = results;
    //   });
    // }
  }

  bodyWidget() {
    return InteractiveViewer(
      child: ListView(
        children: [
          Column(children: [


            ElevatedButton(
              child: const Text('Select Your Favorite Topics'),
              onPressed: () {
                // getTestingLogic().oldDataSourceItem = getTestingLogic().dataSourceItem;
                // countTrue(getTestingLogic().oldDataSourceItem);
                // countTrue(getTestingLogic().dataSourceItem);
                _showMultiSelect();
              },
            ),
            // TextButton(
            //     onPressed: () async {
            //       _showMultiSelectDialog(context);
            //     },
            //     child: Text("show multi dialog")),
            TextButton(
                onPressed: () async {
                  await login();
                  setState(() {});
                },
                child: Text("Login status $loginStatus")),
            TextButton(
                onPressed: () {
                  needRefresh = GetControllers.shared.isTokenNeedRefresh();
                  setState(() {});
                },
                child: Text("check need refresh $needRefresh")),
            TextButton(
                onPressed: () {
                  GetControllers.shared.fakeExpireDateMakeNeedRefresh();
                  needRefresh = GetControllers.shared.isTokenNeedRefresh();
                  setState(() {});
                },
                child: const Text("fake MAKE expire need refresh")),
            TextButton(
                onPressed: () {
                  GetControllers.shared.fakeTokenError();
                },
                child: const Text("fake make token errrr ")),
            TextButton(
                onPressed: () async {
                  await GetControllers.shared.callAPIRefreshToken();
                  setState(() {});
                },
                child: const Text("Force Call API refresh token again")),
            TextButton(
                onPressed: () async {
                  await GetControllers.shared.checkRefreshToken();
                  // setState(() {});
                },
                child: const Text("Check and Call API refresh if expire coming")),
            TextButton(
                onPressed: () async {
                  checkTokenAndCallAPIProfile();
                },
                child: const Text("Get profile")),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Form(
                  key: _formKey,
                  child: TextFormField(
                    onChanged: (value) {
                      if (kDebugMode) {
                        print("value nè $value");
                      }
                      _formKey.currentState!.validate();
                    },
                    // The validator receives the text that the user has entered.
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter some text';
                      }
                      return null;
                    },
                    decoration: const InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: AppColors.formFieldBorderColor),
                        borderRadius: BorderRadius.all(
                          Radius.circular(10.0),
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: AppColors.formFieldBorderColor),
                        borderRadius: BorderRadius.all(
                          Radius.circular(10.0),
                        ),
                      ),
                      border: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: AppColors.formFieldBorderColor),
                        borderRadius: BorderRadius.all(
                          Radius.circular(10.0),
                        ),
                      ),
                      // filled: true,
                      //Filled for the form field
                      // fillColor: AppColors.white, //Fill color for the form field
                    ),
                  )),
            ),
          ]),
        ],
      ),
    );
  }

  login() async {
    LoginSend loginSend = LoginSend(
      email: "phung@yopmail.com",
      password: "aA12345678",
    );
    var data = await CoreService().postWithoutAuthForLogin(
        url: baseUrl + loginUrl, body: loginSend.toJson());
    if (data == null) {
      loginStatus = false;
    } else {
      var result = LoginResponseModel.fromJson(data);
      if (result.accessToken != null) {
        GetControllers.shared.saveToken(result);
        loginStatus = true;
      }
    }
  }

  getProfile() async {
    await GetControllers.shared.getProfileScreenController().getProfileData();
  }

  checkTokenAndCallAPIProfile() async {
    // var doneCheck = await GetControllers.shared.checkRefreshToken();
    getProfile(); // fucntion call API need check token!
  }

  countTrue(List<ItemCheckBoxModel> datasource) {
    var count = 0;
    for (var i = 0; i < datasource.length; i++) {
      if (datasource[i].isChecked) count += 1;
    }
    if (kDebugMode) {
      print("count true $count");
    }
  }
}
//
// // Multi Select widget
// // This widget is reusable
// class MultiSelect extends StatefulWidget {
//   List<ItemCheckBoxModel> dataSourceItemOutSide;
//   final UniqueKey newKey;
//   MultiSelect({Key? key, required this.dataSourceItemOutSide, required this.newKey}) : super(key: newKey);
//   @override
//   State<StatefulWidget> createState() => _MultiSelectState();
// }
//
// class _MultiSelectState extends State<MultiSelect> {
//   var dataSourceItem = <ItemCheckBoxModel>[];
//   var dataSourceCheck = <ItemCheckBoxModel>[];
//
//   countTrue(List<ItemCheckBoxModel> datasource){
//     var count = 0;
//     for (var i = 0; i < datasource.length; i++) {
//       if (datasource[i].isChecked) count += 1;
//     }
//     print("count true $count");
//   }
//
//   TestingLogic getTestingLogic() {
//     if (!Get.isRegistered<TestingLogic>()) {
//       Get.put(TestingLogic());
//     }
//     return Get.find<TestingLogic>();
//   }
//
//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     print("initState MultiSelect run init");
//     // dataSourceItem.clear();
//     // for(var item in widget.dataSourceItemOutSide){
//     //   ItemCheckBoxModel newItem =  ItemCheckBoxModel(
//     //       name: item.name,
//     //       value: item.value,
//     //       isChecked: item.isChecked
//     //   );
//     //   dataSourceItem.add(newItem);
//     //   dataSourceCheck.add(newItem);
//     // }
//     for (var i = 0; i < widget.dataSourceItemOutSide.length; i++) {
//       var item = widget.dataSourceItemOutSide[i];
//         ItemCheckBoxModel newItem =  ItemCheckBoxModel(
//             name: item.name,
//             value: item.value,
//             isChecked: item.isChecked
//         );
//       dataSourceItem.add(newItem);
//       dataSourceCheck.add(newItem);
//     }
//     //
//     // for (var i = 0; i < widget.dataSourceItemOutSide.length; i++) {
//     //   var link = widget.dataSourceItemOutSide[i];
//     //   dataSourceOld.add(link);
//     // }
//
//     print("dataSourceItem after init from widget");
//     countTrue(dataSourceItem);
//   }
//   void _itemChange(String itemValue, bool isSelected, int index) {
//     dataSourceCheck[index].isChecked = isSelected;
//     setState(() {
//
//     });
//     for(var item in getTestingLogic().dataSourceItem){
//       print(item.isChecked);
//     }
//     // dataSourceItem[index].isChecked = isSelected;
//   }
//   // this function is called when the Cancel button is pressed
//   void _cancel() {
//     dataSourceCheck = dataSourceItem;
//     Navigator.pop(context);
//   }
//
// // this function is called when the Submit button is tapped
//   void _submit() {
//     print("_submit clicked newlist have ${dataSourceItem.length}");
//     dataSourceItem = dataSourceCheck;
//     getTestingLogic().dataSourceItem = dataSourceItem;
//     countTrue(dataSourceItem);
//     Navigator.pop(context);
//   }
//
//
//   @override
//   Widget build(BuildContext context) {
//     print("_MultiSelectState BuildContext clicked");
//     return AlertDialog(
//       title: const Text('Select Topics'),
//       content: SingleChildScrollView(
//         child: SizedBox(
//           width: double.maxFinite,
//           child: ListView.builder(
//               shrinkWrap: true,
//               itemCount:  dataSourceCheck.length,
//               itemBuilder: (context, index) {
//             return CheckboxListTile(
//               value:(dataSourceCheck[index].isChecked),
//               title: Text(dataSourceCheck[index].name),
//               controlAffinity: ListTileControlAffinity.leading,
//                 onChanged : (isChecked) {
//                   _itemChange(dataSourceCheck[index].name, isChecked!, index);
//                 }
//             );
//           }),
//         ),
//       ),
//       actions: [
//         TextButton(
//           child: const Text('Cancel'),
//           onPressed: _cancel,
//         ),
//         ElevatedButton(
//           child: const Text('Submit'),
//           onPressed: _submit,
//         ),
//       ],
//     );
//   }
// }
