import 'package:flutter/material.dart';
// import 'colors.dart';
import 'colors.dart';
import 'screen_constants.dart';

class TextStyles {
  static TextStyle get blankTest => TextStyle(
        color: Colors.white70,
        fontSize: FontSize.s30,
        fontWeight: FontWeight.w800,
      );

  static TextStyle get subTitle => TextStyle(
        color: AppColors.background,
        fontSize: FontSize.s14,
        fontWeight: FontWeight.w600,
      );
  static TextStyle get lowerSubTitle => TextStyle(
        color: Colors.black,
        fontSize: FontSize.s12,
        //fontWeight: FontWeight.w600,
      );
  static TextStyle get lowerSubTitleHintText => TextStyle(
        color: Colors.grey.shade400,
        fontSize: FontSize.s12,
        //fontWeight: FontWeight.w600,
      );
  static TextStyle get primaryButtonTextStyle => const TextStyle(
        color: Colors.white,
        fontSize: 20,
        fontWeight: FontWeight.bold,
      );
  static TextStyle get secondaryButtonTextStyle => const TextStyle(
        color: Color(0XFF574E4F),
        fontWeight: FontWeight.bold,
        fontSize: 20,
      );

  static TextStyle get profilePicTextStyle => const TextStyle(
        color: Color(0XFFE09207),
        fontWeight: FontWeight.bold,
        fontSize: 60,
      );

  static TextStyle get secondaryButtonTextStyle2 => const TextStyle(
        color: Colors.grey,
        fontWeight: FontWeight.bold,
        fontSize: 20,
      );

  static TextStyle get introScreenTitles => TextStyle(
        color: const Color(0XFF777373),
        fontSize: FontSize.s30,
        fontWeight: FontWeight.w500,
      );
  static TextStyle get introScreenTitlesUnderlined => TextStyle(
      color: const Color(0XFFF4583C),
      fontSize: FontSize.s30,
      fontWeight: FontWeight.w700,
      decoration: TextDecoration.underline);
  static TextStyle get introScreenDescriptions => TextStyle(
        color: const Color(0XFF87898C),
        fontSize: FontSize.s18,
      );
  static TextStyle get headingTextStyle => const TextStyle(
      color: AppColors.kPrimaryColor,
      fontSize: 18,
      fontWeight: FontWeight.w600);
  static TextStyle get headingSubtitleStyle => TextStyle(
        color: AppColors.kPrimaryTextColor,
        fontSize: FontSize.s16,
        fontWeight: FontWeight.w400,
      );

  static TextStyle get headingSubtitleStyle2 => TextStyle(
        color: Colors.black87,
        fontSize: FontSize.s14,
        fontWeight: FontWeight.w400,
      );

  static TextStyle get headingSubtitleStyle3 => TextStyle(
        color: Colors.black,
        fontSize: FontSize.s18,
        fontWeight: FontWeight.bold,
      );
  static TextStyle get headingSubtitleStyle4 => TextStyle(
        color: Colors.black,
        fontSize: FontSize.s14,
        fontWeight: FontWeight.bold,
      );

  static TextStyle get pdpaSendtitleStyle => TextStyle(
        color: Colors.deepOrange,
        fontSize: FontSize.s18,
        decoration: TextDecoration.underline,
        fontWeight: FontWeight.w400,
      );

  static TextStyle get pdpaSendtitleStyle1 => TextStyle(
        color: Colors.deepOrange,
        fontSize: FontSize.s14,
        decoration: TextDecoration.underline,
        fontWeight: FontWeight.w400,
      );

  static TextStyle get headingSubtitleStyleSmall => TextStyle(
        color: AppColors.kPrimaryTextColor,
        fontSize: FontSize.s14,
        fontWeight: FontWeight.w400,
      );
  static TextStyle get formFieldTitleTextStyle => const TextStyle(
        color: Color(0XFF767676),
        fontSize: 13,
        fontWeight: FontWeight.w400,
      );

  static TextStyle get formFieldTitleTextStyle1 => const TextStyle(
        color: Color(0XFF767676),
        fontSize: 15,
        fontWeight: FontWeight.w400,
      );

  static TextStyle get perAnnumTitleTextStyle => const TextStyle(
        color: Color(0XFF767676),
        fontSize: 16,
        fontWeight: FontWeight.w400,
      );

  static TextStyle get periodTitleTextStyle => const TextStyle(
        color: Color(0XFF767676),
        fontSize: 15,
        fontWeight: FontWeight.w700,
      );

  static TextStyle get pricingTitleTextStyle => const TextStyle(
        color: Color(0XFF767676),
        fontSize: 15,
        fontWeight: FontWeight.w400,
      );

  static TextStyle get orTextStyle => const TextStyle(
        color: Color(0XFFAAAAAA),
        fontSize: 20,
        fontWeight: FontWeight.w600,
      );
  static TextStyle get alreeadyHaveAnAccount => const TextStyle(
        color: Color(0XFF98A2AD),
        fontSize: 17,
        fontWeight: FontWeight.w400,
      );

  static TextStyle get profileClickForENameCard => TextStyle(
      color: Colors.white,
      fontSize: FontSize.s16,
      fontWeight: FontWeight.w700,
      decoration: TextDecoration.underline);

  static TextStyle get profileexpansionTitle => TextStyle(
        color: AppColors.kSecondaryColor,
        fontSize: FontSize.s16,
        fontWeight: FontWeight.w700,
      );

  static TextStyle get profileNameTextStyle => const TextStyle(
        color: AppColors.background,
        fontSize: 18,
        fontWeight: FontWeight.w600,
      );
  static TextStyle get loginTextButtonStyle => TextStyle(
        color: const Color(0XFFDF5356),
        fontSize: FontSize.s17,
        fontWeight: FontWeight.w700,
      );

  static TextStyle get basicTextStyle => const TextStyle(
        color: Colors.black,
        fontSize: 22,
        fontWeight: FontWeight.w500,
      );

  static TextStyle get basicTextStyle2 => const TextStyle(
        color: Colors.white,
        fontSize: 20,
        fontWeight: FontWeight.w500,
      );

  static TextStyle get basicTextStyleRed => TextStyle(
        color: AppColors.leadListTitle,
        fontSize: 22,
        fontWeight: FontWeight.w500,
      );
  static TextStyle get basicTextStyleGreen => TextStyle(
        color: AppColors.leadListTitleGreen,
        fontSize: 22,
        fontWeight: FontWeight.w500,
      );
  static TextStyle get basicTextStyleGrey => TextStyle(
        color: AppColors.leadListTitleGrey,
        fontSize: 22,
        fontWeight: FontWeight.w500,
      );

  static TextStyle get leadsTextStyle => const TextStyle(
      color: Colors.black87,
      fontSize: 18,
      fontWeight: FontWeight.w500,
      height: 1.5);

  static TextStyle get tabTitle =>
      const TextStyle(fontSize: 16, fontWeight: FontWeight.w500);

  static TextStyle get leadsTextStyle1 => const TextStyle(
      color: Colors.black,
      fontSize: 15,
      fontWeight: FontWeight.w400,
      height: 1.5);
  static TextStyle get valueMenuReportTextStyle => const TextStyle(
      color: Colors.black,
      fontSize: 15,
      fontWeight: FontWeight.w700,
      height: 1.5);

  static TextStyle get leadsTextStyleBold => const TextStyle(
      color: Colors.black87,
      fontSize: 20,
      fontWeight: FontWeight.bold,
      height: 1.5);

  static TextStyle get leadsUnderlineTextStyle => const TextStyle(
        color: Colors.black54,
        fontSize: 16,
        fontWeight: FontWeight.w400,
        decoration: TextDecoration.underline,
        height: 1.5,
      );

  static TextStyle get docLinkTextStyle => const TextStyle(
        color: Colors.lightBlue,
        fontSize: 16,
        fontWeight: FontWeight.w400,
        decoration: TextDecoration.underline,
        height: 1.5,
      );

  static TextStyle get leadsNormalTextStyle => const TextStyle(
        color: Colors.black54,
        fontSize: 16,
        fontWeight: FontWeight.w400,
        height: 1.5,
      );

  static TextStyle get docTextStyle => const TextStyle(
        color: Colors.black54,
        fontSize: 20,
        fontWeight: FontWeight.w400,
        height: 1.5,
      );

  static TextStyle get rateExportColorTextStyle => const TextStyle(
      color: Colors.red,
      fontSize: 16,
      fontWeight: FontWeight.w400,
      decoration: TextDecoration.underline,
      height: 1.5);

  static TextStyle get leadsColorTextStyle => const TextStyle(
      color: Colors.red,
      fontSize: 20,
      fontWeight: FontWeight.w400,
      height: 1.5);

  static TextStyle get leadsColorTextStyle1 => const TextStyle(
      color: Colors.red,
      fontSize: 22,
      fontWeight: FontWeight.bold,
      height: 1.5);

  static TextStyle get leadsColorTextStyle2 => const TextStyle(
      color: Colors.black87,
      fontSize: 20,
      fontWeight: FontWeight.bold,
      height: 1.5);

  static TextStyle get freeTextStyle => const TextStyle(
        color: Color(0XFFE68117),
        fontSize: 21,
        fontWeight: FontWeight.w600,
      );

  static TextStyle get tosAndPrivacyPolicyStyle => const TextStyle(
        fontSize: 19,
        color: Color(0XFF98A2AD),
      );
  static TextStyle get tosAndPrivacyPolicyStyleLink => const TextStyle(
        fontSize: 19,
        color: Color(0XFF98A2AD),
        decoration: TextDecoration.underline,
      );
  static TextStyle get introScreenTitlesSmall => TextStyle(
        color: const Color(0XFFF4583C),
        fontSize: FontSize.s22,
        fontWeight: FontWeight.w500,
      );

  static TextStyle get forgetInstructionTitleTextStyle => const TextStyle(
        color: Color(0XFF767676),
        fontSize: 15,
        fontWeight: FontWeight.w400,
      );

  static TextStyle get authTextTitleStyle => const TextStyle(
        color: Color(0XFFDF5356),
        fontSize: 17,
        fontWeight: FontWeight.w600,
      );
  static TextStyle get appBarTextStyle => TextStyle(
        fontSize: FontSize.s24,
        fontWeight: FontWeight.w700,
        color: Colors.white,
      );

  static TextStyle get appBarTextStyle2 => TextStyle(
        fontSize: FontSize.s16,
        fontWeight: FontWeight.w700,
        color: Colors.white,
      );

  static TextStyle get createLeadPrivacyTextStyle => const TextStyle(
      color: Colors.black54,
      fontSize: 16,
      fontWeight: FontWeight.w400,
      height: 1.5);

  // Rates screen textStyles
  static TextStyle get cardHeading => TextStyle(
        color: const Color(0xff483f3f),
        fontSize: FontSize.s17,
        fontWeight: FontWeight.w700,
      );
  static TextStyle get cardSubHeading => TextStyle(
        color: const Color(0xff483f3f),
        fontSize: FontSize.s14,
        fontWeight: FontWeight.w400,
      );
  static TextStyle get detailsButton => TextStyle(
        color: Colors.white,
        fontSize: FontSize.s12,
        fontWeight: FontWeight.w700,
      );
  // Notification page text Style
  static TextStyle get notificationBody => TextStyle(
        color: const Color(0xff483f3f),
        fontSize: FontSize.s16,
        fontWeight: FontWeight.w400,
      );
  // More view textStyle
  static TextStyle get cardTitle => TextStyle(
        color: const Color(0xff483f3f),
        fontSize: FontSize.s20,
        fontWeight: FontWeight.w600,
      );

  static TextStyle get cardTitle2 => TextStyle(
        color: const Color(0xff483f3f),
        fontSize: FontSize.s16,
        fontWeight: FontWeight.w600,
      );

  // Training view textStyle
  static TextStyle get cardContentTextStyle => TextStyle(
        color: const Color(0xff483f3f),
        fontSize: FontSize.s16,
        fontWeight: FontWeight.w400,
      );

  static TextStyle get dashboardPopUpTextStyle => TextStyle(
        color: Colors.black87,
        fontSize: FontSize.s18,
      );
  // Subscription screen TextStyle
  static TextStyle get titleTextStyle => TextStyle(
        color: Colors.black,
        fontSize: FontSize.s14,
        fontWeight: FontWeight.w700,
      );
  static TextStyle get descriptionTextStyle => TextStyle(
        color: Colors.black,
        fontSize: FontSize.s11,
        fontWeight: FontWeight.w600,
      );

  static TextStyle get descriptionTextStyleGrey => TextStyle(
        color: Colors.grey,
        fontSize: FontSize.s11,
        fontWeight: FontWeight.w600,
      );

  static TextStyle get fadedTextStyle => TextStyle(
        color: Colors.black.withOpacity(0.5),
        fontSize: FontSize.s12,
        fontWeight: FontWeight.w700,
      );
  static TextStyle get headingTextStyleWhite => TextStyle(
        color: Colors.white,
        fontSize: FontSize.s12,
        fontWeight: FontWeight.w700,
      );
  static TextStyle get planTermSelectionTextStyle => TextStyle(
        color: Colors.white,
        fontSize: FontSize.s14,
        fontWeight: FontWeight.w700,
      );
  static TextStyle get redPlanTermSelectionTextStyle => TextStyle(
        color: AppColors.kPrimaryColor,
        fontSize: FontSize.s14,
        fontWeight: FontWeight.w700,
      );
  static TextStyle get planTermSelectionPriceTextStyle => TextStyle(
        color: Colors.white,
        fontSize: FontSize.s20,
        fontWeight: FontWeight.w700,
      );

  static TextStyle get planTermSelectionPriceTextStyle3 => TextStyle(
        color: Colors.white70,
        fontSize: FontSize.s24,
        fontWeight: FontWeight.w500,
      );

  static TextStyle get planTermSelectionPriceTextStyle2 => TextStyle(
        color: Colors.white70,
        fontSize: FontSize.s20,
        fontWeight: FontWeight.w400,
      );

  static TextStyle get blackPlanTermSelectionPriceTextStyle => TextStyle(
        color: Colors.black,
        fontSize: FontSize.s20,
        fontWeight: FontWeight.w700,
      );
  static TextStyle get planTermSelectionPriceSavingTextStyle => TextStyle(
        color: Colors.green,
        fontSize: FontSize.s12,
        fontWeight: FontWeight.w400,
      );
  static TextStyle get whitePlanTermSelectionPriceSavingTextStyle => TextStyle(
        color: Colors.white,
        fontSize: FontSize.s12,
        fontWeight: FontWeight.w400,
      );
  static TextStyle get paymentDetailsTitle => TextStyle(
        color: Colors.black,
        fontSize: FontSize.s18,
        fontWeight: FontWeight.w700,
      );
  static TextStyle get paymentDetailsSubTitle => TextStyle(
        color: Colors.black.withOpacity(0.5),
        fontSize: FontSize.s14,
        fontWeight: FontWeight.w600,
      );

// Manage loan -> Bank submission
  static TextStyle get selectedBankTextTextStyles => TextStyle(
        color: Colors.black,
        fontSize: FontSize.s18,
        fontWeight: FontWeight.w700,
      );
  static TextStyle get bankNameTextStyle => TextStyle(
        color: Colors.black,
        fontSize: FontSize.s18,
        fontWeight: FontWeight.w400,
      );
  static TextStyle get title => TextStyle(
        color: AppColors.kPrimaryColor,
        fontSize: FontSize.s16,
        fontWeight: FontWeight.w400,
      );
  static TextStyle get manageLoanText => TextStyle(
        color: Colors.white,
        fontSize: FontSize.s20,
        fontWeight: FontWeight.w700,
      );
  static TextStyle get resendButtonTextStyle => TextStyle(
        color: AppColors.kPrimaryColor,
        fontSize: FontSize.s18,
        fontWeight: FontWeight.w500,
      );

  static TextStyle get jointApplicantHeading => TextStyle(
        color: AppColors.kPrimaryColor,
        fontSize: FontSize.s18,
        fontWeight: FontWeight.w700,
      );
  static TextStyle get cardSubTitle => TextStyle(
        color: const Color(0XFFA9A9A9),
        fontSize: FontSize.s13,
        fontWeight: FontWeight.w400,
      );
  static TextStyle get calculatorHeadingsTextStyle => TextStyle(
      color: AppColors.kPrimaryColor,
      fontSize: FontSize.s20,
      fontWeight: FontWeight.w600);

  static TextStyle get faqDescriptionTextStyle => TextStyle(
        color: const Color(0xff483f3f),
        fontSize: FontSize.s20,
        fontWeight: FontWeight.w400,
      );
  static TextStyle get onlyFaqDescriptionTextStyle => TextStyle(
        color: const Color(0xff483f3f),
        fontSize: FontSize.s16,
        fontWeight: FontWeight.w400,
      );
  static TextStyle get trainingDetailsCategoryHeadingTextSTyle => TextStyle(
        color: AppColors.background,
        fontSize: FontSize.s24,
        fontWeight: FontWeight.w700,
      );
  static TextStyle get trainingDetailsCategoryBodyTextSTyle => TextStyle(
        color: AppColors.background,
        fontSize: FontSize.s20,
        fontWeight: FontWeight.w400,
      );
  static TextStyle get calculatorResultTextStyle => TextStyle(
        color: Colors.black,
        fontSize: FontSize.s23,
        fontWeight: FontWeight.w600,
      );

  // LawFirm details list screen
  static TextStyle get headingTextStyleLawFirm => TextStyle(
        color: AppColors.kPrimaryColor,
        fontSize: FontSize.s18,
        fontWeight: FontWeight.w600,
      );
  static TextStyle get bigHeadingTextStyleLawFirm => TextStyle(
        color: AppColors.titleBlack,
        fontSize: FontSize.s24,
        fontWeight: FontWeight.w700,
      );
  static TextStyle get bodyTextStyleLawFirm => TextStyle(
        color: const Color(0xff6F767E),
        fontSize: FontSize.s14,
        fontWeight: FontWeight.w600,
      );
  static TextStyle get bodyTextStyUnderLinedleLawFirm => TextStyle(
      color: AppColors.titleBlack,
      fontSize: FontSize.s14,
      fontWeight: FontWeight.w600);

// Filter textSTyle
  static TextStyle get filterDropdownButton => TextStyle(
        color: AppColors.kPrimaryColor,
        fontSize: FontSize.s14,
        fontWeight: FontWeight.w400,
      );
  // Bank submission
  static TextStyle get bankSubmissionCardTitle => TextStyle(
        color: Colors.black,
        fontSize: FontSize.s18,
        fontWeight: FontWeight.w600,
      );

  static TextStyle get bankSubmissionTaskTextStyle => TextStyle(
        color: Colors.red,
        fontSize: FontSize.s16,
        fontWeight: FontWeight.w400,
      );
  static TextStyle get bankSubmissionCompletedTaskTextStyle => TextStyle(
        color: Colors.green,
        fontSize: FontSize.s16,
        fontWeight: FontWeight.w400,
      );

  static TextStyle get bankSubmissionTaskTextStyle1 => TextStyle(
        color: Colors.deepOrange,
        fontSize: FontSize.s16,
        fontWeight: FontWeight.w500,
      );

  static TextStyle get bankSubmissionTitleTestStyle => leadsColorTextStyle1;
  static TextStyle get bankSubmissionCardTitleDisabled => TextStyle(
        color: Colors.grey,
        fontSize: FontSize.s18,
        fontWeight: FontWeight.w600,
      );
  static TextStyle get bankSubmissionTitles => headingSubtitleStyleSmall;
  static TextStyle get bankSubmissionBody => TextStyle(
        color: Colors.black,
        fontSize: FontSize.s16,
        fontWeight: FontWeight.w400,
      );
  static TextStyle get bankSubmissionSuccessScreenText => TextStyle(
        color: Colors.black,
        fontSize: FontSize.s18,
        fontWeight: FontWeight.w400,
      );
  static TextStyle get bankSubmissionDocumentScreenTextStyle => TextStyle(
        color: Colors.black,
        fontSize: FontSize.s14,
        fontWeight: FontWeight.w600,
      );
}



/*
{
FontWeight.w100: 'Thin',
FontWeight.w200: 'ExtraLight',
FontWeight.w300: 'Light',
FontWeight.w400: 'Regular',
FontWeight.w500: 'Medium',
FontWeight.w600: 'SemiBold',
FontWeight.w700: 'Bold',
FontWeight.w800: 'ExtraBold',
FontWeight.w900: 'Black',
}*/
