import 'package:flutter/material.dart';

//Colors are defined here so that everything remains in a single file to avoid any confusions
class AppColors {
  static const Color kPrimaryColor = Color(0XFFF4583C);
  static const Color kSecondaryColor = Color(0XFFF9484A);
  static const Color kPrimaryTextColor = Color(0XFF87898C);
  static const Color formFieldBorderColor = Color(0XFF9A9696);
  static const Color white = Color(0xFFFFFFFF);
  static const Color boxWhite = Color(0xFFF2F3F9);
  static const Color transparent = Color(0x00000000);
  static const Color background = Color(0xFF000000);
  static const Color backgroundWhite = Color(0xFFFBFBFE);
  static const Color red = Color(0xFFE93941);
  static const Color leadBG = Color(0xFFBCBDC4);
  static const Color textFieldTextColor = Color(0xFF707070);
  static const Color subTitleColor = Color(0xFFD9D9D9);
  static const Color buttonTextColor = Color(0xFF080909);
  static const Color iconColor = Color(0xFFB9CECC);
  static const Color borderColor = Color(0xFF262A2B);
  static const Color buttonBackground = Color(0xFFDFEFEE);
  static const Color buttonBackgroundBlack = Color(0xFF212121);
  static const Color titleBlack = Color(0xFF1C2140);
  static const Color subTitleBlack = Color(0xFF5B6084);
  static const Color subtitleHighlight = Color(0xFF6F6004);
  static const Color filledColor = Color(0xFFFFC107);
  static const Color imageBack = Color(0xFF98A9BC);
  static const Color lightTextGreen = Color(0xFF10A16B);
  static const Color textRegular = Color(0xFFB9C3CF);
  static const Color blogSub = Color(0xFF828282);
  static const Color greyF4F4F4 = Color(0xFFF4F4F4);
  static const Color grey606060 = Color(0xFF606060);
  static const Color grey747474 = Color(0xFF747474);
  static const Color grey636363 = Color(0xFF636363);
  static const Color greyA8A8A8 = Color(0xFFA8A8A8);
  static const Color greyDEDEDE = Color(0xFFDEDEDE);
  static const Color greyF5F5F5 = Color(0xFFF5F5F5);
  static const Color grey625F5A = Color(0xFF625F5A);
  static const Color grey979FAA = Color(0xFF979FAA);
  static const Color grey7B838E = Color(0xFF7B838E);
  static const Color greyD4D8D8 = Color(0xFFD4D8D8);
  static const Color greyDFE2E9 = Color(0xFFD4D8D8);
  static const Color greyD1D8D8 = Color(0xFFD1D8D8);
  static const Color greyFBFBFB = Color(0xFFFBFBFB);
  static const Color black1D232A = Color(0xFF1D232A);
  static const Color tealAEDBD8 = Color(0xFFAEDBD8);
  static const Color teal7AA4A1 = Color(0xFF7AA4A1);
  static const Color tealF4F8F8 = Color(0xFFF4F8F8);
  static Color leadListExpansionBG = const Color(0xffF4080D).withOpacity(0.2);
  static Color leadListExpansionGreenBG =
      const Color(0xff34A853).withOpacity(0.2);
  static Color leadListTitle = const Color(0xffDA3B3E);
  static Color leadListTitleGreen = const Color(0xff34A853);
  static Color leadListTitleGrey = const Color(0xffA7A6A6);

  static Color tabBg = const Color(0xffFFEBEB);

  static const MaterialColor cyan = MaterialColor(
    0xFF278FA4,
    <int, Color>{
      50: Color(0xFFE0F7FA),
      100: Color(0xFFB2EBF2),
      200: Color(0xFF80DEEA),
      300: Color(0xFF4DD0E1),
      400: Color(0xFF26C6DA),
      500: Color(0xFF278FA4),
      600: Color(0xFF00ACC1),
      700: Color(0xFF0097A7),
      800: Color(0xFF00838F),
      900: Color(0xFF006064),
    },
  );
}
