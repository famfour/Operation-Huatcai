class AppLabels {
  static const String signInPasswordValidation = "Please enter your password";
  static const String expiryValidation = "Please enter a valid expiry date";
  static const String emailOrRegMobile =
      "Please enter valid email or mobile number";
  static const String mobileDigits = "Mobile Number must be digits";
  static const String mobile9Digits = "Enter minimum 8 digits number";
  //static const String mobile9Digits = "Mobile number must be minimum 10 digits (International Format)";
  static const String mobile12Digits = "Enter maximum 12 digits number";
  //static const String mobile12Digits = "Mobile number must be maximum 12 digits (International Format)";
  static const String mobileRequired = "Mobile number is required";
  static const String invalidEmail = "Invalid Email";
  static const String emailRequired = "Email is required";
  static const String fNameRequired = "First Name is required";
  static const String lNameRequired = "Last Name is required";
  static const String passwordValidation =
      "Password must be 8 alphanumeric characters";
  static const String passwordValidation2 =
      "Password must be 8 alphanumeric characters";
  static const String passwordValidation3 =
      "Password must be atleast one uppercase character";
  static const String dobValidation = "Date of birth is required.";
  static const String defaultValidation = "Field is required.";
  static const String nameRequired = "Name is required";
  static const String incorrectPostalCode =
      "Postal code must be of 6 characters";
}
