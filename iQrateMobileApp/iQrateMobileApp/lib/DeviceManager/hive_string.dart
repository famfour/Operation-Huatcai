class HiveString {
  static const String hiveName = "storage";
  static const String fcmToken = "fcmToken";
  static const String deviceId = "deviceId";
  static const String token = "token";
  static const String userId = "userId";
  static const String userData = "userData";
  static const String first = "first";
  static const String refreshToken = "refreshToken";
}
