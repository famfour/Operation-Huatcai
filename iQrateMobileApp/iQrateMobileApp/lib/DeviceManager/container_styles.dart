import 'package:flutter/material.dart';

//This file contains all the different container styles that have been used through out the UI of the app.
class ContainerStyles {
  //=================================Secondary button container styles for gradient buttons=================================//
  static BoxDecoration get kInnerDecorationSecondaryButton => BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.white),
        borderRadius: BorderRadius.circular(20),
      );

  static BoxDecoration get kGradientBoxDecorationSecondaryButton =>
      BoxDecoration(
        gradient: const LinearGradient(colors: [
          Color(0XFFE09207),
          Color(0XFFF9484A),
        ]),
        border: Border.all(
          color: Colors.blueGrey,
        ),
        borderRadius: BorderRadius.circular(20),
      );
  //=================================Secondary button container styles for gradient buttons=================================//

  //calculator button
  static BoxDecoration get boxDecorationCalculatorButton => BoxDecoration(
        border: Border.all(
          color: Colors.black54,
        ),
        borderRadius: BorderRadius.circular(20),
      );

  static BoxDecoration get kGradientBoxDecorationDashboardAppbar =>
      const BoxDecoration(
          gradient: LinearGradient(colors: [
        Color(0XFF9C0E10),
        Color(0XFFDF5356),
      ]));

//Subscription Option Button Container
  static BoxDecoration get subscriptionOptionsViewButtonContainerStyle =>
      (BoxDecoration(
        borderRadius: BorderRadius.circular(7),
        border: Border.all(
          color: const Color(0XFFDA3B3E),
        ),
      ));

  //Subscription Options Container
  static BoxDecoration get subscriptionOptionsContainer => (BoxDecoration(
        borderRadius: BorderRadius.circular(4),
        border: Border.all(
          color: const Color(0XFF9A9696),
        ),
      ));
}
