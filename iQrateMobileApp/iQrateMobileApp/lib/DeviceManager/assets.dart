class Assets {
  static const String tempLogo = 'https://cdn.dribbble.com/users/1622791/screenshots/11174104/flutter_intro.png';
  static const String logo = 'assets/logos/iqrate_logo.svg';
  static const String logoWhite = 'assets/logos/iqrate_logo_white.svg';
  static const String dashboardDollarItem = 'assets/icons/dashboard_dollar.png';
  static const String avatar = 'https://images.unsplash.com/photo-1580518324671-c2f0833a3af3?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8ZmFjZSUyMG1hbnxlbnwwfHwwfHw%3D&w=1000&q=80';
  static const String editProfile = 'assets/images/edit_profile.png';
  static const String manageLoanDone = 'assets/icons/done.svg';
  static const String manageLoanIncomplete = 'assets/icons/incomplete.svg';
  static const String manageLoanClose = 'assets/icons/closeEnclosed.svg';

}
