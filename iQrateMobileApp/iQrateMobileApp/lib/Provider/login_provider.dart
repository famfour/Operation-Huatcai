import 'package:get/get.dart';

class LoginProvider extends GetConnect {
  Future<dynamic> postWithoutAuth(
      {Map<String, dynamic>? body, required String url}) async {
    var data = await post(url, body);
    if (data.hasError) {
      return null;
    } else {
      return data.body;
    }
  }
}
