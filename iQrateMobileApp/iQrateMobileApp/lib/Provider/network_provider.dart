import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:iqrate/Service/url.dart';
import 'package:logger/logger.dart';

/// PLEASE DONT EDIT ANYTHING HERE !! - message phung for more details!!
class Logging extends Interceptor {
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    if (kDebugMode) {
      print('REQUEST[${options.method}] => PATH: ${options.path}');
    }
    return super.onRequest(options, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    if (kDebugMode) {
      print(
        'RESPONSE[${response.statusCode}] => ✅ ✅ FullUrl PATH: ${response.requestOptions.path}',
      );
    }
    if (kDebugMode) {
      print(
        'DATA[${response.data}',
      );
    }
    return super.onResponse(response, handler);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    if (kDebugMode) {
      print(
        'ERROR[${err.response?.statusCode}] => PATH: ${err.requestOptions.path}',
      );
    }
    return super.onError(err, handler);
  }
}

class NetworkProvider {
  final Dio _dio = Dio(
    BaseOptions(
      baseUrl: baseUrl,
      connectTimeout: 15000,
      receiveTimeout: 15000,
      // contentType: Headers.formUrlEncodedContentType
    ),
  )..interceptors.add(
      Logging(),
    );

  isSuccessAPI(dynamic data) {
    return (data['success'] ?? true);
  }

  getErrorMessage(dynamic data) {
    Logger().d(data);
    return data['message'].toString().replaceAll('[', '').replaceAll(']', '');
  }

  Future callAPIPost(
      {String token = "",
      String url = "",
      dynamic data,
      Map<String, dynamic>? query}) async {
    if (kDebugMode) {
      print("start callAPIPost");
    }
    if (token != "") {
      _dio.options.headers["Authorization"] = "Bearer $token";
    }
    try {
      Response response;
      if (data != null) {
        response = await _dio.post(url, data: data);
      } else {
        response = await _dio.post(url);
      }
      var jsonBody = response.data;
      return jsonBody;
    } on DioError catch (e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      if (e.response != null) {
        if (kDebugMode) {
          print('Dio error!');
          print('STATUS: ${e.response?.statusCode}');
          print('DATA: ${e.response?.data}');
          print('HEADERS: ${e.response?.headers}');
        }

        //data
        if (data != null && data is FormData) {
          Logger().d(data.fields.toString());
        }
        // callback(e.response?.data);
        var a = {"success": false, "message": e.response?.data};
        // return e.response?.data;
        return a;
      } else {
        // Error due to setting up or sending the request
        if (kDebugMode) {
          print('Error sending request!');
          print(e.message);
        }
        var a = {"success": false, "message": e.message};
        return a;
      }
    }
  }
}
