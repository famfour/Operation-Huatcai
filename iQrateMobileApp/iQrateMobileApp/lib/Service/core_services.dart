// ignore_for_file: non_constant_identifier_names, prefer_adjacent_string_concatenation

import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:iqrate/Controller/login_controller.dart';
import 'package:iqrate/DeviceManager/hive_string.dart';
import 'package:iqrate/Model/header_model.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Service/string_utils.dart';
import 'package:iqrate/Service/url.dart';
import 'package:logger/logger.dart';

import '../Model/send_model.dart/header_model_file.dart';

class CoreService extends GetConnect {
  final Box hive = Hive.box(HiveString.hiveName);

  Future<dynamic> getWithoutAuth({required String url}) async {
    debugPrint("Url : $url");
    var data = await get(url);
    debugPrint("Body : ${data.body}");
    if (data.hasError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: data.body["message"],
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.hasError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isServerError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });

      return null;
    } else if (data.status.connectionError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.connectionErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isNotFound) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.isNotFoundMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else {
      return data.body;
    }
  }

  Future<dynamic> getWithAuth({required String url}) async {
    HeaderModel headerModel =
        HeaderModel(authorization: "Bearer ${hive.get(HiveString.token)}");
    debugPrint("Header : ${"Bearer ${hive.get(HiveString.token)}"}");
    debugPrint("Url : $url");
    var data = await get(url, headers: headerModel.toHeader());
    log("statusCode : ${data.statusCode}");
    log("User Body : ${jsonEncode(data.body)}" + url);
    if (data.statusCode == 400 && data.body["detail"] != null) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: data.body["detail"],
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });

      return null;
    } else if (data.hasError) {
      return data.statusCode;
    } else if (data.status.hasError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isServerError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });

      return null;
    } else if (data.status.connectionError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.connectionErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isNotFound) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.isNotFoundMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else {
      return data.body;
    }
  }

  Future<dynamic> getWithAuthAndQuery(
      {required String url, Map<String, dynamic>? query}) async {
    HeaderModel headerModel =
        HeaderModel(authorization: "Bearer ${hive.get(HiveString.token)}");
    debugPrint("Header : ${"Bearer ${hive.get(HiveString.token)}"}");
    debugPrint("Url : $url");
    log("query : ${query.toString()}");
    var data = await get(url, query: query, headers: headerModel.toHeader());
    log("Response Body : ${data.body}");
    if (data.hasError) {
      return null;
    } else if (data.status.hasError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isServerError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });

      return null;
    } else if (data.status.connectionError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.connectionErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isNotFound) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.isNotFoundMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else {
      return data.body;
    }
  }

  Future<dynamic> postWithoutAuth(
      {Map<String, dynamic>? body, required String url}) async {
    debugPrint("Url : $url");
    log("Body : $body");
    var data = await post(url, body);
    // var data = await post(url, FormData(body!)); //This is used for passing the body as form data

    debugPrint("response : ${data.body}");
    debugPrint("response code : ${data.status.code}");

    if (data.hasError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        if (data.body["name"] != null && data.body["name"] == "OTP_ERROR") {
          Get.find<SignInController>().redirectFromLogin.value = true;
          Get.toNamed(authenticatorOtp);
          return null;
        }
        if (data.body['errorMessage'] != null) {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: data.body["errorMessage"],
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        }

        if (data.body["name"] != null) {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: data.body["message"].toString(),
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        }
      });
      return null;
    } else if (data.status.hasError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isServerError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });

      return null;
    } else if (data.status.connectionError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.connectionErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isNotFound) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.isNotFoundMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else {
      return data.body;
    }
  }

  Future<dynamic> postWithoutAuthGoogleLogin(
      {Map<String, dynamic>? body, required String url}) async {
    debugPrint("Url : $url");
    debugPrint("Body : $body");
    var data = await post(url, body);
    // var data = await post(url, FormData(body!)); //This is used for passing the body as form data

    debugPrint("response : ${data.body}");
    debugPrint("response code : ${data.status.code}");

    if (data.hasError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        if (data.body["name"] != null && data.body["name"] == "OTP_ERROR") {
          if (Get.find<SignInController>().isGoogleLogin2FAEnable) {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: data.body["message"].toString(),
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          }
          Get.find<SignInController>().isGoogleLogin2FAEnable = true;
          Get.toNamed(authenticatorOtp);
          return null;
        }

        if (data.body["name"] != null) {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: data.body["message"].toString(),
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
          return null;
        }

        if (data.body["message"] != null) {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: data.body["message"].toString(),
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        }
      });
      return null;
    } else if (data.status.hasError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isServerError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });

      return null;
    } else if (data.status.connectionError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.connectionErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isNotFound) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.isNotFoundMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else {
      return data.body;
    }
  }

  Future<dynamic> postWithoutAuthAppleLogin(
      {Map<String, dynamic>? body, required String url}) async {
    debugPrint("Url : $url");
    debugPrint("Body : $body");
    var data = await post(url, body);
    // var data = await post(url, FormData(body!)); //This is used for passing the body as form data

    debugPrint("response : ${data.body}");
    debugPrint("response code : ${data.status.code}");

    if (data.hasError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        if (data.body["name"] != null && data.body["name"] == "OTP_ERROR") {
          if (Get.find<SignInController>().isGoogleLogin2FAEnable) {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: data.body["message"].toString(),
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          }
          Get.find<SignInController>().isAppleLogin2FAEnable = true;
          Get.toNamed(authenticatorOtp);
          return null;
        }

        if (data.body["name"] != null) {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: data.body["message"].toString(),
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );

          return null;
        }

        if (data.body["message"] != null) {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: data.body["message"].toString(),
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        }
      });
      return null;
    } else if (data.status.hasError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isServerError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });

      return null;
    } else if (data.status.connectionError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.connectionErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isNotFound) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.isNotFoundMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else {
      return data.body;
    }
  }

  Future<dynamic> postWithoutAuth2faVerification(
      {Map<String, dynamic>? body, required String url}) async {
    debugPrint("Url : $url");
    debugPrint("Body : $body");
    var data = await post(url, body);
    // var data = await post(url, FormData(body!)); //This is used for passing the body as form data

    debugPrint("response : ${data.body}" + " ::: Url :: " + url);
    debugPrint("response code : ${data.status.code}");

    if (data.hasError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        if (data.body["name"] != null) {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: data.body["message"].toString(),
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        }
      });
      return null;
    } else if (data.status.hasError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isServerError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });

      return null;
    } else if (data.status.connectionError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.connectionErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isNotFound) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.isNotFoundMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else {
      return data.body;
    }
  }

  Future<dynamic> postWithAuth(
      {Map<String, dynamic>? body,
      Map<String, dynamic>? bodyRaw,
      required String url}) async {
    HeaderModel headerModel =
        HeaderModel(authorization: "Bearer ${hive.get(HiveString.token)}");
    debugPrint("Header : ${"Bearer ${hive.get(HiveString.token)}"}");
    debugPrint("Url : $url");

    log("Body : ${jsonEncode(body).toString()}");

    var data = await post(url, body!, headers: headerModel.toHeader());

    log("response code : ${data.statusCode.toString()}");

    log("response:: " + data.body.toString() + " ========= " + url);

    if (data.hasError) {
      if (data.statusCode == 500) {
        Future.delayed(const Duration(milliseconds: 500), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: StringUtils.hasErrorMessage,
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });

        return null;
      } else if (data.statusCode == 401) {
        return data.statusCode;
      } else {
        if (data.body["message"] != null) {
          Future.delayed(const Duration(milliseconds: 500), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: data.body["errMessage"] ?? data.body["message"] == null
                  ? data.body["detail"]
                  : data.body["message"]
                      .toString()
                      .replaceAll('[', '')
                      .replaceAll(']', ''),
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        } else if (data.body["detail"] != null) {
          debugPrint('****');
          Future.delayed(
            const Duration(milliseconds: 0),
            () {
              FocusManager.instance.primaryFocus!.unfocus();
              Fluttertoast.showToast(
                timeInSecForIosWeb: 3,
                msg: data.body["errMessage"] ?? data.body["message"] == null
                    ? data.body["detail"]
                    : data.body["message"]
                        .toString()
                        .replaceAll('[', '')
                        .replaceAll(']', ''),
                backgroundColor: Colors.red,
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.BOTTOM,
                textColor: Colors.white,
                fontSize: 20.0,
              );
            },
          );
        } else {
          if (data.body['lead'] != null) {
            Future.delayed(const Duration(milliseconds: 500), () {
              FocusManager.instance.primaryFocus!.unfocus();
              Fluttertoast.showToast(
                timeInSecForIosWeb: 3,
                msg: data.body['lead']
                    .toString()
                    .replaceAll("[", "")
                    .replaceAll("]", ""),
                backgroundColor: Colors.red,
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.BOTTOM,
                textColor: Colors.white,
                fontSize: 20.0,
              );
            });
          } else {
            Future.delayed(const Duration(milliseconds: 500), () {
              FocusManager.instance.primaryFocus!.unfocus();
              Fluttertoast.showToast(
                timeInSecForIosWeb: 3,
                msg: StringUtils.hasErrorMessage,
                backgroundColor: Colors.red,
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.BOTTOM,
                textColor: Colors.white,
                fontSize: 20.0,
              );
            });
          }
        }
        return null;
      }
    } else if (data.status.hasError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isServerError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });

      return null;
    } else if (data.status.connectionError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.connectionErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isNotFound) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.isNotFoundMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else {
      return data.body;
    }
  }

  Future<dynamic> putFileWithAuth({
    required List<int> upload,
    required String filename,
    required String url,
    required String key,
  }) async {
    HeaderModelFile headerModel =
        HeaderModelFile(authorization: "Bearer ${hive.get(HiveString.token)}");
    final image = MultipartFile(
      upload,
      // contentType: "multipart/form-data",
      filename: filename,
    );
    debugPrint(url);
    // debugPrint(image.length.toString());
    debugPrint(filename);

    var data = await put(
        url,
        FormData({
          'upload': image,
        }),
        headers: headerModel.toHeader());
    debugPrint(data.statusCode.toString());

    debugPrint(data.body.toString());

    if (data.hasError) {
      if (data.statusCode == 401) {
        return data.statusCode;
      } else {
        Future.delayed(const Duration(milliseconds: 500), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: StringUtils.hasErrorMessage,
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
        return null;
      }
    } else if (data.status.hasError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isServerError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });

      return null;
    } else if (data.status.connectionError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.connectionErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isNotFound) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.isNotFoundMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else {
      return data.body;
    }
  }

  Future<dynamic> putWithoutAuth(
      {Map<String, dynamic>? body, required String url}) async {
    debugPrint("Url : $url");
    debugPrint("Body : $body");
    var data = await put(url, body);
    // var data = await post(url, FormData(body!)); //This is used for passing the body as form data
    debugPrint("response body: ${data.body}");
    debugPrint("response http statusCode: ${data.statusCode}");
    if (data.hasError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: data.body["message"].toString(),
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.hasError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isServerError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });

      return null;
    } else if (data.status.connectionError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.connectionErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isNotFound) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.isNotFoundMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else {
      return data.body;
    }
  }

  Future<dynamic> postWithoutAuthForLogin(
      {Map<String, dynamic>? body, required String url}) async {
    debugPrint("Url : $url");
    debugPrint("Body : $body");
    var data = await post(url, body);
    // var data = await post(url, FormData(body!)); //This is used for passing the body as form data

    debugPrint("response : ${data.body}" + " ::: Url" + url);
    debugPrint("response code : ${data.status.code}");

    if (data.hasError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        if (data.body["name"] != null) {
          debugPrint("==============");
          Get.find<SignInController>().redirectFromLogin.value = true;
          Get.toNamed(authenticatorOtp);
        } else {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: data.body["message"].toString(),
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        }
      });

      return null;
    } else if (data.status.hasError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isServerError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });

      return null;
    } else if (data.status.connectionError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.connectionErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isNotFound) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.isNotFoundMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else {
      return data.body;
    }
  }

  Future<dynamic> putWithAuth(
      {Map<String, dynamic>? body, required String url}) async {
    HeaderModel headerModel =
        HeaderModel(authorization: "Bearer ${hive.get(HiveString.token)}");
    debugPrint("Header : ${"Bearer ${hive.get(HiveString.token)}"}");
    debugPrint("Url : $url");
    debugPrint("Body : $body");
    log(hive.get(HiveString.token));
    var data = await put(url, body!, headers: headerModel.toHeader());

    debugPrint(data.statusCode.toString());
    debugPrint(data.body.toString());

    if (data.hasError) {
      if (data.statusCode == 401) {
        return data.statusCode;
      } else {
        Future.delayed(const Duration(milliseconds: 500), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: data.body["errMessage"] ?? data.body["message"] == null
                ? data.body["detail"]
                : data.body["message"]
                    .toString()
                    .replaceAll('[', '')
                    .replaceAll(']', ''),
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
        return null;
      }
    } else if (data.statusCode == 400) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });

      return null;
    } else if (data.status.isServerError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });

      return null;
    } else if (data.status.connectionError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.connectionErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isNotFound) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.isNotFoundMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else {
      return data.body;
    }
  }

  Future<dynamic> putWithoutAuthMobileVerification(
      {Map<String, dynamic>? body, required String url}) async {
    debugPrint("Url : $url");
    debugPrint("Body : $body");
    var data = await put(url, body!);

    debugPrint(data.statusCode.toString());
    debugPrint(data.body.toString());

    if (data.hasError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: data.body["errMessage"] ?? data.body["message"],
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );

        if (data.body["message"] != null &&
            data.body["message"]
                .toString()
                .contains("Account has been deleted")) {
          Get.offAndToNamed(signUp);
        }
      });
      return null;
    } else if (data.statusCode == 400) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: data.body["name"],
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isServerError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });

      return null;
    } else if (data.status.connectionError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.connectionErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isNotFound) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.isNotFoundMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else {
      return data.body;
    }
  }

  Future<dynamic> deleteWithAuth(
      {Map<String, dynamic>? body, required String url}) async {
    HeaderModel headerModel =
        HeaderModel(authorization: "Bearer ${hive.get(HiveString.token)}");
    log("Header : ${"Bearer ${hive.get(HiveString.token)}"}");
    debugPrint("Url : $url");
    var data = await delete(url, headers: headerModel.toHeader());
    debugPrint(data.statusCode.toString());
    debugPrint(data.body.toString());

    if (data.hasError) {
      if (data.statusCode == 401) {
        return data.statusCode;
      } else if (data.statusCode == 403) {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: data.body["message"] == null
              ? data.body["detail"] == null
                  ? data.body['errMessage'].toString()
                  : data.body["detail"].toString()
              : data.body["message"].toString(),
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      } else {
        Future.delayed(const Duration(milliseconds: 500), () {
          if (data.body["errMessage"] != null) {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: data.body["errMessage"].toString(),
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          } else {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: data.body["message"].toString(),
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          }
        });
        return null;
      }
    } else if (data.statusCode == 204) {
      return data.statusCode;
    } else if (data.statusCode == 400) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isServerError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });

      return null;
    } else if (data.status.connectionError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.connectionErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isNotFound) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.isNotFoundMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else {
      return data.body;
    }
  }

  Future<dynamic> patchWithAuthImageDelete(
      {Map<String, dynamic>? body, required String url}) async {
    HeaderModel headerModel =
        HeaderModel(authorization: "Bearer ${hive.get(HiveString.token)}");
    debugPrint("Header : ${"Bearer ${hive.get(HiveString.token)}"}");
    debugPrint("Url : $url");
    debugPrint("Body : $body");
    var data = await patch(url, null, headers: headerModel.toHeader());

    debugPrint(data.statusCode.toString());
    if (data.hasError) {
      if (data.statusCode == 401) {
        return data.statusCode;
      } else {
        Future.delayed(const Duration(milliseconds: 500), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: data.body["errMessage"].toString(),
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
        return null;
      }
    } else if (data.statusCode == 400) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isServerError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });

      return null;
    } else if (data.status.connectionError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.connectionErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isNotFound) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.isNotFoundMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else {
      return data.body;
    }
  }

  Future<dynamic> postWithAuthRaw(
      {Map<String, dynamic>? body,
      Map<String, dynamic>? bodyRaw,
      required String url}) async {
    HeaderModel headerModel =
        HeaderModel(authorization: "Bearer ${hive.get(HiveString.token)}");
    debugPrint("Header : ${"Bearer ${hive.get(HiveString.token)}"}");
    debugPrint("Url : $url");
    debugPrint("Body : $body");
    var data = await post(url, body!, headers: headerModel.toHeader());

    debugPrint("response code : ${data.statusCode.toString()}");

    debugPrint(data.body.toString());

    if (data.hasError) {
      if (data.statusCode == 401) {
        return data.statusCode;
      } else {
        Future.delayed(const Duration(milliseconds: 500), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: StringUtils.hasErrorMessage,
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
        return null;
      }
    } else if (data.status.hasError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isServerError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });

      return null;
    } else if (data.status.connectionError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.connectionErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isNotFound) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.isNotFoundMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else {
      return data.body;
    }
  }

  Future<dynamic> putWithAuthNoBody({
    required String url,
  }) async {
    HeaderModel headerModel =
        HeaderModel(authorization: "Bearer ${hive.get(HiveString.token)}");
    debugPrint("Header : ${"Bearer ${hive.get(HiveString.token)}"}");
    debugPrint("Url : $url");
    var data = await put(url, null, headers: headerModel.toHeader());

    debugPrint(data.statusCode.toString());
    debugPrint(data.body.toString());

    if (data.hasError) {
      if (data.statusCode == 401) {
        return data.statusCode;
      } else {
        Future.delayed(const Duration(milliseconds: 500), () {
          FocusManager.instance.primaryFocus!.unfocus();
          Fluttertoast.showToast(
            timeInSecForIosWeb: 3,
            msg: data.body["errMessage"] ?? data.body["message"],
            backgroundColor: Colors.red,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            fontSize: 20.0,
          );
        });
        return null;
      }
    } else if (data.statusCode == 400) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isServerError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });

      return null;
    } else if (data.status.connectionError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.connectionErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isNotFound) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.isNotFoundMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else {
      return data.body;
    }
  }

  Future<dynamic> postWithAuthForCalculators(
      {Map<String, dynamic>? body,
      Map<String, dynamic>? bodyRaw,
      required String url}) async {
    HeaderModel headerModel =
        HeaderModel(authorization: "Bearer ${hive.get(HiveString.token)}");
    debugPrint("Header : ${"Bearer ${hive.get(HiveString.token)}"}");
    debugPrint("Url : $url");
    debugPrint("Body : $body");
    var data = await post(url, body!, headers: headerModel.toHeader());

    debugPrint("response code : ${data.statusCode.toString()}");

    debugPrint("response:: " + data.body.toString());
    debugPrint(
        data.body['calculator_response'].toString().replaceAll('{,},[,]', ''));

    if (data.hasError) {
      if (data.statusCode == 401) {
        return data.statusCode;
      } else {
        if (data.body["message"] != null) {
          Future.delayed(const Duration(milliseconds: 500), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: data.body["calculator_response"].toString(),
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        } else {
          Future.delayed(const Duration(milliseconds: 500), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: data.body["calculator_response"]
                  .toString()
                  .replaceAll("{", '')
                  .replaceAll('}', '')
                  .replaceAll('[', '')
                  .replaceAll(']', ''),
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }
        return null;
      }
    } else if (data.status.hasError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isServerError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });

      return null;
    } else if (data.status.connectionError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.connectionErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isNotFound) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.isNotFoundMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else {
      return data.body;
    }
  }

  //====================document drawer api call================

  Future<dynamic> postFileWithAuthDocumentDrawer({
    required List<int> upload,
    required String filename,
    required String url,
    required String id,
    required String nameField,
    required String doc_type,
  }) async {
    HeaderModelFile headerModel =
        HeaderModelFile(authorization: "Bearer ${hive.get(HiveString.token)}");
    debugPrint("Header : ${"Bearer ${hive.get(HiveString.token)}"}");
    debugPrint("Url : $url");

    final image = MultipartFile(
      upload,
      //contentType: "multipart/form-data",
      filename: filename,
    );
    debugPrint(url);
    // debugPrint(image.length.toString());
    debugPrint(filename);
    debugPrint(id);
    debugPrint(nameField);
    debugPrint(doc_type);

    var data = await post(
        url,
        FormData({
          'file': image,
          'filename': nameField,
          'drawer': id,
          'doc_type': doc_type,
        }),
        headers: headerModel.toHeader());

    debugPrint(data.statusCode.toString());

    debugPrint(data.body.toString());

    if (data.hasError) {
      if (data.statusCode == 401) {
        return data.statusCode;
      } else {
        if (data.body['message'] != null) {
          Future.delayed(const Duration(milliseconds: 500), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: data.body["message"].toString(),
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        } else {
          Future.delayed(const Duration(milliseconds: 500), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: StringUtils.hasErrorMessage,
              backgroundColor: Colors.red,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        }
        return null;
      }
    } else if (data.status.hasError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isServerError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.hasErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });

      return null;
    } else if (data.status.connectionError) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.connectionErrorMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else if (data.status.isNotFound) {
      Future.delayed(const Duration(milliseconds: 500), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: StringUtils.isNotFoundMessage,
          backgroundColor: Colors.red,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      return null;
    } else {
      return data.body;
    }
  }

  Future<dynamic> getWithAuthV2(
      {required String url, Map<String, dynamic>? query}) async {
    HeaderModel headerModel =
        HeaderModel(authorization: "Bearer ${hive.get(HiveString.token)}");

    //debugPrint("Headerv2 : ${"Bearer ${hive.get(HiveString.token)}"}", wrapWidth: 2000);
    log("Bearer ${hive.get(HiveString.token)}");
    debugPrint("Url Bank: $url");
    debugPrint("query: $query");
    var data = await get(url, headers: headerModel.toHeader(), query: query);
    log("Body : ${data.body} ::: " + url);
    if (data.isOk) {
      return data.body;
    } else {
      if (data.statusCode == 401) {
        return data.statusCode;
      } else {
        Logger().e(data.body);
        return null;
      }
    }
  }

  Future<bool> getNewAccessTokenWithRefreshToken() async {
    var value = await CoreService().postWithAuth(
      url: baseUrl + getAccessTokenUrl,
      body: {
        "refresh_token": hive.get(HiveString.refreshToken),
      },
    );
    if (value != null) {
      //Get.back();
      await hive.put(
          HiveString.token, value['access_token']); //set new accessToken
      await hive.put(HiveString.refreshToken,
          value['refresh_token']); // Set new refreshToken
      return true;
    } else {
      Get.offAllNamed(loginScreen);
      return false;
    }
  }
}
