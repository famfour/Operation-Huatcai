// ignore_for_file: file_names, unnecessary_null_comparison

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:iqrate/Controller/bank_submission_document_drawer_screen_controller.dart';
import 'package:iqrate/Controller/buc_mortgage_repayment_controller.dart';
import 'package:iqrate/Controller/calculator_mortgage_repayment_controller.dart';
import 'package:iqrate/Controller/leads_view_controller.dart';
import 'package:iqrate/Controller/login_controller.dart';
import 'package:iqrate/Controller/mortgage_repayment_report_controller.dart';
import 'package:iqrate/Controller/profile_controller.dart';
import 'package:iqrate/Controller/rates_view_controller.dart';
import 'package:iqrate/Controller/selected_packages_controller.dart';
import 'package:iqrate/DeviceManager/hive_string.dart';
import 'package:iqrate/Model/response_model.dart/login_response_model.dart';
import 'package:iqrate/Service/url.dart';
import 'package:iqrate/Utils/AlertPopupOneButton.dart';
import 'package:iqrate/Controller/buc_mortgage_progressive_repayment_table_screen_controller.dart';
import 'package:iqrate/Controller/buc_mortgage_repayment_compare_controller.dart';
import 'package:iqrate/Views/initial_screen.dart';
import '../Controller/bank_submission_controller.dart';
import '../Controller/email_log_controller.dart';
import '../Controller/generate_packages_controller.dart';
import '../Controller/law_firm_submission_controller.dart';
import '../Controller/lead_profile_controller.dart';
import '../Controller/loan_details_controller.dart';
import '../Controller/mobile_verification_controller.dart';
import '../Controller/property_details_controller.dart';
import '../Controller/submit_for_payout_controller.dart';
import '../Controller/buc_mortgage_repayment_report_controller.dart';
import '../Views/co_broke/co_broke_submit.dart';

class GetControllers {
  static final GetControllers _singleton = GetControllers._internal();
  final Box hive = Hive.box(HiveString.hiveName);
  factory GetControllers() {
    return _singleton;
  }

  GetControllers._internal();

  static GetControllers get shared => _singleton;

  SignInController getSignInController() {
    if (!Get.isRegistered<SignInController>()) {
      Get.put(SignInController());
    }
    return Get.find<SignInController>();
  }

  ProfileScreenController getProfileScreenController() {
    if (!Get.isRegistered<ProfileScreenController>()) {
      Get.put(ProfileScreenController());
    }
    return Get.find<ProfileScreenController>();
  }

  //mobileVerificationController
  MobileVerificationController getMobileVerificationController() {
    if (!Get.isRegistered<MobileVerificationController>()) {
      Get.put(MobileVerificationController());
    }
    return Get.find<MobileVerificationController>();
  }

  RatesViewController getRatesViewController() {
    if (!Get.isRegistered<RatesViewController>()) {
      Get.put(RatesViewController());
    }
    return Get.find<RatesViewController>();
  }

//LoanDetailsController
  LoanDetailsController getLoanDetailsController() {
    if (!Get.isRegistered<LoanDetailsController>()) {
      Get.put(LoanDetailsController());
    }
    return Get.find<LoanDetailsController>();
  }

//LeadProfileController
  LeadProfileController getLeadProfileController() {
    if (!Get.isRegistered<LeadProfileController>()) {
      Get.put(LeadProfileController());
    }
    return Get.find<LeadProfileController>();
  }

  //SubmitForPayoutViewController
  SubmitForPayoutViewController getSubmitForPayoutViewController() {
    if (!Get.isRegistered<SubmitForPayoutViewController>()) {
      Get.put(SubmitForPayoutViewController());
    }
    return Get.find<SubmitForPayoutViewController>();
  }

  //PropertyDetailsController
  PropertyDetailsController getPropertyDetailsController() {
    if (!Get.isRegistered<PropertyDetailsController>()) {
      Get.put(PropertyDetailsController());
    }
    return Get.find<PropertyDetailsController>();
  }

  //GeneratePackagesController
  GeneratePackagesController getGeneratePackagesController() {
    if (!Get.isRegistered<GeneratePackagesController>()) {
      Get.put(GeneratePackagesController());
    }
    return Get.find<GeneratePackagesController>();
  }

  //GeneratePackagesController
  SelectedPackagesController getSelectedPackagesController() {
    if (!Get.isRegistered<SelectedPackagesController>()) {
      Get.put(SelectedPackagesController());
    }
    return Get.find<SelectedPackagesController>();
  }

  //LeadsViewController
  LeadsViewController getLeadsViewController() {
    if (!Get.isRegistered<LeadsViewController>()) {
      Get.put(LeadsViewController());
    }
    return Get.find<LeadsViewController>();
  }

  //BankSubmissionController
  BankSubmissionController getBankSubmissionController() {
    if (!Get.isRegistered<BankSubmissionController>()) {
      Get.put(BankSubmissionController());
    }
    return Get.find<BankSubmissionController>();
  }

  BankSubmissionDocumentDrawerScreenController getBankSubmissionDocumentDrawerScreenController() {
    if (!Get.isRegistered<BankSubmissionDocumentDrawerScreenController>()) {
      Get.put(BankSubmissionDocumentDrawerScreenController());
    }
    return Get.find<BankSubmissionDocumentDrawerScreenController>();
  }

  //LawFirmSubmissionController
  LawFirmSubmissionController getLawFirmSubmissionController() {
    if (!Get.isRegistered<LawFirmSubmissionController>()) {
      Get.put(LawFirmSubmissionController());
    }
    return Get.find<LawFirmSubmissionController>();
  }

  //EmailLogController
  EmailLogController getEmailLogController() {
    if (!Get.isRegistered<EmailLogController>()) {
      Get.put(EmailLogController());
    }
    return Get.find<EmailLogController>();
  }

  //CalculatorMortgageRepaymentController
  MortgageRepaymentReportController getMortgageRepaymentReportController() {
    if (!Get.isRegistered<MortgageRepaymentReportController>()) {
      Get.put(MortgageRepaymentReportController());
    }
    return Get.find<MortgageRepaymentReportController>();
  }

  //CalculatorMortgageRepaymentController
  CalculatorMortgageRepaymentController
      getCalculatorMortgageRepaymentController() {
    if (!Get.isRegistered<CalculatorMortgageRepaymentController>()) {
      Get.put(CalculatorMortgageRepaymentController());
    }
    return Get.find<CalculatorMortgageRepaymentController>();
  }

  //BucMortgageRepaymentController
  BucMortgageRepaymentController getBucMortgageRepaymentController() {
    if (!Get.isRegistered<BucMortgageRepaymentController>()) {
      Get.put(BucMortgageRepaymentController());
    }
    return Get.find<BucMortgageRepaymentController>();
  }

  //BucMortgageRepaymentReportController
  BucMortgageRepaymentReportController
      getBucMortgageRepaymentReportController() {
    if (!Get.isRegistered<BucMortgageRepaymentReportController>()) {
      Get.put(BucMortgageRepaymentReportController());
    }
    return Get.find<BucMortgageRepaymentReportController>();
  }

  //BucMortgageRepaymentCompareController
  BucMortgageRepaymentCompareController
      getBucMortgageRepaymentCompareController() {
    if (!Get.isRegistered<BucMortgageRepaymentCompareController>()) {
      Get.put(BucMortgageRepaymentCompareController());
    }
    return Get.find<BucMortgageRepaymentCompareController>();
  }

  //BucMortgageProgressiveRepaymentTableScreenController
  BucMortgageProgressiveRepaymentTableScreenController
      getBucMortgageProgressiveRepaymentTableScreenController() {
    if (!Get.isRegistered<
        BucMortgageProgressiveRepaymentTableScreenController>()) {
      Get.put(BucMortgageProgressiveRepaymentTableScreenController());
    }
    return Get.find<BucMortgageProgressiveRepaymentTableScreenController>();
  }

  //SubmitCoBrokeController
  SubmitCoBrokeController getSubmitCoBrokeController() {
    if (!Get.isRegistered<SubmitCoBrokeController>()) {
      Get.put(SubmitCoBrokeController());
    }
    return Get.find<SubmitCoBrokeController>();
  }

//showlading
  showLoading() {
    Get.dialog(
        const Center(
          child: CircularProgressIndicator(),
        ),
        barrierDismissible: false);
  }

//hideLoading
  hideLoading() {
    if (Get.isDialogOpen ?? false) Get.back();
  }

  String getErrorMessage(dynamic data) {
    // data["message"]
    // if (data["message"] is List) {
    //   return data["message"].toString();
    // }
    return data["message"].toString();
  }

  //getToken

  getToken() {
    var token = hive.get(HiveString.token);
    return token;
  }

  Future<bool> checkRefreshToken() async {
    //
    if (isTokenNeedRefresh()) {
      return await callAPIRefreshToken(); //true/ false
    } else {
      debugPrint("token is OK, no need refresh");
      return true; //true
    }
  }

  // isTokenNeedRefresh(){
  //   _isTokenNeedRefresh();
  // }

  isTokenNeedRefresh() {
    var currentTime = DateTime.now();
    var expireDateBack = hive.get("expireDate"); //fake data
    // var currentTime = convertDateFromString('2022-06-28 09:59:00');//fake data
    debugPrint(currentTime.toString());
    debugPrint(expireDateBack.toString());
    debugPrint("start compare");
    debugPrint(
        "remain in minutes: ${expireDateBack.difference(currentTime).inMinutes.toString()}");
    bool needRefresh = expireDateBack.difference(currentTime).inMinutes <
        180; //check if expire in 3hrs then refresh it
    debugPrint("needRefresh $needRefresh");
    return needRefresh;
  }

  Future saveToken(LoginResponseModel model) async {
    debugPrint("saveToken ");
    //real
    var currentTime = DateTime.now();
    var expireDate = currentTime.add(Duration(seconds: model.expiresIn!));
    //fake
    // var formatterDate = DateFormat('dd/MM/yy HH:mm');
    // var currentTime = convertDateFromString('2022-06-28 00:00:00');
    // var expireDate = currentTime.add(Duration(seconds: expireTime));
    await hive.put("expireDate", expireDate);
    debugPrint(currentTime.toString());
    debugPrint(expireDate.toString());
    await hive.put(HiveString.token, model.accessToken!);
    await hive.put(HiveString.refreshToken, model.refreshToken!);
    debugPrint("done saveToken ");
  }

  convertDateFromString(String strDate) {
    DateTime todayDate = DateTime.parse(strDate);
    return todayDate;
  }

  Future<bool> callAPIRefreshToken() async {
    debugPrint("start callAPIRefreshToken ");
    showLoading();
    var dio = Dio();
    dio.options.headers["Authorization"] =
        "Bearer ${hive.get(HiveString.token)}";
    var data = {"refresh_token": hive.get(HiveString.refreshToken)};
    var endpoint = "user/userRefreshToken";
    debugPrint("Url : $endpoint");
    try {
      var response = await dio.post(baseUrl + endpoint, data: data);
      var jsonBody = response.data;
      if (jsonBody != null) {
        var model = LoginResponseModel.fromJson(jsonBody);
        await saveToken(model);
        hideLoading();
        return true;
      } else {
        hideLoading();
        debugPrint("callAPIRefreshToken something went wrong}");

        return false;
      }
      // Logger().d(model.toJson());
    } catch (e) {
      debugPrint("callAPIRefreshToken ${e.toString()}");
      hideLoading();
      return false;
    }
  }

  showAlertPopupOneButtonWithCallBack(
      {String title = 'Error',
      String content = "Something went wrong!",
      required Function() callback}) {
    Get.dialog(
        AlertPopupOneButton(
            title: title, content: content, callbackAfterPressOK: callback),
        barrierDismissible: false);
  }

  fakeExpireDateMakeNeedRefresh() async {
    // final Box hive = Hive.box(HiveString.hiveName);
    var currentTime = DateTime.now();
    var expireDate = currentTime.add(const Duration(minutes: 15));
    await hive.put("expireDate", expireDate);
    debugPrint(currentTime.toString());
    debugPrint(expireDate.toString());
    // needRefresh = GetControllers.shared.isTokenNeedRefresh();
  }

  fakeTokenError() async {
    // final Box hive = Hive.box(HiveString.hiveName);
    await hive.put(HiveString.token, "hahaha");
    // needRefresh = GetControllers.shared.isTokenNeedRefresh();
  }

  changeRootViewToInit() {
    Get.offAll(() => const InitialScreen());
  }

  showAlertPopupOneButtonWithCallback(BuildContext context,
      {String title = 'Error',
      String content = '',
      required Function callback}) {
    showDialog(
        context: context,
        builder: (context) {
          return CupertinoAlertDialog(
            title: Text(title),
            content: Text(content),
            actions: [
              CupertinoDialogAction(
                isDefaultAction: true,
                child: const Text("OK"),
                onPressed: () {
                  Navigator.of(context).pop();
                  callback();
                },
              ),
            ],
          );
        });
  }

  //VALIDATE
  String? validateForNumberIntMaxis30(String? value) {
    if (value != null) {
      if (value.isEmpty) {
        // return 'Required!';
        return null;
      } else if (int.parse(value) < 1 || int.parse(value) > 35) {
        return 'Value need in range 1 - 35';
      }
    }
    return null;
  }

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return double.tryParse(s) != null;
  }

  //allow 0.1-> 100
  String? yearRateRequired(String? value) {
    if (value != null) {
      if (value.isEmpty) {
        return 'Required!';
        // return null;
      }
      if (isNumeric(value)) {
        if (double.parse(value) <= 0 || double.parse(value) > 100) {
          return 'The value should be between 0.1 and 100';
        }
      } else {
        return 'Invalid number!';
      }
    }
    return null;
  }

  String? validateRateValueRequired(String? value) {
    if (value != null) {
      if (value.isEmpty) {
        return 'Required!';
        // return null;
      }
      if (isNumeric(value)) {
        if (double.parse(value) <= 0 || double.parse(value) > 100) {
          return 'The value should be between 0.1 and 100';
        }
      } else {
        return 'Invalid number!';
      }
    }
    return null;
  }

  //
  // String? validateRequired(String? value){
  //   if (value == null) {
  //     return 'Required!';
  //   } else {
  //     return null;
  //   }
  // }
}
