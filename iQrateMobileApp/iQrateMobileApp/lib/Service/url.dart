// ignore_for_file: constant_identifier_names

// const String baseUrl = "https://dev-api.iqrate.io/";
// const String strapiUrl = "https://iqrate-strapi.dprizm.dev/";
// const String baseUrlIp = "http://13.229.247.176/";

//*-------------------------On Prod--------------------------
const String strapiUrl = "https://strapi.iqrate.io/";
const String baseUrl = "https://api.iqrate.io/";
//*---------------------------------------------------------

//*----------------------------------OnBoarding API-------------
const String loginUrl = "auth/login";
const String registerUrl = "auth/register";
const String emailVerificationUrl = "auth/verifyEmailCode/";
const String resendEmailCodeUrl = "auth/resendEmailCode/";
const String mobileVerificationUrl = "auth/verifySmsCode/";
const String resendSmsCodeUrl = "auth/resendSmsCode/";
const String sendResetPasswordUrl = "auth/sendResetPassword/";
const String updatePasswordUrl = "auth/updatePassword/";
const String registerWithGoogleUrl = "auth/gmailRegister";
const String signInWithGoogleUrl = "auth";
const String getAccessTokenUrl = 'auth/getRefreshToken';
//*----------------------------------OnBoarding API------------
//!___________________________________________________________________________

//*----------------------------------Profile API--------------
const String getUserProfileUrl = "user";
const String changePasswordUrl = "user/changePassword";
const String resendEmailCodeLoginUrl = "auth/resendEmailCode/email";
const String changeEmailUrl = "user/changeEmail";
const String googleAuth = "auth/google_verify";
const String appleAuth = "auth/apple_verify";
const String changeMobileUrl = "user/changeMobile";
const String uploadImageUrl = "user/image";
const String updateImageUrl = "user/image/update";
const String updateNameDobGoogle = 'user/updateNameDOB';
const String deleteImageUrl = "user/image/deletePhoto";
const String deleteMyAccountUrl = "user/deleteUser";
const String updateNameAndDobUrl = 'user/updateNamePhone';
const String updateBankAccInfo = 'user/updateBankdetail';
const String checkEmailVerifiedUrl = 'auth/checkEmailStatus/';
const String logoutUrl = 'auth/logout';
const String twoFAonUrl = 'auth/turn-on';
const String twoMfaUrl = 'user/enable_mfa';
const String verifyWithCodeUrl = 'auth/verifyWithCode/';
const String check2FAstatusUrl = 'user/chk2FAstatus/';
const String disable2FAUrl = 'user/disable2FA';
const String deleteBankAccountUrl = 'user/deleteBankdetail';
const String updateReferalCodeUrl = 'auth/updateReferalCode/';
//*------------------------Profile API--------------------
//!___________________________________________________________________________

//*------------------------Subscriptions API---------------
const String getsubscriptionPlanDetails = 'user/subscription/products';
const String registerWithStripeUrl = 'user/subscription/customer';
const String createSubscriptionUrl = 'user/subscription/create';
const String createPaymentMethodUrl = 'user/subscription/payment-method/';
const String updatePaymentMethodUrl =
    'user/subscription/update-payment-method/';
const String getDefaultPaymentMethodUrl = 'user/subscription/customer/';
const String savedPaymentMethodUrl = 'user/subscription/payment-methods';
const String setPaymentMethodUrl = 'user/subscription/set-payment-method';
const String removePaymentUrl = 'user/subscription/remove-payment-method';
const String getSubscriptionDetailsUrl = 'user/subscription';
const String upgradeDowngradeUrl = 'user/subscription/upgrade-downgrade/';
const String detachPaymentMethod = 'user/subscription/remove-payment-method';
//*------------------------Subscriptions API------------------
//!___________________________________________________________________________

//*------------------------CMS API------------------
const String faqUrl = 'api/faqs';
const String searchFaqUrl = 'api/faqs/search?title=';
const String trainingCatagoryUrl = 'api/trainings/search?training_category=';
const String trainingCatagorySearchUrl = 'api/trainings/search?title=';
const String resourcesCategoryUrl = 'api/resources/search?resource_category=';
const String resourcesBankFormsUrls =
    'customer/bank/banks?page=1&page_size=500';
const String lawFirmsListUrls = 'customer/lawfirm/all';
//*------------------------CMS API------------------
//!___________________________________________________________________________

//*------------------------Leads API------------------
const String leadCreateUrl = 'customer/lead/create';
const String leadListUrl = 'customer/lead/all';
const String oneLeadUrl = 'customer/lead/';
const String customerLeadCreateUrl = 'customer/lead/clients/create';
const String getKvValuesUrl = 'customer/kvstore/all';
const String getInvoicesUrl = 'user/subscription/invoices/';
const String leadClientCreateUrl = 'customer/lead/clients/create';
const String pdpaTokenGenerateUrl = 'customer/lead/client/token/';
const String validatePdpaUrl = 'magic/validate_pdpa';
const String getLeadClientUrl = 'customer/lead/client/';
const String updateLeadClientUrl = getLeadClientUrl;
const String deleteLeadClientUrl = getLeadClientUrl;
const String createPropertyUrl = 'customer/lead/property/create';
const String updatePropertyUrl = 'customer/lead/property/';
const String loanDetailsUrl = 'customer/lead/';
const String getBanksUrl = 'customer/bank/banks';
const String createLoadDetailUrl = 'customer/lead/loan-details/create';
const String contactUsUrl = 'user/contactUs';
const String verifyPdpaUrl = 'customer/validate_pdpa';
const String drawerLoginWithOTPUrl = 'customer/drawer/';
const String drawerDeleteFileUrl = 'customer/drawer/delete-file/';
const String drawerDownloadFileUrl = 'customer/drawer/download-file/';
const String drawerUpdateFileUrl = 'customer/drawer/update-file/';
const String drawerUploadFileUrl = 'customer/drawer/upload';
const String drawerGetOtpUrl = 'customer/drawer/get-otp';
const String drawerValidOtpUrl = 'customer/drawer/validate-otp';
const String addressWithPostalCodeUrl =
    'https://developers.onemap.sg/commonapi/search?getAddrDetails=Y&returnGeom=N&pageNum=1&searchVal=';
const String getLowestPackagesApi = 'customer/lead/packages/generate/';
const String allRatesApi = 'customer/lead/packages/view-more/';
const String moreRatesApi = 'customer/lead/packages/view-more/';
const String generatePackagesApi = 'customer/lead/leadpackage/create';

//Bank submission
const String banksubmissionGetBanksUrl = 'customer/submission/banks/';
const String customerBankPackages = 'customer/bank/packages';
const String bankPackagesExportToPdf = 'customer/bank/packages/export-to-pdf';
const String emailToLeadEmailBodyUrl = 'customer/submission/email-lead/';
const String emailToBankerEmailBodyUrl = 'customer/submission/email-bank/';
const String bankSubmissionGetLawfirmsUrl = 'customer/submission/lawfirms/';

//Selected packages
const String selectedPackagesUrl = 'customer/lead/leadpackages/';
const String leadPackageDeleteUrl = 'customer/lead/leadpackage/delete/';

//Co-broke
const String requestForCoBrokeUrl = 'customer/lead/request-co-broke/';

//email log
const String emailLogUrl = "customer/submission/email-logs/";

//payout
const String bankersUrl = "customer/bank/bankers?bank=";
const String createPayoutUrl = "customer/payout/create";
const String detailsPayoutUrl = "customer/payout/details/";
const String updatePayoutUrl = "customer/payout/update/";
const String lawFirmPayoutUrl = "customer/payout/lawfirms/";
const String emailLawFirmPayoutUrl = "customer/payout/email-lawfirm/";

//*------------------------Leads API------------------
//!___________________________________________________________________________

//*----------------------------------Calculators API------------------
//new purchase calculator
const String newPurchaseCalcApi = 'user/api/calculator/new-purchase';
const String newPurchaseTotalApi = 'user/api/calculator/new-purchase-total';
const String newPurchaseReportApi = 'user/api/calculator/new-purchase-report';
const String newPurchaseExportReportApi =
    'user/api/calculator/new-purchase-report-pdf';
const String newPurchaseGetPledgeAmountApi =
    "user/api/calculator/get-pledge-amount";
const String newPurchaseGetunPledgeAmountApi =
    "user/api/calculator/get-unpledge-amount";

//Refinance calculator
const String refinanceTotalUrl = 'user/api/calculator/refinance-total';
const String refinanceUrl = 'user/api/calculator/refinance';
const String refinanceReportUrl = 'user/api/calculator/refinance-report';
const String refinanceExportReportUrl = 'user/api/calculator/refinance-pdf';
const String refinanceNewLeadStep2CalculatorUrl =
    'customer/bank/packages/calculator';

//Equity Loan Calculator
const String equityCalculatorUrl = 'user/api/calculator/equity-loan';

//Mortgage Calculator
const String mortgageCalculatorUrl = 'user/api/calculator/mortgage-repayment';

//BUC Mortgage Calculator
const String calculator_buc_mortgage_downpayment =
    'user/api/calculator/buc-mortgage-downpayment';
const String calculator_buc_mortgage_report =
    'user/api/calculator/buc-mortgage-report';

//Seller Stamp Duty
const String sellerStampDutyUrl = 'user/api/calculator/seller-stamp-duty';

//Buyer Stamp Duty
const String buyerStampDutyUrl = 'user/api/calculator/buyer-stamp-duty';
//*----------------------------------Calculators Api--------------------
//!___________________________________________________________________________

