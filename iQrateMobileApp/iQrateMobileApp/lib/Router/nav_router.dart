import 'package:iqrate/Views/Refinance/Existing%20Lead/generated_packages_refinance_existing_lead.dart';
import 'package:iqrate/Views/Refinance/Existing%20Lead/mortgage_repayment_table_refinance_existing_lead_mlt.dart';
import 'package:iqrate/Views/Refinance/Existing%20Lead/refinance_for_existing_lead.dart';
import 'package:iqrate/Views/Refinance/Existing%20Lead/refinance_for_existing_lead_screen.dart';
import 'package:iqrate/Views/Refinance/Existing%20Lead/savings_from_refinancing_existing_lead.dart';
import 'package:iqrate/Views/Refinance/New%20Lead/generated_packages_refinance_new_lead.dart';
import 'package:iqrate/Views/Refinance/New%20Lead/savings_from_refinance_new_lead.dart';
import 'package:iqrate/Views/account_locked_view.dart';
import 'package:iqrate/Views/add_new_card_screen.dart';
import 'package:iqrate/Views/bank_submission_banker_email_success_screen.dart';
import 'package:iqrate/Views/bank_submission_document_compression_screen.dart';
import 'package:iqrate/Views/bank_submission_document_drawer_screen.dart';
import 'package:iqrate/Views/bank_submission_email_banker_screen.dart';
import 'package:iqrate/Views/bank_submission_email_screen.dart';
import 'package:iqrate/Views/bank_submission_one_bank_task_screen.dart';
import 'package:iqrate/Views/bank_submission_success_screen.dart';
import 'package:iqrate/Views/buc_calculator/buc_mortgage_repayment_screen.dart';
import 'package:iqrate/Views/buyer_stamp_duty_calculator_selectnumberofapplicants.dart';
import 'package:iqrate/Views/buyer_stamp_duty_calculator_view.dart';
import 'package:iqrate/Views/contact_us_view.dart';
import 'package:iqrate/Views/dashboard.dart';
import 'package:iqrate/Views/document_mobile_verification.dart';
import 'package:iqrate/Views/document_send_otp_screen.dart';
import 'package:iqrate/Views/edit_new_card_screen.dart';
import 'package:iqrate/Views/email_sent_notify_screen.dart';
import 'package:iqrate/Views/enable_2fa_view.dart';
import 'package:iqrate/Views/equity_calculator_view.dart';
import 'package:iqrate/Views/faqs_view.dart';
import 'package:iqrate/Views/lead_list_screen.dart';
import 'package:iqrate/Views/loan_package_screen.dart';
import 'package:iqrate/Views/manage_loan_screen.dart';
import 'package:iqrate/Views/more_rates_view.dart';
import 'package:iqrate/Views/my_documents_screen.dart';
import 'package:iqrate/Views/new_purchase/existing_lead/new_purchase_existing_lead.dart';
import 'package:iqrate/Views/new_purchase/existing_lead/new_purchase_existing_lead_mlt_loan_package_view.dart';
import 'package:iqrate/Views/new_purchase/existing_lead/new_purchase_existing_lead_mlt_mortgage_repayment_view.dart';
import 'package:iqrate/Views/new_purchase/existing_lead/new_purchase_for_existing_lead_screen.dart';
import 'package:iqrate/Views/new_purchase/new_lead/new_purchase_new_lead_calculator.dart';
import 'package:iqrate/Views/new_purchase/new_lead/new_purchase_new_lead_mlt_loan_package_view.dart';
import 'package:iqrate/Views/new_purchase/new_lead/new_purchase_new_lead_mlt_mortgage_repayment_view.dart';
import 'package:iqrate/Views/new_purchase/new_lead/new_purchase_new_lead_plt_loan_package.dart';
import 'package:iqrate/Views/new_purchase/new_lead/new_purchase_new_lead_plt_mortgage_repayment.dart';
import 'package:iqrate/Views/new_purchase/new_purchase_screen.dart';
import 'package:iqrate/Views/notifications_view.dart';
import 'package:iqrate/Views/payment_details_view.dart';
import 'package:iqrate/Views/payment_history_view.dart';
import 'package:iqrate/Views/pricing_table_screen.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Views/authenticator_otp.dart';
import 'package:iqrate/Views/email_address_verification_screen.dart';
import 'package:iqrate/Views/email_verification.dart';
import 'package:iqrate/Views/forget_password_screen.dart';
import 'package:iqrate/Views/initial_screen.dart';
import 'package:iqrate/Views/intro.dart';
import 'package:iqrate/Views/login_screen.dart';
import 'package:iqrate/Views/mobile_verification.dart';
import 'package:iqrate/Views/new_password_screen.dart';
import 'package:iqrate/Views/profile_screen.dart';
import 'package:iqrate/Views/qrcode_scanner.dart';
import 'package:iqrate/Views/rates_view.dart';
import 'package:iqrate/Views/Refinance/New%20Lead/refinance_savings_for_new_lead_calc_screen.dart';
import 'package:iqrate/Views/Refinance/New%20Lead/refinance_savings_for_new_lead_screen.dart';
import 'package:iqrate/Views/Refinance/refinance_savings_screen.dart';
import 'package:iqrate/Views/resources/bank_forms_view.dart';
import 'package:iqrate/Views/resources/important_links_view.dart';
import 'package:iqrate/Views/resources/law_firm_listing_details_view.dart';
import 'package:iqrate/Views/resources/law_firm_listing_view.dart';
import 'package:iqrate/Views/resources/others_view.dart';
import 'package:iqrate/Views/resources_views.dart';
import 'package:iqrate/Views/saved_cards_view.dart';
import 'package:iqrate/Views/seller_stamp_duty_calculator.dart';
import 'package:iqrate/Views/sign_up.dart';
import 'package:iqrate/Views/splash_screen.dart';
import 'package:get/get.dart';
import 'package:iqrate/Views/subscription_plans_view.dart';
import 'package:iqrate/Views/subscription_summary_view.dart';
import 'package:iqrate/Views/training_application_demo_view.dart';
import 'package:iqrate/Views/training_basic_modules_view.dart';
import 'package:iqrate/Views/training_category_details_view.dart';
import 'package:iqrate/Views/training_user_guide_view.dart';
import 'package:iqrate/Views/training_view.dart';
import 'package:iqrate/Views/two_fact_auth_quesution.dart';
import 'package:iqrate/Views/two_fact_auth_screen2.dart';
import 'package:iqrate/Views/success_view.dart';

import '../Views/Refinance/Existing Lead/mortgage_repayment_table_refinance_existing_lead_plt.dart';
import '../Views/Refinance/New Lead/mortgage_repayment_table_refinance_new_lead_mlt.dart';
import '../Views/Refinance/New Lead/mortgage_repayment_table_refinance_new_lead_plt.dart';
import '../Views/calculator_mortgage/mortgage_calculator_view.dart';
import '../Views/new_purchase/existing_lead/new_purchase_existing_lead_plt_loan_package.dart';
import '../Views/new_purchase/existing_lead/new_purchase_existing_lead_plt_mortgage_repayment.dart';
import '../Views/new_purchase/new_lead/new_purchase_for_new_lead_screen.dart';

//Contains router information. This file has all the routes defined accross various screens.
class NavRouter {
  static final generateRoute = [
    GetPage(
      name: splash,
      page: () => const SplashScreen(),
    ),
    GetPage(
      name: intro,
      page: () => const Intro(),
    ),
    GetPage(
      name: initialScreen,
      page: () => const InitialScreen(),
    ),
    GetPage(
      name: signUp,
      page: () => const SignUp(),
    ),
    GetPage(
      name: emailVerification,
      page: () => const EmailVerification(),
    ),
    GetPage(
      name: emailSentNotifyScreen,
      page: () => const EmailSentNotifyScreen(),
    ),
    GetPage(
      name: mobileVerification,
      page: () => MobileVerification(),
    ),
    GetPage(
      name: twoFactorQuestion,
      page: () => TwoFactAuthQuestion(),
    ),
    GetPage(
      name: twoFactorScreenTwo,
      page: () => const TwoFactAuthScreenTwo(),
    ),
    GetPage(
      name: qrCodeScanner,
      page: () => const QRCodeScanner(),
    ),
    GetPage(
      name: authenticatorOtp,
      page: () => const AuthenticatorOtp(),
    ),
    GetPage(
      name: loginScreen,
      page: () => LoginScreen(),
    ),
    GetPage(
      name: forgetPasswordScreen,
      page: () => ForgetPasswordScreen(),
    ),
    GetPage(
      name: emailAddressVerificationScreen,
      page: () => const EmailAddressVerificationScreen(),
    ),
    GetPage(
      name: newPasswordScreen,
      page: () => NewPasswordScreen(),
    ),
    GetPage(
      name: pricingTableScreen,
      page: () => const PricingTableScreen(),
    ),
    GetPage(
      name: profileScreen,
      page: () => const ProfileScreen(),
    ),
    GetPage(
      name: dashboard,
      page: () => const Dashboard(),
    ),
    GetPage(
      name: manageLoanScreen,
      page: () => const ManageLoanScreen(),
    ),
    GetPage(
      name: myDocumentsScreen,
      page: () => const MyDocumentsScreen(),
    ),
    GetPage(
      name: notificationScreen,
      page: () => const NotificationsView(),
    ),
    GetPage(name: trainingScreen, page: () => const TrainingView()),
    GetPage(
        name: basicModulesTrainingScreen,
        page: () => TrainingBasicModulesView()),
    GetPage(
        name: applicationDemoTrainingScreen,
        page: () => TrainingApplicationDemoView()),
    GetPage(name: userGuideTrainingScreen, page: () => TrainingUserGuideView()),
    GetPage(
        name: trainingCategoryDetailsScreenUrl,
        page: () => TrainingCategoryDetailsView()),
    GetPage(
      name: successScreen,
      page: () => SuccessView(),
    ),
    GetPage(
      name: accountLockedScreen,
      page: () => const AccountLockedView(),
    ),
    GetPage(
      name: enable2faScreen,
      page: () => const Enable2FAView(),
    ),
    GetPage(
      name: subscriptionPlansScreen,
      page: () => SubscriptionPlansView(),
    ),
    GetPage(
      name: paymentDetailsScreen,
      page: () => PaymentDetailsView(),
    ),
    GetPage(
      name: ratesViewScreen,
      page: () => const RatesView(),
    ),
    GetPage(
      name: loanPackageScreen,
      page: () => const LoanPackageScreen(),
    ),
    GetPage(
      name: leadListScreen,
      page: () => const LeadListScreen(),
    ),
    GetPage(
      name: resourseScreen,
      page: () => ResourcesView(),
    ),
    GetPage(
      name: faqScreen,
      page: () => const FaqView(),
    ),
    GetPage(
      name: paymentHistoryScreen,
      page: () => PaymentHistoryView(),
    ),
    GetPage(
      name: subscriptionMenusScreen,
      page: () => SubscriptionSummary(),
    ),
    GetPage(
      name: savedCardsViewScreen,
      page: () => SavedCardsView(),
    ),
    GetPage(
      name: addNewCardScreen,
      page: () => AddNewCardScreen(),
    ),
    GetPage(
      name: editCardScreen,
      page: () => EditCardScreen(),
    ),
    GetPage(
      name: contactUs,
      page: () => ContactusView(),
    ),
    GetPage(
      name: mortgageCalculatorScreen,
      page: () => const MortgageRepaymentView(),
    ),
    GetPage(
      name: equityCalculatorScreen,
      page: () => const EquityCalculatorView(),
    ),
    GetPage(
      name: buyerStampDutyCalculatorScreen,
      page: () => const BuyerStampDutyView(),
    ),
    GetPage(
      name: newPurchaseScreen,
      page: () => const NewPurchaseScreen(),
    ),
    GetPage(
      name: refinanceSavingsScreen,
      page: () => const RefinanceSavingsScreen(),
    ),
    GetPage(
        name: sellerStampDutyCalculatorScreen,
        page: () => SellerStampDutyView()),
    GetPage(
      name: importantLinksScreen,
      page: () => ImportantLinksView(),
    ),
    GetPage(
      name: bankFormsScreen,
      page: () => BankFormsView(),
    ),
    GetPage(
      name: lawFirmListingScreen,
      page: () => LawFirmListingScreen(),
    ),
    GetPage(
      name: othersScreen,
      page: () => OthersView(),
    ),
    GetPage(
      name: buyerStampDutyCalculatorScreen2,
      page: () => const BuyerStampDutyCalculator(),
    ),
    GetPage(
      name: lawFirmProductDetailsScreen,
      page: () => LawFirmListingDetailsView(),
    ),
    GetPage(
      name: myDocumentsScreen,
      page: () => const MyDocumentsScreen(),
    ),
    GetPage(
      name: documentSendOtpScreen,
      page: () => const DocumentSendOtpScreen(),
    ),
    GetPage(
      name: documentMobileVerification,
      page: () => const DocumentMobileVerification(),
    ),
    GetPage(
      name: bankSubmissionBankTaskScreen,
      page: () => const BankSubmissionOneBankTaskScreen(),
    ),
    GetPage(
      name: bankSubmissionEmailScreen,
      page: () => const BankSubmissionEmailScreen(),
    ),
    GetPage(
      name: bankSubmissionEmailBankerScreen,
      page: () => const BankSubmissionEmailBankerScreen(),
    ),
    GetPage(
      name: bankSubmissionEmailSentScreen,
      page: () => const BankSubmissionEmailSuccessScreen(),
    ),
    GetPage(
      name: bankSubmissionEmailSentBankerScreen,
      page: () => const BankSubmissionBankerEmailSuccessScreen(),
    ),
    GetPage(
      name: bankSubmissionDocumentUploadScreen,
      page: () => const BankSubmissionDocumentDrawerScreen(),
    ),
    GetPage(
      name: bankSubmissionDocumentCompressionScreen,
      page: () => BankSubmissionDocumentCompressionScreen(),
    ),
    GetPage(
      name: existingLeadCalculatorScreen,
      page: () => const NewPurchaseExistingLeadCalculator(),
    ),
    GetPage(
      name: newPurchaseNewLeadCalc,
      page: () => const NewPurchaseNewLeadCalculator(),
    ),
    GetPage(
      name: newPurchaseForNewLeadScreen,
      page: () => const NewPurchaseForNewLeadScreen(),
    ),
    GetPage(
      name: newPurchaseForExistingLeadScreen,
      page: () => const NewPurchaseForExistingLeadScreen(),
    ),
    GetPage(
      name: refinanceSavingsForNewLeadScreen,
      page: () => const RefinanceSavingsForNewLeadScreen(),
    ),
    GetPage(
      name: refinanceSavingsForNewLeadCalcScreen,
      page: () => const RefinanceSavingsForNewLeadCalcScreen(),
    ),
    GetPage(
      name: moreRatesScreen,
      page: () => const MoreRatesView(),
    ),
    GetPage(
      name: bucMortgageRepaymentScreen,
      page: () => const BucMortgageRepaymentScreen(),
    ),
    GetPage(
      name: newPurchaseExistingLeadMltLoanPackage,
      page: () => NewPurchaseExistingLeadMltLoanPackage(),
    ),
    GetPage(
      name: newPurchaseExistingLeadMltMortgageRepayment,
      page: () => NewPurchaseExistingLeadMltMortgageRepayment(),
    ),
    GetPage(
      name: newPurchaseExistingLeadPltLoanPackage,
      page: () => NewPurchaseExistingLeadPltLoanPackage(),
    ),
    GetPage(
      name: newPurchaseExistingLeadPltMortgageRepayment,
      page: () => NewPurchaseExistingLeadPltMortgageRepayment(),
    ),

    //new lead
    GetPage(
      name: newPurchaseNewLeadMltLoanPackage,
      page: () => NewPurchaseNewLeadMltLoanPackage(),
    ),
    GetPage(
      name: newPurchaseNewLeadMltMortgageRepayment,
      page: () => NewPurchaseNewLeadMltMortgageRepayment(),
    ),
    GetPage(
      name: newPurchaseNewLeadPltLoanPackage,
      page: () => NewPurchaseNewLeadPltLoanPackage(),
    ),
    GetPage(
      name: newPurchaseNewLeadPltMortgageRepayment,
      page: () => NewPurchaseNewLeadPltMortgageRepayment(),
    ),
    GetPage(
      name: refinanceExistingLead,
      page: () => const RefinanceForExistingLeadScreen(),
    ),
    GetPage(
      name: refinanceExistingLeadCalculator,
      page: () => const RefinanceExistingLead(),
    ),
    GetPage(
      name: refinanceSavingsPackageExistingLead,
      page: () => SavingsFromRefinancingExistingLead(),
    ),
    GetPage(
      name: generatedPackagesRefinanceNewLead,
      page: () => const GeneratedPackagesRefinanceNewLead(),
    ),
    GetPage(
      name: refinanceSavingsPackageNewLead,
      page: () => SavingsFromRefinancingNewLead(),
    ),
    GetPage(
      name: generatedPackagesRefinanceExistingLead,
      page: () => const GeneratedPackagesRefinanceExistingLead(),
    ),
    GetPage(
      name: mortgageRepaymentTableRefinanceExistingLeadMlt,
      page: () => MortagageRepaymentTableRefinanceExistingLeadMlt(),
    ),
    GetPage(
      name: mortgageRepaymentTableRefinanceExistingLeadPlt,
      page: () => MortagageRepaymentTableRefinanceExistingLeadPlt(),
    ),
    GetPage(
      name: mortgageRepaymentTableRefinanceNewLeadMlt,
      page: () => MortagageRepaymentTableRefinanceNewLeadMlt(),
    ),
    GetPage(
      name: mortgageRepaymentTableRefinanceNewLeadPlt,
      page: () => MortagageRepaymentTableRefinanceNewLeadPlt(),
    ),
  ];
}
