//route constants
const String splash = '/splash';
const String intro = '/intro';
const String initialScreen = '/initialScreen';
const String signUp = '/signUp';
const String emailVerification = '/emailVerification';
const String emailSentNotifyScreen = '/emailSentNotifyScreen';
const String loginScreen = '/loginScreen';
const String forgetPasswordScreen = '/forgetPasswordScreen';
const String mobileVerification = '/mobileVerification';
const String twoFactorQuestion = '/twoFactorQuestion';
const String twoFactorScreenTwo = '/twoFactorScreenTwo';
const String qrCodeScanner = '/qrCodeScanner';
const String authenticatorOtp = '/authenticatorOtp';
const String emailAddressVerificationScreen = '/emailAddressVerificationScreen';
const String newPasswordScreen = '/newPasswordScreen';
const String pricingTableScreen = '/PricingTableScreen';
const String profileScreen = '/ProfileScreen';
const String dashboard = '/dashboard';
const String manageLoanScreen = '/ManageLoanScreen';
const String myDocumentsScreen = '/MyDocumentsScreen';
const String notificationScreen = '/notificationScreen';
const String trainingScreen = '/trainingScreen';
const String basicModulesTrainingScreen = '/basicModulesTrainingScreen';
const String applicationDemoTrainingScreen = '/applicationDemoTrainingScreen';
const String userGuideTrainingScreen = '/userGuideTrainingScreen';
const String trainingCategoryDetailsScreenUrl =
    '/trainingCategoryDetailsScreenUrl';
const String successScreen = '/successScreen';
const String accountLockedScreen = '/accountLockedScreen';
const String enable2faScreen = '/enable2faScreen';
const String subscriptionPlansScreen = '/subscriptionPlansScreen';
const String paymentDetailsScreen = '/paymentDetailsScreen';
const String ratesViewScreen = '/RatesViewScreen';
const String loanPackageScreen = '/LoanPackageScreen';
const String leadListScreen = '/LeadListScreen';
const String subscriptionDetailsScreen = '/subscriptionDetailsScreen';
const String resourseScreen = '/resourseScreen';
const String faqScreen = '/faqScreen';
const String paymentHistoryScreen = '/paymentHistoryScreen';
const String subscriptionMenusScreen = '/subscriptionMenusScreen';
const String savedCardsViewScreen = '/SavedCardsViewScreen';
const String addNewCardScreen = '/AddNewCardScreen';
const String editCardScreen = '/EditCardScreen';
const String contactUs = '/contactUs';
const String equityCalculatorScreen = '/equityCalculatorScreen';
const String buyerStampDutyCalculatorScreen = '/buyerStampDutyCalculatorScreen';
const String sellerStampDutyCalculatorScreen =
    '/sellerStampDutyCalculatorScreen';
const String buyerStampDutyCalculatorScreen2 =
    '/buyerStampDutyCalculatorScreen2';
const String importantLinksScreen = '/importantLinkScreen';
const String bankFormsScreen = '/bankFormScreen';
const String lawFirmListingScreen = '/lawfirmListingScreen';
const String othersScreen = '/othersScreen';
const String lawFirmProductDetailsScreen = '/lawFirmProductDetailsScreen';
const String documentSendOtpScreen = '/DocumentSendOtpScreen';
const String documentMobileVerification = '/DocumentMobileVerification';
const String newPurchaseScreen = '/NewPurchaseScreen';
const String refinanceSavingsScreen = '/RefinanceSavingsScreen';
const String bankSubmissionBankTaskScreen = '/bankSubmissionBankTaskScreen';
const String bankSubmissionEmailScreen = '/bankSubmissionEmailScreen';
const String bankSubmissionEmailBankerScreen =
    '/bankSubmissionEmailBankerScreen';
const String bankSubmissionEmailSentScreen = '/bankSubmissionEmailSentScreen';
const String bankSubmissionEmailSentBankerScreen =
    '/bankSubmissionEmailSentBankerScreen';

const String bankSubmissionDocumentUploadScreen =
    '/bankSubmissionDocumentUploadScreen';
const String bankSubmissionDocumentCompressionScreen =
    '/bankSubmissionDocumentCompressionScreen';
const String existingLeadCalculatorScreen = '/ExistingLeadCalculatorScreen';
const String newPurchaseForNewLeadScreen = '/NewPurchaseForNewLeadScreen';
const String newPurchaseForExistingLeadScreen =
    '/NewPurchaseForExistingLeadScreen';
const String newPurchaseNewLeadCalc = '/NewPurchaseNewLeadCalculator';
const String refinanceSavingsForNewLeadScreen =
    '/RefinanceSavingsForNewLeadScreen';
const String refinanceSavingsForNewLeadCalcScreen =
    '/RefinanceSavingsForNewLeadCalcScreen';
const String moreRatesScreen = '/moreratesScreen';
const String mortgageCalculatorScreen = '/mortgageCalculatorScreen';
const String bucMortgageRepaymentScreen = '/bucMortgageRepaymentScreen';

const String newPurchaseExistingLeadMltLoanPackage =
    '/NewPurchaseExistingLeadMltLoanPackage';
const String newPurchaseExistingLeadMltMortgageRepayment =
    '/NewPurchaseExistingLeadMltMortgageRepayment';
const String newPurchaseExistingLeadPltLoanPackage =
    '/NewPurchaseExistingLeadPltLoanPackage';
const String newPurchaseExistingLeadPltMortgageRepayment =
    '/NewPurchaseExistingLeadPltMortgageRepayment';

//new lead
const String newPurchaseNewLeadMltLoanPackage =
    '/NewPurchaseNewLeadMltLoanPackage';
const String newPurchaseNewLeadMltMortgageRepayment =
    '/NewPurchaseNewLeadMltMortgageRepayment';
const String newPurchaseNewLeadPltLoanPackage =
    '/NewPurchaseNewLeadPltLoanPackage';
const String newPurchaseNewLeadPltMortgageRepayment =
    '/NewPurchaseNewLeadPltMortgageRepayment';

const String refinanceExistingLead = '/RefinanceExistingLead';
const String refinanceSavingsPackageExistingLead =
    '/RefinanceSavingsPackageExistingLead';
const String generatedPackagesRefinanceExistingLead =
    '/GeneratedPackagesRefinanceExistingLead';
const String generatedPackagesRefinanceNewLead =
    '/GeneratedPackagesRefinanceNewLead';
const String mortgageRepaymentTableRefinanceExistingLeadMlt =
    '/MortgageRepaymentTableRefinanceExistingLeadMlt';
const String mortgageRepaymentTableRefinanceExistingLeadPlt =
    '/MortgageRepaymentTableRefinanceExistingLeadPlt';

const String refinanceExistingLeadCalculator =
    '/RefinanceExistingLeadCalculator';
const String refinanceSavingsPackageNewLead = '/RefinanceSavingsPackage';

const String mortgageRepaymentTableRefinanceNewLeadMlt =
    '/MortgageRepaymentTableRefinanceNewLeadMlt';
const String mortgageRepaymentTableRefinanceNewLeadPlt =
    '/MortgageRepaymentTableRefinanceNewLeadPlt';
