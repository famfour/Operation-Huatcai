import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/payment_details_view_controller.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Utils/config.dart';
import 'package:iqrate/Utils/text_formatter.dart';
import 'package:iqrate/Widgets/default_appbar.dart';
import 'package:iqrate/Widgets/form_field_title.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';

// ignore: must_be_immutable
class PaymentDetailsView extends StatelessWidget {
  PaymentDetailsView({Key? key}) : super(key: key);

  PaymentDetailsViewController paymentDetailsViewController =
      Get.put(PaymentDetailsViewController());

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: DefaultAppBar(
          title: 'Payment Details', windowHeight: windowHeight * 0.09),
      body: Padding(
        padding: EdgeInsets.all(windowWidth * 0.05),
        child: ListView(
          children: [
            SizedBox(height: windowHeight * 0.03),
            Text(
              'Your Selection',
              style: TextStyles.basicTextStyle,
            ),
            const Divider(
              thickness: 2,
              color: Colors.black,
            ),
            SizedBox(height: windowHeight * 0.02),
            Obx(
              () => PlanDetailsCard(
                planPrice: paymentDetailsViewController.amount.value.toString(),
                planTerm:
                    paymentDetailsViewController.planTerm.value.toString(),
                planType:
                    paymentDetailsViewController.planType.value.toString(),
                windowHeight: windowHeight,
                windowWidth: windowWidth,
                controller: paymentDetailsViewController,
              ),
            ),
            SizedBox(height: windowHeight * 0.03),
            const Divider(
              thickness: 2,
            ),
            SizedBox(height: windowHeight * 0.03),
            Obx(
              () => paymentDetailsViewController
                      .stripePaymentMethodId!.value.isNotEmpty
                  ? PrimaryButton(
                      windowHeight: windowHeight,
                      windowWidth: windowWidth,
                      buttonTitle: 'Submit',
                      isDisable: false,
                      onPressed: () {
                        // Call API for upgrade and downgrade
                        paymentDetailsViewController
                            .upgradeDowngradeSubscriptionFunction();
                      })
                  : CardDetailsForm(
                      windowHeight: windowHeight,
                      windowWidth: windowWidth,
                      controller: paymentDetailsViewController,
                    ),
            )
          ],
        ),
      ),
    );
  }
}

// ignore: must_be_immutable
class PlanDetailsCard extends StatelessWidget {
  PlanDetailsCard(
      {Key? key,
      required this.planType,
      required this.planPrice,
      required this.planTerm,
      required this.windowHeight,
      required this.windowWidth,
      required this.controller})
      : super(key: key);
  final String planType;
  final String planPrice;
  final String planTerm;
  double windowHeight;
  double windowWidth;
  PaymentDetailsViewController controller;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          border: Border(
              top: BorderSide(color: Colors.grey),
              bottom: BorderSide(color: Colors.grey),
              left: BorderSide(color: Colors.grey),
              right: BorderSide(color: Colors.grey)),
          borderRadius: BorderRadius.all(Radius.circular(5))),
      child: Padding(
        padding: EdgeInsets.symmetric(
            vertical: windowWidth * 0.07, horizontal: windowWidth * 0.03),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  planType.toUpperCase() + ' Subscription Plan',
                  style: TextStyles.paymentDetailsTitle,
                ),
                Text(
                  '\$ ${AppConfig.extraDecimalAdd(planPrice)}',
                  style: TextStyles.basicTextStyle,
                )
              ],
            ),
            SizedBox(height: windowHeight * 0.01),
            Text(
              planTerm,
              style: TextStyles.paymentDetailsSubTitle,
            ),
            Text(
              controller.planExpiration.value,
              style: TextStyles.paymentDetailsSubTitle,
            ),
            SizedBox(height: windowHeight * 0.01),
            Divider(
              thickness: 1,
              color: Colors.black.withOpacity(0.5),
            ),
            SizedBox(height: windowHeight * 0.01),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Total',
                  style: TextStyles.paymentDetailsTitle,
                ),
                Text(
                  '\$ ${AppConfig.extraDecimalAdd(planPrice)}',
                  style: TextStyles.basicTextStyle,
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}

// ignore: must_be_immutable
class CardDetailsForm extends StatefulWidget {
  CardDetailsForm(
      {Key? key,
      required this.windowHeight,
      required this.windowWidth,
      required this.controller})
      : super(key: key);
  double windowHeight;
  double windowWidth;
  PaymentDetailsViewController controller;

  @override
  State<CardDetailsForm> createState() => _CardDetailsFormState();
}

class _CardDetailsFormState extends State<CardDetailsForm> {
  // CardFieldInputDetails? _card;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: const [
            FormFieldTitle(title: 'Name on Card*'),
          ],
        ),
        SizedBox(height: widget.windowHeight * 0.005),
        RequireTextField(
          type: Type.fullname,
          controller: widget.controller.nameOnCardTextEditingController,
        ),
        SizedBox(height: widget.windowHeight * 0.01),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: const [
            FormFieldTitle(title: 'Card Number*'),
          ],
        ),
        SizedBox(height: widget.windowHeight * 0.005),
        RequireTextField(
          numberInputFormatter: [
            FilteringTextInputFormatter.digitsOnly,
            CardNumberFormatter(),
          ],
          key: const Key("Card Number"),
          type: Type.number,
          maxLength: 19,
          controller: widget.controller.cardNumberTextEditingController,
          onChanged: (String value) {
            widget.controller.cardDetails.value = widget
                .controller.cardDetails.value
                .copyWith(number: value.toString().replaceAll(" ", ""));
          },
        ),
        SizedBox(height: widget.windowHeight * 0.01),
        Row(
          children: [
            Expanded(
              flex: 5,
              child: Padding(
                padding: const EdgeInsets.only(right: 10),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: const [
                        FormFieldTitle(title: 'Expiry*'),
                      ],
                    ),
                    SizedBox(height: widget.windowHeight * 0.005),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          flex: 2,
                          child: RequireTextField(
                            maxLength: 2,
                            textInputAction: TextInputAction.next,
                            type: Type.expiry,
                            labelText: 'MM',
                            controller: widget.controller
                                .expirationMonthTextEditingController,
                            onChanged: (String value) {
                              /*widget.controller.cardDetails.value =
                                  widget.controller.cardDetails.value.copyWith(
                                      expirationMonth: int.tryParse(value));*/

                              widget.controller.cardDetails.value =
                                  widget.controller.cardDetails.value.copyWith(
                                      expirationMonth: int.tryParse(widget
                                          .controller
                                          .expirationMonthTextEditingController
                                          .text));
                            },
                          ),
                        ),
                        SizedBox(
                          width: widget.windowWidth * 0.03,
                        ),
                        Expanded(
                          flex: 2,
                          child: RequireTextField(
                            maxLength: 2,
                            type: Type.expiry,
                            labelText: 'YY',
                            controller: widget
                                .controller.expirationYearTextEditingController,
                            onChanged: (String value) {
                              /*widget.controller.cardDetails.value =
                                  widget.controller.cardDetails.value.copyWith(
                                      expirationMonth: int.tryParse(value));*/

                              widget.controller.cardDetails.value =
                                  widget.controller.cardDetails.value.copyWith(
                                      expirationYear: int.tryParse(widget
                                          .controller
                                          .expirationYearTextEditingController
                                          .text));
                            },
                          ),
                        ),
                        /*Expanded(
                            flex: 1,
                            child: Center(
                              child: Text(
                                '/',
                                style: TextStyles.basicTextStyle,
                              ),
                            )),
                        Expanded(
                          flex: 4,
                          child: RequireTextField(
                            type: Type.number,
                            maxLength: 2,
                            labelText: 'YY',
                            controller: widget
                                .controller.expirationYearTextEditingController,
                            onChanged: (String value) {
                              widget.controller.cardDetails.value =
                                  widget.controller.cardDetails.value.copyWith(
                                      expirationYear: int.tryParse(value));
                            },
                          ),
                        ),*/
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 5,
              child: Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: const [
                        FormFieldTitle(title: 'CVC*'),
                      ],
                    ),
                    SizedBox(height: widget.windowHeight * 0.005),
                    RequireTextField(
                      type: Type.number,
                      maxLength: 3,
                      labelText: 'CVC',
                      controller: widget.controller.cvcTextEditingController,
                      onChanged: (String value) {
                        widget.controller.cardDetails.value = widget
                            .controller.cardDetails.value
                            .copyWith(cvc: value);
                      },
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        // SizedBox(height: widget.windowHeight * 0.02),
        // Row(
        //   mainAxisAlignment: MainAxisAlignment.start,
        //   children: const [
        //     FormFieldTitle(title: 'Country'),
        //   ],
        // ),
        // SizedBox(height: widget.windowHeight * 0.005),
        // RequireTextField(
        //   type: Type.dropDownCountry,
        //   controller: widget.controller.countryTextEditingController,
        //   onChanged: (String value) {},
        // ),
        // SizedBox(height: widget.windowHeight * 0.02),
        /*Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: const [
            FormFieldTitle(title: 'Postal code'),
          ],
        ),
        SizedBox(height: widget.windowHeight * 0.005),
        RequireTextField(
          type: Type.expiry,
          maxLength: 6,
          labelText: '',
          controller: widget.controller.postalCodeController,
        ),*/
        // Obx(
        //   () => CheckboxListTile(
        //     title: const Text(
        //       "Save card for future use.",
        //       style: TextStyle(color: Colors.black54),
        //     ),
        //     value: widget.controller.checkedValue.value,
        //     activeColor: Colors.black,
        //     contentPadding: EdgeInsets.zero,
        //     onChanged: (newValue) {
        //       widget.controller.checkedValue.value = newValue!;
        //     },
        //     controlAffinity:
        //         ListTileControlAffinity.leading, //  <-- leading Checkbox
        //   ),
        // ),
        SizedBox(height: widget.windowHeight * 0.04),
        PrimaryButton(
            windowHeight: widget.windowHeight,
            windowWidth: widget.windowWidth,
            buttonTitle: 'Submit',
            isDisable: false,
            onPressed: () {
              widget.controller.onTapSubmit(
                  widget.controller.cardDetails,
                  widget.controller.nameOnCardTextEditingController.text,
                  widget.controller.countryTextEditingController.text,
                  widget.controller.postalCodeController.text);
            }),
      ],
    );
  }
}
