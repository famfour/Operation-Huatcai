// ignore_for_file: avoid_print, must_be_immutable, duplicate_ignore, unused_local_variable

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/rates_view_controller.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Service/GetControllers.dart';
import 'package:iqrate/Utils/ItemCheckBoxModel.dart';
import 'package:iqrate/Utils/multi_checkbox_custom.dart';
import 'package:iqrate/Widgets/primary_button.dart';

import 'rates/filter_rate_section.dart';
import 'rates/loan_package_card_V2.dart';

class RatesView extends StatelessWidget {
  const RatesView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // List of loan packages cards
    return const CardsList();
  }
}

class CardsList extends StatefulWidget {
  const CardsList({Key? key}) : super(key: key);

  @override
  State<CardsList> createState() => _CardsListState();
}

class _CardsListState extends State<CardsList> {
  late double windowHeight;
  late double windowWidth;

  final RatesViewController ratesViewController =
      GetControllers.shared.getRatesViewController();

  @override
  void initState() {
    super.initState();
    debugPrint("CardsList initState");

    ratesViewController.callAPIGetListAllPackages();
  }

  @override
  Widget build(BuildContext context) {
    // debugPrint("CardsList ${ratesViewController.moreRates.value.results}");
    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.width;
    return Scaffold(
        extendBody: true,
        extendBodyBehindAppBar: true,
        body: NotificationListener<ScrollEndNotification>(
          onNotification: (scrollEnd) {
            var metrics = scrollEnd.metrics;
            if (metrics.atEdge) {
              if (metrics.pixels != 0) {
                print('At bottom loadMore');
                ratesViewController.loadMore();
              }
            }
            return true;
          },
          child: RefreshIndicator(
              displacement: 100,
              backgroundColor: Colors.green,
              color: Colors.white,
              strokeWidth: 3,
              triggerMode: RefreshIndicatorTriggerMode.onEdge,
              onRefresh: () async {
                ratesViewController.callAPiRefresh();
              },
              child: Obx(
                () => Stack(children: [
                  listPackage(),
                  popUps(),
                ]),
              )),
        ));
  }

  popUps() {
    switch (ratesViewController.showFilter.value) {
      case 1:
        {
          return FilterRateSection(
            windowHeight: windowHeight,
            windowWidth: windowWidth,
            resetFilterClicked: () {
              // ratesViewController.showFilter.value = 1;
              // ratesViewController.showFilter.refresh();
              debugPrint(
                  "ratesViewController.showFilter ${ratesViewController.showFilter}");
              Future.delayed(const Duration(milliseconds: 1200), () {
                setState(() {
                  ratesViewController.showFilter.value = 1;
                });
              });
            },
            closeFilterClicked: () {
              Future.delayed(const Duration(milliseconds: 200), () {
                setState(() {
                  ratesViewController.showFilter.value = 0;
                });
              });
            },
          );
        }
      case 2:
        {
          return ExportPDFSection(
              windowHeight: windowHeight, windowWidth: windowWidth);
        }
      case 3:
        {
          return Container(
            color: AppColors.white,
            width: windowWidth,
            height: windowHeight,
            child: const Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
      default:
        {
          return const Offstage();
        }
    }
  }

  listPackage() {
    return Obx(() => ratesViewController.listData.isNotEmpty
        ? ListView.builder(
            itemCount: ratesViewController
                .listData.length, // This count needs to be dynamic
            itemBuilder: (BuildContext context, int index) {
              var item = ratesViewController.listData.elementAt(index);
              return LoanPackageCardV2(
                data: item,
                image: item.bank!.logo!,
                headingSubText:
                    '${item.rateType.toString().capitalize} (${item.rateCategory.toString().capitalize})',
                headingText: item.bank!.name!,
                onChanged: (val) {
                  debugPrint(
                      "update model $val, now  selected ${ratesViewController.numberBankSelected}");
                  //check if > 4 not allow check anymore~
                  var a = ratesViewController.getCountItemSelected();
                  debugPrint("b4 $a");
                  if (a >= 4 && (val ?? false)) {
                    debugPrint("max not allow selected anymore");
                    Future.delayed(const Duration(milliseconds: 0), () {
                      FocusManager.instance.primaryFocus!.unfocus();
                      Fluttertoast.showToast(
                        timeInSecForIosWeb: 3,
                        msg: "You have reached the maximum limit of 4 banks",
                        backgroundColor: Colors.red,
                        toastLength: Toast.LENGTH_LONG,
                        gravity: ToastGravity.BOTTOM,
                        textColor: Colors.white,
                        fontSize: 20.0,
                      );
                    });
                  } else {
                    ratesViewController.listData[index].isChecked = val;
                    var b = ratesViewController.getCountItemSelected();
                    debugPrint("after $b");
                    ratesViewController.listData.refresh();
                  }
                },
                // checkBoxValue: false,
                cardIndex: index,
              );
            })
        : Center(
            child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                  height: windowHeight * 0.15,
                  child: SvgPicture.asset('assets/images/nodata.svg')),
              Text(
                'No data to show',
                style: TextStyles.bankSubmissionCardTitleDisabled,
              )
            ],
          )));
  }
}

class ExportPDFSection extends StatelessWidget {
  ExportPDFSection(
      {Key? key, required this.windowHeight, required this.windowWidth})
      : super(key: key);

  // final LeadsViewController leadsViewController = Get.find();
  final RatesViewController ratesViewController =
      GetControllers.shared.getRatesViewController();
  double windowHeight;
  double windowWidth;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black38,
      height: windowHeight,
      child: SingleChildScrollView(
        child: Column(
          // mainAxisSize: MainAxisSize.min,
          children: [
            topBar(),
            body(),
          ],
        ),
      ),
    );
  }

  Widget topBar() {
    return Container(
      color: Colors.white,
      child: Row(
        children: [
          Expanded(
            flex: 5,
            child: Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 20),
                child: Text(
                  "Export Selected Loan Packages to PDF",
                  style: TextStyles.headingSubtitleStyle3,
                ),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: InkWell(
                onTap: () {
                  ratesViewController.showFilter.value = 0;
                },
                child: SvgPicture.asset(
                  'assets/icons/closeEnclosed.svg',
                  color: Colors.red,
                )),
          )
        ],
      ),
    );
  }

  Widget body() {
    return Container(
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 50),
          Center(
            child: PrimaryButton(
                windowHeight: windowHeight,
                windowWidth: windowWidth * 0.85,
                buttonTitle: 'Export',
                onPressed: () {
                  debugPrint("Export clicked ");
                  ratesViewController.callAPIExportPackage();
                  // ratesViewController.callAPIGetBankListWithFilter();
                }),
          ),
          const SizedBox(height: 50),
          // Container(color: Colors.black38,height: 100,width: windowWidth,)
        ],
      ),
    );
  }
}

class BankDropDown extends StatefulWidget {
  const BankDropDown({Key? key}) : super(key: key);

  @override
  _BankDropDownState createState() => _BankDropDownState();
}

class _BankDropDownState extends State<BankDropDown> {
  List<ItemCheckBoxModel> dataSource = [];
  var controller = GetControllers.shared.getRatesViewController();

  @override
  void initState() {
    super.initState();
    loadData();
  }

  loadData() {
    dataSource.clear();
    var array = controller.bankDataSourceV2;
    print('array have ${array.length}');
    for (var i = 0; i < array.length; i++) {
      var item = array[i];
      dataSource.add(ItemCheckBoxModel(
          name: item.name, value: item.value, isChecked: item.isChecked));
    }
  }

  //
  // void _showMultiSelect() async {
  //   print("backup");
  //   await showDialog(
  //     context: context,
  //     builder: (BuildContext context) {
  //       // final TestingLogic controller = Get.put(TestingLogic());
  //       return MultiSelect(
  //           titleDropdown: "bank aaaaaaa",
  //           didChoose: (list) {
  //             controller.bankDataSourceV2 = list;
  //             controller.getSelectedBankDataSourceV2();
  //             setState(() {});
  //             //get selected item
  //           },
  //           dataSourceItemOutSide: controller.bankDataSourceV2);
  //     },
  //   );
  // }
  void _showMultiSelectBank() async {
    print("_showMultiSelectBank");
    await showDialog(
      context: context,
      builder: (BuildContext context) {
        // final TestingLogic controller = Get.put(TestingLogic());
        return MultiSelectBank(
          titleDropdown: "bank aaaaaaa",
          didChoose: (list) {
            controller.bankDataSourceV2 = list;
            controller.getSelectedBankDataSourceV2();
            setState(() {});
            //get selected item
          },
          // dataSourceItemOutSide: controller.bankDataSourceV2
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 0),
      child: Column(
        children: [
          // ElevatedButton(
          //     onPressed: () {
          //       _showMultiSelect();
          //     },
          //     child: Text("show popup bank")),
          InkWell(
            onTap: () {
              debugPrint("show popup bank");
              _showMultiSelectBank();
            },
            child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                height: 50,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: AppColors.formFieldBorderColor),
                  borderRadius: const BorderRadius.all(Radius.circular(10.0)),
                ),
                child: Row(
                  children: const [
                    Icon(
                      Icons.search,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      "Search a Bank",
                      style: TextStyle(fontSize: 14, color: Color(0xFFA8A8A8)),
                    ),
                    Spacer(),
                    Icon(Icons.expand_more)
                  ],
                )),
          ),
          Container(
            color: Colors.white,
            child: showChipList(),
            width: double.infinity,
          ),
          // showChipList()
          const SizedBox(
            height: 10,
          )
        ],
      ),
      // child: MultiSelect(didChoose: (list) {
      //   GetControllers.shared.getRatesViewController().properTypeDataSourceV2 = list;
      // }, dataSourceItemOutSide: GetControllers.shared.getRatesViewController().properTypeDataSourceV2)
    );
  }

  showChipList() {
    //get array choosed
    return Wrap(
        alignment: WrapAlignment.start,
        spacing: 5,
        children: List<Widget>.generate(
          controller.selectedBankDataSourceV2.length,
          (int index) {
            return Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Chip(
                padding: const EdgeInsets.all(10),
                side: const BorderSide(color: AppColors.kPrimaryColor),
                backgroundColor: AppColors.kPrimaryColor,
                elevation: 1,
                labelStyle: const TextStyle(color: Colors.white),
                label: Text(controller.selectedBankDataSourceV2[index].name),
              ),
            );
          },
        ).toList());
  }
}

class PropertyTypeDropDown extends StatefulWidget {
  const PropertyTypeDropDown({Key? key}) : super(key: key);

  @override
  _PropertyTypeDropDownState createState() => _PropertyTypeDropDownState();
}

class _PropertyTypeDropDownState extends State<PropertyTypeDropDown> {
  List<ItemCheckBoxModel> dataSource = [];
  var controller = GetControllers.shared.getRatesViewController();

  @override
  void initState() {
    super.initState();
    loadData();
  }

  void _showMultiSelect() async {
    print("backup");
    await showDialog(
      context: context,
      builder: (BuildContext context) {
        // final TestingLogic controller = Get.put(TestingLogic());
        return MultiSelect(
            titleDropdown: "bbbb aaaaaaa",
            didChoose: (list) {
              controller.properTypeDataSourceV2 = list;
              controller.getSelectedProperTypeDataSourceV2();
              setState(() {});
              //get selected item
            },
            dataSourceItemOutSide: controller.properTypeDataSourceV2);
      },
    );
  }

  loadData() {
    dataSource.clear();
    var array = controller.properTypeDataSourceV2;
    print('array have ${array.length}');
    for (var i = 0; i < array.length; i++) {
      var item = array[i];
      dataSource.add(ItemCheckBoxModel(
          name: item.name, value: item.value, isChecked: item.isChecked));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 0),
      child: Column(
        children: [
          // ElevatedButton(
          // onPressed: () {
          //   _showMultiSelect();
          // },
          // child: Text("show popup")),
          InkWell(
            onTap: () {
              _showMultiSelect();
            },
            child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                height: 50,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: AppColors.formFieldBorderColor),
                  borderRadius: const BorderRadius.all(Radius.circular(10.0)),
                ),
                child: Row(
                  children: const [
                    Text(
                      "Select a Property Type",
                      style: TextStyle(fontSize: 14, color: Color(0xFFA8A8A8)),
                    ),
                    Spacer(),
                    Icon(Icons.expand_more)
                  ],
                )),
          ),
          Container(
            color: Colors.white,
            child: showChipList(),
            width: double.infinity,
          )
          // showChipList()
        ],
      ),
      // child: MultiSelect(didChoose: (list) {
      //   GetControllers.shared.getRatesViewController().properTypeDataSourceV2 = list;
      // }, dataSourceItemOutSide: GetControllers.shared.getRatesViewController().properTypeDataSourceV2)
    );
  }

  showChipList() {
    //get array choosed
    return Wrap(
        alignment: WrapAlignment.start,
        spacing: 5,
        children: List<Widget>.generate(
          controller.selectedProperTypeDataSourceV2.length,
          (int index) {
            return Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Chip(
                padding: const EdgeInsets.all(10),
                side: const BorderSide(color: AppColors.kPrimaryColor),
                backgroundColor: AppColors.kPrimaryColor,
                elevation: 1,
                labelStyle: const TextStyle(color: Colors.white),
                label:
                    Text(controller.selectedProperTypeDataSourceV2[index].name),
              ),
            );
          },
        ).toList());
  }

//
// Widget showChipList(dynamic array) => Container(
//
//       // mainAxisAlignment: MainAxisAlignment.start,
//       // crossAxisAlignment: CrossAxisAlignment.start,
//       children: [
//         for (var i = 0; i < 10; i++)
//           Container(
//             alignment: Alignment.center,
//             margin: EdgeInsets.all(2),
//             height: 50,
//             decoration: BoxDecoration(borderRadius: BorderRadius.circular(20)),
//             child: Chip(
//               label: Text("haha $i", style: TextStyle(
//                 color: Colors.black,
//                 fontSize: 16,
//                 fontWeight: FontWeight.bold,
//               ),),
//               // backgroundColor: Theme.of(context).accentColor,
//             ),
//           ),
//       ],
//     );
//

}

class PropertyStatusChip extends StatefulWidget {
  const PropertyStatusChip({Key? key}) : super(key: key);

  @override
  _PropertyStatusChipState createState() => _PropertyStatusChipState();
}

class _PropertyStatusChipState extends State<PropertyStatusChip> {
  var ratesViewController = GetControllers.shared.getRatesViewController();

  @override
  Widget build(BuildContext context) {
    var windowHeight = MediaQuery.of(context).size.height;
    var windowWidth = MediaQuery.of(context).size.width;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Property Status",
          style: TextStyles.headingSubtitleStyle3,
        ),
        SizedBox(
          height: windowHeight * 0.01,
        ),
        Obx(
          () => Wrap(
              alignment: WrapAlignment.start,
              children: List<Widget>.generate(
                ratesViewController.properStatusDataSource.length,
                (int index) {
                  return Padding(
                    padding: const EdgeInsets.only(right: 5.0),
                    child: ChoiceChip(
                      side: const BorderSide(color: AppColors.kPrimaryColor),
                      selectedColor: AppColors.kPrimaryColor,
                      disabledColor: Colors.white,
                      backgroundColor: Colors.white,
                      elevation: 0,
                      labelStyle: TextStyle(
                          color: ratesViewController
                                      .properStatusSelectedIndex.value ==
                                  index
                              ? Colors.white
                              : Colors.black),
                      label: Text(
                          ratesViewController.properStatusDataSource[index]),
                      selected:
                          ratesViewController.properStatusSelectedIndex.value ==
                              index,
                      onSelected: (bool selected) {
                        print("selected  $selected");
                        ratesViewController.properStatusSelectedIndex.value =
                            (selected ? index : null)!;
                      },
                    ),
                  );
                },
              ).toList()),
        ),
      ],
    );
  }
}

class RateCategoryChip extends StatefulWidget {
  const RateCategoryChip({Key? key}) : super(key: key);

  @override
  _RateCategoryChipState createState() => _RateCategoryChipState();
}

class _RateCategoryChipState extends State<RateCategoryChip> {
  var ratesViewController = GetControllers.shared.getRatesViewController();

  @override
  Widget build(BuildContext context) {
    var windowHeight = MediaQuery.of(context).size.height;
    var windowWidth = MediaQuery.of(context).size.width;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Rate Category",
          style: TextStyles.headingSubtitleStyle3,
        ),
        SizedBox(
          height: windowHeight * 0.01,
        ),
        Obx(
          () => Wrap(
              alignment: WrapAlignment.start,
              children: List<Widget>.generate(
                ratesViewController.rateCategoryDataSource.length,
                (int index) {
                  return Padding(
                    padding: const EdgeInsets.only(right: 5.0),
                    child: ChoiceChip(
                      side: const BorderSide(color: AppColors.kPrimaryColor),
                      selectedColor: AppColors.kPrimaryColor,
                      disabledColor: Colors.white,
                      backgroundColor: Colors.white,
                      elevation: 0,
                      labelStyle: TextStyle(
                          color: ratesViewController
                                      .rateCategorySelectedIndex.value ==
                                  index
                              ? Colors.white
                              : Colors.black),
                      label: Text(
                          ratesViewController.rateCategoryDataSource[index]),
                      selected:
                          ratesViewController.rateCategorySelectedIndex.value ==
                              index,
                      onSelected: (bool selected) {
                        print("selected $selected");
                        ratesViewController.rateCategorySelectedIndex.value =
                            (selected ? index : null)!;
                      },
                    ),
                  );
                },
              ).toList()),
        ),
      ],
    );
  }
}

class LoanTypeChip extends StatefulWidget {
  const LoanTypeChip({Key? key}) : super(key: key);

  @override
  _LoanTypeChipState createState() => _LoanTypeChipState();
}

class _LoanTypeChipState extends State<LoanTypeChip> {
  var ratesViewController = GetControllers.shared.getRatesViewController();

  @override
  Widget build(BuildContext context) {
    var windowHeight = MediaQuery.of(context).size.height;
    var windowWidth = MediaQuery.of(context).size.width;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Loan Type",
          style: TextStyles.headingSubtitleStyle3,
        ),
        SizedBox(
          height: windowHeight * 0.01,
        ),
        Obx(
          () => Wrap(
              alignment: WrapAlignment.start,
              children: List<Widget>.generate(
                ratesViewController.loanTypeDataSource.length,
                (int index) {
                  return Padding(
                    padding: const EdgeInsets.only(right: 5.0),
                    child: ChoiceChip(
                      side: const BorderSide(color: AppColors.kPrimaryColor),
                      selectedColor: AppColors.kPrimaryColor,
                      disabledColor: Colors.white,
                      backgroundColor: Colors.white,
                      elevation: 0,
                      labelStyle: TextStyle(
                          color:
                              ratesViewController.loanTypeSelectedIndex.value ==
                                      index
                                  ? Colors.white
                                  : Colors.black),
                      label:
                          Text(ratesViewController.loanTypeDataSource[index]),
                      selected:
                          ratesViewController.loanTypeSelectedIndex.value ==
                              index,
                      onSelected: (bool selected) {
                        print("selected $selected");
                        ratesViewController.loanTypeSelectedIndex.value =
                            (selected ? index : null)!;
                      },
                    ),
                  );
                },
              ).toList()),
        ),
      ],
    );
  }
}

class CheckBoxFilter extends StatefulWidget {
  bool initValue;
  final String textString;
  final Function(bool) onChanged;

  CheckBoxFilter(
      {Key? key,
      required this.onChanged,
      required this.textString,
      this.initValue = false})
      : super(key: key);

  @override
  _CheckBoxFilterState createState() => _CheckBoxFilterState();
}

class _CheckBoxFilterState extends State<CheckBoxFilter> {
  // var isChecked = false.obs;

  @override
  void initState() {
    super.initState();
    // isChecked.value = widget.initValue;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Checkbox(
          value: widget.initValue,
          onChanged: (value) {
            // isChecked.value = value ?? false;
            widget.onChanged(value ?? false);
          },
          //checkColor: AppColors.kSecondaryColor,
          //fillColor: MaterialStateProperty.all(Colors.white),
          hoverColor: AppColors.kSecondaryColor,
          activeColor: AppColors.kSecondaryColor,
          side: const BorderSide(
              color: AppColors.kSecondaryColor,
              width: 2,
              style: BorderStyle.solid),
        ),
        Expanded(
            child: Text(
          widget.textString,
          maxLines: 4,
        ))
        // Wrap(children: [Text(widget.textString)]),
      ],
    );
  }
}

class MultiSelectBank extends StatefulWidget {
  final String titleDropdown;
  final Function(List<ItemCheckBoxModel>) didChoose;

  // List<ItemCheckBoxModel> dataSourceItemOutSide;
  const MultiSelectBank(
      {Key? key,
      // required this.dataSourceItemOutSide,
      required this.didChoose,
      required this.titleDropdown})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _MultiSelectBankState();
}

class _MultiSelectBankState extends State<MultiSelectBank> {
  var dataSourceItem = <ItemCheckBoxModel>[];

  // var dataSourceCheck = <ItemCheckBoxModel>[];
  final RatesViewController ratesViewController =
      GetControllers.shared.getRatesViewController();

  // var dataSourceCheck = <ItemCheckBoxModel>[];
  final _filter = TextEditingController();
  String _searchText = "";
  var filteredDataSourceCheck = <ItemCheckBoxModel>[];

  setupSearchListener() {
    _filter.addListener(() {
      if (_filter.text.isEmpty) {
        setState(() {
          _searchText = "";
          filteredDataSourceCheck = dataSourceItem;
        });
      } else {
        setState(() {
          _searchText = _filter.text;
          // if (kDebugMode) {
          //   print(_searchText);
          // }
        });
      }
    });
  }

  @override
  void dispose() {
    _filter.dispose();
    super.dispose();
  }

  countTrue(List<ItemCheckBoxModel> datasource) {
    var count = 0;
    for (var i = 0; i < datasource.length; i++) {
      if (datasource[i].isChecked) count += 1;
    }
    if (kDebugMode) {
      print("count true $count");
    }
  }

  @override
  void initState() {
    super.initState();
    if (kDebugMode) {
      print("MultiSelectBank initState MultiSelect run init");
    }
    dataSourceItem.clear();
    filteredDataSourceCheck.clear();
    setupSearchListener();
    // for(var item in widget.dataSourceItemOutSide){
    //   ItemCheckBoxModel newItem =  ItemCheckBoxModel(
    //       name: item.name,
    //       value: item.value,
    //       isChecked: item.isChecked
    //   );
    //   dataSourceItem.add(newItem);
    //   dataSourceCheck.add(newItem);
    // }
    for (var i = 0; i < ratesViewController.bankDataSourceV2.length; i++) {
      var item = ratesViewController.bankDataSourceV2[i];
      var newItem = ItemCheckBoxModel(
          name: item.name, value: item.value, isChecked: item.isChecked);
      dataSourceItem.add(newItem);
      filteredDataSourceCheck.add(newItem);
    }
    //
    // for (var i = 0; i < widget.dataSourceItemOutSide.length; i++) {
    //   var link = widget.dataSourceItemOutSide[i];
    //   dataSourceOld.add(link);
    // }
    if (kDebugMode) {
      print("dataSourceItem after init from widget");
    }
    countTrue(dataSourceItem);
  }

  void _itemChange(String itemValue, bool isSelected, int index) {
    // dataSourceCheck[index].isChecked = isSelected;
    // setState(() {});
    filteredDataSourceCheck[index].isChecked = isSelected;
    setState(() {});
  }

  void _cancel() {
    _filter.clear();
    filteredDataSourceCheck = dataSourceItem;
    Navigator.pop(context);
  }

  void _submit() {
    _filter.clear();
    dataSourceItem = filteredDataSourceCheck;
    widget.didChoose(dataSourceItem);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    if (kDebugMode) {
      print("_MultiSelectState BuildContext clicked");
    }
    if (_searchText.isNotEmpty) {
      var tempList = <ItemCheckBoxModel>[];
      for (var item in filteredDataSourceCheck) {
        if (item.name.toLowerCase().contains(_searchText.toLowerCase())) {
          tempList.add(item);
        }
      }
      filteredDataSourceCheck = tempList;
    }

    return AlertDialog(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10))),
      // scrollable : true,//bo vao tach luon!
      // title: Text(widget.titleDropdown),
      title: TextField(
        controller: _filter,
        decoration: InputDecoration(
          prefixIcon: const Icon(Icons.search),
          hintText: 'Search',
          suffixIcon: IconButton(
            iconSize: 14,
            onPressed: _filter.clear,
            icon: const Icon(Icons.clear),
          ),
        ),
      ),
      content: SizedBox(
        width: double.maxFinite,
        // height: double.maxFinite,
        child: ListView.builder(
            shrinkWrap: true,
            itemCount: filteredDataSourceCheck.length,
            addAutomaticKeepAlives: true,
            itemBuilder: (context, index) {
              return CheckboxListTile(
                  value: (filteredDataSourceCheck[index].isChecked),
                  title: Text(filteredDataSourceCheck[index].name),
                  controlAffinity: ListTileControlAffinity.leading,
                  onChanged: (isChecked) {
                    _itemChange(
                        filteredDataSourceCheck[index].name, isChecked!, index);
                  });
            }),
      ),
      actions: [
        TextButton(
          child: const Text('Cancel'),
          onPressed: _cancel,
        ),
        TextButton(
          child: const Text('Confirm'),
          onPressed: _submit,
        ),
        // ElevatedButton(
        //   child: const Text('Submit'),
        //   onPressed: _submit,
        // ),
      ],
    );
  }
}
