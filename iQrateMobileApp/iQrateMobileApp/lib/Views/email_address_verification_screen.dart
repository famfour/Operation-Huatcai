import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/email_address_verification_controller.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';

class EmailAddressVerificationScreen extends StatelessWidget {
  const EmailAddressVerificationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double windowWidth = MediaQuery.of(context).size.width; //get screen width
    double windowHeight = MediaQuery.of(context).size.height; //get screen height
    final EmailAddressVerificationScreenController
        emailAddressVerificationScreenController =
        Get.put(EmailAddressVerificationScreenController()); //initialize email address verification controller
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: SafeArea(
          child: SingleChildScrollView(
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 25.0, horizontal: 18),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: windowHeight * 0.03,
                  ),
                  Center(
                    child: SvgPicture.asset(
                      "assets/images/verify_email.svg",
                    ),
                  ),
                  SizedBox(
                    height: windowHeight * 0.12,
                  ),
                  Text("Please verify your email address",
                      style: TextStyles.introScreenTitlesSmall),
                  SizedBox(
                    height: windowHeight * 0.03,
                  ),
                  Text(
                    "An email has been sent to your registered email address. Please click and verify.",
                    style: TextStyles.introScreenDescriptions,
                  ),
                  SizedBox(
                    height: windowHeight * 0.08,
                  ),
                  Center(
                    child: RequireTextField(
                        type: Type.otp,
                        controller: emailAddressVerificationScreenController
                            .emailVerificationOtpController),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        "Resend in 20 seconds",
                        style: TextStyles.headingSubtitleStyle,
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      const SizedBox(
                          height: 15,
                          width: 15,
                          child: CircularProgressIndicator(
                            color: Colors.deepOrange,
                            strokeWidth: 2,
                          ))
                    ],
                  ),
                  SizedBox(
                    height: windowHeight * 0.08,
                  ),
                  PrimaryButton(
                    windowHeight: windowHeight,
                    windowWidth: windowWidth,
                    buttonTitle: "Continue",
                    onPressed: () async {
                      emailAddressVerificationScreenController.onSubmit(); // function submit for email address verification
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
