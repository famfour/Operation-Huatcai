// ignore_for_file: invalid_use_of_protected_member

import 'dart:developer';

import 'package:container_tab_indicator/container_tab_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/email_log_controller.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Utils/config.dart';
import 'package:iqrate/Views/email_log_banker_view.dart';
import 'package:iqrate/Views/email_log_law_firm_view.dart';
import 'package:iqrate/Views/email_log_lead_view.dart';
import 'package:iqrate/Model/response_model.dart/fetch_lead_list_response_model.dart';
import '../Service/GetControllers.dart';

class EmailLogView extends StatefulWidget {
  final Result data;

  const EmailLogView({Key? key, required this.data}) : super(key: key);

  @override
  _EmailLogViewState createState() => _EmailLogViewState();
}

class _EmailLogViewState extends State<EmailLogView> {
  late double windowHeight;
  late double windowWidth;

  EmailLogController emailLogController =
      GetControllers.shared.getEmailLogController();

  @override
  void initState() {
    log('message11 email');
    initData();
    super.initState();
  }

  late Result leadData;

  initData() async {
    leadData = widget.data;
    emailLogController.leadID = leadData.id.toString();

    await emailLogController.getEmailLogs(leadData.id.toString());
  }

  var leadProfileController = GetControllers.shared.getLeadProfileController();

  @override
  Widget build(BuildContext context) {
    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.width;
    return Obx(
      () => IgnorePointer(
        ignoring: !leadProfileController.clientPDPAStatus.value,
        child: expandableCard(
          "Email Log",
          false,
        ),
      ),
    );
  }

  Widget expandableCard(String title, bool isDone) {
    return GestureDetector(
      onTap: () {
        log('11 email exapnd');
        emailLogController.isExpand.toggle();
        // selectedPackagesController.getSelectedPackages(leadData.id.toString());
        GetControllers.shared.getGeneratePackagesController().isExpand.value =
            false;
        GetControllers.shared.getLoanDetailsController().isExpand.value = false;
        GetControllers.shared.getPropertyDetailsController().isExpand.value =
            false;
        GetControllers.shared.getLeadProfileController().isExpand.value = false;
        GetControllers.shared.getBankSubmissionController().isExpand.value =
            false;
        GetControllers.shared
            .getSubmitForPayoutViewController()
            .isExpand
            .value = false;
        GetControllers.shared.getLawFirmSubmissionController().isExpand.value =
            false;
        GetControllers.shared.getEmailLogController().isExpand.value =
            emailLogController.isExpand.value;
        GetControllers.shared.getSubmitCoBrokeController().isExpand.value =
            false;
        if (emailLogController.isExpand.value == true) {
          emailLogController
              .getEmailLogsWhenExpanded(widget.data.id.toString());
        }
      },
      child: Obx(() => Column(
            children: [
              Container(
                margin: EdgeInsets.only(bottom: windowWidth * 0.02),
                padding: EdgeInsets.all(windowWidth * 0.05),
                decoration: BoxDecoration(
                    boxShadow: const <BoxShadow>[
                      BoxShadow(
                          color: Colors.black54,
                          blurRadius: 5.0,
                          offset: Offset(0.0, 0.3))
                    ],
                    color: !leadProfileController.clientPDPAStatus.value
                        ? AppColors.greyDEDEDE
                        : Colors.white),
                child: Row(
                  children: [
                    Text(title, style: TextStyles.basicTextStyle),
                    const Spacer(),
                    // SvgPicture.asset(
                    //   isDone
                    //       ? Assets.manageLoanDone
                    //       : Assets.manageLoanIncomplete,
                    //   height: 24,
                    //   width: 24,
                    // ),
                    SizedBox(width: windowWidth * 0.03),
                    Icon(
                      emailLogController.isExpand.value
                          ? Icons.keyboard_arrow_up_rounded
                          : Icons.keyboard_arrow_down,
                      size: 30,
                    )
                  ],
                ),
              ),
              emailLogController.isExpand.value
                  ? DefaultTabController(
                      length: 4,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: windowHeight * 0.03,
                          ),
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: const BorderRadius.all(
                                    Radius.circular(30.0)),
                                color: AppColors.tabBg),
                            margin: EdgeInsets.symmetric(
                                horizontal: windowWidth * 0.04),
                            child: SizedBox(
                              height: 50,
                              child: TabBar(
                                unselectedLabelColor: Colors.red,
                                labelColor: Colors.white,
                                tabs: [
                                  Text('All', style: TextStyles.tabTitle),
                                  Text('Lead', style: TextStyles.tabTitle),
                                  Text('Banker', style: TextStyles.tabTitle),
                                  Text('Law Firm', style: TextStyles.tabTitle),
                                ],
                                indicator: const ContainerTabIndicator(
                                  width: 96,
                                  height: 40,
                                  radius:
                                      BorderRadius.all(Radius.circular(20.0)),
                                  //borderWidth: 2.0,
                                  //borderColor: Colors.black,
                                  color: Colors.red,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: windowHeight * 0.01,
                          ),
                          SizedBox(
                            height: windowHeight * 0.6,
                            child: const TabBarView(
                              children: [
                                All(),
                                LeadView(),
                                BankerView(),
                                LawFirmView(),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: windowHeight * 0.03,
                          ),
                        ],
                      ),
                    )
                  : Container()
            ],
          )),
    );
  }
}

class All extends StatelessWidget {
  const All({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;

    EmailLogController emailLogController =
        GetControllers.shared.getEmailLogController();

    return SingleChildScrollView(
      child: Column(
        children: [
          emailLogController.emailLogListModelAll.isNotEmpty
              ? ListView.builder(
                  primary: true,
                  shrinkWrap: true,
                  //padding: const EdgeInsets.all(8),
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: emailLogController.emailLogListModelAll.length,
                  itemBuilder: (BuildContext context, int index) {
                    var item = emailLogController.emailLogListModelAll.value
                        .elementAt(index);
                    return GestureDetector(
                      onTap: () {
                        log(item.name.toString());

                        if (item.emailType.toString() == "lead") {
                          Get.to(() => EmailLogLeadView(
                                data: item,
                              ));
                        } else if (item.emailType.toString() == "banker") {
                          Get.to(() => EmailLogBankerView(
                                data: item,
                              ));
                        } else if (item.emailType == "law_firm") {
                          Get.to(() => EmailLogLawFirmView(
                                data: item,
                              ));
                        }
                      },
                      child: Container(
                        padding: EdgeInsets.all(windowWidth * 0.03),
                        height: windowHeight * 0.13,
                        child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                          elevation: 10,
                          child: Container(
                            padding: EdgeInsets.all(windowWidth * 0.03),
                            margin: EdgeInsets.symmetric(
                                horizontal: windowWidth * 0.04),
                            decoration: const BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(30.0)),
                              //color: AppColors.tabBg
                            ),
                            child: Row(
                              children: [
                                Text(
                                  AppConfig.getDateTimeFormatForEmailLog(
                                      item.created!),
                                  style: TextStyles.leadsTextStyle,
                                ),
                                const Spacer(),
                                const Icon(
                                  Icons.arrow_forward_ios_rounded,
                                  size: 17,
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  })
              : Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                          height: windowHeight * 0.15,
                          child: SvgPicture.asset('assets/images/nodata.svg')),
                      Text(
                        'No data to show',
                        style: TextStyles.bankSubmissionCardTitleDisabled,
                      )
                    ],
                  ),
                )
        ],
      ),
    );
  }
}

class LeadView extends StatelessWidget {
  const LeadView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    EmailLogController emailLogController =
        GetControllers.shared.getEmailLogController();

    return SingleChildScrollView(
      child: Column(
        children: [
          emailLogController.emailLogListModelLead.isNotEmpty
              ? ListView.builder(
                  primary: true,
                  shrinkWrap: true,
                  //padding: const EdgeInsets.all(8),
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: emailLogController.emailLogListModelLead.length,
                  itemBuilder: (BuildContext context, int index) {
                    var item = emailLogController.emailLogListModelLead.value
                        .elementAt(index);
                    return GestureDetector(
                      onTap: () {
                        log(item.name.toString());

                        Get.to(() => EmailLogLeadView(
                              data: item,
                            ));
                      },
                      child: Container(
                        padding: EdgeInsets.all(windowWidth * 0.03),
                        height: windowHeight * 0.13,
                        child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                          elevation: 10,
                          child: Container(
                            padding: EdgeInsets.all(windowWidth * 0.03),
                            margin: EdgeInsets.symmetric(
                                horizontal: windowWidth * 0.04),
                            decoration: const BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(30.0)),
                              //color: AppColors.tabBg
                            ),
                            child: Row(
                              children: [
                                Text(
                                  AppConfig.getDateTimeFormatForEmailLog(
                                      item.created!),
                                  style: TextStyles.leadsTextStyle,
                                ),
                                const Spacer(),
                                const Icon(
                                  Icons.arrow_forward_ios_rounded,
                                  size: 17,
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  })
              : Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                          height: windowHeight * 0.15,
                          child: SvgPicture.asset('assets/images/nodata.svg')),
                      Text(
                        'No data to show',
                        style: TextStyles.bankSubmissionCardTitleDisabled,
                      )
                    ],
                  ),
                )
        ],
      ),
    );
  }
}

class BankerView extends StatelessWidget {
  const BankerView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    EmailLogController emailLogController =
        GetControllers.shared.getEmailLogController();
    return SingleChildScrollView(
      child: Column(
        children: [
          emailLogController.emailLogListModelBanker.isNotEmpty
              ? ListView.builder(
                  primary: true,
                  shrinkWrap: true,
                  //padding: const EdgeInsets.all(8),
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: emailLogController.emailLogListModelBanker.length,
                  itemBuilder: (BuildContext context, int index) {
                    var item = emailLogController.emailLogListModelBanker.value
                        .elementAt(index);
                    return GestureDetector(
                      onTap: () {
                        log(item.name.toString());

                        Get.to(() => EmailLogBankerView(
                              data: item,
                            ));
                      },
                      child: Container(
                        padding: EdgeInsets.all(windowWidth * 0.03),
                        height: windowHeight * 0.13,
                        child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                          elevation: 10,
                          child: Container(
                            padding: EdgeInsets.all(windowWidth * 0.03),
                            margin: EdgeInsets.symmetric(
                                horizontal: windowWidth * 0.04),
                            decoration: const BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(30.0)),
                              //color: AppColors.tabBg
                            ),
                            child: Row(
                              children: [
                                Text(
                                  // "12/11/2022 - 09:50am",
                                  AppConfig.getDateTimeFormatForEmailLog(
                                      item.created!),
                                  style: TextStyles.leadsTextStyle,
                                ),
                                const Spacer(),
                                const Icon(
                                  Icons.arrow_forward_ios_rounded,
                                  size: 17,
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  })
              : Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                          height: windowHeight * 0.15,
                          child: SvgPicture.asset('assets/images/nodata.svg')),
                      Text(
                        'No data to show',
                        style: TextStyles.bankSubmissionCardTitleDisabled,
                      )
                    ],
                  ),
                )
        ],
      ),
    );
  }
}

class LawFirmView extends StatelessWidget {
  const LawFirmView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;

    EmailLogController emailLogController =
        GetControllers.shared.getEmailLogController();

    return SingleChildScrollView(
      child: Column(
        children: [
          emailLogController.emailLogListModelLawFirm.isNotEmpty
              ? ListView.builder(
                  primary: true,
                  shrinkWrap: true,
                  //padding: const EdgeInsets.all(8),
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: emailLogController.emailLogListModelLawFirm.length,
                  itemBuilder: (BuildContext context, int index) {
                    var item = emailLogController.emailLogListModelLawFirm.value
                        .elementAt(index);
                    return InkWell(
                      onTap: () {
                        Get.to(() => EmailLogLawFirmView(
                              data: item,
                            ));
                      },
                      child: Container(
                        padding: EdgeInsets.all(windowWidth * 0.03),
                        height: windowHeight * 0.13,
                        child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                          elevation: 10,
                          child: Container(
                            padding: EdgeInsets.all(windowWidth * 0.03),
                            margin: EdgeInsets.symmetric(
                                horizontal: windowWidth * 0.04),
                            decoration: const BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(30.0)),
                              //color: AppColors.tabBg
                            ),
                            child: Row(
                              children: [
                                //"Email sent on 20th November 2021"
                                Text(
                                  "Email sent on " +
                                      AppConfig.getDateFormatForEmailLog(
                                          item.created!),
                                  style: TextStyles.leadsTextStyle,
                                ),
                                const Spacer(),
                                const Icon(
                                  Icons.arrow_forward_ios_rounded,
                                  size: 17,
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  })
              : Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                          height: windowHeight * 0.15,
                          child: SvgPicture.asset('assets/images/nodata.svg')),
                      Text(
                        'No data to show',
                        style: TextStyles.bankSubmissionCardTitleDisabled,
                      )
                    ],
                  ),
                )
        ],
      ),
    );
  }
}
