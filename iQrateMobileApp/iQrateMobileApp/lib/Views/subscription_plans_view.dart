import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/subscription_plans_view_controller.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Widgets/default_appbar.dart';
import 'package:iqrate/Widgets/primary_button.dart';

// ignore: must_be_immutable
class SubscriptionPlansView extends StatelessWidget {
  SubscriptionPlansView({Key? key}) : super(key: key);

  SubscriptionPlansViewController subscriptionPlansViewController =
      Get.put(SubscriptionPlansViewController());

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: DefaultAppBar(
          title: 'Subscription Plans', windowHeight: windowHeight * 0.09),
      body: Padding(
        padding:
            EdgeInsets.fromLTRB(windowWidth * 0.02, 0, 0, windowWidth * 0.02),
        child: ListView(children: [
          PeicingTable(
            windowHeight: windowHeight,
            windowWidth: windowWidth,
            controller: subscriptionPlansViewController,
          ),
          SubscriptionTermPlanSelectionSection(
            windowHeight: windowHeight,
            windowWidth: windowWidth,
          ),
          Padding(
            padding: EdgeInsets.only(
                left: windowWidth * .06,
                right: windowWidth * .06,
                top: windowWidth * .03,
                bottom: windowWidth * .06),
            child: Obx(
              () => subscriptionPlansViewController.subscribedPlan.value == 0 &&
                      subscriptionPlansViewController.selectedValue.value == 0
                  ? Container()
                  : PrimaryButton(
                      windowHeight: windowHeight,
                      windowWidth: windowWidth,
                      buttonTitle: 'Subscribe Now',
                      onPressed: () {
                        debugPrint(">>>>>>:: onTapSubscribeNow");
                        subscriptionPlansViewController.onTapSubscribeNow(
                          amount: subscriptionPlansViewController.getAmount(),
                          planType:
                              subscriptionPlansViewController.getPlanType(),
                          planTerm:
                              subscriptionPlansViewController.getPlanTerm(),
                          planId: subscriptionPlansViewController.getPlanId(),
                        );
                      },
                    ),
            ),
          )
        ]),
      ),
    );
  }
}

// ignore: must_be_immutable
class PeicingTable extends StatelessWidget {
  PeicingTable(
      {Key? key,
      required this.windowWidth,
      required this.windowHeight,
      required this.controller})
      : super(key: key);
  double windowWidth;
  double windowHeight;
  SubscriptionPlansViewController controller;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(
          windowWidth * 0.02, windowWidth * 0.02, windowWidth * 0.02, 0),
      child: Column(
        children: [
          Obx(
            () => titleRow(
                Container(),
                Opacity(
                  opacity: controller.selectedValue.value == 0 ? 1 : 0.3,
                  child: Container(
                      decoration: const BoxDecoration(
                          gradient: LinearGradient(
                              colors: [Color(0xffe58415), Color(0xfff84e46)]),
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10))),
                      child: Padding(
                        padding:
                            EdgeInsets.symmetric(vertical: windowWidth * 0.02),
                        child: Center(
                            child: Text(
                          'Basic',
                          style: TextStyles.headingTextStyleWhite,
                        )),
                      )),
                ),
                Opacity(
                  opacity: controller.selectedValue.value == 1 ? 1 : 0.3,
                  child: Container(
                      decoration: const BoxDecoration(
                          gradient: LinearGradient(
                              colors: [Color(0xffe58415), Color(0xfff84e46)]),
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10))),
                      child: Padding(
                        padding:
                            EdgeInsets.symmetric(vertical: windowWidth * 0.02),
                        child: Center(
                            child: Text(
                          'Premium',
                          style: TextStyles.headingTextStyleWhite,
                        )),
                      )),
                ),
                Opacity(
                  opacity: controller.selectedValue.value == 2 ? 1 : 0.3,
                  child: Container(
                      decoration: const BoxDecoration(
                          gradient: LinearGradient(
                              colors: [Color(0xffe58415), Color(0xfff84e46)]),
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10))),
                      child: Padding(
                        padding:
                            EdgeInsets.symmetric(vertical: windowWidth * 0.02),
                        child: Center(
                            child: Text(
                          'Premium+',
                          style: TextStyles.headingTextStyleWhite,
                        )),
                      )),
                )),
          ),
          Obx(
            () => dataRow(
                Column(
                  children: [
                    Text(
                      "Earn % 0f Bank's Referral Fee.",
                      style: TextStyles.titleTextStyle,
                    ),
                    Text(
                      '(Based on 0.15% Loan Amount Referred)',
                      style: TextStyles.fadedTextStyle,
                    ),
                  ],
                ),
                Text(
                  ' \n \n',
                  style: TextStyles.titleTextStyle,
                ),
                Text(
                  ' \n \n',
                  style: TextStyles.titleTextStyle,
                ),
                Text(
                  ' \n \n',
                  style: TextStyles.titleTextStyle,
                ),
                showDivider: false),
          ),
          Obx(
            () => dataRow(
                Row(
                  children: [
                    const Icon(
                      Icons.circle,
                      size: 10,
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Text(
                      'Apply Loan for Others',
                      style: TextStyles.descriptionTextStyle,
                    ),
                  ],
                ),
                Text(
                  '80%',
                  style: TextStyles.titleTextStyle,
                ),
                Text(
                  '82%',
                  style: TextStyles.titleTextStyle,
                ),
                Text(
                  '85%',
                  style: TextStyles.titleTextStyle,
                ),
                showDivider: false),
          ),
          Obx(
            () => dataRow(
              Row(
                children: [
                  const Icon(
                    Icons.circle,
                    size: 10,
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  Text(
                    'Co-Broke',
                    style: TextStyles.descriptionTextStyle,
                  ),
                ],
              ),
              Text(
                '40%',
                style: TextStyles.titleTextStyle,
              ),
              Text(
                '41%',
                style: TextStyles.titleTextStyle,
              ),
              Text(
                '42.5%',
                style: TextStyles.titleTextStyle,
              ),
            ),
          ),
          Obx(
            () => dataRow(
              Text(
                'Standard Rates',
                style: TextStyles.titleTextStyle,
              ),
              const Icon(Icons.check),
              const Icon(Icons.check),
              const Icon(Icons.check),
            ),
          ),
          /*    Obx(
            () => dataRow(
              Text(
                'Standard Rates',
                style: TextStyles.titleTextStyle,
              ),
              const Icon(Icons.check),
              const Icon(Icons.check),
              const Icon(Icons.check),
            ),
          ),*/
          Obx(
            () => dataRow(
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Exclusive Rates',
                    style: TextStyles.titleTextStyle,
                  ),
                  const SizedBox(height: 5),
                  Text(
                    '(Rates Exclusive to iQrate)',
                    style: TextStyles.fadedTextStyle,
                  ),
                ],
              ),
              const Icon(Icons.close),
              const Icon(Icons.check),
              const Icon(Icons.check),
            ),
          ),
          Obx(
            () => dataRow(
                Text(
                  '',
                  style: TextStyles.titleTextStyle,
                ),
                const SizedBox(),
                const SizedBox(),
                const SizedBox(),
                showDivider: false),
          ),
          Obx(
            () => dataRow(
                Text(
                  'Calculators',
                  style: TextStyles.titleTextStyle,
                ),
                const SizedBox(),
                const SizedBox(),
                const SizedBox(),
                showDivider: false),
          ),
          Obx(
            () => dataRow(
              Row(
                children: [
                  const Icon(
                    Icons.circle,
                    size: 10,
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  Expanded(
                    child: Text(
                      'Property purchase affordability',
                      overflow: TextOverflow.visible,
                      style: TextStyles.descriptionTextStyle,
                    ),
                  ),
                ],
              ),
              const Icon(Icons.close),
              const Icon(Icons.check),
              const Icon(Icons.check),
            ),
          ),
          Obx(
            () => dataRow(
              Row(
                children: [
                  const Icon(
                    Icons.circle,
                    size: 10,
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  Expanded(
                    child: Text(
                      'Loan Eligibility',
                      overflow: TextOverflow.visible,
                      style: TextStyles.descriptionTextStyle,
                    ),
                  ),
                ],
              ),
              const Icon(Icons.close),
              const Icon(Icons.check),
              const Icon(Icons.check),
            ),
          ),
          Obx(
            () => dataRow(
              Row(
                children: [
                  const Icon(
                    Icons.circle,
                    size: 10,
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  Expanded(
                    child: Text(
                      'Total savings on Refinancing',
                      overflow: TextOverflow.visible,
                      style: TextStyles.descriptionTextStyle,
                    ),
                  ),
                ],
              ),
              const Icon(Icons.close),
              const Icon(Icons.check),
              const Icon(Icons.check),
            ),
          ),
          Obx(
            () => dataRow(
              Row(
                children: [
                  const Icon(
                    Icons.circle,
                    size: 10,
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  Expanded(
                    child: Text(
                      'Equity Loan',
                      overflow: TextOverflow.visible,
                      style: TextStyles.descriptionTextStyle,
                    ),
                  ),
                ],
              ),
              const Icon(Icons.close),
              const Icon(Icons.check),
              const Icon(Icons.check),
            ),
          ),
          Obx(
            () => dataRow(
              Row(
                children: [
                  const Icon(
                    Icons.circle,
                    size: 10,
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  Expanded(
                    child: Text(
                      "Buyer's Stamp Duty",
                      overflow: TextOverflow.visible,
                      style: TextStyles.descriptionTextStyle,
                    ),
                  ),
                ],
              ),
              const Icon(Icons.check),
              const Icon(Icons.check),
              const Icon(Icons.check),
            ),
          ),
          Obx(
            () => dataRow(
              Row(
                children: [
                  const Icon(
                    Icons.circle,
                    size: 10,
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  Expanded(
                    child: Text(
                      "Seller's Stamp Duty",
                      overflow: TextOverflow.visible,
                      style: TextStyles.descriptionTextStyle,
                    ),
                  ),
                ],
              ),
              const Icon(Icons.check),
              const Icon(Icons.check),
              const Icon(Icons.check),
            ),
          ),
          Obx(
            () => dataRow(
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      const Icon(
                        Icons.circle,
                        size: 10,
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      Expanded(
                        child: Text(
                          'Mortgage Repayment',
                          overflow: TextOverflow.visible,
                          style: TextStyles.descriptionTextStyle,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 5),
                  Text(
                    '    (Completed Property)',
                    overflow: TextOverflow.visible,
                    style: TextStyles.fadedTextStyle,
                  ),
                ],
              ),
              const Icon(Icons.check),
              const Icon(Icons.check),
              const Icon(Icons.check),
            ),
          ),
          Obx(
            () => dataRow(
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      const Icon(
                        Icons.circle,
                        size: 10,
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      Expanded(
                        child: Text(
                          'Mortgage Repayment',
                          overflow: TextOverflow.visible,
                          style: TextStyles.descriptionTextStyle,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 5),
                  Text(
                    '    (Building Under Construction)',
                    overflow: TextOverflow.visible,
                    style: TextStyles.fadedTextStyle,
                  ),
                ],
              ),
              const Icon(Icons.check),
              const Icon(Icons.check),
              const Icon(Icons.check),
            ),
          ),
          Obx(
            () => dataRow(
              Text(
                'No. of Lead Storage',
                style: TextStyles.titleTextStyle,
              ),
              Text(
                '5',
                style: TextStyles.titleTextStyle,
              ),
              Text(
                '100',
                style: TextStyles.titleTextStyle,
              ),
              Text(
                'Unlimited',
                style: TextStyles.titleTextStyle,
              ),
            ),
          ),
          Obx(
            () => dataRow(
                Text(
                  'Web-Based CRM',
                  style: TextStyles.titleTextStyle,
                ),
                const Icon(Icons.close),
                const Icon(Icons.close),
                const Icon(Icons.check),
                showDivider: false),
          ),
          Obx(
            () => dataRow(
                Row(
                  children: [
                    const Icon(
                      Icons.circle,
                      size: 10,
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      child: Text(
                        'Refinance Monitoring System',
                        overflow: TextOverflow.visible,
                        style: TextStyles.descriptionTextStyle,
                      ),
                    ),
                  ],
                ),
                Container(),
                Container(),
                Container(),
                showDivider: false),
          ),
          Obx(
            () => dataRow(
                Row(
                  children: [
                    const Icon(
                      Icons.circle,
                      size: 10,
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      child: Text(
                        'Leads Follow up Reminder',
                        overflow: TextOverflow.visible,
                        style: TextStyles.descriptionTextStyle,
                      ),
                    ),
                  ],
                ),
                Container(),
                Container(),
                Container(),
                showDivider: false),
          ),
          Obx(
            () => dataRow(
                Row(
                  children: [
                    const Icon(
                      Icons.circle,
                      size: 10,
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      child: Text(
                        'Task Manager',
                        overflow: TextOverflow.visible,
                        style: TextStyles.descriptionTextStyle,
                      ),
                    ),
                  ],
                ),
                Container(),
                Container(),
                Container(),
                showDivider: false),
          ),
          Obx(
            () => lastRow(
              Row(
                children: [
                  const Icon(
                    Icons.circle,
                    size: 10,
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  Text(
                    'Lead Generation ',
                    overflow: TextOverflow.visible,
                    style: TextStyles.descriptionTextStyle,
                  ),
                  Expanded(
                    child: Text(
                      '(Coming soon)',
                      overflow: TextOverflow.visible,
                      style: TextStyles.descriptionTextStyleGrey,
                    ),
                  ),
                ],
              ),
              Container(),
              Container(),
              Container(),
            ),
          ),
        ],
      ),
    );
  }

  Widget titleRow(
      Widget cellOne, Widget cellTwo, Widget cellThree, Widget cellFour) {
    return Row(
      children: [
        SizedBox(width: windowWidth * 0.4, child: cellOne),
        SizedBox(
            width: windowWidth * 0.18,
            child: GestureDetector(
                onTap: () {
                  controller.onTapUpdatePlan(0);
                  controller.finalSubscriptionTermSelected.value = 0;
                },
                child: cellTwo)),
        SizedBox(
            width: windowWidth * 0.18,
            child: GestureDetector(
                onTap: () {
                  controller.onTapUpdatePlan(1);
                  controller.finalSubscriptionTermSelected.value = 0;
                },
                child: cellThree)),
        SizedBox(
            width: windowWidth * 0.18,
            child: GestureDetector(
                onTap: () {
                  controller.onTapUpdatePlan(2);
                  controller.finalSubscriptionTermSelected.value = 0;
                },
                child: cellFour))
      ],
    );
  }

  Widget dataRow(
      Widget cellOne, Widget cellTwo, Widget cellThree, Widget cellFour,
      {bool showDivider = true}) {
    return Column(
      children: [
        Row(
          children: [
            // For cell one
            SizedBox(width: windowWidth * 0.4, child: cellOne),
            // For cell two
            GestureDetector(
              onTap: () {
                controller.onTapUpdatePlan(0);
                controller.finalSubscriptionTermSelected.value = 0;
              },
              child: Container(
                  decoration: BoxDecoration(
                      border: Border(
                    left: controller.selectedValue.value == 0
                        ? const BorderSide(color: Colors.amber, width: 3)
                        : const BorderSide(color: Colors.amber),
                    right: controller.selectedValue.value == 0
                        ? const BorderSide(color: Colors.amber, width: 1.5)
                        : const BorderSide(color: Colors.amber),
                  )),
                  width: windowWidth * 0.18,
                  child: Center(
                      child: Padding(
                    padding: EdgeInsets.symmetric(vertical: windowWidth * 0.02),
                    child: Opacity(
                        opacity: controller.selectedValue.value == 0 ? 1 : 0.3,
                        child: cellTwo),
                  ))),
            ),
            // For cell three
            GestureDetector(
              onTap: () {
                controller.onTapUpdatePlan(1);
                controller.finalSubscriptionTermSelected.value = 0;
              },
              child: Container(
                  decoration: BoxDecoration(
                      border: Border(
                    left: controller.selectedValue.value == 1
                        ? const BorderSide(color: Colors.amber, width: 1.5)
                        : const BorderSide(color: Colors.amber),
                    right: controller.selectedValue.value == 1
                        ? const BorderSide(color: Colors.amber, width: 1.5)
                        : const BorderSide(color: Colors.amber),
                  )),
                  width: windowWidth * 0.18,
                  child: Center(
                      child: Padding(
                    padding: EdgeInsets.symmetric(vertical: windowWidth * 0.02),
                    child: Opacity(
                        opacity: controller.selectedValue.value == 1 ? 1 : 0.3,
                        child: cellThree),
                  ))),
            ),
            // For cell four
            GestureDetector(
              onTap: () {
                controller.onTapUpdatePlan(2);
                controller.finalSubscriptionTermSelected.value = 0;
              },
              child: Container(
                  decoration: BoxDecoration(
                      border: Border(
                    left: controller.selectedValue.value == 2
                        ? const BorderSide(color: Colors.amber, width: 1.5)
                        : const BorderSide(color: Colors.amber),
                    right: controller.selectedValue.value == 2
                        ? const BorderSide(color: Colors.amber, width: 3)
                        : const BorderSide(color: Colors.amber),
                  )),
                  width: windowWidth * 0.18,
                  child: Center(
                      child: Padding(
                    padding: EdgeInsets.symmetric(vertical: windowWidth * 0.02),
                    child: Opacity(
                        opacity: controller.selectedValue.value == 2 ? 1 : 0.3,
                        child: cellFour),
                  ))),
            )
          ],
        ),
        showDivider
            ? SvgPicture.asset(
                "assets/icons/line.svg",
                color: Colors.grey,
              )
            : Container(),
      ],
    );
  }

  Widget lastRow(
      Widget cellOne, Widget cellTwo, Widget cellThree, Widget cellFour) {
    return Row(
      children: [
        // For cell one
        SizedBox(width: windowWidth * 0.4, child: cellOne),
        // For cell two
        Container(
            decoration: BoxDecoration(
                border: Border(
              bottom: controller.selectedValue.value == 0
                  ? const BorderSide(color: Colors.amber, width: 3)
                  : const BorderSide(color: Colors.amber),
              left: controller.selectedValue.value == 0
                  ? const BorderSide(color: Colors.amber, width: 3)
                  : const BorderSide(color: Colors.amber),
              right: controller.selectedValue.value == 0
                  ? const BorderSide(color: Colors.amber, width: 1.5)
                  : const BorderSide(color: Colors.amber),
            )),
            width: windowWidth * 0.18,
            child: Center(
                child: Padding(
              padding: EdgeInsets.symmetric(vertical: windowWidth * 0.02),
              child: cellTwo,
            ))),
        // For cell three
        Container(
            decoration: BoxDecoration(
                border: Border(
              bottom: controller.selectedValue.value == 1
                  ? const BorderSide(color: Colors.amber, width: 3)
                  : const BorderSide(color: Colors.amber),
              left: controller.selectedValue.value == 1
                  ? const BorderSide(color: Colors.amber, width: 1.5)
                  : const BorderSide(color: Colors.amber),
              right: controller.selectedValue.value == 1
                  ? const BorderSide(color: Colors.amber, width: 1.5)
                  : const BorderSide(color: Colors.amber),
            )),
            width: windowWidth * 0.18,
            child: Center(
                child: Padding(
              padding: EdgeInsets.symmetric(vertical: windowWidth * 0.02),
              child: cellThree,
            ))),
        // For cell four
        Container(
            decoration: BoxDecoration(
                border: Border(
              bottom: controller.selectedValue.value == 2
                  ? const BorderSide(color: Colors.amber, width: 3)
                  : const BorderSide(color: Colors.amber),
              left: controller.selectedValue.value == 2
                  ? const BorderSide(color: Colors.amber, width: 1.5)
                  : const BorderSide(color: Colors.amber),
              right: controller.selectedValue.value == 2
                  ? const BorderSide(color: Colors.amber, width: 1.5)
                  : const BorderSide(color: Colors.amber),
            )),
            width: windowWidth * 0.18,
            child: Center(
                child: Padding(
              padding: EdgeInsets.symmetric(vertical: windowWidth * 0.02),
              child: cellFour,
            )))
      ],
    );
  }
}

// ignore: must_be_immutable
class SubscriptionTermPlanSelectionSection extends StatelessWidget {
  SubscriptionTermPlanSelectionSection(
      {Key? key, required this.windowHeight, required this.windowWidth})
      : super(key: key);
  double windowWidth;
  double windowHeight;
  SubscriptionPlansViewController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(
        windowWidth * 0.05,
        windowWidth * 0.1,
        windowWidth * 0.05,
        windowWidth * 0.03,
      ),
      child: Obx(
        () => Column(
          children: [
            controller.selectedValue.value == 0
                ? AbsorbPointer(
                    absorbing: controller.planTermPriceDaily.value.contains(
                        (int.parse(controller.subscribedPlanAmount.value) / 100)
                            .toString()),
                    child: GestureDetector(
                      onTap: () {
                        controller.subscriptionTermSelection(0);
                      },
                      child: Card(
                        elevation: controller.planTermPriceDaily.value.contains(
                                (int.parse(controller
                                            .subscribedPlanAmount.value) /
                                        100)
                                    .toString())
                            ? 0
                            : controller.selectedValue.value == 0
                                ? 7
                                : controller.finalSubscriptionTermSelected
                                            .value ==
                                        0
                                    ? 0
                                    : 7,
                        shape: const RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(20))),
                        child: Container(
                          width: windowWidth,
                          decoration: BoxDecoration(
                              color: /*controller.selectedValue.value == 0
                            ? Colors.white
                            : */
                                  controller.finalSubscriptionTermSelected
                                              .value ==
                                          0
                                      ? AppColors.kPrimaryColor
                                      : Colors.white,
                              border: controller.selectedValue.value == 0
                                  ? null
                                  : controller.finalSubscriptionTermSelected
                                              .value ==
                                          0
                                      ? Border.all(color: Colors.black)
                                      : null,
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(20))),
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                vertical: windowHeight * 0.02),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Flexible(
                                  flex: 1,
                                  child: SizedBox(
                                    width: windowWidth * .2,
                                  ),
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    controller.selectedValue.value == 0
                                        ? Container()
                                        : Text(
                                            'Daily Subscription',
                                            style: controller
                                                        .finalSubscriptionTermSelected
                                                        .value ==
                                                    0
                                                ? TextStyles
                                                    .planTermSelectionTextStyle
                                                : TextStyles
                                                    .redPlanTermSelectionTextStyle,
                                          ),
                                    Text(
                                      controller.planTermPriceDaily.value,
                                      style: /*controller.selectedValue.value == 0
                                    ? TextStyles
                                        .blackPlanTermSelectionPriceTextStyle
                                    :*/
                                          controller.finalSubscriptionTermSelected
                                                      .value ==
                                                  0
                                              ? TextStyles
                                                  .planTermSelectionPriceTextStyle
                                              : TextStyles
                                                  .blackPlanTermSelectionPriceTextStyle,
                                    )
                                  ],
                                ),
                                SizedBox(
                                  width: controller.selectedValue.value == 0
                                      ? windowWidth * .08
                                      : windowWidth * .13,
                                ),
                                /*controller.selectedValue.value == 0
                              ? const SizedBox()
                              :*/
                                Padding(
                                  padding: controller.selectedValue.value == 0
                                      ? const EdgeInsets.only(right: 10.0)
                                      : const EdgeInsets.only(right: 0.0),
                                  child: SvgPicture.asset(
                                    "assets/icons/sub_selected.svg",
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                : const Offstage(),
            SizedBox(
              height: windowHeight * 0.02,
            ),
            controller.selectedValue.value == 0
                ? Container()
                : AbsorbPointer(
                    absorbing: controller.planTermPriceMonthly.value.contains(
                        (int.parse(controller.subscribedPlanAmount.value) / 100)
                            .toString()),
                    child: GestureDetector(
                      onTap: () {
                        controller.subscriptionTermSelection(1);
                      },
                      child: Card(
                        elevation: controller.planTermPriceMonthly.value
                                .contains((int.parse(controller
                                            .subscribedPlanAmount.value) /
                                        100)
                                    .toString())
                            ? 0
                            : controller.finalSubscriptionTermSelected.value ==
                                    1
                                ? 0
                                : 7,
                        shape: const RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(20))),
                        child: Container(
                          width: windowWidth,
                          decoration: BoxDecoration(
                              color: controller.finalSubscriptionTermSelected
                                          .value ==
                                      1
                                  ? AppColors.kPrimaryColor
                                  : Colors.white,
                              border: controller.finalSubscriptionTermSelected
                                          .value ==
                                      1
                                  ? Border.all(color: Colors.black)
                                  : null,
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(20))),
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                vertical: windowHeight * 0.01),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SizedBox(
                                  width: windowWidth * .2,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Monthly Subscription',
                                      style: controller
                                                  .finalSubscriptionTermSelected
                                                  .value ==
                                              1
                                          ? TextStyles
                                              .planTermSelectionTextStyle
                                          : TextStyles
                                              .redPlanTermSelectionTextStyle,
                                    ),
                                    Text(
                                      controller.planTermPriceMonthly.value,
                                      style: controller
                                                  .finalSubscriptionTermSelected
                                                  .value ==
                                              1
                                          ? TextStyles
                                              .planTermSelectionPriceTextStyle
                                          : TextStyles
                                              .blackPlanTermSelectionPriceTextStyle,
                                    ),
                                    // Text(
                                    //   '\$ 50 worth of savings!',
                                    //   style: controller
                                    //               .finalSubscriptionTermSelected
                                    //               .value ==
                                    //           1
                                    //       ? TextStyles
                                    //           .whitePlanTermSelectionPriceSavingTextStyle
                                    //       : TextStyles
                                    //           .planTermSelectionPriceSavingTextStyle,
                                    // )
                                    Text(
                                      '',
                                      style: controller
                                                  .finalSubscriptionTermSelected
                                                  .value ==
                                              1
                                          ? TextStyles
                                              .whitePlanTermSelectionPriceSavingTextStyle
                                          : TextStyles
                                              .planTermSelectionPriceSavingTextStyle,
                                    )
                                  ],
                                ),
                                SizedBox(
                                  width: windowWidth * .13,
                                ),
                                SvgPicture.asset(
                                    "assets/icons/sub_selected.svg")
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
            SizedBox(
              height: windowHeight * 0.02,
            ),
            controller.selectedValue.value == 0
                ? Container()
                : AbsorbPointer(
                    absorbing: controller.planTermPriceYearly.value.contains(
                        (int.parse(controller.subscribedPlanAmount.value) / 100)
                            .toString()),
                    child: GestureDetector(
                      onTap: () {
                        controller.subscriptionTermSelection(2);
                      },
                      child: Card(
                        elevation: controller.planTermPriceYearly.value
                                .contains((int.parse(controller
                                            .subscribedPlanAmount.value) /
                                        100)
                                    .toString())
                            ? 0
                            : controller.finalSubscriptionTermSelected.value ==
                                    2
                                ? 0
                                : 7,
                        shape: const RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(20))),
                        child: Container(
                          width: windowWidth,
                          decoration: BoxDecoration(
                              color: controller.finalSubscriptionTermSelected
                                          .value ==
                                      2
                                  ? AppColors.kPrimaryColor
                                  : Colors.white,
                              border: controller.finalSubscriptionTermSelected
                                          .value ==
                                      2
                                  ? Border.all(color: Colors.black)
                                  : null,
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(20))),
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                vertical: windowHeight * 0.01),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SizedBox(
                                  width: windowWidth * .2,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Annual Subscription',
                                      style: controller
                                                  .finalSubscriptionTermSelected
                                                  .value ==
                                              2
                                          ? TextStyles
                                              .planTermSelectionTextStyle
                                          : TextStyles
                                              .redPlanTermSelectionTextStyle,
                                    ),
                                    Text(
                                      controller.planTermPriceYearly.value,
                                      style: controller
                                                  .finalSubscriptionTermSelected
                                                  .value ==
                                              2
                                          ? TextStyles
                                              .planTermSelectionPriceTextStyle
                                          : TextStyles
                                              .blackPlanTermSelectionPriceTextStyle,
                                    ),
                                    // Text(
                                    //   '\$ 100 worth of savings!',
                                    //   style: controller
                                    //               .finalSubscriptionTermSelected
                                    //               .value ==
                                    //           2
                                    //       ? TextStyles
                                    //           .whitePlanTermSelectionPriceSavingTextStyle
                                    //       : TextStyles
                                    //           .planTermSelectionPriceSavingTextStyle,
                                    // )
                                    Text(
                                      '',
                                      style: controller
                                                  .finalSubscriptionTermSelected
                                                  .value ==
                                              2
                                          ? TextStyles
                                              .whitePlanTermSelectionPriceSavingTextStyle
                                          : TextStyles
                                              .planTermSelectionPriceSavingTextStyle,
                                    )
                                  ],
                                ),
                                SizedBox(
                                  width: windowWidth * .13,
                                ),
                                SvgPicture.asset(
                                    "assets/icons/sub_selected.svg")
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}
