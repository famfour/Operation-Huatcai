// ignore_for_file: invalid_use_of_protected_member

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/submit_for_payout_controller.dart';
import 'package:iqrate/DeviceManager/assets.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/container_styles.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Model/response_model.dart/bank_forms_response_model.dart';
import 'package:iqrate/Model/response_model.dart/bank_submission_lawfirms_list_response_model.dart';
import 'package:iqrate/Model/response_model.dart/banker_model.dart';
import 'package:iqrate/Model/response_model.dart/documents_drawer_response_model.dart';
import 'package:iqrate/Model/response_model.dart/fetch_lead_list_response_model.dart';
import 'package:iqrate/Utils/ItemCheckBoxModel.dart';
import 'package:iqrate/Widgets/form_field_title.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';
import 'package:iqrate/Widgets/secondary_button.dart';

import '../Service/GetControllers.dart';

class SubmitForPayoutView extends StatefulWidget {
  final Result data;
  const SubmitForPayoutView({Key? key, required this.data}) : super(key: key);

  @override
  _SubmitForPayoutViewState createState() => _SubmitForPayoutViewState();
}

class _SubmitForPayoutViewState extends State<SubmitForPayoutView> {
  late double windowHeight;
  late double windowWidth;

  SubmitForPayoutViewController submitForPayoutViewController =
      GetControllers.shared.getSubmitForPayoutViewController();
  var leadProfileController = GetControllers.shared.getLeadProfileController();

  @override
  void initState() {

    submitForPayoutViewController.getRateTypeDropdown();

    submitForPayoutViewController.leadID = widget.data.id;
    submitForPayoutViewController.documentID = widget.data.document_drawer_id;
    submitForPayoutViewController.getLoDocuments();
    submitForPayoutViewController.getLawfirmList();

    submitForPayoutViewController.getDetailsPayout();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.width;
    return Obx(() => IgnorePointer(
        ignoring: !leadProfileController.clientPDPAStatus.value,
        child: expandableCard("Submit for Payout \$",
            submitForPayoutViewController.isSubmitPayoutApproved.value)));
  }

  Widget leadCreateView() {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(windowHeight * 0.025),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: windowHeight * 0.01),
          const FormFieldTitle(title: 'Bank*'),
          SizedBox(height: windowHeight * 0.01),

          Container(
            width: windowWidth,
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
            decoration: BoxDecoration(
                border: Border.all(
                    color: AppColors.formFieldBorderColor, width: 1.0),
                borderRadius: const BorderRadius.all(Radius.circular(
                        10) //                 <--- border radius here
                    )),
            child: DropdownButtonHideUnderline(
              child: DropdownButton<OneBankForm>(
                hint: const Text('Select Bank'),
                value: submitForPayoutViewController.selectedBank,
                onChanged: (newValue) {
                  debugPrint(newValue.toString());
                  setState(() {
                    submitForPayoutViewController.selectedBank = newValue;
                    submitForPayoutViewController
                        .setBankersDropdown(newValue!.id.toString());
                  });
                },
                items: submitForPayoutViewController.banks.map((location) {
                  return DropdownMenuItem<OneBankForm>(
                    child: Text(location.name!),
                    value: location,
                  );
                }).toList(),
              ),
            ),
          ),
          //const BankDropDown(),

          SizedBox(height: windowHeight * 0.03),

          const FormFieldTitle(title: 'Banker*'),
          SizedBox(height: windowHeight * 0.01),
          Container(
            width: windowWidth,
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
            decoration: BoxDecoration(
                border: Border.all(
                    color: AppColors.formFieldBorderColor, width: 1.0),
                borderRadius: const BorderRadius.all(Radius.circular(
                        10) //                 <--- border radius here
                    )),
            child: DropdownButtonHideUnderline(
              child: DropdownButton<BankerModel>(
                hint: const Text('Select Banker'),
                value: submitForPayoutViewController.selectedBanker,
                onChanged: (newValue) {
                  debugPrint(newValue.toString());
                  setState(() {
                    submitForPayoutViewController.selectedBanker = newValue!;
                  });
                },
                items: submitForPayoutViewController.bankers.map((location) {
                  return DropdownMenuItem<BankerModel>(
                    child: Text(location.name!),
                    value: location,
                  );
                }).toList(),
              ),
            ),
          ),
          SizedBox(height: windowHeight * 0.03),
          const FormFieldTitle(title: 'Loan Acceptance Date*'),
          SizedBox(height: windowHeight * 0.01),
          RequireTextField(
            type: Type.date,
            dateFormat: "yyyy-MM-dd",
            controller:
                submitForPayoutViewController.loanAcceptanceDateController,
            labelText: "YYYY-MM-DD",
            key: const Key("date"),
          ),
          SizedBox(height: windowHeight * 0.03),
          const FormFieldTitle(title: "Letter of Offer's Reference Number*"),
          SizedBox(height: windowHeight * 0.01),
          RequireTextField(
            type: Type.text,
            labelText: "Reference Number",
            controller: submitForPayoutViewController.referenceNumberController,
            key: const Key("Letter of Officer's"),
          ),
          SizedBox(height: windowHeight * 0.03),
          const FormFieldTitle(title: 'Bank Referral Fee %*'),
          SizedBox(height: windowHeight * 0.01),
          RequireTextField(
            type: Type.percentage,
            labelText: "Bank Referral Fee",
            controller: submitForPayoutViewController.referralFeesController,
            key: const Key("Referral Fee"),
          ),
          SizedBox(height: windowHeight * 0.03),
          submitForPayoutViewController.loanNumber.value > 0
              ? loanView(
                  "1",
                  submitForPayoutViewController.loanAmountController1,
                  submitForPayoutViewController.cashRebateController1,
                  submitForPayoutViewController.nextReviewDateController1,
                  submitForPayoutViewController.selectedLockInPeriod1,
                  submitForPayoutViewController.selectedClawbackPeriod1,
                  submitForPayoutViewController.selectedRateType1!)
              : const SizedBox(),
          submitForPayoutViewController.loanNumber.value > 1
              ? loanView(
                  "2",
                  submitForPayoutViewController.loanAmountController2,
                  submitForPayoutViewController.cashRebateController2,
                  submitForPayoutViewController.nextReviewDateController2,
              submitForPayoutViewController.selectedLockInPeriod2,
              submitForPayoutViewController.selectedClawbackPeriod2,
                  submitForPayoutViewController.selectedRateType2!)
              : const SizedBox(),
          submitForPayoutViewController.loanNumber.value > 2
              ? loanView(
                  "3",
                  submitForPayoutViewController.loanAmountController3,
                  submitForPayoutViewController.cashRebateController3,
                  submitForPayoutViewController.nextReviewDateController3,
              submitForPayoutViewController.selectedLockInPeriod3,
              submitForPayoutViewController.selectedClawbackPeriod3,
                  submitForPayoutViewController.selectedRateType3!)
              : const SizedBox(),
          submitForPayoutViewController.loanNumber.value > 3
              ? loanView(
                  "4",
                  submitForPayoutViewController.loanAmountController4,
                  submitForPayoutViewController.cashRebateController4,
                  submitForPayoutViewController.nextReviewDateController4,
              submitForPayoutViewController.selectedLockInPeriod4,
              submitForPayoutViewController.selectedClawbackPeriod4,
                  submitForPayoutViewController.selectedRateType4!)
              : const SizedBox(),
          submitForPayoutViewController.loanNumber.value > 4
              ? loanView(
                  "5",
                  submitForPayoutViewController.loanAmountController5,
                  submitForPayoutViewController.cashRebateController5,
                  submitForPayoutViewController.nextReviewDateController5,
              submitForPayoutViewController.selectedLockInPeriod5,
              submitForPayoutViewController.selectedClawbackPeriod5,
                  submitForPayoutViewController.selectedRateType5!)
              : const SizedBox(),
          submitForPayoutViewController.loanNumber.value != 5
              ? SecondaryButton(
                  windowHeight: windowHeight,
                  windowWidth: windowWidth,
                  kGradientBoxDecoration:
                      ContainerStyles.boxDecorationCalculatorButton,
                  kInnerDecoration:
                      ContainerStyles.boxDecorationCalculatorButton,
                  buttonTitle: 'Add Loan',
                  onPressed: () {
                    submitForPayoutViewController.onTapAddLoan();
                  },
                )
              : const SizedBox(),

          SizedBox(height: windowHeight * 0.03),
          const FormFieldTitle(title: 'Law Firm*'),
          SizedBox(height: windowHeight * 0.01),

          Container(
            width: windowWidth,
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
            decoration: BoxDecoration(
                border: Border.all(
                    color: AppColors.formFieldBorderColor, width: 1.0),
                borderRadius: const BorderRadius.all(Radius.circular(
                        10) //                 <--- border radius here
                    )),
            child: DropdownButtonHideUnderline(
              child: DropdownButton<LawFirmModel>(
                hint: const Text('Select Law Firm'),
                value: submitForPayoutViewController.selectedLawFirm,
                onChanged: (newValue) {
                  debugPrint(newValue!.id.toString());
                  setState(() {
                    submitForPayoutViewController.selectedLawFirm = newValue;
                    submitForPayoutViewController.legalFeesController.text =
                        newValue.legalFee.toString();
                  });
                },
                items:
                    submitForPayoutViewController.lawFirmList.map((location) {
                  return DropdownMenuItem<LawFirmModel>(
                    child: Text(location.name!),
                    value: location,
                  );
                }).toList(),
              ),
            ),
          ),

          SizedBox(height: windowHeight * 0.03),
          const FormFieldTitle(title: 'Legal Fees*'),
          SizedBox(height: windowHeight * 0.01),
          RequireTextField(
            maxLength: 5,
            type: Type.money,
            labelText: "Legal",
            controller: submitForPayoutViewController.legalFeesController,
            key: const Key("Legal"),
          ),
          SizedBox(height: windowHeight * 0.03),
          Obx(() => docUploadView("Letter Of Offer",
              submitForPayoutViewController.documents.value, Get.width)),
          SizedBox(height: windowHeight * 0.03),
          submitForPayoutViewController.isSubmitPayoutApproved.value == false
              ? PrimaryButton(
                  windowHeight: windowHeight,
                  windowWidth: windowWidth,
                  buttonTitle: "Submit",
                  onPressed: () {
                    submitForPayoutViewController.onTapSubmit();
                  },
                )
              : const SizedBox(),
          SizedBox(height: windowHeight * 0.03),
        ],
      ),
    );
  }

  Widget docUploadView(
      String docType, List<Documents> documents, double windowWidth) {
    debugPrint("documents.length:: " + documents.length.toString());

    // bool allchecked = false;
    return Container(
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.black26,
          style: BorderStyle.solid,
          width: 1.0,
        ),
        color: Colors.transparent,
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(windowWidth * 0.05),
            width: Get.width + 120,
            decoration: BoxDecoration(
              color: Colors.black12,
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 5.0),
                    child: Text(
                      docType + " (Full Set)",
                      style: TextStyles.bankSubmissionDocumentScreenTextStyle,
                    ),
                  ),
                ),
                SizedBox(width: windowWidth * 0.03),
                InkWell(
                    onTap: () {
                      submitForPayoutViewController.onTapUploadPopup(docType);
                      debugPrint("onTap add");
                    },
                    child: const Icon(
                      Icons.add,
                      color: Colors.red,
                      size: 35,
                    ))
              ],
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ListView.builder(
                itemCount: documents.length,
                //itemCount: 0,
                primary: false,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  var item = documents.elementAt(index);
                  return Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(10),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Text(
                                item.filename!,
                                style: TextStyles.docTextStyle,
                              ),
                            ),
                            InkWell(
                                onTap: () {
                                  debugPrint("onTap edit");
                                  submitForPayoutViewController
                                      .onTapPopUpUpdate(item);
                                },
                                child: Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: SvgPicture.asset(
                                    "assets/images/edit.svg",
                                    width: 20,
                                  ),
                                )),
                            SizedBox(width: windowWidth * 0.03),
                            InkWell(
                                onTap: () {
                                  debugPrint("onTap downlaod");
                                  submitForPayoutViewController
                                      .onTapDownloadButton(item.id!);
                                },
                                child: const Icon(
                                  Icons.remove_red_eye_outlined,
                                  size: 28,
                                )),
                            SizedBox(width: windowWidth * 0.03),
                            InkWell(
                                onTap: () {
                                  debugPrint("onTap delete");
                                  submitForPayoutViewController
                                      .onTapPopUpDelete(item.id!);
                                },
                                child: const Icon(
                                  Icons.delete,
                                  size: 28,
                                ))
                          ],
                        ),
                      ),
                      documents.length != 1 && documents.length != index + 1
                          ? const Divider(
                              height: 1,
                              thickness: 1,
                              color: Colors.black26,
                            )
                          : const SizedBox(),
                    ],
                  );
                },
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget expandableCard(String title, bool isDone) {
    return GestureDetector(
      onTap: () {
        submitForPayoutViewController.isExpand.toggle();
        if(submitForPayoutViewController.isExpand.value) {
          submitForPayoutViewController.getLawfirmList();
        }
        GetControllers.shared.getLoanDetailsController().isExpand.value = false;
        GetControllers.shared.getPropertyDetailsController().isExpand.value =
            false;
        GetControllers.shared.getGeneratePackagesController().isExpand.value =
            false;
        GetControllers.shared.getSelectedPackagesController().isExpand.value =
            false;
        GetControllers.shared.getBankSubmissionController().isExpand.value =
            false;
        GetControllers.shared.getLeadProfileController().isExpand.value = false;
        GetControllers.shared.getLawFirmSubmissionController().isExpand.value =
            false;
        GetControllers.shared.getEmailLogController().isExpand.value = false;
        GetControllers.shared.getSubmitCoBrokeController().isExpand.value =
            false;
      },
      child: Obx(() => Container(
            color: Colors.white,
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(bottom: windowWidth * 0.02),
                  padding: EdgeInsets.all(windowWidth * 0.05),
                  decoration: BoxDecoration(
                      boxShadow: const <BoxShadow>[
                        BoxShadow(
                            color: Colors.black54,
                            blurRadius: 5.0,
                            offset: Offset(0.0, 0.3))
                      ],
                      color: !leadProfileController.clientPDPAStatus.value
                          ? AppColors.greyDEDEDE
                          : Colors.white),
                  child: Row(
                    children: [
                      Text(title, style: TextStyles.basicTextStyle),
                      const Spacer(),
                      SvgPicture.asset(
                        isDone
                            ? Assets.manageLoanDone
                            : Assets.manageLoanIncomplete,
                        height: 24,
                        width: 24,
                      ),
                      SizedBox(width: windowWidth * 0.03),
                      Icon(
                        submitForPayoutViewController.isExpand.value
                            ? Icons.keyboard_arrow_up_rounded
                            : Icons.keyboard_arrow_down,
                        size: 30,
                      )
                    ],
                  ),
                ),
                submitForPayoutViewController.isExpand.value
                    ? leadCreateView()
                    : Container()
              ],
            ),
          )),
    );
  }

  loanView(
      String loanNumber,
      TextEditingController loanAmountController,
      TextEditingController cashRebateController,
      TextEditingController nextReviewDateController,
      RxString selectedLockInPeriod,
      RxString selectedClawbackPeriod,
  ItemCheckBoxModel selectedRateType) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: windowHeight * 0.08,
          width: windowWidth * 0.9,
          /*decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: AppColors.greyF4F4F4,
                ),*/
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Loan #$loanNumber',
                    style: TextStyles.calculatorHeadingsTextStyle),
                SizedBox(width: windowWidth * 0.01),
                loanNumber != "1"
                    ? GestureDetector(
                        onTap: () {
                          submitForPayoutViewController
                              .onTapDeleteLoan(loanNumber);
                        },
                        child: const Icon(Icons.delete_outline_sharp,
                            color: Colors.black),
                      )
                    : const SizedBox(),
              ],
            ),
          ),
        ),
        const FormFieldTitle(title: 'Loan Amount*'),
        SizedBox(height: windowHeight * 0.01),
        RequireTextField(
          type: Type.money,
          controller: loanAmountController,
          labelText: "Loan Amount",
          key: const Key("Loan Amount"),
        ),
        SizedBox(height: windowHeight * 0.03),
        const FormFieldTitle(title: 'Rate Type*'),
        SizedBox(height: windowHeight * 0.01),
        Container(
          width: windowWidth,
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
          decoration: BoxDecoration(
              border:
                  Border.all(color: AppColors.formFieldBorderColor, width: 1.0),
              borderRadius: const BorderRadius.all(
                  Radius.circular(10) //                 <--- border radius here
                  )),
          child: DropdownButtonHideUnderline(
            child: DropdownButton<ItemCheckBoxModel>(
              hint: const Text('Select Rate Type'),
              value: selectedRateType,
              onChanged: (newValue) {
                debugPrint(newValue.toString());
                setState(() {
                  if (loanNumber == "1") {
                    submitForPayoutViewController.selectedRateType1 = newValue!;
                  } else if (loanNumber == "2") {
                    submitForPayoutViewController.selectedRateType2 = newValue!;
                  } else if (loanNumber == "3") {
                    submitForPayoutViewController.selectedRateType3 = newValue!;
                  } else if (loanNumber == "4") {
                    submitForPayoutViewController.selectedRateType4 = newValue!;
                  } else if (loanNumber == "5") {
                    submitForPayoutViewController.selectedRateType5 = newValue!;
                  }
                });
              },
              items: submitForPayoutViewController.rateTypeDataSourceV2
                  .map((location) {
                return DropdownMenuItem<ItemCheckBoxModel>(
                  child: Text(location.name),
                  value: location,
                );
              }).toList(),
            ),
          ),
        ),

        SizedBox(height: windowHeight * 0.03),
        const FormFieldTitle(title: 'Lock In Period*'),
        SizedBox(height: windowHeight * 0.01),
        Container(
          width: windowWidth,
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
          decoration: BoxDecoration(
              border:
                  Border.all(color: AppColors.formFieldBorderColor, width: 1.0),
              borderRadius: const BorderRadius.all(
                  Radius.circular(10) //                 <--- border radius here
                  )),
          child: DropdownButtonHideUnderline(
            child: DropdownButton(
              hint: const Text('Select Lock In Period'),
              // Not necessary for Option 1
              value: selectedLockInPeriod.value,
              onChanged: (newValue) {

                if (loanNumber == "1") {
                  submitForPayoutViewController.selectedLockInPeriod1.value =
                      newValue.toString();
                } else if (loanNumber == "2") {
                  submitForPayoutViewController.selectedLockInPeriod2.value =
                      newValue.toString();
                } else if (loanNumber == "3") {
                  submitForPayoutViewController.selectedLockInPeriod3.value =
                      newValue.toString();
                } else if (loanNumber == "4") {
                  submitForPayoutViewController.selectedLockInPeriod4.value =
                      newValue.toString();
                } else if (loanNumber == "5") {
                  submitForPayoutViewController.selectedLockInPeriod5.value =
                      newValue.toString();
                }

              },
              items:
                  submitForPayoutViewController.lockInPeriods.map((location) {
                return DropdownMenuItem(
                  child: Text(location),
                  value: location,
                );
              }).toList(),
            ),
          ),
        ),
        SizedBox(height: windowHeight * 0.03),
        const FormFieldTitle(title: 'Clawback Periods'),
        SizedBox(height: windowHeight * 0.01),
        Container(
          width: windowWidth,
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
          decoration: BoxDecoration(
              border:
                  Border.all(color: AppColors.formFieldBorderColor, width: 1.0),
              borderRadius: const BorderRadius.all(
                  Radius.circular(10) //                 <--- border radius here
                  )),
          child: DropdownButtonHideUnderline(
            child: DropdownButton(
              hint: const Text('Select Clawback Period'),
              // Not necessary for Option 1
              value:
                  selectedClawbackPeriod.value,
              onChanged: (newValue) {

                if (loanNumber == "1") {
                  submitForPayoutViewController.selectedClawbackPeriod1.value =
                      newValue.toString();
                } else if (loanNumber == "2") {
                  submitForPayoutViewController.selectedClawbackPeriod2.value =
                      newValue.toString();
                } else if (loanNumber == "3") {
                  submitForPayoutViewController.selectedClawbackPeriod3.value =
                      newValue.toString();
                } else if (loanNumber == "4") {
                  submitForPayoutViewController.selectedClawbackPeriod4.value =
                      newValue.toString();
                } else if (loanNumber == "5") {
                  submitForPayoutViewController.selectedClawbackPeriod5.value =
                      newValue.toString();
                }

              },
              items:
                  submitForPayoutViewController.clawbackPeriods.map((location) {
                return DropdownMenuItem(
                  child: Text(location),
                  value: location,
                );
              }).toList(),
            ),
          ),
        ),
        SizedBox(height: windowHeight * 0.03),
        const FormFieldTitle(
            title: 'Cash Rebate/ Legal Subsidy/ Valuation Subsidy'),
        SizedBox(height: windowHeight * 0.01),
        RequireTextField(
          type: Type.money,
          controller: cashRebateController,
          labelText: "\$",
          key: const Key("Cash Rebate"),
        ),
        SizedBox(height: windowHeight * 0.03),
        const FormFieldTitle(title: 'Next Review Date'),
        SizedBox(height: windowHeight * 0.01),
        RequireTextField(
          type: Type.date,
          dateFormat: "dd-MM-yyyy",
          controller: nextReviewDateController,
          labelText: "DD-MM-YYYY",
          key: const Key("ext Review date"),
        ),
        SizedBox(height: windowHeight * 0.03),
      ],
    );
  }
}
