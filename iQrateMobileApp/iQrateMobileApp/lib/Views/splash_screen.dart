import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/landing_controller.dart';
import 'package:iqrate/DeviceManager/assets.dart';
import 'package:iqrate/DeviceManager/screen_constants.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  final LandingController _landingController = Get.put(
      LandingController()); //initialize controller. Gives access to all the functions and variables in LandingController.
  AnimationController?
      animationController; //animation controller for the splash screen. Allows to control the animation effects like duration and physics.
  Animation<double>? animation;
  var _visible =
      true; //variable to control the visibility of the splash screen.

//function to initialize the splash screen and the duration for which it is visible.
  startTime() async {
    var _duration = const Duration(seconds: 3);
    return Timer(_duration, _landingController.toLanding);
  }

//init state is is called before any widget is created. This is the starting point of the splash screen.
  @override
  void initState() {
    super.initState();
    //_selectedLang = LocalizationService.langs.first;
    animationController =
        AnimationController(vsync: this, duration: const Duration(seconds: 2));
    animation =
        CurvedAnimation(parent: animationController!, curve: Curves.easeInOut);

    animation!.addListener(() => setState(() {}));
    animationController!.forward();

    setState(() {
      _visible = !_visible;
    });
    // getData();

    startTime();
  }
  //   getData()async{
  //   final id=await getId();
  //   hive.put(HiveString.deviceId, id);
  //   if(hive.containsKey(HiveString.token)){
  //     getAccountDetails();
  //   }
  // }

  //   getAccountDetails() async{

  //   var data =await CoreService().getWithAuth(
  //       url: baseUrl+profileDetails);

  //   if(Get.isDialogOpen??false)
  //     Get.back();
  //   if(data==null){

  //   }
  //   else{
  //     var result= AccountDetailsResponseModel.fromJson(data);
  //     hive.put(HiveString.userData, JsonEncoder().convert(result.data));

  //   }
  // }

//anything that is to be done after the splash screen is closed is done in dispose.
  @override
  void dispose() {
    animationController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ScreenConstant.setScreenAwareConstant(context);

    return Scaffold(
      body: Stack(
        fit: StackFit.loose,
        children: <Widget>[
          Center(
            child: SvgPicture.asset(
              Assets.logo,
              fit: BoxFit.contain,
            ),
          ),
        ],
      ),
    );
  }
}
