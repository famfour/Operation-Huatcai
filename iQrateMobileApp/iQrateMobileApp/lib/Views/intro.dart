import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/intro_controller.dart';
import 'package:iqrate/Widgets/intro_screen_pageview.dart';
import 'package:iqrate/Widgets/primary_button.dart';

class Intro extends StatefulWidget {
  const Intro({Key? key}) : super(key: key);

  @override
  State<Intro> createState() => _IntroState();
}

class _IntroState extends State<Intro> {
  final int pages = 3;
  int currentPage = 0;

  final IntroController introController = Get.put(IntroController());

  // List<Widget> _buildPageIndicator() {
  //   List<Widget> list = [];
  //   for (int i = 0; i < pages; i++) {
  //     list.add(i == currentPage ? _indicator(true) : _indicator(false));
  //   }
  //   return list;
  // }

  // Widget _indicator(bool isActive) {
  //   return AnimatedContainer(
  //     duration: const Duration(milliseconds: 150),
  //     margin: const EdgeInsets.symmetric(horizontal: 8.0),
  //     height: 12.0,
  //     width: isActive ? 12.0 : 12.0,
  //     decoration: BoxDecoration(
  //       gradient: isActive
  //           ? const LinearGradient(
  //               colors: [
  //                 Color(0XFFE09207),
  //                 Color(0XFFF9484A),
  //               ],
  //             )
  //           : const LinearGradient(
  //               colors: [
  //                 Colors.white,
  //                 Colors.white24,
  //               ],
  //             ),
  //       borderRadius: const BorderRadius.all(Radius.circular(12)),
  //       border: Border.all(color: const Color(0XFFF9484A), width: 2.0),
  //     ),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    //Media query is used to make the app responsive for every screen size. It calculates the size of the screen and then returns the value.

    double windowHeight =
        MediaQuery.of(context).size.height; //height of the screen in pixles
    double windowWidth =
        MediaQuery.of(context).size.width; //width of the screen in pixles
    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: Container(
          decoration: const BoxDecoration(
            color: Colors.white,
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 40.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  alignment: Alignment.centerRight,
                  child: TextButton(
                    onPressed: () {
                      introController.toSignUp();
                    },
                    child: const Text(
                      'Skip',
                      style: TextStyle(
                        color: Color(0XFF87898C),
                        fontSize: 20.0,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: windowHeight * 0.72,
                  child: PageView(
                    physics: const ClampingScrollPhysics(),
                    controller: introController.controller,
                    onPageChanged: (int page) {
                      setState(() {
                        currentPage = page;
                      });
                    },
                    children: [
                      IntroScreenPageViewScreens(
                        windowHeight: windowHeight,
                        description:
                            'View our extensive loan packages with direct application to the selected bank!',
                        image: 'assets/images/easy_loan.png',
                        titlePartOne: 'We make ',
                        underlinedText: 'Mortgage\n',
                        titlePartTwo: 'applications a breeze!',
                      ),
                      IntroScreenPageViewScreens(
                        windowHeight: windowHeight,
                        description:
                            "Hop onto our platform and earn referral fees by helping mortgage loan seekers search out the best deals on our platform",
                        image: 'assets/images/easy_mortgage.png',
                        titlePartOne: 'Earn ',
                        underlinedText: 'Extra Income ',
                        titlePartTwo: 'by\nhelping mortgage\nloan seekers',
                      ),
                      IntroScreenPageViewScreens(
                        windowHeight: windowHeight,
                        description:
                            "Refer Your Friend and win \nAn exciting reward",
                        image: 'assets/images/refer_and_earn.png',
                        titlePartOne: 'Do-it-yourself or let our\n',
                        underlinedText: 'Brokers',
                        titlePartTwo: ' handle it!',
                      ),
                    ],
                  ),
                ),
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.center,
                //   children: _buildPageIndicator(),
                // ),
                SizedBox(
                  height: windowHeight * 0.03,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30.0),
                  child: PrimaryButton(
                    onPressed: () {
                      //if the current page is the last page then navigate to the home screen
                      currentPage != pages - 1
                          ? introController.controller.nextPage(
                              duration: const Duration(milliseconds: 500),
                              curve: Curves.ease,
                            )
                          : introController.toSignUp();
                    },
                    buttonTitle: 'Next',
                    windowHeight: windowHeight,
                    windowWidth: windowWidth,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
