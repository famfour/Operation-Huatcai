import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Widgets/primary_button.dart';

// ignore: must_be_immutable
class AccountLockedView extends StatelessWidget {
  const AccountLockedView({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: windowWidth * 0.07),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.asset('assets/images/accountLocked.svg'),
            SizedBox(
              height: windowHeight * 0.05,
            ),
            Text(
              'Your Account Has Been Locked!',
              style: TextStyles.introScreenTitlesSmall,
            ),
            SizedBox(
              height: windowHeight * 0.02,
            ),
            Text(
              'Please contact IQrate support',
              style: TextStyles.introScreenDescriptions,
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: windowHeight * 0.09,
            ),
            PrimaryButton(
                windowHeight: windowHeight,
                windowWidth: windowWidth,
                buttonTitle: 'Close',
                onPressed: () {})
          ],
        ),
      ),
    );
  }
}
