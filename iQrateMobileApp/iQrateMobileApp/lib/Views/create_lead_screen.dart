import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/leads_view_controller.dart';
import 'package:iqrate/DeviceManager/container_styles.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Widgets/form_field_title.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';
import 'package:iqrate/Widgets/secondary_button.dart';

class CreateLeadScreen extends StatefulWidget {
  const CreateLeadScreen({Key? key}) : super(key: key);

  @override
  _CreateLeadScreenState createState() => _CreateLeadScreenState();
}

class _CreateLeadScreenState extends State<CreateLeadScreen> {
  late double windowHeight;
  late double windowWidth;

  final LeadsViewController leadsViewController = Get.find();

  @override
  Widget build(BuildContext context) {
    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Colors.black,
      body: Container(
        height: windowHeight * .5,
        width: windowWidth,
        color: Colors.white,
        child: createLead(),
      ),
    );
  }

  Widget createLead() {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Center(
            child: Text(
              'Create a Lead',
              style: TextStyle(
                  color: Colors.red,
                  fontSize: 22,
                  fontWeight: FontWeight.w600,
                  height: 1.5),
            ),
          ),
          SizedBox(height: windowHeight * 0.03),
          const FormFieldTitle(title: 'Full Name'),
          SizedBox(height: windowHeight * 0.01),
          RequireTextField(
            type: Type.fullname,
            labelText: "Enter your full name",
            controller: leadsViewController.nameController,
            key: const Key("name"),
          ),
          SizedBox(height: windowHeight * 0.03),
          const FormFieldTitle(title: 'Mobile Number'),
          SizedBox(height: windowHeight * 0.01),
          RequireTextField(
            type: Type.dropdownCountryCodeWithPhone,
            controller: leadsViewController.phoneController,
            countryCodeController: leadsViewController.countryCodeController,
            countryCodeLabelController:
                leadsViewController.countryCodeLabelController,
            labelText: "Enter your phone number",
            key: const Key("phone"),
          ),
          SizedBox(height: windowHeight * 0.03),
          const FormFieldTitle(title: 'Email Address'),
          SizedBox(height: windowHeight * 0.01),
          RequireTextField(
            type: Type.email,
            controller: leadsViewController.emailController,
            labelText: "Enter your email address",
            key: const Key("email"),
          ),
          SizedBox(height: windowHeight * 0.04),
          PrimaryButton(
            windowHeight: windowHeight,
            windowWidth: windowWidth,
            buttonTitle: "Create Now",
            onPressed: () {
              leadsViewController.onTapLeadCreateNowScreen();
            },
          ),
          SizedBox(height: windowHeight * 0.03),
          SecondaryButton(
            windowHeight: windowHeight,
            windowWidth: windowWidth,
            kGradientBoxDecoration:
                ContainerStyles.kGradientBoxDecorationSecondaryButton,
            kInnerDecoration: ContainerStyles.kInnerDecorationSecondaryButton,
            buttonTitle: 'Back',
            onPressed: () {
              leadsViewController.onTapBackPreLeadCreateScreen();
            },
          ),
          SizedBox(height: windowHeight * 0.02),
          Text.rich(
            TextSpan(
                text:
                    'Upon creating a lead, an sms will be sent to the lead to ',
                //textAlign : Alignment.center,
                style: TextStyles.leadsNormalTextStyle,
                children: <InlineSpan>[
                  TextSpan(
                    text: 'download and agree to IQrate’s ',
                    style: TextStyles.leadsNormalTextStyle,
                  ),
                  TextSpan(
                    text: 'Terms & Conditions',
                    style: TextStyles.leadsUnderlineTextStyle,
                  ),
                  TextSpan(
                    text: ' & ',
                    style: TextStyles.leadsNormalTextStyle,
                  ),
                  TextSpan(
                    text: 'Privacy Policy',
                    style: TextStyles.leadsUnderlineTextStyle,
                  )
                ]),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}
