import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/training_view_controller.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Widgets/default_appbar.dart';
import 'package:iqrate/Widgets/require_text_field.dart';

// ignore: must_be_immutable
class TrainingUserGuideView extends StatelessWidget {
  TrainingUserGuideView({Key? key}) : super(key: key);
  TrainingViewController controller = Get.put(TrainingViewController());

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    // double windowWidth = MediaQuery.of(context).size.width;

    return Scaffold(
        appBar: DefaultAppBar(
          title: 'User Guide',
          windowHeight: windowHeight * 0.09,
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 0),
          child: ListView(children: [
            SizedBox(
              height: windowHeight * 0.01,
            ),
            RequireTextField(
              type: Type.search,
              controller: controller.userGuideSearchTextEditingController,
              onSubmit: (value) {
                controller.searchBasicModules(value);
              },
              onSubmitSearch: () {
                controller.searchBasicModules(
                    controller.userGuideSearchTextEditingController.text);
              },
            ),
            SizedBox(
              height: windowHeight * 0.02,
            ),
            Obx(
              () => controller.trainingViewResponse.value.value != null
                  ? ListView.builder(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount:
                          controller.trainingViewResponse.value.value!.length,
                      itemBuilder: ((context, index) {
                        return ModulesCard(
                            id: (index + 1).toString(),
                            title: controller.trainingViewResponse.value
                                    .value![index].title ??
                                '-',
                            imageUrl: controller.trainingViewResponse.value
                                        .value![index].thumbnail !=
                                    null
                                ? 'https://iqrate-strapi.dprizm.dev' +
                                    controller
                                        .trainingViewResponse
                                        .value
                                        .value![index]
                                        .thumbnail!
                                        .formats!
                                        .thumbnail!
                                        .url
                                        .toString()
                                : "https://iqrate-strapi.dprizm.dev/uploads/iqrate_logo_86b4ef12de.png?width=70&height=88", //* This is a dymmy image from the server, we show this image when there is no other image to show
                            windowHeight: windowHeight,
                            onTap: () {
                              controller.navigateToRequiredDetailedPage(
                                  controller.trainingViewResponse.value
                                      .value![index]);
                            });
                      }),
                    )
                  : Container(),
            ),
          ]),
        ));
  }
}

// ignore: must_be_immutable
class ModulesCard extends StatelessWidget {
  ModulesCard(
      {Key? key,
      required this.id,
      required this.title,
      required this.imageUrl,
      required this.windowHeight,
      required this.onTap})
      : super(key: key);
  final String id;
  final String title;
  final String imageUrl;
  double windowHeight;
  final void Function() onTap;

  @override
  Widget build(BuildContext context) {
    debugPrint(imageUrl);
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 7.5),
      child: GestureDetector(
        onTap: onTap,
        child: Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          elevation: 3,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              imageUrl != 'null'
                  ? SizedBox(
                      width: windowHeight,
                      height: windowHeight * 0.2,
                      child: ClipRRect(
                        borderRadius: const BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20)),
                        child: Image.network(
                          imageUrl,
                          width: MediaQuery.of(context).size.width,
                          fit: imageUrl.contains("iqrate_logo_86b4ef12de.png")
                              ? BoxFit.contain
                              : BoxFit.cover,
                        ),
                      ),
                    )
                  : Center(
                      child: SizedBox(
                          width: windowHeight * 0.2,
                          height: windowHeight * 0.1,
                          child:
                              const Center(child: Text('No image available')))),
              Container(
                padding: const EdgeInsets.fromLTRB(10, 10, 0, 10),
                color: Colors.white,
                child: Text(
                  '#$id  $title',
                  style: TextStyles.cardContentTextStyle,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
