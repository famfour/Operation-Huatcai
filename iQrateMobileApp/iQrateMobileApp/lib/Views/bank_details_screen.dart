// ignore_for_file: prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/bank_details_controller.dart';
import 'package:iqrate/Controller/rates_view_controller.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Widgets/common_appbar_options.dart';

class BankDetailsScreen extends StatefulWidget {
  const BankDetailsScreen({Key? key}) : super(key: key);

  @override
  _BankDetailsScreenState createState() => _BankDetailsScreenState();
}

class _BankDetailsScreenState extends State<BankDetailsScreen> {
  final BankDetailsController bankDetailsController =
      Get.put(BankDetailsController());

  late double windowWidth;
  late double windowHeight;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    windowWidth = MediaQuery.of(context).size.width;
    windowHeight = MediaQuery.of(context).size.height;

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          automaticallyImplyLeading: false,
          centerTitle: false,
          flexibleSpace: const Padding(
            padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 30),
            child: CommonAppBarOptions(
              windowWidth: 0,
              title: 'Manage Loan',
            ),
          ),
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(windowHeight * 0.03),
            child: const SizedBox(),
          ),
          backgroundColor: const Color(0xFFDF5356),
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(mainAxisSize: MainAxisSize.max,
                // primary: true,
                // physics: const AlwaysScrollableScrollPhysics(), m
                children: [
                  Column(
                    children: [
                      Card(
                        shape: const RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(15))),
                        elevation: 5,
                        child: Column(
                          children: [
                            LoanPackageCardSmall(
                              headingSubText: 'Fixed(Special)',
                              headingText: 'Standard characted Bank',
                              onChanged: (val) {},
                              checkBoxValue: true,
                              cardIndex: 0,
                            ),
                            expandedSectionHeading('Rates', [
                              expansionBody(
                                  "Year 1", "Fixed (0.00)% + 1.000% = 1.000%"),
                              expansionBody(
                                  "Year 1", "Fixed (0.00)% + 1.000% = 1.000%"),
                              expansionBody(
                                  "Year 1", "Fixed (0.00)% + 1.000% = 1.000%"),
                              expansionBody(
                                  "Year 1", "Fixed (0.00)% + 1.000% = 1.000%")
                            ]),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: windowHeight * 0.01,
                      ),
                      Card(
                        shape: const RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(15))),
                        elevation: 5,
                        child: expandedSectionHeading('Key Features', [
                          expansionBody("Property Type", "Private Residential"),
                          expansionBody("Property Type", "Private Residential"),
                          expansionBody("Property Type", "Private Residential"),
                          expansionBody("Property Type", "Private Residential"),
                        ]),
                      ),
                      SizedBox(
                        height: windowHeight * 0.01,
                      ),
                      Card(
                        shape: const RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(15))),
                        elevation: 5,
                        child: expandedSectionHeading('Bank’s Subsidy', [
                          expansionBody(
                              "Cash Rebate/ Legal Subsidy", "PTE: ≥ \$500,000"),
                          expansionBody(
                              "Cash Rebate/ Legal Subsidy", "PTE: ≥ \$500,000"),
                          expansionBody(
                              "Cash Rebate/ Legal Subsidy", "PTE: ≥ \$500,000"),
                        ]),
                      ),
                      SizedBox(
                        height: windowHeight * 0.01,
                      ),
                      Card(
                        shape: const RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(15))),
                        elevation: 5,
                        child:
                            expandedSectionHeading('Early Repayment Penalty', [
                          expansionBody("Partial Repayment Penalty",
                              "1.5% \nAllow partial repayment up to 50%"),
                          expansionBody("Partial Repayment Penalty",
                              "1.5% \nAllow partial repayment up to 50%"),
                          expansionBody("Partial Repayment Penalty",
                              "1.5% \nAllow partial repayment up to 50%"),
                        ]),
                      )
                    ],
                  )
                ]),
          ),
        ));
  }

  Widget expansionBody(String title, String subTitle) {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Row(
          children: [
            Text(title),
            const Spacer(),
            Flexible(child: Text(subTitle)),
          ],
        ),
      ),
    );
  }

  // This section is displayed when the card is expanded and this expansion tile is shown in a list
  Widget expandedSectionHeading(String title, List<Widget> body) {
    return ExpansionTile(
      initiallyExpanded: true,
      iconColor: Colors.white,
      backgroundColor: AppColors.kSecondaryColor,
      collapsedBackgroundColor: AppColors.kSecondaryColor,
      collapsedIconColor: Colors.white,
      trailing: const Icon(
        Icons.remove,
        color: Colors.white,
      ),
      leading: Text(
        title,
        style: TextStyles.detailsButton,
      ),
      title: Container(),
      children: body,
    );
  }
}

// ignore: must_be_immutable
class LoanPackageCardSmall extends StatelessWidget {
  String? image;
  String headingText;
  String headingSubText;
  void Function(bool?)? onChanged;
  bool checkBoxValue;
  int cardIndex;

  LoanPackageCardSmall(
      {Key? key,
      this.image,
      required this.headingText,
      required this.headingSubText,
      required this.onChanged,
      required this.checkBoxValue,
      required this.cardIndex})
      : super(key: key);

  final RatesViewController ratesViewController =
      Get.put(RatesViewController());

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        rowOne(),
        const SizedBox(height: 10),
        rowTwo(),
        const SizedBox(height: 10)
      ],
    );
  }

  // 1st row that shows the Bank name , image and a checkbox
  Widget rowOne() {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Row(
        children: [
          const Expanded(child: Icon(Icons.ac_unit)),
          Expanded(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                Text(
                  headingText,
                  style: TextStyles.cardHeading,
                ),
                const SizedBox(height: 10),
                Text(
                  headingSubText,
                  style: TextStyles.cardSubHeading,
                )
              ])),
          Expanded(
              child: Checkbox(
            value: checkBoxValue,
            onChanged: onChanged,
            checkColor: AppColors.kSecondaryColor,
            fillColor: MaterialStateProperty.all(Colors.white),
            side: const BorderSide(
                color: AppColors.kSecondaryColor,
                width: 2,
                style: BorderStyle.solid),
          )),
        ],
      ),
    );
  }

  // 2nd row that shows the percentage on year based
  Widget rowTwo() {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.black26,
          style: BorderStyle.solid,
          width: 1.0,
        ),
        color: Colors.transparent,
      ),
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 5),
      child: IntrinsicHeight(
        child: Row(
          children: [
            Expanded(child: titleValueRow(title: 'Year 1', value: '1.000%')),
            const VerticalDivider(
              color: Colors.black,
            ),
            Expanded(child: titleValueRow(title: 'Year 2', value: '1.000%')),
            const VerticalDivider(
              color: Colors.black,
            ),
            Expanded(child: titleValueRow(title: 'Year 3', value: '1.000%')),
          ],
        ),
      ),
    );
  }

  // A simple widget used in the 2nd row, which only shows a title and a value
  Widget titleValueRow({required String title, required String value}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          child: Text(
            title + ': ',
            style: TextStyles.headingSubtitleStyleSmall,
          ),
        ),
        Expanded(
          child: Text(
            value,
            style: TextStyles.headingSubtitleStyleSmall,
          ),
        )
      ],
    );
  }
}
