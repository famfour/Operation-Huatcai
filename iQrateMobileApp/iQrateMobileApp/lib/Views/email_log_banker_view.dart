import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/DeviceManager/assets.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Model/response_model.dart/email_log_response_model.dart';
import 'package:iqrate/Utils/config.dart';
import 'package:iqrate/Widgets/form_field_title.dart';
import 'package:iqrate/Widgets/primary_button.dart';

class EmailLogBankerView extends StatefulWidget {
  final EmailLogResponseModel data;
  const EmailLogBankerView({Key? key, required this.data}) : super(key: key);

  @override
  _EmailLogBankerViewState createState() => _EmailLogBankerViewState();
}

class _EmailLogBankerViewState extends State<EmailLogBankerView> {
  late double windowHeight;
  late double windowWidth;

  @override
  Widget build(BuildContext context) {
    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.width;
    return Scaffold(body: widgetView());
  }

  Widget widgetView() {
    return SingleChildScrollView(
      child: Container(
          color: Colors.white,
          margin: const EdgeInsets.only(top: 50),
          padding: EdgeInsets.all(windowHeight * 0.025),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Align(
                alignment: Alignment.topRight,
                child: GestureDetector(
                  onTap: () {
                    Get.back();
                  },
                  child: SvgPicture.asset(
                    Assets.manageLoanClose,
                    color: Colors.black,
                  ),
                ),
              ),
              SizedBox(height: windowHeight * 0.03),
              Text(
                "Email to Banker Log",
                style: TextStyles.bankSubmissionTaskTextStyle1,
              ),
              SizedBox(height: windowHeight * 0.005),
              FormFieldTitle(
                  title:
                      'Email sent on ${AppConfig.getDateFormatForEmailLog(widget.data.created!)}'),
              SizedBox(height: windowHeight * 0.01),
              const Divider(
                color: Colors.red,
                thickness: 1.5,
              ),
              SizedBox(height: windowHeight * 0.03),
              const FormFieldTitle(title: 'Banker Name'),
              SizedBox(height: windowHeight * 0.01),
              Text(
                widget.data.name.toString(),
                style: TextStyles.dashboardPopUpTextStyle,
              ),
              SizedBox(height: windowHeight * 0.03),
              const FormFieldTitle(title: 'Email Address'),
              SizedBox(height: windowHeight * 0.02),
              Text(
                widget.data.toEmail!,
                style: TextStyles.dashboardPopUpTextStyle,
              ),
              SizedBox(height: windowHeight * 0.03),
              const FormFieldTitle(title: 'Subject:'),
              SizedBox(height: windowHeight * 0.02),
              Text(
                widget.data.subject!,
                style: TextStyles.dashboardPopUpTextStyle,
              ),
              SizedBox(height: windowHeight * 0.03),
              const FormFieldTitle(title: 'Email Content:'),
              SizedBox(height: windowHeight * 0.02),
              Html(
                data: widget.data.content!,
              ),
              SizedBox(height: windowHeight * 0.07),
              PrimaryButton(
                windowHeight: windowHeight,
                windowWidth: windowWidth,
                buttonTitle: "Close",
                onPressed: () {
                  Get.back();
                },
              ),
              SizedBox(height: windowHeight * 0.03),
            ],
          )),
    );
  }
}
