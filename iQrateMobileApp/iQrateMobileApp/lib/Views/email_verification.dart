import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/email_verification_controller.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';

class EmailVerification extends StatelessWidget {
  const EmailVerification({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double windowWidth = MediaQuery.of(context).size.width;
    double windowHeight = MediaQuery.of(context).size.height;
    final EmailVerificationController emailVerificationController =
        Get.put(EmailVerificationController());
    return Scaffold(
      body: GestureDetector(
        //This is to dismiss the keyboard when the user taps outside of the text field
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: SafeArea(
          child: SingleChildScrollView(
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 25.0, horizontal: 18),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: SvgPicture.asset(
                      "assets/images/verify_email.svg",
                    ),
                  ),
                  SizedBox(
                    height: windowHeight * 0.15,
                  ),
                  Text("Please verify your email address",
                      style: TextStyles.introScreenTitlesSmall),
                  SizedBox(
                    height: windowHeight * 0.03,
                  ),
                  Text(
                    "An email has been sent to your registered email address. Please click and verify.",
                    style: TextStyles.introScreenDescriptions,
                  ),
                  SizedBox(
                    height: windowHeight * 0.08,
                  ),
                  SizedBox(
                    height: windowHeight * 0.08,
                    child: RequireTextField(
                        type: Type.otp,
                        controller: emailVerificationController
                            .emailVerificationOtpController),
                  ),
                  Obx(() => emailVerificationController
                              .isResendButtonDisable.value &&
                          emailVerificationController.countTimes != 3
                      ? Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              "Can resend code after ",
                              style: TextStyles.headingSubtitleStyle,
                            ),
                            Text(
                              emailVerificationController.countDownSecond.value
                                  .toString(),
                              style: TextStyles.headingSubtitleStyle,
                            ),
                            Text(
                              " seconds",
                              style: TextStyles.headingSubtitleStyle,
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                            const SizedBox(
                                height: 15,
                                width: 15,
                                child: CircularProgressIndicator(
                                  color: Colors.deepOrange,
                                  strokeWidth: 2,
                                )),
                            const SizedBox(
                              width: 25,
                            ),
                          ],
                        )
                      : Container()),
                  SizedBox(
                    height: windowHeight * 0.08,
                  ),
                  Obx(() => PrimaryButton(
                        windowHeight: windowHeight,
                        windowWidth: windowWidth,
                        buttonTitle: "Resend Code",
                        isDisable: emailVerificationController
                            .isResendButtonDisable.value,
                        onPressed: () async {
                          emailVerificationController.apiCallResendEmailCode();
                        },
                      )),
                  SizedBox(
                    height: windowHeight * 0.03,
                  ),
                  PrimaryButton(
                    windowHeight: windowHeight,
                    windowWidth: windowWidth,
                    buttonTitle: "Continue",
                    onPressed: () async {
                      emailVerificationController.onSubmitButtonTap();
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
