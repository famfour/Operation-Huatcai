import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/pricing_table_controller.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Widgets/primary_button.dart';

class PricingTableScreen extends StatefulWidget {
  const PricingTableScreen({Key? key}) : super(key: key);

  @override
  State<PricingTableScreen> createState() => _PricingTableScreenState();
}

class _PricingTableScreenState extends State<PricingTableScreen> {
  bool toggleValue = true;

  PricingTableScreenController pricingTableScreenController =
      Get.put(PricingTableScreenController());

  @override
  Widget build(BuildContext context) {
    double windowWidth = MediaQuery.of(context).size.width; //get screen width
    double windowHeight =
        MediaQuery.of(context).size.height; //get screen height

    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: AppColors.red, // status bar color
    ));

    return Scaffold(
      appBar: PreferredSize(
        //preferredSize: const Size.fromHeight(70.0),
        preferredSize: AppBar().preferredSize,
        child: SafeArea(
          child: Container(
            color: AppColors.white,
            child: AppBar(
              shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(40.0),
              )),
              elevation: 0,
              backgroundColor: AppColors.red,
              title: Text(
                "Pricing Table",
                style: TextStyles.primaryButtonTextStyle,
              ),
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: windowHeight * 0.02,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  "Monthly",
                  style: TextStyles.periodTitleTextStyle,
                ),
                //
                SizedBox(
                  width: windowWidth * 0.03,
                ),
                Obx(
                  () => CupertinoSwitch(
                      activeColor: AppColors.red,
                      value: pricingTableScreenController
                          .isAnnulPackageSelected.value,
                      onChanged: (value) {
                        pricingTableScreenController.onToggle(
                            value); //package selection monthly or annual
                      }),
                ),
                SizedBox(
                  width: windowWidth * 0.03,
                ),
                Text(
                  "Annual",
                  style: TextStyles.periodTitleTextStyle,
                ),
                //
              ],
            ), //

            SizedBox(
              height: windowHeight * 0.03,
            ),

            SizedBox(
              height: windowHeight * .61,
              child: PageView.builder(
                itemCount: 3,
                controller: PageController(viewportFraction: 0.76),
                onPageChanged: (int index) => setState(() => _index = index),
                itemBuilder: (_, i) {
                  return Transform.scale(
                      scale: i == _index ? 1 : 0.85,
                      child: packageCard(
                          windowHeight, windowWidth) //widget package card
                      );
                },
              ),
            ),

            SizedBox(
              height: windowHeight * 0.03,
            ),
            PrimaryButton(
              windowHeight: windowHeight,
              windowWidth: windowWidth,
              onPressed: () {},
              buttonTitle: 'Select',
            ),
            SizedBox(
              height: windowHeight * 0.02,
            ),
            GestureDetector(
              onTap: () {
                pricingTableScreenController.onSkip();
              },
              child: const Text(
                "Skip",
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ),
    );
  }

  int _index = 0;

  Widget packageCard(windowHeight, double windowWidth) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: windowWidth * .009),
      decoration: BoxDecoration(
          border: Border.all(
            color: AppColors.red,
          ),
          borderRadius: BorderRadius.circular(25)),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 25),
        child: Column(
          children: [
            SizedBox(
              height: windowHeight * 0.03,
            ),
            Text(
              "BASIC",
              style: TextStyles.basicTextStyle,
            ),
            const Divider(
              color: Colors.grey,
            ),
            SizedBox(
              height: windowHeight * 0.01,
            ),
            Text(
              "Free",
              style: TextStyles.freeTextStyle,
            ),
            SizedBox(
              height: windowHeight * 0.015,
            ),
            Text(
              "Per Annum",
              style: TextStyles.perAnnumTitleTextStyle,
            ),
            SizedBox(
              height: windowHeight * 0.04,
            ),
            Row(
              children: [
                Text("1 Devices", style: TextStyles.pricingTitleTextStyle),
                const Spacer(),
                SizedBox(
                  child: Image.asset(
                    'assets/images/select_pricing.png',
                    height: 15,
                    // semanticsLabel: 'Logo',
                  ),
                ),
              ],
            ),
            SizedBox(
              height: windowHeight * 0.02,
            ),
            Row(
              children: [
                Text("Clearance Fee 20%",
                    style: TextStyles.pricingTitleTextStyle),
                const Spacer(),
                SizedBox(
                  child: Image.asset(
                    'assets/images/select_pricing.png',
                    height: 15,
                    // semanticsLabel: 'Logo',
                  ),
                ),
              ],
            ),
            SizedBox(
              height: windowHeight * 0.02,
            ),
            Row(
              children: [
                Text("Standard Rates", style: TextStyles.pricingTitleTextStyle),
                const Spacer(),
                SizedBox(
                  child: Image.asset(
                    'assets/images/select_pricing.png',
                    height: 15,
                    // semanticsLabel: 'Logo',
                  ),
                ),
              ],
            ),
            SizedBox(
              height: windowHeight * 0.02,
            ),
            Row(
              children: [
                Text("Excluding Profile analysis \n & Equity loan Calculator",
                    style: TextStyles.pricingTitleTextStyle),
                const Spacer(),
                SizedBox(
                  child: Image.asset(
                    'assets/images/select_pricing.png',
                    height: 15,
                    // semanticsLabel: 'Logo',
                  ),
                ),
              ],
            ),
            SizedBox(
              height: windowHeight * 0.02,
            ),
            Row(
              children: [
                Text("Limit to 5 Leads",
                    style: TextStyles.pricingTitleTextStyle),
                const Spacer(),
                SizedBox(
                  child: Image.asset(
                    'assets/images/select_pricing.png',
                    height: 15,
                    // semanticsLabel: 'Logo',
                  ),
                ),
              ],
            ),
            SizedBox(
              height: windowHeight * 0.02,
            ),
            Row(
              children: [
                Text("Lead Generation",
                    style: TextStyles.pricingTitleTextStyle),
                const Spacer(),
                SizedBox(
                  child: Image.asset(
                    'assets/images/close_pricing.png',
                    height: 15,
                    // semanticsLabel: 'Logo',
                  ),
                ),
              ],
            ),
            SizedBox(
              height: windowHeight * 0.02,
            ),
            Row(
              children: [
                Text("Renewal Leads", style: TextStyles.pricingTitleTextStyle),
                const Spacer(),
                SizedBox(
                  child: Image.asset(
                    'assets/images/close_pricing.png',
                    height: 15,
                    // semanticsLabel: 'Logo',
                  ),
                ),
              ],
            ),
            SizedBox(
              height: windowHeight * 0.02,
            ),
            Row(
              children: [
                Text("TO Do List", style: TextStyles.pricingTitleTextStyle),
                const Spacer(),
                SizedBox(
                  child: Image.asset(
                    'assets/images/close_pricing.png',
                    height: 15,
                    // semanticsLabel: 'Logo',
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
