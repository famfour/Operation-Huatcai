import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/scan_qr_controller.dart';
import 'package:iqrate/Controller/two_fact_auth_screen2_controller.dart';
import 'package:iqrate/DeviceManager/container_styles.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/secondary_button.dart';
import 'package:qr_flutter/qr_flutter.dart';

class QRCodeScanner extends StatelessWidget {
  const QRCodeScanner({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ScanQrController scanQrController = Get.put(ScanQrController());
    final TwoFactAuthScreenTwoController twoFactAuthScreenTwoController =
        Get.find();

    double windowWidth = MediaQuery.of(context).size.width;
    double windowHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SafeArea(
          child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 25.0, horizontal: 18),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // SizedBox(
              //   height: windowHeight * 0.1,
              // ),
              Center(
                child: SvgPicture.asset(
                  "assets/images/two_fact.svg",
                ),
              ),
              SizedBox(
                height: windowHeight * 0.1,
              ),
              Text("Scan Through Google Authenticator",
                  style: TextStyles.introScreenTitlesSmall),
              SizedBox(
                height: windowHeight * 0.03,
              ),
              Text(
                "Please use your Google Authenticator app to scan the QR code below",
                style: TextStyles.introScreenDescriptions,
              ),
              SizedBox(
                height: windowHeight * 0.05,
              ),
              Obx(
                () => Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    QrImage(
                      size: windowWidth * 0.36,
                      data: twoFactAuthScreenTwoController
                          .enableTwoMfaResponseModel.value.otpauthUrl
                          .toString(),
                      version: QrVersions.auto,
                    ),
                    const Text("or"),
                    InkWell(
                      onTap: () {
                        twoFactAuthScreenTwoController.copyCode();
                      },
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          Image.asset(
                            "assets/images/authenticator_code.png",
                            height: windowWidth * 0.36,
                          ),
                          /*SvgPicture.asset(
                            "assets/images/authenticator_code.svg",
                          ),*/
                          Center(
                              child: Text(
                            twoFactAuthScreenTwoController
                                .enableTwoMfaResponseModel.value.secret
                                .toString(),
                            style: const TextStyle(fontSize: 13),
                          ))
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: windowHeight * 0.05,
              ),
              PrimaryButton(
                windowHeight: windowHeight,
                windowWidth: windowWidth,
                buttonTitle: "Use Authentication App",
                onPressed: () {
                  scanQrController.onSubmit();
                },
              ),
              SizedBox(
                height: windowHeight * 0.03,
              ),
              SecondaryButton(
                windowHeight: windowHeight,
                windowWidth: windowWidth,
                kGradientBoxDecoration:
                    ContainerStyles.kGradientBoxDecorationSecondaryButton,
                kInnerDecoration:
                    ContainerStyles.kInnerDecorationSecondaryButton,
                onPressed: () {
                  scanQrController.onCancel();
                },
                buttonTitle: "Cancel",
              ),
            ],
          ),
        ),
      )),
    );
  }
}
