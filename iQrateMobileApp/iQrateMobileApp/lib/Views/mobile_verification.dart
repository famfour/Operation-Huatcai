import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/mobile_verification_controller.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class MobileVerification extends StatelessWidget {
  MobileVerification({Key? key}) : super(key: key);
  final MobileVerificationController mobileVerificationController =
      Get.put(MobileVerificationController());

  @override
  Widget build(BuildContext context) {
    double windowWidth = MediaQuery.of(context).size.width;
    double windowHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      body: GestureDetector(
        // This onTap is used to dismiss the keyboard when the user taps outside of the keyboard
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: SafeArea(
          child: SingleChildScrollView(
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 25.0, horizontal: 18),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: windowHeight * 0.08,
                  ),
                  Center(
                    child: SvgPicture.asset(
                      "assets/images/mobile_verification.svg",
                    ),
                  ),
                  SizedBox(
                    height: windowHeight * 0.08,
                  ),
                  Text("Please verify your Mobile Number",
                      style: TextStyles.introScreenTitlesSmall),
                  SizedBox(
                    height: windowHeight * 0.03,
                  ),
                  Text(
                    "An OTP has been sent to your registered mobile number. Please enter the verification code.",
                    style: TextStyles.introScreenDescriptions,
                  ),
                  SizedBox(
                    height: windowHeight * 0.08,
                  ),

                  /* Center(
                    child: SizedBox(
                      height: windowHeight * 0.08,
                      child: RequireTextField(
                          type: Type.otpV2,
                          otpFieldController: mobileVerificationController.otpFieldController,
                          controller:
                              mobileVerificationController.mobileOtpController),
                    ),
                  ),
*/

                  Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: windowWidth * 0.02),
                    child: PinCodeTextField(
                      appContext: context,
                      pastedTextStyle: TextStyle(
                        color: Colors.green.shade600,
                        fontWeight: FontWeight.bold,
                      ),
                      length: 6,
                      obscureText: false,
                      obscuringCharacter: '*',
                      blinkWhenObscuring: true,
                      animationType: AnimationType.fade,
                      pinTheme: PinTheme(
                        shape: PinCodeFieldShape.box,
                        borderWidth: 1,
                        borderRadius: BorderRadius.circular(5),
                        fieldHeight: 55,
                        fieldWidth: 55,
                        activeFillColor: Colors.white,
                        inactiveFillColor: Colors.white,
                        inactiveColor: Colors.grey,
                        selectedFillColor: Colors.white,
                        disabledColor: Colors.red,
                        selectedColor: Colors.grey,
                        activeColor: Colors.black54,
                      ),
                      cursorColor: Colors.black,
                      textStyle: TextStyles.leadsTextStyle1,
                      animationDuration: const Duration(milliseconds: 300),
                      enableActiveFill: true,
                      controller:
                          mobileVerificationController.mobileOtpController,
                      keyboardType: TextInputType.number,
                      /*boxShadows: const [
                      BoxShadow(
                        offset: Offset(0, 1),
                        color: AppColors.filledColor,
                        blurRadius: 10,
                      )
                    ],*/
                      onCompleted: (v) {
                        if (kDebugMode) {
                          print("Completed");
                        }
                      },
                      onChanged: (value) {
                        if (kDebugMode) {
                          print(value);
                        }
                        /* setState(() {
                        currentText = value;
                      });*/
                      },
                      beforeTextPaste: (text) {
                        if (kDebugMode) {
                          print("Allowing to paste $text");
                        }
                        return true;
                      },
                    ),
                  ),
                  Obx(() => mobileVerificationController
                              .isResendButtonDisable.value &&
                          mobileVerificationController.countTimes != 3
                      ? Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              "Can resend code after ",
                              style: TextStyles.headingSubtitleStyle,
                            ),
                            Text(
                              mobileVerificationController.countDownSecond.value
                                  .toString(),
                              style: TextStyles.headingSubtitleStyle,
                            ),
                            Text(
                              " seconds",
                              style: TextStyles.headingSubtitleStyle,
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                            const SizedBox(
                                height: 15,
                                width: 15,
                                child: CircularProgressIndicator(
                                  color: Colors.deepOrange,
                                  strokeWidth: 2,
                                )),
                            const SizedBox(
                              width: 25,
                            ),
                          ],
                        )
                      : Container()),
                  SizedBox(
                    height: windowHeight * 0.08,
                  ),
                  Obx(() => !mobileVerificationController
                          .isResendButtonHide.value
                      ? PrimaryButton(
                          windowHeight: windowHeight,
                          windowWidth: windowWidth,
                          buttonTitle: "Resend Code",
                          isDisable: mobileVerificationController
                              .isResendButtonDisable.value,
                          onPressed: () async {
                            mobileVerificationController.apiCallResendSMSCode();
                          },
                        )
                      : const SizedBox()),
                  SizedBox(
                    height: windowHeight * 0.03,
                  ),
                  PrimaryButton(
                    windowHeight: windowHeight,
                    windowWidth: windowWidth,
                    buttonTitle: "Continue",
                    onPressed: () async {
                      mobileVerificationController.onSubmitButtonTap();
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
