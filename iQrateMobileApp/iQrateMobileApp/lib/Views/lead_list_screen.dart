// ignore_for_file: unnecessary_null_comparison, deprecated_member_use

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/leads_view_controller.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/container_styles.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Model/response_model.dart/fetch_lead_list_response_model.dart';
import 'package:iqrate/Service/GetControllers.dart';
import 'package:iqrate/Utils/text_formatter.dart';
import 'package:iqrate/Views/co_broke/LeadItemModel.dart';
import 'package:iqrate/Widgets/form_field_title.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';
import 'package:iqrate/Widgets/secondary_button.dart';
import 'package:logger/logger.dart';
import 'package:multiselect/multiselect.dart';
import 'package:url_launcher/url_launcher.dart';

class LeadListScreen extends StatefulWidget {
  const LeadListScreen({Key? key}) : super(key: key);

  @override
  _LeadListScreenState createState() => _LeadListScreenState();
}

class _LeadListScreenState extends State<LeadListScreen> {
  late double windowHeight;
  late double windowWidth;
  // final oCcy = NumberFormat("#,##0.00", "en_US");
  final List<String> leadStatus = [
    'All',
    'Pending',
    'Won',
    'Submitted',
    'Rework',
    'Lost'
  ]; // Option 2
  // String? selectedStatus;
  Result? data;
  int currentleadId = 0;

  final LeadsViewController leadsViewController =
      Get.put(LeadsViewController());
  bool isCoBroke(Result data) {
    return data.leadType == 'co_broke';
  }

  @override
  Widget build(BuildContext context) {
    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.width;
    return Scaffold(
        extendBody: true,
        extendBodyBehindAppBar: true,
        body: Stack(
          children: [
            RefreshIndicator(
                displacement: 100,
                backgroundColor: Colors.green,
                color: Colors.white,
                strokeWidth: 3,
                triggerMode: RefreshIndicatorTriggerMode.onEdge,
                onRefresh: () async {
                  await leadsViewController.fetchLeads();
                  // await leadsViewController.fetchLeadsV2();
                  Future.delayed(const Duration(milliseconds: 0), () {
                    FocusManager.instance.primaryFocus!.unfocus();
                    Fluttertoast.showToast(
                      timeInSecForIosWeb: 3,
                      msg: "Refreshed Successfully",
                      backgroundColor: Colors.green,
                      toastLength: Toast.LENGTH_LONG,
                      gravity: ToastGravity.BOTTOM,
                      textColor: Colors.white,
                      fontSize: 20.0,
                    );
                  });
                },
                child: Obx(
                  () => Stack(children: [
                    leadLists(),
                    leadsViewController.showFilter.value
                        ? FilterSection(
                            windowHeight: windowHeight,
                            windowWidth: windowWidth,
                          )
                        : const Offstage(),
                  ]),
                )),
            Obx(() {
              return leadsViewController.errorPopUpCondition.value != 0
                  ? Container(
                      color: Colors.transparent,
                      height: windowHeight,
                      width: windowWidth,
                    )
                  : const Offstage();
            }),
            Obx(() => coBrokeMessagePopUp())
          ],
        ));
  }

  coBrokeMessagePopUp() {
    switch (leadsViewController.errorPopUpCondition.value) {
      case 1: //Case 0 where PDPA is not approved hence we show this message
        return Center(
          child: Container(
            margin: const EdgeInsets.all(10),
            decoration: const BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      blurRadius: 100,
                      spreadRadius: 5,
                      offset: Offset(5, 5),
                      color: Color(0xff808080)),
                  BoxShadow(
                      blurRadius: 100,
                      spreadRadius: 5,
                      offset: Offset(-5, -5),
                      color: Color(0xff808080))
                ],
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.fromLTRB(20, 40, 0, 10),
                  child: Text(
                    'Co-Broke',
                    style: TextStyle(
                        color: AppColors.kPrimaryColor,
                        fontSize: 22,
                        fontWeight: FontWeight.w700),
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.fromLTRB(20, 20, 20, 10),
                  child: Text(
                      'IQrate partnered mortgage broker will close the deal for you. Referral fee will be co-shared at 50%.',
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.w400)),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(20, 50, 20, 40),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Icon(
                        Icons.info_outline_rounded,
                        color: AppColors.kPrimaryColor,
                      ),
                      SizedBox(
                        width: windowWidth * 0.02,
                      ),
                      const Expanded(
                        child: Text(
                            'PDPA not approved. Please get lead to accept PDPA before proceeding.',
                            style: TextStyle(
                                color: AppColors.kPrimaryColor,
                                fontSize: 16,
                                fontWeight: FontWeight.w400)),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(20.0, 0, 20, 20),
                  child: SecondaryButton(
                    windowHeight: windowHeight,
                    windowWidth: windowWidth,
                    kGradientBoxDecoration:
                        ContainerStyles.kGradientBoxDecorationSecondaryButton,
                    kInnerDecoration:
                        ContainerStyles.kInnerDecorationSecondaryButton,
                    buttonTitle: 'Back',
                    onPressed: () {
                      leadsViewController.errorPopUpCondition.value = 0;
                    },
                  ),
                ),
              ],
            ),
          ),
        );
      case 2: //Case 1 where Bank submission is already initiated and we this error message
        return Center(
          child: Container(
            margin: const EdgeInsets.all(10),
            decoration: const BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      blurRadius: 100,
                      spreadRadius: 5,
                      offset: Offset(5, 5),
                      color: Color(0xff808080)),
                  BoxShadow(
                      blurRadius: 100,
                      spreadRadius: 5,
                      offset: Offset(-5, -5),
                      color: Color(0xff808080))
                ],
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.fromLTRB(20, 40, 0, 10),
                  child: Text(
                    'Co-Broke',
                    style: TextStyle(
                        color: AppColors.kPrimaryColor,
                        fontSize: 22,
                        fontWeight: FontWeight.w700),
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.fromLTRB(20, 20, 20, 10),
                  child: Text(
                      'IQrate partnered mortgage broker will close the deal for you. Referral fee will be co-shared at 50%.',
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.w400)),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(20, 50, 20, 40),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Icon(
                        Icons.info_outline_rounded,
                        color: AppColors.kPrimaryColor,
                      ),
                      SizedBox(
                        width: windowWidth * 0.02,
                      ),
                      const Expanded(
                        child: Text(
                            'Bank Submission has been initiated for lead. Unable to proceed with co-broke. Please complete your bank submission process',
                            style: TextStyle(
                                color: AppColors.kPrimaryColor,
                                fontSize: 16,
                                fontWeight: FontWeight.w400)),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(20.0, 0, 20, 20),
                  child: SecondaryButton(
                    windowHeight: windowHeight,
                    windowWidth: windowWidth,
                    kGradientBoxDecoration:
                        ContainerStyles.kGradientBoxDecorationSecondaryButton,
                    kInnerDecoration:
                        ContainerStyles.kInnerDecorationSecondaryButton,
                    buttonTitle: 'Back',
                    onPressed: () {
                      leadsViewController.errorPopUpCondition.value = 0;
                    },
                  ),
                ),
              ],
            ),
          ),
        );
      case 3: //case 2 confirmation popup where we ask again the user if they wanna proceed for co-broke
        return Center(
          child: Container(
            margin: const EdgeInsets.all(10),
            decoration: const BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      blurRadius: 100,
                      spreadRadius: 5,
                      offset: Offset(5, 5),
                      color: Color(0xff808080)),
                  BoxShadow(
                      blurRadius: 100,
                      spreadRadius: 5,
                      offset: Offset(-5, -5),
                      color: Color(0xff808080))
                ],
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.fromLTRB(20, 40, 0, 10),
                  child: Text(
                    'Co-Broke',
                    style: TextStyle(
                        color: AppColors.kPrimaryColor,
                        fontSize: 22,
                        fontWeight: FontWeight.w700),
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.fromLTRB(20, 20, 20, 10),
                  child: Text(
                      'IQrate partnered mortgage broker will close the deal for you. Referral fee will be co-shared at 50%.',
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.w400)),
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: PrimaryButton(
                    windowHeight: windowHeight,
                    windowWidth: windowWidth,
                    buttonTitle: 'Continue',
                    onPressed: () {
                      leadsViewController.errorPopUpCondition.value = 0;
                      leadsViewController.applyForCoBroke(
                          currentleadId); //API call requesting for co-broke
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(20.0, 0, 20, 20),
                  child: SecondaryButton(
                    windowHeight: windowHeight,
                    windowWidth: windowWidth,
                    kGradientBoxDecoration:
                        ContainerStyles.kGradientBoxDecorationSecondaryButton,
                    kInnerDecoration:
                        ContainerStyles.kInnerDecorationSecondaryButton,
                    buttonTitle: 'Back',
                    onPressed: () {
                      leadsViewController.errorPopUpCondition.value = 0;
                    },
                  ),
                ),
              ],
            ),
          ),
        );

      default:
        {
          return const Offstage();
        }
    }
  }

  Widget leadLists() {
    return Column(
      children: [
        Container(
          margin: const EdgeInsets.only(left: 15, right: 15, bottom: 9),
          height: windowHeight * 0.07,
          width: windowWidth * 0.9,
          padding: EdgeInsets.symmetric(horizontal: windowWidth * 0.05),
          decoration: BoxDecoration(
            color: Colors.black12,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.2),
                spreadRadius: 3,
                blurRadius: 7,
                offset: const Offset(0, 3), // changes position of shadow
              ),
            ],
          ),
          child: DropdownButtonHideUnderline(
            key: const Key("leadStatusDropdown"),
            child: DropdownButton(
              hint: Text(
                'All',
                style: TextStyles.leadsTextStyleBold,
              ),
              // Not necessary for Option 1
              value: leadsViewController.selectedStatus!.value,
              onChanged: (String? value) {
                if (value == 'All') {
                  setState(() {
                    leadsViewController.selectedStatus!.value = value!;
                    leadsViewController.filterByLeadStatus(
                        leadsViewController.selectedStatus!.value);
                  });
                } else {
                  setState(() {
                    leadsViewController.selectedStatus!.value = value!;
                    leadsViewController.filterByLeadStatus(leadsViewController
                        .selectedStatus!.value
                        .toLowerCase());
                  });
                }
              },
              style: TextStyles.leadsTextStyleBold,
              icon: const Icon(
                Icons.keyboard_arrow_down_outlined,
                color: Colors.black,
              ),
              items: leadStatus.map((status) {
                return DropdownMenuItem(
                  child: Text(status),
                  value: status,
                );
              }).toList(),
            ),
          ),
        ),
        SizedBox(
          height: windowHeight * 0.61,
          width: windowWidth * 0.9,
          child: Obx(() => leadsViewController.listOfLeads != null &&
                  leadsViewController.listOfLeads.isNotEmpty
              ? ListView.builder(
                  controller: leadsViewController.scrollController,
                  key: const Key("leadList"),
                  itemCount: leadsViewController.listOfLeads.length,
                  itemBuilder: (BuildContext context, int index) {
                    data = leadsViewController.listOfLeads.elementAt(index);
                    var leadItemModel =
                        leadsViewController.listOfLeadsV2[index];

                    // Logger().d(leadItemModel.toJson());
                    return leadListItem(data, index, leadItemModel);
                  })
              : Center(
                  child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                        height: windowHeight * 0.15,
                        child: SvgPicture.asset('assets/images/nodata.svg')),
                    Text(
                      'No data to show',
                      style: TextStyles.bankSubmissionCardTitleDisabled,
                    )
                  ],
                ))),
        ),
      ],
    );
  }

  Widget leadListItem(Result? data, int index, LeadItemModel leadItemModel) {
    data!.clients!.sort((a, b) => a.id!.compareTo(b.id!));

    return data.clients!.isNotEmpty
        ? Container(
            margin: const EdgeInsets.all(10),
            padding: EdgeInsets.zero,
            width: windowWidth * 0.9,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.2),
                  spreadRadius: 3,
                  blurRadius: 7,
                  offset: const Offset(0, 3), // changes position of shadow
                ),
              ],
            ),
            child: Column(
              children: [
                Container(
                  height: windowHeight * 0.05,
                  padding: EdgeInsets.only(left: windowWidth * 0.04),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      leadItemModel.leadStatus == 'won'
                          ? SvgPicture.asset(
                              'assets/icons/won_status_icon.svg',
                              color: AppColors.leadListTitleGreen,
                            )
                          : leadItemModel.leadStatus == 'rework'
                              ? SvgPicture.asset(
                                  'assets/icons/lead_rework_icon.svg',
                                  color: AppColors.leadListTitle,
                                )
                              : leadItemModel.leadStatus == 'lost'
                                  ? SvgPicture.asset(
                                      'assets/icons/lead_lost_icon.svg',
                                      color: AppColors.leadListTitleGrey,
                                    )
                                  : SvgPicture.asset(
                                      'assets/icons/leads_time.svg',
                                      color: AppColors.leadListTitle,
                                    ),
                      SizedBox(
                        width: windowWidth * 0.02,
                      ),
                      Text(
                        (leadItemModel.leadStatus ?? "Pending")
                                .capitalizeFirst ??
                            "Pending",
                        style: leadItemModel.leadStatus == 'won'
                            ? TextStyles.basicTextStyleGreen
                            : leadItemModel.leadStatus == 'lost'
                                ? TextStyles.basicTextStyleGrey
                                : TextStyles.basicTextStyleRed,
                      ),
                      const Spacer(),
                      isCoBroke(data)
                          ? Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 20),
                              child: Text(
                                data.coBrokeStatus.toString(),
                                style: const TextStyle(
                                    color: AppColors.kPrimaryColor),
                              ),
                            )
                          : data.coBrokeStatus != null
                              ? Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 20),
                                  child: Text(
                                    data.coBrokeStatus.toString(),
                                    style: const TextStyle(
                                        color: AppColors.kPrimaryColor),
                                  ),
                                )
                              : const SizedBox(),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: windowWidth * 0.04),
                  child: Divider(
                    thickness: 3,
                    color: leadItemModel.leadStatus == 'won'
                        ? AppColors.leadListExpansionGreenBG
                        : leadItemModel.leadStatus == 'lost'
                            ? AppColors.leadListTitleGrey
                            : AppColors.leadListExpansionBG,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      left: windowWidth * 0.04,
                      right: windowWidth * 0.04,
                      top: windowHeight * 0.01),
                  child: Row(
                    children: [
                      Expanded(
                        child: Text(
                          data.clients![0].name.toString(),
                          style: const TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                      // const Spacer(),
                      SizedBox(
                        height: windowHeight * 0.03,
                        child: Container(
                          padding: const EdgeInsets.only(left: 20, right: 20),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              GestureDetector(
                                onTap: () async {
                                  final url =
                                      "tel:${'+' + data.clients![0].countryCode.toString()}${data.clients![0].phoneNumber}";
                                  if (await canLaunch(url)) {
                                    await launch(url);
                                  } else {
                                    Future.delayed(
                                        const Duration(milliseconds: 0), () {
                                      FocusManager.instance.primaryFocus!
                                          .unfocus();
                                      Fluttertoast.showToast(
                                        timeInSecForIosWeb: 3,
                                        msg: "Unable to lauch dialer",
                                        backgroundColor: Colors.red,
                                        toastLength: Toast.LENGTH_LONG,
                                        gravity: ToastGravity.BOTTOM,
                                        textColor: Colors.white,
                                        fontSize: 20.0,
                                      );
                                    });
                                  }
                                },
                                child: SvgPicture.asset(
                                  'assets/icons/leads_call.svg',
                                  color: Colors.black,
                                ),
                              ),
                              SizedBox(
                                width: windowWidth * 0.02,
                              ),
                              Container(
                                height: 20.0,
                                width: 1.0,
                                color: Colors.black,
                                margin: const EdgeInsets.only(
                                    left: 10.0, right: 10.0),
                              ),
                              SizedBox(
                                width: windowWidth * 0.02,
                              ),
                              GestureDetector(
                                onTap: () async {
                                  final url =
                                      "mailto:${data.clients![0].email}";
                                  if (await canLaunch(url)) {
                                    await launch(url);
                                  } else {
                                    Future.delayed(
                                        const Duration(milliseconds: 0), () {
                                      FocusManager.instance.primaryFocus!
                                          .unfocus();
                                      Fluttertoast.showToast(
                                        timeInSecForIosWeb: 3,
                                        msg: "Unable to launch mail",
                                        backgroundColor: Colors.red,
                                        toastLength: Toast.LENGTH_LONG,
                                        gravity: ToastGravity.BOTTOM,
                                        textColor: Colors.white,
                                        fontSize: 20.0,
                                      );
                                    });
                                  }
                                },
                                child: SvgPicture.asset(
                                  'assets/icons/leads_mail.svg',
                                  color: Colors.black,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: windowHeight * 0.02,
                ),
                Padding(
                  padding: EdgeInsets.only(
                    left: windowWidth * 0.04,
                  ),
                  child: Row(
                    children: [
                      data.clients![0].pdpaStatus == false
                          ? Container(
                              // height: windowHeight * 0.03,
                              // width: windowWidth * 0.35,
                              //padding: EdgeInsets.only(left: windowWidth * 0.04),
                              decoration: BoxDecoration(
                                color: const Color(0XFFFF6A3A),
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: const Center(
                                child: Padding(
                                  padding: EdgeInsets.all(10.0),
                                  child: Text(
                                    'PDPA Pending',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 18,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ),
                            )
                          : Container(
                              // height: windowHeight * 0.03,
                              // width: windowWidth * 0.35,
                              //padding: EdgeInsets.only(left: windowWidth * 0.04),
                              decoration: BoxDecoration(
                                color: const Color(0XFF34A853),
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: const Center(
                                child: Padding(
                                  padding: EdgeInsets.all(10.0),
                                  child: Text(
                                    'PDPA Approved',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 18,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ),
                            ),
                    ],
                  ),
                ),
                SizedBox(
                  height: windowHeight * 0.02,
                ),
                Padding(
                  padding: EdgeInsets.only(
                    left: windowWidth * 0.04,
                    right: windowWidth * 0.04,
                  ),
                  child: Row(
                    children: [
                      Expanded(
                        child: Text(
                          data.loanAmount == null || data.loanAmount == 0.0
                              ? "Loan Amount: "
                              : "Loan Amount: " +
                                  convertToCurrency(data.loanAmount),
                          style: const TextStyle(
                              color: Colors.grey,
                              fontSize: 20,
                              fontWeight: FontWeight.normal),
                        ),
                      ),
                      const Spacer(),
                      const Text(
                        '+\$',
                        style: TextStyle(
                            color: Color(0xff34A853),
                            fontSize: 20,
                            fontWeight: FontWeight.normal),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: windowHeight * 0.02,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: windowWidth * 0.04),
                  child: const Divider(thickness: 3),
                ),
                data.isExpanded
                    ? Column(
                        children: [
                          expandableSummery("Summary"),
                          SizedBox(
                            height: windowHeight * 0.01,
                          ),
                          expandableButton("Manage Loan",
                              // canEdit: leadItemModel.canEdit!,
                              //canEdit: getcanEdit(leadItemModel), //Checking condition if we can enter the manage loan screen with this condition
                              canEdit: getCanEdit(
                                  leadItemModel), //Can edit is true here even after we submit for co-broke
                              onTap: () {
                            debugPrint("clicked Manage Loan" +
                                data.clients![0].id.toString() +
                                " data.leadType ${data.leadType}");
                            GetControllers.shared
                                .getGeneratePackagesController()
                                .coBrokerData = leadItemModel;
                            Logger().d(leadItemModel.toJson());
                            // Logger().d(GetControllers.shared
                            //     .getGeneratePackagesController()
                            //     .coBrokerData
                            //     .toJson());
                            GetControllers.shared
                                .getGeneratePackagesController()
                                .isCoBroker = data.leadType == "co_broke";
                            GetControllers.shared
                                .getGeneratePackagesController()
                                .coBrokeStatus
                                .value = data.coBrokeStatus!;
                            leadsViewController.onTapManageLoan(data);
                            // GetControllers.shared.getGeneratePackagesController().isCoBroker = true;
                            // print(
                            //     "GetControllers.shared.getGeneratePackagesController().isCoBroker ${GetControllers.shared.getGeneratePackagesController().isCoBroker}");
                            // leadsViewController.onTapManageLoan(data);
                            // print(
                            // "GetControllers.shared.getGeneratePackagesController().isCoBroker ${GetControllers.shared.getGeneratePackagesController().isCoBroker}");
                          }),
                          isCoBroke(data)
                              ? const SizedBox()
                              : data.leadType ==
                                      'yourself' //if lead_type is yourself then we hide this button
                                  ? const SizedBox()
                                  : SizedBox(
                                      height: windowHeight * 0.01,
                                    ),
                          isCoBroke(data)
                              ? const SizedBox()
                              : data.leadType ==
                                      'yourself' //if lead_type is yourself then we hide this button
                                  ? const SizedBox()
                                  : expandableButton("Request To Co-Broke",
                                      canEdit: getcanEdit(leadItemModel),
                                      onTap: () {
                                      currentleadId = data.id!;
                                      // if (data.canEdit == true &&
                                      //     data.clients != null) {
                                      //   for (int i = 0;
                                      //       i < data.clients!.length;
                                      //       i++) {
                                      //     if (data.clients![i].pdpaStatus !=
                                      //         true) {
                                      //       leadsViewController
                                      //           .allAplicantPDPAApproved
                                      //           .value = false;
                                      //     }
                                      //     if (i == data.clients!.length - 1) {
                                      //       if (leadsViewController
                                      //               .allAplicantPDPAApproved
                                      //               .value ==
                                      //           false) {
                                      //         leadsViewController
                                      //                 .errorPopUpCondition
                                      //                 .value =
                                      //             1; //pop-up to show PDPA not complete error
                                      //       } else {
                                      //         leadsViewController
                                      //                 .errorPopUpCondition
                                      //                 .value =
                                      //             3; //Allow for co-broke pop-up
                                      //       }
                                      //     }
                                      //   }
                                      // } else if (data.canEdit == false) {
                                      //   leadsViewController
                                      //           .errorPopUpCondition.value =
                                      //       2; //pop-up to show bank submission started error
                                      // } else {
                                      //   debugPrint('NOT SATISFIED CONDITION');
                                      // }
                                      currentleadId = data.id!;
                                      if (data.canEdit == true &&
                                          leadsViewController
                                                  .checkIfPDPACompletedForAll(
                                                      data.clients) ==
                                              true) {
                                        leadsViewController
                                                .errorPopUpCondition.value =
                                            3; //Allow for co-broke pop-up
                                      } else if (leadsViewController
                                              .checkIfPDPACompletedForAll(
                                                  data.clients) ==
                                          false) {
                                        leadsViewController
                                                .errorPopUpCondition.value =
                                            1; //pop-up to show PDPA not complete error
                                      } else if (data.canEdit == false) {
                                        leadsViewController
                                                .errorPopUpCondition.value =
                                            2; //pop-up to show bank submission started error
                                      } else {
                                        debugPrint('NOT SATISFIED CONDITION');
                                      }
                                    }),
                          isCoBroke(data)
                              ? const SizedBox()
                              : SizedBox(
                                  height: windowHeight * 0.01,
                                ),
                          isCoBroke(data)
                              ? const SizedBox()
                              : expandableButton("My Documents",
                                  canEdit: true, //getcanEdit(leadItemModel),
                                  onTap: () {
                                  leadsViewController.onTapDocuments(data);
                                  //Get.toNamed(myDocumentsScreen);
                                }),
                          SizedBox(
                            height: windowHeight * 0.01,
                          ),
                          expandableButton(
                            "Mark Lead as Lost",
                            onTap: () {
                              leadsViewController.showMarkLeadLostAlert();
                            },
                            canEdit: leadItemModel.canEdit!,
                          ),
                          SizedBox(
                            height: windowHeight * 0.01,
                          ),
                        ],
                      )
                    : const SizedBox(),
                GestureDetector(
                  onTap: () {
                    //leadsViewController.isExpandItem.toggle();

                    debugPrint(">>>>>tapped>>>>>" + data.isExpanded.toString());
                    bool nextStatus =
                        !(leadsViewController.listOfLeads[index].isExpanded);
                    for (var i = 0;
                        i < leadsViewController.listOfLeads.length;
                        i++) {
                      leadsViewController.listOfLeads[i].isExpanded = false;
                    }
                    leadsViewController.listOfLeads[index].isExpanded =
                        nextStatus;
                    setState(() {});

                    // if (data.isExpanded) {
                    //   setState(() {
                    //     leadsViewController.leadList.value.results
                    //         ?.elementAt(index)
                    //         .isExpanded = false;
                    //   });
                    // } else {
                    //   setState(() {
                    //     leadsViewController.leadList.value.results
                    //         ?.elementAt(index)
                    //         .isExpanded = true;
                    //   });
                    // }
                    //
                    // setState(() {});
                  },
                  child: Container(
                    height: windowHeight * 0.024,
                    width: windowWidth,
                    padding: EdgeInsets.only(left: windowWidth * 0.04),
                    child: data.isExpanded == true
                        ? SvgPicture.asset(
                            'assets/icons/leads_arrow_up.svg',
                          )
                        : SvgPicture.asset(
                            'assets/icons/leads_arrow_down.svg',
                          ),
                  ),
                ),
              ],
            ),
          )
        : Container();
  }

  // Widget leadListItem(Result? data, int index) {
  //   data!.clients!.sort((a, b) => a.id!.compareTo(b.id!));
  //
  //   return data.clients!.isNotEmpty
  //       ? Container(
  //     margin: const EdgeInsets.all(10),
  //     padding: EdgeInsets.zero,
  //     width: windowWidth * 0.9,
  //     decoration: BoxDecoration(
  //       color: Colors.white,
  //       borderRadius: BorderRadius.circular(20),
  //       boxShadow: [
  //         BoxShadow(
  //           color: Colors.grey.withOpacity(0.2),
  //           spreadRadius: 3,
  //           blurRadius: 7,
  //           offset: const Offset(0, 3), // changes position of shadow
  //         ),
  //       ],
  //     ),
  //     child: Column(
  //       children: [
  //         Container(
  //           height: windowHeight * 0.05,
  //           padding: EdgeInsets.only(left: windowWidth * 0.04),
  //           child: Row(
  //             crossAxisAlignment: CrossAxisAlignment.center,
  //             children: [
  //               SvgPicture.asset(
  //                 'assets/icons/leads_time.svg',
  //                 color: AppColors.leadListTitle,
  //               ),
  //               SizedBox(
  //                 width: windowWidth * 0.02,
  //               ),
  //               Text(
  //                 'Pending',
  //                 style: TextStyles.basicTextStyleRed,
  //               ),
  //               const Spacer(),
  //               data.leadType == "co_broke" ?  Padding(
  //                 padding:   const EdgeInsets.symmetric(horizontal: 20),
  //                 child: Text(data.coBrokeStatus.toString(),style: const TextStyle(color: AppColors.kPrimaryColor),),
  //               ) : const SizedBox(),
  //             ],
  //           ),
  //         ),
  //         Padding(
  //           padding: EdgeInsets.symmetric(horizontal: windowWidth * 0.04),
  //           child: Divider(
  //             thickness: 3,
  //             color: AppColors.leadListExpansionBG,
  //           ),
  //         ),
  //         Padding(
  //           padding: EdgeInsets.only(
  //               left: windowWidth * 0.04,
  //               right: windowWidth * 0.04,
  //               top: windowHeight * 0.01),
  //           child: Row(
  //             children: [
  //               Expanded(
  //                 child: Text(
  //                   data.clients![0].name.toString(),
  //                   style: const TextStyle(
  //                       color: Colors.black,
  //                       fontSize: 20,
  //                       fontWeight: FontWeight.w600),
  //                 ),
  //               ),
  //               // const Spacer(),
  //               SizedBox(
  //                 height: windowHeight * 0.03,
  //                 child: Container(
  //                   padding: const EdgeInsets.only(left: 20, right: 20),
  //                   child: Row(
  //                     crossAxisAlignment: CrossAxisAlignment.center,
  //                     children: [
  //                       GestureDetector(
  //                         onTap: () async {
  //                           final url =
  //                               "tel:${'+' + data.clients![0].countryCode.toString()}${data.clients![0].phoneNumber}";
  //                           if (await canLaunch(url)) {
  //                             await launch(url);
  //                           } else {
  //                             Get.snackbar(StringUtils.error,
  //                                 'Unable to launch the dialer',
  //                                 snackPosition: SnackPosition.TOP,
  //                                 backgroundColor: Colors.red,
  //                                 colorText: Colors.white);
  //                           }
  //                         },
  //                         child: SvgPicture.asset(
  //                           'assets/icons/leads_call.svg',
  //                           color: Colors.black,
  //                         ),
  //                       ),
  //                       SizedBox(
  //                         width: windowWidth * 0.02,
  //                       ),
  //                       Container(
  //                         height: 20.0,
  //                         width: 1.0,
  //                         color: Colors.black,
  //                         margin: const EdgeInsets.only(
  //                             left: 10.0, right: 10.0),
  //                       ),
  //                       SizedBox(
  //                         width: windowWidth * 0.02,
  //                       ),
  //                       GestureDetector(
  //                         onTap: () async {
  //                           final url =
  //                               "mailto:${data.clients![0].email}";
  //                           if (await canLaunch(url)) {
  //                             await launch(url);
  //                           } else {
  //                             Get.snackbar(StringUtils.error,
  //                                 'Unable to launch mail app',
  //                                 snackPosition: SnackPosition.TOP,
  //                                 backgroundColor: Colors.red,
  //                                 colorText: Colors.white);
  //                           }
  //                         },
  //                         child: SvgPicture.asset(
  //                           'assets/icons/leads_mail.svg',
  //                           color: Colors.black,
  //                         ),
  //                       ),
  //                     ],
  //                   ),
  //                 ),
  //               ),
  //             ],
  //           ),
  //         ),
  //         SizedBox(
  //           height: windowHeight * 0.02,
  //         ),
  //         Padding(
  //           padding: EdgeInsets.only(
  //             left: windowWidth * 0.04,
  //           ),
  //           child: Row(
  //             children: [
  //               data.clients![0].pdpaStatus == false
  //                   ? Container(
  //                 // height: windowHeight * 0.03,
  //                 // width: windowWidth * 0.35,
  //                 //padding: EdgeInsets.only(left: windowWidth * 0.04),
  //                 decoration: BoxDecoration(
  //                   color: const Color(0XFFFF6A3A),
  //                   borderRadius: BorderRadius.circular(8),
  //                 ),
  //                 child: const Center(
  //                   child: Padding(
  //                     padding: EdgeInsets.all(10.0),
  //                     child: Text(
  //                       'PDPA Pending',
  //                       style: TextStyle(
  //                           color: Colors.white,
  //                           fontSize: 18,
  //                           fontWeight: FontWeight.w600),
  //                     ),
  //                   ),
  //                 ),
  //               )
  //                   : Container(
  //                 // height: windowHeight * 0.03,
  //                 // width: windowWidth * 0.35,
  //                 //padding: EdgeInsets.only(left: windowWidth * 0.04),
  //                 decoration: BoxDecoration(
  //                   color: const Color(0XFF34A853),
  //                   borderRadius: BorderRadius.circular(8),
  //                 ),
  //                 child: const Center(
  //                   child: Padding(
  //                     padding: EdgeInsets.all(10.0),
  //                     child: Text(
  //                       'PDPA Approved',
  //                       style: TextStyle(
  //                           color: Colors.white,
  //                           fontSize: 18,
  //                           fontWeight: FontWeight.w600),
  //                     ),
  //                   ),
  //                 ),
  //               ),
  //             ],
  //           ),
  //         ),
  //         SizedBox(
  //           height: windowHeight * 0.02,
  //         ),
  //         Padding(
  //           padding: EdgeInsets.only(
  //             left: windowWidth * 0.04,
  //             right: windowWidth * 0.04,
  //           ),
  //           child: Row(
  //             children: [
  //               Expanded(
  //                 child: Text(
  //                   data.loanAmount == null || data.loanAmount == 0.0
  //                       ? "Loan Amount: "
  //                       : "Loan Amount: " + getAmountString(data.loanAmount),
  //                   style: const TextStyle(
  //                       color: Colors.grey,
  //                       fontSize: 20,
  //                       fontWeight: FontWeight.normal),
  //                 ),
  //               ),
  //               const Spacer(),
  //               const Text(
  //                 '+\$',
  //                 style: TextStyle(
  //                     color: Color(0xff34A853),
  //                     fontSize: 20,
  //                     fontWeight: FontWeight.normal),
  //               ),
  //             ],
  //           ),
  //         ),
  //         SizedBox(
  //           height: windowHeight * 0.02,
  //         ),
  //         Padding(
  //           padding: EdgeInsets.symmetric(horizontal: windowWidth * 0.04),
  //           child: const Divider(thickness: 3),
  //         ),
  //         data.isExpanded
  //             ? Column(
  //           children: [
  //             expandableSummery("Summary"),
  //             SizedBox(
  //               height: windowHeight * 0.01,
  //             ),
  //             expandableButton("Manage Loan", onTap: () {
  //               debugPrint(
  //                   ">>>>>>" + data.clients![0].id.toString());
  //               leadsViewController.onTapManageLoan(data);
  //             }),
  //             SizedBox(
  //               height: windowHeight * 0.01,
  //             ),
  //             expandableButton("Request To Co-Broke", onTap: () {
  //               //sleadsViewController.onTapManageLoan();
  //               debugPrint("clicked Request To Co-Broke");
  //               // if (data.pd ) {
  //               //
  //               // }
  //               Get.snackbar(
  //                 StringUtils.error,
  //                 'Please get leads to approve PDPA before sending co-broke request',
  //                 backgroundColor: Colors.red,
  //                 colorText: Colors.white,
  //                 duration: const Duration(seconds: 3),
  //               );
  //               // if ()
  //               // leadsViewController.showPopupCoBrokeUnable();
  //             }),
  //             SizedBox(
  //               height: windowHeight * 0.01,
  //             ),
  //             expandableButton("My Documents", onTap: () {
  //               leadsViewController.onTapDocuments(data);
  //               //Get.toNamed(myDocumentsScreen);
  //             }),
  //             SizedBox(
  //               height: windowHeight * 0.01,
  //             ),
  //             expandableButton("Mark Lead as Lost", onTap: () {
  //               leadsViewController.showMarkLeadLostAlert();
  //             }),
  //             SizedBox(
  //               height: windowHeight * 0.01,
  //             ),
  //           ],
  //         )
  //             : const SizedBox(),
  //         GestureDetector(
  //           onTap: () {
  //             //leadsViewController.isExpandItem.toggle();
  //
  //             debugPrint(">>>>>tapped>>>>>" + data.isExpanded.toString());
  //             bool nextStatus =
  //             !(leadsViewController.listOfLeads[index].isExpanded);
  //             for (var i = 0;
  //             i < leadsViewController.listOfLeads.length;
  //             i++) {
  //               leadsViewController.listOfLeads[i].isExpanded = false;
  //             }
  //             leadsViewController.listOfLeads[index].isExpanded =
  //                 nextStatus;
  //             setState(() {});
  //
  //             // if (data.isExpanded) {
  //             //   setState(() {
  //             //     leadsViewController.leadList.value.results
  //             //         ?.elementAt(index)
  //             //         .isExpanded = false;
  //             //   });
  //             // } else {
  //             //   setState(() {
  //             //     leadsViewController.leadList.value.results
  //             //         ?.elementAt(index)
  //             //         .isExpanded = true;
  //             //   });
  //             // }
  //             //
  //             // setState(() {});
  //           },
  //           child: Container(
  //             height: windowHeight * 0.024,
  //             width: windowWidth,
  //             padding: EdgeInsets.only(left: windowWidth * 0.04),
  //             child: data.isExpanded == true
  //                 ? SvgPicture.asset(
  //               'assets/icons/leads_arrow_up.svg',
  //             )
  //                 : SvgPicture.asset(
  //               'assets/icons/leads_arrow_down.svg',
  //             ),
  //           ),
  //         ),
  //       ],
  //     ),
  //   )
  //       : Container();
  // }

  Widget leadListSummery() {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: EdgeInsets.all(
          windowWidth * 0.04,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 1,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const FormFieldTitle(title: 'Lead Creation Date'),
                  Text(
                    data!.clients![0].created!
                        .toString()
                        .substring(0, 10)
                        .split('-')
                        .reversed
                        .join('-'),
                    style: TextStyles.leadsTextStyle,
                  ),
                  SizedBox(
                    height: windowHeight * 0.01,
                  ),
                  const FormFieldTitle(title: 'Lead Full Name'),
                  Text(
                    data!.clients![0].name.toString(),
                    style: TextStyles.leadsTextStyle,
                  ),
                  SizedBox(
                    height: windowHeight * 0.01,
                  ),
                  const FormFieldTitle(title: 'Loan Amount:'),
                  Text(
                    "-",
                    style: TextStyles.leadsTextStyle,
                  ),
                  SizedBox(
                    height: windowHeight * 0.01,
                  ),
                  const FormFieldTitle(title: 'Bank:'),
                  Text(
                    "-",
                    style: TextStyles.leadsTextStyle,
                  ),
                  SizedBox(
                    height: windowHeight * 0.01,
                  ),
                  const FormFieldTitle(title: 'Banker:'),
                  Text(
                    "-",
                    style: TextStyles.leadsTextStyle,
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 1,
              child: Column(
                children: [
                  const FormFieldTitle(title: 'Co-Broke'),
                  Text(
                    "-",
                    style: TextStyles.leadsTextStyle,
                  ),
                  SizedBox(
                    height: windowHeight * 0.01,
                  ),
                  const FormFieldTitle(title: 'Payout Amount'),
                  Text(
                    "-",
                    style: TextStyles.leadsTextStyle,
                  ),
                  SizedBox(
                    height: windowHeight * 0.01,
                  ),
                  const FormFieldTitle(title: 'Payout Status:'),
                  Text(
                    "-",
                    style: TextStyles.leadsTextStyle,
                  ),
                  SizedBox(
                    height: windowHeight * 0.01,
                  ),
                  const FormFieldTitle(title: 'Law Firm:'),
                  Text(
                    "-",
                    style: TextStyles.leadsTextStyle,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget leadListSummeryCoBroker() {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: EdgeInsets.all(
          windowWidth * 0.04,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 1,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const FormFieldTitle(title: 'Lead ID'),
                  Container(
                    padding: const EdgeInsets.all(10),
                    margin: const EdgeInsets.symmetric(vertical: 5),
                    width: 150,
                    decoration: BoxDecoration(
                        color: Colors.grey.shade100,
                        // border: Border.all(),
                        borderRadius: BorderRadius.circular(5)),
                    child: Text(
                      data!.clients![0].lead!.toString(),
                      style: TextStyles.leadsTextStyle.copyWith(fontSize: 16),
                    ),
                  ),
                  SizedBox(
                    height: windowHeight * 0.01,
                  ),
                  const FormFieldTitle(title: 'Payout Amount'),
                  Container(
                    padding: const EdgeInsets.all(10),
                    margin: const EdgeInsets.symmetric(vertical: 5),
                    width: 150,
                    decoration: BoxDecoration(
                        color: Colors.grey.shade100,
                        // border: Border.all(),
                        borderRadius: BorderRadius.circular(5)),
                    child: Text(
                      "-",
                      style: TextStyles.leadsTextStyle.copyWith(fontSize: 16),
                    ),
                  ),
                  SizedBox(
                    height: windowHeight * 0.01,
                  ),
                  const FormFieldTitle(title: 'Bank'),
                  Container(
                    padding: const EdgeInsets.all(10),
                    margin: const EdgeInsets.symmetric(vertical: 5),
                    width: 150,
                    decoration: BoxDecoration(
                        color: Colors.grey.shade100,
                        // border: Border.all(),
                        borderRadius: BorderRadius.circular(5)),
                    child: Text(
                      "-",
                      style: TextStyles.leadsTextStyle.copyWith(fontSize: 16),
                    ),
                  ),
                  SizedBox(
                    height: windowHeight * 0.01,
                  ),
                  const FormFieldTitle(title: 'Co-Broker Name'),
                  Container(
                    padding: const EdgeInsets.all(10),
                    margin: const EdgeInsets.symmetric(vertical: 5),
                    width: 150,
                    decoration: BoxDecoration(
                        color: Colors.grey.shade100,
                        // border: Border.all(),
                        borderRadius: BorderRadius.circular(5)),
                    child: data!.coBrokeAgent != null
                        ? Text(data!.coBrokeAgent!.name ?? '-',
                            style: TextStyles.leadsTextStyle
                                .copyWith(fontSize: 16))
                        : Text("-",
                            style: TextStyles.leadsTextStyle
                                .copyWith(fontSize: 16)),
                  ),
                  SizedBox(
                    height: windowHeight * 0.01,
                  ),
                  const FormFieldTitle(title: 'Law Firm'),
                  Container(
                    padding: const EdgeInsets.all(10),
                    margin: const EdgeInsets.symmetric(vertical: 5),
                    width: 150,
                    decoration: BoxDecoration(
                        color: Colors.grey.shade100,
                        // border: Border.all(),
                        borderRadius: BorderRadius.circular(5)),
                    child: Text(
                      "-",
                      style: TextStyles.leadsTextStyle.copyWith(fontSize: 16),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 1,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const FormFieldTitle(title: 'Lead Creation Date'),
                  Container(
                    padding: const EdgeInsets.all(10),
                    margin: const EdgeInsets.symmetric(vertical: 5),
                    width: 150,
                    decoration: BoxDecoration(
                        color: Colors.grey.shade100,
                        // border: Border.all(),
                        borderRadius: BorderRadius.circular(5)),
                    child: Text(
                      data!.clients![0].created!
                          .toString()
                          .substring(0, 10)
                          .split('-')
                          .reversed
                          .join('-'),
                      style: TextStyles.leadsTextStyle.copyWith(fontSize: 16),
                    ),
                  ),
                  SizedBox(
                    height: windowHeight * 0.01,
                  ),
                  const FormFieldTitle(title: 'Payout Status'),
                  Container(
                    padding: const EdgeInsets.all(10),
                    margin: const EdgeInsets.symmetric(vertical: 5),
                    width: 150,
                    decoration: BoxDecoration(
                        color: Colors.grey.shade100,
                        // border: Border.all(),
                        borderRadius: BorderRadius.circular(5)),
                    child: Text(
                      "-",
                      style: TextStyles.leadsTextStyle.copyWith(fontSize: 16),
                    ),
                  ),
                  SizedBox(
                    height: windowHeight * 0.01,
                  ),
                  const FormFieldTitle(title: 'Banker Name'),
                  Container(
                    padding: const EdgeInsets.all(10),
                    margin: const EdgeInsets.symmetric(vertical: 5),
                    width: 150,
                    decoration: BoxDecoration(
                        color: Colors.grey.shade100,
                        // border: Border.all(),
                        borderRadius: BorderRadius.circular(5)),
                    child: Text(
                      "-",
                      style: TextStyles.leadsTextStyle.copyWith(fontSize: 16),
                    ),
                  ),
                  SizedBox(
                    height: windowHeight * 0.01,
                  ),
                  const FormFieldTitle(title: 'Co-Broker Mobile No.'),
                  Container(
                    padding: const EdgeInsets.all(10),
                    margin: const EdgeInsets.symmetric(vertical: 5),
                    width: 150,
                    decoration: BoxDecoration(
                        color: Colors.grey.shade100,
                        // border: Border.all(),
                        borderRadius: BorderRadius.circular(5)),
                    child: data!.coBrokeAgent != null
                        ? data!.coBrokeAgent!.phoneNumber != null
                            ? Text("+ ${data!.coBrokeAgent!.phoneNumber}",
                                style: TextStyles.leadsTextStyle
                                    .copyWith(fontSize: 16))
                            : Text("-",
                                style: TextStyles.leadsTextStyle
                                    .copyWith(fontSize: 16))
                        : Text("-",
                            style: TextStyles.leadsTextStyle
                                .copyWith(fontSize: 16)),
                  ),
                  SizedBox(
                    height: windowHeight * 0.01,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget expandableSummery(String title) {
    return Container(
      decoration: const BoxDecoration(
        boxShadow: [
          BoxShadow(
              offset: Offset(0, 12), //x,y
              blurRadius: 10,
              color: Colors.black12),
        ],
      ),
      child: Theme(
        data: ThemeData(
          dividerColor: Colors.white,
        ),
        child: ExpansionTile(
          tilePadding: EdgeInsets.only(
              left: windowWidth * 0.05, right: windowWidth * 0.05),
          title: Text(title,
              style: TextStyles.leadsTextStyle.copyWith(color: Colors.black)),
          backgroundColor: AppColors.white,
          collapsedBackgroundColor: AppColors.white,
          collapsedTextColor: Colors.white,
          iconColor: Colors.black,
          collapsedIconColor: Colors.black,
          children: [
            // isCoBroke(data!) ? leadListSummeryCoBroker() : leadListSummery()
            leadListSummeryCoBroker() //!Lead summary UI for all the lead type not only for co-broke
          ],
        ),
      ),
    );
  }

  getcanEdit(LeadItemModel data) {
    if (data.leadType != "co_broke" && data.coBrokeStatus != "Co-Broke Sent") {
      return true;
    } else if (data.leadType == 'yourself') {
      return true;
    } else {
      return data.canEdit;
    }
  }

  getCanEdit(LeadItemModel data) {
    if (data.leadType == "co_broke" && data.coBrokeStatus == "Co-Broke Sent") {
      return false;
    } else {
      return true;
    }
  }

  Widget expandableButton(String title,
      {required Function onTap, bool canEdit = true}) {
    return GestureDetector(
      onTap: () {
        if (canEdit) {
          onTap();
        }
      },
      child: Card(
        elevation: 3,
        child: Container(
          color: canEdit ? Colors.white : AppColors.greyD4D8D8,
          height: windowHeight * 0.05,
          padding: EdgeInsets.only(
              left: windowWidth * 0.04, right: windowWidth * 0.04),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                title,
                style: TextStyles.leadsTextStyle,
              ),
              const Spacer(),
              const Icon(
                Icons.keyboard_arrow_right,
                size: 25,
              )
            ],
          ),
        ),
      ),
    );
  }
}

// ignore: must_be_immutable
class FilterSection extends StatelessWidget {
  FilterSection(
      {Key? key, required this.windowHeight, required this.windowWidth})
      : super(key: key);
  final LeadsViewController leadsViewController = Get.find();
  double windowHeight;
  double windowWidth;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(0),
      color: Colors.white,
      child: Card(
        elevation: 8,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              topBar(),
              SizedBox(
                height: windowHeight * 0.04,
              ),
              body()
            ],
          ),
        ),
      ),
    );
  }

  Widget topBar() {
    return Row(
      children: [
        Expanded(
          flex: 5,
          child: InkWell(
            onTap: () {
              leadsViewController.resetFilter();
            },
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 8),
              decoration: BoxDecoration(
                  color: AppColors.kPrimaryColor.withOpacity(0.2),
                  borderRadius: BorderRadius.circular(10)),
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5.0),
                  child: Text(
                    'Reset',
                    style: TextStyles.filterDropdownButton,
                  ),
                ),
              ),
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: InkWell(
              onTap: () {
                leadsViewController.showFilter.value = false;
              },
              child: SvgPicture.asset(
                'assets/icons/closeEnclosed.svg',
                color: Colors.black,
              )),
        )
      ],
    );
  }

  Widget body() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: const [
            FormFieldTitle(title: 'Lead name'),
          ],
        ),
        SizedBox(
          height: windowHeight * 0.01,
        ),
        RequireTextField(
            type: Type.search,
            controller:
                leadsViewController.filterLeadNameTextEditingController),
        SizedBox(
          height: windowHeight * 0.02,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: const [
            FormFieldTitle(title: 'Bank'),
          ],
        ),
        DropDownMultiSelect(
          onChanged: (List<String> x) {
            // setState(() {
            //   selected =x;
            // });
            // debugPrint(x.toString());
          },
          options: leadsViewController.bankName,
          selectedValues: leadsViewController.slectedValues,
          whenEmpty: 'Select Banks',
          decoration: InputDecoration(
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: const BorderSide(color: Colors.grey)),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: const BorderSide(color: Colors.grey))),
        ),
        SizedBox(
          height: windowHeight * 0.01,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: const [
            FormFieldTitle(title: 'Co-Broke'),
          ],
        ),
        SizedBox(
          height: windowHeight * 0.01,
        ),
        Wrap(
            alignment: WrapAlignment.start,
            children: List<Widget>.generate(
              3,
              (int index) {
                int selectedIndex = -1;
                return Obx(
                  () => Padding(
                    padding: const EdgeInsets.only(right: 5.0),
                    child: ChoiceChip(
                      side: const BorderSide(color: AppColors.kPrimaryColor),
                      selectedColor: AppColors.kPrimaryColor,
                      disabledColor: Colors.white,
                      backgroundColor: Colors.white,
                      elevation: 0,
                      labelStyle: TextStyle(
                          color: leadsViewController.coBrokeVlue!.value == index
                              ? AppColors.kPrimaryColor
                              : Colors.black),
                      label: Text(leadsViewController.coBrokeOptionList[index]),
                      selected: leadsViewController.coBrokeVlue!.value ==
                          selectedIndex,
                      onSelected: (bool selected) {
                        leadsViewController.coBrokeVlue!.value =
                            (selected ? index : null)!;
                        // setState(() {
                        //   _value = selected ? index : null;
                        // });
                      },
                    ),
                  ),
                );
              },
            ).toList()),
        SizedBox(
          height: windowHeight * 0.02,
        ),
        Center(
          child: PrimaryButton(
              windowHeight: windowHeight,
              windowWidth: windowWidth * 0.85,
              buttonTitle: 'Apply',
              onPressed: () {
                debugPrint("filter Apply:=======");
                leadsViewController.getFilteredData(
                    leadsViewController
                        .filterLeadNameTextEditingController.text,
                    '');
              }),
        ),
        SizedBox(
          height: windowHeight * 0.03,
        ),
        // Wrap(alignment: WrapAlignment.start, children: [
        //   Padding(
        //     padding: const EdgeInsets.only(right: 5.0),
        //     child: Obx(
        //       () => FilterChip(
        //         side: const BorderSide(color: AppColors.kPrimaryColor),
        //         label: const Text('Pending'),
        //         selectedColor: AppColors.kPrimaryColor,
        //         disabledColor: Colors.white,
        //         backgroundColor: Colors.white,
        //         showCheckmark: false,
        //         elevation: 0,
        // labelStyle: TextStyle(
        //     color: leadsViewController.coBrokePendingCheck.value
        //         ? Colors.white
        //         : Colors.black),
        //         selected: leadsViewController.coBrokePendingCheck.value,
        //         onSelected: (bool value) {
        //           leadsViewController.coBrokePendingCheck.value = value;
        //         },
        //       ),
        //     ),
        //   ),
        //   Obx(
        //     () => FilterChip(
        //       side: const BorderSide(color: AppColors.kPrimaryColor),
        //       label: const Text('Submitted'),
        //       selectedColor: AppColors.kPrimaryColor,
        //       disabledColor: Colors.white,
        //       backgroundColor: Colors.white,
        //       elevation: 0,
        //       labelStyle: TextStyle(
        //           color: leadsViewController.coBrokeSubmittedCheck.value
        //               ? Colors.white
        //               : Colors.black),
        //       selected: leadsViewController.coBrokeSubmittedCheck.value,
        //       showCheckmark: false,
        //       onSelected: (bool value) {
        //         leadsViewController.coBrokeSubmittedCheck.value = value;
        //       },
        //     ),
        //   ),
        //   SizedBox(
        //     height: windowHeight * 0.07,
        //   ),
        // Center(
        //   child: PrimaryButton(
        //       windowHeight: windowHeight,
        //       windowWidth: windowWidth * 0.85,
        //       buttonTitle: 'Apply',
        //       onPressed: () {}),
        // )
        // ]),
      ],
    );
  }

  // Function to check if the PDPA has been completed for the main and the joint applicants
}
