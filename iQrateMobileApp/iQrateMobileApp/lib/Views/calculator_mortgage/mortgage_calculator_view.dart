import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/calculator_mortgage_repayment_controller.dart';
import 'package:iqrate/DeviceManager/screen_constants.dart';
import 'package:iqrate/Service/GetControllers.dart';
import 'package:iqrate/Utils/text_field_custom.dart';
import 'package:iqrate/Views/calculator/your_preferred_interest_rate.dart';
import 'package:iqrate/Views/calculator_mortgage/mortgage_repayment_report.dart';
import 'package:iqrate/Views/rates/currency_amount_textField.dart';
import 'package:iqrate/Widgets/default_appbar.dart';
import 'package:iqrate/Widgets/form_field_title.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';

import '../../DeviceManager/colors.dart';
import '../../DeviceManager/container_styles.dart';
import '../../DeviceManager/text_styles.dart';
import '../../Widgets/secondary_button.dart';

class MortgageRepaymentView extends StatefulWidget {
  const MortgageRepaymentView({Key? key}) : super(key: key);

  @override
  State<MortgageRepaymentView> createState() => _MortgageRepaymentViewState();
}

class _MortgageRepaymentViewState extends State<MortgageRepaymentView> {
  CalculatorMortgageRepaymentController controller =
      GetControllers.shared.getCalculatorMortgageRepaymentController();
  int activeStep = 0; // Initial step set to 0.
  var formKey = GlobalKey<FormState>();
  @override
  void initState() {
    super.initState();
    //add item 1 always
    var item1 = YourPreferredInterestRateWidget(
      index: 0,
      removeMe: (index) {},
      showRemoveIcon: false,
    );
    controller.arrayYourPreferredInterestRateWidget.add(item1);
  }

  addYourPreferredInterestRateWidget() {
    var newIndex = controller.arrayYourPreferredInterestRateWidget.length;
    var item = YourPreferredInterestRateWidget(
      index: newIndex,
      removeMe: (index) {
        debugPrint("remove at $index");
        controller.arrayYourPreferredInterestRateWidget.removeAt(index);
        setState(() {});
      },
    );
    controller.arrayYourPreferredInterestRateWidget.add(item);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: DefaultAppBar(
          title: "Mortgage Repayment", windowHeight: windowHeight * 0.09),
      body: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 20,
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: Row(
                  children: [
                    Container(
                      height: 30,
                      width: 30,
                      child: Center(
                        child: Text(
                          '1',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: FontSize.s15,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                      decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          color: AppColors.kPrimaryColor),
                    ),
                    Expanded(
                      child: Container(
                        height: 1,
                        color: Colors.grey,
                      ),
                    ),
                    Container(
                      height: 30,
                      width: 30,
                      child: Center(
                        child: Text(
                          '2',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: FontSize.s15,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                      decoration: const BoxDecoration(
                          shape: BoxShape.circle, color: AppColors.greyA8A8A8),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Mortgage Repayment',
                        style: TextStyles.calculatorHeadingsTextStyle),
                    const SizedBox(
                      height: 20,
                    ),
                    const FormFieldTitle(
                      title: 'Name*',
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    RequireTextField(
                        labelText: "Name",
                        type: Type.fullname,
                        controller: controller.nameController),
                    SizedBox(
                      height: windowHeight * 0.01,
                    ),
                    const FormFieldTitle(title: 'Loan Amount*'),
                    const SizedBox(
                      height: 10,
                    ),
                    CurrencyAmountTextField(
                      textEditingController: controller.loanAmountController,
                      labelText: 'Loan Amount',
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    const FormFieldTitle(title: 'Loan Tenure*'),
                    const SizedBox(
                      height: 10,
                    ),
                    RequireTextFieldV2(
                        labelText: "Loan Tenure",
                        hintText:"Loan Tenure",
                        type: TypeRequireTextFieldV2.numberInt,
                        validator: GetControllers.shared.validateForNumberIntMaxis30,
                        controller: controller.loanTenureController, formKey: formKey,),
                    const SizedBox(
                      height: 10,
                    ),
                    buildListYourPreferredInterestRateWidget(),
                    const SizedBox(
                      height: 20,
                    ),

                    controller.arrayYourPreferredInterestRateWidget.length < 2
                        ? PrimaryButton(
                            windowHeight: windowHeight,
                            windowWidth: windowWidth,
                            buttonTitle:
                                "Add Preferred Interest Rate ${controller.arrayYourPreferredInterestRateWidget.length + 1}",
                            onPressed: () {
                              debugPrint("remove Add Preferred Interest Rate");
                              addYourPreferredInterestRateWidget();
                            },
                          )
                        : const SizedBox(),
                    const SizedBox(
                      height: 20,
                    ),
                    controller.arrayYourPreferredInterestRateWidget.length == 1
                        ? SecondaryButton(
                            windowHeight: windowHeight,
                            windowWidth: windowWidth,
                            buttonTitle: "Continue",
                            onPressed: () async {
                              debugPrint("Continue clicked ");
                              hideKeyboard();
                              controller.callAPICalculatorMortgageRepayment(
                                  context, (data) {
                                debugPrint(
                                    "done callAPICalculatorMortgageRepayment");
                                GetControllers.shared
                                    .getMortgageRepaymentReportController()
                                    .loadDetailsDataResult = data;
                                Get.to(() => const MortgageRepaymentReport());
                              });
                              // controller.callAPICalculatorMortgageRepayment((data) {
                              //   GetControllers.shared.getMortgageRepaymentReportController().loadDetailsDataResult = data;
                              //   Get.to(() => const MortgageRepaymentReport());
                              // });
                            },
                            kGradientBoxDecoration: ContainerStyles
                                .kGradientBoxDecorationSecondaryButton,
                            kInnerDecoration:
                                ContainerStyles.kInnerDecorationSecondaryButton,
                          )
                        : const SizedBox(),
                    const SizedBox(
                      height: 10,
                    ),
                    controller.arrayYourPreferredInterestRateWidget.length == 2
                        ? PrimaryButton(
                            windowHeight: windowHeight,
                            windowWidth: windowWidth,
                            buttonTitle: "Generate Report",
                            onPressed: () {
                              debugPrint("Generate Report clicked ");
                              hideKeyboard();
                              controller.callAPICalculatorMortgageRepayment(
                                  context, (data) {
                                GetControllers.shared
                                    .getMortgageRepaymentReportController()
                                    .loadDetailsDataResult = data;
                                Get.to(() => const MortgageRepaymentReport());
                              });
                            },
                          )
                        : const SizedBox(),
                    // SecondaryButton(
                    //   windowHeight: windowHeight,
                    //   windowWidth: windowWidth,
                    //   buttonTitle: "Back",
                    //   onPressed: () {},
                    //   kGradientBoxDecoration:
                    //       ContainerStyles.kGradientBoxDecorationSecondaryButton,
                    //   kInnerDecoration:
                    //       ContainerStyles.kInnerDecorationSecondaryButton,
                    // ),
                    const SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  hideKeyboard() {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }

  Widget buildListYourPreferredInterestRateWidget() => Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          for (var i = 0;
              i < controller.arrayYourPreferredInterestRateWidget.length;
              i++)
            controller.arrayYourPreferredInterestRateWidget[i]
        ],
      );
}
