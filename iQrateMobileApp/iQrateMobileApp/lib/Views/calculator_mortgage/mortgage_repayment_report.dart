// ignore_for_file: deprecated_member_use

import 'dart:io';
import 'dart:isolate';
import 'dart:math';
import 'dart:ui';
import 'package:android_path_provider/android_path_provider.dart';
import 'package:dio/dio.dart';
import 'package:file_utils/file_utils.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/mortgage_repayment_report_controller.dart';
import 'package:iqrate/DeviceManager/screen_constants.dart';
import 'package:iqrate/Service/GetControllers.dart';
import 'package:iqrate/Utils/text_formatter.dart';
import 'package:iqrate/Views/calculator_mortgage/mortgage_repayment_compare_screen.dart';
import 'package:iqrate/Views/calculator_mortgage/mortgage_repayment_table_screen.dart';
import 'package:iqrate/Widgets/default_appbar.dart';
import 'package:open_filex/open_filex.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../DeviceManager/colors.dart';
import '../../DeviceManager/text_styles.dart';
import '../../Widgets/primary_button.dart';

class MortgageRepaymentReport extends StatefulWidget {
  const MortgageRepaymentReport({Key? key}) : super(key: key);

  @override
  State<MortgageRepaymentReport> createState() =>
      _MortgageRepaymentReportState();
}

class _MortgageRepaymentReportState extends State<MortgageRepaymentReport> {
  MortgageRepaymentReportController controller =
      GetControllers.shared.getMortgageRepaymentReportController();
  int activeStep = 0; // Initial step set to 0.
  late double windowHeight;
  late double windowWidth;

  //for report

  @override
  void initState() {
    super.initState();
    controller.getInterestRateComparisonModels();
    prepareSaveDir();
  }

  @override
  Widget build(BuildContext context) {
    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: DefaultAppBar(
          title: "Mortgage Repayment", windowHeight: windowHeight * 0.09),
      body: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: SingleChildScrollView(
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            const SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: Row(
                children: [
                  Container(
                    height: 30,
                    width: 30,
                    child: Center(
                      child: Text(
                        '1',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: FontSize.s15,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                    decoration: const BoxDecoration(
                        shape: BoxShape.circle, color: AppColors.kPrimaryColor),
                  ),
                  Expanded(
                    child: Container(
                      height: 1,
                      color: Colors.grey,
                    ),
                  ),
                  Container(
                    height: 30,
                    width: 30,
                    child: Center(
                      child: Text(
                        '2',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: FontSize.s15,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                    decoration: const BoxDecoration(
                        shape: BoxShape.circle, color: AppColors.kPrimaryColor),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Colors.black12,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 20, top: 20, bottom: 20, right: 20),
                          child: Text(
                            "My Mortgage Repayment Report ",
                            style: TextStyles.leadsColorTextStyle2,
                          ),
                        ),
                        expandableSummery("Loan Details"),
                        menuMortgageRepaymentReportGo(
                            title: "Interest Rate Comparison",
                            onTap: () {
                              debugPrint(" click Interest Rate Comparison");
                              debugPrint(
                                  "reporttttt done callAPICalculatorBucMortgageReport with Comparison ${controller.loadDetailsDataResult["Rate Package Comparison"]}");
                              Get.to(
                                  () => const MortgageRepaymentCompareScreen());
                            }),
                        menuMortgageRepaymentReportGo(
                            title: "Mortgage Repayment Table",
                            subString: "(Based on Preferred Rate 1)",
                            onTap: () {
                              var a =
                                  "Mortgage Repayment Table\n(Based on Preferred Rate 1)";
                              controller.viewTable1 = true;
                              Get.to(() => MortgageRepaymentTableScreen(
                                    titleString: a,
                                  ));
                              debugPrint(" click (Based on Preferred Rate 1)");
                            }),
                        if (controller.modelCompare2 != null)
                          menuMortgageRepaymentReportGo(
                              title: "Mortgage Repayment Table",
                              subString: "(Based on Preferred Rate 2)",
                              onTap: () {
                                var a =
                                    "Mortgage Repayment Table\n(Based on Preferred Rate 2)";
                                controller.viewTable1 = false;
                                Get.to(() => MortgageRepaymentTableScreen(
                                      titleString: a,
                                    ));
                                debugPrint(
                                    " click (Based on Preferred Rate 2)");
                              })
                      ],
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Center(
                child: Obx(
                  () => PrimaryButton(
                    windowHeight: windowHeight,
                    windowWidth: windowWidth,
                    buttonTitle: "Download Report $progress",
                    onPressed: () {
                      debugPrint("wait backend return pdf export link");
                      //mortgage-repayment-pdf
                      GetControllers.shared
                          .getCalculatorMortgageRepaymentController()
                          .callAPIReportCalculatorMortgageRepayment(
                              (data) async {
                        if (kDebugMode) {
                          print(data);
                        }
                        if (data["url"] != null) {
                          var url = data["url"];
                          if (Platform.isAndroid) {
                            getPermission();
                            await downloadFile(url, "mortgate_report.pdf");
                            //await downloadFileV2(url);
                            // Android-specific code
                          } else if (Platform.isIOS) {
                            // iOS-specific code
                            launch(url, forceWebView: true);
                          }
                        }
                      });
                      //newPurchaseController.onTapBackStep3Lead();
                    },
                  ),
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }

  Future getPermission() async {
    var status = await Permission.storage.status;
    if (status.isDenied) {
      debugPrint("=======storage denied=======");
      await Permission.storage.request();
      // We didn't ask for permission yet or the permission has been denied before but not permanently.
    }
  }

  bool downloading = false;
  var progress = "".obs;
  var path = "No Data";
  var platformVersion = "Unknown";
  static final Random random = Random();
  late Directory externalDir;

  Future<void> downloadFileV2(String url) async {
    debugPrint('downloadFileV2');
    Dio dio = Dio();
    progress.value = "";
    if (await Permission.storage.request().isGranted) {
      // Either the permission was already granted before or the user just granted it.
      String dirloc = "";
      if (Platform.isAndroid) {
        dirloc = "/sdcard/download/";
      } else {
        dirloc = (await getApplicationDocumentsDirectory()).path;
      }
      var randid = random.nextInt(10000);
      path = dirloc + randid.toString() + ".pdf";
      try {
        FileUtils.mkdir([dirloc]);
        await dio.download(url, path,
            onReceiveProgress: (receivedBytes, totalBytes) {
          setState(() {
            downloading = true;
            progress.value =
                ((receivedBytes / totalBytes) * 100).toStringAsFixed(0) + "%";
          });
        });
      } catch (e) {
        if (kDebugMode) {
          print(e);
        }
      }
      setState(() {
        downloading = false;
        progress.value = "";
        if (kDebugMode) {
          print("path $path");
        }
        OpenFilex.open(path);
        // path = dirloc + randid.toString() + ".pdf";
      });
    } else {
      setState(() {
        progress.value = "Permission Denied!";
      });
    }
  }

  // ignore: body_might_complete_normally_nullable
  late String _localPath;
  Future<File?> downloadFile(String url, String name) async {
    Future.delayed(const Duration(milliseconds: 0), () {
      FocusManager.instance.primaryFocus!.unfocus();
      Fluttertoast.showToast(
        timeInSecForIosWeb: 3,
        msg: "Downloading",
        backgroundColor: Colors.green,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        textColor: Colors.white,
        fontSize: 20.0,
      );
    });
    final taskId = await FlutterDownloader.enqueue(
        url: url,
        savedDir: _localPath,
        showNotification: true,
        // show download progress in status bar (for Android)
        openFileFromNotification: true,
        // click on notification to open downloaded file (for Android)
        saveInPublicStorage: false);
    Get.snackbar("Downloaded", "Download completed",
        colorText: Colors.white,
        backgroundColor: Colors.green,
        mainButton: TextButton(
            onPressed: () async {
              debugPrint(">>>>>>>> taskId $taskId");
              await FlutterDownloader.open(taskId: taskId!);
            },
            child: const Text("Open", style: TextStyle(color: Colors.white)),),
        duration: const Duration(seconds: 7));
    return null;
  }

  Future<void> prepareSaveDir() async {
    _localPath = (await _findLocalPath())!;
    final savedDir = Directory(_localPath);
    bool hasExisted = await savedDir.exists();
    if (!hasExisted) {
      savedDir.create();
    }
  }

  Future<String?> _findLocalPath() async {
    String? externalStorageDirPath;
    if (Platform.isAndroid) {
      try {
        externalStorageDirPath = await AndroidPathProvider.downloadsPath;
      } catch (e) {
        final directory = await getExternalStorageDirectory();
        externalStorageDirPath = directory?.path;
      }
    } else if (Platform.isIOS) {
      externalStorageDirPath =
          (await getApplicationDocumentsDirectory()).absolute.path;
    }
    return externalStorageDirPath;
  }

  void downloadCallback(String id, DownloadTaskStatus status, int progress) {
    if (kDebugMode) {
      print(
          'Background Isolate Callback: task ($id) is in status ($status) and process ($progress)');
    }

    final SendPort send =
        IsolateNameServer.lookupPortByName('downloader_send_port')!;
    send.send([id, status, progress]);
  }

  // void launchUrl(String url) async {
  //   // html.window.open(url, 'PlaceholderName');
  //   final file = await http.get(Uri.parse(url));
  //   final content = file.bodyBytes;
  //   //
  //   // if (await canLaunch(url)) {
  //   //   await launch(url);
  //   // } else {
  //   //   Get.snackbar(StringUtils.error, 'Invalid download link',
  //   //       backgroundColor: Colors.red, colorText: Colors.white);
  //   // }
  // }

  Widget expandableSummery(String title) {
    return Padding(
      padding: const EdgeInsets.all(1.0),
      child: Container(
        color: AppColors.white,
        child: ExpansionTile(
          leading: Checkbox(
              value: controller.isCheckLoanDetails,
              onChanged: (bool? value) {
                setState(() {
                  controller.isCheckLoanDetails = value ?? false;
                });
              },
              checkColor: AppColors.red,
              activeColor: AppColors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(2.0),
              ),
              side: MaterialStateBorderSide.resolveWith(
                (states) => const BorderSide(width: 1.0, color: Colors.red),
              )),
          childrenPadding: EdgeInsets.zero,
          title: Text(title, style: TextStyles.leadsTextStyle),
          collapsedIconColor: Colors.black,
          children: [
            const Divider(height: 1, thickness: 1, color: Colors.black12),
            const SizedBox(
              height: 20,
            ),
            loanDetailsItem(
                title: "Prepared For",
                value: controller.loadDetailsDataResult["Loan Details"]
                    ["Prepared For"]),
            loanDetailsItem(
                title: "Loan Amount",
                value: controller.loadDetailsDataResult["Loan Details"]
                        ["Loan Amount"]
                    .toString(),
                isCurrency: true),
            loanDetailsItem(
                title: "Loan Tenure",
                value: controller.loadDetailsDataResult["Loan Details"]
                    ["Loan Tenure"],
                isYear: true),
            // Obx(() => buildListContentLoanDetails(controller.arrayLoanDetail)),
            const SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }

  Widget loanDetailsItem(
      {String title = "",
      String value = "",
      bool isYear = false,
      bool isCurrency = false}) {
    var finalValue = value;
    if (isCurrency) {
      var a = double.parse(value);
      finalValue = convertToCurrency(a);
    }
    if (isYear) {
      finalValue = "$value Years";
    }

    return Padding(
      padding: const EdgeInsets.only(left: 80, right: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            //mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 5),
                    child:
                        Text(title, style: TextStyles.formFieldTitleTextStyle1),
                  )),
              Expanded(
                flex: 1,
                child: Text(
                  finalValue,
                  style: TextStyles.leadsTextStyle1,
                  textAlign: TextAlign.left,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
  //
  // Widget interestRateComparison(String title) {
  //   return Card(
  //     elevation: 3,
  //     child: ExpansionTile(
  //       trailing: const Icon(
  //         Icons.chevron_right,
  //         color: AppColors.grey625F5A,
  //       ),
  //       leading: SizedBox(
  //           width: 10,
  //           child: Obx(() => Checkbox(
  //                 value: true,
  //                 onChanged: (bool? value) {
  //                   // controller.isInterestRateCheckBoxEnable.value = value!;
  //                   // debugPrint(
  //                   //     " check Inter Rate ${controller.isInterestRateCheckBoxEnable.value}");
  //                 },
  //                 checkColor: AppColors.red,
  //                 activeColor: Colors.red.withOpacity(0),
  //                 shape: RoundedRectangleBorder(
  //                   borderRadius: BorderRadius.circular(2.0),
  //                 ),
  //                 side: MaterialStateBorderSide.resolveWith(
  //                   (states) => const BorderSide(width: 1.0, color: Colors.red),
  //                 ),
  //               ))),
  //       childrenPadding: EdgeInsets.zero,
  //       title: Text(title, style: TextStyles.leadsTextStyle),
  //       collapsedIconColor: Colors.black,
  //     ),
  //   );
  // }

  getValueCheckBox(String sub) {
    switch (sub) {
      case '':
        {
          return controller.isInterestRateComparison;
        }
      case '(Based on Preferred Rate 1)':
        {
          return controller.isMortgageRepayment1;
        }
      case '(Based on Preferred Rate 2)':
        {
          return controller.isMortgageRepayment2;
        }
      default:
        {
          return controller.isMortgageRepayment2;
        }
    }
  }

  setValueCheckBox(String sub, bool value) {
    switch (sub) {
      case '':
        {
          // return isInterestRateComparison;
          setState(() {
            controller.isInterestRateComparison = value;
          });
        }
        break;
      case '(Based on Preferred Rate 1)':
        {
          // return isMortgageRepayment1;
          setState(() {
            controller.isMortgageRepayment1 = value;
          });
        }
        break;
      case '(Based on Preferred Rate 2)':
        {
          // return isMortgageRepayment2;
          setState(() {
            controller.isMortgageRepayment2 = value;
          });
        }
        break;
      default:
        {}
        break;
    }
  }

  //menuMortgageRepaymentReportGo
  Widget menuMortgageRepaymentReportGo(
      {String title = "", String subString = "", required Function() onTap}) {
    return Padding(
      padding: const EdgeInsets.only(top: 0, left: 1, right: 1, bottom: 1),
      child: Container(
        color: AppColors.white,
        child: ExpansionTile(
          trailing: InkWell(
            onTap: () {
              onTap();
            },
            child: const Icon(
              Icons.chevron_right,
              color: AppColors.grey625F5A,
            ),
          ),
          leading: Checkbox(
              // value: true,
              value: getValueCheckBox(subString),
              onChanged: (bool? value) {
                setValueCheckBox(subString, value ?? false);
              },
              checkColor: AppColors.red,
              activeColor: AppColors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(2.0),
              ),
              side: MaterialStateBorderSide.resolveWith(
                (states) => const BorderSide(width: 1.0, color: Colors.red),
              )),
          childrenPadding: EdgeInsets.zero,
          title: InkWell(
            onTap: () {
              onTap();
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(title, style: TextStyles.leadsTextStyle),
                if (subString != "")
                  Text(subString, style: TextStyles.leadsTextStyle1)
              ],
            ),
          ),
          collapsedIconColor: Colors.black,
        ),
      ),
    );
  }
}
//
// class WebViewExample extends StatefulWidget {
//   @override
//   WebViewExampleState createState() => WebViewExampleState();
// }
//
// class WebViewExampleState extends State<WebViewExample> {
//   @override
//   void initState() {
//     super.initState();
//     // Enable virtual display.
//     if (Platform.isAndroid) WebView.platform = AndroidWebView();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return WebView(
//       initialUrl: 'https://iqrate-images.s3.ap-southeast-1.amazonaws.com/calculator-pdf/mortgage-report1656575711.pdf',
//     );
//   }
// }