import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/mortgage_repayment_report_controller.dart';
import 'package:iqrate/Service/GetControllers.dart';
import 'package:iqrate/Utils/text_formatter.dart';
import 'package:iqrate/Model/response_model.dart/MortgageRepaymentTableModel.dart';
import 'package:iqrate/Widgets/default_appbar.dart';
import '../../DeviceManager/colors.dart';
import '../../DeviceManager/text_styles.dart';
import '../../Widgets/primary_button.dart';

class MortgageRepaymentTableScreen extends StatefulWidget {
  final String titleString;
  const MortgageRepaymentTableScreen({Key? key, this.titleString = ""})
      : super(key: key);

  @override
  State<MortgageRepaymentTableScreen> createState() =>
      _MortgageRepaymentTableScreenState();
}

class _MortgageRepaymentTableScreenState
    extends State<MortgageRepaymentTableScreen> {
  MortgageRepaymentReportController controller =
      GetControllers.shared.getMortgageRepaymentReportController();
  int activeStep = 0; // Initial step set to 0.
  late double windowHeight;
  late double windowWidth;
  // GlobalKey<FormState> exp1 = GlobalKey<FormState>();
  // GlobalKey<FormState> exp2 = GlobalKey<FormState>();
  // GlobalKey<FormState> exp3 = GlobalKey<FormState>();
  // GlobalKey<FormState> exp4 = GlobalKey<FormState>();
  // GlobalKey<FormState> exp5 = GlobalKey<FormState>();
  // GlobalKey<FormState> exp6 = GlobalKey<FormState>();
  // GlobalKey<FormState> exp7 = GlobalKey<FormState>();
  //
  // var isExpandTable1 = false;
  // var isExpandTable2 = false.obs;
  var selected = 0;
  @override
  void initState() {
    super.initState();
    controller.clearDataTable();
    controller.getDataTableModels();
  }

  @override
  Widget build(BuildContext context) {
    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.width;
    if (kDebugMode) {
      print("rebuild ");
    }
    return Scaffold(
      appBar: DefaultAppBar(
          title: "Mortgage Repayment", windowHeight: windowHeight * 0.09),
      body: InteractiveViewer(
        minScale: 1,
        maxScale: 4,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(
                  left: 20, top: 20, bottom: 20, right: 20),
              child: Text(
                widget.titleString,
                // "Mortgage Repayment Table\n(Based on Preferred Rate 1)",
                style: TextStyles.leadsColorTextStyle2
                    .copyWith(color: AppColors.kPrimaryColor),
              ),
            ),
            titleTable(),

            Expanded(
              child: SingleChildScrollView(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // ZoomOverlay(
                      //   minScale: 1, // Optional
                      //   maxScale: 3.0, // Optional
                      //   twoTouchOnly:
                      //       false, // Defaults to false : 1 ngon, 2 true fox 2 ngon
                      //   child: expandTable1("Year 1-5"),
                      // ),
                      // InteractiveViewer(
                      //   // minScale: 1, // Optional
                      //   // maxScale: 3.0, // Optional
                      //   constrained: false,
                      //   child: Text("Year 1-5"),
                      // ),
                      // InteractiveViewer(
                      //   // minScale: 1, // Optional
                      //   // maxScale: 3.0, // Optional
                      //   constrained: false,
                      //   child: expandTable1("Year 1-5"),
                      // ),
                      expandTable1("Year 1-5"),
                      if (controller.dataSourceTable2.isNotEmpty)
                        expandTable2("Year 6-10"),
                      if (controller.dataSourceTable3.isNotEmpty)
                        expandTable3("Year 11-15"),
                      if (controller.dataSourceTable4.isNotEmpty)
                        expandTable4("Year 16-20"),
                      if (controller.dataSourceTable5.isNotEmpty)
                        expandTable5("Year 21-25"),
                      if (controller.dataSourceTable6.isNotEmpty)
                        expandTable6("Year 26-30"),
                      if (controller.dataSourceTable7.isNotEmpty)
                        expandTable7("Year 31-35"),
                      const SizedBox(
                        height: 30,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Center(
                          child: PrimaryButton(
                            windowHeight: windowHeight,
                            windowWidth: windowWidth,
                            buttonTitle: "Close",
                            onPressed: () {
                              Get.back();
                              //newPurchaseController.onTapBackStep3Lead();
                            },
                          ),
                        ),
                      ),
                    ]),
              ),
            ),
            //
            // Expanded(
            //   child: ListView(
            //     shrinkWrap: true,
            //     children: [
            //       Column(
            //           crossAxisAlignment: CrossAxisAlignment.start,
            //           children: [
            //             // ZoomOverlay(
            //             //   minScale: 1, // Optional
            //             //   maxScale: 3.0, // Optional
            //             //   twoTouchOnly:
            //             //       false, // Defaults to false : 1 ngon, 2 true fox 2 ngon
            //             //   child: expandTable1("Year 1-5"),
            //             // ),
            //             // InteractiveViewer(
            //             //   // minScale: 1, // Optional
            //             //   // maxScale: 3.0, // Optional
            //             //   constrained: false,
            //             //   child: Text("Year 1-5"),
            //             // ),
            //             // InteractiveViewer(
            //             //   // minScale: 1, // Optional
            //             //   // maxScale: 3.0, // Optional
            //             //   constrained: false,
            //             //   child: expandTable1("Year 1-5"),
            //             // ),
            //             expandTable1("Year 1-5"),
            //             if (controller.dataSourceTable2.isNotEmpty)
            //               expandTable2("Year 6-10"),
            //             if (controller.dataSourceTable3.isNotEmpty)
            //               expandTable3("Year 11-15"),
            //             if (controller.dataSourceTable4.isNotEmpty)
            //               expandTable4("Year 16-20"),
            //             if (controller.dataSourceTable5.isNotEmpty)
            //               expandTable5("Year 21-25"),
            //             if (controller.dataSourceTable6.isNotEmpty)
            //               expandTable6("Year 26-30"),
            //             if (controller.dataSourceTable7.isNotEmpty)
            //               expandTable7("Year 31-35"),
            //             const SizedBox(
            //               height: 30,
            //             ),
            //             Padding(
            //               padding: const EdgeInsets.symmetric(horizontal: 20),
            //               child: Center(
            //                 child: PrimaryButton(
            //                   windowHeight: windowHeight,
            //                   windowWidth: windowWidth,
            //                   buttonTitle: "Close",
            //                   onPressed: () {
            //                     Get.back();
            //                     //newPurchaseController.onTapBackStep3Lead();
            //                   },
            //                 ),
            //               ),
            //             ),
            //           ]),
            //     ],
            //   ),
            // )

            // SingleChildScrollView(
            //   child:
            //       Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            //     // Padding(
            //     //   padding: const EdgeInsets.only(
            //     //       left: 20, top: 20, bottom: 20, right: 20),
            //     //   child: Text(
            //     //     widget.titleString,
            //     //     // "Mortgage Repayment Table\n(Based on Preferred Rate 1)",
            //     //     style: TextStyles.leadsColorTextStyle2
            //     //         .copyWith(color: AppColors.kPrimaryColor),
            //     //   ),
            //     // ),
            //     // titleTable(),
            //     expandTable1("Year 1-5"),
            //     if (controller.dataSourceTable2.isNotEmpty)
            //       expandTable2("Year 6-10"),
            //     if (controller.dataSourceTable3.isNotEmpty)
            //       expandTable3("Year 11-15"),
            //     if (controller.dataSourceTable4.isNotEmpty)
            //       expandTable4("Year 16-20"),
            //     if (controller.dataSourceTable5.isNotEmpty)
            //       expandTable5("Year 21-25"),
            //     if (controller.dataSourceTable6.isNotEmpty)
            //       expandTable6("Year 26-30"),
            //     const SizedBox(
            //       height: 30,
            //     ),
            //     Padding(
            //       padding: const EdgeInsets.symmetric(horizontal: 20),
            //       child: Center(
            //         child: PrimaryButton(
            //           windowHeight: windowHeight,
            //           windowWidth: windowWidth,
            //           buttonTitle: "Close",
            //           onPressed: () {
            //             Get.back();
            //             //newPurchaseController.onTapBackStep3Lead();
            //           },
            //         ),
            //       ),
            //     ),
            //   ]),
            // ),
          ],
        ),
      ),
    );
  }

  Widget expandTable1(String title) {
    // if (kDebugMode) {
    //   print('build expandTable1');
    // }
    return Container(
      color: AppColors.white,
      child: ExpansionTile(
        maintainState: true,
        key: GlobalKey(),
        initiallyExpanded: 1 == selected,
        title: Text(title,
            style: TextStyles.leadsTextStyleBold.copyWith(fontSize: 16)),
        collapsedIconColor: Colors.black,
        children: [
          const Divider(height: 1, thickness: 1, color: Colors.black12),
          getBodyYear1Row(),
        ],
        onExpansionChanged: (isOpen) {
          // setState(() {
          //   selected = 1;
          // });
          if (isOpen) {
            setState(() {
              selected = 1;
            });
          } else {
            setState(() {
              selected = 0;
            });
          }
        },
      ),
    );
  }

  Widget expandTable2(String title) {
    if (kDebugMode) {
      print('build expandTable2');
    }
    return Padding(
      padding: const EdgeInsets.all(1.0),
      child: Container(
        color: AppColors.white,
        child: ExpansionTile(
          key: GlobalKey(),
          initiallyExpanded: 2 == selected,
          title: Text(title,
              style: TextStyles.leadsTextStyleBold.copyWith(fontSize: 16)),
          collapsedIconColor: Colors.black,
          children: [
            const Divider(height: 1, thickness: 1, color: Colors.black12),
            getBodyYear2Row(),
          ],
          onExpansionChanged: (isOpen) {
            if (isOpen) {
              setState(() {
                selected = 2;
              });
            } else {
              setState(() {
                selected = 0;
              });
            }
          },
        ),
      ),
    );
  }

  Widget expandTable3(String title) {
    return Padding(
      padding: const EdgeInsets.all(1.0),
      child: Container(
        color: AppColors.white,
        child: ExpansionTile(
          key: GlobalKey(),
          initiallyExpanded: 3 == selected,
          title: Text(title,
              style: TextStyles.leadsTextStyleBold.copyWith(fontSize: 16)),
          collapsedIconColor: Colors.black,
          children: [
            const Divider(height: 1, thickness: 1, color: Colors.black12),
            getBodyYear3Row(),
          ],
          onExpansionChanged: (isOpen) {
            if (isOpen) {
              setState(() {
                selected = 3;
              });
            } else {
              setState(() {
                selected = 0;
              });
            }
          },
        ),
      ),
    );
  }

  Widget expandTable4(String title) {
    return Padding(
      padding: const EdgeInsets.all(1.0),
      child: Container(
        color: AppColors.white,
        child: ExpansionTile(
          key: GlobalKey(),
          initiallyExpanded: 4 == selected,
          title: Text(title,
              style: TextStyles.leadsTextStyleBold.copyWith(fontSize: 16)),
          collapsedIconColor: Colors.black,
          children: [
            const Divider(height: 1, thickness: 1, color: Colors.black12),
            getBodyYear4Row(),
          ],
          onExpansionChanged: (isOpen) {
            if (isOpen) {
              setState(() {
                selected = 4;
              });
            } else {
              setState(() {
                selected = 0;
              });
            }
          },
        ),
      ),
    );
  }

  Widget expandTable5(String title) {
    return Padding(
      padding: const EdgeInsets.all(1.0),
      child: Container(
        color: AppColors.white,
        child: ExpansionTile(
          key: GlobalKey(),
          initiallyExpanded: 5 == selected,
          title: Text(title,
              style: TextStyles.leadsTextStyleBold.copyWith(fontSize: 16)),
          collapsedIconColor: Colors.black,
          children: [
            const Divider(height: 1, thickness: 1, color: Colors.black12),
            getBodyYear5Row(),
          ],
          onExpansionChanged: (isOpen) {
            if (isOpen) {
              setState(() {
                selected = 5;
              });
            } else {
              setState(() {
                selected = 0;
              });
            }
          },
        ),
      ),
    );
  }

  Widget expandTable6(String title) {
    return Padding(
      padding: const EdgeInsets.all(1.0),
      child: Container(
        color: AppColors.white,
        child: ExpansionTile(
          key: GlobalKey(),
          initiallyExpanded: 6 == selected,
          title: Text(title,
              style: TextStyles.leadsTextStyleBold.copyWith(fontSize: 16)),
          collapsedIconColor: Colors.black,
          children: [
            const Divider(height: 1, thickness: 1, color: Colors.black12),
            getBodyYear6Row(),
          ],
          onExpansionChanged: (isOpen) {
            if (isOpen) {
              setState(() {
                selected = 6;
              });
            } else {
              setState(() {
                selected = 0;
              });
            }
          },
        ),
      ),
    );
  }

  Widget expandTable7(String title) {
    return Padding(
      padding: const EdgeInsets.all(1.0),
      child: Container(
        color: AppColors.white,
        child: ExpansionTile(
          key: GlobalKey(),
          initiallyExpanded: 7 == selected,
          title: Text(title,
              style: TextStyles.leadsTextStyleBold.copyWith(fontSize: 16)),
          collapsedIconColor: Colors.black,
          children: [
            const Divider(height: 1, thickness: 1, color: Colors.black12),
            getBodyYear7Row(),
          ],
          onExpansionChanged: (isOpen) {
            if (isOpen) {
              setState(() {
                selected = 7;
              });
            } else {
              setState(() {
                selected = 0;
              });
            }
          },
        ),
      ),
    );
  }

  // getBodyYear1Row() {
  //   for (var i = 0; i < modelBannerResult.data.length; i++) {
  //     var link = modelBannerResult.data[i].image;
  //     listString.add(link);
  //   }
  //
  // }
  Widget getBodyYear1Row() => Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          for (var i = 0; i < controller.dataSourceTable1.length; i++)
            bodyTable(index: i, model: controller.dataSourceTable1[i])
        ],
      );

  Widget getBodyYear2Row() => Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          for (var i = 0; i < controller.dataSourceTable2.length; i++)
            bodyTable(
                index: i, model: controller.dataSourceTable2[i], startYear: 5)
        ],
      );
  Widget getBodyYear3Row() => Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          for (var i = 0; i < controller.dataSourceTable3.length; i++)
            bodyTable(
                index: i, model: controller.dataSourceTable3[i], startYear: 10)
        ],
      );
  Widget getBodyYear4Row() => Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          for (var i = 0; i < controller.dataSourceTable4.length; i++)
            bodyTable(
                index: i, model: controller.dataSourceTable4[i], startYear: 15)
        ],
      );
  Widget getBodyYear5Row() => Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          for (var i = 0; i < controller.dataSourceTable5.length; i++)
            bodyTable(
                index: i, model: controller.dataSourceTable5[i], startYear: 20)
        ],
      );
  Widget getBodyYear6Row() => Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          for (var i = 0; i < controller.dataSourceTable6.length; i++)
            bodyTable(
                index: i, model: controller.dataSourceTable6[i], startYear: 25)
        ],
      );

  Widget getBodyYear7Row() => Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          for (var i = 0; i < controller.dataSourceTable7.length; i++)
            bodyTable(
                index: i, model: controller.dataSourceTable7[i], startYear: 30)
        ],
      );

  titleTable() {
    return Container(
      color: Colors.grey[100],
      // color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          titleMenu("Year"),
          titleMenu("Interest \nRate"),
          titleMenu("Installment\nAmount"),
          titleMenu("Interest\nRepayment"),
          titleMenu("Principal\nRepayment"),
          titleMenu("Outstanding\nAmount"),
          titleMenu("Cum\nInterest Paid"),
        ],
      ),
    );
  }

  bodyTable(
      {required MortgageRepaymentTableModel model,
      required int index,
      int startYear = 0}) {
    return Container(
      height: 25,
      color: Colors.grey[100],
      // color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          contentBodyYear1(
              title: "Year ${(index + 12) ~/ 12 + startYear}",
              isShow: ((index + 12) % 12) == 0),
          contentBodyMonths("${((index + 12) % 12) + 1}"),
          contentBody("${model.interestRate}0%"),
          contentBody(convertToCurrency(model.installmentAmount)),
          contentBody(convertToCurrency(model.interestRepayment)),
          contentBody(convertToCurrency(model.principalRepayment)),
          contentBody(convertToCurrency(model.outstandingAmount)),
          contentBody(convertToCurrency(model.cumInterestPaid)),
        ],
      ),
    );
  }

  //
  titleMenu(String title) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.all(1),
        child: Container(
          color: AppColors.white,
          height: 35,
          child: Padding(
              padding: const EdgeInsets.all(4.0),
              child: AutoSizeText(
                title,
                style: TextStyles.leadsColorTextStyle2
                    .copyWith(color: AppColors.kPrimaryColor, fontSize: 10),
                maxLines: 2,
                minFontSize: 7,
                maxFontSize: 10,
              )

              // child: FittedBox(
              //
              //   alignment: Alignment.topLeft,
              //   child: Text(
              //     title,
              //     style: TextStyles.leadsColorTextStyle2
              //         .copyWith(color: AppColors.kPrimaryColor, fontSize: 9),
              //   ),
              // ),
              ),
        ),
      ),
    );
  }

  contentBody(String title) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.all(1),
        child: Container(
          color: AppColors.white,
          height: 40,
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: Text(
              title,
              style: TextStyles.leadsColorTextStyle2
                  .copyWith(color: AppColors.grey625F5A, fontSize: 8),
            ),
          ),
        ),
      ),
    );
  }

  contentBodyYear1({String title = "", bool isShow = false}) {
    return isShow
        ? Padding(
            padding: const EdgeInsets.all(1),
            child: Container(
              color: AppColors.white,
              height: 40,
              width: (windowWidth / 7) * 0.7 - 3,
              child: Padding(
                padding: const EdgeInsets.all(4.0),
                child: Text(
                  title,
                  style: TextStyles.leadsColorTextStyle2
                      .copyWith(color: AppColors.grey625F5A, fontSize: 8),
                ),
              ),
            ),
          )
        : Padding(
            padding: const EdgeInsets.all(1),
            child: Container(
              color: AppColors.white,
              height: 40,
              width: (windowWidth / 7) * 0.7 - 3,
              child: Padding(
                padding: const EdgeInsets.all(4.0),
                child: Text(
                  "",
                  style: TextStyles.leadsColorTextStyle2
                      .copyWith(color: AppColors.grey625F5A, fontSize: 8),
                ),
              ),
            ),
          );
  }

  contentBodyMonths(String title) {
    return Padding(
      padding: const EdgeInsets.all(1),
      child: Container(
        color: AppColors.white,
        height: 40,
        width: (windowWidth / 7) * 0.3,
        child: Padding(
          padding: const EdgeInsets.all(4.0),
          child: Text(
            title,
            style: TextStyles.leadsColorTextStyle2
                .copyWith(color: AppColors.grey625F5A, fontSize: 8),
          ),
        ),
      ),
    );
  }

  contentBodyYear(String title) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.all(1),
        child: Container(
          color: AppColors.grey625F5A,
          height: 40,
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: Row(
              children: [
                Expanded(
                  flex: 2,
                  child: Container(
                    color: AppColors.white,
                    child: Text(
                      title,
                      style: TextStyles.leadsColorTextStyle2
                          .copyWith(color: AppColors.grey625F5A, fontSize: 8),
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    color: AppColors.white,
                    child: Text(
                      "1",
                      style: TextStyles.leadsColorTextStyle2
                          .copyWith(color: AppColors.grey625F5A, fontSize: 8),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  headerTitle(String title) {
    return Expanded(
      flex: 1,
      child: Padding(
        padding: const EdgeInsets.all(1),
        child: Container(
          color: AppColors.white,
          child: Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Text(
                title,
                style: TextStyles.leadsTextStyle1.copyWith(fontSize: 11),
                maxLines: 2,
              ),
            ),
          ),
        ),
      ),
    );
  }

  valueTitle(String value) {
    return Expanded(
      flex: 1,
      child: Padding(
        padding: const EdgeInsets.all(1),
        child: Container(
          color: AppColors.white,
          child: Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Text(
                  value,
                  style: TextStyles.leadsTextStyle1.copyWith(fontSize: 11),
                ),
              )),
        ),
      ),
    );
  }
}
