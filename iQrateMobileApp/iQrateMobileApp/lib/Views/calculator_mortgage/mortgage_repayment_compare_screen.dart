import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/mortgage_repayment_report_controller.dart';
import 'package:iqrate/Service/GetControllers.dart';
import 'package:iqrate/Utils/text_formatter.dart';
import 'package:iqrate/Widgets/default_appbar.dart';
import '../../DeviceManager/colors.dart';
import '../../DeviceManager/text_styles.dart';
import '../../Widgets/primary_button.dart';

class MortgageRepaymentCompareScreen extends StatefulWidget {
  const MortgageRepaymentCompareScreen({Key? key}) : super(key: key);

  @override
  State<MortgageRepaymentCompareScreen> createState() =>
      _MortgageRepaymentCompareScreenState();
}

class _MortgageRepaymentCompareScreenState
    extends State<MortgageRepaymentCompareScreen> {
  MortgageRepaymentReportController controller =
      GetControllers.shared.getMortgageRepaymentReportController();
  int activeStep = 0; // Initial step set to 0.
  late double windowHeight;
  late double windowWidth;

  @override
  void initState() {
    super.initState();
    // controller.getInterestRateComparisonModels();
    // controller.arrayLoanDetail.value = getArrayContentLoanDetails();
  }

  @override
  Widget build(BuildContext context) {
    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: DefaultAppBar(
          title: "Mortgage Repayment", windowHeight: windowHeight * 0.09),
      body: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: SingleChildScrollView(
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Padding(
              padding: const EdgeInsets.only(
                  left: 20, top: 20, bottom: 20, right: 20),
              child: Text(
                "Interest Rate Comparison",
                style: TextStyles.leadsColorTextStyle2
                    .copyWith(color: AppColors.kPrimaryColor),
              ),
            ),
            bodyTable(),
            const SizedBox(
              height: 30,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Center(
                child: PrimaryButton(
                  windowHeight: windowHeight,
                  windowWidth: windowWidth,
                  buttonTitle: "Close",
                  onPressed: () {
                    Get.back();
                    //newPurchaseController.onTapBackStep3Lead();
                  },
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }

  bodyTable() {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Container(
        color: Colors.grey[100],
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            //row 1
            Row(
              children: [
                Expanded(
                    child: Padding(
                  padding: const EdgeInsets.all(1),
                  child: Container(
                    color: AppColors.white,
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Text(
                        "",
                        style: TextStyles.leadsColorTextStyle2.copyWith(
                            color: AppColors.kPrimaryColor, fontSize: 14),
                      ),
                    ),
                  ),
                )),
                // Container(color: AppColors.white,child: he),
                Expanded(
                    child: Padding(
                  padding: const EdgeInsets.all(1),
                  child: Container(
                    color: AppColors.white,
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Text(
                        "Interest Rate 1",
                        style: TextStyles.leadsColorTextStyle2.copyWith(
                            color: AppColors.kPrimaryColor, fontSize: 14),
                      ),
                    ),
                  ),
                )),
                if (controller.modelCompare2 != null)
                  Expanded(
                      child: Padding(
                    padding: const EdgeInsets.all(1),
                    child: Container(
                      color: AppColors.white,
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Text(
                          "Interest Rate 2",
                          style: TextStyles.leadsColorTextStyle2.copyWith(
                              color: AppColors.kPrimaryColor, fontSize: 14),
                        ),
                      ),
                    ),
                  )),
              ],
            ),

            // row2
            IntrinsicHeight(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  headerTitle("Monthly Installment"),
                  valueTitle(convertToCurrency(
                      controller.getModelCompare1().monthlyInstallment)),
                  if (controller.modelCompare2 != null)
                    valueTitle(convertToCurrency(
                        controller.getModelCompare2().monthlyInstallment)),
                ],
              ),
            ),
            IntrinsicHeight(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  headerTitle("Total Payment"),
                  valueTitle(convertToCurrency(
                      controller.getModelCompare1().totalPayment)),
                  if (controller.modelCompare2 != null)
                    valueTitle(convertToCurrency(
                        controller.getModelCompare2().totalPayment)),
                ],
              ),
            ),
            IntrinsicHeight(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  headerTitle("Total Principal"),
                  valueTitle(convertToCurrency(
                      controller.getModelCompare1().totalPrincipal)),
                  if (controller.modelCompare2 != null)
                    valueTitle(convertToCurrency(
                        controller.getModelCompare2().totalPrincipal)),
                ],
              ),
            ),
            IntrinsicHeight(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  headerTitle("Total Interest"),
                  valueTitle(convertToCurrency(
                      controller.getModelCompare1().totalInterest)),
                  if (controller.modelCompare2 != null)
                    valueTitle(convertToCurrency(
                        controller.getModelCompare2().totalInterest)),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  headerTitle(String title) {
    return Expanded(
      flex: 1,
      child: Padding(
        padding: const EdgeInsets.all(1),
        child: Container(
          color: AppColors.white,
          child: Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Text(
                title,
                style: TextStyles.leadsTextStyle1.copyWith(fontSize: 12),
                maxLines: 2,
              ),
            ),
          ),
        ),
      ),
    );
  }

  valueTitle(String value) {
    return Expanded(
      flex: 1,
      child: Padding(
        padding: const EdgeInsets.all(1),
        child: Container(
          color: AppColors.white,
          child: Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Text(
                  value,
                  style: TextStyles.leadsTextStyle1.copyWith(fontSize: 12),
                ),
              )),
        ),
      ),
    );
  }
}
