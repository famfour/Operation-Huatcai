// ignore_for_file: prefer_typing_uninitialized_variables, must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/bank_submission_email_banker_screen_controller.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/container_styles.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Widgets/common_appbar_options.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';
import 'package:iqrate/Widgets/secondary_button.dart';

class BankSubmissionEmailBankerScreen extends StatefulWidget {
  const BankSubmissionEmailBankerScreen({Key? key}) : super(key: key);

  @override
  _BankSubmissionEmailBankerScreenState createState() =>
      _BankSubmissionEmailBankerScreenState();
}

class _BankSubmissionEmailBankerScreenState
    extends State<BankSubmissionEmailBankerScreen> {
  BankSubmissionEmailBankerScreenController
      bankSubmissionEmailScreenController =
      Get.put(BankSubmissionEmailBankerScreenController());
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double windowWidth = MediaQuery.of(context).size.width;
    double windowHeight = MediaQuery.of(context).size.height;

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          automaticallyImplyLeading: false,
          centerTitle: false,
          flexibleSpace: const Padding(
            padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 30),
            child: CommonAppBarOptions(
              windowWidth: 0,
              title: 'Manage Loan',
            ),
          ),
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(windowHeight * 0.03),
            child: const SizedBox(),
          ),
          backgroundColor: const Color(0xFFDF5356),
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
            ),
          ),
        ),
        body: Stack(
          children: [
            const Body(),
            Obx(
              () => bankSubmissionEmailScreenController
                      .showPackageExpiredError.value
                  ? Container(
                      height: windowHeight,
                      width: windowWidth,
                      color: Colors.white.withOpacity(0.8),
                    )
                  : const Offstage(),
            ),
            Obx(
              () => bankSubmissionEmailScreenController
                      .showPackageExpiredError.value
                  ? Center(
                      child: ErrorWidget(
                        windowHeight: windowHeight,
                        windowWidth: windowWidth,
                      ),
                    )
                  : const Offstage(),
            )
          ],
        ));
  }
}

class ErrorWidget extends StatelessWidget {
  double windowHeight;
  double windowWidth;
  ErrorWidget({Key? key, required this.windowWidth, required this.windowHeight})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IntrinsicHeight(
      child: Container(
        width: windowWidth * 0.8,
        padding: const EdgeInsets.all(50),
        decoration: BoxDecoration(boxShadow: [
          BoxShadow(
              color: Colors.black.withOpacity(0.5),
              blurRadius: 9.0,
              offset: const Offset(5, 5))
        ], color: Colors.white, borderRadius: BorderRadius.circular(10)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              'Oops!',
              style: TextStyles.bankSubmissionTitleTestStyle,
            ),
            SizedBox(
              height: windowHeight * 0.03,
            ),
            const Text(
                'There are inactive packages in your selected list, please remove them.',
                textAlign: TextAlign.center),
            SizedBox(
              height: windowHeight * 0.03,
            ),
            SecondaryButton(
              windowHeight: windowHeight,
              windowWidth: windowWidth,
              buttonTitle: 'Back',
              onPressed: () {
                Get.back();
              },
              kGradientBoxDecoration:
                  ContainerStyles.kGradientBoxDecorationSecondaryButton,
              kInnerDecoration: ContainerStyles.kInnerDecorationSecondaryButton,
            )
          ],
        ),
      ),
    );
  }
}

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  BankSubmissionEmailBankerScreenController
      bankSubmissionEmailBankerScreenController = Get.find();

  @override
  Widget build(BuildContext context) {
    double windowWidth = MediaQuery.of(context).size.width;
    double windowHeight = MediaQuery.of(context).size.height;
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: ListView(
        children: [
          Text(
            'Email to Banker',
            style: TextStyles.bankSubmissionTitleTestStyle,
          ),
          const Divider(
            thickness: 2,
            color: AppColors.kPrimaryColor,
          ),
          emailBody(windowHeight, windowWidth,
              bankSubmissionEmailBankerScreenController),
          PrimaryButton(
              windowHeight: windowHeight,
              windowWidth: windowWidth,
              buttonTitle: 'Submit',
              onPressed: () {
                // Need to send email to banker on press of the button
                bankSubmissionEmailBankerScreenController.sendEmailToBanker();
              }),
          SizedBox(height: windowHeight * 0.015),
          SecondaryButton(
              windowHeight: windowHeight,
              windowWidth: windowWidth,
              kGradientBoxDecoration:
                  ContainerStyles.kGradientBoxDecorationSecondaryButton,
              kInnerDecoration: ContainerStyles.kInnerDecorationSecondaryButton,
              onPressed: () {
                Get.back();
              },
              buttonTitle: 'Back')
        ],
      ),
    );
  }

  Widget emailBody(
      double windowHeight,
      double windowWidth,
      BankSubmissionEmailBankerScreenController
          bankSubmissionEmailBankerScreenController) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: windowHeight * 0.01,
        ),
        Text(
          'Banker Name:',
          style: TextStyles.bankSubmissionTitles,
        ),
        Obx(
          () => Padding(
            padding: const EdgeInsets.fromLTRB(0.0, 5, 0, 10),
            child: Text(
                bankSubmissionEmailBankerScreenController
                        .bankSubmissionEmailToBankerResponseModel
                        .value
                        .banker ??
                    '',
                style: TextStyles.bankSubmissionBody),
          ),
        ),
        SizedBox(
          height: windowHeight * 0.01,
        ),
        Text(
          'Email Address:',
          style: TextStyles.bankSubmissionTitles,
        ),
        Obx(
          (() => Padding(
                padding: const EdgeInsets.fromLTRB(0.0, 5, 0, 10),
                child: Text(
                    bankSubmissionEmailBankerScreenController
                            .bankSubmissionEmailToBankerResponseModel
                            .value
                            .email ??
                        '',
                    style: TextStyles.bankSubmissionBody),
              )),
        ),
        SizedBox(
          height: windowHeight * 0.01,
        ),
        Text(
          'Law firm selection:',
          style: TextStyles.bankSubmissionTitles,
        ),
        Obx(
          () => bankSubmissionEmailBankerScreenController
                      .bankSubmissionLawfirmListResponseModel
                      .value
                      .lawFirmList !=
                  null
              ? Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 5, 0, 10),
                  child: Container(
                    width: windowWidth,
                    padding:
                        const EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: AppColors.formFieldBorderColor, width: 1.0),
                        borderRadius: const BorderRadius.all(Radius.circular(
                                10) //                 <--- border radius here
                            )),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton(
                        hint: const Text('Select Lawfirm'),
                        value: bankSubmissionEmailBankerScreenController
                            .selectedExistingLawfirm,
                        onChanged: (newValue) {
                          debugPrint(newValue.toString());
                          setState(() {
                            bankSubmissionEmailBankerScreenController
                                    .selectedExistingLawfirm =
                                int.tryParse(newValue.toString());
                          });
                        },
                        items: bankSubmissionEmailBankerScreenController
                            .bankSubmissionLawfirmListResponseModel
                            .value
                            .lawFirmList!
                            .map((lawFirm) {
                          return DropdownMenuItem(
                            child: Text(lawFirm.name!),
                            value: lawFirm.id!,
                          );
                        }).toList(),
                      ),
                    ),
                  ),
                )
              : Container(),
        ),
        // SizedBox(
        //   height: windowHeight * 0.01,
        // ),
        // Text(
        //   'CC:',
        //   style: TextStyles.bankSubmissionTitles,
        // ),
        // Padding(
        //   padding: const EdgeInsets.fromLTRB(0.0, 5, 0, 10),
        //   child: RequireTextField(
        //       type: Type.optional,
        //       controller: bankSubmissionEmailBankerScreenController
        //           .ccTextEditingController,
        //       onChanged: (String value) {
        //         if (value.contains('.com') ||
        //             value.contains('.net') ||
        //             value.contains('.in') ||
        //             value.contains('.sg')) {
        //           if (value.contains(RegExp(
        //               r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+"))) {
        //             bankSubmissionEmailBankerScreenController.ccEmailList
        //                 .add(value);
        //             bankSubmissionEmailBankerScreenController.emailList
        //                 .add(EmailModel(value, value.split('@').first));
        //             bankSubmissionEmailBankerScreenController
        //                 .ccTextEditingController
        //                 .clear();
        //           }
        //         }
        //         // debugPrint(value);
        //       }),
        // ),
        // Obx(
        //   () => SizedBox(
        //     height: bankSubmissionEmailBankerScreenController.emailWidgets
        //             .toList()
        //             .isEmpty
        //         ? 0
        //         : windowHeight * 0.05,
        //     child: Wrap(
        //       children: bankSubmissionEmailBankerScreenController.emailWidgets
        //           .toList(),
        //     ),
        //   ),
        // ),
        Padding(
          padding: const EdgeInsets.only(top: 10.0),
          child: Text(
            'Subject:',
            style: TextStyles.bankSubmissionTitles,
          ),
        ),
        Obx(
          () => Padding(
            padding: const EdgeInsets.fromLTRB(0.0, 5, 0, 10),
            child: Text(
                bankSubmissionEmailBankerScreenController
                        .bankSubmissionEmailToBankerResponseModel
                        .value
                        .subject ??
                    '',
                style: TextStyles.bankSubmissionBody),
          ),
        ),
        SizedBox(
          height: windowHeight * 0.015,
        ),
        Text(
          'Email content:',
          style: TextStyles.bankSubmissionTitles,
        ),
        Obx(
          () => bankSubmissionEmailBankerScreenController
                      .bankSubmissionEmailToBankerResponseModel.value.content !=
                  null
              ? Html(
                  style: {
                    "body":
                        Style(margin: EdgeInsets.zero, padding: EdgeInsets.zero)
                  },
                  data: bankSubmissionEmailBankerScreenController
                      .bankSubmissionEmailToBankerResponseModel.value.content,
                )
              : Container(),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(0.0, 25, 0, 5),
          child: Text(
            'Add Remarks into Email:',
            style: TextStyles.bankSubmissionTitles,
          ),
        ),
        RequireTextField(
          type: Type.optionalTextArea,
          maxLength: 500,
          controller: bankSubmissionEmailBankerScreenController
              .remarksTextEditingController,
        ),
        SizedBox(
          height: windowHeight * 0.02,
        ),
        bankSubmissionEmailBankerScreenController.documentsList.isNotEmpty
            ? Obx(
                () => bankSubmissionEmailBankerScreenController
                            .bankSubmissionEmailToBankerResponseModel
                            .value
                            .attachments !=
                        null
                    ? Padding(
                        padding: const EdgeInsets.fromLTRB(0.0, 10, 0, 10),
                        child: ListView.builder(
                            shrinkWrap: true,
                            itemCount: bankSubmissionEmailBankerScreenController
                                .bankSubmissionEmailToBankerResponseModel
                                .value
                                .attachments
                                ?.length,
                            itemBuilder: (context, index) {
                              return Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  const Icon(Icons.attachment),
                                  Text(
                                      bankSubmissionEmailBankerScreenController
                                          .bankSubmissionEmailToBankerResponseModel
                                          .value
                                          .attachments![index],
                                      style: TextStyles.bankSubmissionBody)
                                ],
                              );
                            }),
                      )
                    : SizedBox(
                        height: windowHeight * 0.02,
                      ),
              )
            : SizedBox(
                height: windowHeight * 0.02,
              ),
        SizedBox(
          height: windowHeight * 0.01,
        ),
      ],
    );
  }
}
