// ignore_for_file: prefer_typing_uninitialized_variables, invalid_use_of_protected_member, non_constant_identifier_names

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/my_documents_controller.dart';
import 'package:iqrate/DeviceManager/container_styles.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Model/response_model.dart/documents_drawer_response_model.dart';
import 'package:iqrate/Widgets/common_appbar_options.dart';
import 'package:iqrate/Widgets/secondary_button.dart';

class MyDocumentsScreen extends StatefulWidget {
  const MyDocumentsScreen({Key? key}) : super(key: key);

  @override
  _MyDocumentsScreenState createState() => _MyDocumentsScreenState();
}

class _MyDocumentsScreenState extends State<MyDocumentsScreen> {
  final MyDocumentsController myDocumentsController =
      Get.put(MyDocumentsController());

  late double windowHeight;
  late double windowWidth;

  /*void downlaod() async {
    launchu("https://s3.ap-southeast-1.amazonaws.com/iqrate-loan-files/1cd1e4c5-6d5e-4bb3-a10d-77ca5de76ff4/f120a301-7dd8-4f06-92d8-988f3e1aed56.jpg?AWSAccessKeyId=AKIARMFLUTLW7GU7JBPR&Signature=gxgwlL4PWR1daRXz%2FMBQTOimgvs%3D&Expires=1652901295");
  }*/

  @override
  void initState() {
    Timer(const Duration(milliseconds: 100), () {
      Get.isDialogOpen ?? true
          ? const Offstage() //Used for avoiding the dialog to be open when the user is already on the next screen.
          : Get.dialog(const Center(child: CircularProgressIndicator()),
              //Used for showing the loading dialog.
              barrierDismissible:
                  false); //Used for preventing the user to dismiss the dialog.
      myDocumentsController.getDocuments();
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.width;

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          automaticallyImplyLeading: false,
          centerTitle: false,
          flexibleSpace: const Padding(
            padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 30),
            child: CommonAppBarOptions(
              windowWidth: 0,
              title: "My Documents",
            ),
          ),
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(windowHeight * 0.03),
            child: const SizedBox(),
          ),
          backgroundColor: const Color(0xFFDF5356),
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                  margin: EdgeInsets.all(windowWidth * 0.05),
                  padding: EdgeInsets.all(windowWidth * 0.05),
                  width: windowWidth,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.2),
                        spreadRadius: 3,
                        blurRadius: 7,
                        offset:
                            const Offset(0, 3), // changes position of shadow
                      ),
                    ],
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Flexible(
                            child: Text(
                              'Get lead to complete the following document upload',
                              style: TextStyles.leadsTextStyleBold,
                            ),
                          ),
                          SizedBox(width: windowWidth * 0.03),
                          InkWell(
                              onTap: () {
                                debugPrint("onTap Share");
                                //myDocumentsController.getDocuments();
                                myDocumentsController.share(
                                    myDocumentsController.leadsViewController
                                        .document_drawer_url);
                              },
                              child: const Icon(
                                Icons.share,
                                size: 30,
                              ))
                        ],
                      ),
                      SizedBox(height: windowHeight * 0.02),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Flexible(
                            flex: 3,
                            child: InkWell(
                                onTap: () {
                                  myDocumentsController.launchUrl(
                                      myDocumentsController.leadsViewController
                                          .document_drawer_url);
                                  debugPrint("url launcher");
                                },
                                child: Text(
                                  myDocumentsController
                                      .leadsViewController.document_drawer_url,
                                  style: TextStyles.docLinkTextStyle,
                                  maxLines: 2,
                                )),
                          ),
                          SizedBox(height: windowHeight * 0.03),
                          InkWell(
                              onTap: () {
                                debugPrint("onTap Share");
                                //myDocumentsController.getDocuments();
                                myDocumentsController.copy(myDocumentsController
                                    .leadsViewController.document_drawer_url);
                              },
                              child: Image.asset(
                                "assets/images/copy.png",
                                width: 25,
                              )),
                        ],
                      ),
                    ],
                  )),
              Obx(() => section(
                  "Bank Forms",
                  myDocumentsController.bankForms.value,
                  myDocumentsController.leadData.canEdit!,
                  myDocumentsController.leadData.leadType!)),
              Obx(() => section(
                  "NRIC",
                  myDocumentsController.nRICs.value,
                  myDocumentsController.leadData.canEdit!,
                  myDocumentsController.leadData.leadType!)),
              Obx(() => section(
                  "Option to Purchase",
                  myDocumentsController.oTPOptiontoPurchase.value,
                  myDocumentsController.leadData.canEdit!,
                  myDocumentsController.leadData.leadType!)),
              Obx(() => section(
                  "IRAS Statements",
                  myDocumentsController.iRASs.value,
                  myDocumentsController.leadData.canEdit!,
                  myDocumentsController.leadData.leadType!)),
              Obx(() => section(
                  "CPF Statements",
                  myDocumentsController.cPFs.value,
                  myDocumentsController.leadData.canEdit!,
                  myDocumentsController.leadData.leadType!)),
              Obx(() => section(
                  "HDB Statements",
                  myDocumentsController.hDBs.value,
                  myDocumentsController.leadData.canEdit!,
                  myDocumentsController.leadData.leadType!)),
              Obx(() => section(
                  "Payslips",
                  myDocumentsController.payslips.value,
                  myDocumentsController.leadData.canEdit!,
                  myDocumentsController.leadData.leadType!)),
              Obx(() => section(
                  "Valuation Reports",
                  myDocumentsController.valuationReports.value,
                  myDocumentsController.leadData.canEdit!,
                  myDocumentsController.leadData.leadType!)),
              Obx(() => section(
                  "Outstanding Loan Statements",
                  myDocumentsController.loanStatements.value,
                  myDocumentsController.leadData.canEdit!,
                  myDocumentsController.leadData.leadType!)),
              Obx(() => section(
                  "Tenancy Agreement",
                  myDocumentsController.tenancyAgreement.value,
                  myDocumentsController.leadData.canEdit!,
                  myDocumentsController.leadData.leadType!)),
              Obx(() => section(
                  "Others",
                  myDocumentsController.others.value,
                  myDocumentsController.leadData.canEdit!,
                  myDocumentsController.leadData.leadType!)),
              Obx(() => section(
                  "LO",
                  myDocumentsController.lOs.value,
                  myDocumentsController.leadData.canEdit!,
                  myDocumentsController.leadData.leadType!)),
              Obx(() => section(
                  "Calculator Reports",
                  myDocumentsController.calculatorReports.value,
                  myDocumentsController.leadData.canEdit!,
                  myDocumentsController.leadData.leadType!)),
              SizedBox(height: windowHeight * 0.02),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: windowWidth * 0.05),
                child: SecondaryButton(
                  windowHeight: windowHeight,
                  windowWidth: windowWidth,
                  kGradientBoxDecoration:
                      ContainerStyles.kGradientBoxDecorationSecondaryButton,
                  kInnerDecoration:
                      ContainerStyles.kInnerDecorationSecondaryButton,
                  buttonTitle: 'Back',
                  onPressed: () {
                    myDocumentsController.onPressBack();
                  },
                ),
              ),
            ],
          ),
        ));
  }

  Widget section(String doc_type, List<Documents> documents, bool canEdit,
      String leadType) {
    return Container(
      margin: EdgeInsets.all(windowWidth * 0.05),
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.black26,
          style: BorderStyle.solid,
          width: 1.0,
        ),
        color: Colors.transparent,
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(windowWidth * 0.05),
            width: windowWidth,
            decoration: BoxDecoration(
              color: Colors.black12,
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: Text(
                    doc_type,
                    style: TextStyles.leadsTextStyleBold,
                  ),
                ),
                ((canEdit && leadType == 'others') || leadType == 'yourself')
                    ? Text(
                        'Max 3 Mb',
                        style: TextStyles.leadsNormalTextStyle,
                      )
                    : const Offstage(),
                SizedBox(width: windowWidth * 0.03),
                //((canEdit && leadType == 'others') || leadType == 'yourself')
                ((leadType == 'others') || leadType == 'yourself')
                    ? InkWell(
                        onTap: () {
                          myDocumentsController.onTapUploadPopup(doc_type);

                          debugPrint("onTap add");
                        },
                        child: const Icon(
                          Icons.add,
                          color: Colors.red,
                          size: 35,
                        ))
                    : const Offstage()
              ],
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ListView.builder(
                itemCount: documents.length,
                primary: false,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  var item = documents.elementAt(index);
                  return Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(20),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Text(
                                item.filename!,
                                style: TextStyles.docTextStyle,
                              ),
                            ),
                            //((canEdit && leadType == 'others') ||
                            ((leadType == 'others') ||
                                    leadType == 'yourself')
                                ? InkWell(
                                    onTap: () {
                                      myDocumentsController
                                          .onTapPopUpUpdate(item);
                                      debugPrint("onTap edit");
                                    },
                                    child: const Icon(
                                      Icons.edit,
                                      size: 30,
                                    ))
                                : const Offstage(),
                            SizedBox(width: windowWidth * 0.03),
                            InkWell(
                                onTap: () {
                                  myDocumentsController
                                      .onTapDownloadButton(item.id!);
                                  debugPrint("onTap downlaod");
                                },
                                child: const Icon(
                                  Icons.remove_red_eye_outlined,
                                  size: 30,
                                )),
                            SizedBox(width: windowWidth * 0.03),
                            //((canEdit && leadType == 'others') ||
                            ((leadType == 'others') ||
                                    leadType == 'yourself')
                                ? InkWell(
                                    onTap: () {
                                      myDocumentsController
                                          .onTapPopUpDelete(item.id);
                                      debugPrint("onTap delete");
                                    },
                                    child: const Icon(
                                      Icons.delete_outline,
                                      size: 30,
                                    ))
                                : const Offstage()
                          ],
                        ),
                      ),
                      documents.length != 1 && documents.length != index + 1
                          ? const Divider(
                              height: 1,
                              thickness: 1,
                              color: Colors.black26,
                            )
                          : const SizedBox(),
                    ],
                  );
                },
              ),
              /*const Divider(
                height: 1,
                thickness: 1,
                color: Colors.black26,
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flexible(
                      child: Text(
                        'Documents',
                        style: TextStyles.docTextStyle,
                      ),
                    ),
                    SizedBox(width: windowWidth * 0.03),
                    const Spacer(),
                    InkWell(
                        onTap: () {
                          debugPrint("onTap edit");
                        },
                        child: const Icon(
                          Icons.edit,
                          size: 30,
                        )),
                    SizedBox(width: windowWidth * 0.03),
                    InkWell(
                        onTap: () {
                          debugPrint("onTap eye");
                        },
                        child: const Icon(
                          Icons.remove_red_eye_outlined,
                          size: 30,
                        )),
                    SizedBox(width: windowWidth * 0.03),
                    InkWell(
                        onTap: () {
                          myDocumentsController.onTapPopUpDelete();
                          debugPrint("onTap delete2");
                        },
                        child: const Icon(
                          Icons.delete_outline,
                          size: 30,
                        ))
                  ],
                ),
              ),*/
            ],
          ),
        ],
      ),
    );
  }
}
