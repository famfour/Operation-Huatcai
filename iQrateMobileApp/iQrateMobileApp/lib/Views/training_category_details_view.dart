// ignore_for_file: deprecated_member_use

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/training_category_details_view_controller.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:video_player/video_player.dart';

// ignore: must_be_immutable
class TrainingCategoryDetailsView extends StatelessWidget {
  TrainingCategoryDetailsView({Key? key}) : super(key: key);
  TrainingCategoryDetailsViewController trainingCategoryDetailsViewController =
      Get.put(TrainingCategoryDetailsViewController());
  late double windowHeight;
  late double windowWidth;

  @override
  Widget build(BuildContext context) {
    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.height;
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.all(10.0),
          child: ListView(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  InkWell(
                    onTap: () {
                      Get.delete<TrainingCategoryDetailsViewController>();
                      Get.back();
                    },
                    child: SvgPicture.asset(
                      'assets/icons/closeEnclosed.svg',
                      color: Colors.black,
                    ),
                  )
                ],
              ),
              SizedBox(height: windowHeight * 0.02),
              ContentWidget(
                url: trainingCategoryDetailsViewController
                    .trainingViewResponseModel.media!.url,
                heading: trainingCategoryDetailsViewController
                    .trainingViewResponseModel.title,
                body: trainingCategoryDetailsViewController
                    .trainingViewResponseModel.body,
                date: trainingCategoryDetailsViewController
                    .trainingViewResponseModel.createdAt
                    .toString(),
                mediaType: trainingCategoryDetailsViewController
                    .trainingViewResponseModel.media!.ext,
                windowHeight: windowHeight,
                windowWidth: windowWidth,
              )
            ],
          ),
        ),
      ),
    );
  }
}

// ignore: must_be_immutable
class ContentWidget extends StatefulWidget {
  ContentWidget(
      {Key? key,
      required this.heading,
      required this.body,
      required this.url,
      this.mediaType,
      required this.date,
      required this.windowHeight,
      required this.windowWidth})
      : super(key: key);
  String? url;
  String? heading;
  String? body;
  String? mediaType;
  String? date;
  double windowWidth;
  double windowHeight;

  @override
  State<ContentWidget> createState() => _ContentWidgetState();
}

class _ContentWidgetState extends State<ContentWidget>
    with SingleTickerProviderStateMixin {
  TrainingCategoryDetailsViewController trainingCategoryDetailsViewController =
      Get.find();
  late VideoPlayerController _controller;

  //* Animation controller for the playpause button
  late final AnimationController animationController = AnimationController(
    duration: const Duration(milliseconds: 200),
    vsync: this,
  );

  //* Animation type for the playpause button
  late final Animation<double> _animation =
      Tween<double>(begin: 1, end: 0).animate(animationController);

  @override
  void initState() {
    super.initState();

    //* images ext= .jpeg, .png, .gif, .jpg)
    //* video ext= .mp4, .mpeg, .mpg, .mkv,.avi

    if (widget.mediaType == '.mpeg' ||
        widget.mediaType == '.mp4' ||
        widget.mediaType == '.mkv' ||
        widget.mediaType == '.avi' ||
        widget.mediaType == '.webp') {
      if (widget.url != null) {
        // Initialize the controller for videoPlayer
        _controller = VideoPlayerController.network(
            "https://iqrate-strapi.dprizm.dev" + widget.url!)
          ..initialize().then((_) {
            // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
            setState(() {});
          })
          ..setLooping(false)
          ..addListener(() {
            setState(
                () {}); // used for changing the status of the play/pause button
          });
      }
      // This listener is used for resetting the status of the play/pause button after video completion
      animationController.addListener(() {
        setState(() {});
      });
    } else {}
  }

  // Disposing the controllers
  @override
  void dispose() {
    animationController.dispose();
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // double windowheight = MediaQuery.of(context).size.height;
    debugPrint(widget.mediaType);
    return ListView(
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      children: [
        widget.heading != null
            ? Padding(
                padding: const EdgeInsets.only(bottom: 5.0),
                child: Text(
                  widget.heading!,
                  style: TextStyles.trainingDetailsCategoryHeadingTextSTyle,
                ),
              )
            : const Text(''),
        widget.date != null
            ? Padding(
                padding: const EdgeInsets.only(bottom: 5.0),
                child: Text(
                  trainingCategoryDetailsViewController
                      .dateFormatter(widget.date!),
                ))
            : const Text(''),
        widget.mediaType == '.jpeg' ||
                widget.mediaType == '.jpg' ||
                widget.mediaType == '.png' ||
                widget.mediaType == '.gif'
            ? widget.url != null
                ? Image.network(
                    "https://iqrate-strapi.dprizm.dev" + widget.url!)
                : SizedBox(
                    height: widget.windowHeight * 0.2,
                    child: Image.asset(
                      'assets/images/noImage.png',
                      fit: BoxFit.contain,
                    ),
                  )
            : Stack(
                children: [
                  _controller.value.isInitialized
                      ? AspectRatio(
                          aspectRatio: _controller.value.aspectRatio,
                          child: Stack(
                            children: [
                              GestureDetector(
                                  onTap: () {
                                    // Show and hide widget
                                    animationController.reverse();
                                  },
                                  child: VideoPlayer(_controller)),
                              _animation.value > 0
                                  ? Center(
                                      child: _controller.value.isPlaying
                                          ? IconButton(
                                              onPressed: () {
                                                _controller.pause();
                                              },
                                              icon: const Icon(
                                                Icons.pause,
                                                color: Colors.white,
                                                size: 50,
                                              ))
                                          : IconButton(
                                              onPressed: () {
                                                _controller.play();
                                                animationController.forward();
                                              },
                                              icon: const Icon(
                                                Icons.play_circle_outline,
                                                color: Colors.white,
                                                size: 50,
                                              )),
                                    )
                                  : const Offstage()
                            ],
                          ),
                        )
                      : Container(),
                  Align(
                    alignment: Alignment.topRight,
                    child: Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: CircleAvatar(
                        backgroundColor: Colors.white,
                        child: IconButton(
                            onPressed: () async {
                              if (Platform.isIOS) {
                                if (await canLaunch(
                                    "https://iqrate-strapi.dprizm.dev" +
                                        widget.url!.toString())) {
                                  await launch(
                                      "https://iqrate-strapi.dprizm.dev" +
                                          widget.url!.toString(),
                                      forceSafariVC: false);
                                } else {
                                  if (await canLaunch(
                                      "https://iqrate-strapi.dprizm.dev" +
                                          widget.url!.toString())) {
                                    await launch(
                                        "https://iqrate-strapi.dprizm.dev" +
                                            widget.url!.toString());
                                  } else {
                                    throw 'Could not launch ${"https://iqrate-strapi.dprizm.dev" + widget.url!.toString()}';
                                  }
                                }
                              } else {
                                String url =
                                    trainingCategoryDetailsViewController
                                        .trainingViewResponseModel.url
                                        .toString();
                                if (await canLaunch(url)) {
                                  await launch(url);
                                } else {
                                  throw 'Could not launch $url';
                                }
                              }
                            },
                            icon: const Icon(
                              Icons.open_in_new,
                              color: Colors.black,
                            )),
                      ),
                    ),
                  ),
                ],
              ),
        // : widget.url != null
        //     ? Image.network(widget.url!)
        //     : SizedBox(
        //         height: windowheight * 0.15,
        // child: Image.asset(
        //   'assets/images/noImage.png',
        //   fit: BoxFit.contain,
        // )),
        SizedBox(height: widget.windowHeight * 0.02),
        widget.body != null
            ? Text(
                widget.body!,
                style: TextStyles.trainingDetailsCategoryBodyTextSTyle,
              )
            : const Text('')
      ],
    );
  }
}
