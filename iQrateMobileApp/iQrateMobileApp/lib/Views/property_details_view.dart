import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:iqrate/Controller/login_controller.dart';
import 'package:iqrate/Controller/property_details_controller.dart';
import 'package:iqrate/DeviceManager/assets.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Model/response_model.dart/fetch_lead_list_response_model.dart';

import 'package:iqrate/Widgets/form_field_title.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';

import '../Service/GetControllers.dart';

class PropertyDetailsView extends StatefulWidget {
  final Result data;
  const PropertyDetailsView({required this.data, Key? key}) : super(key: key);

  @override
  _PropertyDetailsViewState createState() => _PropertyDetailsViewState();
}

class _PropertyDetailsViewState extends State<PropertyDetailsView> {
  late double windowHeight;
  late double windowWidth;

  PropertyDetailsController propertyDetailsController =
      Get.put(PropertyDetailsController());
  SignInController signInController = Get.find();
  final oCcy = NumberFormat("#,##0.00", "en_US");

  @override
  void initState() {
    initData();
    super.initState();
  }

  late Result leadData;
  late bool isCompleted;

  initData() {
    leadData = widget.data;
    //debugPrint(">>>>>>>>" + leadData.toJson().toString());

    propertyDetailsController.leadID = leadData.id;

    if (leadData.property != null) {
      debugPrint("source:: " + leadData.property['property_status'].toString());

      propertyDetailsController.propertyID.value =
          leadData.property['id'].toString();
      propertyDetailsController.propertyPurchasePriceController
          .text = leadData.property['purchase_price'].toString() !=
              'null'
          ? oCcy.format(
              double.tryParse(leadData.property['purchase_price'].toString()))
          : "";
      propertyDetailsController.postCodeController.text =
          leadData.property['postal_code'].toString();
      propertyDetailsController.streetNameController.text =
          leadData.property['street_name'].toString();
      propertyDetailsController.unitNoController.text =
          leadData.property['unit_number'].toString();
      propertyDetailsController.projectNameController.text =
          leadData.property['project_name'].toString();
      // Populate the data in dropdown for property type
      propertyDetailsController.selectedPropertyType =
          propertyDetailsController.dropdownPropertyType[
              propertyDetailsController.dropDownPropertyTypeKey
                  .indexOf(leadData.property['property_type'])];
      propertyDetailsController.propertyTypeValueToPassInAPI =
          leadData.property['property_type'].toString();

      // Populate the data in dropdown for property status
      propertyDetailsController.selectedPropertyStatusType =
          propertyDetailsController.dropdownPropertyStatusType[
              propertyDetailsController.dropdownPropertyStatusTypeKey
                  .indexOf(leadData.property['property_status'])];
      propertyDetailsController.propertyStatusTypeToPassInAPI =
          leadData.property['property_status'].toString();
    }
    if (propertyDetailsController
            .propertyPurchasePriceController.text.isEmpty &&
        propertyDetailsController.postCodeController.text.isEmpty) {
      isCompleted = false;
    } else {
      isCompleted = true;
    }
  }

  var leadProfileController = GetControllers.shared.getLeadProfileController();

  @override
  Widget build(BuildContext context) {
    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.width;
    return Obx(() => IgnorePointer(
        ignoring: !leadProfileController.clientPDPAStatus.value,
        child: expandableCard("Property Details", isCompleted)));
  }

  Widget propertyDetails() {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(windowHeight * 0.025),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          /*const FormFieldTitle(title: 'KV Store*'),
          SizedBox(height: windowHeight * 0.01),
          Container(
            width: windowWidth,
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
            decoration: BoxDecoration(
                border:
                Border.all(color: AppColors.formFieldBorderColor, width: 1.0),
                borderRadius: const BorderRadius.all(
                    Radius.circular(10) //                 <--- border radius here
                )),
            child: DropdownButtonHideUnderline(
              child: DropdownButton<KvValueStoreResponseModel>(
                hint: const Text('Select KV'),
                // Not necessary for Option 1
                value: propertyDetailsController.selectedKVStore,
                onChanged: ( newValue) {
                  propertyDetailsController.selectedKVStore = newValue!;
                  debugPrint(">>>>>>>"+newValue.key.toString());
                  setState(() {});
                },
                items: signInController.kvStoreValues.map((KvValueStoreResponseModel item) {
                  return DropdownMenuItem<KvValueStoreResponseModel>(
                    child: Text(item.key.toString()),
                    value: item,
                  );
                }).toList(),
              ),
            ),
          ),
          SizedBox(height: windowHeight * 0.02),*/
          const FormFieldTitle(title: 'Property Status*'),
          SizedBox(height: windowHeight * 0.01),
          IgnorePointer(
            ignoring: !widget.data.canEdit!, //readonly if can edit is false
            child: Container(
              width: windowWidth,
              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
              decoration: BoxDecoration(
                  border: Border.all(
                      color: AppColors.formFieldBorderColor, width: 1.0),
                  borderRadius: const BorderRadius.all(Radius.circular(
                          10) //                 <--- border radius here
                      )),
              child: DropdownButtonHideUnderline(
                child: DropdownButton<String>(
                  hint: const Text('Select Status'),
                  // Not necessary for Option 1
                  value: propertyDetailsController.selectedPropertyStatusType,
                  onChanged: (newValue) {
                    setState(() {
                      propertyDetailsController.selectedPropertyStatusType =
                          newValue!;
                    });
                    // Assign the value in the string variable that will be passed to the server
                    propertyDetailsController.propertyStatusTypeToPassInAPI =
                        propertyDetailsController.dropdownPropertyStatusTypeKey[
                            propertyDetailsController.dropdownPropertyStatusType
                                .indexOf(newValue!)];
                  },
                  items: propertyDetailsController.dropdownPropertyStatusType
                      .map((location) {
                    return DropdownMenuItem(
                      child: SizedBox(
                          width: windowWidth * 0.7,
                          child: Text(location
                              .toString()
                              .replaceAll('_', ' ')
                              .capitalizeFirst!)),
                      value: location,
                    );
                  }).toList(),
                ),
              ),
            ),
          ),
          SizedBox(height: windowHeight * 0.02),
          const FormFieldTitle(title: 'Property Type*'),
          SizedBox(height: windowHeight * 0.01),
          Container(
            width: windowWidth,
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
            decoration: BoxDecoration(
                border: Border.all(
                    color: AppColors.formFieldBorderColor, width: 1.0),
                borderRadius: const BorderRadius.all(Radius.circular(
                        10) //                 <--- border radius here
                    )),
            child: IgnorePointer(
              ignoring: !widget.data.canEdit!, //readonly if can edit is false,
              child: DropdownButtonHideUnderline(
                child: DropdownButton<String>(
                  hint: const Text('Select Type'),
                  // Not necessary for Option 1
                  value: propertyDetailsController.selectedPropertyType,
                  onChanged: (newValue) {
                    setState(() {
                      propertyDetailsController.selectedPropertyType =
                          newValue!;
                    });
                    // Assign the value in the string variable that will be passed to the server
                    propertyDetailsController.propertyTypeValueToPassInAPI =
                        propertyDetailsController.dropDownPropertyTypeKey[
                            propertyDetailsController.dropdownPropertyType
                                .indexOf(newValue!)];
                  },
                  items: propertyDetailsController.dropdownPropertyType
                      .map((location) {
                    debugPrint(propertyDetailsController.dropdownPropertyType
                        .toString());
                    return DropdownMenuItem(
                      child: Text(location.toString()),
                      value: location,
                    );
                  }).toList(),
                ),
              ),
            ),
          ),
          SizedBox(height: windowHeight * 0.02),
          const FormFieldTitle(title: 'Property Purchase Price'),
          SizedBox(height: windowHeight * 0.01),
          RequireTextField(
            type: Type.money,
            controller:
                propertyDetailsController.propertyPurchasePriceController,
            labelText: "Enter amount",
            readOnly: !widget.data.canEdit!, //readonly if can edit is false,
            key: const Key("Price"),
          ),
          // RequireTextField(
          //   type: Type.number,
          //   controller:
          //       propertyDetailsController.propertyPurchasePriceController,
          //   labelText: "Enter amount",
          //   key: const Key("Price"),
          // ),
          SizedBox(height: windowHeight * 0.02),
          const FormFieldTitle(title: 'Postal Code'),
          SizedBox(height: windowHeight * 0.01),
          RequireTextField(
            type: Type.number,
            controller: propertyDetailsController.postCodeController,
            labelText: "Enter Postal Code",
            key: const Key("Postal"),
            readOnly: !widget.data.canEdit!, //readonly if can edit is false,
            maxLength: 6,
            onChanged: (String value) {
              if (value.length == 6) {
                propertyDetailsController.getAddressWithPostalCode(value);
              }
            },
          ),
          SizedBox(height: windowHeight * 0.02),
          const FormFieldTitle(title: 'Street Name'),
          SizedBox(height: windowHeight * 0.01),
          RequireTextField(
            type: Type.optional,
            controller: propertyDetailsController.streetNameController,
            labelText: "",
            readOnly: !widget.data.canEdit!, //readonly if can edit is false,
            key: const Key("Street"),
          ),
          SizedBox(height: windowHeight * 0.02),
          const FormFieldTitle(title: 'Unit No'),
          SizedBox(height: windowHeight * 0.01),
          RequireTextField(
            type: Type.optional,
            controller: propertyDetailsController.unitNoController,
            labelText: "Enter Unit No",
            readOnly: !widget.data.canEdit!, //readonly if can edit is false,
            key: const Key("Unit"),
          ),
          SizedBox(height: windowHeight * 0.02),
          const FormFieldTitle(title: 'Project Name'),
          SizedBox(height: windowHeight * 0.01),
          RequireTextField(
            type: Type.optional,
            controller: propertyDetailsController.projectNameController,
            labelText: "Enter Project Name",
            readOnly: !widget.data.canEdit!, //readonly if can edit is false,
            key: const Key("Project Name"),
          ),
          widget.data.canEdit ??
                  false //Condition to hide widget if canEdit is false
              ? SizedBox(height: windowHeight * 0.03)
              : Container(),
          widget.data.canEdit ??
                  false //Condition to hide widget if canEdit is false
              ? Center(
                  child: PrimaryButton(
                    windowHeight: windowHeight,
                    windowWidth: windowWidth,
                    buttonTitle: "Save",
                    onPressed: () {
                      propertyDetailsController.onTapSave();
                    },
                  ),
                )
              : Container(),
          widget.data.canEdit ??
                  false //Condition to hide widget if canEdit is false
              ? SizedBox(height: windowHeight * 0.01)
              : Container(),
        ],
      ),
    );
  }

  Widget expandableCard(String title, bool isDone) {
    return GestureDetector(
      onTap: () {
        propertyDetailsController.isExpand.toggle();
        GetControllers.shared.getLoanDetailsController().isExpand.value = false;
        GetControllers.shared.getLeadProfileController().isExpand.value = false;
        GetControllers.shared.getGeneratePackagesController().isExpand.value =
            false;
        GetControllers.shared.getSelectedPackagesController().isExpand.value =
            false;
        GetControllers.shared.getBankSubmissionController().isExpand.value =
            false;
        GetControllers.shared
            .getSubmitForPayoutViewController()
            .isExpand
            .value = false;
        GetControllers.shared.getLawFirmSubmissionController().isExpand.value =
            false;
        GetControllers.shared.getEmailLogController().isExpand.value = false;
        GetControllers.shared.getSubmitCoBrokeController().isExpand.value =
            false;
      },
      child: Obx(() => Container(
            color: Colors.white,
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(bottom: windowWidth * 0.02),
                  padding: EdgeInsets.all(windowWidth * 0.05),
                  decoration: BoxDecoration(
                      boxShadow: const <BoxShadow>[
                        BoxShadow(
                            color: Colors.black54,
                            blurRadius: 5.0,
                            offset: Offset(0.0, 0.3))
                      ],
                      color: !leadProfileController.clientPDPAStatus.value
                          ? AppColors.greyDEDEDE
                          : Colors.white),
                  child: Row(
                    children: [
                      Text(title, style: TextStyles.basicTextStyle),
                      const Spacer(),
                      Obx(() => SvgPicture.asset(
                            propertyDetailsController
                                    .propertyID.value.isNotEmpty
                                ? Assets.manageLoanDone
                                : Assets.manageLoanIncomplete,
                            height: 24,
                            width: 24,
                          )),
                      SizedBox(width: windowWidth * 0.03),
                      Icon(
                        propertyDetailsController.isExpand.value
                            ? Icons.keyboard_arrow_up_rounded
                            : Icons.keyboard_arrow_down,
                        size: 30,
                      )
                    ],
                  ),
                ),
                propertyDetailsController.isExpand.value
                    ? propertyDetails()
                    : Container()
              ],
            ),
          )),
    );
  }
}
