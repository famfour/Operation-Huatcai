import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/bank_submission_controller.dart';
import 'package:iqrate/DeviceManager/assets.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Model/response_model.dart/bank_submission_banks_list_response_model.dart';
import 'package:iqrate/Model/response_model.dart/fetch_lead_list_response_model.dart';
import 'package:iqrate/Router/route_constants.dart';

import '../Service/GetControllers.dart';

class BankSubmissionView extends StatefulWidget {
  final Result data;
  const BankSubmissionView({Key? key, required this.data}) : super(key: key);

  @override
  _BankSubmissionViewState createState() => _BankSubmissionViewState();
}

class _BankSubmissionViewState extends State<BankSubmissionView> {
  late double windowHeight;
  late double windowWidth;

  BankSubmissionController bankSubmissionController =
      Get.put(BankSubmissionController());

  @override
  void initState() {
    bankSubmissionController.getBanks(widget.data.id.toString());
    super.initState();
  }

  var leadProfileController = GetControllers.shared.getLeadProfileController();

  @override
  Widget build(BuildContext context) {
    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.width;
    return Obx(
      () => IgnorePointer(
        ignoring: !leadProfileController.clientPDPAStatus.value,
        child: expandableCard("Bank Submission", false),
      ),
    );
  }

  Widget banksTaskList() {
    return Container(
      color: Colors.white,
      //padding: EdgeInsets.all(windowHeight * 0.025),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: windowHeight * 0.02),
          Obx(() => bankSubmissionController
                      .bankSubmissionBanksListResponseModel
                      .value
                      .bankSubmissionOneBank !=
                  null
              ? bankSubmissionController.bankSubmissionBanksListResponseModel
                      .value.bankSubmissionOneBank!.length
                      .isEqual(0)
                  ? Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                            height: windowHeight * 0.15,
                            child:
                                SvgPicture.asset('assets/images/nodata.svg')),
                        Text(
                          'No data to show',
                          style: TextStyles.bankSubmissionCardTitleDisabled,
                        )
                      ],
                    )
                  : ListView.builder(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: bankSubmissionController
                          .bankSubmissionBanksListResponseModel
                          .value
                          .bankSubmissionOneBank!
                          .length,
                      itemBuilder: (context, index) {
                        return taskCard(
                            bankSubmissionController
                                .bankSubmissionBanksListResponseModel
                                .value
                                .bankSubmissionOneBank![index]
                                .bankDetails!
                                .bankName!,
                            bankSubmissionController.getBankTaskCounterString(
                              bankSubmissionController
                                  .bankSubmissionBanksListResponseModel
                                  .value
                                  .bankSubmissionOneBank![index]
                                  .emailToLeasTask!,
                              bankSubmissionController
                                  .bankSubmissionBanksListResponseModel
                                  .value
                                  .bankSubmissionOneBank![index]
                                  .emailToBankTask!,
                            ),
                            bankSubmissionController
                                .bankSubmissionBanksListResponseModel
                                .value
                                .bankSubmissionOneBank![index],
                            widget.data.id!,
                            widget.data.document_drawer_id!,
                            bankSubmissionController,
                            index,
                            widget.data.canEdit ?? false);
                      })
              : Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                        height: windowHeight * 0.15,
                        child: SvgPicture.asset('assets/images/nodata.svg')),
                    Text(
                      'No data to show',
                      style: TextStyles.bankSubmissionCardTitleDisabled,
                    )
                  ],
                )),
          bankSubmissionController.isSubmitted.value
              ? Container()
              : SizedBox(height: windowHeight * 0.01),
          SizedBox(height: windowHeight * 0.01),
        ],
      ),
    );
  }

  Widget taskCard(
      String title,
      String task,
      BankSubmissionOneBank bankSubmissionOneBank,
      int leadId,
      String documentDrawerId,
      BankSubmissionController bankSubmissionController,
      int index,
      bool canEdit) {
    return InkWell(
      onTap:
          // canEdit //If can edit is false then we cannot navigate to other page
          //     ?
          () {
        GetControllers.shared
                .getLeadProfileController()
                .checkIfPDPACompletedForAll()
            ? Get.toNamed(bankSubmissionBankTaskScreen, arguments: [
                bankSubmissionOneBank,
                leadId,
                index,
                documentDrawerId
              ])
            : Future.delayed(const Duration(milliseconds: 0), () {
                FocusManager.instance.primaryFocus!.unfocus();
                Fluttertoast.showToast(
                  timeInSecForIosWeb: 3,
                  msg: "Please verify PDPA for all applicants",
                  backgroundColor: Colors.red,
                  toastLength: Toast.LENGTH_LONG,
                  gravity: ToastGravity.BOTTOM,
                  textColor: Colors.white,
                  fontSize: 20.0,
                );
              });
        bankSubmissionController.isExpand.value = false;
      },
      // : null,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 5),
        child: Card(
          elevation: 5,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      title,
                      style: TextStyles.bankSubmissionCardTitle,
                    ),
                    SizedBox(height: windowHeight * 0.03),
                    task == '2/2'
                        ? Text(
                            task + ' Task completed',
                            style:
                                TextStyles.bankSubmissionCompletedTaskTextStyle,
                          )
                        : Text(
                            task + ' Task',
                            style: TextStyles.bankSubmissionTaskTextStyle,
                          ),
                  ],
                ),
                const Icon(
                  Icons.chevron_right,
                  color: Colors.black,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget expandableCard(String title, bool isDone) {
    return GestureDetector(
      onTap: () {
        bankSubmissionController.isExpand.toggle();
        GetControllers.shared.getLoanDetailsController().isExpand.value = false;
        GetControllers.shared.getPropertyDetailsController().isExpand.value =
            false;
        GetControllers.shared.getGeneratePackagesController().isExpand.value =
            false;
        GetControllers.shared.getSelectedPackagesController().isExpand.value =
            false;
        GetControllers.shared.getLeadProfileController().isExpand.value = false;
        GetControllers.shared
            .getSubmitForPayoutViewController()
            .isExpand
            .value = false;
        GetControllers.shared.getLawFirmSubmissionController().isExpand.value =
            false;
        GetControllers.shared.getSubmitCoBrokeController().isExpand.value =
            false;
        GetControllers.shared.getEmailLogController().isExpand.value = false;
        if (bankSubmissionController.isExpand.value == true) {
          bankSubmissionController.getBanks(widget.data.id.toString());
        }
      },
      child: Obx(() => Container(
            color: Colors.white,
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(bottom: windowWidth * 0.02),
                  padding: EdgeInsets.all(windowWidth * 0.05),
                  decoration: BoxDecoration(
                      boxShadow: const <BoxShadow>[
                        BoxShadow(
                            color: Colors.black54,
                            blurRadius: 5.0,
                            offset: Offset(0.0, 0.3))
                      ],
                      color: !leadProfileController.clientPDPAStatus.value
                          ? AppColors.greyDEDEDE
                          : Colors.white),
                  child: Row(
                    children: [
                      Text(title, style: TextStyles.basicTextStyle),
                      const Spacer(),
                      Obx(
                        () =>
                            bankSubmissionController.isBankSubmissionDone.value
                                ? SvgPicture.asset(
                                    Assets.manageLoanDone,
                                    height: 24,
                                    width: 24,
                                  )
                                : SvgPicture.asset(
                                    Assets.manageLoanIncomplete,
                                    height: 24,
                                    width: 24,
                                  ),
                      ),
                      SizedBox(width: windowWidth * 0.03),
                      Icon(
                        bankSubmissionController.isExpand.value
                            ? Icons.keyboard_arrow_up_rounded
                            : Icons.keyboard_arrow_down,
                        size: 30,
                      )
                    ],
                  ),
                ),
                bankSubmissionController.isExpand.value
                    ? banksTaskList()
                    : Container()
              ],
            ),
          )),
    );
  }
}

// ignore: must_be_immutable
class SelectedBankCards extends StatelessWidget {
  String? image;
  String headingText;
  String headingSubText;
  void Function(bool?)? onChanged;
  bool checkBoxValue;
  int cardIndex;

  SelectedBankCards(
      {Key? key,
      this.image,
      required this.headingText,
      required this.headingSubText,
      required this.onChanged,
      required this.checkBoxValue,
      required this.cardIndex})
      : super(key: key);

  final BankSubmissionController bankSubmissionController =
      Get.put(BankSubmissionController());

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15))),
      elevation: 5,
      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Column(
        children: [rowOne(), expansionSection(cardIndex)],
      ),
    );
  }

  // 1st row that shows the Bank name , image and a checkbox
  Widget rowOne() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.0),
            child: Icon(Icons.ac_unit),
          ),
          Expanded(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                Text(
                  headingText,
                  style: TextStyles.cardHeading,
                ),
                Text(
                  headingSubText,
                  style: TextStyles.cardSubHeading,
                )
              ])),
          Expanded(
              child: Container(
            decoration: BoxDecoration(border: Border.all(color: Colors.grey)),
            child: titleValueRow(title: 'Year 1', value: '1.00%'),
          )),
          Checkbox(
            value: checkBoxValue,
            onChanged: onChanged,
            checkColor: AppColors.kSecondaryColor,
            fillColor: MaterialStateProperty.all(Colors.white),
            side: const BorderSide(
                color: AppColors.kSecondaryColor,
                width: 2,
                style: BorderStyle.solid),
          ),
        ],
      ),
    );
  }

  // A simple widget used in the 2nd row, which only shows a title and a value
  Widget titleValueRow({required String title, required String value}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Text(
          title + ': ',
          style: TextStyles.headingSubtitleStyle,
        ),
        Text(
          value,
          style: TextStyles.headingSubtitleStyle,
        )
      ],
    );
  }

  // This sections expands to show additional details in the specific
  Widget expansionSection(int index) {
    return Obx(
      () => InkWell(
        onTap: () {
          bankSubmissionController.expandingCard(index);
        },
        child: ClipRRect(
          borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(15),
              bottomRight: Radius.circular(15)),
          child: Container(
            color: AppColors.kSecondaryColor,
            child: bankSubmissionController.selectedIndex.value == index
                ? Container(
                    color: Colors.white,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        InkWell(
                            onTap: () {
                              //  close the expansion pannel on tapping here
                              bankSubmissionController.reset();
                            },
                            child: const Padding(
                              padding: EdgeInsets.only(bottom: 10.0),
                              child: Icon(Icons.minimize),
                            )),
                        ListView(
                          shrinkWrap: true,
                          children: [
                            expandedSectionHeading(
                                'Rates', [const Text('Enjoy!')]),
                            expandedSectionHeading(
                                'Key Features', [const Text('Enjoy!')]),
                            expandedSectionHeading(
                                "Bank's Subsidy", [const Text('Enjoy!')]),
                            expandedSectionHeading('Early Repayment Penalty',
                                [const Text('Enjoy!')]),
                          ],
                        )
                      ],
                    ),
                  )
                : Center(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        'Details',
                        style: TextStyles.detailsButton,
                      ),
                    ),
                  ),
          ),
        ),
      ),
    );
  }

  // This section is displayed when the card is expanded and this expansion tile is shown in a list
  Widget expandedSectionHeading(String title, List<Widget> body) {
    return ExpansionTile(
      iconColor: Colors.white,
      backgroundColor: AppColors.kSecondaryColor,
      collapsedBackgroundColor: AppColors.kSecondaryColor,
      collapsedIconColor: Colors.white,
      leading: Text(
        title,
        style: TextStyles.detailsButton,
      ),
      title: Container(),
      children: body,
    );
  }
}

// ignore: must_be_immutable
class ConfirmedSelectedBankSection extends StatelessWidget {
  ConfirmedSelectedBankSection(
      {Key? key,
      required this.index,
      required this.bankName,
      required this.isDone,
      required this.windowHeight,
      required this.windowWidth})
      : super(key: key);

  int index;
  String bankName;
  bool isDone;
  double windowHeight;
  double windowWidth;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(10.0, 10, 10, 10),
      child: Column(
        children: [
          heading(),
          SizedBox(
            height: windowHeight * 0.01,
          ),
          body()
        ],
      ),
    );
  }

  Widget heading() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Selected Bank #${index + 1}',
              style: TextStyles.selectedBankTextTextStyles,
            ),
            Text(
              bankName,
              style: TextStyles.bankNameTextStyle,
            )
          ],
        ),
        const IconButton(onPressed: null, icon: Icon(Icons.delete))
      ],
    );
  }

  Widget body() {
    return Container(
      // padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.grey.withOpacity(0.2),
          border: Border.all(color: AppColors.kPrimaryColor)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 8, 0, 0),
            child: Text(
              'Loan Submission Status',
              style: TextStyles.selectedBankTextTextStyles,
            ),
          ),
          SizedBox(
            height: windowHeight * 0.01,
          ),
          iconAndtextRow('Email Lead'),
          iconAndtextRow('Verify Uploaded Document'),
          iconAndtextRow('Email Banker'),
          SizedBox(
            height: windowHeight * 0.01,
          ),
          Container(
            decoration: const BoxDecoration(
                color: AppColors.kPrimaryColor,
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10),
                    bottomRight: Radius.circular(10))),
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: Text(
                  'Manage Loan',
                  style: TextStyles.manageLoanText,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget iconAndtextRow(String title) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(10, 2, 0, 2),
      child: Row(
        children: [
          SvgPicture.asset(
            isDone ? Assets.manageLoanDone : Assets.manageLoanIncomplete,
            height: 24,
            width: 24,
          ),
          SizedBox(
            width: windowWidth * 0.04,
          ),
          Text(
            title,
            style: TextStyles.title,
          ),
        ],
      ),
    );
  }
}
