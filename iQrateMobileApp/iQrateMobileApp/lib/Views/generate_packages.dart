// ignore_for_file: must_be_immutable

import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/dashboard_controller.dart';
import 'package:iqrate/Controller/generate_packages_controller.dart';
import 'package:iqrate/Controller/lead_profile_controller.dart';
import 'package:iqrate/Controller/rates_view_controller.dart';
import 'package:iqrate/Controller/selected_packages_controller.dart';
import 'package:iqrate/DeviceManager/assets.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/container_styles.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Model/response_model.dart/fetch_lead_list_response_model.dart';
import 'package:iqrate/Model/response_model.dart/refinance_new_lead_package_response_model_v2.dart';
import 'package:iqrate/Model/response_model.dart/refinance_report_response_model_v2.dart';
import 'package:iqrate/Utils/config.dart';
import 'package:iqrate/Views/rates_view_details.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/secondary_button.dart';

import '../Service/GetControllers.dart';

class GeneratePackages extends StatefulWidget {
  final Result data;

  const GeneratePackages({Key? key, required this.data}) : super(key: key);

  @override
  _GeneratePackagesState createState() => _GeneratePackagesState();
}

class _GeneratePackagesState extends State<GeneratePackages> {
  late double windowHeight;
  late double windowWidth;

  GeneratePackagesController generatePackagesController =
      GetControllers.shared.getGeneratePackagesController();
  SelectedPackagesController selectedPackagesController =
      GetControllers.shared.getSelectedPackagesController();

  final DashboardController dashboardController = Get.find();
  final RatesViewController ratesViewController = Get.find();

  @override
  void initState() {
    initData();
    super.initState();
  }

  late Result leadData;

  initData() async {
    leadData = widget.data;
    await generatePackagesController.getLowestPackages(leadData.id.toString());

    if (leadProfileController.clientPDPAStatus.value) {
      generatePackagesController.checkGeneratedPackage(
          leadData.id.toString(), leadData);
    }
  }

  LeadProfileController leadProfileController =
      GetControllers.shared.getLeadProfileController();

  @override
  Widget build(BuildContext context) {
    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.width;
    return Obx(() => IgnorePointer(
        ignoring: !leadProfileController.clientPDPAStatus.value,
        child: expandableCard("Generate Packages", false)));
  }

  Widget packageList() {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(windowHeight * 0.025),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: const EdgeInsets.all(15),
            width: windowWidth,
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey),
                borderRadius: const BorderRadius.all(Radius.circular(10.0))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Our AI has generated the lowest 4 packages based on your requirements!",
                  style: TextStyles.dashboardPopUpTextStyle,
                ),
              ],
            ),
          ),
          SizedBox(
            height: windowHeight * 0.03,
          ),
          GestureDetector(
            onTap: () {
              //this is export icon???
              debugPrint("export generate rate");

              if (generatePackagesController
                  .selectedPackgesForGenerateckages.isEmpty) {
                Future.delayed(const Duration(milliseconds: 0), () {
                  FocusManager.instance.primaryFocus!.unfocus();
                  Fluttertoast.showToast(
                    timeInSecForIosWeb: 3,
                    msg: "Please select packages first",
                    backgroundColor: Colors.white,
                    toastLength: Toast.LENGTH_LONG,
                    gravity: ToastGravity.BOTTOM,
                    textColor: Colors.black,
                    fontSize: 20.0,
                  );
                });
                return;
              }

              generatePackagesController.callAPIExportGeneratePackage();

              //generatePackagesController.showFilterConditionalVariable.value = 2;
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                  "Export Selected Rates",
                  style: TextStyles.rateExportColorTextStyle,
                ),
                SizedBox(width: windowWidth * 0.025),
                SvgPicture.asset(
                  'assets/icons/share.svg',
                  color: AppColors.kPrimaryColor,
                ),
              ],
            ),
          ),
          SizedBox(
            height: windowHeight * 0.03,
          ),
          GeneratePackagesHeading(
            windowHeight: windowHeight,
            windowWidth: windowWidth,
            title: 'Lowest Fixed',
          ),
          SizedBox(
            height: windowHeight * 0.02,
          ),
          Obx(() => generatePackagesController.lowestPackageListModel.isNotEmpty
              ? ListView.builder(
                  itemCount:
                      generatePackagesController.lowestPackageListModel.length,
                  shrinkWrap: true,
                  primary: false,
                  // This count needs to be dynamic
                  itemBuilder: (BuildContext context, int index) {
                    var item = generatePackagesController.lowestPackageListModel
                        .elementAt(index);

                    bool isSelected =
                        generatePackagesController.selectedPackages(item.id!);

                    item.isChecked = isSelected;

                    return LoanPackageCardFixed(
                      isFromMoreRate: 1,
                      data: item,
                      image: item.bank!.logo == null
                          ? AppConfig.blankImage
                          : item.bank!.logo!,
                      headingSubText:
                          '${item.rateType.toString().replaceAll('_', ' ').capitalize} (${item.rateCategory.toString().replaceAll('_', ' ').capitalize})',
                      headingText: item.bank!.name!,
                      onChanged: (val) {
                        // Function for lowest fixed checkboxes
                        if (val == true) {
                          /*if (generatePackagesController
                                  .selectedPackgesForGenerateckages.length >=
                              3) {
                            Get.snackbar(StringUtils.attention,
                                "Maximum 3 packages can be selected",
                                backgroundColor: Colors.white,
                                colorText: Colors.black);
                          } else {*/
                          setState(() {
                            item.isChecked = val!;
                          });
                          generatePackagesController
                              .selectedPackgesForGenerateckages
                              .add(item.id!);
                          //}
                        } else {
                          setState(() {
                            item.isChecked = val!;
                          });
                          generatePackagesController
                              .selectedPackgesForGenerateckages
                              .removeWhere(((element) => element == item.id));
                        }
                      },
                      checkBoxValue: isSelected,
                      cardIndex: index,
                    );
                  })
              : Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                          height: windowHeight * 0.15,
                          child: SvgPicture.asset('assets/images/nodata.svg')),
                      Text(
                        'No data to show',
                        style: TextStyles.bankSubmissionCardTitleDisabled,
                      )
                    ],
                  ),
                )),
          SizedBox(
            height: windowHeight * 0.03,
          ),
          GeneratePackagesHeading(
            windowHeight: windowHeight,
            windowWidth: windowWidth,
            title: 'Lowest Floating',
          ),
          SizedBox(
            height: windowHeight * 0.02,
          ),
          Obx(() => generatePackagesController
                  .floatingPackageListModel.isNotEmpty
              ? ListView.builder(
                  itemCount: generatePackagesController
                      .floatingPackageListModel.length,
                  shrinkWrap: true,
                  primary: false,
                  // This count needs to be dynamic
                  itemBuilder: (BuildContext context, int index) {
                    var item = generatePackagesController
                        .floatingPackageListModel
                        .elementAt(index);

                    bool isSelected =
                        generatePackagesController.selectedPackages(item.id!);
                    item.isChecked = isSelected;

                    return LoanPackageCardFixed(
                      isFromMoreRate: 2,
                      data: item,
                      image: item.bank!.logo == null
                          ? AppConfig.blankImage
                          : item.bank!.logo!,
                      headingSubText:
                          '${item.rateType.toString().replaceAll('_', ' ').capitalize} (${item.rateCategory.toString().replaceAll('_', ' ').capitalize})',
                      headingText: item.bank!.name!,
                      onChanged: (val) {
                        // Function for lowest floating checkboxes
                        if (val == true) {
                          /*if (generatePackagesController
                                  .selectedPackgesForGenerateckages.length >=
                              3) {
                            Get.snackbar(StringUtils.attention,
                                "Maximum 3 packages can be selected",
                                backgroundColor: Colors.white,
                                colorText: Colors.black);
                          } else {*/
                          setState(() {
                            item.isChecked = val!;
                          });
                          generatePackagesController
                              .selectedPackgesForGenerateckages
                              .add(item.id!);
                          // }
                        } else {
                          setState(() {
                            item.isChecked = val!;
                          });
                          generatePackagesController
                              .selectedPackgesForGenerateckages
                              .removeWhere(((element) => element == item.id));
                        }
                      },
                      checkBoxValue: isSelected,
                      cardIndex: index,
                    );
                  })
              : Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                          height: windowHeight * 0.15,
                          child: SvgPicture.asset('assets/images/nodata.svg')),
                      Text(
                        'No data to show',
                        style: TextStyles.bankSubmissionCardTitleDisabled,
                      )
                    ],
                  ),
                )),
          SizedBox(height: windowHeight * 0.02),
          // widget.data.leadType == 'yourself'
          //     ? Container()
          //     :
          // widget.data.canEdit ??
          //         false //Condition to hide widget if canEdit is false
          //     ?
          widget.data.coBrokeStatus!.trim() == 'Co-Broke Sent'
              ? Container()
              : Center(
                  child: PrimaryButton(
                    windowHeight: windowHeight,
                    windowWidth: windowWidth,
                    buttonTitle: "Proceed",
                    onPressed: () {
                      generatePackagesController.generatePackage(
                          leadData.id.toString(), true);
                    },
                  ),
                ),
          // : Container(),
          // widget.data.leadType == 'yourself'
          //     ? Container()
          //     :
          // widget.data.canEdit ??
          //         false //Condition to hide widget if canEdit is false
          //     ?
          widget.data.coBrokeStatus!.trim() == 'Co-Broke Sent'
              ? Container()
              : SizedBox(height: windowHeight * 0.02),
          // : Container(),
          // widget.data.leadType == 'yourself'
          //     ? Container()
          //     :
          // widget.data.canEdit ??
          //         false //Condition to hide widget if canEdit is false
          //     ?
          widget.data.coBrokeStatus!.trim() == 'Co-Broke Sent'
              ? Container()
              : Center(
                  child: SecondaryButton(
                    windowHeight: windowHeight,
                    windowWidth: windowWidth,
                    kGradientBoxDecoration:
                        ContainerStyles.kGradientBoxDecorationSecondaryButton,
                    kInnerDecoration:
                        ContainerStyles.kInnerDecorationSecondaryButton,
                    buttonTitle: 'See More Rates',
                    onPressed: () {
                      // generatePackagesController
                      //             .selectedPackgesForGenerateckages
                      //             .length >=
                      //         3
                      //     ? Get.snackbar(StringUtils.attention,
                      //         "Maximum 3 packages can be selected",
                      //         backgroundColor: Colors.white,
                      //         colorText: Colors.black)
                      //     :
                      ratesViewController.getMoreRates(leadData.id.toString());
                    },
                  ),
                )
          // : Container(),
        ],
      ),
    );
  }

  Widget expandableCard(String title, bool isDone) {
    return GestureDetector(
      onTap: () {
        generatePackagesController.isExpand.toggle();
        generatePackagesController.getLowestPackages(leadData.id.toString());
        GetControllers.shared.getSelectedPackagesController().isExpand.value =
            false;
        GetControllers.shared.getLoanDetailsController().isExpand.value = false;
        GetControllers.shared.getPropertyDetailsController().isExpand.value =
            false;
        GetControllers.shared.getLeadProfileController().isExpand.value = false;
        GetControllers.shared.getBankSubmissionController().isExpand.value =
            false;
        GetControllers.shared
            .getSubmitForPayoutViewController()
            .isExpand
            .value = false;
        GetControllers.shared.getLawFirmSubmissionController().isExpand.value =
            false;
        GetControllers.shared.getEmailLogController().isExpand.value = false;
        GetControllers.shared.getSubmitCoBrokeController().isExpand.value =
            false;

        //generatePackagesController.selectedPackages();
      },
      child: Obx(
        () => Container(
          color: Colors.white,
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(bottom: windowWidth * 0.02),
                padding: EdgeInsets.all(windowWidth * 0.05),
                decoration: BoxDecoration(
                    boxShadow: const <BoxShadow>[
                      BoxShadow(
                          color: Colors.black54,
                          blurRadius: 5.0,
                          offset: Offset(0.0, 0.3))
                    ],
                    color: !leadProfileController.clientPDPAStatus.value
                        ? AppColors.greyDEDEDE
                        : Colors.white),
                child: Row(
                  children: [
                    Text(title, style: TextStyles.basicTextStyle),
                    const Spacer(),
                    Obx(() {
                      return SvgPicture.asset(
                        leadData.coBrokeStatus!.trim() == 'Co-Broke Sent'
                            ? (generatePackagesController
                                        .lowestPackageListModel.length
                                        .isEqual(0) ==
                                    false)
                                ? Assets.manageLoanDone
                                : Assets.manageLoanIncomplete
                            : leadData.leadType != 'yourself'
                                ? (generatePackagesController
                                            .isGeneratedPackage.value) ||
                                        (generatePackagesController
                                                    .lowestPackageListModel
                                                    .length
                                                    .isEqual(0) ==
                                                false &&
                                            selectedPackagesController
                                                    .selectedPackageListModel
                                                    .length
                                                    .isEqual(0) ==
                                                false)
                                    ? Assets.manageLoanDone
                                    : Assets.manageLoanIncomplete
                                : generatePackagesController
                                        .lowestPackageListModel.length
                                        .isEqual(0)
                                    ? Assets.manageLoanIncomplete
                                    : Assets.manageLoanDone,
                        height: 24,
                        width: 24,
                      );
                    }),
                    SizedBox(width: windowWidth * 0.03),
                    Icon(
                      generatePackagesController.isExpand.value
                          ? Icons.keyboard_arrow_up_rounded
                          : Icons.keyboard_arrow_down,
                      size: 30,
                    )
                  ],
                ),
              ),
              generatePackagesController.isExpand.value
                  ? packageList()
                  : Container()
            ],
          ),
        ),
      ),
    );
  }
}

class GeneratePackagesHeading extends StatelessWidget {
  const GeneratePackagesHeading({
    Key? key,
    required this.windowHeight,
    required this.windowWidth,
    required this.title,
  }) : super(key: key);

  final double windowHeight;
  final double windowWidth;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: windowHeight * 0.03,
          padding: EdgeInsets.only(left: windowWidth * 0.01),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                title,
                style: TextStyles.basicTextStyle,
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: windowWidth * 0.01),
          child: Divider(
            thickness: 1,
            color: AppColors.leadListExpansionBG,
          ),
        ),
      ],
    );
  }
}

class LoanPackageCard extends StatelessWidget {
  String? image;
  String headingText;
  String headingSubText;
  void Function(bool?)? onChanged;
  void Function(String?)? onTapDelete;
  bool checkBoxValue;
  bool isCheckboxShow;
  int cardIndex;
  final Fixed data;
  final int isFromMoreRate;

  LoanPackageCard(
      {Key? key,
      this.image,
      required this.headingText,
      required this.headingSubText,
      required this.onChanged,
      this.onTapDelete,
      required this.checkBoxValue,
      required this.cardIndex,
      required this.data,
      this.isCheckboxShow = true,
      this.isFromMoreRate = 0})
      : super(key: key);

  final RatesViewController ratesViewController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15))),
      elevation: 5,
      margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      child: Column(
        children: [rowOne(), rowTwo(), expansionSection(cardIndex)],
      ),
    );
  }

  // 1st row that shows the Bank name , image and a checkbox
  Widget rowOne() {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(30)),
                color: Colors.deepOrangeAccent.withOpacity(.3)),
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: image!.isURL
                  ? Image.network(
                      image!,
                      width: 100,
                      height: 50,
                    )
                  : const SizedBox(width: 100, height: 50),
            ),
          ),
          Expanded(
              child: Padding(
            padding: const EdgeInsets.only(left: 15.0),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text(
                headingText,
                style: TextStyles.cardHeading,
              ),
              const SizedBox(height: 10),
              Text(
                headingSubText,
                style: TextStyles.cardSubHeading,
              )
            ]),
          )),
          isCheckboxShow
              ? Transform.scale(
                  scale: 1.3,
                  child: Checkbox(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    value: data.isChecked,
                    onChanged: onChanged,
                    //checkColor: AppColors.kSecondaryColor,
                    //fillColor: MaterialStateProperty.all(Colors.white),
                    hoverColor: AppColors.kSecondaryColor,
                    activeColor: AppColors.kSecondaryColor,
                    side: const BorderSide(
                        color: AppColors.kSecondaryColor,
                        width: 1,
                        style: BorderStyle.solid),
                  ),
                )
              : InkWell(
                  onTap: () {
                    if (onTapDelete != null) {
                      onTapDelete!(data.id.toString());
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SvgPicture.asset(
                      "assets/images/card_delete.svg",
                      color: Colors.black54,
                    ),
                  )),
        ],
      ),
    );
  }

  // 2nd row that shows the percentage on year based
  Widget rowTwo() {
    var rates = data.rates![0].year.toString() != 'year_1'
        ? data.rates!.reversed
        : data.rates;
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 5),
      child: Row(
        children: [
          Expanded(
            child: Center(
              child: SizedBox(
                height: 20.0,
                child: ListView.builder(
                  physics: const ClampingScrollPhysics(),
                  shrinkWrap: true,
                  primary: false,
                  scrollDirection: Axis.horizontal,
                  itemCount: rates!.length > 3 ? 3 : rates.length,
                  itemBuilder: (BuildContext context, int index) => Row(
                    children: [
                      const SizedBox(
                        width: 10,
                      ),
                      titleValueRow(
                          title: rates
                                  .elementAt(index)
                                  .year
                                  .toString()
                                  .replaceAll("_", " ")
                                  .capitalizeFirst! +
                              " ",
                          value: rates
                                  .elementAt(index)
                                  .totalInterestRate
                                  .toString() +
                              "%"),
                      const SizedBox(
                        width: 10,
                      ),
                      index < 2
                          ? const VerticalDivider(
                              color: Colors.black,
                            )
                          : const SizedBox(),
                    ],
                  ),
                ),
              ),
            ),
          ),
          /* Expanded(
            child: ListView.builder(
              shrinkWrap: true,
              primary: false,
              scrollDirection: Axis.horizontal,
              itemCount: 2,
              itemBuilder: (BuildContext context, int index) =>  Row(
                children: [
                  titleValueRow(title: 'Year 1', value: data.rates!.elementAt(0).referenceRate.toString()),
                  const VerticalDivider(
                    color: Colors.black,
                  ),
                ],
              ),

            ),
          ),*/

          /*Expanded(child: titleValueRow(title: 'Year 2', value: '1.000%')),
          const VerticalDivider(
            color: Colors.black,
          ),
          Expanded(child: titleValueRow(title: 'Year 3', value: '1.000%')),*/
        ],
      ),
    );
  }

  // A simple widget used in the 2nd row, which only shows a title and a value
  Widget titleValueRow({required String title, required String value}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          title + ': ',
          style: TextStyles.headingSubtitleStyle,
        ),
        Text(
          value,
          style: TextStyles.headingSubtitleStyle,
        )
      ],
    );
  }

  // This sections expands to show additional details in the specific
  Widget expansionSection(int index) {
    return InkWell(
      onTap: () {
        Get.to(RatesViewDetails(
          data: data,
          isFromMoreRate: isFromMoreRate,
          isCheckboxShow: isCheckboxShow,
        ));
        //ratesViewController.expandingCard(index);
      },
      child: ClipRRect(
        borderRadius: const BorderRadius.only(
            bottomLeft: Radius.circular(15), bottomRight: Radius.circular(15)),
        child: Container(
          color: AppColors.kSecondaryColor,
          child: /*ratesViewController.selectedIndex.value == index
                ? Container(
                    color: Colors.white,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        InkWell(
                            onTap: () {
                              //  close the expansion pannel on tapping here
                              ratesViewController.reset();
                            },
                            child: const Padding(
                              padding: EdgeInsets.only(bottom: 10.0),
                              child: Icon(Icons.minimize),
                            )),
                        ListView(
                          shrinkWrap: true,
                          children: [
                            expandedSectionHeading(
                                'Rates', [const Text('Enjoy!')]),
                            expandedSectionHeading(
                                'Key Features', [const Text('Enjoy!')]),
                            expandedSectionHeading(
                                "Bank's Subsidy", [const Text('Enjoy!')]),
                            expandedSectionHeading('Early Repayment Penalty',
                                [const Text('Enjoy!')]),
                          ],
                        )
                      ],
                    ),
                  )
                :*/
              Center(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Details',
                style: TextStyles.detailsButton,
              ),
            ),
          ),
        ),
      ),
    );
  }

// This section is displayed when the card is expanded and this expansion tile is shown in a list
// Widget expandedSectionHeading(String title, List<Widget> body) {
//   return ExpansionTile(
//     iconColor: Colors.white,
//     backgroundColor: AppColors.kSecondaryColor,
//     collapsedBackgroundColor: AppColors.kSecondaryColor,
//     collapsedIconColor: Colors.white,
//     leading: Text(
//       title,
//       style: TextStyles.detailsButton,
//     ),
//     title: Container(),
//     children: body,
//   );
// }
}

class LoanPackageCardFixed extends StatelessWidget {
  String? image;
  String headingText;
  String headingSubText;
  void Function(bool?)? onChanged;
  void Function(String?)? onTapDelete;
  bool checkBoxValue;
  bool isCheckboxShow;
  int cardIndex;
  final Fixed data;
  final int isFromMoreRate;
  bool isExpired;
  bool showDeleteIcon;

  LoanPackageCardFixed(
      {Key? key,
      this.image,
      required this.headingText,
      required this.headingSubText,
      required this.onChanged,
      this.onTapDelete,
      required this.checkBoxValue,
      required this.cardIndex,
      required this.data,
      this.isCheckboxShow = true,
      this.isFromMoreRate = 0,
      this.isExpired = false,
      this.showDeleteIcon = true})
      : super(key: key);

  final RatesViewController ratesViewController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Badge(
      elevation: 5,
      badgeColor: const Color(0xffffcccc),
      badgeContent: const Text(
        'Expired',
        style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
      ),
      toAnimate: false,
      shape: BadgeShape.square,
      showBadge: isExpired,
      borderRadius: BorderRadius.circular(10),
      child: Card(
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(15))),
        elevation: 5,
        margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: Column(
          children: [rowOne(), rowTwo(), expansionSection(cardIndex)],
        ),
      ),
    );
  }

  // 1st row that shows the Bank name , image and a checkbox
  Widget rowOne() {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          image!.isURL
              ? Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Image.network(
                    image!,
                    width: 100,
                    height: 50,
                  ),
                )
              : const SizedBox(width: 100, height: 50),
          Expanded(
              child: Padding(
            padding: const EdgeInsets.only(left: 15.0),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text(
                headingText,
                style: TextStyles.cardHeading,
              ),
              const SizedBox(height: 10),
              Text(
                headingSubText,
                style: TextStyles.cardSubHeading,
              )
            ]),
          )),
          isCheckboxShow
              ? Transform.scale(
                  scale: 1.3,
                  child: Checkbox(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    value: data.isChecked,
                    onChanged: onChanged,
                    //checkColor: AppColors.kSecondaryColor,
                    //fillColor: MaterialStateProperty.all(Colors.white),
                    hoverColor: AppColors.kSecondaryColor,
                    activeColor: AppColors.kSecondaryColor,
                    side: const BorderSide(
                        color: AppColors.kSecondaryColor,
                        width: 1,
                        style: BorderStyle.solid),
                  ),
                )
              : InkWell(
                  onTap: () {
                    if (onTapDelete != null) {
                      onTapDelete!(data.id.toString());
                    }
                  },
                  child: showDeleteIcon
                      ? Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: SvgPicture.asset(
                            "assets/images/card_delete.svg",
                            color: Colors.black54,
                          ),
                        )
                      : const Offstage()),
        ],
      ),
    );
  }

  // 2nd row that shows the percentage on year based
  Widget rowTwo() {
    var rates = data.rates![0].year.toString() != 'year_1'
        ? data.rates!.reversed
        : data.rates;
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 5),
      child: Row(
        children: [
          Expanded(
            child: Center(
              child: SizedBox(
                height: 20.0,
                child: ListView.builder(
                  physics: const ClampingScrollPhysics(),
                  shrinkWrap: true,
                  primary: false,
                  scrollDirection: Axis.horizontal,
                  itemCount: rates!.length > 3 ? 3 : rates.length,
                  itemBuilder: (BuildContext context, int index) => Row(
                    children: [
                      const SizedBox(
                        width: 10,
                      ),
                      titleValueRow(
                          title: rates
                                  .elementAt(index)
                                  .year
                                  .toString()
                                  .replaceAll("_", " ")
                                  .capitalize! +
                              " ",
                          value: rates
                                  .elementAt(index)
                                  .totalInterestRate
                                  .toString() +
                              "%"),
                      const SizedBox(
                        width: 10,
                      ),
                      index < 2
                          ? const VerticalDivider(
                              color: Colors.black,
                            )
                          : const SizedBox(),
                    ],
                  ),
                ),
              ),
            ),
          ),
          /* Expanded(
            child: ListView.builder(
              shrinkWrap: true,
              primary: false,
              scrollDirection: Axis.horizontal,
              itemCount: 2,
              itemBuilder: (BuildContext context, int index) =>  Row(
                children: [
                  titleValueRow(title: 'Year 1', value: data.rates!.elementAt(0).referenceRate.toString()),
                  const VerticalDivider(
                    color: Colors.black,
                  ),
                ],
              ),

            ),
          ),*/

          /*Expanded(child: titleValueRow(title: 'Year 2', value: '1.000%')),
          const VerticalDivider(
            color: Colors.black,
          ),
          Expanded(child: titleValueRow(title: 'Year 3', value: '1.000%')),*/
        ],
      ),
    );
  }

  // A simple widget used in the 2nd row, which only shows a title and a value
  Widget titleValueRow({required String title, required String value}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          title + ': ',
          style: TextStyles.headingSubtitleStyle,
        ),
        Text(
          value,
          style: TextStyles.headingSubtitleStyle,
        )
      ],
    );
  }

  // This sections expands to show additional details in the specific
  Widget expansionSection(int index) {
    return InkWell(
      onTap: () {
        Get.to(RatesViewDetails(
          data: data,
          isFromMoreRate: isFromMoreRate,
          isCheckboxShow: isCheckboxShow,
        ));
        //ratesViewController.expandingCard(index);
      },
      child: ClipRRect(
        borderRadius: const BorderRadius.only(
            bottomLeft: Radius.circular(15), bottomRight: Radius.circular(15)),
        child: Container(
          color: AppColors.kSecondaryColor,
          child: /*ratesViewController.selectedIndex.value == index
                ? Container(
                    color: Colors.white,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        InkWell(
                            onTap: () {
                              //  close the expansion pannel on tapping here
                              ratesViewController.reset();
                            },
                            child: const Padding(
                              padding: EdgeInsets.only(bottom: 10.0),
                              child: Icon(Icons.minimize),
                            )),
                        ListView(
                          shrinkWrap: true,
                          children: [
                            expandedSectionHeading(
                                'Rates', [const Text('Enjoy!')]),
                            expandedSectionHeading(
                                'Key Features', [const Text('Enjoy!')]),
                            expandedSectionHeading(
                                "Bank's Subsidy", [const Text('Enjoy!')]),
                            expandedSectionHeading('Early Repayment Penalty',
                                [const Text('Enjoy!')]),
                          ],
                        )
                      ],
                    ),
                  )
                :*/
              Center(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Details',
                style: TextStyles.detailsButton,
              ),
            ),
          ),
        ),
      ),
    );
  }

// This section is displayed when the card is expanded and this expansion tile is shown in a list
// Widget expandedSectionHeading(String title, List<Widget> body) {
//   return ExpansionTile(
//     iconColor: Colors.white,
//     backgroundColor: AppColors.kSecondaryColor,
//     collapsedBackgroundColor: AppColors.kSecondaryColor,
//     collapsedIconColor: Colors.white,
//     leading: Text(
//       title,
//       style: TextStyles.detailsButton,
//     ),
//     title: Container(),
//     children: body,
//   );
// }
}

class LoanPackageCardForRefinance extends StatelessWidget {
  String? image;
  String headingText;
  String headingSubText;
  void Function(bool?)? onChanged;
  void Function(String?)? onTapDelete;
  bool checkBoxValue;
  bool isCheckboxShow;
  int cardIndex;
  final SelectedPackageDetails data;
  final int isFromMoreRate;

  LoanPackageCardForRefinance(
      {Key? key,
      this.image,
      required this.headingText,
      required this.headingSubText,
      required this.onChanged,
      this.onTapDelete,
      required this.checkBoxValue,
      required this.cardIndex,
      required this.data,
      this.isCheckboxShow = true,
      this.isFromMoreRate = 0})
      : super(key: key);

  final RatesViewController ratesViewController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15))),
      elevation: 5,
      margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      child: Column(
        children: [rowOne(), rowTwo(), expansionSection(cardIndex)],
      ),
    );
  }

  // 1st row that shows the Bank name , image and a checkbox
  Widget rowOne() {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: image!.isURL
                ? Image.network(
                    image!,
                    width: 100,
                    height: 50,
                  )
                : const SizedBox(width: 100, height: 50),
          ),
          Expanded(
              child: Padding(
            padding: const EdgeInsets.only(left: 15.0),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text(
                headingText,
                style: TextStyles.cardHeading,
              ),
              const SizedBox(height: 10),
              Text(
                headingSubText,
                style: TextStyles.cardSubHeading,
              )
            ]),
          )),
          isCheckboxShow
              ? Transform.scale(
                  scale: 1.3,
                  child: Checkbox(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    value: false,
                    onChanged: onChanged,
                    //checkColor: AppColors.kSecondaryColor,
                    //fillColor: MaterialStateProperty.all(Colors.white),
                    hoverColor: AppColors.kSecondaryColor,
                    activeColor: AppColors.kSecondaryColor,
                    side: const BorderSide(
                        color: AppColors.kSecondaryColor,
                        width: 1,
                        style: BorderStyle.solid),
                  ),
                )
              : const SizedBox()
        ],
      ),
    );
  }

  // 2nd row that shows the percentage on year based
  Widget rowTwo() {
    var rates = data.rates![0].year.toString() != 'year_1'
        ? data.rates!.reversed
        : data.rates;
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 5),
      child: Row(
        children: [
          Expanded(
            child: Center(
              child: SizedBox(
                height: 20.0,
                child: ListView.builder(
                  physics: const ClampingScrollPhysics(),
                  shrinkWrap: true,
                  primary: false,
                  scrollDirection: Axis.horizontal,
                  itemCount: rates!.length > 3 ? 3 : rates.length,
                  itemBuilder: (BuildContext context, int index) => Row(
                    children: [
                      const SizedBox(
                        width: 10,
                      ),
                      titleValueRow(
                          title: rates
                                  .elementAt(index)
                                  .year
                                  .toString()
                                  .replaceAll("_", " ")
                                  .capitalizeFirst! +
                              " ",
                          value: rates
                                  .elementAt(index)
                                  .totalInterestRate
                                  .toString() +
                              "%"),
                      const SizedBox(
                        width: 10,
                      ),
                      index < 2
                          ? const VerticalDivider(
                              color: Colors.black,
                            )
                          : const SizedBox(),
                    ],
                  ),
                ),
              ),
            ),
          ),
          /* Expanded(
            child: ListView.builder(
              shrinkWrap: true,
              primary: false,
              scrollDirection: Axis.horizontal,
              itemCount: 2,
              itemBuilder: (BuildContext context, int index) =>  Row(
                children: [
                  titleValueRow(title: 'Year 1', value: data.rates!.elementAt(0).referenceRate.toString()),
                  const VerticalDivider(
                    color: Colors.black,
                  ),
                ],
              ),

            ),
          ),*/

          /*Expanded(child: titleValueRow(title: 'Year 2', value: '1.000%')),
          const VerticalDivider(
            color: Colors.black,
          ),
          Expanded(child: titleValueRow(title: 'Year 3', value: '1.000%')),*/
        ],
      ),
    );
  }

  // A simple widget used in the 2nd row, which only shows a title and a value
  Widget titleValueRow({required String title, required String value}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          title + ': ',
          style: TextStyles.headingSubtitleStyle,
        ),
        Text(
          value,
          style: TextStyles.headingSubtitleStyle,
        )
      ],
    );
  }

  // This sections expands to show additional details in the specific
  Widget expansionSection(int index) {
    return InkWell(
      onTap: () {
        Get.to(RatesViewDetailsRefinance(
          data: data,
          isFromMoreRate: isFromMoreRate,
        ));
        //ratesViewController.expandingCard(index);
      },
      child: ClipRRect(
        borderRadius: const BorderRadius.only(
            bottomLeft: Radius.circular(15), bottomRight: Radius.circular(15)),
        child: Container(
          color: AppColors.kSecondaryColor,
          child: /*ratesViewController.selectedIndex.value == index
                ? Container(
                    color: Colors.white,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        InkWell(
                            onTap: () {
                              //  close the expansion pannel on tapping here
                              ratesViewController.reset();
                            },
                            child: const Padding(
                              padding: EdgeInsets.only(bottom: 10.0),
                              child: Icon(Icons.minimize),
                            )),
                        ListView(
                          shrinkWrap: true,
                          children: [
                            expandedSectionHeading(
                                'Rates', [const Text('Enjoy!')]),
                            expandedSectionHeading(
                                'Key Features', [const Text('Enjoy!')]),
                            expandedSectionHeading(
                                "Bank's Subsidy", [const Text('Enjoy!')]),
                            expandedSectionHeading('Early Repayment Penalty',
                                [const Text('Enjoy!')]),
                          ],
                        )
                      ],
                    ),
                  )
                :*/
              Center(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Details',
                style: TextStyles.detailsButton,
              ),
            ),
          ),
        ),
      ),
    );
  }

// This section is displayed when the card is expanded and this expansion tile is shown in a list
// Widget expandedSectionHeading(String title, List<Widget> body) {
//   return ExpansionTile(
//     iconColor: Colors.white,
//     backgroundColor: AppColors.kSecondaryColor,
//     collapsedBackgroundColor: AppColors.kSecondaryColor,
//     collapsedIconColor: Colors.white,
//     leading: Text(
//       title,
//       style: TextStyles.detailsButton,
//     ),
//     title: Container(),
//     children: body,
//   );
// }
}
