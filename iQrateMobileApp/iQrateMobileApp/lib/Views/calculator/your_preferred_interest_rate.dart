// ignore_for_file: must_be_immutable

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Service/GetControllers.dart';
import 'package:iqrate/Widgets/form_field_title.dart';

import '../../Widgets/require_text_field.dart';

class YourPreferredInterestRateWidget extends StatelessWidget {
  final int index;
  final Function(int) removeMe;
  final bool showRemoveIcon;
  final bool showIndexOrder;
  YourPreferredInterestRateWidget(
      {Key? key,
      required this.index,
      required this.removeMe,
      this.showRemoveIcon = true,
      this.showIndexOrder = true})
      : super(key: key);
  var year1TextController = TextEditingController();
  var year2TextController = TextEditingController();
  var year3TextController = TextEditingController();
  var year4TextController = TextEditingController();
  var year5TextController = TextEditingController();
  var formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    if (kDebugMode) {
      year1TextController.text = "1";
      year2TextController.text = "1";
      year3TextController.text = "1";
      year4TextController.text = "1";
      year5TextController.text = "1";
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text(
                'Your Preferred Interest Rate ${showIndexOrder ? index + 1 : ""}',
                style: TextStyles.calculatorHeadingsTextStyle),
            const Spacer(),
            showRemoveIcon
                ? IconButton(
                    onPressed: () {
                      removeMe(index);
                    },
                    icon: SvgPicture.asset(
                      'assets/icons/closeEnclosed.svg',
                      color: Colors.black,
                    ),
                  )
                : const SizedBox()
          ],
        ),
        const SizedBox(
          height: 20,
        ),
        const FormFieldTitle(title: 'Year 1*'),
        const SizedBox(
          height: 10,
        ),
        RequireTextField(
          labelText: "Year 1",
          validator: GetControllers.shared.yearRateRequired,
          type: Type.percentage,
          controller: year1TextController,
        ),
        const SizedBox(
          height: 10,
        ),
        const FormFieldTitle(title: 'Year 2*'),
        const SizedBox(
          height: 10,
        ),
        RequireTextField(
          labelText: "Year 2",
          validator: GetControllers.shared.yearRateRequired,
          type: Type.percentage,
          controller: year2TextController,
        ),
        const SizedBox(
          height: 10,
        ),
        const FormFieldTitle(title: 'Year 3*'),
        const SizedBox(
          height: 10,
        ),
        RequireTextField(
          labelText: "Year 3",
          validator: GetControllers.shared.yearRateRequired,
          type: Type.percentage,
          controller: year3TextController,
        ),
        const SizedBox(
          height: 10,
        ),
        const FormFieldTitle(title: 'Year 4*'),
        const SizedBox(
          height: 10,
        ),
        RequireTextField(
          labelText: "Year 4",
          validator: GetControllers.shared.yearRateRequired,
          type: Type.percentage,
          controller: year4TextController,
        ),
        const SizedBox(
          height: 10,
        ),
        const FormFieldTitle(title: 'Year 5 & Thereafter*'),
        const SizedBox(
          height: 10,
        ),
        RequireTextField(
          labelText: "Year 5 & Thereafter",
          validator: GetControllers.shared.yearRateRequired,
          type: Type.percentage,
          controller: year5TextController,
        ),
      ],
    );
  }
}
