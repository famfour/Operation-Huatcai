import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/two_fact_auth_question_controller.dart';
import 'package:iqrate/DeviceManager/container_styles.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/secondary_button.dart';

class TwoFactAuthQuestion extends StatelessWidget {
  TwoFactAuthQuestion({Key? key}) : super(key: key);

  final TwoFactAuthQuestionController twoFactAuthQuestionController =
      Get.put(TwoFactAuthQuestionController());

  @override
  Widget build(BuildContext context) {
    double windowWidth = MediaQuery.of(context).size.width;
    double windowHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SafeArea(
          child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 25.0, horizontal: 18),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: windowHeight * 0.06,
              ),
              Center(
                child: SvgPicture.asset(
                  "assets/images/two_fact.svg",
                ),
              ),
              SizedBox(
                height: windowHeight * 0.15,
              ),
              Text("Last thing -2FA Authentication",
                  style: TextStyles.introScreenTitlesSmall),
              SizedBox(
                height: windowHeight * 0.03,
              ),
              Text(
                "Enable 2 Factor verification for an enhanced Security and additional layer of security to your account",
                style: TextStyles.introScreenDescriptions,
              ),
              SizedBox(
                height: windowHeight * 0.2,
              ),
              PrimaryButton(
                windowHeight: windowHeight,
                windowWidth: windowWidth,
                buttonTitle: "Enable 2FA",
                onPressed: () {
                  twoFactAuthQuestionController.onSubmit();
                },
              ),
              SizedBox(
                height: windowHeight * 0.03,
              ),
              SecondaryButton(
                windowHeight: windowHeight,
                windowWidth: windowWidth,
                kGradientBoxDecoration:
                    ContainerStyles.kGradientBoxDecorationSecondaryButton,
                kInnerDecoration:
                    ContainerStyles.kInnerDecorationSecondaryButton,
                onPressed: () {
                  twoFactAuthQuestionController.onCancel();
                },
                buttonTitle: "No Thanks",
              ),
            ],
          ),
        ),
      )),
    );
  }
}
