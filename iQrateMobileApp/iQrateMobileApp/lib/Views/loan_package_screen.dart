import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/loan_package_controller.dart';
import 'package:iqrate/DeviceManager/container_styles.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Widgets/dashboard_appbar_options3.dart';
import 'package:iqrate/Widgets/primary_button.dart';

import '../Widgets/secondary_button.dart';

class LoanPackageScreen extends StatefulWidget {
  const LoanPackageScreen({Key? key}) : super(key: key);

  @override
  _LoanPackageScreenState createState() => _LoanPackageScreenState();
}

class _LoanPackageScreenState extends State<LoanPackageScreen> {
  LoanPackageController loanPackageController =
      Get.put(LoanPackageController());

  @override
  Widget build(BuildContext context) {
    double windowWidth = MediaQuery.of(context).size.width;
    double windowHeight = MediaQuery.of(context).size.height;

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          automaticallyImplyLeading: false,
          centerTitle: false,
          flexibleSpace: Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 20.0, vertical: 20),
              child: DashboardAppBarOptions3(windowWidth: Get.width)),
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(windowHeight * 0.05),
            child: const SizedBox(),
          ),
          backgroundColor: const Color(0xFFDF5356),
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
            ),
          ),
        ),
        body: SingleChildScrollView(
          physics: const AlwaysScrollableScrollPhysics(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: const EdgeInsets.all(20.0),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey),
                  borderRadius: const BorderRadius.all(Radius.circular(15)),
                ),
                child: const Padding(
                  padding: EdgeInsets.all(20.0),
                  child: Text(
                      "Our AI has generated the lowest 4 packages based on your requirements!"),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20.0, bottom: 10),
                child: Text(
                  "Lowest Fixed",
                  style: TextStyles.dashboardPopUpTextStyle,
                ),
              ),
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.0),
                child: Divider(height: 1, color: Colors.deepOrangeAccent),
              ),
              const SizedBox(
                height: 10,
              ),
              /*SizedBox(
                height: 400,
                child: Expanded(
                  child: ListView.builder(
                      primary: false,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: 2, // This count needs to be dynamic
                      itemBuilder: (BuildContext context, int index) {
                        return LoanPackageCard(
                          headingSubText: 'Fixed(Special)',
                          headingText: 'Standard characted Bank',
                          onChanged: (val) {},
                          checkBoxValue: false,
                          cardIndex: index,
                        );
                      }),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20.0, bottom: 10),
                child: Text(
                  "Lowest Floating",
                  style: TextStyles.dashboardPopUpTextStyle,
                ),
              ),
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.0),
                child: Divider(height: 1, color: Colors.deepOrangeAccent),
              ),
              const SizedBox(
                height: 10,
              ),
              SizedBox(
                height: 400,
                child: Expanded(
                  child: ListView.builder(
                      primary: false,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: 2, // This count needs to be dynamic
                      itemBuilder: (BuildContext context, int index) {
                        return LoanPackageCard(
                          headingSubText: 'Floating (Exclusive)',
                          headingText: 'DBS Bank',
                          onChanged: (val) {},
                          checkBoxValue: false,
                          cardIndex: index,
                        );
                      }),
                ),
              ),*/
              Center(
                child: Column(
                  children: [
                    PrimaryButton(
                      windowHeight: windowHeight,
                      windowWidth: windowWidth,
                      buttonTitle: "Proceed",
                      onPressed: () {
                        loanPackageController.onTapProceed();
                      },
                    ),
                    SizedBox(height: windowHeight * 0.03),
                    SecondaryButton(
                      windowHeight: windowHeight,
                      windowWidth: windowWidth,
                      kGradientBoxDecoration:
                          ContainerStyles.kGradientBoxDecorationSecondaryButton,
                      kInnerDecoration:
                          ContainerStyles.kInnerDecorationSecondaryButton,
                      buttonTitle: 'See More Rates',
                      onPressed: () {
                        loanPackageController.onTapMoreRates();
                      },
                    ),
                    SizedBox(height: windowHeight * 0.03)
                  ],
                ),
              )
            ],
          ),
        ));
  }
}
