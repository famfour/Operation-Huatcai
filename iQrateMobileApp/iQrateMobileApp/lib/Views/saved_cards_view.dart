import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/saved_cards_controller.dart';
import 'package:iqrate/DeviceManager/screen_constants.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Widgets/primary_button.dart';

class SavedCardsView extends StatelessWidget {
  SavedCardsView({Key? key}) : super(key: key);
  final SavedCardsController savedCardsController = Get.find();

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        automaticallyImplyLeading: false,
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 8.0, top: 20),
            child: InkWell(
              onTap: () {
                Get.back(); // Pop the existing page and go back to previous page
              },
              child: SvgPicture.asset(
                'assets/icons/closeEnclosed.svg',
                color: Colors.black,
              ),
            ),
          )
        ],
      ),
      body: RefreshIndicator(
        displacement: 100,
        backgroundColor: Colors.green,
        color: Colors.white,
        strokeWidth: 3,
        triggerMode: RefreshIndicatorTriggerMode.onEdge,
        onRefresh: () async {
          await savedCardsController.getSavedCards();
          Future.delayed(const Duration(milliseconds: 0), () {
            FocusManager.instance.primaryFocus!.unfocus();
            Fluttertoast.showToast(
              timeInSecForIosWeb: 3,
              msg: "Refreshed",
              backgroundColor: Colors.green,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 20.0,
            );
          });
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'My Saved Cards',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: FontSize.s22),
                ),
                SizedBox(height: windowHeight * 0.02),
                Obx(() => savedCardsController
                            .cardListResponseModel.value.data !=
                        null
                    ? ListView.builder(
                        key: Key(
                            '${savedCardsController.cardListResponseModel.value.data?.length}'),
                        shrinkWrap: true,
                        primary: false,
                        itemCount: savedCardsController
                            .cardListResponseModel.value.data?.length,
                        //itemCount: 1,
                        itemBuilder: (context, index) {
                          var data = savedCardsController
                              .cardListResponseModel.value.data
                              ?.elementAt(index);
                          return Padding(
                            padding: const EdgeInsets.symmetric(vertical: 20.0),
                            child: Column(
                              children: [
                                Stack(
                                  children: [
                                    Image.asset("assets/images/card_bg.png"),
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: windowWidth * 0.07),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Row(
                                            children: [
                                              Image.asset(
                                                "assets/images/card_chip.png",
                                                width: windowWidth * 0.15,
                                                height: windowHeight * 0.12,
                                              ),
                                              Image.asset(
                                                  "assets/images/card_signal.png",
                                                  width: windowWidth * 0.1,
                                                  height: windowHeight * 0.04),
                                            ],
                                          ),
                                          Image.asset(
                                              data!.card!.brand! == "mastercard"
                                                  ? "assets/images/master_card.svg"
                                                  : "assets/images/visa_card.png",
                                              width: windowWidth * 0.15,
                                              height: windowHeight * 0.12),
                                        ],
                                      ),
                                    ),
                                    Positioned(
                                        top: windowHeight * 0.13,
                                        left: windowWidth * 0.06,
                                        child: Text(
                                          "XXXX XXXX XXXX " + data.card!.last4!,
                                          style: TextStyles
                                              .planTermSelectionPriceTextStyle3,
                                        )),
                                    Positioned(
                                        top: windowHeight * 0.168,
                                        left: windowWidth * 0.07,
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            SizedBox(
                                              width: windowWidth * 0.1,
                                            ),
                                            Text(
                                              "valid \nThru",
                                              style: TextStyles
                                                  .whitePlanTermSelectionPriceSavingTextStyle,
                                            ),
                                            SizedBox(
                                              width: windowWidth * 0.015,
                                            ),
                                            Text(
                                              data.card!.expMonth.toString() +
                                                  "/" +
                                                  data.card!.expYear.toString(),
                                              style: TextStyles
                                                  .planTermSelectionPriceTextStyle2,
                                            ),
                                          ],
                                        )),
                                    Positioned(
                                        top: windowHeight * 0.21,
                                        left: windowWidth * 0.07,
                                        width: windowWidth * 0.78,
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          //crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            Text(
                                              data.billingDetails!.name
                                                  .toString(),
                                              style: TextStyles
                                                  .planTermSelectionPriceTextStyle2,
                                            ),
                                            const Spacer(),
                                            /*InkWell(
                                        onTap: () {
                                          savedCardsController.onEdit(data);
                                        },
                                        child: Image.asset(
                                            "assets/images/card_edit.png",
                                            width: windowWidth * 0.07,
                                            height: windowHeight * 0.1),
                                      ),*/
                                            data.id !=
                                                    savedCardsController
                                                        .defaultPaymentMethodResponseModel
                                                        .value
                                                        .invoiceSettings!
                                                        .defaultPaymentMethod
                                                ? Row(
                                                    children: [
                                                      SizedBox(
                                                        width:
                                                            windowWidth * 0.03,
                                                      ),
                                                      InkWell(
                                                        onTap: () {
                                                          savedCardsController
                                                              .onTapDelete(
                                                                  data);
                                                        },
                                                        child: SvgPicture.asset(
                                                            "assets/images/card_delete.svg",
                                                            width: windowWidth *
                                                                0.02,
                                                            height:
                                                                windowHeight *
                                                                    0.035),
                                                      ),
                                                    ],
                                                  )
                                                : const SizedBox(),
                                          ],
                                        )),
                                  ],
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 25.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text('Set as Primary Card',
                                          style: TextStyles
                                              .introScreenDescriptions),
                                      CupertinoSwitch(
                                          value: data.id ==
                                                  savedCardsController
                                                      .defaultPaymentMethodResponseModel
                                                      .value
                                                      .invoiceSettings!
                                                      .defaultPaymentMethod
                                              ? true
                                              : false,
                                          onChanged: (newValue) {
                                            savedCardsController.switchOnTap(
                                                data.id!, newValue);
                                          })
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          );
                        })
                    : SizedBox(
                        height: windowHeight * 0.65,
                        child: Center(
                          child: Text('No saved Cards',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: FontSize.s22)),
                        ),
                      )),
                SizedBox(height: windowHeight * 0.04),
                Center(
                  child: PrimaryButton(
                    windowHeight: windowHeight,
                    windowWidth: windowWidth,
                    buttonTitle: "Add New Card",
                    onPressed: () {
                      savedCardsController.addNewCard();
                    },
                  ),
                ),
                SizedBox(height: windowHeight * 0.04),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
