import 'dart:io';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/login_controller.dart';
import 'package:iqrate/DeviceManager/container_styles.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Views/email_resend_screen.dart';
import 'package:iqrate/Widgets/apple_signin_button.dart';
import 'package:iqrate/Widgets/forget_password.dart';
import 'package:iqrate/Widgets/form_field_title.dart';
import 'package:iqrate/Widgets/google_signin_button.dart';
import 'package:iqrate/Widgets/new_to_iqrate.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';

class LoginScreen extends StatelessWidget {
  final SignInController signInController =
      Get.put(SignInController()); //initialize signin controller
  // final ContinueWithGoogleButtonController continueWithGoogleButtonController =
  //     Get.put(ContinueWithGoogleButtonController());
  LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double windowWidth = MediaQuery.of(context).size.width; //get screen width
    double windowHeight =
        MediaQuery.of(context).size.height; //get screen height
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: windowHeight * 0.06),
                Center(
                  child: SizedBox(
                    child: SvgPicture.asset(
                      'assets/logos/iqrate_logo.svg',
                      fit: BoxFit.contain,
                      // semanticsLabel: 'Logo',
                    ),
                  ),
                ),
                SizedBox(height: windowHeight * 0.05),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Log in",
                      style: TextStyles.authTextTitleStyle,
                    ),
                    SizedBox(height: windowHeight * 0.02),
                    const FormFieldTitle(title: 'Email Address'),
                    SizedBox(height: windowHeight * 0.01),
                    RequireTextField(
                      type: Type.emailWithClear,
                      controller: signInController.emailTextController,
                      labelText: "Enter your email address",
                    ),
                    SizedBox(height: windowHeight * 0.01),
                    const FormFieldTitle(title: 'Password'),
                    SizedBox(height: windowHeight * 0.01),
                    RequireTextField(
                      key: const Key("password"),
                      type: Type.passWordWithClear,
                      controller: signInController.passwordTextController,
                      labelText: "Alphanumeric (8+ characters)",
                    ),
                    SizedBox(height: windowHeight * 0.01),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ForgetPassword(
                          title: "Resend Verification Email",
                          key: const Key("resend"),
                          tapGestureRecognizer: TapGestureRecognizer()
                            ..onTap = () {
                              debugPrint("clicked Resend Email Code");
                              //signInController.apiCallResendEmailCode();

                              Get.to(() => EmailResendScreen());
                            },
                        ),
                        ForgetPassword(
                          title: "Forgot Password?",
                          key: const Key("forget"),
                          tapGestureRecognizer: TapGestureRecognizer()
                            ..onTap = () {
                              debugPrint("clicked forgot password");
                              Get.toNamed(
                                  forgetPasswordScreen); //navigate to forget Password screen
                            },
                        ),
                      ],
                    ),
                    SizedBox(height: windowHeight * 0.05),
                    Center(
                      child: PrimaryButton(
                        windowHeight: windowHeight,
                        windowWidth: windowWidth,
                        buttonTitle: "Log in",
                        onPressed: () {
                          signInController.onSignIn(); //sign in
                          // Get.toNamed(dashboard); //navigate to signin screen
                        },
                      ),
                    ),
                    SizedBox(height: windowHeight * 0.02),
                    Center(
                      child: Text(
                        'or',
                        style: TextStyles.orTextStyle,
                      ),
                    ),
                    SizedBox(height: windowHeight * 0.02),
                    Row(
                      children: [
                        Flexible(
                          flex: 1,
                          child: GoogleSignIn(
                            windowHeight: windowHeight,
                            windowWidth: windowWidth,
                            kGradientBoxDecoration: ContainerStyles
                                .kGradientBoxDecorationSecondaryButton,
                            kInnerDecoration:
                                ContainerStyles.kInnerDecorationSecondaryButton,
                            onPressed: () {
                              signInController.handleGoogleLogin("");
                            },
                            buttonTitle: "",
                          ),
                        ),
                        Platform.isIOS
                            ? Expanded(
                                child: Row(
                                  children: [
                                    SizedBox(
                                      width: windowWidth * 0.03,
                                    ),
                                    Flexible(
                                      flex: 1,
                                      child: AppleSignIn(
                                        windowHeight: windowHeight,
                                        windowWidth: windowWidth,
                                        kGradientBoxDecoration: ContainerStyles
                                            .kGradientBoxDecorationSecondaryButton,
                                        kInnerDecoration: ContainerStyles
                                            .kInnerDecorationSecondaryButton,
                                        onPressed: () {
                                          signInController.handleAppleLogin("");
                                        },
                                        buttonTitle: "",
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            : const SizedBox(),
                      ],
                    ),
                    SizedBox(height: windowHeight * 0.08),
                    NewToiQrate(
                      tapGestureRecognizerToLogin: TapGestureRecognizer()
                        ..onTap = () {
                          debugPrint("clicked signup for free");
                          Get.toNamed(signUp); //navigate to signup screen
                        },
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
