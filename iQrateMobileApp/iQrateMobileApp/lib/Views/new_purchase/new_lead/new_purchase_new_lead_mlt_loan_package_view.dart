// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/new_purchase_new_lead_calculator_controller.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/screen_constants.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Views/new_purchase/existing_lead/new_purchase_existing_lead_mlt_loan_package_view.dart';
import '../../../Widgets/default_appbar.dart';

class NewPurchaseNewLeadMltLoanPackage extends StatelessWidget {
  NewPurchaseNewLeadMltLoanPackage({Key? key}) : super(key: key);
  final NewPurchaseNewLeadCalcController newPurchaseExistingLeadController =
      Get.find();

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: DefaultAppBar2(
        title: "New Purchase",
        windowHeight: windowHeight * 0.09,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: windowHeight * 0.02,
              ),
              Text(
                'Loan Packages',
                style: TextStyle(
                  color: AppColors.kPrimaryColor,
                  fontSize: FontSize.s22,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: windowHeight * 0.003,
              ),
              Text(
                'Based On Maximum Loan Tenure',
                style: TextStyle(
                  color: const Color(0XFF9A9696),
                  fontSize: FontSize.s20,
                ),
              ),
              SizedBox(
                height: windowHeight * 0.03,
              ),
              Obx(
                () => Table(
                  defaultColumnWidth: FixedColumnWidth(windowWidth * 0.3),
                  border: TableBorder.all(
                      color: const Color(0XFFE2E2E2),
                      style: BorderStyle.solid,
                      width: 2),
                  children: [
                    TableRow(
                      children: [
                        TableTitleRowData(
                          windowHeight: windowHeight,
                          title: '',
                        ),
                        TableTitleRowData(
                          windowHeight: windowHeight,
                          title: 'Lowest Floating',
                        ),
                        TableTitleRowData(
                          windowHeight: windowHeight,
                          title: 'Lowest Fixed',
                        ),
                      ],
                    ),
                    TableRow(
                      children: [
                        DataRow(
                          windowHeight: windowHeight,
                          stringData: 'Monthly Installment',
                        ),
                        DataRow(
                          windowHeight: windowHeight,
                          stringData: newPurchaseExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .loanPackageMaximumLoanTenureBased!
                                      .lowestFloating !=
                                  null
                              ? newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .loanPackageMaximumLoanTenureBased!
                                  .lowestFloating!
                                  .monthlyInstallment
                                  .toString()
                              : '-',
                        ),
                        DataRow(
                          windowHeight: windowHeight,
                          stringData: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .loanPackageMaximumLoanTenureBased!
                              .lowestFixed!
                              .monthlyInstallment
                              .toString(),
                        ),
                      ],
                    ),
                    TableRow(
                      children: [
                        DataRow(
                          windowHeight: windowHeight,
                          stringData: 'Total Payment',
                        ),
                        DataRow(
                          windowHeight: windowHeight,
                          stringData: newPurchaseExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .loanPackageMaximumLoanTenureBased!
                                      .lowestFloating !=
                                  null
                              ? newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .loanPackageMaximumLoanTenureBased!
                                  .lowestFloating!
                                  .totalPayment
                                  .toString()
                              : '-',
                        ),
                        DataRow(
                          windowHeight: windowHeight,
                          stringData: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .loanPackageMaximumLoanTenureBased!
                              .lowestFixed!
                              .totalPayment
                              .toString(),
                        ),
                      ],
                    ),
                    TableRow(
                      children: [
                        DataRow(
                          windowHeight: windowHeight,
                          stringData: 'Total Principal',
                        ),
                        DataRow(
                          windowHeight: windowHeight,
                          stringData: newPurchaseExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .loanPackageMaximumLoanTenureBased!
                                      .lowestFloating !=
                                  null
                              ? newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .loanPackageMaximumLoanTenureBased!
                                  .lowestFloating!
                                  .totalPrincipal
                                  .toString()
                              : '-',
                        ),
                        DataRow(
                          windowHeight: windowHeight,
                          stringData: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .loanPackageMaximumLoanTenureBased!
                              .lowestFixed!
                              .totalPrincipal
                              .toString(),
                        ),
                      ],
                    ),
                    TableRow(
                      children: [
                        DataRow(
                          windowHeight: windowHeight,
                          stringData: 'Total Interest',
                        ),
                        DataRow(
                          windowHeight: windowHeight,
                          stringData: newPurchaseExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .loanPackageMaximumLoanTenureBased!
                                      .lowestFloating !=
                                  null
                              ? newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .loanPackageMaximumLoanTenureBased!
                                  .lowestFloating!
                                  .totalInterest
                                  .toString()
                              : '-',
                        ),
                        DataRow(
                          windowHeight: windowHeight,
                          stringData: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .loanPackageMaximumLoanTenureBased!
                              .lowestFixed!
                              .totalInterest
                              .toString(),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: windowHeight * 0.03,
                  ),
                  Text(
                    'Lowest Floating',
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: FontSize.s16,
                    ),
                  ),
                  Divider(
                    color: const Color(0X9A969666).withOpacity(0.4),
                    thickness: 2,
                  ),
                  SizedBox(
                    height: windowHeight * 0.02,
                  ),
                  newPurchaseExistingLeadController
                              .resultMainApplicantStep3.value.lowestFloating !=
                          null
                      ? DetailsCardForPackages(
                          windowHeight: windowHeight,
                          windowWidth: windowWidth,
                          image: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFloating!
                              .bankLogo
                              .toString(),
                          bankName: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFloating!
                              .bankName
                              .toString(),
                          rateType:
                              "${newPurchaseExistingLeadController.resultMainApplicantStep3.value.lowestFloating!.rateType.toString().replaceAll('_', ' ').capitalize!} (${newPurchaseExistingLeadController.resultMainApplicantStep3.value.lowestFloating!.details!.rateCategory.toString().capitalizeFirst})",
                          yearOneValue: newPurchaseExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .lowestFloating!
                                      .details!
                                      .rates!
                                      .elementAt(1)
                                      .year ==
                                  'year_5'
                              ? newPurchaseExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .lowestFloating!
                                      .details!
                                      .rates![5]
                                      .totalInterestRate
                                      .toString() +
                                  "%"
                              : newPurchaseExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .lowestFloating!
                                      .details!
                                      .rates![0]
                                      .totalInterestRate
                                      .toString() +
                                  "%",
                          itemCount: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFloating!
                              .rateList!
                              .length,
                          rateValue: newPurchaseExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .lowestFloating!
                                      .details!
                                      .rates!
                                      .elementAt(1)
                                      .year ==
                                  'year_5'
                              ? newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .lowestFloating!
                                  .details!
                                  .rates!
                                  .reversed
                              : newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .lowestFloating!
                                  .details!
                                  .rates!,
                          propertyType: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFloating!
                              .details!
                              .propertyTypes,
                          typeOfloan: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFloating!
                              .details!
                              .loanCategory,
                          lockInPeriod: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFloating!
                              .details!
                              .lockInPeriod
                              .toString(),
                          minLoanAmount: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFloating!
                              .details!
                              .minLoanAmount
                              .toString(),
                          depositToPlace: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFloating!
                              .details!
                              .depositToPlace
                              .toString(),
                          remarksForApplicant: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFloating!
                              .details!
                              .remarksForClient
                              .toString(),
                          remarksForBroker: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFloating!
                              .details!
                              .remarksForBroker
                              .toString(),
                          cashRebateLegalSubsidy:
                              newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .lowestFloating!
                                  .details!
                                  .cashRebateLegalSubsidy
                                  .toString(),
                          valuationSubsidy: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFloating!
                              .details!
                              .valuationSubsidy
                              .toString(),
                          cashRebateSubsidyClawback:
                              newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .lowestFloating!
                                  .details!
                                  .cashRebateSubsidyClawbackPeriodYears
                                  .toString(),
                          fireInsuranceSubsidy:
                              newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .lowestFloating!
                                  .details!
                                  .fireInsuranceSubsidy
                                  .toString(),
                          partialRepaymentPenalty:
                              newPurchaseExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .lowestFloating!
                                      .details!
                                      .partialRepaymentPenalty
                                      .toString() +
                                  '%',
                          partialRepaymentPenaltyRemark:
                              newPurchaseExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .lowestFloating!
                                      .details!
                                      .partialRepaymentPenaltyRemarks ??
                                  'NA',
                          fullRepaymentPenalty:
                              newPurchaseExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .lowestFloating!
                                      .details!
                                      .fullRepaymentPenalty
                                      .toString() +
                                  '%',
                          fullRepaymentPenaltyRemarks:
                              newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .lowestFloating!
                                  .details!
                                  .fullRepaymentPenaltyRemarks
                                  .toString(),
                          cancellationFee: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFloating!
                              .details!
                              .cancellationFee
                              .toString(),
                          cancellationFeeRemarks:
                              newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .lowestFloating!
                                  .details!
                                  .cancellationFeeRemarks
                                  .toString(),
                          propertyStatus: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFloating!
                              .details!
                              .propertyStatus,
                        )
                      : Center(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SizedBox(
                                  height: windowHeight * 0.15,
                                  child: SvgPicture.asset(
                                      'assets/images/nodata.svg')),
                              Text(
                                'No data to show',
                                style:
                                    TextStyles.bankSubmissionCardTitleDisabled,
                              )
                            ],
                          ),
                        ),
                  SizedBox(
                    height: windowHeight * 0.04,
                  ),
                  Text(
                    'Lowest Fixed',
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: FontSize.s16,
                    ),
                  ),
                  Divider(
                    color: const Color(0X9A969666).withOpacity(0.4),
                    thickness: 2,
                  ),
                  SizedBox(
                    height: windowHeight * 0.02,
                  ),
                  DetailsCardForPackages(
                    windowHeight: windowHeight,
                    windowWidth: windowWidth,
                    image: newPurchaseExistingLeadController
                        .resultMainApplicantStep3.value.lowestFixed!.bankLogo
                        .toString(),
                    bankName: newPurchaseExistingLeadController
                        .resultMainApplicantStep3.value.lowestFixed!.bankName
                        .toString(),
                    rateType:
                        "${newPurchaseExistingLeadController.resultMainApplicantStep3.value.lowestFixed!.rateType.toString().replaceAll('_', ' ').capitalize!} (${newPurchaseExistingLeadController.resultMainApplicantStep3.value.lowestFixed!.details!.rateCategory.toString().capitalizeFirst})",
                    yearOneValue: newPurchaseExistingLeadController
                                .resultMainApplicantStep3
                                .value
                                .lowestFixed!
                                .details!
                                .rates!
                                .elementAt(1)
                                .year ==
                            'year_5'
                        ? newPurchaseExistingLeadController
                                .resultMainApplicantStep3
                                .value
                                .lowestFixed!
                                .details!
                                .rates![5]
                                .totalInterestRate
                                .toString() +
                            "%"
                        : newPurchaseExistingLeadController
                                .resultMainApplicantStep3
                                .value
                                .lowestFixed!
                                .details!
                                .rates![0]
                                .totalInterestRate
                                .toString() +
                            "%",
                    itemCount: newPurchaseExistingLeadController
                        .resultMainApplicantStep3
                        .value
                        .lowestFixed!
                        .rateList!
                        .length,
                    rateValue: newPurchaseExistingLeadController
                                .resultMainApplicantStep3
                                .value
                                .lowestFixed!
                                .details!
                                .rates!
                                .elementAt(1)
                                .year ==
                            'year_5'
                        ? newPurchaseExistingLeadController
                            .resultMainApplicantStep3
                            .value
                            .lowestFixed!
                            .details!
                            .rates!
                            .reversed
                        : newPurchaseExistingLeadController
                            .resultMainApplicantStep3
                            .value
                            .lowestFixed!
                            .details!
                            .rates!,
                    propertyType: newPurchaseExistingLeadController
                        .resultMainApplicantStep3
                        .value
                        .lowestFixed!
                        .details!
                        .propertyTypes,
                    typeOfloan: newPurchaseExistingLeadController
                        .resultMainApplicantStep3
                        .value
                        .lowestFixed!
                        .details!
                        .loanCategory,
                    lockInPeriod: newPurchaseExistingLeadController
                        .resultMainApplicantStep3
                        .value
                        .lowestFixed!
                        .details!
                        .lockInPeriod
                        .toString(),
                    minLoanAmount: newPurchaseExistingLeadController
                        .resultMainApplicantStep3
                        .value
                        .lowestFixed!
                        .details!
                        .minLoanAmount
                        .toString(),
                    depositToPlace: newPurchaseExistingLeadController
                        .resultMainApplicantStep3
                        .value
                        .lowestFixed!
                        .details!
                        .depositToPlace
                        .toString(),
                    remarksForApplicant: newPurchaseExistingLeadController
                        .resultMainApplicantStep3
                        .value
                        .lowestFixed!
                        .details!
                        .remarksForClient
                        .toString(),
                    remarksForBroker: newPurchaseExistingLeadController
                        .resultMainApplicantStep3
                        .value
                        .lowestFixed!
                        .details!
                        .remarksForBroker
                        .toString(),
                    cashRebateLegalSubsidy: newPurchaseExistingLeadController
                        .resultMainApplicantStep3
                        .value
                        .lowestFixed!
                        .details!
                        .cashRebateLegalSubsidy
                        .toString(),
                    valuationSubsidy: newPurchaseExistingLeadController
                        .resultMainApplicantStep3
                        .value
                        .lowestFixed!
                        .details!
                        .valuationSubsidy
                        .toString(),
                    cashRebateSubsidyClawback: newPurchaseExistingLeadController
                        .resultMainApplicantStep3
                        .value
                        .lowestFixed!
                        .details!
                        .cashRebateSubsidyClawbackPeriodYears
                        .toString(),
                    fireInsuranceSubsidy: newPurchaseExistingLeadController
                        .resultMainApplicantStep3
                        .value
                        .lowestFixed!
                        .details!
                        .fireInsuranceSubsidy
                        .toString(),
                    partialRepaymentPenalty: newPurchaseExistingLeadController
                        .resultMainApplicantStep3
                        .value
                        .lowestFixed!
                        .details!
                        .partialRepaymentPenalty
                        .toString(),
                    partialRepaymentPenaltyRemark:
                        newPurchaseExistingLeadController
                                .resultMainApplicantStep3
                                .value
                                .lowestFixed!
                                .details!
                                .partialRepaymentPenaltyRemarks ??
                            'NA',
                    fullRepaymentPenalty: newPurchaseExistingLeadController
                        .resultMainApplicantStep3
                        .value
                        .lowestFixed!
                        .details!
                        .fullRepaymentPenalty
                        .toString(),
                    fullRepaymentPenaltyRemarks:
                        newPurchaseExistingLeadController
                            .resultMainApplicantStep3
                            .value
                            .lowestFixed!
                            .details!
                            .fullRepaymentPenaltyRemarks
                            .toString(),
                    cancellationFee: newPurchaseExistingLeadController
                        .resultMainApplicantStep3
                        .value
                        .lowestFixed!
                        .details!
                        .cancellationFee
                        .toString(),
                    cancellationFeeRemarks: newPurchaseExistingLeadController
                        .resultMainApplicantStep3
                        .value
                        .lowestFixed!
                        .details!
                        .cancellationFeeRemarks
                        .toString(),
                    propertyStatus: newPurchaseExistingLeadController
                        .resultMainApplicantStep3
                        .value
                        .lowestFixed!
                        .details!
                        .propertyStatus,
                  ),
                ],
              ),
              SizedBox(
                height: windowHeight * 0.1,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class TitleValueRow extends StatelessWidget {
  TitleValueRow({Key? key, required this.title, required this.value})
      : super(key: key);
  String title;
  String value;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 3,
            child: Text(
              title,
              style: const TextStyle(
                fontWeight: FontWeight.bold,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Expanded(
            flex: 7,
            child: Text(
              value
                  .toString()
                  .replaceAll('[', '')
                  .replaceAll(']', '')
                  .replaceAll('_', ' ')
                  .capitalize!,
              textAlign: TextAlign.right,
            ),
          ),
        ],
      ),
    );
  }
}

class DataRow extends StatelessWidget {
  final String stringData;
  const DataRow({
    Key? key,
    required this.windowHeight,
    required this.stringData,
  }) : super(key: key);

  final double windowHeight;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            height: windowHeight * 0.02,
          ),
          Text(
            double.tryParse(stringData) != null
                ? "\$" +
                    double.tryParse(stringData)!
                        .toStringAsFixed(2)
                        .replaceAllMapped(
                            RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))'),
                            (Match m) => '${m[1]},')
                : stringData.replaceAllMapped(
                    RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))'),
                    (Match m) => '${m[1]},'),
            style: TextStyle(fontSize: FontSize.s14),
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: windowHeight * 0.02,
          ),
        ],
      ),
    );
  }
}

class TableTitleRowData extends StatelessWidget {
  const TableTitleRowData({
    Key? key,
    required this.windowHeight,
    required this.title,
  }) : super(key: key);

  final double windowHeight;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Column(
      // mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          height: windowHeight * 0.02,
        ),
        Text(
          title,
          style:
              TextStyle(fontSize: FontSize.s17, color: AppColors.kPrimaryColor),
          textAlign: TextAlign.center,
        ),
        SizedBox(
          height: windowHeight * 0.02,
        ),
      ],
    );
  }
}

class TitleValueRowWithListForType extends StatelessWidget {
  TitleValueRowWithListForType(
      {Key? key, required this.title, required this.values})
      : super(key: key);
  String title;
  List<String> values;

  @override
  Widget build(BuildContext context) {
    double windowWidth = MediaQuery.of(context).size.width;
    return Padding(
      padding:
          const EdgeInsets.only(right: 10.0, left: 10.0, top: 8, bottom: 0),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 4,
                child: Text(
                  title,
                  style: TextStyles.headingSubtitleStyle4,
                ),
              ),
              SizedBox(
                width: windowWidth * 0.05,
              ),
              Flexible(
                flex: 4,
                child: ListView.builder(
                    physics: const ClampingScrollPhysics(),
                    shrinkWrap: true,
                    primary: false,
                    scrollDirection: Axis.vertical,
                    itemCount: values.length,
                    itemBuilder: (BuildContext context, int index) => Align(
                          alignment: Alignment.topRight,
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 0.0),
                            child: Align(
                              alignment: Alignment.centerRight,
                              child: Text(
                                values.elementAt(index).trim(),

                                /*maxLines: 1,
                                overflow: TextOverflow.ellipsis,*/
                                textAlign: TextAlign.start,
                              ),
                            ),
                          ),
                        )),
              )
            ],
          ),
        ],
      ),
    );
  }
}
