import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/new_purchase_new_lead_calculator_controller.dart';
import 'package:iqrate/Utils/config.dart';

import '../../../DeviceManager/colors.dart';
import '../../../DeviceManager/screen_constants.dart';
import '../../../Widgets/default_appbar.dart';

class NewPurchaseNewLeadMltMortgageRepayment extends StatelessWidget {
  NewPurchaseNewLeadMltMortgageRepayment({Key? key}) : super(key: key);

  final NewPurchaseNewLeadCalcController newPurchaseExistingLeadController =
      Get.find();

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    // double windowWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: DefaultAppBar2(
        title: "New Purchase",
        windowHeight: windowHeight * 0.09,
      ),
      body: InteractiveViewer(
        minScale: 1,
        maxScale: 4,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: windowHeight * 0.02,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15.0),
              child: Text(
                'Mortgage Repayment Table',
                style: TextStyle(
                  color: AppColors.kPrimaryColor,
                  fontSize: FontSize.s22,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(
              height: windowHeight * 0.003,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15.0),
              child: Text(
                newPurchaseExistingLeadController
                    .titleForMortgageTableScreen.value
                    .toString(),
                style: TextStyle(
                  color: const Color(0XFF9A9696),
                  fontSize: FontSize.s20,
                ),
              ),
            ),
            SizedBox(
              height: windowHeight * 0.01,
            ),
            tableTitle(windowHeight),
            Expanded(
              child: ListView(
                children: [
                  SingleChildScrollView(
                    child: Column(
                      children: [
                        if (newPurchaseExistingLeadController
                            .yearOneToFiveList.isNotEmpty)
                          Card(
                            elevation: 2,
                            child: ExpansionTile(
                              title: Text(
                                'Year 1-5',
                                style: TextStyle(
                                  fontSize: FontSize.s19,
                                  color: Colors.black,
                                ),
                              ),
                              children: [
                                ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: newPurchaseExistingLeadController
                                      .yearOneToFiveList.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Table(
                                      border: TableBorder.all(
                                        color: const Color(0XFFE2E2E2),
                                        style: BorderStyle.solid,
                                        width: 2,
                                      ),
                                      children: [
                                        TableRow(
                                          decoration: BoxDecoration(
                                            color: newPurchaseExistingLeadController
                                                        .resultMainApplicantStep3
                                                        .value
                                                        .loanPackageMaximumLoanTenureBased!
                                                        .lowestFloating!
                                                        .mortgageRepaymentTable![
                                                            index]
                                                        .month
                                                        .toString() ==
                                                    '12'
                                                ? Colors.grey.shade400
                                                : Colors.white,
                                          ),
                                          children: [
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                      .yearOneToFiveList[index]!
                                                      .year
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                      .yearOneToFiveList[index]!
                                                      .month
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                          .yearOneToFiveList[
                                                              index]!
                                                          .interestRate
                                                          .toString() +
                                                      "%",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                        AppConfig.extraDecimalAdd2Digit(newPurchaseExistingLeadController
                                                      .yearOneToFiveList[index]!
                                                      .installmentAmount
                                                      .toString()),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .yearOneToFiveList[index]!
                                                      .interestRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .yearOneToFiveList[index]!
                                                      .principalRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .yearOneToFiveList[index]!
                                                      .outstandingAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                        AppConfig.extraDecimalAdd2Digit(newPurchaseExistingLeadController
                                                      .yearOneToFiveList[index]!
                                                      .cumInterestPaid
                                                      .toString()),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        if (newPurchaseExistingLeadController
                            .yearSixToTenList.isNotEmpty)
                          Card(
                            elevation: 2,
                            child: ExpansionTile(
                              title: Text(
                                'Year 6-10',
                                style: TextStyle(
                                  fontSize: FontSize.s19,
                                  color: Colors.black,
                                ),
                              ),
                              children: [
                                ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: newPurchaseExistingLeadController
                                      .yearSixToTenList.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Table(
                                      border: TableBorder.all(
                                        color: const Color(0XFFE2E2E2),
                                        style: BorderStyle.solid,
                                        width: 2,
                                      ),
                                      children: [
                                        TableRow(
                                          decoration: BoxDecoration(
                                            color: newPurchaseExistingLeadController
                                                        .resultMainApplicantStep3
                                                        .value
                                                        .loanPackageMaximumLoanTenureBased!
                                                        .lowestFloating!
                                                        .mortgageRepaymentTable![
                                                            index]
                                                        .month
                                                        .toString() ==
                                                    '12'
                                                ? Colors.grey.shade400
                                                : Colors.white,
                                          ),
                                          children: [
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                      .yearSixToTenList[index]!
                                                      .year
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                      .yearSixToTenList[index]!
                                                      .month
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                          .yearSixToTenList[
                                                              index]!
                                                          .interestRate
                                                          .toString() +
                                                      "%",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                        AppConfig.extraDecimalAdd2Digit(newPurchaseExistingLeadController
                                                      .yearSixToTenList[index]!
                                                      .installmentAmount
                                                      .toString()),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .yearSixToTenList[index]!
                                                      .interestRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .yearSixToTenList[index]!
                                                      .principalRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .yearSixToTenList[index]!
                                                      .outstandingAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                        AppConfig.extraDecimalAdd2Digit(newPurchaseExistingLeadController
                                                      .yearSixToTenList[index]!
                                                      .cumInterestPaid
                                                      .toString()),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        if (newPurchaseExistingLeadController
                            .yearElevenToFifteenList.isNotEmpty)
                          Card(
                            elevation: 2,
                            child: ExpansionTile(
                              title: Text(
                                'Year 11-15',
                                style: TextStyle(
                                  fontSize: FontSize.s19,
                                  color: Colors.black,
                                ),
                              ),
                              children: [
                                ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: newPurchaseExistingLeadController
                                      .yearElevenToFifteenList.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Table(
                                      border: TableBorder.all(
                                        color: const Color(0XFFE2E2E2),
                                        style: BorderStyle.solid,
                                        width: 2,
                                      ),
                                      children: [
                                        TableRow(
                                          decoration: BoxDecoration(
                                            color: newPurchaseExistingLeadController
                                                        .resultMainApplicantStep3
                                                        .value
                                                        .loanPackageMaximumLoanTenureBased!
                                                        .lowestFloating!
                                                        .mortgageRepaymentTable![
                                                            index]
                                                        .month
                                                        .toString() ==
                                                    '12'
                                                ? Colors.grey.shade400
                                                : Colors.white,
                                          ),
                                          children: [
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                      .yearElevenToFifteenList[
                                                          index]!
                                                      .year
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                      .yearElevenToFifteenList[
                                                          index]!
                                                      .month
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                          .yearElevenToFifteenList[
                                                              index]!
                                                          .interestRate
                                                          .toString() +
                                                      "%",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                        AppConfig.extraDecimalAdd2Digit(newPurchaseExistingLeadController
                                                      .yearElevenToFifteenList[
                                                          index]!
                                                      .installmentAmount
                                                      .toString()),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .yearElevenToFifteenList[
                                                          index]!
                                                      .interestRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .yearElevenToFifteenList[
                                                          index]!
                                                      .principalRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .yearElevenToFifteenList[
                                                          index]!
                                                      .outstandingAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                        AppConfig.extraDecimalAdd2Digit(newPurchaseExistingLeadController
                                                      .yearElevenToFifteenList[
                                                          index]!
                                                      .cumInterestPaid
                                                      .toString()),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        if (newPurchaseExistingLeadController
                            .sixteenToTwentyList.isNotEmpty)
                          Card(
                            elevation: 2,
                            child: ExpansionTile(
                              title: Text(
                                'Year 16-20',
                                style: TextStyle(
                                  fontSize: FontSize.s19,
                                  color: Colors.black,
                                ),
                              ),
                              children: [
                                ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: newPurchaseExistingLeadController
                                      .sixteenToTwentyList.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Table(
                                      border: TableBorder.all(
                                        color: const Color(0XFFE2E2E2),
                                        style: BorderStyle.solid,
                                        width: 2,
                                      ),
                                      children: [
                                        TableRow(
                                          decoration: BoxDecoration(
                                            color: newPurchaseExistingLeadController
                                                        .resultMainApplicantStep3
                                                        .value
                                                        .loanPackageMaximumLoanTenureBased!
                                                        .lowestFloating!
                                                        .mortgageRepaymentTable![
                                                            index]
                                                        .month
                                                        .toString() ==
                                                    '12'
                                                ? Colors.grey.shade400
                                                : Colors.white,
                                          ),
                                          children: [
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                      .sixteenToTwentyList[
                                                          index]!
                                                      .year
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                      .sixteenToTwentyList[
                                                          index]!
                                                      .month
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                          .sixteenToTwentyList[
                                                              index]!
                                                          .interestRate
                                                          .toString() +
                                                      "%",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                        AppConfig.extraDecimalAdd2Digit(newPurchaseExistingLeadController
                                                      .sixteenToTwentyList[
                                                          index]!
                                                      .installmentAmount
                                                      .toString()),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .sixteenToTwentyList[
                                                          index]!
                                                      .interestRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .sixteenToTwentyList[
                                                          index]!
                                                      .principalRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .sixteenToTwentyList[
                                                          index]!
                                                      .outstandingAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                        AppConfig.extraDecimalAdd2Digit(newPurchaseExistingLeadController
                                                      .sixteenToTwentyList[
                                                          index]!
                                                      .cumInterestPaid
                                                      .toString()),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        if (newPurchaseExistingLeadController
                            .twentyOneToTwnetyFiveList.isNotEmpty)
                          Card(
                            elevation: 2,
                            child: ExpansionTile(
                              title: Text(
                                'Year 21-25',
                                style: TextStyle(
                                  fontSize: FontSize.s19,
                                  color: Colors.black,
                                ),
                              ),
                              children: [
                                ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: newPurchaseExistingLeadController
                                      .twentyOneToTwnetyFiveList.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Table(
                                      border: TableBorder.all(
                                        color: const Color(0XFFE2E2E2),
                                        style: BorderStyle.solid,
                                        width: 2,
                                      ),
                                      children: [
                                        TableRow(
                                          decoration: BoxDecoration(
                                            color: newPurchaseExistingLeadController
                                                        .resultMainApplicantStep3
                                                        .value
                                                        .loanPackageMaximumLoanTenureBased!
                                                        .lowestFloating!
                                                        .mortgageRepaymentTable![
                                                            index]
                                                        .month
                                                        .toString() ==
                                                    '12'
                                                ? Colors.grey.shade400
                                                : Colors.white,
                                          ),
                                          children: [
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                      .twentyOneToTwnetyFiveList[
                                                          index]!
                                                      .year
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                      .twentyOneToTwnetyFiveList[
                                                          index]!
                                                      .month
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                          .twentyOneToTwnetyFiveList[
                                                              index]!
                                                          .interestRate
                                                          .toString() +
                                                      "%",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                        AppConfig.extraDecimalAdd2Digit(newPurchaseExistingLeadController
                                                      .twentyOneToTwnetyFiveList[
                                                          index]!
                                                      .installmentAmount
                                                      .toString()),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .twentyOneToTwnetyFiveList[
                                                          index]!
                                                      .interestRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .twentyOneToTwnetyFiveList[
                                                          index]!
                                                      .principalRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .twentyOneToTwnetyFiveList[
                                                          index]!
                                                      .outstandingAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                        AppConfig.extraDecimalAdd2Digit(newPurchaseExistingLeadController
                                                      .twentyOneToTwnetyFiveList[
                                                          index]!
                                                      .cumInterestPaid
                                                      .toString()),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        if (newPurchaseExistingLeadController
                            .twentySixToThirtyList.isNotEmpty)
                          Card(
                            elevation: 2,
                            child: ExpansionTile(
                              title: Text(
                                'Year 26-30',
                                style: TextStyle(
                                  fontSize: FontSize.s19,
                                  color: Colors.black,
                                ),
                              ),
                              children: [
                                ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: newPurchaseExistingLeadController
                                      .twentySixToThirtyList.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Table(
                                      border: TableBorder.all(
                                        color: const Color(0XFFE2E2E2),
                                        style: BorderStyle.solid,
                                        width: 2,
                                      ),
                                      children: [
                                        TableRow(
                                          decoration: BoxDecoration(
                                            color: newPurchaseExistingLeadController
                                                        .resultMainApplicantStep3
                                                        .value
                                                        .loanPackageMaximumLoanTenureBased!
                                                        .lowestFloating!
                                                        .mortgageRepaymentTable![
                                                            index]
                                                        .month
                                                        .toString() ==
                                                    '12'
                                                ? Colors.grey.shade400
                                                : Colors.white,
                                          ),
                                          children: [
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                      .twentySixToThirtyList[
                                                          index]!
                                                      .year
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                      .twentySixToThirtyList[
                                                          index]!
                                                      .month
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                          .twentySixToThirtyList[
                                                              index]!
                                                          .interestRate
                                                          .toString() +
                                                      "%",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                        AppConfig.extraDecimalAdd2Digit(newPurchaseExistingLeadController
                                                      .twentySixToThirtyList[
                                                          index]!
                                                      .installmentAmount
                                                      .toString()),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .twentySixToThirtyList[
                                                          index]!
                                                      .interestRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .twentySixToThirtyList[
                                                          index]!
                                                      .principalRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .twentySixToThirtyList[
                                                          index]!
                                                      .outstandingAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                        AppConfig.extraDecimalAdd2Digit(newPurchaseExistingLeadController
                                                      .twentySixToThirtyList[
                                                          index]!
                                                      .cumInterestPaid
                                                      .toString()),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        if (newPurchaseExistingLeadController
                            .thirtyOneToThirtyFiveList.isNotEmpty)
                          Card(
                            elevation: 2,
                            child: ExpansionTile(
                              title: Text(
                                'Year 31-35',
                                style: TextStyle(
                                  fontSize: FontSize.s19,
                                  color: Colors.black,
                                ),
                              ),
                              children: [
                                ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: newPurchaseExistingLeadController
                                      .thirtyOneToThirtyFiveList.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Table(
                                      border: TableBorder.all(
                                        color: const Color(0XFFE2E2E2),
                                        style: BorderStyle.solid,
                                        width: 2,
                                      ),
                                      children: [
                                        TableRow(
                                          decoration: BoxDecoration(
                                            color: newPurchaseExistingLeadController
                                                        .resultMainApplicantStep3
                                                        .value
                                                        .loanPackageMaximumLoanTenureBased!
                                                        .lowestFloating!
                                                        .mortgageRepaymentTable![
                                                            index]
                                                        .month
                                                        .toString() ==
                                                    '12'
                                                ? Colors.grey.shade400
                                                : Colors.white,
                                          ),
                                          children: [
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                      .thirtyOneToThirtyFiveList[
                                                          index]!
                                                      .year
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                      .thirtyOneToThirtyFiveList[
                                                          index]!
                                                      .month
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                          .thirtyOneToThirtyFiveList[
                                                              index]!
                                                          .interestRate
                                                          .toString() +
                                                      "%",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                        AppConfig.extraDecimalAdd2Digit(newPurchaseExistingLeadController
                                                      .thirtyOneToThirtyFiveList[
                                                          index]!
                                                      .installmentAmount
                                                      .toString()),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .thirtyOneToThirtyFiveList[
                                                          index]!
                                                      .interestRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .thirtyOneToThirtyFiveList[
                                                          index]!
                                                      .principalRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .thirtyOneToThirtyFiveList[
                                                          index]!
                                                      .outstandingAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                        AppConfig.extraDecimalAdd2Digit(newPurchaseExistingLeadController
                                                      .thirtyOneToThirtyFiveList[
                                                          index]!
                                                      .cumInterestPaid
                                                      .toString()),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  tableTitle(windowHeight) {
    return Table(
      // defaultColumnWidth: FixedColumnWidth(windowWidth * 0.001),
      border: TableBorder.all(
          color: const Color(0XFFE2E2E2), style: BorderStyle.solid, width: 2),
      children: [
        TableRow(
          children: [
            TableTitleRowData(
              windowHeight: windowHeight,
              title: 'Year',
            ),
            TableTitleRowData(
              windowHeight: windowHeight,
              title: 'Month',
            ),
            TableTitleRowData(
              windowHeight: windowHeight,
              title: 'Interest Rate',
            ),
            TableTitleRowData(
              windowHeight: windowHeight,
              title: 'Installment Amount',
            ),
            TableTitleRowData(
              windowHeight: windowHeight,
              title: 'Interest Repayment',
            ),
            TableTitleRowData(
              windowHeight: windowHeight,
              title: 'Principal Repayment',
            ),
            TableTitleRowData(
              windowHeight: windowHeight,
              title: 'Outstanding Amount',
            ),
            TableTitleRowData(
              windowHeight: windowHeight,
              title: 'Cum Interest Paid',
            ),
          ],
        ),
      ],
    );
  }
}

class DataRow extends StatelessWidget {
  final String stringData;
  const DataRow({
    Key? key,
    required this.windowHeight,
    required this.stringData,
  }) : super(key: key);

  final double windowHeight;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            stringData,
            style: TextStyle(fontSize: FontSize.s10),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}

class TableTitleRowData extends StatelessWidget {
  const TableTitleRowData({
    Key? key,
    required this.windowHeight,
    required this.title,
  }) : super(key: key);

  final double windowHeight;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Column(
      // mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          height: windowHeight * 0.006,
        ),
        Text(
          title,
          style: TextStyle(
            fontSize: FontSize.s9,
            color: AppColors.kPrimaryColor,
          ),
          textAlign: TextAlign.center,
        ),
        SizedBox(
          height: windowHeight * 0.006,
        ),
      ],
    );
  }
}
