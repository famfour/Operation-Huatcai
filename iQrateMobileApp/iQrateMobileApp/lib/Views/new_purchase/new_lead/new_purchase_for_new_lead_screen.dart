// ignore_for_file: unused_local_variable

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/buyer_stamp_duty_calculator_controller.dart';
import 'package:iqrate/DeviceManager/container_styles.dart';
import 'package:iqrate/DeviceManager/screen_constants.dart';
import 'package:iqrate/Widgets/default_appbar.dart';
import 'package:iqrate/Widgets/form_field_title.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Controller/new_purchase_controller.dart';
import 'package:iqrate/Widgets/secondary_button.dart';

import '../../../../DeviceManager/colors.dart';

class NewPurchaseForNewLeadScreen extends StatefulWidget {
  const NewPurchaseForNewLeadScreen({Key? key}) : super(key: key);

  @override
  State<NewPurchaseForNewLeadScreen> createState() =>
      _NewPurchaseForNewLeadScreenState();
}

class _NewPurchaseForNewLeadScreenState
    extends State<NewPurchaseForNewLeadScreen> {
  final NewPurchaseController newPurchaseController =
      Get.put(NewPurchaseController());

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    final BuyerStampDutyCalculatorController
        buyerStampDutyCalculatorController =
        Get.put(BuyerStampDutyCalculatorController());
    return Scaffold(
      appBar: DefaultAppBar2(
        title: "New Purchase",
        windowHeight: windowHeight * 0.09,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                child: Center(
                  child: Image.asset('assets/images/new-purchase.png'),
                ),
              ),
              SizedBox(
                height: windowHeight * 0.03,
              ),
              Center(
                child: Text(
                  'Find out with our calculator!',
                  style: TextStyle(
                    color: const Color(0XFF777373),
                    fontSize: FontSize.s24,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              SizedBox(
                height: windowHeight * 0.03,
              ),
              const FormFieldTitle(title: 'Select Number of Loan Applicants'),
              SizedBox(height: windowHeight * 0.01),
              Container(
                width: windowWidth,
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
                decoration: BoxDecoration(
                    border: Border.all(
                        color: AppColors.formFieldBorderColor, width: 1.0),
                    borderRadius: const BorderRadius.all(Radius.circular(
                            10) //                 <--- border radius here
                        )),
                child: DropdownButtonHideUnderline(
                  child: DropdownButton<String>(
                    hint: const Text('Select'),
                    // Not necessary for Option 1
                    value: newPurchaseController.selectedApplicant,
                    onChanged: (newValue) {
                      newPurchaseController.selectedApplicant = newValue;
                      setState(() {});
                    },
                    items: newPurchaseController.dropdownLoanApplicant
                        .map((location) {
                      return DropdownMenuItem(
                        child: SizedBox(
                            width: windowWidth * 0.7, child: Text(location)),
                        value: location,
                      );
                    }).toList(),
                  ),
                ),
              ),
              SizedBox(
                height: windowHeight * 0.03,
              ),
              Center(
                child: PrimaryButton(
                  windowHeight: windowHeight,
                  windowWidth: windowWidth,
                  buttonTitle: "Submit",
                  onPressed: () {
                    newPurchaseController.onSubmitButtonTap();
                  },
                ),
              ),
              SizedBox(
                height: windowHeight * 0.03,
              ),
              SecondaryButton(
                windowHeight: windowHeight,
                windowWidth: windowWidth,
                kGradientBoxDecoration:
                    ContainerStyles.kGradientBoxDecorationSecondaryButton,
                kInnerDecoration:
                    ContainerStyles.kInnerDecorationSecondaryButton,
                onPressed: () {
                  newPurchaseController.onCancel();
                },
                buttonTitle: "Cancel",
              ),
            ],
          ),
        ),
      ),
    );
  }
}
