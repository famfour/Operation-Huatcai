// ignore_for_file: unused_local_variable

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/buyer_stamp_duty_calculator_controller.dart';
import 'package:iqrate/DeviceManager/screen_constants.dart';
import 'package:iqrate/Views/more_view.dart';
import 'package:iqrate/Widgets/default_appbar.dart';
import 'package:iqrate/Controller/new_purchase_controller.dart';

class NewPurchaseScreen extends StatefulWidget {
  const NewPurchaseScreen({Key? key}) : super(key: key);

  @override
  State<NewPurchaseScreen> createState() => _NewPurchaseScreenState();
}

class _NewPurchaseScreenState extends State<NewPurchaseScreen> {
  final NewPurchaseController newPurchaseController =
      Get.put(NewPurchaseController());

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    final BuyerStampDutyCalculatorController
        buyerStampDutyCalculatorController =
        Get.put(BuyerStampDutyCalculatorController());
    return Scaffold(
      appBar: DefaultAppBar2(
        title: "New Purchase",
        windowHeight: windowHeight * 0.09,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                child: Center(
                  child: Image.asset('assets/images/new-purchase.png'),
                ),
              ),
              SizedBox(
                height: windowHeight * 0.03,
              ),
              Text(
                'Find out with our calculator!',
                style: TextStyle(
                  color: const Color(0XFF777373),
                  fontSize: FontSize.s24,
                  fontWeight: FontWeight.w600,
                ),
              ),
              SizedBox(
                height: windowHeight * 0.03,
              ),
              GridView.count(
                shrinkWrap: true,
                primary: false,
                padding: EdgeInsets.all(windowHeight * 0.02),
                crossAxisCount: 2,
                mainAxisSpacing: windowHeight * 0.02,
                crossAxisSpacing: windowHeight * 0.02,
                children: [
                  GridItemWithoutSubText(
                    assetImage: 'assets/images/existing-lead.svg',
                    title: 'Calculate For\nExisting Lead',
                    onTap: () {
                      newPurchaseController.onTapCalculateExistingLead();
                    },
                  ),
                  GridItemWithoutSubText(
                    assetImage: 'assets/images/refin1.svg',
                    title: 'Calculate for\nNew Lead(s)',
                    onTap: () {
                      newPurchaseController.onTapCalculateNewLead();
                    },
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
