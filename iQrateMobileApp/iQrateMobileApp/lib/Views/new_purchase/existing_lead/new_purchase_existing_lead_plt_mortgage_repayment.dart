import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Utils/config.dart';

import '../../../Controller/new_purchase_existing_lead_controller.dart';
import '../../../DeviceManager/colors.dart';
import '../../../DeviceManager/screen_constants.dart';
import '../../../Widgets/default_appbar.dart';

class NewPurchaseExistingLeadPltMortgageRepayment extends StatelessWidget {
  NewPurchaseExistingLeadPltMortgageRepayment({Key? key}) : super(key: key);

  final NewPurchaseExistingLeadController newPurchaseExistingLeadController =
      Get.find();

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    // double windowWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: DefaultAppBar2(
        title: "New Purchase",
        windowHeight: windowHeight * 0.09,
      ),
      body: InteractiveViewer(
        minScale: 1,
        maxScale: 4,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: windowHeight * 0.02,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15.0),
              child: Text(
                'Mortgage Repayment Table',
                style: TextStyle(
                  color: AppColors.kPrimaryColor,
                  fontSize: FontSize.s22,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(
              height: windowHeight * 0.003,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15.0),
              child: Text(
                newPurchaseExistingLeadController
                    .titleForMortgageTableScreen.value
                    .toString(),
                style: TextStyle(
                  color: const Color(0XFF9A9696),
                  fontSize: FontSize.s20,
                ),
              ),
            ),
            SizedBox(
              height: windowHeight * 0.02,
            ),
            tableTitle(windowHeight),
            Expanded(
              child: ListView(
                children: [
                  SingleChildScrollView(
                    child: Column(
                      children: [
                        if (newPurchaseExistingLeadController
                            .yearOneToFiveListPlt.isNotEmpty)
                          Card(
                            elevation: 2,
                            child: ExpansionTile(
                              title: Text(
                                'Year 1-5',
                                style: TextStyle(
                                  fontSize: FontSize.s19,
                                  color: Colors.black,
                                ),
                              ),
                              children: [
                                ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: newPurchaseExistingLeadController
                                      .yearOneToFiveListPlt.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Table(
                                      border: TableBorder.all(
                                        color: const Color(0XFFE2E2E2),
                                        style: BorderStyle.solid,
                                        width: 2,
                                      ),
                                      children: [
                                        TableRow(
                                          decoration: BoxDecoration(
                                            color: newPurchaseExistingLeadController
                                                        .resultMainApplicantStep3
                                                        .value
                                                        .loanPackagePreferredLoanTenureBased!
                                                        .lowestFloating!
                                                        .mortgageRepaymentTable![
                                                            index]
                                                        .month
                                                        .toString() ==
                                                    '12'
                                                ? Colors.grey.shade400
                                                : Colors.white,
                                          ),
                                          children: [
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                      .yearOneToFiveListPlt[
                                                          index]!
                                                      .year
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                      .yearOneToFiveListPlt[
                                                          index]!
                                                      .month
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                          .yearOneToFiveListPlt[
                                                              index]!
                                                          .interestRate
                                                          .toString() +
                                                      "%",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                        AppConfig.extraDecimalAdd2Digit(newPurchaseExistingLeadController
                                                      .yearOneToFiveListPlt[
                                                          index]!
                                                      .installmentAmount
                                                      .toString()),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .yearOneToFiveListPlt[
                                                          index]!
                                                      .interestRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .yearOneToFiveListPlt[
                                                          index]!
                                                      .principalRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .yearOneToFiveListPlt[
                                                          index]!
                                                      .outstandingAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                        AppConfig.extraDecimalAdd2Digit(newPurchaseExistingLeadController
                                                      .yearOneToFiveListPlt[
                                                          index]!
                                                      .cumInterestPaid
                                                      .toString()),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        if (newPurchaseExistingLeadController
                            .yearSixToTenListPlt.isNotEmpty)
                          Card(
                            elevation: 2,
                            child: ExpansionTile(
                              title: Text(
                                'Year 6-10',
                                style: TextStyle(
                                  fontSize: FontSize.s19,
                                  color: Colors.black,
                                ),
                              ),
                              children: [
                                ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: newPurchaseExistingLeadController
                                      .yearSixToTenListPlt.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Table(
                                      border: TableBorder.all(
                                        color: const Color(0XFFE2E2E2),
                                        style: BorderStyle.solid,
                                        width: 2,
                                      ),
                                      children: [
                                        TableRow(
                                          decoration: BoxDecoration(
                                            color: newPurchaseExistingLeadController
                                                        .resultMainApplicantStep3
                                                        .value
                                                        .loanPackagePreferredLoanTenureBased!
                                                        .lowestFloating!
                                                        .mortgageRepaymentTable![
                                                            index]
                                                        .month
                                                        .toString() ==
                                                    '12'
                                                ? Colors.grey.shade400
                                                : Colors.white,
                                          ),
                                          children: [
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                      .yearSixToTenListPlt[
                                                          index]!
                                                      .year
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                      .yearSixToTenListPlt[
                                                          index]!
                                                      .month
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                          .yearSixToTenListPlt[
                                                              index]!
                                                          .interestRate
                                                          .toString() +
                                                      "%",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                        AppConfig.extraDecimalAdd2Digit(newPurchaseExistingLeadController
                                                      .yearSixToTenListPlt[
                                                          index]!
                                                      .installmentAmount
                                                      .toString()),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .yearSixToTenListPlt[
                                                          index]!
                                                      .interestRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .yearSixToTenListPlt[
                                                          index]!
                                                      .principalRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .yearSixToTenListPlt[
                                                          index]!
                                                      .outstandingAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                        AppConfig.extraDecimalAdd2Digit(newPurchaseExistingLeadController
                                                      .yearSixToTenListPlt[
                                                          index]!
                                                      .cumInterestPaid
                                                      .toString()),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        if (newPurchaseExistingLeadController
                            .yearElevenToFifteenListPlt.isNotEmpty)
                          Card(
                            elevation: 2,
                            child: ExpansionTile(
                              title: Text(
                                'Year 11-15',
                                style: TextStyle(
                                  fontSize: FontSize.s19,
                                  color: Colors.black,
                                ),
                              ),
                              children: [
                                ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: newPurchaseExistingLeadController
                                      .yearElevenToFifteenListPlt.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Table(
                                      border: TableBorder.all(
                                        color: const Color(0XFFE2E2E2),
                                        style: BorderStyle.solid,
                                        width: 2,
                                      ),
                                      children: [
                                        TableRow(
                                          decoration: BoxDecoration(
                                            color: newPurchaseExistingLeadController
                                                        .resultMainApplicantStep3
                                                        .value
                                                        .loanPackagePreferredLoanTenureBased!
                                                        .lowestFloating!
                                                        .mortgageRepaymentTable![
                                                            index]
                                                        .month
                                                        .toString() ==
                                                    '12'
                                                ? Colors.grey.shade400
                                                : Colors.white,
                                          ),
                                          children: [
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                      .yearElevenToFifteenListPlt[
                                                          index]!
                                                      .year
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                      .yearElevenToFifteenListPlt[
                                                          index]!
                                                      .month
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                          .yearElevenToFifteenListPlt[
                                                              index]!
                                                          .interestRate
                                                          .toString() +
                                                      "%",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                        AppConfig.extraDecimalAdd2Digit(newPurchaseExistingLeadController
                                                      .yearElevenToFifteenListPlt[
                                                          index]!
                                                      .installmentAmount
                                                      .toString()),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .yearElevenToFifteenListPlt[
                                                          index]!
                                                      .interestRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .yearElevenToFifteenListPlt[
                                                          index]!
                                                      .principalRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .yearElevenToFifteenListPlt[
                                                          index]!
                                                      .outstandingAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                        AppConfig.extraDecimalAdd2Digit(newPurchaseExistingLeadController
                                                      .yearElevenToFifteenListPlt[
                                                          index]!
                                                      .cumInterestPaid
                                                      .toString()),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        if (newPurchaseExistingLeadController
                            .sixteenToTwentyListPlt.isNotEmpty)
                          Card(
                            elevation: 2,
                            child: ExpansionTile(
                              title: Text(
                                'Year 16-20',
                                style: TextStyle(
                                  fontSize: FontSize.s19,
                                  color: Colors.black,
                                ),
                              ),
                              children: [
                                ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: newPurchaseExistingLeadController
                                      .sixteenToTwentyListPlt.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Table(
                                      border: TableBorder.all(
                                        color: const Color(0XFFE2E2E2),
                                        style: BorderStyle.solid,
                                        width: 2,
                                      ),
                                      children: [
                                        TableRow(
                                          decoration: BoxDecoration(
                                            color: newPurchaseExistingLeadController
                                                        .resultMainApplicantStep3
                                                        .value
                                                        .loanPackagePreferredLoanTenureBased!
                                                        .lowestFloating!
                                                        .mortgageRepaymentTable![
                                                            index]
                                                        .month
                                                        .toString() ==
                                                    '12'
                                                ? Colors.grey.shade400
                                                : Colors.white,
                                          ),
                                          children: [
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                      .sixteenToTwentyListPlt[
                                                          index]!
                                                      .year
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                      .sixteenToTwentyListPlt[
                                                          index]!
                                                      .month
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                          .sixteenToTwentyListPlt[
                                                              index]!
                                                          .interestRate
                                                          .toString() +
                                                      "%",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                        AppConfig.extraDecimalAdd2Digit(newPurchaseExistingLeadController
                                                      .sixteenToTwentyListPlt[
                                                          index]!
                                                      .installmentAmount
                                                      .toString()),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .sixteenToTwentyListPlt[
                                                          index]!
                                                      .interestRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .sixteenToTwentyListPlt[
                                                          index]!
                                                      .principalRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .sixteenToTwentyListPlt[
                                                          index]!
                                                      .outstandingAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                        AppConfig.extraDecimalAdd2Digit(newPurchaseExistingLeadController
                                                      .sixteenToTwentyListPlt[
                                                          index]!
                                                      .cumInterestPaid
                                                      .toString()),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        if (newPurchaseExistingLeadController
                            .twentyOneToTwnetyFiveListPlt.isNotEmpty)
                          Card(
                            elevation: 2,
                            child: ExpansionTile(
                              title: Text(
                                'Year 21-25',
                                style: TextStyle(
                                  fontSize: FontSize.s19,
                                  color: Colors.black,
                                ),
                              ),
                              children: [
                                ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: newPurchaseExistingLeadController
                                      .twentyOneToTwnetyFiveListPlt.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Table(
                                      border: TableBorder.all(
                                        color: const Color(0XFFE2E2E2),
                                        style: BorderStyle.solid,
                                        width: 2,
                                      ),
                                      children: [
                                        TableRow(
                                          decoration: BoxDecoration(
                                            color: newPurchaseExistingLeadController
                                                        .resultMainApplicantStep3
                                                        .value
                                                        .loanPackagePreferredLoanTenureBased!
                                                        .lowestFloating!
                                                        .mortgageRepaymentTable![
                                                            index]
                                                        .month
                                                        .toString() ==
                                                    '12'
                                                ? Colors.grey.shade400
                                                : Colors.white,
                                          ),
                                          children: [
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                      .twentyOneToTwnetyFiveListPlt[
                                                          index]!
                                                      .year
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                      .twentyOneToTwnetyFiveListPlt[
                                                          index]!
                                                      .month
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                          .twentyOneToTwnetyFiveListPlt[
                                                              index]!
                                                          .interestRate
                                                          .toString() +
                                                      "%",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                        AppConfig.extraDecimalAdd2Digit(newPurchaseExistingLeadController
                                                      .twentyOneToTwnetyFiveListPlt[
                                                          index]!
                                                      .installmentAmount
                                                      .toString()),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .twentyOneToTwnetyFiveListPlt[
                                                          index]!
                                                      .interestRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .twentyOneToTwnetyFiveListPlt[
                                                          index]!
                                                      .principalRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .twentyOneToTwnetyFiveList[
                                                          index]!
                                                      .outstandingAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                        AppConfig.extraDecimalAdd2Digit(newPurchaseExistingLeadController
                                                      .twentyOneToTwnetyFiveList[
                                                          index]!
                                                      .cumInterestPaid
                                                      .toString()),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        if (newPurchaseExistingLeadController
                            .twentySixToThirtyListPlt.isNotEmpty)
                          Card(
                            elevation: 2,
                            child: ExpansionTile(
                              title: Text(
                                'Year 26-30',
                                style: TextStyle(
                                  fontSize: FontSize.s19,
                                  color: Colors.black,
                                ),
                              ),
                              children: [
                                ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: newPurchaseExistingLeadController
                                      .twentySixToThirtyListPlt.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Table(
                                      border: TableBorder.all(
                                        color: const Color(0XFFE2E2E2),
                                        style: BorderStyle.solid,
                                        width: 2,
                                      ),
                                      children: [
                                        TableRow(
                                          decoration: BoxDecoration(
                                            color: newPurchaseExistingLeadController
                                                        .resultMainApplicantStep3
                                                        .value
                                                        .loanPackagePreferredLoanTenureBased!
                                                        .lowestFloating!
                                                        .mortgageRepaymentTable![
                                                            index]
                                                        .month
                                                        .toString() ==
                                                    '12'
                                                ? Colors.grey.shade400
                                                : Colors.white,
                                          ),
                                          children: [
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                      .twentySixToThirtyListPlt[
                                                          index]!
                                                      .year
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                      .twentySixToThirtyListPlt[
                                                          index]!
                                                      .month
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                          .twentySixToThirtyListPlt[
                                                              index]!
                                                          .interestRate
                                                          .toString() +
                                                      "%",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                        AppConfig.extraDecimalAdd2Digit(newPurchaseExistingLeadController
                                                      .twentySixToThirtyListPlt[
                                                          index]!
                                                      .installmentAmount
                                                      .toString()),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .twentySixToThirtyListPlt[
                                                          index]!
                                                      .interestRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .twentySixToThirtyListPlt[
                                                          index]!
                                                      .principalRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .twentySixToThirtyListPlt[
                                                          index]!
                                                      .outstandingAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                        AppConfig.extraDecimalAdd2Digit(newPurchaseExistingLeadController
                                                      .twentySixToThirtyListPlt[
                                                          index]!
                                                      .cumInterestPaid
                                                      .toString()),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        if (newPurchaseExistingLeadController
                            .thirtyOneToThirtyFiveListPlt.isNotEmpty)
                          Card(
                            elevation: 2,
                            child: ExpansionTile(
                              title: Text(
                                'Year 31-35',
                                style: TextStyle(
                                  fontSize: FontSize.s19,
                                  color: Colors.black,
                                ),
                              ),
                              children: [
                                ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: newPurchaseExistingLeadController
                                      .thirtyOneToThirtyFiveListPlt.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Table(
                                      border: TableBorder.all(
                                        color: const Color(0XFFE2E2E2),
                                        style: BorderStyle.solid,
                                        width: 2,
                                      ),
                                      children: [
                                        TableRow(
                                          decoration: BoxDecoration(
                                            color: newPurchaseExistingLeadController
                                                        .resultMainApplicantStep3
                                                        .value
                                                        .loanPackagePreferredLoanTenureBased!
                                                        .lowestFloating!
                                                        .mortgageRepaymentTable![
                                                            index]
                                                        .month
                                                        .toString() ==
                                                    '12'
                                                ? Colors.grey.shade400
                                                : Colors.white,
                                          ),
                                          children: [
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                      .thirtyOneToThirtyFiveListPlt[
                                                          index]!
                                                      .year
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                      .thirtyOneToThirtyFiveListPlt[
                                                          index]!
                                                      .month
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  newPurchaseExistingLeadController
                                                          .thirtyOneToThirtyFiveListPlt[
                                                              index]!
                                                          .interestRate
                                                          .toString() +
                                                      "%",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                        AppConfig.extraDecimalAdd2Digit(newPurchaseExistingLeadController
                                                      .thirtyOneToThirtyFiveListPlt[
                                                          index]!
                                                      .installmentAmount
                                                      .toString()),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .thirtyOneToThirtyFiveListPlt[
                                                          index]!
                                                      .interestRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .thirtyOneToThirtyFiveListPlt[
                                                          index]!
                                                      .principalRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  newPurchaseExistingLeadController
                                                      .thirtyOneToThirtyFiveListPlt[
                                                          index]!
                                                      .outstandingAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                        AppConfig.extraDecimalAdd2Digit(newPurchaseExistingLeadController
                                                      .thirtyOneToThirtyFiveListPlt[
                                                          index]!
                                                      .cumInterestPaid
                                                      .toString()),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  tableTitle(windowHeight) {
    return Table(
      // defaultColumnWidth: FixedColumnWidth(windowWidth * 0.001),
      border: TableBorder.all(
          color: const Color(0XFFE2E2E2), style: BorderStyle.solid, width: 2),
      children: [
        TableRow(
          children: [
            TableTitleRowData(
              windowHeight: windowHeight,
              title: 'Year',
            ),
            TableTitleRowData(
              windowHeight: windowHeight,
              title: 'Month',
            ),
            TableTitleRowData(
              windowHeight: windowHeight,
              title: 'Interest Rate',
            ),
            TableTitleRowData(
              windowHeight: windowHeight,
              title: 'Installment Amount',
            ),
            TableTitleRowData(
              windowHeight: windowHeight,
              title: 'Interest Repayment',
            ),
            TableTitleRowData(
              windowHeight: windowHeight,
              title: 'Principal Repayment',
            ),
            TableTitleRowData(
              windowHeight: windowHeight,
              title: 'Outstanding Amount',
            ),
            TableTitleRowData(
              windowHeight: windowHeight,
              title: 'Cum Interest Paid',
            ),
          ],
        ),
      ],
    );
  }
}

class DataRow extends StatelessWidget {
  final String stringData;
  const DataRow({
    Key? key,
    required this.windowHeight,
    required this.stringData,
  }) : super(key: key);

  final double windowHeight;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            stringData,
            style: TextStyle(fontSize: FontSize.s10),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}

class TableTitleRowData extends StatelessWidget {
  const TableTitleRowData({
    Key? key,
    required this.windowHeight,
    required this.title,
  }) : super(key: key);

  final double windowHeight;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Column(
      // mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          height: windowHeight * 0.006,
        ),
        Text(
          title,
          style: TextStyle(
            fontSize: FontSize.s9,
            color: AppColors.kPrimaryColor,
          ),
          textAlign: TextAlign.center,
        ),
        SizedBox(
          height: windowHeight * 0.006,
        ),
      ],
    );
  }
}
