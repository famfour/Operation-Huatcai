// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:im_stepper/stepper.dart';
import 'package:intl/intl.dart';
import 'package:iqrate/Controller/new_purchase_existing_lead_controller.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/container_styles.dart';
import 'package:iqrate/DeviceManager/screen_constants.dart';
import 'package:iqrate/Model/response_model.dart/new_purchase_response_model.dart';
import 'package:iqrate/Views/profile_screen.dart';
import 'package:iqrate/Views/rates/currency_amount_textField.dart';
import 'package:iqrate/Widgets/currency_amount_with_onchanged_textField.dart';
import 'package:iqrate/Widgets/default_appbar.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/secondary_button.dart';
import 'package:pie_chart/pie_chart.dart';

import '../../../DeviceManager/text_styles.dart';
import '../../../Widgets/form_field_title.dart';
import '../../../Widgets/require_text_field.dart';

class NewPurchaseExistingLeadCalculator extends StatefulWidget {
  const NewPurchaseExistingLeadCalculator({Key? key}) : super(key: key);

  @override
  State<NewPurchaseExistingLeadCalculator> createState() =>
      _NewPurchaseExistingLeadCalculatorState();
}

class _NewPurchaseExistingLeadCalculatorState
    extends State<NewPurchaseExistingLeadCalculator> {
  final NewPurchaseExistingLeadController newPurchaseController =
      Get.put(NewPurchaseExistingLeadController());
  int activeStep = 0; // Initial step set to 0.

  // OPTIONAL: can be set directly.
  int dotCount = 5;
  late double windowHeight;
  late double windowWidth;

  @override
  Widget build(BuildContext context) {
    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: DefaultAppBar2(
        title: "New Purchase",
        windowHeight: windowHeight * 0.09,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              NumberStepper(
                enableNextPreviousButtons: false,
                enableStepTapping: false,
                stepRadius: windowWidth * 0.03,
                lineLength: windowWidth * 0.33,
                lineDotRadius: 0.1,
                stepPadding: 0,
                // stepReachedAnimationEffect: Curves.easeIn,
                // stepReachedAnimationDuration: const Duration(seconds: 15),
                activeStepColor: AppColors.kPrimaryColor,
                activeStepBorderPadding: 0,
                activeStepBorderColor: AppColors.kPrimaryColor,
                stepColor: const Color(0XFFD0C8C9),
                numbers: const [1, 2, 3],
                numberStyle: TextStyle(
                  color: Colors.white,
                  fontSize: FontSize.s18,
                  fontWeight: FontWeight.w400,
                ),
                activeStep: activeStep,

                // This ensures step-tapping updates the activeStep.
                onStepReached: (index) {
                  setState(() {
                    activeStep = index;
                  });
                },
              ),
              activeStep == 0
                  ? Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        if (newPurchaseController.leadDetails.length == 1 ||
                            newPurchaseController.leadDetails.length == 2 ||
                            newPurchaseController.leadDetails.length == 3 ||
                            newPurchaseController.leadDetails.length == 4)
                          Obx(
                            () {
                              return NewPurchaseApplicantDetails(
                                applicantNumber: "1",
                                totalMonthlyIncome: newPurchaseController
                                    .totalMonthlyIncomeMainApplicant
                                    .toStringAsFixed(2),
                                totalFinancialCommitments: newPurchaseController
                                    .totalFinancialCommitmentsMainApplicant
                                    .toStringAsFixed(2),
                                nameController: newPurchaseController
                                    .nameControllerMainApplicant,
                                emailController: newPurchaseController
                                    .emailControllerMainApplicant,
                                phoneController: newPurchaseController
                                    .phoneControllerMainApplicant,
                                countryCodeController: newPurchaseController
                                    .countryCodeControllerMainApplicant,
                                dobController: newPurchaseController
                                    .dobControllerMainApplicant,
                                nationalityController: newPurchaseController
                                    .nationalityControllerMainApplicant,
                                selectedEmploymentTypeController:
                                    newPurchaseController
                                        .selectedEmploymentTypeMainApplicant!,
                                monthlyFixedIncomeController: newPurchaseController
                                    .monthlyFixedIncomeControllerMainApplicant,
                                annualIncomeController: newPurchaseController
                                    .annualIncomeControllerMainApplicant,
                                monthlyRentalIncomeController: newPurchaseController
                                    .monthlyRentalIncomeControllerMainApplicant,
                                housingLoanController: newPurchaseController
                                    .housingLoansInstallmentControllerMainApplicant,
                                carLoanController: newPurchaseController
                                    .carLoansInstallmentControllerMainApplicant,
                                personalEducationLoanController:
                                    newPurchaseController
                                        .personalEducationRenovationControllerMainApplicant,
                                cardRepaymentController: newPurchaseController
                                    .cardRepaymentAmountControllerMainApplicant,
                                gurantorLoanController: newPurchaseController
                                    .guarantorLoanAmountControllerMainApplicant,
                                commercialPropertyPersonalLoanController:
                                    newPurchaseController
                                        .commercialPropertyLoanPersonalControllerMainApplicant,
                                commercialPropertyCompanyLoanController:
                                    newPurchaseController
                                        .commercialPropertyLoanCompanyControllerMainApplicant,
                                housingLoans: newPurchaseController
                                    .selectedHousingLoanMainApplicant!.value,
                                selectedProperties: newPurchaseController
                                    .selectedPropertiesMainApplicant!,
                              );
                            },
                          ),
                        if (newPurchaseController.leadDetails.length == 2 ||
                            newPurchaseController.leadDetails.length == 3 ||
                            newPurchaseController.leadDetails.length == 4)
                          Obx(
                            () => NewPurchaseApplicantDetails(
                              applicantNumber: "2",
                              totalMonthlyIncome: newPurchaseController
                                  .totalMonthlyIncomeJointApplicant1
                                  .toStringAsFixed(2),
                              totalFinancialCommitments: newPurchaseController
                                  .totalFinancialCommitmentsJointApplicant1
                                  .toStringAsFixed(2),
                              nameController: newPurchaseController
                                  .nameControllerJointApplicant1,
                              emailController: newPurchaseController
                                  .emailControllerJointApplicant1,
                              phoneController: newPurchaseController
                                  .phoneControllerJointApplicant1,
                              countryCodeController: newPurchaseController
                                  .countryCodeControllerJointApplicant1,
                              dobController: newPurchaseController
                                  .dobControllerJointApplicant1,
                              nationalityController: newPurchaseController
                                  .nationalityControllerJointApplicant1,
                              selectedEmploymentTypeController:
                                  newPurchaseController
                                      .selectedEmploymentTypeJointApplicant1!,
                              monthlyFixedIncomeController: newPurchaseController
                                  .monthlyFixedIncomeControllerJointApplicant1,
                              annualIncomeController: newPurchaseController
                                  .annualIncomeControllerJointApplicant1,
                              monthlyRentalIncomeController: newPurchaseController
                                  .monthlyRentalIncomeControllerJointApplicant1,
                              housingLoanController: newPurchaseController
                                  .housingLoansInstallmentControllerJointApplicant1,
                              carLoanController: newPurchaseController
                                  .carLoansInstallmentControllerJointApplicant1,
                              personalEducationLoanController: newPurchaseController
                                  .personalEducationRenovationControllerJointApplicant1,
                              cardRepaymentController: newPurchaseController
                                  .cardRepaymentAmountControllerJointApplicant1,
                              gurantorLoanController: newPurchaseController
                                  .guarantorLoanAmountControllerJointApplicant1,
                              commercialPropertyPersonalLoanController:
                                  newPurchaseController
                                      .commercialPropertyLoanPersonalControllerJointApplicant1,
                              commercialPropertyCompanyLoanController:
                                  newPurchaseController
                                      .commercialPropertyLoanCompanyControllerJointApplicant1,
                              housingLoans: newPurchaseController
                                  .selectedHousingLoanJointApplicant1!.value,
                              selectedProperties: newPurchaseController
                                  .selectedPropertiesJointApplicant1!,
                            ),
                          ),
                        if (newPurchaseController.leadDetails.length == 3 ||
                            newPurchaseController.leadDetails.length == 4)
                          Obx(
                            () => NewPurchaseApplicantDetails(
                              applicantNumber: "3",
                              totalMonthlyIncome: newPurchaseController
                                  .totalMonthlyIncomeJointApplicant2
                                  .toStringAsFixed(2),
                              totalFinancialCommitments: newPurchaseController
                                  .totalFinancialCommitmentsJointApplicant2
                                  .toStringAsFixed(2),
                              nameController: newPurchaseController
                                  .nameControllerJointApplicant2,
                              emailController: newPurchaseController
                                  .emailControllerJointApplicant2,
                              phoneController: newPurchaseController
                                  .phoneControllerJointApplicant2,
                              countryCodeController: newPurchaseController
                                  .countryCodeControllerJointApplicant2,
                              dobController: newPurchaseController
                                  .dobControllerJointApplicant2,
                              nationalityController: newPurchaseController
                                  .nationalityControllerJointApplicant2,
                              selectedEmploymentTypeController:
                                  newPurchaseController
                                      .selectedEmploymentTypeJointApplicant2!,
                              monthlyFixedIncomeController: newPurchaseController
                                  .monthlyFixedIncomeControllerJointApplicant2,
                              annualIncomeController: newPurchaseController
                                  .annualIncomeControllerJointApplicant2,
                              monthlyRentalIncomeController: newPurchaseController
                                  .monthlyRentalIncomeControllerJointApplicant2,
                              housingLoanController: newPurchaseController
                                  .housingLoansInstallmentControllerJointApplicant2,
                              carLoanController: newPurchaseController
                                  .carLoansInstallmentControllerJointApplicant2,
                              personalEducationLoanController: newPurchaseController
                                  .personalEducationRenovationControllerJointApplicant2,
                              cardRepaymentController: newPurchaseController
                                  .cardRepaymentAmountControllerJointApplicant2,
                              gurantorLoanController: newPurchaseController
                                  .guarantorLoanAmountControllerJointApplicant2,
                              commercialPropertyPersonalLoanController:
                                  newPurchaseController
                                      .commercialPropertyLoanPersonalControllerJointApplicant2,
                              commercialPropertyCompanyLoanController:
                                  newPurchaseController
                                      .commercialPropertyLoanCompanyControllerJointApplicant2,
                              housingLoans: newPurchaseController
                                  .selectedHousingLoanJointApplicant2!.value,
                              selectedProperties: newPurchaseController
                                  .selectedPropertiesJointApplicant2!,
                            ),
                          ),
                        if (newPurchaseController.leadDetails.length == 4)
                          Obx(
                            () => NewPurchaseApplicantDetails(
                              applicantNumber: "4",
                              totalMonthlyIncome: newPurchaseController
                                  .totalMonthlyIncomeJointApplicant3
                                  .toStringAsFixed(2),
                              totalFinancialCommitments: newPurchaseController
                                  .totalFinancialCommitmentsJointApplicant3
                                  .toStringAsFixed(2),
                              nameController: newPurchaseController
                                  .nameControllerJointApplicant3,
                              emailController: newPurchaseController
                                  .emailControllerJointApplicant3,
                              phoneController: newPurchaseController
                                  .phoneControllerJointApplicant3,
                              countryCodeController: newPurchaseController
                                  .countryCodeControllerJointApplicant3,
                              dobController: newPurchaseController
                                  .dobControllerJointApplicant3,
                              nationalityController: newPurchaseController
                                  .nationalityControllerJointApplicant3,
                              selectedEmploymentTypeController:
                                  newPurchaseController
                                      .selectedEmploymentTypeJointApplicant3!,
                              monthlyFixedIncomeController: newPurchaseController
                                  .monthlyFixedIncomeControllerJointApplicant3,
                              annualIncomeController: newPurchaseController
                                  .annualIncomeControllerJointApplicant3,
                              monthlyRentalIncomeController: newPurchaseController
                                  .monthlyRentalIncomeControllerJointApplicant3,
                              housingLoanController: newPurchaseController
                                  .housingLoansInstallmentControllerJointApplicant3,
                              carLoanController: newPurchaseController
                                  .carLoansInstallmentControllerJointApplicant3,
                              personalEducationLoanController: newPurchaseController
                                  .personalEducationRenovationControllerJointApplicant3,
                              cardRepaymentController: newPurchaseController
                                  .cardRepaymentAmountControllerJointApplicant3,
                              gurantorLoanController: newPurchaseController
                                  .guarantorLoanAmountControllerJointApplicant3,
                              commercialPropertyPersonalLoanController:
                                  newPurchaseController
                                      .commercialPropertyLoanPersonalControllerJointApplicant3,
                              commercialPropertyCompanyLoanController:
                                  newPurchaseController
                                      .commercialPropertyLoanCompanyControllerJointApplicant3,
                              housingLoans: newPurchaseController
                                  .selectedHousingLoanJointApplicant3!.value,
                              selectedProperties: newPurchaseController
                                  .selectedPropertiesJointApplicant3!,
                            ),
                          ),
                        Obx(() {
                          return newPurchaseController.isCalculated.value
                              ? Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 15.0),
                                  child: Column(
                                    children: [
                                      Card(
                                        shape: const RoundedRectangleBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5))),
                                        elevation: 5,
                                        margin: const EdgeInsets.symmetric(
                                            horizontal: 5, vertical: 10),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 20.0,
                                                  top: 20,
                                                  bottom: 5),
                                              child: Text(
                                                "Total Monthly Income",
                                                style: TextStyles
                                                    .bankSubmissionTaskTextStyle1,
                                              ),
                                            ),
                                            const Divider(
                                                color: Colors.deepOrange),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 20.0,
                                                  top: 5,
                                                  bottom: 20),
                                              child: Text(
                                                "\$ " +
                                                    newPurchaseController
                                                        .totalMonthlyIncomeMainApplicant
                                                        .toStringAsFixed(2)
                                                        .replaceAllMapped(
                                                            RegExp(
                                                                r'(\d{1,3})(?=(\d{3})+(?!\d))'),
                                                            (Match m) =>
                                                                '${m[1]},'),
                                                style: TextStyles
                                                    .bankSubmissionBody,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(height: windowHeight * 0.02),
                                      Card(
                                        shape: const RoundedRectangleBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5))),
                                        elevation: 5,
                                        margin: const EdgeInsets.symmetric(
                                            horizontal: 5, vertical: 10),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 20.0,
                                                  top: 5,
                                                  bottom: 20),
                                              child: Text(
                                                "Total Financial Commitments",
                                                style: TextStyles
                                                    .bankSubmissionTaskTextStyle1,
                                              ),
                                            ),
                                            const Divider(
                                                color: Colors.deepOrange),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 20.0,
                                                  top: 5,
                                                  bottom: 20),
                                              child: Text(
                                                "\$ " +
                                                    newPurchaseController
                                                        .totalFinancialCommitmentsMainApplicant
                                                        .toStringAsFixed(2)
                                                        .replaceAllMapped(
                                                            RegExp(
                                                                r'(\d{1,3})(?=(\d{3})+(?!\d))'),
                                                            (Match m) =>
                                                                '${m[1]},'),
                                                style: TextStyles
                                                    .bankSubmissionBody,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(height: windowHeight * 0.04),
                                    ],
                                  ),
                                )
                              : const SizedBox();
                        }),
                      ],
                    )
                  : activeStep == 1
                      ? Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10.0),
                          child: SizedBox(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Obx(
                                  () {
                                    return NewPurchasePropertyDetails(
                                      applicantNumber: "1",
                                      propertyPriceController: newPurchaseController
                                          .propertyPriceControllerMainApplicant,
                                      otherFeesController: newPurchaseController
                                          .otherFeesControllerMainApplicant,
                                      selectedPropertyStatus: newPurchaseController
                                          .selectedPropertyStatusMainApplicant!,
                                      dropdownPropertyStatus:
                                          newPurchaseController
                                              .dropdownPropertyStatus,
                                      selectedPropertyType: newPurchaseController
                                          .selectedPropertyTypeMainApplicant!,
                                      dropdownPropertyType:
                                          newPurchaseController
                                              .dropdownPropertyType,
                                      dropdownLongerLoanTenure:
                                          newPurchaseController
                                              .dropdownLongerLoanTenure,
                                      selectedLongerLoanTenure:
                                          newPurchaseController
                                              .selectedLongerLoanTenureMainApplicant
                                              .value,
                                      loanTenureYearController:
                                          newPurchaseController
                                              .loanTenureYearControllerMainApplicant,
                                      loanAmountController: newPurchaseController
                                          .loanAmountControllerMainApplicant,
                                      loanToValue: newPurchaseController
                                          .loanToValueMainApplicant
                                          .toString(),
                                      maximumLoanTenure: newPurchaseController
                                          .maximumLoanTenureMainApplicant
                                          .toString(),
                                      buyerStampDuty: newPurchaseController
                                          .buyerStampDutyMainApplicant
                                          .toString(),
                                      additionalBuyerStampDuty:
                                          newPurchaseController
                                              .additionalBuyerStampDutyMainApplicant
                                              .toString(),
                                      legalFees: newPurchaseController
                                          .legalFeesMainApplicant
                                          .toString(),
                                      valuationFees: newPurchaseController
                                          .valuationFeesMainApplicant
                                          .toString(),
                                      maximumQualifiedLoan:
                                          newPurchaseController
                                              .maximumQualifiedLoanMainApplicant
                                              .toString(),
                                      maximumPropertyPrice:
                                          newPurchaseController
                                              .maximumPropertyPriceMainApplicant
                                              .toString(),
                                      msr: newPurchaseController
                                          .msrMainApplicant
                                          .toString(),
                                      tdsr: newPurchaseController
                                          .tdsrMainApplicant
                                          .toString(),
                                      fullFludgeAmount: newPurchaseController
                                          .fullFludgeAmountMainApplicant
                                          .toString(),
                                      fullUnfludgeAmount: newPurchaseController
                                          .fullUnfludgeAmountMainApplicant
                                          .toString(),
                                      loanTenure: newPurchaseController
                                          .loanTenureMainApplicant
                                          .toString(),
                                      newPurchaseResponseModel:
                                          newPurchaseController
                                              .resultMainApplicantStep2.value,
                                      selectedLoanType: newPurchaseController
                                          .selectedLoanTypeMainApplicant,
                                      remainingPledgeAmountMlt:
                                          newPurchaseController
                                              .fludgeAmountMainApplicant
                                              .toString(),
                                      remainingUnpledgeAmoutnMlt:
                                          newPurchaseController
                                              .unfludgeAmountMainApplicant
                                              .toString(),
                                      preferredPledgeAmountMltController:
                                          newPurchaseController
                                              .preferredPledgeAmountMltControllerMainApplicant,
                                      preferredUnpledgeAmountMltController:
                                          newPurchaseController
                                              .preferredUnpledgeAmountMltControllerMainApplicant,
                                      tempremainingUnpledgeAmoutnMlt:
                                          newPurchaseController
                                              .unfludgeAmountMainApplicant
                                              .toString(),
                                      tempRemainingPledgeAmountMlt:
                                          newPurchaseController
                                              .fludgeAmountMainApplicant
                                              .toString(),
                                      remainingPledgeAmountPlt:
                                          newPurchaseController
                                              .fludgeAmountMainApplicantPlt
                                              .toString(),
                                      remainingUnpledgeAmoutnPlt:
                                          newPurchaseController
                                              .unfludgeAmountMainApplicantPlt
                                              .toString(),
                                      tempRemainingPledgeAmountPlt:
                                          newPurchaseController
                                              .fludgeAmountMainApplicantPlt
                                              .toString(),
                                      tempremainingUnpledgeAmoutnPlt:
                                          newPurchaseController
                                              .unfludgeAmountMainApplicantPlt
                                              .toString(),
                                      preferredPledgeAmountPltController:
                                          newPurchaseController
                                              .preferredPledgeAmountPltControllerMainApplicant,
                                      preferredUnpledgeAmountPltController:
                                          newPurchaseController
                                              .preferredUnpledgeAmountPltControllerMainApplicant,
                                      pledgeAmountMlt: newPurchaseController
                                          .pledgeAmountMlt
                                          .toString(),
                                      unPLedgeAmountMlt: newPurchaseController
                                          .unpledgeAmountMlt
                                          .toString(),
                                      pledgeAmountPlt: newPurchaseController
                                          .pledgeAmountPlt
                                          .toString(),
                                      unPLedgeAmountPlt: newPurchaseController
                                          .unpledgeAmountPlt
                                          .toString(),
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        )
                      : SizedBox(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: windowHeight * 0.01,
                              ),
                              Container(
                                padding: EdgeInsets.zero,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: Colors.black12,
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 20, top: 10, bottom: 10),
                                      child: Text(
                                        "My Property Purchase Report ",
                                        style: TextStyles.leadsColorTextStyle2,
                                      ),
                                    ),
                                    expandableSummary("Summary"),
                                    expandableSummeryLoanSummaryMaximumLoanTenureLoanEligibility(
                                        "Loan Eligibility (Maximum Loan Tenure)"),
                                    Card(
                                      elevation: 3,
                                      child: ListTile(
                                        onTap: () {
                                          newPurchaseController
                                              .onTapMltLoanPackage();
                                        },
                                        leading: SizedBox(
                                          width: 10,
                                          child: Obx(
                                            () => Checkbox(
                                              value: newPurchaseController
                                                  .loanEligibiltyLoanPackageMltCheckBox
                                                  .value,
                                              onChanged: (bool? value) {
                                                newPurchaseController
                                                    .loanEligibiltyLoanPackageMltCheckBox
                                                    .value = value!;
                                                if (newPurchaseController
                                                        .loanEligibiltyLoanPackageMltCheckBox
                                                        .value ==
                                                    true) {
                                                  newPurchaseController
                                                      .exportFieldsCheckBoxesList
                                                      .add(
                                                          "Loan_eligibility_maximum_package");
                                                  debugPrint(newPurchaseController
                                                      .exportFieldsCheckBoxesList
                                                      .toString());
                                                } else if (newPurchaseController
                                                        .loanEligibiltyLoanPackageMltCheckBox
                                                        .value ==
                                                    false) {
                                                  newPurchaseController
                                                      .exportFieldsCheckBoxesList
                                                      .remove(
                                                          "Loan_eligibility_maximum_package");
                                                  debugPrint(newPurchaseController
                                                      .exportFieldsCheckBoxesList
                                                      .toString());
                                                }
                                              },
                                              checkColor: AppColors.red,
                                              activeColor:
                                                  Colors.red.withOpacity(0),
                                              shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(2.0),
                                              ),
                                              side: MaterialStateBorderSide
                                                  .resolveWith(
                                                (states) => const BorderSide(
                                                    width: 1.0,
                                                    color: Colors.red),
                                              ),
                                            ),
                                          ),
                                        ),
                                        title: Text(
                                            'Loan Eligibility (Maximum Loan Tenure) Loan Package',
                                            style: TextStyles.leadsTextStyle),
                                        trailing: const Icon(
                                          Icons.arrow_forward_ios,
                                          color: Colors.black,
                                          size: 15,
                                        ),
                                      ),
                                    ),
                                    Obx(
                                      () => newPurchaseController
                                                  .resultMainApplicantStep3
                                                  .value
                                                  .loanPackageMaximumLoanTenureBased !=
                                              null
                                          ? newPurchaseController
                                                      .resultMainApplicantStep3
                                                      .value
                                                      .loanPackageMaximumLoanTenureBased!
                                                      .lowestFloating !=
                                                  null
                                              ? Card(
                                                  elevation: 3,
                                                  child: ListTile(
                                                    onTap: () {
                                                      newPurchaseController
                                                          .onTapMltMortgageRepaymentTable();
                                                    },
                                                    leading: SizedBox(
                                                      width: 10,
                                                      child: Obx(
                                                        () => Checkbox(
                                                          value: newPurchaseController
                                                              .loanEligiblityMortgageRepaymentMltCheckBox
                                                              .value,
                                                          onChanged:
                                                              (bool? value) {
                                                            newPurchaseController
                                                                .loanEligiblityMortgageRepaymentMltCheckBox
                                                                .value = value!;
                                                            if (newPurchaseController
                                                                    .loanEligiblityMortgageRepaymentMltCheckBox
                                                                    .value ==
                                                                true) {
                                                              newPurchaseController
                                                                  .exportFieldsCheckBoxesList
                                                                  .add(
                                                                      "Loan_eligibility_maximum_mortgage");
                                                              debugPrint(newPurchaseController
                                                                  .exportFieldsCheckBoxesList
                                                                  .toString());
                                                            } else if (newPurchaseController
                                                                    .loanEligiblityMortgageRepaymentMltCheckBox
                                                                    .value ==
                                                                false) {
                                                              newPurchaseController
                                                                  .exportFieldsCheckBoxesList
                                                                  .remove(
                                                                      "Loan_eligibility_maximum_mortgage");
                                                              debugPrint(newPurchaseController
                                                                  .exportFieldsCheckBoxesList
                                                                  .toString());
                                                            }
                                                          },
                                                          checkColor:
                                                              AppColors.red,
                                                          activeColor: Colors
                                                              .red
                                                              .withOpacity(0),
                                                          shape:
                                                              RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        2.0),
                                                          ),
                                                          side:
                                                              MaterialStateBorderSide
                                                                  .resolveWith(
                                                            (states) =>
                                                                const BorderSide(
                                                                    width: 1.0,
                                                                    color: Colors
                                                                        .red),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    title: Text(
                                                        'Loan Eligibility (Maximum Loan Tenure) Mortgage Repayment based on Lowest Floating',
                                                        style: TextStyles
                                                            .leadsTextStyle),
                                                    trailing: const Icon(
                                                      Icons.arrow_forward_ios,
                                                      color: Colors.black,
                                                      size: 15,
                                                    ),
                                                  ),
                                                )
                                              : Container()
                                          : Container(),
                                    ),
                                    Obx(() => newPurchaseController
                                                .resultMainApplicantStep3
                                                .value
                                                .loanPackageMaximumLoanTenureBased !=
                                            null
                                        ? newPurchaseController
                                                    .resultMainApplicantStep3
                                                    .value
                                                    .loanPackageMaximumLoanTenureBased!
                                                    .lowestFixed !=
                                                null
                                            ? Card(
                                                elevation: 3,
                                                child: ListTile(
                                                  onTap: () {
                                                    newPurchaseController
                                                        .onTapMltMortgageRepaymentTableLowestFixed();
                                                  },
                                                  leading: SizedBox(
                                                    width: 10,
                                                    child: Obx(
                                                      () => Checkbox(
                                                        value: newPurchaseController
                                                            .loanEligiblityMortgageRepaymentMltCheckBox
                                                            .value,
                                                        onChanged:
                                                            (bool? value) {
                                                          newPurchaseController
                                                              .loanEligiblityMortgageRepaymentMltCheckBox
                                                              .value = value!;
                                                          if (newPurchaseController
                                                                  .loanEligiblityMortgageRepaymentMltCheckBox
                                                                  .value ==
                                                              true) {
                                                            newPurchaseController
                                                                .exportFieldsCheckBoxesList
                                                                .add(
                                                                    "Loan_eligibility_maximum_mortgage");
                                                            debugPrint(
                                                                newPurchaseController
                                                                    .exportFieldsCheckBoxesList
                                                                    .toString());
                                                          } else if (newPurchaseController
                                                                  .loanEligiblityMortgageRepaymentMltCheckBox
                                                                  .value ==
                                                              false) {
                                                            newPurchaseController
                                                                .exportFieldsCheckBoxesList
                                                                .remove(
                                                                    "Loan_eligibility_maximum_mortgage");
                                                            debugPrint(
                                                                newPurchaseController
                                                                    .exportFieldsCheckBoxesList
                                                                    .toString());
                                                          }
                                                        },
                                                        checkColor:
                                                            AppColors.red,
                                                        activeColor: Colors.red
                                                            .withOpacity(0),
                                                        shape:
                                                            RoundedRectangleBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      2.0),
                                                        ),
                                                        side:
                                                            MaterialStateBorderSide
                                                                .resolveWith(
                                                          (states) =>
                                                              const BorderSide(
                                                                  width: 1.0,
                                                                  color: Colors
                                                                      .red),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  title: Text(
                                                      'Loan Eligibility (Maximum Loan Tenure) Mortgage Repayment based on Lowest Fixed',
                                                      style: TextStyles
                                                          .leadsTextStyle),
                                                  trailing: const Icon(
                                                    Icons.arrow_forward_ios,
                                                    color: Colors.black,
                                                    size: 15,
                                                  ),
                                                ),
                                              )
                                            : Container()
                                        : Container()),
                                    expandableSummeryLoanSummaryPreferredLoanTenureLoanEligibility(
                                        "Loan Eligibility (Preferred Loan Tenure)"),
                                    Obx(
                                      () => newPurchaseController
                                                  .resultMainApplicantStep3
                                                  .value
                                                  .loanEligiblityPreferredLoanTenureBased ==
                                              null
                                          ? Container()
                                          : Card(
                                              elevation: 3,
                                              child: ListTile(
                                                onTap: () {
                                                  newPurchaseController
                                                      .onTapPltLoanPackage();
                                                },
                                                leading: SizedBox(
                                                  width: 10,
                                                  child: Obx(
                                                    () => Checkbox(
                                                      value: newPurchaseController
                                                          .loanEligibiltyLoanPackagePltCheckBox
                                                          .value,
                                                      onChanged: (bool? value) {
                                                        newPurchaseController
                                                            .loanEligibiltyLoanPackagePltCheckBox
                                                            .value = value!;
                                                        if (newPurchaseController
                                                                .loanEligibiltyLoanPackagePltCheckBox
                                                                .value ==
                                                            true) {
                                                          newPurchaseController
                                                              .exportFieldsCheckBoxesList
                                                              .add(
                                                                  "Loan_eligibility_preferred_package");
                                                          debugPrint(
                                                              newPurchaseController
                                                                  .exportFieldsCheckBoxesList
                                                                  .toString());
                                                        } else if (newPurchaseController
                                                                .loanEligibiltyLoanPackagePltCheckBox
                                                                .value ==
                                                            false) {
                                                          newPurchaseController
                                                              .exportFieldsCheckBoxesList
                                                              .remove(
                                                                  "Loan_eligibility_preferred_package");
                                                          debugPrint(
                                                              newPurchaseController
                                                                  .exportFieldsCheckBoxesList
                                                                  .toString());
                                                        }
                                                      },
                                                      checkColor: AppColors.red,
                                                      activeColor: Colors.red
                                                          .withOpacity(0),
                                                      shape:
                                                          RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(2.0),
                                                      ),
                                                      side:
                                                          MaterialStateBorderSide
                                                              .resolveWith(
                                                        (states) =>
                                                            const BorderSide(
                                                                width: 1.0,
                                                                color:
                                                                    Colors.red),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                title: Text(
                                                    'Loan Eligibility (Preferred Loan Tenure) Loan Package',
                                                    style: TextStyles
                                                        .leadsTextStyle),
                                                trailing: const Icon(
                                                  Icons.arrow_forward_ios,
                                                  color: Colors.black,
                                                  size: 15,
                                                ),
                                              ),
                                            ),
                                    ),
                                    Obx(
                                      () => newPurchaseController
                                                  .resultMainApplicantStep3
                                                  .value
                                                  .loanEligiblityPreferredLoanTenureBased ==
                                              null
                                          ? Container()
                                          : newPurchaseController
                                                      .resultMainApplicantStep3
                                                      .value
                                                      .loanPackagePreferredLoanTenureBased!
                                                      .lowestFloating !=
                                                  null
                                              ? Card(
                                                  elevation: 3,
                                                  child: ListTile(
                                                    onTap: () {
                                                      newPurchaseController
                                                          .onTapPltMortgageRepaymentTable();
                                                    },
                                                    leading: SizedBox(
                                                      width: 10,
                                                      child: Obx(
                                                        () => Checkbox(
                                                          value: newPurchaseController
                                                              .loanEligiblityMortgageRepaymentPltCheckBox
                                                              .value,
                                                          onChanged:
                                                              (bool? value) {
                                                            newPurchaseController
                                                                .loanEligiblityMortgageRepaymentPltCheckBox
                                                                .value = value!;
                                                            if (newPurchaseController
                                                                    .loanEligiblityMortgageRepaymentPltCheckBox
                                                                    .value ==
                                                                true) {
                                                              newPurchaseController
                                                                  .exportFieldsCheckBoxesList
                                                                  .add(
                                                                      "Loan_eligibility_preferred_mortgage");
                                                              debugPrint(newPurchaseController
                                                                  .exportFieldsCheckBoxesList
                                                                  .toString());
                                                            } else if (newPurchaseController
                                                                    .loanEligiblityMortgageRepaymentPltCheckBox
                                                                    .value ==
                                                                false) {
                                                              newPurchaseController
                                                                  .exportFieldsCheckBoxesList
                                                                  .remove(
                                                                      "Loan_eligibility_preferred_mortgage");

                                                              debugPrint(newPurchaseController
                                                                  .exportFieldsCheckBoxesList
                                                                  .toString());
                                                            }
                                                          },
                                                          checkColor:
                                                              AppColors.red,
                                                          activeColor: Colors
                                                              .red
                                                              .withOpacity(0),
                                                          shape:
                                                              RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        2.0),
                                                          ),
                                                          side:
                                                              MaterialStateBorderSide
                                                                  .resolveWith(
                                                            (states) =>
                                                                const BorderSide(
                                                                    width: 1.0,
                                                                    color: Colors
                                                                        .red),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    title: Text(
                                                        'Loan Eligibility (Preferred Loan Tenure) Mortgage Repayment Based on Lowest Floating',
                                                        style: TextStyles
                                                            .leadsTextStyle),
                                                    trailing: const Icon(
                                                      Icons.arrow_forward_ios,
                                                      color: Colors.black,
                                                      size: 15,
                                                    ),
                                                  ),
                                                )
                                              : Container(),
                                    ),
                                    Obx(
                                      () => newPurchaseController
                                                  .resultMainApplicantStep3
                                                  .value
                                                  .loanEligiblityPreferredLoanTenureBased ==
                                              null
                                          ? Container()
                                          : Card(
                                              elevation: 3,
                                              child: ListTile(
                                                onTap: () {
                                                  newPurchaseController
                                                      .onTapPltMortgageRepaymentTableLowestFixed();
                                                },
                                                leading: SizedBox(
                                                  width: 10,
                                                  child: Obx(
                                                    () => Checkbox(
                                                      value: newPurchaseController
                                                          .loanEligiblityMortgageRepaymentPltCheckBox
                                                          .value,
                                                      onChanged: (bool? value) {
                                                        newPurchaseController
                                                            .loanEligiblityMortgageRepaymentPltCheckBox
                                                            .value = value!;
                                                        if (newPurchaseController
                                                                .loanEligiblityMortgageRepaymentPltCheckBox
                                                                .value ==
                                                            true) {
                                                          newPurchaseController
                                                              .exportFieldsCheckBoxesList
                                                              .add(
                                                                  "Loan_eligibility_preferred_mortgage");
                                                          debugPrint(
                                                              newPurchaseController
                                                                  .exportFieldsCheckBoxesList
                                                                  .toString());
                                                        } else if (newPurchaseController
                                                                .loanEligiblityMortgageRepaymentPltCheckBox
                                                                .value ==
                                                            false) {
                                                          newPurchaseController
                                                              .exportFieldsCheckBoxesList
                                                              .remove(
                                                                  "Loan_eligibility_preferred_mortgage");

                                                          debugPrint(
                                                              newPurchaseController
                                                                  .exportFieldsCheckBoxesList
                                                                  .toString());
                                                        }
                                                      },
                                                      checkColor: AppColors.red,
                                                      activeColor: Colors.red
                                                          .withOpacity(0),
                                                      shape:
                                                          RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(2.0),
                                                      ),
                                                      side:
                                                          MaterialStateBorderSide
                                                              .resolveWith(
                                                        (states) =>
                                                            const BorderSide(
                                                                width: 1.0,
                                                                color:
                                                                    Colors.red),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                title: Text(
                                                    'Loan Eligibility (Preferred Loan Tenure) Mortgage Repayment Based on Lowest Fixed',
                                                    style: TextStyles
                                                        .leadsTextStyle),
                                                trailing: const Icon(
                                                  Icons.arrow_forward_ios,
                                                  color: Colors.black,
                                                  size: 15,
                                                ),
                                              ),
                                            ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(height: windowHeight * 0.03),
                              Center(
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 20.0),
                                  child: PrimaryButton(
                                    windowHeight: windowHeight,
                                    windowWidth: windowWidth,
                                    buttonTitle: "Download Report",
                                    onPressed: () {
                                      newPurchaseController
                                          .onTapDownloadReport();
                                    },
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: windowHeight * 0.01,
                              ),
                            ],
                          ),
                        ),
              SizedBox(
                height: windowHeight * 0.01,
              ),
              Obx(() => newPurchaseController.isCalculated.value &&
                      (activeStep == 0 || activeStep == 1)
                  ? Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      child: Column(
                        children: [
                          activeStep == 0 || activeStep == 1
                              ? PrimaryButton(
                                  windowHeight: windowHeight,
                                  windowWidth: windowWidth,
                                  buttonTitle: activeStep == 1
                                      ? "View Report"
                                      : "Continue",
                                  onPressed: () {
                                    if (activeStep != 0 && activeStep != 2) {
                                      if (newPurchaseController
                                          .otherFeesControllerMainApplicant
                                          .text
                                          .isEmpty) {
                                        Future.delayed(
                                            const Duration(milliseconds: 0),
                                            () {
                                          FocusManager.instance.primaryFocus!
                                              .unfocus();
                                          Fluttertoast.showToast(
                                            timeInSecForIosWeb: 3,
                                            msg: "All fields are mandatory",
                                            backgroundColor: Colors.red,
                                            toastLength: Toast.LENGTH_LONG,
                                            gravity: ToastGravity.BOTTOM,
                                            textColor: Colors.white,
                                            fontSize: 20.0,
                                          );
                                        });
                                        return;
                                      }

                                      newPurchaseController
                                          .onTapContinueToReportsCheck();
                                    }
                                    setState(() {
                                      newPurchaseController.isCalculated.value =
                                          false;

                                      if (activeStep == 0) {
                                        activeStep = activeStep + 1;
                                      } else if (activeStep == 1) {
                                        if (newPurchaseController
                                            .otherFeesControllerMainApplicant
                                            .text
                                            .isNotEmpty) {
                                          activeStep = 2;
                                        }
                                      }
                                    });
                                  })
                              : Container(),
                          SizedBox(height: windowHeight * 0.02),
                        ],
                      ),
                    )
                  : Container()),
              // Obx(() {
              // return
              activeStep != 2
                  ? Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      child: Column(
                        children: [
                          PrimaryButton(
                              windowHeight: windowHeight,
                              windowWidth: windowWidth,
                              buttonTitle: "Calculate",
                              onPressed: () {
                                if (activeStep == 0) {
                                  newPurchaseController
                                      .onTapCalculateStep1Check();
                                } else if (activeStep == 1) {
                                  newPurchaseController
                                      .onTapCalculateStep2Check();
                                } else {}
                              }),
                          SizedBox(height: windowHeight * 0.02),
                        ],
                      ),
                    )
                  : Container(),
              // : Padding(
              //     padding: const EdgeInsets.symmetric(horizontal: 20.0),
              //     child: Column(
              //       children: [
              //         activeStep == 0 || activeStep == 1
              //             ? PrimaryButton(
              //                 windowHeight: windowHeight,
              //                 windowWidth: windowWidth,
              //                 buttonTitle: activeStep == 1
              //                     ? "View Report"
              //                     : "Continue",
              //                 onPressed: () {
              //                   if (activeStep != 0 &&
              //                       activeStep != 2) {
              //                     if (newPurchaseController
              //                         .otherFeesControllerMainApplicant
              //                         .text
              //                         .isEmpty) {
              //                       Get.snackbar(
              //                         'Sorry',
              //                         'All fields are mandatory',
              //                         colorText: Colors.white,
              //                         backgroundColor: Colors.red,
              //                         duration:
              //                             const Duration(seconds: 3),
              //                       );
              //                       return;
              //                     }

              //                     newPurchaseController
              //                         .onTapContinueToReportsCheck();
              //                   }
              //                   setState(() {
              //                     newPurchaseController
              //                         .isCalculated.value = false;

              //                     if (activeStep == 0) {
              //                       activeStep = activeStep + 1;
              //                     } else if (activeStep == 1) {
              //                       if (newPurchaseController
              //                           .otherFeesControllerMainApplicant
              //                           .text
              //                           .isNotEmpty) {
              //                         activeStep = 2;
              //                       }
              //                     }
              //                   });
              //                 })
              //             : Container(),
              //         SizedBox(height: windowHeight * 0.02),
              //       ],
              // ),
              // );
              // }),
              // Obx(() => newPurchaseController.isCalculated.value
              //     ? Padding(
              //         padding: const EdgeInsets.symmetric(horizontal: 20.0),
              //         child: Column(
              //           children: [
              //             PrimaryButton(
              //                 windowHeight: windowHeight,
              //                 windowWidth: windowWidth,
              //                 buttonTitle: "Continue",
              //                 onPressed: () {
              //                   setState(() {
              //                     activeStep =
              //                         activeStep != 2 ? activeStep + 1 : 2;
              //                   });
              //                 }),
              //             SizedBox(height: windowHeight * 0.02),
              //           ],
              //         ),
              //       )
              //     : const SizedBox()),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: SecondaryButton(
                  buttonTitle: activeStep == 0 ? 'Reset' : 'Back',
                  windowHeight: windowHeight,
                  windowWidth: windowWidth,
                  onPressed: () {
                    setState(() {
                      // ! clear the setep 2 fields
                      if (activeStep == 1) {
                        newPurchaseController
                            .propertyPriceControllerMainApplicant
                            .value = const TextEditingValue(text: '0');
                        newPurchaseController.loanAmountControllerMainApplicant
                            .value = const TextEditingValue(text: '0');
                        newPurchaseController
                                .loanTenureYearControllerMainApplicant =
                            TextEditingController(text: '0');
                        newPurchaseController
                            .selectedLongerLoanTenureMainApplicant.value = 'No';
                        newPurchaseController
                            .selectedPropertyTypeMainApplicant = 'HDB';
                      }
                      if (activeStep == 0) {
                        // ! Need to clear the text field Starts here
                        //! Clear monthly fixed incomne
                        newPurchaseController
                            .monthlyFixedIncomeControllerMainApplicant
                            .value = const TextEditingValue(text: '0');
                        newPurchaseController
                            .monthlyFixedIncomeControllerJointApplicant1
                            .value = const TextEditingValue(text: '0');
                        newPurchaseController
                            .monthlyFixedIncomeControllerJointApplicant2
                            .value = const TextEditingValue(text: '0');
                        newPurchaseController
                            .monthlyFixedIncomeControllerJointApplicant3
                            .value = const TextEditingValue(text: '0');
                        // ! Clear Anual income
                        newPurchaseController
                            .annualIncomeControllerMainApplicant
                            .value = const TextEditingValue(text: '0');
                        newPurchaseController
                            .annualIncomeControllerJointApplicant1
                            .value = const TextEditingValue(text: '0');
                        newPurchaseController
                            .annualIncomeControllerJointApplicant2
                            .value = const TextEditingValue(text: '0');
                        newPurchaseController
                            .annualIncomeControllerJointApplicant3
                            .value = const TextEditingValue(text: '0');
                        // ! Clear monthly rental income
                        newPurchaseController
                            .monthlyRentalIncomeControllerMainApplicant
                            .value = const TextEditingValue(text: '0');
                        newPurchaseController
                            .monthlyRentalIncomeControllerJointApplicant1
                            .value = const TextEditingValue(text: '0');
                        newPurchaseController
                            .monthlyRentalIncomeControllerJointApplicant1
                            .value = const TextEditingValue(text: '0');
                        newPurchaseController
                            .monthlyRentalIncomeControllerJointApplicant1
                            .value = const TextEditingValue(text: '0');
                        // ! Set how many residential properties do you own to 0
                        newPurchaseController.selectedPropertiesMainApplicant =
                            '0';
                        newPurchaseController
                            .selectedPropertiesJointApplicant1 = '0';
                        newPurchaseController
                            .selectedPropertiesJointApplicant2 = '0';
                        newPurchaseController
                            .selectedPropertiesJointApplicant3 = '0';
                        // ! Set how many housing loan do you have to 0
                        newPurchaseController
                            .selectedHousingLoanMainApplicant!.value = '0';
                        newPurchaseController
                            .selectedHousingLoanJointApplicant1!.value = '0';
                        newPurchaseController
                            .selectedHousingLoanJointApplicant2!.value = '0';
                        newPurchaseController
                            .selectedHousingLoanJointApplicant3!.value = '0';
                        // ! Clear total monthly installment for your current housing loan
                        newPurchaseController
                            .housingLoansInstallmentControllerMainApplicant
                            .value = const TextEditingValue(text: '0');
                        newPurchaseController
                            .housingLoansInstallmentControllerJointApplicant1
                            .value = const TextEditingValue(text: '0');
                        newPurchaseController
                            .housingLoansInstallmentControllerJointApplicant2
                            .value = const TextEditingValue(text: '0');
                        newPurchaseController
                            .housingLoansInstallmentControllerJointApplicant3
                            .value = const TextEditingValue(text: '0');
                        // ! Clear Total monthly instalments for car loan
                        newPurchaseController
                            .carLoansInstallmentControllerMainApplicant
                            .value = const TextEditingValue(text: '0');
                        newPurchaseController
                            .carLoansInstallmentControllerJointApplicant1
                            .value = const TextEditingValue(text: '0');
                        newPurchaseController
                            .carLoansInstallmentControllerJointApplicant2
                            .value = const TextEditingValue(text: '0');
                        newPurchaseController
                            .carLoansInstallmentControllerJointApplicant3
                            .value = const TextEditingValue(text: '0');

                        // ! Clear total monthly installment for personal renovation or education loan
                        newPurchaseController
                            .personalEducationRenovationControllerMainApplicant
                            .value = const TextEditingValue(text: '0');
                        newPurchaseController
                            .personalEducationRenovationControllerJointApplicant1
                            .value = const TextEditingValue(text: '0');
                        newPurchaseController
                            .personalEducationRenovationControllerJointApplicant2
                            .value = const TextEditingValue(text: '0');
                        newPurchaseController
                            .personalEducationRenovationControllerJointApplicant3
                            .value = const TextEditingValue(text: '0');
                        // ! Clear Minimum credit card repayment amount
                        newPurchaseController
                            .cardRepaymentAmountControllerMainApplicant
                            .value = const TextEditingValue(text: '0');
                        newPurchaseController
                            .cardRepaymentAmountControllerJointApplicant1
                            .value = const TextEditingValue(text: '0');
                        newPurchaseController
                            .cardRepaymentAmountControllerJointApplicant2
                            .value = const TextEditingValue(text: '0');
                        newPurchaseController
                            .cardRepaymentAmountControllerJointApplicant3
                            .value = const TextEditingValue(text: '0');
                        // ! Clear monthly instalments if you are a gaurenter for any loan
                        newPurchaseController
                            .guarantorLoanAmountControllerMainApplicant
                            .value = const TextEditingValue(text: '0');
                        newPurchaseController
                            .guarantorLoanAmountControllerJointApplicant1
                            .value = const TextEditingValue(text: '0');
                        newPurchaseController
                            .guarantorLoanAmountControllerJointApplicant2
                            .value = const TextEditingValue(text: '0');
                        newPurchaseController
                            .guarantorLoanAmountControllerJointApplicant3
                            .value = const TextEditingValue(text: '0');
                        // ! Clear total monthly instalments for commercial property loan (Under Personal Name)
                        newPurchaseController
                            .commercialPropertyLoanPersonalControllerMainApplicant
                            .value = const TextEditingValue(text: '0');
                        newPurchaseController
                            .commercialPropertyLoanPersonalControllerJointApplicant1
                            .value = const TextEditingValue(text: '0');
                        newPurchaseController
                            .commercialPropertyLoanPersonalControllerJointApplicant2
                            .value = const TextEditingValue(text: '0');
                        newPurchaseController
                            .commercialPropertyLoanPersonalControllerJointApplicant3
                            .value = const TextEditingValue(text: '0');
                        // ! Clear total monthly instalments for commercial property loan (Under Company Name)
                        newPurchaseController
                            .commercialPropertyLoanCompanyControllerMainApplicant
                            .value = const TextEditingValue(text: '0');
                        newPurchaseController
                            .commercialPropertyLoanCompanyControllerJointApplicant1
                            .value = const TextEditingValue(text: '0');
                        newPurchaseController
                            .commercialPropertyLoanCompanyControllerJointApplicant2
                            .value = const TextEditingValue(text: '0');
                        newPurchaseController
                            .commercialPropertyLoanCompanyControllerJointApplicant3
                            .value = const TextEditingValue(text: '0');
                        // ! Need to clear the text field Ends here
                      }
                      newPurchaseController.isCalculated.value = false;
                      newPurchaseController.nameList.clear();
                      newPurchaseController.phoneList.clear();
                      newPurchaseController.countryCodeList.clear();
                      newPurchaseController.emailAddressList.clear();
                      newPurchaseController.dobList.clear();
                      newPurchaseController.nationalityList.clear();
                      newPurchaseController.employementTypeList.clear();
                      newPurchaseController.monthlyFixedIncomeList.clear();
                      newPurchaseController.annualIncomeList.clear();
                      newPurchaseController.monthlyRentalIncomeList.clear();
                      newPurchaseController.numberOfOwnedPropertiesList.clear();
                      newPurchaseController.numberOfHousingLoanLoansList
                          .clear();
                      newPurchaseController
                          .totalMonthlyInstallmentForCurrentHousingLoanList
                          .clear();
                      newPurchaseController
                          .totalMonthlyInstallmentForCarLoanList
                          .clear();
                      newPurchaseController
                          .totalMonthlyInstallmentForPersonalEducationRenovationList
                          .clear();
                      newPurchaseController
                          .minimumMonthlyCreditCardRepaymentAmountList
                          .clear();
                      newPurchaseController.totalMonthlyGuarantorInstallmentList
                          .clear();
                      newPurchaseController
                          .totalMonthlyCommercialPropertyCompanyLoanList
                          .clear();
                      newPurchaseController
                          .totalMonthlyCommercialPropertyLoanPersonalList
                          .clear();

                      /*if (activeStep == 0) {
                        newPurchaseController.onCancel();
                      }*/
                      activeStep = 0;
                    });
                  },
                  kGradientBoxDecoration:
                      ContainerStyles.kGradientBoxDecorationSecondaryButton,
                  kInnerDecoration:
                      ContainerStyles.kInnerDecorationSecondaryButton,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget expandableSummary(String title) {
    // NewPurchaseReportResponseModel newPurchaseReportResponseModel;
    return Card(
      elevation: 3,
      child: Obx(
        () {
          debugPrint(newPurchaseController
              .resultMainApplicantStep3.value.summary
              .toString());
          return newPurchaseController.resultMainApplicantStep3.value.summary ==
                  null
              ? Container()
              : ExpansionTile(
                  leading: SizedBox(
                      width: 10,
                      child: Obx(() => Checkbox(
                            value: newPurchaseController.summaryCheckbox.value,
                            onChanged: (bool? value) {
                              newPurchaseController.summaryCheckbox.value =
                                  value!;

                              if (newPurchaseController.summaryCheckbox.value ==
                                  true) {
                                newPurchaseController.exportFieldsCheckBoxesList
                                    .add("Summary");
                                debugPrint(newPurchaseController
                                    .exportFieldsCheckBoxesList
                                    .toString());
                              } else if (newPurchaseController
                                      .summaryCheckbox.value ==
                                  false) {
                                newPurchaseController.exportFieldsCheckBoxesList
                                    .remove("Summary");
                                debugPrint(newPurchaseController
                                    .exportFieldsCheckBoxesList
                                    .toString());
                              }
                            },
                            checkColor: AppColors.red,
                            activeColor: Colors.red.withOpacity(0),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(2.0),
                            ),
                            side: MaterialStateBorderSide.resolveWith(
                              (states) => const BorderSide(
                                  width: 1.0, color: Colors.red),
                            ),
                          ))),
                  childrenPadding: EdgeInsets.zero,
                  title: Text(title, style: TextStyles.leadsTextStyle),
                  collapsedIconColor: Colors.black,
                  children: [
                    Column(
                      children: [
                        const Divider(
                            height: 4, thickness: 4, color: Colors.black12),
                        SizedBox(
                          height: windowWidth * 0.03,
                        ),
                        summeryItem(
                            "Prepared For",
                            newPurchaseController.resultMainApplicantStep3.value
                                        .summary ==
                                    null
                                ? "0"
                                : newPurchaseController.resultMainApplicantStep3
                                    .value.summary!.preparedFor
                                    .toString()),
                        summeryItem("Property Purchase Price",
                            "\$ ${newPurchaseController.resultMainApplicantStep3.value.summary!.propertyPurchasePrice!.toStringAsFixed(2).replaceAllMapped(RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))'), (Match m) => '${m[1]},')}"),
                        summeryItem("Cash Down Payment",
                            "\$ ${newPurchaseController.resultMainApplicantStep3.value.summary!.cashDownPayment.toString().replaceAllMapped(RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))'), (Match m) => '${m[1]},')}.00"),
                        summeryItem("CPF/ Cash Down Payment",
                            "\$ ${newPurchaseController.resultMainApplicantStep3.value.summary!.cpfOrCashDownPayment.toString().replaceAllMapped(RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))'), (Match m) => '${m[1]},')}.00"),
                        summeryItem("Loan Amount",
                            "\$ ${newPurchaseController.resultMainApplicantStep3.value.summary!.loanAmount!.toStringAsFixed(2).replaceAllMapped(RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))'), (Match m) => '${m[1]},')}"),
                        summeryItem("Buyer's Stamp Duty",
                            "\$ ${newPurchaseController.resultMainApplicantStep3.value.summary!.buyerStampDuty!.toStringAsFixed(2).replaceAllMapped(RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))'), (Match m) => '${m[1]},')}"),
                        summeryItem("Additional Buyer's Stamp Duty",
                            "\$ ${newPurchaseController.resultMainApplicantStep3.value.summary!.additionalBuyerStampDuty!.toStringAsFixed(2).replaceAllMapped(RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))'), (Match m) => '${m[1]},')}"),
                        summeryItem("Legal Fees",
                            "\$ ${newPurchaseController.resultMainApplicantStep3.value.summary!.legalFees!.toStringAsFixed(2).replaceAllMapped(RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))'), (Match m) => '${m[1]},')}"),
                        summeryItem("Valuation Fees",
                            "\$ ${newPurchaseController.resultMainApplicantStep3.value.summary!.valuationFees!.toStringAsFixed(2).replaceAllMapped(RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))'), (Match m) => '${m[1]},')}"),
                        summeryItem("Other Fees",
                            "\$ ${newPurchaseController.resultMainApplicantStep3.value.summary!.otherFees!.toStringAsFixed(2).replaceAllMapped(RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))'), (Match m) => '${m[1]},')}"),
                        SizedBox(
                          height: windowWidth * 0.03,
                        ),
                      ],
                    ),
                  ],
                );
        },
      ),
    );
  }

  Widget expandableSummeryLoanSummaryMaximumLoanTenureLoanEligibility(
      String title) {
    // NewPurchaseReportResponseModel newPurchaseReportResponseModel;
    return Card(
      elevation: 3,
      child: Obx(
        () => newPurchaseController.resultMainApplicantStep3.value
                    .loanEligiblityMaximumLoanTenureBased ==
                null
            ? Container()
            : ExpansionTile(
                leading: SizedBox(
                    width: 10,
                    child: Obx(() => Checkbox(
                          value: newPurchaseController
                              .loanEligibiltyMltCheckBox.value,
                          onChanged: (bool? value) {
                            newPurchaseController
                                .loanEligibiltyMltCheckBox.value = value!;
                            if (newPurchaseController
                                    .loanEligibiltyMltCheckBox.value ==
                                true) {
                              newPurchaseController.exportFieldsCheckBoxesList
                                  .add("Loan_eligibility_maximum");
                              debugPrint(newPurchaseController
                                  .exportFieldsCheckBoxesList
                                  .toString());
                            } else if (newPurchaseController
                                    .loanEligibiltyMltCheckBox.value ==
                                false) {
                              newPurchaseController.exportFieldsCheckBoxesList
                                  .remove("Loan_eligibility_maximum");
                              debugPrint(newPurchaseController
                                  .exportFieldsCheckBoxesList
                                  .toString());
                            }
                          },
                          checkColor: AppColors.red,
                          activeColor: Colors.red.withOpacity(0),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(2.0),
                          ),
                          side: MaterialStateBorderSide.resolveWith(
                            (states) =>
                                const BorderSide(width: 1.0, color: Colors.red),
                          ),
                        ))),
                childrenPadding: EdgeInsets.zero,
                title: Text(title, style: TextStyles.leadsTextStyle),
                collapsedIconColor: Colors.black,
                children: [
                  const Divider(height: 4, thickness: 4, color: Colors.black12),
                  SizedBox(
                    height: windowWidth * 0.03,
                  ),
                  Text(
                    'Based on Your Current Financial Status',
                    style: TextStyle(
                      fontSize: FontSize.s18,
                      fontWeight: FontWeight.bold,
                      color: AppColors.kPrimaryColor,
                    ),
                  ),
                  Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 45.0),
                        child: DataColumnForLoanEligibilityMaximumLoanTenure(
                          maximumQualifiedLoanValue:
                              '\$ ${newPurchaseController.resultMainApplicantStep3.value.loanEligiblityMaximumLoanTenureBased == null ? 0 : newPurchaseController.resultMainApplicantStep3.value.loanEligiblityMaximumLoanTenureBased!.maximumLoanQualified.toString()}',
                          maximumPropertyPriceValue:
                              '\$ ${newPurchaseController.resultMainApplicantStep3.value.loanEligiblityMaximumLoanTenureBased == null ? 0 : newPurchaseController.resultMainApplicantStep3.value.loanEligiblityMaximumLoanTenureBased!.maximumPropertyPrice.toString()}',
                          msrValue: newPurchaseController
                              .resultMainApplicantStep3
                              .value
                              .loanEligiblityMaximumLoanTenureBased!
                              .msr
                              .toString(),
                          msrStatus: newPurchaseController
                              .resultMainApplicantStep2
                              .value
                              .maximumTenureBasedAffordability!
                              .msrStatus
                              .toString()
                              .toUpperCase(),
                          tdsrValue: newPurchaseController
                              .resultMainApplicantStep3
                              .value
                              .loanEligiblityMaximumLoanTenureBased!
                              .tdsr
                              .toString(),
                          tdsrStatus: newPurchaseController
                              .resultMainApplicantStep2
                              .value
                              .maximumTenureBasedAffordability!
                              .tdsrStatus
                              .toString()
                              .toUpperCase(),
                          maximumLoanTenureValue:
                              '${newPurchaseController.resultMainApplicantStep3.value.loanEligiblityMaximumLoanTenureBased!.maximumLoanTenure.toString()} years',
                          fullPLedgeAmountValue:
                              '\$ ${newPurchaseController.resultMainApplicantStep3.value.loanEligiblityMaximumLoanTenureBased!.fullPledgeAmount.toString()}',
                          fullUnpledgeAmountValue:
                              '\$ ${newPurchaseController.resultMainApplicantStep3.value.loanEligiblityMaximumLoanTenureBased!.fullUnpledgeAmount.toString()}',
                          preferredPledgeAmountValue:
                              '\$ ${newPurchaseController.resultMainApplicantStep3.value.loanEligiblityMaximumLoanTenureBased!.preferredPledgeAmount.toString()}',
                          preferredUnpledgeAmountValue:
                              '\$ ${newPurchaseController.resultMainApplicantStep3.value.loanEligiblityMaximumLoanTenureBased!.preferredUnpledgeAmount.toString()}',
                          pledgeAmountValue:
                              '\$ ${newPurchaseController.resultMainApplicantStep3.value.loanEligiblityMaximumLoanTenureBased!.pledgeAmount.toString()}',
                          unpledgeAmountValue:
                              '\$ ${newPurchaseController.resultMainApplicantStep3.value.loanEligiblityMaximumLoanTenureBased!.unPledgeAmount.toString()}',
                        ),
                      ),
                      SizedBox(
                        height: windowWidth * 0.1,
                      ),
                      const Divider(
                          height: 4, thickness: 4, color: Colors.black12),
                      SizedBox(
                        height: windowWidth * 0.03,
                      ),
                    ],
                  ),
                ],
              ),
      ),
    );
  }

  Widget expandableSummeryLoanSummaryPreferredLoanTenureLoanEligibility(
      String title) {
    // NewPurchaseReportResponseModel newPurchaseReportResponseModel;
    return Card(
      elevation: 3,
      child: Obx(
        () => newPurchaseController.resultMainApplicantStep3.value
                    .loanEligiblityPreferredLoanTenureBased ==
                null
            ? Container()
            : ExpansionTile(
                leading: SizedBox(
                    width: 10,
                    child: Obx(() => Checkbox(
                          value: newPurchaseController
                              .loanEligibiltyPltCheckBox.value,
                          onChanged: (bool? value) {
                            newPurchaseController
                                .loanEligibiltyPltCheckBox.value = value!;
                            if (newPurchaseController
                                    .loanEligibiltyPltCheckBox.value ==
                                true) {
                              newPurchaseController.exportFieldsCheckBoxesList
                                  .add("Loan_eligibility_preferred");
                              debugPrint(newPurchaseController
                                  .exportFieldsCheckBoxesList
                                  .toString());
                            } else if (newPurchaseController
                                    .loanEligibiltyPltCheckBox.value ==
                                false) {
                              newPurchaseController.exportFieldsCheckBoxesList
                                  .remove("Loan_eligibility_preferred");
                              debugPrint(newPurchaseController
                                  .exportFieldsCheckBoxesList
                                  .toString());
                            }
                          },
                          checkColor: AppColors.red,
                          activeColor: Colors.red.withOpacity(0),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(2.0),
                          ),
                          side: MaterialStateBorderSide.resolveWith(
                            (states) =>
                                const BorderSide(width: 1.0, color: Colors.red),
                          ),
                        ))),
                childrenPadding: EdgeInsets.zero,
                title: Text(title, style: TextStyles.leadsTextStyle),
                collapsedIconColor: Colors.black,
                children: [
                  const Divider(height: 4, thickness: 4, color: Colors.black12),
                  SizedBox(
                    height: windowWidth * 0.03,
                  ),
                  Text(
                    'Based on Your Current Financial Status',
                    style: TextStyle(
                      fontSize: FontSize.s18,
                      fontWeight: FontWeight.bold,
                      color: AppColors.kPrimaryColor,
                    ),
                  ),
                  Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 45.0),
                        child: DataColumnForLoanEligibilityMaximumLoanTenure(
                          maximumQualifiedLoanValue:
                              '\$ ${newPurchaseController.resultMainApplicantStep3.value.loanEligiblityPreferredLoanTenureBased == null ? 0 : newPurchaseController.resultMainApplicantStep3.value.loanEligiblityPreferredLoanTenureBased!.preferredLoanQualified.toString()}',
                          maximumPropertyPriceValue:
                              '\$ ${newPurchaseController.resultMainApplicantStep3.value.loanEligiblityPreferredLoanTenureBased == null ? 0 : newPurchaseController.resultMainApplicantStep3.value.loanEligiblityPreferredLoanTenureBased!.maximumPropertyPrice.toString()}',
                          msrValue: newPurchaseController
                                      .resultMainApplicantStep3
                                      .value
                                      .loanEligiblityPreferredLoanTenureBased ==
                                  null
                              ? "0"
                              : newPurchaseController
                                  .resultMainApplicantStep3
                                  .value
                                  .loanEligiblityPreferredLoanTenureBased!
                                  .msr
                                  .toString(),
                          msrStatus: newPurchaseController
                              .resultMainApplicantStep2
                              .value
                              .preferedTenureBasedAffordability!
                              .msrStatus
                              .toString()
                              .toUpperCase(),
                          tdsrValue: newPurchaseController
                                      .resultMainApplicantStep3
                                      .value
                                      .loanEligiblityPreferredLoanTenureBased ==
                                  null
                              ? "0"
                              : newPurchaseController
                                  .resultMainApplicantStep3
                                  .value
                                  .loanEligiblityPreferredLoanTenureBased!
                                  .tdsr
                                  .toString(),
                          tdsrStatus: newPurchaseController
                              .resultMainApplicantStep2
                              .value
                              .preferedTenureBasedAffordability!
                              .tdsrStatus
                              .toString()
                              .toUpperCase(),
                          maximumLoanTenureValue:
                              '${newPurchaseController.resultMainApplicantStep3.value.loanEligiblityPreferredLoanTenureBased!.preferredLoanTenure.toString()} years',
                          fullPLedgeAmountValue:
                              '\$ ${newPurchaseController.resultMainApplicantStep3.value.loanEligiblityPreferredLoanTenureBased!.fullPledgeAmount.toString()}',
                          fullUnpledgeAmountValue:
                              '\$ ${newPurchaseController.resultMainApplicantStep3.value.loanEligiblityPreferredLoanTenureBased!.fullUnpledgeAmount.toString()}',
                          preferredPledgeAmountValue:
                              '\$ ${newPurchaseController.resultMainApplicantStep3.value.loanEligiblityPreferredLoanTenureBased!.preferredPledgeAmount.toString()}',
                          preferredUnpledgeAmountValue:
                              '\$ ${newPurchaseController.resultMainApplicantStep3.value.loanEligiblityPreferredLoanTenureBased!.preferredUnpledgeAmount.toString()}',
                          pledgeAmountValue:
                              '\$ ${newPurchaseController.resultMainApplicantStep3.value.loanEligiblityPreferredLoanTenureBased!.pledgeAmount.toString()}',
                          unpledgeAmountValue:
                              '\$ ${newPurchaseController.resultMainApplicantStep3.value.loanEligiblityPreferredLoanTenureBased!.unPledgeAmount.toString()}',
                        ),
                      ),
                      SizedBox(
                        height: windowWidth * 0.1,
                      ),
                      const Divider(
                          height: 4, thickness: 4, color: Colors.black12),
                      SizedBox(
                        height: windowWidth * 0.03,
                      ),
                    ],
                  ),
                ],
              ),
      ),
    );
  }

  Widget summeryItem(title, value) {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: windowWidth * 0.04, vertical: windowHeight * 0.003),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            //mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                  flex: 5,
                  child:
                      Text(title, style: TextStyles.formFieldTitleTextStyle1)),
              Expanded(
                flex: 2,
                child: Text(
                  value,
                  style: TextStyles.leadsTextStyle1,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

class DataRowForLoanEligibilityMaximumLoanTenure extends StatelessWidget {
  const DataRowForLoanEligibilityMaximumLoanTenure({
    required this.title,
    required this.value,
    Key? key,
  }) : super(key: key);

  final String title;
  final String value;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          flex: 2,
          child: Text(
            title,
            style: TextStyle(
              color: const Color(0XFF767676),
              fontWeight: FontWeight.w400,
              fontSize: FontSize.s18,
            ),
            textAlign: TextAlign.left,
          ),
        ),
        Expanded(
          flex: 1,
          child: Text(
            value.toString().replaceAllMapped(
                RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))'),
                (Match m) => '${m[1]},'),
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontSize: FontSize.s18,
            ),
            textAlign: TextAlign.right,
          ),
        ),
      ],
    );
  }
}

class DataColumnForLoanEligibilityMaximumLoanTenure extends StatelessWidget {
  final String maximumQualifiedLoanValue;
  final String maximumPropertyPriceValue;
  final String msrValue;
  final String msrStatus;
  final String tdsrValue;
  final String tdsrStatus;
  final String maximumLoanTenureValue;
  final String fullPLedgeAmountValue;
  final String fullUnpledgeAmountValue;
  final String preferredPledgeAmountValue;
  final String preferredUnpledgeAmountValue;
  final String unpledgeAmountValue;
  final String pledgeAmountValue;

  const DataColumnForLoanEligibilityMaximumLoanTenure({
    Key? key,
    required this.maximumQualifiedLoanValue,
    required this.maximumPropertyPriceValue,
    required this.msrValue,
    required this.tdsrValue,
    required this.maximumLoanTenureValue,
    required this.fullPLedgeAmountValue,
    required this.fullUnpledgeAmountValue,
    required this.preferredPledgeAmountValue,
    required this.preferredUnpledgeAmountValue,
    required this.unpledgeAmountValue,
    required this.pledgeAmountValue,
    required this.msrStatus,
    required this.tdsrStatus,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: windowHeight * 0.03,
        ),
        DataRowForLoanEligibilityMaximumLoanTenure(
          title: 'Maximum Qualified Loan',
          value: maximumQualifiedLoanValue,
        ),
        SizedBox(
          height: windowHeight * 0.03,
        ),
        DataRowForLoanEligibilityMaximumLoanTenure(
          title: 'Maximum Property Price\nThat can be Purchased',
          value: maximumPropertyPriceValue,
        ),
        SizedBox(
          height: windowHeight * 0.03,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            PieChartDetails(
              windowHeight: windowHeight,
              centerText: msrValue + "%",
              chartTitle: 'MSR ($msrStatus)',
              statusForTextColor: msrStatus,
              dataMap: {
                "empty value": 100.0 - double.tryParse(msrValue)!,
                "data value": double.tryParse(msrValue)!,
              },
              gradientColorList: msrStatus == "FAIL"
                  ? const [
                      Color(0XFFDF5356),
                      Color(0XFFFF989A),
                    ]
                  : [
                      const Color(0XFF73CC70),
                      const Color(0X2F9A2B00).withOpacity(0),
                    ],
            ),
            PieChartDetails(
              windowHeight: windowHeight,
              centerText: tdsrValue + "%",
              chartTitle: 'TDSR ($tdsrStatus)',
              statusForTextColor: tdsrStatus,
              dataMap: {
                "empty value": 100.0 - double.tryParse(tdsrValue)!,
                "data value": double.tryParse(tdsrValue)!,
              },
              gradientColorList: tdsrStatus == "FAIL"
                  ? const [
                      Color(0XFFDF5356),
                      Color(0XFFFF989A),
                    ]
                  : [
                      const Color(0XFF73CC70),
                      const Color(0X2F9A2B00).withOpacity(0),
                    ],
            ),
          ],
        ),
        SizedBox(
          height: windowHeight * 0.03,
        ),
        DataRowForLoanEligibilityMaximumLoanTenure(
          title: 'Maximum Loan Tenure',
          value: maximumLoanTenureValue.capitalize!,
        ),
        SizedBox(
          height: windowHeight * 0.03,
        ),
        DataRowForLoanEligibilityMaximumLoanTenure(
          title: 'Maximum Loan Tenure',
          value: maximumLoanTenureValue.capitalize!,
        ),
        SizedBox(
          height: windowWidth * 0.05,
        ),
        Text(
          'You can choose Pledge and/or Unpledge Funds to pass TDSR/MSR',
          style: TextStyle(
            fontSize: FontSize.s18,
            fontWeight: FontWeight.bold,
            color: AppColors.kPrimaryColor,
          ),
        ),
        SizedBox(
          height: windowWidth * 0.05,
        ),
        DataRowForLoanEligibilityMaximumLoanTenure(
          title: 'Full Pledge Amount',
          value: fullPLedgeAmountValue,
        ),
        SizedBox(
          height: windowWidth * 0.05,
        ),
        Align(
          alignment: Alignment.center,
          child: Text(
            'OR',
            style: TextStyle(
              fontSize: FontSize.s18,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          ),
        ),
        SizedBox(
          height: windowWidth * 0.05,
        ),
        DataRowForLoanEligibilityMaximumLoanTenure(
          title: 'Full Unpledge Amount',
          value: fullUnpledgeAmountValue,
        ),
        SizedBox(
          height: windowWidth * 0.05,
        ),
        Align(
          alignment: Alignment.center,
          child: Text(
            'OR',
            style: TextStyle(
              fontSize: FontSize.s18,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          ),
        ),
        SizedBox(
          height: windowWidth * 0.05,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Your Preferred\nPledged Amount',
                  style: TextStyle(
                    color: const Color(0XFF767676),
                    fontWeight: FontWeight.w400,
                    fontSize: FontSize.s18,
                  ),
                ),
                SizedBox(
                  height: windowHeight * 0.01,
                ),
                Text(
                  preferredPledgeAmountValue,
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: FontSize.s18,
                  ),
                ),
              ],
            ),
            Text(
              '+',
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: FontSize.s18,
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Unledged Amount\n',
                  style: TextStyle(
                    color: const Color(0XFF767676),
                    fontWeight: FontWeight.w400,
                    fontSize: FontSize.s18,
                  ),
                ),
                SizedBox(
                  height: windowHeight * 0.01,
                ),
                Text(
                  unpledgeAmountValue,
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: FontSize.s18,
                  ),
                ),
              ],
            ),
          ],
        ),
        SizedBox(
          height: windowWidth * 0.05,
        ),
        Align(
          alignment: Alignment.center,
          child: Text(
            'OR',
            style: TextStyle(
              fontSize: FontSize.s18,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          ),
        ),
        SizedBox(
          height: windowWidth * 0.05,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Your Preferred\nUnpledged Amount',
                  style: TextStyle(
                    color: const Color(0XFF767676),
                    fontWeight: FontWeight.w400,
                    fontSize: FontSize.s18,
                  ),
                ),
                SizedBox(
                  height: windowHeight * 0.01,
                ),
                Text(
                  preferredUnpledgeAmountValue,
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: FontSize.s18,
                  ),
                ),
              ],
            ),
            Text(
              '+',
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: FontSize.s18,
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Pledged Amount\n',
                  style: TextStyle(
                    color: const Color(0XFF767676),
                    fontWeight: FontWeight.w400,
                    fontSize: FontSize.s18,
                  ),
                ),
                SizedBox(
                  height: windowHeight * 0.01,
                ),
                Text(
                  pledgeAmountValue,
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: FontSize.s18,
                  ),
                ),
              ],
            ),
          ],
        ),
      ],
    );
  }
}

class PieChartDetails extends StatelessWidget {
  const PieChartDetails(
      {Key? key,
      required this.windowHeight,
      required this.chartTitle,
      required this.gradientColorList,
      required this.centerText,
      required this.dataMap,
      required this.statusForTextColor})
      : super(key: key);

  final double windowHeight;
  final String chartTitle;
  final List<Color> gradientColorList;
  final String centerText;
  final String statusForTextColor;
  final Map<String, double> dataMap;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          chartTitle,
          style: TextStyle(
            color: statusForTextColor == 'PASS'
                ? AppColors.lightTextGreen
                : AppColors.kPrimaryColor,
            fontSize: FontSize.s19,
            fontWeight: FontWeight.bold,
          ),
        ),
        SizedBox(
          height: windowHeight * 0.01,
        ),
        Card(
          elevation: 10,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(80),
          ),
          child: SizedBox(
            height: windowHeight * 0.15,
            width: windowHeight * 0.15,
            child: Padding(
              padding: const EdgeInsets.all(7.0),
              child: PieChart(
                gradientList: [gradientColorList],
                emptyColorGradient: const [Colors.white, Colors.white],
                baseChartColor: Colors.white,
                // colorList: [Colors.white, Colors.white],
                centerText: centerText,
                centerTextStyle: TextStyle(
                  fontSize: FontSize.s18,
                  fontWeight: FontWeight.bold,
                  color: statusForTextColor == 'PASS'
                      ? AppColors.lightTextGreen
                      : AppColors.kPrimaryColor,
                ),
                totalValue: 100,
                dataMap: dataMap,
                animationDuration: const Duration(milliseconds: 800),
                chartLegendSpacing: 32,
                chartRadius: windowHeight * 0.12,
                initialAngleInDegree: 90,
                chartType: ChartType.ring,
                ringStrokeWidth: 14,
                legendOptions: const LegendOptions(
                  showLegendsInRow: false,
                  legendPosition: LegendPosition.top,
                  showLegends: false,
                  legendShape: BoxShape.circle,
                  legendTextStyle: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                chartValuesOptions: const ChartValuesOptions(
                  showChartValueBackground: false,
                  showChartValues: false,
                  showChartValuesInPercentage: true,
                  showChartValuesOutside: true,
                  decimalPlaces: 1,
                ),
                // gradientList: ---To add gradient colors---
                // emptyColorGradient: ---Empty Color gradient---
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class NewPurchaseApplicantDetails extends StatefulWidget {
  final TextEditingController nameController;
  final TextEditingController emailController;
  final TextEditingController phoneController;
  final TextEditingController countryCodeController;
  final TextEditingController dobController;
  final TextEditingController nationalityController;
  String selectedEmploymentTypeController;
  final TextEditingController monthlyFixedIncomeController;
  final TextEditingController annualIncomeController;
  final TextEditingController monthlyRentalIncomeController;
  final TextEditingController housingLoanController;
  final TextEditingController carLoanController;
  final TextEditingController personalEducationLoanController;
  final TextEditingController cardRepaymentController;
  final TextEditingController gurantorLoanController;
  final TextEditingController commercialPropertyPersonalLoanController;
  final TextEditingController commercialPropertyCompanyLoanController;
  final String applicantNumber;
  String totalMonthlyIncome;
  String totalFinancialCommitments;
  String selectedProperties;
  String housingLoans;

  NewPurchaseApplicantDetails({
    Key? key,
    required this.nameController,
    required this.emailController,
    required this.phoneController,
    required this.countryCodeController,
    required this.dobController,
    required this.nationalityController,
    required this.selectedEmploymentTypeController,
    required this.monthlyFixedIncomeController,
    required this.annualIncomeController,
    required this.monthlyRentalIncomeController,
    required this.housingLoanController,
    required this.carLoanController,
    required this.personalEducationLoanController,
    required this.cardRepaymentController,
    required this.gurantorLoanController,
    required this.commercialPropertyPersonalLoanController,
    required this.commercialPropertyCompanyLoanController,
    required this.applicantNumber,
    required this.totalMonthlyIncome,
    required this.totalFinancialCommitments,
    required this.selectedProperties,
    required this.housingLoans,
  }) : super(key: key);

  @override
  State<NewPurchaseApplicantDetails> createState() =>
      _NewPurchaseApplicantDetailsState();
}

class _NewPurchaseApplicantDetailsState
    extends State<NewPurchaseApplicantDetails> {
  final NewPurchaseExistingLeadController newPurchaseController = Get.find();

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    return ExpansionSection(
        initiallyExpanded: widget.applicantNumber == "1" ? true : false,
        title: "Loan Applicant #${widget.applicantNumber} Details",
        body: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: windowHeight * 0.01,
              ),
              // Text(
              //   "Loan Applicant #${widget.applicantNumber} Details",
              //   style: TextStyles.leadsColorTextStyle1,
              // ),
              // SizedBox(height: windowHeight * 0.02),
              const FormFieldTitle(title: 'Name*'),
              SizedBox(height: windowHeight * 0.01),
              RequireTextField(
                type: Type.fullname,
                labelText: "Full name",
                controller: widget.nameController,
                key: const Key("fullname"),
              ),
              SizedBox(height: windowHeight * 0.02),
              const FormFieldTitle(title: 'Mobile Number*'),
              SizedBox(height: windowHeight * 0.01),
              RequireTextField(
                type: Type.dropdownCountryCodeWithPhone,
                countryCodeController: widget.countryCodeController,
                controller: widget.phoneController,
                labelText: "Mobile Number",
                key: const Key("phone"),
              ),
              const FormFieldTitle(title: 'Email Address*'),
              SizedBox(height: windowHeight * 0.01),
              RequireTextField(
                type: Type.email,
                controller: widget.emailController,
                labelText: "Email address",
                key: const Key("email"),
              ),
              SizedBox(height: windowHeight * 0.02),
              const FormFieldTitle(title: 'Date of Birth*'),
              SizedBox(height: windowHeight * 0.01),
              RequireTextField(
                type: Type.dob,
                controller: widget.dobController,
                labelText: "DD/MM/YY",
                key: const Key("dob"),
                dateFormat: "dd-MM-yyyy",
              ),
              SizedBox(height: windowHeight * 0.02),
              const FormFieldTitle(title: 'Nationality*'),
              SizedBox(height: windowHeight * 0.01),
              RequireTextField(
                type: Type.dropdownForLeadCountry,
                controller: widget.nationalityController,
                labelText: "Country",
                dropdownForLeadCountryDefaultValue:
                    widget.nationalityController.text,
                key: const Key("Nationality"),
              ),
              SizedBox(height: windowHeight * 0.02),
              Text(
                "Income",
                style: TextStyles.leadsColorTextStyle1,
              ),
              SizedBox(height: windowHeight * 0.02),
              const FormFieldTitle(title: 'Employment Type*'),
              SizedBox(height: windowHeight * 0.01),
              Container(
                width: windowWidth,
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
                decoration: BoxDecoration(
                    border: Border.all(
                        color: AppColors.formFieldBorderColor, width: 1.0),
                    borderRadius: const BorderRadius.all(Radius.circular(
                            10) //                 <--- border radius here
                        )),
                child: DropdownButtonHideUnderline(
                  child: DropdownButton<String>(
                    hint: const Text('Select'),
                    // Not necessary for Option 1
                    value: widget.selectedEmploymentTypeController,
                    onChanged: (newValue) {
                      setState(() {
                        if (widget.applicantNumber == '1') {
                          newPurchaseController
                              .selectedEmploymentTypeMainApplicant = newValue;
                        } else if (widget.applicantNumber == '2') {
                          newPurchaseController
                              .selectedEmploymentTypeJointApplicant1 = newValue;
                        } else if (widget.applicantNumber == '3') {
                          newPurchaseController
                              .selectedEmploymentTypeJointApplicant2 = newValue;
                        } else {
                          newPurchaseController
                              .selectedEmploymentTypeJointApplicant3 = newValue;
                        }

                        widget.selectedEmploymentTypeController = newValue!;
                        debugPrint(newValue);
                      });
                    },
                    items: newPurchaseController.dropdownEmploymentType
                        .map((location) {
                      return DropdownMenuItem(
                        child: SizedBox(
                            width: windowWidth * 0.7, child: Text(location)),
                        value: location,
                      );
                    }).toList(),
                  ),
                ),
              ),
              SizedBox(height: windowHeight * 0.02),
              const FormFieldTitle(
                  title:
                      'Monthly Fixed Income* (Basic Salary, Fixed Allowance)'),
              SizedBox(height: windowHeight * 0.01),

              CurrencyAmountTextField(
                textEditingController: widget.monthlyFixedIncomeController,
                labelText: "\$",
                key: const Key("loan ammount"),
              ),

              SizedBox(height: windowHeight * 0.01),
              const FormFieldTitle(
                  title: 'Annual Income* (IRAS Notice of Assessment)'),
              SizedBox(height: windowHeight * 0.01),

              CurrencyAmountTextField(
                textEditingController: widget.annualIncomeController,
                labelText: "\$",
                key: const Key("Income"),
              ),

              SizedBox(height: windowHeight * 0.01),
              const FormFieldTitle(title: 'Monthly Rental Income*'),
              SizedBox(height: windowHeight * 0.01),

              CurrencyAmountTextField(
                textEditingController: widget.monthlyRentalIncomeController,
                labelText: "\$",
                key: const Key("Rental Income"),
              ),

              SizedBox(height: windowHeight * 0.02),
              Text(
                "Financial Commitments",
                style: TextStyles.leadsColorTextStyle1,
              ),
              SizedBox(height: windowHeight * 0.02),
              const FormFieldTitle(
                  title: 'How many Residential Properties Do you Own*'),
              SizedBox(height: windowHeight * 0.01),
              Container(
                width: windowWidth,
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
                decoration: BoxDecoration(
                    border: Border.all(
                        color: AppColors.formFieldBorderColor, width: 1.0),
                    borderRadius: const BorderRadius.all(Radius.circular(
                            10) //                 <--- border radius here
                        )),
                child: DropdownButtonHideUnderline(
                  child: DropdownButton<String>(
                    hint: const Text('Select'),
                    // Not necessary for Option 1
                    value: widget.selectedProperties,
                    onChanged: (newValue) {
                      debugPrint(newValue);

                      if (widget.applicantNumber == "1") {
                        newPurchaseController.selectedPropertiesMainApplicant =
                            newValue;
                      }

                      if (widget.applicantNumber == "2") {
                        newPurchaseController
                            .selectedPropertiesJointApplicant1 = newValue;
                      }

                      if (widget.applicantNumber == "3") {
                        newPurchaseController
                            .selectedPropertiesJointApplicant2 = newValue;
                      }

                      if (widget.applicantNumber == "4") {
                        newPurchaseController
                            .selectedPropertiesJointApplicant3 = newValue;
                      }

                      widget.selectedProperties = newValue!;

                      setState(() {});
                    },
                    items: newPurchaseController.dropdownSelectedProperties
                        .map((location) {
                      return DropdownMenuItem(
                        child: SizedBox(
                            width: windowWidth * 0.7, child: Text(location)),
                        value: location,
                      );
                    }).toList(),
                  ),
                ),
              ),
              SizedBox(height: windowHeight * 0.02),
              const FormFieldTitle(
                  title: 'How many Housing Loans Do you have*'),
              SizedBox(height: windowHeight * 0.01),
              Container(
                width: windowWidth,
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
                decoration: BoxDecoration(
                    border: Border.all(
                        color: AppColors.formFieldBorderColor, width: 1.0),
                    borderRadius: const BorderRadius.all(Radius.circular(
                            10) //                 <--- border radius here
                        )),
                child: DropdownButtonHideUnderline(
                  child: DropdownButton<String>(
                    hint: const Text('Select'),
                    // Not necessary for Option 1
                    value: widget.housingLoans,
                    onChanged: (newValue) {
                      if (widget.applicantNumber == "1") {
                        newPurchaseController.selectedHousingLoanMainApplicant!
                            .value = newValue!;
                      }

                      if (widget.applicantNumber == "2") {
                        newPurchaseController
                            .selectedHousingLoanJointApplicant1!
                            .value = newValue!;
                      }

                      if (widget.applicantNumber == "3") {
                        newPurchaseController
                            .selectedHousingLoanJointApplicant2!
                            .value = newValue!;
                      }

                      if (widget.applicantNumber == "4") {
                        newPurchaseController
                            .selectedHousingLoanJointApplicant3!
                            .value = newValue!;
                      }

                      widget.housingLoans = newValue!;
                      setState(() {});
                    },
                    items: newPurchaseController.dropdownSelectedHousingLoan
                        .map((location) {
                      return DropdownMenuItem(
                        child: SizedBox(
                            width: windowWidth * 0.7, child: Text(location)),
                        value: location,
                      );
                    }).toList(),
                  ),
                ),
              ),
              Obx(() {
                if (widget.applicantNumber == "1") {
                  return newPurchaseController
                              .selectedHousingLoanMainApplicant!.value ==
                          '0'
                      ? SizedBox(height: windowHeight * 0.01)
                      : SizedBox(height: windowHeight * 0.02);
                } else if (widget.applicantNumber == "2") {
                  return newPurchaseController
                              .selectedHousingLoanJointApplicant1!.value ==
                          '0'
                      ? SizedBox(height: windowHeight * 0.01)
                      : SizedBox(height: windowHeight * 0.02);
                } else if (widget.applicantNumber == "3") {
                  return newPurchaseController
                              .selectedHousingLoanJointApplicant2!.value ==
                          '0'
                      ? SizedBox(height: windowHeight * 0.01)
                      : SizedBox(height: windowHeight * 0.02);
                } else {
                  return newPurchaseController
                              .selectedHousingLoanJointApplicant3!.value ==
                          '0'
                      ? SizedBox(height: windowHeight * 0.01)
                      : SizedBox(height: windowHeight * 0.02);
                }
              }),
              Obx(
                () {
                  if (widget.applicantNumber == "1") {
                    return newPurchaseController
                                .selectedHousingLoanMainApplicant!.value ==
                            '0'
                        ? Container()
                        : const FormFieldTitle(
                            title:
                                'Total Monthly Installments for your Current Housing Loans*');
                  } else if (widget.applicantNumber == "2") {
                    return newPurchaseController
                                .selectedHousingLoanJointApplicant1!.value ==
                            '0'
                        ? Container()
                        : const FormFieldTitle(
                            title:
                                'Total Monthly Installments for your Current Housing Loans*');
                  } else if (widget.applicantNumber == "3") {
                    return newPurchaseController
                                .selectedHousingLoanJointApplicant2!.value ==
                            '0'
                        ? Container()
                        : const FormFieldTitle(
                            title:
                                'Total Monthly Installments for your Current Housing Loans*');
                  } else {
                    return newPurchaseController
                                .selectedHousingLoanJointApplicant3!.value ==
                            '0'
                        ? Container()
                        : const FormFieldTitle(
                            title:
                                'Total Monthly Installments for your Current Housing Loans*');
                  }
                },
              ),

              Obx(() {
                if (widget.applicantNumber == "1") {
                  return newPurchaseController
                              .selectedHousingLoanMainApplicant!.value ==
                          '0'
                      ? Container()
                      : SizedBox(height: windowHeight * 0.01);
                } else if (widget.applicantNumber == "2") {
                  return newPurchaseController
                              .selectedHousingLoanJointApplicant1!.value ==
                          '0'
                      ? Container()
                      : SizedBox(height: windowHeight * 0.01);
                } else if (widget.applicantNumber == "3") {
                  return newPurchaseController
                              .selectedHousingLoanJointApplicant2!.value ==
                          '0'
                      ? Container()
                      : SizedBox(height: windowHeight * 0.01);
                } else {
                  return newPurchaseController
                              .selectedHousingLoanJointApplicant3!.value ==
                          '0'
                      ? Container()
                      : SizedBox(height: windowHeight * 0.01);
                }
              }),

              Obx(
                () {
                  if (widget.applicantNumber == "1") {
                    newPurchaseController
                                .selectedHousingLoanMainApplicant!.value ==
                            '0'
                        ? widget.housingLoanController.text = '0'
                        : null;
                    return newPurchaseController
                                .selectedHousingLoanMainApplicant!.value ==
                            '0'
                        ? Container()
                        : CurrencyAmountTextField(
                            textEditingController: widget.housingLoanController,
                            labelText: "\$",
                            key: const Key("Total Monthly Installments"),
                          );
                  } else if (widget.applicantNumber == "2") {
                    newPurchaseController
                                .selectedHousingLoanJointApplicant1!.value ==
                            '0'
                        ? widget.housingLoanController.text = '0'
                        : null;
                    return newPurchaseController
                                .selectedHousingLoanJointApplicant1!.value ==
                            '0'
                        ? Container()
                        : CurrencyAmountTextField(
                            textEditingController: widget.housingLoanController,
                            labelText: "\$",
                            key: const Key("Total Monthly Installments"),
                          );
                  } else if (widget.applicantNumber == "3") {
                    newPurchaseController
                                .selectedHousingLoanJointApplicant2!.value ==
                            '0'
                        ? widget.housingLoanController.text = '0'
                        : null;
                    return newPurchaseController
                                .selectedHousingLoanJointApplicant2!.value ==
                            '0'
                        ? Container()
                        : CurrencyAmountTextField(
                            textEditingController: widget.housingLoanController,
                            labelText: "\$",
                            key: const Key("Total Monthly Installments"),
                          );
                  } else {
                    newPurchaseController
                                .selectedHousingLoanJointApplicant3!.value ==
                            '0'
                        ? widget.housingLoanController.text = '0'
                        : null;
                    return newPurchaseController
                                .selectedHousingLoanJointApplicant3!.value ==
                            '0'
                        ? Container()
                        : CurrencyAmountTextField(
                            textEditingController: widget.housingLoanController,
                            labelText: "\$",
                            key: const Key("Total Monthly Installments"),
                          );
                  }
                },
              ),

              SizedBox(height: windowHeight * 0.01),
              const FormFieldTitle(
                  title: 'Total Monthly Installments for Car Loan*'),
              SizedBox(height: windowHeight * 0.01),
              CurrencyAmountTextField(
                textEditingController: widget.carLoanController,
                labelText: "\$",
                key: const Key("car loan"),
              ),
              SizedBox(height: windowHeight * 0.01),
              const FormFieldTitle(
                  title:
                      'Total Monthly Installments for Personal, Renovation, Or Education Loans*'),
              SizedBox(height: windowHeight * 0.01),
              CurrencyAmountTextField(
                textEditingController: widget.personalEducationLoanController,
                labelText: "\$",
                key: const Key("personal education"),
              ),
              SizedBox(height: windowHeight * 0.01),
              const FormFieldTitle(
                  title: 'Minimum Monthly Credit Card Repayment Amount*'),
              SizedBox(height: windowHeight * 0.01),
              CurrencyAmountTextField(
                textEditingController: widget.cardRepaymentController,
                labelText: "\$",
                key: const Key("Card Repayment"),
              ),
              SizedBox(height: windowHeight * 0.01),
              const FormFieldTitle(
                  title:
                      'Total Monthly Installments if you are a Guarantor for any Loans*'),
              SizedBox(height: windowHeight * 0.01),
              CurrencyAmountTextField(
                textEditingController: widget.gurantorLoanController,
                labelText: "\$",
                key: const Key("any Loans"),
              ),
              SizedBox(height: windowHeight * 0.01),
              const FormFieldTitle(
                  title:
                      'Total Monthly Installments for Commercial Property Loan (Under Personal Name)*'),
              SizedBox(height: windowHeight * 0.01),
              CurrencyAmountTextField(
                textEditingController:
                    widget.commercialPropertyPersonalLoanController,
                labelText: "\$",
                key: const Key("Commercial Property"),
              ),
              SizedBox(height: windowHeight * 0.01),
              const FormFieldTitle(
                  title:
                      'Total Monthly Installments for Commercial Property Loan (Under Company Name)*'),
              SizedBox(height: windowHeight * 0.01),
              CurrencyAmountTextField(
                textEditingController:
                    widget.commercialPropertyCompanyLoanController,
                labelText: "\$",
                key: const Key("Company Name"),
              ),
              SizedBox(height: windowHeight * 0.03),
            ],
          ),
        ]);
  }
}

class NewPurchasePropertyDetails extends StatefulWidget {
  final TextEditingController propertyPriceController;
  final TextEditingController loanAmountController;

  String selectedLongerLoanTenure;
  final List<String> dropdownLongerLoanTenure;

  String selectedPropertyType;
  final List<String> dropdownPropertyType;

  String selectedPropertyStatus;
  final List<String> dropdownPropertyStatus;

  final TextEditingController loanTenureYearController;
  final String applicantNumber;

  String loanToValue;
  String maximumLoanTenure;
  String buyerStampDuty;
  String additionalBuyerStampDuty;
  String legalFees;
  String valuationFees;
  String maximumQualifiedLoan;

  String maximumPropertyPrice;
  String msr;
  // String msrStatus;
  // String tdsrStatus;
  String tdsr;
  String fullFludgeAmount;
  String fullUnfludgeAmount;
  String loanTenure;
  String selectedLoanType;
  NewPurchaseResponseModel newPurchaseResponseModel;
  final TextEditingController otherFeesController;
  String remainingPledgeAmountMlt;
  String tempRemainingPledgeAmountMlt;
  String remainingUnpledgeAmoutnMlt;
  String tempremainingUnpledgeAmoutnMlt;
  String remainingPledgeAmountPlt;
  String tempRemainingPledgeAmountPlt;
  String remainingUnpledgeAmoutnPlt;
  String tempremainingUnpledgeAmoutnPlt;
  String pledgeAmountMlt;
  String unPLedgeAmountMlt;
  String pledgeAmountPlt;
  String unPLedgeAmountPlt;

  final TextEditingController preferredPledgeAmountMltController;
  final TextEditingController preferredUnpledgeAmountMltController;

  final TextEditingController preferredPledgeAmountPltController;
  final TextEditingController preferredUnpledgeAmountPltController;

  NewPurchasePropertyDetails({
    Key? key,
    required this.propertyPriceController,
    required this.loanAmountController,
    required this.loanTenureYearController,
    required this.applicantNumber,
    required this.dropdownLongerLoanTenure,
    required this.dropdownPropertyType,
    required this.selectedLongerLoanTenure,
    required this.selectedPropertyType,
    required this.selectedPropertyStatus,
    required this.dropdownPropertyStatus,
    required this.loanToValue,
    required this.maximumLoanTenure,
    required this.buyerStampDuty,
    required this.additionalBuyerStampDuty,
    required this.legalFees,
    required this.valuationFees,
    required this.maximumQualifiedLoan,
    required this.maximumPropertyPrice,
    required this.msr,
    // required this.msrStatus,
    required this.tdsr,
    // required this.tdsrStatus,
    required this.fullFludgeAmount,
    required this.fullUnfludgeAmount,
    required this.loanTenure,
    required this.selectedLoanType,
    required this.newPurchaseResponseModel,
    required this.otherFeesController,
    required this.remainingPledgeAmountMlt, // Remaining pledge amt MLT
    required this.tempRemainingPledgeAmountMlt, // temp remaining pledge amt MLT
    required this.remainingUnpledgeAmoutnMlt, // Remaining unpledged amt MLT
    required this.tempremainingUnpledgeAmoutnMlt, // temp remaining unpledged amt MLT
    required this.preferredPledgeAmountMltController,
    required this.preferredUnpledgeAmountMltController,
    required this.remainingPledgeAmountPlt, // Remaining pledge amt PLT
    required this.tempRemainingPledgeAmountPlt, // temp remaining pledge amt PLT
    required this.remainingUnpledgeAmoutnPlt, // Remaining unpledged amt PLT
    required this.tempremainingUnpledgeAmoutnPlt, // temp remaining unpledged amt PLT
    required this.preferredPledgeAmountPltController,
    required this.preferredUnpledgeAmountPltController,
    required this.pledgeAmountMlt, // Pledge amt MLT
    required this.unPLedgeAmountMlt, // Unpledged amt MLT
    required this.pledgeAmountPlt, // Pledge amt PLT
    required this.unPLedgeAmountPlt, // Unpledged amt PLT
  }) : super(key: key);

  @override
  State<NewPurchasePropertyDetails> createState() =>
      _NewPurchasePropertyDetailsState();
}

class _NewPurchasePropertyDetailsState
    extends State<NewPurchasePropertyDetails> {
  final NewPurchaseExistingLeadController newPurchaseController =
      Get.put(NewPurchaseExistingLeadController());
  static const _locale = 'en';
  String _formatNumber(String s) =>
      NumberFormat.decimalPattern(_locale).format(double.parse(s));

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;

    return ExpansionSection(
        initiallyExpanded: widget.applicantNumber == "1" ? true : false,
        title: "Subject Property #${widget.applicantNumber}",
        body: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              /*SizedBox(
          height: windowHeight * 0.01,
        ),
        Text(
          "Subject Property",
          style: TextStyles.leadsColorTextStyle1,
        ),*/
              SizedBox(height: windowHeight * 0.02),
              const FormFieldTitle(title: 'Property Price*'),
              SizedBox(height: windowHeight * 0.01),
              CurrencyAmountWithOnchangedTextField(
                textEditingController: widget.propertyPriceController,
                labelText: "\$",
                key: const Key("Property"),
                onChanged: (value) {
                  value = _formatNumber(value!.replaceAll(',', ''));
                  widget.propertyPriceController.value = TextEditingValue(
                    text: value,
                    selection: TextSelection.collapsed(offset: value.length),
                  );
                  newPurchaseController
                      .checkLTVvalueToHideShowLongerTenureLoanCondition(
                          loanAmount:
                              double
                                  .tryParse(
                                      widget.loanAmountController.text.replaceAll(
                                          ',',
                                          ''))!,
                          propertyPrice: double.tryParse(value.replaceAll(',',
                              ''))!,
                          totalNumberOfHousingLoan:
                              newPurchaseController
                                  .calculateTotalHousingLoan(
                                      newPurchaseController
                                          .selectedHousingLoanMainApplicant!
                                          .value,
                                      newPurchaseController
                                          .selectedHousingLoanJointApplicant1!
                                          .value,
                                      newPurchaseController
                                          .selectedHousingLoanJointApplicant2!
                                          .value,
                                      newPurchaseController
                                          .selectedHousingLoanJointApplicant3!
                                          .value));
                },
              ),
              // CurrencyAmountTextField(
              //   textEditingController: widget.propertyPriceController,
              //   labelText: "\$",
              //   key: const Key("Property"),
              // ),
              SizedBox(height: windowHeight * 0.01),
              const FormFieldTitle(title: 'Loan Amount*'),
              SizedBox(height: windowHeight * 0.01),
              CurrencyAmountWithOnchangedTextField(
                textEditingController: widget.loanAmountController,
                labelText: "\$",
                key: const Key("Loan Amount*"),
                onChanged: (value) {
                  value = _formatNumber(value!.replaceAll(',', ''));
                  widget.loanAmountController.value = TextEditingValue(
                    text: value,
                    selection: TextSelection.collapsed(offset: value.length),
                  );
                  newPurchaseController
                      .checkLTVvalueToHideShowLongerTenureLoanCondition(
                          loanAmount: double.tryParse(value.replaceAll(
                              ',', ''))!,
                          propertyPrice: double
                              .tryParse(
                                  widget.propertyPriceController.text.replaceAll(
                                      ',',
                                      ''))!,
                          totalNumberOfHousingLoan:
                              newPurchaseController
                                  .calculateTotalHousingLoan(
                                      newPurchaseController
                                          .selectedHousingLoanMainApplicant!
                                          .value,
                                      newPurchaseController
                                          .selectedHousingLoanJointApplicant1!
                                          .value,
                                      newPurchaseController
                                          .selectedHousingLoanJointApplicant2!
                                          .value,
                                      newPurchaseController
                                          .selectedHousingLoanJointApplicant3!
                                          .value));
                },
              ),
              Obx(() =>
                  newPurchaseController.showLongerLoanTenureCondition.value
                      ? SizedBox(height: windowHeight * 0.01)
                      : Container()),
              Obx(
                () => newPurchaseController.showLongerLoanTenureCondition.value
                    ? const FormFieldTitle(
                        title: 'Would you like to have a Longer Loan Tenure*')
                    : Container(),
              ),
              Obx(() =>
                  newPurchaseController.showLongerLoanTenureCondition.value
                      ? SizedBox(height: windowHeight * 0.01)
                      : Container()),
              Obx(
                () => newPurchaseController.showLongerLoanTenureCondition.value
                    ? Container(
                        width: windowWidth,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 15, vertical: 3),
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: AppColors.formFieldBorderColor,
                                width: 1.0),
                            borderRadius: const BorderRadius.all(
                                Radius.circular(
                                    10) //                 <--- border radius here
                                )),
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton<String>(
                            hint: const Text('Select'),
                            // Not necessary for Option 1
                            value: widget.selectedLongerLoanTenure,
                            onChanged: (newValue) {
                              newPurchaseController
                                  .selectedLongerLoanTenureMainApplicant
                                  .value = newValue!;
                              widget.selectedLongerLoanTenure = newValue;
                              setState(() {});
                            },
                            items:
                                widget.dropdownLongerLoanTenure.map((location) {
                              return DropdownMenuItem(
                                child: SizedBox(
                                    width: windowWidth * 0.7,
                                    child: Text(location)),
                                value: location,
                              );
                            }).toList(),
                          ),
                        ),
                      )
                    : Container(),
              ),
              Obx(() =>
                  newPurchaseController.showLongerLoanTenureCondition.value
                      ? SizedBox(height: windowHeight * 0.02)
                      : SizedBox(height: windowHeight * 0.01)),
              const FormFieldTitle(title: 'Property Type*'),
              SizedBox(height: windowHeight * 0.01),
              Container(
                width: windowWidth,
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
                decoration: BoxDecoration(
                    border: Border.all(
                        color: AppColors.formFieldBorderColor, width: 1.0),
                    borderRadius: const BorderRadius.all(Radius.circular(
                            10) //                 <--- border radius here
                        )),
                child: DropdownButtonHideUnderline(
                  child: DropdownButton<String>(
                    hint: const Text('Select'),
                    // Not necessary for Option 1
                    value: widget.selectedPropertyType,
                    onChanged: (newValue) {
                      // ! if-else condition to check the show and hide of longer loan tenure Yes/No condition
                      // if (newPurchaseController
                      //             .ltvValueToShowAndHideTheLongerLoanTenureCondition
                      //             .roundToDouble() <=
                      //         55.0
                      //     // &&newValue == 'HDB'
                      //     ) {
                      //   newPurchaseController
                      //           .showLongerLoanTenureCondition.value =
                      //       true; //!Show the longer loan tenure yes/no field
                      // }
                      // // else if (newPurchaseController
                      // //             .ltvValueToShowAndHideTheLongerLoanTenureCondition
                      // //             .roundToDouble() <=
                      // //         55.0 &&
                      // //     newValue == 'EC (From Developer/ within MOP)') {
                      // //   newPurchaseController
                      // //           .showLongerLoanTenureCondition.value =
                      // //       true; //!Show the longer loan tenure yes/no field
                      // // }
                      // // else if (newValue != 'HDB' &&
                      // //     newValue != 'EC (From Developer/ within MOP)') {
                      // //   newPurchaseController
                      // //           .showLongerLoanTenureCondition.value =
                      // //       true; //!Show the longer loan tenure yes/no field
                      // // }
                      // else {
                      //   newPurchaseController
                      //           .showLongerLoanTenureCondition.value =
                      //       false; //!Hide the longer loan tenure yes/no field
                      //   newPurchaseController
                      //       .selectedLongerLoanTenureMainApplicant.value = 'No';
                      // }
                      setState(() {
                        newPurchaseController
                            .selectedPropertyTypeMainApplicant = newValue;
                        widget.selectedPropertyType = newValue!;

                        debugPrint(newValue);
                        newPurchaseController.indexOfPropertyTypeDropDown =
                            newPurchaseController.dropdownPropertyType
                                .indexWhere((element) =>
                                    element ==
                                    newPurchaseController
                                        .selectedPropertyTypeMainApplicant);
                        debugPrint(newPurchaseController
                            .indexOfPropertyTypeDropDown
                            .toString());
                      });
                    },
                    items: widget.dropdownPropertyType.map((location) {
                      return DropdownMenuItem(
                        child: SizedBox(
                            width: windowWidth * 0.7, child: Text(location)),
                        value: location,
                      );
                    }).toList(),
                  ),
                ),
              ),
              // SizedBox(height: windowHeight * 0.02),
              // const FormFieldTitle(title: 'Property Status*'),
              // SizedBox(height: windowHeight * 0.01),
              // Container(
              //   width: windowWidth,
              //   padding:
              //       const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
              //   decoration: BoxDecoration(
              //       border: Border.all(
              //           color: AppColors.formFieldBorderColor, width: 1.0),
              //       borderRadius: const BorderRadius.all(Radius.circular(
              //               10) //                 <--- border radius here
              //           )),
              //   child: DropdownButtonHideUnderline(
              //     child: DropdownButton<String>(
              //       hint: const Text('Select'),
              //       // Not necessary for Option 1
              //       value: widget.selectedPropertyStatus,
              //       onChanged: (newValue) {
              //         newPurchaseController
              //             .selectedPropertyStatusMainApplicant = newValue;
              //         widget.selectedPropertyStatus = newValue!;
              //         setState(() {});
              //       },
              //       items: widget.dropdownPropertyStatus.map((location) {
              //         return DropdownMenuItem(
              //           child: SizedBox(
              //               width: windowWidth * 0.7, child: Text(location)),
              //           value: location,
              //         );
              //       }).toList(),
              //     ),
              //   ),
              // ),
              // SizedBox(height: windowHeight * 0.02),
              // const FormFieldTitle(title: 'Loan Category*'),
              // SizedBox(height: windowHeight * 0.01),
              // Container(
              //   width: windowWidth,
              //   padding:
              //       const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
              //   decoration: BoxDecoration(
              //       border: Border.all(
              //           color: AppColors.formFieldBorderColor, width: 1.0),
              //       borderRadius: const BorderRadius.all(Radius.circular(
              //               10) //                 <--- border radius here
              //           )),
              //   child: DropdownButtonHideUnderline(
              //     child: DropdownButton(
              //       hint: const Text('Select Loan Category'),
              //       // Not necessary for Option 1
              //       value: widget.selectedLoanType,
              //       onChanged: (newValue) {
              //         setState(() {
              //           newPurchaseController.selectedLoanTypeMainApplicant =
              //               newValue.toString();
              //           widget.selectedLoanType = newValue.toString();
              //         });
              //       },
              //       items: newPurchaseController.loanTypes.map((location) {
              //         return DropdownMenuItem(
              //           child: Text(
              //             location
              //                 .toString()
              //                 .replaceAll('_', ' ')
              //                 .capitalizeFirst!,
              //           ),
              //           value: location,
              //         );
              //       }).toList(),
              //     ),
              //   ),
              // ),
              SizedBox(height: windowHeight * 0.01),
              const FormFieldTitle(title: 'Your Preferred Loan Tenure (Years)'),
              SizedBox(height: windowHeight * 0.01),
              RequireTextField(
                type: Type.number,
                labelText: "",
                controller: widget.loanTenureYearController,
                key: const Key("Company Name"),
              ),
              Obx(() => newPurchaseController.isCalculated.value
                  ? Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: windowHeight * 0.02),
                        Card(
                          shape: const RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5))),
                          elevation: 5,
                          margin: const EdgeInsets.symmetric(
                              horizontal: 5, vertical: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0, top: 5, bottom: 20),
                                child: Text(
                                  "Loan-to-Value (LTV)",
                                  style:
                                      TextStyles.bankSubmissionTaskTextStyle1,
                                ),
                              ),
                              const Divider(color: Colors.deepOrange),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0, top: 5, bottom: 20),
                                child: Text(
                                  widget.newPurchaseResponseModel.loanToValue
                                      .toString(),
                                  style: TextStyles.bankSubmissionBody,
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: windowHeight * 0.02),
                        Card(
                          shape: const RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5))),
                          elevation: 5,
                          margin: const EdgeInsets.symmetric(
                              horizontal: 5, vertical: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0, top: 5, bottom: 20, right: 10),
                                child: Text(
                                  "Maximum Loan Tenure (Years) You Can Qualify",
                                  style:
                                      TextStyles.bankSubmissionTaskTextStyle1,
                                ),
                              ),
                              const Divider(color: Colors.deepOrange),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0, top: 5, bottom: 20),
                                child: Text(
                                  widget.newPurchaseResponseModel
                                      .maximumLoanTenure
                                      .toString(),
                                  style: TextStyles.bankSubmissionBody,
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: windowHeight * 0.02),
                        Text(
                          "Affordability Analysis",
                          style: TextStyles.leadsColorTextStyle1,
                        ),
                        Text(
                          "(Based on Maximum Loan Tenure You Can Qualify)",
                          style: TextStyles.leadsNormalTextStyle,
                        ),
                        SizedBox(height: windowHeight * 0.02),
                        Text(
                          "Based on your current financial status",
                          style: TextStyles.leadsNormalTextStyle,
                        ),
                        SizedBox(height: windowHeight * 0.02),
                        Card(
                          shape: const RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5))),
                          elevation: 5,
                          margin: const EdgeInsets.symmetric(
                              horizontal: 5, vertical: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0, top: 5, bottom: 20, right: 10),
                                child: Text(
                                  "Your Mortgage Servicing Ratio (MSR)",
                                  style:
                                      TextStyles.bankSubmissionTaskTextStyle1,
                                ),
                              ),
                              const Divider(color: Colors.deepOrange),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0, top: 5, bottom: 20),
                                child: Text(
                                  "${widget.newPurchaseResponseModel.maximumTenureBasedAffordability == null ? 0 : widget.newPurchaseResponseModel.maximumTenureBasedAffordability!.msr.toString()}% ${widget.newPurchaseResponseModel.maximumTenureBasedAffordability!.msrStatus.toString().toUpperCase()}",
                                  style: TextStyle(
                                    color: widget
                                                .newPurchaseResponseModel
                                                .maximumTenureBasedAffordability!
                                                .msrStatus
                                                .toString()
                                                .toUpperCase() ==
                                            "FAIL"
                                        ? Colors.deepOrange
                                        : Colors.green,
                                    fontSize: FontSize.s16,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Card(
                          shape: const RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5))),
                          elevation: 5,
                          margin: const EdgeInsets.symmetric(
                              horizontal: 5, vertical: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0, top: 5, bottom: 20, right: 10),
                                child: Text(
                                  "Your Total Debt Servicing Ratio (TDSR)",
                                  style:
                                      TextStyles.bankSubmissionTaskTextStyle1,
                                ),
                              ),
                              const Divider(color: Colors.deepOrange),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0, top: 5, bottom: 20),
                                child: Text(
                                  "${widget.newPurchaseResponseModel.maximumTenureBasedAffordability == null ? 0 : widget.newPurchaseResponseModel.maximumTenureBasedAffordability!.tdsr.toString()}% ${widget.newPurchaseResponseModel.maximumTenureBasedAffordability!.tdsrStatus.toString().toUpperCase()}",
                                  style: TextStyle(
                                    color: widget
                                                .newPurchaseResponseModel
                                                .maximumTenureBasedAffordability!
                                                .tdsrStatus
                                                .toString()
                                                .toUpperCase() ==
                                            "FAIL"
                                        ? Colors.deepOrange
                                        : Colors.green,
                                    fontSize: FontSize.s16,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: windowHeight * 0.02),

                        Card(
                          shape: const RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5))),
                          elevation: 5,
                          margin: const EdgeInsets.symmetric(
                              horizontal: 5, vertical: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0, top: 5, bottom: 20, right: 10),
                                child: Text(
                                  "Maximum Loan Qualified",
                                  style:
                                      TextStyles.bankSubmissionTaskTextStyle1,
                                ),
                              ),
                              const Divider(color: Colors.deepOrange),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0, top: 5, bottom: 20),
                                child: Text(
                                  "\$ ${widget.newPurchaseResponseModel.maximumTenureBasedAffordability == null ? 0 : widget.newPurchaseResponseModel.maximumTenureBasedAffordability!.maximumQualifiedLoan.toString()}",
                                  style: TextStyles.bankSubmissionBody,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Card(
                          shape: const RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5))),
                          elevation: 5,
                          margin: const EdgeInsets.symmetric(
                              horizontal: 5, vertical: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0, top: 5, bottom: 20, right: 10),
                                child: Text(
                                  "Maximum Property Price that can be Purchased",
                                  style:
                                      TextStyles.bankSubmissionTaskTextStyle1,
                                ),
                              ),
                              const Divider(color: Colors.deepOrange),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0, top: 5, bottom: 20),
                                child: Text(
                                  "\$ ${widget.newPurchaseResponseModel.maximumTenureBasedAffordability == null ? 0 : widget.newPurchaseResponseModel.maximumTenureBasedAffordability!.maximumPropertyPrice.toString()}",
                                  style: TextStyles.bankSubmissionBody,
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: windowHeight * 0.02),

                        // ! Condition to hide UI if TDSR and MSR is PASS(Maximum)
                        widget
                                        .newPurchaseResponseModel
                                        .maximumTenureBasedAffordability!
                                        .tdsrStatus
                                        .toString()
                                        .toUpperCase() ==
                                    "PASS" &&
                                widget
                                        .newPurchaseResponseModel
                                        .maximumTenureBasedAffordability!
                                        .msrStatus
                                        .toString()
                                        .toUpperCase() ==
                                    "PASS"
                            ? Container()
                            : SizedBox(height: windowHeight * 0.02),
                        // ! Condition to hide UI if TDSR and MSR is PASS(Maximum)
                        widget
                                        .newPurchaseResponseModel
                                        .maximumTenureBasedAffordability!
                                        .tdsrStatus
                                        .toString()
                                        .toUpperCase() ==
                                    "PASS" &&
                                widget
                                        .newPurchaseResponseModel
                                        .maximumTenureBasedAffordability!
                                        .msrStatus
                                        .toString()
                                        .toUpperCase() ==
                                    "PASS"
                            ? Container()
                            : Text(
                                "In order to pass MSR/TDSR, you may :\n\nPlace this amount as a Fixed Deposit with the bank for 4 years",
                                style: TextStyles.leadsNormalTextStyle,
                              ),
                        // ! Condition to hide UI if TDSR and MSR is PASS(Maximum)
                        widget
                                        .newPurchaseResponseModel
                                        .maximumTenureBasedAffordability!
                                        .tdsrStatus
                                        .toString()
                                        .toUpperCase() ==
                                    "PASS" &&
                                widget
                                        .newPurchaseResponseModel
                                        .maximumTenureBasedAffordability!
                                        .msrStatus
                                        .toString()
                                        .toUpperCase() ==
                                    "PASS"
                            ? Container()
                            : SizedBox(height: windowHeight * 0.01),
                        // ! Condition to hide UI if TDSR and MSR is PASS(Maximum)
                        widget
                                        .newPurchaseResponseModel
                                        .maximumTenureBasedAffordability!
                                        .tdsrStatus
                                        .toString()
                                        .toUpperCase() ==
                                    "PASS" &&
                                widget
                                        .newPurchaseResponseModel
                                        .maximumTenureBasedAffordability!
                                        .msrStatus
                                        .toString()
                                        .toUpperCase() ==
                                    "PASS"
                            ? Container()
                            : Card(
                                shape: const RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5))),
                                elevation: 5,
                                margin: const EdgeInsets.symmetric(
                                    horizontal: 5, vertical: 10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 20.0,
                                          top: 5,
                                          bottom: 20,
                                          right: 10),
                                      child: Text(
                                        "Full Pledge Amount",
                                        style: TextStyles
                                            .bankSubmissionTaskTextStyle1,
                                      ),
                                    ),
                                    const Divider(color: Colors.deepOrange),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 20.0, top: 5, bottom: 20),
                                      child: Text(
                                        "\$ ${widget.newPurchaseResponseModel.maximumTenureBasedAffordability == null ? 0 : widget.newPurchaseResponseModel.maximumTenureBasedAffordability!.fullFludgeAmount.toString()}",
                                        style: TextStyles.bankSubmissionBody,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                        // ! Condition to hide UI if TDSR and MSR is PASS(Maximum)
                        widget
                                        .newPurchaseResponseModel
                                        .maximumTenureBasedAffordability!
                                        .tdsrStatus
                                        .toString()
                                        .toUpperCase() ==
                                    "PASS" &&
                                widget
                                        .newPurchaseResponseModel
                                        .maximumTenureBasedAffordability!
                                        .msrStatus
                                        .toString()
                                        .toUpperCase() ==
                                    "PASS"
                            ? Container()
                            : SizedBox(height: windowHeight * 0.02),
                        // ! Condition to hide UI if TDSR and MSR is PASS(Maximum)
                        widget
                                        .newPurchaseResponseModel
                                        .maximumTenureBasedAffordability!
                                        .tdsrStatus
                                        .toString()
                                        .toUpperCase() ==
                                    "PASS" &&
                                widget
                                        .newPurchaseResponseModel
                                        .maximumTenureBasedAffordability!
                                        .msrStatus
                                        .toString()
                                        .toUpperCase() ==
                                    "PASS"
                            ? Container()
                            : Text(
                                "OR \n\nShow the bank this amount in your bank account at the point of loan application and 7 days before loan disbursement.",
                                style: TextStyles.leadsNormalTextStyle,
                              ),
                        // ! Condition to hide UI if TDSR and MSR is PASS(Maximum)
                        widget
                                        .newPurchaseResponseModel
                                        .maximumTenureBasedAffordability!
                                        .tdsrStatus
                                        .toString()
                                        .toUpperCase() ==
                                    "PASS" &&
                                widget
                                        .newPurchaseResponseModel
                                        .maximumTenureBasedAffordability!
                                        .msrStatus
                                        .toString()
                                        .toUpperCase() ==
                                    "PASS"
                            ? Container()
                            : SizedBox(height: windowHeight * 0.01),
                        // ! Condition to hide UI if TDSR and MSR is PASS(Maximum)
                        widget
                                        .newPurchaseResponseModel
                                        .maximumTenureBasedAffordability!
                                        .tdsrStatus
                                        .toString()
                                        .toUpperCase() ==
                                    "PASS" &&
                                widget
                                        .newPurchaseResponseModel
                                        .maximumTenureBasedAffordability!
                                        .msrStatus
                                        .toString()
                                        .toUpperCase() ==
                                    "PASS"
                            ? Container()
                            : Card(
                                shape: const RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5))),
                                elevation: 5,
                                margin: const EdgeInsets.symmetric(
                                    horizontal: 5, vertical: 10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 20.0,
                                          top: 5,
                                          bottom: 20,
                                          right: 10),
                                      child: Text(
                                        "Full Un-Pledged Amount",
                                        style: TextStyles
                                            .bankSubmissionTaskTextStyle1,
                                      ),
                                    ),
                                    const Divider(color: Colors.deepOrange),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 20.0, top: 5, bottom: 20),
                                      child: Text(
                                        "\$ ${widget.newPurchaseResponseModel.maximumTenureBasedAffordability == null ? 0 : widget.newPurchaseResponseModel.maximumTenureBasedAffordability!.fullUnFludgeAmount.toString()}",
                                        style: TextStyles.bankSubmissionBody,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                        // ! Condition to hide UI if TDSR and MSR is PASS(Maximum)
                        widget
                                        .newPurchaseResponseModel
                                        .maximumTenureBasedAffordability!
                                        .tdsrStatus
                                        .toString()
                                        .toUpperCase() ==
                                    "PASS" &&
                                widget
                                        .newPurchaseResponseModel
                                        .maximumTenureBasedAffordability!
                                        .msrStatus
                                        .toString()
                                        .toUpperCase() ==
                                    "PASS"
                            ? Container()
                            : SizedBox(height: windowHeight * 0.02),
                        // ! Condition to hide UI if TDSR and MSR is PASS(Maximum)
                        widget
                                        .newPurchaseResponseModel
                                        .maximumTenureBasedAffordability!
                                        .tdsrStatus
                                        .toString()
                                        .toUpperCase() ==
                                    "PASS" &&
                                widget
                                        .newPurchaseResponseModel
                                        .maximumTenureBasedAffordability!
                                        .msrStatus
                                        .toString()
                                        .toUpperCase() ==
                                    "PASS"
                            ? Container()
                            : Text(
                                "OR \n\nYou may choose your preferred Pledged and Unpledged Funds to pass TDSR/MSR.",
                                style: TextStyles.leadsNormalTextStyle,
                              ),
                        SizedBox(height: windowHeight * 0.01),
                        // ! Condition to hide UI if TDSR and MSR is PASS(Maximum)
                        widget
                                        .newPurchaseResponseModel
                                        .maximumTenureBasedAffordability!
                                        .tdsrStatus
                                        .toString()
                                        .toUpperCase() ==
                                    "PASS" &&
                                widget
                                        .newPurchaseResponseModel
                                        .maximumTenureBasedAffordability!
                                        .msrStatus
                                        .toString()
                                        .toUpperCase() ==
                                    "PASS"
                            ? Container()
                            : Row(
                                children: [
                                  Expanded(
                                    flex: 3,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        const FormFieldTitle(
                                            title:
                                                'Your Preferred \nPledged Amount'),
                                        SizedBox(height: windowHeight * 0.01),
                                        RequireTextField(
                                          type: Type.number,
                                          labelText: "\$ 500,000",
                                          controller: widget
                                              .preferredPledgeAmountMltController,
                                          key: const Key("Your Preferred"),
                                          onChanged: (String? val) {
                                            if (val!.isNotEmpty) {
                                              newPurchaseController
                                                  .getUnpledgeAmountMlt();
                                            }
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    child: Center(
                                      child: Text(
                                        "+",
                                        style: TextStyles.docTextStyle,
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 4,
                                    child: Card(
                                      shape: const RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(5))),
                                      elevation: 5,
                                      margin: const EdgeInsets.symmetric(
                                          horizontal: 5, vertical: 10),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 20.0,
                                                top: 5,
                                                bottom: 20,
                                                right: 10),
                                            child: Text(
                                              "Remaining UnPledged\nAmount Required",
                                              style: TextStyles
                                                  .bankSubmissionTaskTextStyle1,
                                            ),
                                          ),
                                          const Divider(
                                              color: Colors.deepOrange),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 20.0, top: 5, bottom: 20),
                                            child: Text(
                                              "\$ ${widget.unPLedgeAmountMlt.toString()}",
                                              style:
                                                  TextStyles.bankSubmissionBody,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                        // ! Condition to hide UI if TDSR and MSR is PASS(Maximum)
                        widget
                                        .newPurchaseResponseModel
                                        .maximumTenureBasedAffordability!
                                        .tdsrStatus
                                        .toString()
                                        .toUpperCase() ==
                                    "PASS" &&
                                widget
                                        .newPurchaseResponseModel
                                        .maximumTenureBasedAffordability!
                                        .msrStatus
                                        .toString()
                                        .toUpperCase() ==
                                    "PASS"
                            ? Container()
                            : SizedBox(height: windowHeight * 0.01),
                        // ! Condition to hide UI if TDSR and MSR is PASS(Maximum)
                        widget
                                        .newPurchaseResponseModel
                                        .maximumTenureBasedAffordability!
                                        .tdsrStatus
                                        .toString()
                                        .toUpperCase() ==
                                    "PASS" &&
                                widget
                                        .newPurchaseResponseModel
                                        .maximumTenureBasedAffordability!
                                        .msrStatus
                                        .toString()
                                        .toUpperCase() ==
                                    "PASS"
                            ? Container()
                            : Text(
                                "OR",
                                style: TextStyles.leadsNormalTextStyle,
                              ),
                        // ! Condition to hide UI if TDSR and MSR is PASS (Maximum)
                        widget
                                        .newPurchaseResponseModel
                                        .maximumTenureBasedAffordability!
                                        .tdsrStatus
                                        .toString()
                                        .toUpperCase() ==
                                    "PASS" &&
                                widget
                                        .newPurchaseResponseModel
                                        .maximumTenureBasedAffordability!
                                        .msrStatus
                                        .toString()
                                        .toUpperCase() ==
                                    "PASS"
                            ? Container()
                            : Row(
                                children: [
                                  Expanded(
                                    flex: 3,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        const FormFieldTitle(
                                            title:
                                                'Your Preferred \n UnPledged Amount'),
                                        SizedBox(height: windowHeight * 0.01),
                                        RequireTextField(
                                          type: Type.number,
                                          labelText: "\$ 24",
                                          controller: widget
                                              .preferredUnpledgeAmountMltController,
                                          key: const Key("Your UnPreferred"),
                                          onChanged: (String? val) {
                                            if (val!.isNotEmpty) {
                                              newPurchaseController
                                                  .getPledgeAmountMlt();
                                            }
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    child: Center(
                                      child: Text(
                                        "+",
                                        style: TextStyles.docTextStyle,
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 4,
                                    child: Card(
                                      shape: const RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(5))),
                                      elevation: 5,
                                      margin: const EdgeInsets.symmetric(
                                          horizontal: 5, vertical: 10),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 20.0,
                                                top: 5,
                                                bottom: 20,
                                                right: 10),
                                            child: Text(
                                              "Remaining Pledged\nAmount Required",
                                              style: TextStyles
                                                  .bankSubmissionTaskTextStyle1,
                                            ),
                                          ),
                                          const Divider(
                                              color: Colors.deepOrange),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 20.0, top: 5, bottom: 20),
                                            child: Text(
                                              "\$ ${widget.pledgeAmountMlt.toString()}",
                                              style:
                                                  TextStyles.bankSubmissionBody,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                        SizedBox(height: windowHeight * 0.02),
                        widget.newPurchaseResponseModel
                                    .preferedTenureBasedAffordability ==
                                null
                            ? Container()
                            : Text(
                                "Affordability Analysis",
                                style: TextStyles.leadsColorTextStyle1,
                              ),
                        widget.newPurchaseResponseModel
                                    .preferedTenureBasedAffordability ==
                                null
                            ? Container()
                            : Text(
                                "(Based on Your Preferred Loan Tenure)",
                                style: TextStyles.leadsNormalTextStyle,
                              ),
                        widget.newPurchaseResponseModel
                                    .preferedTenureBasedAffordability ==
                                null
                            ? Container()
                            : SizedBox(height: windowHeight * 0.02),
                        widget.newPurchaseResponseModel
                                    .preferedTenureBasedAffordability ==
                                null
                            ? Container()
                            : Text(
                                "Based on your current financial status",
                                style: TextStyles.leadsNormalTextStyle,
                              ),
                        widget.newPurchaseResponseModel
                                    .preferedTenureBasedAffordability ==
                                null
                            ? Container()
                            : SizedBox(height: windowHeight * 0.02),
                        widget.newPurchaseResponseModel
                                    .preferedTenureBasedAffordability ==
                                null
                            ? Container()
                            : Card(
                                shape: const RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5))),
                                elevation: 5,
                                margin: const EdgeInsets.symmetric(
                                    horizontal: 5, vertical: 10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 20.0,
                                          top: 5,
                                          bottom: 20,
                                          right: 10),
                                      child: Text(
                                        "Your Mortgage Servicing Ratio (MSR)",
                                        style: TextStyles
                                            .bankSubmissionTaskTextStyle1,
                                      ),
                                    ),
                                    const Divider(color: Colors.deepOrange),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 20.0, top: 5, bottom: 20),
                                      child: Text(
                                        "${widget.newPurchaseResponseModel.preferedTenureBasedAffordability == null ? 0 : widget.newPurchaseResponseModel.preferedTenureBasedAffordability!.msr.toString()}% ${widget.newPurchaseResponseModel.preferedTenureBasedAffordability!.msrStatus.toString().toUpperCase()}",
                                        style: TextStyle(
                                          color: widget
                                                      .newPurchaseResponseModel
                                                      .preferedTenureBasedAffordability!
                                                      .msrStatus
                                                      .toString()
                                                      .toUpperCase() ==
                                                  "FAIL"
                                              ? Colors.deepOrange
                                              : Colors.green,
                                          fontSize: FontSize.s16,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                        widget.newPurchaseResponseModel
                                    .preferedTenureBasedAffordability ==
                                null
                            ? Container()
                            : Card(
                                shape: const RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5))),
                                elevation: 5,
                                margin: const EdgeInsets.symmetric(
                                    horizontal: 5, vertical: 10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 20.0,
                                          top: 5,
                                          bottom: 20,
                                          right: 10),
                                      child: Text(
                                        "Your Total Debt Servicing Ratio (TDSR)",
                                        style: TextStyles
                                            .bankSubmissionTaskTextStyle1,
                                      ),
                                    ),
                                    const Divider(color: Colors.deepOrange),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 20.0, top: 5, bottom: 20),
                                      child: Text(
                                        "${widget.newPurchaseResponseModel.preferedTenureBasedAffordability == null ? 0 : widget.newPurchaseResponseModel.preferedTenureBasedAffordability!.tdsr.toString()}% ${widget.newPurchaseResponseModel.preferedTenureBasedAffordability!.tdsrStatus.toString().toUpperCase()}",
                                        style: TextStyle(
                                          color: widget
                                                      .newPurchaseResponseModel
                                                      .preferedTenureBasedAffordability!
                                                      .tdsrStatus
                                                      .toString()
                                                      .toUpperCase() ==
                                                  "FAIL"
                                              ? Colors.deepOrange
                                              : Colors.green,
                                          fontSize: FontSize.s16,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                        widget.newPurchaseResponseModel
                                    .preferedTenureBasedAffordability ==
                                null
                            ? Container()
                            : SizedBox(height: windowHeight * 0.02),
                        widget.newPurchaseResponseModel
                                    .preferedTenureBasedAffordability ==
                                null
                            ? Container()
                            : Card(
                                shape: const RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5))),
                                elevation: 5,
                                margin: const EdgeInsets.symmetric(
                                    horizontal: 5, vertical: 10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 20.0,
                                          top: 5,
                                          bottom: 20,
                                          right: 10),
                                      child: Text(
                                        "Maximum Loan Qualified",
                                        style: TextStyles
                                            .bankSubmissionTaskTextStyle1,
                                      ),
                                    ),
                                    const Divider(color: Colors.deepOrange),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 20.0, top: 5, bottom: 20),
                                      child: Text(
                                        "\$ ${widget.newPurchaseResponseModel.preferedTenureBasedAffordability == null ? 0 : widget.newPurchaseResponseModel.preferedTenureBasedAffordability!.maximumQualifiedLoan.toString()}",
                                        style: TextStyles.bankSubmissionBody,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                        widget.newPurchaseResponseModel
                                    .preferedTenureBasedAffordability ==
                                null
                            ? Container()
                            : Card(
                                shape: const RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5))),
                                elevation: 5,
                                margin: const EdgeInsets.symmetric(
                                    horizontal: 5, vertical: 10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 20.0,
                                          top: 5,
                                          bottom: 20,
                                          right: 10),
                                      child: Text(
                                        "Maximum Property Price that can be Purchased",
                                        style: TextStyles
                                            .bankSubmissionTaskTextStyle1,
                                      ),
                                    ),
                                    const Divider(color: Colors.deepOrange),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 20.0, top: 5, bottom: 20),
                                      child: Text(
                                        "\$ ${widget.newPurchaseResponseModel.preferedTenureBasedAffordability == null ? 0 : widget.newPurchaseResponseModel.preferedTenureBasedAffordability!.maximumPropertyPrice.toString()}",
                                        style: TextStyles.bankSubmissionBody,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                        widget.newPurchaseResponseModel
                                    .preferedTenureBasedAffordability ==
                                null
                            ? Container()
                            :
                            // ! Condition to hide UI based on TDSR and MSR (preferred)
                            widget
                                            .newPurchaseResponseModel
                                            .preferedTenureBasedAffordability!
                                            .tdsrStatus
                                            .toString()
                                            .toUpperCase() ==
                                        "PASS" &&
                                    widget
                                            .newPurchaseResponseModel
                                            .preferedTenureBasedAffordability!
                                            .msrStatus
                                            .toString()
                                            .toUpperCase() ==
                                        "PASS"
                                ? Container()
                                : SizedBox(height: windowHeight * 0.02),
                        widget.newPurchaseResponseModel
                                    .preferedTenureBasedAffordability ==
                                null
                            ? Container()
                            :
                            // ! Condition to hide UI based on TDSR and MSR (preferred)
                            widget
                                            .newPurchaseResponseModel
                                            .preferedTenureBasedAffordability!
                                            .tdsrStatus
                                            .toString()
                                            .toUpperCase() ==
                                        "PASS" &&
                                    widget
                                            .newPurchaseResponseModel
                                            .preferedTenureBasedAffordability!
                                            .msrStatus
                                            .toString()
                                            .toUpperCase() ==
                                        "PASS"
                                ? Container()
                                : Text(
                                    "In order to pass MSR/TDSR, you may :\n\nPlace this amount as a Fixed Deposit with the bank for 4 years",
                                    style: TextStyles.leadsNormalTextStyle,
                                  ),
                        widget.newPurchaseResponseModel
                                    .preferedTenureBasedAffordability ==
                                null
                            ? Container()
                            :
                            // ! Condition to hide UI based on TDSR and MSR (preferred)
                            widget
                                            .newPurchaseResponseModel
                                            .preferedTenureBasedAffordability!
                                            .tdsrStatus
                                            .toString()
                                            .toUpperCase() ==
                                        "PASS" &&
                                    widget
                                            .newPurchaseResponseModel
                                            .preferedTenureBasedAffordability!
                                            .msrStatus
                                            .toString()
                                            .toUpperCase() ==
                                        "PASS"
                                ? Container()
                                : SizedBox(height: windowHeight * 0.01),
                        widget.newPurchaseResponseModel
                                    .preferedTenureBasedAffordability ==
                                null
                            ? Container()
                            :
                            // ! Condition to hide UI based on TDSR and MSR (preferred)
                            widget
                                            .newPurchaseResponseModel
                                            .preferedTenureBasedAffordability!
                                            .tdsrStatus
                                            .toString()
                                            .toUpperCase() ==
                                        "PASS" &&
                                    widget
                                            .newPurchaseResponseModel
                                            .preferedTenureBasedAffordability!
                                            .msrStatus
                                            .toString()
                                            .toUpperCase() ==
                                        "PASS"
                                ? Container()
                                : Card(
                                    shape: const RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5))),
                                    elevation: 5,
                                    margin: const EdgeInsets.symmetric(
                                        horizontal: 5, vertical: 10),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              left: 20.0,
                                              top: 5,
                                              bottom: 20,
                                              right: 10),
                                          child: Text(
                                            "Full Pledge Amount",
                                            style: TextStyles
                                                .bankSubmissionTaskTextStyle1,
                                          ),
                                        ),
                                        const Divider(color: Colors.deepOrange),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              left: 20.0, top: 5, bottom: 20),
                                          child: Text(
                                            "\$ ${widget.newPurchaseResponseModel.preferedTenureBasedAffordability == null ? 0 : widget.newPurchaseResponseModel.preferedTenureBasedAffordability!.fullFludgeAmount.toString()}",
                                            style:
                                                TextStyles.bankSubmissionBody,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                        widget.newPurchaseResponseModel
                                    .preferedTenureBasedAffordability ==
                                null
                            ? Container()
                            :
                            // ! Condition to hide UI based on TDSR and MSR (preferred)
                            widget
                                            .newPurchaseResponseModel
                                            .preferedTenureBasedAffordability!
                                            .tdsrStatus
                                            .toString()
                                            .toUpperCase() ==
                                        "PASS" &&
                                    widget
                                            .newPurchaseResponseModel
                                            .preferedTenureBasedAffordability!
                                            .msrStatus
                                            .toString()
                                            .toUpperCase() ==
                                        "PASS"
                                ? Container()
                                : SizedBox(height: windowHeight * 0.02),
                        widget.newPurchaseResponseModel
                                    .preferedTenureBasedAffordability ==
                                null
                            ? Container()
                            :
                            // ! Condition to hide UI based on TDSR and MSR (preferred)
                            widget
                                            .newPurchaseResponseModel
                                            .preferedTenureBasedAffordability!
                                            .tdsrStatus
                                            .toString()
                                            .toUpperCase() ==
                                        "PASS" &&
                                    widget
                                            .newPurchaseResponseModel
                                            .preferedTenureBasedAffordability!
                                            .msrStatus
                                            .toString()
                                            .toUpperCase() ==
                                        "PASS"
                                ? Container()
                                : Text(
                                    "OR \n\nShow the bank this amount in your bank account at the point of loan application and 7 days before loan disbursement.",
                                    style: TextStyles.leadsNormalTextStyle,
                                  ),
                        widget.newPurchaseResponseModel
                                    .preferedTenureBasedAffordability ==
                                null
                            ? Container()
                            :
                            // ! Condition to hide UI based on TDSR and MSR (preferred)
                            widget
                                            .newPurchaseResponseModel
                                            .preferedTenureBasedAffordability!
                                            .tdsrStatus
                                            .toString()
                                            .toUpperCase() ==
                                        "PASS" &&
                                    widget
                                            .newPurchaseResponseModel
                                            .preferedTenureBasedAffordability!
                                            .msrStatus
                                            .toString()
                                            .toUpperCase() ==
                                        "PASS"
                                ? Container()
                                : SizedBox(height: windowHeight * 0.01),
                        widget.newPurchaseResponseModel
                                    .preferedTenureBasedAffordability ==
                                null
                            ? Container()
                            :
                            // ! Condition to hide UI based on TDSR and MSR (preferred)
                            widget
                                            .newPurchaseResponseModel
                                            .preferedTenureBasedAffordability!
                                            .tdsrStatus
                                            .toString()
                                            .toUpperCase() ==
                                        "PASS" &&
                                    widget
                                            .newPurchaseResponseModel
                                            .preferedTenureBasedAffordability!
                                            .msrStatus
                                            .toString()
                                            .toUpperCase() ==
                                        "PASS"
                                ? Container()
                                : Card(
                                    shape: const RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5))),
                                    elevation: 5,
                                    margin: const EdgeInsets.symmetric(
                                        horizontal: 5, vertical: 10),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              left: 20.0,
                                              top: 5,
                                              bottom: 20,
                                              right: 10),
                                          child: Text(
                                            "Full Un-Pledged Amount",
                                            style: TextStyles
                                                .bankSubmissionTaskTextStyle1,
                                          ),
                                        ),
                                        const Divider(color: Colors.deepOrange),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              left: 20.0, top: 5, bottom: 20),
                                          child: Text(
                                            "\$ ${widget.newPurchaseResponseModel.preferedTenureBasedAffordability == null ? 0 : widget.newPurchaseResponseModel.preferedTenureBasedAffordability!.fullUnFludgeAmount.toString()}",
                                            style:
                                                TextStyles.bankSubmissionBody,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                        widget.newPurchaseResponseModel
                                    .preferedTenureBasedAffordability ==
                                null
                            ? Container()
                            :
                            // ! Condition to hide UI based on TDSR and MSR (preferred)
                            widget
                                            .newPurchaseResponseModel
                                            .preferedTenureBasedAffordability!
                                            .tdsrStatus
                                            .toString()
                                            .toUpperCase() ==
                                        "PASS" &&
                                    widget
                                            .newPurchaseResponseModel
                                            .preferedTenureBasedAffordability!
                                            .msrStatus
                                            .toString()
                                            .toUpperCase() ==
                                        "PASS"
                                ? Container()
                                : SizedBox(height: windowHeight * 0.02),
                        widget.newPurchaseResponseModel
                                    .preferedTenureBasedAffordability ==
                                null
                            ? Container()
                            :
                            // ! Condition to hide UI based on TDSR and MSR (preferred)
                            widget
                                            .newPurchaseResponseModel
                                            .preferedTenureBasedAffordability!
                                            .tdsrStatus
                                            .toString()
                                            .toUpperCase() ==
                                        "PASS" &&
                                    widget
                                            .newPurchaseResponseModel
                                            .preferedTenureBasedAffordability!
                                            .msrStatus
                                            .toString()
                                            .toUpperCase() ==
                                        "PASS"
                                ? Container()
                                : Text(
                                    "OR \n\nYou may choose your preferred Pledged and Unpledged Funds to pass TDSR/MSR.",
                                    style: TextStyles.leadsNormalTextStyle,
                                  ),
                        widget.newPurchaseResponseModel
                                    .preferedTenureBasedAffordability ==
                                null
                            ? Container()
                            :
                            // ! Condition to hide UI based on TDSR and MSR (preferred)
                            widget
                                            .newPurchaseResponseModel
                                            .preferedTenureBasedAffordability!
                                            .tdsrStatus
                                            .toString()
                                            .toUpperCase() ==
                                        "PASS" &&
                                    widget
                                            .newPurchaseResponseModel
                                            .preferedTenureBasedAffordability!
                                            .msrStatus
                                            .toString()
                                            .toUpperCase() ==
                                        "PASS"
                                ? Container()
                                : SizedBox(height: windowHeight * 0.01),
                        widget.newPurchaseResponseModel
                                    .preferedTenureBasedAffordability ==
                                null
                            ? Container()
                            :
                            // ! Condition to hide UI based on TDSR and MSR (preferred)
                            widget
                                            .newPurchaseResponseModel
                                            .preferedTenureBasedAffordability!
                                            .tdsrStatus
                                            .toString()
                                            .toUpperCase() ==
                                        "PASS" &&
                                    widget
                                            .newPurchaseResponseModel
                                            .preferedTenureBasedAffordability!
                                            .msrStatus
                                            .toString()
                                            .toUpperCase() ==
                                        "PASS"
                                ? Container()
                                : Row(
                                    children: [
                                      Expanded(
                                        flex: 3,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            const FormFieldTitle(
                                                title:
                                                    'Your Preferred \nPledged Amount'),
                                            SizedBox(
                                                height: windowHeight * 0.01),
                                            RequireTextField(
                                              type: Type.number,
                                              labelText: "\$ 300,000",
                                              controller: widget
                                                  .preferredPledgeAmountPltController,
                                              key: const Key("Your Preferred"),
                                              onChanged: (String? val) {
                                                if (val!.isNotEmpty) {
                                                  newPurchaseController
                                                      .getUnpledgeAmountPlt();
                                                }
                                              },
                                            ),
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        child: Center(
                                          child: Text(
                                            "+",
                                            style: TextStyles.docTextStyle,
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 4,
                                        child: Card(
                                          shape: const RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          elevation: 5,
                                          margin: const EdgeInsets.symmetric(
                                              horizontal: 5, vertical: 10),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 20.0,
                                                    top: 5,
                                                    bottom: 20,
                                                    right: 10),
                                                child: Text(
                                                  "Remaining UnPledged\nAmount Required",
                                                  style: TextStyles
                                                      .bankSubmissionTaskTextStyle1,
                                                ),
                                              ),
                                              const Divider(
                                                  color: Colors.deepOrange),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 20.0,
                                                    top: 5,
                                                    bottom: 20),
                                                child: Text(
                                                  "\$ ${widget.unPLedgeAmountPlt}",
                                                  style: TextStyles
                                                      .bankSubmissionBody,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                        widget.newPurchaseResponseModel
                                    .preferedTenureBasedAffordability ==
                                null
                            ? Container()
                            :
                            // ! Condition to hide UI based on TDSR and MSR (preferred)
                            widget
                                            .newPurchaseResponseModel
                                            .preferedTenureBasedAffordability!
                                            .tdsrStatus
                                            .toString()
                                            .toUpperCase() ==
                                        "PASS" &&
                                    widget
                                            .newPurchaseResponseModel
                                            .preferedTenureBasedAffordability!
                                            .msrStatus
                                            .toString()
                                            .toUpperCase() ==
                                        "PASS"
                                ? Container()
                                : SizedBox(height: windowHeight * 0.01),
                        widget.newPurchaseResponseModel
                                    .preferedTenureBasedAffordability ==
                                null
                            ? Container()
                            :
                            // ! Condition to hide UI based on TDSR and MSR (preferred)
                            widget
                                            .newPurchaseResponseModel
                                            .preferedTenureBasedAffordability!
                                            .tdsrStatus
                                            .toString()
                                            .toUpperCase() ==
                                        "PASS" &&
                                    widget
                                            .newPurchaseResponseModel
                                            .preferedTenureBasedAffordability!
                                            .msrStatus
                                            .toString()
                                            .toUpperCase() ==
                                        "PASS"
                                ? Container()
                                : Text(
                                    "OR",
                                    style: TextStyles.leadsNormalTextStyle,
                                  ),
                        widget.newPurchaseResponseModel
                                    .preferedTenureBasedAffordability ==
                                null
                            ? Container()
                            :
                            // ! Condition to hide UI based on TDSR and MSR (preferred)
                            widget
                                            .newPurchaseResponseModel
                                            .preferedTenureBasedAffordability!
                                            .tdsrStatus
                                            .toString()
                                            .toUpperCase() ==
                                        "PASS" &&
                                    widget
                                            .newPurchaseResponseModel
                                            .preferedTenureBasedAffordability!
                                            .msrStatus
                                            .toString()
                                            .toUpperCase() ==
                                        "PASS"
                                ? Container()
                                : Row(
                                    children: [
                                      Expanded(
                                        flex: 3,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            const FormFieldTitle(
                                                title:
                                                    'Your Preferred\nUnPledged Amount'),
                                            SizedBox(
                                                height: windowHeight * 0.01),
                                            RequireTextField(
                                              type: Type.number,
                                              labelText: "\$ 800,000",
                                              controller: widget
                                                  .preferredUnpledgeAmountPltController,
                                              key:
                                                  const Key("Your UnPreferred"),
                                              onChanged: (String? val) {
                                                if (val!.isNotEmpty) {
                                                  newPurchaseController
                                                      .getPledgeAmountPlt();
                                                }
                                              },
                                            ),
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        child: Center(
                                          child: Text(
                                            "+",
                                            style: TextStyles.docTextStyle,
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 4,
                                        child: Card(
                                          shape: const RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          elevation: 5,
                                          margin: const EdgeInsets.symmetric(
                                              horizontal: 5, vertical: 10),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 20.0,
                                                    top: 5,
                                                    bottom: 20,
                                                    right: 10),
                                                child: Text(
                                                  "Remaining Pledged\nAmount Required",
                                                  style: TextStyles
                                                      .bankSubmissionTaskTextStyle1,
                                                ),
                                              ),
                                              const Divider(
                                                  color: Colors.deepOrange),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 20.0,
                                                    top: 5,
                                                    bottom: 20),
                                                child: Text(
                                                  "\$ ${widget.pledgeAmountPlt}",
                                                  style: TextStyles
                                                      .bankSubmissionBody,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                        SizedBox(height: windowHeight * 0.02),
                        Text(
                          "Cost",
                          style: TextStyles.leadsColorTextStyle1,
                        ),
                        SizedBox(height: windowHeight * 0.02),
                        Card(
                          shape: const RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5))),
                          elevation: 5,
                          margin: const EdgeInsets.symmetric(
                              horizontal: 5, vertical: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0, top: 5, bottom: 20, right: 10),
                                child: GestureDetector(
                                  onTap: () {
                                    newPurchaseController.getPledgeAmountMlt();
                                  },
                                  child: Text(
                                    "Buyer's Stamp Duty",
                                    style:
                                        TextStyles.bankSubmissionTaskTextStyle1,
                                  ),
                                ),
                              ),
                              const Divider(color: Colors.deepOrange),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0, top: 5, bottom: 20),
                                child: Text(
                                  "\$ ${widget.newPurchaseResponseModel.buyerStampDuty.toString()}",
                                  style: TextStyles.bankSubmissionBody,
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: windowHeight * 0.02),
                        Card(
                          shape: const RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5))),
                          elevation: 5,
                          margin: const EdgeInsets.symmetric(
                              horizontal: 5, vertical: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0, top: 5, bottom: 20, right: 10),
                                child: Text(
                                  "Additional Buyer's Stamp Duty (ABSD)",
                                  style:
                                      TextStyles.bankSubmissionTaskTextStyle1,
                                ),
                              ),
                              const Divider(color: Colors.deepOrange),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0, top: 5, bottom: 20),
                                child: Text(
                                  "\$ ${widget.newPurchaseResponseModel.additionalBuyerStampDuty.toString()}",
                                  style: TextStyles.bankSubmissionBody,
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: windowHeight * 0.02),
                        Card(
                          shape: const RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5))),
                          elevation: 5,
                          margin: const EdgeInsets.symmetric(
                              horizontal: 5, vertical: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0, top: 5, bottom: 20, right: 10),
                                child: Text(
                                  "Legal Fees (To be confimed with your selected law firm)",
                                  style:
                                      TextStyles.bankSubmissionTaskTextStyle1,
                                ),
                              ),
                              const Divider(color: Colors.deepOrange),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0, top: 5, bottom: 20),
                                child: Text(
                                  "\$ ${widget.newPurchaseResponseModel.legalFees == null ? 0 : widget.newPurchaseResponseModel.legalFees!.toString()}",
                                  style: TextStyles.bankSubmissionBody,
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: windowHeight * 0.02),
                        Card(
                          shape: const RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5))),
                          elevation: 5,
                          margin: const EdgeInsets.symmetric(
                              horizontal: 5, vertical: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0, top: 5, bottom: 20, right: 10),
                                child: Text(
                                  "Valuation Fees (This is an approximate, this may vary from bank to bank)",
                                  style:
                                      TextStyles.bankSubmissionTaskTextStyle1,
                                ),
                              ),
                              const Divider(color: Colors.deepOrange),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0, top: 5, bottom: 20),
                                child: Text(
                                  "\$ ${widget.newPurchaseResponseModel.valuationFees == null ? 0 : widget.newPurchaseResponseModel.valuationFees!.toString()}",
                                  style: TextStyles.bankSubmissionBody,
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: windowHeight * 0.01),
                        const FormFieldTitle(title: 'Other Fees (If any)'),
                        SizedBox(height: windowHeight * 0.01),
                        CurrencyAmountTextField(
                          textEditingController: widget.otherFeesController,
                          labelText: "\$",
                          key: const Key("Card Repayment"),
                        ),
                      ],
                    )
                  : const SizedBox()),
              SizedBox(height: windowHeight * 0.04),
            ],
          ),
        ]);
  }
}
