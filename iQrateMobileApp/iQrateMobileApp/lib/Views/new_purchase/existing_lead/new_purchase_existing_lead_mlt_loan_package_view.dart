// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/leads_view_controller.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/screen_constants.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Model/common_key_value_model.dart';
import 'package:iqrate/Service/GetControllers.dart';
import 'package:iqrate/Utils/config.dart';

import '../../../Controller/new_purchase_existing_lead_controller.dart';
import '../../../Model/response_model.dart/new_purchase_report_response_model.dart';
import '../../../Widgets/default_appbar.dart';

class NewPurchaseExistingLeadMltLoanPackage extends StatelessWidget {
  NewPurchaseExistingLeadMltLoanPackage({Key? key}) : super(key: key);
  final NewPurchaseExistingLeadController newPurchaseExistingLeadController =
      Get.find();

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: DefaultAppBar2(
        title: "New Purchase",
        windowHeight: windowHeight * 0.09,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: windowHeight * 0.02,
              ),
              Text(
                'Loan Packages',
                style: TextStyle(
                  color: AppColors.kPrimaryColor,
                  fontSize: FontSize.s22,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: windowHeight * 0.003,
              ),
              Text(
                'Based On Maximum Loan Tenure',
                style: TextStyle(
                  color: const Color(0XFF9A9696),
                  fontSize: FontSize.s20,
                ),
              ),
              SizedBox(
                height: windowHeight * 0.03,
              ),
              Obx(
                () => Table(
                  defaultColumnWidth: FixedColumnWidth(windowWidth * 0.3),
                  border: TableBorder.all(
                      color: const Color(0XFFE2E2E2),
                      style: BorderStyle.solid,
                      width: 2),
                  children: [
                    TableRow(
                      children: [
                        TableTitleRowData(
                          windowHeight: windowHeight,
                          title: '',
                        ),
                        TableTitleRowData(
                          windowHeight: windowHeight,
                          title: 'Lowest Floating',
                        ),
                        TableTitleRowData(
                          windowHeight: windowHeight,
                          title: 'Lowest Fixed',
                        ),
                      ],
                    ),
                    TableRow(
                      children: [
                        DataRow(
                          windowHeight: windowHeight,
                          stringData: 'Monthly Installment',
                        ),
                        DataRow(
                          windowHeight: windowHeight,
                          stringData: newPurchaseExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .loanPackageMaximumLoanTenureBased!
                                      .lowestFloating !=
                                  null
                              ? newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .loanPackageMaximumLoanTenureBased!
                                  .lowestFloating!
                                  .monthlyInstallment
                                  .toString()
                              : '-',
                        ),
                        DataRow(
                          windowHeight: windowHeight,
                          stringData: newPurchaseExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .loanPackageMaximumLoanTenureBased!
                                      .lowestFixed !=
                                  null
                              ? newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .loanPackageMaximumLoanTenureBased!
                                  .lowestFixed!
                                  .monthlyInstallment
                                  .toString()
                              : '-',
                        ),
                      ],
                    ),
                    TableRow(
                      children: [
                        DataRow(
                          windowHeight: windowHeight,
                          stringData: 'Total Payment',
                        ),
                        DataRow(
                          windowHeight: windowHeight,
                          stringData: newPurchaseExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .loanPackageMaximumLoanTenureBased!
                                      .lowestFloating !=
                                  null
                              ? newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .loanPackageMaximumLoanTenureBased!
                                  .lowestFloating!
                                  .totalPayment
                                  .toString()
                              : '-',
                        ),
                        DataRow(
                          windowHeight: windowHeight,
                          stringData: newPurchaseExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .loanPackageMaximumLoanTenureBased!
                                      .lowestFixed !=
                                  null
                              ? newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .loanPackageMaximumLoanTenureBased!
                                  .lowestFixed!
                                  .totalPayment
                                  .toString()
                              : '-',
                        ),
                      ],
                    ),
                    TableRow(
                      children: [
                        DataRow(
                          windowHeight: windowHeight,
                          stringData: 'Total Principal',
                        ),
                        DataRow(
                          windowHeight: windowHeight,
                          stringData: newPurchaseExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .loanPackageMaximumLoanTenureBased!
                                      .lowestFloating !=
                                  null
                              ? newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .loanPackageMaximumLoanTenureBased!
                                  .lowestFloating!
                                  .totalPrincipal
                                  .toString()
                              : '-',
                        ),
                        DataRow(
                          windowHeight: windowHeight,
                          stringData: newPurchaseExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .loanPackageMaximumLoanTenureBased!
                                      .lowestFixed !=
                                  null
                              ? newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .loanPackageMaximumLoanTenureBased!
                                  .lowestFixed!
                                  .totalPrincipal
                                  .toString()
                              : '-',
                        ),
                      ],
                    ),
                    TableRow(
                      children: [
                        DataRow(
                          windowHeight: windowHeight,
                          stringData: 'Total Interest',
                        ),
                        DataRow(
                          windowHeight: windowHeight,
                          stringData: newPurchaseExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .loanPackageMaximumLoanTenureBased!
                                      .lowestFloating !=
                                  null
                              ? newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .loanPackageMaximumLoanTenureBased!
                                  .lowestFloating!
                                  .totalInterest
                                  .toString()
                              : '-',
                        ),
                        DataRow(
                          windowHeight: windowHeight,
                          stringData: newPurchaseExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .loanPackageMaximumLoanTenureBased!
                                      .lowestFixed !=
                                  null
                              ? newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .loanPackageMaximumLoanTenureBased!
                                  .lowestFixed!
                                  .totalInterest
                                  .toString()
                              : '-',
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: windowHeight * 0.03,
                  ),
                  Text(
                    'Lowest Floating',
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: FontSize.s16,
                    ),
                  ),
                  Divider(
                    color: const Color(0X9A969666).withOpacity(0.4),
                    thickness: 2,
                  ),
                  SizedBox(
                    height: windowHeight * 0.02,
                  ),
                  newPurchaseExistingLeadController
                              .resultMainApplicantStep3.value.lowestFloating !=
                          null
                      ? DetailsCardForPackages(
                          windowHeight: windowHeight,
                          windowWidth: windowWidth,
                          image: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFloating!
                              .bankLogo
                              .toString(),
                          bankName: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFloating!
                              .bankName
                              .toString(),
                          rateType:
                              "${newPurchaseExistingLeadController.resultMainApplicantStep3.value.lowestFloating!.rateType.toString().replaceAll('_', ' ').capitalize!} (${newPurchaseExistingLeadController.resultMainApplicantStep3.value.lowestFloating!.details!.rateCategory.toString().capitalizeFirst})",
                          yearOneValue: newPurchaseExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .lowestFloating!
                                      .details!
                                      .rates!
                                      .elementAt(1)
                                      .year ==
                                  'year_5'
                              ? newPurchaseExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .lowestFloating!
                                      .details!
                                      .rates![5]
                                      .totalInterestRate
                                      .toString() +
                                  "%"
                              : newPurchaseExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .lowestFloating!
                                      .details!
                                      .rates![0]
                                      .totalInterestRate
                                      .toString() +
                                  "%",
                          itemCount: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFloating!
                              .rateList!
                              .length,
                          rateValue: newPurchaseExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .lowestFloating!
                                      .details!
                                      .rates!
                                      .elementAt(1)
                                      .year ==
                                  'year_5'
                              ? newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .lowestFloating!
                                  .details!
                                  .rates!
                                  .reversed
                              : newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .lowestFloating!
                                  .details!
                                  .rates!,
                          propertyType: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFloating!
                              .details!
                              .propertyTypes,
                          typeOfloan: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFloating!
                              .details!
                              .loanCategory!,
                          lockInPeriod: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFloating!
                              .details!
                              .lockInPeriod
                              .toString(),
                          minLoanAmount: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFloating!
                              .details!
                              .minLoanAmount
                              .toString(),
                          depositToPlace: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFloating!
                              .details!
                              .depositToPlace
                              .toString(),
                          remarksForApplicant: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFloating!
                              .details!
                              .remarksForClient
                              .toString(),
                          remarksForBroker: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFloating!
                              .details!
                              .remarksForBroker
                              .toString(),
                          cashRebateLegalSubsidy:
                              newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .lowestFloating!
                                  .details!
                                  .cashRebateLegalSubsidy
                                  .toString(),
                          valuationSubsidy: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFloating!
                              .details!
                              .valuationSubsidy
                              .toString(),
                          cashRebateSubsidyClawback:
                              newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .lowestFloating!
                                  .details!
                                  .cashRebateSubsidyClawbackPeriodYears
                                  .toString(),
                          fireInsuranceSubsidy:
                              newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .lowestFloating!
                                  .details!
                                  .fireInsuranceSubsidy
                                  .toString(),
                          partialRepaymentPenalty:
                              newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .lowestFloating!
                                  .details!
                                  .partialRepaymentPenalty
                                  .toString(),
                          partialRepaymentPenaltyRemark:
                              newPurchaseExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .lowestFloating!
                                      .details!
                                      .partialRepaymentPenaltyRemarks ??
                                  'NA',
                          fullRepaymentPenalty:
                              newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .lowestFloating!
                                  .details!
                                  .fullRepaymentPenalty
                                  .toString(),
                          fullRepaymentPenaltyRemarks:
                              newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .lowestFloating!
                                  .details!
                                  .fullRepaymentPenaltyRemarks
                                  .toString(),
                          cancellationFee: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFloating!
                              .details!
                              .cancellationFee
                              .toString(),
                          cancellationFeeRemarks:
                              newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .lowestFloating!
                                  .details!
                                  .cancellationFeeRemarks
                                  .toString(),
                          propertyStatus: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFloating!
                              .details!
                              .propertyStatus,
                        )
                      : Center(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SizedBox(
                                  height: windowHeight * 0.15,
                                  child: SvgPicture.asset(
                                      'assets/images/nodata.svg')),
                              Text(
                                'No data to show',
                                style:
                                    TextStyles.bankSubmissionCardTitleDisabled,
                              )
                            ],
                          ),
                        ),
                  SizedBox(
                    height: windowHeight * 0.04,
                  ),
                  Text(
                    'Lowest Fixed',
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: FontSize.s16,
                    ),
                  ),
                  Divider(
                    color: const Color(0X9A969666).withOpacity(0.4),
                    thickness: 2,
                  ),
                  SizedBox(
                    height: windowHeight * 0.02,
                  ),
                  newPurchaseExistingLeadController
                              .resultMainApplicantStep3.value.lowestFixed !=
                          null
                      ? DetailsCardForPackages(
                          windowHeight: windowHeight,
                          windowWidth: windowWidth,
                          image: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFixed!
                              .bankLogo
                              .toString(),
                          bankName: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFixed!
                              .bankName
                              .toString(),
                          rateType:
                              "${newPurchaseExistingLeadController.resultMainApplicantStep3.value.lowestFixed!.rateType.toString().capitalizeFirst} (${newPurchaseExistingLeadController.resultMainApplicantStep3.value.lowestFixed!.details!.rateCategory.toString().capitalizeFirst})",
                          yearOneValue: newPurchaseExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .lowestFixed!
                                      .details!
                                      .rates!
                                      .elementAt(1)
                                      .year ==
                                  'year_5'
                              ? newPurchaseExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .lowestFixed!
                                      .details!
                                      .rates![5]
                                      .totalInterestRate
                                      .toString() +
                                  "%"
                              : newPurchaseExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .lowestFixed!
                                      .details!
                                      .rates![0]
                                      .totalInterestRate
                                      .toString() +
                                  "%",
                          itemCount: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFixed!
                              .rateList!
                              .length,
                          rateValue: newPurchaseExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .lowestFixed!
                                      .details!
                                      .rates!
                                      .elementAt(1)
                                      .year ==
                                  'year_5'
                              ? newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .lowestFixed!
                                  .details!
                                  .rates!
                                  .reversed
                              : newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .lowestFixed!
                                  .details!
                                  .rates!,
                          propertyType: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFixed!
                              .details!
                              .propertyTypes,
                          typeOfloan: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFixed!
                              .details!
                              .loanCategory!,
                          lockInPeriod: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFixed!
                              .details!
                              .lockInPeriod
                              .toString(),
                          minLoanAmount: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFixed!
                              .details!
                              .minLoanAmount
                              .toString(),
                          depositToPlace: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFixed!
                              .details!
                              .depositToPlace
                              .toString(),
                          remarksForApplicant: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFixed!
                              .details!
                              .remarksForClient
                              .toString(),
                          remarksForBroker: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFixed!
                              .details!
                              .remarksForBroker
                              .toString(),
                          cashRebateLegalSubsidy:
                              newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .lowestFixed!
                                  .details!
                                  .cashRebateLegalSubsidy
                                  .toString(),
                          valuationSubsidy: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFixed!
                              .details!
                              .valuationSubsidy
                              .toString(),
                          cashRebateSubsidyClawback:
                              newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .lowestFixed!
                                  .details!
                                  .cashRebateSubsidyClawbackPeriodYears
                                  .toString(),
                          fireInsuranceSubsidy:
                              newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .lowestFixed!
                                  .details!
                                  .fireInsuranceSubsidy
                                  .toString(),
                          partialRepaymentPenalty:
                              newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .lowestFixed!
                                  .details!
                                  .partialRepaymentPenalty
                                  .toString(),
                          partialRepaymentPenaltyRemark:
                              newPurchaseExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .lowestFixed!
                                      .details!
                                      .partialRepaymentPenaltyRemarks ??
                                  'NA',
                          fullRepaymentPenalty:
                              newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .lowestFixed!
                                  .details!
                                  .fullRepaymentPenalty
                                  .toString(),
                          fullRepaymentPenaltyRemarks:
                              newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .lowestFixed!
                                  .details!
                                  .fullRepaymentPenaltyRemarks
                                  .toString(),
                          cancellationFee: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFixed!
                              .details!
                              .cancellationFee
                              .toString(),
                          cancellationFeeRemarks:
                              newPurchaseExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .lowestFixed!
                                  .details!
                                  .cancellationFeeRemarks
                                  .toString(),
                          propertyStatus: newPurchaseExistingLeadController
                              .resultMainApplicantStep3
                              .value
                              .lowestFixed!
                              .details!
                              .propertyStatus,
                        )
                      : Center(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SizedBox(
                                  height: windowHeight * 0.15,
                                  child: SvgPicture.asset(
                                      'assets/images/nodata.svg')),
                              Text(
                                'No data to show',
                                style:
                                    TextStyles.bankSubmissionCardTitleDisabled,
                              )
                            ],
                          ),
                        ),
                ],
              ),
              SizedBox(
                height: windowHeight * 0.1,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class DetailsCardForPackages extends StatefulWidget {
  final double windowHeight;
  final double windowWidth;
  final String image;
  final String bankName;
  final String rateType;
  final Iterable<Rate> rateValue;
  final String yearOneValue;
  final int itemCount;
  List<String>? propertyType;
  List<String>? propertyStatus;
  List<String>? typeOfloan;
  final String lockInPeriod;
  final String minLoanAmount;
  final String depositToPlace;
  final String remarksForApplicant;
  final String remarksForBroker;
  final String cashRebateLegalSubsidy;
  final String valuationSubsidy;
  final String cashRebateSubsidyClawback;
  final String fireInsuranceSubsidy;
  final String partialRepaymentPenalty;
  final String partialRepaymentPenaltyRemark;
  final String fullRepaymentPenalty;
  final String fullRepaymentPenaltyRemarks;
  final String cancellationFee;
  final String cancellationFeeRemarks;

  DetailsCardForPackages({
    Key? key,
    required this.windowHeight,
    required this.windowWidth,
    required this.image,
    required this.bankName,
    required this.rateType,
    required this.rateValue,
    required this.yearOneValue,
    required this.itemCount,
    required this.propertyType,
    required this.typeOfloan,
    required this.lockInPeriod,
    required this.minLoanAmount,
    required this.depositToPlace,
    required this.remarksForApplicant,
    required this.remarksForBroker,
    required this.cashRebateLegalSubsidy,
    required this.valuationSubsidy,
    required this.cashRebateSubsidyClawback,
    required this.fireInsuranceSubsidy,
    required this.partialRepaymentPenalty,
    required this.partialRepaymentPenaltyRemark,
    required this.fullRepaymentPenalty,
    required this.fullRepaymentPenaltyRemarks,
    required this.cancellationFee,
    required this.cancellationFeeRemarks,
    required this.propertyStatus,
  }) : super(key: key);

  @override
  State<DetailsCardForPackages> createState() => _DetailsCardForPackagesState();
}

class _DetailsCardForPackagesState extends State<DetailsCardForPackages> {
  bool expandTheDetailsSection = false;
  final LeadsViewController leadsViewController =
      GetControllers.shared.getLeadsViewController();

  @override
  Widget build(BuildContext context) {
    List<CommonKeyValueModel> propertyTypeListFromKVStore = [];
    List<String> propertyTypeValues = [];

    for (var element in leadsViewController.kvStoreValues) {
      if (element.code == "property_type") {
        element.value.forEach((key, value) {
          propertyTypeListFromKVStore.add(CommonKeyValueModel(key, value));
        });
      }
    }

    for (var key in widget.propertyType!) {
      for (var element in propertyTypeListFromKVStore) {
        if (key == element.key) {
          propertyTypeValues.add(element.value);
        }
      }
    }

    List<CommonKeyValueModel> propertyStatuses = [];
    List<String> propertyStatusValues = [];

    for (var element in leadsViewController.kvStoreValues) {
      if (element.code == "property_status") {
        //debugPrint("=====kvstore====="+element.value.toString());
        element.value.forEach((key, value) {
          propertyStatuses.add(CommonKeyValueModel(key, value));
          debugPrint("array_key:: " + value);
        });
      }
    }

    for (var key in widget.propertyStatus!) {
      for (var element in propertyStatuses) {
        if (key == element.key) {
          propertyStatusValues.add(element.value);
        }
      }
    }

    List<CommonKeyValueModel> loanTypeListFromKVStore = [];
    List<String> loanTypeValues = [];

    for (var element in leadsViewController.kvStoreValues) {
      if (element.code == "loan_category") {
        element.value.forEach((key, value) {
          loanTypeListFromKVStore.add(CommonKeyValueModel(key, value));
        });
      }
    }

    for (var key in widget.typeOfloan!) {
      for (var element in loanTypeListFromKVStore) {
        if (key == element.key) {
          loanTypeValues.add(element.value);
        }
      }
    }
    debugPrint(widget.image);
    debugPrint('************');
    return Card(
      elevation: 10,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(25.0),
      ),
      child: SizedBox(
        width: widget.windowWidth,
        child: Padding(
          padding: const EdgeInsets.only(top: 10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Image.network(
                        widget.image.toString().toString().contains('http')
                            ? widget.image
                            : AppConfig.blankImage,
                        height: widget.windowHeight * 0.08,
                        width: widget.windowWidth * 0.3,
                        // fit: BoxFit.cover,
                      ),
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            widget.bankName,
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: FontSize.s16,
                            ),
                          ),
                          Text(
                            widget.rateType,
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: FontSize.s16,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Container(
                        width: widget.windowWidth * 0.2,
                        height: widget.windowHeight * 0.03,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: const Color(0XFFF3F3F3),
                            width: 1,
                          ),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text(
                              'Year 1:',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            Text(
                              widget.yearOneValue,
                              style: const TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              // Padding(
              //   padding: const EdgeInsets.only(top: 8.0),
              //   child: Row(
              //     mainAxisAlignment: MainAxisAlignment.center,
              //     children: [
              //       Container(
              //         padding: const EdgeInsets.symmetric(horizontal: 5),
              //         height: widget.windowHeight * 0.03,
              //         decoration: BoxDecoration(
              //           border: Border.all(
              //             color: const Color(0XFFF3F3F3),
              //             width: 1,
              //           ),
              //         ),
              //         child: Row(
              //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //           children: [
              //             const Text(
              //               'Year 1: ',
              //               style: TextStyle(
              //                 color: Colors.black,
              //                 fontWeight: FontWeight.w500,
              //               ),
              //             ),
              //             Text(
              //               widget.yearOneValue,
              //               style: const TextStyle(
              //                 color: Colors.black,
              //                 fontWeight: FontWeight.w500,
              //               ),
              //             ),
              //           ],
              //         ),
              //       ),
              //       Container(
              //         padding: const EdgeInsets.symmetric(horizontal: 5),
              //         // width: widget.windowWidth * 0.2,
              //         height: widget.windowHeight * 0.03,
              //         decoration: BoxDecoration(
              //           border: Border.all(
              //             color: const Color(0XFFF3F3F3),
              //             width: 1,
              //           ),
              //         ),
              //         child: Row(
              //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //           children: [
              //             const Text(
              //               'Year 2: ',
              //               style: TextStyle(
              //                 color: Colors.black,
              //                 fontWeight: FontWeight.w500,
              //               ),
              //             ),
              //             Text(
              //               widget.rateValue[1].toString(),
              //               style: const TextStyle(
              //                 color: Colors.black,
              //                 fontWeight: FontWeight.w500,
              //               ),
              //             ),
              //           ],
              //         ),
              //       ),
              //       Container(
              //         padding: const EdgeInsets.symmetric(horizontal: 5),
              //         // width: widget.windowWidth * 0.2,
              //         height: widget.windowHeight * 0.03,
              //         decoration: BoxDecoration(
              //           border: Border.all(
              //             color: const Color(0XFFF3F3F3),
              //             width: 1,
              //           ),
              //         ),
              //         child: Row(
              //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //           children: [
              //             const Text(
              //               'Year 3: ',
              //               style: TextStyle(
              //                 color: Colors.black,
              //                 fontWeight: FontWeight.w500,
              //               ),
              //             ),
              //             Text(
              //               widget.rateValue[2].toString(),
              //               style: const TextStyle(
              //                 color: Colors.black,
              //                 fontWeight: FontWeight.w500,
              //               ),
              //             ),
              //           ],
              //         ),
              //       ),
              //     ],
              //   ),
              // ),
              Container(
                margin: const EdgeInsets.only(top: 20),
                decoration: BoxDecoration(
                  color: expandTheDetailsSection == true
                      ? Colors.white
                      : Colors.red,
                  borderRadius: expandTheDetailsSection == true
                      ? null
                      : const BorderRadius.only(
                          bottomLeft: Radius.circular(25),
                          bottomRight: Radius.circular(25),
                        ),
                ),
                child: expandTheDetailsSection == true
                    ? Column(
                        children: [
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 10.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  'Details',
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: FontSize.s17,
                                  ),
                                ),
                                IconButton(
                                    onPressed: () {
                                      debugPrint("Colapse the container");
                                      setState(() {
                                        expandTheDetailsSection = false;
                                      });
                                    },
                                    icon: const Icon(
                                      Icons.minimize_outlined,
                                    ))
                              ],
                            ),
                          ),
                          SizedBox(
                            height: widget.windowHeight * 0.02,
                          ),
                          Container(
                            decoration:
                                BoxDecoration(color: Colors.white, boxShadow: [
                              BoxShadow(
                                  blurRadius: 5,
                                  spreadRadius: 3,
                                  offset: const Offset(-1, -1),
                                  color: Colors.grey.withOpacity(0.2)),
                              BoxShadow(
                                  blurRadius: 5,
                                  spreadRadius: 3,
                                  offset: const Offset(1, 1),
                                  color: Colors.black.withOpacity(0.2))
                            ]),
                            child: Column(
                              children: [
                                Container(
                                  width: widget.windowWidth,
                                  color: Colors.red,
                                  child: const Padding(
                                    padding: EdgeInsets.all(8.0),
                                    child: Text(
                                      'Rates',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ),
                                ListView.builder(
                                  shrinkWrap: true,
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemCount: widget.itemCount,
                                  itemBuilder: (context, index) {
                                    debugPrint(widget.rateType.toString());
                                    return TitleValueRow(
                                        title: widget.rateValue
                                            .elementAt(index)
                                            .year
                                            .toString()
                                            .replaceAll('_', ' ')
                                            .capitalize!,
                                        value: widget.rateValue
                                                    .elementAt(index)
                                                    .referenceRate!
                                                    .rateType
                                                    .toString()
                                                    .toLowerCase() !=
                                                'fixed'
                                            ? "${widget.rateValue.elementAt(index).referenceRate!.reference.toString().toUpperCase()} (${widget.rateValue.elementAt(index).referenceRate!.interestRate})% ${widget.rateValue.elementAt(index).referenceRate!.equation} ${widget.rateValue.elementAt(index).bankSpread}% = " +
                                                widget.rateValue
                                                    .elementAt(index)
                                                    .totalInterestRate
                                                    .toString() +
                                                "%"
                                            : widget.rateValue
                                                    .elementAt(index)
                                                    .referenceRate!
                                                    .reference
                                                    .toString()
                                                    .capitalizeFirst! +
                                                ' ' +
                                                widget.rateValue
                                                    .elementAt(index)
                                                    .totalInterestRate
                                                    .toString() +
                                                "%");
                                  },
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: widget.windowHeight * 0.02,
                          ),
                          Container(
                            decoration:
                                BoxDecoration(color: Colors.white, boxShadow: [
                              BoxShadow(
                                  blurRadius: 5,
                                  spreadRadius: 3,
                                  offset: const Offset(-1, -1),
                                  color: Colors.grey.withOpacity(0.2)),
                              BoxShadow(
                                  blurRadius: 5,
                                  spreadRadius: 3,
                                  offset: const Offset(1, 1),
                                  color: Colors.black.withOpacity(0.2))
                            ]),
                            child: Column(
                              children: [
                                Container(
                                  width: widget.windowWidth,
                                  color: Colors.red,
                                  child: const Padding(
                                    padding: EdgeInsets.all(8.0),
                                    child: Text(
                                      'Key Features',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ),
                                Column(
                                  children: [
                                    TitleValueRowWithListForType(
                                      title: "Property Type",
                                      values: propertyTypeValues,
                                    ),
                                    TitleValueRowWithListForType(
                                      title: "Property Status",
                                      values: propertyTypeValues,
                                    ),
                                    const Divider(
                                      thickness: 1,
                                      color: Colors.grey,
                                    ),
                                    TitleValueRowWithListForType(
                                      title: "Type of Loan",
                                      values: loanTypeValues,
                                    ),
                                    const Divider(
                                      thickness: 1,
                                      color: Colors.grey,
                                    ),
                                    TitleValueRow(
                                      title: "Lock In Period",
                                      value: widget.lockInPeriod + " Years",
                                    ),
                                    const Divider(
                                      thickness: 1,
                                      color: Colors.grey,
                                    ),
                                    TitleValueRow(
                                      title: "Min Loan Amount",
                                      value: "\$" +
                                          AppConfig.getFormattedAmount(
                                              widget.minLoanAmount),
                                    ),
                                    const Divider(
                                      thickness: 1,
                                      color: Colors.grey,
                                    ),
                                    TitleValueRow(
                                      title: "Deposit To Place",
                                      value: widget.depositToPlace,
                                    ),
                                    const Divider(
                                      thickness: 1,
                                      color: Colors.grey,
                                    ),
                                    TitleValueRow(
                                      title: "Remarks for Applicant",
                                      value: widget.remarksForApplicant,
                                    ),
                                    const Divider(
                                      thickness: 1,
                                      color: Colors.grey,
                                    ),
                                    TitleValueRow(
                                      title: "Remarks for Broker",
                                      value: widget.remarksForBroker,
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: widget.windowHeight * 0.02,
                          ),
                          Container(
                            decoration:
                                BoxDecoration(color: Colors.white, boxShadow: [
                              BoxShadow(
                                  blurRadius: 5,
                                  spreadRadius: 3,
                                  offset: const Offset(-1, -1),
                                  color: Colors.grey.withOpacity(0.2)),
                              BoxShadow(
                                  blurRadius: 5,
                                  spreadRadius: 3,
                                  offset: const Offset(1, 1),
                                  color: Colors.black.withOpacity(0.2))
                            ]),
                            child: Column(
                              children: [
                                Container(
                                  width: widget.windowWidth,
                                  color: Colors.red,
                                  child: const Padding(
                                    padding: EdgeInsets.all(8.0),
                                    child: Text(
                                      "Bank's Subsidy",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ),
                                Column(
                                  children: [
                                    TitleValueRow(
                                      title: "Cash Rebate/ Legal Subsidy",
                                      value: widget.cashRebateLegalSubsidy,
                                    ),
                                    TitleValueRow(
                                      title: "Valuation Subsidy",
                                      value: widget.valuationSubsidy,
                                    ),
                                    TitleValueRow(
                                      title: "Cash Rebate/ Subsidy Clawback",
                                      value: widget.cashRebateSubsidyClawback +
                                          " Years",
                                    ),
                                    TitleValueRow(
                                      title: "Fire Insurance Subsidy",
                                      value: widget.fireInsuranceSubsidy,
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: widget.windowHeight * 0.02,
                          ),
                          Container(
                            decoration:
                                BoxDecoration(color: Colors.white, boxShadow: [
                              BoxShadow(
                                  blurRadius: 5,
                                  spreadRadius: 3,
                                  offset: const Offset(-1, -1),
                                  color: Colors.grey.withOpacity(0.2)),
                              BoxShadow(
                                  blurRadius: 5,
                                  spreadRadius: 3,
                                  offset: const Offset(1, 1),
                                  color: Colors.black.withOpacity(0.2))
                            ]),
                            child: Column(
                              children: [
                                Container(
                                  width: widget.windowWidth,
                                  color: Colors.red,
                                  child: const Padding(
                                    padding: EdgeInsets.all(8.0),
                                    child: Text(
                                      "Early Repayment Penalty",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ),
                                Column(
                                  children: [
                                    TitleValueRow(
                                      title: "Partial Repayment Penalty",
                                      value:
                                          widget.partialRepaymentPenalty + '%',
                                    ),
                                    TitleValueRow(
                                      title:
                                          "Partial Repayment Penalty Remarks",
                                      value:
                                          widget.partialRepaymentPenaltyRemark,
                                    ),
                                    TitleValueRow(
                                      title: "Full Repayment Penalty",
                                      value: widget.fullRepaymentPenalty + '%',
                                    ),
                                    TitleValueRow(
                                        title: "Full Repayment Penalty Remarks",
                                        value: widget
                                                .fullRepaymentPenaltyRemarks
                                                .toString()
                                                .isEmpty
                                            ? "NA"
                                            : widget.fullRepaymentPenaltyRemarks
                                                .toString()),
                                    TitleValueRow(
                                      title: "Cancellation Fee",
                                      value: widget.cancellationFee,
                                    ),
                                    TitleValueRow(
                                        title: "Cancellation Fee Remarks",
                                        value: widget.cancellationFeeRemarks
                                                .toString()
                                                .isEmpty
                                            ? "NA"
                                            : widget.cancellationFeeRemarks
                                                .toString())
                                  ],
                                )
                              ],
                            ),
                          )
                        ],
                      )
                    : InkWell(
                        onTap: () {
                          debugPrint("Expand the container");
                          setState(() {
                            expandTheDetailsSection = true;
                          });
                        },
                        child: Center(
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text(
                              'Details',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: FontSize.s17,
                              ),
                            ),
                          ),
                        ),
                      ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class TitleValueRow extends StatelessWidget {
  TitleValueRow({Key? key, required this.title, required this.value})
      : super(key: key);
  String title;
  String value;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 1,
            child: Text(
              title,
              style: const TextStyle(
                fontWeight: FontWeight.bold,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Expanded(
            flex: 1,
            child: Text(
              value.toString().trim() == "NA"
                  ? "NA"
                  : value.toString().trim() == "[NA]"
                      ? "NA"
                      : value
                          .toString()
                          .replaceAll('[', '')
                          .replaceAll(']', '')
                          .replaceAll('_', ' ')
                          .capitalize!,
              textAlign: TextAlign.left,
            ),
          ),
        ],
      ),
    );
  }
}

class TitleValueRowWithListForType extends StatelessWidget {
  TitleValueRowWithListForType(
      {Key? key, required this.title, required this.values})
      : super(key: key);
  String title;
  List<String> values;

  @override
  Widget build(BuildContext context) {
    double windowWidth = MediaQuery.of(context).size.width;
    return Padding(
      padding:
          const EdgeInsets.only(right: 10.0, left: 10.0, top: 8, bottom: 0),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 8,
                child: Text(
                  title,
                  style: TextStyles.headingSubtitleStyle4,
                ),
              ),
              SizedBox(
                width: windowWidth * 0.05,
              ),
              Flexible(
                flex: 9,
                child: ListView.builder(
                    physics: const ClampingScrollPhysics(),
                    shrinkWrap: true,
                    primary: false,
                    scrollDirection: Axis.vertical,
                    itemCount: values.length,
                    itemBuilder: (BuildContext context, int index) => Align(
                          alignment: Alignment.topRight,
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 0.0),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                values.elementAt(index).trim(),

                                /*maxLines: 1,
                                overflow: TextOverflow.ellipsis,*/
                                textAlign: TextAlign.start,
                              ),
                            ),
                          ),
                        )),
              )
            ],
          ),
        ],
      ),
    );
  }
}

class DataRow extends StatelessWidget {
  final String stringData;

  const DataRow({
    Key? key,
    required this.windowHeight,
    required this.stringData,
  }) : super(key: key);

  final double windowHeight;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            height: windowHeight * 0.02,
          ),
          Text(
            double.tryParse(stringData) != null
                ? "\$" +
                    double.tryParse(stringData)!
                        .toStringAsFixed(2)
                        .replaceAllMapped(
                            RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))'),
                            (Match m) => '${m[1]},')
                : stringData.replaceAllMapped(
                    RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))'),
                    (Match m) => '${m[1]},'),
            style: TextStyle(fontSize: FontSize.s14),
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: windowHeight * 0.02,
          ),
        ],
      ),
    );
  }
}

class TableTitleRowData extends StatelessWidget {
  const TableTitleRowData({
    Key? key,
    required this.windowHeight,
    required this.title,
  }) : super(key: key);

  final double windowHeight;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Column(
      // mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          height: windowHeight * 0.02,
        ),
        Text(
          title,
          style:
              TextStyle(fontSize: FontSize.s17, color: AppColors.kPrimaryColor),
          textAlign: TextAlign.center,
        ),
        SizedBox(
          height: windowHeight * 0.02,
        ),
      ],
    );
  }
}
