import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';

import '../Controller/contact_us_controller.dart';
import '../Widgets/default_appbar.dart';
import '../Widgets/form_field_title.dart';

class ContactusView extends StatelessWidget {
  ContactusView({Key? key}) : super(key: key);
  final ContactUsContoller contactUsContoller = Get.put(ContactUsContoller());

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: const Color(0XFFFFD8B9).withOpacity(0.2),
      appBar: DefaultAppBar(
        title: 'Contact Us',
        windowHeight: windowHeight * 0.09,
      ),
      body: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: SingleChildScrollView(
          child: ListView(
            shrinkWrap: true,
            children: [
              Container(
                color: Colors.transparent,
                child: Center(
                  child: SvgPicture.asset("assets/images/contact.svg"),
                ),
              ),
              Stack(
                children: [
                  Container(
                    color: Colors.white,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: Card(
                      elevation: 8,
                      child: SizedBox(
                        height: windowHeight * 0.52,
                        width: double.infinity,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20.0, vertical: 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const FormFieldTitle(
                                title: 'Name*',
                              ),
                              SizedBox(
                                height: windowHeight * 0.01,
                              ),
                              RequireTextField(
                                type: Type.fullname,
                                controller: contactUsContoller.nameController,
                                readOnly: true,
                              ),
                              SizedBox(
                                height: windowHeight * 0.02,
                              ),
                              const FormFieldTitle(
                                title: 'Email*',
                              ),
                              SizedBox(
                                height: windowHeight * 0.01,
                              ),
                              RequireTextField(
                                type: Type.email,
                                controller: contactUsContoller.emailController,
                                readOnly: true,
                              ),
                              SizedBox(
                                height: windowHeight * 0.02,
                              ),
                              const FormFieldTitle(
                                title: 'Message*',
                              ),
                              SizedBox(
                                height: windowHeight * 0.01,
                              ),
                              RequireTextField(
                                type: Type.textArea,
                                maxLength: 500,
                                controller:
                                    contactUsContoller.messageController,
                              ),
                              SizedBox(
                                height: windowHeight * 0.02,
                              ),
                              PrimaryButton(
                                buttonTitle: 'Submit',
                                onPressed: () {
                                  contactUsContoller.onSubmit();
                                },
                                windowHeight: windowHeight,
                                windowWidth: windowWidth,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
