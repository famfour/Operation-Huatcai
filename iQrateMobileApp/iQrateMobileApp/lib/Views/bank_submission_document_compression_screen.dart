// ignore_for_file: unused_local_variable

import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/bank_submission_document_drawer_compression_screen_controller.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Widgets/common_appbar_options.dart';

// ignore: must_be_immutable
class BankSubmissionDocumentCompressionScreen extends StatelessWidget {
  BankSubmissionDocumentCompressionScreen({Key? key}) : super(key: key);

  BankSubmissionDocumentDrawerCompressionScreenController
      bankSubmissionDocumentDrawerCompressionScreenController =
      Get.put(BankSubmissionDocumentDrawerCompressionScreenController());

  @override
  Widget build(BuildContext context) {
    // double windowWidth = MediaQuery.of(context).size.width;
    double windowHeight = MediaQuery.of(context).size.height;
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          automaticallyImplyLeading: false,
          centerTitle: false,
          flexibleSpace: const Padding(
            padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 30),
            child: CommonAppBarOptions(
              windowWidth: 0,
              title: 'Manage Loan',
            ),
          ),
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(windowHeight * 0.03),
            child: const SizedBox(),
          ),
          backgroundColor: const Color(0xFFDF5356),
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
            ),
          ),
        ),
        body: const Body());
  }
}

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            height: windowHeight * 0.13,
          ),
          Center(
              child: Lottie.asset('assets/images/compressionAnimation.json')),
          SizedBox(
            height: windowHeight * 0.05,
          ),
          Text(
            'Generating zip',
            style: TextStyles.bankSubmissionCardTitle,
          ),
          SizedBox(
            height: windowHeight * 0.05,
          ),
          // PrimaryButton(
          //     windowHeight: windowHeight,
          //     windowWidth: windowWidth * 0.8,
          //     buttonTitle: 'Next',
          //     onPressed: () {
          //       Get.offNamed(bankSubmissionEmailBankerScreen, arguments: [
          //         Get.arguments[1], //BankId
          //         Get.arguments[2], //LeadID
          //         Get.arguments[0], // DocumentList,
          //       ]);
          //     }),
        ],
      ),
    );
  }
}
