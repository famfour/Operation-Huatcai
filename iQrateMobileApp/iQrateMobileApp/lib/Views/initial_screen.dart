import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/initial_screen_controller.dart';
import 'package:iqrate/DeviceManager/container_styles.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/secondary_button.dart';

class InitialScreen extends StatelessWidget {
  const InitialScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final InitialScreenController initialScreenController =
        Get.put(InitialScreenController()); //Initial Screen controller
    double windowWidth = MediaQuery.of(context).size.width;
    double windowHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(),
            Center(
              child: SizedBox(
                child: SvgPicture.asset(
                  'assets/logos/iqrate_logo.svg',
                  // semanticsLabel: 'Logo',
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Column(
                children: [
                  const Text('v (1.0.5)'),
                  const SizedBox(
                    height: 20,
                  ),
                  PrimaryButton(
                    windowHeight: windowHeight,
                    windowWidth: windowWidth,
                    onPressed: () {
                      initialScreenController.signUpButtonController();
                    },
                    buttonTitle: 'Register',
                  ),
                  SizedBox(
                    height: windowHeight * 0.02,
                  ),
                  // SecondaryButton(
                  //   windowHeight: windowHeight,
                  //   windowWidth: windowWidth,
                  //   kGradientBoxDecoration:
                  //       ContainerStyles.kGradientBoxDecorationSecondaryButton,
                  //   kInnerDecoration:
                  //       ContainerStyles.kInnerDecorationSecondaryButton,
                  //   buttonTitle: 'Log in',
                  //   onPressed: () {
                  //     initialScreenController.loginButtonController();
                  //   },
                  // ),
                  SecondaryButton(
                    windowHeight: windowHeight,
                    windowWidth: windowWidth,
                    kGradientBoxDecoration:
                        ContainerStyles.kGradientBoxDecorationSecondaryButton,
                    kInnerDecoration:
                        ContainerStyles.kInnerDecorationSecondaryButton,
                    buttonTitle: 'Log in',
                    onPressed: () {
                      initialScreenController.loginButtonController();
                    },
                  ),
                  SizedBox(
                    height: windowHeight * 0.06,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
