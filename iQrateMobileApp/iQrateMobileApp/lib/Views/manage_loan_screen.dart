// ignore_for_file: prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/login_controller.dart';
import 'package:iqrate/Service/GetControllers.dart';
import 'package:iqrate/Views/bank_submission_view.dart';
import 'package:iqrate/Views/co_broke/co_broke_submit.dart';
import 'package:iqrate/Views/email_log.dart';
import 'package:iqrate/Views/generate_packages.dart';
import 'package:iqrate/Views/law_firm_submission.dart';
import 'package:iqrate/Views/lead_profile.dart';
import 'package:iqrate/Views/loan_details.dart';
import 'package:iqrate/Views/property_details_view.dart';
import 'package:iqrate/Views/selected_packages.dart';
import 'package:iqrate/Views/submit_for_payout.dart';
import 'package:iqrate/Widgets/common_appbar_options.dart';
import 'package:logger/logger.dart';

class ManageLoanScreen extends StatefulWidget {
  const ManageLoanScreen({Key? key}) : super(key: key);

  @override
  _ManageLoanScreenState createState() => _ManageLoanScreenState();
}

class _ManageLoanScreenState extends State<ManageLoanScreen> {
  final SignInController signInController = Get.put(SignInController());
  var generatePackagesController =
      GetControllers.shared.getGeneratePackagesController();
  var selectedPackagesController =
      GetControllers.shared.getSelectedPackagesController();
  var leadProfileController = GetControllers.shared.getLeadProfileController();
  var leadData;

  @override
  void initState() {
    leadData = Get.arguments;
    super.initState();
    var data =
        GetControllers.shared.getGeneratePackagesController().coBrokerData;
    Logger().d(data.toJson());

    if (!leadProfileController.clientPDPAStatus.value) {
      GetControllers.shared.getSubmitCoBrokeController().isExpand.value = false;
      GetControllers.shared.getLoanDetailsController().isExpand.value = false;
      GetControllers.shared.getPropertyDetailsController().isExpand.value =
          false;
      GetControllers.shared.getGeneratePackagesController().isExpand.value =
          false;
      GetControllers.shared.getSelectedPackagesController().isExpand.value =
          false;
      GetControllers.shared.getBankSubmissionController().isExpand.value =
          false;
      GetControllers.shared.getSubmitForPayoutViewController().isExpand.value =
          false;
      GetControllers.shared.getLawFirmSubmissionController().isExpand.value =
          false;
      GetControllers.shared.getLeadProfileController().isExpand.value = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    // double windowWidth = MediaQuery.of(context).size.width;
    double windowHeight = MediaQuery.of(context).size.height;

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          automaticallyImplyLeading: false,
          centerTitle: false,
          flexibleSpace: const Padding(
            padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 30),
            child: CommonAppBarOptions(
              windowWidth: 0,
              title: 'Manage Loan',
            ),
          ),
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(windowHeight * 0.03),
            child: const SizedBox(),
          ),
          backgroundColor: const Color(0xFFDF5356),
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Column(mainAxisSize: MainAxisSize.max,
              // primary: true,
              // physics: const AlwaysScrollableScrollPhysics(), m
              children: [
                LeadProfile(data: leadData), //* Show this in all lead type

                //* Normal Agent -> Show when leadType is co-broke and cobroke not initiated
                // *CoBroker Agent -> We don't show
                if (generatePackagesController.isCoBroker == true &&
                    leadData.canEdit == true)
                  generatePackagesController.coBrokeStatus.value.trim() ==
                          'Co-Broke Received'
                      ? Container()
                      : SubmitCoBrokeView(data: leadData),

                //* Normal Agent -> Show this for leadType Yourslef (Always), Others(Always), Co-broke (We don't show / after co-broke we cant enter manage loan)
                //* CoBroker Agent -> Show this for Co_broke agent for all leadType
                generatePackagesController.coBrokeStatus.value.trim() ==
                        'Co-Broke Received'
                    ? LoanDetails(data: leadData)
                    : generatePackagesController.isCoBroker == false
                        ? LoanDetails(data: leadData)
                        : Container(),

                //* Normal Agent -> Show this for leadType Yourslef (Always), Others(Always), Co-broke (We don't show / after co-broke we cant enter manage loan)
                //* CoBroker Agent -> Show this for Co_broke agent for all leadType
                generatePackagesController.coBrokeStatus.value.trim() ==
                        'Co-Broke Received'
                    ? PropertyDetailsView(
                        data: leadData,
                      )
                    : generatePackagesController.isCoBroker == false
                        ? PropertyDetailsView(
                            data: leadData,
                          )
                        : Container(),

                //* Normal Agent -> Show this for leadType Yourslef (Always), Others(Always), Co-broke (We don't show / after co-broke we cant enter manage loan)
                //* CoBroker Agent -> Show this for Co_broke agent for all leadType
                generatePackagesController.coBrokeStatus.value.trim() ==
                        'Co-Broke Received'
                    ? GeneratePackages(data: leadData)
                    : generatePackagesController.isCoBroker == false
                        ? GeneratePackages(data: leadData)
                        : Container(),

                //*  Normal Agent -> Show this for leadType Yourslef (Always), Others(Always), Co-broke (We don't show / after co-broke we cant enter manage loan)
                //* CoBroker Agent -> Show this for Co_broke agent for all leadType
                generatePackagesController.coBrokeStatus.value.trim() ==
                        'Co-Broke Received'
                    ? SelectedPackages(data: leadData)
                    : generatePackagesController.isCoBroker == false
                        ? SelectedPackages(data: leadData)
                        : Container(),

                //* Normal Agent -> Show this for leadType Yourslef (Only after cobroke initiated), Others(Always), Co-broke (We don't show / after co-broke we cant enter manage loan)
                //* CoBroker Agent -> Show this for Co_broke agent for all leadType
                generatePackagesController.coBrokeStatus.value.trim() ==
                        'Co-Broke Received'
                    ? BankSubmissionView(
                        data: leadData,
                      )
                    : generatePackagesController.isCoBroker == false
                        ? generatePackagesController.coBrokeStatus.value
                                    .trim() ==
                                'Co-Broke Received'
                            ? BankSubmissionView(
                                data: leadData,
                              )
                            : (leadData.leadType == 'yourself' &&
                                    leadData.canEdit == true)
                                ? Container()
                                : BankSubmissionView(
                                    data: leadData,
                                  )
                        : Container(),

                //* Normal Agent -> Show this for leadType Yourslef (Only after cobroke initiated), Others(Always), Co-broke (We don't show / after co-broke we cant enter manage loan)
                //* CoBroker Agent -> Show this for Co_broke agent for all leadType
                generatePackagesController.coBrokeStatus.value.trim() ==
                        'Co-Broke Received'
                    ?  SubmitForPayoutView(data: leadData)
                    : generatePackagesController.isCoBroker == false
                        ? generatePackagesController.coBrokeStatus.value
                                    .trim() ==
                                'Co-Broke Received'
                            ? SubmitForPayoutView(data: leadData)
                            : (leadData.leadType == 'yourself' &&
                                    leadData.canEdit == true)
                                ? Container()
                                : SubmitForPayoutView(data: leadData)
                        : Container(),

                //* Normal Agent -> Show this for leadType Yourslef (Only after cobroke initiated), Others(Always), Co-broke (We don't show / after co-broke we cant enter manage loan)
                //* CoBroker Agent -> Show this for Co_broke agent for all leadType
                generatePackagesController.coBrokeStatus.value.trim() ==
                        'Co-Broke Received'
                    ? const LawFirmSubmission()
                    : generatePackagesController.isCoBroker == false
                        ? generatePackagesController.coBrokeStatus.value
                                    .trim() ==
                                'Co-Broke Received'
                            ? const LawFirmSubmission()
                            : (leadData.leadType == 'yourself' &&
                                    leadData.canEdit == true)
                                ? Container()
                                : const LawFirmSubmission()
                        : Container(),

                //* Normal Agent -> Show this for leadType Yourslef (Only after cobroke initiated), Others(Always), Co-broke (We don't show / after co-broke we cant enter manage loan)
                //* CoBroker Agent -> Show this for Co_broke agent for all leadType
                generatePackagesController.coBrokeStatus.value.trim() ==
                        'Co-Broke Received'
                    ? EmailLogView(
                        data: leadData,
                      )
                    : generatePackagesController.isCoBroker == false
                        ? generatePackagesController.coBrokeStatus.value
                                    .trim() ==
                                'Co-Broke Received'
                            ? EmailLogView(
                                data: leadData,
                              )
                            : (leadData.leadType == 'yourself' &&
                                    leadData.canEdit == true)
                                ? Container()
                                : EmailLogView(
                                    data: leadData,
                                  )
                        : Container(),

                //* Normal Agent -> Show this for leadType Yourslef (always), Others(We Don't show), Co-broke (We don't show / after co-broke we cant enter manage loan)
                //* CoBroker Agent -> NEVER Show this for Co_broke agent for all leadType
                if (leadData.leadType == 'yourself')
                  generatePackagesController.coBrokeStatus.value.trim() ==
                          'Co-Broke Received'
                      ? Container()
                      : SubmitCoBrokeView(
                          data: leadData,
                        ),
                const SizedBox(height: 60)
              ]),
        ));
  }
}
