// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:iqrate/Widgets/require_text_field.dart';

// ignore: must_be_immutable
class CurrencyAmountTextField extends StatefulWidget {
  final TextEditingController textEditingController;
  final String labelText;
  bool? readOnly;
  CurrencyAmountTextField(
      {Key? key,
      required this.textEditingController,
      required this.labelText,
      this.readOnly})
      : super(key: key);
  @override
  _CurrencyAmountTextFieldState createState() =>
      _CurrencyAmountTextFieldState();
}

class _CurrencyAmountTextFieldState extends State<CurrencyAmountTextField> {
  static const _locale = 'en';
  String _formatNumber(String s) =>
      NumberFormat.decimalPattern(_locale).format(double.parse(s));

  @override
  Widget build(BuildContext context) {
    return RequireTextField(
      type: Type.money,
      hintText: widget.labelText,
      controller: widget.textEditingController,
      labelText: widget.labelText,
      readOnly: widget.readOnly ?? false,
      onChanged: (string) {
        string = _formatNumber(string.replaceAll(',', ''));
        // string = "\$ $string";
        widget.textEditingController.value = TextEditingValue(
          text: _formatNumber(
            double.tryParse(string.replaceAll(',', ''))!
                .toStringAsFixed(2)
                .replaceAll(',', ''),
          ),
          selection: TextSelection.collapsed(offset: string.length),
        );
      },
    );
  }
}
