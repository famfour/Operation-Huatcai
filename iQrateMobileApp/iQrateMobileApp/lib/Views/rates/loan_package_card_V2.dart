// ignore: must_be_immutable
// ignore_for_file: must_be_immutable, duplicate_ignore, file_names

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/rates_view_controller.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Model/response_model.dart/list_all_packages_response_model_v2.dart';
import 'package:iqrate/Service/GetControllers.dart';

import 'rates_view_details.dart';

class LoanPackageCardV2 extends StatelessWidget {
  String? image;
  String headingText;
  String headingSubText;
  void Function(bool?)? onChanged;
  int cardIndex;
  // final Results data;
  final PackageModel data;

  LoanPackageCardV2(
      {Key? key,
      this.image,
      required this.headingText,
      required this.headingSubText,
      required this.onChanged,
      required this.cardIndex,
      required this.data})
      : super(key: key);

  // final RatesViewController ratesViewController = Get.find();
  final RatesViewController ratesViewController =
      GetControllers.shared.getRatesViewController();

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15))),
      elevation: 5,
      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Column(
        children: [rowOne(), rowTwo(), expansionSection()],
      ),
    );
  }

  // 1st row that shows the Bank name , image and a checkbox
  Widget rowOne() {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Row(
        children: [
          SizedBox(
              height: 80,
              width: 120,
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Image.network(image!),
              )),
          Expanded(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                Text(
                  headingText.toString().replaceAll('_', ' '),
                  style: TextStyles.cardHeading,
                ),
                Text(
                  headingSubText.toString().replaceAll('_', ' ').capitalize!,
                  style: TextStyles.cardSubHeading,
                )
              ])),
          Transform.scale(
            scale: 1.5,
            child: Checkbox(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
              ),
              value: data.isChecked,
              onChanged: onChanged,
              hoverColor: AppColors.kSecondaryColor,
              activeColor: AppColors.kSecondaryColor,
              side: const BorderSide(
                  color: AppColors.kSecondaryColor,
                  width: 1,
                  style: BorderStyle.solid),
            ),
          ),
        ],
      ),
    );
  }

  // 2nd row that shows the percentage on year based
  Widget rowTwo() {
    var rates = data.rates![0].year.toString() != 'year_1'
        ? data.rates!.reversed
        : data.rates;
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 5),
      child: Row(
        children: [
          Expanded(
            child: Center(
              child: SizedBox(
                height: 20.0,
                child: ListView.builder(
                  physics: const ClampingScrollPhysics(),
                  shrinkWrap: true,
                  primary: false,
                  scrollDirection: Axis.horizontal,
                  itemCount: rates!.length > 3 ? 3 : rates.length,
                  itemBuilder: (BuildContext context, int index) => Row(
                    children: [
                      const SizedBox(
                        width: 10,
                      ),
                      titleValueRow(
                          // title: rates.elementAt(index).year.toString() + ": ",
                          title: "Year ${index + 1}",
                          value: rates
                                  .elementAt(index)
                                  .totalInterestRate
                                  .toString() +
                              "%"),
                      const SizedBox(
                        width: 10,
                      ),
                      index < 2
                          ? const VerticalDivider(
                              color: Colors.black,
                            )
                          : const SizedBox(),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  // A simple widget used in the 2nd row, which only shows a title and a value
  Widget titleValueRow({required String title, required String value}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          title + ': ',
          style: TextStyles.headingSubtitleStyle,
        ),
        Text(
          value,
          style: TextStyles.headingSubtitleStyle,
        )
      ],
    );
  }

  // This sections expands to show additional details in the specific
  Widget expansionSection() {
    return InkWell(
      onTap: () {
        debugPrint("RatesViewDetailsV2");
        Get.to(() => RatesViewDetailsV2(
              data: data,
            ));
      },
      child: ClipRRect(
        borderRadius: const BorderRadius.only(
            bottomLeft: Radius.circular(15), bottomRight: Radius.circular(15)),
        child: Container(
          color: AppColors.kSecondaryColor,
          child: Center(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Details',
                style: TextStyles.detailsButton,
              ),
            ),
          ),
        ),
      ),
    );
  }

  // This section is displayed when the card is expanded and this expansion tile is shown in a list
  Widget expandedSectionHeading(String title, List<Widget> body) {
    return ExpansionTile(
      iconColor: Colors.white,
      backgroundColor: AppColors.kSecondaryColor,
      collapsedBackgroundColor: AppColors.kSecondaryColor,
      collapsedIconColor: Colors.white,
      leading: Text(
        title,
        style: TextStyles.detailsButton,
      ),
      title: Container(),
      children: body,
    );
  }
}
