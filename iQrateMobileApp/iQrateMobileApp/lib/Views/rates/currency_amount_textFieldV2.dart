// ignore_for_file: file_names, unused_element

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:iqrate/Widgets/require_text_field.dart';

class CurrencyAmountTextFieldV2 extends StatefulWidget {
  final TextEditingController textEditingController;
  final String labelText;
  const CurrencyAmountTextFieldV2(
      {Key? key, required this.textEditingController, required this.labelText})
      : super(key: key);
  @override
  _CurrencyAmountTextFieldV2State createState() =>
      _CurrencyAmountTextFieldV2State();
}

class _CurrencyAmountTextFieldV2State extends State<CurrencyAmountTextFieldV2> {
  static const _locale = 'en';
  String _formatNumber(String s) =>
      NumberFormat.decimalPattern(_locale).format(double.parse(s));

  @override
  Widget build(BuildContext context) {
    return RequireTextField(
      type: Type.money,
      hintText: widget.labelText,
      controller: widget.textEditingController,
      labelText: widget.labelText,
      onChanged: (string) {
        String amount = double.parse(string).toStringAsFixed(2);

        //amount = _formatNumber(amount.replaceAll(',', ''));
        //string = "\$ $string";
        widget.textEditingController.value = TextEditingValue(
          text: amount,
          selection: TextSelection.collapsed(offset: amount.length),
        );
      },
    );
  }
}

/*
    .toStringAsFixed(
2)
.replaceAllMapped(
RegExp(
r'(\d{1,3})(?=(\d{3})+(?!\d))'),
(Match m) =>
'${m[1]},')*/
