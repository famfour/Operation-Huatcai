import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/rates_view_controller.dart';
import 'package:iqrate/Model/response_model.dart/list_all_packages_response_model_v2.dart';
import 'package:iqrate/Service/GetControllers.dart';
import 'package:iqrate/Views/rates/load_package_card_details.dart';
import 'package:iqrate/Widgets/common_appbar_options.dart';

class RatesViewDetailsV2 extends StatefulWidget {
  final PackageModel data;

  const RatesViewDetailsV2({Key? key, required this.data}) : super(key: key);

  @override
  State<RatesViewDetailsV2> createState() => _RatesViewDetailsV2State();
}

class _RatesViewDetailsV2State extends State<RatesViewDetailsV2> {
  late double windowHeight;
  late double windowWidth;

  final RatesViewController ratesViewController =
      GetControllers.shared.getRatesViewController();

  @override
  Widget build(BuildContext context) {
    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.width;

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          automaticallyImplyLeading: false,
          centerTitle: false,
          flexibleSpace: const Padding(
            padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 30),
            child: CommonAppBarOptions(
              windowWidth: 0,
              title: 'Manage Loan',
            ),
          ),
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(windowHeight * 0.03),
            child: const SizedBox(),
          ),
          backgroundColor: const Color(0xFFDF5356),
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.only(top: windowWidth * 0.05),
            child: LoanPackageCardDetails(
              data: widget.data,
              image: widget.data.bank!.logo!,
              headingSubText: widget.data.rateType
                      .toString()
                      .replaceAll('_', ' ')
                      .capitalize! +
                  (" (${widget.data.rateCategory.toString().capitalize})"),
              headingText: widget.data.bank!.name!,
              onChanged: (val) {
                if (val == true) {
                  setState(() {
                    widget.data.isChecked = val!;
                  });

                  //unchecked main list
                  PackageModel selectedData = widget.data;
                  selectedData.isChecked = true;

                  ratesViewController.listData[ratesViewController.listData
                          .indexWhere(
                              (element) => element.id == widget.data.id)] =
                      selectedData;

                  var b = ratesViewController.getCountItemSelected();
                  debugPrint("after $b");
                  ratesViewController.listData.refresh();
                } else {
                  setState(() {
                    widget.data.isChecked = val!;
                  });

                  //unchecked main list
                  PackageModel selectedData = widget.data;
                  selectedData.isChecked = false;

                  ratesViewController.listData[ratesViewController.listData
                          .indexWhere(
                              (element) => element.id == widget.data.id)] =
                      selectedData;

                  var b = ratesViewController.getCountItemSelected();
                  debugPrint("after $b");
                  ratesViewController.listData.refresh();
                }
              },
              // checkBoxValue: false,
              cardIndex: 0,
            ),
          ),
        ));
  }
}
