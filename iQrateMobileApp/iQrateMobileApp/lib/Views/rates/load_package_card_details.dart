// ignore_for_file: must_be_immutable, prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:iqrate/Controller/leads_view_controller.dart';
import 'package:iqrate/Controller/rates_view_controller.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Model/common_key_value_model.dart';
import 'package:iqrate/Model/response_model.dart/list_all_packages_response_model_v2.dart';
import 'package:iqrate/Service/GetControllers.dart';

class LoanPackageCardDetails extends StatelessWidget {
  String? image;
  String headingText;
  String headingSubText;
  void Function(bool?)? onChanged;
  int cardIndex;
  final PackageModel data;
  final oCcy = NumberFormat("#,##0.0", "en_US");
  LoanPackageCardDetails(
      {Key? key,
      this.image,
      required this.headingText,
      required this.headingSubText,
      required this.onChanged,
      required this.cardIndex,
      required this.data})
      : super(key: key);

  // final RatesViewController ratesViewController = Get.find();
  final RatesViewController ratesViewController =
      GetControllers.shared.getRatesViewController();
  final LeadsViewController leadsViewController = Get.find();

  late double windowHeight;
  late double windowWidth;
  late var rates;
  @override
  Widget build(BuildContext context) {
    debugPrint("LoanPackageCardDetails");
    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.width;
    rates = data.rates![0].year.toString() != 'year_1'
        ? data.rates!.reversed
        : data.rates;
    return Card(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15))),
      elevation: 5,
      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Column(
        children: [rowOne(), rowTwo(), expansionSection()],
      ),
    );
  }

  // 1st row that shows the Bank name , image and a checkbox
  Widget rowOne() {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Row(
        children: [
          Expanded(
              child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Image.network(image!),
          )),
          Expanded(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                Text(
                  headingText,
                  style: TextStyles.cardHeading,
                ),
                Text(
                  headingSubText,
                  style: TextStyles.cardSubHeading,
                )
              ])),
          Transform.scale(
            scale: 1.3,
            child: Checkbox(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
              ),
              value: data.isChecked,
              onChanged: onChanged,
              //checkColor: AppColors.kSecondaryColor,
              //fillColor: MaterialStateProperty.all(Colors.white),
              hoverColor: AppColors.kSecondaryColor,
              activeColor: AppColors.kSecondaryColor,
              side: const BorderSide(
                  color: AppColors.kSecondaryColor,
                  width: 1,
                  style: BorderStyle.solid),
            ),
          )
        ],
      ),
    );
  }

  // 2nd row that shows the percentage on year based
  Widget rowTwo() {
    rates = data.rates![0].year.toString() != 'year_1'
        ? data.rates!.reversed
        : data.rates;
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 5),
      child: Row(
        children: [
          Expanded(
            child: Center(
              child: SizedBox(
                height: 20.0,
                child: ListView.builder(
                  physics: const ClampingScrollPhysics(),
                  shrinkWrap: true,
                  primary: false,
                  scrollDirection: Axis.horizontal,
                  itemCount: rates.length > 3 ? 3 : rates.length,
                  itemBuilder: (BuildContext context, int index) => Row(
                    children: [
                      const SizedBox(
                        width: 10,
                      ),
                      titleValueRow(
                          // title: rates.elementAt(index).year.toString() + ": ",
                          title: "Year ${index + 1}",
                          value: rates
                                  .elementAt(index)
                                  .totalInterestRate
                                  .toString() +
                              "%"),
                      const SizedBox(
                        width: 10,
                      ),
                      index < 2
                          ? const VerticalDivider(
                              color: Colors.black,
                            )
                          : const SizedBox(),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
  //
  // // A simple widget used in the 2nd row, which only shows a title and a value
  // Widget titleValueRow({required String title, required String value}) {
  //   return Row(
  //     mainAxisAlignment: MainAxisAlignment.center,
  //     children: [
  //       Text(
  //         title + ': ',
  //         style: TextStyles.headingSubtitleStyle,
  //       ),
  //       Text(
  //         value,
  //         style: TextStyles.headingSubtitleStyle,
  //       )
  //     ],
  //   );
  // }

  Widget expansionSection() {
    return InkWell(
      onTap: () {
        //ratesViewController.expandingCard(index);
      },
      child: ClipRRect(
        /*borderRadius: const BorderRadius.only(
            bottomLeft: Radius.circular(15), bottomRight: Radius.circular(15)),*/
        child: Container(
            color: AppColors.kSecondaryColor,
            child: Container(
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  ListView(
                    primary: false,
                    shrinkWrap: true,
                    children: [
                      expandableCardRates(),
                      SizedBox(height: windowHeight * 0.02),
                      expandableCardKeyFeatures(),
                      SizedBox(height: windowHeight * 0.02),
                      expandableCardBankSubsidy(),
                      SizedBox(height: windowHeight * 0.02),
                      expandableCardEarlyRepaymentPenalty(),
                    ],
                  )
                ],
              ),
            )),
      ),
    );
  }

  Widget expandableCardRates() {
    return GestureDetector(
      onTap: () {
        ratesViewController.isExpandedRates.toggle();
      },
      child: Obx(() => Container(
            decoration: const BoxDecoration(boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.black54,
                  blurRadius: 5.0,
                  offset: Offset(0.0, 0.3))
            ], color: Colors.white),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.all(windowWidth * 0.02),
                  color: AppColors.red,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text("Rates", style: TextStyles.basicTextStyle2),
                      const Spacer(),
                      const Padding(
                        padding: EdgeInsets.only(bottom: 10.0),
                        child: Icon(Icons.minimize, color: Colors.white),
                      ),
                    ],
                  ),
                ),
                ratesViewController.isExpandedRates.value
                    ? Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 7.0),
                            child: ListView.builder(
                              //physics: ClampingScrollPhysics(),
                              shrinkWrap: true,
                              primary: false,
                              scrollDirection: Axis.vertical,
                              itemCount: rates.length,
                              itemBuilder: (BuildContext context, int index) =>
                                  titleValueRow2(
                                      title: rates
                                              .elementAt(index)
                                              .year
                                              .toString()
                                              .replaceAll("_", " ")
                                              .capitalizeFirst! +
                                          " ",
                                      value: rates
                                                  .elementAt(index)
                                                  .referenceRate
                                                  .rateType
                                                  .toString()
                                                  .toLowerCase() !=
                                              "fixed"
                                          ? "${rates.elementAt(index).referenceRate.reference} (${rates.elementAt(index).referenceRate.interestRate.toString()})% ${rates.elementAt(index).referenceRate.equation.toString()} ${rates.elementAt(index).bankSpread.toString()}% = " +
                                              rates
                                                  .elementAt(index)
                                                  .totalInterestRate
                                                  .toString() +
                                              "%"
                                          : rates
                                                  .elementAt(index)
                                                  .referenceRate
                                                  .reference +
                                              ' ' +
                                              rates
                                                  .elementAt(index)
                                                  .totalInterestRate
                                                  .toString() +
                                              "%"),
                            ),
                          ),
                        ],
                      )
                    : Container(),
              ],
            ),
          )),
    );
  }

  Widget expandableCardKeyFeatures() {
    //! Making this to display the values from KVSTore by matching the keys that we recive from the Server response--- STARTS HERE----
    // * This is to show the values in the property status values with the keys we receive from the server response--- STARTS HERE---
    List<CommonKeyValueModel> propertyStatus = [];
    List<String> propertyStatusValues = [];
    for (var element in leadsViewController.kvStoreValues) {
      if (element.code == "property_status") {
        element.value.forEach((key, value) {
          propertyStatus.add(CommonKeyValueModel(key, value));
          debugPrint("array_key:: " + value);
        });
      }
    }
    for (var key in data.propertyStatus!) {
      for (var element in propertyStatus) {
        if (key == element.key) {
          propertyStatusValues.add(element.value);
        }
      }
    }
    // * This is to show the values in the property status values with the keys we receive from the server response--- ENDS HERE---
    // * This is to show the values in the property type values with the keys we receive from the server response--- STARTS HERE---
    List<CommonKeyValueModel> propertyType = [];
    List<String> propertyTypeValues = [];
    for (var element in leadsViewController.kvStoreValues) {
      if (element.code == "property_type") {
        element.value.forEach((key, value) {
          propertyType.add(CommonKeyValueModel(key, value));
          debugPrint("array_key:: " + value);
        });
      }
    }
    for (var key in data.propertyTypes!) {
      for (var element in propertyType) {
        if (key == element.key) {
          propertyTypeValues.add(element.value);
        }
      }
    }
    // * This is to show the values in the property type values with the keys we receive from the server response--- ENDS HERE---
    // * This is to show the values in the loan type values with the keys we receive from the server response--- STARTS HERE---
    List<CommonKeyValueModel> loanType = [];
    List<String> loanTypeValues = [];
    for (var element in leadsViewController.kvStoreValues) {
      if (element.code == "loan_category") {
        //debugPrint("=====kvstore====="+element.value.toString());
        element.value.forEach((key, value) {
          loanType.add(CommonKeyValueModel(key, value));
          debugPrint("array_key:: " + value);
        });
      }
    }

    for (var key in data.loanCategory!) {
      for (var element in loanType) {
        if (key == element.key) {
          loanTypeValues.add(element.value);
        }
      }
    }
    // * This is to show the values in the loan type values with the keys we receive from the server response--- ENDS HERE---
    //! Making this to display the values from KVSTore by matching the keys that we recive from the Server response--- ENDS HERE----
    return GestureDetector(
      onTap: () {
        ratesViewController.isExpandedKeyFeatures.toggle();
      },
      child: Obx(() => Container(
            decoration: const BoxDecoration(boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.black54,
                  blurRadius: 5.0,
                  offset: Offset(0.0, 0.3))
            ], color: Colors.white),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.all(windowWidth * 0.02),
                  color: AppColors.red,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text("Key Features", style: TextStyles.basicTextStyle2),
                      const Spacer(),
                      const Padding(
                        padding: EdgeInsets.only(bottom: 10.0),
                        child: Icon(Icons.minimize, color: Colors.white),
                      ),
                    ],
                  ),
                ),
                ratesViewController.isExpandedKeyFeatures.value
                    ? Padding(
                        padding: const EdgeInsets.symmetric(vertical: 3.0),
                        child: Column(
                          children: [
                            titleValueRowWithListForType(
                                title: "Property Type",
                                values: propertyTypeValues),
                            titleValueRowWithListForType(
                                title: "Property Status",
                                values: propertyStatusValues),
                            titleValueRowWithListForType(
                                title: "Loan Type", values: loanTypeValues),
                            titleValueRow3(
                                title: "Lock in Period",
                                value: data.lockInPeriod.toString() + " Years"),
                            titleValueRow3(
                                title: "Min Loan Amount",
                                value: "\$" + oCcy.format(data.minLoanAmount)),
                            titleValueRow3(
                                title: "Deposit to Place",
                                value: "\$" + oCcy.format(data.depositToPlace)),
                            titleValueRowWithListForType(
                                title: "Remarks for Client",
                                values: data.remarksForClient!),
                            titleValueRow3(
                                title: "Remarks for Broker",
                                value: data.remarksForBroker.toString()),
                            titleValueRow3(
                                title: "Interest Reset Date",
                                value: data.interestResetDate! ? "Yes" : "No"),
                            titleValueRow3(
                                title: "Processing Fee",
                                value: data.processingFee! ? "Yes" : "No"),
                          ],
                        ),
                      )
                    : Container()
              ],
            ),
          )),
    );
  }

  Widget expandableCardBankSubsidy() {
    return GestureDetector(
      onTap: () {
        ratesViewController.isExpandedBankSubsidy.toggle();
      },
      child: Obx(() => Container(
            decoration: const BoxDecoration(boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.black54,
                  blurRadius: 5.0,
                  offset: Offset(0.0, 0.3))
            ], color: Colors.white),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.all(windowWidth * 0.02),
                  color: AppColors.red,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text("Bank’s Subsidy", style: TextStyles.basicTextStyle2),
                      const Spacer(),
                      const Padding(
                        padding: EdgeInsets.only(bottom: 10.0),
                        child: Icon(Icons.minimize, color: Colors.white),
                      ),
                    ],
                  ),
                ),
                ratesViewController.isExpandedBankSubsidy.value
                    ? Padding(
                        padding: const EdgeInsets.symmetric(vertical: 5.0),
                        child: Column(
                          children: [
                            titleValueRowWithListForType(
                                title: "Cash Rebate/ Legal Subsidy",
                                values: data.cashRebateLegalSubsidy!),
                            titleValueRowWithListForType(
                                title: "Valuation Subsidy",
                                values: data.valuationSubsidy!),
                            titleValueRow3(
                                title: "Cash Rebate/ Subsidy Clawback",
                                value: data.cashRebateSubsidyClawbackPeriodYears
                                        .toString() +
                                    " Years"),
                            titleValueRow3(
                                title: "Fire Insurance Subsidy",
                                value: data.fireInsuranceSubsidy.toString()),
                          ],
                        ),
                      )
                    : Container()
              ],
            ),
          )),
    );
  }

  Widget expandableCardEarlyRepaymentPenalty() {
    return GestureDetector(
      onTap: () {
        ratesViewController.isExpandedBankPenalty.toggle();
      },
      child: Obx(() => Container(
            decoration: const BoxDecoration(boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.black54,
                  blurRadius: 5.0,
                  offset: Offset(0.0, 0.3))
            ], color: Colors.white),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.all(windowWidth * 0.02),
                  color: AppColors.red,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text("Early Repayment Penalty",
                          style: TextStyles.basicTextStyle2),
                      const Spacer(),
                      const Padding(
                        padding: EdgeInsets.only(bottom: 10.0),
                        child: Icon(Icons.minimize, color: Colors.white),
                      ),
                    ],
                  ),
                ),
                ratesViewController.isExpandedBankPenalty.value
                    ? Padding(
                        padding: const EdgeInsets.symmetric(vertical: 5.0),
                        child: Column(
                          children: [
                            titleValueRow3(
                                title: "Partial Repayment Penalty",
                                value:
                                    data.partialRepaymentPenalty!.toString() +
                                        "%"),
                            titleValueRow3(
                                title: "Partial Repayment Penalty Remarks",
                                value: data.partialRepaymentPenaltyRemarks ??
                                    'NA'),
                            titleValueRow3(
                                title: "Full Repayment Penalty",
                                value: data.fullRepaymentPenalty!.toString() +
                                    "%"),
                            titleValueRow3(
                                title: "Full Repayment Penalty Remarks",
                                value:
                                    data.fullRepaymentPenaltyRemarks ?? "NA"),
                            titleValueRow3(
                                title: "Cancellation Fee",
                                value: data.cancellationFee!.toString()),
                            titleValueRow3(
                                title: "Cancellation Fee Remarks",
                                value: data.cancellationFeeRemarks!.isEmpty
                                    ? "NA"
                                    : data.cancellationFeeRemarks.toString()),
                          ],
                        ),
                      )
                    : Container()
              ],
            ),
          )),
    );
  }

  //! A simple widget used in the 2nd row, which only shows a title and a value (percentage)
  Widget titleValueRow({required String title, required String value}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          title + ': ',
          style: TextStyles.headingSubtitleStyle,
        ),
        Text(
          value,
          style: TextStyles.headingSubtitleStyle,
        )
      ],
    );
  }

  // A simple widget used in the 2nd row, which only shows a title and a value
  Widget titleValueRow2({required String title, required String value}) {
    return Padding(
      padding:
          const EdgeInsets.only(right: 10.0, left: 10.0, top: 3, bottom: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 3,
            child: Text(
              title,
              style: TextStyles.headingSubtitleStyle2,
            ),
          ),
          //Spacer(),
          Expanded(
            flex: 7,
            child: Text(
              value,
              style: TextStyles.headingSubtitleStyle2,
            ),
          )
        ],
      ),
    );
  }

  Widget titleValueRow3({required String title, required String value}) {
    return Padding(
      padding:
          const EdgeInsets.only(right: 10.0, left: 10.0, top: 8, bottom: 5),
      //padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: 4,
                child: Text(
                  title,
                  style: TextStyles.headingSubtitleStyle4,
                ),
              ),
              SizedBox(
                width: windowWidth * 0.01,
              ),
              Flexible(
                flex: 4,
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    value,
                    style: TextStyles.headingSubtitleStyle2,
                    textAlign: TextAlign.left,
                  ),
                ),
              )
            ],
          ),
          const Divider(
            thickness: 1,
            color: Colors.black26,
          )
        ],
      ),
    );
  }

  Widget titleValueRowWithListForType(
      {required String title, required List<String> values}) {
    return Padding(
      padding:
          const EdgeInsets.only(right: 10.0, left: 10.0, top: 8, bottom: 5),
      //padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: 4,
                child: Text(
                  title,
                  style: TextStyles.headingSubtitleStyle4,
                ),
              ),
              SizedBox(
                width: windowWidth * 0.05,
              ),
              Flexible(
                flex: 4,
                child: ListView.builder(
                    physics: const ClampingScrollPhysics(),
                    shrinkWrap: true,
                    primary: false,
                    scrollDirection: Axis.vertical,
                    itemCount: values.length,
                    itemBuilder: (BuildContext context, int index) => Align(
                          alignment: Alignment.topRight,
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 8.0),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                values.elementAt(index).trim() +
                                    ((values.length - 1) == index ? "" : ","),
                                style: TextStyles.headingSubtitleStyle2,
                                /*maxLines: 1,
                                overflow: TextOverflow.ellipsis,*/
                                textAlign: TextAlign.start,
                              ),
                            ),
                          ),
                        )),
              )
            ],
          ),
          const Divider(
            thickness: 1,
            color: Colors.black26,
          )
        ],
      ),
    );
  }

  //! Title and value row containing list
  Widget titleValueRowWithList(
      {required String title, required List<String> values}) {
    return Padding(
      padding:
          const EdgeInsets.only(right: 10.0, left: 10.0, top: 8, bottom: 5),
      //padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: 4,
                child: Text(
                  title,
                  style: TextStyles.headingSubtitleStyle4,
                ),
              ),
              // SizedBox(
              //   width: windowWidth * 0.05,
              // ),
              const SizedBox(
                width: 5,
              ),
              Flexible(
                  flex: 4,
                  child: ListView.builder(
                    physics: const ClampingScrollPhysics(),
                    shrinkWrap: true,
                    primary: false,
                    scrollDirection: Axis.vertical,
                    itemCount: values.length,
                    itemBuilder: (BuildContext context, int index) => Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                            padding: const EdgeInsets.only(bottom: 8.0),
                            child: Text(
                              values.elementAt(index) == "NA"
                                  ? "NA"
                                  : values
                                      .elementAt(index)
                                      .replaceAll("_", " "),
                              style: TextStyles.headingSubtitleStyle2,
                              textAlign: TextAlign.left,
                            ))),
                  ))
            ],
          ),
          const Divider(
            thickness: 1,
            color: Colors.black26,
          )
        ],
      ),
    );
  }

  //
  // // This sections expands to show additional details in the specific
  // Widget expansionSection() {
  //   return InkWell(
  //     onTap: () {
  //       debugPrint("on off RatesViewDetailsV2");
  //       // // ratesViewController.selectedIndex.value == index
  //       // ratesViewController.isExpandedRates.value = true;
  //     },
  //     child: ClipRRect(
  //       borderRadius: const BorderRadius.only(
  //           bottomLeft: Radius.circular(15), bottomRight: Radius.circular(15)),
  //       child: Container(
  //         color: AppColors.white,
  //         child: Container(
  //                   color: Colors.white,
  //                   child: Column(
  //                     crossAxisAlignment: CrossAxisAlignment.end,
  //                     children: [
  //                       InkWell(
  //                           onTap: () {
  //                             //  close the expansion pannel on tapping here
  //                             ratesViewController.reset();
  //                           },
  //                           child: const Padding(
  //                             padding: EdgeInsets.only(bottom: 10.0),
  //                             child: Icon(Icons.minimize),
  //                           )),
  //                       ListView(
  //                         shrinkWrap: true,
  //                         children: [
  //                           expandedSectionHeading(
  //                               'Rates', [const Text('Enjoy!')]),
  //                           expandedSectionHeading(
  //                               'Key Features', [const Text('Enjoy!')]),
  //                           expandedSectionHeading(
  //                               "Bank's Subsidy", [const Text('Enjoy!')]),
  //                           expandedSectionHeading('Early Repayment Penalty',
  //                               [const Text('Enjoy!')]),
  //                         ],
  //                       )
  //                     ],
  //                   ),
  //                 )
  //
  //         // Center(
  //         //   child: Padding(
  //         //     padding: const EdgeInsets.all(8.0),
  //         //     child: Text(
  //         //       'Details',
  //         //       style: TextStyles.detailsButton,
  //         //     ),
  //         //   ),
  //         // ),
  //       ),
  //     ),
  //   );
  // }

  // This section is displayed when the card is expanded and this expansion tile is shown in a list
  // Widget expandedSectionHeading(String title, List<Widget> body) {
  //   return ExpansionTile(
  //     iconColor: Colors.white,
  //     backgroundColor: AppColors.kSecondaryColor,
  //     collapsedBackgroundColor: AppColors.kSecondaryColor,
  //     collapsedIconColor: Colors.white,
  //     leading: Text(
  //       title,
  //       style: TextStyles.detailsButton,
  //     ),
  //     title: Container(),
  //     children: body,
  //   );
  // }
}
