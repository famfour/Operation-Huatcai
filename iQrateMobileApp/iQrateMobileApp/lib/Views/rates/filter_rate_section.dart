// ignore_for_file: must_be_immutable, duplicate_ignore

// ignore: must_be_immutable
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/rates_view_controller.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Service/GetControllers.dart';
import 'package:iqrate/Utils/ItemCheckBoxModel.dart';
import 'package:iqrate/Utils/multi_checkbox_custom.dart';
import 'package:iqrate/Views/rates/currency_amount_textField.dart';
import 'package:iqrate/Views/rates_view.dart';
import 'package:iqrate/Widgets/primary_button.dart';

class FilterRateSection extends StatefulWidget {
  final Function resetFilterClicked;
  final Function closeFilterClicked;
  FilterRateSection(
      {Key? key,
      required this.windowHeight,
      required this.windowWidth,
      required this.resetFilterClicked,
      required this.closeFilterClicked})
      : super(key: key);
  double windowHeight;
  double windowWidth;

  @override
  State<FilterRateSection> createState() => _FilterRateSectionState();
}

class _FilterRateSectionState extends State<FilterRateSection> {
  // final LeadsViewController leadsViewController = Get.find();
  final RatesViewController ratesViewController =
      GetControllers.shared.getRatesViewController();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: const EdgeInsets.only(top: 0),
      child: Card(
        elevation: 0,
        color: Colors.black38,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            topBar(),
            body(),
          ],
        ),
      ),
    );
  }

  Widget topBar() {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          children: [
            Expanded(
              flex: 5,
              child: InkWell(
                onTap: () {
                  ratesViewController.isWantToFilterApiCall = false;
                  ratesViewController.callAPIGetListAllPackages();
                  ratesViewController.resetFilterDefault();
                  widget.resetFilterClicked();
                  // setState(() {
                  // });
                },
                child: Container(
                  padding: const EdgeInsets.symmetric(vertical: 8),
                  decoration: BoxDecoration(
                      color: AppColors.kPrimaryColor.withOpacity(0.2),
                      borderRadius: BorderRadius.circular(10)),
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 5.0),
                      child: Text(
                        'Reset',
                        style: TextStyles.filterDropdownButton,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: InkWell(
                  onTap: () {
                    ratesViewController.resetFilterDefault();
                    widget.closeFilterClicked();
                    ratesViewController.showFilter.value = 0;
                  },
                  child: SvgPicture.asset(
                    'assets/icons/closeEnclosed.svg',
                    color: Colors.black,
                  )),
            )
          ],
        ),
      ),
    );
  }

  Widget body() {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Bank",
              style: TextStyles.headingSubtitleStyle3,
            ),
            const BankDropDown(),

            Text(
              "Property Type",
              style: TextStyles.headingSubtitleStyle3,
            ),
            const PropertyTypeDropDown(),

            const SizedBox(
              height: 10,
            ),

            Text(
              "Property Status",
              style: TextStyles.headingSubtitleStyle3,
            ),
            const PropertyStatusDropDown(),

            const SizedBox(
              height: 10,
            ),

            Text(
              "Rate Type",
              style: TextStyles.headingSubtitleStyle3,
            ),
            const RateTypeDropDown(),

            /*const SizedBox(
              height: 10,
            ),
            const RateCategoryChip(),*/

            const SizedBox(
              height: 10,
            ),
            Text(
              "Loan Amount",
              style: TextStyles.headingSubtitleStyle3,
            ),
            const SizedBox(
              height: 10,
            ),

            CurrencyAmountTextField(
              textEditingController: ratesViewController.loanAmountController,
              labelText: "Loan Amount",
            ),
            const SizedBox(
              height: 10,
            ),
            //const LoanTypeChip(),

            Text(
              "Loan Type",
              style: TextStyles.headingSubtitleStyle3,
            ),
            const LoanTypeDropDown(),

            const SizedBox(
              height: 10,
            ),

            Obx(
              () => CheckBoxFilter(
                initValue: ratesViewController.is3year.value,
                textString:
                    "Do you have intention to sell your property within 3 years?",
                onChanged: (value) {
                  // print("value $value ");
                  ratesViewController.is3year.value = value;
                },
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Obx(
              () => CheckBoxFilter(
                initValue: ratesViewController.is200k.value,
                textString:
                    "Are you willing to place a minimum \$200k fresh funds with the bank to enjoy better rates?",
                onChanged: (value) {
                  // print("value $value ");
                  ratesViewController.is200k.value = value;
                },
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Center(
              child: PrimaryButton(
                  windowHeight: widget.windowHeight,
                  windowWidth: widget.windowWidth * 0.85,
                  buttonTitle: 'Filter',
                  onPressed: () {
                    debugPrint("callAPIGetBankListWithFilter PrimaryButton ");
                    // ratesViewController.resetFilter();//need reset 1st
                    ratesViewController.next = ""; //reset load more
                    ratesViewController.isWantToFilterApiCall =
                        true; //filter enable
                    ratesViewController.callAPIGetListAllPackages();
                  }),
            ),
            SizedBox(
              height: widget.windowHeight * 0.03,
            ),
            // Container(color: Colors.black38,height: 100,width: windowWidth,)
          ],
        ),
      ),
    );
  }
}

class RateTypeDropDown extends StatefulWidget {
  const RateTypeDropDown({Key? key}) : super(key: key);
  @override
  _RateTypeDropDownState createState() => _RateTypeDropDownState();
}

class _RateTypeDropDownState extends State<RateTypeDropDown> {
  List<ItemCheckBoxModel> dataSource = [];
  // var controller = GetControllers.shared.getRatesViewController();

  final RatesViewController generatePackagesController =
      GetControllers.shared.getRatesViewController();

  //GeneratePackagesController generatePackagesController = Get.find();

  @override
  void initState() {
    super.initState();
    loadData();
  }

  loadData() {
    dataSource.clear();
    var array = generatePackagesController.rateTypeDataSourceV2;
    debugPrint('array have ${array.length}');
    for (var i = 0; i < array.length; i++) {
      var item = array[i];
      dataSource.add(ItemCheckBoxModel(
          name: item.name, value: item.value, isChecked: item.isChecked));
    }
  }

  void _showMultiSelectRateType() async {
    debugPrint("rate type");
    debugPrint("Hello");
    await showDialog(
      context: context,
      builder: (BuildContext context) {
        // final TestingLogic controller = Get.put(TestingLogic());
        return MultiSelect(
            titleDropdown: "rate type aaaaaaa",
            didChoose: (list) {
              //! This function is called when we hit continue in multiselect dropdown
              generatePackagesController.rateTypeDataSourceV2 = list;
              generatePackagesController.getSelectedRateTypeDataSourceV2();
              setState(() {});
              //get selected item
            },
            dataSourceItemOutSide:
                generatePackagesController.rateTypeDataSourceV2);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 0),
      child: Column(
        children: [
          SizedBox(
            height: Get.height * 0.01,
          ),
          InkWell(
            onTap: () {
              _showMultiSelectRateType();
            },
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              height: 50,
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: AppColors.formFieldBorderColor),
                borderRadius: const BorderRadius.all(Radius.circular(10.0)),
              ),
              child: Row(
                children: const [
                  Icon(
                    Icons.search,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Select a Rate Type",
                    style: TextStyle(fontSize: 14, color: Color(0xFFA8A8A8)),
                  ),
                  Spacer(),
                  Icon(Icons.expand_more)
                ],
              ),
            ),
          ),
          Container(
            color: Colors.white,
            child: showChipList(),
            width: double.infinity,
          ),
          // showChipList()
          const SizedBox(
            height: 10,
          )
        ],
      ),
    );
  }

  showChipList() {
    //get array choosed

    return Wrap(
        alignment: WrapAlignment.start,
        spacing: 5,
        children: List<Widget>.generate(
          generatePackagesController.selectedRateTypeDataSourceV2.length,
          (int index) {
            return Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Chip(
                padding: const EdgeInsets.all(10),
                side: const BorderSide(color: AppColors.kPrimaryColor),
                backgroundColor: AppColors.kPrimaryColor,
                elevation: 1,
                labelStyle: const TextStyle(color: Colors.white),
                label: Text(generatePackagesController
                    .selectedRateTypeDataSourceV2[index].name),
              ),
            );
          },
        ).toList());
  }
}

class LoanTypeDropDown extends StatefulWidget {
  const LoanTypeDropDown({Key? key}) : super(key: key);
  @override
  _LoanTypeDropDownState createState() => _LoanTypeDropDownState();
}

class _LoanTypeDropDownState extends State<LoanTypeDropDown> {
  List<ItemCheckBoxModel> dataSource = [];
  // var controller = GetControllers.shared.getRatesViewController();

  final RatesViewController generatePackagesController =
      GetControllers.shared.getRatesViewController();

  //GeneratePackagesController generatePackagesController = Get.find();

  @override
  void initState() {
    super.initState();
    loadData();
  }

  loadData() {
    dataSource.clear();
    var array = generatePackagesController.loanTypeDataSourceV2;
    debugPrint('array have ${array.length}');
    for (var i = 0; i < array.length; i++) {
      var item = array[i];
      dataSource.add(ItemCheckBoxModel(
          name: item.name, value: item.value, isChecked: item.isChecked));
    }
  }

  void _showMultiSelectRateType() async {
    debugPrint("rate type");
    debugPrint("Hello");
    await showDialog(
      context: context,
      builder: (BuildContext context) {
        // final TestingLogic controller = Get.put(TestingLogic());
        return MultiSelect(
            titleDropdown: "rate type aaaaaaa",
            didChoose: (list) {
              //! This function is called when we hit continue in multiselect dropdown
              generatePackagesController.loanTypeDataSourceV2 = list;
              generatePackagesController.getSelectedLoanTypeDataSourceV2();
              setState(() {});
              //get selected item
            },
            dataSourceItemOutSide:
                generatePackagesController.loanTypeDataSourceV2);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 0),
      child: Column(
        children: [
          SizedBox(
            height: Get.height * 0.01,
          ),
          InkWell(
            onTap: () {
              _showMultiSelectRateType();
            },
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              height: 50,
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: AppColors.formFieldBorderColor),
                borderRadius: const BorderRadius.all(Radius.circular(10.0)),
              ),
              child: Row(
                children: const [
                  Icon(
                    Icons.search,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Select a Loan Type",
                    style: TextStyle(fontSize: 14, color: Color(0xFFA8A8A8)),
                  ),
                  Spacer(),
                  Icon(Icons.expand_more)
                ],
              ),
            ),
          ),
          Container(
            color: Colors.white,
            child: showChipList(),
            width: double.infinity,
          ),
          // showChipList()
          const SizedBox(
            height: 10,
          )
        ],
      ),
    );
  }

  showChipList() {
    //get array choosed

    return Wrap(
        alignment: WrapAlignment.start,
        spacing: 5,
        children: List<Widget>.generate(
          generatePackagesController.selectedLoanTypeDataSourceV2.length,
          (int index) {
            return Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Chip(
                padding: const EdgeInsets.all(10),
                side: const BorderSide(color: AppColors.kPrimaryColor),
                backgroundColor: AppColors.kPrimaryColor,
                elevation: 1,
                labelStyle: const TextStyle(color: Colors.white),
                label: Text(generatePackagesController
                    .selectedLoanTypeDataSourceV2[index].name),
              ),
            );
          },
        ).toList());
  }
}

class PropertyStatusDropDown extends StatefulWidget {
  const PropertyStatusDropDown({Key? key}) : super(key: key);
  @override
  _PropertyStatusDropDownState createState() => _PropertyStatusDropDownState();
}

class _PropertyStatusDropDownState extends State<PropertyStatusDropDown> {
  List<ItemCheckBoxModel> dataSource = [];
  // var controller = GetControllers.shared.getRatesViewController();

  final RatesViewController generatePackagesController =
      GetControllers.shared.getRatesViewController();

  //GeneratePackagesController generatePackagesController = Get.find();

  @override
  void initState() {
    super.initState();
    loadData();
  }

  loadData() {
    dataSource.clear();
    var array = generatePackagesController.propertyStatusDataSourceV2;
    debugPrint('array have ${array.length}');
    for (var i = 0; i < array.length; i++) {
      var item = array[i];
      dataSource.add(ItemCheckBoxModel(
          name: item.name, value: item.value, isChecked: item.isChecked));
    }
  }

  void _showMultiSelectRateType() async {
    debugPrint("rate type");
    debugPrint("Hello");
    await showDialog(
      context: context,
      builder: (BuildContext context) {
        // final TestingLogic controller = Get.put(TestingLogic());
        return MultiSelect(
            titleDropdown: "rate type aaaaaaa",
            didChoose: (list) {
              //! This function is called when we hit continue in multiselect dropdown
              generatePackagesController.propertyStatusDataSourceV2 = list;
              generatePackagesController
                  .getSelectedPropertyStatusDataSourceV2();
              setState(() {});
              //get selected item
            },
            dataSourceItemOutSide:
                generatePackagesController.propertyStatusDataSourceV2);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 0),
      child: Column(
        children: [
          SizedBox(
            height: Get.height * 0.01,
          ),
          InkWell(
            onTap: () {
              _showMultiSelectRateType();
            },
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              height: 50,
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: AppColors.formFieldBorderColor),
                borderRadius: const BorderRadius.all(Radius.circular(10.0)),
              ),
              child: Row(
                children: const [
                  Icon(
                    Icons.search,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Select a Property Status",
                    style: TextStyle(fontSize: 14, color: Color(0xFFA8A8A8)),
                  ),
                  Spacer(),
                  Icon(Icons.expand_more)
                ],
              ),
            ),
          ),
          Container(
            color: Colors.white,
            child: showChipList(),
            width: double.infinity,
          ),
          // showChipList()
          const SizedBox(
            height: 10,
          )
        ],
      ),
    );
  }

  showChipList() {
    //get array choosed

    return Wrap(
        alignment: WrapAlignment.start,
        spacing: 5,
        children: List<Widget>.generate(
          generatePackagesController.selectedPropertyStatusDataSourceV2.length,
          (int index) {
            return Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Chip(
                padding: const EdgeInsets.all(10),
                side: const BorderSide(color: AppColors.kPrimaryColor),
                backgroundColor: AppColors.kPrimaryColor,
                elevation: 1,
                labelStyle: const TextStyle(color: Colors.white),
                label: Text(generatePackagesController
                    .selectedPropertyStatusDataSourceV2[index].name),
              ),
            );
          },
        ).toList());
  }
}

class MultiSelectBank extends StatefulWidget {
  final String titleDropdown;
  final Function(List<ItemCheckBoxModel>) didChoose;
  // List<ItemCheckBoxModel> dataSourceItemOutSide;
  const MultiSelectBank(
      {Key? key,
      // required this.dataSourceItemOutSide,
      required this.didChoose,
      required this.titleDropdown})
      : super(key: key);
  @override
  State<StatefulWidget> createState() => _MultiSelectBankState();
}

class _MultiSelectBankState extends State<MultiSelectBank> {
  var dataSourceItem = <ItemCheckBoxModel>[];
  // var dataSourceCheck = <ItemCheckBoxModel>[];
  final RatesViewController ratesViewController =
      GetControllers.shared.getRatesViewController();
  // var dataSourceCheck = <ItemCheckBoxModel>[];
  final _filter = TextEditingController();
  String _searchText = "";
  var filteredDataSourceCheck = <ItemCheckBoxModel>[];

  setupSearchListener() {
    _filter.addListener(() {
      if (_filter.text.isEmpty) {
        setState(() {
          _searchText = "";
          filteredDataSourceCheck = dataSourceItem;
        });
      } else {
        setState(() {
          _searchText = _filter.text;
          // if (kDebugMode) {
          //   print(_searchText);
          // }
        });
      }
    });
  }

  @override
  void dispose() {
    _filter.dispose();
    super.dispose();
  }

  countTrue(List<ItemCheckBoxModel> datasource) {
    var count = 0;
    for (var i = 0; i < datasource.length; i++) {
      if (datasource[i].isChecked) count += 1;
    }
    if (kDebugMode) {
      print("count true $count");
    }
  }

  @override
  void initState() {
    super.initState();
    if (kDebugMode) {
      print("MultiSelectBank MultiSelect run init");
    }
    dataSourceItem.clear();
    filteredDataSourceCheck.clear();
    setupSearchListener();
    // for(var item in widget.dataSourceItemOutSide){
    //   ItemCheckBoxModel newItem =  ItemCheckBoxModel(
    //       name: item.name,
    //       value: item.value,
    //       isChecked: item.isChecked
    //   );
    //   dataSourceItem.add(newItem);
    //   dataSourceCheck.add(newItem);
    // }
    for (var i = 0; i < ratesViewController.bankDataSourceV2.length; i++) {
      var item = ratesViewController.bankDataSourceV2[i];
      ItemCheckBoxModel newItem = ItemCheckBoxModel(
          name: item.name, value: item.value, isChecked: item.isChecked);
      dataSourceItem.add(newItem);
      filteredDataSourceCheck.add(newItem);
    }
    //
    // for (var i = 0; i < widget.dataSourceItemOutSide.length; i++) {
    //   var link = widget.dataSourceItemOutSide[i];
    //   dataSourceOld.add(link);
    // }
    if (kDebugMode) {
      print("dataSourceItem after init from widget");
    }
    countTrue(dataSourceItem);
  }

  void _itemChange(String itemValue, bool isSelected, int index) {
    // dataSourceCheck[index].isChecked = isSelected;
    // setState(() {});
    filteredDataSourceCheck[index].isChecked = isSelected;
    setState(() {});
  }

  void _cancel() {
    _filter.clear();
    filteredDataSourceCheck = dataSourceItem;
    Navigator.pop(context);
  }

  void _submit() {
    _filter.clear();
    dataSourceItem = filteredDataSourceCheck;
    widget.didChoose(dataSourceItem);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    if (kDebugMode) {
      print("_MultiSelectState BuildContext clicked");
    }
    if (_searchText.isNotEmpty) {
      var tempList = <ItemCheckBoxModel>[];
      for (var item in filteredDataSourceCheck) {
        if (item.name.toLowerCase().contains(_searchText.toLowerCase())) {
          tempList.add(item);
        }
      }
      filteredDataSourceCheck = tempList;
    }

    return AlertDialog(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10))),
      // scrollable : true,//bo vao tach luon!
      // title: Text(widget.titleDropdown),
      title: TextField(
        controller: _filter,
        decoration: InputDecoration(
          prefixIcon: const Icon(Icons.search),
          hintText: 'Search',
          suffixIcon: IconButton(
            iconSize: 14,
            onPressed: _filter.clear,
            icon: const Icon(Icons.clear),
          ),
        ),
      ),
      content: SizedBox(
        width: double.maxFinite,
        // height: double.maxFinite,
        child: ListView.builder(
            shrinkWrap: true,
            itemCount: filteredDataSourceCheck.length,
            addAutomaticKeepAlives: true,
            itemBuilder: (context, index) {
              return CheckboxListTile(
                  value: (filteredDataSourceCheck[index].isChecked),
                  title: Text(filteredDataSourceCheck[index].name),
                  controlAffinity: ListTileControlAffinity.leading,
                  onChanged: (isChecked) {
                    _itemChange(
                        filteredDataSourceCheck[index].name, isChecked!, index);
                  });
            }),
      ),
      actions: [
        TextButton(
          child: const Text('Cancel'),
          onPressed: _cancel,
        ),
        TextButton(
          child: const Text('Confirm'),
          onPressed: _submit,
        ),
        // ElevatedButton(
        //   child: const Text('Submit'),
        //   onPressed: _submit,
        // ),
      ],
    );
  }
}
