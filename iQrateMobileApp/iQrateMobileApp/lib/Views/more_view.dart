import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/more_view_controller.dart';
import 'package:iqrate/Controller/profile_controller.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';

// ignore: must_be_immutable
class MoreView extends StatelessWidget {
  MoreView({Key? key}) : super(key: key);

  MoreViewController moreViewController = Get.put(MoreViewController());
  ProfileScreenController profileScreenController =
      Get.put(ProfileScreenController());

  @override
  Widget build(BuildContext context) {
    double windowheight = MediaQuery.of(context).size.height;
    return GridView.count(
      padding: EdgeInsets.all(windowheight * 0.02),
      crossAxisCount: 2,
      mainAxisSpacing: windowheight * 0.02,
      crossAxisSpacing: windowheight * 0.02,
      children: [
        GridItem(
          assetImage: 'assets/images/profile.svg',
          title: 'Profile',
          subTitle: '',
          onTap: () {
            moreViewController.onTapProfile();
          },
        ),
        GridItem(
          assetImage: 'assets/images/subscription.svg',
          title: 'Subscription',
          subTitle: '',
          onTap: () {
            moreViewController.onTapSubscription();
          },
        ),
        GridItem(
          assetImage: 'assets/images/training.svg',
          title: 'Training',
          subTitle: '',
          onTap: () {
            moreViewController.onTapTraining();
          },
        ),
        GridItem(
          assetImage: 'assets/images/resource.svg',
          title: 'Resource',
          subTitle: '',
          onTap: () {
            moreViewController.onTapResources();
          },
        ),
        GridItem(
          assetImage: 'assets/images/faq.svg',
          title: 'FAQ',
          subTitle: '',
          onTap: () {
            moreViewController.onTapFAQ();
          },
        ),
        GridItem(
          assetImage: 'assets/images/contactUs.svg',
          title: 'Contact Us',
          subTitle: '',
          onTap: () {
            moreViewController.onTapContactUs();
          },
        ),
        GridItem(
          assetImage: 'assets/images/logout.svg',
          title: 'Logout',
          subTitle: '',
          onTap: () {
            moreViewController.onTapLogout();
          },
        ),
      ],
    );
  }
}

class GridItem extends StatelessWidget {
  const GridItem(
      {Key? key,
      required this.assetImage,
      required this.title,
      this.onTap,
      this.subTitle})
      : super(key: key);
  final String assetImage;
  final String title;
  final String? subTitle;
  final Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Card(
        elevation: 4,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset(assetImage),
            Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    title,
                    style: TextStyles.cardTitle,
                    textAlign: TextAlign.center,
                  ),
                  Text(
                    subTitle!,
                    style: TextStyles.cardSubTitle,
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}


class GridItemWithoutSubText extends StatelessWidget {
  const GridItemWithoutSubText(
      {Key? key,
        required this.assetImage,
        required this.title,
        this.onTap,})
      : super(key: key);
  final String assetImage;
  final String title;
  final Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Card(
        elevation: 4,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(height: 5,),
            SvgPicture.asset(assetImage),
            Padding(
              padding: const EdgeInsets.all(5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    title,
                    style: TextStyles.cardTitle2,
                    textAlign: TextAlign.center,
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}