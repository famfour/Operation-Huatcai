// ignore_for_file: must_be_immutable,

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/instance_manager.dart';
import 'package:iqrate/Controller/others_links_view_controller.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Widgets/default_appbar.dart';

class OthersView extends StatelessWidget {
  OthersView({Key? key}) : super(key: key);

  OthersLinkViewController othersLinkViewController =
      Get.put(OthersLinkViewController());
  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: DefaultAppBar(
        title: "Others",
        windowHeight: windowHeight * 0.09,
      ),
      body: Padding(
          padding: const EdgeInsets.symmetric(vertical: 25.0, horizontal: 20),
          child: Obx(
            () => othersLinkViewController.othersLinkResponseModel.value.data !=
                    null
                ? ListView.separated(
                    itemBuilder: (BuildContext conrtext, int index) {
                      return TitleAndLink(
                        link: othersLinkViewController.othersLinkResponseModel
                                    .value.data![index].document !=
                                null
                            ? "https://iqrate-strapi.dprizm.dev" +
                                othersLinkViewController.othersLinkResponseModel
                                    .value.data![index].document![0]!.url
                                    .toString()
                            : othersLinkViewController.othersLinkResponseModel
                                .value.data![index].link,
                        title: othersLinkViewController
                            .othersLinkResponseModel.value.data![index].name,
                        isLinkAUrl: othersLinkViewController
                                .othersLinkResponseModel
                                .value
                                .data![index]
                                .document ==
                            null,
                      );
                    },
                    separatorBuilder: (BuildContext context, int i) {
                      return Padding(
                        padding: EdgeInsets.all(windowHeight * 0.015),
                        child: const Divider(
                          color: Colors.grey,
                        ),
                      );
                    },
                    itemCount: othersLinkViewController
                        .othersLinkResponseModel.value.data!.length)
                : Container(),
          )),
    );
  }
}

class TitleAndLink extends StatelessWidget {
  TitleAndLink(
      {Key? key,
      required this.title,
      required this.link,
      required this.isLinkAUrl})
      : super(key: key);
  final String? title;
  final String? link;
  final bool isLinkAUrl;
  OthersLinkViewController othersLinkViewController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        title != null
            ? Expanded(
                flex: 9,
                child: Text(
                  title!,
                  style: TextStyles.faqDescriptionTextStyle,
                  overflow: TextOverflow.visible,
                ),
              )
            : Text(
                '',
                style: TextStyles.faqDescriptionTextStyle,
              ),
        Expanded(
          flex: 1,
          child: Center(
            child: isLinkAUrl
                ? InkWell(
                    onTap: () {
                      othersLinkViewController.launchUrl(link);
                    },
                    child: SvgPicture.asset('assets/icons/link.svg'))
                : InkWell(
                    onTap: () {
                      // Download pdf function
                      othersLinkViewController.onDownloadPdfButtonTap(link!);
                    },
                    child: SvgPicture.asset('assets/icons/fileIcon.svg')),
          ),
        )
      ],
    );
  }
}
