import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/DeviceManager/container_styles.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Model/response_model.dart/bank_forms_response_model.dart';
import 'package:iqrate/Widgets/default_appbar.dart';
import 'package:iqrate/Controller/bank_forms_view_controller.dart';
import 'package:iqrate/Widgets/secondary_button.dart';

// ignore: must_be_immutable
class BankFormsView extends StatelessWidget {
  BankFormsView({Key? key}) : super(key: key);

  BankFormsViewController bankFormsViewController =
      Get.put(BankFormsViewController());

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: DefaultAppBar(
        title: "Bank Forms",
        windowHeight: windowHeight * 0.09,
      ),
      body: Stack(
        children: [
          Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 25.0, horizontal: 20),
              child: Obx(() {
                debugPrint(
                    bankFormsViewController.bankForms.isNotEmpty.toString());
                return bankFormsViewController.bankForms.isNotEmpty
                    ? ListView.separated(
                        physics: const AlwaysScrollableScrollPhysics(),
                        controller: bankFormsViewController.scrollController,
                        itemBuilder: (BuildContext conrtext, int index) {
                          return bankFormsViewController
                                      .bankForms[index].bankFormUrls !=
                                  null
                              ? TitleAndButton(
                                  title: bankFormsViewController
                                      .bankForms[index].name!,
                                  onPressed: () {
                                    debugPrint(index.toString());
                                    bankFormsViewController.fileIndex = index;
                                    bankFormsViewController
                                        .showFormsList.value = true;
                                  },
                                )
                              : Container();
                        },
                        separatorBuilder: (BuildContext context, int i) {
                          return bankFormsViewController
                                      .bankForms[i].bankFormUrls !=
                                  null
                              ? Padding(
                                  padding: EdgeInsets.all(windowHeight * 0.015),
                                  child: const Divider(
                                    color: Colors.grey,
                                  ),
                                )
                              : Container();
                        },
                        itemCount: bankFormsViewController.bankForms.length)
                    : Container();
              })),
          Obx(
            () => bankFormsViewController.showFormsList.value == true
                ? formsList(
                    windowHeight,
                    windowWidth,
                    bankFormsViewController.bankForms,
                    bankFormsViewController.fileIndex)
                : const Offstage(),
          ),
        ],
      ),
    );
  }

  formsList(double windowHeight, double windowWidth,
      RxList<OneBankForm> bankForms, index) {
    return Container(
      margin: EdgeInsets.all(windowHeight * 0.03),
      padding: const EdgeInsets.fromLTRB(20, 60, 20, 20),
      decoration: BoxDecoration(boxShadow: const [
        BoxShadow(
            offset: Offset(5, 5),
            color: Colors.grey,
            spreadRadius: 10,
            blurRadius: 30)
      ], color: Colors.white, borderRadius: BorderRadius.circular(10)),
      height: windowHeight * 0.65,
      child: Column(
        children: [
          SizedBox(
            height: windowHeight * 0.4,
            child: ListView.separated(
              itemCount: bankForms[index].bankFormUrls!.length,
              itemBuilder: (context, i) {
                return TitleAndLink(
                  title: bankForms[index].bankFormUrls![i].name!,
                  link: bankForms[index].bankFormUrls![i].url,
                  isLinkAUrl: false,
                );
              },
              separatorBuilder: (BuildContext context, int index) {
                return bankFormsViewController.bankForms[index].bankFormUrls !=
                        null
                    ? Padding(
                        padding: EdgeInsets.all(windowHeight * 0.015),
                        child: const Divider(
                          color: Colors.grey,
                        ),
                      )
                    : Container();
              },
            ),
          ),
          const Spacer(),
          SecondaryButton(
              windowHeight: windowHeight,
              windowWidth: windowWidth,
              kGradientBoxDecoration:
                  ContainerStyles.kGradientBoxDecorationSecondaryButton,
              kInnerDecoration: ContainerStyles.kInnerDecorationSecondaryButton,
              onPressed: () {
                bankFormsViewController.showFormsList.value = false;
              },
              buttonTitle: 'Done')
        ],
      ),
    );
  }
}

// ignore: must_be_immutable
class TitleAndButton extends StatelessWidget {
  TitleAndButton({Key? key, required this.title, required this.onPressed})
      : super(key: key);
  final String title;
  Function()? onPressed;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: Text(
            title,
            style: TextStyles.faqDescriptionTextStyle,
            overflow: TextOverflow.visible,
          ),
        ),
        IconButton(
            onPressed: onPressed,
            icon: const Icon(
              Icons.sim_card_download_outlined,
              color: Colors.blue,
            ))
      ],
    );
  }
}

// ignore: must_be_immutable
class TitleAndLink extends StatelessWidget {
  TitleAndLink(
      {Key? key,
      required this.title,
      required this.link,
      this.isLinkAUrl = true})
      : super(key: key);
  final String title;
  final String? link;
  final bool isLinkAUrl;

  BankFormsViewController bankFormsViewController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: Text(
            title,
            style: TextStyles.faqDescriptionTextStyle,
            overflow: TextOverflow.visible,
          ),
        ),
        isLinkAUrl
            ? SvgPicture.asset('assets/icons/link.svg')
            : InkWell(
                onTap: () {
                  if (link != null) {
                    bankFormsViewController.onDownloadPdfButtonTap(link!);
                  } else {
                    Future.delayed(const Duration(milliseconds: 0), () {
                      FocusManager.instance.primaryFocus!.unfocus();
                      Fluttertoast.showToast(
                        timeInSecForIosWeb: 3,
                        msg: "File unavailable",
                        backgroundColor: Colors.red,
                        toastLength: Toast.LENGTH_LONG,
                        gravity: ToastGravity.BOTTOM,
                        textColor: Colors.white,
                        fontSize: 20.0,
                      );
                    });
                  }
                },
                child: SvgPicture.asset(
                  'assets/icons/fileIcon.svg',
                  color: link == null ? Colors.grey.withOpacity(0.5) : null,
                ))
      ],
    );
  }
}
