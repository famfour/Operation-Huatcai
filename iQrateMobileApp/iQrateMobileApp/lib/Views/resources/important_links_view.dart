import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/important_links_view_controller.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Widgets/default_appbar.dart';

// ignore: must_be_immutable
class ImportantLinksView extends StatelessWidget {
  ImportantLinksView({Key? key}) : super(key: key);

  ImportantLinksViewController importantLinksViewController =
      Get.put(ImportantLinksViewController());

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: DefaultAppBar(
        title: "Important Links",
        windowHeight: windowHeight * 0.09,
      ),
      body: Padding(
          padding: const EdgeInsets.symmetric(vertical: 25.0, horizontal: 10),
          child: Obx(
            () => importantLinksViewController
                        .importantLinksResponseModel.value.data !=
                    null
                ? ListView.separated(
                    itemBuilder: (BuildContext conrtext, int index) {
                      return TitleAndLink(
                        link: importantLinksViewController
                                    .importantLinksResponseModel
                                    .value
                                    .data![index]
                                    .document ==
                                null
                            ? importantLinksViewController
                                .importantLinksResponseModel
                                .value
                                .data![index]
                                .link
                            : "https://iqrate-strapi.dprizm.dev" +
                                importantLinksViewController
                                    .importantLinksResponseModel
                                    .value
                                    .data![index]
                                    .document![0]!
                                    .url
                                    .toString(),
                        title: importantLinksViewController
                            .importantLinksResponseModel
                            .value
                            .data![index]
                            .name,
                        isLinkAUrl: importantLinksViewController
                                .importantLinksResponseModel
                                .value
                                .data![index]
                                .document ==
                            null,
                      );
                    },
                    separatorBuilder: (BuildContext context, int i) {
                      return Padding(
                        padding: EdgeInsets.all(windowHeight * 0.015),
                        child: const Divider(
                          color: Colors.grey,
                        ),
                      );
                    },
                    itemCount: importantLinksViewController
                        .importantLinksResponseModel.value.data!.length)
                : Container(),
          )),
    );
  }
}

// ignore: must_be_immutable
class TitleAndLink extends StatelessWidget {
  TitleAndLink(
      {Key? key,
      required this.title,
      required this.link,
      required this.isLinkAUrl})
      : super(key: key);
  final String? title;
  final String? link;
  final bool isLinkAUrl;
  ImportantLinksViewController importantLinksViewController = Get.find();
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        title != null
            ? Expanded(
                flex: 9,
                child: Padding(
                  padding: const EdgeInsets.only(left: 15.0),
                  child: Text(
                    title!,
                    style: TextStyles.faqDescriptionTextStyle,
                    overflow: TextOverflow.visible,
                  ),
                ),
              )
            : Text(
                '',
                style: TextStyles.faqDescriptionTextStyle,
              ),
        Expanded(
          flex: 1,
          child: isLinkAUrl
              ? InkWell(
                  onTap: () {
                    importantLinksViewController.launchUrl(link);
                  },
                  child: SvgPicture.asset('assets/icons/link.svg'))
              : InkWell(
                  onTap: () {
                    // Download file function
                    importantLinksViewController.onDownloadPdfButtonTap(link!);
                  },
                  child: SvgPicture.asset('assets/icons/fileIcon.svg')),
        )
      ],
    );
  }
}
