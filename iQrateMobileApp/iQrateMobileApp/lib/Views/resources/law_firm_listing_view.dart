import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/lawfirm_listing_view_controller.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Model/response_model.dart/lawfirm_listing_response_model.dart';
import 'package:iqrate/Widgets/default_appbar.dart';

// ignore: must_be_immutable
class LawFirmListingScreen extends StatelessWidget {
  LawFirmListingScreen({Key? key}) : super(key: key);

  LawfirmListingViewController lawfirmListingViewController =
      Get.put(LawfirmListingViewController());

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: DefaultAppBar(
        title: "Law Firm Listing",
        windowHeight: windowHeight * 0.09,
      ),
      body: Padding(
          padding: const EdgeInsets.symmetric(vertical: 25.0, horizontal: 20),
          child: Obx(
            () => lawfirmListingViewController
                        .lawFirmListsResponseModel.value.count !=
                    null
                ? ListView.separated(
                    controller: lawfirmListingViewController.scrollController,
                    physics: const AlwaysScrollableScrollPhysics(),
                    itemBuilder: (BuildContext conrtext, int index) {
                      return Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: TitleAndicon(
                          title: lawfirmListingViewController
                              .lawfirmList[index].name!,
                          products: lawfirmListingViewController
                              .lawfirmList[index].productList!,
                          oneResult:
                              lawfirmListingViewController.lawfirmList[index],
                        ),
                      );
                    },
                    separatorBuilder: (BuildContext context, int i) {
                      return Padding(
                        padding: EdgeInsets.all(windowHeight * 0.005),
                        child: const Divider(
                          color: Colors.grey,
                        ),
                      );
                    },
                    itemCount: lawfirmListingViewController.lawfirmList.length)
                : Container(),
          )),
    );
  }
}

// ignore: must_be_immutable
class TitleAndicon extends StatelessWidget {
  TitleAndicon(
      {Key? key,
      required this.title,
      required this.products,
      required this.oneResult})
      : super(key: key);
  final String title;
  final List<OneProduct> products;
  final OneResult oneResult;

  LawfirmListingViewController lawfirmListingViewController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: Text(
            title,
            style: TextStyles.faqDescriptionTextStyle,
            overflow: TextOverflow.visible,
          ),
        ),
        IconButton(
            onPressed: () {
              lawfirmListingViewController.onTapOneLawFirm(
                  productList: products, oneResult: oneResult);
            },
            icon: const Icon(Icons.chevron_right, color: AppColors.black1D232A))
      ],
    );
  }
}
