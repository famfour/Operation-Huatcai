// ignore_for_file: unused_element

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/lawfirm_listing_details_view_controller.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Model/response_model.dart/lawfirm_listing_response_model.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

// ignore: must_be_immutable
class LawFirmListingDetailsView extends StatelessWidget {
  LawFirmListingDetailsView({Key? key}) : super(key: key);
  late double windowHeight;

  LawFirmListingDetailsViewController lawFirmListingDetailsViewController =
      Get.put(LawFirmListingDetailsViewController());

  @override
  Widget build(BuildContext context) {
    windowHeight = MediaQuery.of(context).size.height;
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.all(10.0),
          child: ListView(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  InkWell(
                    onTap: () {
                      Get.back();
                    },
                    child: SvgPicture.asset(
                      'assets/icons/closeEnclosed.svg',
                      color: Colors.black,
                    ),
                  )
                ],
              ),
              SizedBox(height: windowHeight * 0.02),
              ContentWidget(
                productList: lawFirmListingDetailsViewController.productList,
                oneProduct:
                    lawFirmListingDetailsViewController.oneProduct.value,
              )
            ],
          ),
        ),
      ),
    );
  }
}

// ignore: must_be_immutable
class ContentWidget extends StatelessWidget {
  ContentWidget({Key? key, required this.productList, required this.oneProduct})
      : super(key: key);

  List<OneProduct> productList;
  OneResult oneProduct;
  var formatter = NumberFormat.currency(locale: 'en_US', symbol: '\$');

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Text(
          oneProduct.name.toString(),
          style: TextStyles.bigHeadingTextStyleLawFirm,
        ),
        const SizedBox(
          height: 5,
        ),
        InkWell(
          onTap: () {
            emailAppOpen(oneProduct.email.toString(), "subject", "emailBody");
          },
          child: Row(
            children: [
              const Icon(Icons.email_outlined),
              const SizedBox(
                width: 5,
              ),
              Text(
                oneProduct.email.toString(),
                style: TextStyles.bodyTextStyUnderLinedleLawFirm,
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        productList.isNotEmpty
            ? Container(
                decoration: const BoxDecoration(
                    border: Border(
                        left: BorderSide(color: Colors.grey),
                        right: BorderSide(color: Colors.grey),
                        top: BorderSide(color: Colors.grey),
                        bottom: BorderSide(color: Colors.grey))),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'Products',
                          style: TextStyles.headingTextStyleLawFirm,
                        ),
                      ),
                    ),
                    const Divider(
                      color: Colors.grey,
                    ),
                    ListView.separated(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemBuilder: (BuildContext context, int index) {
                          return Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 15.0),
                            child: OneProductDetails(
                              typeOfLoan: productList[index]
                                  .loanCategory!,
                              propertyType: productList[index]
                                  .propertyType!,
                              legalFees: formatter
                                  .format(productList[index].legalFees)
                                  .toString(),
                              purchasePriceRangeEnd: formatter
                                  .format(productList[index].loanRangesTo)
                                  .toString(),
                              purchasePriceRangeStart: formatter
                                  .format(productList[index].loanRangesFrom)
                                  .toString(),
                            ),
                          );
                        },
                        separatorBuilder: (BuildContext context, int index) {
                          return const Divider(
                            color: Colors.grey,
                          );
                        },
                        itemCount: productList.length)
                  ],
                ),
              )
            : Center(
                child: Text(
                'No product to show',
                style: TextStyles.bigHeadingTextStyleLawFirm,
              ))
      ],
    );
  }

  /*String emailBody = "Dear BR LAW \nOur mortgage broker James Tan 97836265 is engaging your firm’s service for our client. \n\nClient(s) Details :\nName of Lead: Homer Simple \nLead’s contact no: 8888 8888 \nLead’s email address: homer@mail.com \nLegal Fee : \$1500\n\nPlease do not hesitate to contact your mortgage broker if you have any queries. Have a great day ahead.\n\nThank you.\n\nFrom IQrate Admin\n(Please DO NOT reply to the email)";
  String subject = "Processing of Loan for Frank";
  String to = "brlaw@brlaw.com";*/

  Future<void> emailAppOpen(String to, String subject, String emailBody) async {
    String? encodeQueryParameters(Map<String, String> params) {
      return params.entries
          .map((e) =>
              '${Uri.encodeComponent(e.key)}=${Uri.encodeComponent(e.value)}')
          .join('&');
    }

    final Uri emailLaunchUri = Uri(
      scheme: 'mailto',
      path: to,
      /*query: encodeQueryParameters(<String, String>{
        'subject': subject,
        'body': emailBody,
      }),*/
    );

    await launchUrl(emailLaunchUri);
  }
}

class OneProductDetails extends StatelessWidget {
  const OneProductDetails(
      {Key? key,
      required this.typeOfLoan,
      required this.propertyType,
      required this.purchasePriceRangeStart,
      required this.purchasePriceRangeEnd,
      required this.legalFees})
      : super(key: key);
  final List<dynamic> typeOfLoan;
  final List<dynamic> propertyType;
  final String purchasePriceRangeStart;
  final String purchasePriceRangeEnd;
  final String legalFees;

  @override
  Widget build(BuildContext context) {

    String loanTypes ="";
    String propertyTypes ="";

    for (var element in typeOfLoan) {
      loanTypes +=element.replaceAll('_', ' ')
        .toString()
        .capitalizeFirst!+", ";
    }

    for (var element in propertyType) {
      propertyTypes +=element.replaceAll('_', ' ')
          .toString()
          .capitalizeFirst!+", ";
    }

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                flex: 2,
                child: Text(
                  'Type of loan: ',
                  style: TextStyles.bodyTextStyleLawFirm,
                ),
              ),
              Expanded(
                flex: 5,
                child: Text(
                  loanTypes,
                  //maxLines: 1,
                  //overflow: TextOverflow.ellipsis,
                  style: TextStyles.bodyTextStyleLawFirm,
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: Text(
                  'Property Type: ',
                  style: TextStyles.bodyTextStyleLawFirm,
                ),
              ),
              Expanded(
                flex: 5,
                child: Text(
                  propertyTypes,
                  style: TextStyles.bodyTextStyleLawFirm,
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: Text(
                  'Purchase price: ',
                  style: TextStyles.bodyTextStyleLawFirm,
                ),
              ),
              Expanded(
                flex: 5,
                child: Text(
                  purchasePriceRangeStart + ' to ' + purchasePriceRangeEnd,
                  style: TextStyles.bodyTextStyleLawFirm,
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: Text(
                  'Legal fees: ',
                  style: TextStyles.bodyTextStyleLawFirm,
                ),
              ),
              Expanded(
                flex: 5,
                child: Text(
                  legalFees,
                  style: TextStyles.bodyTextStyleLawFirm,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
