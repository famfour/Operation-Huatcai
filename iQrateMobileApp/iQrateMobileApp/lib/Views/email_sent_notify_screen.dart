import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Widgets/primary_button.dart';

class EmailSentNotifyScreen extends StatelessWidget {
  const EmailSentNotifyScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double windowWidth = MediaQuery.of(context).size.width;
    double windowHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: windowHeight * 0.1,
                ),
                Center(
                  child: SvgPicture.asset(
                    "assets/images/verify_email.svg",
                  ),
                ),
                SizedBox(
                  height: windowHeight * 0.09,
                ),
                Text("Please verify your email address",
                    style: TextStyles.introScreenTitlesSmall),
                SizedBox(
                  height: windowHeight * 0.03,
                ),
                Text(
                  "An email has been sent to your registered email address. Please click and verify.",
                  style: TextStyles.introScreenDescriptions,
                ),
                SizedBox(
                  height: windowHeight * 0.08,
                ),
                SizedBox(
                  height: windowHeight * 0.08,
                ),
                PrimaryButton(
                  windowHeight: windowHeight,
                  windowWidth: windowWidth,
                  buttonTitle: "Continue",
                  onPressed: () async {
                    // Navigate to login page
                    Get.toNamed(loginScreen);
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
