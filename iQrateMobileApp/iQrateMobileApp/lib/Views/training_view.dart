import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Widgets/default_appbar.dart';

// ignore: must_be_immutable
class TrainingView extends StatelessWidget {
  const TrainingView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: DefaultAppBar(
        title: 'Training',
        windowHeight: windowHeight * 0.09,
      ),
      body: ListView(
        children: [
          TrainingCards(
            heading: 'Basic Modules',
            content: 'Learn how to perform loan submission.',
            imageAsset: 'assets/images/trainingBasicModule.svg',
            onTap: () {
              Get.toNamed(basicModulesTrainingScreen,
                  arguments: ['BasicModules']);
            },
          ),
          TrainingCards(
            heading: 'Application Demo',
            content: 'Watch the application demo here.',
            imageAsset: 'assets/images/trainingApplicationDemo.svg',
            onTap: () {
              Get.toNamed(applicationDemoTrainingScreen,
                  arguments: ['ApplicationDemo']);
            },
          ),
          TrainingCards(
            heading: 'User Guide',
            content: 'View step-by-step guide on each key features.',
            imageAsset: 'assets/images/trainingUserGuide.svg',
            onTap: () {
              Get.toNamed(userGuideTrainingScreen, arguments: ['UserGuide']);
            },
          )
        ],
      ),
    );
  }
}

class TrainingCards extends StatelessWidget {
  const TrainingCards(
      {Key? key,
      required this.heading,
      required this.content,
      required this.imageAsset,
      required this.onTap})
      : super(key: key);
  final String heading;
  final String content;
  final String imageAsset;
  final void Function() onTap;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 7.5),
      child: GestureDetector(
        onTap: onTap,
        child: Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
          elevation: 3,
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 15.0),
            child: Row(
              children: [
                Expanded(
                    flex: 3,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: SvgPicture.asset(imageAsset),
                    )),
                Expanded(
                  flex: 7,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 15.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          heading,
                          style: TextStyles.cardTitle,
                        ),
                        const SizedBox(height: 10),
                        Text(
                          content,
                          style: TextStyles.cardContentTextStyle,
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
