// ignore_for_file: unused_local_variable

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Service/GetControllers.dart';
import 'package:iqrate/Utils/text_formatter.dart';
import 'package:iqrate/Controller/buc_mortgage_progressive_repayment_table_screen_controller.dart';
import 'package:iqrate/Widgets/default_appbar.dart';
import '../../DeviceManager/colors.dart';
import '../../DeviceManager/text_styles.dart';
import '../../Widgets/primary_button.dart';

class BucProgressiveRepaymentTableScreen extends StatefulWidget {
  const BucProgressiveRepaymentTableScreen({Key? key}) : super(key: key);

  @override
  State<BucProgressiveRepaymentTableScreen> createState() =>
      _BucProgressiveRepaymentTableScreenState();
}

class _BucProgressiveRepaymentTableScreenState
    extends State<BucProgressiveRepaymentTableScreen> {
  BucMortgageProgressiveRepaymentTableScreenController controller =
      GetControllers.shared
          .getBucMortgageProgressiveRepaymentTableScreenController();

  //Disbursement Schedule

  // int activeStep = 0; // Initial step set to 0.
  late double windowHeight;
  late double windowWidth;

  @override
  void initState() {
    super.initState();
    controller.getData();
    // controller.arrayLoanDetail.value = getArrayContentLoanDetails();
  }

  @override
  Widget build(BuildContext context) {
    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: DefaultAppBar(
          title: "BUC Mortgage Repayment", windowHeight: windowHeight * 0.09),
      body: InteractiveViewer(
        minScale: 1,
        maxScale: 4,
        child: GestureDetector(
          onTap: () {
            FocusScopeNode currentFocus = FocusScope.of(context);

            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
          },
          child: SingleChildScrollView(
            child: Column(
              children: [
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 20, top: 20, bottom: 20, right: 20),
                    child: Text(
                      "Progressive Repayment Table",
                      style: TextStyles.leadsColorTextStyle2
                          .copyWith(color: AppColors.kPrimaryColor),
                    ),
                  ),
                  bodyTable(),
                  const SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Center(
                      child: PrimaryButton(
                        windowHeight: windowHeight,
                        windowWidth: windowWidth,
                        buttonTitle: "Close",
                        onPressed: () {
                          Get.back();
                          //newPurchaseController.onTapBackStep3Lead();
                        },
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  )
                ]),
              ],
            ),
          ),
        ),
      ),
    );
  }

  bodyTable() {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Container(
        color: Colors.grey[100],
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            //row 1
            IntrinsicHeight(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                      child: Padding(
                    padding: const EdgeInsets.all(1),
                    child: Container(
                      color: AppColors.white,
                      child: Padding(
                        padding: const EdgeInsets.all(0),
                        child: Center(
                          child: Text(
                            "Disbursement\nStage",
                            style: TextStyles.leadsColorTextStyle2.copyWith(
                                color: AppColors.kPrimaryColor, fontSize: 8),
                          ),
                        ),
                      ),
                    ),
                  )),
                  // Container(color: AppColors.white,child: he),
                  Expanded(
                      child: Padding(
                    padding: const EdgeInsets.all(1),
                    child: Container(
                      color: AppColors.white,
                      child: Padding(
                        padding: const EdgeInsets.all(0),
                        child: Center(
                          child: Text(
                            "Disbursement \nRange",
                            style: TextStyles.leadsColorTextStyle2.copyWith(
                                color: AppColors.kPrimaryColor, fontSize: 8),
                          ),
                        ),
                      ),
                    ),
                  )),

                  Padding(
                    padding: const EdgeInsets.all(1),
                    child: Container(
                      width: windowWidth / 7 * 0.5,
                      color: AppColors.white,
                      child: Padding(
                        padding: const EdgeInsets.all(0),
                        child: Center(
                          child: Text(
                            "%",
                            style: TextStyles.leadsColorTextStyle2.copyWith(
                                color: AppColors.kPrimaryColor, fontSize: 8),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                      child: Padding(
                    padding: const EdgeInsets.all(1),
                    child: Container(
                      color: AppColors.white,
                      child: Center(
                        child: Text(
                          "Disbursement \nAmount",
                          style: TextStyles.leadsColorTextStyle2.copyWith(
                              color: AppColors.kPrimaryColor, fontSize: 8),
                        ),
                      ),
                    ),
                  )),
                  Expanded(
                      child: Padding(
                    padding: const EdgeInsets.all(1),
                    child: Container(
                      color: AppColors.white,
                      child: Center(
                        child: Text(
                          "Payment\nMode",
                          style: TextStyles.leadsColorTextStyle2.copyWith(
                              color: AppColors.kPrimaryColor, fontSize: 8),
                        ),
                      ),
                    ),
                  )),
                  Expanded(
                      child: Padding(
                    padding: const EdgeInsets.all(1),
                    child: Container(
                      color: AppColors.white,
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Center(
                          child: Text(
                            "Monthly Installment (IQrate Exclusive BUC rate)",
                            style: TextStyles.leadsColorTextStyle2.copyWith(
                                color: AppColors.kPrimaryColor, fontSize: 8),
                          ),
                        ),
                      ),
                    ),
                  )),
                  // check have interest rate

                  if (GetControllers.shared
                      .getBucMortgageRepaymentController()
                      .prefered_rate_enable)
                    Expanded(
                        child: Padding(
                      padding: const EdgeInsets.all(1),
                      child: Container(
                        color: AppColors.white,
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: Center(
                            child: Text(
                              "Monthly Installment (Your Preferred Interest Rate)",
                              style: TextStyles.leadsColorTextStyle2.copyWith(
                                  color: AppColors.kPrimaryColor, fontSize: 8),
                            ),
                          ),
                        ),
                      ),
                    )),
                ],
              ),
            ),
            buildListGetRowValueAtIndex()
          ],
        ),
      ),
    );
  }

  Widget buildListGetRowValueAtIndex() => Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          for (var i = 0; i < 10; i++) getRowValueatIndex(i),
        ],
      );

  headerTitle(String title) {
    return Expanded(
      flex: 1,
      child: Padding(
        padding: const EdgeInsets.all(1),
        child: Container(
          height: 40,
          color: AppColors.white,
          child: Align(
            alignment: Alignment.centerLeft,
            child: Padding(
                padding: const EdgeInsets.only(left: 5),
                child:
                    // Text(
                    //   title,
                    //   style: TextStyles.leadsTextStyle1
                    //       .copyWith(color: AppColors.kPrimaryColor, fontSize: 8),
                    //   maxLines: 3,
                    // ),
                    AutoSizeText(
                  title,
                  style: TextStyles.leadsTextStyle1
                      .copyWith(color: AppColors.kPrimaryColor, fontSize: 8),
                  maxLines: 3,
                  minFontSize: 7,
                  maxFontSize: 10,
                )),
          ),
        ),
      ),
    );
  }

  getRowValueatIndex(int index) {
    var getMonthlyPrefered = "";
    var aa = controller.disbursementSchedule[index]
        ['Monthly Installment based on Prefered'];
    // print("item thu $index co payment mode is ${a is List}");
    if (aa is List) {
      for (var i = 0; i < aa.length; i++) {
        var b = aa[i].toString();
        getMonthlyPrefered =
            getMonthlyPrefered + "\n" + convertToCurrency(b) + "\n";
      }
    } else {
      getMonthlyPrefered = convertToCurrency(aa);
    }

    var getPaymentMode = "";
    var ar = <String>[];
    var ab = controller.disbursementSchedule[index]['Disbursement Name'];
    for (var i = 0; i < ab.length; i++) {
      // var b = convertToCurrency(ar[i]);
      var b = (ab[i]);
      getPaymentMode = getPaymentMode + "\n" + b + "\n";
    }

    // var getDisbursementAmount = "";
    // var c = controller.disbursementSchedule[index]['Payment Mode'];
    // // print("item thu $index co payment mode is ${a is List}");
    // if (c is List) {
    //   for (var i = 0; i < c.length; i++) {
    //     var b = c[i];
    //     getDisbursementAmount =
    //         getDisbursementAmount + "\n" + convertToCurrency(b) + "\n";
    //   }
    // } else {
    //   getDisbursementAmount = convertToCurrency(c);
    // }

    var getDisbursementAmount = "";
    var br = <String>[];

    var c = controller.disbursementSchedule[index]['Payment Mode'];
    // print("item thu $index co payment mode is ${a is List}");
    c.forEach((key, value) {
      br.add(value.toString());
    });
    if (br.length < 2) {
      getDisbursementAmount = convertToCurrency(br.first);
    } else {
      for (var i = 0; i < br.length; i++) {
        getDisbursementAmount =
            getDisbursementAmount + "\n" + convertToCurrency(br[i]) + "\n";
      }
    }

    var getIQrateMoney = "";
    var d = controller.disbursementSchedule[index]
        ['Monthly Installment based on IQrate'];
    // print("item thu $index co payment mode is ${a is List}");
    if (d is List) {
      for (var i = 0; i < d.length; i++) {
        var b = d[i];
        getIQrateMoney = getIQrateMoney + "\n" + convertToCurrency(b) + "\n";
      }
    } else {
      getIQrateMoney = convertToCurrency(d);
    }
    var getMonth = "";
    var e = controller.disbursementSchedule[index]['Disbursement Range'];
    getMonth = "$e Months";
    var getPercent = "";
    var p = controller.disbursementSchedule[index]['%'].toString();
    getPercent = "$p%";

    return IntrinsicHeight(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          headerTitle(
              controller.disbursementSchedule[index]['Disbursement Stage']),
          valueTitle(getMonth),
          valueTitlePercent(getPercent),
          valueTitle(getDisbursementAmount),
          // valueTitle(controller.disbursementSchedule[index]['%'] + "%"),
          valueTitle(getPaymentMode),
          valueTitle(getIQrateMoney),
          if (GetControllers.shared
              .getBucMortgageRepaymentController()
              .prefered_rate_enable)
            valueTitle(getMonthlyPrefered),
        ],
      ),
    );
  }

  valueTitle(dynamic value) {
    return Expanded(
      flex: 1,
      child: Padding(
        padding: const EdgeInsets.all(1),
        child: Container(
          color: AppColors.white,
          child: Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.only(left: 5),
                child: Text(
                  value.toString(),
                  style: TextStyles.leadsTextStyle1.copyWith(fontSize: 8),
                ),
              )),
        ),
      ),
    );
  }

  valueTitlePercent(dynamic value) {
    return Padding(
      padding: const EdgeInsets.all(1),
      child: Container(
        width: windowWidth / 7 * 0.5,
        color: AppColors.white,
        child: Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: const EdgeInsets.all(0),
              child: Center(
                child: Text(
                  value.toString(),
                  style: TextStyles.leadsTextStyle1.copyWith(fontSize: 8),
                ),
              ),
            )),
      ),
    );
  }

  valueTitleAmount(String value, String value1) {
    return Expanded(
      flex: 1,
      child: Padding(
        padding: const EdgeInsets.all(1),
        child: Column(
          children: [
            Expanded(
              child: Container(
                color: AppColors.white,
                child: Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: const EdgeInsets.all(0),
                      child: Center(
                        child: Text(
                          value,
                          style:
                              TextStyles.leadsTextStyle1.copyWith(fontSize: 8),
                        ),
                      ),
                    )),
              ),
            ),
            Expanded(
              child: Container(
                color: AppColors.white,
                child: Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: const EdgeInsets.all(0),
                      child: Center(
                        child: Text(
                          value1,
                          style:
                              TextStyles.leadsTextStyle1.copyWith(fontSize: 8),
                        ),
                      ),
                    )),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
