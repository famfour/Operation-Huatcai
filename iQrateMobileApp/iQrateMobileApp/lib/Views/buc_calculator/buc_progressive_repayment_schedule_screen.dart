// ignore_for_file: unused_local_variable, invalid_use_of_protected_member

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/buc_mortgage_repayment_controller.dart';
import 'package:iqrate/DeviceManager/screen_constants.dart';
import 'package:iqrate/Service/GetControllers.dart';
import 'package:iqrate/Utils/text_formatter.dart';
import 'package:iqrate/Widgets/default_appbar.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';
import '../../DeviceManager/colors.dart';
import '../../DeviceManager/container_styles.dart';
import '../../DeviceManager/text_styles.dart';
import '../../Widgets/secondary_button.dart';
import 'buc_mortgage_repayment_report.dart';

class ProgressiveRepaymentSchedule extends StatefulWidget {
  const ProgressiveRepaymentSchedule({Key? key}) : super(key: key);

  @override
  State<ProgressiveRepaymentSchedule> createState() =>
      _ProgressiveRepaymentScheduleState();
}

class _ProgressiveRepaymentScheduleState
    extends State<ProgressiveRepaymentSchedule> {
  BucMortgageRepaymentController controller =
      GetControllers.shared.getBucMortgageRepaymentController();

  int activeStep = 1;
  late double windowHeight;
  late double windowWidth;

  @override
  void initState() {
    super.initState();
    controller.resetDataArrayBucProgressiveRepaymentScheduleModelSent();
  }

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: DefaultAppBar(
          title: "BUC Mortgage Repayment", windowHeight: windowHeight * 0.09),
      body: InteractiveViewer(
        minScale: 1,
        maxScale: 4,
        child: GestureDetector(
          onTap: () {
            FocusScopeNode currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
          },
          child: ListView(
            shrinkWrap: true,
            children: [
              const SizedBox(
                height: 20,
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: Row(
                  children: [
                    Container(
                      height: 30,
                      width: 30,
                      child: Center(
                        child: Text(
                          '1',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: FontSize.s15,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                      decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          color: AppColors.kPrimaryColor),
                    ),
                    Expanded(
                      child: Container(
                        height: 1,
                        color: Colors.grey,
                      ),
                    ),
                    Container(
                      height: 30,
                      width: 30,
                      child: Center(
                        child: Text(
                          '2',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: FontSize.s15,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                      decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          color: AppColors.kPrimaryColor),
                    ),
                    Expanded(
                      child: Container(
                        height: 1,
                        color: Colors.grey,
                      ),
                    ),
                    Container(
                      height: 30,
                      width: 30,
                      child: Center(
                        child: Text(
                          '3',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: FontSize.s15,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                      decoration: const BoxDecoration(
                          shape: BoxShape.circle, color: AppColors.greyA8A8A8),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 30, top: 20, bottom: 10, right: 20),
                child: Text(
                  "Progressive Repayment Schedule",
                  style: TextStyles.calculatorHeadingsTextStyle,
                ),
              ),
              Obx(
                () => controller.textEditingControllerList.value.isEmpty
                    ? Container()
                    : bodyTable(),
              ),
              const SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Center(
                  child: PrimaryButton(
                    windowHeight: windowHeight,
                    windowWidth: windowWidth,
                    buttonTitle: "Generate Report",
                    onPressed: () {
                      hideKeyboard();
                      if (controller.validateTextFieldB4GenerateReport()) {
                        if ((int.tryParse(controller.textEditingController0.text)! < 1 ||
                            int.tryParse(controller.textEditingController0.text)! >
                                12 ||
                            int.tryParse(controller.textEditingController1.text)! <
                                1 ||
                            int.tryParse(controller.textEditingController1.text)! >
                                12 ||
                            int.tryParse(controller.textEditingController2.text)! <
                                1 ||
                            int.tryParse(controller.textEditingController2.text)! >
                                12 ||
                            int.tryParse(controller.textEditingController3.text)! <
                                1 ||
                            int.tryParse(controller.textEditingController3.text)! >
                                12 ||
                            int.tryParse(controller.textEditingController4.text)! <
                                1 ||
                            int.tryParse(controller.textEditingController4.text)! >
                                12 ||
                            int.tryParse(controller.textEditingController5.text)! <
                                1 ||
                            int.tryParse(controller.textEditingController5.text)! >
                                12 ||
                            int.tryParse(controller.textEditingController6.text)! <
                                1 ||
                            int.tryParse(
                                    controller.textEditingController6.text)! >
                                12 ||
                            int.tryParse(
                                    controller.textEditingController7.text)! <
                                1 ||
                            int.tryParse(
                                    controller.textEditingController7.text)! >
                                12 ||
                            int.tryParse(
                                    controller.textEditingController8.text)! <
                                1 ||
                            int.tryParse(
                                    controller.textEditingController8.text)! >
                                12 ||
                            int.tryParse(
                                    controller.textEditingController9.text)! <
                                1 ||
                            int.tryParse(
                                    controller.textEditingController9.text)! >
                                12 ||
                            controller.textEditingController0.text.isEmpty ||
                            controller.textEditingController1.text.isEmpty ||
                            controller.textEditingController2.text.isEmpty ||
                            controller.textEditingController3.text.isEmpty ||
                            controller.textEditingController4.text.isEmpty ||
                            controller.textEditingController5.text.isEmpty ||
                            controller.textEditingController6.text.isEmpty ||
                            controller.textEditingController7.text.isEmpty ||
                            controller.textEditingController8.text.isEmpty ||
                            controller.textEditingController9.text.isEmpty)) {
                          FocusManager.instance.primaryFocus!.unfocus();
                          Fluttertoast.showToast(
                            msg:
                                'Please check the inputted years (EXPECTED : 1-12)',
                            backgroundColor: Colors.red,
                            textColor: Colors.white,
                            toastLength: Toast.LENGTH_LONG,
                            gravity: ToastGravity.BOTTOM,
                            fontSize: 16,
                            timeInSecForIosWeb: 3,
                          );
                        } else {
                          controller.callAPICalculatorBucMortgageReport(context,
                              (data) {
                            GetControllers.shared
                                .getBucMortgageRepaymentReportController()
                                .loadDetailsDataResult = data;
                            debugPrint(
                                "done callAPICalculatorBucMortgageReport with data ${GetControllers.shared.getBucMortgageRepaymentReportController().loadDetailsDataResult}");
                            debugPrint(
                                "done callAPICalculatorBucMortgageReport with Comparison ${data["Rate Package Comparison"]}");
                            // GetControllers.shared.getBucMortgageRepaymentCompareController().interestRateComparisonData = data["Rate Package Comparison"];
                            Get.to(() => const BucMortgageRepaymentReport());
                          });
                        }
                      }
                    },
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: SecondaryButton(
                  windowHeight: windowHeight,
                  windowWidth: windowWidth,
                  buttonTitle: "Back",
                  onPressed: () {
                    debugPrint('Back');
                    Get.back();
                  },
                  kGradientBoxDecoration:
                      ContainerStyles.kGradientBoxDecorationSecondaryButton,
                  kInnerDecoration:
                      ContainerStyles.kInnerDecorationSecondaryButton,
                ),
              ),
              const SizedBox(
                height: 20,
              )
            ],
          ),
        ),
      ),
    );
  }

  hideKeyboard() {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }

  bodyTable() {
    List data = controller.disbursement_schedule;
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Container(
        color: Colors.grey[100],
        // color: AppColors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            titleMenu(),
            for (var i = 0; i < data.length; i++)
              Row(
                // crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  monthColum(i, controller.textEditingControllerList[i],
                      controller.textEditingControllerList),
                  valueTitleStage(
                      name: controller.disbursement_schedule[i]['name'],
                      value: controller.disbursement_schedule[i]['percentage']
                          .toString()),
                  // valueTitleStage(
                  //     value: "Completion of \nFoundation", index: 0),
                  //for example here loan amount is 50000 and user enter value is 10% then u get the 10% from 50000.
                  //res is 5000
                  // controller.resultLoanAmount.value;
                  valueTitle(
                      controller.disbursement_schedule[i]['total_amount'])
                ],
              ),
          ],
        ),
      ),
    );
  }

  titleMenu() {
    return Row(
      children: [
        Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.all(1),
              child: Container(
                color: AppColors.white,
                height: 80,
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Text(
                    "Month",
                    style: TextStyles.leadsColorTextStyle2
                        .copyWith(color: AppColors.kPrimaryColor, fontSize: 13),
                  ),
                ),
              ),
            )),
        // Container(color: AppColors.white,child: he),
        //from Ajay: for example here loan amount is 50000 and user enter value is 10% then u get the 10% from 50000.
        //input %.. will calculator
        Expanded(
            flex: 3,
            child: Padding(
              padding: const EdgeInsets.all(1),
              child: Container(
                height: 80,
                color: AppColors.white,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Text(
                    "Disbursement Stage(%)",
                    style: TextStyles.leadsColorTextStyle2
                        .copyWith(color: AppColors.kPrimaryColor, fontSize: 14),
                  ),
                ),
              ),
            )),

        //from Ajay: for example here loan amount is 50000 and user enter value is 10% then u get the 10% from 50000.
        //input %.. will calculator
        Expanded(
            flex: 2,
            child: Padding(
              padding: const EdgeInsets.all(1),
              child: Container(
                height: 80,
                color: AppColors.white,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Text(
                    "Disbursement \nAmount(\$)",
                    style: TextStyles.leadsColorTextStyle2
                        .copyWith(color: AppColors.kPrimaryColor, fontSize: 14),
                  ),
                ),
              ),
            )),
      ],
    );
  }

  Widget monthColum(int index, TextEditingController textEditingController,
      List<TextEditingController> textEditingControllerList) {
    final categories = List.generate(12, (ind) => '${ind + 1}');
    textEditingController.text = textEditingControllerList[index].text;
    return Expanded(
      flex: 1,
      child: Container(
        color: AppColors.white,
        child: Center(
          // child: Text(controller.disbursement_schedule[index]['month']
          //     .toString())),
          child: Padding(
            padding: const EdgeInsets.all(17.0),
            child: TextField(
              controller: textEditingController,
              keyboardType: TextInputType.number,
              maxLength: 2,
              onChanged: (text) {
                if (int.tryParse(text)! < 1 || int.tryParse(text)! > 12) {
                  FocusManager.instance.primaryFocus!.unfocus();
                  Fluttertoast.showToast(
                    msg: 'Please enter a valid month (EXPECTED : 1-12)',
                    backgroundColor: Colors.red,
                    textColor: Colors.white,
                    toastLength: Toast.LENGTH_LONG,
                    gravity: ToastGravity.BOTTOM,
                    fontSize: 16,
                    timeInSecForIosWeb: 3,
                  );
                }
              },
              textAlign: TextAlign.center,
              decoration: InputDecoration(
                border: InputBorder.none,
                counter: Container(),
              ),
            ),
          ),
        ),
      ),
    );
  }

  String getValueInitMonthDropdown(int index) {
    return controller
        .arrayBucProgressiveRepaymentScheduleModelSent[index].month;
  }

  setValueMonthDropdown(int index, String value) {
    controller.arrayBucProgressiveRepaymentScheduleModelSent[index].month =
        value;
  }

  Widget valueTitle(dynamic value) {
    // var data = value.forEach((key, value) {
    //
    // }
    // var a = <String>[];
    // value.forEach((key, value) {
    //   a.add(value.toString());
    // });
    // var moneyString = "";
    // if (a.length < 2) {
    //   // moneyString += double.parse(a.first).toStringAsFixed(2);
    //   moneyString += convertToCurrency(a.first);
    // } else {
    //   for (var i = 0; i < a.length; i++) {
    //     var b = convertToCurrency(a[i]);
    //     (i == a.length - 1) ? moneyString += b :  moneyString += b + "\n";
    //   }
    // }
    var moneyString = "";
    // moneyString += double.parse(value).toStringAsFixed(2);
    moneyString = convertToCurrency(value);
    return Expanded(
      flex: 2,
      child: Container(
        color: AppColors.white,
        child: Center(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 17.0, vertical: 36),
            child: Text(
              moneyString,
              style: TextStyles.leadsTextStyle1.copyWith(fontSize: 11),
            ),
          ),
        ),
      ),
    );
  }

  // valueTitle(String value) {
  //   return Expanded(
  //     flex: 2,
  //     child: Padding(
  //       padding: const EdgeInsets.all(1),
  //       child: Container(
  //         color: AppColors.white,
  //         child: Align(
  //             alignment: Alignment.centerLeft,
  //             child: Padding(
  //               padding: const EdgeInsets.all(20),
  //               child: Text(
  //                 value,
  //                 style: TextStyles.leadsTextStyle1.copyWith(fontSize: 11),
  //               ),
  //             )),
  //       ),
  //     ),
  //   );
  // }

  Widget valueTitleStage({String name = "", String value = ""}) {
    return Expanded(
      flex: 3,
      child: Padding(
        padding: const EdgeInsets.all(1),
        child: Row(
          children: [
            Expanded(
              flex: 7,
              child: Container(
                height: 90,
                color: AppColors.white,
                child: Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 12),
                      child: Text(
                        name,
                        style:
                            TextStyles.leadsTextStyle1.copyWith(fontSize: 11),
                      ),
                    )),
              ),
            ),
            Expanded(
              flex: 2,
              child: Container(
                height: 90,
                color: AppColors.white,
                child: Center(
                  child: Text(
                    value,
                    style: TextStyles.leadsTextStyle1.copyWith(fontSize: 11),
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                  height: 90,
                  color: AppColors.white,
                  child: const Center(child: Text("% "))),
            ),
            // const Text(" % ")
          ],
        ),
      ),
    );
  }
  // valueTitleStage({String value = "", int index = 0}) {
  //   return Expanded(
  //     flex: 3,
  //     child: Padding(
  //       padding: const EdgeInsets.all(1),
  //       child: Row(
  //         children: [
  //           Expanded(
  //             flex: 4,
  //             child: Container(
  //               height: 80,
  //               color: AppColors.white,
  //               child: Align(
  //                   alignment: Alignment.centerLeft,
  //                   child: Padding(
  //                     padding: const EdgeInsets.all(15),
  //                     child: Text(
  //                       value,
  //                       style:
  //                       TextStyles.leadsTextStyle1.copyWith(fontSize: 11),
  //                     ),
  //                   )),
  //             ),
  //           ),
  //           Expanded(
  //             flex: 2,
  //             child: Container(
  //               height: 80,
  //               color: AppColors.white,
  //               child: Center(
  //                 child: TextField(
  //                   controller:
  //                   controller.arrayDisbursementStageController[index],
  //                   keyboardType: TextInputType.number,
  //                   textAlign: TextAlign.right,
  //                   style: const TextStyle(fontSize: 12),
  //                   onChanged: (value) {
  //                     if (double.parse(value) > 100) {
  //                       Get.snackbar(
  //                           StringUtils.hasErrorTitle, "Range must 1 - 100",
  //                           backgroundColor: Colors.red,
  //                           colorText: Colors.white,
  //                           duration: const Duration(seconds: 1));
  //                     }
  //                   },
  //                   decoration: const InputDecoration(
  //                     hintText: '%',
  //                     enabledBorder: OutlineInputBorder(
  //                       borderSide: BorderSide(color: AppColors.transparent),
  //                     ),
  //                     focusedBorder: OutlineInputBorder(
  //                       borderSide:
  //                       BorderSide(color: AppColors.formFieldBorderColor),
  //                       borderRadius: BorderRadius.all(
  //                         Radius.circular(10.0),
  //                       ),
  //                     ),
  //                   ),
  //                 ),
  //               ),
  //             ),
  //           ),
  //           Expanded(
  //             flex: 1,
  //             child: Container(
  //                 height: 80,
  //                 color: AppColors.white,
  //                 child: const Center(child: Text("% "))),
  //           ),
  //           // const Text(" % ")
  //         ],
  //       ),
  //     ),
  //   );
  // }

  // valueTitleStage(String value, String value1) {
  //   return Expanded(
  //     flex: 2,
  //     child: Padding(
  //       padding: const EdgeInsets.all(1),
  //       child: Row(
  //         children: [
  //           Expanded(
  //             flex: 2,
  //             child: Container(
  //               height: 80,
  //               color: AppColors.white,
  //               child: Align(
  //                   alignment: Alignment.centerLeft,
  //                   child: Padding(
  //                     padding: const EdgeInsets.all(15),
  //                     child: Text(
  //                       value,
  //                       style:
  //                       TextStyles.leadsTextStyle1.copyWith(fontSize: 11),
  //                     ),
  //                   )),
  //             ),
  //           ),
  //           Expanded(
  //             flex: 1,
  //             child: Container(
  //               height: 80,
  //               color: AppColors.white,
  //               child: Align(
  //                   alignment: Alignment.centerLeft,
  //                   child: Padding(
  //                     padding: const EdgeInsets.all(15),
  //                     child: Text(
  //                       value1 + " %",
  //                       style:
  //                       TextStyles.leadsTextStyle1.copyWith(fontSize: 11),
  //                     ),
  //                   )),
  //             ),
  //           )
  //         ],
  //       ),
  //     ),
  //   );
  // }

  valueTitleAmount(String value) {
    return Expanded(
      flex: 2,
      child: Padding(
        padding: const EdgeInsets.all(1),
        child: Container(
          height: 80,
          color: AppColors.white,
          child: Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.all(15),
                child: Text(
                  value,
                  style: TextStyles.leadsTextStyle1.copyWith(fontSize: 11),
                ),
              )),
        ),
      ),
    );
  }
}

class MonthColumn extends StatelessWidget {
  final TextEditingController controller;
  const MonthColumn({
    Key? key,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final categories = List.generate(12, (ind) => '${ind + 1}');
    return Expanded(
      flex: 1,
      child: Padding(
        padding: const EdgeInsets.all(1),
        child: Container(
          color: AppColors.white,
          child: Align(
            alignment: Alignment.centerLeft,
            child: Padding(
                // padding: const EdgeInsets.all(20),
                padding: const EdgeInsets.symmetric(horizontal: 5),
                child: Center(
                  child: RequireTextField(
                    type: Type.number,
                    controller: controller,
                    maxLength: 2,
                  ),
                  // child: Text(controller.disbursement_schedule[index]['month']
                  //     .toString())),
                  //   child: PopupMenuButton(
                  //       onSelected: (value) {
                  //         setValueMonthDropdown(index, value.toString());
                  //         setState(() {});
                  //       },
                  //       itemBuilder: (context) {
                  //         return categories
                  //             .map(
                  //               (value) => PopupMenuItem(
                  //             value: value,
                  //             child: Text(value),
                  //           ),
                  //         )
                  //             .toList();
                  //       },
                  //       offset: const Offset(1, 1),
                  //       child: Container(
                  //         padding: const EdgeInsets.all(5.0),
                  //         decoration: BoxDecoration(
                  //             border: Border.all(color: Colors.black12),
                  //             borderRadius:
                  //             const BorderRadius.all(Radius.circular(10))),
                  //         child: Row(
                  //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //           children: [
                  //             Text(getValueInitMonthDropdown(index)),
                  //             const Icon(Icons.arrow_drop_down),
                  //           ],
                  //         ),
                  //       )),
                  // ),
                )),
          ),
        ),
      ),
    );
  }
}
