// ignore_for_file: must_be_immutable, prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Model/response_model.dart/list_all_packages_response_model_v2.dart';
import 'package:iqrate/Service/GetControllers.dart';

import '../../Controller/buc_mortgage_repayment_compare_controller.dart';

class BucPackageCardDetails extends StatelessWidget {
  // String? image;
  // String headingText;
  // String headingSubText;
  void Function(bool?)? onChanged;
  int cardIndex;
  final PackageModel data;
  final oCcy = NumberFormat("#,##0.0", "en_US");
  BucPackageCardDetails(
      {Key? key,
      required this.onChanged,
      required this.cardIndex,
      required this.data})
      : super(key: key);

  // final RatesViewController ratesViewController = Get.find();
  final BucMortgageRepaymentCompareController bucController =
      GetControllers.shared.getBucMortgageRepaymentCompareController();

  late double windowHeight;
  late double windowWidth;
  late var rates;
  @override
  Widget build(BuildContext context) {
    debugPrint("BucPackageCardDetails");
    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.width;
    rates = data.rates![0].year.toString() != 'year_1'
        ? data.rates!.reversed
        : data.rates;
    return expansionSection();
  }
  //
  // // A simple widget used in the 2nd row, which only shows a title and a value
  // Widget titleValueRow({required String title, required String value}) {
  //   return Row(
  //     mainAxisAlignment: MainAxisAlignment.center,
  //     children: [
  //       Text(
  //         title + ': ',
  //         style: TextStyles.headingSubtitleStyle,
  //       ),
  //       Text(
  //         value,
  //         style: TextStyles.headingSubtitleStyle,
  //       )
  //     ],
  //   );
  // }

  Widget expansionSection() {
    return ClipRRect(
      borderRadius: const BorderRadius.only(
          bottomLeft: Radius.circular(15), bottomRight: Radius.circular(15)),
      child: Container(
          color: AppColors.kSecondaryColor,
          child: Container(
            color: Colors.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ListView(
                  padding: EdgeInsets.zero,
                  primary: false,
                  shrinkWrap: true,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 20),
                      child: expandableCardRates(),
                    ),

                    expandableCardKeyFeatures(),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 20),
                      child: expandableCardBankSubsidy(),
                    ),

                    expandableCardEarlyRepaymentPenalty(),

                    InkWell(
                      onTap: () {
                        GetControllers.shared
                            .getBucMortgageRepaymentCompareController()
                            .isExpanded
                            .value = false;
                      },
                      child: Container(
                        color: AppColors.kSecondaryColor,
                        child: Center(
                          child: Padding(
                            padding: const EdgeInsets.all(5),
                            child: SvgPicture.asset(
                              'assets/icons/leads_arrow_up.svg',
                              color: AppColors.white,
                            ),
                          ),
                        ),
                      ),
                    ),

                    // Container(color: Colors.green, height: 70,)
                  ],
                )
              ],
            ),
          )),
    );
  }

  Widget expandableCardRates() {
    return GestureDetector(
      onTap: () {
        bucController.isExpandedRates.toggle();
      },
      child: Obx(() => Container(
            decoration: const BoxDecoration(boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.black54,
                  blurRadius: 5.0,
                  offset: Offset(0.0, 0.3))
            ], color: Colors.white),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.all(windowWidth * 0.02),
                  color: AppColors.red,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text("Rates", style: TextStyles.basicTextStyle2),
                      const Spacer(),
                      const Padding(
                        padding: EdgeInsets.only(bottom: 10.0),
                        child: Icon(Icons.minimize, color: Colors.white),
                      ),
                    ],
                  ),
                ),
                bucController.isExpandedRates.value
                    ? Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 7.0),
                            child: ListView.builder(
                              //physics: ClampingScrollPhysics(),
                              padding: EdgeInsets.zero,
                              shrinkWrap: true,
                              primary: false,
                              scrollDirection: Axis.vertical,
                              itemCount: rates.length,
                              itemBuilder: (BuildContext context, int index) =>
                                  titleValueRow2(
                                      title: rates
                                              .elementAt(index)
                                              .year
                                              .toString()
                                              .replaceAll("_", " ")
                                              .capitalizeFirst! +
                                          " ",
                                      value: rates
                                                  .elementAt(index)
                                                  .referenceRate !=
                                              'Fixed'
                                          ? rates
                                                  .elementAt(index)
                                                  .referenceRate
                                                  .reference +
                                              ' (${rates.elementAt(index).referenceRate.interestRate})% ' +
                                              rates
                                                  .elementAt(index)
                                                  .referenceRate
                                                  .equation +
                                              ' ' +
                                              rates
                                                  .elementAt(index)
                                                  .bankSpread
                                                  .toString() +
                                              '%' +
                                              ' = ' +
                                              rates
                                                  .elementAt(index)
                                                  .totalInterestRate
                                                  .toStringAsFixed(2) +
                                              "%"
                                          : rates
                                              .elementAt(index)
                                              .totalInterestRate
                                              .toStringAsFixed(2)),
                            ),
                          ),
                        ],
                      )
                    : Container(),
              ],
            ),
          )),
    );
  }

  Widget expandableCardKeyFeatures() {
    return GestureDetector(
      onTap: () {
        bucController.isExpandedKeyFeatures.toggle();
      },
      child: Obx(() => Container(
            decoration: const BoxDecoration(boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.black54,
                  blurRadius: 5.0,
                  offset: Offset(0.0, 0.3))
            ], color: Colors.white),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.all(windowWidth * 0.02),
                  color: AppColors.red,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text("Key Features", style: TextStyles.basicTextStyle2),
                      const Spacer(),
                      const Padding(
                        padding: EdgeInsets.only(bottom: 10.0),
                        child: Icon(Icons.minimize, color: Colors.white),
                      ),
                    ],
                  ),
                ),
                bucController.isExpandedKeyFeatures.value
                    ? Padding(
                        padding: const EdgeInsets.symmetric(vertical: 3.0),
                        child: Column(
                          children: [
                            titleValueRowWithList(
                                title: "Property Type",
                                values: data.propertyTypes!),
                            titleValueRowWithList(
                                title: "Loan Type", values: data.loanCategory!),
                            titleValueRow3(
                                title: "Min Loan Amount",
                                value: "\$" + oCcy.format(data.minLoanAmount)),
                            titleValueRow3(
                                title: "Deposit to Place",
                                value: "\$" + oCcy.format(data.depositToPlace)),
                            titleValueRow3(
                                title: "Deposit to Place Remarks",
                                value: data.depositToPlaceRemarks.toString()),
                            titleValueRow3(
                                title: "Lock in Period (Years)",
                                value: data.lockInPeriod.toString() + " Years"),
                            titleValueRowWithList(
                                title: "Remarks for Client",
                                values: data.remarksForClient!),
                            titleValueRow3(
                                title: "Interest Reset Date",
                                value: data.interestResetDate! ? "Yes" : "No"),
                            titleValueRow3(
                                isLast: true,
                                title: "Processing Fee",
                                value: data.processingFee! ? "Yes" : "No"),
                            // titleValueRow3(
                            //     isLast: true,
                            //     title: "Remarks for Broker",
                            //     value: data.remarksForBroker.toString()),
                          ],
                        ),
                      )
                    : Container()
              ],
            ),
          )),
    );
  }

  Widget expandableCardBankSubsidy() {
    return GestureDetector(
      onTap: () {
        bucController.isExpandedBankSubsidy.toggle();
      },
      child: Obx(() => Container(
            decoration: const BoxDecoration(boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.black54,
                  blurRadius: 5.0,
                  offset: Offset(0.0, 0.3))
            ], color: Colors.white),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.all(windowWidth * 0.02),
                  color: AppColors.red,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text("Bank’s Subsidy", style: TextStyles.basicTextStyle2),
                      const Spacer(),
                      const Padding(
                        padding: EdgeInsets.only(bottom: 10.0),
                        child: Icon(Icons.minimize, color: Colors.white),
                      ),
                    ],
                  ),
                ),
                bucController.isExpandedBankSubsidy.value
                    ? Padding(
                        padding: const EdgeInsets.symmetric(vertical: 5.0),
                        child: Column(
                          children: [
                            titleValueRowWithList(
                                title: "Cash Rebate/ Legal \nSubsidy",
                                values: data.cashRebateLegalSubsidy!),
                            titleValueRowWithList(
                                title: "Valuation Subsidy",
                                values: data.valuationSubsidy!),
                            titleValueRow3(
                                title: "Fire Insurance Subsidy",
                                value: data.fireInsuranceSubsidy.toString()),
                            titleValueRow3(
                                isLast: true,
                                title:
                                    "Cash Rebate/ Subsidy Clawback Period (Years)",
                                value: data.cashRebateSubsidyClawbackPeriodYears
                                        .toString() +
                                    " Years"),
                          ],
                        ),
                      )
                    : Container()
              ],
            ),
          )),
    );
  }

  Widget expandableCardEarlyRepaymentPenalty() {
    return GestureDetector(
      onTap: () {
        bucController.isExpandedBankPenalty.toggle();
      },
      child: Obx(() => Column(
            children: [
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                color: AppColors.red,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text("Early Repayment Penalty",
                        style: TextStyles.basicTextStyle2),
                    const Spacer(),
                    const Padding(
                      padding: EdgeInsets.only(bottom: 10.0),
                      child: Icon(Icons.minimize, color: Colors.white),
                    ),
                  ],
                ),
              ),
              bucController.isExpandedBankPenalty.value
                  ? Column(
                      children: [
                        const SizedBox(
                          height: 10,
                        ),
                        // titleValueRow3(
                        //     title: "Partial Repayment Penalty",
                        //     value:
                        //         data.partialRepaymentPenalty!.toString() +
                        //             "%"),
                        titleValueRow3(
                          title: "Partial Repayment Penalty",
                          value: data.partialRepaymentPenalty!.toString() + "%",
                        ),

                        // titleValueRow3(
                        //     title: "Full Repayment Penalty",
                        //     value: data.fullRepaymentPenalty!.toString() +
                        //         "%"),
                        titleValueRow3(
                          title: "Partial Repayment Penalty Remarks",
                          value:
                              data.partialRepaymentPenaltyRemarks!.toString(),
                        ),
                        titleValueRow3(
                          title: "Full Repayment Penalty",
                          value: data.fullRepaymentPenalty!.toString() + "%",
                        ),
                        titleValueRow3(
                            title: "Full Repayment Penalty Remarks",
                            value:
                                data.fullRepaymentPenaltyRemarks!.toString()),

                        titleValueRow3(
                          title: "Cancellation Fee",
                          value: data.cancellationFee!.toString(),
                        ),
                        titleValueRow3(
                          isLast: true,
                          title: "Cancellation Fee Remarks",
                          value: data.cancellationFeeRemarks!.toString().isEmpty
                              ? "NA"
                              : data.cancellationFeeRemarks!.toString(),
                        )
                      ],
                    )
                  : Container()
            ],
          )),
    );
  }

  // A simple widget used in the 2nd row, which only shows a title and a value
  Widget titleValueRow({required String title, required String value}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          title + ': ',
          style: TextStyles.headingSubtitleStyle,
        ),
        Text(
          value,
          style: TextStyles.headingSubtitleStyle,
        )
      ],
    );
  }

  // A simple widget used in the 2nd row, which only shows a title and a value
  Widget titleValueRow2({required String title, required String value}) {
    return Padding(
      padding:
          const EdgeInsets.only(right: 10.0, left: 10.0, top: 3, bottom: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 3,
            child: Text(
              title,
              style: TextStyles.headingSubtitleStyle2.copyWith(fontSize: 14),
            ),
          ),
          //Spacer(),
          Expanded(
            flex: 7,
            child: Text(
              value,
              style: TextStyles.headingSubtitleStyle2.copyWith(fontSize: 14),
            ),
          )
        ],
      ),
    );
  }

  Widget titleValueRow3(
      {required String title, required String value, bool isLast = false}) {
    return Column(
      children: [
        Row(
          // mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,

          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  title,
                  style: TextStyles.headingSubtitleStyle4,
                ),
              ),
            ),
            const SizedBox(
              width: 5,
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  value,
                  style:
                      TextStyles.headingSubtitleStyle2.copyWith(fontSize: 14),
                  // textAlign: TextAlign.left,
                ),
              ),
            )
          ],
        ),
        if (isLast == false)
          const Divider(
            thickness: 1,
            color: Colors.black26,
          )
      ],
    );
  }

  Widget titleValueRowWithList(
      {required String title, required List<String> values}) {
    // var values = ["NA", "abc_xyz", "abc2_xyz2"];
    return Column(
      children: [
        Row(
          // mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  title,
                  style: TextStyles.headingSubtitleStyle4,
                  textAlign: TextAlign.left,
                ),
              ),
            ),
            const SizedBox(
              width: 5,
            ),
            values.length > 1
                ? Flexible(
                    child: ListView.builder(
                    padding: EdgeInsets.zero,
                    physics: const ClampingScrollPhysics(),
                    shrinkWrap: true,
                    primary: false,
                    scrollDirection: Axis.vertical,
                    itemCount: values.length,
                    itemBuilder: (BuildContext context, int index) => Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        values.elementAt(index) == "NA"
                            ? "NA"
                            : '${values.elementAt(index).replaceAll("_", " ").capitalizeFirst}',
                        style: TextStyles.headingSubtitleStyle2
                            .copyWith(fontSize: 14),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ))
                : Expanded(
                    flex: 1,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        values.first == "NA"
                            ? "NA"
                            : '${values.first.replaceAll("_", " ").capitalizeFirst}',
                        style: TextStyles.headingSubtitleStyle2
                            .copyWith(fontSize: 14),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ),
          ],
        ),
        // ListTile(
        //     contentPadding: const EdgeInsets.symmetric(horizontal: 10),
        //     title: Row(
        //       children: [
        //         Expanded(
        //           child: Align(
        //             alignment: Alignment.topLeft,
        //             child: Text(
        //               title,
        //               style: TextStyles.headingSubtitleStyle4,
        //               textAlign: TextAlign.start,
        //             ),
        //           ),
        //         ),
        //         Flexible(
        //             child: ListView.builder(
        //           physics: const ClampingScrollPhysics(),
        //           shrinkWrap: true,
        //           primary: false,
        //           scrollDirection: Axis.vertical,
        //           itemCount: values.length,
        //           itemBuilder: (BuildContext context, int index) => Align(
        //               alignment: Alignment.centerLeft,
        //               child: Padding(
        //                   padding: const EdgeInsets.only(bottom: 8.0),
        //                   child: Text(
        //                     values.elementAt(index) == "NA"
        //                         ? "NA"
        //                         : '${values.elementAt(index).replaceAll("_", " ").capitalizeFirst}',
        //                     style: TextStyles.headingSubtitleStyle2,
        //                     textAlign: TextAlign.left,
        //                   ))),
        //         ))
        //       ],
        //     )),
        const Divider(
          thickness: 1,
          color: Colors.black26,
        )
      ],
    );
  }

  // This section is displayed when the card is expanded and this expansion tile is shown in a list
  Widget expandedSectionHeading(String title, List<Widget> body) {
    return ExpansionTile(
      iconColor: Colors.white,
      backgroundColor: AppColors.kSecondaryColor,
      collapsedBackgroundColor: AppColors.kSecondaryColor,
      collapsedIconColor: Colors.white,
      leading: Text(
        title,
        style: TextStyles.detailsButton,
      ),
      title: Container(),
      children: body,
    );
  }
}
