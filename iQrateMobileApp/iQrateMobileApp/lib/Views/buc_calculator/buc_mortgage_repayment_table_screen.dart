import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Service/GetControllers.dart';
import 'package:iqrate/Utils/text_formatter.dart';
import 'package:iqrate/Controller/buc_mortgage_repayment_report_controller.dart';
import 'package:iqrate/Widgets/default_appbar.dart';
import '../../DeviceManager/colors.dart';
import '../../DeviceManager/text_styles.dart';
import '../../Model/response_model.dart/BucMortgageRepaymentTableModel.dart';
import '../../Widgets/primary_button.dart';

class BucMortgageRepaymentTableScreen extends StatefulWidget {
  final String titleString;
  const BucMortgageRepaymentTableScreen({Key? key, this.titleString = ""})
      : super(key: key);

  @override
  State<BucMortgageRepaymentTableScreen> createState() =>
      _BucMortgageRepaymentTableScreenState();
}

class _BucMortgageRepaymentTableScreenState
    extends State<BucMortgageRepaymentTableScreen> {
  BucMortgageRepaymentReportController controller =
      GetControllers.shared.getBucMortgageRepaymentReportController();
  int activeStep = 0;
  // Initial step set to 0.
  var selected = 0;
  late double windowHeight;
  late double windowWidth;

  @override
  void initState() {
    super.initState();
    controller.clearDataTable();
    controller.getDataTableModels();
  }

  @override
  Widget build(BuildContext context) {
    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: DefaultAppBar(
          title: "BUC Mortgage Repayment", windowHeight: windowHeight * 0.09),
      body: InteractiveViewer(
        minScale: 1,
        maxScale: 4,
        child: GestureDetector(
          onTap: () {
            FocusScopeNode currentFocus = FocusScope.of(context);

            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(
                    left: 20, top: 20, bottom: 20, right: 20),
                child: controller.viewTable1
                    ? Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            // widget.titleString,
                            "BUC Mortgage Repayment Table",
                            style: TextStyles.leadsColorTextStyle2
                                .copyWith(color: AppColors.kPrimaryColor),
                          ),
                          Text(
                            "(IQrate Exclusive BUC Rate)",
                            style: TextStyles.leadsColorTextStyle
                                .copyWith(fontSize: 16),
                          )
                        ],
                      )
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            // widget.titleString,
                            "BUC Mortgage Repayment Table",
                            style: TextStyles.leadsColorTextStyle2
                                .copyWith(color: AppColors.kPrimaryColor),
                          ),
                          Text(
                            "(Your Preferred Interest Rate)",
                            style: TextStyles.leadsColorTextStyle
                                .copyWith(fontSize: 16),
                          )
                        ],
                      ),
              ),
              titleTable(),
              Expanded(
                child: ListView(
                  children: [
                    SingleChildScrollView(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            expandTable1("Year 1-5"),
                            if (controller.dataSourceTable2.isNotEmpty)
                              expandTable2("Year 6-10"),
                            if (controller.dataSourceTable3.isNotEmpty)
                              expandTable3("Year 11-15"),
                            if (controller.dataSourceTable4.isNotEmpty)
                              expandTable4("Year 16-20"),
                            if (controller.dataSourceTable5.isNotEmpty)
                              expandTable5("Year 21-25"),
                            if (controller.dataSourceTable6.isNotEmpty)
                              expandTable6("Year 26-30"),
                            if (controller.dataSourceTable7.isNotEmpty)
                              expandTable7("Year 31-35"),
                            const SizedBox(
                              height: 30,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 20),
                              child: Center(
                                child: PrimaryButton(
                                  windowHeight: windowHeight,
                                  windowWidth: windowWidth,
                                  buttonTitle: "Close",
                                  onPressed: () {
                                    Get.back();
                                    //newPurchaseController.onTapBackStep3Lead();
                                  },
                                ),
                              ),
                            ),
                          ]),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget expandTable1(String title) {
    return Padding(
      padding: const EdgeInsets.all(1.0),
      child: Container(
        color: AppColors.white,
        child: ExpansionTile(
          key: GlobalKey(),
          initiallyExpanded: 1 == selected,
          onExpansionChanged: (isOpen) {
            if (isOpen) {
              setState(() {
                selected = 1;
              });
            } else {
              setState(() {
                selected = 0;
              });
            }
          },
          title: Text(title,
              style: TextStyles.leadsTextStyleBold.copyWith(fontSize: 16)),
          collapsedIconColor: Colors.black,
          children: [
            const Divider(height: 1, thickness: 1, color: Colors.black12),
            getBodyYear1Row(),
          ],
        ),
      ),
    );
  }

  Widget expandTable2(String title) {
    return Padding(
      padding: const EdgeInsets.all(1.0),
      child: Container(
        color: AppColors.white,
        child: ExpansionTile(
          key: GlobalKey(),
          initiallyExpanded: 2 == selected,
          onExpansionChanged: (isOpen) {
            if (isOpen) {
              setState(() {
                selected = 2;
              });
            } else {
              setState(() {
                selected = 0;
              });
            }
          },
          title: Text(title,
              style: TextStyles.leadsTextStyleBold.copyWith(fontSize: 16)),
          collapsedIconColor: Colors.black,
          children: [
            const Divider(height: 1, thickness: 1, color: Colors.black12),
            getBodyYear2Row(),
          ],
        ),
      ),
    );
  }

  Widget expandTable3(String title) {
    return Padding(
      padding: const EdgeInsets.all(1.0),
      child: Container(
        color: AppColors.white,
        child: ExpansionTile(
          key: GlobalKey(),
          initiallyExpanded: 3 == selected,
          onExpansionChanged: (isOpen) {
            if (isOpen) {
              setState(() {
                selected = 3;
              });
            } else {
              setState(() {
                selected = 0;
              });
            }
          },
          title: Text(title,
              style: TextStyles.leadsTextStyleBold.copyWith(fontSize: 16)),
          collapsedIconColor: Colors.black,
          children: [
            const Divider(height: 1, thickness: 1, color: Colors.black12),
            getBodyYear3Row(),
          ],
        ),
      ),
    );
  }

  Widget expandTable4(String title) {
    return Padding(
      padding: const EdgeInsets.all(1.0),
      child: Container(
        color: AppColors.white,
        child: ExpansionTile(
          key: GlobalKey(),
          initiallyExpanded: 4 == selected,
          onExpansionChanged: (isOpen) {
            if (isOpen) {
              setState(() {
                selected = 4;
              });
            } else {
              setState(() {
                selected = 0;
              });
            }
          },
          title: Text(title,
              style: TextStyles.leadsTextStyleBold.copyWith(fontSize: 16)),
          collapsedIconColor: Colors.black,
          children: [
            const Divider(height: 1, thickness: 1, color: Colors.black12),
            getBodyYear4Row(),
          ],
        ),
      ),
    );
  }

  Widget expandTable5(String title) {
    return Padding(
      padding: const EdgeInsets.all(1.0),
      child: Container(
        color: AppColors.white,
        child: ExpansionTile(
          key: GlobalKey(),
          initiallyExpanded: 5 == selected,
          onExpansionChanged: (isOpen) {
            if (isOpen) {
              setState(() {
                selected = 5;
              });
            } else {
              setState(() {
                selected = 0;
              });
            }
          },
          title: Text(title,
              style: TextStyles.leadsTextStyleBold.copyWith(fontSize: 16)),
          collapsedIconColor: Colors.black,
          children: [
            const Divider(height: 1, thickness: 1, color: Colors.black12),
            getBodyYear5Row(),
          ],
        ),
      ),
    );
  }

  Widget expandTable6(String title) {
    return Padding(
      padding: const EdgeInsets.all(1.0),
      child: Container(
        color: AppColors.white,
        child: ExpansionTile(
          key: GlobalKey(),
          initiallyExpanded: 6 == selected,
          onExpansionChanged: (isOpen) {
            if (isOpen) {
              setState(() {
                selected = 6;
              });
            } else {
              setState(() {
                selected = 0;
              });
            }
          },
          title: Text(title,
              style: TextStyles.leadsTextStyleBold.copyWith(fontSize: 16)),
          collapsedIconColor: Colors.black,
          children: [
            const Divider(height: 1, thickness: 1, color: Colors.black12),
            getBodyYear6Row(),
          ],
        ),
      ),
    );
  }

  Widget expandTable7(String title) {
    return Padding(
      padding: const EdgeInsets.all(1.0),
      child: Container(
        color: AppColors.white,
        child: ExpansionTile(
          key: GlobalKey(),
          initiallyExpanded: 7 == selected,
          onExpansionChanged: (isOpen) {
            if (isOpen) {
              setState(() {
                selected = 7;
              });
            } else {
              setState(() {
                selected = 0;
              });
            }
          },
          title: Text(title,
              style: TextStyles.leadsTextStyleBold.copyWith(fontSize: 16)),
          collapsedIconColor: Colors.black,
          children: [
            const Divider(height: 1, thickness: 1, color: Colors.black12),
            getBodyYear7Row(),
          ],
        ),
      ),
    );
  }

  // getBodyYear1Row() {
  //   for (var i = 0; i < modelBannerResult.data.length; i++) {
  //     var link = modelBannerResult.data[i].image;
  //     listString.add(link);
  //   }
  //
  // }
  Widget getBodyYear1Row() => Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          for (var i = 0; i < controller.dataSourceTable1.length; i++)
            bodyTable(
                index: i, model: controller.dataSourceTable1[i], startYear: 0)
        ],
      );

  Widget getBodyYear2Row() => Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          for (var i = 0; i < controller.dataSourceTable2.length; i++)
            bodyTable(
                index: i, model: controller.dataSourceTable2[i], startYear: 5)
        ],
      );
  Widget getBodyYear3Row() => Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          for (var i = 0; i < controller.dataSourceTable3.length; i++)
            bodyTable(
                index: i, model: controller.dataSourceTable3[i], startYear: 10)
        ],
      );
  Widget getBodyYear4Row() => Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          for (var i = 0; i < controller.dataSourceTable4.length; i++)
            bodyTable(
                index: i, model: controller.dataSourceTable4[i], startYear: 15)
        ],
      );
  Widget getBodyYear5Row() => Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          for (var i = 0; i < controller.dataSourceTable5.length; i++)
            bodyTable(
                index: i, model: controller.dataSourceTable5[i], startYear: 20)
        ],
      );
  Widget getBodyYear6Row() => Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          for (var i = 0; i < controller.dataSourceTable6.length; i++)
            bodyTable(
                index: i, model: controller.dataSourceTable6[i], startYear: 25)
        ],
      );
  Widget getBodyYear7Row() => Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          for (var i = 0; i < controller.dataSourceTable7.length; i++)
            bodyTable(
                index: i, model: controller.dataSourceTable7[i], startYear: 30)
        ],
      );
  titleTable() {
    return Padding(
      padding: const EdgeInsets.all(0.0),
      child: Container(
        color: Colors.grey[100],
        // color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            titleMenuYearMonth("Year"),
            titleMenuYearMonth("Month"),
            titleMenu("Interest\nRate"),
            titleMenu("Disbursement Ratio\n(Property Price (%))"),
            titleMenu("Outstanding Principal\n(Start of Month)"),
            titleMenu("Monthly\nInstallment"),
            titleMenu("Interest\nPayment"),
            titleMenu("Principal\nPayment"),
            titleMenu("Outstanding Principal\n(End of Month)"),
          ],
        ),
      ),
    );
  }

  bodyTable(
      {required BucMortgageRepaymentTableModel model,
      required int index,
      int startYear = 0}) {
    return Container(
      height: 25,
      color: Colors.grey[100],
      // color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          contentBodyYear1(
              title: "Yr ${(index + 12) ~/ 12 + startYear}",
              isShow: ((index + 12) % 12) == 0),
          contentBodyMonths("${((index + 12) % 12) + 1}"),
          contentBody("${model.interestRate}%"),
          contentBody("${model.disbursement_ratio}%"),
          contentBody(convertToCurrency(model.outstanding_principal_start)),
          contentBody(convertToCurrency(model.monthly_installment)),
          contentBody(convertToCurrency(model.interest_payment)),
          contentBody(convertToCurrency(model.principal_payment)),
          contentBody(convertToCurrency(model.outstanding_principal_end)),
        ],
      ),
    );
  }

  //
  titleMenu(String title) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.all(1),
        child: Container(
          color: AppColors.white,
          height: 40,
          child: Padding(
              padding: const EdgeInsets.all(2.0),
              child: AutoSizeText(
                title,
                style: TextStyles.leadsColorTextStyle2
                    .copyWith(color: AppColors.kPrimaryColor, fontSize: 8),
                maxLines: 4,
                minFontSize: 6,
                maxFontSize: 6,
              )
              // child: Text(
              //   title,
              //   style: TextStyles.leadsColorTextStyle2
              //       .copyWith(color: AppColors.kPrimaryColor, fontSize: 8),
              // ),
              ),
        ),
      ),
    );
  }

  titleMenuYearMonth(String title) {
    return Padding(
      padding: const EdgeInsets.all(1),
      child: Container(
        color: AppColors.white,
        height: 40,
        width: (windowWidth / 9) / 2,
        child: Padding(
            padding: const EdgeInsets.all(2.0),
            child: AutoSizeText(
              title,
              style: TextStyles.leadsColorTextStyle2
                  .copyWith(color: AppColors.kPrimaryColor, fontSize: 8),
              maxLines: 1,
              minFontSize: 6,
              maxFontSize: 6,
            )
            // child: Text(
            //   title,
            //   style: TextStyles.leadsColorTextStyle2
            //       .copyWith(color: AppColors.kPrimaryColor, fontSize: 8),
            // ),
            ),
      ),
    );
  }

  contentBody(String title) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.all(1),
        child: Container(
          color: AppColors.white,
          height: 40,
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: Text(
              title,
              style: TextStyles.leadsColorTextStyle2
                  .copyWith(color: AppColors.grey625F5A, fontSize: 8),
            ),
          ),
        ),
      ),
    );
  }

  contentBodyYear1({String title = "", bool isShow = false}) {
    return isShow
        ? Padding(
            padding: const EdgeInsets.all(1),
            child: Container(
              color: AppColors.white,
              height: 40,
              width: (windowWidth / 9) / 2 - 1,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 5,horizontal: 2),
                child: Text(
                  title,
                  style: TextStyles.leadsColorTextStyle2
                      .copyWith(color: AppColors.grey625F5A, fontSize: 7),
                ),
              ),
            ),
          )
        : Padding(
            padding: const EdgeInsets.all(1),
            child: Container(
              color: AppColors.white,
              height: 40,
              width: (windowWidth / 9) / 2 - 1,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 5,horizontal: 2),
                child: Text(
                  "",
                  style: TextStyles.leadsColorTextStyle2
                      .copyWith(color: AppColors.grey625F5A, fontSize: 7),
                ),
              ),
            ),
          );
  }

  contentBodyMonths(String title) {
    return Padding(
      padding: const EdgeInsets.all(1),
      child: Container(
        color: AppColors.white,
        height: 40,
        width: (windowWidth / 9) / 2,
        child: Padding(
          padding: const EdgeInsets.all(4.0),
          child: Text(
            title,
            style: TextStyles.leadsColorTextStyle2
                .copyWith(color: AppColors.grey625F5A, fontSize: 8),
          ),
        ),
      ),
    );
  }

  contentBodyYear(String title) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.all(1),
        child: Container(
          color: AppColors.grey625F5A,
          height: 40,
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: Row(
              children: [
                Expanded(
                  flex: 2,
                  child: Container(
                    color: AppColors.white,
                    child: Text(
                      title,
                      style: TextStyles.leadsColorTextStyle2
                          .copyWith(color: AppColors.grey625F5A, fontSize: 9),
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    color: AppColors.white,
                    child: Text(
                      "1",
                      style: TextStyles.leadsColorTextStyle2
                          .copyWith(color: AppColors.grey625F5A, fontSize: 9),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  headerTitle(String title) {
    return Expanded(
      flex: 1,
      child: Padding(
        padding: const EdgeInsets.all(1),
        child: Container(
          color: AppColors.white,
          child: Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Text(
                title,
                style: TextStyles.leadsTextStyle1.copyWith(fontSize: 11),
                maxLines: 2,
              ),
            ),
          ),
        ),
      ),
    );
  }

  valueTitle(String value) {
    return Expanded(
      flex: 1,
      child: Padding(
        padding: const EdgeInsets.all(1),
        child: Container(
          color: AppColors.white,
          child: Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Text(
                  value,
                  style: TextStyles.leadsTextStyle1.copyWith(fontSize: 11),
                ),
              )),
        ),
      ),
    );
  }
}
