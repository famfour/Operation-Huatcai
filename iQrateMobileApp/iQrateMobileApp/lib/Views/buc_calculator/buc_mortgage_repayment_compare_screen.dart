import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Model/response_model.dart/list_all_packages_response_model_v2.dart';
import 'package:iqrate/Service/GetControllers.dart';
import 'package:iqrate/Utils/text_formatter.dart';
import 'package:iqrate/Controller/buc_mortgage_repayment_compare_controller.dart';
import 'package:iqrate/Views/buc_calculator/buc_package_card_details.dart';
import 'package:iqrate/Widgets/default_appbar.dart';
import '../../Controller/bank_submission_controller.dart';
import '../../DeviceManager/colors.dart';
import '../../DeviceManager/text_styles.dart';
import '../../Widgets/primary_button.dart';

class BucMortgageRepaymentCompareScreen extends StatefulWidget {
  const BucMortgageRepaymentCompareScreen({Key? key}) : super(key: key);

  @override
  State<BucMortgageRepaymentCompareScreen> createState() =>
      _BucMortgageRepaymentCompareScreenState();
}

class _BucMortgageRepaymentCompareScreenState
    extends State<BucMortgageRepaymentCompareScreen> {
  BucMortgageRepaymentCompareController controller =
      GetControllers.shared.getBucMortgageRepaymentCompareController();

  BankSubmissionController controllerBank =
      GetControllers.shared.getBankSubmissionController();
  int activeStep = 2; // Initial step set to 0.
  late double windowHeight;
  late double windowWidth;

  var showDetails = false;
  @override
  void initState() {
    super.initState();
    controller.getInterestRateComparisonModels();
    //chua lam
    // controller.arrayLoanDetail.value = getArrayContentLoanDetails();
  }

  @override
  Widget build(BuildContext context) {
    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: DefaultAppBar(
          title: "BUC Mortgage Repayment", windowHeight: windowHeight * 0.09),
      body: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: SingleChildScrollView(
          child: Column(
            children: [
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Padding(
                  padding: const EdgeInsets.only(
                      left: 20, top: 20, bottom: 20, right: 20),
                  child: Text(
                    "Rate Package Comparison",
                    style: TextStyles.leadsColorTextStyle2
                        .copyWith(color: AppColors.kPrimaryColor),
                  ),
                ),
                bodyTable(),
                const SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: Text(
                    "IQrate Lowest BUC Package",
                    style: TextStyles.leadsColorTextStyle2
                        .copyWith(color: AppColors.kPrimaryColor, fontSize: 16),
                  ),
                ),
                buildCardBank(),
                const SizedBox(
                  height: 30,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Center(
                    child: PrimaryButton(
                      windowHeight: windowHeight,
                      windowWidth: windowWidth,
                      buttonTitle: "Close",
                      onPressed: () {
                        Get.back();
                        //newPurchaseController.onTapBackStep3Lead();
                      },
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                )
              ]),
            ],
          ),
        ),
      ),
    );
  }

  bodyTable() {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Container(
        color: Colors.grey[100],
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            //row 1
            IntrinsicHeight(
              child: Row(
                children: [
                  Expanded(
                      child: Padding(
                    padding: const EdgeInsets.all(1),
                    child: Container(
                      height: 70,
                      color: AppColors.white,
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text(
                          "",
                          style: TextStyles.leadsColorTextStyle2.copyWith(
                              color: AppColors.kPrimaryColor, fontSize: 14),
                        ),
                      ),
                    ),
                  )),
                  // Container(color: AppColors.white,child: he),
                  controller.arrayModelCompare.isNotEmpty
                      ? Expanded(
                          child: Padding(
                          padding: const EdgeInsets.all(1),
                          child: Container(
                            height: 70,
                            color: AppColors.white,
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 10, horizontal: 5),
                              child: Text(
                                controller.arrayModelCompare[0].name ?? "",
                                style: TextStyles.leadsColorTextStyle2.copyWith(
                                    color: AppColors.kPrimaryColor,
                                    fontSize: 14),
                              ),
                            ),
                          ),
                        ))
                      : const SizedBox(),

                  controller.arrayModelCompare.length > 1
                      ? Expanded(
                          child: Padding(
                          padding: const EdgeInsets.all(1),
                          child: Container(
                            height: 70,
                            color: AppColors.white,
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 10, horizontal: 5),
                              child: Text(
                                controller.arrayModelCompare[1].name ?? "",
                                style: TextStyles.leadsColorTextStyle2.copyWith(
                                    color: AppColors.kPrimaryColor,
                                    fontSize: 14),
                              ),
                            ),
                          ),
                        ))
                      : const SizedBox(),
                ],
              ),
            ),

            // row2
            // IntrinsicHeight(
            //   child: Row(
            //     crossAxisAlignment: CrossAxisAlignment.stretch,
            //     children: [
            //       headerTitle("Monthly Installment"),
            //       valueTitle(convertToCurrency(
            //           controller.getModelCompare1().monthlyInstallment)),
            //       if (controller.modelCompare2 != null)
            //         valueTitle(convertToCurrency(
            //             controller.getModelCompare2().monthlyInstallment)),
            //     ],
            //   ),
            // ),
            IntrinsicHeight(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  headerTitle("Total Payment"),
                  valueTitle(convertToCurrency(
                      controller.arrayModelCompare[0].model?.totalPayment ??
                          "")),
                  if (controller.arrayModelCompare.length > 1)
                    valueTitle(convertToCurrency(
                        controller.arrayModelCompare[1].model?.totalPayment ??
                            "")),
                ],
              ),
            ),

            IntrinsicHeight(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  headerTitle("Total Principal"),
                  valueTitle(convertToCurrency(
                      controller.arrayModelCompare[0].model?.totalPrincipal ??
                          "")),
                  if (controller.arrayModelCompare.length > 1)
                    valueTitle(convertToCurrency(
                        controller.arrayModelCompare[1].model?.totalPrincipal ??
                            "")),
                ],
              ),
            ),
            IntrinsicHeight(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  headerTitle("Total Interest"),
                  valueTitle(convertToCurrency(
                      controller.arrayModelCompare[0].model?.totalInterest ??
                          "")),
                  if (controller.arrayModelCompare.length > 1)
                    valueTitle(convertToCurrency(
                        controller.arrayModelCompare[1].model?.totalInterest ??
                            "")),
                ],
              ),
            ),
            controller.arrayModelCompare.first.model?.yearlyInterest != null
                ? getYearlyFirsRow()
                : const SizedBox(),
            // controller.arrayModelCompare.length> .model?.yearlyInterest != null ? getYearlyFirsRow() : const SizedBox(),
            // controller.arrayModelCompare.first.model?.yearlyInterest != null
            //     ? IntrinsicHeight(
            //         child: Row(
            //           crossAxisAlignment: CrossAxisAlignment.stretch,
            //           children: [
            //             headerTitle("Year 1"),
            //             //cot 1
            //             // valueTitle(convertToCurrency(
            //             //     controller.getModelCompare1().totalInterest)),
            //             valueTitle(
            //                 "${controller.arrayModelCompare[0].model?.yearlyInterest![0]} %"),
            //             if (controller.arrayModelCompare.length > 1)
            //               valueTitle(
            //                   "${controller.arrayModelCompare[1].model?.yearlyInterest![0]} %"),
            //             // if (controller.modelCompare2 != null)
            //             //   valueTitle(convertToCurrency(
            //             //       controller.getModelCompare2().totalInterest)
            //             //   ),
            //           ],
            //         ),
            //       )
            //     : const SizedBox(),

            // controller.arrayModelCompare.first.model?.yearlyInterest != null
            //     ? IntrinsicHeight(
            //         child: Row(
            //           crossAxisAlignment: CrossAxisAlignment.stretch,
            //           children: [
            //             headerTitle("Year 2"),
            //             valueTitle(
            //                 "${controller.arrayModelCompare.first.model?.yearlyInterest![1]} %"),
            //             if (controller.arrayModelCompare.length > 1)
            //               valueTitle(
            //                   "${controller.arrayModelCompare[1].model?.yearlyInterest![1]} %"),
            //           ],
            //         ),
            //       )
            //     : const SizedBox(),
            // controller.arrayModelCompare.first.model?.yearlyInterest != null
            //     ? IntrinsicHeight(
            //         child: Row(
            //           crossAxisAlignment: CrossAxisAlignment.stretch,
            //           children: [
            //             headerTitle("Year 3"),
            //             valueTitle(
            //                 "${controller.arrayModelCompare.first.model?.yearlyInterest![2]} %"),
            //             if (controller.arrayModelCompare.length > 1)
            //               valueTitle(
            //                   "${controller.arrayModelCompare[1].model?.yearlyInterest![2]} %"),
            //           ],
            //         ),
            //       )
            //     : const SizedBox(),
            // controller.arrayModelCompare.first.model?.yearlyInterest != null
            //     ? IntrinsicHeight(
            //         child: Row(
            //           crossAxisAlignment: CrossAxisAlignment.stretch,
            //           children: [
            //             headerTitle("Year 4"),
            //             valueTitle(
            //                 "${controller.arrayModelCompare.first.model?.yearlyInterest![3]} %"),
            //             if (controller.arrayModelCompare.length > 1)
            //               valueTitle(
            //                   "${controller.arrayModelCompare[1].model?.yearlyInterest![3]} %"),
            //           ],
            //         ),
            //       )
            //     : const SizedBox(),
            // controller.arrayModelCompare.first.model?.yearlyInterest != null
            //     ? IntrinsicHeight(
            //         child: Row(
            //           crossAxisAlignment: CrossAxisAlignment.stretch,
            //           children: [
            //             headerTitle("Year 5"),
            //             valueTitle(
            //                 "${controller.arrayModelCompare.first.model?.yearlyInterest![4]} %"),
            //             if (controller.arrayModelCompare.length > 1)
            //               valueTitle(
            //                   "${controller.arrayModelCompare[1].model?.yearlyInterest![4]} %"),
            //           ],
            //         ),
            //       )
            //     : const SizedBox(),
            // controller.arrayModelCompare.first.model?.yearlyInterest != null
            //     ? IntrinsicHeight(
            //         child: Row(
            //           crossAxisAlignment: CrossAxisAlignment.stretch,
            //           children: [
            //             headerTitle("Thereafter"),
            //             valueTitle(
            //                 "${controller.arrayModelCompare.first.model?.yearlyInterest![5]} %"),
            //             if (controller.arrayModelCompare.length > 1)
            //               valueTitle(
            //                   "${controller.arrayModelCompare[1].model?.yearlyInterest![5]} %"),
            //           ],
            //         ),
            //       )
            //     : const SizedBox(),
          ],
        ),
      ),
    );
  }

  Widget getYearlyFirsRow() {
    var array = controller.arrayModelCompare.first.model?.yearlyInterest;
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        for (var i = 0; i < array!.length; i++)
          IntrinsicHeight(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                headerTitle(i < 5 ? "Year ${i + 1}" : "Thereafter"),
                valueTitle(
                    "${controller.arrayModelCompare.first.model?.yearlyInterest![i]} %"),
                if (controller.arrayModelCompare.length > 1)
                  valueTitle(
                      "${controller.arrayModelCompare[1].model?.yearlyInterest![i]} %"),
              ],
            ),
          )
      ],
    );
  }

  headerTitle(String title) {
    return Expanded(
      flex: 1,
      child: Padding(
        padding: const EdgeInsets.all(1),
        child: Container(
          color: AppColors.white,
          child: Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Text(
                title,
                style: TextStyles.leadsTextStyle1.copyWith(fontSize: 12),
                maxLines: 2,
              ),
            ),
          ),
        ),
      ),
    );
  }

  valueTitle(String value) {
    return Expanded(
      flex: 1,
      child: Padding(
        padding: const EdgeInsets.all(1),
        child: Container(
          color: AppColors.white,
          child: Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Text(
                  value,
                  style: TextStyles.leadsTextStyle1.copyWith(fontSize: 12),
                ),
              )),
        ),
      ),
    );
  }

  buildCardBank() {
    return Container(
      color: Colors.white,
      //padding: EdgeInsets.all(windowHeight * 0.025),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: windowHeight * 0.02),
            controller.dataIQrateLowestBucPackage == null
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                          height: windowHeight * 0.15,
                          child: SvgPicture.asset('assets/images/nodata.svg')),
                      Text(
                        'No data to show',
                        style: TextStyles.bankSubmissionCardTitleDisabled,
                      )
                    ],
                  )
                : taskCard()
          ],
        ),
      ),
    );
  }
  //
  // taskCard(
  //     String title, String task, BankSubmissionOneBank bankSubmissionOneBank) {
  //   return InkWell(
  //     onTap: () {
  //       Get.toNamed(bankSubmissionBankTaskScreen,
  //           arguments: [bankSubmissionOneBank]);
  //     },
  //     child: Padding(
  //       padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 5),
  //       child: Card(
  //         elevation: 5,
  //         shape:
  //             RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
  //         child: Padding(
  //           padding: const EdgeInsets.all(20.0),
  //           child: Row(
  //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //             mainAxisSize: MainAxisSize.max,
  //             children: [
  //               Column(
  //                 mainAxisAlignment: MainAxisAlignment.center,
  //                 mainAxisSize: MainAxisSize.min,
  //                 crossAxisAlignment: CrossAxisAlignment.start,
  //                 children: [
  //                   Padding(
  //                     padding: const EdgeInsets.symmetric(vertical: 10),
  //                     child: Text(
  //                       title,
  //                       style: TextStyles.bankSubmissionCardTitle,
  //                     ),
  //                   ),
  //                   //Floating (Exclusive)
  //                   Padding(
  //                     padding: const EdgeInsets.symmetric(vertical: 10),
  //                     child: Text(
  //                       "Floating (Exclusive)",
  //                       style: TextStyles.bankSubmissionCardTitle,
  //                     ),
  //                   ),
  //                   // SizedBox(height: windowHeight * 0.03),
  //                   // task == '2/2'
  //                   //     ? Text(
  //                   //         task + ' Task completed',
  //                   //         style:
  //                   //             TextStyles.bankSubmissionCompletedTaskTextStyle,
  //                   //       )
  //                   //     : Text(
  //                   //         task + ' Task',
  //                   //         style: TextStyles.bankSubmissionTaskTextStyle,
  //                   //       ),
  //                 ],
  //               ),
  //               const Icon(
  //                 Icons.chevron_right,
  //                 color: Colors.black,
  //               )
  //             ],
  //           ),
  //         ),
  //       ),
  //     ),
  //   );
  // }

  taskCard() {
    return Card(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15))),
      elevation: 5,
      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Column(
        children: [rowOne(), detailsButton()],
      ),
    );
  }

  Widget rowOne() {
    return Padding(
        padding: const EdgeInsets.symmetric(vertical: 5.0),
        child: ListTile(
          leading: controller.dataIQrateLowestBucPackage["details"]["bank"]
                      ["logo"] !=
                  null
              ? Image.network(controller.dataIQrateLowestBucPackage["details"]
                      ["bank"]["logo"]
                  .toString())
              : const SizedBox(),
          title: Text(
            getBankName(),
            style: TextStyles.cardHeading,
          ),
          subtitle: Text(
            controller.dataIQrateLowestBucPackage['details']['rate_type']
                    .toString()
                    .replaceAll('_', ' ')
                    .capitalizeFirst! +
                ' (${controller.dataIQrateLowestBucPackage['details']['rate_category'].toString().capitalizeFirst!})',
            style: TextStyles.cardSubHeading,
          ),
          trailing: Padding(
            padding: const EdgeInsets.only(right: 20),
            child: Container(
                padding: const EdgeInsets.all(5),
                decoration: BoxDecoration(
                    border: Border.all(
                  color: Colors.grey,
                )),
                child: RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(
                        style: TextStyles.cardHeading.copyWith(fontSize: 16),
                        text: "Year 1 : ",
                      ),
                      TextSpan(
                        style: TextStyles.cardHeading.copyWith(fontSize: 16),
                        text: getYear1Rate(),
                      ),
                    ],
                  ),
                )),
          ),
        ));
  }

  // A simple widget used in the 2nd row, which only shows a title and a value
  Widget titleValueRow({required String title, required String value}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Text(
          title + ': ',
          style: TextStyles.cardHeading.copyWith(fontSize: 16),
        ),
        Text(value, style: TextStyles.cardHeading.copyWith(fontSize: 16))
      ],
    );
  }

  // This sections expands to show additional details in the specific
  Widget detailsButton() {
    var json = controller.dataIQrateLowestBucPackage["details"];
    var item = PackageModel.fromJson(json);
    return Obx(() => controller.isExpanded.value == false
        ? InkWell(
            onTap: () {
              controller.isExpanded.value = true;
            },
            child: ClipRRect(
              borderRadius: const BorderRadius.only(
                  bottomLeft: Radius.circular(15),
                  bottomRight: Radius.circular(15)),
              child: Container(
                  color: AppColors.kSecondaryColor,
                  child: Column(
                    children: [
                      Center(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'Details',
                            style: TextStyles.detailsButton,
                          ),
                        ),
                      ),
                    ],
                  )),
            ),
          )
        : detailsSection(item));
  }

  detailsSection(PackageModel model) {
    // return Container(color: Colors.green, height: 100,);
    return BucPackageCardDetails(
      cardIndex: 0,
      data: model,
      onChanged: (onChange) {},
    );
  }

  //
  //
  // Widget expansionSection(int index) {
  //   return Obx(
  //         () => InkWell(
  //       onTap: () {
  //         // bankSubmissionController.expandingCard(index);
  //       },
  //       child: ClipRRect(
  //         borderRadius: const BorderRadius.only(
  //             bottomLeft: Radius.circular(15),
  //             bottomRight: Radius.circular(15)),
  //         child: Container(
  //           color: AppColors.kSecondaryColor,
  //           child: bankSubmissionController.selectedIndex.value == index
  //               ? Container(
  //             color: Colors.white,
  //             child: Column(
  //               crossAxisAlignment: CrossAxisAlignment.end,
  //               children: [
  //                 InkWell(
  //                     onTap: () {
  //                       //  close the expansion pannel on tapping here
  //                       bankSubmissionController.reset();
  //                     },
  //                     child: const Padding(
  //                       padding: EdgeInsets.only(bottom: 10.0),
  //                       child: Icon(Icons.minimize),
  //                     )),
  //                 ListView(
  //                   shrinkWrap: true,
  //                   children: [
  //                     expandedSectionHeading(
  //                         'Rates', [const Text('Enjoy!')]),
  //                     expandedSectionHeading(
  //                         'Key Features', [const Text('Enjoy!')]),
  //                     expandedSectionHeading(
  //                         "Bank's Subsidy", [const Text('Enjoy!')]),
  //                     expandedSectionHeading('Early Repayment Penalty',
  //                         [const Text('Enjoy!')]),
  //                   ],
  //                 )
  //               ],
  //             ),
  //           )
  //               : Center(
  //             child: Padding(
  //               padding: const EdgeInsets.all(8.0),
  //               child: Text(
  //                 'Details',
  //                 style: TextStyles.detailsButton,
  //               ),
  //             ),
  //           ),
  //         ),
  //       ),
  //     ),
  //   );
  // }
  //
  //
  getBankName() {
    var bankName = controller.dataIQrateLowestBucPackage["name"];
    var isMask =
        controller.dataIQrateLowestBucPackage["details"]["mask"] == true;
    var firstChar = bankName[0];
    var hideBankName = firstChar + "*****";

    var finalName = isMask ? hideBankName : bankName;
    debugPrint("finalName $finalName");
    return finalName;
  }

  //logo
  getLogoBank() {
    var url = controller.dataIQrateLowestBucPackage["details"]["bank"]["logo"];
    // return url;
    debugPrint("url $url");
    return url;
  }

  getYear1Rate() {
    var data = controller.dataIQrateLowestBucPackage["interest"];
    var finalRate = data.toString() + "%";
    debugPrint("finalRate $finalRate");
    return finalRate;
  }
}
