import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/buc_mortgage_repayment_controller.dart';
import 'package:iqrate/DeviceManager/screen_constants.dart';
import 'package:iqrate/Service/GetControllers.dart';
import 'package:iqrate/Utils/text_field_custom.dart';
import 'package:iqrate/Utils/text_formatter.dart';
import 'package:iqrate/Views/buc_calculator/buc_progressive_repayment_schedule_screen.dart';
import 'package:iqrate/Widgets/default_appbar.dart';
import 'package:iqrate/Widgets/form_field_title.dart';
import 'package:iqrate/Widgets/primary_button.dart';

import '../../DeviceManager/colors.dart';
import '../../DeviceManager/container_styles.dart';
import '../../DeviceManager/text_styles.dart';
import '../../Widgets/require_text_field.dart';
import '../../Widgets/secondary_button.dart';
import '../calculator/your_preferred_interest_rate.dart';
import '../rates/currency_amount_textField.dart';

class BucMortgageRepaymentScreen extends StatefulWidget {
  const BucMortgageRepaymentScreen({Key? key}) : super(key: key);

  @override
  State<BucMortgageRepaymentScreen> createState() =>
      _BucMortgageRepaymentScreenState();
}

class _BucMortgageRepaymentScreenState
    extends State<BucMortgageRepaymentScreen> {
  BucMortgageRepaymentController controller =
      GetControllers.shared.getBucMortgageRepaymentController();
  int activeStep = 0;
  late double windowHeight;
  late double windowWidth;
  final _formKey = GlobalKey<FormState>();
  @override
  void initState() {
    super.initState();
  }

  var formKey = GlobalKey<FormState>();

  addYourPreferredInterestRateWidget() {
    var newIndex = controller.arrayYourPreferredInterestRateWidget.length;
    var item = YourPreferredInterestRateWidget(
      index: newIndex,
      showIndexOrder: false,
      removeMe: (index) {
        debugPrint("remove at $index");
        controller.arrayYourPreferredInterestRateWidget.removeAt(index);
        setState(() {});
      },
    );
    controller.arrayYourPreferredInterestRateWidget.add(item);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: DefaultAppBar(
          title: "BUC Mortgage Repayment", windowHeight: windowHeight * 0.09),
      body: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 20,
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: Row(
                  children: [
                    Container(
                      height: 30,
                      width: 30,
                      child: Center(
                        child: Text(
                          '1',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: FontSize.s15,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                      decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          color: AppColors.kPrimaryColor),
                    ),
                    Expanded(
                      child: Container(
                        height: 1,
                        color: Colors.grey,
                      ),
                    ),
                    Container(
                      height: 30,
                      width: 30,
                      child: Center(
                        child: Text(
                          '2',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: FontSize.s15,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                      decoration: const BoxDecoration(
                          shape: BoxShape.circle, color: AppColors.greyA8A8A8),
                    ),
                    Expanded(
                      child: Container(
                        height: 1,
                        color: Colors.grey,
                      ),
                    ),
                    Container(
                      height: 30,
                      width: 30,
                      child: Center(
                        child: Text(
                          '3',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: FontSize.s15,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                      decoration: const BoxDecoration(
                          shape: BoxShape.circle, color: AppColors.greyA8A8A8),
                    ),
                  ],
                ),
              ),
              // NumberStepper(
              //   enableNextPreviousButtons: false,
              //   enableStepTapping: false,
              //   stepRadius: 15,
              //   lineLength: windowWidth * 0.35, //
              //   lineDotRadius: 0.1,
              //   stepPadding: 0,
              //   activeStepColor: AppColors.kPrimaryColor,
              //   activeStepBorderPadding: 0,
              //   activeStepBorderColor: AppColors.kPrimaryColor,
              //   stepColor: const Color(0XFFD0C8C9),
              //   numbers: const [1, 2, 3],
              //   numberStyle: TextStyle(
              //     color: Colors.white,
              //     fontSize: FontSize.s18,
              //     fontWeight: FontWeight.w400,
              //   ),
              //   activeStep: activeStep,
              //
              //   // This ensures step-tapping updates the activeStep.
              //   onStepReached: (index) {
              //     setState(() {
              //       activeStep = index;
              //     });
              //   },
              // ),
              const SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('BUC Mortgage Repayment',
                        style: TextStyles.calculatorHeadingsTextStyle),
                    const SizedBox(
                      height: 20,
                    ),
                    const FormFieldTitle(
                      title: 'Name*',
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    RequireTextFieldV2(
                      labelText: "Name",
                      type: TypeRequireTextFieldV2.fullname,
                      controller: controller.nameController,
                      formKey: _formKey,
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    const FormFieldTitle(title: 'Property Price*'),
                    const SizedBox(
                      height: 10,
                    ),
                    CurrencyAmountTextField(
                      textEditingController: controller.propertyPriceController,
                      labelText: 'Property Price',
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    const FormFieldTitle(title: 'Loan to Value %*'),
                    const SizedBox(
                      height: 10,
                    ),
                    // RequireTextFieldV2(
                    //   labelText: "Loan to Value",
                    //   hintText: "Loan to Value",
                    //   type: TypeRequireTextFieldV2.percent,
                    //   controller: controller.loanToValueController,
                    //   formKey: _formKey,
                    // ),
                    RequireTextField(
                      labelText: "Loan to Value",
                      hintText: "Loan to Value",
                      type: Type.percentageWithDecimal,
                      validator: GetControllers.shared
                          .validateRateValueRequired, // validate by popup
                      controller: controller.loanToValueController,
                      //formKey: formKey,
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    const FormFieldTitle(title: 'Loan Tenure*'),
                    const SizedBox(
                      height: 10,
                    ),
                    RequireTextFieldV2(
                      labelText: "Loan Tenure",
                      hintText: "Loan Tenure",
                      type: TypeRequireTextFieldV2.numberInt,
                      controller: controller.loanTenureController,
                      formKey: formKey,
                      validator:
                          GetControllers.shared.validateForNumberIntMaxis30,
                    ),

                    const SizedBox(
                      height: 10,
                    ),

                    const FormFieldTitle(title: 'CPF Available*'),
                    const SizedBox(
                      height: 10,
                    ),
                    CurrencyAmountTextField(
                      textEditingController: controller.cpfAvailableController,
                      labelText: "CPF Available",
                    ),
                    // RequireTextField(
                    //     labelText: "CPF Available",
                    //     type: Type.number,
                    //     controller: controller.cpfAvailableController),
                    // const SizedBox(
                    //   height: 20,
                    // ),
                    controller.arrayYourPreferredInterestRateWidget.isNotEmpty
                        ? const SizedBox(
                            height: 20,
                          )
                        : const SizedBox(),
                    buildListYourPreferredInterestRateWidget(),
                    Obx(() => controller.isCalculated.value
                        ? Card(
                            color: AppColors.greyDEDEDE,
                            shape: const RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10))),
                            elevation: 5,
                            margin: const EdgeInsets.symmetric(
                                horizontal: 5, vertical: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 15, top: 15, bottom: 5),
                                  child: Text(
                                    "Down Payment",
                                    style:
                                        TextStyles.bankSubmissionTaskTextStyle1,
                                  ),
                                ),
                                const Divider(color: Colors.deepOrange),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 15.0, top: 10, bottom: 20),
                                  child: Text(
                                    convertToCurrency(
                                        controller.resultDownPayment.value),
                                    style: TextStyles.bankSubmissionBody,
                                  ),
                                ),
                              ],
                            ),
                          )
                        : const SizedBox()),
                    Obx(() => controller.isCalculated.value
                        ? const SizedBox(
                            height: 20,
                          )
                        : const SizedBox()),
                    Obx(() => controller.isCalculated.value
                        ? Card(
                            color: AppColors.greyDEDEDE,
                            shape: const RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10))),
                            elevation: 5,
                            margin: const EdgeInsets.symmetric(
                                horizontal: 5, vertical: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 15, top: 15, bottom: 5),
                                  child: Text(
                                    "Loan Amount",
                                    style:
                                        TextStyles.bankSubmissionTaskTextStyle1,
                                  ),
                                ),
                                const Divider(color: Colors.deepOrange),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 15.0, top: 10, bottom: 20),
                                  child: Text(
                                    convertToCurrency(
                                        controller.resultLoanAmount.value),
                                    style: TextStyles.bankSubmissionBody,
                                  ),
                                ),
                              ],
                            ),
                          )
                        : const SizedBox()),
                    const SizedBox(
                      height: 10,
                    ),
                    // buildCard("hello", controller.downPayment.value),

                    const SizedBox(
                      height: 20,
                    ),
                    Obx(() => !controller.isCalculated.value
                        ? PrimaryButton(
                            windowHeight: windowHeight,
                            windowWidth: windowWidth,
                            buttonTitle: "Calculate",
                            onPressed: () {
                              debugPrint("Calculate clicked ");
                              // controller.callAPICalculatorMortgageRepayment();
                              hideKeyboard();
                              if (controller.validateTextFieldDownPayment()) {
                                controller
                                    .callAPICalculatorBucMortgageDownpayment(
                                        context);
                              }
                              // else {
                              //   _formKey.currentState!.validate();
                              // }
                            },
                          )
                        : const SizedBox()),
                    Obx(() => controller.isCalculated.value
                        ? Column(
                            children: [
                              PrimaryButton(
                                  windowHeight: windowHeight,
                                  windowWidth: windowWidth,
                                  buttonTitle: "Continue",
                                  onPressed: () {
                                    if (int.tryParse(controller
                                                .loanToValueController.text)! >
                                            55 &&
                                        int.tryParse(controller
                                                .loanTenureController.text)! >
                                            30) {
                                      Future.delayed(
                                          const Duration(milliseconds: 0), () {
                                        FocusManager.instance.primaryFocus!
                                            .unfocus();
                                        Fluttertoast.showToast(
                                          timeInSecForIosWeb: 3,
                                          msg:
                                              "Loan tenure should be less than 30 years",
                                          backgroundColor: Colors.white,
                                          toastLength: Toast.LENGTH_LONG,
                                          gravity: ToastGravity.BOTTOM,
                                          textColor: Colors.black,
                                          fontSize: 20.0,
                                        );
                                      });
                                    } else {
                                      setState(
                                        () {
                                          hideKeyboard();
                                          debugPrint("go next screen");
                                          // controller.callAPICalculatorBucMortgageReport();
                                          if (controller
                                              .validateTextFieldB4Continue()) {
                                            Get.to(() =>
                                                const ProgressiveRepaymentSchedule());
                                          }
                                        },
                                      );
                                    }
                                  }),
                            ],
                          )
                        : const SizedBox()),

                    const SizedBox(
                      height: 20,
                    ),
                    controller.arrayYourPreferredInterestRateWidget.isEmpty
                        ? SecondaryButton(
                            windowHeight: windowHeight,
                            windowWidth: windowWidth,
                            buttonTitle: "Add Preferred Interest Rate",
                            onPressed: () {
                              debugPrint("Add Preferred Interest Rate");
                              //validate
                              addYourPreferredInterestRateWidget();
                            },
                            kGradientBoxDecoration: ContainerStyles
                                .kGradientBoxDecorationSecondaryButton,
                            kInnerDecoration:
                                ContainerStyles.kInnerDecorationSecondaryButton,
                          )
                        : const SizedBox(),

                    controller.arrayYourPreferredInterestRateWidget.isEmpty
                        ? const SizedBox(
                            height: 20,
                          )
                        : const SizedBox(),
                    Obx(() => controller.isCalculated.value
                        ? SecondaryButton(
                            windowHeight: windowHeight,
                            windowWidth: windowWidth,
                            buttonTitle: "Calculate",
                            onPressed: () {
                              debugPrint('Calculator again');
                              controller
                                  .callAPICalculatorBucMortgageDownpayment(
                                      context);
                              // controller.reset();
                              // setState(() {});
                            },
                            kGradientBoxDecoration: ContainerStyles
                                .kGradientBoxDecorationSecondaryButton,
                            kInnerDecoration:
                                ContainerStyles.kInnerDecorationSecondaryButton,
                          )
                        : const SizedBox()),
                    const SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  hideKeyboard() {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }

  Widget buildListYourPreferredInterestRateWidget() => Padding(
        padding: const EdgeInsets.only(bottom: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            for (var i = 0;
                i < controller.arrayYourPreferredInterestRateWidget.length;
                i++)
              controller.arrayYourPreferredInterestRateWidget[i]
          ],
        ),
      );
  Widget buildCard(String title, dynamic num) {
    return Card(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10))),
      elevation: 5,
      margin: const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 15, top: 15, bottom: 5),
            child: Text(
              title,
              style: TextStyles.bankSubmissionTaskTextStyle1,
            ),
          ),
          const Divider(color: Colors.deepOrange),
          Padding(
            padding: const EdgeInsets.only(left: 15.0, top: 10, bottom: 20),
            child: Text(
              convertToCurrency(num),
              style: TextStyles.bankSubmissionBody,
            ),
          ),
        ],
      ),
    );
  }
}
