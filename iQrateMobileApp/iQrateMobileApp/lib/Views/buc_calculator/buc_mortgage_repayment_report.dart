import 'dart:io';
import 'dart:isolate';
import 'dart:ui';

import 'package:android_path_provider/android_path_provider.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/screen_constants.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Service/GetControllers.dart';
import 'package:iqrate/Utils/text_formatter.dart';
import 'package:iqrate/Views/buc_calculator/buc_mortgage_progressive_repayment_table_screen.dart';
import 'package:iqrate/Views/buc_calculator/buc_mortgage_repayment_compare_screen.dart';
import 'package:iqrate/Views/buc_calculator/buc_mortgage_repayment_table_screen.dart';
import 'package:iqrate/Widgets/default_appbar.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:logger/logger.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../Controller/buc_mortgage_repayment_report_controller.dart';

class BucMortgageRepaymentReport extends StatefulWidget {
  const BucMortgageRepaymentReport({Key? key}) : super(key: key);

  @override
  State<BucMortgageRepaymentReport> createState() =>
      _BucMortgageRepaymentReportState();
}

class _BucMortgageRepaymentReportState
    extends State<BucMortgageRepaymentReport> {
  BucMortgageRepaymentReportController controller =
      GetControllers.shared.getBucMortgageRepaymentReportController();
  int activeStep = 2;
  late double windowHeight;
  late double windowWidth;

  @override
  void initState() {
    super.initState();
    // debugPrint(
    //     "done BucMortgageRepaymentReport with data ${GetControllers.shared.getBucMortgageRepaymentReportController().loadDetailsDataResult}");
    // prepareSaveDir();
  }

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: DefaultAppBar(
          title: "BUC Mortgage Repayment", windowHeight: windowHeight * 0.09),
      body: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: SingleChildScrollView(
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            const SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: Row(
                children: [
                  Container(
                    height: 30,
                    width: 30,
                    child: Center(
                      child: Text(
                        '1',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: FontSize.s15,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                    decoration: const BoxDecoration(
                        shape: BoxShape.circle, color: AppColors.kPrimaryColor),
                  ),
                  Expanded(
                    child: Container(
                      height: 1,
                      color: Colors.grey,
                    ),
                  ),
                  Container(
                    height: 30,
                    width: 30,
                    child: Center(
                      child: Text(
                        '2',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: FontSize.s15,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                    decoration: const BoxDecoration(
                        shape: BoxShape.circle, color: AppColors.kPrimaryColor),
                  ),
                  Expanded(
                    child: Container(
                      height: 1,
                      color: Colors.grey,
                    ),
                  ),
                  Container(
                    height: 30,
                    width: 30,
                    child: Center(
                      child: Text(
                        '3',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: FontSize.s15,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                    decoration: const BoxDecoration(
                        shape: BoxShape.circle, color: AppColors.kPrimaryColor),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Colors.black12,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 20, top: 20, bottom: 20, right: 20),
                          child: Text(
                            "BUC Mortgage Repayment Report",
                            style: TextStyles.leadsColorTextStyle2,
                          ),
                        ),
                        expandableLoanDetails("Loan Details"),
                        menuMortgageRepaymentReportGo(
                            index: 0,
                            title: "Rate Package Comparison",
                            onTap: () {
                              debugPrint(
                                  " click (BUC Rate Package Comparison) ${controller.loadDetailsDataResult['IQrate Lowest Buc Package']}");

                              if (controller.loadDetailsDataResult[
                                          'IQrate Lowest Buc Package']
                                      .toString() ==
                                  "[]") {
                                Future.delayed(const Duration(milliseconds: 0),
                                    () {
                                  FocusManager.instance.primaryFocus!.unfocus();
                                  Fluttertoast.showToast(
                                    timeInSecForIosWeb: 3,
                                    msg:
                                        "No data found, please try again later",
                                    backgroundColor: Colors.red,
                                    toastLength: Toast.LENGTH_LONG,
                                    gravity: ToastGravity.BOTTOM,
                                    textColor: Colors.white,
                                    fontSize: 20.0,
                                  );
                                });

                                return;
                              }

                              // debugPrint(" click BUC Rate Package Comparison");
                              debugPrint(
                                  " click BUC Rate Package Comparison ${controller.loadDetailsDataResult['Rate Package Comparison']}");
                              GetControllers.shared
                                  .getBucMortgageRepaymentCompareController()
                                  .interestRateComparisonData = controller
                                      .loadDetailsDataResult[
                                  'Rate Package Comparison'];
                              GetControllers.shared
                                  .getBucMortgageRepaymentCompareController()
                                  .dataIQrateLowestBucPackage = controller
                                      .loadDetailsDataResult[
                                  'IQrate Lowest Buc Package'];
                              Get.to(() =>
                                  const BucMortgageRepaymentCompareScreen());
                            }),
                        menuMortgageRepaymentReportGo(
                            index: 1,
                            title: "Progressive Repayment Schedule",
                            onTap: () {
                              debugPrint(
                                  " click (Progressive Repayment Schedule) ${controller.loadDetailsDataResult['Disbursement Schedule']}");

                              if (controller.loadDetailsDataResult[
                                          'Disbursement Schedule']
                                      .toString() ==
                                  "[]") {
                                Future.delayed(const Duration(milliseconds: 0),
                                    () {
                                  FocusManager.instance.primaryFocus!.unfocus();
                                  Fluttertoast.showToast(
                                    timeInSecForIosWeb: 3,
                                    msg:
                                        "No data found, please try again later",
                                    backgroundColor: Colors.red,
                                    toastLength: Toast.LENGTH_LONG,
                                    gravity: ToastGravity.BOTTOM,
                                    textColor: Colors.white,
                                    fontSize: 20.0,
                                  );
                                });

                                return;
                              }

                              var a = controller.loadDetailsDataResult[
                                  'Disbursement Schedule'];
                              // debugPrint(a.toString());
                              Logger().d(a);
                              GetControllers.shared
                                  .getBucMortgageProgressiveRepaymentTableScreenController()
                                  .disbursementSchedule = a;
                              Get.to(() =>
                                  const BucProgressiveRepaymentTableScreen());
                            }),
                        menuMortgageRepaymentReportGo(
                            index: 2,
                            title: "BUC Mortgage Repayment Table",
                            subString: "(IQrate Exclusive BUC Rate)",
                            onTap: () {
                              debugPrint(
                                  " click (IQrate Exclusive BUC Rate) ${controller.loadDetailsDataResult['IQrate Exclusive BUC Rate']}");

                              if (controller.loadDetailsDataResult[
                                          'IQrate Exclusive BUC Rate']
                                      .toString() ==
                                  "[]") {
                                Future.delayed(const Duration(milliseconds: 0),
                                    () {
                                  FocusManager.instance.primaryFocus!.unfocus();
                                  Fluttertoast.showToast(
                                    timeInSecForIosWeb: 3,
                                    msg:
                                        "No data found, please try again later",
                                    backgroundColor: Colors.red,
                                    toastLength: Toast.LENGTH_LONG,
                                    gravity: ToastGravity.BOTTOM,
                                    textColor: Colors.white,
                                    fontSize: 20.0,
                                  );
                                });

                                return;
                              }

                              controller.viewTable1 = true;
                              Get.to(() =>
                                  const BucMortgageRepaymentTableScreen());
                            }),
                        if (GetControllers.shared
                            .getBucMortgageRepaymentController()
                            .prefered_rate_enable)
                          menuMortgageRepaymentReportGo(
                              title: "BUC Mortgage Repayment Table",
                              index: 3,
                              subString: "Your Preferred Interest Rate",
                              onTap: () {
                                debugPrint(
                                    " click (Prefered BUC Rate) ${controller.loadDetailsDataResult['Prefered BUC Rate']}");

                                if (controller.loadDetailsDataResult[
                                            'Prefered BUC Rate']
                                        .toString() ==
                                    "[]") {
                                  Future.delayed(
                                      const Duration(milliseconds: 0), () {
                                    FocusManager.instance.primaryFocus!
                                        .unfocus();
                                    Fluttertoast.showToast(
                                      timeInSecForIosWeb: 3,
                                      msg:
                                          "No data found, please try again later",
                                      backgroundColor: Colors.red,
                                      toastLength: Toast.LENGTH_LONG,
                                      gravity: ToastGravity.BOTTOM,
                                      textColor: Colors.white,
                                      fontSize: 20.0,
                                    );
                                  });

                                  return;
                                }

                                controller.viewTable1 = false;
                                Get.to(() =>
                                    const BucMortgageRepaymentTableScreen());
                                debugPrint(
                                    " click (Your Preferred Interest Rate2)");
                              }),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Center(
                child: PrimaryButton(
                  windowHeight: windowHeight,
                  windowWidth: windowWidth,
                  buttonTitle: "Download Report",
                  onPressed: () {
                    GetControllers.shared
                        .getBucMortgageRepaymentController()
                        .callAPIDownloadBUCReport();
                  },
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }

  Future getPermission() async {
    var status = await Permission.storage.status;
    if (status.isDenied) {
      debugPrint("=======storage denied=======");
      await Permission.storage.request();
      // We didn't ask for permission yet or the permission has been denied before but not permanently.
    }
  }

  // ignore: body_might_complete_normally_nullable
  late String _localPath;
  Future<File?> downloadFile(String url, String name) async {
    Future.delayed(const Duration(milliseconds: 0), () {
      FocusManager.instance.primaryFocus!.unfocus();
      Fluttertoast.showToast(
        timeInSecForIosWeb: 3,
        msg: "Downloading",
        backgroundColor: Colors.green,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        textColor: Colors.white,
        fontSize: 20.0,
      );
    });
    final taskId = await FlutterDownloader.enqueue(
        url: url,
        savedDir: _localPath,
        showNotification: true,
        // show download progress in status bar (for Android)
        openFileFromNotification: true,
        // click on notification to open downloaded file (for Android)
        saveInPublicStorage: false);
    Get.snackbar("Downloaded", "Download completed",
        colorText: Colors.white,
        backgroundColor: Colors.green,
        mainButton: TextButton(
            onPressed: () async {
              debugPrint(">>>>>>>>");
              await FlutterDownloader.open(taskId: taskId!);
            },
            child: const Text("Open")),
        duration: const Duration(seconds: 7));
    return null;
  }

  Future<void> prepareSaveDir() async {
    _localPath = (await _findLocalPath())!;
    final savedDir = Directory(_localPath);
    bool hasExisted = await savedDir.exists();
    if (!hasExisted) {
      savedDir.create();
    }
  }

  Future<String?> _findLocalPath() async {
    String? externalStorageDirPath;
    if (Platform.isAndroid) {
      try {
        externalStorageDirPath = await AndroidPathProvider.downloadsPath;
      } catch (e) {
        final directory = await getExternalStorageDirectory();
        externalStorageDirPath = directory?.path;
      }
    } else if (Platform.isIOS) {
      externalStorageDirPath =
          (await getApplicationDocumentsDirectory()).absolute.path;
    }
    return externalStorageDirPath;
  }

  void downloadCallback(String id, DownloadTaskStatus status, int progress) {
    if (kDebugMode) {
      print(
          'Background Isolate Callback: task ($id) is in status ($status) and process ($progress)');
    }

    final SendPort send =
        IsolateNameServer.lookupPortByName('downloader_send_port')!;
    send.send([id, status, progress]);
  }

  Widget expandableLoanDetails(String title) {
    return Padding(
      padding: const EdgeInsets.all(1.0),
      child: Container(
        color: AppColors.white,
        child: ExpansionTile(
          initiallyExpanded: true,
          leading: Checkbox(
              value: controller.isCheckLoanDetails,
              onChanged: (bool? value) {
                setState(() {
                  controller.isCheckLoanDetails = value ?? false;
                });
              },
              checkColor: AppColors.red,
              activeColor: AppColors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(2.0),
              ),
              side: MaterialStateBorderSide.resolveWith(
                (states) => const BorderSide(width: 1.0, color: Colors.red),
              )),
          childrenPadding: EdgeInsets.zero,
          title: Text(title, style: TextStyles.leadsTextStyle),
          collapsedIconColor: Colors.black,
          children: [
            const Divider(height: 1, thickness: 1, color: Colors.black12),
            const SizedBox(
              height: 20,
            ),
            loanDetailsItem(
                title: "Prepared For",
                value: controller.loadDetailsDataResult["Loan Details"]
                    ["Prepared For"]),
            loanDetailsItem(
                title: "Property Price",
                value: controller.loadDetailsDataResult["Loan Details"]
                    ["Property Price"],
                isCurrency: true),
            loanDetailsItem(
                title: "Loan-To-Value",
                value: controller.loadDetailsDataResult["Loan Details"]
                        ["Loan-To-Value"] +
                    "%"),
            loanDetailsItem(
                title: "Loan Tenure",
                value: controller.loadDetailsDataResult["Loan Details"]
                        ["Loan Tenure"]
                    .toString(),
                isYear: true),
            loanDetailsItem(
                title: "CPF Available",
                value: controller.loadDetailsDataResult["Loan Details"]
                        ["CPF Available"]
                    .toString(),
                isCurrency: true),
            loanDetailsItem(
                title: "Downpayment",
                value: controller.loadDetailsDataResult["Loan Details"]
                    ["Downpayment"],
                isCurrency: true),
            loanDetailsItem(
                title: "Loan Amount",
                value: controller.loadDetailsDataResult["Loan Details"]
                    ["Loan Amount"],
                isCurrency: true),
            // Obx(() => buildListContentLoanDetails(controller.arrayLoanDetail)),
            const SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }

  Widget loanDetailsItem(
      {String title = "",
      String value = "",
      bool isYear = false,
      bool isCurrency = false}) {
    var finalValue = value;
    if (isCurrency && value != "") {
      var a = double.parse(value);
      finalValue = convertToCurrency(a);
    }
    if (isYear) {
      finalValue = "$value Years";
    }

    return Padding(
      padding: const EdgeInsets.only(left: 80, right: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            //mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 5),
                    child:
                        Text(title, style: TextStyles.formFieldTitleTextStyle1),
                  )),
              Expanded(
                flex: 1,
                child: Text(
                  finalValue,
                  style: TextStyles.leadsTextStyle1,
                  textAlign: TextAlign.left,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  getValueCheckBox(int index) {
    switch (index) {
      case 0:
        {
          return controller.isRateComparison;
        }
      case 2:
        {
          return controller.isDisbursementSchedule;
        }
      case 1:
        {
          return controller.isProgressiveRepayment;
        }

      case 3:
        {
          return controller.isPreferredRepayment;
        }
    }
  }

  setValueCheckBox(int index, bool value) {
    switch (index) {
      case 0:
        {
          // return isInterestRateComparison;
          setState(() {
            controller.isRateComparison = value;
          });
        }
        break;
      case 2:
        {
          // return isMortgageRepayment1;
          setState(() {
            controller.isDisbursementSchedule = value;
          });
        }
        break;
      case 1:
        {
          // return isMortgageRepayment1;
          setState(() {
            controller.isProgressiveRepayment = value;
          });
        }
        break;
      case 3:
        {
          // return isMortgageRepayment1;
          setState(() {
            controller.isPreferredRepayment = value;
          });
        }
        break;
      default:
        {}
        break;
    }
  }

  Widget menuMortgageRepaymentReportGo(
      {String title = "",
      String subString = "",
      required Function() onTap,
      int index = 0}) {
    return Padding(
      padding: const EdgeInsets.only(top: 0, left: 1, right: 1, bottom: 1),
      child: Container(
        color: AppColors.white,
        child: ExpansionTile(
          trailing: InkWell(
            onTap: () {
              onTap();
            },
            child: const Icon(
              Icons.chevron_right,
              color: AppColors.grey625F5A,
            ),
          ),
          leading: Checkbox(
              value: getValueCheckBox(index),
              onChanged: (bool? value) {
                debugPrint("menuMortgageRepaymentReportGo Title::: "+title+" :: $index :: $value");
                setValueCheckBox(index, value ?? false);
              },
              checkColor: AppColors.red,
              activeColor: AppColors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(2.0),
              ),
              side: MaterialStateBorderSide.resolveWith(
                (states) => const BorderSide(width: 1.0, color: Colors.red),
              )),
          childrenPadding: EdgeInsets.zero,
          title: InkWell(
            onTap: () {
              onTap();
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(title, style: TextStyles.leadsTextStyle),
                if (subString != "")
                  Text(subString, style: TextStyles.leadsTextStyle1),
              ],
            ),
          ),
          collapsedIconColor: Colors.black,
        ),
      ),
    );
  }
}
