// ignore_for_file: must_be_immutable, duplicate_ignore

// ignore: must_be_immutable
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/generate_packages_controller.dart';
import 'package:iqrate/Controller/rates_view_controller.dart';

import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Service/GetControllers.dart';
import 'package:iqrate/Utils/ItemCheckBoxModel.dart';
import 'package:iqrate/Utils/multi_checkbox_custom.dart';
import 'package:iqrate/Widgets/primary_button.dart';

class FilterMoreRateSection extends StatefulWidget {
  final Function resetFilterClicked;
  FilterMoreRateSection(
      {Key? key,
      required this.windowHeight,
      required this.windowWidth,
      required this.resetFilterClicked})
      : super(key: key);
  double windowHeight;
  double windowWidth;

  @override
  State<FilterMoreRateSection> createState() => _FilterMoreRateSectionState();
}

class _FilterMoreRateSectionState extends State<FilterMoreRateSection> {
  // final LeadsViewController leadsViewController = Get.find();
  final RatesViewController ratesViewController =
      GetControllers.shared.getRatesViewController();

  GeneratePackagesController generatePackagesController = Get.find();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: const EdgeInsets.only(top: 0),
      child: Card(
        elevation: 0,
        color: Colors.black38,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            topBar(),
            body(),
          ],
        ),
      ),
    );
  }

  Widget topBar() {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Row(
          children: [
            Expanded(
              flex: 7,
              child: InkWell(
                onTap: () {
                  generatePackagesController.resetFilterDefault();
                  widget.resetFilterClicked();
                  // setState(() {
                  // });
                },
                child: Container(
                  padding: const EdgeInsets.symmetric(vertical: 8),
                  decoration: BoxDecoration(
                      color: AppColors.kPrimaryColor.withOpacity(0.2),
                      borderRadius: BorderRadius.circular(30)),
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 5.0),
                      child: Text(
                        'Reset',
                        style: TextStyles.filterDropdownButton,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: InkWell(
                  onTap: () {
                    generatePackagesController
                        .showFilterConditionalVariable.value = 0;
                  },
                  child: SvgPicture.asset(
                    'assets/icons/closeEnclosed.svg',
                    color: Colors.black,
                  )),
            )
          ],
        ),
      ),
    );
  }

  Widget body() {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Bank",
              style: TextStyles.headingSubtitleStyle3,
            ),
            const BankDropDown(),
            SizedBox(
              height: Get.height * 0.005,
            ),
            Text(
              "Rate Type",
              style: TextStyles.headingSubtitleStyle3,
            ),
            const RateTypeDropDown(),
            const SizedBox(
              height: 10,
            ),
            Center(
              child: PrimaryButton(
                  windowHeight: widget.windowHeight,
                  windowWidth: widget.windowWidth * 0.85,
                  buttonTitle: 'Filter',
                  onPressed: () {
                    debugPrint("callAPIGetBankListWithFilter PrimaryButton ");
                    // ratesViewController.resetFilter();//need reset 1st
                    ratesViewController.next = ""; //reset load more
                    generatePackagesController.callAPIGetListAllPackages();
                  }),
            ),
            SizedBox(
              height: widget.windowHeight * 0.04,
            ),
          ],
        ),
      ),
    );
  }
}

//! Bank dropdown widget show in the filter screen
class BankDropDown extends StatefulWidget {
  const BankDropDown({Key? key}) : super(key: key);
  @override
  _BankDropDownState createState() => _BankDropDownState();
}

class _BankDropDownState extends State<BankDropDown> {
  List<ItemCheckBoxModel> dataSource = [];
  // var controller = GetControllers.shared.getRatesViewController();
  GeneratePackagesController generatePackagesController = Get.find();

  @override
  void initState() {
    super.initState();
    loadData();
  }

  loadData() {
    dataSource.clear();
    var array = generatePackagesController.bankDataSourceV2;
    debugPrint('more rate array have ${array.length}');
    for (var i = 0; i < array.length; i++) {
      var item = array[i];
      dataSource.add(ItemCheckBoxModel(
          name: item.name, value: item.value, isChecked: item.isChecked));
    }
  }

  void _showMultiSelect() async {
    debugPrint("backup");
    debugPrint("Hello");
    await showDialog(
      context: context,
      builder: (BuildContext context) {
        // final TestingLogic controller = Get.put(TestingLogic());
        return MultiSelect(
            titleDropdown: "bank aaaaaaa",
            didChoose: (list) {
              //! This function is called when we hit continue in multiselect dropdown
              generatePackagesController.bankDataSourceV2 = list;
              generatePackagesController.getSelectedBankDataSourceV2();
              setState(() {});
              //get selected item
            },
            dataSourceItemOutSide: generatePackagesController.bankDataSourceV2);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 0),
      child: Column(
        children: [
          // ElevatedButton(
          //     onPressed: () {
          //       _showMultiSelect();
          //     },
          //     child: Text("show popup bank")),
          InkWell(
            onTap: () {
              _showMultiSelect();
            },
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              height: 50,
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: AppColors.formFieldBorderColor),
                borderRadius: const BorderRadius.all(Radius.circular(10.0)),
              ),
              child: Row(
                children: const [
                  Icon(
                    Icons.search,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Search a Bank",
                    style: TextStyle(fontSize: 14, color: Color(0xFFA8A8A8)),
                  ),
                  Spacer(),
                  Icon(Icons.expand_more)
                ],
              ),
            ),
          ),
          const SizedBox(
            height: 15,
          ),
          Container(
            color: Colors.white,
            child: showChipList(),
            width: double.infinity,
          ),
          // showChipList()
          const SizedBox(
            height: 10,
          )
        ],
      ),
    );
  }

  showChipList() {
    //get array choosed
    return Wrap(
        alignment: WrapAlignment.start,
        spacing: 5,
        children: List<Widget>.generate(
          generatePackagesController.selectedBankDataSourceV2.length,
          (int index) {
            return Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Chip(
                padding: const EdgeInsets.all(10),
                side: const BorderSide(color: AppColors.kPrimaryColor),
                backgroundColor: AppColors.kPrimaryColor,
                elevation: 1,
                labelStyle: const TextStyle(color: Colors.white),
                label: Text(generatePackagesController
                    .selectedBankDataSourceV2[index].name),
              ),
            );
          },
        ).toList());
  }
}
// !

class RateTypeDropDown extends StatefulWidget {
  const RateTypeDropDown({Key? key}) : super(key: key);
  @override
  _RateTypeDropDownState createState() => _RateTypeDropDownState();
}

class _RateTypeDropDownState extends State<RateTypeDropDown> {
  List<ItemCheckBoxModel> dataSource = [];
  // var controller = GetControllers.shared.getRatesViewController();
  GeneratePackagesController generatePackagesController = Get.find();

  @override
  void initState() {
    super.initState();
    loadData();
  }

  loadData() {
    dataSource.clear();
    var array = generatePackagesController.rateTypeDataSourceV2;
    debugPrint('array have ${array.length}');
    for (var i = 0; i < array.length; i++) {
      var item = array[i];
      dataSource.add(ItemCheckBoxModel(
          name: item.name, value: item.value, isChecked: item.isChecked));
    }
  }

  void _showMultiSelectRateType() async {
    debugPrint("rate type");
    debugPrint("Hello");
    await showDialog(
      context: context,
      builder: (BuildContext context) {
        // final TestingLogic controller = Get.put(TestingLogic());
        return MultiSelect(
            titleDropdown: "rate type aaaaaaa",
            didChoose: (list) {
              //! This function is called when we hit continue in multiselect dropdown
              generatePackagesController.rateTypeDataSourceV2 = list;
              generatePackagesController.getSelectedRateTypeDataSourceV2();
              setState(() {});
              //get selected item
            },
            dataSourceItemOutSide:
                generatePackagesController.rateTypeDataSourceV2);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 0),
      child: Column(
        children: [
          SizedBox(
            height: Get.height * 0.01,
          ),
          InkWell(
            onTap: () {
              _showMultiSelectRateType();
            },
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              height: 50,
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: AppColors.formFieldBorderColor),
                borderRadius: const BorderRadius.all(Radius.circular(10.0)),
              ),
              child: Row(
                children: const [
                  Icon(
                    Icons.search,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Select a Rate Type",
                    style: TextStyle(fontSize: 14, color: Color(0xFFA8A8A8)),
                  ),
                  Spacer(),
                  Icon(Icons.expand_more)
                ],
              ),
            ),
          ),
          Container(
            color: Colors.white,
            child: showChipList(),
            width: double.infinity,
          ),
          // showChipList()
          const SizedBox(
            height: 10,
          )
        ],
      ),
    );
  }

  showChipList() {
    //get array choosed

    return Wrap(
        alignment: WrapAlignment.start,
        spacing: 5,
        children: List<Widget>.generate(
          generatePackagesController.selectedRateTypeDataSourceV2.length,
          (int index) {
            return Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Chip(
                padding: const EdgeInsets.all(10),
                side: const BorderSide(color: AppColors.kPrimaryColor),
                backgroundColor: AppColors.kPrimaryColor,
                elevation: 1,
                labelStyle: const TextStyle(color: Colors.white),
                label: Text(generatePackagesController
                    .selectedRateTypeDataSourceV2[index].name),
              ),
            );
          },
        ).toList());
  }
}

class CheckBoxFilter extends StatefulWidget {
  bool initValue;
  final String textString;
  final Function(bool) onChanged;

  CheckBoxFilter(
      {Key? key,
      required this.onChanged,
      required this.textString,
      this.initValue = false})
      : super(key: key);
  @override
  _CheckBoxFilterState createState() => _CheckBoxFilterState();
}

class _CheckBoxFilterState extends State<CheckBoxFilter> {
  // var isChecked = false.obs;

  @override
  void initState() {
    super.initState();
    // isChecked.value = widget.initValue;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Checkbox(
          value: widget.initValue,
          onChanged: (value) {
            // isChecked.value = value ?? false;
            widget.onChanged(value ?? false);
          },
          //checkColor: AppColors.kSecondaryColor,
          //fillColor: MaterialStateProperty.all(Colors.white),
          hoverColor: AppColors.kSecondaryColor,
          activeColor: AppColors.kSecondaryColor,
          side: const BorderSide(
              color: AppColors.kSecondaryColor,
              width: 2,
              style: BorderStyle.solid),
        ),
        Expanded(
            child: Text(
          widget.textString,
          maxLines: 4,
        ))
        // Wrap(children: [Text(widget.textString)]),
      ],
    );
  }
}

// ! UI for Property type dropdown
class PropertyTypeDropDown extends StatefulWidget {
  const PropertyTypeDropDown({Key? key}) : super(key: key);
  @override
  _PropertyTypeDropDownState createState() => _PropertyTypeDropDownState();
}

class _PropertyTypeDropDownState extends State<PropertyTypeDropDown> {
  List<ItemCheckBoxModel> dataSource = [];
  var controller = GetControllers.shared.getRatesViewController();
  @override
  void initState() {
    super.initState();
    loadData();
  }

  void _showMultiSelect() async {
    if (kDebugMode) {
      print("backup");
    }
    await showDialog(
      context: context,
      builder: (BuildContext context) {
        // final TestingLogic controller = Get.put(TestingLogic());
        return MultiSelect(
            titleDropdown: "bbbb aaaaaaa",
            didChoose: (list) {
              controller.properTypeDataSourceV2 = list;
              controller.getSelectedProperTypeDataSourceV2();
              setState(() {});
              //get selected item
            },
            dataSourceItemOutSide: controller.properTypeDataSourceV2);
      },
    );
  }

  loadData() {
    dataSource.clear();
    var array = controller.properTypeDataSourceV2;
    if (kDebugMode) {
      print('array have ${array.length}');
    }
    for (var i = 0; i < array.length; i++) {
      var item = array[i];
      dataSource.add(ItemCheckBoxModel(
          name: item.name, value: item.value, isChecked: item.isChecked));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 0),
      child: Column(
        children: [
          // ElevatedButton(
          // onPressed: () {
          //   _showMultiSelect();
          // },
          // child: Text("show popup")),
          InkWell(
            onTap: () {
              _showMultiSelect();
            },
            child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                height: 50,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: AppColors.formFieldBorderColor),
                  borderRadius: const BorderRadius.all(Radius.circular(10.0)),
                ),
                child: Row(
                  children: const [
                    Text(
                      "Select a Property Type",
                      style: TextStyle(fontSize: 14, color: Color(0xFFA8A8A8)),
                    ),
                    Spacer(),
                    Icon(Icons.expand_more)
                  ],
                )),
          ),
          Container(
            color: Colors.white,
            child: showChipList(),
            width: double.infinity,
          )
        ],
      ),
    );
  }

  showChipList() {
    //get array choosed
    return Wrap(
        alignment: WrapAlignment.start,
        spacing: 5,
        children: List<Widget>.generate(
          controller.selectedProperTypeDataSourceV2.length,
          (int index) {
            return Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Chip(
                padding: const EdgeInsets.all(10),
                side: const BorderSide(color: AppColors.kPrimaryColor),
                backgroundColor: AppColors.kPrimaryColor,
                elevation: 1,
                labelStyle: const TextStyle(color: Colors.white),
                label:
                    Text(controller.selectedProperTypeDataSourceV2[index].name),
              ),
            );
          },
        ).toList());
  }
}
