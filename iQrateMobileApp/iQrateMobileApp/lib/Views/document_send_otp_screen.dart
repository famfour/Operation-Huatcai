import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/my_documents_controller.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Widgets/primary_button.dart';

class DocumentSendOtpScreen extends StatefulWidget {
  const DocumentSendOtpScreen({Key? key}) : super(key: key);

  @override
  State<DocumentSendOtpScreen> createState() => _DocumentSendOtpScreenState();
}

class _DocumentSendOtpScreenState extends State<DocumentSendOtpScreen> {
  final MyDocumentsController mobileVerificationController =
      Get.put(MyDocumentsController());

  @override
  void initState() {
    mobileVerificationController.leadsViewController.myDocumentID =
        Get.arguments[0] ?? "";
    mobileVerificationController.leadsViewController.document_drawer_url =
        Get.arguments[1] ?? "";

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double windowWidth = MediaQuery.of(context).size.width;
    double windowHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        automaticallyImplyLeading: false,
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 25.0, top: 30),
            child: InkWell(
              onTap: () {
                Get.back(); // Pop the existing page and go back to previous page
              },
              child: SvgPicture.asset(
                'assets/icons/closeEnclosed.svg',
                color: Colors.black,
              ),
            ),
          )
        ],
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 40.0, horizontal: 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: SvgPicture.asset(
                    "assets/logos/iqrate_logo.svg",
                    width: 80,
                  ),
                ),
                SizedBox(
                  height: windowHeight * 0.08,
                ),
                Center(
                  child: Text("Document Upload",
                      style: TextStyles.introScreenTitlesSmall),
                ),
                SizedBox(
                  height: windowHeight * 0.05,
                ),
                Text(
                  "An OTP will be sent to your mobile number and your registered email address to access the document drawer.",
                  style: TextStyles.introScreenDescriptions,
                ),
                SizedBox(
                  height: windowHeight * 0.08,
                ),
                PrimaryButton(
                  windowHeight: windowHeight,
                  windowWidth: windowWidth,
                  buttonTitle: "Send OTP",
                  onPressed: () async {
                    mobileVerificationController.onTapSendOTP();
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
