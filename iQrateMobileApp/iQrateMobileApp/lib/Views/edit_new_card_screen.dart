import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/saved_cards_controller.dart';
import 'package:iqrate/DeviceManager/screen_constants.dart';
import 'package:iqrate/Widgets/form_field_title.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';
import 'package:pattern_formatter/pattern_formatter.dart';

class EditCardScreen extends StatelessWidget {
  EditCardScreen({Key? key}) : super(key: key);
  final SavedCardsController editCardController =
      Get.put(SavedCardsController());

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        automaticallyImplyLeading: false,
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 8.0, top: 20),
            child: InkWell(
              onTap: () {
                Get.back(); // Pop the existing page and go back to previous page
              },
              child: SvgPicture.asset(
                'assets/icons/closeEnclosed.svg',
                color: Colors.black,
              ),
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Text(
                    'Edit Card Details',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: FontSize.s22),
                  ),
                ),
                SizedBox(height: windowHeight * 0.02),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: const [
                    FormFieldTitle(title: 'Name on Card*'),
                  ],
                ),
                SizedBox(height: windowHeight * 0.005),
                RequireTextField(
                  type: Type.fullname,
                  controller:
                      editCardController.nameOnCardTextEditingController,
                ),
                SizedBox(height: windowHeight * 0.02),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: const [
                    FormFieldTitle(title: 'Card Number*'),
                  ],
                ),
                SizedBox(height: windowHeight * 0.005),
                RequireTextField(
                  numberInputFormatter: [
                    CreditCardFormatter(),
                  ],
                  type: Type.number,
                  maxLength: 19,
                  controller:
                      editCardController.cardNumberTextEditingController,
                  onChanged: (String value) {
                    //editCardController.cardDetails.value = editCardController.cardDetails.value.copyWith(number: value);
                  },
                ),
                SizedBox(height: windowHeight * 0.01),
                Row(
                  children: [
                    Expanded(
                      flex: 5,
                      child: Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: const [
                                FormFieldTitle(title: 'Expiration*'),
                              ],
                            ),
                            SizedBox(height: windowHeight * 0.005),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  flex: 2,
                                  child: RequireTextField(
                                    type: Type.expiry,
                                    maxLength: 2,
                                    labelText: 'MM',
                                    controller: editCardController
                                        .expirationMonthTextEditingController,
                                    onChanged: (String value) {
                                      //editCardController.cardDetails.value = editCardController.cardDetails.value.copyWith(expirationMonth: int.tryParse(value));
                                    },
                                  ),
                                ),
                                SizedBox(
                                  width: windowWidth * 0.03,
                                ),
                                Expanded(
                                  flex: 4,
                                  child: RequireTextField(
                                    type: Type.expiry,
                                    maxLength: 2,
                                    labelText: 'YY',
                                    controller: editCardController
                                        .expirationYearTextEditingController,
                                    onChanged: (String value) {
                                      //editCardController.cardDetails.value = editCardController.cardDetails.value.copyWith(expirationMonth: int.tryParse(value));
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 5,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: const [
                                FormFieldTitle(title: 'CVC*'),
                              ],
                            ),
                            SizedBox(height: windowHeight * 0.005),
                            RequireTextField(
                              numberInputFormatter: [CreditCardFormatter()],
                              type: Type.number,
                              maxLength: 3,
                              labelText: 'CVC',
                              controller:
                                  editCardController.cvcTextEditingController,
                              onChanged: (String value) {
                                //editCardController.cardDetails.value = widget.controller.cardDetails.value.copyWith(cvc: value);
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: const [
                    FormFieldTitle(title: 'Country'),
                  ],
                ),
                SizedBox(height: windowHeight * 0.005),
                RequireTextField(
                  type: Type.dropDownCountry,
                  controller: editCardController.countryTextEditingController,
                  onChanged: (String value) {},
                ),
                Obx(() => CheckboxListTile(
                      title: const Text("Save card for future use."),
                      value: editCardController.checkedValue.value,
                      activeColor: Colors.black,
                      contentPadding: EdgeInsets.zero,
                      onChanged: (newValue) {
                        editCardController.checkedValue.value = newValue!;
                      },
                      controlAffinity: ListTileControlAffinity
                          .leading, //  <-- leading Checkbox
                    )),
                SizedBox(height: windowHeight * 0.04),
                Center(
                  child: PrimaryButton(
                    windowHeight: windowHeight,
                    windowWidth: windowWidth,
                    buttonTitle: "Save",
                    onPressed: () async {
                      await editCardController.onUpdate();
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
