import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/calculators_view_controller.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/Views/more_view.dart';

class CalculatorsView extends StatelessWidget {
  CalculatorsView({Key? key}) : super(key: key);
  final CalculatorsViewController calculatorsViewController =
      Get.put(CalculatorsViewController());
  @override
  Widget build(BuildContext context) {
    //double windowWidth = MediaQuery.of(context).size.width; //get screen width
    double windowHeight =
        MediaQuery.of(context).size.height; //get screen height

    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: AppColors.red, // status bar color
    ));

    return GridView.count(
      padding: EdgeInsets.all(windowHeight * 0.02),
      crossAxisCount: 2,
      mainAxisSpacing: windowHeight * 0.02,
      crossAxisSpacing: windowHeight * 0.02,
      children: [
        GridItem(
          assetImage: 'assets/images/newpur1.svg',
          title: 'New Purchase',
          subTitle: ' (Loan Eligibility for Completed Property)',
          onTap: () {
            calculatorsViewController.onTapNewPurchase();
          },
        ),
        GridItem(
          assetImage: 'assets/images/refin1.svg',
          title: 'Refinance',
          subTitle: '(Interest Rate Savings)',
          onTap: () {
            calculatorsViewController.onTapRefinance();
          },
        ),
        GridItem(
          assetImage: 'assets/images/mort3.svg',
          title: 'Mortgage Repayment',
          subTitle: '(Completed Property)',
          onTap: () {
            calculatorsViewController.onTapMortgageRepayment();
          },
        ),
        GridItem(
          assetImage: 'assets/images/buc1.svg',
          title: 'BUC Mortgage Repayment',
          subTitle: '(Under Construction)',
          onTap: () {
            calculatorsViewController.onTapBUCMortageRepayment();
          },
        ),
        GridItem(
          assetImage: 'assets/images/equity1.svg',
          title: 'Equity Loan',
          subTitle: '',
          onTap: () {
            calculatorsViewController.aclNavigationCheckEquityLoanCalculator();
            // calculatorsViewController.onTapEquityLoan();
          },
        ),
        GridItem(
          assetImage: 'assets/images/buyer1.svg',
          title: "Buyer's Stamp Duty",
          subTitle: '',
          onTap: () {
            calculatorsViewController
                .aclNavigationCheckBuyerStampDutyCalculator();
          },
        ),
        GridItem(
          assetImage: 'assets/images/seller`1.svg',
          title: "Seller's Stamp Duty",
          subTitle: '',
          onTap: () {
            calculatorsViewController
                .aclNavigationCheckSellarStampDutynCalculator();
          },
        ),
      ],
    );
  }
}
