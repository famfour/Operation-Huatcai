// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/refinance_existing_lead_controller.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/screen_constants.dart';
import 'package:iqrate/Widgets/default_appbar.dart';

class SavingsFromRefinancingExistingLead extends StatelessWidget {
  final RefinanceExistingLeadController refinanceExistingLeadController =
      Get.find();
  SavingsFromRefinancingExistingLead({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // debugPrint(refinanceExistingLeadController.resultMainApplicantStep3.value
    //     .packageDetails![2].maximumLoanTenure!.monthlyInstallment
    //     .toString());
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: DefaultAppBar3(
        title: "Refinance Savings",
        windowHeight: windowHeight * 0.09,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: windowHeight * 0.1,
              ),
              Text(
                'Based On Maximum Loan Tenure',
                style: TextStyle(
                  color: const Color(0XFF9A9696),
                  fontSize: FontSize.s20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: windowHeight * 0.03,
              ),
              Divider(
                thickness: windowHeight * 0.001,
                color: Colors.grey,
              ),
              SizedBox(
                height: windowHeight * 0.03,
              ),
              Obx(
                () => Table(
                  defaultColumnWidth: FixedColumnWidth(windowWidth * 0.23),
                  border: TableBorder.all(
                      color: const Color(0XFFE2E2E2),
                      style: BorderStyle.solid,
                      width: 2),
                  children: [
                    TableRow(
                      children: [
                        TableTitleRowData(
                          windowHeight: windowHeight,
                          title: '',
                          subTitle: '',
                        ),
                        TableTitleRowData(
                          windowHeight: windowHeight,
                          title: 'Current',
                          subTitle:
                              "(${refinanceExistingLeadController.resultMainApplicantStep3.value.packageDetails![0].maximumLoanTenure!.rate.toString()}%)",
                        ),
                        TableTitleRowData(
                          windowHeight: windowHeight,
                          title: 'Package 1',
                          subTitle: refinanceExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .packageDetails!
                                      .length >
                                  1
                              ? "(${refinanceExistingLeadController.resultMainApplicantStep3.value.packageDetails![1].maximumLoanTenure!.rate.toString()}%)"
                              : "-",
                        ),
                        TableTitleRowData(
                          windowHeight: windowHeight,
                          title: 'Package 2',
                          subTitle: refinanceExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .packageDetails!
                                      .length >
                                  2
                              ? "(${refinanceExistingLeadController.resultMainApplicantStep3.value.packageDetails![2].maximumLoanTenure!.rate.toString()}%)"
                              : "-",
                        ),
                        // TableTitleRowData(
                        //   windowHeight: windowHeight,
                        //   title: 'Package 3',
                        //   subTitle: refinanceExistingLeadController
                        //               .resultMainApplicantStep3
                        //               .value
                        //               .packageDetails!
                        //               .length >
                        //           3
                        //       ? "(${refinanceExistingLeadController.resultMainApplicantStep3.value.packageDetails![3].maximumLoanTenure!.rate.toString()}%)"
                        //       : "-",
                        // ),
                        // TableTitleRowData(
                        //   windowHeight: windowHeight,
                        //   title: 'Package 4',
                        //   subTitle: refinanceExistingLeadController
                        //               .resultMainApplicantStep3
                        //               .value
                        //               .packageDetails!
                        //               .length >
                        //           4
                        //       ? "(${refinanceExistingLeadController.resultMainApplicantStep3.value.packageDetails![4].maximumLoanTenure!.rate.toString()}%)"
                        //       : "-",
                        // ),
                      ],
                    ),
                    TableRow(
                      children: [
                        DataRow(
                          windowHeight: windowHeight,
                          stringData: 'Monthly Installment',
                          isCurrency: false,
                        ),
                        DataRow(
                          isCurrency: true,
                          windowHeight: windowHeight,
                          stringData: refinanceExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .packageDetails![0]
                                      .maximumLoanTenure!
                                      .monthlyInstallment !=
                                  null
                              ? refinanceExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .packageDetails![0]
                                  .maximumLoanTenure!
                                  .monthlyInstallment
                                  .toString()
                              : "-",
                          // stringData: '-',
                        ),
                        DataRow(
                          isCurrency: true,
                          windowHeight: windowHeight,
                          stringData: refinanceExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .packageDetails!
                                      .length >=
                                  2
                              ? refinanceExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .packageDetails![1]
                                  .maximumLoanTenure!
                                  .monthlyInstallment
                                  .toString()
                              : "-",
                          // stringData: "-",
                        ),
                        DataRow(
                          isCurrency: true,

                          windowHeight: windowHeight,
                          stringData: refinanceExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .packageDetails!
                                      .length >=
                                  3
                              ? refinanceExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .packageDetails![2]
                                  .maximumLoanTenure!
                                  .monthlyInstallment
                                  .toString()
                              : "-",
                          // stringData: "-",
                        ),
                        // DataRow(
                        //   isCurrency: true,

                        //   windowHeight: windowHeight,
                        //   stringData: refinanceExistingLeadController
                        //               .resultMainApplicantStep3
                        //               .value
                        //               .packageDetails!
                        //               .length >=
                        //           4
                        //       ? refinanceExistingLeadController
                        //           .resultMainApplicantStep3
                        //           .value
                        //           .packageDetails![3]
                        //           .maximumLoanTenure!
                        //           .monthlyInstallment
                        //           .toString()
                        //       : "-",
                        //   // stringData: "-",
                        // ),
                        // DataRow(
                        //   isCurrency: true,

                        //   windowHeight: windowHeight,
                        //   stringData: refinanceExistingLeadController
                        //               .resultMainApplicantStep3
                        //               .value
                        //               .packageDetails!
                        //               .length >=
                        //           5
                        //       ? refinanceExistingLeadController
                        //           .resultMainApplicantStep3
                        //           .value
                        //           .packageDetails![4]
                        //           .maximumLoanTenure!
                        //           .monthlyInstallment
                        //           .toString()
                        //       : "-",
                        //   // stringData: "-",
                        // ),
                      ],
                    ),
                    TableRow(
                      children: [
                        DataRow(
                          isCurrency: false,
                          windowHeight: windowHeight,
                          stringData: 'Total Principal',
                        ),
                        DataRow(
                          isCurrency: true,

                          windowHeight: windowHeight,
                          stringData: refinanceExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .packageDetails![0]
                                      .maximumLoanTenure!
                                      .totalPrincipal !=
                                  null
                              ? refinanceExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .packageDetails![0]
                                  .maximumLoanTenure!
                                  .totalPrincipal
                                  .toString()
                              : "-",
                          // stringData: "-",
                        ),
                        DataRow(
                          isCurrency: true,

                          windowHeight: windowHeight,
                          stringData: refinanceExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .packageDetails!
                                      .length >=
                                  2
                              ? refinanceExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .packageDetails![1]
                                  .maximumLoanTenure!
                                  .totalPrincipal
                                  .toString()
                              : "-",
                          // stringData: "-",
                        ),
                        DataRow(
                          isCurrency: true,

                          windowHeight: windowHeight,
                          stringData: refinanceExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .packageDetails!
                                      .length >=
                                  3
                              ? refinanceExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .packageDetails![2]
                                  .maximumLoanTenure!
                                  .totalPrincipal
                                  .toString()
                              : "-",
                          // stringData: "-",
                        ),
                        // DataRow(
                        //   isCurrency: true,

                        //   windowHeight: windowHeight,
                        //   stringData: refinanceExistingLeadController
                        //               .resultMainApplicantStep3
                        //               .value
                        //               .packageDetails!
                        //               .length >=
                        //           4
                        //       ? refinanceExistingLeadController
                        //           .resultMainApplicantStep3
                        //           .value
                        //           .packageDetails![3]
                        //           .maximumLoanTenure!
                        //           .totalPrincipal
                        //           .toString()
                        //       : "-",
                        //   // stringData: "-",
                        // ),
                        // DataRow(
                        //   isCurrency: true,

                        //   windowHeight: windowHeight,
                        //   stringData: refinanceExistingLeadController
                        //               .resultMainApplicantStep3
                        //               .value
                        //               .packageDetails!
                        //               .length >=
                        //           5
                        //       ? refinanceExistingLeadController
                        //           .resultMainApplicantStep3
                        //           .value
                        //           .packageDetails![4]
                        //           .maximumLoanTenure!
                        //           .totalPrincipal
                        //           .toString()
                        //       : "-",
                        //   // stringData: "-",
                        // ),
                      ],
                    ),
                    TableRow(
                      children: [
                        DataRow(
                          isCurrency: false,
                          windowHeight: windowHeight,
                          stringData: 'Total Interest',
                        ),
                        DataRow(
                          isCurrency: true,

                          windowHeight: windowHeight,
                          stringData: refinanceExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .packageDetails![0]
                                      .maximumLoanTenure!
                                      .totalInterest !=
                                  null
                              ? refinanceExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .packageDetails![0]
                                  .maximumLoanTenure!
                                  .totalInterest
                                  .toString()
                              : "-",
                          // stringData: "-",
                        ),
                        DataRow(
                          isCurrency: true,

                          windowHeight: windowHeight,
                          stringData: refinanceExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .packageDetails!
                                      .length >=
                                  2
                              ? refinanceExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .packageDetails![1]
                                  .maximumLoanTenure!
                                  .totalInterest
                                  .toString()
                              : "-",
                          // stringData: "-",
                        ),
                        DataRow(
                          isCurrency: true,

                          windowHeight: windowHeight,
                          stringData: refinanceExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .packageDetails!
                                      .length >=
                                  3
                              ? refinanceExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .packageDetails![2]
                                  .maximumLoanTenure!
                                  .totalInterest
                                  .toString()
                              : "-",
                          // stringData: "-",
                        ),
                        // DataRow(
                        //   isCurrency: true,

                        //   windowHeight: windowHeight,
                        //   stringData: refinanceExistingLeadController
                        //               .resultMainApplicantStep3
                        //               .value
                        //               .packageDetails!
                        //               .length >=
                        //           4
                        //       ? refinanceExistingLeadController
                        //           .resultMainApplicantStep3
                        //           .value
                        //           .packageDetails![3]
                        //           .maximumLoanTenure!
                        //           .totalInterest
                        //           .toString()
                        //       : "-",
                        //   // stringData: "-",
                        // ),
                        // DataRow(
                        //   isCurrency: true,

                        //   windowHeight: windowHeight,
                        //   stringData: refinanceExistingLeadController
                        //               .resultMainApplicantStep3
                        //               .value
                        //               .packageDetails!
                        //               .length >=
                        //           5
                        //       ? refinanceExistingLeadController
                        //           .resultMainApplicantStep3
                        //           .value
                        //           .packageDetails![4]
                        //           .maximumLoanTenure!
                        //           .totalInterest
                        //           .toString()
                        //       : "-",
                        //   // stringData: "-",
                        // ),
                      ],
                    ),
                    TableRow(
                      children: [
                        DataRow(
                          isCurrency: false,
                          windowHeight: windowHeight,
                          stringData: 'Cash Rebate/ Subsidy',
                        ),
                        DataRow(
                          isCurrency: true,
                          windowHeight: windowHeight,
                          stringData: refinanceExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .packageDetails![0]
                                      .maximumLoanTenure!
                                      .cashSubsidy !=
                                  null
                              ? refinanceExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .packageDetails![0]
                                  .maximumLoanTenure!
                                  .cashSubsidy
                                  .toString()
                              : "-",
                          // stringData: "-",
                        ),
                        DataRow(
                          isCurrency: true,

                          windowHeight: windowHeight,
                          stringData: refinanceExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .packageDetails!
                                      .length >=
                                  2
                              ? refinanceExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .packageDetails![1]
                                  .maximumLoanTenure!
                                  .cashSubsidy
                                  .toString()
                              : "-",
                          // stringData: "-",
                        ),
                        DataRow(
                          isCurrency: true,

                          windowHeight: windowHeight,
                          stringData: refinanceExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .packageDetails!
                                      .length >=
                                  3
                              ? refinanceExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .packageDetails![2]
                                  .maximumLoanTenure!
                                  .cashSubsidy
                                  .toString()
                              : "-",
                          // stringData: "-",
                        ),
                        // DataRow(
                        //   isCurrency: true,

                        //   windowHeight: windowHeight,
                        //   stringData: refinanceExistingLeadController
                        //               .resultMainApplicantStep3
                        //               .value
                        //               .packageDetails!
                        //               .length >=
                        //           4
                        //       ? refinanceExistingLeadController
                        //           .resultMainApplicantStep3
                        //           .value
                        //           .packageDetails![3]
                        //           .maximumLoanTenure!
                        //           .cashSubsidy
                        //           .toString()
                        //       : "-",
                        //   // stringData: "-",
                        // ),
                        // DataRow(
                        //   isCurrency: true,

                        //   windowHeight: windowHeight,
                        //   stringData: refinanceExistingLeadController
                        //               .resultMainApplicantStep3
                        //               .value
                        //               .packageDetails!
                        //               .length >=
                        //           5
                        //       ? refinanceExistingLeadController
                        //           .resultMainApplicantStep3
                        //           .value
                        //           .packageDetails![4]
                        //           .maximumLoanTenure!
                        //           .cashSubsidy
                        //           .toString()
                        //       : "-",
                        //   // stringData: "-",
                        // ),
                      ],
                    ),
                    TableRow(
                      children: [
                        DataRow(
                          isCurrency: false,
                          windowHeight: windowHeight,
                          stringData: 'Savings (First 3 years)',
                        ),
                        DataRow(
                          isCurrency: true,

                          windowHeight: windowHeight,
                          stringData: refinanceExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .packageDetails![0]
                                      .maximumLoanTenure!
                                      .savings !=
                                  null
                              ? refinanceExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .packageDetails![0]
                                  .maximumLoanTenure!
                                  .savings
                                  .toString()
                              : "-",
                          // stringData: "-",
                        ),
                        DataRow(
                          isCurrency: true,

                          windowHeight: windowHeight,
                          stringData: refinanceExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .packageDetails!
                                      .length >=
                                  2
                              ? refinanceExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .packageDetails![1]
                                  .maximumLoanTenure!
                                  .savings
                                  .toString()
                              : "-",
                          // stringData: "-",
                        ),
                        DataRow(
                          isCurrency: true,

                          windowHeight: windowHeight,
                          stringData: refinanceExistingLeadController
                                      .resultMainApplicantStep3
                                      .value
                                      .packageDetails!
                                      .length >=
                                  3
                              ? refinanceExistingLeadController
                                  .resultMainApplicantStep3
                                  .value
                                  .packageDetails![2]
                                  .maximumLoanTenure!
                                  .savings
                                  .toString()
                              : "-",
                          // stringData: "-",
                        ),
                        // DataRow(
                        //   isCurrency: true,

                        //   windowHeight: windowHeight,
                        //   stringData: refinanceExistingLeadController
                        //               .resultMainApplicantStep3
                        //               .value
                        //               .packageDetails!
                        //               .length >=
                        //           4
                        //       ? refinanceExistingLeadController
                        //           .resultMainApplicantStep3
                        //           .value
                        //           .packageDetails![3]
                        //           .maximumLoanTenure!
                        //           .savings
                        //           .toString()
                        //       : "-",
                        //   // stringData: "-",
                        // ),
                        // DataRow(
                        //   isCurrency: true,

                        //   windowHeight: windowHeight,
                        //   stringData: refinanceExistingLeadController
                        //               .resultMainApplicantStep3
                        //               .value
                        //               .packageDetails!
                        //               .length >=
                        //           5
                        //       ? refinanceExistingLeadController
                        //           .resultMainApplicantStep3
                        //           .value
                        //           .packageDetails![4]
                        //           .maximumLoanTenure!
                        //           .savings
                        //           .toString()
                        //       : "-",
                        //   // stringData: "-",
                        // ),
                      ],
                    ),
                  ],
                ),
              ),
              refinanceExistingLeadController.resultMainApplicantStep3.value
                          .packageDetails![0].preferedLoanTenure !=
                      null
                  ? SizedBox(
                      height: windowHeight * 0.08,
                    )
                  : SizedBox(
                      height: windowHeight * 0.01,
                    ),
              refinanceExistingLeadController.resultMainApplicantStep3.value
                          .packageDetails![0].preferedLoanTenure !=
                      null
                  ? Text(
                      'Based On Preferred Loan Tenure',
                      style: TextStyle(
                        color: const Color(0XFF9A9696),
                        fontSize: FontSize.s20,
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  : const SizedBox(),
              refinanceExistingLeadController.resultMainApplicantStep3.value
                          .packageDetails![0].preferedLoanTenure !=
                      null
                  ? SizedBox(
                      height: windowHeight * 0.03,
                    )
                  : const SizedBox(),
              refinanceExistingLeadController.resultMainApplicantStep3.value
                          .packageDetails![0].preferedLoanTenure !=
                      null
                  ? Divider(
                      thickness: windowHeight * 0.001,
                      color: Colors.grey,
                    )
                  : const SizedBox(),
              refinanceExistingLeadController.resultMainApplicantStep3.value
                          .packageDetails![0].preferedLoanTenure !=
                      null
                  ? SizedBox(
                      height: windowHeight * 0.03,
                    )
                  : const SizedBox(),
              Obx(
                () => refinanceExistingLeadController.resultMainApplicantStep3
                            .value.packageDetails![0].preferedLoanTenure !=
                        null
                    ? Table(
                        defaultColumnWidth:
                            FixedColumnWidth(windowWidth * 0.23),
                        border: TableBorder.all(
                            color: const Color(0XFFE2E2E2),
                            style: BorderStyle.solid,
                            width: 2),
                        children: [
                          TableRow(
                            children: [
                              TableTitleRowData(
                                windowHeight: windowHeight,
                                title: '',
                                subTitle: '',
                              ),
                              TableTitleRowData(
                                windowHeight: windowHeight,
                                title: 'Current',
                                subTitle:
                                    "(${refinanceExistingLeadController.resultMainApplicantStep3.value.packageDetails![0].preferedLoanTenure!.rate.toString()}%)",
                              ),
                              TableTitleRowData(
                                windowHeight: windowHeight,
                                title: 'Package 1',
                                subTitle: refinanceExistingLeadController
                                            .resultMainApplicantStep3
                                            .value
                                            .packageDetails!
                                            .length >
                                        1
                                    ? "(${refinanceExistingLeadController.resultMainApplicantStep3.value.packageDetails![1].preferedLoanTenure!.rate.toString()}%)"
                                    : "-",
                              ),
                              TableTitleRowData(
                                windowHeight: windowHeight,
                                title: 'Package 2',
                                subTitle: refinanceExistingLeadController
                                            .resultMainApplicantStep3
                                            .value
                                            .packageDetails!
                                            .length >
                                        2
                                    ? "(${refinanceExistingLeadController.resultMainApplicantStep3.value.packageDetails![2].preferedLoanTenure!.rate.toString()}%)"
                                    : "-",
                              ),
                              // TableTitleRowData(
                              //   windowHeight: windowHeight,
                              //   title: 'Package 3',
                              //   subTitle: refinanceExistingLeadController
                              //               .resultMainApplicantStep3
                              //               .value
                              //               .packageDetails!
                              //               .length >
                              //           3
                              //       ? "(${refinanceExistingLeadController.resultMainApplicantStep3.value.packageDetails![3].preferedLoanTenure!.rate.toString()}%)"
                              //       : "-",
                              // ),
                              // TableTitleRowData(
                              //   windowHeight: windowHeight,
                              //   title: 'Package 4',
                              //   subTitle: refinanceExistingLeadController
                              //               .resultMainApplicantStep3
                              //               .value
                              //               .packageDetails!
                              //               .length >
                              //           4
                              //       ? "(${refinanceExistingLeadController.resultMainApplicantStep3.value.packageDetails![4].preferedLoanTenure!.rate.toString()}%)"
                              //       : "-",
                              // ),
                            ],
                          ),
                          TableRow(
                            children: [
                              DataRow(
                                isCurrency: false,
                                windowHeight: windowHeight,
                                stringData: 'Monthly Installment',
                              ),
                              DataRow(
                                isCurrency: true,
                                windowHeight: windowHeight,
                                stringData: refinanceExistingLeadController
                                            .resultMainApplicantStep3
                                            .value
                                            .packageDetails![0]
                                            .preferedLoanTenure!
                                            .monthlyInstallment !=
                                        null
                                    ? refinanceExistingLeadController
                                        .resultMainApplicantStep3
                                        .value
                                        .packageDetails![0]
                                        .preferedLoanTenure!
                                        .monthlyInstallment
                                        .toString()
                                    : "-",
                              ),
                              DataRow(
                                isCurrency: true,
                                windowHeight: windowHeight,
                                stringData: refinanceExistingLeadController
                                            .resultMainApplicantStep3
                                            .value
                                            .packageDetails!
                                            .length >=
                                        2
                                    ? refinanceExistingLeadController
                                        .resultMainApplicantStep3
                                        .value
                                        .packageDetails![1]
                                        .preferedLoanTenure!
                                        .monthlyInstallment
                                        .toString()
                                    : "-",
                              ),
                              DataRow(
                                isCurrency: true,
                                windowHeight: windowHeight,
                                stringData: refinanceExistingLeadController
                                            .resultMainApplicantStep3
                                            .value
                                            .packageDetails!
                                            .length >=
                                        3
                                    ? refinanceExistingLeadController
                                        .resultMainApplicantStep3
                                        .value
                                        .packageDetails![2]
                                        .preferedLoanTenure!
                                        .monthlyInstallment
                                        .toString()
                                    : "-",
                              ),
                              // DataRow(
                              //   isCurrency: true,
                              //   windowHeight: windowHeight,
                              //   stringData: refinanceExistingLeadController
                              //               .resultMainApplicantStep3
                              //               .value
                              //               .packageDetails!
                              //               .length >=
                              //           4
                              //       ? refinanceExistingLeadController
                              //           .resultMainApplicantStep3
                              //           .value
                              //           .packageDetails![3]
                              //           .preferedLoanTenure!
                              //           .monthlyInstallment
                              //           .toString()
                              //       : "-",
                              // ),
                              // DataRow(
                              //   isCurrency: true,
                              //   windowHeight: windowHeight,
                              //   stringData: refinanceExistingLeadController
                              //               .resultMainApplicantStep3
                              //               .value
                              //               .packageDetails!
                              //               .length >=
                              //           5
                              //       ? refinanceExistingLeadController
                              //           .resultMainApplicantStep3
                              //           .value
                              //           .packageDetails![4]
                              //           .preferedLoanTenure!
                              //           .monthlyInstallment
                              //           .toString()
                              //       : "-",
                              // ),
                            ],
                          ),
                          TableRow(
                            children: [
                              DataRow(
                                isCurrency: false,
                                windowHeight: windowHeight,
                                stringData: 'Total Principal',
                              ),
                              DataRow(
                                isCurrency: true,
                                windowHeight: windowHeight,
                                stringData: refinanceExistingLeadController
                                            .resultMainApplicantStep3
                                            .value
                                            .packageDetails![0]
                                            .preferedLoanTenure!
                                            .totalPrincipal !=
                                        null
                                    ? refinanceExistingLeadController
                                        .resultMainApplicantStep3
                                        .value
                                        .packageDetails![0]
                                        .preferedLoanTenure!
                                        .totalPrincipal
                                        .toString()
                                    : "-",
                              ),
                              DataRow(
                                isCurrency: true,
                                windowHeight: windowHeight,
                                stringData: refinanceExistingLeadController
                                            .resultMainApplicantStep3
                                            .value
                                            .packageDetails!
                                            .length >=
                                        2
                                    ? refinanceExistingLeadController
                                        .resultMainApplicantStep3
                                        .value
                                        .packageDetails![1]
                                        .preferedLoanTenure!
                                        .totalPrincipal
                                        .toString()
                                    : "-",
                              ),
                              DataRow(
                                isCurrency: true,
                                windowHeight: windowHeight,
                                stringData: refinanceExistingLeadController
                                            .resultMainApplicantStep3
                                            .value
                                            .packageDetails!
                                            .length >=
                                        3
                                    ? refinanceExistingLeadController
                                        .resultMainApplicantStep3
                                        .value
                                        .packageDetails![2]
                                        .preferedLoanTenure!
                                        .totalPrincipal
                                        .toString()
                                    : "-",
                              ),
                              // DataRow(
                              //   isCurrency: true,
                              //   windowHeight: windowHeight,
                              //   stringData: refinanceExistingLeadController
                              //               .resultMainApplicantStep3
                              //               .value
                              //               .packageDetails!
                              //               .length >=
                              //           4
                              //       ? refinanceExistingLeadController
                              //           .resultMainApplicantStep3
                              //           .value
                              //           .packageDetails![3]
                              //           .preferedLoanTenure!
                              //           .totalPrincipal
                              //           .toString()
                              //       : "-",
                              // ),
                              // DataRow(
                              //   isCurrency: true,
                              //   windowHeight: windowHeight,
                              //   stringData: refinanceExistingLeadController
                              //               .resultMainApplicantStep3
                              //               .value
                              //               .packageDetails!
                              //               .length >=
                              //           5
                              //       ? refinanceExistingLeadController
                              //           .resultMainApplicantStep3
                              //           .value
                              //           .packageDetails![4]
                              //           .preferedLoanTenure!
                              //           .totalPrincipal
                              //           .toString()
                              //       : "-",
                              // ),
                            ],
                          ),
                          TableRow(
                            children: [
                              DataRow(
                                isCurrency: false,
                                windowHeight: windowHeight,
                                stringData: 'Total Interest',
                              ),
                              DataRow(
                                isCurrency: true,
                                windowHeight: windowHeight,
                                stringData: refinanceExistingLeadController
                                            .resultMainApplicantStep3
                                            .value
                                            .packageDetails![0]
                                            .preferedLoanTenure!
                                            .totalInterest !=
                                        null
                                    ? refinanceExistingLeadController
                                        .resultMainApplicantStep3
                                        .value
                                        .packageDetails![0]
                                        .preferedLoanTenure!
                                        .totalInterest
                                        .toString()
                                    : "-",
                              ),
                              DataRow(
                                isCurrency: true,
                                windowHeight: windowHeight,
                                stringData: refinanceExistingLeadController
                                            .resultMainApplicantStep3
                                            .value
                                            .packageDetails!
                                            .length >=
                                        2
                                    ? refinanceExistingLeadController
                                        .resultMainApplicantStep3
                                        .value
                                        .packageDetails![1]
                                        .preferedLoanTenure!
                                        .totalInterest
                                        .toString()
                                    : "-",
                              ),
                              DataRow(
                                isCurrency: true,
                                windowHeight: windowHeight,
                                stringData: refinanceExistingLeadController
                                            .resultMainApplicantStep3
                                            .value
                                            .packageDetails!
                                            .length >=
                                        3
                                    ? refinanceExistingLeadController
                                        .resultMainApplicantStep3
                                        .value
                                        .packageDetails![2]
                                        .preferedLoanTenure!
                                        .totalInterest
                                        .toString()
                                    : "-",
                              ),
                              // DataRow(
                              //   isCurrency: true,
                              //   windowHeight: windowHeight,
                              //   stringData: refinanceExistingLeadController
                              //               .resultMainApplicantStep3
                              //               .value
                              //               .packageDetails!
                              //               .length >=
                              //           4
                              //       ? refinanceExistingLeadController
                              //           .resultMainApplicantStep3
                              //           .value
                              //           .packageDetails![3]
                              //           .preferedLoanTenure!
                              //           .totalInterest
                              //           .toString()
                              //       : "-",
                              // ),
                              // DataRow(
                              //   isCurrency: true,
                              //   windowHeight: windowHeight,
                              //   stringData: refinanceExistingLeadController
                              //               .resultMainApplicantStep3
                              //               .value
                              //               .packageDetails!
                              //               .length >=
                              //           5
                              //       ? refinanceExistingLeadController
                              //           .resultMainApplicantStep3
                              //           .value
                              //           .packageDetails![4]
                              //           .preferedLoanTenure!
                              //           .totalInterest
                              //           .toString()
                              //       : "-",
                              // ),
                            ],
                          ),
                          TableRow(
                            children: [
                              DataRow(
                                isCurrency: false,
                                windowHeight: windowHeight,
                                stringData: 'Cash Rebate/ Subsidy',
                              ),
                              DataRow(
                                isCurrency: true,
                                windowHeight: windowHeight,
                                stringData: refinanceExistingLeadController
                                            .resultMainApplicantStep3
                                            .value
                                            .packageDetails![0]
                                            .preferedLoanTenure!
                                            .cashSubsidy !=
                                        null
                                    ? refinanceExistingLeadController
                                        .resultMainApplicantStep3
                                        .value
                                        .packageDetails![0]
                                        .preferedLoanTenure!
                                        .cashSubsidy
                                        .toString()
                                    : "-",
                              ),
                              DataRow(
                                isCurrency: true,
                                windowHeight: windowHeight,
                                stringData: refinanceExistingLeadController
                                            .resultMainApplicantStep3
                                            .value
                                            .packageDetails!
                                            .length >=
                                        2
                                    ? refinanceExistingLeadController
                                        .resultMainApplicantStep3
                                        .value
                                        .packageDetails![1]
                                        .preferedLoanTenure!
                                        .cashSubsidy
                                        .toString()
                                    : "-",
                              ),
                              DataRow(
                                isCurrency: true,
                                windowHeight: windowHeight,
                                stringData: refinanceExistingLeadController
                                            .resultMainApplicantStep3
                                            .value
                                            .packageDetails!
                                            .length >=
                                        3
                                    ? refinanceExistingLeadController
                                        .resultMainApplicantStep3
                                        .value
                                        .packageDetails![2]
                                        .preferedLoanTenure!
                                        .cashSubsidy
                                        .toString()
                                    : "-",
                              ),
                              // DataRow(
                              //   isCurrency: true,
                              //   windowHeight: windowHeight,
                              //   stringData: refinanceExistingLeadController
                              //               .resultMainApplicantStep3
                              //               .value
                              //               .packageDetails!
                              //               .length >=
                              //           4
                              //       ? refinanceExistingLeadController
                              //           .resultMainApplicantStep3
                              //           .value
                              //           .packageDetails![3]
                              //           .preferedLoanTenure!
                              //           .cashSubsidy
                              //           .toString()
                              //       : "-",
                              // ),
                              // DataRow(
                              //   isCurrency: true,
                              //   windowHeight: windowHeight,
                              //   stringData: refinanceExistingLeadController
                              //               .resultMainApplicantStep3
                              //               .value
                              //               .packageDetails!
                              //               .length >=
                              //           5
                              //       ? refinanceExistingLeadController
                              //           .resultMainApplicantStep3
                              //           .value
                              //           .packageDetails![4]
                              //           .preferedLoanTenure!
                              //           .cashSubsidy
                              //           .toString()
                              //       : "-",
                              // ),
                            ],
                          ),
                          TableRow(
                            children: [
                              DataRow(
                                isCurrency: false,
                                windowHeight: windowHeight,
                                stringData: 'Savings (First 3 years)',
                              ),
                              DataRow(
                                isCurrency: true,
                                windowHeight: windowHeight,
                                stringData: refinanceExistingLeadController
                                            .resultMainApplicantStep3
                                            .value
                                            .packageDetails![0]
                                            .preferedLoanTenure!
                                            .savings !=
                                        null
                                    ? refinanceExistingLeadController
                                        .resultMainApplicantStep3
                                        .value
                                        .packageDetails![0]
                                        .preferedLoanTenure!
                                        .savings
                                        .toString()
                                    : "-",
                              ),
                              DataRow(
                                isCurrency: true,
                                windowHeight: windowHeight,
                                stringData: refinanceExistingLeadController
                                            .resultMainApplicantStep3
                                            .value
                                            .packageDetails!
                                            .length >=
                                        2
                                    ? refinanceExistingLeadController
                                        .resultMainApplicantStep3
                                        .value
                                        .packageDetails![1]
                                        .preferedLoanTenure!
                                        .savings
                                        .toString()
                                    : "-",
                              ),
                              DataRow(
                                isCurrency: true,
                                windowHeight: windowHeight,
                                stringData: refinanceExistingLeadController
                                            .resultMainApplicantStep3
                                            .value
                                            .packageDetails!
                                            .length >=
                                        3
                                    ? refinanceExistingLeadController
                                        .resultMainApplicantStep3
                                        .value
                                        .packageDetails![2]
                                        .preferedLoanTenure!
                                        .savings
                                        .toString()
                                    : "-",
                              ),
                              // DataRow(
                              //   isCurrency: true,
                              //   windowHeight: windowHeight,
                              //   stringData: refinanceExistingLeadController
                              //               .resultMainApplicantStep3
                              //               .value
                              //               .packageDetails!
                              //               .length >=
                              //           4
                              //       ? refinanceExistingLeadController
                              //           .resultMainApplicantStep3
                              //           .value
                              //           .packageDetails![3]
                              //           .preferedLoanTenure!
                              //           .savings
                              //           .toString()
                              //       : "-",
                              // ),
                              // DataRow(
                              //   isCurrency: true,
                              //   windowHeight: windowHeight,
                              //   stringData: refinanceExistingLeadController
                              //               .resultMainApplicantStep3
                              //               .value
                              //               .packageDetails!
                              //               .length >=
                              //           5
                              //       ? refinanceExistingLeadController
                              //           .resultMainApplicantStep3
                              //           .value
                              //           .packageDetails![4]
                              //           .preferedLoanTenure!
                              //           .savings
                              //           .toString()
                              //       : "-",
                              // ),
                            ],
                          ),
                        ],
                      )
                    : const SizedBox(),
              ),
              SizedBox(
                height: windowHeight * 0.1,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class TitleValueRow extends StatelessWidget {
  TitleValueRow({Key? key, required this.title, required this.value})
      : super(key: key);
  String title;
  String value;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Text(
              title,
              style: const TextStyle(
                fontWeight: FontWeight.bold,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Expanded(
            child: Text(
              value
                  .toString()
                  .replaceAll('[', '')
                  .replaceAll(']', '')
                  .replaceAll('_', ' ')
                  .capitalize!,
              textAlign: TextAlign.right,
            ),
          ),
        ],
      ),
    );
  }
}

class DataRow extends StatelessWidget {
  final String stringData;
  const DataRow({
    Key? key,
    required this.windowHeight,
    required this.stringData,
    required this.isCurrency,
  }) : super(key: key);

  final double windowHeight;
  final bool isCurrency;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 5.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          SizedBox(
            height: windowHeight * 0.02,
          ),
          isCurrency == true && stringData != '-'
              ? Text(
                  "\$ " +
                      double.tryParse(stringData)!
                          .toStringAsFixed(2)
                          .replaceAllMapped(
                              RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))'),
                              (Match m) => '${m[1]},'),
                  style: TextStyle(fontSize: FontSize.s11),
                  textAlign: TextAlign.center,
                )
              : Text(
                  stringData,
                  style: TextStyle(fontSize: FontSize.s11),
                  textAlign: TextAlign.center,
                ),
          SizedBox(
            height: windowHeight * 0.02,
          ),
        ],
      ),
    );
  }
}

class TableTitleRowData extends StatelessWidget {
  const TableTitleRowData({
    Key? key,
    required this.windowHeight,
    required this.title,
    required this.subTitle,
  }) : super(key: key);

  final double windowHeight;
  final String title;
  final String subTitle;

  @override
  Widget build(BuildContext context) {
    return Column(
      // mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          height: windowHeight * 0.02,
        ),
        Text(
          title,
          style:
              TextStyle(fontSize: FontSize.s11, color: AppColors.kPrimaryColor),
          textAlign: TextAlign.center,
        ),
        Text(
          (subTitle),
          style:
              TextStyle(fontSize: FontSize.s11, color: AppColors.kPrimaryColor),
          textAlign: TextAlign.center,
        ),
        SizedBox(
          height: windowHeight * 0.02,
        ),
      ],
    );
  }
}
