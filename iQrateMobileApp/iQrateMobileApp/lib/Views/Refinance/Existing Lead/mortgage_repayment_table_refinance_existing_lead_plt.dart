import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/refinance_existing_lead_controller.dart';
import 'package:iqrate/Widgets/default_appbar.dart';

import '../../../DeviceManager/colors.dart';
import '../../../DeviceManager/screen_constants.dart';

class MortagageRepaymentTableRefinanceExistingLeadPlt extends StatelessWidget {
  MortagageRepaymentTableRefinanceExistingLeadPlt({Key? key}) : super(key: key);

  final RefinanceExistingLeadController refinanceExistingLeadController =
      Get.find();

  @override
  Widget build(BuildContext context) {
    debugPrint(
        refinanceExistingLeadController.yearOneToFiveList.length.toString());
    double windowHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: DefaultAppBar3(
        title: "Refinance Savings",
        windowHeight: windowHeight * 0.09,
      ),
      body: InteractiveViewer(
        minScale: 1,
        maxScale: 1,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: windowHeight * 0.1,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15.0),
              child: Text(
                'Mortgage Repayment Table',
                style: TextStyle(
                  color: AppColors.kPrimaryColor,
                  fontSize: FontSize.s22,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(
              height: windowHeight * 0.01,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15.0),
              child: Text(
                'Based On Preferred Loan Tenure',
                style: TextStyle(
                  color: const Color(0XFF9A9696),
                  fontSize: FontSize.s20,
                ),
              ),
            ),
            SizedBox(
              height: windowHeight * 0.01,
            ),
            Table(
              // defaultColumnWidth: FixedColumnWidth(windowWidth * 0.001),
              border: TableBorder.all(
                  color: const Color(0XFFE2E2E2),
                  style: BorderStyle.solid,
                  width: 2),
              children: [
                TableRow(
                  children: [
                    TableTitleRowData(
                      windowHeight: windowHeight,
                      title: 'Year',
                    ),
                    TableTitleRowData(
                      windowHeight: windowHeight,
                      title: 'Month',
                    ),
                    TableTitleRowData(
                      windowHeight: windowHeight,
                      title: 'Interest Rate',
                    ),
                    TableTitleRowData(
                      windowHeight: windowHeight,
                      title: 'Installment Amount',
                    ),
                    TableTitleRowData(
                      windowHeight: windowHeight,
                      title: 'Interest Repayment',
                    ),
                    TableTitleRowData(
                      windowHeight: windowHeight,
                      title: 'Principal Repayment',
                    ),
                    TableTitleRowData(
                      windowHeight: windowHeight,
                      title: 'Outstanding Amount',
                    ),
                    TableTitleRowData(
                      windowHeight: windowHeight,
                      title: 'Cum Interest Paid',
                    ),
                  ],
                ),
              ],
            ),
            Expanded(
              child: ListView(
                children: [
                  SingleChildScrollView(
                    child: Column(
                      children: [
                        if (refinanceExistingLeadController
                            .yearOneToFiveList.isNotEmpty)
                          Card(
                            elevation: 2,
                            child: ExpansionTile(
                              title: Text(
                                'Year 1-5',
                                style: TextStyle(
                                  fontSize: FontSize.s19,
                                  color: Colors.black,
                                ),
                              ),
                              children: [
                                ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: refinanceExistingLeadController
                                      .yearOneToFiveList.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Table(
                                      border: TableBorder.all(
                                        color: const Color(0XFFE2E2E2),
                                        style: BorderStyle.solid,
                                        width: 2,
                                      ),
                                      children: [
                                        TableRow(
                                          decoration: BoxDecoration(
                                            color: refinanceExistingLeadController
                                                        .resultMainApplicantStep3
                                                        .value
                                                        .packageDetails![1]
                                                        .preferedLoanTenure!
                                                        .mortgagePaymentList![
                                                            index]
                                                        .month
                                                        .toString() ==
                                                    '12'
                                                ? Colors.grey.shade400
                                                : Colors.white,
                                          ),
                                          children: [
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: index % 12 == 0
                                                  ? refinanceExistingLeadController
                                                      .yearOneToFiveList[index]!
                                                      .year
                                                      .toString()
                                                  : "",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  refinanceExistingLeadController
                                                      .yearOneToFiveList[index]!
                                                      .month
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  refinanceExistingLeadController
                                                          .yearOneToFiveList[
                                                              index]!
                                                          .interestRate
                                                          .toString() +
                                                      "%",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .yearOneToFiveList[index]!
                                                      .installmentAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .yearOneToFiveList[index]!
                                                      .interestRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .yearOneToFiveList[index]!
                                                      .principalRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .yearOneToFiveList[index]!
                                                      .outstandingAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .yearOneToFiveList[index]!
                                                      .cumInterestPaid
                                                      .toString(),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        if (refinanceExistingLeadController
                            .yearSixToTenList.isNotEmpty)
                          Card(
                            elevation: 2,
                            child: ExpansionTile(
                              title: Text(
                                'Year 6-10',
                                style: TextStyle(
                                  fontSize: FontSize.s19,
                                  color: Colors.black,
                                ),
                              ),
                              children: [
                                ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: refinanceExistingLeadController
                                      .yearSixToTenList.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Table(
                                      border: TableBorder.all(
                                        color: const Color(0XFFE2E2E2),
                                        style: BorderStyle.solid,
                                        width: 2,
                                      ),
                                      children: [
                                        TableRow(
                                          decoration: BoxDecoration(
                                            color: refinanceExistingLeadController
                                                        .resultMainApplicantStep3
                                                        .value
                                                        .packageDetails![1]
                                                        .preferedLoanTenure!
                                                        .mortgagePaymentList![
                                                            index]
                                                        .month
                                                        .toString() ==
                                                    '12'
                                                ? Colors.grey.shade400
                                                : Colors.white,
                                          ),
                                          children: [
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: index % 12 == 0
                                                  ? refinanceExistingLeadController
                                                      .yearSixToTenList[index]!
                                                      .year
                                                      .toString()
                                                  : "",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  refinanceExistingLeadController
                                                      .yearSixToTenList[index]!
                                                      .month
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  refinanceExistingLeadController
                                                          .yearSixToTenList[
                                                              index]!
                                                          .interestRate
                                                          .toString() +
                                                      "%",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .yearSixToTenList[index]!
                                                      .installmentAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .yearSixToTenList[index]!
                                                      .interestRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .yearSixToTenList[index]!
                                                      .principalRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .yearSixToTenList[index]!
                                                      .outstandingAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .yearSixToTenList[index]!
                                                      .cumInterestPaid
                                                      .toString(),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        if (refinanceExistingLeadController
                            .yearElevenToFifteenList.isNotEmpty)
                          Card(
                            elevation: 2,
                            child: ExpansionTile(
                              title: Text(
                                'Year 11-15',
                                style: TextStyle(
                                  fontSize: FontSize.s19,
                                  color: Colors.black,
                                ),
                              ),
                              children: [
                                ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: refinanceExistingLeadController
                                      .yearElevenToFifteenList.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Table(
                                      border: TableBorder.all(
                                        color: const Color(0XFFE2E2E2),
                                        style: BorderStyle.solid,
                                        width: 2,
                                      ),
                                      children: [
                                        TableRow(
                                          decoration: BoxDecoration(
                                            color: refinanceExistingLeadController
                                                        .resultMainApplicantStep3
                                                        .value
                                                        .packageDetails![1]
                                                        .preferedLoanTenure!
                                                        .mortgagePaymentList![
                                                            index]
                                                        .month
                                                        .toString() ==
                                                    '12'
                                                ? Colors.grey.shade400
                                                : Colors.white,
                                          ),
                                          children: [
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: index % 12 == 0
                                                  ? refinanceExistingLeadController
                                                      .yearElevenToFifteenList[
                                                          index]!
                                                      .year
                                                      .toString()
                                                  : "",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  refinanceExistingLeadController
                                                      .yearElevenToFifteenList[
                                                          index]!
                                                      .month
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  refinanceExistingLeadController
                                                          .yearElevenToFifteenList[
                                                              index]!
                                                          .interestRate
                                                          .toString() +
                                                      "%",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .yearElevenToFifteenList[
                                                          index]!
                                                      .installmentAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .yearElevenToFifteenList[
                                                          index]!
                                                      .interestRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .yearElevenToFifteenList[
                                                          index]!
                                                      .principalRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .yearElevenToFifteenList[
                                                          index]!
                                                      .outstandingAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .yearElevenToFifteenList[
                                                          index]!
                                                      .cumInterestPaid
                                                      .toString(),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        if (refinanceExistingLeadController
                            .sixteenToTwentyList.isNotEmpty)
                          Card(
                            elevation: 2,
                            child: ExpansionTile(
                              title: Text(
                                'Year 16-20',
                                style: TextStyle(
                                  fontSize: FontSize.s19,
                                  color: Colors.black,
                                ),
                              ),
                              children: [
                                ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: refinanceExistingLeadController
                                      .sixteenToTwentyList.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Table(
                                      border: TableBorder.all(
                                        color: const Color(0XFFE2E2E2),
                                        style: BorderStyle.solid,
                                        width: 2,
                                      ),
                                      children: [
                                        TableRow(
                                          decoration: BoxDecoration(
                                            color: refinanceExistingLeadController
                                                        .resultMainApplicantStep3
                                                        .value
                                                        .packageDetails![1]
                                                        .preferedLoanTenure!
                                                        .mortgagePaymentList![
                                                            index]
                                                        .month
                                                        .toString() ==
                                                    '12'
                                                ? Colors.grey.shade400
                                                : Colors.white,
                                          ),
                                          children: [
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: index % 12 == 0
                                                  ? refinanceExistingLeadController
                                                      .sixteenToTwentyList[
                                                          index]!
                                                      .year
                                                      .toString()
                                                  : "",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  refinanceExistingLeadController
                                                      .sixteenToTwentyList[
                                                          index]!
                                                      .month
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  refinanceExistingLeadController
                                                          .sixteenToTwentyList[
                                                              index]!
                                                          .interestRate
                                                          .toString() +
                                                      "%",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .sixteenToTwentyList[
                                                          index]!
                                                      .installmentAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .sixteenToTwentyList[
                                                          index]!
                                                      .interestRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .sixteenToTwentyList[
                                                          index]!
                                                      .principalRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .sixteenToTwentyList[
                                                          index]!
                                                      .outstandingAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .sixteenToTwentyList[
                                                          index]!
                                                      .cumInterestPaid
                                                      .toString(),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        if (refinanceExistingLeadController
                            .twentyOneToTwnetyFiveList.isNotEmpty)
                          Card(
                            elevation: 2,
                            child: ExpansionTile(
                              title: Text(
                                'Year 21-25',
                                style: TextStyle(
                                  fontSize: FontSize.s19,
                                  color: Colors.black,
                                ),
                              ),
                              children: [
                                ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: refinanceExistingLeadController
                                      .twentyOneToTwnetyFiveList.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Table(
                                      border: TableBorder.all(
                                        color: const Color(0XFFE2E2E2),
                                        style: BorderStyle.solid,
                                        width: 2,
                                      ),
                                      children: [
                                        TableRow(
                                          decoration: BoxDecoration(
                                            color: refinanceExistingLeadController
                                                        .resultMainApplicantStep3
                                                        .value
                                                        .packageDetails![1]
                                                        .preferedLoanTenure!
                                                        .mortgagePaymentList![
                                                            index]
                                                        .month
                                                        .toString() ==
                                                    '12'
                                                ? Colors.grey.shade400
                                                : Colors.white,
                                          ),
                                          children: [
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: index % 12 == 0
                                                  ? refinanceExistingLeadController
                                                      .twentyOneToTwnetyFiveList[
                                                          index]!
                                                      .year
                                                      .toString()
                                                  : "",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  refinanceExistingLeadController
                                                      .twentyOneToTwnetyFiveList[
                                                          index]!
                                                      .month
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  refinanceExistingLeadController
                                                          .twentyOneToTwnetyFiveList[
                                                              index]!
                                                          .interestRate
                                                          .toString() +
                                                      "%",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .twentyOneToTwnetyFiveList[
                                                          index]!
                                                      .installmentAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .twentyOneToTwnetyFiveList[
                                                          index]!
                                                      .interestRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .twentyOneToTwnetyFiveList[
                                                          index]!
                                                      .principalRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .twentyOneToTwnetyFiveList[
                                                          index]!
                                                      .outstandingAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .twentyOneToTwnetyFiveList[
                                                          index]!
                                                      .cumInterestPaid
                                                      .toString(),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        if (refinanceExistingLeadController
                            .twentySixToThirtyList.isNotEmpty)
                          Card(
                            elevation: 2,
                            child: ExpansionTile(
                              title: Text(
                                'Year 26-30',
                                style: TextStyle(
                                  fontSize: FontSize.s19,
                                  color: Colors.black,
                                ),
                              ),
                              children: [
                                ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: refinanceExistingLeadController
                                      .twentySixToThirtyList.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Table(
                                      border: TableBorder.all(
                                        color: const Color(0XFFE2E2E2),
                                        style: BorderStyle.solid,
                                        width: 2,
                                      ),
                                      children: [
                                        TableRow(
                                          decoration: BoxDecoration(
                                            color: refinanceExistingLeadController
                                                        .resultMainApplicantStep3
                                                        .value
                                                        .packageDetails![1]
                                                        .preferedLoanTenure!
                                                        .mortgagePaymentList![
                                                            index]
                                                        .month
                                                        .toString() ==
                                                    '12'
                                                ? Colors.grey.shade400
                                                : Colors.white,
                                          ),
                                          children: [
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: index % 12 == 0
                                                  ? refinanceExistingLeadController
                                                      .twentySixToThirtyList[
                                                          index]!
                                                      .year
                                                      .toString()
                                                  : "",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  refinanceExistingLeadController
                                                      .twentySixToThirtyList[
                                                          index]!
                                                      .month
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  refinanceExistingLeadController
                                                          .twentySixToThirtyList[
                                                              index]!
                                                          .interestRate
                                                          .toString() +
                                                      "%",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .twentySixToThirtyList[
                                                          index]!
                                                      .installmentAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .twentySixToThirtyList[
                                                          index]!
                                                      .interestRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .twentySixToThirtyList[
                                                          index]!
                                                      .principalRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .twentySixToThirtyList[
                                                          index]!
                                                      .outstandingAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .twentySixToThirtyList[
                                                          index]!
                                                      .cumInterestPaid
                                                      .toString(),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        if (refinanceExistingLeadController
                            .thirtyOneToThirtyFiveList.isNotEmpty)
                          Card(
                            elevation: 2,
                            child: ExpansionTile(
                              title: Text(
                                'Year 31-35',
                                style: TextStyle(
                                  fontSize: FontSize.s19,
                                  color: Colors.black,
                                ),
                              ),
                              children: [
                                ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: refinanceExistingLeadController
                                      .thirtyOneToThirtyFiveList.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Table(
                                      border: TableBorder.all(
                                        color: const Color(0XFFE2E2E2),
                                        style: BorderStyle.solid,
                                        width: 2,
                                      ),
                                      children: [
                                        TableRow(
                                          decoration: BoxDecoration(
                                            color: refinanceExistingLeadController
                                                        .resultMainApplicantStep3
                                                        .value
                                                        .packageDetails![1]
                                                        .preferedLoanTenure!
                                                        .mortgagePaymentList![
                                                            index]
                                                        .month
                                                        .toString() ==
                                                    '12'
                                                ? Colors.grey.shade400
                                                : Colors.white,
                                          ),
                                          children: [
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: index % 12 == 0
                                                  ? refinanceExistingLeadController
                                                      .thirtyOneToThirtyFiveList[
                                                          index]!
                                                      .year
                                                      .toString()
                                                  : "",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  refinanceExistingLeadController
                                                      .thirtyOneToThirtyFiveList[
                                                          index]!
                                                      .month
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  refinanceExistingLeadController
                                                          .thirtyOneToThirtyFiveList[
                                                              index]!
                                                          .interestRate
                                                          .toString() +
                                                      "%",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .thirtyOneToThirtyFiveList[
                                                          index]!
                                                      .installmentAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .thirtyOneToThirtyFiveList[
                                                          index]!
                                                      .interestRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .thirtyOneToThirtyFiveList[
                                                          index]!
                                                      .principalRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .thirtyOneToThirtyFiveList[
                                                          index]!
                                                      .outstandingAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceExistingLeadController
                                                      .thirtyOneToThirtyFiveList[
                                                          index]!
                                                      .cumInterestPaid
                                                      .toString(),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class DataRow extends StatelessWidget {
  final String stringData;
  const DataRow({
    Key? key,
    required this.windowHeight,
    required this.stringData,
  }) : super(key: key);

  final double windowHeight;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            stringData,
            style: TextStyle(fontSize: FontSize.s10),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}

class TableTitleRowData extends StatelessWidget {
  const TableTitleRowData({
    Key? key,
    required this.windowHeight,
    required this.title,
  }) : super(key: key);

  final double windowHeight;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Column(
      // mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          height: windowHeight * 0.006,
        ),
        Text(
          title,
          style: TextStyle(
            fontSize: FontSize.s10,
            color: AppColors.kPrimaryColor,
          ),
          textAlign: TextAlign.center,
        ),
        SizedBox(
          height: windowHeight * 0.006,
        ),
      ],
    );
  }
}
