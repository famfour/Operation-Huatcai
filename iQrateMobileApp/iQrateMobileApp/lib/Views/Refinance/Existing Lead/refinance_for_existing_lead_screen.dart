// ignore_for_file: unused_local_variable

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/refinance_savings_controller.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/container_styles.dart';
import 'package:iqrate/DeviceManager/screen_constants.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Widgets/default_appbar.dart';
import 'package:iqrate/Widgets/form_field_title.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/secondary_button.dart';

class RefinanceForExistingLeadScreen extends StatefulWidget {
  const RefinanceForExistingLeadScreen({Key? key}) : super(key: key);

  @override
  State<RefinanceForExistingLeadScreen> createState() =>
      _RefinanceForExistingLeadScreenState();
}

class _RefinanceForExistingLeadScreenState
    extends State<RefinanceForExistingLeadScreen> {
  final RefinanceSavingsController refinanceExistingLeadController =
      Get.put(RefinanceSavingsController());

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: DefaultAppBar3(
        title: "Refinance Savings",
        windowHeight: windowHeight * 0.09,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                child: Center(
                  child: Image.asset('assets/images/refinance-top.png'),
                ),
              ),
              SizedBox(
                height: windowHeight * 0.03,
              ),
              Center(
                child: Text(
                  'Find out with our calculator!',
                  style: TextStyle(
                    color: const Color(0XFF777373),
                    fontSize: FontSize.s24,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              SizedBox(
                height: windowHeight * 0.03,
              ),
              const FormFieldTitle(title: 'Lead name'),
              SizedBox(height: windowHeight * 0.01),
              // RequireTextField(
              //   type: Type.searchLeft,
              //   labelText: "Search name",
              //   controller: newPurchaseController.leadNameController,
              //   key: const Key("name"),
              // ),
              Container(
                height: windowHeight * 0.06,
                width: windowWidth * 0.9,
                padding: EdgeInsets.symmetric(horizontal: windowWidth * 0.03),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(
                    color: AppColors.kPrimaryColor,
                    width: 1,
                  ),
                ),
                child: DropdownButtonHideUnderline(
                  child: DropdownButton(
                    hint: Text(
                      'Leads',
                      style: TextStyles.leadsTextStyleBold,
                    ),
                    value: refinanceExistingLeadController.selectedValue,
                    onChanged: (String? value) {
                      setState(() {
                        refinanceExistingLeadController.selectedValue = value!;
                      });
                    },
                    style: TextStyles.leadsTextStyleBold,
                    icon: const Icon(
                      Icons.keyboard_arrow_down_outlined,
                      color: Colors.black,
                    ),
                    items: refinanceExistingLeadController.leadNamesForDropdown
                        .map((value) {
                      return DropdownMenuItem(
                        child: Text(value),
                        value: value,
                      );
                    }).toList(),
                  ),
                ),
              ),
              SizedBox(
                height: windowHeight * 0.03,
              ),
              Center(
                child: PrimaryButton(
                  windowHeight: windowHeight,
                  windowWidth: windowWidth,
                  buttonTitle: "Submit",
                  onPressed: () {
                    refinanceExistingLeadController.onTapSubmitExitingLead();
                  },
                ),
              ),
              SizedBox(
                height: windowHeight * 0.03,
              ),
              SecondaryButton(
                windowHeight: windowHeight,
                windowWidth: windowWidth,
                kGradientBoxDecoration:
                    ContainerStyles.kGradientBoxDecorationSecondaryButton,
                kInnerDecoration:
                    ContainerStyles.kInnerDecorationSecondaryButton,
                onPressed: () {
                  refinanceExistingLeadController.onCancel();
                },
                buttonTitle: "Cancel",
              ),
            ],
          ),
        ),
      ),
    );
  }
}
