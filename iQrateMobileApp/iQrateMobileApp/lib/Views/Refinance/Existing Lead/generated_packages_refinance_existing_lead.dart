// ignore_for_file: invalid_use_of_protected_member, must_be_immutable, unnecessary_null_comparison

import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/refinance_existing_lead_controller.dart';
import 'package:iqrate/Model/response_model.dart/refinance_new_lead_package_response_model_v2.dart';
import 'package:iqrate/Model/send_model.dart/bank_package_model_refinance.dart';
import 'package:iqrate/Utils/config.dart';
import 'package:iqrate/Views/Refinance/rates_view_details_for_refinance.dart';
import 'package:iqrate/Widgets/default_appbar.dart';
import 'package:iqrate/Widgets/primary_button.dart';

import '../../../DeviceManager/colors.dart';
import '../../../DeviceManager/screen_constants.dart';
import '../../../DeviceManager/text_styles.dart';

class GeneratedPackagesRefinanceExistingLead extends StatefulWidget {
  const GeneratedPackagesRefinanceExistingLead({Key? key}) : super(key: key);

  @override
  State<GeneratedPackagesRefinanceExistingLead> createState() =>
      _GeneratedPackagesRefinanceExistingLeadState();
}

class _GeneratedPackagesRefinanceExistingLeadState
    extends State<GeneratedPackagesRefinanceExistingLead> {
  late double windowHeight;
  late double windowWidth;

  @override
  Widget build(BuildContext context) {
    final RefinanceExistingLeadController refinanceExistingLeadController =
        Get.find();

    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.width;
    return WillPopScope(
        onWillPop: () async {
          refinanceExistingLeadController.activeStep.value = 1;
          Get.back();
          return true;
        },
        child: Scaffold(
          appBar: DefaultAppBarCallBack(
            windowHeight: windowHeight * 0.1,
            title: 'Refinance Existing Lead',
            onTapClose: () {
              refinanceExistingLeadController.activeStep.value = 1;
              Get.back();
            },
          ),
          body: Obx(
            () => SingleChildScrollView(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20.0, vertical: 15),
                child: Column(
                  children: [
                    Container(
                      padding: const EdgeInsets.symmetric(
                          vertical: 15, horizontal: 20),
                      width: windowWidth,
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                          borderRadius:
                              const BorderRadius.all(Radius.circular(10.0))),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Our AI has generated the lowest packages based on your requirements! Select rates to see comparison table",
                            style: TextStyles.dashboardPopUpTextStyle,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: windowHeight * 0.03,
                    ),
                    GeneratePackagesHeading(
                      windowHeight: windowHeight,
                      windowWidth: windowWidth,
                      title: 'Lowest Fixed',
                    ),
                    SizedBox(
                      height: windowHeight * 0.02,
                    ),
                    refinanceExistingLeadController
                                .lowestPackageListModel.value.id !=
                            null
                        ? GeneratedPackagesCard(
                            windowHeight: windowHeight,
                            windowWidth: windowWidth,
                            image: refinanceExistingLeadController
                                        .lowestPackageListModel.value.bank ==
                                    null
                                ? AppConfig.blankImage
                                : refinanceExistingLeadController
                                    .lowestPackageListModel.value.bank!.logo
                                    .toString(),
                            bankName: refinanceExistingLeadController
                                .lowestPackageListModel.value.bank!.name
                                .toString(),
                            rateType: refinanceExistingLeadController
                                .lowestPackageListModel.value.rateType
                                .toString(),
                            checkBoxValue: refinanceExistingLeadController
                                .lowestFixedPackageOneCheckbox.value,
                            onCheckboxSelected: (val) {
                              refinanceExistingLeadController
                                  .lowestFixedPackageOneCheckbox.value = val!;
                              log(refinanceExistingLeadController
                                  .lowestPackageListModel
                                  .value
                                  .rates!
                                  .first
                                  .totalInterestRate
                                  .toString());
                              BankPackageModel bankPackageModel =
                                  BankPackageModel(
                                id: refinanceExistingLeadController
                                    .lowestPackageListModel.value.bank!.id,
                                bank: refinanceExistingLeadController
                                    .lowestPackageListModel.value.bank!.name
                                    .toString(),
                                packageId: refinanceExistingLeadController
                                    .lowestPackageListModel.value.id,
                                bankName: refinanceExistingLeadController
                                    .lowestPackageListModel.value.bank!.name
                                    .toString(),
                                interestRate: refinanceExistingLeadController
                                            .lowestPackageListModel
                                            .value
                                            .rates!
                                            .first
                                            .year ==
                                        'year_1'
                                    ? double.tryParse(
                                        refinanceExistingLeadController
                                            .lowestPackageListModel
                                            .value
                                            .rates!
                                            .first
                                            .totalInterestRate
                                            .toString())
                                    : double.tryParse(
                                        refinanceExistingLeadController
                                            .lowestPackageListModel
                                            .value
                                            .rates!
                                            .last
                                            .totalInterestRate
                                            .toString(),
                                      ),
                              );

                              if (refinanceExistingLeadController
                                      .lowestFixedPackageOneCheckbox.value ==
                                  true) {
                                refinanceExistingLeadController
                                    .bankPackageModelList
                                    .add(bankPackageModel);
                              } else {
                                refinanceExistingLeadController
                                    .bankPackageModelList
                                    .removeWhere((element) =>
                                        element.id == bankPackageModel.id);
                              }

                              debugPrint(jsonEncode(bankPackageModel.toJson())
                                  .toString());

                              debugPrint(refinanceExistingLeadController
                                  .bankPackageModelList
                                  .toString());
                            },
                            data: refinanceExistingLeadController
                                .lowestPackageListModel.value,
                            onTapDetails: () {
                              debugPrint("onTap details");

                              Get.to(RatesViewDetailsForRefinanceNewLead(
                                  data: refinanceExistingLeadController
                                      .lowestPackageListModel.value));
                            },
                          )
                        : Center(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                SizedBox(
                                    height: windowHeight * 0.15,
                                    child: SvgPicture.asset(
                                        'assets/images/nodata.svg')),
                                Text(
                                  'No data to show',
                                  style: TextStyles
                                      .bankSubmissionCardTitleDisabled,
                                )
                              ],
                            ),
                          ),
                    SizedBox(
                      height: windowHeight * 0.03,
                    ),
                    GeneratePackagesHeading(
                      windowHeight: windowHeight,
                      windowWidth: windowWidth,
                      title: 'Lowest Floating',
                    ),
                    SizedBox(
                      height: windowHeight * 0.03,
                    ),
                    Obx(
                      () => refinanceExistingLeadController
                                  .floatingPackageListModel.value.id !=
                              null
                          ? GeneratedPackagesCard(
                              windowHeight: windowHeight,
                              windowWidth: windowWidth,
                              image: refinanceExistingLeadController
                                          .floatingPackageListModel
                                          .value
                                          .bank ==
                                      null
                                  ? AppConfig.blankImage
                                  : refinanceExistingLeadController
                                      .floatingPackageListModel.value.bank!.logo
                                      .toString(),
                              bankName: refinanceExistingLeadController
                                  .floatingPackageListModel.value.bank!.name
                                  .toString(),
                              rateType: refinanceExistingLeadController
                                  .floatingPackageListModel.value.rateType
                                  .toString(),
                              checkBoxValue: refinanceExistingLeadController
                                  .lowsetFloatingPackageOneCheckbox.value,
                              onCheckboxSelected: (val) {
                                refinanceExistingLeadController
                                    .lowsetFloatingPackageOneCheckbox
                                    .value = val!;

                                BankPackageModel bankPackageModel =
                                    BankPackageModel(
                                  id: refinanceExistingLeadController
                                      .floatingPackageListModel.value.bank!.id,
                                  bank: refinanceExistingLeadController
                                      .floatingPackageListModel.value.bank!.name
                                      .toString(),
                                  packageId: refinanceExistingLeadController
                                      .floatingPackageListModel.value.id,
                                  bankName: refinanceExistingLeadController
                                      .floatingPackageListModel.value.bank!.name
                                      .toString(),
                                  // interestRate: double.tryParse(
                                  //   refinanceExistingLeadController
                                  //       .floatingPackageListModel
                                  //       .value
                                  //       .rates!
                                  //       .first
                                  //       .totalInterestRate
                                  //       .toString(),
                                  // ),
                                  interestRate: refinanceExistingLeadController
                                              .floatingPackageListModel
                                              .value
                                              .rates!
                                              .first
                                              .year ==
                                          'year_1'
                                      ? double.tryParse(
                                          refinanceExistingLeadController
                                              .floatingPackageListModel
                                              .value
                                              .rates!
                                              .first
                                              .totalInterestRate
                                              .toString(),
                                        )
                                      : double.tryParse(
                                          refinanceExistingLeadController
                                              .floatingPackageListModel
                                              .value
                                              .rates!
                                              .last
                                              .totalInterestRate
                                              .toString(),
                                        ),
                                );

                                debugPrint(jsonEncode(bankPackageModel.toJson())
                                    .toString());

                                if (refinanceExistingLeadController
                                        .lowsetFloatingPackageOneCheckbox
                                        .value ==
                                    true) {
                                  refinanceExistingLeadController
                                      .bankPackageModelList
                                      .add(bankPackageModel);
                                } else {
                                  refinanceExistingLeadController
                                      .bankPackageModelList
                                      .removeWhere((element) =>
                                          element.id == bankPackageModel.id);
                                }

                                debugPrint(refinanceExistingLeadController
                                    .bankPackageModelList
                                    .toString());
                              },
                              data: refinanceExistingLeadController
                                  .floatingPackageListModel.value,
                              onTapDetails: () {
                                debugPrint(refinanceExistingLeadController
                                    .floatingPackageListModel
                                    .value
                                    .rates!
                                    .length
                                    .toString());
                                Get.to(RatesViewDetailsForRefinanceNewLead(
                                    data: refinanceExistingLeadController
                                        .lowestPackageListModel.value));
                              },
                            )
                          : Center(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  SizedBox(
                                      height: windowHeight * 0.15,
                                      child: SvgPicture.asset(
                                          'assets/images/nodata.svg')),
                                  Text(
                                    'No data to show',
                                    style: TextStyles
                                        .bankSubmissionCardTitleDisabled,
                                  )
                                ],
                              ),
                            ),
                    ),
                    SizedBox(
                      height: windowHeight * 0.03,
                    ),
                    PrimaryButton(
                      windowHeight: windowHeight,
                      windowWidth: windowWidth,
                      buttonTitle: 'Generate Report',
                      onPressed: () {
                        refinanceExistingLeadController
                            .onTapContinueToReports();
                        //refinanceExistingLeadController.bankPackageModelList.clear();
                        /* debugPrint(
                      jsonEncode(refinanceExistingLeadController.bankPackageModelList)
                          .toString(),
                    );*/
                      },
                    ),
                    SizedBox(
                      height: windowHeight * 0.03,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}

class GeneratedPackagesCard extends StatefulWidget {
  final double windowHeight;
  final double windowWidth;
  final String image;
  final String bankName;
  final String rateType;
  bool checkBoxValue;
  Function(bool?) onCheckboxSelected;
  // Function onCheckboxUnselected;
  Function onTapDetails;
  final Fixed data;

  GeneratedPackagesCard({
    Key? key,
    required this.windowHeight,
    required this.windowWidth,
    required this.image,
    required this.bankName,
    required this.rateType,
    required this.checkBoxValue,
    required this.onCheckboxSelected,
    required this.onTapDetails,
    required this.data,
    // required this.onCheckboxUnselected,
  }) : super(key: key);

  @override
  State<GeneratedPackagesCard> createState() => _GeneratedPackagesCardState();
}

class _GeneratedPackagesCardState extends State<GeneratedPackagesCard> {
  RefinanceExistingLeadController refinanceExistingLeadController = Get.find();
  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    return Card(
      elevation: 10,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      child: Container(
        height: widget.windowHeight * 0.2,
        width: widget.windowWidth,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    flex: 1,
                    child: Image.network(
                      widget.image,
                      height: widget.windowHeight * 0.1,
                      width: widget.windowWidth * 0.2,
                      // fit: BoxFit.cover,
                    ),
                  ),
                  SizedBox(
                    width: windowWidth * 0.04,
                  ),
                  Expanded(
                    flex: 3,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.bankName,
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: FontSize.s16,
                          ),
                        ),
                        SizedBox(
                          height: windowHeight * 0.01,
                        ),
                        Text(
                          '${widget.rateType.toString().replaceAll('_', ' ').capitalizeFirst} (${widget.data.rateCategory.toString().capitalizeFirst})',
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: FontSize.s16,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Checkbox(
                      value: widget.checkBoxValue,
                      onChanged: (bool? value) {
                        //! Changed here for your reference __________________________________
                        setState(() {
                          widget.checkBoxValue = !widget.checkBoxValue;
                          widget.onCheckboxSelected(value);
                          debugPrint(value.toString());
                        });
                      },
                    ),
                  ),
                ],
              ),
            ),
            rowTwo(),
            GestureDetector(
              onTap: () {
                Get.to(RatesViewDetailsForRefinanceNewLead(data: widget.data));
                widget.onTapDetails;
              },
              child: Container(
                height: widget.windowHeight * 0.04,
                decoration: const BoxDecoration(
                  color: Colors.red,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20),
                  ),
                ),
                child: Center(
                  child: Text(
                    'Details',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: FontSize.s16,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget rowTwo() {
    var rates = widget.data.rates![0].year.toString() != 'year_1'
        ? widget.data.rates!.reversed
        : widget.data.rates;
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 5),
      child: Row(
        children: [
          Expanded(
            child: Center(
              child: SizedBox(
                height: 20.0,
                child: ListView.builder(
                  physics: const ClampingScrollPhysics(),
                  shrinkWrap: true,
                  primary: false,
                  scrollDirection: Axis.horizontal,
                  itemCount: rates!.length > 3 ? 3 : rates.length,
                  itemBuilder: (BuildContext context, int index) => Row(
                    children: [
                      Container(
                        height: widget.windowHeight * 0.029,
                        width: widget.windowWidth * 0.25,
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                        ),
                        child: Center(
                          child: Text(
                            rates
                                    .elementAt(index)
                                    .year
                                    .toString()
                                    .replaceAll("_", " ")
                                    .capitalizeFirst! +
                                " : " +
                                (rates.elementAt(index).totalInterestRate)
                                    .toString() +
                                "%",
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
          /* Expanded(
            child: ListView.builder(
              shrinkWrap: true,
              primary: false,
              scrollDirection: Axis.horizontal,
              itemCount: 2,
              itemBuilder: (BuildContext context, int index) =>  Row(
                children: [
                  titleValueRow(title: 'Year 1', value: data.rates!.elementAt(0).referenceRate.toString()),
                  const VerticalDivider(
                    color: Colors.black,
                  ),
                ],
              ),

            ),
          ),*/

          /*Expanded(child: titleValueRow(title: 'Year 2', value: '1.000%')),
          const VerticalDivider(
            color: Colors.black,
          ),
          Expanded(child: titleValueRow(title: 'Year 3', value: '1.000%')),*/
        ],
      ),
    );
  }
}

class GeneratePackagesHeading extends StatelessWidget {
  const GeneratePackagesHeading({
    Key? key,
    required this.windowHeight,
    required this.windowWidth,
    required this.title,
  }) : super(key: key);

  final double windowHeight;
  final double windowWidth;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: windowHeight * 0.03,
          padding: EdgeInsets.only(left: windowWidth * 0.01),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                title,
                style: TextStyles.basicTextStyle,
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: windowWidth * 0.01),
          child: Divider(
            thickness: 1,
            color: AppColors.leadListExpansionBG,
          ),
        ),
      ],
    );
  }
}
