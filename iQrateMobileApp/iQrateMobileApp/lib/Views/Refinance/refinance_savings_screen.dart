// ignore_for_file: unused_local_variable

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/refinance_savings_controller.dart';
import 'package:iqrate/DeviceManager/screen_constants.dart';
import 'package:iqrate/Views/more_view.dart';
import 'package:iqrate/Widgets/default_appbar.dart';

class RefinanceSavingsScreen extends StatefulWidget {
  const RefinanceSavingsScreen({Key? key}) : super(key: key);

  @override
  State<RefinanceSavingsScreen> createState() => _RefinanceSavingsScreenState();
}

class _RefinanceSavingsScreenState extends State<RefinanceSavingsScreen> {
  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    final RefinanceSavingsController tefinanceSavingsController =
        Get.put(RefinanceSavingsController());
    return Scaffold(
      appBar: DefaultAppBar(
        title: "Refinance Savings",
        windowHeight: windowHeight * 0.09,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                child: Center(
                  child: Image.asset('assets/images/refinance-top.png'),
                ),
              ),
              SizedBox(
                height: windowHeight * 0.03,
              ),
              Text(
                'Find out with our calculator!',
                style: TextStyle(
                  color: const Color(0XFF777373),
                  fontSize: FontSize.s24,
                  fontWeight: FontWeight.w600,
                ),
              ),
              SizedBox(
                height: windowHeight * 0.03,
              ),
              GridView.count(
                shrinkWrap: true,
                primary: false,
                padding: EdgeInsets.all(windowHeight * 0.02),
                crossAxisCount: 2,
                mainAxisSpacing: windowHeight * 0.02,
                crossAxisSpacing: windowHeight * 0.02,
                children: [
                  GridItemWithoutSubText(
                    assetImage: 'assets/images/existing-lead.svg',
                    title: 'Calculate For\nExisting Lead',
                    onTap: () {
                      tefinanceSavingsController.onTapCalculateExistingLead();
                    },
                  ),
                  GridItemWithoutSubText(
                    assetImage: 'assets/images/refin1.svg',
                    title: 'Calculate for\nNew Lead(s)',
                    onTap: () {
                      tefinanceSavingsController.onTapCalculateNewLead();
                    },
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
