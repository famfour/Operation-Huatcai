// ignore_for_file: prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:iqrate/Controller/leads_view_controller.dart';
import 'package:iqrate/Controller/rates_view_controller.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Model/common_key_value_model.dart';

import 'package:iqrate/Model/response_model.dart/refinance_new_lead_package_response_model_v2.dart';
import 'package:iqrate/Views/generate_packages.dart';
import 'package:iqrate/Widgets/common_appbar_options.dart';

class RatesViewDetailsForRefinance extends StatefulWidget {
  final Fixed data;

  const RatesViewDetailsForRefinance({Key? key, required this.data})
      : super(key: key);

  @override
  State<RatesViewDetailsForRefinance> createState() =>
      _RatesViewDetailsForRefinanceState();
}

class _RatesViewDetailsForRefinanceState
    extends State<RatesViewDetailsForRefinance> {
  late double windowHeight;
  late double windowWidth;

  final RatesViewController ratesViewController = Get.find();

  @override
  Widget build(BuildContext context) {
    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.width;

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          automaticallyImplyLeading: false,
          centerTitle: false,
          flexibleSpace: const Padding(
            padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 30),
            child: CommonAppBarOptions(
              windowWidth: 0,
              title: 'Refinance Savings',
            ),
          ),
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(windowHeight * 0.03),
            child: const SizedBox(),
          ),
          backgroundColor: const Color(0xFFDF5356),
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.only(top: windowWidth * 0.05),
            child: LoanPackageCardFixed(
              data: widget.data,
              image: widget.data.bank!.logo!,
              headingSubText:
                  '${widget.data.rateType.toString().replaceAll("_", " ").replaceAll('Ratetype.', '').capitalize} (${widget.data.rateCategory.toString().capitalize})',
              headingText: widget.data.bank!.name!,
              onChanged: (val) {
                // setState(() {
                //   widget.data.isChecked = val!;
                // });
              },
              checkBoxValue: widget.data.isChecked,
              cardIndex: 0,
            ),
          ),
        ));
  }
}

class RatesViewDetailsForRefinanceNewLead extends StatefulWidget {
  final Fixed data;

  const RatesViewDetailsForRefinanceNewLead({Key? key, required this.data})
      : super(key: key);

  @override
  State<RatesViewDetailsForRefinanceNewLead> createState() =>
      _RatesViewDetailsForRefinanceStateNewLead();
}

class _RatesViewDetailsForRefinanceStateNewLead
    extends State<RatesViewDetailsForRefinanceNewLead> {
  late double windowHeight;
  late double windowWidth;

  final RatesViewController ratesViewController = Get.find();

  @override
  Widget build(BuildContext context) {
    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.width;

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          automaticallyImplyLeading: false,
          centerTitle: false,
          flexibleSpace: const Padding(
            padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 30),
            child: CommonAppBarOptions(
              windowWidth: 0,
              title: 'Refinance Savings',
            ),
          ),
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(windowHeight * 0.03),
            child: const SizedBox(),
          ),
          backgroundColor: const Color(0xFFDF5356),
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.only(top: windowWidth * 0.05),
            child: LoanPackageCardNewLead(
              data: widget.data,
              image: widget.data.bank!.logo!,
              headingSubText:
                  '${widget.data.rateType.toString().replaceAll("_", " ").capitalizeFirst}(${widget.data.rateCategory.toString().capitalizeFirst})',
              headingText: widget.data.bank!.name!,
              onChanged: (val) {
                // setState(() {
                //   widget.data.isChecked = val!;
                // });
              },
              checkBoxValue: widget.data.isChecked,
              cardIndex: 0,
            ),
          ),
        ));
  }
}

// ignore: must_be_immutable
class LoanPackageCardNewLead extends StatelessWidget {
  String? image;
  String headingText;
  String headingSubText;
  void Function(bool?)? onChanged;
  bool checkBoxValue;
  int cardIndex;
  final Fixed data;

  LoanPackageCardNewLead(
      {Key? key,
      this.image,
      required this.headingText,
      required this.headingSubText,
      required this.onChanged,
      required this.checkBoxValue,
      required this.cardIndex,
      required this.data})
      : super(key: key);

  final RatesViewController ratesViewController = Get.find();
  final LeadsViewController leadsViewController = Get.find();

  late double windowHeight;
  late double windowWidth;

  late var rates;

  @override
  Widget build(BuildContext context) {
    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.width;
    rates = data.rates![0].year.toString() != 'year_1'
        ? data.rates!.reversed
        : data.rates;

    return Card(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20), topRight: Radius.circular(20))),
      elevation: 5,
      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Column(
        children: [rowOne(), rowTwo(), expansionSection(cardIndex)],
      ),
    );
  }

  // 1st row that shows the Bank name , image and a checkbox

  Widget rowOne() {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(30)),
                color: Colors.deepOrangeAccent.withOpacity(.3)),
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: image!.isURL
                  ? Image.network(
                      image!,
                      width: 100,
                      height: 50,
                    )
                  : const SizedBox(width: 100, height: 50),
            ),
          ),
          Expanded(
              child: Padding(
            padding: const EdgeInsets.only(left: 15.0),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text(
                headingText,
                style: TextStyles.cardHeading,
              ),
              const SizedBox(height: 10),
              Text(
                headingSubText,
                style: TextStyles.cardSubHeading,
              )
            ]),
          )),
          // Checkbox(
          //   value: data.isChecked,
          //   onChanged: onChanged,
          //   //checkColor: AppColors.kSecondaryColor,
          //   //fillColor: MaterialStateProperty.all(Colors.white),
          //   hoverColor: AppColors.kSecondaryColor,
          //   activeColor: AppColors.kSecondaryColor,
          //   side: const BorderSide(
          //       color: AppColors.kSecondaryColor,
          //       width: 2,
          //       style: BorderStyle.solid),
          // )
        ],
      ),
    );
  }

  // 2nd row that shows the percentage on year based
  Widget rowTwo() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 5),
      child: Row(
        children: [
          Expanded(
            child: Center(
              child: SizedBox(
                height: 20.0,
                child: ListView.builder(
                  physics: const ClampingScrollPhysics(),
                  shrinkWrap: true,
                  primary: false,
                  scrollDirection: Axis.horizontal,
                  itemCount: rates.length > 3 ? 3 : rates.length,
                  itemBuilder: (BuildContext context, int index) => Row(
                    children: [
                      const SizedBox(
                        width: 10,
                      ),
                      titleValueRow(
                          title: rates
                                  .elementAt(index)
                                  .year
                                  .toString()
                                  .replaceAll("_", " ")
                                  .capitalizeFirst! +
                              " ",
                          value: rates
                                  .elementAt(index)
                                  .totalInterestRate
                                  .toString() +
                              "%"),
                      const SizedBox(
                        width: 10,
                      ),
                      index < 2
                          ? const VerticalDivider(
                              color: Colors.black,
                            )
                          : const SizedBox(),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  // A simple widget used in the 2nd row, which only shows a title and a value
  Widget titleValueRow({required String title, required String value}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          title + ': ',
          style: TextStyles.headingSubtitleStyle,
        ),
        Text(
          value,
          style: TextStyles.headingSubtitleStyle,
        )
      ],
    );
  }

  // A simple widget used in the 2nd row, which only shows a title and a value
  Widget titleValueRow2({required String title, required String value}) {
    return Padding(
      padding:
          const EdgeInsets.only(right: 10.0, left: 10.0, top: 3, bottom: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 3,
            child: Text(
              title,
              style: TextStyles.headingSubtitleStyle2,
            ),
          ),
          //Spacer(),
          Expanded(
            flex: 7,
            child: Text(
              value,
              style: TextStyles.headingSubtitleStyle2,
            ),
          )
        ],
      ),
    );
  }

  Widget titleValueRow3({required String title, required dynamic value}) {
    return Padding(
      padding:
          const EdgeInsets.only(right: 10.0, left: 10.0, top: 8, bottom: 5),
      //padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: 4,
                child: Text(
                  title,
                  style: TextStyles.headingSubtitleStyle4,
                ),
              ),
              SizedBox(
                width: windowWidth * 0.01,
              ),
              Flexible(
                flex: 4,
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    value,
                    style: TextStyles.headingSubtitleStyle2,
                    textAlign: TextAlign.left,
                  ),
                ),
              )
            ],
          ),
          const Divider(
            thickness: 1,
            color: Colors.black26,
          )
        ],
      ),
    );
  }

  Widget titleValueRowWithListForType(
      {required String title, required List<String> values}) {
    return Padding(
      padding:
          const EdgeInsets.only(right: 10.0, left: 10.0, top: 8, bottom: 5),
      //padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: 4,
                child: Text(
                  title,
                  style: TextStyles.headingSubtitleStyle4,
                ),
              ),
              SizedBox(
                width: windowWidth * 0.05,
              ),
              Flexible(
                flex: 4,
                child: ListView.builder(
                    physics: const ClampingScrollPhysics(),
                    shrinkWrap: true,
                    primary: false,
                    scrollDirection: Axis.vertical,
                    itemCount: values.length,
                    itemBuilder: (BuildContext context, int index) => Align(
                          alignment: Alignment.topRight,
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 8.0),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                values.elementAt(index).trim() +
                                    ((values.length - 1) == index ? "" : ","),
                                style: TextStyles.headingSubtitleStyle2,
                                /*maxLines: 1,
                                overflow: TextOverflow.ellipsis,*/
                                textAlign: TextAlign.start,
                              ),
                            ),
                          ),
                        )),
              )
            ],
          ),
          const Divider(
            thickness: 1,
            color: Colors.black26,
          )
        ],
      ),
    );
  }

  Widget titleValueRowWithList(
      {required String title, required List<String> values}) {
    return Padding(
      padding:
          const EdgeInsets.only(right: 10.0, left: 10.0, top: 8, bottom: 5),
      //padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: 4,
                child: Text(
                  title,
                  style: TextStyles.headingSubtitleStyle4,
                ),
              ),
              const SizedBox(
                width: 5,
              ),
              Flexible(
                flex: 4,
                child: ListView.builder(
                    physics: const ClampingScrollPhysics(),
                    shrinkWrap: true,
                    primary: false,
                    scrollDirection: Axis.vertical,
                    itemCount: values.length,
                    itemBuilder: (BuildContext context, int index) => Align(
                          alignment: Alignment.centerLeft,
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 8.0),
                            child: Text(
                              values.elementAt(index).trim() == "NA"
                                  ? "NA"
                                  : '${values.elementAt(index).replaceAll("_", " ").capitalizeFirst}',
                              style: TextStyles.headingSubtitleStyle2,
                              /*maxLines: 1,
                              overflow: TextOverflow.ellipsis,*/
                              textAlign: TextAlign.left,
                            ),
                          ),
                        )),
              )
            ],
          ),
          const Divider(
            thickness: 1,
            color: Colors.black26,
          )
        ],
      ),
    );
  }

  // This sections expands to show additional details in the specific
  Widget expansionSection(int index) {
    return InkWell(
      onTap: () {
        //ratesViewController.expandingCard(index);
      },
      child: ClipRRect(
        /*borderRadius: const BorderRadius.only(
            bottomLeft: Radius.circular(15), bottomRight: Radius.circular(15)),*/
        child: Container(
            color: AppColors.kSecondaryColor,
            child: Container(
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  ListView(
                    primary: false,
                    shrinkWrap: true,
                    children: [
                      expandableCardRates(),
                      SizedBox(height: windowHeight * 0.02),
                      expandableCardKeyFeatures(),
                      SizedBox(height: windowHeight * 0.02),
                      expandableCardBankSubsidy(),
                      SizedBox(height: windowHeight * 0.02),
                      expandableCardEarlyRepaymentPenalty(),
                    ],
                  )
                ],
              ),
            )),
      ),
    );
  }

  Widget expandableCardRates() {
    return GestureDetector(
      onTap: () {
        ratesViewController.isExpandedRates.toggle();
      },
      child: Obx(() => Container(
            decoration: const BoxDecoration(boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.black54,
                  blurRadius: 5.0,
                  offset: Offset(0.0, 0.3))
            ], color: Colors.white),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.all(windowWidth * 0.02),
                  color: AppColors.red,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text("Rates", style: TextStyles.basicTextStyle2),
                      const Spacer(),
                      const Padding(
                        padding: EdgeInsets.only(bottom: 10.0),
                        child: Icon(Icons.minimize, color: Colors.white),
                      ),
                    ],
                  ),
                ),
                ratesViewController.isExpandedRates.value
                    ? Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 7.0),
                            child: ListView.builder(
                              //physics: ClampingScrollPhysics(),
                              shrinkWrap: true,
                              primary: false,
                              scrollDirection: Axis.vertical,
                              itemCount: rates.length,
                              itemBuilder: (BuildContext context, int index) =>
                                  titleValueRow2(
                                      title: rates
                                              .elementAt(index)
                                              .year
                                              .toString()
                                              .replaceAll("_", " ")
                                              .capitalizeFirst! +
                                          " ",
                                      value: rates
                                                  .elementAt(index)
                                                  .referenceRate
                                                  .rateType
                                                  .toString()
                                                  .toLowerCase() !=
                                              "fixed"
                                          ? "${rates.elementAt(index).referenceRate.reference} (${rates.elementAt(index).referenceRate.interestRate.toString()})% ${rates.elementAt(index).referenceRate.equation.toString()} ${rates.elementAt(index).bankSpread.toString()}% = " +
                                              rates
                                                  .elementAt(index)
                                                  .totalInterestRate
                                                  .toString() +
                                              "%"
                                          : rates
                                                  .elementAt(index)
                                                  .referenceRate
                                                  .reference
                                                  .toString()
                                                  .capitalize! +
                                              ' ' +
                                              rates
                                                  .elementAt(index)
                                                  .totalInterestRate
                                                  .toString() +
                                              "%"),
                            ),
                          ),
                        ],
                      )
                    : Container(),
              ],
            ),
          )),
    );
  }

  Widget expandableCardKeyFeatures() {
    List<CommonKeyValueModel> propertyType = [];
    List<String> propertyTypeValues = [];

    for (var element in leadsViewController.kvStoreValues) {
      if (element.code == "property_type") {
        //debugPrint("=====kvstore====="+element.value.toString());
        element.value.forEach((key, value) {
          propertyType.add(CommonKeyValueModel(key, value));
          debugPrint("array_key:: " + value);
        });
      }
    }

    for (var key in data.propertyTypes!) {
      for (var element in propertyType) {
        if (key == element.key) {
          propertyTypeValues.add(element.value);
        }
      }
    }

    List<CommonKeyValueModel> propertyStatuses = [];
    List<String> propertyStatusValues = [];

    for (var element in leadsViewController.kvStoreValues) {
      if (element.code == "property_status") {
        //debugPrint("=====kvstore====="+element.value.toString());
        element.value.forEach((key, value) {
          propertyStatuses.add(CommonKeyValueModel(key, value));
          debugPrint("array_key:: " + value);
        });
      }
    }

    for (var key in data.propertyStatus!) {
      for (var element in propertyStatuses) {
        if (key == element.key) {
          propertyStatusValues.add(element.value);
        }
      }
    }

    List<CommonKeyValueModel> loanType = [];
    List<String> loanTypeValues = [];

    for (var element in leadsViewController.kvStoreValues) {
      if (element.code == "loan_category") {
        //debugPrint("=====kvstore====="+element.value.toString());
        element.value.forEach((key, value) {
          loanType.add(CommonKeyValueModel(key, value));
          debugPrint("array_key:: " + value);
        });
      }
    }

    for (var key in data.loanCategory!) {
      for (var element in loanType) {
        if (key == element.key) {
          loanTypeValues.add(element.value);
        }
      }
    }

    return GestureDetector(
      onTap: () {
        ratesViewController.isExpandedKeyFeatures.toggle();
      },
      child: Obx(() => Container(
            decoration: const BoxDecoration(boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.black54,
                  blurRadius: 5.0,
                  offset: Offset(0.0, 0.3))
            ], color: Colors.white),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.all(windowWidth * 0.02),
                  color: AppColors.red,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text("Key Features", style: TextStyles.basicTextStyle2),
                      const Spacer(),
                      const Padding(
                        padding: EdgeInsets.only(bottom: 10.0),
                        child: Icon(Icons.minimize, color: Colors.white),
                      ),
                    ],
                  ),
                ),
                ratesViewController.isExpandedKeyFeatures.value
                    ? Padding(
                        padding: const EdgeInsets.symmetric(vertical: 3.0),
                        child: Column(
                          children: [
                            titleValueRowWithListForType(
                                title: "Property Type",
                                values: propertyTypeValues),
                            titleValueRowWithListForType(
                                title: "Property Status",
                                values: propertyStatusValues),
                            titleValueRowWithListForType(
                                title: "Loan Type", values: loanTypeValues),
                            titleValueRow3(
                                title: "Lock in Period",
                                value: data.lockInPeriod.toString() + " Years"),
                            titleValueRow3(
                                title: "Min Loan Amount",
                                value: "\$" +
                                    getFormattedAmount(
                                        data.minLoanAmount.toString())),
                            titleValueRow3(
                                title: "Deposit to Place",
                                value: "\$" +
                                    getFormattedAmount(
                                        data.depositToPlace.toString())),
                            titleValueRow3(
                                title: "Remarks for Client",
                                value: data.remarksForClient
                                    .toString()
                                    .replaceAll("[", "")
                                    .replaceAll("]", "")),
                            titleValueRow3(
                                title: "Remarks for Broker",
                                value: data.remarksForBroker == null
                                    ? "-"
                                    : data.remarksForBroker.toString()),
                            titleValueRow3(
                                title: "Interest Reset Date",
                                value: data.interestResetDate! == true
                                    ? "Yes"
                                    : "No"),
                            titleValueRow3(
                                title: "Processing Fee",
                                value:
                                    data.processingFee! == true ? "Yes" : "No"),
                          ],
                        ),
                      )
                    : Container()
              ],
            ),
          )),
    );
  }

  getFormattedAmount(String s) {
    const _locale = 'en';
    String _formatNumber =
        NumberFormat.decimalPattern(_locale).format(double.parse(s)).toString();

    return _formatNumber;
  }

  Widget expandableCardBankSubsidy() {
    return GestureDetector(
      onTap: () {
        ratesViewController.isExpandedBankSubsidy.toggle();
      },
      child: Obx(() => Container(
            decoration: const BoxDecoration(boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.black54,
                  blurRadius: 5.0,
                  offset: Offset(0.0, 0.3))
            ], color: Colors.white),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.all(windowWidth * 0.02),
                  color: AppColors.red,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text("Bank’s Subsidy", style: TextStyles.basicTextStyle2),
                      const Spacer(),
                      const Padding(
                        padding: EdgeInsets.only(bottom: 10.0),
                        child: Icon(Icons.minimize, color: Colors.white),
                      ),
                    ],
                  ),
                ),
                ratesViewController.isExpandedBankSubsidy.value
                    ? Padding(
                        padding: const EdgeInsets.symmetric(vertical: 5.0),
                        child: Column(
                          children: [
                            titleValueRowWithList(
                                title: "Cash Rebate/ Legal Subsidy",
                                values: data.cashRebateLegalSubsidy!),
                            titleValueRowWithList(
                                title: "Valuation Subsidy",
                                values: data.valuationSubsidy!),
                            /*titleValueRow3(
                                title: "Cash Rebate/ Subsidy Clawback",
                                value: data.cashRebateSubsidyClawbackPeriodYears
                                    .toString()),*/
                            titleValueRow3(
                                title: "Cash Rebate/ Subsidy Clawback",
                                value: data.cashRebateSubsidyClawbackPeriodYears
                                        .toString() +
                                    " Years"),
                            titleValueRow3(
                                title: "Fire Insurance Subsidy",
                                value: data.fireInsuranceSubsidy.toString()),
                          ],
                        ),
                      )
                    : Container()
              ],
            ),
          )),
    );
  }

  Widget expandableCardEarlyRepaymentPenalty() {
    return GestureDetector(
      onTap: () {
        ratesViewController.isExpandedBankPenalty.toggle();
      },
      child: Obx(() => Container(
            decoration: const BoxDecoration(boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.black54,
                  blurRadius: 5.0,
                  offset: Offset(0.0, 0.3))
            ], color: Colors.white),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.all(windowWidth * 0.02),
                  color: AppColors.red,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text("Early Repayment Penalty",
                          style: TextStyles.basicTextStyle2),
                      const Spacer(),
                      const Padding(
                        padding: EdgeInsets.only(bottom: 10.0),
                        child: Icon(Icons.minimize, color: Colors.white),
                      ),
                    ],
                  ),
                ),
                ratesViewController.isExpandedBankPenalty.value
                    ? Padding(
                        padding: const EdgeInsets.symmetric(vertical: 5.0),
                        child: Column(
                          children: [
                            titleValueRow3(
                                title: "Partial Repayment Penalty",
                                value:
                                    data.partialRepaymentPenalty!.toString() +
                                        "%"),
                            titleValueRow3(
                                title: "Partial Repayment Penalty Remarks",
                                value: data.partialRepaymentPenaltyRemarks!
                                    .toString()),
                            titleValueRow3(
                                title: "Full Repayment Penalty",
                                value: data.fullRepaymentPenalty!.toString() +
                                    "%"),
                            titleValueRow3(
                                title: "Full Repayment Penalty Remarks",
                                value: data.fullRepaymentPenaltyRemarks!
                                    .toString()),
                            titleValueRow3(
                                title: "Cancellation Fee",
                                value: data.cancellationFee!.toString()),
                            titleValueRow3(
                                title: "Cancellation Fee Remarks",
                                value: data.cancellationFeeRemarks!.toString()),
                          ],
                        ),
                      )
                    : Container()
              ],
            ),
          )),
    );
  }
}
