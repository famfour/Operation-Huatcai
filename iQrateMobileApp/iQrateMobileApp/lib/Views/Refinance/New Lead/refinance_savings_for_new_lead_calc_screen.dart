// ignore_for_file: must_be_immutable

import 'dart:developer';

import 'package:flutter/material.dart';
// import 'package:flutter_html/style.dart';
import 'package:get/get.dart';
import 'package:im_stepper/stepper.dart';
import 'package:iqrate/Controller/refinance_new_lead_controller.dart';

import '../../../DeviceManager/colors.dart';
import '../../../DeviceManager/container_styles.dart';
import '../../../DeviceManager/screen_constants.dart';
import '../../../DeviceManager/text_styles.dart';
import '../../../Model/response_model.dart/more_rates_response_model.dart';
import '../../../Widgets/default_appbar.dart';
import '../../../Widgets/form_field_title.dart';
import '../../../Widgets/primary_button.dart';
import '../../../Widgets/require_text_field.dart';
import '../../../Widgets/secondary_button.dart';
import '../../faqs_view.dart';
import '../../generate_packages.dart';

class RefinanceSavingsForNewLeadCalcScreen extends StatefulWidget {
  const RefinanceSavingsForNewLeadCalcScreen({Key? key}) : super(key: key);

  @override
  State<RefinanceSavingsForNewLeadCalcScreen> createState() =>
      _RefinanceSavingsForNewLeadCalcScreenState();
}

class _RefinanceSavingsForNewLeadCalcScreenState
    extends State<RefinanceSavingsForNewLeadCalcScreen> {
  final RefinanceNewLeadController refinanceNewLeadController =
      Get.put(RefinanceNewLeadController());
  //int activeStep = 0; // Initial step set to 0.

  int dotCount = 5;
  late double windowHeight;
  late double windowWidth;

  @override
  Widget build(BuildContext context) {
    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: DefaultAppBar3(
        title: "Refinance Savings",
        windowHeight: windowHeight * 0.09,
      ),
      body: Obx(() => SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 20.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  NumberStepper(
                    enableNextPreviousButtons: false,
                    enableStepTapping: false,
                    stepRadius: windowWidth * 0.03,
                    lineLength: windowWidth * 0.33,
                    lineDotRadius: 0.1,
                    stepPadding: 0,
                    activeStepColor: AppColors.kPrimaryColor,
                    activeStepBorderPadding: 0,
                    activeStepBorderColor: AppColors.kPrimaryColor,
                    stepColor: const Color(0XFFD0C8C9),
                    numbers: const [1, 2, 3],
                    numberStyle: TextStyle(
                      color: Colors.white,
                      fontSize: FontSize.s18,
                      fontWeight: FontWeight.w400,
                    ),
                    activeStep: refinanceNewLeadController.activeStep.value,

                    // This ensures step-tapping updates the activeStep.
                    onStepReached: (index) {
                      setState(() {
                        refinanceNewLeadController.activeStep.value = index;
                      });
                    },
                  ),
                  refinanceNewLeadController.activeStep.value == 0
                      ? Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            if (refinanceNewLeadController.leadDetails == 1 ||
                                refinanceNewLeadController.leadDetails == 2 ||
                                refinanceNewLeadController.leadDetails == 3 ||
                                refinanceNewLeadController.leadDetails == 4)
                              NewPurchaseApplicantDetails(
                                applicantNumber: "1",
                                nameController: refinanceNewLeadController
                                    .nameControllerMainApplicant,
                                emailController: refinanceNewLeadController
                                    .emailControllerMainApplicant,
                                phoneController: refinanceNewLeadController
                                    .phoneControllerMainApplicant,
                                countryCodeController:
                                    refinanceNewLeadController
                                        .countryCodeControllerMainApplicant,
                                dobController: refinanceNewLeadController
                                    .dobControllerMainApplicant,
                                selectedEmploymentTypeController:
                                    refinanceNewLeadController
                                        .selectedEmploymentTypeMainApplicant!,
                                monthlyFixedIncomeController:
                                    refinanceNewLeadController
                                        .monthlyFixedIncomeControllerMainApplicant,
                                annualIncomeController:
                                    refinanceNewLeadController
                                        .annualIncomeControllerMainApplicant,
                                monthlyRentalIncomeController:
                                    refinanceNewLeadController
                                        .monthlyRentalIncomeControllerMainApplicant,
                              ),
                            if (refinanceNewLeadController.leadDetails == 2 ||
                                refinanceNewLeadController.leadDetails == 3 ||
                                refinanceNewLeadController.leadDetails == 4)
                              NewPurchaseApplicantDetails(
                                applicantNumber: "2",
                                nameController: refinanceNewLeadController
                                    .nameControllerJointApplicant1,
                                emailController: refinanceNewLeadController
                                    .emailControllerJointApplicant1,
                                phoneController: refinanceNewLeadController
                                    .phoneControllerJointApplicant1,
                                countryCodeController:
                                    refinanceNewLeadController
                                        .countryCodeControllerJointApplicant1,
                                dobController: refinanceNewLeadController
                                    .dobControllerJointApplicant1,
                                selectedEmploymentTypeController:
                                    refinanceNewLeadController
                                        .selectedEmploymentTypeJointApplicant1!,
                                monthlyFixedIncomeController:
                                    refinanceNewLeadController
                                        .monthlyFixedIncomeControllerJointApplicant1,
                                annualIncomeController:
                                    refinanceNewLeadController
                                        .annualIncomeControllerJointApplicant1,
                                monthlyRentalIncomeController:
                                    refinanceNewLeadController
                                        .monthlyRentalIncomeControllerJointApplicant1,
                              ),
                            if (refinanceNewLeadController.leadDetails == 3 ||
                                refinanceNewLeadController.leadDetails == 4)
                              NewPurchaseApplicantDetails(
                                applicantNumber: "3",
                                nameController: refinanceNewLeadController
                                    .nameControllerJointApplicant2,
                                emailController: refinanceNewLeadController
                                    .emailControllerJointApplicant2,
                                phoneController: refinanceNewLeadController
                                    .phoneControllerJointApplicant2,
                                countryCodeController:
                                    refinanceNewLeadController
                                        .countryCodeControllerJointApplicant2,
                                dobController: refinanceNewLeadController
                                    .dobControllerJointApplicant2,
                                selectedEmploymentTypeController:
                                    refinanceNewLeadController
                                        .selectedEmploymentTypeJointApplicant2!,
                                monthlyFixedIncomeController:
                                    refinanceNewLeadController
                                        .monthlyFixedIncomeControllerJointApplicant2,
                                annualIncomeController:
                                    refinanceNewLeadController
                                        .annualIncomeControllerJointApplicant2,
                                monthlyRentalIncomeController:
                                    refinanceNewLeadController
                                        .monthlyRentalIncomeControllerJointApplicant2,
                              ),
                            if (refinanceNewLeadController.leadDetails == 4)
                              NewPurchaseApplicantDetails(
                                applicantNumber: "4",
                                nameController: refinanceNewLeadController
                                    .nameControllerJointApplicant3,
                                emailController: refinanceNewLeadController
                                    .emailControllerJointApplicant3,
                                phoneController: refinanceNewLeadController
                                    .phoneControllerJointApplicant3,
                                countryCodeController:
                                    refinanceNewLeadController
                                        .countryCodeControllerJointApplicant3,
                                dobController: refinanceNewLeadController
                                    .dobControllerJointApplicant3,
                                selectedEmploymentTypeController:
                                    refinanceNewLeadController
                                        .selectedEmploymentTypeJointApplicant3!,
                                monthlyFixedIncomeController:
                                    refinanceNewLeadController
                                        .monthlyFixedIncomeControllerJointApplicant3,
                                annualIncomeController:
                                    refinanceNewLeadController
                                        .annualIncomeControllerJointApplicant3,
                                monthlyRentalIncomeController:
                                    refinanceNewLeadController
                                        .monthlyRentalIncomeControllerJointApplicant3,
                              ),
                            Obx(
                              () => refinanceNewLeadController
                                      .isCalculated.value
                                  ? Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 15.0),
                                      child: Column(
                                        children: [
                                          Card(
                                            shape: const RoundedRectangleBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(5))),
                                            elevation: 5,
                                            margin: const EdgeInsets.symmetric(
                                                horizontal: 5, vertical: 10),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 20.0,
                                                          top: 20,
                                                          bottom: 5),
                                                  child: Text(
                                                    "Total Monthly Income",
                                                    style: TextStyles
                                                        .bankSubmissionTaskTextStyle1,
                                                  ),
                                                ),
                                                const Divider(
                                                    color: Colors.deepOrange),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 20.0,
                                                          top: 5,
                                                          bottom: 20),
                                                  child: Text(
                                                    "\$" +
                                                        refinanceNewLeadController
                                                            .totalMonthlyIncomeMainApplicant
                                                            .toStringAsFixed(2)
                                                            .replaceAllMapped(
                                                                RegExp(
                                                                    r'(\d{1,3})(?=(\d{3})+(?!\d))'),
                                                                (Match m) =>
                                                                    '${m[1]},'),
                                                    style: TextStyles
                                                        .bankSubmissionBody,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          SizedBox(height: windowHeight * 0.03),
                                        ],
                                      ),
                                    )
                                  : const SizedBox(),
                            ),
                          ],
                        )
                      : refinanceNewLeadController.activeStep.value == 1
                          ? Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 20.0),
                              child: SizedBox(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    RefinancePropertyDetails(
                                      applicantNumber: "1",
                                      propertyPriceController:
                                          refinanceNewLeadController
                                              .yearOfPropertyPurchaseController,
                                      selectedPropertyStatus:
                                          refinanceNewLeadController
                                              .selectedPropertyStatus!,
                                      dropdownPropertyStatus:
                                          refinanceNewLeadController
                                              .dropDownPropertyStatus,
                                      selectedPropertyType:
                                          refinanceNewLeadController
                                              .selectedPropertyType!,
                                      dropdownPropertyType:
                                          refinanceNewLeadController
                                              .dropdownPropertyType,
                                      dropdownLongerLoanTenure:
                                          refinanceNewLeadController
                                              .dropdownLongerLoanTenure,
                                      selectedLongerLoanTenure:
                                          refinanceNewLeadController
                                              .selectedLongerLoanTenure!,
                                      loanTenureYearController:
                                          refinanceNewLeadController
                                              .outstandingLoanAmountController,
                                      loanAmountController:
                                          refinanceNewLeadController
                                              .approximateValuationController,
                                    ),
                                    Obx(
                                      () => refinanceNewLeadController
                                              .isCalculated.value
                                          ? Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 15.0),
                                              child: Column(
                                                children: [
                                                  Card(
                                                    shape: const RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius.all(
                                                                Radius.circular(
                                                                    5))),
                                                    elevation: 5,
                                                    margin: const EdgeInsets
                                                            .symmetric(
                                                        horizontal: 5,
                                                        vertical: 10),
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  left: 20.0,
                                                                  top: 20,
                                                                  bottom: 5),
                                                          child: Text(
                                                            "Early Repayment Penalty Amount",
                                                            style: TextStyles
                                                                .bankSubmissionTaskTextStyle1,
                                                          ),
                                                        ),
                                                        const Divider(
                                                            color: Colors
                                                                .deepOrange),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  left: 20.0,
                                                                  top: 5,
                                                                  bottom: 20),
                                                          child: Text(
                                                            "\$ " +
                                                                refinanceNewLeadController
                                                                    .resultMainApplicantStep2
                                                                    .value
                                                                    .earlyRepaymentPenaltyAmount
                                                                    .toStringAsFixed(
                                                                        2)
                                                                    .replaceAllMapped(
                                                                        RegExp(
                                                                            r'(\d{1,3})(?=(\d{3})+(?!\d))'),
                                                                        (Match m) =>
                                                                            '${m[1]},'),
                                                            style: TextStyles
                                                                .bankSubmissionBody,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  SizedBox(
                                                      height:
                                                          windowHeight * 0.03),
                                                  Card(
                                                    shape: const RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius.all(
                                                                Radius.circular(
                                                                    5))),
                                                    elevation: 5,
                                                    margin: const EdgeInsets
                                                            .symmetric(
                                                        horizontal: 5,
                                                        vertical: 10),
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  left: 20.0,
                                                                  top: 20,
                                                                  bottom: 5),
                                                          child: Text(
                                                            "Cancellation Penalty Amount",
                                                            style: TextStyles
                                                                .bankSubmissionTaskTextStyle1,
                                                          ),
                                                        ),
                                                        const Divider(
                                                            color: Colors
                                                                .deepOrange),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  left: 20.0,
                                                                  top: 5,
                                                                  bottom: 20),
                                                          child: Text(
                                                            "\$ " +
                                                                refinanceNewLeadController
                                                                    .resultMainApplicantStep2
                                                                    .value
                                                                    .cancellationPenaltyAmount
                                                                    .toStringAsFixed(
                                                                        2)
                                                                    .replaceAllMapped(
                                                                        RegExp(
                                                                            r'(\d{1,3})(?=(\d{3})+(?!\d))'),
                                                                        (Match m) =>
                                                                            '${m[1]},'),
                                                            style: TextStyles
                                                                .bankSubmissionBody,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            )
                                          : const SizedBox(),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          : SizedBox(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    height: windowHeight * 0.01,
                                  ),
                                  Container(
                                    padding: EdgeInsets.zero,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: Colors.black12,
                                    ),
                                    child: Obx(
                                      () => refinanceNewLeadController
                                              .isCalculated.value
                                          ? Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 20,
                                                          top: 10,
                                                          bottom: 10),
                                                  child: Text(
                                                    "My Refinancing Report ",
                                                    style: TextStyles
                                                        .leadsColorTextStyle2,
                                                  ),
                                                ),
                                                expandableSummary(
                                                    "Loan Details"),
                                                Card(
                                                  elevation: 3,
                                                  child: ListTile(
                                                    onTap: () {
                                                      refinanceNewLeadController
                                                          .onTapYourSavingsFromRefinancing();
                                                    },
                                                    leading: SizedBox(
                                                      width: 10,
                                                      child: Obx(
                                                        () => Checkbox(
                                                          value: refinanceNewLeadController
                                                              .savingsFromRefinancingCheckBox
                                                              .value,
                                                          onChanged:
                                                              (bool? value) {
                                                            refinanceNewLeadController
                                                                .savingsFromRefinancingCheckBox
                                                                .value = value!;
                                                            if (refinanceNewLeadController
                                                                    .savingsFromRefinancingCheckBox
                                                                    .value ==
                                                                true) {
                                                              refinanceNewLeadController
                                                                  .exportFieldsCheckBoxesList
                                                                  .add(
                                                                      'Savings_from_refinancing');
                                                            } else {
                                                              refinanceNewLeadController
                                                                  .exportFieldsCheckBoxesList
                                                                  .remove(
                                                                      'Savings_from_refinancing');
                                                            }
                                                            debugPrint(
                                                                refinanceNewLeadController
                                                                    .exportFieldsCheckBoxesList
                                                                    .toString());
                                                          },
                                                          checkColor:
                                                              AppColors.red,
                                                          activeColor: Colors
                                                              .red
                                                              .withOpacity(0),
                                                          shape:
                                                              RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        2.0),
                                                          ),
                                                          side:
                                                              MaterialStateBorderSide
                                                                  .resolveWith(
                                                            (states) =>
                                                                const BorderSide(
                                                                    width: 1.0,
                                                                    color: Colors
                                                                        .red),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    title: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text(
                                                          'Your Savings From Refinancing',
                                                          style: TextStyles
                                                              .leadsTextStyle,
                                                        ),
                                                        Text(
                                                          '(Over first 3 years)',
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .black87,
                                                              fontSize:
                                                                  FontSize.s14,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                              height: 1.5),
                                                        ),
                                                      ],
                                                    ),
                                                    trailing: const Icon(
                                                      Icons.arrow_forward_ios,
                                                      color: Colors.black,
                                                      size: 15,
                                                    ),
                                                  ),
                                                ),
                                                if (refinanceNewLeadController.resultMainApplicantStep3.value.packageDetails!.length == 2 ||
                                                    refinanceNewLeadController
                                                            .resultMainApplicantStep3
                                                            .value
                                                            .packageDetails!
                                                            .length ==
                                                        3 ||
                                                    refinanceNewLeadController
                                                            .resultMainApplicantStep3
                                                            .value
                                                            .packageDetails!
                                                            .length ==
                                                        4 ||
                                                    refinanceNewLeadController
                                                            .resultMainApplicantStep3
                                                            .value
                                                            .packageDetails!
                                                            .length ==
                                                        5)
                                                  expandableSummaryPackage1(
                                                      'Selected Loan Package 1'),
                                                if (refinanceNewLeadController.resultMainApplicantStep3.value.packageDetails!.length == 3 ||
                                                    refinanceNewLeadController
                                                            .resultMainApplicantStep3
                                                            .value
                                                            .packageDetails!
                                                            .length ==
                                                        4 ||
                                                    refinanceNewLeadController
                                                            .resultMainApplicantStep3
                                                            .value
                                                            .packageDetails!
                                                            .length ==
                                                        5)
                                                  expandableSummaryPackage2(
                                                      'Selected Loan Package 2'),
                                                if (refinanceNewLeadController
                                                            .resultMainApplicantStep3
                                                            .value
                                                            .packageDetails!
                                                            .length ==
                                                        4 ||
                                                    refinanceNewLeadController
                                                            .resultMainApplicantStep3
                                                            .value
                                                            .packageDetails!
                                                            .length ==
                                                        5)
                                                  expandableSummaryPackage3(
                                                      'Selected Loan Package 3'),
                                                if (refinanceNewLeadController
                                                        .resultMainApplicantStep3
                                                        .value
                                                        .packageDetails!
                                                        .length ==
                                                    5)
                                                  expandableSummaryPackage4(
                                                      'Selected Loan Package 4')
                                              ],
                                            )
                                          : const SizedBox(),
                                    ),
                                  ),
                                  SizedBox(height: windowHeight * 0.03),
                                  Center(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 20.0),
                                      child: PrimaryButton(
                                        windowHeight: windowHeight,
                                        windowWidth: windowWidth,
                                        buttonTitle: "Download Report",
                                        onPressed: () {
                                          refinanceNewLeadController
                                              .onTapDownloadReports();
                                        },
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: windowHeight * 0.03,
                                  ),
                                ],
                              ),
                            ),
                  // Obx(() => !refinanceNewLeadController.isCalculated.value
                  //     ? Padding(
                  //         padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  //         child: Column(
                  //           children: [
                  //             PrimaryButton(
                  //               windowHeight: windowHeight,
                  //               windowWidth: windowWidth,
                  //               buttonTitle: "Calculate",
                  //               onPressed: () {
                  //                 refinanceNewLeadController
                  //                     .onTapCalculateStep1();
                  //               },
                  //             ),
                  //             SizedBox(height: windowHeight * 0.02),
                  //           ],
                  //         ),
                  //       )
                  //     : const SizedBox()),
                  Obx(
                    () {
                      return !refinanceNewLeadController.isCalculated.value &&
                              (refinanceNewLeadController.activeStep.value ==
                                      0 ||
                                  refinanceNewLeadController.activeStep.value ==
                                      1)
                          ? Container()
                          : Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 20.0),
                              child: Column(
                                children: [
                                  refinanceNewLeadController.activeStep.value ==
                                              0 ||
                                          refinanceNewLeadController
                                                  .activeStep.value ==
                                              1
                                      ? PrimaryButton(
                                          windowHeight: windowHeight,
                                          windowWidth: windowWidth,
                                          buttonTitle: "Continue",
                                          onPressed: () async {
                                            if (refinanceNewLeadController
                                                        .activeStep.value !=
                                                    0 &&
                                                refinanceNewLeadController
                                                        .activeStep.value !=
                                                    2) {
                                              await refinanceNewLeadController
                                                  .onTapContinuePackagesCalculator();
                                              //.generatePackages();
                                            }
                                            setState(() {
                                              refinanceNewLeadController
                                                  .isCalculated.value = false;

                                              if (refinanceNewLeadController
                                                      .activeStep.value ==
                                                  0) {
                                                refinanceNewLeadController
                                                        .activeStep.value =
                                                    refinanceNewLeadController
                                                            .activeStep.value +
                                                        1;
                                              } else if (refinanceNewLeadController
                                                      .activeStep.value ==
                                                  1) {
                                                if (refinanceNewLeadController
                                                        .packagesGenerated ==
                                                    true) {
                                                  refinanceNewLeadController
                                                      .activeStep.value = 2;
                                                }
                                              }
                                            });
                                          })
                                      : Container(),
                                  SizedBox(height: windowHeight * 0.02),
                                ],
                              ),
                            );
                    },
                  ),

                  refinanceNewLeadController.activeStep.value != 2
                      ? Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20.0),
                          child: Column(
                            children: [
                              PrimaryButton(
                                  windowHeight: windowHeight,
                                  windowWidth: windowWidth,
                                  buttonTitle: "Calculate",
                                  onPressed: () {
                                    if (refinanceNewLeadController
                                            .activeStep.value ==
                                        0) {
                                      refinanceNewLeadController
                                          .onTapCalculateStep1Check();
                                    } else if (refinanceNewLeadController
                                            .activeStep.value ==
                                        1) {
                                      refinanceNewLeadController
                                          .onTapCalculateStep2Check();
                                    } else {}
                                  }),
                              SizedBox(height: windowHeight * 0.02),
                            ],
                          ),
                        )
                      : Container(),

                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: SecondaryButton(
                      buttonTitle:
                          refinanceNewLeadController.activeStep.value == 0
                              ? 'Reset'
                              : 'Back',
                      windowHeight: windowHeight,
                      windowWidth: windowWidth,
                      onPressed: () {
                        setState(() {
                          if (refinanceNewLeadController.activeStep.value ==
                              0) {
                            refinanceNewLeadController.isCalculated.value =
                                false;

                            refinanceNewLeadController
                                .monthlyFixedIncomeControllerMainApplicant
                                .value = const TextEditingValue(text: '0');
                            refinanceNewLeadController
                                .monthlyFixedIncomeControllerJointApplicant1
                                .value = const TextEditingValue(text: '0');
                            refinanceNewLeadController
                                .monthlyFixedIncomeControllerJointApplicant2
                                .value = const TextEditingValue(text: '0');
                            refinanceNewLeadController
                                .monthlyFixedIncomeControllerJointApplicant3
                                .value = const TextEditingValue(text: '0');

                            // ! Clear Anual income
                            refinanceNewLeadController
                                .annualIncomeControllerMainApplicant
                                .value = const TextEditingValue(text: '0');
                            refinanceNewLeadController
                                .annualIncomeControllerJointApplicant1
                                .value = const TextEditingValue(text: '0');
                            refinanceNewLeadController
                                .annualIncomeControllerJointApplicant2
                                .value = const TextEditingValue(text: '0');
                            refinanceNewLeadController
                                .annualIncomeControllerJointApplicant3
                                .value = const TextEditingValue(text: '0');

                            // ! Clear monthly rental income
                            refinanceNewLeadController
                                .monthlyRentalIncomeControllerMainApplicant
                                .value = const TextEditingValue(text: '0');
                            refinanceNewLeadController
                                .monthlyRentalIncomeControllerJointApplicant1
                                .value = const TextEditingValue(text: '0');
                            refinanceNewLeadController
                                .monthlyRentalIncomeControllerJointApplicant2
                                .value = const TextEditingValue(text: '0');
                            refinanceNewLeadController
                                .monthlyRentalIncomeControllerJointApplicant3
                                .value = const TextEditingValue(text: '0');
                            // ! Need to clear the text field Ends here

                          }

                          if (refinanceNewLeadController.activeStep.value ==
                              1) {
                            refinanceNewLeadController.activeStep.value = 0;
                            refinanceNewLeadController.clearStep1ListData();
                          } else if (refinanceNewLeadController
                                  .activeStep.value ==
                              2) {
                            refinanceNewLeadController.activeStep.value = 1;
                            refinanceNewLeadController.isCalculated.value =
                                false;

                            refinanceNewLeadController.clearPackageSelection();

                            refinanceNewLeadController.bankPackageModelList
                                .clear();

                            refinanceNewLeadController
                                .clearRefinanceSavingsCheckBox();
                          } else {
                            //refinanceNewLeadController.activeStep.value = 0;
                          }
                        });
                      },
                      kGradientBoxDecoration:
                          ContainerStyles.kGradientBoxDecorationSecondaryButton,
                      kInnerDecoration:
                          ContainerStyles.kInnerDecorationSecondaryButton,
                    ),
                  ),
                ],
              ),
            ),
          )),
    );
  }

  Widget expandableSummary(String title) {
    return Card(
      elevation: 3,
      child: ExpansionTile(
        leading: SizedBox(
            width: 10,
            child: Obx(() => Checkbox(
                  value: refinanceNewLeadController.summaryCheckBox.value,
                  onChanged: (bool? value) {
                    refinanceNewLeadController.summaryCheckBox.value = value!;
                    if (refinanceNewLeadController.summaryCheckBox.value ==
                        true) {
                      refinanceNewLeadController.exportFieldsCheckBoxesList
                          .add('Loan_details');
                    } else {
                      refinanceNewLeadController.exportFieldsCheckBoxesList
                          .remove('Loan_details');
                    }
                  },
                  checkColor: AppColors.red,
                  activeColor: Colors.red.withOpacity(0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(2.0),
                  ),
                  side: MaterialStateBorderSide.resolveWith(
                    (states) => const BorderSide(width: 1.0, color: Colors.red),
                  ),
                ))),
        childrenPadding: EdgeInsets.zero,
        title: Text(title, style: TextStyles.leadsTextStyle),
        collapsedIconColor: Colors.black,
        children: [
          const Divider(height: 4, thickness: 4, color: Colors.black12),
          SizedBox(
            height: windowWidth * 0.03,
          ),
          summaryItem(
            "Prepared For",
            refinanceNewLeadController
                .resultMainApplicantStep3.value.loanDetails!.preparedFor
                .toString(),
          ),
          summaryItem(
            "Outstanding Loan Amount",
            "\$ " +
                refinanceNewLeadController.resultMainApplicantStep3.value
                    .loanDetails!.outstandingLoanAmount
                    .toString(),
          ),
          summaryItem(
            "Maximum Loan Tenure",
            refinanceNewLeadController.resultMainApplicantStep3.value
                    .loanDetails!.maximumLoanTenure
                    .toString() +
                " Years",
          ),
          summaryItem(
            "Preferred Loan Tenure",
            refinanceNewLeadController.resultMainApplicantStep3.value
                    .loanDetails!.preferedLoanTenure
                    .toString() +
                " Years",
          ),
          summaryItem(
            "Existing Bank",
            refinanceNewLeadController
                .resultMainApplicantStep3.value.loanDetails!.existingBank
                .toString(),
          ),
          summaryItem(
            "Existing Interest Rate",
            refinanceNewLeadController.resultMainApplicantStep3.value
                    .loanDetails!.existingInterestRate
                    .toString() +
                "%",
          ),
          summaryItem(
            "Legal Fees Payable",
            "\$ " +
                refinanceNewLeadController.resultMainApplicantStep3.value
                    .yourSavingFromRefinancing!.legalFees
                    .toString(),
          ),
          summaryItem(
            "Valuation Fees Payable",
            "\$ " +
                refinanceNewLeadController.resultMainApplicantStep3.value
                    .yourSavingFromRefinancing!.valuationFees
                    .toString(),
          ),
          SizedBox(
            height: windowWidth * 0.03,
          ),
        ],
      ),
    );
  }

  Widget summaryItem(title, value) {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: windowWidth * 0.04, vertical: windowHeight * 0.003),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            //mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: 5,
                child: Text(
                  title,
                  style: TextStyles.formFieldTitleTextStyle1,
                  textAlign: TextAlign.left,
                ),
              ),
              Expanded(
                flex: 2,
                child: Text(
                  value.replaceAllMapped(RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))'),
                      (Match m) => '${m[1]},'),
                  style: TextStyles.leadsTextStyle1,
                  textAlign: TextAlign.left,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget expandableSummaryPackage1(String title) {
    var rates = <Rates>[];
    rates.add(
      Rates(
        year: refinanceNewLeadController.resultMainApplicantStep3.value
            .packageDetails![1].selectedPackageDetails!.rates![0].year,
        referenceRate: int.tryParse(
          refinanceNewLeadController
              .resultMainApplicantStep3
              .value
              .packageDetails![1]
              .selectedPackageDetails!
              .rates![0]
              .referenceRate
              .toString(),
        ),
        bankSpread: double.tryParse(
          refinanceNewLeadController.resultMainApplicantStep3.value
              .packageDetails![1].selectedPackageDetails!.rates![0].bankSpread
              .toString(),
        ),
      ),
    );
    rates.add(
      Rates(
        year: refinanceNewLeadController.resultMainApplicantStep3.value
            .packageDetails![1].selectedPackageDetails!.rates![1].year,
        referenceRate: int.tryParse(
          refinanceNewLeadController
              .resultMainApplicantStep3
              .value
              .packageDetails![1]
              .selectedPackageDetails!
              .rates![1]
              .referenceRate
              .toString(),
        ),
        bankSpread: double.tryParse(
          refinanceNewLeadController.resultMainApplicantStep3.value
              .packageDetails![1].selectedPackageDetails!.rates![1].bankSpread
              .toString(),
        ),
      ),
    );
    rates.add(
      Rates(
        year: refinanceNewLeadController.resultMainApplicantStep3.value
            .packageDetails![1].selectedPackageDetails!.rates![2].year,
        referenceRate: int.tryParse(
          refinanceNewLeadController
              .resultMainApplicantStep3
              .value
              .packageDetails![1]
              .selectedPackageDetails!
              .rates![2]
              .referenceRate
              .toString(),
        ),
        bankSpread: double.tryParse(
          refinanceNewLeadController.resultMainApplicantStep3.value
              .packageDetails![1].selectedPackageDetails!.rates![2].bankSpread
              .toString(),
        ),
      ),
    );
    rates.add(
      Rates(
        year: refinanceNewLeadController.resultMainApplicantStep3.value
            .packageDetails![1].selectedPackageDetails!.rates![3].year,
        referenceRate: int.tryParse(
          refinanceNewLeadController
              .resultMainApplicantStep3
              .value
              .packageDetails![1]
              .selectedPackageDetails!
              .rates![3]
              .referenceRate
              .toString(),
        ),
        bankSpread: double.tryParse(
          refinanceNewLeadController.resultMainApplicantStep3.value
              .packageDetails![1].selectedPackageDetails!.rates![3].bankSpread
              .toString(),
        ),
      ),
    );
    rates.add(
      Rates(
        year: refinanceNewLeadController.resultMainApplicantStep3.value
            .packageDetails![1].selectedPackageDetails!.rates![4].year,
        referenceRate: int.tryParse(
          refinanceNewLeadController
              .resultMainApplicantStep3
              .value
              .packageDetails![1]
              .selectedPackageDetails!
              .rates![4]
              .referenceRate
              .toString(),
        ),
        bankSpread: double.tryParse(
          refinanceNewLeadController.resultMainApplicantStep3.value
              .packageDetails![1].selectedPackageDetails!.rates![4].bankSpread
              .toString(),
        ),
      ),
    );
    rates.add(
      Rates(
        year: refinanceNewLeadController.resultMainApplicantStep3.value
            .packageDetails![1].selectedPackageDetails!.rates![5].year,
        referenceRate: int.tryParse(
          refinanceNewLeadController
              .resultMainApplicantStep3
              .value
              .packageDetails![1]
              .selectedPackageDetails!
              .rates![5]
              .referenceRate
              .toString(),
        ),
        bankSpread: double.tryParse(
          refinanceNewLeadController.resultMainApplicantStep3.value
              .packageDetails![1].selectedPackageDetails!.rates![5].bankSpread
              .toString(),
        ),
      ),
    );

    log("packageDetails![1]::: " +
        refinanceNewLeadController.resultMainApplicantStep3.value
            .packageDetails![1].selectedPackageDetails!.bank!.logo
            .toString());

    return Card(
      elevation: 3,
      child: ExpansionTile(
        leading: SizedBox(
            width: 10,
            child: Obx(() => Checkbox(
                  value: refinanceNewLeadController.package1CheckBox.value,
                  onChanged: (bool? value) {
                    refinanceNewLeadController.package1CheckBox.value = value!;
                    if (refinanceNewLeadController.package1CheckBox.value ==
                        true) {
                      refinanceNewLeadController.exportFieldsCheckBoxesList
                          .add('Package_1');
                    } else {
                      refinanceNewLeadController.exportFieldsCheckBoxesList
                          .remove('Package_1');
                    }
                    debugPrint(
                      refinanceNewLeadController.exportFieldsCheckBoxesList
                          .toString(),
                    );
                  },
                  checkColor: AppColors.red,
                  activeColor: Colors.red.withOpacity(0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(2.0),
                  ),
                  side: MaterialStateBorderSide.resolveWith(
                    (states) => const BorderSide(width: 1.0, color: Colors.red),
                  ),
                ))),
        childrenPadding: EdgeInsets.zero,
        title: Text(title, style: TextStyles.leadsTextStyle),
        collapsedIconColor: Colors.black,
        children: [
          const Divider(height: 4, thickness: 4, color: Colors.black12),
          LoanPackageCardForRefinance(
              data: refinanceNewLeadController.resultMainApplicantStep3.value
                  .packageDetails![1].selectedPackageDetails!,
              image: refinanceNewLeadController.resultMainApplicantStep3.value
                  .packageDetails![1].selectedPackageDetails!.bank!.logo
                  .toString(),
              headingSubText: refinanceNewLeadController
                      .resultMainApplicantStep3
                      .value
                      .packageDetails![1]
                      .selectedPackageDetails!
                      .rateType
                      .toString()
                      .replaceAll('_', '')
                      .replaceAll('RateType', '')
                      .toLowerCase()
                      .capitalize! +
                  '(${refinanceNewLeadController.resultMainApplicantStep3.value.packageDetails![1].selectedPackageDetails!.rateCategory.toString().replaceAll('_', ' ').replaceAll('RateType.', '').toLowerCase().capitalize})',
              headingText: refinanceNewLeadController.resultMainApplicantStep3
                  .value.packageDetails![1].selectedPackageDetails!.bank!.name
                  .toString(),
              onChanged: (val) {},
              checkBoxValue: false,
              cardIndex: 0,
              isCheckboxShow: false),
          SizedBox(
            height: windowWidth * 0.05,
          ),
          GestureDetector(
            onTap: () {
              refinanceNewLeadController
                  .onTapMltMortgageRepaymentTableMltPackage1();
            },
            child: Container(
              height: windowHeight * 0.06,
              width: windowWidth * 0.9,
              padding: EdgeInsets.symmetric(horizontal: windowWidth * 0.03),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                border: Border.all(
                  color: AppColors.kPrimaryColor,
                  width: 1,
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Mortgage Repayment",
                    style: TextStyles.calculatorResultTextStyle,
                  ),
                  const Text("(Based on Maximum Loan Tenure)"),
                ],
              ),
            ),
          ),
          SizedBox(
            height: windowWidth * 0.03,
          ),
          GestureDetector(
            onTap: () {
              refinanceNewLeadController
                  .onTapMltMortgageRepaymentTablePltPackage1();
            },
            child: Container(
              height: windowHeight * 0.06,
              width: windowWidth * 0.9,
              padding: EdgeInsets.symmetric(horizontal: windowWidth * 0.03),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                border: Border.all(
                  color: AppColors.kPrimaryColor,
                  width: 1,
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Mortgage Repayment",
                    style: TextStyles.calculatorResultTextStyle,
                  ),
                  const Text("(Based on Preferred Loan Tenure)"),
                ],
              ),
            ),
          ),
          SizedBox(
            height: windowWidth * 0.05,
          ),
        ],
      ),
    );
  }

  Widget expandableSummaryPackage2(String title) {
    var rates = <Rates>[];
    rates.add(
      Rates(
        year: refinanceNewLeadController.resultMainApplicantStep3.value
            .packageDetails![2].selectedPackageDetails!.rates![0].year,
        referenceRate: int.tryParse(
          refinanceNewLeadController
              .resultMainApplicantStep3
              .value
              .packageDetails![2]
              .selectedPackageDetails!
              .rates![0]
              .referenceRate
              .toString(),
        ),
        bankSpread: double.tryParse(
          refinanceNewLeadController.resultMainApplicantStep3.value
              .packageDetails![2].selectedPackageDetails!.rates![0].bankSpread
              .toString(),
        ),
      ),
    );
    rates.add(
      Rates(
        year: refinanceNewLeadController.resultMainApplicantStep3.value
            .packageDetails![2].selectedPackageDetails!.rates![1].year,
        referenceRate: int.tryParse(
          refinanceNewLeadController
              .resultMainApplicantStep3
              .value
              .packageDetails![2]
              .selectedPackageDetails!
              .rates![1]
              .referenceRate
              .toString(),
        ),
        bankSpread: double.tryParse(
          refinanceNewLeadController.resultMainApplicantStep3.value
              .packageDetails![2].selectedPackageDetails!.rates![1].bankSpread
              .toString(),
        ),
      ),
    );
    rates.add(
      Rates(
        year: refinanceNewLeadController.resultMainApplicantStep3.value
            .packageDetails![2].selectedPackageDetails!.rates![2].year,
        referenceRate: int.tryParse(
          refinanceNewLeadController
              .resultMainApplicantStep3
              .value
              .packageDetails![2]
              .selectedPackageDetails!
              .rates![2]
              .referenceRate
              .toString(),
        ),
        bankSpread: double.tryParse(
          refinanceNewLeadController.resultMainApplicantStep3.value
              .packageDetails![2].selectedPackageDetails!.rates![2].bankSpread
              .toString(),
        ),
      ),
    );
    rates.add(
      Rates(
        year: refinanceNewLeadController.resultMainApplicantStep3.value
            .packageDetails![2].selectedPackageDetails!.rates![3].year,
        referenceRate: int.tryParse(
          refinanceNewLeadController
              .resultMainApplicantStep3
              .value
              .packageDetails![2]
              .selectedPackageDetails!
              .rates![3]
              .referenceRate
              .toString(),
        ),
        bankSpread: double.tryParse(
          refinanceNewLeadController.resultMainApplicantStep3.value
              .packageDetails![2].selectedPackageDetails!.rates![3].bankSpread
              .toString(),
        ),
      ),
    );
    rates.add(
      Rates(
        year: refinanceNewLeadController.resultMainApplicantStep3.value
            .packageDetails![2].selectedPackageDetails!.rates![4].year,
        referenceRate: int.tryParse(
          refinanceNewLeadController
              .resultMainApplicantStep3
              .value
              .packageDetails![2]
              .selectedPackageDetails!
              .rates![4]
              .referenceRate
              .toString(),
        ),
        bankSpread: double.tryParse(
          refinanceNewLeadController.resultMainApplicantStep3.value
              .packageDetails![2].selectedPackageDetails!.rates![4].bankSpread
              .toString(),
        ),
      ),
    );
    rates.add(
      Rates(
        year: refinanceNewLeadController.resultMainApplicantStep3.value
            .packageDetails![2].selectedPackageDetails!.rates![5].year,
        referenceRate: int.tryParse(
          refinanceNewLeadController
              .resultMainApplicantStep3
              .value
              .packageDetails![2]
              .selectedPackageDetails!
              .rates![5]
              .referenceRate
              .toString(),
        ),
        bankSpread: double.tryParse(
          refinanceNewLeadController.resultMainApplicantStep3.value
              .packageDetails![2].selectedPackageDetails!.rates![5].bankSpread
              .toString(),
        ),
      ),
    );
    return Card(
      elevation: 3,
      child: ExpansionTile(
        leading: SizedBox(
            width: 10,
            child: Obx(() => Checkbox(
                  value: refinanceNewLeadController.package2CheckBox.value,
                  onChanged: (bool? value) {
                    refinanceNewLeadController.package2CheckBox.value = value!;
                    if (refinanceNewLeadController.package2CheckBox.value ==
                        true) {
                      refinanceNewLeadController.exportFieldsCheckBoxesList
                          .add('Package_2');
                    } else {
                      refinanceNewLeadController.exportFieldsCheckBoxesList
                          .remove('Package_2');
                    }
                    debugPrint(
                      refinanceNewLeadController.exportFieldsCheckBoxesList
                          .toString(),
                    );
                  },
                  checkColor: AppColors.red,
                  activeColor: Colors.red.withOpacity(0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(2.0),
                  ),
                  side: MaterialStateBorderSide.resolveWith(
                    (states) => const BorderSide(width: 1.0, color: Colors.red),
                  ),
                ))),
        childrenPadding: EdgeInsets.zero,
        title: Text(title, style: TextStyles.leadsTextStyle),
        collapsedIconColor: Colors.black,
        children: [
          const Divider(height: 4, thickness: 4, color: Colors.black12),
          LoanPackageCardForRefinance(
              data: refinanceNewLeadController.resultMainApplicantStep3.value
                  .packageDetails![2].selectedPackageDetails!,
              image: refinanceNewLeadController.resultMainApplicantStep3.value
                  .packageDetails![2].selectedPackageDetails!.bank!.logo
                  .toString(),
              headingSubText: refinanceNewLeadController
                      .resultMainApplicantStep3
                      .value
                      .packageDetails![2]
                      .selectedPackageDetails!
                      .rateType
                      .toString()
                      .replaceAll('_', '')
                      .replaceAll('RateType', '')
                      .toLowerCase()
                      .capitalize! +
                  '(${refinanceNewLeadController.resultMainApplicantStep3.value.packageDetails![2].selectedPackageDetails!.rateCategory.toString().replaceAll('_', ' ').replaceAll('RateType.', '').toLowerCase().capitalize})',
              headingText: refinanceNewLeadController.resultMainApplicantStep3
                  .value.packageDetails![2].selectedPackageDetails!.bank!.name
                  .toString(),
              onChanged: (val) {},
              checkBoxValue: false,
              cardIndex: 0,
              isCheckboxShow: false),
          SizedBox(
            height: windowWidth * 0.05,
          ),
          GestureDetector(
            onTap: () {
              refinanceNewLeadController
                  .onTapMltMortgageRepaymentTableMltPackage2();
            },
            child: Container(
              height: windowHeight * 0.06,
              width: windowWidth * 0.9,
              padding: EdgeInsets.symmetric(horizontal: windowWidth * 0.03),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                border: Border.all(
                  color: AppColors.kPrimaryColor,
                  width: 1,
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Mortgage Repayment",
                    style: TextStyles.calculatorResultTextStyle,
                  ),
                  const Text("(Based on Maximum Loan Tenure)"),
                ],
              ),
            ),
          ),
          SizedBox(
            height: windowWidth * 0.03,
          ),
          GestureDetector(
            onTap: () {
              refinanceNewLeadController
                  .onTapMltMortgageRepaymentTablePltPackage2();
            },
            child: Container(
              height: windowHeight * 0.06,
              width: windowWidth * 0.9,
              padding: EdgeInsets.symmetric(horizontal: windowWidth * 0.03),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                border: Border.all(
                  color: AppColors.kPrimaryColor,
                  width: 1,
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Mortgage Repayment",
                    style: TextStyles.calculatorResultTextStyle,
                  ),
                  const Text("(Based on Preferred Loan Tenure)"),
                ],
              ),
            ),
          ),
          SizedBox(
            height: windowWidth * 0.05,
          ),
        ],
      ),
    );
  }

  Widget expandableSummaryPackage3(String title) {
    var rates = <Rates>[];
    rates.add(Rates(year: "1", referenceRate: 10, bankSpread: 1.0));
    rates.add(Rates(year: "2", referenceRate: 10, bankSpread: 1.0));
    rates.add(Rates(year: "3", referenceRate: 10, bankSpread: 1.0));
    rates.add(Rates(year: "4", referenceRate: 10, bankSpread: 1.0));
    rates.add(Rates(year: "4", referenceRate: 10, bankSpread: 1.0));
    rates.add(Rates(year: "7", referenceRate: 10, bankSpread: 1.0));
    rates.add(Rates(year: "8", referenceRate: 10, bankSpread: 1.0));
    return Card(
      elevation: 3,
      child: ExpansionTile(
        leading: SizedBox(
            width: 10,
            child: Obx(() => Checkbox(
                  value: refinanceNewLeadController.package3CheckBox.value,
                  onChanged: (bool? value) {
                    refinanceNewLeadController.package3CheckBox.value = value!;
                    if (refinanceNewLeadController.package3CheckBox.value ==
                        true) {
                      refinanceNewLeadController.exportFieldsCheckBoxesList
                          .add('Package_3');
                    } else {
                      refinanceNewLeadController.exportFieldsCheckBoxesList
                          .remove('Package_3');
                    }
                    debugPrint(
                      refinanceNewLeadController.exportFieldsCheckBoxesList
                          .toString(),
                    );
                  },
                  checkColor: AppColors.red,
                  activeColor: Colors.red.withOpacity(0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(2.0),
                  ),
                  side: MaterialStateBorderSide.resolveWith(
                    (states) => const BorderSide(width: 1.0, color: Colors.red),
                  ),
                ))),
        childrenPadding: EdgeInsets.zero,
        title: Text(title, style: TextStyles.leadsTextStyle),
        collapsedIconColor: Colors.black,
        children: [
          const Divider(height: 4, thickness: 4, color: Colors.black12),
          LoanPackageCardForRefinance(
              data: refinanceNewLeadController.resultMainApplicantStep3.value
                  .packageDetails![3].selectedPackageDetails!,
              image: refinanceNewLeadController.resultMainApplicantStep3.value
                  .packageDetails![3].selectedPackageDetails!.bank!.logo
                  .toString(),
              headingSubText: '',
              headingText: refinanceNewLeadController.resultMainApplicantStep3
                  .value.packageDetails![3].selectedPackageDetails!.bank!.name
                  .toString(),
              onChanged: (val) {},
              checkBoxValue: false,
              cardIndex: 0,
              isCheckboxShow: false),
          SizedBox(
            height: windowWidth * 0.05,
          ),
          GestureDetector(
            onTap: () {
              refinanceNewLeadController
                  .onTapMltMortgageRepaymentTableMltPackage3();
            },
            child: Container(
              height: windowHeight * 0.06,
              width: windowWidth * 0.9,
              padding: EdgeInsets.symmetric(horizontal: windowWidth * 0.03),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                border: Border.all(
                  color: AppColors.kPrimaryColor,
                  width: 1,
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Mortgage Repayment",
                    style: TextStyles.calculatorResultTextStyle,
                  ),
                  const Text("(Based on Maximum Loan Tenure)"),
                ],
              ),
            ),
          ),
          SizedBox(
            height: windowWidth * 0.03,
          ),
          GestureDetector(
            onTap: () {
              refinanceNewLeadController
                  .onTapMltMortgageRepaymentTablePltPackage3();
            },
            child: Container(
              height: windowHeight * 0.06,
              width: windowWidth * 0.9,
              padding: EdgeInsets.symmetric(horizontal: windowWidth * 0.03),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                border: Border.all(
                  color: AppColors.kPrimaryColor,
                  width: 1,
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Mortgage Repayment",
                    style: TextStyles.calculatorResultTextStyle,
                  ),
                  const Text("(Based on Preferred Loan Tenure)"),
                ],
              ),
            ),
          ),
          SizedBox(
            height: windowWidth * 0.05,
          ),
        ],
      ),
    );
  }

  Widget expandableSummaryPackage4(String title) {
    var rates = <Rates>[];
    rates.add(Rates(year: "1", referenceRate: 10, bankSpread: 1.0));
    rates.add(Rates(year: "2", referenceRate: 10, bankSpread: 1.0));
    rates.add(Rates(year: "3", referenceRate: 10, bankSpread: 1.0));
    rates.add(Rates(year: "4", referenceRate: 10, bankSpread: 1.0));
    rates.add(Rates(year: "4", referenceRate: 10, bankSpread: 1.0));
    rates.add(Rates(year: "7", referenceRate: 10, bankSpread: 1.0));
    rates.add(Rates(year: "8", referenceRate: 10, bankSpread: 1.0));
    return Card(
      elevation: 3,
      child: ExpansionTile(
        leading: SizedBox(
            width: 10,
            child: Obx(() => Checkbox(
                  value: refinanceNewLeadController.package4CheckBox.value,
                  onChanged: (bool? value) {
                    refinanceNewLeadController.package4CheckBox.value = value!;
                    if (refinanceNewLeadController.package4CheckBox.value ==
                        true) {
                      refinanceNewLeadController.exportFieldsCheckBoxesList
                          .add('Package_4');
                    } else {
                      refinanceNewLeadController.exportFieldsCheckBoxesList
                          .remove('Package_4');
                    }
                    debugPrint(
                      refinanceNewLeadController.exportFieldsCheckBoxesList
                          .toString(),
                    );
                  },
                  checkColor: AppColors.red,
                  activeColor: Colors.red.withOpacity(0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(2.0),
                  ),
                  side: MaterialStateBorderSide.resolveWith(
                    (states) => const BorderSide(width: 1.0, color: Colors.red),
                  ),
                ))),
        childrenPadding: EdgeInsets.zero,
        title: Text(title, style: TextStyles.leadsTextStyle),
        collapsedIconColor: Colors.black,
        children: [
          const Divider(height: 4, thickness: 4, color: Colors.black12),
          LoanPackageCardForRefinance(
              data: refinanceNewLeadController.resultMainApplicantStep3.value
                  .packageDetails![4].selectedPackageDetails!,
              image: refinanceNewLeadController.resultMainApplicantStep3.value
                  .packageDetails![4].selectedPackageDetails!.bank!.logo
                  .toString(),
              headingSubText: '',
              headingText: refinanceNewLeadController.resultMainApplicantStep3
                  .value.packageDetails![4].selectedPackageDetails!.bank!.name
                  .toString(),
              onChanged: (val) {},
              checkBoxValue: false,
              cardIndex: 0,
              isCheckboxShow: false),
          SizedBox(
            height: windowWidth * 0.05,
          ),
          GestureDetector(
            onTap: () {
              refinanceNewLeadController
                  .onTapMltMortgageRepaymentTableMltPackage4();
            },
            child: Container(
              height: windowHeight * 0.06,
              width: windowWidth * 0.9,
              padding: EdgeInsets.symmetric(horizontal: windowWidth * 0.03),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                border: Border.all(
                  color: AppColors.kPrimaryColor,
                  width: 1,
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Mortgage Repayment",
                    style: TextStyles.calculatorResultTextStyle,
                  ),
                  const Text("(Based on Maximum Loan Tenure)"),
                ],
              ),
            ),
          ),
          SizedBox(
            height: windowWidth * 0.03,
          ),
          GestureDetector(
            onTap: () {
              refinanceNewLeadController
                  .onTapMltMortgageRepaymentTablePltPackage4();
            },
            child: Container(
              height: windowHeight * 0.06,
              width: windowWidth * 0.9,
              padding: EdgeInsets.symmetric(horizontal: windowWidth * 0.03),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                border: Border.all(
                  color: AppColors.kPrimaryColor,
                  width: 1,
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Mortgage Repayment",
                    style: TextStyles.calculatorResultTextStyle,
                  ),
                  const Text("(Based on Preferred Loan Tenure)"),
                ],
              ),
            ),
          ),
          SizedBox(
            height: windowWidth * 0.05,
          ),
        ],
      ),
    );
  }
}

class NewPurchaseApplicantDetails extends StatefulWidget {
  final TextEditingController nameController;
  final TextEditingController emailController;
  final TextEditingController phoneController;
  final TextEditingController countryCodeController;
  final TextEditingController dobController;
  String selectedEmploymentTypeController;
  final TextEditingController monthlyFixedIncomeController;
  final TextEditingController annualIncomeController;
  final TextEditingController monthlyRentalIncomeController;
  final String applicantNumber;

  NewPurchaseApplicantDetails({
    Key? key,
    required this.nameController,
    required this.emailController,
    required this.phoneController,
    required this.countryCodeController,
    required this.dobController,
    required this.selectedEmploymentTypeController,
    required this.monthlyFixedIncomeController,
    required this.annualIncomeController,
    required this.monthlyRentalIncomeController,
    required this.applicantNumber,
  }) : super(key: key);

  @override
  State<NewPurchaseApplicantDetails> createState() =>
      _NewPurchaseApplicantDetailsState();
}

class _NewPurchaseApplicantDetailsState
    extends State<NewPurchaseApplicantDetails> {
  final RefinanceNewLeadController refinanceNewLeadController = Get.find();

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    return ExpansionSection(
        initiallyExpanded: widget.applicantNumber == "1" ? true : false,
        title: "Loan Applicant #${widget.applicantNumber} Details",
        body: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: windowHeight * 0.01,
              ),
              const FormFieldTitle(title: 'Name*'),
              SizedBox(height: windowHeight * 0.01),
              RequireTextField(
                type: Type.fullname,
                labelText: "Full name",
                controller: widget.nameController,
                key: const Key("fullname"),
              ),
              SizedBox(height: windowHeight * 0.02),
              const FormFieldTitle(title: 'Mobile Number*'),
              SizedBox(height: windowHeight * 0.01),
              RequireTextField(
                type: Type.dropdownCountryCodeWithPhone,
                countryCodeController: widget.countryCodeController,
                controller: widget.phoneController,
                labelText: "Mobile Number",
                key: const Key("phone"),
              ),
              const FormFieldTitle(title: 'Email Address*'),
              SizedBox(height: windowHeight * 0.01),
              RequireTextField(
                type: Type.email,
                controller: widget.emailController,
                labelText: "Email address",
                key: const Key("email"),
              ),
              SizedBox(height: windowHeight * 0.02),
              const FormFieldTitle(title: 'Date of Birth*'),
              SizedBox(height: windowHeight * 0.01),
              RequireTextField(
                type: Type.dob,
                controller: widget.dobController,
                labelText: "DD/MM/YY",
                key: const Key("dob"),
                dateFormat: "dd-MM-yyyy",
              ),
              SizedBox(height: windowHeight * 0.02),
              Text(
                "Income",
                style: TextStyles.leadsColorTextStyle1,
              ),
              SizedBox(height: windowHeight * 0.02),
              const FormFieldTitle(title: 'Employment Type*'),
              SizedBox(height: windowHeight * 0.01),
              Container(
                width: windowWidth,
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
                decoration: BoxDecoration(
                    border: Border.all(
                        color: AppColors.formFieldBorderColor, width: 1.0),
                    borderRadius: const BorderRadius.all(Radius.circular(
                            10) //                 <--- border radius here
                        )),
                child: DropdownButtonHideUnderline(
                  child: DropdownButton<String>(
                    hint: const Text('Select'),
                    // Not necessary for Option 1
                    value: widget.selectedEmploymentTypeController,
                    onChanged: (newValue) {
                      widget.selectedEmploymentTypeController = newValue!;

                      if (widget.applicantNumber == "1") {
                        refinanceNewLeadController
                            .selectedEmploymentTypeMainApplicant = newValue;
                      } else if (widget.applicantNumber == "2") {
                        refinanceNewLeadController
                            .selectedEmploymentTypeJointApplicant1 = newValue;
                      } else if (widget.applicantNumber == "3") {
                        refinanceNewLeadController
                            .selectedEmploymentTypeJointApplicant2 = newValue;
                      } else if (widget.applicantNumber == "4") {
                        refinanceNewLeadController
                            .selectedEmploymentTypeJointApplicant3 = newValue;
                      }

                      setState(() {});
                    },
                    items: refinanceNewLeadController.dropdownEmploymentType
                        .map((location) {
                      return DropdownMenuItem(
                        child: SizedBox(
                            width: windowWidth * 0.7, child: Text(location)),
                        value: location,
                      );
                    }).toList(),
                  ),
                ),
              ),
              SizedBox(height: windowHeight * 0.02),
              const FormFieldTitle(
                  title:
                      'Monthly Fixed Income* (Basic Salary, Fixed Allowance)'),
              SizedBox(height: windowHeight * 0.01),
              RequireTextField(
                type: Type.money,
                controller: widget.monthlyFixedIncomeController,
                labelText: "\$",
                key: const Key("fixed Income"),
              ),
              SizedBox(height: windowHeight * 0.01),
              const FormFieldTitle(
                  title: 'Annual Income* (IRAS Notice of Assessment)'),
              SizedBox(height: windowHeight * 0.01),
              RequireTextField(
                type: Type.money,
                controller: widget.annualIncomeController,
                labelText: "\$",
                key: const Key("Income"),
              ),
              SizedBox(height: windowHeight * 0.01),
              const FormFieldTitle(title: 'Monthly Rental Income*'),
              SizedBox(height: windowHeight * 0.01),
              RequireTextField(
                type: Type.money,
                controller: widget.monthlyRentalIncomeController,
                labelText: "\$",
                key: const Key("Rental Income"),
              ),
              SizedBox(height: windowHeight * 0.02),
            ],
          ),
        ]);
  }
}

class RefinancePropertyDetails extends StatefulWidget {
  final TextEditingController propertyPriceController;
  final TextEditingController loanAmountController;

  String selectedLongerLoanTenure;
  final List<String> dropdownLongerLoanTenure;

  String selectedPropertyType;
  final List<String> dropdownPropertyType;

  String selectedPropertyStatus;
  final List<String> dropdownPropertyStatus;

  final TextEditingController loanTenureYearController;
  final String applicantNumber;

  RefinancePropertyDetails({
    Key? key,
    required this.propertyPriceController,
    required this.loanAmountController,
    required this.loanTenureYearController,
    required this.applicantNumber,
    required this.dropdownLongerLoanTenure,
    required this.dropdownPropertyType,
    required this.selectedLongerLoanTenure,
    required this.selectedPropertyType,
    required this.selectedPropertyStatus,
    required this.dropdownPropertyStatus,
  }) : super(key: key);

  @override
  State<RefinancePropertyDetails> createState() =>
      _RefinancePropertyDetailsState();
}

class _RefinancePropertyDetailsState extends State<RefinancePropertyDetails> {
  final RefinanceNewLeadController refinanceNewLeadController = Get.find();

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: windowHeight * 0.02),
        Text('Property & Loan Details', style: TextStyles.leadsColorTextStyle1),
        SizedBox(height: windowHeight * 0.02),
        const FormFieldTitle(title: 'Year of Property Purchase*'),
        SizedBox(height: windowHeight * 0.01),
        RequireTextField(
          type: Type.number,
          labelText: "Year of Property Purchase",
          controller:
              refinanceNewLeadController.yearOfPropertyPurchaseController,
          key: const Key("Property"),
        ),
        SizedBox(height: windowHeight * 0.01),
        const FormFieldTitle(title: 'Property Type*'),
        SizedBox(height: windowHeight * 0.01),
        Container(
          width: windowWidth,
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
          decoration: BoxDecoration(
              border:
                  Border.all(color: AppColors.formFieldBorderColor, width: 1.0),
              borderRadius: const BorderRadius.all(
                  Radius.circular(10) //                 <--- border radius here
                  )),
          child: DropdownButtonHideUnderline(
            child: DropdownButton<String>(
              hint: const Text('Select'),
              // Not necessary for Option 1
              value: widget.selectedPropertyType,
              onChanged: (newValue) {
                widget.selectedPropertyType = newValue!;

                refinanceNewLeadController.selectedPropertyType = newValue;

                refinanceNewLeadController.propertyTypeValueToPassInAPI =
                    refinanceNewLeadController.dropDownPropertyTypeKey[
                        refinanceNewLeadController.dropdownPropertyType
                            .indexOf(newValue)];

                setState(() {});
              },
              items: widget.dropdownPropertyType.map((location) {
                return DropdownMenuItem(
                  child:
                      SizedBox(width: windowWidth * 0.7, child: Text(location)),
                  value: location,
                );
              }).toList(),
            ),
          ),
        ),
        // SizedBox(height: windowHeight * 0.02),
        // const FormFieldTitle(title: 'Property Status*'),
        // SizedBox(height: windowHeight * 0.01),
        // Container(
        //   width: windowWidth,
        //   padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
        //   decoration: BoxDecoration(
        //       border:
        //           Border.all(color: AppColors.formFieldBorderColor, width: 1.0),
        //       borderRadius: const BorderRadius.all(
        //           Radius.circular(10) //                 <--- border radius here
        //           )),
        //   child: DropdownButtonHideUnderline(
        //     child: DropdownButton<String>(
        //       hint: const Text('Select'),
        //       // Not necessary for Option 1
        //       value: widget.selectedPropertyStatus,
        //       onChanged: (newValue) {
        //         widget.selectedPropertyStatus = newValue!;

        //         refinanceNewLeadController.selectedPropertyStatus = newValue;

        //         refinanceNewLeadController.propertyStatusValueToPassInAPI =
        //             refinanceNewLeadController.dropDownPropertyStatusKey[
        //                 refinanceNewLeadController.dropDownPropertyStatus
        //                     .indexOf(newValue)];

        //         setState(() {});
        //       },
        //       items: widget.dropdownPropertyStatus.map((location) {
        //         return DropdownMenuItem(
        //           child:
        //               SizedBox(width: windowWidth * 0.7, child: Text(location)),
        //           value: location,
        //         );
        //       }).toList(),
        //     ),
        //   ),
        // ),
        SizedBox(height: windowHeight * 0.02),
        const FormFieldTitle(title: 'Approximate Valuation of the Property*'),
        SizedBox(height: windowHeight * 0.01),
        RequireTextField(
          type: Type.money,
          controller: refinanceNewLeadController.approximateValuationController,
          labelText: "\$",
          key: const Key("Loan Amount*"),
        ),
        SizedBox(height: windowHeight * 0.01),
        const FormFieldTitle(title: 'Outstanding Loan Amount*'),
        SizedBox(height: windowHeight * 0.01),
        RequireTextField(
          type: Type.money,
          controller:
              refinanceNewLeadController.outstandingLoanAmountController,
          labelText: "\$",
          key: const Key("Company Name"),
        ),
        SizedBox(height: windowHeight * 0.01),
        const FormFieldTitle(title: 'Outstanding Loan Tenure*'),
        SizedBox(height: windowHeight * 0.01),
        RequireTextField(
          controller:
              refinanceNewLeadController.outstandingLoanTenureController,
          key: const Key("Outstanding Loan Tenure"),
          type: Type.number,
        ),
        SizedBox(height: windowHeight * 0.01),
        const FormFieldTitle(title: 'Existing Bank*'),
        SizedBox(height: windowHeight * 0.01),
        Container(
          width: windowWidth,
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
          decoration: BoxDecoration(
              border:
                  Border.all(color: AppColors.formFieldBorderColor, width: 1.0),
              borderRadius: const BorderRadius.all(
                  Radius.circular(10) //                 <--- border radius here
                  )),
          child: DropdownButtonHideUnderline(
            child: DropdownButton(
              hint: const Text('Select Bank'),
              value: refinanceNewLeadController.selectedExistingBank,
              onChanged: (newValue) {
                debugPrint(newValue.toString());
                setState(() {
                  refinanceNewLeadController.selectedExistingBank =
                      newValue.toString();
                });
              },
              items: refinanceNewLeadController.existingBanks.map((location) {
                return DropdownMenuItem(
                  child: Text(location.name!),
                  value: location.name!,
                );
              }).toList(),
            ),
          ),
        ),
        SizedBox(height: windowHeight * 0.02),
        const FormFieldTitle(title: 'Existing Interest Rate*'),
        SizedBox(height: windowHeight * 0.01),
        RequireTextField(
          type: Type.number,
          labelText: "Existing Interest Rate",
          controller: refinanceNewLeadController.existingInterestRateController,
          key: const Key("Existing"),
        ),
        // SizedBox(height: windowHeight * 0.02),
        // CheckboxListTile(
        //   controlAffinity: ListTileControlAffinity.leading,
        //   title: Text(
        //     'Do you have an intention to sell the property within 3 years?',
        //     style: TextStyle(
        //         fontWeight: FontWeight.w300,
        //         fontSize: FontSize.s14,
        //         color: Colors.black),
        //   ),
        //   autofocus: false,
        //   activeColor: const Color(0xffDF5356),
        //   checkColor: Colors.white,
        //   selected: refinanceNewLeadController.is3Years.value,
        //   value: refinanceNewLeadController.is3Years.value,
        //   onChanged: (bool? value) {
        //     setState(() {
        //       refinanceNewLeadController.is3Years.value = value!;
        //     });
        //   },
        // ),
        // CheckboxListTile(
        //   controlAffinity: ListTileControlAffinity.leading,
        //   title: Text(
        //       'Are you willing to place a minimum \$200k fresh funds with the bank to enjoy better rates?',
        //       style: TextStyle(
        //           fontWeight: FontWeight.w300,
        //           fontSize: FontSize.s14,
        //           color: Colors.black)),
        //   autofocus: false,
        //   activeColor: const Color(0xffDF5356),
        //   //selectedTileColor: const Color(0xffDF5356),
        //   checkColor: Colors.white,
        //   selected: refinanceNewLeadController.is200K.value,
        //   value: refinanceNewLeadController.is200K.value,
        //   onChanged: (bool? value) {
        //     setState(() {
        //       refinanceNewLeadController.is200K.value = value!;
        //     });
        //   },
        // ),
        SizedBox(height: windowHeight * 0.02),
        const FormFieldTitle(title: 'Preferred Loan Tenure (If Any)'),
        SizedBox(height: windowHeight * 0.01),
        RequireTextField(
          type: Type.number,
          labelText: "Preferred Loan Tenure (If Any)",
          controller: refinanceNewLeadController.preferredLoanTenureController,
          key: const Key("Preferred Loan"),
        ),
        SizedBox(height: windowHeight * 0.02),
        const FormFieldTitle(
            title: 'Date that You Took up Your Home Loan with Current Bank*'),
        SizedBox(height: windowHeight * 0.01),
        RequireTextField(
          type: Type.date,
          labelText: "DD/MM/YYYY",
          controller:
              refinanceNewLeadController.dateLoanWithCurrentBankController,
          key: const Key("Date that You Took"),
        ),
        SizedBox(height: windowHeight * 0.02),
        const FormFieldTitle(title: 'Are you still within Lock-in-Period*'),
        SizedBox(height: windowHeight * 0.01),
        Container(
          width: windowWidth,
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
          decoration: BoxDecoration(
              border:
                  Border.all(color: AppColors.formFieldBorderColor, width: 1.0),
              borderRadius: const BorderRadius.all(
                  Radius.circular(10) //                 <--- border radius here
                  )),
          child: DropdownButtonHideUnderline(
            child: DropdownButton<String>(
              hint: const Text('Select'),
              // Not necessary for Option 1
              value: refinanceNewLeadController.selectedLockInPeriod,
              onChanged: (newValue) {
                refinanceNewLeadController.selectedLockInPeriod = newValue!;
                setState(() {});
              },
              items: refinanceNewLeadController.lockInPeriodDropDown
                  .map((location) {
                return DropdownMenuItem(
                  child:
                      SizedBox(width: windowWidth * 0.7, child: Text(location)),
                  value: location,
                );
              }).toList(),
            ),
          ),
        ),
        if (refinanceNewLeadController.selectedLockInPeriod == 'Yes')
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: windowHeight * 0.02),
              const FormFieldTitle(title: 'Date of Expiry*'),
              SizedBox(height: windowHeight * 0.01),
              RequireTextField(
                type: Type.date,
                labelText: "DD/MM/YYYY",
                controller: refinanceNewLeadController.dateofExpiryController,
                key: const Key("Date of Expiry"),
              ),
              SizedBox(height: windowHeight * 0.02),
              const FormFieldTitle(title: 'Early Repayment Penalty %'),
              SizedBox(height: windowHeight * 0.01),
              Container(
                width: windowWidth,
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
                decoration: BoxDecoration(
                    border: Border.all(
                        color: AppColors.formFieldBorderColor, width: 1.0),
                    borderRadius: const BorderRadius.all(Radius.circular(
                            10) //                 <--- border radius here
                        )),
                child: DropdownButtonHideUnderline(
                  child: DropdownButton<String>(
                    hint: const Text('Select'),
                    // Not necessary for Option 1
                    value: refinanceNewLeadController.selectedRepaymentPenalty,
                    onChanged: (newValue) {
                      refinanceNewLeadController.selectedRepaymentPenalty =
                          newValue!;
                      setState(() {});
                    },
                    items: refinanceNewLeadController
                        .earlyRepaymentPenaltyDropDown
                        .map((location) {
                      return DropdownMenuItem(
                        child: SizedBox(
                            width: windowWidth * 0.7, child: Text(location)),
                        value: location,
                      );
                    }).toList(),
                  ),
                ),
              ),
            ],
          ),
        SizedBox(height: windowHeight * 0.02),
        const FormFieldTitle(
            title:
                'Did you receive any Cash Rebate/ Subsidy for Existing Housing Loan?*'),
        SizedBox(height: windowHeight * 0.01),
        Container(
          width: windowWidth,
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
          decoration: BoxDecoration(
              border:
                  Border.all(color: AppColors.formFieldBorderColor, width: 1.0),
              borderRadius: const BorderRadius.all(
                  Radius.circular(10) //                 <--- border radius here
                  )),
          child: DropdownButtonHideUnderline(
            child: DropdownButton<String>(
              hint: const Text('Select'),
              // Not necessary for Option 1
              value: refinanceNewLeadController.selectedCashRebateOrSubsidy,
              onChanged: (newValue) {
                refinanceNewLeadController.selectedCashRebateOrSubsidy =
                    newValue!;
                setState(() {});
              },
              items: refinanceNewLeadController.cashRebateOrSubsidyDropDown
                  .map((location) {
                return DropdownMenuItem(
                  child:
                      SizedBox(width: windowWidth * 0.7, child: Text(location)),
                  value: location,
                );
              }).toList(),
            ),
          ),
        ),
        if (refinanceNewLeadController.selectedCashRebateOrSubsidy == 'Yes')
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: windowHeight * 0.02),
              const FormFieldTitle(
                  title: 'What is the total cash Rebate/subsidy Received?*'),
              SizedBox(height: windowHeight * 0.01),
              RequireTextField(
                type: Type.money,
                controller:
                    refinanceNewLeadController.totalCashRebateController,
                labelText: "\$",
                key: const Key("subsidy Received"),
              ),
            ],
          ),
        SizedBox(height: windowHeight * 0.01),
        const FormFieldTitle(title: 'Is your Loan Fully Disbursed?*'),
        SizedBox(height: windowHeight * 0.01),
        Container(
          width: windowWidth,
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
          decoration: BoxDecoration(
              border:
                  Border.all(color: AppColors.formFieldBorderColor, width: 1.0),
              borderRadius: const BorderRadius.all(
                  Radius.circular(10) //                 <--- border radius here
                  )),
          child: DropdownButtonHideUnderline(
            child: DropdownButton<String>(
              hint: const Text('Select'),
              // Not necessary for Option 1
              value: refinanceNewLeadController.selectedFullyDisbursed,
              onChanged: (newValue) {
                refinanceNewLeadController.selectedFullyDisbursed = newValue!;
                setState(() {});
              },
              items: refinanceNewLeadController.fullyDisbursedDropDown
                  .map((location) {
                return DropdownMenuItem(
                  child:
                      SizedBox(width: windowWidth * 0.7, child: Text(location)),
                  value: location,
                );
              }).toList(),
            ),
          ),
        ),
        if (refinanceNewLeadController.selectedFullyDisbursed != 'No')
          SizedBox(
            height: windowHeight * 0.03,
          ),
        if (refinanceNewLeadController.selectedFullyDisbursed == 'No')
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: windowHeight * 0.02),
              const FormFieldTitle(
                  title: 'What is the Undisbursed Loan Amount?*'),
              SizedBox(height: windowHeight * 0.01),
              RequireTextField(
                type: Type.money,
                controller:
                    refinanceNewLeadController.undisbursedLoanAmountController,
                labelText: "\$",
                key: const Key("Undisbursed Loan Amount"),
              ),
              // SizedBox(height: windowHeight * 0.03),
            ],
          ),
        if (refinanceNewLeadController.selectedFullyDisbursed == 'No')
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: windowHeight * 0.01),
              const FormFieldTitle(title: 'Cancellation Penalty %'),
              SizedBox(height: windowHeight * 0.01),
              Container(
                width: windowWidth,
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
                decoration: BoxDecoration(
                    border: Border.all(
                        color: AppColors.formFieldBorderColor, width: 1.0),
                    borderRadius: const BorderRadius.all(Radius.circular(
                            10) //                 <--- border radius here
                        )),
                child: DropdownButtonHideUnderline(
                  child: DropdownButton<String>(
                    hint: const Text('Select'),
                    // Not necessary for Option 1
                    value:
                        refinanceNewLeadController.selectedCancellationPenalty,
                    onChanged: (newValue) {
                      refinanceNewLeadController.selectedCancellationPenalty =
                          newValue!;
                      setState(() {});
                    },
                    items: refinanceNewLeadController
                        .cancellationPenaltyDropDown
                        .map((location) {
                      return DropdownMenuItem(
                        child: SizedBox(
                            width: windowWidth * 0.7, child: Text(location)),
                        value: location,
                      );
                    }).toList(),
                  ),
                ),
              ),
              SizedBox(height: windowHeight * 0.03),
            ],
          ),
      ],
    );
  }
}
