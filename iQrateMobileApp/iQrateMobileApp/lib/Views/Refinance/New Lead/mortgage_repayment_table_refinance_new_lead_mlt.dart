import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/refinance_new_lead_controller.dart';
import 'package:iqrate/Widgets/default_appbar.dart';

import '../../../DeviceManager/colors.dart';
import '../../../DeviceManager/screen_constants.dart';

class MortagageRepaymentTableRefinanceNewLeadMlt extends StatelessWidget {
  MortagageRepaymentTableRefinanceNewLeadMlt({Key? key}) : super(key: key);

  final RefinanceNewLeadController refinanceNewLeadController = Get.find();

  @override
  Widget build(BuildContext context) {
    debugPrint(refinanceNewLeadController.yearOneToFiveList.length.toString());
    double windowHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: DefaultAppBar3(
        title: "Refinance Savings",
        windowHeight: windowHeight * 0.09,
      ),
      body: InteractiveViewer(
        minScale: 1,
        maxScale: 4,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: windowHeight * 0.1,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15.0),
              child: Text(
                'Mortgage Repayment Table',
                style: TextStyle(
                  color: AppColors.kPrimaryColor,
                  fontSize: FontSize.s22,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(
              height: windowHeight * 0.01,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15.0),
              child: Text(
                'Based On Maximum Loan Tenure',
                style: TextStyle(
                  color: const Color(0XFF9A9696),
                  fontSize: FontSize.s20,
                ),
              ),
            ),
            SizedBox(
              height: windowHeight * 0.01,
            ),
            Table(
              // defaultColumnWidth: FixedColumnWidth(windowWidth * 0.001),
              border: TableBorder.all(
                  color: const Color(0XFFE2E2E2),
                  style: BorderStyle.solid,
                  width: 2),
              children: [
                TableRow(
                  children: [
                    TableTitleRowData(
                      windowHeight: windowHeight,
                      title: 'Year',
                    ),
                    TableTitleRowData(
                      windowHeight: windowHeight,
                      title: 'Month',
                    ),
                    TableTitleRowData(
                      windowHeight: windowHeight,
                      title: 'Interest Rate',
                    ),
                    TableTitleRowData(
                      windowHeight: windowHeight,
                      title: 'Installment Amount',
                    ),
                    TableTitleRowData(
                      windowHeight: windowHeight,
                      title: 'Interest Repayment',
                    ),
                    TableTitleRowData(
                      windowHeight: windowHeight,
                      title: 'Principal Repayment',
                    ),
                    TableTitleRowData(
                      windowHeight: windowHeight,
                      title: 'Outstanding Amount',
                    ),
                    TableTitleRowData(
                      windowHeight: windowHeight,
                      title: 'Cum Interest Paid',
                    ),
                  ],
                ),
              ],
            ),
            Expanded(
              child: ListView(
                children: [
                  SingleChildScrollView(
                    child: Column(
                      children: [
                        if (refinanceNewLeadController
                            .yearOneToFiveList.isNotEmpty)
                          Card(
                            elevation: 2,
                            child: ExpansionTile(
                              title: Text(
                                'Year 1-5',
                                style: TextStyle(
                                  fontSize: FontSize.s19,
                                  color: Colors.black,
                                ),
                              ),
                              children: [
                                ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: refinanceNewLeadController
                                      .yearOneToFiveList.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Table(
                                      border: TableBorder.all(
                                        color: const Color(0XFFE2E2E2),
                                        style: BorderStyle.solid,
                                        width: 2,
                                      ),
                                      children: [
                                        TableRow(
                                          decoration: BoxDecoration(
                                            color: refinanceNewLeadController
                                                        .resultMainApplicantStep3
                                                        .value
                                                        .packageDetails![1]
                                                        .maximumLoanTenure!
                                                        .mortgagePaymentList![
                                                            index]
                                                        .month
                                                        .toString() ==
                                                    '12'
                                                ? Colors.grey.shade400
                                                : Colors.white,
                                          ),
                                          children: [
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: index % 12 == 0
                                                  ? refinanceNewLeadController
                                                      .yearOneToFiveList[index]!
                                                      .year
                                                      .toString()
                                                  : "",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  refinanceNewLeadController
                                                      .yearOneToFiveList[index]!
                                                      .month
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  refinanceNewLeadController
                                                          .yearOneToFiveList[
                                                              index]!
                                                          .interestRate
                                                          .toString() +
                                                      "%",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .yearOneToFiveList[index]!
                                                      .installmentAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .yearOneToFiveList[index]!
                                                      .interestRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .yearOneToFiveList[index]!
                                                      .principalRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .yearOneToFiveList[index]!
                                                      .outstandingAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .yearOneToFiveList[index]!
                                                      .cumInterestPaid
                                                      .toString(),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        if (refinanceNewLeadController
                            .yearSixToTenList.isNotEmpty)
                          Card(
                            elevation: 2,
                            child: ExpansionTile(
                              title: Text(
                                'Year 6-10',
                                style: TextStyle(
                                  fontSize: FontSize.s19,
                                  color: Colors.black,
                                ),
                              ),
                              children: [
                                ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: refinanceNewLeadController
                                      .yearSixToTenList.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Table(
                                      border: TableBorder.all(
                                        color: const Color(0XFFE2E2E2),
                                        style: BorderStyle.solid,
                                        width: 2,
                                      ),
                                      children: [
                                        TableRow(
                                          decoration: BoxDecoration(
                                            color: refinanceNewLeadController
                                                        .resultMainApplicantStep3
                                                        .value
                                                        .packageDetails![1]
                                                        .maximumLoanTenure!
                                                        .mortgagePaymentList![
                                                            index]
                                                        .month
                                                        .toString() ==
                                                    '12'
                                                ? Colors.grey.shade400
                                                : Colors.white,
                                          ),
                                          children: [
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: index % 12 == 0
                                                  ? refinanceNewLeadController
                                                      .yearSixToTenList[index]!
                                                      .year
                                                      .toString()
                                                  : "",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  refinanceNewLeadController
                                                      .yearSixToTenList[index]!
                                                      .month
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  refinanceNewLeadController
                                                          .yearSixToTenList[
                                                              index]!
                                                          .interestRate
                                                          .toString() +
                                                      "%",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .yearSixToTenList[index]!
                                                      .installmentAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .yearSixToTenList[index]!
                                                      .interestRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .yearSixToTenList[index]!
                                                      .principalRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .yearSixToTenList[index]!
                                                      .outstandingAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .yearSixToTenList[index]!
                                                      .cumInterestPaid
                                                      .toString(),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        if (refinanceNewLeadController
                            .yearElevenToFifteenList.isNotEmpty)
                          Card(
                            elevation: 2,
                            child: ExpansionTile(
                              title: Text(
                                'Year 11-15',
                                style: TextStyle(
                                  fontSize: FontSize.s19,
                                  color: Colors.black,
                                ),
                              ),
                              children: [
                                ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: refinanceNewLeadController
                                      .yearElevenToFifteenList.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Table(
                                      border: TableBorder.all(
                                        color: const Color(0XFFE2E2E2),
                                        style: BorderStyle.solid,
                                        width: 2,
                                      ),
                                      children: [
                                        TableRow(
                                          decoration: BoxDecoration(
                                            color: refinanceNewLeadController
                                                        .resultMainApplicantStep3
                                                        .value
                                                        .packageDetails![1]
                                                        .maximumLoanTenure!
                                                        .mortgagePaymentList![
                                                            index]
                                                        .month
                                                        .toString() ==
                                                    '12'
                                                ? Colors.grey.shade400
                                                : Colors.white,
                                          ),
                                          children: [
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: index % 12 == 0
                                                  ? refinanceNewLeadController
                                                      .yearElevenToFifteenList[
                                                          index]!
                                                      .year
                                                      .toString()
                                                  : "",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  refinanceNewLeadController
                                                      .yearElevenToFifteenList[
                                                          index]!
                                                      .month
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  refinanceNewLeadController
                                                          .yearElevenToFifteenList[
                                                              index]!
                                                          .interestRate
                                                          .toString() +
                                                      "%",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .yearElevenToFifteenList[
                                                          index]!
                                                      .installmentAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .yearElevenToFifteenList[
                                                          index]!
                                                      .interestRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .yearElevenToFifteenList[
                                                          index]!
                                                      .principalRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .yearElevenToFifteenList[
                                                          index]!
                                                      .outstandingAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .yearElevenToFifteenList[
                                                          index]!
                                                      .cumInterestPaid
                                                      .toString(),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        if (refinanceNewLeadController
                            .sixteenToTwentyList.isNotEmpty)
                          Card(
                            elevation: 2,
                            child: ExpansionTile(
                              title: Text(
                                'Year 16-20',
                                style: TextStyle(
                                  fontSize: FontSize.s19,
                                  color: Colors.black,
                                ),
                              ),
                              children: [
                                ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: refinanceNewLeadController
                                      .sixteenToTwentyList.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Table(
                                      border: TableBorder.all(
                                        color: const Color(0XFFE2E2E2),
                                        style: BorderStyle.solid,
                                        width: 2,
                                      ),
                                      children: [
                                        TableRow(
                                          decoration: BoxDecoration(
                                            color: refinanceNewLeadController
                                                        .resultMainApplicantStep3
                                                        .value
                                                        .packageDetails![1]
                                                        .maximumLoanTenure!
                                                        .mortgagePaymentList![
                                                            index]
                                                        .month
                                                        .toString() ==
                                                    '12'
                                                ? Colors.grey.shade400
                                                : Colors.white,
                                          ),
                                          children: [
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: index % 12 == 0
                                                  ? refinanceNewLeadController
                                                      .sixteenToTwentyList[
                                                          index]!
                                                      .year
                                                      .toString()
                                                  : "",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  refinanceNewLeadController
                                                      .sixteenToTwentyList[
                                                          index]!
                                                      .month
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  refinanceNewLeadController
                                                          .sixteenToTwentyList[
                                                              index]!
                                                          .interestRate
                                                          .toString() +
                                                      "%",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .sixteenToTwentyList[
                                                          index]!
                                                      .installmentAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .sixteenToTwentyList[
                                                          index]!
                                                      .interestRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .sixteenToTwentyList[
                                                          index]!
                                                      .principalRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .sixteenToTwentyList[
                                                          index]!
                                                      .outstandingAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .sixteenToTwentyList[
                                                          index]!
                                                      .cumInterestPaid
                                                      .toString(),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        if (refinanceNewLeadController
                            .twentyOneToTwnetyFiveList.isNotEmpty)
                          Card(
                            elevation: 2,
                            child: ExpansionTile(
                              title: Text(
                                'Year 21-25',
                                style: TextStyle(
                                  fontSize: FontSize.s19,
                                  color: Colors.black,
                                ),
                              ),
                              children: [
                                ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: refinanceNewLeadController
                                      .twentyOneToTwnetyFiveList.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Table(
                                      border: TableBorder.all(
                                        color: const Color(0XFFE2E2E2),
                                        style: BorderStyle.solid,
                                        width: 2,
                                      ),
                                      children: [
                                        TableRow(
                                          decoration: BoxDecoration(
                                            color: refinanceNewLeadController
                                                        .resultMainApplicantStep3
                                                        .value
                                                        .packageDetails![1]
                                                        .maximumLoanTenure!
                                                        .mortgagePaymentList![
                                                            index]
                                                        .month
                                                        .toString() ==
                                                    '12'
                                                ? Colors.grey.shade400
                                                : Colors.white,
                                          ),
                                          children: [
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: index % 12 == 0
                                                  ? refinanceNewLeadController
                                                      .twentyOneToTwnetyFiveList[
                                                          index]!
                                                      .year
                                                      .toString()
                                                  : "",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  refinanceNewLeadController
                                                      .twentyOneToTwnetyFiveList[
                                                          index]!
                                                      .month
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: refinanceNewLeadController
                                                      .twentyOneToTwnetyFiveList[
                                                          index]!
                                                      .interestRate
                                                      .toString() +
                                                  "%",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .twentyOneToTwnetyFiveList[
                                                          index]!
                                                      .installmentAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .twentyOneToTwnetyFiveList[
                                                          index]!
                                                      .interestRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .twentyOneToTwnetyFiveList[
                                                          index]!
                                                      .principalRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .twentyOneToTwnetyFiveList[
                                                          index]!
                                                      .outstandingAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .twentyOneToTwnetyFiveList[
                                                          index]!
                                                      .cumInterestPaid
                                                      .toString(),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        if (refinanceNewLeadController
                            .twentySixToThirtyList.isNotEmpty)
                          Card(
                            elevation: 2,
                            child: ExpansionTile(
                              title: Text(
                                'Year 26-30',
                                style: TextStyle(
                                  fontSize: FontSize.s19,
                                  color: Colors.black,
                                ),
                              ),
                              children: [
                                ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: refinanceNewLeadController
                                      .twentySixToThirtyList.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Table(
                                      border: TableBorder.all(
                                        color: const Color(0XFFE2E2E2),
                                        style: BorderStyle.solid,
                                        width: 2,
                                      ),
                                      children: [
                                        TableRow(
                                          decoration: BoxDecoration(
                                            color: refinanceNewLeadController
                                                        .resultMainApplicantStep3
                                                        .value
                                                        .packageDetails![1]
                                                        .maximumLoanTenure!
                                                        .mortgagePaymentList![
                                                            index]
                                                        .month
                                                        .toString() ==
                                                    '12'
                                                ? Colors.grey.shade400
                                                : Colors.white,
                                          ),
                                          children: [
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: index % 12 == 0
                                                  ? refinanceNewLeadController
                                                      .twentySixToThirtyList[
                                                          index]!
                                                      .year
                                                      .toString()
                                                  : "",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  refinanceNewLeadController
                                                      .twentySixToThirtyList[
                                                          index]!
                                                      .month
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  refinanceNewLeadController
                                                          .twentySixToThirtyList[
                                                              index]!
                                                          .interestRate
                                                          .toString() +
                                                      "%",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .twentySixToThirtyList[
                                                          index]!
                                                      .installmentAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .twentySixToThirtyList[
                                                          index]!
                                                      .interestRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .twentySixToThirtyList[
                                                          index]!
                                                      .principalRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .twentySixToThirtyList[
                                                          index]!
                                                      .outstandingAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .twentySixToThirtyList[
                                                          index]!
                                                      .cumInterestPaid
                                                      .toString(),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        if (refinanceNewLeadController
                            .thirtyOneToThirtyFiveList.isNotEmpty)
                          Card(
                            elevation: 2,
                            child: ExpansionTile(
                              title: Text(
                                'Year 31-35',
                                style: TextStyle(
                                  fontSize: FontSize.s19,
                                  color: Colors.black,
                                ),
                              ),
                              children: [
                                ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: refinanceNewLeadController
                                      .thirtyOneToThirtyFiveList.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Table(
                                      border: TableBorder.all(
                                        color: const Color(0XFFE2E2E2),
                                        style: BorderStyle.solid,
                                        width: 2,
                                      ),
                                      children: [
                                        TableRow(
                                          decoration: BoxDecoration(
                                            color: refinanceNewLeadController
                                                        .resultMainApplicantStep3
                                                        .value
                                                        .packageDetails![1]
                                                        .maximumLoanTenure!
                                                        .mortgagePaymentList![
                                                            index]
                                                        .month
                                                        .toString() ==
                                                    '12'
                                                ? Colors.grey.shade400
                                                : Colors.white,
                                          ),
                                          children: [
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: index % 12 == 0
                                                  ? refinanceNewLeadController
                                                      .thirtyOneToThirtyFiveList[
                                                          index]!
                                                      .year
                                                      .toString()
                                                  : "",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData:
                                                  refinanceNewLeadController
                                                      .thirtyOneToThirtyFiveList[
                                                          index]!
                                                      .month
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: refinanceNewLeadController
                                                      .thirtyOneToThirtyFiveList[
                                                          index]!
                                                      .interestRate
                                                      .toString() +
                                                  "%",
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .thirtyOneToThirtyFiveList[
                                                          index]!
                                                      .installmentAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .thirtyOneToThirtyFiveList[
                                                          index]!
                                                      .interestRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .thirtyOneToThirtyFiveList[
                                                          index]!
                                                      .principalRepayment
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .thirtyOneToThirtyFiveList[
                                                          index]!
                                                      .outstandingAmount
                                                      .toString(),
                                            ),
                                            DataRow(
                                              windowHeight: windowHeight,
                                              stringData: "\$" +
                                                  refinanceNewLeadController
                                                      .thirtyOneToThirtyFiveList[
                                                          index]!
                                                      .cumInterestPaid
                                                      .toString(),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class DataRow extends StatelessWidget {
  final String stringData;
  const DataRow({
    Key? key,
    required this.windowHeight,
    required this.stringData,
  }) : super(key: key);

  final double windowHeight;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            stringData,
            style: TextStyle(fontSize: FontSize.s10),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}

class TableTitleRowData extends StatelessWidget {
  const TableTitleRowData({
    Key? key,
    required this.windowHeight,
    required this.title,
  }) : super(key: key);

  final double windowHeight;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Column(
      // mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          height: windowHeight * 0.006,
        ),
        Text(
          title,
          style: TextStyle(
            fontSize: FontSize.s10,
            color: AppColors.kPrimaryColor,
          ),
          textAlign: TextAlign.center,
        ),
        SizedBox(
          height: windowHeight * 0.006,
        ),
      ],
    );
  }
}
