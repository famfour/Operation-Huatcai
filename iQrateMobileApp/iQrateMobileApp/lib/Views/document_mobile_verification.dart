import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/my_documents_controller.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';

class DocumentMobileVerification extends StatefulWidget {
  const DocumentMobileVerification({Key? key}) : super(key: key);

  @override
  State<DocumentMobileVerification> createState() =>
      _DocumentMobileVerificationState();
}

class _DocumentMobileVerificationState
    extends State<DocumentMobileVerification> {
  final MyDocumentsController mobileVerificationController =
      Get.put(MyDocumentsController());

  @override
  Widget build(BuildContext context) {
    double windowWidth = MediaQuery.of(context).size.width;
    double windowHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        automaticallyImplyLeading: false,
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 20.0, top: 30),
            child: InkWell(
              onTap: () {
                Get.back(); // Pop the existing page and go back to previous page
              },
              child: SvgPicture.asset(
                'assets/icons/closeEnclosed.svg',
                color: Colors.black,
              ),
            ),
          )
        ],
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 40.0, horizontal: 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: SvgPicture.asset(
                    "assets/logos/iqrate_logo.svg",
                    width: 80,
                  ),
                ),
                SizedBox(
                  height: windowHeight * 0.1,
                ),
                Center(
                  child: Text("Document Upload",
                      style: TextStyles.introScreenTitlesSmall),
                ),
                SizedBox(
                  height: windowHeight * 0.03,
                ),
                Text(
                  "An OTP has been sent to your registered mobile number and email address. Please enter the verification code.",
                  style: TextStyles.introScreenDescriptions,
                ),
                SizedBox(
                  height: windowHeight * 0.05,
                ),
                /*Center(
                  child: SizedBox(
                    height: windowHeight * 0.08,
                    child: RequireTextField(
                        type: Type.otp,
                        controller:
                            mobileVerificationController.mobileOtpController),
                  ),
                ),*/

                Center(
                  child: SizedBox(
                    height: windowHeight * 0.08,
                    child: RequireTextField(
                        type: Type.text,
                        controller:
                            mobileVerificationController.mobileOtpController),
                  ),
                ),
                SizedBox(
                  height: windowHeight * 0.02,
                ),
                SizedBox(
                  height: windowHeight * 0.02,
                ),
                Text(
                  "Didn't receive the OTP?",
                  style: TextStyles.introScreenDescriptions,
                ),
                SizedBox(
                  height: windowHeight * 0.01,
                ),
                InkWell(
                  onTap: () {
                    mobileVerificationController.sendOTP();
                  },
                  child: Text(
                    "Resend OTP",
                    style: TextStyles.pdpaSendtitleStyle,
                  ),
                ),
                SizedBox(
                  height: windowHeight * 0.06,
                ),
                PrimaryButton(
                  windowHeight: windowHeight,
                  windowWidth: windowWidth,
                  buttonTitle: "Continue",
                  onPressed: () async {
                    mobileVerificationController.onTapOTPSubmit();
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
