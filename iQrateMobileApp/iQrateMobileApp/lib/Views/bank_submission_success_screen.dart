import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Widgets/common_appbar_options.dart';
import 'package:iqrate/Widgets/primary_button.dart';

class BankSubmissionEmailSuccessScreen extends StatefulWidget {
  const BankSubmissionEmailSuccessScreen({Key? key}) : super(key: key);

  @override
  _BankSubmissionEmailSuccessScreenState createState() =>
      _BankSubmissionEmailSuccessScreenState();
}

class _BankSubmissionEmailSuccessScreenState
    extends State<BankSubmissionEmailSuccessScreen> {
  @override
  void initState() {
    debugPrint(Get.arguments[0].toString());
    debugPrint(Get.arguments[1].toString());
    debugPrint(Get.arguments[2].toString());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double windowWidth = MediaQuery.of(context).size.width;
    double windowHeight = MediaQuery.of(context).size.height;

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          automaticallyImplyLeading: false,
          centerTitle: false,
          flexibleSpace: const Padding(
            padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 30),
            child: CommonAppBarOptions(
              windowWidth: 0,
              title: 'Manage Loan',
            ),
          ),
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(windowHeight * 0.03),
            child: const SizedBox(),
          ),
          backgroundColor: const Color(0xFFDF5356),
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
            ),
          ),
        ),
        body: body(windowHeight, windowWidth));
  }

  Widget body(double windowHeight, double windowWidth) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SvgPicture.asset(
          "assets/images/verify_email.svg",
        ),
        SizedBox(
          height: windowHeight * 0.09,
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: windowWidth * 0.2),
          child: Text(
            'Email has been successfully send to the lead ${Get.arguments[2].value.name}.',
            style: TextStyles.bankSubmissionSuccessScreenText,
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(20.0, 30, 20, 20),
          child: PrimaryButton(
              windowHeight: windowHeight,
              windowWidth: windowWidth,
              buttonTitle: 'Done',
              onPressed: () {
                Get.back();
              }),
        )
      ],
    );
  }
}
