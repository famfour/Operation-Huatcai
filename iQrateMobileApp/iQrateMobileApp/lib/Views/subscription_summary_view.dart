import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/acl_controller.dart';
import 'package:iqrate/Controller/profile_controller.dart';
import 'package:iqrate/Controller/saved_cards_controller.dart';
import 'package:iqrate/Controller/subscription_options_controller.dart';
import 'package:iqrate/DeviceManager/container_styles.dart';
import 'package:iqrate/DeviceManager/screen_constants.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Service/url.dart';
import 'package:iqrate/Utils/config.dart';
import 'package:iqrate/Widgets/default_appbar.dart';

class SubscriptionSummary extends StatelessWidget {
  SubscriptionSummary({Key? key}) : super(key: key);
  final SubscriptionOptionsController subscriptionOptionsController =
      Get.put(SubscriptionOptionsController());
  final ProfileScreenController profileScreenController = Get.find();
  final ACLController aclController = Get.find();
  final SavedCardsController savedCardsController =
      Get.put(SavedCardsController());
  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: DefaultAppBar(
        title: "Subscription",
        windowHeight: windowHeight * 0.09,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 25),
        child: Column(
          children: [
            SubscriptionOptionsContainer(
              windowHeight: windowHeight,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Obx(
                              () => Container(
                                height: windowHeight * 0.03,
                                width: windowWidth * 0.3,
                                child: Center(
                                  child: subscriptionOptionsController
                                              .subscriptionSummary.value.product
                                              .toString() !=
                                          "null"
                                      ? Text(
                                          subscriptionOptionsController
                                              .subscriptionSummary.value.product
                                              .toString(),
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                              fontSize: FontSize.s18),
                                        )
                                      : Text(
                                          "Fetching",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                              fontSize: FontSize.s18),
                                        ),
                                ),
                                decoration: BoxDecoration(
                                  color: const Color(0XFFDA3B3E),
                                  borderRadius: BorderRadius.circular(5),
                                ),
                              ),
                            ),
                            Text(
                              '  Plan',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: FontSize.s18),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: windowHeight * 0.007,
                        ),
                        Obx(
                          () => Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              subscriptionOptionsController
                                          .subscriptionSummary.value.currency
                                          .toString() !=
                                      "null"
                                  ? Text(
                                      subscriptionOptionsController
                                          .subscriptionSummary.value.currency
                                          .toString()
                                          .toUpperCase(),
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: FontSize.s16,
                                      ),
                                    )
                                  : Text(
                                      "...",
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: FontSize.s16,
                                      ),
                                    ),
                              subscriptionOptionsController.subscriptionSummary
                                          .value.currentPlanAmount
                                          .toString() !=
                                      "null"
                                  ? Text(
                                      AppConfig.extraDecimalAdd(
                                          subscriptionOptionsController
                                              .subscriptionSummary
                                              .value
                                              .currentPlanAmount
                                              .toString()),
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: FontSize.s22,
                                      ),
                                    )
                                  : Text(
                                      "...",
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: FontSize.s22,
                                      ),
                                    ),
                              subscriptionOptionsController
                                          .subscriptionSummary.value.interval
                                          .toString() !=
                                      "null"
                                  ? Text(
                                      "/" +
                                          subscriptionOptionsController
                                              .subscriptionSummary
                                              .value
                                              .interval
                                              .toString(),
                                      style: TextStyle(
                                        color: const Color(0XFF9A9696),
                                        fontWeight: FontWeight.w400,
                                        fontSize: FontSize.s16,
                                      ),
                                    )
                                  : Text(
                                      "...",
                                      style: TextStyle(
                                        color: const Color(0XFF9A9696),
                                        fontWeight: FontWeight.w400,
                                        fontSize: FontSize.s16,
                                      ),
                                    ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    SubscriptionOptionsButton(
                      windowHeight: windowHeight,
                      windowWidth: windowWidth,
                      onPressed: () {
                        subscriptionOptionsController.onTapChangePlan();
                      },
                      title: "Change Plan",
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: windowHeight * 0.03,
            ),
            SubscriptionOptionsContainer(
              windowHeight: windowHeight,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Obx(
                      () => Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Next Payment On',
                            style: TextStyle(
                              color: const Color(0XFF9A9696),
                              fontSize: FontSize.s15,
                            ),
                          ),
                          subscriptionOptionsController
                                      .subscriptionSummary.value.product ==
                                  'Basic'
                              ? Text(
                                  '...',
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: FontSize.s17,
                                  ),
                                )
                              : subscriptionOptionsController
                                          .subscriptionSummary
                                          .value
                                          .currentPeriodEnd
                                          .toString() !=
                                      "null"
                                  ? Text(
                                      subscriptionOptionsController
                                          .subscriptionSummary
                                          .value
                                          .currentPeriodEnd
                                          .toString()
                                          .substring(0, 10)
                                          .replaceAll(',', ''),
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: FontSize.s17,
                                      ),
                                    )
                                  : Text(
                                      "...",
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: FontSize.s17,
                                      ),
                                    ),
                        ],
                      ),
                    ),
                    SubscriptionOptionsButton(
                      windowHeight: windowHeight,
                      windowWidth: windowWidth,
                      onPressed: () {
                        subscriptionOptionsController.onTapManageCards();
                      },
                      title: 'Manage Cards',
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: windowHeight * 0.03,
            ),
            SubscriptionOptionsContainer(
              windowHeight: windowHeight,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Payment History',
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: FontSize.s17,
                          ),
                        ),
                      ],
                    ),
                    SubscriptionOptionsButton(
                      windowHeight: windowHeight,
                      windowWidth: windowWidth,
                      onPressed: () {
                        aclController.aclNavigationCheck(
                            getInvoicesUrl, paymentHistoryScreen);
                      },
                      title: 'View Details',
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SubscriptionOptionsContainer extends StatelessWidget {
  const SubscriptionOptionsContainer({
    Key? key,
    required this.windowHeight,
    required this.child,
  }) : super(key: key);

  final double windowHeight;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: windowHeight * 0.13,
      decoration: ContainerStyles.subscriptionOptionsContainer,
      child: child,
    );
  }
}

class SubscriptionOptionsButton extends StatelessWidget {
  const SubscriptionOptionsButton({
    Key? key,
    required this.windowHeight,
    required this.windowWidth,
    required this.onPressed,
    required this.title,
  }) : super(key: key);

  final double windowHeight;
  final double windowWidth;
  final Function onPressed;
  final String title;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onPressed();
      },
      child: Container(
        height: windowHeight * 0.04,
        width: windowWidth * 0.33,
        decoration: ContainerStyles.subscriptionOptionsViewButtonContainerStyle,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                flex: 5,
                child: Text(
                  title,
                  style: TextStyle(
                    color: const Color(0XFFDA3B3E),
                    fontSize: FontSize.s15,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Icon(
                  Icons.arrow_forward_ios_outlined,
                  color: const Color(0XFFDA3B3E),
                  size: FontSize.s15,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
