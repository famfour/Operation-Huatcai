import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/law_firm_submission_controller.dart';
import 'package:iqrate/DeviceManager/assets.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/container_styles.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Model/response_model.dart/bank_submission_lawfirms_list_response_model.dart';
import 'package:iqrate/Widgets/form_field_title.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';
import 'package:iqrate/Widgets/secondary_button.dart';

import '../Service/GetControllers.dart';

class LawFirmSubmission extends StatefulWidget {
  const LawFirmSubmission({Key? key}) : super(key: key);

  @override
  _LawFirmSubmissionState createState() => _LawFirmSubmissionState();
}

class _LawFirmSubmissionState extends State<LawFirmSubmission> {
  late double windowHeight;
  late double windowWidth;

  var leadProfileController = GetControllers.shared.getLeadProfileController();

  LawFirmSubmissionController lawFirmSubmissionController =
      GetControllers.shared.getLawFirmSubmissionController();

  @override
  void initState() {
    lawFirmSubmissionController.leadID =
        leadProfileController.leadID.toString();
    lawFirmSubmissionController.documentsDrawerID =
        leadProfileController.documentsDrawerID.toString();
    lawFirmSubmissionController.getLawFirmList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.width;
    return Obx(() => IgnorePointer(
        ignoring: !leadProfileController.clientPDPAStatus.value,
        child: expandableCard("Law Firm Submission", false)));
  }

  Widget leadCreateView() {
    return Container(
        color: Colors.white,
        padding: EdgeInsets.all(windowHeight * 0.025),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const FormFieldTitle(title: 'Select Law Firm'),
            SizedBox(height: windowHeight * 0.01),
            Container(
              width: windowWidth,
              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
              decoration: BoxDecoration(
                  border: Border.all(
                      color: AppColors.formFieldBorderColor, width: 1.0),
                  borderRadius: const BorderRadius.all(Radius.circular(
                          10) //                 <--- border radius here
                      )),
              child: DropdownButtonHideUnderline(
                child: DropdownButton<LawFirmModel>(
                  hint: const Text('Select Law Firm'),
                  value: lawFirmSubmissionController.selectedLawFirm,
                  onChanged: (newValue) {
                    debugPrint(newValue!.id.toString());
                    setState(() {
                      lawFirmSubmissionController.selectedLawFirm = newValue;

                      lawFirmSubmissionController.legalFeesController.text =
                          newValue.legalFee.toString();
                      lawFirmSubmissionController.lawFirmEmailController.text =
                          newValue.email.toString();
                    });
                  },
                  items:
                      lawFirmSubmissionController.lawFirmList.map((location) {
                    return DropdownMenuItem<LawFirmModel>(
                      child: Text(location.name!),
                      value: location,
                    );
                  }).toList(),
                ),
              ),
            ),
            /* lawFirmSubmissionController.selectedLawFirm.value ==
                    lawFirmSubmissionController.ownLawFirm
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: windowHeight * 0.03),
                      const FormFieldTitle(title: 'Law Firm Name'),
                      SizedBox(height: windowHeight * 0.01),
                      RequireTextField(
                        type: Type.fullname,
                        labelText: "Name",
                        controller:
                            lawFirmSubmissionController.legalFeesController,
                        key: const Key("Name"),
                      ),
                    ],
                  )
                : const SizedBox(),*/
            SizedBox(height: windowHeight * 0.03),
            const FormFieldTitle(title: 'Legal Fees'),
            SizedBox(height: windowHeight * 0.01),
            RequireTextField(
              type: Type.customText,
              labelText: "Legal Fees",
              controller: lawFirmSubmissionController.legalFeesController,
              key: const Key("Legal"),
            ),
            SizedBox(height: windowHeight * 0.03),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const FormFieldTitle(title: 'Law Firm Email Address'),
                SizedBox(height: windowHeight * 0.01),
                RequireTextField(
                  type: Type.email,
                  labelText: "Email Address",
                  controller:
                      lawFirmSubmissionController.lawFirmEmailController,
                  key: const Key("Email"),
                ),
                SizedBox(height: windowHeight * 0.03),
              ],
            ),
            /* const FormFieldTitle(title: 'Email Address'),
            SizedBox(height: windowHeight * 0.02),
            InkWell(
              onTap: () {
                lawFirmSubmissionController.onTapSubmit();
              },
              child: Padding(
                padding: const EdgeInsets.only(left: 10.0),
                child: Text(
                  lawFirmSubmissionController.lawFirmEmailController.text,
                  style: TextStyles.dashboardPopUpTextStyle,
                ),
              ),
            ),*/
            //SizedBox(height: windowHeight * 0.03),
            const FormFieldTitle(title: 'Subject:'),
            SizedBox(height: windowHeight * 0.02),
            Padding(
              padding: const EdgeInsets.only(left: 10.0),
              child: Text(
                lawFirmSubmissionController
                        .lawFirmEmailTemplate.value.subject ??
                    "",
                style: TextStyles.dashboardPopUpTextStyle,
              ),
            ),
            SizedBox(height: windowHeight * 0.03),
            const FormFieldTitle(title: 'Email Content:'),
            SizedBox(height: windowHeight * 0.02),
            Padding(
              padding: const EdgeInsets.only(left: 10.0),
              child: Html(
                data: lawFirmSubmissionController
                        .lawFirmEmailTemplate.value.content ??
                    "",
              ),
            ),
            SizedBox(height: windowHeight * 0.05),
            const FormFieldTitle(title: 'Add Remarks into Email:'),
            SizedBox(height: windowHeight * 0.01),
            RequireTextField(
              type: Type.textArea,
              controller: lawFirmSubmissionController.remarksController,
              labelText: "Remarks",
              key: const Key("Remarks"),
            ),
            SizedBox(height: windowHeight * 0.03),
            Obx(() => ListView.builder(
                itemCount:
                    lawFirmSubmissionController.documentUploadList.length,
                primary: false,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  var item = lawFirmSubmissionController.documentUploadList
                      .elementAt(index);
                  return Padding(
                    padding: const EdgeInsets.symmetric(vertical: 5.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            const Icon(Icons.attach_file_outlined),
                            SizedBox(width: windowWidth * 0.02),
                            Text(
                              item.docType!,
                              style: TextStyles.dashboardPopUpTextStyle,
                            ),
                          ],
                        ),
                      ],
                    ),
                  );
                })),
            SizedBox(height: windowHeight * 0.03),

            SecondaryButton(
              windowHeight: windowHeight,
              windowWidth: windowWidth,
              kGradientBoxDecoration:
                  ContainerStyles.boxDecorationCalculatorButton,
              kInnerDecoration: ContainerStyles.boxDecorationCalculatorButton,
              buttonTitle: 'Add Documents',
              onPressed: () {
                lawFirmSubmissionController.addDocumentsScreen();
              },
            ),

            SizedBox(height: windowHeight * 0.03),
            PrimaryButton(
              windowHeight: windowHeight,
              windowWidth: windowWidth,
              buttonTitle: "Submit",
              onPressed: () {
                lawFirmSubmissionController.onTapSubmit();
              },
            ),
            SizedBox(height: windowHeight * 0.03),
            //documentSection(windowWidth, windowHeight),
            /*SizedBox(height: windowHeight * 0.03),
            PrimaryButton(
              windowHeight: windowHeight,
              windowWidth: windowWidth,
              buttonTitle: "Next",
              onPressed: () {},
            ),
            SizedBox(height: windowHeight * 0.03),
            */
            SecondaryButton(
              windowHeight: windowHeight,
              windowWidth: windowWidth,
              kGradientBoxDecoration:
                  ContainerStyles.boxDecorationCalculatorButton,
              kInnerDecoration: ContainerStyles.boxDecorationCalculatorButton,
              buttonTitle: 'Back',
              onPressed: () {
                lawFirmSubmissionController.isExpand.toggle();
              },
            ),
          ],
        ));
  }

  Widget expandableCard(String title, bool isDone) {
    return GestureDetector(
      onTap: () {
        lawFirmSubmissionController.isExpand.toggle();
        GetControllers.shared.getLoanDetailsController().isExpand.value = false;
        GetControllers.shared.getPropertyDetailsController().isExpand.value =
            false;
        GetControllers.shared.getGeneratePackagesController().isExpand.value =
            false;
        GetControllers.shared.getSelectedPackagesController().isExpand.value =
            false;
        GetControllers.shared.getBankSubmissionController().isExpand.value =
            false;
        GetControllers.shared
            .getSubmitForPayoutViewController()
            .isExpand
            .value = false;
        GetControllers.shared.getLeadProfileController().isExpand.value = false;
        GetControllers.shared.getEmailLogController().isExpand.value = false;
        GetControllers.shared.getSubmitCoBrokeController().isExpand.value =
            false;
      },
      child: Obx(() => Container(
            color: Colors.white,
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(bottom: windowWidth * 0.02),
                  padding: EdgeInsets.all(windowWidth * 0.05),
                  decoration: BoxDecoration(
                      boxShadow: const <BoxShadow>[
                        BoxShadow(
                            color: Colors.black54,
                            blurRadius: 5.0,
                            offset: Offset(0.0, 0.3))
                      ],
                      color: !leadProfileController.clientPDPAStatus.value
                          ? AppColors.greyDEDEDE
                          : Colors.white),
                  child: Row(
                    children: [
                      Text(title, style: TextStyles.basicTextStyle),
                      const Spacer(),
                      SvgPicture.asset(
                        isDone
                            ? Assets.manageLoanDone
                            : Assets.manageLoanIncomplete,
                        height: 24,
                        width: 24,
                      ),
                      SizedBox(width: windowWidth * 0.03),
                      Icon(
                        lawFirmSubmissionController.isExpand.value
                            ? Icons.keyboard_arrow_up_rounded
                            : Icons.keyboard_arrow_down,
                        size: 30,
                      )
                    ],
                  ),
                ),
                lawFirmSubmissionController.isExpand.value
                    ? leadCreateView()
                    : Container()
              ],
            ),
          )),
    );
  }

  var demoDocuments = ["Document1", "Document2"];

  Widget documentSection(double windowWidth, double windowHeight) {
    return SingleChildScrollView(
      child: Column(
        children: [
          section("NRIC", demoDocuments, windowWidth,
              lawFirmSubmissionController.isChecked),
          section("OTP-Option to Purchase", demoDocuments, windowWidth,
              lawFirmSubmissionController.isChecked),
          section("IRAS", demoDocuments, windowWidth,
              lawFirmSubmissionController.isChecked),
          section("CPF", demoDocuments, windowWidth,
              lawFirmSubmissionController.isChecked),
          section("HDB", demoDocuments, windowWidth,
              lawFirmSubmissionController.isChecked),
          section("Payslips", demoDocuments, windowWidth,
              lawFirmSubmissionController.isChecked),
          section("Valuation Report", demoDocuments, windowWidth,
              lawFirmSubmissionController.isChecked),
          section("HDB", demoDocuments, windowWidth,
              lawFirmSubmissionController.isChecked),
          section("Outstanding Loan statements", demoDocuments, windowWidth,
              lawFirmSubmissionController.isChecked),
          section("Others", demoDocuments, windowWidth,
              lawFirmSubmissionController.isChecked),
          section("Bank Forms", demoDocuments, windowWidth,
              lawFirmSubmissionController.isChecked),
          section("Letter of Offer", demoDocuments, windowWidth,
              lawFirmSubmissionController.isChecked),
          section("Calculator Reports", demoDocuments, windowWidth,
              lawFirmSubmissionController.isChecked),
          SizedBox(height: windowHeight * 0.02),
        ],
      ),
    );
  }

  Widget section(String docType, var documents, double windowWidth,
      RxBool selectAllCheckBoxValue) {
    // bool allchecked = false;
    return Container(
      margin: EdgeInsets.symmetric(vertical: windowWidth * 0.03),
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.black12,
          style: BorderStyle.solid,
          width: 1.0,
        ),
        color: Colors.transparent,
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(windowWidth * 0.05),
            width: windowWidth,
            decoration: BoxDecoration(
              color: Colors.black12,
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  width: windowWidth * 0.05,
                  child: Obx(
                    () => Checkbox(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                      //checkColor: AppColors.kSecondaryColor,
                      //fillColor: MaterialStateProperty.all(Colors.white),
                      hoverColor: AppColors.kSecondaryColor,
                      activeColor: Colors.black54,
                      side: const BorderSide(
                          color: AppColors.kSecondaryColor,
                          width: 1,
                          style: BorderStyle.solid),
                      onChanged: (newValue) {
                        selectAllCheckBoxValue.value = newValue!;
                        /*if (newValue == false) {
                          for (int i = 0; i < documents.length; i++) {
                            setState(() {
                              documents[i].isChecked = false;
                            });
                            bankSubmissionDocumentDrawerScreenController
                                .documentUploadIdList
                                .removeWhere(
                                ((element) => element == documents[i].id));
                          }
                        } else {
                          for (int i = 0; i < documents.length; i++) {
                            setState(() {
                              documents[i].isChecked = true;
                            });
                            bankSubmissionDocumentDrawerScreenController
                                .documentUploadIdList
                                .add(documents[i].id!);
                          }
                        }*/
                      },
                      value: selectAllCheckBoxValue.value,
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 5.0),
                    child: Text(
                      docType,
                      style: TextStyles.bankSubmissionDocumentScreenTextStyle,
                    ),
                  ),
                ),
                Text(
                  'Max 3 Mb',
                  style: TextStyles.leadsNormalTextStyle,
                ),
                SizedBox(width: windowWidth * 0.03),
                InkWell(
                    onTap: () {
                      debugPrint("onTap add");
                    },
                    child: const Icon(
                      Icons.add,
                      //color: Colors.red,
                      size: 30,
                    ))
              ],
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ListView.builder(
                itemCount: documents.length,
                primary: false,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  var item = documents.elementAt(index);
                  return Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(10),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: windowWidth * 0.08,
                              child: Checkbox(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5.0),
                                ),
                                //checkColor: AppColors.kSecondaryColor,
                                //fillColor: MaterialStateProperty.all(Colors.white),
                                hoverColor: AppColors.kSecondaryColor,
                                activeColor: Colors.black54,
                                side: const BorderSide(
                                    color: AppColors.kSecondaryColor,
                                    width: 1,
                                    style: BorderStyle.solid),
                                onChanged: (newValue) {
                                  setState(() {
                                    item.isChecked = newValue;
                                  });
                                  // Function to add and remove file id in list
                                  /*if (newValue!) {
                                    bankSubmissionDocumentDrawerScreenController
                                        .documentUploadIdList
                                        .add(item.id!);
                                    if (bankSubmissionDocumentDrawerScreenController
                                        .documentUploadIdList.length ==
                                        documents.length) {
                                      setState(() {
                                        selectAllCheckBoxValue.value = true;
                                      });
                                    }
                                  } else {
                                    bankSubmissionDocumentDrawerScreenController
                                        .documentUploadIdList
                                        .removeWhere(
                                            (element) => element == item.id);
                                    if (bankSubmissionDocumentDrawerScreenController
                                        .documentUploadIdList.length !=
                                        documents.length) {
                                      setState(() {
                                        selectAllCheckBoxValue.value = false;
                                      });
                                    }
                                  }*/
                                },
                                //value: item.isChecked ?? false,
                                value: true,
                              ),
                            ),
                            Expanded(
                              child: Text(
                                item,
                                style: TextStyles.docTextStyle,
                              ),
                            ),
                            InkWell(
                                onTap: () {
                                  debugPrint("onTap downlaod");
                                },
                                child: const Icon(
                                  Icons.remove_red_eye_outlined,
                                  size: 30,
                                )),
                            SizedBox(width: windowWidth * 0.03),
                            InkWell(
                              onTap: () {
                                debugPrint("onTap edit");
                              },
                              child: SvgPicture.asset(
                                "assets/images/doc_download.svg",
                                width: 24,
                              ),
                            ),
                            SizedBox(width: windowWidth * 0.03),
                            InkWell(
                                onTap: () {
                                  debugPrint("onTap delete");
                                },
                                child: SvgPicture.asset(
                                  "assets/images/card_delete.svg",
                                  color: Colors.black,
                                ))
                          ],
                        ),
                      ),
                      documents.length != 1 && documents.length != index + 1
                          ? const Divider(
                              height: 1,
                              thickness: 1,
                              color: Colors.black26,
                            )
                          : const SizedBox(),
                    ],
                  );
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}
