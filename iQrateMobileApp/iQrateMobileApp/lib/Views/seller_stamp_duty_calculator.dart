import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/seller_stamp_duty_calculator_controller.dart';
import 'package:iqrate/Views/rates/currency_amount_textField.dart';
import 'package:iqrate/Widgets/default_appbar.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';

import '../DeviceManager/text_styles.dart';
import '../Widgets/calculated_value_container.dart';
import '../Widgets/form_field_title.dart';

class SellerStampDutyView extends StatelessWidget {
  SellerStampDutyView({Key? key}) : super(key: key);
  final SellerStampDutyCalculatorController
      sellerStampDutyCalculatorController =
      Get.put(SellerStampDutyCalculatorController());

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: DefaultAppBar(
        title: "Seller's Stamp Duty",
        windowHeight: windowHeight * 0.09,
      ),
      body: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: windowHeight * 0.03,
                ),
                Text("Seller's Stamp Duty",
                    style: TextStyles.calculatorHeadingsTextStyle),
                SizedBox(
                  height: windowHeight * 0.03,
                ),
                const FormFieldTitle(
                  title: 'Selling Price*',
                ),
                SizedBox(
                  height: windowHeight * 0.01,
                ),
                CurrencyAmountTextField(
                  textEditingController: sellerStampDutyCalculatorController
                      .sellingPriceController,
                  labelText: "Selling Price",
                ),
                // RequireTextField(
                //   type: Type.number,
                //   controller: sellerStampDutyCalculatorController
                //       .sellingPriceController,
                //   labelText: "Selling Price",
                // ),
                SizedBox(
                  height: windowHeight * 0.01,
                ),
                const FormFieldTitle(
                  title: 'Date Of Purchase (Option Excercised)*',
                ),
                SizedBox(
                  height: windowHeight * 0.01,
                ),
                RequireTextField(
                  type: Type.date,
                  controller:
                      sellerStampDutyCalculatorController.sellingDateController,
                  labelText: "DD/MM/YY",
                  key: const Key("dob"),
                  dateFormat: "dd-MM-yyyy",
                ),
                SizedBox(
                  height: windowHeight * 0.03,
                ),
                CalculatedViewContainer(
                  title: "Seller's Stamp Duty",
                  windowWidth: windowWidth,
                  windowHeight: windowHeight,
                  calculatedValue:
                      sellerStampDutyCalculatorController.maximumSellingPrice !=
                              null
                          ? Obx(() => Text(
                                "\$" +
                                    sellerStampDutyCalculatorController
                                        .maximumSellingPrice
                                        .toString(),
                                style: TextStyles.calculatorResultTextStyle,
                              ))
                          : Text('\$0',
                              style: TextStyles.calculatorResultTextStyle),
                ),
                SizedBox(
                  height: windowHeight * 0.03,
                ),
                Center(
                  child: Text(
                      '"With the assumption that you are selling today"',
                      style: TextStyles.alreeadyHaveAnAccount),
                ),
                SizedBox(
                  height: windowHeight * 0.03,
                ),
                PrimaryButton(
                  windowHeight: windowHeight,
                  windowWidth: windowWidth,
                  buttonTitle:
                      // sellerStampDutyCalculatorController
                      //             .calculated.value ==
                      //         0
                      //     ?
                      "Calculate",
                  // : "Reset",
                  onPressed: () {
                    sellerStampDutyCalculatorController.calculate();
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
