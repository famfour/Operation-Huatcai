import 'dart:io';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/continue_with_google_button_controller.dart';
import 'package:iqrate/Controller/sign_up_controller.dart';
import 'package:iqrate/DeviceManager/container_styles.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Widgets/apple_signin_button.dart';
import 'package:iqrate/Widgets/form_field_title.dart';
import 'package:iqrate/Widgets/google_signin_button.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/privacy_policy_tos.dart';
import 'package:iqrate/Widgets/require_text_field.dart';
import 'package:url_launcher/url_launcher_string.dart';

class SignUp extends StatelessWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ContinueWithGoogleButtonController
        // ignore: unused_local_variable
        continueWithGoogleButtonController =
        Get.put(ContinueWithGoogleButtonController());
    final SignUpController signUpController = Get.put(SignUpController());
    double windowWidth = MediaQuery.of(context).size.width;
    double windowHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: SafeArea(
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(height: windowHeight * 0.02),
                  Text(
                    "Welcome to IQRATE",
                    style: TextStyles.headingTextStyle,
                  ),
                  SizedBox(height: windowHeight * 0.01),
                  RichText(
                      text: TextSpan(
                          text:
                              'Create an account to access exclusive home loan rates and more. Already have an account?',
                          style: TextStyles.headingSubtitleStyle,
                          children: [
                        TextSpan(
                            text: ' Log in',
                            style: TextStyles.loginTextButtonStyle,
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                Get.toNamed(loginScreen);
                              })
                      ])),
                  SizedBox(height: windowHeight * 0.02),
                  const FormFieldTitle(title: 'Full Name as per NRIC*'),
                  SizedBox(height: windowHeight * 0.01),
                  RequireTextField(
                    type: Type.fullnameWithClear,
                    labelText: "Enter your full name",
                    controller: signUpController.nameController,
                    key: const Key("fullname"),
                  ),
                  SizedBox(height: windowHeight * 0.01),
                  const FormFieldTitle(title: 'Email Address*'),
                  SizedBox(height: windowHeight * 0.01),
                  RequireTextField(
                    type: Type.emailWithClear,
                    controller: signUpController.emailController,
                    labelText: "Enter your email address",
                    key: const Key("email"),
                  ),
                  SizedBox(height: windowHeight * 0.01),
                  const FormFieldTitle(title: 'Mobile Number*'),
                  SizedBox(height: windowHeight * 0.01),
                  RequireTextField(
                    type: Type.dropdownCountryCodeWithPhoneClear,
                    controller: signUpController.phoneController,
                    countryCodeController:
                        signUpController.countryCodeController,
                    countryCodeLabelController:
                        signUpController.countryCodeLabelController,
                    labelText: "Enter your phone number",
                    key: const Key("phone"),
                  ),
                  //SizedBox(height: windowHeight * 0.01),
                  const FormFieldTitle(title: 'Date of Birth*'),
                  SizedBox(height: windowHeight * 0.01),
                  RequireTextField(
                    type: Type.dob,
                    controller: signUpController.dobController,
                    labelText: "DD/MM/YY",
                    key: const Key("dob"),
                  ),
                  SizedBox(height: windowHeight * 0.01),
                  const FormFieldTitle(title: 'Password*'),
                  SizedBox(height: windowHeight * 0.01),
                  RequireTextField(
                    type: Type.passWordWithClear,
                    controller: signUpController.passwordController,
                    labelText: "Alphanumeric (8+ characters)",
                    key: const Key("password"),
                  ),
                  SizedBox(height: windowHeight * 0.01),
                  const FormFieldTitle(title: 'Referral Code (Optional)'),
                  SizedBox(height: windowHeight * 0.01),
                  RequireTextField(
                    type: Type.optionalWithClear,
                    labelText: "Enter referral code",
                    controller: signUpController.referralCodeController,
                    key: const Key("code"),
                  ),
                  SizedBox(height: windowHeight * 0.04),
                  Center(
                    child: PrimaryButton(
                      windowHeight: windowHeight,
                      windowWidth: windowWidth,
                      buttonTitle: "Register",
                      onPressed: () {
                        signUpController.onSignUpButtonTap();
                      },
                    ),
                  ),
                  SizedBox(height: windowHeight * 0.02),
                  Center(
                    child: Text(
                      'or',
                      style: TextStyles.orTextStyle,
                    ),
                  ),
                  SizedBox(height: windowHeight * 0.02),
                  /* GoogleSignIn(
                    windowHeight: windowHeight,
                    windowWidth: windowWidth,
                    kGradientBoxDecoration:
                        ContainerStyles.kGradientBoxDecorationSecondaryButton,
                    kInnerDecoration:
                        ContainerStyles.kInnerDecorationSecondaryButton,
                    onPressed: () {
                      // continueWithGoogleButtonController.onPressedWebView();
                      signUpController.handleGoogleSignUp();
                    },
                    buttonTitle: "Continue with Google",
                  ),*/
                  Row(
                    children: [
                      Flexible(
                        flex: 1,
                        child: GoogleSignIn(
                          windowHeight: windowHeight,
                          windowWidth: windowWidth,
                          kGradientBoxDecoration: ContainerStyles
                              .kGradientBoxDecorationSecondaryButton,
                          kInnerDecoration:
                              ContainerStyles.kInnerDecorationSecondaryButton,
                          onPressed: () {
                            signUpController.handleGoogleSignUp();
                          },
                          buttonTitle: "",
                        ),
                      ),
                      Platform.isIOS ? Expanded(
                        child: Row(
                          children: [
                            SizedBox(
                              width: windowWidth * 0.03,
                            ),
                            Flexible(
                              flex: 1,
                              child: AppleSignIn(
                                windowHeight: windowHeight,
                                windowWidth: windowWidth,
                                kGradientBoxDecoration: ContainerStyles
                                    .kGradientBoxDecorationSecondaryButton,
                                kInnerDecoration:
                                    ContainerStyles.kInnerDecorationSecondaryButton,
                                onPressed: () {
                                  signUpController.handleAppleSignUp("");
                                },
                                buttonTitle: "",
                              ),
                            ),
                          ],
                        ),
                      ) : const SizedBox(),
                    ],
                  ),
                  SizedBox(height: windowHeight * 0.03),
                  // SizedBox(height: windowHeight * 0.03),
                  TosAndPrivacyPolicy(
                    tapGestureRecognizerPrivacyPolicy: TapGestureRecognizer()
                      ..onTap = () async {
                        // Open the privacy policy URL
                        String url = 'https://iqrate.netinu.io/privacy-policy/';
                        try {
                          if (await canLaunchUrlString(url)) {
                            await launchUrlString(url);
                          } else {
                            throw "Could not launch $url";
                          }
                        } catch (e) {
                          debugPrint(e.toString());
                        }
                      },
                    tapGestureRecognizerTos: TapGestureRecognizer()
                      ..onTap = () async {
                        // Open the terms and condition URL
                        String url =
                            'https://iqrate.netinu.io/terms-conditions/';
                        try {
                          if (await canLaunchUrlString(url)) {
                            await launchUrlString(url);
                          } else {
                            throw "Could not launch $url";
                          }
                        } catch (e) {
                          debugPrint(e.toString());
                        }
                      },
                  ),
                  SizedBox(height: windowHeight * 0.03),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
