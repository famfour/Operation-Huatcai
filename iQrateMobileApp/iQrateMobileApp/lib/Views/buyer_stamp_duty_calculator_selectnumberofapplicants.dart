import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/buyer_stamp_duty_calculator_controller.dart';
import 'package:iqrate/DeviceManager/screen_constants.dart';
import 'package:iqrate/Widgets/default_appbar.dart';
import 'package:iqrate/Widgets/form_field_title.dart';
import 'package:iqrate/Widgets/primary_button.dart';

import '../DeviceManager/colors.dart';

class BuyerStampDutyView extends StatefulWidget {
  const BuyerStampDutyView({Key? key}) : super(key: key);

  @override
  State<BuyerStampDutyView> createState() => _BuyerStampDutyViewState();
}

class _BuyerStampDutyViewState extends State<BuyerStampDutyView> {
  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    final BuyerStampDutyCalculatorController
        buyerStampDutyCalculatorController =
        Get.put(BuyerStampDutyCalculatorController());
    return Scaffold(
      appBar: DefaultAppBar(
        title: "Buyer's Stamp Duty",
        windowHeight: windowHeight * 0.09,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              child: Center(
                child: SvgPicture.asset('assets/images/stamp.svg'),
              ),
            ),
            SizedBox(
              height: windowHeight * 0.03,
            ),
            Text(
              'Find out with our calculator!',
              style: TextStyle(
                color: const Color(0XFF777373),
                fontSize: FontSize.s24,
                fontWeight: FontWeight.w600,
              ),
            ),
            SizedBox(
              height: windowHeight * 0.03,
            ),
            const Align(
              alignment: Alignment.centerLeft,
              child: FormFieldTitle(
                title: "Select Number of Loan Applicants",
              ),
            ),
            SizedBox(
              height: windowHeight * 0.01,
            ),
            Container(
              width: windowWidth,
              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
              decoration: BoxDecoration(
                  border: Border.all(
                      color: AppColors.formFieldBorderColor, width: 1.0),
                  borderRadius: const BorderRadius.all(Radius.circular(
                          10) //                 <--- border radius here
                      )),
              child: DropdownButtonHideUnderline(
                child: DropdownButton(
                  hint: const Text('1'),
                  // Not necessary for Option 1
                  value: buyerStampDutyCalculatorController.numberOfApplicants,
                  onChanged: (newValue) {
                    setState(() {
                      buyerStampDutyCalculatorController.numberOfApplicants =
                          int.tryParse(newValue.toString())!;
                      debugPrint(buyerStampDutyCalculatorController
                          .numberOfApplicants
                          .toString());
                    });
                  },
                  items: const [
                    DropdownMenuItem(child: Text('1'), value: 1),
                    DropdownMenuItem(child: Text('2'), value: 2),
                    DropdownMenuItem(child: Text('3'), value: 3),
                    DropdownMenuItem(child: Text('4'), value: 4),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: windowHeight * 0.03,
            ),
            PrimaryButton(
              windowHeight: windowHeight,
              windowWidth: windowWidth,
              buttonTitle: "Continue",
              onPressed: () {
                buyerStampDutyCalculatorController
                    .onPressContinueForNumberOfApplicants();
              },
            ),
          ],
        ),
      ),
    );
  }
}
