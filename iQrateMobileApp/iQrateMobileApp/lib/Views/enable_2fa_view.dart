import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/enable_twofa_view_controller.dart';
import 'package:iqrate/Controller/profile_controller.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Widgets/primary_button.dart';

// ignore: must_be_immutable
class Enable2FAView extends StatefulWidget {
  const Enable2FAView({Key? key}) : super(key: key);

  @override
  State<Enable2FAView> createState() => _Enable2FAViewState();
}

class _Enable2FAViewState extends State<Enable2FAView> {
  Enable2FAViewController enable2faViewController =
      Get.put(Enable2FAViewController());

  ProfileScreenController profileScreenController = Get.find();

  @override
  void initState() {
    enable2faViewController.switchValue.value = Get.arguments == null
        ? false
        : profileScreenController
            .userData.value.isTwoFactorAuthenticationEnabled!;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: EdgeInsets.symmetric(horizontal: windowWidth * 0.05),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(vertical: windowHeight * 0.02),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    InkWell(
                      onTap: () {
                        Get.back(); // Pop the existing page and go back to previous page
                      },
                      child: SvgPicture.asset(
                        'assets/icons/closeEnclosed.svg',
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: windowHeight * 0.05,
              ),
              SvgPicture.asset(
                'assets/images/two_fact.svg',
              ),
              SizedBox(
                height: windowHeight * 0.05,
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  'Google Authenticator',
                  style: TextStyles.introScreenTitlesSmall,
                ),
              ),
              SizedBox(
                height: windowHeight * 0.01,
              ),
              Text(
                'Enable two-factor (2FA) authentication for an extra layer of protection. This is to ensure that your account is secured.',
                style: TextStyles.introScreenDescriptions,
              ),
              SizedBox(
                height: windowHeight * 0.03,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Status', style: TextStyles.introScreenDescriptions),
                  Obx(
                    () => CupertinoSwitch(
                        value: enable2faViewController.switchValue.value,
                        onChanged: enable2faViewController.switchOnTap),
                  )
                ],
              ),
              SizedBox(
                height: windowHeight * 0.1,
              ),
              if (enable2faViewController.switchValue.value == false)
                Obx(
                  () => PrimaryButton(
                      windowHeight: windowHeight,
                      windowWidth: windowWidth,
                      buttonTitle: 'Enable 2FA',
                      isDisable: !enable2faViewController.switchValue.value,
                      onPressed: () {
                        enable2faViewController.onPressEnable2fa();
                      }),
                )
            ],
          ),
        ),
      ),
    );
  }
}
