import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/authenticator_otp_controller.dart';
import 'package:iqrate/DeviceManager/container_styles.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/secondary_button.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class AuthenticatorOtp extends StatefulWidget {
  const AuthenticatorOtp({Key? key}) : super(key: key);

  @override
  State<AuthenticatorOtp> createState() => _AuthenticatorOtpState();
}

class _AuthenticatorOtpState extends State<AuthenticatorOtp> {
  final AuthenticatorOtpController authenticatorOtpController = Get.put(
      AuthenticatorOtpController()); //AuthenticatorOtpController used to access all functions and variables in AuthenticatorOtpController;

  @override
  void dispose() {
    authenticatorOtpController.clearRedirectionFromLogin();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double windowWidth =
        MediaQuery.of(context).size.width; //get width of the screen
    double windowHeight =
        MediaQuery.of(context).size.height; //get height of the screen
    return Scaffold(
      body: GestureDetector(
        //This onTap is used to dismiss the keyboard when the user taps outside the textfield
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: SafeArea(
            child: SingleChildScrollView(
          child: Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 25.0, vertical: 18.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: SvgPicture.asset(
                    "assets/images/two_fact.svg",
                  ),
                ),
                SizedBox(
                  height: windowHeight * 0.15,
                ),
                Text("Enter Confirmation Code",
                    style: TextStyles.introScreenTitlesSmall),
                SizedBox(
                  height: windowHeight * 0.02,
                ),
                Text(
                  "Please enter the Confirmation code that you see on your Google Authnticator app",
                  style: TextStyles.introScreenDescriptions,
                ),
                SizedBox(
                  height: windowHeight * 0.05,
                ),
                /*Center(
                  child: RequireTextField(
                    type: Type.otp,
                    controller:
                        authenticatorOtpController.authenticatorOtpController,
                  ),
                ),*/
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: windowWidth * 0.02),
                  child: PinCodeTextField(
                    appContext: context,
                    pastedTextStyle: TextStyle(
                      color: Colors.green.shade600,
                      fontWeight: FontWeight.bold,
                    ),
                    length: 6,
                    obscureText: false,
                    obscuringCharacter: '*',
                    blinkWhenObscuring: true,
                    animationType: AnimationType.fade,
                    pinTheme: PinTheme(
                      shape: PinCodeFieldShape.box,
                      borderWidth: 1,
                      borderRadius: BorderRadius.circular(5),
                      fieldHeight: 55,
                      fieldWidth: 55,
                      activeFillColor: Colors.white,
                      inactiveFillColor: Colors.white,
                      inactiveColor: Colors.grey,
                      selectedFillColor: Colors.white,
                      disabledColor: Colors.red,
                      selectedColor: Colors.grey,
                      activeColor: Colors.black54,
                    ),
                    cursorColor: Colors.black,
                    textStyle: TextStyles.leadsTextStyle1,
                    animationDuration: const Duration(milliseconds: 300),
                    enableActiveFill: true,
                    controller:
                        authenticatorOtpController.authenticatorOtpController,
                    keyboardType: TextInputType.number,
                    /*boxShadows: const [
                      BoxShadow(
                        offset: Offset(0, 1),
                        color: AppColors.filledColor,
                        blurRadius: 10,
                      )
                    ],*/
                    onCompleted: (v) {
                      if (kDebugMode) {
                        print("Completed");
                      }
                    },
                    onChanged: (value) {
                      if (kDebugMode) {
                        print(value);
                      }
                      /* setState(() {
                        currentText = value;
                      });*/
                    },
                    beforeTextPaste: (text) {
                      if (kDebugMode) {
                        print("Allowing to paste $text");
                      }
                      return true;
                    },
                  ),
                ),
                SizedBox(
                  height: windowHeight * 0.05,
                ),
                PrimaryButton(
                  windowHeight: windowHeight,
                  windowWidth: windowWidth,
                  buttonTitle: "Continue",
                  onPressed: () {
                    authenticatorOtpController.onSubmitButtonTap();
                  },
                ),
                SizedBox(
                  height: windowHeight * 0.03,
                ),
                SecondaryButton(
                  windowHeight: windowHeight,
                  windowWidth: windowWidth,
                  kGradientBoxDecoration:
                      ContainerStyles.kGradientBoxDecorationSecondaryButton,
                  kInnerDecoration:
                      ContainerStyles.kInnerDecorationSecondaryButton,
                  onPressed: () {
                    authenticatorOtpController.onCancel();
                  },
                  buttonTitle: "Cancel",
                ),
              ],
            ),
          ),
        )),
      ),
    );
  }
}
