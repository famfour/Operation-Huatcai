// ignore_for_file: prefer_typing_uninitialized_variables, must_be_immutable

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/bank_submission_document_drawer_screen_controller.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/container_styles.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Model/response_model.dart/documents_drawer_response_model.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Widgets/common_appbar_options.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/secondary_button.dart';

class BankSubmissionDocumentDrawerScreen extends StatefulWidget {
  const BankSubmissionDocumentDrawerScreen({Key? key}) : super(key: key);

  @override
  _BankSubmissionDocumentDrawerScreenState createState() =>
      _BankSubmissionDocumentDrawerScreenState();
}

class _BankSubmissionDocumentDrawerScreenState
    extends State<BankSubmissionDocumentDrawerScreen> {
  BankSubmissionDocumentDrawerScreenController
      bankSubmissionDocumentDrawerScreenController =
      Get.put(BankSubmissionDocumentDrawerScreenController());
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // double windowWidth = MediaQuery.of(context).size.width;
    double windowHeight = MediaQuery.of(context).size.height;

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          automaticallyImplyLeading: false,
          centerTitle: false,
          flexibleSpace: const Padding(
            padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 30),
            child: CommonAppBarOptions(
              windowWidth: 0,
              title: 'Manage Loan',
            ),
          ),
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(windowHeight * 0.03),
            child: const SizedBox(),
          ),
          backgroundColor: const Color(0xFFDF5356),
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
            ),
          ),
        ),
        body: const Body());
  }
}

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  BankSubmissionDocumentDrawerScreenController
      bankSubmissionDocumentDrawerScreenController = Get.find();

  @override
  Widget build(BuildContext context) {
    double windowWidth = MediaQuery.of(context).size.width;
    double windowHeight = MediaQuery.of(context).size.height;
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: ListView(
        children: [
          Text(
            'Email to Banker',
            style: TextStyles.bankSubmissionTitleTestStyle,
          ),
          const Divider(
            thickness: 2,
            color: AppColors.kPrimaryColor,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 20.0),
            child: Text(
              'Select Documents to send:',
              style: TextStyles.bankSubmissionBody,
            ),
          ),
          documentSection(windowWidth, windowHeight),
          PrimaryButton(
              windowHeight: windowHeight,
              windowWidth: windowWidth,
              buttonTitle: 'Next',
              onPressed: () {
                if (bankSubmissionDocumentDrawerScreenController
                    .documentUploadIdList.isNotEmpty) {
                  var convertedListToSet =
                      bankSubmissionDocumentDrawerScreenController
                          .documentUploadIdList
                          .toSet();
                  bankSubmissionDocumentDrawerScreenController
                      .documentUploadIdList = convertedListToSet.toList();
                  Get.offNamed(bankSubmissionDocumentCompressionScreen,
                      arguments: [
                        bankSubmissionDocumentDrawerScreenController
                            .documentUploadIdList,
                        Get.arguments[0], //BankID
                        Get.arguments[1] //LeadID
                      ]);
                } else {
                  //* user can't procceed without selecting a document

                  Future.delayed(const Duration(milliseconds: 0), () {
                    FocusManager.instance.primaryFocus!.unfocus();
                    Fluttertoast.showToast(
                      timeInSecForIosWeb: 3,
                      msg: "Please select at least one document",
                      backgroundColor: Colors.white,
                      toastLength: Toast.LENGTH_LONG,
                      gravity: ToastGravity.BOTTOM,
                      textColor: Colors.black,
                      fontSize: 20.0,
                    );
                  });
                  // Get.offNamed(bankSubmissionEmailBankerScreen, arguments: [
                  //   Get.arguments[0], //BankID
                  //   Get.arguments[1], //LeadID
                  //   null
                  // ]);
                }
              }),
          SizedBox(height: windowHeight * 0.015),
          SecondaryButton(
              windowHeight: windowHeight,
              windowWidth: windowWidth,
              kGradientBoxDecoration:
                  ContainerStyles.kGradientBoxDecorationSecondaryButton,
              kInnerDecoration: ContainerStyles.kInnerDecorationSecondaryButton,
              onPressed: () {
                Get.back();
              },
              buttonTitle: 'Back')
        ],
      ),
    );
  }

  Widget documentSection(double windowWidth, double windowHeight) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Obx(() => section(
              "Bank Forms",
              bankSubmissionDocumentDrawerScreenController.bankForms,
              windowWidth,
              bankSubmissionDocumentDrawerScreenController.bankFormsCheckBox)),
          Obx(() => section(
              "NRIC",
              bankSubmissionDocumentDrawerScreenController.nRICs,
              windowWidth,
              bankSubmissionDocumentDrawerScreenController.nRICsCheckBox)),
          Obx(() => section(
              "Option to Purchase",
              bankSubmissionDocumentDrawerScreenController.oTPOptiontoPurchase,
              windowWidth,
              bankSubmissionDocumentDrawerScreenController
                  .oTPOptiontoPurchaseCheckBox)),
          Obx(() => section(
              "IRAS Statements",
              bankSubmissionDocumentDrawerScreenController.iRASs,
              windowWidth,
              bankSubmissionDocumentDrawerScreenController.iRASsCheckBox)),
          Obx(() => section(
              "CPF Statements",
              bankSubmissionDocumentDrawerScreenController.cPFs,
              windowWidth,
              bankSubmissionDocumentDrawerScreenController.cPFsCheckBox)),
          Obx(() => section(
              "HDB Statements",
              bankSubmissionDocumentDrawerScreenController.hDBs,
              windowWidth,
              bankSubmissionDocumentDrawerScreenController.hDBsCheckBox)),
          Obx(() => section(
              "Payslips",
              bankSubmissionDocumentDrawerScreenController.payslips,
              windowWidth,
              bankSubmissionDocumentDrawerScreenController.payslipsCheckBox)),
          Obx(() => section(
              "Valuation Reports",
              bankSubmissionDocumentDrawerScreenController.valuationReports,
              windowWidth,
              bankSubmissionDocumentDrawerScreenController
                  .valuationReportsCheckBox)),
          Obx(() => section(
              "Outstanding Loan Statements",
              bankSubmissionDocumentDrawerScreenController.loanStatements,
              windowWidth,
              bankSubmissionDocumentDrawerScreenController
                  .loanStatementsCheckBox)),
          Obx(() => section(
              "Tenancy Agreement",
              bankSubmissionDocumentDrawerScreenController.tenancyAgreement,
              windowWidth,
              bankSubmissionDocumentDrawerScreenController
                  .tenancyAgreementCheckBox)),
          Obx(() => section(
              "Others",
              bankSubmissionDocumentDrawerScreenController.others,
              windowWidth,
              bankSubmissionDocumentDrawerScreenController.othersCheckBox)),
          Obx(() => section(
              "LO",
              bankSubmissionDocumentDrawerScreenController.lOs,
              windowWidth,
              bankSubmissionDocumentDrawerScreenController
                  .loanStatementsCheckBox)),
          Obx(() => section(
              "Calculator Reports",
              bankSubmissionDocumentDrawerScreenController.calculatorReports,
              windowWidth,
              bankSubmissionDocumentDrawerScreenController
                  .calculatorReportsCheckBox)),
          SizedBox(height: windowHeight * 0.02),
        ],
      ),
    );
  }

  Widget section(String docType, List<Documents> documents, double windowWidth,
      RxBool selectAllCheckBoxValue) {
    // bool allchecked = false;
    return Container(
      margin: EdgeInsets.all(windowWidth * 0.05),
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.black26,
          style: BorderStyle.solid,
          width: 1.0,
        ),
        color: Colors.transparent,
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(windowWidth * 0.05),
            width: windowWidth,
            decoration: BoxDecoration(
              color: Colors.black12,
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  width: windowWidth * 0.05,
                  child: Obx(
                    () => Checkbox(
                      onChanged: (newValue) {
                        selectAllCheckBoxValue.value = newValue!;
                        if (newValue == false) {
                          for (int i = 0; i < documents.length; i++) {
                            setState(() {
                              documents[i].isChecked = false;
                            });
                            bankSubmissionDocumentDrawerScreenController
                                .documentUploadIdList
                                .removeWhere(
                                    ((element) => element == documents[i].id));
                          }
                        } else {
                          for (int i = 0; i < documents.length; i++) {
                            setState(() {
                              documents[i].isChecked = true;
                            });
                            bankSubmissionDocumentDrawerScreenController
                                .documentUploadIdList
                                .add(documents[i].id!);
                          }
                        }
                      },
                      value: selectAllCheckBoxValue.value,
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 5.0),
                    child: Text(
                      docType,
                      style: TextStyles.bankSubmissionDocumentScreenTextStyle,
                    ),
                  ),
                ),
                Text(
                  'Max 3 Mb',
                  style: TextStyles.leadsNormalTextStyle,
                ),
                SizedBox(width: windowWidth * 0.03),
                InkWell(
                    onTap: () {
                      bankSubmissionDocumentDrawerScreenController
                          .onTapUploadPopup(docType);

                      debugPrint("onTap add");
                    },
                    child: const Icon(
                      Icons.add,
                      color: Colors.red,
                      size: 35,
                    ))
              ],
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ListView.builder(
                itemCount: documents.length,
                primary: false,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  var item = documents.elementAt(index);
                  return Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(10),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: windowWidth * 0.08,
                              child: Checkbox(
                                onChanged: (newValue) {
                                  setState(() {
                                    item.isChecked = newValue;
                                  });
                                  // Function to add and remove file id in list
                                  if (newValue!) {
                                    bankSubmissionDocumentDrawerScreenController
                                        .documentUploadIdList
                                        .add(item.id!);
                                    if (bankSubmissionDocumentDrawerScreenController
                                            .documentUploadIdList.length ==
                                        documents.length) {
                                      setState(() {
                                        selectAllCheckBoxValue.value = true;
                                      });
                                    }
                                  } else {
                                    bankSubmissionDocumentDrawerScreenController
                                        .documentUploadIdList
                                        .removeWhere(
                                            (element) => element == item.id);
                                    if (bankSubmissionDocumentDrawerScreenController
                                            .documentUploadIdList.length !=
                                        documents.length) {
                                      setState(() {
                                        selectAllCheckBoxValue.value = false;
                                      });
                                    }
                                  }
                                },
                                value: item.isChecked ?? false,
                              ),
                            ),
                            Expanded(
                              child: Text(
                                item.filename!,
                                style: TextStyles.docTextStyle,
                              ),
                            ),
                            InkWell(
                                onTap: () {
                                  bankSubmissionDocumentDrawerScreenController
                                      .onTapPopUpUpdate(item);
                                  debugPrint("onTap edit");
                                },
                                child: const Icon(
                                  Icons.edit,
                                  size: 30,
                                )),
                            SizedBox(width: windowWidth * 0.03),
                            InkWell(
                                onTap: () {
                                  bankSubmissionDocumentDrawerScreenController
                                      .onTapDownloadButton(item.id!);
                                  debugPrint("onTap downlaod");
                                },
                                child: const Icon(
                                  Icons.remove_red_eye_outlined,
                                  size: 30,
                                )),
                            SizedBox(width: windowWidth * 0.03),
                            InkWell(
                                onTap: () {
                                  bankSubmissionDocumentDrawerScreenController
                                      .onTapPopUpDelete(item.id);
                                  debugPrint("onTap delete");
                                },
                                child: const Icon(
                                  Icons.delete_outline,
                                  size: 30,
                                ))
                          ],
                        ),
                      ),
                      documents.length != 1 && documents.length != index + 1
                          ? const Divider(
                              height: 1,
                              thickness: 1,
                              color: Colors.black26,
                            )
                          : const SizedBox(),
                    ],
                  );
                },
              ),
              // Padding(
              //   padding: const EdgeInsets.all(20.0),
              //   child: Row(
              //     crossAxisAlignment: CrossAxisAlignment.start,
              //     children: [
              //       Flexible(
              //         child: Text(
              //           'Documents',
              //           style: TextStyles.docTextStyle,
              //         ),
              //       ),
              //       SizedBox(width: windowWidth * 0.03),
              //       const Spacer(),
              //       InkWell(
              //           onTap: () {
              //             debugPrint("onTap edit");
              //           },
              //           child: const Icon(
              //             Icons.edit,
              //             size: 30,
              //           )),
              //       SizedBox(width: windowWidth * 0.03),
              //       InkWell(
              //           onTap: () {
              //             debugPrint("onTap eye");
              //           },
              //           child: const Icon(
              //             Icons.remove_red_eye_outlined,
              //             size: 30,
              //           )),
              //       SizedBox(width: windowWidth * 0.03),
              //       InkWell(
              //           onTap: () {
              //             // bankSubmissionDocumentDrawerScreenController
              //             //     .onTapPopUpDelete();
              //             debugPrint("onTap delete2");
              //           },
              //           child: const Icon(
              //             Icons.delete_outline,
              //             size: 30,
              //           ))
              //     ],
              //   ),
              // ),
            ],
          ),
        ],
      ),
    );
  }
}
