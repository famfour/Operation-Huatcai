// ignore_for_file: file_names

import '../../Model/response_model.dart/safe_convert.dart';

class LeadListModelResult {
  int? count = 0;
  Object? next;
  Object? previous;
  dynamic results = [];

  LeadListModelResult({
    this.count,
    this.next,
    this.previous,
    this.results,
  });

  LeadListModelResult.fromJson(Map<String, dynamic> json)
      : count = SafeManager.parseInt(json, 'count'),
        next = json['next'],
        previous = json['previous'],
        results = json['results'];

  Map<String, dynamic> toJson() => {
        'count': count,
        'next': next,
        'previous': previous,
        'results': results,
      };
}
