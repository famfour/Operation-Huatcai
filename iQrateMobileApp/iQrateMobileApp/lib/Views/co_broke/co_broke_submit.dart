import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:iqrate/DeviceManager/assets.dart';
import 'package:iqrate/DeviceManager/container_styles.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Model/response_model.dart/fetch_lead_list_response_model.dart';
import 'package:iqrate/Provider/network_provider.dart';
import 'package:iqrate/Widgets/secondary_button.dart';
import 'package:logger/logger.dart';
import '../../DeviceManager/colors.dart';
import '../../Service/GetControllers.dart';
import '../../Service/url.dart';
import '../../Widgets/primary_button.dart';

class SubmitCoBrokeView extends StatefulWidget {
  final Result data;
  const SubmitCoBrokeView({Key? key, required this.data}) : super(key: key);

  @override
  _SubmitCoBrokeViewState createState() => _SubmitCoBrokeViewState();
}

class _SubmitCoBrokeViewState extends State<SubmitCoBrokeView> {
  late double windowHeight;
  late double windowWidth;

  var submitController = GetControllers.shared.getSubmitCoBrokeController();
  var leadProfileController = GetControllers.shared
      .getLeadProfileController(); //Accessing lead controller because we are using the PDPA status to update the UI
  // Result? data;

  @override
  Widget build(BuildContext context) {
    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.width;
    return Obx(() => IgnorePointer(
        ignoring: !leadProfileController.clientPDPAStatus.value,
        child: expandableCard("Submit to Co-Broke",
            widget.data.coBrokeStatus == 'Co-Broke Sent' ? true : false)));
  }

  Widget expandableCard(String title, bool isDone) {
    // debugPrint('***********');
    // debugPrint(leadProfileController.clientPDPAStatus.value.toString());
    // debugPrint(leadProfileController.jointClient1PDPAStatus.value.toString());
    // debugPrint(leadProfileController.jointClient2PDPAStatus.value.toString());
    // debugPrint(leadProfileController.jointClient3PDPAStatus.value.toString());
    // debugPrint('***********');
    var datas =
        GetControllers.shared.getGeneratePackagesController().coBrokerData;
    Logger().d(datas.toJson());
    return GestureDetector(
      onTap: () {
        submitController.isExpand.toggle();
        GetControllers.shared.getLoanDetailsController().isExpand.value = false;
        GetControllers.shared.getPropertyDetailsController().isExpand.value =
            false;
        GetControllers.shared.getGeneratePackagesController().isExpand.value =
            false;
        GetControllers.shared.getSelectedPackagesController().isExpand.value =
            false;
        GetControllers.shared.getBankSubmissionController().isExpand.value =
            false;
        GetControllers.shared
            .getSubmitForPayoutViewController()
            .isExpand
            .value = false;
        GetControllers.shared.getLawFirmSubmissionController().isExpand.value =
            false;
        GetControllers.shared.getLeadProfileController().isExpand.value = false;
        GetControllers.shared
            .getLeadProfileController()
            .checkIfPDPACompletedForAll();
      },
      child: Obx(() => Container(
            color: Colors.white,
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(bottom: windowWidth * 0.02),
                  padding: EdgeInsets.all(windowWidth * 0.05),
                  decoration: BoxDecoration(
                      boxShadow: const <BoxShadow>[
                        BoxShadow(
                            color: Colors.black54,
                            blurRadius: 5.0,
                            offset: Offset(0.0, 0.3))
                      ],
                      color: !leadProfileController.clientPDPAStatus.value
                          ? AppColors.greyDEDEDE
                          : Colors.white),
                  child: Row(
                    children: [
                      Text(title, style: TextStyles.basicTextStyle),
                      const Spacer(),
                      SvgPicture.asset(
                        isDone
                            ? Assets.manageLoanDone
                            : Assets.manageLoanIncomplete,
                        height: 24,
                        width: 24,
                      ),
                      SizedBox(width: windowWidth * 0.03),
                      Icon(
                        submitController.isExpand.value
                            ? Icons.keyboard_arrow_up_rounded
                            : Icons.keyboard_arrow_down,
                        size: 30,
                      )
                    ],
                  ),
                ),
                submitController.isExpand.value
                    ? Padding(
                        padding: EdgeInsets.all(windowHeight * 0.025),
                        child: Column(
                          children: [
                            Text(
                                'IQrate partnered mortgage broker will close the deal for you. Referral fee will be co-shared at 50%.',
                                style: TextStyles.leadsTextStyle),
                            const SizedBox(
                              height: 30,
                            ),

                            !GetControllers.shared
                                    .getLeadProfileController()
                                    .checkIfPDPACompletedForAll()
                                ? Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      const Icon(
                                        Icons.report,
                                        color: AppColors.kPrimaryColor,
                                        size: 35,
                                      ),
                                      const SizedBox(
                                        width: 10,
                                      ),
                                      Flexible(
                                          child: Text(
                                        "PDPA not approved. Please get lead to accept PDPA before proceeding.",
                                        style: TextStyles.leadsTextStyle
                                            .copyWith(
                                                color: AppColors.kPrimaryColor,
                                                fontSize: 14),
                                        maxLines: 3,
                                      ))
                                    ],
                                  )
                                : const SizedBox(),
                            // : const SizedBox(),
                            !GetControllers.shared
                                    .getLeadProfileController()
                                    .checkIfPDPACompletedForAll()
                                ? const SizedBox(
                                    height: 30,
                                  )
                                : const SizedBox(),

                            !GetControllers.shared
                                    .getLeadProfileController()
                                    .checkIfPDPACompletedForAll()
                                ? SecondaryButton(
                                    windowHeight: windowHeight,
                                    windowWidth: windowWidth,
                                    kGradientBoxDecoration: ContainerStyles
                                        .kGradientBoxDecorationSecondaryButton,
                                    kInnerDecoration: ContainerStyles
                                        .kInnerDecorationSecondaryButton,
                                    buttonTitle: 'Back',
                                    onPressed: () {
                                      Get.back();
                                    },
                                  )
                                : widget.data.coBrokeStatus == 'Co-Broke Sent'
                                    ? Container()
                                    : PrimaryButton(
                                        windowHeight: windowHeight,
                                        windowWidth: windowWidth,
                                        buttonTitle: "Submit",
                                        onPressed: () {
                                          // print("aaaaa  ${GetControllers.shared
                                          //     .getGeneratePackagesController()
                                          //     .coBrokerData
                                          //     .clients!}");
                                          submitController.callAPISubmitCoBroke(
                                              GetControllers.shared
                                                  .getGeneratePackagesController()
                                                  .coBrokerData
                                                  .id
                                                  .toString());
                                          debugPrint(
                                              "co=broke submit ${GetControllers.shared.getGeneratePackagesController().coBrokerData.id.toString()}");
                                        },
                                      ),
                          ],
                        ),
                      )
                    : Container()
              ],
            ),
          )),
    );
  }
}

class SubmitCoBrokeController extends GetxController {
  // var itemCoBroke = <LeadItemModel>[].obs;
  var isExpand = false.obs;
  Future<bool> callAPISubmitCoBroke(id) async {
    debugPrint("start callAPISubmitCoBroke ");
    GetControllers.shared.showLoading();
    var dio = Dio();
    dio.options.headers["Authorization"] =
        "Bearer ${GetControllers.shared.getToken()}";
    var endpoint = baseUrl + "customer/lead/request-co-broke/" + id;
    debugPrint("Url : $endpoint");
    var token = GetControllers.shared.getToken();
    var data = await NetworkProvider()
        .callAPIPost(token: token, url: endpoint); //Send the otp to the server.
    GetControllers.shared.hideLoading();
    if (NetworkProvider().isSuccessAPI(data)) {
      debugPrint("✅ OK mortgageCalculatorUrlf");
      GetControllers.shared.hideLoading(); //closing the loader
      Get.back(); //Poping back the screen
      Future.delayed(const Duration(milliseconds: 0), () {
        FocusManager.instance.primaryFocus!.unfocus();
        Fluttertoast.showToast(
          timeInSecForIosWeb: 3,
          msg: data["message"],
          backgroundColor: Colors.green,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          textColor: Colors.white,
          fontSize: 20.0,
        );
      });
      Logger().d(data);
      return true;
    } else {
      debugPrint("❌  mortgageCalculatorUrl");
      GetControllers.shared.showAlertPopupOneButtonWithCallBack(
          content: NetworkProvider().getErrorMessage(data), callback: () {});
      return false;
    }
  }
}
