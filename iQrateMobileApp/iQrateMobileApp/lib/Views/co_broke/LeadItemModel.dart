// ignore_for_file: file_names

import 'package:iqrate/Model/response_model.dart/safe_convert.dart';

class LeadItemModel {
  int? id = 0;
  String? leadType = "";
  String? leadStatus = "";
  String? agentApplied = "";
  dynamic coBrokeAgent;
  bool? sellWithinThreeYears = false;
  bool? has200kDown = false;
  dynamic loanCategory;
  double? loanAmount = 0.0;
  dynamic outstandingLoanAmount;
  dynamic existingBank;
  dynamic currentInterestRate;
  // Clients? clients;
  List<Clients>? clients = [];
  dynamic packages;
  dynamic wonLead;
  dynamic property;
  String? documentDrawerUrl = "";
  bool? canEdit = false;
  String? coBrokeStatus = "";
  String? documentDrawerId = "";
  String? createdBy = "";
  dynamic updatedBy;
  String? created = "";
  String? updated = "";

  LeadItemModel({
    this.id,
    this.leadType,
    this.leadStatus,
    this.agentApplied,
    this.coBrokeAgent,
    this.sellWithinThreeYears,
    this.has200kDown,
    this.loanCategory,
    this.loanAmount,
    this.outstandingLoanAmount,
    this.existingBank,
    this.currentInterestRate,
    this.clients,
    this.packages,
    this.wonLead,
    this.property,
    this.documentDrawerUrl,
    this.canEdit,
    this.coBrokeStatus,
    this.documentDrawerId,
    this.createdBy,
    this.updatedBy,
    this.created,
    this.updated,
  });

  LeadItemModel.fromJson(Map<String, dynamic> json)
      : id = SafeManager.parseInt(json, 'id'),
        leadType = SafeManager.parseString(json, 'lead_type'),
        leadStatus = SafeManager.parseString(json, 'lead_status'),
        agentApplied = SafeManager.parseString(json, 'agent_applied'),
        coBrokeAgent = json['co_broke_agent'],
        sellWithinThreeYears =
            SafeManager.parseBoolean(json, 'sell_within_three_years'),
        has200kDown = SafeManager.parseBoolean(json, 'has_200k_down'),
        loanCategory = json['loan_category'],
        loanAmount = SafeManager.parseDouble(json, 'loan_amount'),
        outstandingLoanAmount = json['outstanding_loan_amount'],
        existingBank = json['existing_bank'],
        currentInterestRate = json['current_interest_rate'],
        // clients = json['clients'],
        // clients = Clients.fromJson(
        //   SafeManager.parseObject(json, 'clients'),
        // ),
        clients = SafeManager.parseList(json, 'clients')
            .map((e) => Clients.fromJson(e))
            .toList(),
        packages = json['packages'],
        wonLead = json['won_lead'],
        property = json['property'],
        documentDrawerUrl =
            SafeManager.parseString(json, 'document_drawer_url'),
        canEdit = SafeManager.parseBoolean(json, 'can_edit'),
        coBrokeStatus = SafeManager.parseString(json, 'co_broke_status'),
        documentDrawerId = SafeManager.parseString(json, 'document_drawer_id'),
        createdBy = SafeManager.parseString(json, 'created_by'),
        updatedBy = json['updated_by'],
        created = SafeManager.parseString(json, 'created'),
        updated = SafeManager.parseString(json, 'updated');

  Map<String, dynamic> toJson() => {
        'id': id,
        'lead_type': leadType,
        'lead_status': leadStatus,
        'agent_applied': agentApplied,
        'co_broke_agent': coBrokeAgent,
        'sell_within_three_years': sellWithinThreeYears,
        'has_200k_down': has200kDown,
        'loan_category': loanCategory,
        'loan_amount': loanAmount,
        'outstanding_loan_amount': outstandingLoanAmount,
        'existing_bank': existingBank,
        'current_interest_rate': currentInterestRate,
        'clients': clients,
        'packages': packages,
        'won_lead': wonLead,
        'property': property,
        'document_drawer_url': documentDrawerUrl,
        'can_edit': canEdit,
        'co_broke_status': coBrokeStatus,
        'document_drawer_id': documentDrawerId,
        'created_by': createdBy,
        'updated_by': updatedBy,
        'created': created,
        'updated': updated,
      };
}

class Clients {
  int? id = 0;
  int? lead = 0;
  String? name = "";
  int? countryCode = 0;
  int? phoneNumber = 0;
  bool? mainApplicant = false;
  bool? pdpaStatus = false;
  String? email = "";
  dynamic annualIncome;
  dynamic dob;
  String? nationality = "";
  dynamic streetName;
  dynamic blockNumber;
  dynamic unitNumber;
  dynamic projectName;
  dynamic postalCode;
  dynamic createdBy = "";
  dynamic updatedBy;
  String? created = "";
  String? updated = "";

  Clients({
    this.id,
    this.lead,
    this.name,
    this.countryCode,
    this.phoneNumber,
    this.mainApplicant,
    this.pdpaStatus,
    this.email,
    this.annualIncome,
    this.dob,
    this.nationality,
    this.streetName,
    this.blockNumber,
    this.unitNumber,
    this.projectName,
    this.postalCode,
    this.createdBy,
    this.updatedBy,
    this.created,
    this.updated,
  });

  Clients.fromJson(Map<String, dynamic> json)
      : id = SafeManager.parseInt(json, 'id'),
        lead = SafeManager.parseInt(json, 'lead'),
        name = SafeManager.parseString(json, 'name'),
        countryCode = SafeManager.parseInt(json, 'country_code'),
        phoneNumber = SafeManager.parseInt(json, 'phone_number'),
        mainApplicant = SafeManager.parseBoolean(json, 'main_applicant'),
        pdpaStatus = SafeManager.parseBoolean(json, 'pdpa_status'),
        email = SafeManager.parseString(json, 'email'),
        annualIncome = json['annual_income'],
        dob = json['dob'],
        nationality = SafeManager.parseString(json, 'nationality'),
        streetName = json['street_name'],
        blockNumber = json['block_number'],
        unitNumber = json['unit_number'],
        projectName = json['project_name'],
        postalCode = json['postal_code'],
        createdBy = SafeManager.parseString(json, 'created_by'),
        updatedBy = json['updated_by'],
        created = SafeManager.parseString(json, 'created'),
        updated = SafeManager.parseString(json, 'updated');

  Map<String, dynamic> toJson() => {
        'id': id,
        'lead': lead,
        'name': name,
        'country_code': countryCode,
        'phone_number': phoneNumber,
        'main_applicant': mainApplicant,
        'pdpa_status': pdpaStatus,
        'email': email,
        'annual_income': annualIncome,
        'dob': dob,
        'nationality': nationality,
        'street_name': streetName,
        'block_number': blockNumber,
        'unit_number': unitNumber,
        'project_name': projectName,
        'postal_code': postalCode,
        'created_by': createdBy,
        'updated_by': updatedBy,
        'created': created,
        'updated': updated,
      };
}
