import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/payment_history_controller.dart';
import 'package:iqrate/DeviceManager/container_styles.dart';
import 'package:iqrate/DeviceManager/screen_constants.dart';
import 'package:iqrate/Utils/config.dart';

class PaymentHistoryView extends StatelessWidget {
  PaymentHistoryView({Key? key}) : super(key: key);
  final PaymentHistoryController paymentHistoryController =
      Get.put(PaymentHistoryController());

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        automaticallyImplyLeading: false,
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 8.0, top: 20),
            child: InkWell(
              onTap: () {
                Get.back(); // Pop the existing page and go back to previous page
              },
              child: SvgPicture.asset(
                'assets/icons/closeEnclosed.svg',
                color: Colors.black,
              ),
            ),
          )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Payment History',
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: FontSize.s22),
            ),
            SizedBox(height: windowHeight * 0.02),
            Obx(
              () => Expanded(
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: paymentHistoryController.invoicesList.length,
                  itemBuilder: (context, index) => Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10.0),
                    child: Container(
                      height: windowHeight * 0.1,
                      decoration: ContainerStyles.subscriptionOptionsContainer,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 14.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Obx(
                                  () {
                                    return PaymentHistoryDetailsRichText(
                                      title: "Invoice Number",
                                      value: paymentHistoryController
                                          .invoicesList
                                          .elementAt(
                                          index)['status'] !=
                                              null
                                          ? paymentHistoryController
                                                      .invoicesList
                                                      .elementAt(
                                                          index)['status']
                                                      .toString() !=
                                                  'paid'
                                              ? 'NA'
                                              : "..." +
                                                  paymentHistoryController
                                                      .invoicesList
                                                      .elementAt(
                                                          index)['number']
                                                      .toString()
                                                      .substring(5)
                                          : "Fetching...",
                                    );
                                  },
                                ),
                                SizedBox(
                                  height: windowHeight * 0.007,
                                ),
                                Obx(
                                  () => PaymentHistoryDetailsRichText(
                                    title: "Amount",
                                    value: paymentHistoryController
                                                .invoices.value.amountPaid !=
                                            null
                                        ? "\$" +
                                            AppConfig.extraDecimalAdd(
                                                paymentHistoryController
                                                    .invoicesList
                                                    .elementAt(
                                                        index)['amount_paid']
                                                    .toString())
                                        : "Fetching...",
                                  ),
                                ),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Obx(
                                  () => PaymentHistoryDetailsRichText(
                                    title: "Date",
                                    value: paymentHistoryController
                                        .invoicesList
                                        .elementAt(index)[
                                    'period_start'] !=
                                            null
                                        ? DateTime.fromMicrosecondsSinceEpoch(
                                                paymentHistoryController
                                                            .invoicesList
                                                            .elementAt(index)[
                                                        'period_start'] *
                                                    1000000)
                                            .toString()
                                            .substring(0, 10)
                                            .split('-')
                                            .reversed
                                            .join('/')
                                        : "Fetching...",
                                  ),
                                ),
                                SizedBox(
                                  height: windowHeight * 0.007,
                                ),
                                Row(
                                  children: [
                                    Obx(
                                      () => Container(
                                        height: windowHeight * 0.02,
                                        width: windowWidth * 0.15,
                                        decoration: paymentHistoryController
                                                    .invoices.value.status
                                                    .toString() ==
                                                "paid"
                                            ? BoxDecoration(
                                                color: const Color(0XFF91DB98)
                                                    .withOpacity(0.57),
                                                borderRadius:
                                                    const BorderRadius.all(
                                                  Radius.circular(3),
                                                ),
                                                border: Border.all(
                                                  color:
                                                      const Color(0XFF91DB98),
                                                  width: 1,
                                                ),
                                              )
                                            : BoxDecoration(
                                                color: const Color(0XFFDB9191)
                                                    .withOpacity(0.57),
                                                borderRadius:
                                                    const BorderRadius.all(
                                                  Radius.circular(3),
                                                ),
                                                border: Border.all(
                                                  color:
                                                      const Color(0XFFDB9191),
                                                  width: 1,
                                                ),
                                              ),
                                        child: Center(
                                          child: Text(
                                            paymentHistoryController
                                                        .invoicesList
                                                        .elementAt(
                                                            index)['status']
                                                        .toString() ==
                                                    "paid"
                                                ? 'Paid'
                                                : 'Failed',
                                            style: const TextStyle(
                                              color: Color(0XFF537356),
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    InkWell(
                                      onTap: () {
                                        debugPrint('url>>>>>>>>');
                                        debugPrint(paymentHistoryController
                                            .invoicesList
                                            .elementAt(index)['invoice_pdf']);
                                        paymentHistoryController
                                            .onDownloadPdfButtonTap(
                                                paymentHistoryController
                                                    .invoicesList
                                                    .elementAt(
                                                        index)['invoice_pdf'],
                                                paymentHistoryController
                                                        .invoicesList
                                                        .elementAt(
                                                            index)['number'] +
                                                    ".pdf");
                                      },
                                      child: const Padding(
                                        padding: EdgeInsets.all(8.0),
                                        child: Icon(
                                          Icons.file_download_sharp,
                                          color: Color(0XFFDA3B3E),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class PaymentHistoryDetailsRichText extends StatelessWidget {
  const PaymentHistoryDetailsRichText({
    Key? key,
    required this.title,
    required this.value,
  }) : super(key: key);

  final String title;
  final String value;

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        children: [
          TextSpan(
            text: title + " :",
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: FontSize.s14),
          ),
          TextSpan(
            text: "  " + value,
            style: TextStyle(
              color: const Color(0XFF6F767E),
              fontSize: FontSize.s14,
            ),
          ),
        ],
      ),
    );
  }
}
