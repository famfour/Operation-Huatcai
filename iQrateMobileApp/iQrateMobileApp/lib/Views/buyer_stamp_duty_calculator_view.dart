import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/buyer_stamp_duty_calculator_controller.dart';
import 'package:iqrate/Views/rates/currency_amount_textField.dart';
import 'package:iqrate/Widgets/calculated_value_container.dart';
import 'package:iqrate/Widgets/default_appbar.dart';
import 'package:iqrate/Widgets/primary_button.dart';

import '../DeviceManager/colors.dart';
import '../DeviceManager/text_styles.dart';
import '../Widgets/form_field_title.dart';

class BuyerStampDutyCalculator extends StatefulWidget {
  const BuyerStampDutyCalculator({Key? key}) : super(key: key);

  @override
  State<BuyerStampDutyCalculator> createState() =>
      _BuyerStampDutyCalculatorState();
}

class _BuyerStampDutyCalculatorState extends State<BuyerStampDutyCalculator> {
  final BuyerStampDutyCalculatorController buyerStampDutyCalculatorController =
      Get.find();

  @override
  void dispose() {
    buyerStampDutyCalculatorController.isCalculated.value = false;

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: DefaultAppBar(
        title: "Buyer's Stamp Duty Calculator",
        windowHeight: windowHeight * 0.09,
      ),
      body: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: windowHeight * 0.03,
                ),
                Text("Buyer's Stamp Duty",
                    style: TextStyles.calculatorHeadingsTextStyle),
                SizedBox(
                  height: windowHeight * 0.03,
                ),
                const FormFieldTitle(
                  title: 'Purchase Price*',
                ),
                SizedBox(
                  height: windowHeight * 0.01,
                ),
                CurrencyAmountTextField(
                  textEditingController:
                      buyerStampDutyCalculatorController.purchasePriceContoller,
                  labelText: "Purchase Price",
                ),
                // RequireTextField(
                //     labelText: '\$ Purchase Price',
                //     type: Type.number,
                //     controller: buyerStampDutyCalculatorController
                //         .purchasePriceContoller),
                if (buyerStampDutyCalculatorController.numberOfApplicants >=
                        1 &&
                    buyerStampDutyCalculatorController.numberOfApplicants <= 4)
                  Obx(
                    () => BuyerStampDutyApplicantForm(
                      windowHeight: windowHeight,
                      windowWidth: windowWidth,
                      applicantNumber: "1",
                      numberOfPropertiesApplicant:
                          buyerStampDutyCalculatorController
                              .numberOfProperties.value,
                      onChangedNumberOfProperties: (newValue) {
                        setState(() {
                          buyerStampDutyCalculatorController
                              .numberOfProperties.value = newValue;
                        });
                      },
                      nationalityApplicant:
                          buyerStampDutyCalculatorController.nationality.value,
                      onChangedNationality: (newValue) {
                        setState(() {
                          buyerStampDutyCalculatorController.nationality.value =
                              newValue;
                        });
                      },
                    ),
                  ),
                if (buyerStampDutyCalculatorController.numberOfApplicants >=
                        2 &&
                    buyerStampDutyCalculatorController.numberOfApplicants <= 4)
                  Obx(
                    () => BuyerStampDutyApplicantForm(
                      windowHeight: windowHeight,
                      windowWidth: windowWidth,
                      applicantNumber: "2",
                      numberOfPropertiesApplicant:
                          buyerStampDutyCalculatorController
                              .numberOfPropertiesApplicant2.value,
                      onChangedNumberOfProperties: (newValue) {
                        setState(() {
                          buyerStampDutyCalculatorController
                              .numberOfPropertiesApplicant2.value = newValue;
                        });
                      },
                      nationalityApplicant: buyerStampDutyCalculatorController
                          .nationalityApplicant2.value,
                      onChangedNationality: (newValue) {
                        setState(() {
                          buyerStampDutyCalculatorController
                              .nationalityApplicant2.value = newValue;
                        });
                      },
                    ),
                  ),
                if (buyerStampDutyCalculatorController.numberOfApplicants >=
                        3 &&
                    buyerStampDutyCalculatorController.numberOfApplicants <= 4)
                  Obx(
                    () => BuyerStampDutyApplicantForm(
                      windowHeight: windowHeight,
                      windowWidth: windowWidth,
                      applicantNumber: "3",
                      numberOfPropertiesApplicant:
                          buyerStampDutyCalculatorController
                              .numberOfPropertiesApplicant3.value,
                      onChangedNumberOfProperties: (newValue) {
                        setState(() {
                          buyerStampDutyCalculatorController
                              .numberOfPropertiesApplicant3.value = newValue;
                        });
                      },
                      nationalityApplicant: buyerStampDutyCalculatorController
                          .nationalityApplicant3.value,
                      onChangedNationality: (newValue) {
                        setState(() {
                          buyerStampDutyCalculatorController
                              .nationalityApplicant3.value = newValue;
                        });
                      },
                    ),
                  ),
                if (buyerStampDutyCalculatorController.numberOfApplicants == 4)
                  Obx(
                    () => BuyerStampDutyApplicantForm(
                      windowHeight: windowHeight,
                      windowWidth: windowWidth,
                      applicantNumber: "4",
                      numberOfPropertiesApplicant:
                          buyerStampDutyCalculatorController
                              .numberOfPropertiesApplicant4.value,
                      onChangedNumberOfProperties: (newValue) {
                        setState(() {
                          buyerStampDutyCalculatorController
                              .numberOfPropertiesApplicant4.value = newValue;
                        });
                      },
                      nationalityApplicant: buyerStampDutyCalculatorController
                          .nationalityApplicant4.value,
                      onChangedNationality: (newValue) {
                        setState(() {
                          buyerStampDutyCalculatorController
                              .nationalityApplicant4.value = newValue;
                        });
                      },
                    ),
                  ),
                Obx(() => buyerStampDutyCalculatorController.isCalculated.value
                    ? Column(
                        children: [
                          SizedBox(
                            height: windowHeight * 0.03,
                          ),
                          CalculatedViewContainer(
                            title: "Buyer's Stamp Duty",
                            windowWidth: windowWidth,
                            windowHeight: windowHeight,
                            calculatedValue: buyerStampDutyCalculatorController
                                        .buyerStampDuty !=
                                    null
                                ? Obx(
                                    () => Text(
                                        "\$" +
                                            buyerStampDutyCalculatorController
                                                .buyerStampDuty
                                                .toString(),
                                        style: TextStyles
                                            .calculatorResultTextStyle),
                                  )
                                : Text('\$0',
                                    style:
                                        TextStyles.calculatorResultTextStyle),
                          ),
                          SizedBox(
                            height: windowHeight * 0.03,
                          ),
                          CalculatedViewContainer(
                            title: "Additional Buyer's Stamp Duty",
                            windowWidth: windowWidth,
                            windowHeight: windowHeight,
                            calculatedValue: buyerStampDutyCalculatorController
                                        .additionalBuyerStampDuty !=
                                    null
                                ? Obx(
                                    () => Text(
                                        "\$" +
                                            buyerStampDutyCalculatorController
                                                .additionalBuyerStampDuty
                                                .toString(),
                                        style: TextStyles
                                            .calculatorResultTextStyle),
                                  )
                                : Text('\$0',
                                    style:
                                        TextStyles.calculatorResultTextStyle),
                          ),
                          SizedBox(
                            height: windowHeight * 0.03,
                          ),
                          CalculatedViewContainer(
                            title: "Total Buyer's Stamp Duty",
                            windowWidth: windowWidth,
                            windowHeight: windowHeight,
                            calculatedValue: buyerStampDutyCalculatorController
                                        .totalBuyerStampDuty !=
                                    null
                                ? Obx(
                                    () => Text(
                                        "\$" +
                                            buyerStampDutyCalculatorController
                                                .totalBuyerStampDuty
                                                .toString(),
                                        style: TextStyles
                                            .calculatorResultTextStyle),
                                  )
                                : Text('\$0',
                                    style:
                                        TextStyles.calculatorResultTextStyle),
                          ),
                        ],
                      )
                    : const SizedBox()),
                SizedBox(
                  height: windowHeight * 0.03,
                ),
                PrimaryButton(
                  windowHeight: windowHeight,
                  windowWidth: windowWidth,
                  buttonTitle:
                      // buyerStampDutyCalculatorController.calculated.value ==
                      //         0
                      // ?
                      "Calculate",
                  // : "Reset",
                  onPressed: () {
                    buyerStampDutyCalculatorController.calculate();
                  },
                ),
                SizedBox(
                  height: windowHeight * 0.03,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class BuyerStampDutyApplicantForm extends StatefulWidget {
  const BuyerStampDutyApplicantForm({
    Key? key,
    required this.windowHeight,
    required this.windowWidth,
    required this.applicantNumber,
    required this.numberOfPropertiesApplicant,
    required this.onChangedNumberOfProperties,
    required this.onChangedNationality,
    required this.nationalityApplicant,
  }) : super(key: key);

  @override
  State<BuyerStampDutyApplicantForm> createState() =>
      _BuyerStampDutyApplicantFormState();

  final double windowHeight;
  final double windowWidth;
  final String applicantNumber;
  final String numberOfPropertiesApplicant;
  final String nationalityApplicant;
  final Function onChangedNumberOfProperties;
  final Function onChangedNationality;
}

class _BuyerStampDutyApplicantFormState
    extends State<BuyerStampDutyApplicantForm> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: widget.windowHeight * 0.04,
        ),
        Text('Applicant ${widget.applicantNumber}',
            style: TextStyles.calculatorHeadingsTextStyle),
        SizedBox(
          height: widget.windowHeight * 0.02,
        ),
        const FormFieldTitle(
            title: 'How many residential properties do you own?'),
        SizedBox(
          height: widget.windowHeight * 0.01,
        ),
        Container(
          width: widget.windowWidth,
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
          decoration: BoxDecoration(
              border:
                  Border.all(color: AppColors.formFieldBorderColor, width: 1.0),
              borderRadius: const BorderRadius.all(
                  Radius.circular(10) //                 <--- border radius here
                  )),
          child: DropdownButtonHideUnderline(
            child: DropdownButton(
              hint: const Text('0'),
              // Not necessary for Option 1
              value: widget.numberOfPropertiesApplicant,
              onChanged: (newValue) {
                widget.onChangedNumberOfProperties(newValue);
              },
              items: const [
                DropdownMenuItem(child: Text('0'), value: "0"),
                DropdownMenuItem(child: Text('1'), value: "1"),
                DropdownMenuItem(child: Text('2 or more'), value: "2"),
                // DropdownMenuItem(child: Text('3 or more'), value: "3"),
              ],
            ),
          ),
        ),
        SizedBox(
          height: widget.windowHeight * 0.02,
        ),
        const FormFieldTitle(title: 'Your Nationality*'),
        SizedBox(
          height: widget.windowHeight * 0.01,
        ),
        Container(
          width: widget.windowWidth,
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
          decoration: BoxDecoration(
              border:
                  Border.all(color: AppColors.formFieldBorderColor, width: 1.0),
              borderRadius: const BorderRadius.all(
                  Radius.circular(10) //                 <--- border radius here
                  )),
          child: DropdownButtonHideUnderline(
            child: DropdownButton(
              hint: const Text('Singaporean Permanent Resident (SPR)'),
              // Not necessary for Option 1
              value: widget.nationalityApplicant,
              onChanged: (newValue) {
                widget.onChangedNationality(newValue);
              },
              items: const [
                DropdownMenuItem(
                  child: Text('Singapore Citizens'),
                  value: "Singapore Citizens",
                ),
                DropdownMenuItem(
                  child: Text('Singapore Permanent Residents'),
                  value: "Singapore Permanent Residents",
                ),
                DropdownMenuItem(
                  child: Text('Iceland Citizens / Permanent Residents'),
                  value: "Iceland Citizens / Permanent Residents",
                ),
                DropdownMenuItem(
                  child: Text('Liechtenstein Citizens / Permanent Residents'),
                  value: "Liechtenstein Citizens / Permanent Residents",
                ),
                DropdownMenuItem(
                  child: Text('Norway Citizens / Permanent Residents'),
                  value: "Norway Citizens / Permanent Residents",
                ),
                DropdownMenuItem(
                  child: Text('Switzerland Citizens / Permanent Residents'),
                  value: "Switzerland Citizens / Permanent Residents",
                ),
                DropdownMenuItem(
                  child: Text('USA Citizens'),
                  value: "USA Citizens",
                ),
                DropdownMenuItem(
                  child: Text('Others'),
                  value: "Others",
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
