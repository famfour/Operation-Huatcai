// ignore_for_file: prefer_typing_uninitialized_variables, must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/bank_submission_onebank_task_screen_controller.dart';
import 'package:iqrate/DeviceManager/assets.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Model/response_model.dart/bank_submission_banks_list_response_model.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Widgets/common_appbar_options.dart';

class BankSubmissionOneBankTaskScreen extends StatefulWidget {
  const BankSubmissionOneBankTaskScreen({Key? key}) : super(key: key);

  @override
  _BankSubmissionOneBankTaskScreenState createState() =>
      _BankSubmissionOneBankTaskScreenState();
}

class _BankSubmissionOneBankTaskScreenState
    extends State<BankSubmissionOneBankTaskScreen> {
  late BankSubmissionOneBank bankSubmissionOneBank;
  late int leadId;
  late String documentDrawerId;
  BankSubmissionOneBankTaskScreenController
      bankSubmissionOneBankTaskScreenController =
      Get.put(BankSubmissionOneBankTaskScreenController());
  @override
  void initState() {
    bankSubmissionOneBank = Get.arguments[0];
    leadId = Get.arguments[1];
    documentDrawerId = Get.arguments[3];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // double windowWidth = MediaQuery.of(context).size.width;
    double windowHeight = MediaQuery.of(context).size.height;

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          automaticallyImplyLeading: false,
          centerTitle: false,
          flexibleSpace: const Padding(
            padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 30),
            child: CommonAppBarOptions(
              windowWidth: 0,
              title: 'Manage Loan',
            ),
          ),
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(windowHeight * 0.03),
            child: const SizedBox(),
          ),
          backgroundColor: const Color(0xFFDF5356),
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
            ),
          ),
        ),
        body: Body(
          bankSubmissionOneBank: bankSubmissionOneBank,
          leadId: leadId,
          documentDrawerId: documentDrawerId,
        ));
  }
}

class Body extends StatelessWidget {
  BankSubmissionOneBank bankSubmissionOneBank;
  int leadId;
  String documentDrawerId;
  Body(
      {Key? key,
      required this.bankSubmissionOneBank,
      required this.leadId,
      required this.documentDrawerId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double windowWidth = MediaQuery.of(context).size.width;
    double windowHeight = MediaQuery.of(context).size.height;
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            bankSubmissionOneBank.bankDetails!.bankName!,
            style: TextStyles.bankSubmissionTitleTestStyle,
          ),
          SizedBox(
            height: windowHeight * 0.05,
          ),
          taskCard(
              title: 'Email to Lead',
              windowHeight: windowHeight,
              windowWidth: windowWidth,
              isEmailToLeadTaskDone: bankSubmissionOneBank.emailToLeasTask!,
              isEmailToBankerTaskDone: bankSubmissionOneBank.emailToBankTask!,
              isActive: bankSubmissionOneBank.emailToLeasTask!,
              emailToLeadCard: true,
              onTap: () {
                // Need to pass bankId and lead id to call the APi that will fetch the email body
                Get.offNamed(bankSubmissionEmailScreen,
                    arguments: [bankSubmissionOneBank.bankDetails!.id, leadId]);
              }),
          SizedBox(
            height: windowHeight * 0.02,
          ),
          taskCard(
              title: 'Email to Banker',
              windowHeight: windowHeight,
              windowWidth: windowWidth,
              isEmailToLeadTaskDone: bankSubmissionOneBank.emailToLeasTask!,
              isEmailToBankerTaskDone: bankSubmissionOneBank.emailToBankTask!,
              isActive: bankSubmissionOneBank.emailToBankTask!,
              emailToLeadCard: false,
              onTap: () {
                if (bankSubmissionOneBank.displayDocumentDrawer!) {
                  Get.offNamed(bankSubmissionDocumentUploadScreen, arguments: [
                    bankSubmissionOneBank.bankDetails!.id,
                    leadId,
                    documentDrawerId
                  ]);
                } else {
                  Get.offNamed(bankSubmissionEmailBankerScreen, arguments: [
                    bankSubmissionOneBank.bankDetails!.id,
                    leadId,
                    null
                  ]);
                }
              }),
          SizedBox(
            height: windowHeight * 0.05,
          ),
        ],
      ),
    );
  }

  Widget taskCard(
      {required String title,
      required double windowHeight,
      required double windowWidth,
      required bool isEmailToLeadTaskDone,
      required bool isEmailToBankerTaskDone,
      required bool isActive,
      required bool emailToLeadCard,
      Function()? onTap}) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 5),
      child: Stack(
        children: [
          InkWell(
            onTap: emailToLeadCard
                ? onTap
                : isEmailToLeadTaskDone
                    ? onTap
                    : () {
                        debugPrint('Finish previous task');
                      },
            child: Card(
              elevation: 5,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 40.0, horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        emailToLeadCard
                            ? isEmailToLeadTaskDone
                                ? SvgPicture.asset(
                                    Assets.manageLoanDone,
                                    height: 24,
                                    width: 24,
                                  )
                                : SvgPicture.asset(
                                    Assets.manageLoanIncomplete,
                                    height: 24,
                                    width: 24,
                                  )
                            : isEmailToBankerTaskDone
                                ? SvgPicture.asset(
                                    Assets.manageLoanDone,
                                    height: 24,
                                    width: 24,
                                  )
                                : SvgPicture.asset(
                                    Assets.manageLoanIncomplete,
                                    height: 24,
                                    width: 24,
                                  ),
                        SizedBox(
                          width: windowWidth * 0.02,
                        ),
                        Text(
                          title,
                          style: emailToLeadCard
                              ? TextStyles.bankSubmissionCardTitle
                              : isEmailToLeadTaskDone
                                  ? TextStyles.bankSubmissionCardTitle
                                  : TextStyles.bankSubmissionCardTitleDisabled,
                        ),
                      ],
                    ),
                    Icon(
                      Icons.chevron_right,
                      color: emailToLeadCard
                          ? Colors.black
                          : isEmailToLeadTaskDone
                              ? Colors.black
                              : Colors.grey,
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
