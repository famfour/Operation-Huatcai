import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/dashboard_view_controller2.dart';
import 'package:iqrate/Controller/shared_prefs_controller.dart';
import 'package:iqrate/DeviceManager/assets.dart';
import 'package:iqrate/DeviceManager/screen_constants.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Widgets/primary_button.dart';

class DashboardView2 extends StatelessWidget {
  const DashboardView2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    SharedPrefController sharedPrefController = Get.find();
    final DashboardController2 dashboardController2 = Get.find();
    debugPrint(sharedPrefController.prefs.getInt("counter").toString());
    if (sharedPrefController.prefs.getInt('counter') == 1) {
      Future.delayed(Duration.zero, () => showAlert(context));
      sharedPrefController.prefs.setInt("counter", 0);
    }
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.only(top: windowHeight * .015),
        child: ListView.builder(
            itemCount: dashboardController2.dashboardList.length,
            itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                  onTap: () {
                    dashboardController2.onTapItem(); //ontap list item
                  },
                  child: itemDashboard(windowHeight, windowWidth));
            }),
      ),
    );
  }

  //list item view
  Widget itemDashboard(windowHeight, windowWidth) {
    return Padding(
      padding: EdgeInsets.symmetric(
          vertical: windowHeight * .015, horizontal: windowWidth * .05),
      child: Card(
        elevation: 5,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        child: SizedBox(
          height: windowHeight * 0.1268,
          width: windowWidth * 0.87,
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Row(
              children: [
                GestureDetector(
                  onTap: () {},
                  child: Image.asset(
                    Assets.dashboardDollarItem,
                    height: 90,
                  ),
                ),
                SizedBox(width: windowWidth * 0.05),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Text(
                        'Total Amount Paid out',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: windowHeight * 0.022,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                    // SizedBox(height: windowHeight * 0.01),
                    Expanded(
                      child: Text(
                        '0',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: windowHeight * 0.022,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void showAlert(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) => Padding(
        padding: const EdgeInsets.symmetric(vertical: 50, horizontal: 40),
        child: Container(
          height: Get.height * 0.8,
          width: Get.width * 0.8,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: Get.height * 0.25,
                color: const Color(0XFFFFF6E7),
              ),
              SizedBox(height: Get.height * 0.05),
              SizedBox(
                child: Center(
                  child: Text(
                    "Upgrade your subscription to get these features and more!",
                    textAlign: TextAlign.center,
                    style:
                        TextStyle(color: Colors.black, fontSize: FontSize.s22),
                  ),
                ),
              ),
              SizedBox(height: Get.height * 0.07),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 50.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "• Exclusive Loan Packages",
                      style: TextStyles.dashboardPopUpTextStyle,
                    ),
                    SizedBox(height: Get.height * 0.03),
                    Text(
                      "• Higher Payput",
                      style: TextStyles.dashboardPopUpTextStyle,
                    ),
                    SizedBox(height: Get.height * 0.03),
                    Text("• Profile Analysis Calculators",
                        style: TextStyles.dashboardPopUpTextStyle),
                    SizedBox(height: Get.height * 0.03),
                    Text(
                      "• Powerful CRM platform to manage leads",
                      style: TextStyles.dashboardPopUpTextStyle,
                    ),
                    SizedBox(height: Get.height * 0.03),
                  ],
                ),
              ),
              SizedBox(height: Get.height * 0.03),
              Center(
                child: PrimaryButton(
                  windowHeight: Get.height,
                  windowWidth: Get.width,
                  buttonTitle: "Explore Subscription",
                  onPressed: () {},
                ),
              ),
              SizedBox(height: Get.height * 0.03),
              Center(
                child: TextButton(
                  onPressed: () {},
                  child: Text(
                    "Or read our FAQ here",
                    style: TextStyle(
                      color: const Color(0XFFDA3B3E),
                      fontSize: FontSize.s22,
                      fontWeight: FontWeight.w400,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
