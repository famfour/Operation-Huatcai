import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/resource_screen_controller.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Widgets/default_appbar.dart';

// ignore: must_be_immutable
class ResourcesView extends StatelessWidget {
  ResourcesView({Key? key}) : super(key: key);

  ResourceScreenController resourceScreenController =
      Get.put(ResourceScreenController());

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: DefaultAppBar(
        title: "Resources",
        windowHeight: windowHeight * 0.09,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 25.0, horizontal: 20),
        child: Column(
          children: [
            // ResourcesCard(
            //   onPressed: () {},
            //   title: "Glossary",
            // ),
            ResourcesCard(
              onPressed: () {
                Get.toNamed(importantLinksScreen);
              },
              title: "Important Links",
            ),
            ResourcesCard(
              onPressed: () {
                resourceScreenController.checkBankFormsNavigation();
              },
              title: "Bank Forms",
            ),
            ResourcesCard(
              onPressed: () {
                resourceScreenController.checkLawfirmNavigation();
              },
              title: "Law Firm Listing",
            ),
            ResourcesCard(
              onPressed: () {
                Get.toNamed(othersScreen);
              },
              title: "Others",
            ),
          ],
        ),
      ),
    );
  }
}

class ResourcesCard extends StatelessWidget {
  const ResourcesCard({
    Key? key,
    required this.onPressed,
    required this.title,
  }) : super(key: key);

  final Function onPressed;
  final String title;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        // Navigate to settings page
        onPressed();
      },
      child: Column(
        children: [
          Card(
            elevation: 2,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5.0),
              child: Padding(
                padding: const EdgeInsets.symmetric(
                    horizontal: 15.0, vertical: 17.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      title,
                      style: TextStyles.profileexpansionTitle,
                    ),
                    const Icon(
                      Icons.chevron_right,
                      color: AppColors.grey625F5A,
                    )
                  ],
                ),
              ),
            ),
          ),
          SizedBox(height: Get.height * 0.02),
        ],
      ),
    );
  }
}
