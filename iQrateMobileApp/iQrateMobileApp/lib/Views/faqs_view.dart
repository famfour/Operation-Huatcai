// ignore_for_file: unused_local_variable, must_be_immutable, duplicate_ignore

import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/instance_manager.dart';
import 'package:iqrate/Controller/faqs_view_controller.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Widgets/default_appbar.dart';
import 'package:iqrate/Widgets/require_text_field.dart';

// ignore: must_be_immutable
class FaqView extends StatefulWidget {
  const FaqView({Key? key}) : super(key: key);

  @override
  State<FaqView> createState() => _FaqViewState();
}

class _FaqViewState extends State<FaqView> {
  FaqsController faqsController = Get.put(FaqsController());

  int selected = -1;

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: DefaultAppBar(
        title: 'FAQ',
        windowHeight: windowHeight * 0.09,
      ),
      body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 0),
          child: ListView(
            children: [
              SizedBox(
                height: windowHeight * 0.01,
              ),
              RequireTextField(
                type: Type.search,
                controller: faqsController.searchTextEditingController,
                onSubmit: (value) {
                  faqsController.searchFaq(value);
                },
                onSubmitSearch: () {
                  debugPrint("search on submit:::");
                  faqsController.searchFaq(
                      faqsController.searchTextEditingController.text);
                },
              ),
              SizedBox(
                height: windowHeight * 0.02,
              ),
              Obx(
                () => faqsController.faqResponseModel.value.data != null
                    ? ListView.builder(
                        key: Key('builder ${selected.toString()}'),
                        //attention
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemCount:
                            faqsController.faqResponseModel.value.data!.length,
                        itemBuilder: (BuildContext context, int index) {
                          var item = faqsController
                              .faqResponseModel.value.data![index];
                          return Column(
                            children: [
                              /*ExpansionSection(
                                  title: faqsController.faqResponseModel.value
                                      .data![index].title!,
                                  index: index,
                                  body: [
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(bottom: 15.0),
                                      child: Column(
                                        children: [
                                          const Padding(
                                            padding:
                                                EdgeInsets.only(bottom: 8.0),
                                            child: Divider(
                                              color: Colors.grey,
                                            ),
                                          ),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                              faqsController
                                                  .faqResponseModel
                                                  .value
                                                  .data![index]
                                                  .description!,
                                              style: TextStyles
                                                  .faqDescriptionTextStyle,
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  ]),*/
                              Card(
                                elevation: 2,
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 5.0),
                                  child: ExpansionTile(
                                    key: Key(index.toString()),
                                    //attention
                                    initiallyExpanded: index == selected,
                                    //attention
                                    iconColor: AppColors.grey625F5A,
                                    collapsedIconColor: AppColors.grey625F5A,
                                    title: Text(
                                      item.title!,
                                      style: TextStyles.profileexpansionTitle,
                                    ),
                                    childrenPadding: const EdgeInsets.symmetric(
                                        horizontal: 20.0),
                                    children: [
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 15.0),
                                        child: Column(
                                          children: [
                                            const Padding(
                                              padding:
                                                  EdgeInsets.only(bottom: 8.0),
                                              child: Divider(
                                                color: Colors.grey,
                                              ),
                                            ),
                                            Align(
                                              alignment: Alignment.centerLeft,
                                              child: Text(
                                                faqsController
                                                    .faqResponseModel
                                                    .value
                                                    .data![index]
                                                    .description!,
                                                style: TextStyles
                                                    .onlyFaqDescriptionTextStyle,
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                    onExpansionChanged: ((newState) {
                                      if (newState) {
                                        setState(() {
                                          const Duration(seconds: 20000);
                                          selected = index;
                                        });
                                      } else {
                                        setState(() {
                                          selected = -1;
                                        });
                                      }
                                    }),
                                  ),
                                ),
                              ),
                              SizedBox(height: windowHeight * 0.02),
                            ],
                          );
                        })
                    : Container(),
              ),
            ],
          )),
    );
  }
}

// A widget that expands on tap and shows the necessary deilds and button to update the data
class ExpansionSection extends StatefulWidget {
  ExpansionSection(
      {Key? key,
      required this.title,
      required this.body,
      this.initiallyExpanded = false,
      this.index = 0})
      : super(key: key);
  final String title;
  final List<Widget> body;
  bool initiallyExpanded;
  int index;

  @override
  State<ExpansionSection> createState() => _ExpansionSectionState();
}

class _ExpansionSectionState extends State<ExpansionSection> {
  int selected = 0;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 5.0),
        child: ExpansionTile(
          key: Key(widget.index.toString()),
          //attention
          initiallyExpanded: widget.index == selected,
          //attention
          iconColor: AppColors.grey625F5A,
          collapsedIconColor: AppColors.grey625F5A,
          title: Text(
            widget.title,
            style: TextStyles.profileexpansionTitle,
          ),
          childrenPadding: const EdgeInsets.symmetric(horizontal: 20.0),
          children: widget.body,
          onExpansionChanged: ((newState) {
            if (newState) {
              setState(() {
                const Duration(seconds: 20000);
                selected = widget.index;
              });
            } else {
              setState(() {
                selected = -1;
              });
            }
          }),
        ),
      ),
    );
  }
}
