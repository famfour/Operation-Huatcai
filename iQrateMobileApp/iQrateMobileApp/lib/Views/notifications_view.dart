import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Widgets/default_appbar.dart';

class NotificationsView extends StatelessWidget {
  const NotificationsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Color(0xFFDF5356), // status bar color
    ));
    return SafeArea(
      child: Scaffold(
        appBar: DefaultAppBar(
          // A default AppBar reusable widget is used with a title
          title: 'Notifications',
          windowHeight: windowHeight * 0.06,
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ListView.builder(
              // List of notifications
              itemCount: 5, // Number of notification cards
              itemBuilder: (BuildContext context, int index) {
                return NotificationCard(
                    // Individual notification card
                    windowHeight: windowHeight,
                    windowWidth: windowWidth,
                    data:
                        'Your co-broke case with Justin Beetle has been marked as won.');
              }),
        ),
      ),
    );
  }
}

// Notification card widget used inside a listview builder
// ignore: must_be_immutable
class NotificationCard extends StatelessWidget {
  NotificationCard(
      {Key? key,
      required this.data,
      required this.windowHeight,
      required this.windowWidth})
      : super(key: key);
  final String data;
  double windowWidth;
  double windowHeight;

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      child: Padding(
        padding: EdgeInsets.all(windowWidth * 0.09),
        child: Text(
          data, // The data that will be displayed in the notification card
          style: TextStyles
              .notificationBody, // textStyle of the text inside the card
        ),
      ),
    );
  }
}
