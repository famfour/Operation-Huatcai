import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/generate_packages_controller.dart';
import 'package:iqrate/Controller/rates_view_controller.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Utils/config.dart';
import 'package:iqrate/Views/filter_more_rates_section.dart';
import 'package:iqrate/Views/generate_packages.dart';
import 'package:iqrate/Widgets/common_appbar_options.dart';
import 'package:iqrate/Widgets/primary_button.dart';

class MoreRatesView extends StatelessWidget {
  const MoreRatesView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // List of loan packages cards
    return const CardsList();
  }
}

class CardsList extends StatefulWidget {
  const CardsList({Key? key}) : super(key: key);

  @override
  State<CardsList> createState() => _CardsListState();
}

class _CardsListState extends State<CardsList> {
  late double windowHeight;
  late double windowWidth;

  final RatesViewController ratesViewController = Get.find();
  final GeneratePackagesController generatePackagesController = Get.find();
  String leadId = Get.arguments[0];

  @override
  void initState() {
    generatePackagesController.leadID = leadId;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.width;

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          automaticallyImplyLeading: false,
          centerTitle: false,
          flexibleSpace: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                const Padding(
                  padding: EdgeInsets.symmetric(vertical: 5.0),
                  child: CommonAppBarOptions(
                    windowWidth: 0,
                    title: 'More Rates',
                  ),
                ),
                Divider(
                  color: Colors.white,
                  thickness: windowWidth * 0.002,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    SizedBox(width: windowWidth * 0.025),
                    GestureDetector(
                      onTap: () {
                        //this is export icon???
                        debugPrint("export icon rate view clicked, show popup");
                        generatePackagesController
                            .showFilterConditionalVariable.value = 2;
                      },
                      child: SvgPicture.asset('assets/icons/share.svg'),
                    ),
                    SizedBox(width: windowWidth * 0.025),
                    GestureDetector(
                      onTap: () {
                        debugPrint("filter icon rate view clicked show Export");
                        //reset filter after click
                        generatePackagesController
                            .showFilterConditionalVariable.value = 1;
                      },
                      child: Icon(
                        Icons.filter_list_alt,
                        size: windowWidth * 0.06,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(windowHeight * 0.06),
            child: const SizedBox(),
          ),
          backgroundColor: const Color(0xFFDF5356),
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
            ),
          ),
        ),
        body: Obx(() => ratesViewController.moreRates.isNotEmpty
            ? Stack(
                children: [
                  ListView(
                    children: [
                      ListView.builder(
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: ratesViewController.moreRates.length,
                          // This count needs to be dynamic
                          itemBuilder: (BuildContext context, int index) {
                            var item =
                                ratesViewController.moreRates.elementAt(index);

                            bool isSelected = generatePackagesController.selectedPackages(item.id!);

                            item.isChecked = isSelected;

                            return LoanPackageCardFixed(
                              data: item,
                              isFromMoreRate: 3,
                              image: item.bank!.logo == null
                                  ? AppConfig.blankImage
                                  : item.bank!.logo!,
                              headingSubText:
                                  '${item.rateType.toString().replaceAll('_', ' ').capitalize} (${item.rateCategory.toString().replaceAll('_', ' ').capitalize})',
                              headingText: item.bank!.name!,
                              onChanged: (val) {
                                if (val == true) {
                                  /*if (generatePackagesController
                                          .selectedPackgesForGenerateckages
                                          .length >=
                                      3) {
                                    Get.snackbar(StringUtils.attention,
                                        "Maximum 3 packages can be selected",
                                        backgroundColor: Colors.white,
                                        colorText: Colors.black);
                                  } else {*/
                                  setState(() {
                                    item.isChecked = val!;
                                  });
                                  generatePackagesController
                                      .selectedPackgesForGenerateckages
                                      .add(item.id!);
                                  //}
                                } else {
                                  setState(() {
                                    item.isChecked = val!;
                                  });
                                  generatePackagesController
                                      .selectedPackgesForGenerateckages
                                      .removeWhere(
                                          ((element) => element == item.id));
                                }
                              },
                              checkBoxValue: item.isChecked,
                              cardIndex: index,
                              isCheckboxShow: true,
                            );
                          }),
                      Center(
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: PrimaryButton(
                            windowHeight: windowHeight,
                            windowWidth: windowWidth,
                            buttonTitle: "Proceed",
                            onPressed: () {
                              // Print the selected list
                              // debugPrint()
                              generatePackagesController
                                  .generatePackagesFromMorePackagesScreen(
                                      leadId);
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                  popUps()
                ],
              )
            : Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    popUps(),
                    SizedBox(
                        height: windowHeight * 0.15,
                        child: SvgPicture.asset('assets/images/nodata.svg')),
                    Text(
                      'No data to show',
                      style: TextStyles.bankSubmissionCardTitleDisabled,
                    ),
                  ],
                ),
              )));
  }

  popUps() {
    switch (generatePackagesController.showFilterConditionalVariable.value) {
      case 1:
        {
          return FilterMoreRateSection(
            windowHeight: windowHeight,
            windowWidth: windowWidth,
            resetFilterClicked: () {
              // ratesViewController.showFilter.value = 1;
              // ratesViewController.showFilter.refresh();
              debugPrint(
                  "ratesViewController.showFilter ${generatePackagesController.showFilterConditionalVariable}");
              Future.delayed(const Duration(milliseconds: 200), () {
                setState(() {
                  generatePackagesController
                      .showFilterConditionalVariable.value = 1;
                });

                ratesViewController.getMoreRates(leadId);
              });
            },
          );
        }
      case 2:
        {
          return ExportPDFSection(
              windowHeight: windowHeight, windowWidth: windowWidth);
        }
      case 3:
        {
          return Container(
            color: AppColors.white,
            width: windowWidth,
            height: windowHeight,
            child: const Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
      default:
        {
          return const Offstage();
        }
    }
  }
}

// ignore: must_be_immutable
class ExportPDFSection extends StatelessWidget {
  ExportPDFSection(
      {Key? key, required this.windowHeight, required this.windowWidth})
      : super(key: key);
  // final LeadsViewController leadsViewController = Get.find();
  // final RatesViewController ratesViewController =
  //     GetControllers.shared.getRatesViewController();
  final GeneratePackagesController generatePackagesController = Get.find();

  double windowHeight;
  double windowWidth;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black38,
      height: windowHeight,
      child: SingleChildScrollView(
        child: Column(
          // mainAxisSize: MainAxisSize.min,
          children: [
            topBar(),
            body(),
          ],
        ),
      ),
    );
  }

  Widget topBar() {
    return Container(
      color: Colors.white,
      child: Row(
        children: [
          Expanded(
            flex: 5,
            child: Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 20),
                child: Text(
                  "Export Selected Loan Packages to PDF",
                  style: TextStyles.headingSubtitleStyle3,
                ),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: InkWell(
                onTap: () {
                  generatePackagesController
                      .showFilterConditionalVariable.value = 0;
                },
                child: SvgPicture.asset(
                  'assets/icons/closeEnclosed.svg',
                  color: Colors.red,
                )),
          )
        ],
      ),
    );
  }

  Widget body() {
    return Container(
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 50),
          Center(
            child: PrimaryButton(
                windowHeight: windowHeight,
                windowWidth: windowWidth * 0.85,
                buttonTitle: 'Export',
                onPressed: () {
                  debugPrint("Export clicked ");
                  generatePackagesController.callAPIExportPackage();
                  // ratesViewController.callAPIGetBankListWithFilter();
                }),
          ),
          const SizedBox(height: 50),
          // Container(color: Colors.black38,height: 100,width: windowWidth,)
        ],
      ),
    );
  }
}
