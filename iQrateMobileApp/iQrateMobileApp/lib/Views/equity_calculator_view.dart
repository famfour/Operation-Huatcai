import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/equity_calculator_controller.dart';
import 'package:iqrate/DeviceManager/screen_constants.dart';
import 'package:iqrate/Views/rates/currency_amount_textField.dart';
import 'package:iqrate/Widgets/default_appbar.dart';
import 'package:iqrate/Widgets/form_field_title.dart';
import 'package:iqrate/Widgets/primary_button.dart';

import '../DeviceManager/colors.dart';
import '../DeviceManager/text_styles.dart';

class EquityCalculatorView extends StatefulWidget {
  const EquityCalculatorView({Key? key}) : super(key: key);

  @override
  State<EquityCalculatorView> createState() => _EquityCalculatorViewState();
}

class _EquityCalculatorViewState extends State<EquityCalculatorView> {
  final EquityCalculatorController equityCalculatorController =
      Get.put(EquityCalculatorController());

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: DefaultAppBar(
          title: "Equity Loan", windowHeight: windowHeight * 0.09),
      body: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: windowHeight * 0.03,
                ),
                Text('Equity Loan',
                    style: TextStyles.calculatorHeadingsTextStyle),
                SizedBox(
                  height: windowHeight * 0.03,
                ),
                const FormFieldTitle(
                  title: 'Total Number of Existing Housing Loan*',
                ),
                SizedBox(
                  height: windowHeight * 0.01,
                ),
                Container(
                  width: windowWidth,
                  padding:
                      const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: AppColors.formFieldBorderColor, width: 1.0),
                      borderRadius: const BorderRadius.all(Radius.circular(
                              10) //                 <--- border radius here
                          )),
                  child: DropdownButtonHideUnderline(
                    child: Obx(
                      () => DropdownButton(
                        hint: const Text('0'),
                        // Not necessary for Option 1
                        value: equityCalculatorController
                            .numberOfHousingLoan.value,
                        onChanged: (newValue) {
                          debugPrint(newValue.toString());
                          setState(() {
                            equityCalculatorController.numberOfHousingLoan
                                .value = int.tryParse(newValue.toString())!;
                          });
                        },
                        items: const [
                          DropdownMenuItem(child: Text('0'), value: 0),
                          DropdownMenuItem(child: Text('1'), value: 1),
                          DropdownMenuItem(child: Text('2 or more'), value: 2),
                        ],
                      ),
                    ),
                  ),
                ),

                SizedBox(
                  height: windowHeight * 0.02,
                ),

                Obx(() =>
                    equityCalculatorController.numberOfHousingLoan.value == 1
                        ? Column(
                            children: [
                              const FormFieldTitle(
                                  title:
                                      'Is the housing loan tagged to the property that you plan to take equity loan?'),
                              SizedBox(height: windowHeight * 0.01),
                              Container(
                                width: windowWidth,
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 15, vertical: 3),
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: AppColors.formFieldBorderColor,
                                        width: 1.0),
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(
                                            10) //                 <--- border radius here
                                        )),
                                child: DropdownButtonHideUnderline(
                                  child: DropdownButton<String>(
                                    hint: const Text('Select'),
                                    // Not necessary for Option 1
                                    value: equityCalculatorController
                                        .selectedPropertyPlan,
                                    onChanged: (newValue) {
                                      equityCalculatorController
                                          .selectedPropertyPlan = newValue!;
                                      setState(() {});
                                    },
                                    items: equityCalculatorController
                                        .dropdownLongerLoanTenure
                                        .map((location) {
                                      return DropdownMenuItem(
                                        child: SizedBox(
                                            width: windowWidth * 0.7,
                                            child: Text(location)),
                                        value: location,
                                      );
                                    }).toList(),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: windowHeight * 0.02,
                              ),
                            ],
                          )
                        : const SizedBox()),

                Text(
                  'Details Of Subject Property (For Equity Loan)',
                  style: TextStyle(
                    color: const Color(0XFF767676),
                    fontSize: FontSize.s14,
                    fontWeight: FontWeight.w600,
                  ),
                ),

                SizedBox(
                  height: windowHeight * 0.01,
                ),

                const FormFieldTitle(title: 'Current Valuation*'),
                SizedBox(
                  height: windowHeight * 0.01,
                ),
                CurrencyAmountTextField(
                  textEditingController:
                      equityCalculatorController.currentValuationController,
                  labelText: "",
                ),
                // RequireTextField(
                //     type: Type.number,
                //     controller:
                //         equityCalculatorController.currentValuationController),
                SizedBox(
                  height: windowHeight * 0.01,
                ),
                const FormFieldTitle(title: 'Total CPF Used (If any)'),
                SizedBox(
                  height: windowHeight * 0.01,
                ),
                CurrencyAmountTextField(
                  textEditingController:
                      equityCalculatorController.totalCPFController,
                  labelText: "",
                ),
                // RequireTextField(
                //     type: Type.number,
                //     controller: equityCalculatorController.totalCPFController),
                SizedBox(
                  height: windowHeight * 0.01,
                ),
                const FormFieldTitle(
                    title: 'Current Outstanding Loan (If Any)'),
                SizedBox(
                  height: windowHeight * 0.01,
                ),
                CurrencyAmountTextField(
                  textEditingController: equityCalculatorController
                      .currentOutStandingLoanController,
                  labelText: "",
                ),
                // RequireTextField(
                //     type: Type.number,
                //     controller: equityCalculatorController
                //         .currentOutStandingLoanController),
                SizedBox(
                  height: windowHeight * 0.03,
                ),
                Container(
                  width: windowWidth,
                  height: windowHeight * 0.15,
                  decoration: BoxDecoration(
                    color: const Color(0XFFF6F6F6),
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: windowHeight * 0.02,
                        ),
                        Text("Maximum Equity Loan",
                            style: TextStyles.calculatorHeadingsTextStyle),
                        const Divider(
                          color: Color(0XFFBEBEBE),
                          thickness: 2,
                        ),
                        SizedBox(
                          height: windowHeight * 0.02,
                        ),
                        equityCalculatorController.maximumEquityLoan != null
                            ? Obx(() => Text(
                                  "\$" +
                                      equityCalculatorController
                                          .maximumEquityLoan
                                          .toString(),
                                  style: TextStyles.calculatorResultTextStyle,
                                ))
                            : Text('\$0',
                                style: TextStyles.calculatorResultTextStyle),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: windowHeight * 0.01,
                ),

            SizedBox(
              child: Obx(() => FormFieldTitle(
                      title: equityCalculatorController.calculated.value == 1 ? 'Disclaimer : Please check with respective banks for their loan-to-value for equity loan' : '')),
            ),

                SizedBox(
                  height: windowHeight * 0.05,
                ),
                PrimaryButton(
                  windowHeight: windowHeight,
                  windowWidth: windowWidth,
                  buttonTitle:
                      // equityCalculatorController.calculated.value == 0
                      //     ?
                      "Calculate",
                  // : "Reset",
                  onPressed: () {
                    equityCalculatorController.onPressCalculate();
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
