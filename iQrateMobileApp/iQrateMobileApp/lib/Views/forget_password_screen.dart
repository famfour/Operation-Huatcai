import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/forget_password_controller.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Widgets/form_field_title.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';

class ForgetPasswordScreen extends StatelessWidget {
  final ForgetPasswordController forgetController = Get.put(
      ForgetPasswordController()); //initialize forget password controller
  ForgetPasswordScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double windowWidth = MediaQuery.of(context).size.width; //get screen width
    double windowHeight =
        MediaQuery.of(context).size.height; //get screen height
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        automaticallyImplyLeading: false,
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 8.0, top: 20),
            child: InkWell(
              onTap: () {
                Get.back(); // Pop the existing page and go back to previous page
              },
              child: SvgPicture.asset(
                'assets/icons/closeEnclosed.svg',
                color: Colors.black,
              ),
            ),
          )
        ],
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: windowHeight * 0.10),
                Center(
                  child: SizedBox(
                    child: SvgPicture.asset(
                      'assets/images/forget.svg',
                      fit: BoxFit.contain,
                      // semanticsLabel: 'Logo',
                    ),
                  ),
                ),
                SizedBox(height: windowHeight * 0.07),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Forgot Password",
                      style: TextStyles.authTextTitleStyle,
                    ),
                    SizedBox(height: windowHeight * 0.02),
                    Text(
                      "Enter your registered email address below to receive reset instructions",
                      style: TextStyles.forgetInstructionTitleTextStyle,
                    ),
                    // SizedBox(height: windowHeight * 0.01),
                    // Text(
                    //   "",
                    //   style: TextStyles.forgetInstructionTitleTextStyle,
                    // ),
                    SizedBox(height: windowHeight * 0.03),
                    const FormFieldTitle(
                        title: 'Enter your registered email address'),
                    SizedBox(height: windowHeight * 0.01),
                    RequireTextField(
                      type: Type.email,
                      controller: forgetController.emailTextController,
                      labelText: "Email Address",
                    ),
                    SizedBox(height: windowHeight * 0.05),
                    Center(
                      child: Obx(() => PrimaryButton(
                            windowHeight: windowHeight,
                            windowWidth: windowWidth,
                            buttonTitle: forgetController.resendCount.value == 0
                                ? "Submit"
                                : "Resend",
                            onPressed: () {
                              // Get.toNamed(emailAddressVerificationScreen); //navigate to email address verification screen
                              forgetController.onSubmitButtonTap();
                            },
                          )),
                    ),
                    SizedBox(height: windowHeight * 0.12),
                    // NewToiQrate(
                    //   tapGestureRecognizerToLogin: TapGestureRecognizer()
                    //     ..onTap = () {
                    //       debugPrint("clicked signup for free");
                    //       Get.toNamed(signUp); //navigate to signup screen
                    //     },
                    // ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
