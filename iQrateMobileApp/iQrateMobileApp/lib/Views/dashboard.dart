import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/acl_controller.dart';
import 'package:iqrate/Controller/dashboard_controller.dart';
import 'package:iqrate/Controller/dashboard_view_controller2.dart';
import 'package:iqrate/Controller/leads_view_controller.dart';
import 'package:iqrate/Controller/rates_view_controller.dart';
import 'package:iqrate/Views/calculators_view.dart';
import 'package:iqrate/Views/dashboard_view2.dart';
import 'package:iqrate/Views/leads_view.dart';
import 'package:iqrate/Views/more_view.dart';
import 'package:iqrate/Views/rates_view.dart';
import 'package:iqrate/Widgets/calculators_appbar_options.dart';
import 'package:iqrate/Widgets/lead_list_appbar_options.dart';
import 'package:iqrate/Widgets/dashbaord_appbar_options2.dart';
import 'package:iqrate/Widgets/dashboard_appbar_options3.dart';
import 'package:iqrate/Widgets/more_appbar_options.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  final DashboardController dashboardController =
      Get.put(DashboardController());
  final DashboardController2 dashboardController2 =
      Get.put(DashboardController2());
  final LeadsViewController leadsViewController =
      Get.put(LeadsViewController());
  final ACLController aclController = Get.put(ACLController());
  final RatesViewController ratesViewController =
      Get.put(RatesViewController());

  @override
  Widget build(BuildContext context) {
    double windowWidth = MediaQuery.of(context).size.width;
    double windowHeight = MediaQuery.of(context).size.height;
    return Obx(() => Scaffold(
          // extendBody: true,
          // extendBodyBehindAppBar: true,
          // backgroundColor: leadsViewController.selectedIndex.toInt() == 5
          //     ? Colors.white
          //     : dashboardController.selectedIndex.toInt() == 1
          //         ? AppColors.leadBG
          //         : Colors.white,
          appBar: AppBar(
            elevation: 0,
            automaticallyImplyLeading: false,
            centerTitle: false,
            flexibleSpace: Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 20.0, vertical: 20),
              child: dashboardController.selectedIndex.toInt() == 0
                  ? DashboardAppBarOptions2(windowWidth: windowWidth)
                  : dashboardController.selectedIndex.toInt() == 1
                      ? LeadsAppBarOptions(windowWidth: windowWidth)
                      : dashboardController.selectedIndex.toInt() == 3
                          ? CalculatorsAppBarOptions(windowWidth: windowWidth)
                          : dashboardController.selectedIndex.toInt() == 2
                              ? DashboardAppBarOptions3(
                                  //load package
                                  windowWidth: windowWidth)
                              : MoreAppbarOptions(windowWidth: windowWidth),
            ),
            bottom: PreferredSize(
              preferredSize:
                  Size.fromHeight(dashboardController.selectedIndex.toInt() == 0
                      ? windowHeight * 0.35
                      : dashboardController.selectedIndex.toInt() == 3
                          ? windowHeight * 0.03
                          : dashboardController.selectedIndex.toInt() == 4
                              ? windowHeight * 0.03
                              : windowHeight * 0.06),
              child: SizedBox(
                height: windowHeight * 0.06,
              ),
            ),
            backgroundColor: const Color(0xFFDF5356),
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(30),
              ),
            ),
          ),
          bottomNavigationBar: Obx(
            () => BottomNavigationBar(
              backgroundColor: Colors.white,
              currentIndex: dashboardController.selectedIndex.toInt(),
              onTap: dashboardController.onItemTapped,
              selectedItemColor: const Color(0XFFDF5356),
              unselectedItemColor: Colors.grey.shade400,
              type: BottomNavigationBarType.fixed,
              iconSize: 40,
              elevation: 3,
              items: [
                BottomNavigationBarItem(
                  icon: dashboardController.selectedIndex.toInt() == 0
                      ? SvgPicture.asset(
                          'assets/icons/dashboard_sel.svg',
                        )
                      : SvgPicture.asset(
                          'assets/icons/dashboard_un.svg',
                        ),
                  label: 'Dashboard',
                ),
                BottomNavigationBarItem(
                  icon: dashboardController.selectedIndex.toInt() == 1
                      ? SvgPicture.asset(
                          'assets/icons/leads_sel.svg',
                        )
                      : SvgPicture.asset(
                          'assets/icons/leads_un.svg',
                        ),
                  label: 'Leads',
                ),
                BottomNavigationBarItem(
                  icon: dashboardController.selectedIndex.toInt() == 2
                      ? SvgPicture.asset(
                          'assets/icons/rates_sel.svg',
                        )
                      : SvgPicture.asset(
                          'assets/icons/rates_un.svg',
                        ),
                  label: 'Rates',
                ),
                BottomNavigationBarItem(
                  icon: dashboardController.selectedIndex.toInt() == 3
                      ? SvgPicture.asset(
                          'assets/icons/calc_sel.svg',
                        )
                      : SvgPicture.asset(
                          'assets/icons/calc_un.svg',
                        ),
                  label: 'Calculators',
                ),
                BottomNavigationBarItem(
                  icon: dashboardController.selectedIndex.toInt() == 4
                      ? SvgPicture.asset(
                          'assets/icons/more_sel.svg',
                        )
                      : SvgPicture.asset(
                          'assets/icons/more_un.svg',
                        ),
                  label: 'More',
                ),
              ],
            ),
          ),
          body: Obx(() => _body(dashboardController.selectedIndex.toInt())),
        ));
  }

  _body(int index) {
    switch (index) {
      case 0:
        return const DashboardView2();
      case 1:
        return const LeadsView();
      case 2:
        return const RatesView();
      case 3:
        return CalculatorsView();
      case 4:
        return MoreView();
      default:
        return const Offstage();
    }
  }
}
