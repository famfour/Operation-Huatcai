import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/two_fact_auth_screen2_controller.dart';
import 'package:iqrate/DeviceManager/container_styles.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/secondary_button.dart';

class TwoFactAuthScreenTwo extends StatelessWidget {
  const TwoFactAuthScreenTwo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TwoFactAuthScreenTwoController twoFactAuthScreenTwoController =
        Get.put(TwoFactAuthScreenTwoController());
    double windowWidth = MediaQuery.of(context).size.width;
    double windowHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SafeArea(
          child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 25.0, horizontal: 18),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(
                child: SvgPicture.asset(
                  "assets/images/two_fact.svg",
                ),
              ),
              SizedBox(
                height: windowHeight * 0.15,
              ),
              Text("Two-Step-Authenticaton",
                  style: TextStyles.introScreenTitlesSmall),
              SizedBox(
                height: windowHeight * 0.03,
              ),
              Text(
                "Recommeded: Google Authenticator from Play store or App store to generate verification codes for more protection & future logins",
                style: TextStyles.introScreenDescriptions,
              ),
              SizedBox(
                height: windowHeight * 0.2,
              ),
              PrimaryButton(
                windowHeight: windowHeight,
                windowWidth: windowWidth,
                buttonTitle: "Use Authenticator App",
                onPressed: () {
                  twoFactAuthScreenTwoController.onSubmit();
                },
              ),
              SizedBox(
                height: windowHeight * 0.03,
              ),
              SecondaryButton(
                windowHeight: windowHeight,
                windowWidth: windowWidth,
                kGradientBoxDecoration:
                    ContainerStyles.kGradientBoxDecorationSecondaryButton,
                kInnerDecoration:
                    ContainerStyles.kInnerDecorationSecondaryButton,
                onPressed: () {
                  twoFactAuthScreenTwoController.onCancel();
                },
                buttonTitle: "Cancel",
              ),
            ],
          ),
        ),
      )),
    );
  }
}
