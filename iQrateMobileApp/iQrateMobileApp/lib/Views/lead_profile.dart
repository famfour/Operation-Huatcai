import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:iqrate/Controller/lead_profile_controller.dart';
import 'package:iqrate/DeviceManager/assets.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/container_styles.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Model/response_model.dart/fetch_lead_list_response_model.dart';
import 'package:iqrate/Views/rates/currency_amount_textField.dart';
import 'package:iqrate/Widgets/form_field_title.dart';
import 'package:iqrate/Widgets/info_button.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';
import 'package:iqrate/Widgets/secondary_button.dart';

import '../Service/GetControllers.dart';

class LeadProfile extends StatefulWidget {
  final Result data;

  const LeadProfile({required this.data, Key? key, clients}) : super(key: key);

  @override
  _LeadProfileState createState() => _LeadProfileState();
}

class _LeadProfileState extends State<LeadProfile> {
  late double windowHeight;
  late double windowWidth;
  final oCcy = NumberFormat("#,##0.00", "en_US");
  LeadProfileController leadProfileController =
      Get.put(LeadProfileController());

  @override
  void initState() {
    initData();
    super.initState();
  }

  late Result leadData;

  initData() {
    leadData = widget.data;
    leadProfileController.documentsDrawerID = leadData.document_drawer_id;

    leadProfileController.leadDataFromView = leadData;
    //debugPrint(">>>>>>>>" + leadData.toJson().toString());

    // Calling the function to check the done status of the lead profile
    // leadProfileController.checkTheCompletedStatus(
    //     pdpaStatus: leadData.clients![0].pdpaStatus ?? false,
    //     dob: leadData.clients![0].dob.toString(),
    //     nationality: leadData.clients![0].nationality.toString(),
    //     annualIncome: leadData.clients![0].annualIncome.toString());

    leadData.clients!.sort((a, b) => a.id!.compareTo(b.id!));

    if (leadData.clients!.isNotEmpty) {
      var clients = leadData.clients!.elementAt(0);
      leadProfileController.mainClientID = clients.id.toString();
      leadProfileController.clientPDPAStatus.value = clients.pdpaStatus!;
      leadProfileController.leadID = clients.lead!;
      leadProfileController.nameController.text = clients.name.toString();
      leadProfileController.phoneController.text =
          clients.phoneNumber.toString();
      leadProfileController.countryCodeController.text =
          "+" + clients.countryCode.toString();
      leadProfileController.emailController.text = clients.email.toString();
      leadProfileController.dobController.text = clients.dob != null
          ? clients.dob.toString().split('-').reversed.join('-')
          : "";
      leadProfileController.nationalityController.text =
          clients.nationality.toString() == 'null'
              ? "Singapore Citizens"
              : clients.nationality.toString();
      leadProfileController.annualIncomeController.text =
          clients.annualIncome == null ? "" : oCcy.format(clients.annualIncome);

      //if main applicant pdpa not verified then timer started
      /*if (!leadProfileController.clientPDPAStatus.value) {
        leadProfileController.resendSMSCodeTimer();
      }*/
      leadProfileController
          .checkIfPDPACompletedForAll(); //Calling this function here as to update the check status after entering the screen
    }

    if (leadData.clients!.length >= 2) {
      leadProfileController.isShowJointAccount.value = true;
      var clients = leadData.clients!.elementAt(1);
      leadProfileController.jointClientID1.value = clients.id.toString();
      leadProfileController.jointClient1PDPAStatus.value = clients.pdpaStatus!;
      leadProfileController.nameJointController.text = clients.name.toString();
      leadProfileController.phoneJointController.text =
          clients.phoneNumber.toString();
      leadProfileController.countryCodeJointController.text =
          "+" + clients.countryCode.toString();
      leadProfileController.emailJointController.text =
          clients.email.toString();
      leadProfileController.dobJointController.text = clients.dob != null
          ? clients.dob.toString().split('-').reversed.join('-')
          : "";
      leadProfileController.nationalityJointController.text =
          clients.nationality.toString();
      leadProfileController.annualIncomeJointController.text =
          clients.annualIncome == null ? "" : oCcy.format(clients.annualIncome);
      leadProfileController.checkIfPDPACompletedForAll();
    }

    if (leadData.clients!.length >= 3) {
      leadProfileController.isShowJointAccount2.value = true;
      var clients = leadData.clients!.elementAt(2);
      leadProfileController.jointClientID2.value = clients.id.toString();
      leadProfileController.jointClient2PDPAStatus.value = clients.pdpaStatus!;
      leadProfileController.nameJointController2.text = clients.name.toString();
      leadProfileController.phoneJointController2.text =
          clients.phoneNumber.toString();
      leadProfileController.countryCodeJointController2.text =
          "+" + clients.countryCode.toString();
      leadProfileController.emailJointController2.text =
          clients.email.toString();
      leadProfileController.dobJointController2.text = clients.dob != null
          ? clients.dob.toString().split('-').reversed.join('-')
          : "";
      leadProfileController.nationalityJointController2.text =
          clients.nationality.toString();
      leadProfileController.annualIncomeJointController2.text =
          clients.annualIncome == null ? "" : oCcy.format(clients.annualIncome);
      leadProfileController.checkIfPDPACompletedForAll();
    }

    if (leadData.clients!.length == 4) {
      leadProfileController.isShowJointAccount3.value = true;
      var clients = leadData.clients!.elementAt(3);
      leadProfileController.jointClientID3.value = clients.id.toString();
      leadProfileController.jointClient3PDPAStatus.value = clients.pdpaStatus!;
      leadProfileController.nameJointController3.text = clients.name.toString();
      leadProfileController.phoneJointController3.text =
          clients.phoneNumber.toString();
      leadProfileController.countryCodeJointController3.text =
          "+" + clients.countryCode.toString();
      leadProfileController.emailJointController3.text =
          clients.email.toString();
      leadProfileController.dobJointController3.text = clients.dob != null
          ? clients.dob.toString().split('-').reversed.join('-')
          : "";
      leadProfileController.nationalityJointController3.text =
          clients.nationality.toString();
      leadProfileController.annualIncomeJointController3.text =
          clients.annualIncome == null ? "" : oCcy.format(clients.annualIncome);
      leadProfileController.checkIfPDPACompletedForAll();
    }
  }

  @override
  Widget build(BuildContext context) {
    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.width;
    return expandableCard(
        "Lead Profile", leadProfileController.clientPDPAStatus.value);
  }

  Widget leadCreateView() {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(windowHeight * 0.025),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: windowHeight * 0.01),
          const FormFieldTitle(title: 'Lead Full Name*'),
          SizedBox(height: windowHeight * 0.01),
          RequireTextField(
            type: Type.fullname,
            labelText: "Lead full name",
            controller: leadProfileController.nameController,
            readOnly: !widget.data.canEdit!,
            //Make filed readonly if can Edit is false
            key: const Key("fullname"),
          ),
          SizedBox(height: windowHeight * 0.03),
          const FormFieldTitle(title: 'Lead Mobile No.*'),
          SizedBox(height: windowHeight * 0.01),
          RequireTextField(
            onChanged: (value) {
              if (value == leadProfileController.phoneController.text) {
                return;
              } else {
                setState(() {
                  leadProfileController.clientPDPAStatus.value = false;
                });
              }
            },
            type: Type.dropdownCountryCodeWithPhone,
            countryCodeController: leadProfileController.countryCodeController,
            controller: leadProfileController.phoneController,
            labelText: "Lead Mobile No.",
            readOnly: widget.data.clients![0].pdpaStatus ??
                false || !widget.data.canEdit!,
            //Make filed readonly if can Edit is false
            key: const Key("phone"),
          ),
          SizedBox(height: windowHeight * 0.01),
          const FormFieldTitle(title: 'Lead Email Address*'),
          SizedBox(height: windowHeight * 0.01),
          RequireTextField(
            type: Type.email,
            controller: leadProfileController.emailController,
            labelText: "Lead Email address",
            readOnly: !widget.data.canEdit!,
            //Make filed readonly if can Edit is false
            key: const Key("email"),
          ),
          SizedBox(height: windowHeight * 0.03),
          Obx(
            () => InfoButton(
              windowHeight: windowHeight,
              windowWidth: windowWidth,
              buttonTitle: leadProfileController.clientPDPAStatus.value
                  ? "PDPA Approval Complete"
                  : "Send For PDPA Approval ",
              backgroundColor: leadProfileController.clientPDPAStatus.value
                  ? AppColors.lightTextGreen
                  : leadProfileController.isResendButtonDisable.value
                      ? Colors.black26
                      : AppColors.red,
              icon: Icons.info_outline_rounded,
              iconDisable: leadProfileController.clientPDPAStatus.value,
              onPressed: () {
                if (leadProfileController.clientPDPAStatus.value) {
                  return;
                }

                leadProfileController
                    .onTapSendPDPAApproval(leadProfileController.mainClientID);
              },
            ),
          ),
          SizedBox(height: windowHeight * 0.01),
          Row(children: [
            leadProfileController.clientPDPAStatus.value == false
                ? InkWell(
                    onTap: () {
                      leadProfileController
                          .onTapPDPARefresh(leadProfileController.mainClientID);
                    },
                    child: Text(
                      "Refresh PDPA Status",
                      style: TextStyles.pdpaSendtitleStyle,
                    ),
                  )
                : const SizedBox(),
            const Spacer(),
            Obx(() => !leadProfileController.isResendButtonHide.value &&
                    leadProfileController.countTimes != 3 &&
                    !leadProfileController.clientPDPAStatus.value
                ? Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        "Resend in ",
                        style: TextStyles.headingSubtitleStyle,
                      ),
                      Text(
                        leadProfileController.countDownSecond.value.toString(),
                        style: TextStyles.headingSubtitleStyle,
                      ),
                      Text(
                        " seconds",
                        style: TextStyles.headingSubtitleStyle,
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      const SizedBox(
                          height: 15,
                          width: 15,
                          child: CircularProgressIndicator(
                            color: Colors.deepOrange,
                            strokeWidth: 2,
                          )),
                      const SizedBox(
                        width: 25,
                      ),
                    ],
                  )
                : Container()),
          ]),
          Obx(() => leadProfileController.clientPDPAStatus.value
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: windowHeight * 0.03),
                    const FormFieldTitle(title: 'Date of Birth*'),
                    SizedBox(height: windowHeight * 0.01),
                    RequireTextField(
                      type: Type.dob,
                      controller: leadProfileController.dobController,
                      labelText: "DD/MM/YY",
                      key: const Key("dob"),
                      readOnly: !widget.data.canEdit!,
                      //Make filed readonly if can Edit is false
                      dateFormat: "dd-MM-yyyy",
                    ),
                    SizedBox(height: windowHeight * 0.03),
                    const FormFieldTitle(title: 'Nationality*'),
                    SizedBox(height: windowHeight * 0.01),
                    RequireTextField(
                      type: Type.dropdownForLeadCountry,
                      controller: leadProfileController.nationalityController,
                      labelText: "Country",
                      readOnly: !widget.data.canEdit!,
                      //Make filed readonly if can Edit is false
                      key: const Key("Nationality"),
                    ),
                    SizedBox(height: windowHeight * 0.03),
                    const FormFieldTitle(
                        title:
                            'Annual Income (Based on Latest IRAS Tax Assessment)*'),
                    SizedBox(height: windowHeight * 0.01),
                    RequireTextField(
                        type: Type.money,
                        controller:
                            leadProfileController.annualIncomeController,
                        labelText: "\$ Annual Income",
                        readOnly: !widget.data
                            .canEdit!, //Make filed readonly if can Edit is false
                        key: const Key("Income")),
                  ],
                )
              : const SizedBox()),
          // RequireTextField(
          //   type: Type.number,
          //   labelText: "\$ Annual Income",
          //   controller: leadProfileController.annualIncomeController,
          //   key: const Key("Income"),
          // ),
          widget.data.canEdit ??
                  false //Condition to hide the widget if can edit is false
              ? SizedBox(height: windowHeight * 0.03)
              : Container(),
          widget.data.canEdit ??
                  false //Condition to hide the widget if canEdit is false
              ? PrimaryButton(
                  windowHeight: windowHeight,
                  windowWidth: windowWidth,
                  buttonTitle: "Save",
                  onPressed: () {
                    leadProfileController.onTapSaveMainClient(
                        leadProfileController.mainClientID);
                  },
                )
              : Container(),
          SizedBox(height: windowHeight * 0.03),
          widget.data.canEdit ??
                  false //Condition to hide the widget if can edit is false
              ? Obx(
                  () => leadProfileController.isShowJointAccount.value ==
                              false &&
                          leadProfileController.clientPDPAStatus.value
                      ? SecondaryButton(
                          windowHeight: windowHeight,
                          windowWidth: windowWidth,
                          kGradientBoxDecoration: ContainerStyles
                              .kGradientBoxDecorationSecondaryButton,
                          kInnerDecoration:
                              ContainerStyles.kInnerDecorationSecondaryButton,
                          buttonTitle: 'Add Joint Applicant',
                          onPressed: () {
                            leadProfileController.onTapAddJoint();
                          },
                        )
                      : Container(),
                )
              : Container(),
          widget.data.canEdit ??
                  false //Condition to hide the widget if can edit is false
              ? leadProfileController.isShowJointAccount.value == false
                  ? SizedBox(height: windowHeight * 0.03)
                  : SizedBox(height: windowHeight * 0.0)
              : Container(),
          Obx(() => leadProfileController.isShowJointAccount.value
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: windowHeight * 0.01),
                    Container(
                      height: windowHeight * 0.08,
                      width: windowWidth * 0.9,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: AppColors.greyF4F4F4,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('Joint Applicant #1',
                                style: TextStyles.jointApplicantHeading),
                            SizedBox(width: windowWidth * 0.01),
                            !widget.data.canEdit!
                                ? Container()
                                : GestureDetector(
                                    onTap: () {
                                      leadProfileController
                                          .onTapDeleteJointAccount();
                                    },
                                    child: const Icon(
                                        Icons.delete_outline_sharp,
                                        color: Colors.black),
                                  ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: windowHeight * 0.03),
                    const FormFieldTitle(title: 'Lead Full Name*'),
                    SizedBox(height: windowHeight * 0.01),
                    RequireTextField(
                      type: Type.fullname,
                      labelText: "Lead full name",
                      controller: leadProfileController.nameJointController,
                      readOnly: !widget.data
                          .canEdit!, //Make filed readonly if can Edit is false
                      key: const Key("fullnameJoint"),
                    ),
                    SizedBox(height: windowHeight * 0.03),
                    const FormFieldTitle(title: 'Lead Mobile No.*'),
                    SizedBox(height: windowHeight * 0.01),
                    RequireTextField(
                      type: Type.dropdownCountryCodeWithPhone,
                      countryCodeController:
                          leadProfileController.countryCodeJointController,
                      controller: leadProfileController.phoneJointController,
                      labelText: "Lead Mobile No.",
                      readOnly: widget.data.clients?.length == 2
                          ? widget.data.clients![1].pdpaStatus ??
                              false || !widget.data.canEdit!
                          : !widget.data
                              .canEdit!, //Make filed readonly if can Edit is false
                      key: const Key("phoneJoint"),
                    ),
                    SizedBox(height: windowHeight * 0.01),
                    const FormFieldTitle(title: 'Lead Email Address*'),
                    SizedBox(height: windowHeight * 0.01),
                    RequireTextField(
                      type: Type.email,
                      controller: leadProfileController.emailJointController,
                      labelText: "Lead Email address",
                      readOnly: !widget.data
                          .canEdit!, //Make filed readonly if can Edit is false
                      key: const Key("emailJoint"),
                    ),
                    SizedBox(height: windowHeight * 0.03),

                    Obx(() => leadProfileController.jointClientID1.isNotEmpty
                        ? Column(
                            children: [
                              InfoButton(
                                windowHeight: windowHeight,
                                windowWidth: windowWidth,
                                buttonTitle: leadProfileController
                                        .jointClient1PDPAStatus.value
                                    ? "PDPA Approval Complete"
                                    : "Send For PDPA Approval ",
                                backgroundColor: leadProfileController
                                        .jointClient1PDPAStatus.value
                                    ? AppColors.lightTextGreen
                                    : leadProfileController
                                                .isResendButtonDisable1.value ||
                                            leadProfileController
                                                .jointClientID1.value.isEmpty
                                        ? Colors.black26
                                        : AppColors.red,
                                icon: Icons.info_outline_rounded,
                                iconDisable: leadProfileController
                                    .jointClient1PDPAStatus.value,
                                onPressed: () {
                                  if (leadProfileController
                                          .jointClient1PDPAStatus.value ||
                                      leadProfileController
                                          .jointClientID1.value.isEmpty) {
                                    return;
                                  }

                                  leadProfileController.onTapSendPDPAApproval1(
                                      leadProfileController
                                          .jointClientID1.value);
                                },
                              ),
                              SizedBox(height: windowHeight * 0.01),
                              Row(children: [
                                leadProfileController
                                            .jointClient1PDPAStatus.value ==
                                        false
                                    ? InkWell(
                                        onTap: () {
                                          leadProfileController
                                              .onTapPDPARefresh1(
                                                  leadProfileController
                                                      .jointClientID1.value);
                                        },
                                        child: Text(
                                          "Refresh PDPA Status",
                                          style: TextStyles.pdpaSendtitleStyle,
                                        ),
                                      )
                                    : const SizedBox(),
                                const Spacer(),
                                Obx(() => !leadProfileController
                                            .isResendButtonHide1.value &&
                                        leadProfileController.countTimes1 !=
                                            3 &&
                                        !leadProfileController
                                            .jointClient1PDPAStatus.value
                                    ? Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          Text(
                                            "Resend in ",
                                            style:
                                                TextStyles.headingSubtitleStyle,
                                          ),
                                          Text(
                                            leadProfileController
                                                .countDownSecond1.value
                                                .toString(),
                                            style:
                                                TextStyles.headingSubtitleStyle,
                                          ),
                                          Text(
                                            " seconds",
                                            style:
                                                TextStyles.headingSubtitleStyle,
                                          ),
                                          const SizedBox(
                                            width: 5,
                                          ),
                                          const SizedBox(
                                              height: 15,
                                              width: 15,
                                              child: CircularProgressIndicator(
                                                color: Colors.deepOrange,
                                                strokeWidth: 2,
                                              )),
                                          const SizedBox(
                                            width: 25,
                                          ),
                                        ],
                                      )
                                    : Container()),
                              ]),
                            ],
                          )
                        : const SizedBox()),

                    Obx(() => leadProfileController.jointClient1PDPAStatus.value
                        ? Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(height: windowHeight * 0.03),
                              const FormFieldTitle(title: 'Date of Birth*'),
                              SizedBox(height: windowHeight * 0.01),
                              RequireTextField(
                                type: Type.dob,
                                controller:
                                    leadProfileController.dobJointController,
                                labelText: "DD/MM/YY",
                                key: const Key("dobJoint"),
                                readOnly: !widget.data
                                    .canEdit!, //Make filed readonly if can Edit is false
                                dateFormat: "dd-MM-yyyy",
                              ),
                              SizedBox(height: windowHeight * 0.03),
                              const FormFieldTitle(title: 'Nationality*'),
                              SizedBox(height: windowHeight * 0.01),
                              RequireTextField(
                                type: Type.dropdownForLeadCountry,
                                controller: leadProfileController
                                    .nationalityJointController,
                                labelText: "country",
                                readOnly: !widget.data
                                    .canEdit!, //Make filed readonly if can Edit is false
                                key: const Key("NationalityJoint"),
                              ),
                              SizedBox(height: windowHeight * 0.03),
                              const FormFieldTitle(
                                  title:
                                      'Annual Income (Based on Latest IRAS Tax Assessment)*'),
                              SizedBox(height: windowHeight * 0.01),
                              CurrencyAmountTextField(
                                  textEditingController: leadProfileController
                                      .annualIncomeJointController,
                                  labelText: "\$ Annual Income",
                                  readOnly: !widget.data
                                      .canEdit!, //Make filed readonly if can Edit is false
                                  key: const Key("IncomeJoint")),
                            ],
                          )
                        : const SizedBox()),
                    // RequireTextField(
                    //   type: Type.number,
                    //   labelText: "\$ Annual Income",
                    //   controller:
                    //       leadProfileController.annualIncomeJointController,
                    //   key: const Key("IncomeJoint"),
                    // ),
                    widget.data.canEdit ??
                            false //Condition to hide the widget if canEdit is false
                        ? SizedBox(height: windowHeight * 0.03)
                        : Container(),
                    widget.data.canEdit ??
                            false //Condition to hide the widget if canEdit is false
                        ? PrimaryButton(
                            windowHeight: windowHeight,
                            windowWidth: windowWidth,
                            buttonTitle: "Save",
                            onPressed: () {
                              leadProfileController.onTapAddJointSave();
                            },
                          )
                        : Container(),
                    SizedBox(height: windowHeight * 0.03),
                    // SecondaryButton(
                    //   windowHeight: windowHeight,
                    //   windowWidth: windowWidth,
                    //   kGradientBoxDecoration:
                    //       ContainerStyles.kGradientBoxDecorationSecondaryButton,
                    //   kInnerDecoration:
                    //       ContainerStyles.kInnerDecorationSecondaryButton,
                    //   buttonTitle: 'Cancel',
                    //   onPressed: () {
                    //     leadProfileController.onTapCancelAddJoint();
                    //   },
                    // ),
                    // SizedBox(height: windowHeight * 0.03),
                    widget.data.canEdit ??
                            false //Condition to hide the widget if canEdit is false
                        ? Obx(
                            () => leadProfileController
                                        .isShowJointAccount2.value ==
                                    false
                                ? SecondaryButton(
                                    windowHeight: windowHeight,
                                    windowWidth: windowWidth,
                                    kGradientBoxDecoration: ContainerStyles
                                        .kGradientBoxDecorationSecondaryButton,
                                    kInnerDecoration: ContainerStyles
                                        .kInnerDecorationSecondaryButton,
                                    buttonTitle: 'Add Joint Applicant',
                                    onPressed: () {
                                      leadProfileController.onTapAddJoint2();
                                    },
                                  )
                                : Container(),
                          )
                        : Container(),
                    widget.data.canEdit ??
                            false //Condition to hide the widget if canEdit is false
                        ? leadProfileController.isShowJointAccount2.value ==
                                false
                            ? SizedBox(height: windowHeight * 0.03)
                            : SizedBox(height: windowHeight * 0.00)
                        : Container(),
                  ],
                )
              : const SizedBox()),
          Obx(() => leadProfileController.isShowJointAccount2.value
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: windowHeight * 0.01),
                    Container(
                      height: windowHeight * 0.08,
                      width: windowWidth * 0.9,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: AppColors.greyF4F4F4,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('Joint Applicant #2',
                                style: TextStyles.jointApplicantHeading),
                            SizedBox(width: windowWidth * 0.01),
                            !widget.data.canEdit!
                                ? Container()
                                : GestureDetector(
                                    onTap: () {
                                      leadProfileController
                                          .onTapDeleteJointAccount2();
                                    },
                                    child: const Icon(
                                        Icons.delete_outline_sharp,
                                        color: Colors.black),
                                  ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: windowHeight * 0.03),
                    const FormFieldTitle(title: 'Lead Full Name*'),
                    SizedBox(height: windowHeight * 0.01),
                    RequireTextField(
                      type: Type.fullname,
                      labelText: "Lead full name",
                      controller: leadProfileController.nameJointController2,
                      readOnly: !widget.data
                          .canEdit!, //Make filed readonly if can Edit is false
                      key: const Key("fullnameJoint2"),
                    ),
                    SizedBox(height: windowHeight * 0.03),
                    const FormFieldTitle(title: 'Lead Mobile No.*'),
                    SizedBox(height: windowHeight * 0.01),
                    RequireTextField(
                      type: Type.dropdownCountryCodeWithPhone,
                      countryCodeController:
                          leadProfileController.countryCodeJointController2,
                      controller: leadProfileController.phoneJointController2,
                      labelText: "Lead Mobile No.",
                      readOnly: widget.data.clients?.length == 3
                          ? widget.data.clients![2].pdpaStatus ??
                              false || !widget.data.canEdit!
                          : !widget.data
                              .canEdit!, //Make filed readonly if can Edit is false
                      key: const Key("phoneJoint2"),
                    ),
                    SizedBox(height: windowHeight * 0.01),
                    const FormFieldTitle(title: 'Lead Email Address*'),
                    SizedBox(height: windowHeight * 0.01),
                    RequireTextField(
                      type: Type.email,
                      controller: leadProfileController.emailJointController2,
                      labelText: "Lead Email address",
                      readOnly: !widget.data
                          .canEdit!, //Make filed readonly if can Edit is false
                      key: const Key("emailJoint2"),
                    ),
                    SizedBox(height: windowHeight * 0.03),

                    Obx(() => leadProfileController.jointClientID2.isNotEmpty
                        ? Column(
                            children: [
                              InfoButton(
                                windowHeight: windowHeight,
                                windowWidth: windowWidth,
                                buttonTitle: leadProfileController
                                        .jointClient2PDPAStatus.value
                                    ? "PDPA Approval Complete"
                                    : "Send For PDPA Approval ",
                                backgroundColor: leadProfileController
                                        .jointClient2PDPAStatus.value
                                    ? AppColors.lightTextGreen
                                    : leadProfileController
                                                .isResendButtonDisable2.value ||
                                            leadProfileController
                                                .jointClientID2.value.isEmpty
                                        ? Colors.black26
                                        : AppColors.red,
                                icon: Icons.info_outline_rounded,
                                iconDisable: leadProfileController
                                    .jointClient2PDPAStatus.value,
                                onPressed: () {
                                  if (leadProfileController
                                          .jointClient2PDPAStatus.value ||
                                      leadProfileController
                                          .jointClientID2.value.isEmpty) {
                                    return;
                                  }

                                  leadProfileController.onTapSendPDPAApproval2(
                                      leadProfileController
                                          .jointClientID2.value);
                                },
                              ),
                              SizedBox(height: windowHeight * 0.01),
                              Row(children: [
                                leadProfileController
                                            .jointClient2PDPAStatus.value ==
                                        false
                                    ? InkWell(
                                        onTap: () {
                                          leadProfileController
                                              .onTapPDPARefresh2(
                                                  leadProfileController
                                                      .jointClientID2.value);
                                        },
                                        child: Text(
                                          "Refresh PDPA Status",
                                          style: TextStyles.pdpaSendtitleStyle,
                                        ),
                                      )
                                    : const SizedBox(),
                                const Spacer(),
                                Obx(() => !leadProfileController
                                            .isResendButtonHide2.value &&
                                        leadProfileController.countTimes2 !=
                                            3 &&
                                        !leadProfileController
                                            .jointClient2PDPAStatus.value
                                    ? Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          Text(
                                            "Resend in ",
                                            style:
                                                TextStyles.headingSubtitleStyle,
                                          ),
                                          Text(
                                            leadProfileController
                                                .countDownSecond2.value
                                                .toString(),
                                            style:
                                                TextStyles.headingSubtitleStyle,
                                          ),
                                          Text(
                                            " seconds",
                                            style:
                                                TextStyles.headingSubtitleStyle,
                                          ),
                                          const SizedBox(
                                            width: 5,
                                          ),
                                          const SizedBox(
                                              height: 15,
                                              width: 15,
                                              child: CircularProgressIndicator(
                                                color: Colors.deepOrange,
                                                strokeWidth: 2,
                                              )),
                                          const SizedBox(
                                            width: 25,
                                          ),
                                        ],
                                      )
                                    : Container()),
                              ]),
                            ],
                          )
                        : const SizedBox()),

                    Obx(() => leadProfileController.jointClient2PDPAStatus.value
                        ? Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(height: windowHeight * 0.03),
                              const FormFieldTitle(title: 'Date of Birth*'),
                              SizedBox(height: windowHeight * 0.01),
                              RequireTextField(
                                type: Type.dob,
                                controller:
                                    leadProfileController.dobJointController2,
                                labelText: "DD/MM/YY",
                                key: const Key("dobJoint2"),
                                readOnly: !widget.data
                                    .canEdit!, //Make filed readonly if can Edit is false
                                dateFormat: "dd-MM-yyyy",
                              ),
                              SizedBox(height: windowHeight * 0.03),
                              const FormFieldTitle(title: 'Nationality*'),
                              SizedBox(height: windowHeight * 0.01),
                              RequireTextField(
                                type: Type.dropdownForLeadCountry,
                                controller: leadProfileController
                                    .nationalityJointController2,
                                labelText: "country",
                                readOnly: !widget.data
                                    .canEdit!, //Make filed readonly if can Edit is false
                                key: const Key("NationalityJoint2"),
                              ),
                              SizedBox(height: windowHeight * 0.03),
                              const FormFieldTitle(
                                  title:
                                      'Annual Income (Based on Latest IRAS Tax Assessment)*'),
                              SizedBox(height: windowHeight * 0.01),
                              CurrencyAmountTextField(
                                  textEditingController: leadProfileController
                                      .annualIncomeJointController2,
                                  labelText: "\$ Annual Income",
                                  readOnly: !widget.data
                                      .canEdit!, //Make filed readonly if can Edit is false
                                  key: const Key("IncomeJoint2")),
                            ],
                          )
                        : const SizedBox()),
                    // RequireTextField(
                    //   type: Type.number,
                    //   labelText: "\$ Annual Income",
                    //   controller:
                    //       leadProfileController.annualIncomeJointController2,
                    //   key: const Key("IncomeJoint2"),
                    // ),
                    widget.data.canEdit ??
                            false //Condition to hide the widget if canEdit is false
                        ? SizedBox(height: windowHeight * 0.03)
                        : Container(),
                    widget.data.canEdit ??
                            false //Condition to hide the widget if canEdit is false
                        ? PrimaryButton(
                            windowHeight: windowHeight,
                            windowWidth: windowWidth,
                            buttonTitle: "Save",
                            onPressed: () {
                              leadProfileController.onTapAddJointSave2();
                            },
                          )
                        : Container(),
                    widget.data.canEdit ??
                            false //Condition to hide the widget if canEdit is false
                        ? SizedBox(height: windowHeight * 0.03)
                        : Container(),
                    // SecondaryButton(
                    //   windowHeight: windowHeight,
                    //   windowWidth: windowWidth,
                    //   kGradientBoxDecoration:
                    //       ContainerStyles.kGradientBoxDecorationSecondaryButton,
                    //   kInnerDecoration:
                    //       ContainerStyles.kInnerDecorationSecondaryButton,
                    //   buttonTitle: 'Cancel',
                    //   onPressed: () {
                    //     leadProfileController.onTapCancelAddJoint2();
                    //   },
                    // ),
                    // SizedBox(height: windowHeight * 0.03),
                    widget.data.canEdit ??
                            false //Condition to hide the widget if canEdit is false
                        ? Obx(
                            () => leadProfileController
                                        .isShowJointAccount3.value ==
                                    false
                                ? SecondaryButton(
                                    windowHeight: windowHeight,
                                    windowWidth: windowWidth,
                                    kGradientBoxDecoration: ContainerStyles
                                        .kGradientBoxDecorationSecondaryButton,
                                    kInnerDecoration: ContainerStyles
                                        .kInnerDecorationSecondaryButton,
                                    buttonTitle: 'Add Joint Applicant',
                                    onPressed: () {
                                      leadProfileController.onTapAddJoint3();
                                    },
                                  )
                                : Container(),
                          )
                        : Container(),
                    widget.data.canEdit ??
                            false //Condition to hide the widget if canEdit is false
                        ? leadProfileController.isShowJointAccount3.value ==
                                false
                            ? SizedBox(height: windowHeight * 0.03)
                            : SizedBox(height: windowHeight * 0.00)
                        : Container(),
                  ],
                )
              : const SizedBox()),
          Obx(() => leadProfileController.isShowJointAccount3.value
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: windowHeight * 0.01),
                    Container(
                      height: windowHeight * 0.08,
                      width: windowWidth * 0.9,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: AppColors.greyF4F4F4,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('Joint Applicant #3',
                                style: TextStyles.jointApplicantHeading),
                            SizedBox(width: windowWidth * 0.01),
                            !widget.data.canEdit!
                                ? Container()
                                : GestureDetector(
                                    onTap: () {
                                      leadProfileController
                                          .onTapDeleteJointAccount3();
                                    },
                                    child: const Icon(
                                        Icons.delete_outline_sharp,
                                        color: Colors.black),
                                  ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: windowHeight * 0.03),
                    const FormFieldTitle(title: 'Lead Full Name*'),
                    SizedBox(height: windowHeight * 0.01),
                    RequireTextField(
                      type: Type.fullname,
                      labelText: "Lead full name",
                      controller: leadProfileController.nameJointController3,
                      readOnly: !widget.data
                          .canEdit!, //Make filed readonly if can Edit is false
                      key: const Key("fullnameJoint3"),
                    ),
                    SizedBox(height: windowHeight * 0.03),
                    const FormFieldTitle(title: 'Lead Mobile No.*'),
                    SizedBox(height: windowHeight * 0.01),
                    RequireTextField(
                      type: Type.dropdownCountryCodeWithPhone,
                      countryCodeController:
                          leadProfileController.countryCodeJointController3,
                      controller: leadProfileController.phoneJointController3,
                      labelText: "Lead Mobile No.",
                      readOnly: widget.data.clients?.length == 4
                          ? widget.data.clients![3].pdpaStatus ??
                              false || !widget.data.canEdit!
                          : !widget.data
                              .canEdit!, //Make filed readonly if can Edit is false
                      key: const Key("phoneJoint3"),
                    ),
                    SizedBox(height: windowHeight * 0.01),
                    const FormFieldTitle(title: 'Lead Email Address*'),
                    SizedBox(height: windowHeight * 0.01),
                    RequireTextField(
                      type: Type.email,
                      controller: leadProfileController.emailJointController3,
                      labelText: "Lead Email address",
                      readOnly: !widget.data
                          .canEdit!, //Make filed readonly if can Edit is false
                      key: const Key("emailJoint3"),
                    ),
                    SizedBox(height: windowHeight * 0.03),

                    Obx(() => leadProfileController.jointClientID3.isNotEmpty
                        ? Column(
                            children: [
                              InfoButton(
                                windowHeight: windowHeight,
                                windowWidth: windowWidth,
                                buttonTitle: leadProfileController
                                        .jointClient3PDPAStatus.value
                                    ? "PDPA Approval Complete"
                                    : "Send For PDPA Approval ",
                                backgroundColor: leadProfileController
                                        .jointClient3PDPAStatus.value
                                    ? AppColors.lightTextGreen
                                    : leadProfileController
                                                .isResendButtonDisable3.value ||
                                            leadProfileController
                                                .jointClientID3.value.isEmpty
                                        ? Colors.black26
                                        : AppColors.red,
                                icon: Icons.info_outline_rounded,
                                iconDisable: leadProfileController
                                    .jointClient3PDPAStatus.value,
                                onPressed: () {
                                  if (leadProfileController
                                          .jointClient3PDPAStatus.value ||
                                      leadProfileController
                                          .jointClientID3.value.isEmpty) {
                                    return;
                                  }

                                  leadProfileController.onTapSendPDPAApproval3(
                                      leadProfileController
                                          .jointClientID3.value);
                                },
                              ),
                              SizedBox(height: windowHeight * 0.01),
                              Row(children: [
                                leadProfileController
                                            .jointClient3PDPAStatus.value ==
                                        false
                                    ? InkWell(
                                        onTap: () {
                                          leadProfileController
                                              .onTapPDPARefresh3(
                                                  leadProfileController
                                                      .jointClientID3.value);
                                        },
                                        child: Text(
                                          "Refresh PDPA Status",
                                          style: TextStyles.pdpaSendtitleStyle,
                                        ),
                                      )
                                    : const SizedBox(),
                                const Spacer(),
                                Obx(() => !leadProfileController
                                            .isResendButtonHide3.value &&
                                        leadProfileController.countTimes3 !=
                                            3 &&
                                        !leadProfileController
                                            .jointClient3PDPAStatus.value
                                    ? Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          Text(
                                            "Resend in ",
                                            style:
                                                TextStyles.headingSubtitleStyle,
                                          ),
                                          Text(
                                            leadProfileController
                                                .countDownSecond3.value
                                                .toString(),
                                            style:
                                                TextStyles.headingSubtitleStyle,
                                          ),
                                          Text(
                                            " seconds",
                                            style:
                                                TextStyles.headingSubtitleStyle,
                                          ),
                                          const SizedBox(
                                            width: 5,
                                          ),
                                          const SizedBox(
                                              height: 15,
                                              width: 15,
                                              child: CircularProgressIndicator(
                                                color: Colors.deepOrange,
                                                strokeWidth: 2,
                                              )),
                                          const SizedBox(
                                            width: 25,
                                          ),
                                        ],
                                      )
                                    : Container()),
                              ]),
                            ],
                          )
                        : const SizedBox()),

                    Obx(() => leadProfileController.jointClient3PDPAStatus.value
                        ? Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(height: windowHeight * 0.03),
                              const FormFieldTitle(title: 'Date of Birth*'),
                              SizedBox(height: windowHeight * 0.01),
                              RequireTextField(
                                type: Type.dob,
                                controller:
                                    leadProfileController.dobJointController3,
                                labelText: "DD/MM/YY",
                                key: const Key("dobJoint3"),
                                readOnly: !widget.data
                                    .canEdit!, //Make filed readonly if can Edit is false
                                dateFormat: "dd-MM-yyyy",
                              ),
                              SizedBox(height: windowHeight * 0.03),
                              const FormFieldTitle(title: 'Nationality*'),
                              SizedBox(height: windowHeight * 0.01),
                              RequireTextField(
                                type: Type.dropdownForLeadCountry,
                                controller: leadProfileController
                                    .nationalityJointController3,
                                labelText: "country",
                                readOnly: !widget.data
                                    .canEdit!, //Make filed readonly if can Edit is false
                                key: const Key("NationalityJoint3"),
                              ),
                              SizedBox(height: windowHeight * 0.03),
                              const FormFieldTitle(
                                  title:
                                      'Annual Income (Based on Latest IRAS Tax Assessment)*'),
                              SizedBox(height: windowHeight * 0.01),
                              CurrencyAmountTextField(
                                textEditingController: leadProfileController
                                    .annualIncomeJointController3,
                                labelText: "\$ Annual Income",
                                readOnly: !widget.data
                                    .canEdit!, //Make filed readonly if can Edit is false
                                key: const Key("IncomeJoint3"),
                              ),
                            ],
                          )
                        : const SizedBox()),
                    // RequireTextField(
                    //   type: Type.number,
                    //   labelText: "\$ Annual Income",
                    //   controller:
                    //       leadProfileController.annualIncomeJointController3,
                    //   key: const Key("IncomeJoint3"),
                    // ),
                    SizedBox(height: windowHeight * 0.03),
                    PrimaryButton(
                      windowHeight: windowHeight,
                      windowWidth: windowWidth,
                      buttonTitle: "Save",
                      onPressed: () {
                        leadProfileController.onTapAddJointSave3();
                      },
                    ),
                    SizedBox(height: windowHeight * 0.03),
                    // SecondaryButton(
                    //   windowHeight: windowHeight,
                    //   windowWidth: windowWidth,
                    //   kGradientBoxDecoration:
                    //       ContainerStyles.kGradientBoxDecorationSecondaryButton,
                    //   kInnerDecoration:
                    //       ContainerStyles.kInnerDecorationSecondaryButton,
                    //   buttonTitle: 'Cancel',
                    //   onPressed: () {
                    //     leadProfileController.onTapCancelAddJoint3();
                    //   },
                    // ),
                    // SizedBox(height: windowHeight * 0.02),
                  ],
                )
              : const SizedBox()),
        ],
      ),
    );
  }

  Widget expandableCard(String title, bool isDone) {
    return GestureDetector(
        onTap: () {
          GetControllers.shared.getLeadProfileController().isExpand.toggle();

          GetControllers.shared.getSubmitCoBrokeController().isExpand.value =
              false;
          GetControllers.shared.getLoanDetailsController().isExpand.value =
              false;
          GetControllers.shared.getPropertyDetailsController().isExpand.value =
              false;
          GetControllers.shared.getGeneratePackagesController().isExpand.value =
              false;
          GetControllers.shared.getSelectedPackagesController().isExpand.value =
              false;
          GetControllers.shared.getBankSubmissionController().isExpand.value =
              false;
          GetControllers.shared
              .getSubmitForPayoutViewController()
              .isExpand
              .value = false;
          GetControllers.shared
              .getLawFirmSubmissionController()
              .isExpand
              .value = false;
          GetControllers.shared.getEmailLogController().isExpand.value = false;

          leadProfileController.checkIfPDPACompletedForAll();
        },
        child: Obx(
          () => Container(
            color: Colors.white,
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(bottom: windowWidth * 0.02),
                  padding: EdgeInsets.all(windowWidth * 0.05),
                  decoration: const BoxDecoration(boxShadow: <BoxShadow>[
                    BoxShadow(
                        color: Colors.black54,
                        blurRadius: 5.0,
                        offset: Offset(0.0, 0.3))
                  ], color: Colors.white),
                  child: Row(
                    children: [
                      Text(title, style: TextStyles.basicTextStyle),
                      const Spacer(),
                      Obx(() {
                        log(leadProfileController
                            .pdpaStatusForAllApplicant.value
                            .toString());
                        return SvgPicture.asset(
                          leadProfileController.pdpaStatusForAllApplicant.value
                              ? Assets.manageLoanDone
                              : Assets.manageLoanIncomplete,
                          height: 24,
                          width: 24,
                        );
                      }),
                      SizedBox(width: windowWidth * 0.03),
                      Icon(
                        leadProfileController.isExpand.value
                            ? Icons.keyboard_arrow_up_rounded
                            : Icons.keyboard_arrow_down,
                        size: 30,
                      )
                    ],
                  ),
                ),
                leadProfileController.isExpand.value
                    ? leadCreateView()
                    : Container()
              ],
            ),
          ),
        ));
  }
}
