// ignore_for_file: unused_field

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/leads_view_controller.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/container_styles.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Views/lead_list_screen.dart';
import 'package:iqrate/Widgets/form_field_title.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';
import 'package:iqrate/Widgets/secondary_button.dart';
import 'package:url_launcher/url_launcher_string.dart';

class LeadsView extends StatefulWidget {
  const LeadsView({Key? key}) : super(key: key);

  @override
  State<LeadsView> createState() => _LeadsViewState();
}

class _LeadsViewState extends State<LeadsView>
    with SingleTickerProviderStateMixin {
  late double windowHeight;
  late double windowWidth;

  // final List<String> _locations = ['A', 'B']; // Option 2
  String? _selectedLocation;
  bool isSell = false;
  bool isAgree = false;

  final LeadsViewController leadsViewController =
      Get.put(LeadsViewController());
  late AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 450),
      upperBound: 0.5,
    );
    _controller.addListener(() {
      if (_controller.status == AnimationStatus.completed) {
        _controller.reset();
      }
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.width;

    return Obx(
      () => Scaffold(
        //backgroundColor: AppColors.leadBG,
        backgroundColor: leadsViewController.selectedIndex.value == 6
            ? Colors.white
            : AppColors.white,
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: leadsViewController.selectedIndex.value == 6
                          ? 0
                          : windowWidth * 0.05),
                  child: Card(
                    elevation: leadsViewController.selectedIndex.value == 6
                        ? 0
                        : 10, //Elevation for the create leads card. The background colour has been changed to white and elevation has been added.
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(
                          leadsViewController.selectedIndex.value == 6
                              ? 0
                              : 20),
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(
                        leadsViewController.selectedIndex.value == 6
                            ? 10.0
                            : 28.0,
                      ),
                      child: leadsViewController.selectedIndex.value == 0
                          ? viewTutorialLeadView()
                          : leadsViewController.selectedIndex.value == 1
                              ? proceedLeadCreation()
                              : leadsViewController.selectedIndex.value == 2
                                  ? leadCreateOptionsView()
                                  : leadsViewController.selectedIndex.value == 3
                                      ? applyLoanForOthersView()
                                      : leadsViewController
                                                  .selectedIndex.value ==
                                              4
                                          ? createLead()
                                          : leadsViewController
                                                      .selectedIndex.value ==
                                                  5
                                              ? leadCreateDoneView()
                                              : leadsViewController
                                                          .selectedIndex
                                                          .value ==
                                                      6
                                                  ? SizedBox(
                                                      height:
                                                          windowHeight * 0.7,
                                                      child: Stack(
                                                        children: [
                                                          const LeadListScreen(), //! Lead list screen
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .fromLTRB(
                                                                    0.0,
                                                                    0,
                                                                    15,
                                                                    20),
                                                            child: Align(
                                                              //! This is a refresh button used to refresh the lead list screen
                                                              alignment: Alignment
                                                                  .bottomRight,
                                                              child: Container(
                                                                decoration: const BoxDecoration(
                                                                    shape: BoxShape
                                                                        .circle,
                                                                    gradient:
                                                                        LinearGradient(
                                                                            colors: [
                                                                          Color(
                                                                              0xffe09277),
                                                                          Color(
                                                                              0xfff9484a)
                                                                        ])),
                                                                child:
                                                                    RotationTransition(
                                                                  turns: Tween(
                                                                          begin:
                                                                              0.0,
                                                                          end:
                                                                              4.0)
                                                                      .animate(
                                                                          _controller),
                                                                  child:
                                                                      IconButton(
                                                                    onPressed:
                                                                        () {
                                                                      leadsViewController.selectedStatus!.value ==
                                                                              'All'
                                                                          ? leadsViewController
                                                                              .fetchLeads()
                                                                          : leadsViewController.filterByLeadStatus(leadsViewController
                                                                              .selectedStatus!
                                                                              .value
                                                                              .toLowerCase());
                                                                      _controller.status ==
                                                                              AnimationStatus
                                                                                  .completed
                                                                          ? _controller
                                                                              .reset()
                                                                          : _controller
                                                                              .forward();
                                                                    },
                                                                    icon:
                                                                        const Icon(
                                                                      Icons
                                                                          .replay_outlined,
                                                                      color: Colors
                                                                          .white,
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    )
                                                  : leadsViewController
                                                              .selectedIndex
                                                              .value ==
                                                          7
                                                      ? coBrokeView()
                                                      : leadsViewController
                                                                  .selectedIndex
                                                                  .value ==
                                                              8
                                                          ? applyLoanForYourSelfView()
                                                          : applyLoanForYourSelfView(),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
  // getViews(){
  //   switch(leadsViewController.selectedIndex.value) {
  //     case 1: {
  //       return viewTutorialLeadView();
  //     }
  //     default: {
  //       return SizedBox();
  //     }
  //
  //   }
  // }

  Widget proceedLeadCreation() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(height: windowHeight * 0.001),
        GestureDetector(
          onTap: () {
            debugPrint('Navigate to tutorial page');
            leadsViewController.onTapViewTutorial();
          },
          child: Text.rich(
            TextSpan(
                text: 'I hereby acknowledge that I have\n',
                //textAlign : Alignment.center,
                style: TextStyles.leadsTextStyle,
                children: <InlineSpan>[
                  TextSpan(
                    text: ' completed the tutorial @ ',
                    style: TextStyles.leadsTextStyle,
                  ),
                  TextSpan(
                    text: '\nTutorial Section',
                    style: TextStyles.leadsColorTextStyle,
                  ),
                ]),
            textAlign: TextAlign.center,
          ),
        ),
        SizedBox(height: windowHeight * 0.04),
        PrimaryButton(
          windowHeight: windowHeight,
          windowWidth: windowWidth,
          buttonTitle: "Proceed to Lead Creation",
          onPressed: () {
            leadsViewController.onProceedToLeadCreation();
          },
        ),
        SizedBox(height: windowHeight * 0.03),
        SecondaryButton(
          windowHeight: windowHeight,
          windowWidth: windowWidth,
          kGradientBoxDecoration:
              ContainerStyles.kGradientBoxDecorationSecondaryButton,
          kInnerDecoration: ContainerStyles.kInnerDecorationSecondaryButton,
          buttonTitle: 'Back',
          onPressed: () {
            leadsViewController.onProceedToLeadCreationBack();
          },
        ),
        SizedBox(height: windowHeight * 0.0009),
      ],
    );
  }

  Widget applyLoanForOthersView() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        const Text(
          'Apply Loan for Others',
          style: TextStyle(
              color: Colors.red,
              fontSize: 20,
              fontWeight: FontWeight.w600,
              height: 1.5),
        ),
        SizedBox(height: windowHeight * 0.04),
        Text(
            "Handle the entire loan \napplication yourself and earn \nmaximum referral fee",
            textAlign: TextAlign.center,
            style: TextStyles.leadsTextStyle),
        SizedBox(height: windowHeight * 0.04),
        PrimaryButton(
          windowHeight: windowHeight,
          windowWidth: windowWidth,
          buttonTitle: "Continue",
          onPressed: () {
            leadsViewController.onTapApplyLoanForOthers();
          },
        ),
        SizedBox(height: windowHeight * 0.03),
        SecondaryButton(
          windowHeight: windowHeight,
          windowWidth: windowWidth,
          kGradientBoxDecoration:
              ContainerStyles.kGradientBoxDecorationSecondaryButton,
          kInnerDecoration: ContainerStyles.kInnerDecorationSecondaryButton,
          buttonTitle: 'Back',
          onPressed: () {
            leadsViewController.onTapApplyLoanForOthersBack();
          },
        ),
        SizedBox(height: windowHeight * 0.0009),
      ],
    );
  }

  Widget applyLoanForYourSelfView() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        const Text(
          'Apply Loan for Yourself',
          style: TextStyle(
              color: Colors.red,
              fontSize: 20,
              fontWeight: FontWeight.w600,
              height: 1.5),
        ),
        SizedBox(height: windowHeight * 0.04),
        Text(
            "If you are applying loan for yourself, the application needs to be processed as co-broke.\n\nJust complete your details and IQrate partnered mortgage brokers will assist on your application. \n\nReferral fee will be co-shared at 50%.",
            textAlign: TextAlign.center,
            style: TextStyles.leadsTextStyle),
        SizedBox(height: windowHeight * 0.04),
        PrimaryButton(
          windowHeight: windowHeight,
          windowWidth: windowWidth,
          buttonTitle: "Continue",
          onPressed: () {
            leadsViewController.onTapLoanForOthersContinue();
          },
        ),
        SizedBox(height: windowHeight * 0.03),
        SecondaryButton(
          windowHeight: windowHeight,
          windowWidth: windowWidth,
          kGradientBoxDecoration:
              ContainerStyles.kGradientBoxDecorationSecondaryButton,
          kInnerDecoration: ContainerStyles.kInnerDecorationSecondaryButton,
          buttonTitle: 'Back',
          onPressed: () {
            leadsViewController.onTapLoanForOthersContinueBack();
          },
        ),
        SizedBox(height: windowHeight * 0.0009),
      ],
    );
  }

  Widget coBrokeView() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Text(
          'Co-Broke',
          textAlign: TextAlign.left,
          style: TextStyle(
              color: Colors.red,
              fontSize: 20,
              fontWeight: FontWeight.w600,
              height: 1.5),
        ),
        SizedBox(height: windowHeight * 0.03),
        Text("Loan application process too difficult for you?",
            textAlign: TextAlign.left, style: TextStyles.leadsTextStyle),
        SizedBox(height: windowHeight * 0.03),
        Text(
            "Just fill up the lead details and IQrate partnered mortgage broker will close the deal for you",
            textAlign: TextAlign.left,
            style: TextStyles.leadsTextStyle),
        SizedBox(height: windowHeight * 0.03),
        Text("Referral fee will be co-share at 50%",
            textAlign: TextAlign.left, style: TextStyles.leadsTextStyle),
        SizedBox(height: windowHeight * 0.04),
        PrimaryButton(
          windowHeight: windowHeight,
          windowWidth: windowWidth,
          buttonTitle: "Continue",
          onPressed: () {
            leadsViewController.onTapCoBrokeContinue();
          },
        ),
        SizedBox(height: windowHeight * 0.03),
        SecondaryButton(
          windowHeight: windowHeight,
          windowWidth: windowWidth,
          kGradientBoxDecoration:
              ContainerStyles.kGradientBoxDecorationSecondaryButton,
          kInnerDecoration: ContainerStyles.kInnerDecorationSecondaryButton,
          buttonTitle: 'Back',
          onPressed: () {
            leadsViewController.onTapCoBrokeContinueBack();
          },
        ),
        SizedBox(height: windowHeight * 0.0009),
      ],
    );
  }

  Widget viewTutorialLeadView() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        GestureDetector(
          onTap: () {
            leadsViewController.selectedIndex.value = 6;
          },
          child: const Align(
            alignment: Alignment.centerRight,
            child: Icon(Icons.close),
          ),
        ),
        SizedBox(height: windowHeight * 0.01),
        Text.rich(
          TextSpan(
              text: 'Please watch the tutorial before the \n',
              //textAlign : Alignment.center,
              style: TextStyles.leadsTextStyle,
              children: <InlineSpan>[
                TextSpan(
                  text: ' creation of your first lead',
                  style: TextStyles.leadsTextStyle,
                )
              ]),
          textAlign: TextAlign.center,
        ),
        SizedBox(height: windowHeight * 0.04),
        PrimaryButton(
          windowHeight: windowHeight,
          windowWidth: windowWidth,
          buttonTitle: "View Tutorial",
          onPressed: () {
            leadsViewController.onTapViewTutorial();
          },
        ),
        SizedBox(height: windowHeight * 0.03),
        Obx(
          () => leadsViewController.enableContinueButton.value
              ? SecondaryButton(
                  windowHeight: windowHeight,
                  windowWidth: windowWidth,
                  kGradientBoxDecoration:
                      ContainerStyles.kGradientBoxDecorationSecondaryButton,
                  kInnerDecoration:
                      ContainerStyles.kInnerDecorationSecondaryButton,
                  buttonTitle: 'Continue',
                  onPressed: () {
                    leadsViewController.onTapContinueScreen();
                  },
                )
              : Container(),
        ),
        SizedBox(height: windowHeight * 0.0009),
      ],
    );
  }

  Widget leadCreateOptionsView() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        GestureDetector(
          onTap: () {
            leadsViewController.selectedIndex.value = 6;
          },
          child: const Align(
            alignment: Alignment.centerRight,
            child: Icon(
              Icons.close,
              color: Colors.black,
            ),
          ),
        ),
        const Text(
          'Create a Lead',
          style: TextStyle(
              color: Colors.red,
              fontSize: 20,
              fontWeight: FontWeight.w600,
              height: 1.5),
        ),
        SizedBox(height: windowHeight * 0.03),
        PrimaryButton(
          windowHeight: windowHeight,
          windowWidth: windowWidth,
          buttonTitle: "Apply Loan for Others",
          onPressed: () {
            leadsViewController.onTapApplyLoanOthersScreen();
          },
        ),
        SizedBox(height: windowHeight * 0.04),
        SecondaryButton(
          windowHeight: windowHeight,
          windowWidth: windowWidth,
          kGradientBoxDecoration:
              ContainerStyles.kGradientBoxDecorationSecondaryButton,
          kInnerDecoration: ContainerStyles.kInnerDecorationSecondaryButton,
          buttonTitle: 'Apply Loan for Yourself',
          onPressed: () {
            leadsViewController.onTapApplyLoanSelfScreen();
          },
        ),
        SizedBox(height: windowHeight * 0.04),
        SecondaryButton(
          windowHeight: windowHeight,
          windowWidth: windowWidth,
          kGradientBoxDecoration:
              ContainerStyles.kGradientBoxDecorationSecondaryButton,
          kInnerDecoration: ContainerStyles.kInnerDecorationSecondaryButton,
          buttonTitle: 'Co Broke',
          onPressed: () {
            leadsViewController.onTapCoBrokeScreen();
          },
        ),
        SizedBox(height: windowHeight * 0.04),
        // const Text(
        //   'If you are the applicant, you will \n have to send the loan package for\n co-broke.',
        //   textAlign: TextAlign.center,
        //   style: TextStyle(
        //       color: Colors.black54,
        //       fontSize: 16,
        //       fontWeight: FontWeight.w400,
        //       height: 1.5),
        // )
      ],
    );
  }

  Widget leadCreateDoneView() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(height: windowHeight * 0.09),
        const Text(
          'Congratulations!',
          style: TextStyle(
              color: Colors.black, fontSize: 22, fontWeight: FontWeight.w500),
        ),
        SizedBox(height: windowHeight * 0.02),
        Text(
          'Your lead has been created. \n Please check the message received, click on the link to accept the PDPA to allow IQrate to process your loan.',
          textAlign: TextAlign.center,
          style: TextStyles.leadsTextStyle,
        ),
        SizedBox(height: windowHeight * 0.05),
        PrimaryButton(
          windowHeight: windowHeight,
          windowWidth: windowWidth,
          buttonTitle: "Done",
          onPressed: () {
            leadsViewController.onTapLeadCreateDoneScreen();
          },
        ),
        SizedBox(height: windowHeight * 0.06),
      ],
    );
  }

  Widget createLead() {
    return Column(
      //mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Center(
          child: Text(
            'Create a Lead',
            style: TextStyle(
                color: Colors.red,
                fontSize: 22,
                fontWeight: FontWeight.w600,
                height: 1.5),
          ),
        ),
        SizedBox(height: windowHeight * 0.03),
        const FormFieldTitle(title: 'Lead Full Name*'),
        SizedBox(height: windowHeight * 0.01),
        RequireTextField(
          type: Type.fullname,
          labelText: "Enter lead full name",
          controller: leadsViewController.nameController,
          key: const Key("name"),
        ),
        SizedBox(height: windowHeight * 0.03),
        const FormFieldTitle(title: 'Lead Mobile Number*'),
        SizedBox(height: windowHeight * 0.01),
        RequireTextField(
          type: Type.dropdownCountryCodeWithPhone,
          controller: leadsViewController.phoneController,
          countryCodeController: leadsViewController.countryCodeController,
          countryCodeLabelController:
              leadsViewController.countryCodeLabelController,
          labelText: "Enter lead phone number",
          key: const Key("phone"),
        ),
        SizedBox(height: windowHeight * 0.01),
        const FormFieldTitle(title: 'Lead Email Address*'),
        SizedBox(height: windowHeight * 0.01),
        RequireTextField(
          type: Type.email,
          controller: leadsViewController.emailController,
          labelText: "Enter lead email address",
          key: const Key("email"),
        ),
        SizedBox(height: windowHeight * 0.04),
        PrimaryButton(
          windowHeight: windowHeight,
          windowWidth: windowWidth,
          buttonTitle: "Create Now",
          onPressed: () {
            leadsViewController.onTapLeadCreateNowScreen();
          },
        ),
        SizedBox(height: windowHeight * 0.03),
        SecondaryButton(
          windowHeight: windowHeight,
          windowWidth: windowWidth,
          kGradientBoxDecoration:
              ContainerStyles.kGradientBoxDecorationSecondaryButton,
          kInnerDecoration: ContainerStyles.kInnerDecorationSecondaryButton,
          buttonTitle: 'Back',
          onPressed: () {
            leadsViewController.onTapBackPreLeadCreateScreen();
          },
        ),
        SizedBox(height: windowHeight * 0.02),
        Text.rich(
          TextSpan(
              text:
                  'Upon creating a lead, an SMS will be sent to the lead for lead to agree to IQrate’s ',
              //textAlign : Alignment.center,
              style: TextStyles.leadsNormalTextStyle,
              children: <InlineSpan>[
                TextSpan(
                  text: 'Terms & Conditions',
                  style: TextStyles.leadsUnderlineTextStyle,
                  recognizer: TapGestureRecognizer()
                    ..onTap = () async {
                      // Open the terms and condition URL
                      String url = 'https://iqrate.netinu.io/terms-conditions/';
                      try {
                        if (await canLaunchUrlString(url)) {
                          await launchUrlString(url);
                        } else {
                          throw "Could not launch $url";
                        }
                      } catch (e) {
                        debugPrint(e.toString());
                      }
                    },
                ),
                TextSpan(
                  text: ' & ',
                  style: TextStyles.leadsNormalTextStyle,
                ),
                TextSpan(
                  text: 'Privacy Policy',
                  style: TextStyles.leadsUnderlineTextStyle,
                  recognizer: TapGestureRecognizer()
                    ..onTap = () async {
                      // Open the privacy policy URL
                      String url = 'https://iqrate.netinu.io/privacy-policy/';
                      try {
                        if (await canLaunchUrlString(url)) {
                          await launchUrlString(url);
                        } else {
                          throw "Could not launch $url";
                        }
                      } catch (e) {
                        debugPrint(e.toString());
                      }
                    },
                )
              ]),
          textAlign: TextAlign.center,
        ),
      ],
    );
  }
}
