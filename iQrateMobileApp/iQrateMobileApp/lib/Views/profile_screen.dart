// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/acl_controller.dart';
import 'package:iqrate/Controller/profile_controller.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Widgets/form_field_title.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  ProfileScreenController profileScreenController =
      Get.put(ProfileScreenController());

  @override
  void initState() {
    profileScreenController.getProfileDataOnEnteringProfilePage();
    profileScreenController.clearPasswordField();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double windowWidth = MediaQuery.of(context).size.width; //get screen width
    double windowHeight =
        MediaQuery.of(context).size.height; //get screen height

    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: AppColors.kSecondaryColor, // status bar color
    ));

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // Top container that shows the user image and text that shows 'Click here for eNameCard'
            TopContainer(
              windowWidth: windowWidth,
            ),
            // This widget contains the form in the profile page
            FormBody(
              windowHeight: windowHeight,
              profileScreenController: profileScreenController,
              windowWidth: windowWidth,
            )
          ],
        ),
      ),
    );
  }

// final int _index = 0;
// Widget packageCard(windowHeight, double windowWidth) {
//   return Container(
//     margin: EdgeInsets.symmetric(horizontal: windowWidth * .009),
//     decoration: BoxDecoration(
//         border: Border.all(
//           color: AppColors.red,
//         ),
//         borderRadius: BorderRadius.circular(25)),
//     child: Padding(
//       padding: const EdgeInsets.symmetric(horizontal: 25),
//       child: Column(
//         children: [
//           SizedBox(
//             height: windowHeight * 0.03,
//           ),
//           Text(
//             "BASIC",
//             style: TextStyles.basicTextStyle,
//           ),
//           const Divider(
//             color: Colors.grey,
//           ),
//           SizedBox(
//             height: windowHeight * 0.01,
//           ),
//           Text(
//             "Free",
//             style: TextStyles.freeTextStyle,
//           ),
//           SizedBox(
//             height: windowHeight * 0.015,
//           ),
//           Text(
//             "Per Annum",
//             style: TextStyles.perAnnumTitleTextStyle,
//           ),
//           SizedBox(
//             height: windowHeight * 0.04,
//           ),
//           Row(
//             children: [
//               Text("1 Devices", style: TextStyles.pricingTitleTextStyle),
//               const Spacer(),
//               SizedBox(
//                 child: Image.asset(
//                   'assets/images/select_pricing.png',
//                   height: 15,
//                   // semanticsLabel: 'Logo',
//                 ),
//               ),
//             ],
//           ),
//           SizedBox(
//             height: windowHeight * 0.02,
//           ),
//           Row(
//             children: [
//               Text("Clearance Fee 20%",
//                   style: TextStyles.pricingTitleTextStyle),
//               const Spacer(),
//               SizedBox(
//                 child: Image.asset(
//                   'assets/images/select_pricing.png',
//                   height: 15,
//                   // semanticsLabel: 'Logo',
//                 ),
//               ),
//             ],
//           ),
//           SizedBox(
//             height: windowHeight * 0.02,
//           ),
//           Row(
//             children: [
//               Text("Standard Rates", style: TextStyles.pricingTitleTextStyle),
//               const Spacer(),
//               SizedBox(
//                 child: Image.asset(
//                   'assets/images/select_pricing.png',
//                   height: 15,
//                   // semanticsLabel: 'Logo',
//                 ),
//               ),
//             ],
//           ),
//           SizedBox(
//             height: windowHeight * 0.02,
//           ),
//           Row(
//             children: [
//               Text("Excluding Profile analysis \n & Equity loan Calculator",
//                   style: TextStyles.pricingTitleTextStyle),
//               const Spacer(),
//               SizedBox(
//                 child: Image.asset(
//                   'assets/images/select_pricing.png',
//                   height: 15,
//                   // semanticsLabel: 'Logo',
//                 ),
//               ),
//             ],
//           ),
//           SizedBox(
//             height: windowHeight * 0.02,
//           ),
//           Row(
//             children: [
//               Text("Limit to 5 Leads",
//                   style: TextStyles.pricingTitleTextStyle),
//               const Spacer(),
//               SizedBox(
//                 child: Image.asset(
//                   'assets/images/select_pricing.png',
//                   height: 15,
//                   // semanticsLabel: 'Logo',
//                 ),
//               ),
//             ],
//           ),
//           SizedBox(
//             height: windowHeight * 0.02,
//           ),
//           Row(
//             children: [
//               Text("Lead Generation",
//                   style: TextStyles.pricingTitleTextStyle),
//               const Spacer(),
//               SizedBox(
//                 child: Image.asset(
//                   'assets/images/close_pricing.png',
//                   height: 15,
//                   // semanticsLabel: 'Logo',
//                 ),
//               ),
//             ],
//           ),
//           SizedBox(
//             height: windowHeight * 0.02,
//           ),
//           Row(
//             children: [
//               Text("Renewal Leads", style: TextStyles.pricingTitleTextStyle),
//               const Spacer(),
//               SizedBox(
//                 child: Image.asset(
//                   'assets/images/close_pricing.png',
//                   height: 15,
//                   // semanticsLabel: 'Logo',
//                 ),
//               ),
//             ],
//           ),
//           SizedBox(
//             height: windowHeight * 0.02,
//           ),
//           Row(
//             children: [
//               Text("TO Do List", style: TextStyles.pricingTitleTextStyle),
//               const Spacer(),
//               SizedBox(
//                 child: Image.asset(
//                   'assets/images/close_pricing.png',
//                   height: 15,
//                   // semanticsLabel: 'Logo',
//                 ),
//               ),
//             ],
//           ),
//         ],
//       ),
//     ),
//   );
// }
}

class TopContainer extends StatelessWidget {
  final double windowWidth;

  TopContainer({Key? key, required this.windowWidth}) : super(key: key);
  final ProfileScreenController profileScreenController =
      Get.put(ProfileScreenController());

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.kSecondaryColor,
      child: Padding(
        padding:
            const EdgeInsets.only(left: 20, right: 20, top: 60, bottom: 20),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                InkWell(
                    onTap: () {
                      profileScreenController.onCloseIconPress(
                          Get.parameters['from'] == 'GoogleAuth',
                          profileScreenController
                                      .userData.value.isMobileVerified ==
                                  0
                              ? false
                              : true);
                    },
                    child: SvgPicture.asset('assets/icons/closeEnclosed.svg'))
              ],
            ),
            // User profile image
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Obx(
                  () => GestureDetector(
                    onTap: () {
                      debugPrint("profileUrl::" +
                          profileScreenController.userData.value.photo
                              .toString());
                      profileScreenController.toAccountInformation();
                    },
                    child: CircleAvatar(
                      radius: 50,
                      backgroundColor: AppColors.white,
                      //backgroundImage: const AssetImage("assets/images/avater.jpeg"),
                      child: profileScreenController.userData.value.photo ==
                                  null ||
                              profileScreenController.userData.value.photo
                                  .toString()
                                  .isEmpty
                          ? Text(
                              profileScreenController.userData.value.fullName!
                                  .substring(0, 1),
                              style: TextStyles.profilePicTextStyle,
                            )
                          : CircleAvatar(
                              backgroundColor: Colors.transparent,
                              foregroundImage: NetworkImage(
                                  profileScreenController.userData.value.photo
                                      .toString()),
                              radius: 50,
                            ),
                    ),
                  ),
                ),

                SizedBox(
                    width: windowWidth * .06), // Padding between image and text
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  child: Text("Click here for eNamecard",
                      style: TextStyles.profileClickForENameCard),
                ),
                // const Spacer(),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

enum SectionType {
  mobileNumber,
  profile,
  emailAddress,
  resetPassword,
  bankAccount,
  hide
}

class FormBody extends StatefulWidget {
  final double windowHeight;
  final ProfileScreenController profileScreenController;
  final double windowWidth;

  const FormBody(
      {Key? key,
      required this.windowHeight,
      required this.profileScreenController,
      required this.windowWidth})
      : super(key: key);

  @override
  State<FormBody> createState() => _FormBodyState();
}

class _FormBodyState extends State<FormBody> {
  ACLController aclController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        // Update basic profile details for the user including name and dob
        getProfileWidget(),
        // Update mobile number
        getNumberWidget(),

        // Update email address
        widget.profileScreenController.userData.value.registerType != 'manual'
            ? Container()
            : getEmailAddressWidget(),
        // Reset password for the account
        widget.profileScreenController.userData.value.registerType != 'manual'
            ? Container()
            : getResetPasswordWidget(),
        // Update bank account information for the account
        getBankAccountWidget(),
        // 2FA section
        GestureDetector(
          onTap: () {
            // Navigate to enable2fa screen
            debugPrint("===============");
            debugPrint(widget.profileScreenController.userData.value
                .isTwoFactorAuthenticationEnabled
                .toString());
            debugPrint("===============");
            Get.toNamed(enable2faScreen, arguments: {
              "isTwoFactorAuthEnabled": widget.profileScreenController.userData
                  .value.isTwoFactorAuthenticationEnabled
            });
          },
          child: Card(
            elevation: 2,
            child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5.0),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 15.0, vertical: 17.0),
                  child: Obx(
                    () => Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        widget.profileScreenController.userData.value
                                    .isTwoFactorAuthenticationEnabled ==
                                true
                            ? Text(
                                'Disable 2 Factor Authentication',
                                style: TextStyles.profileexpansionTitle,
                              )
                            : Text(
                                'Enable 2 Factor Authentication',
                                style: TextStyles.profileexpansionTitle,
                              ),
                        const Icon(
                          Icons.chevron_right,
                          color: AppColors.grey625F5A,
                        )
                      ],
                    ),
                  ),
                )),
          ),
        ),
        // Connected accounts section
        // GestureDetector(
        //   onTap: () {
        //     // Navigate to settings page
        //   },
        //   child: Card(
        //     elevation: 2,
        //     child: Padding(
        //         padding: const EdgeInsets.symmetric(horizontal: 5.0),
        //         child: Padding(
        //           padding: const EdgeInsets.symmetric(
        //               horizontal: 15.0, vertical: 17.0),
        //           child: Row(
        //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //             children: [
        //               Text(
        //                 'Connected Accounts',
        //                 style: TextStyles.profileexpansionTitle,
        //               ),
        //               const Icon(
        //                 Icons.chevron_right,
        //                 color: AppColors.grey625F5A,
        //               )
        //             ],
        //           ),
        //         )),
        //   ),
        // ),
        // Delete account section
        GestureDetector(
          onTap: () {
            // Navigate to settings page
            widget.profileScreenController.deleteMyAccountDialog();
          },
          child: Card(
            elevation: 2,
            child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5.0),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 15.0, vertical: 17.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Delete My Account',
                        style: TextStyles.profileexpansionTitle,
                      ),
                      const Icon(
                        Icons.chevron_right,
                        color: AppColors.grey625F5A,
                      )
                    ],
                  ),
                )),
          ),
        )
      ],
    );
  }

  setHideShow(type) {
    widget.profileScreenController.isProfileExpanded.value = false;
    widget.profileScreenController.isNumberExpanded.value = false;
    widget.profileScreenController.isEmailExpanded.value = false;
    widget.profileScreenController.isPasswordExpanded.value = false;
    widget.profileScreenController.isBankExpanded.value = false;

    switch (type) {
      case SectionType.profile:
        widget.profileScreenController.isProfileExpanded.value = true;
        break;

      case SectionType.mobileNumber:
        widget.profileScreenController.isNumberExpanded.value = true;
        break;

      case SectionType.emailAddress:
        widget.profileScreenController.isEmailExpanded.value = true;
        break;

      case SectionType.resetPassword:
        widget.profileScreenController.isPasswordExpanded.value = true;
        break;

      case SectionType.bankAccount:
        widget.profileScreenController.isBankExpanded.value = true;
        break;
    }
  }

  Widget getProfileWidget() {
    return GestureDetector(
      onTap: () {
        if (widget.profileScreenController.isProfileExpanded.value) {
          setHideShow(SectionType.hide);
        } else {
          setHideShow(SectionType.profile);
        }
      },
      child: Card(
        elevation: 2,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5.0),
          child: Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 15.0, vertical: 17.0),
            child: Column(children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Profile',
                    style: TextStyles.profileexpansionTitle,
                  ),
                  Obx(
                    () => widget.profileScreenController.isProfileExpanded.value
                        ? const Icon(
                            Icons.keyboard_arrow_up_rounded,
                            color: AppColors.grey625F5A,
                          )
                        : const Icon(
                            Icons.keyboard_arrow_down,
                            color: AppColors.grey625F5A,
                          ),
                  )
                ],
              ),
              Obx(() => widget.profileScreenController.isProfileExpanded.value
                  ? Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Row(
                            children: const [
                              FormFieldTitle(title: 'Name as per NRIC*'),
                            ],
                          ),
                          SizedBox(height: widget.windowHeight * 0.01),
                          //Used for padding
                          RequireTextField(
                            type: Type.fullname,
                            labelText: "Enter your name",
                            controller:
                                widget.profileScreenController.nameController,
                            key: const Key("fullname"),
                          ),
                          SizedBox(height: widget.windowHeight * 0.01),
                          //Used for padding
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: const [
                              FormFieldTitle(title: 'Date of Birth*'),
                            ],
                          ),
                          SizedBox(height: widget.windowHeight * 0.01),
                          //Used for padding
                          RequireTextField(
                            type: Type.dob,
                            controller: widget
                                .profileScreenController.dateOfBirthController,
                            labelText: "Enter your date of birth",
                            key: const Key("dateOfBirth"),
                          ),
                          SizedBox(height: widget.windowHeight * 0.03),
                          //Used for padding
                          PrimaryButton(
                              borderRadius: 10.0,
                              windowHeight: widget.windowHeight,
                              windowWidth: widget.windowWidth,
                              buttonTitle: 'Update',
                              onPressed: () {
                                aclController.aclPutMethodCheck(
                                    '/user/updateNameDOB',
                                    widget.profileScreenController
                                        .onUpdateProfileButtonPressed);
                              }),
                          SizedBox(height: widget.windowHeight * 0.02),
                        ],
                      ),
                    )
                  : const SizedBox()),
            ]),
          ),
        ),
      ),
    );
  }

  Widget getNumberWidget() {
    return GestureDetector(
      onTap: () {
        if (widget.profileScreenController.isNumberExpanded.value) {
          setHideShow(SectionType.hide);
        } else {
          setHideShow(SectionType.mobileNumber);
        }
      },
      child: Card(
        elevation: 2,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5.0),
          child: Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 15.0, vertical: 17.0),
            child: Column(children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Update Mobile Number',
                    style: TextStyles.profileexpansionTitle,
                  ),
                  Obx(
                    () => widget.profileScreenController.isNumberExpanded.value
                        ? const Icon(
                            Icons.keyboard_arrow_up_rounded,
                            color: AppColors.grey625F5A,
                          )
                        : const Icon(
                            Icons.keyboard_arrow_down,
                            color: AppColors.grey625F5A,
                          ),
                  )
                ],
              ),
              Obx(() => widget.profileScreenController.isNumberExpanded.value
                  ? Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Obx(
                            () => Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const FormFieldTitle(title: 'Phone'),
                                widget.profileScreenController.userData.value
                                            .isMobileVerified ==
                                        1
                                    ? const Icon(
                                        Icons.verified,
                                        color: Colors.green,
                                      )
                                    : const Icon(
                                        Icons.error,
                                        color: Colors.red,
                                      ),
                              ],
                            ),
                          ),
                          SizedBox(height: widget.windowHeight * 0.01),
                          //Used for padding
                          RequireTextField(
                            type: Type.dropdownCountryCodeWithPhone,
                            controller:
                                widget.profileScreenController.phoneController,
                            countryCodeController: widget
                                .profileScreenController.countryCodeController,
                            countryCodeLabelController: widget
                                .profileScreenController
                                .countryCodeLabelController,
                            labelText: "Enter your phone number",
                            key: const Key("phone"),
                          ),
                          SizedBox(height: widget.windowHeight * 0.02),
                          //Used for padding
                          Obx(() => PrimaryButton(
                              borderRadius: 10.0,
                              windowHeight: widget.windowHeight,
                              windowWidth: widget.windowWidth,
                              buttonTitle: widget.profileScreenController
                                          .userData.value.isMobileVerified ==
                                      1
                                  ? 'Update'
                                  : 'Verify',
                              onPressed: () {
                                widget.profileScreenController
                                    .onUpdatePhoneButtonPressed();
                              })),
                          SizedBox(height: widget.windowHeight * 0.02),
                        ],
                      ),
                    )
                  : const SizedBox()),
            ]),
          ),
        ),
      ),
    );
  }

  Widget getEmailAddressWidget() {
    return GestureDetector(
      onTap: () {
        if (widget.profileScreenController.isEmailExpanded.value) {
          setHideShow(SectionType.hide);
        } else {
          setHideShow(SectionType.emailAddress);
        }
      },
      child: Card(
        elevation: 2,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5.0),
          child: Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 15.0, vertical: 17.0),
            child: Column(children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Update Email Address',
                    style: TextStyles.profileexpansionTitle,
                  ),
                  Obx(
                    () => widget.profileScreenController.isEmailExpanded.value
                        ? const Icon(
                            Icons.keyboard_arrow_up_rounded,
                            color: AppColors.grey625F5A,
                          )
                        : const Icon(
                            Icons.keyboard_arrow_down,
                            color: AppColors.grey625F5A,
                          ),
                  )
                ],
              ),
              Obx(() => widget.profileScreenController.isEmailExpanded.value
                  ? Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Obx(
                            () => Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const FormFieldTitle(title: 'Email'),
                                widget.profileScreenController.userData.value
                                            .isEmailVerified ==
                                        1
                                    ? const Icon(
                                        Icons.verified,
                                        color: Colors.green,
                                      )
                                    : const Icon(
                                        Icons.error,
                                        color: Colors.red,
                                      ),
                              ],
                            ),
                          ),
                          SizedBox(
                              height: widget.windowHeight *
                                  0.01), //Used for padding
                          RequireTextField(
                            type: Type.email,
                            controller:
                                widget.profileScreenController.emailController,
                            labelText: "Enter your email address",
                            key: const Key("email"),
                          ),
                          SizedBox(
                              height: widget.windowHeight *
                                  0.02), //Used for padding
                          PrimaryButton(
                              borderRadius: 10.0,
                              windowHeight: widget.windowHeight,
                              windowWidth: widget.windowWidth,
                              buttonTitle: 'Update',
                              onPressed: () {
                                widget.profileScreenController
                                    .onUpdateEmailButtonPressed();
                              }),
                          SizedBox(height: widget.windowHeight * 0.02),
                        ],
                      ),
                    )
                  : const SizedBox()),
            ]),
          ),
        ),
      ),
    );
  }

  Widget getResetPasswordWidget() {
    return GestureDetector(
      onTap: () {
        if (widget.profileScreenController.isPasswordExpanded.value) {
          setHideShow(SectionType.hide);
        } else {
          setHideShow(SectionType.resetPassword);
        }
      },
      child: Card(
        elevation: 2,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5.0),
          child: Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 15.0, vertical: 17.0),
            child: Column(children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Reset Password',
                    style: TextStyles.profileexpansionTitle,
                  ),
                  Obx(
                    () =>
                        widget.profileScreenController.isPasswordExpanded.value
                            ? const Icon(
                                Icons.keyboard_arrow_up_rounded,
                                color: AppColors.grey625F5A,
                              )
                            : const Icon(
                                Icons.keyboard_arrow_down,
                                color: AppColors.grey625F5A,
                              ),
                  )
                ],
              ),
              Obx(() => widget.profileScreenController.isPasswordExpanded.value
                  ? Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: const [
                              FormFieldTitle(title: 'Current password'),
                            ],
                          ),
                          SizedBox(
                              height: widget.windowHeight *
                                  0.01), //Used for padding
                          RequireTextField(
                            type: Type.passWord,
                            controller: widget
                                .profileScreenController.oldPasswordController,
                            labelText: "Alphanumeric (8+ characters)",
                            key: const Key("old_password"),
                          ),
                          SizedBox(
                              height: widget.windowHeight *
                                  0.01), //Used for padding
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: const [
                              FormFieldTitle(title: 'New password'),
                            ],
                          ),
                          SizedBox(
                              height: widget.windowHeight *
                                  0.01), //Used for padding
                          RequireTextField(
                            type: Type.passWord,
                            controller: widget
                                .profileScreenController.newPasswordController,
                            labelText: "Alphanumeric (8+ characters)",
                            key: const Key("password"),
                          ),
                          SizedBox(
                              height: widget.windowHeight *
                                  0.01), //Used for padding
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: const [
                              FormFieldTitle(title: 'Confirm password'),
                            ],
                          ),
                          SizedBox(
                              height: widget.windowHeight *
                                  0.01), //Used for padding
                          RequireTextField(
                            type: Type.passWord,
                            controller: widget.profileScreenController
                                .confirmPasswordController,
                            labelText: "Alphanumeric (8+ characters)",
                            key: const Key("confirmPassword"),
                          ),
                          SizedBox(
                              height: widget.windowHeight *
                                  0.02), //Used for padding
                          PrimaryButton(
                              borderRadius: 10.0,
                              windowHeight: widget.windowHeight,
                              windowWidth: widget.windowWidth,
                              buttonTitle: 'Update',
                              onPressed: () {
                                widget.profileScreenController
                                    .onChangePasswordButtonTap();
                              }),
                          SizedBox(height: widget.windowHeight * 0.02),
                        ],
                      ),
                    )
                  : const SizedBox()),
            ]),
          ),
        ),
      ),
    );
  }

  Widget getBankAccountWidget() {
    return GestureDetector(
      onTap: () {
        if (widget.profileScreenController.isBankExpanded.value) {
          setHideShow(SectionType.hide);
        } else {
          setHideShow(SectionType.bankAccount);
        }
      },
      child: Card(
        elevation: 2,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5.0),
          child: Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 15.0, vertical: 17.0),
            child: Column(children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Bank Account Information',
                    style: TextStyles.profileexpansionTitle,
                  ),
                  Obx(
                    () => widget.profileScreenController.isBankExpanded.value
                        ? const Icon(
                            Icons.keyboard_arrow_up_rounded,
                            color: AppColors.grey625F5A,
                          )
                        : const Icon(
                            Icons.keyboard_arrow_down,
                            color: AppColors.grey625F5A,
                          ),
                  )
                ],
              ),
              Obx(() => widget.profileScreenController.isBankExpanded.value
                  ? Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: const [
                              FormFieldTitle(title: 'Bank'),
                            ],
                          ),
                          SizedBox(
                              height: widget.windowHeight *
                                  0.01), //Used for padding

                          Container(
                            width: widget.windowWidth,
                            padding: const EdgeInsets.symmetric(
                                horizontal: 15, vertical: 3),
                            decoration: BoxDecoration(
                                border: Border.all(
                                    color: AppColors.formFieldBorderColor,
                                    width: 1.0),
                                borderRadius: const BorderRadius.all(
                                    Radius.circular(
                                        10) //                 <--- border radius here
                                    )),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton(
                                hint: const Text('Select Bank'),
                                // Not necessary for Option 1
                                value:
                                    widget.profileScreenController.selectedBank,
                                onChanged: (newValue) {
                                  setState(() {
                                    widget.profileScreenController
                                        .selectedBank = newValue.toString();
                                  });
                                },
                                items: widget.profileScreenController.banks
                                    .map((location) {
                                  return DropdownMenuItem(
                                    child: Text(
                                      location,
                                    ),
                                    value: location,
                                  );
                                }).toList(),
                              ),
                            ),
                          ),

                          SizedBox(
                              height: widget.windowHeight *
                                  0.01), //Used for padding
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: const [
                              FormFieldTitle(title: 'Bank Account No.'),
                            ],
                          ),
                          SizedBox(
                              height: widget.windowHeight *
                                  0.01), //Used for padding
                          RequireTextField(
                            type: Type.number,
                            labelText: "Enter bank account number",
                            controller: widget.profileScreenController
                                .accountNumberController,
                            key: const Key("accountNo"),
                          ),
                          SizedBox(
                              height: widget.windowHeight *
                                  0.02), //Used for padding
                          PrimaryButton(
                              borderRadius: 10.0,
                              windowHeight: widget.windowHeight,
                              windowWidth: widget.windowWidth,
                              buttonTitle: 'Update',
                              onPressed: () {
                                widget.profileScreenController
                                    .onUpdateBankDetailsButtonPress();
                              }),
                          SizedBox(height: widget.windowHeight * 0.02),
                        ],
                      ),
                    )
                  : const SizedBox()),
            ]),
          ),
        ),
      ),
    );
  }
}

// A widget that expands on tap and shows the necessary deilds and button to update the data
class ExpansionSection extends StatefulWidget {
  ExpansionSection(
      {Key? key,
      required this.title,
      required this.body,
      this.initiallyExpanded = false})
      : super(key: key);
  final String title;
  final List<Widget> body;
  bool initiallyExpanded;

  @override
  State<ExpansionSection> createState() => _ExpansionSectionState();
}

class _ExpansionSectionState extends State<ExpansionSection> {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 5.0),
        child: ExpansionTile(
          initiallyExpanded: widget.initiallyExpanded,
          onExpansionChanged: ((newState) {
            if (newState) {
              setState(() {
                const Duration(seconds: 20000);
                //selected = index;
              });
            } else {
              setState(() {
                //selected = -1;
              });
            }
          }),
          iconColor: AppColors.grey625F5A,
          collapsedIconColor: AppColors.grey625F5A,
          title: Text(
            widget.title,
            style: TextStyles.profileexpansionTitle,
          ),
          childrenPadding: const EdgeInsets.symmetric(horizontal: 20.0),
          children: widget.body,
        ),
      ),
    );
  }
}
