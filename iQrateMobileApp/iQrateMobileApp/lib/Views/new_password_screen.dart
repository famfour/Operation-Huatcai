import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/new_password_controller.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Router/route_constants.dart';
import 'package:iqrate/Widgets/form_field_title.dart';
import 'package:iqrate/Widgets/new_to_iqrate.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';

class NewPasswordScreen extends StatelessWidget {
  final NewPasswordController newPasswordController =
      Get.put(NewPasswordController());  //initialize new password controller
  NewPasswordScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double windowWidth = MediaQuery.of(context).size.width; //get screen width
    double windowHeight = MediaQuery.of(context).size.height; //get screen height
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: windowHeight * 0.10),
                Center(
                  child: SizedBox(
                    child: SvgPicture.asset(
                      'assets/images/forget.svg',
                      fit: BoxFit.contain,
                      // semanticsLabel: 'Logo',
                    ),
                  ),
                ),
                SizedBox(height: windowHeight * 0.07),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Create a new password",
                      style: TextStyles.authTextTitleStyle,
                    ),
                    SizedBox(height: windowHeight * 0.02),
                    Text(
                      "Your new password should be",
                      style: TextStyles.forgetInstructionTitleTextStyle,
                    ),
                    SizedBox(height: windowHeight * 0.01),
                    Text(
                      "different from previous one",
                      style: TextStyles.forgetInstructionTitleTextStyle,
                    ),
                    SizedBox(height: windowHeight * 0.03),
                    const FormFieldTitle(title: 'Password'),
                    SizedBox(height: windowHeight * 0.01),
                    RequireTextField(
                      key: const Key("password"),
                      type: Type.passWord,
                      controller: newPasswordController.passwordTextController,
                      labelText: "Enter new password",
                    ),
                    SizedBox(height: windowHeight * 0.03),
                    const FormFieldTitle(title: 'Confirm'),
                    SizedBox(height: windowHeight * 0.01),
                    RequireTextField(
                      type: Type.passWord,
                      controller:
                          newPasswordController.confirmPasswordTextController,
                      labelText: "Enter confirm password",
                    ),
                    SizedBox(height: windowHeight * 0.05),
                    Center(
                      child: PrimaryButton(
                        windowHeight: windowHeight,
                        windowWidth: windowWidth,
                        buttonTitle: "Submit",
                        onPressed: () {
                          newPasswordController.onSubmit(); //function submit for set new password
                        },
                      ),
                    ),
                    SizedBox(height: windowHeight * 0.12),
                    NewToiQrate(
                      tapGestureRecognizerToLogin: TapGestureRecognizer()
                        ..onTap = () {
                          debugPrint("clicked signup for free");
                          Get.toNamed(signUp); //navigate to signup screen
                        },
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
