import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:iqrate/Controller/cussess_view_controller.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Widgets/primary_button.dart';

// ignore: must_be_immutable
class SuccessView extends StatelessWidget {
  SuccessView({
    Key? key,
  }) : super(key: key);

  SuccessViewController successViewController = SuccessViewController();

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: windowWidth * 0.07),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.asset('assets/images/success.svg'),
            SizedBox(
              height: windowHeight * 0.05,
            ),
            Text(
              'Success!',
              style: TextStyles.introScreenTitlesSmall,
            ),
            SizedBox(
              height: windowHeight * 0.02,
            ),
            Text(
              'Congratulations! Your account has been registered, login to continue.',
              style: TextStyles.introScreenDescriptions,
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: windowHeight * 0.09,
            ),
            PrimaryButton(
                windowHeight: windowHeight,
                windowWidth: windowWidth,
                buttonTitle: 'Done',
                onPressed: successViewController.onTapPrimaryButton)
          ],
        ),
      ),
    );
  }
}
