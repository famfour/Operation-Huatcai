import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/selected_packages_controller.dart';
import 'package:iqrate/DeviceManager/assets.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Model/response_model.dart/fetch_lead_list_response_model.dart';
import 'package:iqrate/Utils/config.dart';
import 'package:iqrate/Views/generate_packages.dart';

import '../Service/GetControllers.dart';

class SelectedPackages extends StatefulWidget {
  final Result data;

  const SelectedPackages({Key? key, required this.data}) : super(key: key);

  @override
  _SelectedPackagesState createState() => _SelectedPackagesState();
}

class _SelectedPackagesState extends State<SelectedPackages> {
  late double windowHeight;
  late double windowWidth;

  SelectedPackagesController selectedPackagesController =
      Get.put(SelectedPackagesController());

  @override
  void initState() {
    initData();
    super.initState();
  }

  late Result leadData;

  initData() async {
    leadData = widget.data;
    selectedPackagesController.leadID = leadData.id.toString();

    await selectedPackagesController
        .getSelectedPackages(leadData.id.toString());
  }

  var leadProfileController = GetControllers.shared.getLeadProfileController();

  @override
  Widget build(BuildContext context) {
    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.width;
    return Obx(
      () => IgnorePointer(
        ignoring: !leadProfileController.clientPDPAStatus.value,
        child: expandableCard(
          "Selected Packages",
          false,
        ),
      ),
    );
  }

  Widget packageList() {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(windowHeight * 0.025),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // SizedBox(
          //   height: windowHeight * 0.01,
          // ),
          Obx(() => selectedPackagesController
                  .selectedPackageListModel.isNotEmpty
              ? ListView.builder(
                  itemCount: selectedPackagesController
                      .selectedPackageListModel.length,
                  shrinkWrap: true,
                  primary: false,
                  // This count needs to be dynamic
                  itemBuilder: (BuildContext context, int index) {
                    var item = selectedPackagesController
                        .selectedPackageListModel
                        .elementAt(index);
                    return Container(
                      margin: !item.isPublished!
                          ? const EdgeInsets.only(top: 0)
                          : const EdgeInsets.only(top: 20),
                      child: LoanPackageCardFixed(
                        isFromMoreRate: 4,
                        data: item,
                        image: item.bank!.logo == null
                            ? AppConfig.blankImage
                            : item.bank!.logo!,
                        headingSubText:
                            '${item.rateType.toString().replaceAll('_', ' ').capitalize} (${item.rateCategory.toString().replaceAll('_', ' ').capitalize})',
                        headingText: item.bank!.name!,
                        onChanged: (val) {},
                        checkBoxValue: false,
                        cardIndex: index,
                        isCheckboxShow: false,
                        onTapDelete: (val) {
                          debugPrint("onTapDelete:: $val");

                          selectedPackagesController
                              .deleteGeneratedPackage(item.id.toString());
                        },
                        isExpired: !item.isPublished!,
                        showDeleteIcon: leadProfileController
                                    .leadDataFromView!.coBrokeStatus!
                                    .trim() ==
                                'Co-Broke Sent'
                            ? false
                            : true,
                      ),
                    );
                  },
                )
              : Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                          height: windowHeight * 0.15,
                          child: SvgPicture.asset('assets/images/nodata.svg')),
                      Text(
                        'No data to show',
                        style: TextStyles.bankSubmissionCardTitleDisabled,
                      )
                    ],
                  ),
                )),
          SizedBox(
            height: windowHeight * 0.01,
          ),
        ],
      ),
    );
  }

  Widget expandableCard(String title, bool isDone) {
    return GestureDetector(
      onTap: () {
        selectedPackagesController.isExpand.toggle();
        // selectedPackagesController.getSelectedPackages(leadData.id.toString());
        GetControllers.shared.getGeneratePackagesController().isExpand.value =
            false;
        GetControllers.shared.getLoanDetailsController().isExpand.value = false;
        GetControllers.shared.getPropertyDetailsController().isExpand.value =
            false;
        GetControllers.shared.getLeadProfileController().isExpand.value = false;
        GetControllers.shared.getBankSubmissionController().isExpand.value =
            false;
        GetControllers.shared
            .getSubmitForPayoutViewController()
            .isExpand
            .value = false;
        GetControllers.shared.getLawFirmSubmissionController().isExpand.value =
            false;
        GetControllers.shared.getEmailLogController().isExpand.value = false;
        GetControllers.shared.getSubmitCoBrokeController().isExpand.value =
            false;
        if (selectedPackagesController.isExpand.value == true) {
          selectedPackagesController
              .getSelectedPackagesWhenExapnded(widget.data.id.toString());
        }
      },
      child: Obx(
        () => Container(
          color: Colors.white,
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(bottom: windowWidth * 0.02),
                padding: EdgeInsets.all(windowWidth * 0.05),
                decoration: BoxDecoration(
                    boxShadow: const <BoxShadow>[
                      BoxShadow(
                          color: Colors.black54,
                          blurRadius: 5.0,
                          offset: Offset(0.0, 0.3))
                    ],
                    color: !leadProfileController.clientPDPAStatus.value
                        ? AppColors.greyDEDEDE
                        : Colors.white),
                child: Row(
                  children: [
                    Text(title, style: TextStyles.basicTextStyle),
                    const Spacer(),
                    Obx(() => SvgPicture.asset(
                          selectedPackagesController
                                  .selectedPackageListModel.isNotEmpty
                              ? Assets.manageLoanDone
                              : Assets.manageLoanIncomplete,
                          height: 24,
                          width: 24,
                        )),
                    SizedBox(width: windowWidth * 0.03),
                    Icon(
                      selectedPackagesController.isExpand.value
                          ? Icons.keyboard_arrow_up_rounded
                          : Icons.keyboard_arrow_down,
                      size: 30,
                    )
                  ],
                ),
              ),
              selectedPackagesController.isExpand.value
                  ? packageList()
                  : Container()
            ],
          ),
        ),
      ),
    );
  }
}
