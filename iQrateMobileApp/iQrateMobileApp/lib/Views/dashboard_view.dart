import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/dashboard_controller.dart';
import 'package:iqrate/Widgets/primary_button.dart';

class DashboardView extends StatelessWidget {
  const DashboardView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    final DashboardController dashboardController = Get.find();
    // final DashboardViewController dashboardViewController =
    //     Get.put(DashboardViewController());
    return Scaffold(
      body: Center(
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          child: SizedBox(
            height: windowHeight * 0.2,
            width: windowWidth * 0.8,
            child: Padding(
              padding: const EdgeInsets.all(28.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'No leads to show.\nPress + or Click below to create\na lead',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: windowHeight * 0.017,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  PrimaryButton(
                    windowHeight: windowHeight,
                    windowWidth: windowWidth,
                    buttonTitle: "Create Now",
                    onPressed: () {
                      dashboardController.selectedIndex++;
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
