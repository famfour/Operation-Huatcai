import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:iqrate/Controller/loan_details_controller.dart';
import 'package:iqrate/DeviceManager/assets.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/screen_constants.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Model/response_model.dart/fetch_lead_list_response_model.dart';

import 'package:iqrate/Widgets/form_field_title.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';

import '../Service/GetControllers.dart';

class LoanDetails extends StatefulWidget {
  final Result data;

  const LoanDetails({required this.data, Key? key}) : super(key: key);

  @override
  _LoanDetailsState createState() => _LoanDetailsState();
}

class _LoanDetailsState extends State<LoanDetails> {
  late double windowHeight;
  late double windowWidth;
  final oCcy = NumberFormat("#,##0.00", "en_US");
  LoanDetailsController loanDetailsController =
      Get.put(LoanDetailsController());

  @override
  void initState() {
    initData();
    super.initState();
  }

  late Result leadData;

  initData() {
    loanDetailsController.leadData = widget.data;
    leadData = widget.data;

    //debugPrint(">>>>>>>>" + leadData.toJson().toString());

    loanDetailsController.leadID = leadData.id;

    loanDetailsController.currentInterestRateController.text =
        leadData.currentInterestRate.toString() == "null"
            ? ""
            : leadData.currentInterestRate.toString();

    loanDetailsController.is3Years.value = leadData.sellWithinThreeYears!;
    loanDetailsController.is200K.value = leadData.has200KDown!;

    // var a = double.tryParse(leadData.loanAmount.toString())!.toStringAsFixed(2);
    loanDetailsController.amountController.text =
        leadData.loanAmount.toString() == "null"
            ? ""
            : oCcy.format(double.tryParse(leadData.loanAmount.toString()));

    loanDetailsController.selectedLoanType =
        leadData.loanCategory.toString() == "null"
            ? loanDetailsController.defaultLoanType!
            : leadData.loanCategory.toString();

    loanDetailsController.isLoanDetailsFilled.value =
        leadData.loanCategory.toString() != "null" ? true : false;

    if (leadData.existingBank != null) {
      debugPrint(leadData.existingBank.toString());
      // loanDetailsController.selectedLoanType = leadData.existingBank.toString();
      loanDetailsController.selectedExistingBank = leadData.existingBank['id'];
    }

    if (loanDetailsController.amountController.text == "" &&
        loanDetailsController.currentInterestRateController.text == "") {
      loanDetailsController.isCompleted.value = false;
    } else {
      loanDetailsController.isCompleted.value = true;
    }
  }

  var leadProfileController = GetControllers.shared.getLeadProfileController();

  @override
  Widget build(BuildContext context) {
    windowHeight = MediaQuery.of(context).size.height;
    windowWidth = MediaQuery.of(context).size.width;
    return Obx(() => IgnorePointer(
          ignoring: !leadProfileController.clientPDPAStatus.value,
          child: expandableCard(
              "Loan Details", loanDetailsController.isCompleted.value),
        ));
  }

  Widget leadCreateView() {
    return Container(
        color: Colors.white,
        padding: EdgeInsets.all(windowHeight * 0.025),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const FormFieldTitle(title: 'Loan Type*'),
            SizedBox(height: windowHeight * 0.01),
            Container(
              width: windowWidth,
              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
              decoration: BoxDecoration(
                  border: Border.all(
                      color: AppColors.formFieldBorderColor, width: 1.0),
                  borderRadius: const BorderRadius.all(Radius.circular(
                          10) //                 <--- border radius here
                      )),
              child: IgnorePointer(
                ignoring: !widget.data.canEdit!, //readonly if can edit is false
                child: DropdownButtonHideUnderline(
                  child: DropdownButton(
                    hint: const Text('Select Loan Type'),
                    // Not necessary for Option 1
                    value: loanDetailsController.selectedLoanType,
                    onChanged: (newValue) {
                      debugPrint(newValue.toString());
                      setState(() {
                        loanDetailsController.selectedLoanType =
                            newValue.toString();
                      });
                    },
                    items: loanDetailsController.loanTypeValues.map((oneValue) {
                      debugPrint(oneValue);
                      return DropdownMenuItem(
                        child: Text(
                          oneValue,
                        ),
                        value: loanDetailsController.loanTypes[
                            loanDetailsController.loanTypeValues
                                .indexOf(oneValue)],
                      );
                    }).toList(),
                  ),
                ),
              ),
            ),
            SizedBox(height: windowHeight * 0.02),
            loanDetailsController.selectedLoanType ==
                        loanDetailsController.defaultLoanType2 ||
                    loanDetailsController.selectedLoanType ==
                        loanDetailsController.defaultLoanType
                ? const FormFieldTitle(title: 'Prefered Loan Amount*')
                : const FormFieldTitle(title: 'Outstanding Loan Amount*'),
            SizedBox(height: windowHeight * 0.01),
            // RequireTextField(
            //   type: Type.number,
            //   controller: loanDetailsController.amountController,
            //   labelText: "\$",
            //   key: const Key("loan ammount"),
            // ),
            RequireTextField(
              type: Type.money,
              controller: loanDetailsController.amountController,
              labelText: "\$",
              readOnly: !widget.data.canEdit!, //readonly if can edit is false
              key: const Key("loan ammount"),
            ),
            loanDetailsController.selectedLoanType !=
                        loanDetailsController.defaultLoanType &&
                    loanDetailsController.selectedLoanType !=
                        loanDetailsController.defaultLoanType2
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: windowHeight * 0.02),
                      const FormFieldTitle(title: 'Existing Bank*'),
                      SizedBox(height: windowHeight * 0.01),
                      Container(
                        width: windowWidth,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 15, vertical: 3),
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: AppColors.formFieldBorderColor,
                                width: 1.0),
                            borderRadius: const BorderRadius.all(
                                Radius.circular(
                                    10) //                 <--- border radius here
                                )),
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton(
                            hint: const Text('Select Bank'),
                            value: loanDetailsController.selectedExistingBank,
                            onChanged: (newValue) {
                              // debugPrint(newValue.toString());
                              setState(() {
                                loanDetailsController.selectedExistingBank =
                                    int.tryParse(newValue.toString());
                              });
                            },
                            items: loanDetailsController.existingBanks
                                .map((location) {
                              return DropdownMenuItem(
                                child: Text(location.name!),
                                value: location.id!,
                              );
                            }).toList(),
                          ),
                        ),
                      ),
                      SizedBox(height: windowHeight * 0.02),
                      const FormFieldTitle(title: 'Current Interest Rate*(%)'),
                      SizedBox(height: windowHeight * 0.01),
                      RequireTextField(
                        type: Type.number,
                        controller:
                            loanDetailsController.currentInterestRateController,
                        labelText: "%",
                        maxLength: 5,
                        key: const Key("interest"),
                      ),
                    ],
                  )
                : const SizedBox(),
            SizedBox(height: windowHeight * 0.02),
            CheckboxListTile(
              controlAffinity: ListTileControlAffinity.leading,
              title: Text(
                'Do you have an intention to sell your property within 3 years?',
                style: TextStyle(
                    fontWeight: FontWeight.w300,
                    fontSize: FontSize.s14,
                    color: Colors.black),
              ),
              autofocus: false,
              activeColor: const Color(0xffDF5356),
              checkColor: Colors.white,
              selected: loanDetailsController.is3Years.value,
              value: loanDetailsController.is3Years.value,
              onChanged: !widget.data.canEdit! //readonly if can edit is false
                  ? null
                  : (bool? value) {
                      setState(() {
                        loanDetailsController.is3Years.value = value!;
                      });
                    },
            ),
            CheckboxListTile(
              controlAffinity: ListTileControlAffinity.leading,
              title: Text(
                  'Are you willing to place a minimum \$200k fresh funds with the bank to enjoy better rates?',
                  style: TextStyle(
                      fontWeight: FontWeight.w300,
                      fontSize: FontSize.s14,
                      color: Colors.black)),
              autofocus: false,
              activeColor: const Color(0xffDF5356),
              //selectedTileColor: const Color(0xffDF5356),
              checkColor: Colors.white,
              selected: loanDetailsController.is200K.value,
              value: loanDetailsController.is200K.value,
              onChanged: !widget.data.canEdit! //readonly if can edit is false
                  ? null
                  : (bool? value) {
                      setState(() {
                        loanDetailsController.is200K.value = value!;
                      });
                    },
            ),
            widget.data.canEdit ??
                    false //Condition to hide widget if canEdit is false
                ? SizedBox(height: windowHeight * 0.02)
                : Container(),
            widget.data.canEdit ??
                    false //Condition to hide widget if canEdit is false
                ? PrimaryButton(
                    windowHeight: windowHeight,
                    windowWidth: windowWidth,
                    buttonTitle: "Save",
                    onPressed: () {
                      loanDetailsController.onTapSave();
                    },
                  )
                : Container(),
            widget.data.canEdit ??
                    false //Condition to hide widget if canEdit is false
                ? SizedBox(height: windowHeight * 0.03)
                : Container(),
          ],
        ));
  }

  Widget expandableCard(String title, bool isDone) {
    return GestureDetector(
      onTap: () {
        loanDetailsController.isExpand.toggle();
        GetControllers.shared.getLeadProfileController().isExpand.value = false;
        GetControllers.shared.getPropertyDetailsController().isExpand.value =
            false;
        GetControllers.shared.getGeneratePackagesController().isExpand.value =
            false;
        GetControllers.shared.getSelectedPackagesController().isExpand.value =
            false;
        GetControllers.shared.getBankSubmissionController().isExpand.value =
            false;
        GetControllers.shared
            .getSubmitForPayoutViewController()
            .isExpand
            .value = false;
        GetControllers.shared.getLawFirmSubmissionController().isExpand.value =
            false;
        GetControllers.shared.getEmailLogController().isExpand.value = false;
        GetControllers.shared.getSubmitCoBrokeController().isExpand.value =
            false;
      },
      child: Obx(() => Container(
            color: Colors.white,
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(bottom: windowWidth * 0.02),
                  padding: EdgeInsets.all(windowWidth * 0.05),
                  decoration: BoxDecoration(
                      boxShadow: const <BoxShadow>[
                        BoxShadow(
                            color: Colors.black54,
                            blurRadius: 5.0,
                            offset: Offset(0.0, 0.3))
                      ],
                      color: !leadProfileController.clientPDPAStatus.value
                          ? AppColors.greyDEDEDE
                          : Colors.white),
                  child: Row(
                    children: [
                      Text(title, style: TextStyles.basicTextStyle),
                      const Spacer(),
                      Obx(() => SvgPicture.asset(
                            loanDetailsController.isLoanDetailsFilled.value
                                ? Assets.manageLoanDone
                                : Assets.manageLoanIncomplete,
                            height: 24,
                            width: 24,
                          )),
                      SizedBox(width: windowWidth * 0.03),
                      Icon(
                        loanDetailsController.isExpand.value
                            ? Icons.keyboard_arrow_up_rounded
                            : Icons.keyboard_arrow_down,
                        size: 30,
                      )
                    ],
                  ),
                ),
                loanDetailsController.isExpand.value
                    ? leadCreateView()
                    : Container()
              ],
            ),
          )),
    );
  }
}
