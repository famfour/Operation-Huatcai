import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/saved_cards_controller.dart';
import 'package:iqrate/DeviceManager/screen_constants.dart';
import 'package:iqrate/Utils/text_formatter.dart';
import 'package:iqrate/Widgets/form_field_title.dart';
import 'package:iqrate/Widgets/primary_button.dart';
import 'package:iqrate/Widgets/require_text_field.dart';

class AddNewCardScreen extends StatelessWidget {
  AddNewCardScreen({Key? key}) : super(key: key);
  final SavedCardsController addCardController =
      Get.put(SavedCardsController());

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;
    double windowWidth = MediaQuery.of(context).size.width;
    return WillPopScope(
      onWillPop: () => addCardController.clearCardInfo(),
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          automaticallyImplyLeading: false,
          actions: [
            Padding(
              padding: const EdgeInsets.only(right: 8.0, top: 20),
              child: InkWell(
                onTap: () {
                  addCardController.clearCardInfo();
                  Get.back(); // Pop the existing page and go back to previous page
                },
                child: SvgPicture.asset(
                  'assets/icons/closeEnclosed.svg',
                  color: Colors.black,
                ),
              ),
            )
          ],
        ),
        body: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: Text(
                      'Add New Card',
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: FontSize.s22),
                    ),
                  ),
                  SizedBox(height: windowHeight * 0.02),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: const [
                      FormFieldTitle(title: 'Name on Card*'),
                    ],
                  ),
                  SizedBox(height: windowHeight * 0.005),
                  RequireTextField(
                    type: Type.firstName,
                    controller:
                        addCardController.nameOnCardTextEditingController,
                  ),
                  SizedBox(height: windowHeight * 0.02),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: const [
                      FormFieldTitle(title: 'Card Number*'),
                    ],
                  ),
                  SizedBox(height: windowHeight * 0.005),
                  RequireTextField(
                    numberInputFormatter: [
                      FilteringTextInputFormatter.digitsOnly,
                      CardNumberFormatter(),
                    ],
                    type: Type.number,
                    maxLength: 19,
                    controller:
                        addCardController.cardNumberTextEditingController,
                  ),
                  SizedBox(height: windowHeight * 0.01),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        flex: 5,
                        child: Padding(
                          padding: const EdgeInsets.only(right: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const FormFieldTitle(title: 'Expiry*'),
                              SizedBox(height: windowHeight * 0.005),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Expanded(
                                    flex: 3,
                                    child: RequireTextField(
                                      type: Type.expiry,
                                      maxLength: 2,
                                      labelText: 'MM',
                                      controller: addCardController
                                          .expirationMonthTextEditingController,
                                      onChanged: (String value) {
                                        //editCardController.cardDetails.value = editCardController.cardDetails.value.copyWith(expirationMonth: int.tryParse(value));
                                      },
                                    ),
                                  ),
                                  SizedBox(
                                    width: windowWidth * 0.04,
                                  ),
                                  Expanded(
                                    flex: 3,
                                    child: RequireTextField(
                                      type: Type.expiry,
                                      maxLength: 2,
                                      labelText: 'YY',
                                      controller: addCardController
                                          .expirationYearTextEditingController,
                                      onChanged: (String value) {
                                        //editCardController.cardDetails.value = editCardController.cardDetails.value.copyWith(expirationMonth: int.tryParse(value));
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 5,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: const [
                                  FormFieldTitle(title: 'CVC'),
                                ],
                              ),
                              SizedBox(height: windowHeight * 0.005),
                              RequireTextField(
                                // numberInputFormatter: const [],
                                type: Type.number,
                                maxLength: 3,
                                labelText: 'CVC',
                                controller:
                                    addCardController.cvcTextEditingController,
                                onChanged: (String value) {
                                  //addCardController.cardDetails.value = widget.controller.cardDetails.value.copyWith(cvc: value);
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.start,
                  //   children: const [
                  //     FormFieldTitle(title: 'Country'),
                  //   ],
                  // ),
                  // SizedBox(height: windowHeight * 0.005),
                  // RequireTextField(
                  //   type: Type.dropDownCountry,
                  //   controller: addCardController.countryTextEditingController,
                  //   onChanged: (String value) {},
                  // ),
                  /*Obx(() => CheckboxListTile(
                        title: const Text("Save card for future use."),
                        value: addCardController.checkedValue.value,
                        activeColor: Colors.black,
                        contentPadding: EdgeInsets.zero,
                        onChanged: (newValue) {
                          addCardController.checkedValue.value = newValue!;
                        },
                        controlAffinity: ListTileControlAffinity
                            .leading, //  <-- leading Checkbox
                      )),*/
                  SizedBox(height: windowHeight * 0.02),
                  Center(
                    child: PrimaryButton(
                      windowHeight: windowHeight,
                      windowWidth: windowWidth,
                      buttonTitle: "Save",
                      onPressed: () {
                        addCardController.onTapSave(false);
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
