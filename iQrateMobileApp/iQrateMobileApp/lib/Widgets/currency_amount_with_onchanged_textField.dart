// ignore_for_file: unused_element, file_names

import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:iqrate/Widgets/require_text_field.dart';

class CurrencyAmountWithOnchangedTextField extends StatefulWidget {
  final TextEditingController textEditingController;
  final String labelText;
  final Function(String?)? onChanged;
  const CurrencyAmountWithOnchangedTextField(
      {Key? key,
      required this.textEditingController,
      required this.labelText,
      this.onChanged})
      : super(key: key);
  @override
  _CurrencyAmountWithOnchangedTextFieldState createState() =>
      _CurrencyAmountWithOnchangedTextFieldState();
}

class _CurrencyAmountWithOnchangedTextFieldState
    extends State<CurrencyAmountWithOnchangedTextField> {
  static const _locale = 'en';
  String _formatNumber(String s) =>
      NumberFormat.decimalPattern(_locale).format(double.parse(s));

  @override
  Widget build(BuildContext context) {
    return RequireTextField(
      type: Type.money,
      hintText: widget.labelText,
      controller: widget.textEditingController,
      labelText: widget.labelText,
      onChanged: widget.onChanged,
    );
  }
}
