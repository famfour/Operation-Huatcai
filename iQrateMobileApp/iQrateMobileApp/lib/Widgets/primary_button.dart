import 'package:flutter/material.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';

class PrimaryButton extends StatelessWidget {
  const PrimaryButton(
      {Key? key,
      required this.windowHeight, //Get windowHeight from DeviceManager
      required this.windowWidth, //Get windowWidth from DeviceManager
      required this.buttonTitle, //Button title
      required this.onPressed, //Button onPressed
      this.isDisable = false, //Button enable/disable
      this.borderRadius = 20 //Button border radius
      })
      : super(key: key);
  final double windowHeight;
  final double windowWidth;
  final String buttonTitle;
  final bool isDisable;
  final Function onPressed;
  final double borderRadius;
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: isDisable
          ? null
          : () {
              onPressed();
            },
      style: ElevatedButton.styleFrom(
          padding: EdgeInsets.zero,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20))),
      child: Ink(
        decoration: BoxDecoration(
            gradient: const LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: [
                Color(0XFFE09207),
                Color(0XFFF9484A),
              ],
            ),
            borderRadius: BorderRadius.circular(20)),
        child: Container(
          // width: 300,
          // height: 100,
          alignment: Alignment.center,
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Text(buttonTitle, style: TextStyles.primaryButtonTextStyle),
          ),
        ),
      ),
    );
  }
}

//
// class PrimaryButton extends StatelessWidget {
//   const PrimaryButton(
//       {Key? key,
//         required this.windowHeight, //Get windowHeight from DeviceManager
//         required this.windowWidth, //Get windowWidth from DeviceManager
//         required this.buttonTitle, //Button title
//         required this.onPressed, //Button onPressed
//         this.isDisable = false, //Button enable/disable
//         this.borderRadius = 20 //Button border radius
//       })
//       : super(key: key);
//
//   final double windowHeight;
//   final double windowWidth;
//   final String buttonTitle;
//   final bool isDisable;
//   final Function onPressed;
//   final double borderRadius;
//
//   // @override
//   // Widget build(BuildContext context) {
//   //   return GestureDetector(
//   //     onTap: () {
//   //       HapticFeedback.heavyImpact();
//   //       if (isDisable) {
//   //         return;
//   //       }
//   //       onPressed(); //Call onPressed function
//   //     },
//   //     child: Card(
//   //       elevation: 5,
//   //       shape: RoundedRectangleBorder(
//   //         borderRadius: BorderRadius.circular(borderRadius),
//   //       ),
//   //       child: Container(
//   //         height: windowHeight * 0.06,
//   //         width: windowWidth,
//   //         decoration: BoxDecoration(
//   //           borderRadius: BorderRadius.circular(borderRadius),
//   //           gradient: LinearGradient(
//   //             begin: Alignment.centerLeft,
//   //             end: Alignment.centerRight,
//   //             colors: [
//   //               Color(isDisable ? 0xFF636363 : 0XFFE09207),
//   //               Color(isDisable ? 0xFF7B838E : 0XFFF9484A),
//   //             ],
//   //           ),
//   //         ),
//   //         child: Center(
//   //           child: Text(buttonTitle, style: TextStyles.primaryButtonTextStyle),
//   //         ),
//   //       ),
//   //     ),
//   //   );
//   // }
//
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       height: windowHeight * 0.06,
//       width: windowWidth,
//       decoration: BoxDecoration(
//         borderRadius: BorderRadius.circular(borderRadius),
//         gradient: LinearGradient(
//           begin: Alignment.centerLeft,
//           end: Alignment.centerRight,
//           colors: [
//             Color(isDisable ? 0xFF636363 : 0XFFE09207),
//             Color(isDisable ? 0xFF7B838E : 0XFFF9484A),
//           ],
//         ),
//       ),
//       child: OutlineButton(
//         splashColor: Colors.white,
//         shape: RoundedRectangleBorder(
//             borderRadius: BorderRadius.circular(borderRadius)),
//         onPressed: () {
//           HapticFeedback.heavyImpact();
//           if (isDisable) {
//             return;
//           }
//           onPressed(); //Call onPressed function
//         },
//         child: Center(
//           child: Text(buttonTitle, style: TextStyles.primaryButtonTextStyle),
//         ),
//       ),
//     );
//   }
// }
