// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';

// Custom Default appBar widget which is used in multiple screen
class DefaultAppBarCallBack extends StatelessWidget
    implements PreferredSizeWidget {
  DefaultAppBarCallBack(
      {Key? key,
      required this.title,
      required this.windowHeight,
      required this.onTapClose})
      : super(key: key);
  final String title;
  double windowHeight;
  final void Function() onTapClose;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0,
      automaticallyImplyLeading: false,
      centerTitle: false,
      title: Padding(
        padding: const EdgeInsets.only(top: 20.0),
        child: Text(
          title, // Appbar title
          style: TextStyles.appBarTextStyle, // Appbar title textStyle
        ),
      ),
      actions: [
        Padding(
          padding: const EdgeInsets.only(right: 8.0, top: 20),
          child: InkWell(
            onTap: () {
              onTapClose();
              //Get.back(); // Pop the existing page and go back to previous page
            },
            child: SvgPicture.asset('assets/icons/closeEnclosed.svg'),
          ),
        )
      ],
      backgroundColor: const Color(0xFFDF5356), //Appbar color
      shape: const RoundedRectangleBorder(
        //Appbar shape
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(30),
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(windowHeight);
}

class DefaultAppBar extends StatelessWidget implements PreferredSizeWidget {
  DefaultAppBar({Key? key, required this.title, required this.windowHeight})
      : super(key: key);
  final String title;
  double windowHeight;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0,
      automaticallyImplyLeading: false,
      centerTitle: false,
      title: Padding(
        padding: const EdgeInsets.only(top: 20.0),
        child: Text(
          title, // Appbar title
          style: TextStyles.appBarTextStyle, // Appbar title textStyle
        ),
      ),
      actions: [
        Padding(
          padding: const EdgeInsets.only(right: 8.0, top: 20),
          child: InkWell(
            onTap: () {
              Get.back(); // Pop the existing page and go back to previous page
            },
            child: SvgPicture.asset('assets/icons/closeEnclosed.svg'),
          ),
        )
      ],
      backgroundColor: const Color(0xFFDF5356), //Appbar color
      shape: const RoundedRectangleBorder(
        //Appbar shape
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(30),
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(windowHeight);
}

class DefaultAppBar2 extends StatelessWidget implements PreferredSizeWidget {
  const DefaultAppBar2(
      {Key? key, required this.title, required this.windowHeight})
      : super(key: key);
  final String title;
  final double windowHeight;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0,
      automaticallyImplyLeading: false,
      centerTitle: false,
      title: Padding(
        padding: const EdgeInsets.only(left: 20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: windowHeight * 0.1,
            ),
            Text(
              title, // Appbar title
              style: TextStyles.appBarTextStyle, // Appbar title textStyle
            ),
            SizedBox(
              height: windowHeight * 0.01,
            ),
            Text(
              "(Loan Eligibility)", // Appbar title
              style: TextStyles.appBarTextStyle2, // Appbar title textStyle
            ),
            SizedBox(
              height: windowHeight * 0.03,
            ),
          ],
        ),
      ),
      actions: [
        Padding(
          padding: const EdgeInsets.only(right: 8.0, top: 20),
          child: InkWell(
            onTap: () {
              Get.back(); // Pop the existing page and go back to previous page
            },
            child: SvgPicture.asset('assets/icons/closeEnclosed.svg'),
          ),
        )
      ],
      backgroundColor: const Color(0xFFDF5356), //Appbar color
      shape: const RoundedRectangleBorder(
        //Appbar shape
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(30),
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(windowHeight);
}

class DefaultAppBar3 extends StatelessWidget implements PreferredSizeWidget {
  const DefaultAppBar3(
      {Key? key, required this.title, required this.windowHeight})
      : super(key: key);
  final String title;
  final double windowHeight;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0,
      automaticallyImplyLeading: false,
      centerTitle: false,
      title: Padding(
        padding: const EdgeInsets.only(left: 20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const SizedBox(
              height: 10,
            ),
            Text(
              title, // Appbar title
              style: TextStyles.appBarTextStyle, // Appbar title textStyle
            )
          ],
        ),
      ),
      actions: [
        Padding(
          padding: const EdgeInsets.only(right: 8.0, top: 20),
          child: InkWell(
            onTap: () {
              Get.back(); // Pop the existing page and go back to previous page
            },
            child: SvgPicture.asset('assets/icons/closeEnclosed.svg'),
          ),
        )
      ],
      backgroundColor: const Color(0xFFDF5356), //Appbar color
      shape: const RoundedRectangleBorder(
        //Appbar shape
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(30),
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(windowHeight);
}
