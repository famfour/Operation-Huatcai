import 'package:flutter/material.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';

class CalculatorButton extends StatelessWidget {
  const CalculatorButton({
    Key? key,
    required this.windowHeight, //get the height of the screen
    required this.windowWidth, //get the width of the screen
    required this.kGradientBoxDecoration, //get the gradient of the button
    required this.kInnerDecoration, //get the inner decoration of the button
    required this.onPressed, //get the onPressed function
    required this.buttonTitle, //get the button title
  }) : super(key: key);

  final double windowHeight;
  final double windowWidth;
  final BoxDecoration kGradientBoxDecoration;
  final BoxDecoration kInnerDecoration;
  final Function onPressed;
  final String buttonTitle;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onPressed(); //call the onPressed function
      },
      child: Container(
        height: windowHeight * 0.07,
        width: windowWidth * 0.9,
        decoration: kGradientBoxDecoration,
        child: Container(
          padding: const EdgeInsets.only(left: 15),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              buttonTitle,
              style: TextStyles.secondaryButtonTextStyle, //get the text style
            ),
          ),
          decoration: kInnerDecoration,
        ),
      ),
    );
  }
}
