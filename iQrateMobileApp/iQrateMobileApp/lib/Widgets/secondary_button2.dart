import 'package:flutter/material.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';

class SecondaryButton2 extends StatelessWidget {
  const SecondaryButton2({
    Key? key,
    required this.windowHeight, //get the height of the screen
    required this.windowWidth, //get the width of the screen
    required this.kGradientBoxDecoration, //get the gradient of the button
    required this.kInnerDecoration, //get the inner decoration of the button
    required this.onPressed, //get the onPressed function
    required this.buttonTitle, //get the button title
  }) : super(key: key);

  final double windowHeight;
  final double windowWidth;
  final BoxDecoration kGradientBoxDecoration;
  final BoxDecoration kInnerDecoration;
  final Function onPressed;
  final String buttonTitle;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: windowWidth,
      child: TextButton(
          onPressed: () {
            onPressed();
          },
          child: Text(buttonTitle, style: TextStyles.primaryButtonTextStyle),
          style: ButtonStyle(
              side: MaterialStateProperty.all(
                  const BorderSide(width: 1, color: Colors.black)),
              foregroundColor: MaterialStateProperty.all(Colors.black),
              padding: MaterialStateProperty.all(
                  const EdgeInsets.symmetric(vertical: 20, horizontal: 20)),
              textStyle: MaterialStateProperty.all(
                  TextStyles.secondaryButtonTextStyle))),
    );
  }
}
