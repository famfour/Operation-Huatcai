import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';

// Custom Default appBar widget which is used in multiple screen
// ignore: must_be_immutable
class DefaultAppBarTwo extends StatelessWidget implements PreferredSizeWidget {
  DefaultAppBarTwo(
      {Key? key,
      required this.title,
      required this.windowHeight,
      required this.windowWidth,
      required this.icons})
      : super(key: key);
  final String title;
  double windowHeight;
  double windowWidth;
  List<Widget> icons;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0,
      automaticallyImplyLeading: false,
      centerTitle: false,
      flexibleSpace: Padding(
        padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 20),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  title, // Appbar title
                  style: TextStyles.appBarTextStyle, // Appbar title textStyle
                ),
                InkWell(
                  onTap: () {
                    Get.back(); // Pop the existing page and go back to previous page
                  },
                  child: SvgPicture.asset('assets/icons/closeEnclosed.svg'),
                )
              ],
            ),
            Divider(
              color: Colors.white,
              thickness: windowWidth * 0.002,
            ),
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.end,
            //   children: icons,
            // ),
          ],
        ),
      ),
      bottom: PreferredSize(
        preferredSize: Size.fromHeight(windowHeight),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: icons,
        ),
      ),
      backgroundColor: const Color(0xFFDF5356), //Appbar color
      shape: const RoundedRectangleBorder(
        //Appbar shape
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(30),
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(windowHeight);
}
