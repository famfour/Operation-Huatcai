import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/leads_view_controller.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';

class LeadsAppBarOptions extends StatefulWidget {
  const LeadsAppBarOptions({
    Key? key,
    required this.windowWidth,
  }) : super(key: key);

  final double windowWidth;

  @override
  State<LeadsAppBarOptions> createState() => _LeadsAppBarOptionsState();
}

class _LeadsAppBarOptionsState extends State<LeadsAppBarOptions> {
  @override
  Widget build(BuildContext context) {
    final LeadsViewController leadsViewController = Get.find();

    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Lead',
              style: TextStyles.appBarTextStyle,
            ),
            // GestureDetector(
            //   onTap: () {},
            //   child: Icon(
            //     Icons.notifications_outlined,
            //     size: widget.windowWidth * 0.06,
            //     color: Colors.white,
            //   ),
            // ),
          ],
        ),
        Divider(
          color: Colors.white,
          thickness: widget.windowWidth * 0.002,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            GestureDetector(
              onTap: () {
                leadsViewController.selectedIndex.value = 6;
                debugPrint('Function for Filter');
                leadsViewController.showFilter.value = true;
              },
              child: Icon(
                Icons.filter_list_alt,
                size: widget.windowWidth * 0.06,
                color: Colors.white,
              ),
            ),
            SizedBox(width: widget.windowWidth * 0.025),
            GestureDetector(
              onTap: () {
                // Show pop-up based on the length of leads list
                leadsViewController.listOfLeads.isNotEmpty
                    ? leadsViewController.selectedIndex.value =
                        2 // Don't show tutorial pop-up if lead list length > 0
                    : leadsViewController.selectedIndex.value =
                        0; // Show tutorial pop-up if lead list length == 0
                debugPrint(">>>>>>selectedIndex>>>>>" +
                    leadsViewController.selectedIndex.value.toString());
              },
              child: Icon(
                Icons.add_box_outlined,
                size: widget.windowWidth * 0.06,
                color: Colors.white,
              ),
            ),
          ],
        ),
      ],
    );
  }
}
