import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/continue_with_google_button_controller.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';

class AppleSignIn extends StatelessWidget {
  final ContinueWithGoogleButtonController continueWithGoogleButtonController =
      Get.put(ContinueWithGoogleButtonController());
  AppleSignIn({
    Key? key,
    required this.windowHeight,
    required this.windowWidth,
    required this.kGradientBoxDecoration,
    required this.kInnerDecoration,
    required this.onPressed,
    required this.buttonTitle,
  }) : super(key: key);

  final double windowHeight;
  final double windowWidth;
  final BoxDecoration kGradientBoxDecoration;
  final BoxDecoration kInnerDecoration;
  final Function onPressed;
  final String buttonTitle;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onPressed();
      },
      child: Container(
        height: windowHeight * 0.06,
        width: windowWidth * 0.9,
        decoration: kGradientBoxDecoration,
        child: Padding(
          padding: const EdgeInsets.all(1.0),
          child: Container(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset("assets/logos/3.0x/apple.png", width: 22,),
                  Text(
                    buttonTitle,
                    style: TextStyles.secondaryButtonTextStyle,
                  ),
                ],
              ),
            ),
            decoration: kInnerDecoration,
          ),
        ),
      ),
    );
  }
}
