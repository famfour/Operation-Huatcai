import 'package:flutter/material.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';

class FormFieldTitle extends StatelessWidget {
  const FormFieldTitle({
    Key? key,
    required this.title,
  }) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: TextStyles.formFieldTitleTextStyle,
    );
  }
}
