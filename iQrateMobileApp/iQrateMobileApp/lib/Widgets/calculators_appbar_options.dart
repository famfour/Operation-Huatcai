import 'package:flutter/material.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';

class CalculatorsAppBarOptions extends StatelessWidget {
  const CalculatorsAppBarOptions({
    Key? key,
    required this.windowWidth,
  }) : super(key: key);

  final double windowWidth;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Calculator',
              style: TextStyles.appBarTextStyle,
            ),
            // GestureDetector(
            //   onTap: () {},
            //   child: Icon(
            //     Icons.notifications_outlined,
            //     size: windowWidth * 0.06,
            //     color: Colors.white,
            //   ),
            // ),
          ],
        )
      ],
    );
  }
}
