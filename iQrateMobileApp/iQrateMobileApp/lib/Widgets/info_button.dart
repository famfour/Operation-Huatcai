import 'package:flutter/material.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';

class InfoButton extends StatelessWidget {
  const InfoButton({
    Key? key,
    required this.windowHeight, //Get windowHeight from DeviceManager
    required this.windowWidth, //Get windowWidth from DeviceManager
    required this.buttonTitle, //Button title
    required this.onPressed,
    required this.backgroundColor,
    required this.icon, //Button onPressed
    this.iconDisable = false, //Button onPressed
  }) : super(key: key);

  final double windowHeight;
  final double windowWidth;
  final String buttonTitle;
  final Function onPressed;
  final Color backgroundColor;
  final IconData icon;
  final bool iconDisable;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onPressed(); //Call onPressed function
      },
      child: Container(
        height: windowHeight * 0.06,
        width: windowWidth * 0.9,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: backgroundColor,
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(buttonTitle, style: TextStyles.primaryButtonTextStyle),
            SizedBox(width: windowWidth * 0.01),
            iconDisable ? const SizedBox() : Icon(icon, color: Colors.white),
          ],
        ),
      ),
    );
  }
}
