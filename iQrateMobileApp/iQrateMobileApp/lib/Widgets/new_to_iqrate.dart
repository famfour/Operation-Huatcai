import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';

class NewToiQrate extends StatelessWidget {
  const NewToiQrate({
    Key? key,
    required this.tapGestureRecognizerToLogin,
  }) : super(key: key);

  final TapGestureRecognizer tapGestureRecognizerToLogin;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: RichText(
        text: TextSpan(
          children: [
            TextSpan(
              style: TextStyles.alreeadyHaveAnAccount,
              text: "New to iQrate?",
            ),
            TextSpan(
              style: TextStyles.loginTextButtonStyle,
              text: " Register for free",
              recognizer: tapGestureRecognizerToLogin,
            ),
          ],
        ),
      ),
    );
  }
}
