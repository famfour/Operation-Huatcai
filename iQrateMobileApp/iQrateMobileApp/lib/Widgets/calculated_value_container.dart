import 'package:flutter/material.dart';

import '../DeviceManager/text_styles.dart';

class CalculatedViewContainer extends StatelessWidget {
  const CalculatedViewContainer({
    Key? key,
    required this.windowWidth,
    required this.windowHeight,
    required this.calculatedValue,
    required this.title,
  }) : super(key: key);

  final double windowWidth;
  final double windowHeight;
  final Widget calculatedValue;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: windowWidth,
      height: windowHeight * 0.15,
      decoration: BoxDecoration(
        color: const Color(0XFFF6F6F6),
        borderRadius: BorderRadius.circular(20),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: windowHeight * 0.02,
            ),
            Text(
              title,
              style: TextStyles.calculatorHeadingsTextStyle,
            ),
            const Divider(
              color: Color(0XFFBEBEBE),
              thickness: 2,
            ),
            SizedBox(
              height: windowHeight * 0.02,
            ),
            calculatedValue,
          ],
        ),
      ),
    );
  }
}
