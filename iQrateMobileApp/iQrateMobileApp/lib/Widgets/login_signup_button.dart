import 'package:flutter/material.dart';

class LoginSignUpButton extends StatelessWidget {
  //If correct data for the required fields are not inserted, the app will keep crashing.
  const LoginSignUpButton({
    Key? key,
    required this.windowHeight,
    required this.windowWidth,
    required this.onPress, //Function when the button is tapped upon.
    required this.buttonTitle, //Title for the button.
  }) : super(key: key);

  final double windowHeight;
  final double windowWidth;
  final Function onPress;
  final String buttonTitle;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onPress(); // This is defined so that we can pass any function required to the parameter.
      },
      child: Container(
        height: windowHeight * 0.06,
        width: windowWidth * 0.8,
        decoration: BoxDecoration(
          color: Colors.blue.shade800,
          borderRadius: BorderRadius.circular(30),
        ),
        child: Center(
          child: Text(
            buttonTitle, //This is the title. Anything can be passed here.
            style: const TextStyle(color: Colors.white, fontSize: 20),
          ),
        ),
      ),
    );
  }
}
