import 'package:flutter/material.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/screen_constants.dart';

class BoxEntryTextField extends StatefulWidget {
  final String? lastPin;
  final int fields;
  final ValueChanged<String>? onSubmit;
  final num fieldWidth;
  final double fontSize;
  final bool isTextObscure;
  final bool showFieldAsBox;
  // ignore: prefer_typing_uninitialized_variables
  final cursorColor; // Leaving the data type dynamic for adding Color or Material Color
  // ignore: prefer_typing_uninitialized_variables
  final boxColor;
  // ignore: prefer_typing_uninitialized_variables
  final textColor;

  const BoxEntryTextField({
    Key? key,
    this.lastPin,
    this.fields = 6,
    this.onSubmit,
    this.fieldWidth = 25.0,
    this.fontSize = 18.0,
    this.isTextObscure = false,
    this.showFieldAsBox = true,
    this.cursorColor = AppColors
        .background, // Adding a Material Color so that if the user want black, it get accepted too
    this.boxColor = AppColors.white,
    this.textColor = AppColors.background,
  })  : assert(fields > 0),
        super(key: key);

  @override
  State createState() {
    return BoxEntryTextFieldState();
  }
}

class BoxEntryTextFieldState extends State<BoxEntryTextField> {
  final List<String> _pin = <String>[];
  final List<FocusNode> _focusNodes = <FocusNode>[];
  final List<TextEditingController> _textControllers =
      <TextEditingController>[];

  Widget textFields = Container();

  @override
  void initState() {
    super.initState();
    for (int i = 0; i < widget.fields; i++) {
      _textControllers.add(TextEditingController());
      _pin.add("");
      _focusNodes.add(FocusNode());
    }
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      setState(() {
        if (widget.lastPin != null) {
          for (var i = 0; i < widget.lastPin!.length; i++) {
            _pin[i] = widget.lastPin![i];
          }
        }
        textFields = generateTextFields(context);
      });
    });
  }

  @override
  void dispose() {
    for (var t in _textControllers) {
      t.dispose();
    }
    super.dispose();
  }

  Widget generateTextFields(BuildContext context) {
    List<Widget> textFields = List.generate(widget.fields, (int i) {
      return buildTextField(i, context, i == 0);
    });

    FocusScope.of(context).requestFocus(_focusNodes[0]);

    return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        verticalDirection: VerticalDirection.down,
        children: textFields);
  }

  void clearTextFields() {
    for (var tEditController in _textControllers) {
      tEditController.clear();
    }
    _pin.clear();
  }

  Widget buildTextField(int i, BuildContext context, [bool autofocus = false]) {
    _focusNodes[i] = FocusNode();
    _textControllers[i] = TextEditingController();
    if (widget.lastPin != null) {
      _textControllers[i].text = widget.lastPin![i];
    }

    _focusNodes[i].addListener(() {
      if (_focusNodes[i].hasFocus) {}
    });

    // final String lastDigit = _textControllers[i].text;

    return Container(
      width: MediaQuery.of(context).size.width / 8,
      height: MediaQuery.of(context).size.height / 8,
      margin: const EdgeInsets.all(5.0),
      child: TextField(
        controller: _textControllers[i],
        keyboardType: const TextInputType.numberWithOptions(
          decimal: true,
          signed: false,
        ),
        textAlign: TextAlign.center,
        cursorColor: widget.cursorColor,
        maxLength: 1,
        autofocus: autofocus,
        style: TextStyle(
          color: AppColors.black1D232A,
          fontSize: FontSize.s14,
          fontWeight: FontWeight.bold,
        ),
        focusNode: _focusNodes[i],
        obscureText: widget.isTextObscure,
        decoration: InputDecoration(
            // contentPadding: EdgeInsets.zero,
            filled: true,
            fillColor: AppColors.white,
            counterText: "",
            enabledBorder: widget.showFieldAsBox
                ? const OutlineInputBorder(
                    borderSide: BorderSide(
                        width: 1.0, color: AppColors.formFieldBorderColor))
                : null,
            focusedBorder: widget.showFieldAsBox
                ? const OutlineInputBorder(
                    borderSide: BorderSide(
                        width: 1.0, color: AppColors.formFieldBorderColor))
                : null),
        onChanged: (String str) {
          setState(() {
            _pin[i] = str;
          });
          if (i + 1 != widget.fields) {
            _focusNodes[i].unfocus();
            if (_pin[i] == '') {
              FocusScope.of(context).requestFocus(_focusNodes[i - 1]);
            } else {
              FocusScope.of(context).requestFocus(_focusNodes[i + 1]);
            }
          } else {
            _focusNodes[i].unfocus();
            if (_pin[i] == '') {
              FocusScope.of(context).requestFocus(_focusNodes[i - 1]);
            }
          }
          if (_pin.every((String digit) => digit != '')) {
            widget.onSubmit!(_pin.join());
          }
        },
        onSubmitted: (String str) {
          if (_pin.every((String digit) => digit != '')) {
            widget.onSubmit!(_pin.join());
          }
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return textFields;
  }
}
