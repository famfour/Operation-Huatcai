import 'package:flutter/material.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';

class ResendButton extends StatelessWidget {
  const ResendButton({
    Key? key,
    required this.buttonTitle, //Button title
    required this.onPressed, //Button onPressed
    this.isDisable = false, //Button enable/disable
  }) : super(key: key);

  final String buttonTitle;
  final bool isDisable;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () {
        if (isDisable) {
          return;
        }
        onPressed(); //Call onPressed function
      },
      child: Text(
        buttonTitle,
        style: TextStyles.resendButtonTextStyle,
      ),
    );
  }
}
