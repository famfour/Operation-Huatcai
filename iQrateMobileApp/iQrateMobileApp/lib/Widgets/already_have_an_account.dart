import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';

class AlreadyHaveAnAccount extends StatelessWidget {
  const AlreadyHaveAnAccount({
    Key? key,
    required this.tapGestureRecognizerToLogin,
  }) : super(key: key);

  final TapGestureRecognizer tapGestureRecognizerToLogin;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: RichText(
        text: TextSpan(
          children: [
            TextSpan(
              style: TextStyles.alreeadyHaveAnAccount,
              text: "Already Have an Account?",
            ),
            TextSpan(
              style: TextStyles.loginTextButtonStyle,
              text: " Login",
              recognizer: tapGestureRecognizerToLogin,
            ),
          ],
        ),
      ),
    );
  }
}
