import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/DeviceManager/assets.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';

class CommonAppBarOptions extends StatelessWidget {
  final String title;
  const CommonAppBarOptions({
    Key? key,
    required this.windowWidth, required this.title,
  }) : super(key: key);

  final double windowWidth;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              title,
              style: TextStyles.appBarTextStyle,
            ),
            GestureDetector(
              onTap: () {Get.back();},
              child: SvgPicture.asset(
                Assets.manageLoanClose,
              ),
            ),
          ],
        ),
      ],
    );
  }
}
