import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';

class ForgetPassword extends StatelessWidget {
  final String title;
  const ForgetPassword({
    Key? key,
    required this.tapGestureRecognizer, required this.title,
  }) : super(key: key);

  final TapGestureRecognizer tapGestureRecognizer;

  @override
  Widget build(BuildContext context) {
    return Align(
      key: key,
      alignment: Alignment.centerRight,
      child: RichText(
        text: TextSpan(
          children: [
            TextSpan(
              style: TextStyles.alreeadyHaveAnAccount,
              text: title,
              recognizer: tapGestureRecognizer,
            )
          ],
        ),
      ),
    );
  }
}
