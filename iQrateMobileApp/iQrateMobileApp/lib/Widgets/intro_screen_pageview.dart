import 'package:flutter/material.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';

class IntroScreenPageViewScreens extends StatelessWidget {
  const IntroScreenPageViewScreens(
      {Key? key,
      required this.windowHeight,
      required this.image,
      required this.titlePartOne,
      required this.titlePartTwo,
      required this.description,
      required this.underlinedText})
      : super(key: key);

  final double windowHeight;
  final String image;
  final String titlePartOne;
  final String titlePartTwo;
  final String description;
  final String underlinedText;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(windowHeight * 0.04),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Center(
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                  text: titlePartOne,
                  style: TextStyles.introScreenTitles,
                  children: [
                    TextSpan(
                        text: underlinedText,
                        style: TextStyles.introScreenTitlesUnderlined),
                    TextSpan(
                        text: titlePartTwo,
                        style: TextStyles.introScreenTitles),
                  ]),
            ),
          ),
          const SizedBox(height: 10.0),
          Center(
            child: Text(
              description,
              style: TextStyles.introScreenDescriptions,
              textAlign: TextAlign.center,
            ),
          ),
          const SizedBox(height: 30.0),
          Center(
            child: Image(
              // fit: BoxFit.fitWidth,
              image: AssetImage(
                image,
              ),
              height: windowHeight * 0.4,
              // width: 300.0,
            ),
          ),
        ],
      ),
    );
  }
}
