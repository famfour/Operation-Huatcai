import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';
import 'package:iqrate/Service/GetControllers.dart';

// This variant of appbar is shown in the 3rd tab(Rates).
class DashboardAppBarOptions3 extends StatelessWidget {
  const DashboardAppBarOptions3({
    Key? key,
    required this.windowWidth,
  }) : super(key: key);

  final double windowWidth;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Rates',
              style: TextStyles.appBarTextStyle,
            ),
            // GestureDetector(
            //   onTap: () {},
            //   child: SvgPicture.asset(
            //     Assets.manageLoanClose,
            //   ),
            // ),
          ],
        ),
        Divider(
          color: Colors.white,
          thickness: windowWidth * 0.002,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            SizedBox(width: windowWidth * 0.025),
            GestureDetector(
              onTap: () {
                //this is export icon???
                debugPrint("export icon rate view clicked, show popup");
                GetControllers.shared
                    .getRatesViewController()
                    .showFilter
                    .value = 2;
              },
              child: SvgPicture.asset('assets/icons/share.svg'),
            ),
            SizedBox(width: windowWidth * 0.025),
            GestureDetector(
              onTap: () {
                debugPrint("filter icon rate view clicked show Export");
                //reset filter after click
                /*GetControllers.shared
                    .getRatesViewController()
                    .resetFilterDefault();*/
                GetControllers.shared
                    .getRatesViewController()
                    .showFilter
                    .value = 1;
              },
              child: Icon(
                Icons.filter_list_alt,
                size: windowWidth * 0.06,
                color: Colors.white,
              ),
            ),
          ],
        ),
      ],
    );
  }
}
