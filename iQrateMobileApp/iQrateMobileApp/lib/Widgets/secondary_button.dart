import 'package:flutter/material.dart';
import 'package:iqrate/DeviceManager/colors.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';

class SecondaryButton extends StatelessWidget {
  const SecondaryButton({
    Key? key,
    required this.windowHeight, //get the height of the screen
    required this.windowWidth, //get the width of the screen
    required this.kGradientBoxDecoration, //get the gradient of the button
    required this.kInnerDecoration, //get the inner decoration of the button
    required this.onPressed, //get the onPressed function
    required this.buttonTitle, //get the button title
  }) : super(key: key);

  final double windowHeight;
  final double windowWidth;
  final BoxDecoration kGradientBoxDecoration;
  final BoxDecoration kInnerDecoration;
  final Function onPressed;
  final String buttonTitle;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {
        onPressed();
      },
      style: ElevatedButton.styleFrom(
          // primary: Colors.red,
          side: const BorderSide(color: AppColors.red, width: 1),
          padding: EdgeInsets.zero,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20))),
      child: Ink(
        decoration: BoxDecoration(
            // gradient: const LinearGradient(
            //   begin: Alignment.centerLeft,
            //   end: Alignment.centerRight,
            //   colors: [
            //     // Colors.transparent,
            //     // Color(0XFFF9484A),
            //     Colors.transparent,
            //   ],
            // ),
            shape: BoxShape.rectangle,
            color: Colors.white,
            borderRadius: BorderRadius.circular(20)),
        child: Container(
          // width: 300,
          // height: 100,

          alignment: Alignment.center,
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child:
                Text(buttonTitle, style: TextStyles.secondaryButtonTextStyle),
          ),
        ),
      ),
    );
  }
}


//
// class SecondaryButton extends StatelessWidget {
//   const SecondaryButton({
//     Key? key,
//     required this.windowHeight, //get the height of the screen
//     required this.windowWidth, //get the width of the screen
//     required this.kGradientBoxDecoration, //get the gradient of the button
//     required this.kInnerDecoration, //get the inner decoration of the button
//     required this.onPressed, //get the onPressed function
//     required this.buttonTitle, //get the button title
//   }) : super(key: key);
//
//   final double windowHeight;
//   final double windowWidth;
//   final BoxDecoration kGradientBoxDecoration;
//   final BoxDecoration kInnerDecoration;
//   final Function onPressed;
//   final String buttonTitle;
//
//   @override
//   Widget build(BuildContext context) {
//     return GestureDetector(
//       onTap: () {
//         HapticFeedback.heavyImpact();
//         onPressed(); //call the onPressed function
//       },
//       child: Card(
//         elevation: 5,
//         shape: RoundedRectangleBorder(
//           borderRadius: BorderRadius.circular(30),
//         ),
//         child: Container(
//           height: windowHeight * 0.06,
//           width: windowWidth,
//           decoration: kGradientBoxDecoration,
//           child: Padding(
//             padding: const EdgeInsets.all(1.0),
//             child: Container(
//               child: Center(
//                 child: Text(
//                   buttonTitle,
//                   style:
//                   TextStyles.secondaryButtonTextStyle, //get the text style
//                 ),
//               ),
//               decoration: kInnerDecoration,
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }
