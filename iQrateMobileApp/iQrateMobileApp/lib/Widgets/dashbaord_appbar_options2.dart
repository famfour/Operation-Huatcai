import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:iqrate/Controller/dashboard_view_controller2.dart';
import 'package:iqrate/DeviceManager/assets.dart';

class DashboardAppBarOptions2 extends StatelessWidget {
  const DashboardAppBarOptions2({
    Key? key,
    required this.windowWidth,
  }) : super(key: key);

  final double windowWidth;

  @override
  Widget build(BuildContext context) {
    double windowHeight = MediaQuery.of(context).size.height;

    // Commented this code throwing error in testing
    // final DashboardController2 dashboardController2 = Get.find();

    final DashboardController2 dashboardController2 =
        Get.put(DashboardController2());

    return Column(
      //mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Expanded(
          child: SizedBox(height: windowHeight * 0.06),
          flex: 1,
        ),
        SvgPicture.asset(
          Assets.logoWhite,
          height: 100,
        ),
        SizedBox(height: windowHeight * 0.03),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text(
              'Total estimated Payout',
              style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.w400,
                color: Colors.white,
              ),
            ),
            SizedBox(width: windowWidth * 0.03),
            GestureDetector(
              onTap: () {
                dashboardController2.onTapPayoutInfo();
              },
              child: Icon(
                Icons.info_outline,
                size: windowWidth * 0.06,
                color: Colors.white,
              ),
            ),
          ],
        ),
        SizedBox(height: windowHeight * 0.03),
        const Text(
          "\$ 0",
          style: TextStyle(
            fontSize: 40,
            fontWeight: FontWeight.w700,
            color: Colors.white,
          ),
        ),
        SizedBox(height: windowHeight * 0.03),
        // PrimaryButton(
        //   windowHeight: windowHeight,
        //   windowWidth: windowWidth * .85,
        //   onPressed: () {
        //     dashboardController2.onTapCalendar();
        //   },
        //   buttonTitle: 'CALENDAR',
        // ),
      ],
    );
  }
}
