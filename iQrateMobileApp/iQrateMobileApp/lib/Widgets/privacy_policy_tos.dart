import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:iqrate/DeviceManager/text_styles.dart';

class TosAndPrivacyPolicy extends StatelessWidget {
  const TosAndPrivacyPolicy({
    Key? key,
    required this.tapGestureRecognizerPrivacyPolicy,
    required this.tapGestureRecognizerTos,
  }) : super(key: key);

  final TapGestureRecognizer tapGestureRecognizerPrivacyPolicy;
  final TapGestureRecognizer tapGestureRecognizerTos;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: RichText(
        text: TextSpan(
          children: [
            TextSpan(
              style: TextStyles.tosAndPrivacyPolicyStyle, //Textstyle for TOS
              text: "By continuing, you agree to IQrate’s\n",
            ),
            TextSpan(
              style:
                  TextStyles.tosAndPrivacyPolicyStyleLink, // Textstyle for link
              text: "Terms & Conditions",
              recognizer:
                  tapGestureRecognizerTos, // Tap gesture recognizer for TOS. Implement the function here
            ),
            TextSpan(
              style: TextStyles
                  .tosAndPrivacyPolicyStyle, //Textstyle for Privacy Policy
              text: " & ",
            ),
            TextSpan(
              style: TextStyles.tosAndPrivacyPolicyStyleLink,
              text: " Privacy Policy",
              recognizer:
                  tapGestureRecognizerPrivacyPolicy, // Tap gesture recognizer for Privacy Policy. Implement the function here
            ),
          ],
        ),
      ),
    );
  }
}
