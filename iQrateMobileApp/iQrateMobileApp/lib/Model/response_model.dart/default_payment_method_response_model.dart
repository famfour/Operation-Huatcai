// ignore_for_file: unnecessary_question_mark, prefer_void_to_null, unnecessary_this, prefer_collection_literals

class DefaultPaymentMethodResponseModel {
  String? id;
  String? object;
  Null? address;
  int? balance;
  int? created;
  String? currency;
  Null? defaultSource;
  bool? delinquent;
  Null? description;
  Null? discount;
  String? email;
  String? invoicePrefix;
  InvoiceSettings? invoiceSettings;
  bool? livemode;
  Metadata? metadata;
  String? name;
  int? nextInvoiceSequence;
  Null? phone;
  List<Null>? preferredLocales;
  Null? shipping;
  String? taxExempt;
  Null? testClock;

  DefaultPaymentMethodResponseModel(
      {this.id,
      this.object,
      this.address,
      this.balance,
      this.created,
      this.currency,
      this.defaultSource,
      this.delinquent,
      this.description,
      this.discount,
      this.email,
      this.invoicePrefix,
      this.invoiceSettings,
      this.livemode,
      this.metadata,
      this.name,
      this.nextInvoiceSequence,
      this.phone,
      this.preferredLocales,
      this.shipping,
      this.taxExempt,
      this.testClock});

  DefaultPaymentMethodResponseModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    object = json['object'];
    address = json['address'];
    balance = json['balance'];
    created = json['created'];
    currency = json['currency'];
    defaultSource = json['default_source'];
    delinquent = json['delinquent'];
    description = json['description'];
    discount = json['discount'];
    email = json['email'];
    invoicePrefix = json['invoice_prefix'];
    invoiceSettings = json['invoice_settings'] != null
        ? InvoiceSettings.fromJson(json['invoice_settings'])
        : null;
    livemode = json['livemode'];
    metadata =
        json['metadata'] != null ? Metadata.fromJson(json['metadata']) : null;
    name = json['name'];
    nextInvoiceSequence = json['next_invoice_sequence'];
    phone = json['phone'];
    if (json['preferred_locales'] != null) {
      preferredLocales = <Null>[];
      //json['preferred_locales'].forEach((v) { preferredLocales!.add(new Null.fromJson(v)); });
    }
    shipping = json['shipping'];
    taxExempt = json['tax_exempt'];
    testClock = json['test_clock'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = this.id;
    data['object'] = this.object;
    data['address'] = this.address;
    data['balance'] = this.balance;
    data['created'] = this.created;
    data['currency'] = this.currency;
    data['default_source'] = this.defaultSource;
    data['delinquent'] = this.delinquent;
    data['description'] = this.description;
    data['discount'] = this.discount;
    data['email'] = this.email;
    data['invoice_prefix'] = this.invoicePrefix;
    if (this.invoiceSettings != null) {
      data['invoice_settings'] = this.invoiceSettings!.toJson();
    }
    data['livemode'] = this.livemode;
    if (this.metadata != null) {
      data['metadata'] = this.metadata!.toJson();
    }
    data['name'] = this.name;
    data['next_invoice_sequence'] = this.nextInvoiceSequence;
    data['phone'] = this.phone;
    if (this.preferredLocales != null) {
      //data['preferred_locales'] = this.preferredLocales!.map((v) => v.toJson()).toList();
    }
    data['shipping'] = this.shipping;
    data['tax_exempt'] = this.taxExempt;
    data['test_clock'] = this.testClock;
    return data;
  }
}

class InvoiceSettings {
  Null? customFields;
  String? defaultPaymentMethod;
  Null? footer;

  InvoiceSettings({this.customFields, this.defaultPaymentMethod, this.footer});

  InvoiceSettings.fromJson(Map<String, dynamic> json) {
    customFields = json['custom_fields'];
    defaultPaymentMethod = json['default_payment_method'];
    footer = json['footer'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['custom_fields'] = this.customFields;
    data['default_payment_method'] = this.defaultPaymentMethod;
    data['footer'] = this.footer;
    return data;
  }
}

class Metadata {
  Metadata();

  Metadata.fromJson(Map<String, dynamic> json);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    return data;
  }
}
