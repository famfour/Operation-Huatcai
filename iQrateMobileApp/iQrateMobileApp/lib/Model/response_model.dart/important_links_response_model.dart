class ResourcesLinksResponseModel {
  List<OneResponse>? data;
  ResourcesLinksResponseModel({this.data});

  factory ResourcesLinksResponseModel.fromJson(json) {
    return ResourcesLinksResponseModel(
        data: List<OneResponse>.from(json.map((x) {
      return OneResponse.fromJson(x);
    })));
  }
}

class OneResponse {
  int? displayOrder;
  int? id;
  String? name;
  String? link;
  List<Document?>? document;

  OneResponse(
      {this.displayOrder,
      this.id,
      this.name,
      this.link,
      required this.document});

  factory OneResponse.fromJson(json) {
    return OneResponse(
        displayOrder: json['display_order'],
        id: json['id'],
        name: json['name'],
        link: json['link'],
        document: json["document"] != null
            ? List<Document>.from(
                json["document"].map((x) => Document.fromJson(x)))
            : null);
  }
}

class Document {
  Document(
      {this.id,
      this.name,
      this.alternativeText,
      this.caption,
      this.hash,
      this.ext,
      this.url,
      this.provider});
  int? id;
  String? name;
  String? alternativeText;
  String? caption;
  String? hash;
  String? ext;
  String? url;
  String? provider;

  factory Document.fromJson(Map<String, dynamic> json) => Document(
        id: json["id"],
        name: json["name"],
        alternativeText: json["alternativeText"],
        caption: json["caption"],
        hash: json["hash"],
        ext: json["ext"],
        url: json["url"],
        provider: json["provider"],
      );
}
