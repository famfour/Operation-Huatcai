class LawfirmListResponseModel {
  List<LawFirmModel>? lawFirmList;
  LawfirmListResponseModel({this.lawFirmList});

  factory LawfirmListResponseModel.fromJson(json) {
    return LawfirmListResponseModel(
        lawFirmList: List<LawFirmModel>.from(
            json.map((data) => LawFirmModel.fromJson(data))));
  }
}

class LawFirmModel {
  int? id;
  String? name;
  String? email;
  int? legalFee;

  LawFirmModel({this.id, this.name, this.email, this.legalFee});

  factory LawFirmModel.fromJson(json) {
    return LawFirmModel(id: json['id'], name: json['name'], email: json['email'], legalFee:json['legal_fee']);
  }
}
