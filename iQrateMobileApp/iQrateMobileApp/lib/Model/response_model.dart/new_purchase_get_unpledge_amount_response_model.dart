import 'dart:convert';

NewPurchaseGetUnpledgeAmountResponseModel welcomeFromJson(String str) =>
    NewPurchaseGetUnpledgeAmountResponseModel.fromJson(json.decode(str));

String welcomeToJson(NewPurchaseGetUnpledgeAmountResponseModel data) =>
    json.encode(data.toJson());

class NewPurchaseGetUnpledgeAmountResponseModel {
  NewPurchaseGetUnpledgeAmountResponseModel({
    this.unfludgeAmount,
  });

  String? unfludgeAmount;

  factory NewPurchaseGetUnpledgeAmountResponseModel.fromJson(
          Map<String, dynamic> json) =>
      NewPurchaseGetUnpledgeAmountResponseModel(
        unfludgeAmount: json["unfludge_amount"],
      );

  Map<String, dynamic> toJson() => {
        "unfludge_amount": unfludgeAmount,
      };
}
