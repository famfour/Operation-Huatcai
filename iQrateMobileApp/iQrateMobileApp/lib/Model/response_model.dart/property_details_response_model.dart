class PropertyDetailsResponseModel {
  PropertyDetailsResponseModel({
    this.id,
    this.lead,
    this.postalCode,
    this.propertyStatus,
    this.propertyType,
    this.purchasePrice,
    this.streetName,
    this.unitNumber,
    this.projectName,
  });

  PropertyDetailsResponseModel.fromJson(dynamic json) {
    id = json['id'];
    lead = json['lead'];
    postalCode = json['postal_code'];
    propertyStatus = json['property_status'];
    propertyType = json['property_type'];
    purchasePrice = json['purchase_price'];
    streetName = json['street_name'];
    unitNumber = json['unit_number'];
    projectName = json['project_name'];
  }
  int? id;
  int? lead;
  String? postalCode;
  String? propertyStatus;
  String? propertyType;
  double? purchasePrice;
  String? streetName;
  String? unitNumber;
  String? projectName;
  PropertyDetailsResponseModel copyWith({
    int? id,
    int? lead,
    String? postalCode,
    String? propertyStatus,
    String? propertyType,
    double? purchasePrice,
    String? streetName,
    String? unitNumber,
    String? projectName,
  }) =>
      PropertyDetailsResponseModel(
        id: id ?? this.id,
        lead: lead ?? this.lead,
        postalCode: postalCode ?? this.postalCode,
        propertyStatus: propertyStatus ?? this.propertyStatus,
        propertyType: propertyType ?? this.propertyType,
        purchasePrice: purchasePrice ?? this.purchasePrice,
        streetName: streetName ?? this.streetName,
        unitNumber: unitNumber ?? this.unitNumber,
        projectName: projectName ?? this.projectName,
      );
}
