// ignore_for_file: file_names

import 'banker_model.dart';

class ResponseBankers {
  ResponseBankers({
    required this.count,
    this.next,
    this.previous,
    required this.results,
  });

  ResponseBankers.fromJson(dynamic json) {
    count = json['count'];
    next = json['next'];
    previous = json['previous'];
    if (json['results'] != null) {
      results = [];
      json['results'].forEach((v) {
        results?.add(BankerModel.fromJson(v));
      });
    }
  }
  int? count;
  dynamic next;
  dynamic previous;
  List<BankerModel>? results;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['count'] = count;
    map['next'] = next;
    map['previous'] = previous;
    if (results != null) {
      map['results'] = results?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}
