// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

Welcome welcomeFromJson(String str) => Welcome.fromJson(json.decode(str));

String welcomeToJson(Welcome data) => json.encode(data.toJson());

class Welcome {
  Welcome({
    this.id,
    this.lead,
    this.propertyType,
    this.purchasePrice,
    this.propertyStatus,
    this.postalCode,
    this.streetName,
    this.unitNumber,
    this.projectName,
  });

  int? id;
  int? lead;
  String? propertyType;
  int? purchasePrice;
  String? propertyStatus;
  String? postalCode;
  String? streetName;
  String? unitNumber;
  String? projectName;

  factory Welcome.fromJson(Map<String, dynamic> json) => Welcome(
        id: json["id"],
        lead: json["lead"],
        propertyType: json["property_type"],
        purchasePrice: json["purchase_price"],
        propertyStatus: json["property_status"],
        postalCode: json["postal_code"],
        streetName: json["street_name"],
        unitNumber: json["unit_number"],
        projectName: json["project_name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "lead": lead,
        "property_type": propertyType,
        "purchase_price": purchasePrice,
        "property_status": propertyStatus,
        "postal_code": postalCode,
        "street_name": streetName,
        "unit_number": unitNumber,
        "project_name": projectName,
      };
}
