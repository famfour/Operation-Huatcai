class BankSubmissionEmailToLeadResponseModel {
  String? name;
  String? email;
  String? subject;
  String? content;
  List? attachments;

  BankSubmissionEmailToLeadResponseModel(
      {this.name, this.email, this.subject, this.content, this.attachments});

  factory BankSubmissionEmailToLeadResponseModel.fromJson(json) =>
      BankSubmissionEmailToLeadResponseModel(
          name: json["name"],
          email: json["email"],
          subject: json["subject"],
          content: json["content"],
          attachments: json["attachments"]);
}
