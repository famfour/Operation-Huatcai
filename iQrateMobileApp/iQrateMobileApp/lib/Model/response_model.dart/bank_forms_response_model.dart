class BankFormsResponseModel {
  int? count;
  String? next;
  String? previous;
  List<OneBankForm>? results;

  BankFormsResponseModel({this.count, this.next, this.previous, this.results});

  factory BankFormsResponseModel.fromJson(json) {
    return BankFormsResponseModel(
        count: json['count'],
        next: json['next'],
        previous: json['previous'],
        results: List<OneBankForm>.from(
            json['results'].map((data) => OneBankForm.fromJson(data))));
  }
}

class OneBankForm {
  int? id;
  String? name;
  String? logo;
  List<FormsList>? bankFormUrls;
  OneBankForm({this.id, this.name, this.logo, this.bankFormUrls});
  factory OneBankForm.fromJson(json) {
    return OneBankForm(
        id: json['id'],
        name: json['name'],
        logo: json['logo'],
        bankFormUrls: List<FormsList>.from(
            json['forms'].map((data) => FormsList.fromJson(data))));
  }
}

class FormsList {
  String? name;
  String? url;
  FormsList({this.name, this.url});
  factory FormsList.fromJson(json) {
    return FormsList(name: json['name'], url: json['url']);
  }
}
