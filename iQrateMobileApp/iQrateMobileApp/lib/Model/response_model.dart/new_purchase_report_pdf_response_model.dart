import 'dart:convert';

NewPurchaseReportPdfResponseModel welcomeFromJson(String str) =>
    NewPurchaseReportPdfResponseModel.fromJson(json.decode(str));

String welcomeToJson(NewPurchaseReportPdfResponseModel data) =>
    json.encode(data.toJson());

class NewPurchaseReportPdfResponseModel {
  NewPurchaseReportPdfResponseModel({
    this.url,
  });

  String? url;

  factory NewPurchaseReportPdfResponseModel.fromJson(
          Map<String, dynamic> json) =>
      NewPurchaseReportPdfResponseModel(
        url: json["url"],
      );

  Map<String, dynamic> toJson() => {
        "url": url,
      };
}
