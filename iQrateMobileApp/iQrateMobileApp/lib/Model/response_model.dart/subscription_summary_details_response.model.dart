// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

SubscriptionSummaryDetailsResponseModel welcomeFromJson(String str) =>
    SubscriptionSummaryDetailsResponseModel.fromJson(json.decode(str));

String welcomeToJson(SubscriptionSummaryDetailsResponseModel data) =>
    json.encode(data.toJson());

class SubscriptionSummaryDetailsResponseModel {
  SubscriptionSummaryDetailsResponseModel({
    this.product,
    this.interval,
    this.intervalCount,
    this.currentPlanAmount,
    this.currency,
    this.currentPeriodStart,
    this.currentPeriodEnd,
    this.invoice,
  });

  String? product;
  String? interval;
  int? intervalCount;
  int? currentPlanAmount;
  String? currency;
  String? currentPeriodStart;
  String? currentPeriodEnd;
  Invoice? invoice;

  factory SubscriptionSummaryDetailsResponseModel.fromJson(
          Map<String, dynamic> json) =>
      SubscriptionSummaryDetailsResponseModel(
        product: json["product"],
        interval: json["interval"],
        intervalCount: json["interval_count"],
        currentPlanAmount: json["current_plan_amount"],
        currency: json["currency"],
        currentPeriodStart: json["current_period_start"],
        currentPeriodEnd: json["current_period_end"],
        invoice: Invoice.fromJson(json["invoice"]),
      );

  Map<String, dynamic> toJson() => {
        "product": product,
        "interval": interval,
        "interval_count": intervalCount,
        "current_plan_amount": currentPlanAmount,
        "currency": currency,
        "current_period_start": currentPeriodStart,
        "current_period_end": currentPeriodEnd,
        "invoice": invoice!.toJson(),
      };
}

class Invoice {
  Invoice({
    this.amountDue,
    this.amountPaid,
    this.amountRemaining,
    this.periodStart,
    this.periodEnd,
    this.status,
  });

  int? amountDue;
  int? amountPaid;
  int? amountRemaining;
  String? periodStart;
  String? periodEnd;
  String? status;

  factory Invoice.fromJson(Map<String, dynamic> json) => Invoice(
        amountDue: json["amount_due"],
        amountPaid: json["amount_paid"],
        amountRemaining: json["amount_remaining"],
        periodStart: json["period_start"],
        periodEnd: json["period_end"],
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "amount_due": amountDue,
        "amount_paid": amountPaid,
        "amount_remaining": amountRemaining,
        "period_start": periodStart,
        "period_end": periodEnd,
        "status": status,
      };
}
