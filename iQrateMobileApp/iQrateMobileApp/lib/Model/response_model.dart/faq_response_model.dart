class FaqResponseModel {
  List<OneFaqModel>? data;
  FaqResponseModel({this.data});

  factory FaqResponseModel.fromJson(json) {
    return FaqResponseModel(
      data: List<OneFaqModel>.from(json.map((x) => OneFaqModel.fromJson(x))),
    );
  }
}

class OneFaqModel {
  int? id;
  String? title;
  String? description;
  String? createdAt;
  String? updatedAt;
  String? publishedAt;
  int? displayOrder;
  FAQCategoryModel? faqCategoryModel;

  OneFaqModel(
      {this.id,
      this.title,
      this.description,
      this.createdAt,
      this.updatedAt,
      this.publishedAt,
      this.displayOrder,
      this.faqCategoryModel});

  factory OneFaqModel.fromJson(json) {
    return OneFaqModel(
        id: json['id'],
        title: json['Title'],
        description: json['Description'],
        createdAt: json['createdAt'],
        updatedAt: json['updatedAt'],
        publishedAt: json['publishedAt'],
        displayOrder: json['display_order'],
        faqCategoryModel: json['faq_category'] != null
            ? FAQCategoryModel.fromJson(json['faq_category'])
            : null);
  }
}

class FAQCategoryModel {
  int? id;
  String? categoryName;
  int? categoryDisplayOrder;

  FAQCategoryModel({this.id, this.categoryName, this.categoryDisplayOrder});

  factory FAQCategoryModel.fromJson(json) {
    return FAQCategoryModel(
        id: json['id'],
        categoryName: json['name'],
        categoryDisplayOrder: json['display_order']);
  }
}
