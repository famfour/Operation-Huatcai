class DocumentsDrawerResponseModel {
  DocumentsDrawerResponseModel({
    this.id,
    this.documents,
  });

  DocumentsDrawerResponseModel.fromJson(dynamic json) {
    id = json['id'];
    if (json['documents'] != null) {
      documents = [];
      json['documents'].forEach((v) {
        documents?.add(Documents.fromJson(v));
      });
    }
  }
  String? id;
  List<Documents>? documents;
  DocumentsDrawerResponseModel copyWith({
    String? id,
    List<Documents>? documents,
  }) =>
      DocumentsDrawerResponseModel(
        id: id ?? this.id,
        documents: documents ?? this.documents,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    if (documents != null) {
      map['documents'] = documents?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class Documents {
  Documents({
    this.id,
    this.filename,
    this.docType,
    this.created,
    this.updated,
    bool? isChecked,
  });

  Documents.fromJson(dynamic json) {
    id = json['id'];
    filename = json['filename'];
    docType = json['doc_type'];
    created = json['created'];
    updated = json['updated'];
    isChecked = false;
  }
  String? id;
  String? filename;
  String? docType;
  String? created;
  String? updated;
  bool? isChecked;
  Documents copyWith(
          {String? id,
          String? filename,
          String? docType,
          String? created,
          String? updated,
          bool? isChecked}) =>
      Documents(
        id: id ?? this.id,
        filename: filename ?? this.filename,
        docType: docType ?? this.docType,
        created: created ?? this.created,
        updated: updated ?? this.updated,
        isChecked: isChecked ?? false,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['filename'] = filename;
    map['doc_type'] = docType;
    map['created'] = created;
    map['updated'] = updated;
    return map;
  }
}
