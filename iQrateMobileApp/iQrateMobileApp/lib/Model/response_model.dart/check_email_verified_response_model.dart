// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

CheckEmailVerifiedResponseModel welcomeFromJson(String str) =>
    CheckEmailVerifiedResponseModel.fromJson(json.decode(str));

String welcomeToJson(CheckEmailVerifiedResponseModel data) =>
    json.encode(data.toJson());

class CheckEmailVerifiedResponseModel {
  CheckEmailVerifiedResponseModel({
    this.isEmailVerified,
  });

  int? isEmailVerified;

  factory CheckEmailVerifiedResponseModel.fromJson(Map<String, dynamic> json) =>
      CheckEmailVerifiedResponseModel(
        isEmailVerified: json["is_email_verified"],
      );

  Map<String, dynamic> toJson() => {
        "is_email_verified": isEmailVerified,
      };
}
