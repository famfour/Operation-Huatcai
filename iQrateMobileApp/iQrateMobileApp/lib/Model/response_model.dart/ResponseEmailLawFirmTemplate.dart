// ignore_for_file: file_names

class ResponseEmailLawFirmTemplate {
  ResponseEmailLawFirmTemplate({
    this.subject,
    this.content,
  });

  ResponseEmailLawFirmTemplate.fromJson(dynamic json) {
    subject = json['subject'];
    content = json['content'];
  }

  String? subject;
  String? content;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['subject'] = subject;
    map['content'] = content;
    return map;
  }
}
