import 'dart:convert';

NewPurchaseGetPledgeAmountResponseModel welcomeFromJson(String str) =>
    NewPurchaseGetPledgeAmountResponseModel.fromJson(json.decode(str));

String welcomeToJson(NewPurchaseGetPledgeAmountResponseModel data) =>
    json.encode(data.toJson());

class NewPurchaseGetPledgeAmountResponseModel {
  NewPurchaseGetPledgeAmountResponseModel({
    this.unfludgeAmount,
  });

  String? unfludgeAmount;

  factory NewPurchaseGetPledgeAmountResponseModel.fromJson(
          Map<String, dynamic> json) =>
      NewPurchaseGetPledgeAmountResponseModel(
        unfludgeAmount: json["fludge_amount"],
      );

  Map<String, dynamic> toJson() => {
        "fludge_amount": unfludgeAmount,
      };
}
