// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

// ignore_for_file: non_constant_identifier_names

import 'dart:convert';

ChangePhoneResponseModel welcomeFromJson(String str) =>
    ChangePhoneResponseModel.fromJson(json.decode(str));

String welcomeToJson(ChangePhoneResponseModel data) =>
    json.encode(data.toJson());

class ChangePhoneResponseModel {
  ChangePhoneResponseModel({
    this.message,
    this.verification_token,
  });

  String? message;
  String? verification_token;

  factory ChangePhoneResponseModel.fromJson(Map<String, dynamic> json) =>
      ChangePhoneResponseModel(
        message: json["message"],
        verification_token: json["verification_token"],
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "verification_token": verification_token,
      };
}
