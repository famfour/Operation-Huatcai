// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

MobileVerificationResponseModel welcomeFromJson(String str) =>
    MobileVerificationResponseModel.fromJson(json.decode(str));

String welcomeToJson(MobileVerificationResponseModel data) =>
    json.encode(data.toJson());

class MobileVerificationResponseModel {
  MobileVerificationResponseModel({
    this.message,
  });

  String? message;

  factory MobileVerificationResponseModel.fromJson(Map<String, dynamic> json) =>
      MobileVerificationResponseModel(
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "message": message,
      };
}
