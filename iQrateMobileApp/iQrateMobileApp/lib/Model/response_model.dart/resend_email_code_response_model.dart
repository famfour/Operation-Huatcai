class ResendEmailCodeResponseModel {
  ResendEmailCodeResponseModel({
    int? resultcode,
    Data? data,
  }) {
    _resultcode = resultcode;
    _data = data;
  }

  ResendEmailCodeResponseModel.fromJson(dynamic json) {
    _resultcode = json['resultcode'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  int? _resultcode;
  Data? _data;

  int? get resultcode => _resultcode;
  Data? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['resultcode'] = _resultcode;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    return map;
  }
}

class Data {
  Data({
    int? id,
    String? fullName,
    String? email,
    String? password,
    String? mobile,
    dynamic phone,
    dynamic userType,
    dynamic photo,
    dynamic otp,
    String? token,
    int? agreedTerm,
    String? verificationCode,
    dynamic isVerified,
    int? status,
    dynamic createdUserId,
    dynamic updatedUserId,
    String? createdAt,
    String? updatedAt,
    int? resendcodeEmailCount,
    int? resendcodeSmsCount,
    String? dob,
    Device? device,
    dynamic ip,
    dynamic location,
    dynamic occupation,
    dynamic postalCode,
    int? isEmailVerified,
    int? isMobileVerified,
    dynamic address,
    dynamic bankName,
    dynamic bankAcNumber,
    dynamic bankBranchCode,
    dynamic bankNation,
    dynamic bankNationId,
  }) {
    _id = id;
    _fullName = fullName;
    _email = email;
    _password = password;
    _mobile = mobile;
    _phone = phone;
    _userType = userType;
    _photo = photo;
    _otp = otp;
    _token = token;
    _agreedTerm = agreedTerm;
    _verificationCode = verificationCode;
    _isVerified = isVerified;
    _status = status;
    _createdUserId = createdUserId;
    _updatedUserId = updatedUserId;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _resendcodeEmailCount = resendcodeEmailCount;
    _resendcodeSmsCount = resendcodeSmsCount;
    _dob = dob;
    _device = device;
    _ip = ip;
    _location = location;
    _occupation = occupation;
    _postalCode = postalCode;
    _isEmailVerified = isEmailVerified;
    _isMobileVerified = isMobileVerified;
    _address = address;
    _bankName = bankName;
    _bankAcNumber = bankAcNumber;
    _bankBranchCode = bankBranchCode;
    _bankNation = bankNation;
    _bankNationId = bankNationId;
  }

  Data.fromJson(dynamic json) {
    _id = json['id'];
    _fullName = json['full_name'];
    _email = json['email'];
    _password = json['password'];
    _mobile = json['mobile'];
    _phone = json['phone'];
    _userType = json['user_type'];
    _photo = json['photo'];
    _otp = json['otp'];
    _token = json['token'];
    _agreedTerm = json['agreed_term'];
    _verificationCode = json['verification_code'];
    _isVerified = json['is_verified'];
    _status = json['status'];
    _createdUserId = json['created_user_id'];
    _updatedUserId = json['updated_user_id'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    _resendcodeEmailCount = json['resendcode_email_count'];
    _resendcodeSmsCount = json['resendcode_sms_count'];
    _dob = json['dob'];
    _device = json['device'] != null ? Device.fromJson(json['device']) : null;
    _ip = json['ip'];
    _location = json['location'];
    _occupation = json['occupation'];
    _postalCode = json['postal_code'];
    _isEmailVerified = json['is_email_verified'];
    _isMobileVerified = json['is_mobile_verified'];
    _address = json['address'];
    _bankName = json['bank_name'];
    _bankAcNumber = json['bank_ac_number'];
    _bankBranchCode = json['bank_branch_code'];
    _bankNation = json['bank_nation'];
    _bankNationId = json['bank_nation_id'];
  }
  int? _id;
  String? _fullName;
  String? _email;
  String? _password;
  String? _mobile;
  dynamic _phone;
  dynamic _userType;
  dynamic _photo;
  dynamic _otp;
  String? _token;
  int? _agreedTerm;
  String? _verificationCode;
  dynamic _isVerified;
  int? _status;
  dynamic _createdUserId;
  dynamic _updatedUserId;
  String? _createdAt;
  String? _updatedAt;
  int? _resendcodeEmailCount;
  int? _resendcodeSmsCount;
  String? _dob;
  Device? _device;
  dynamic _ip;
  dynamic _location;
  dynamic _occupation;
  dynamic _postalCode;
  int? _isEmailVerified;
  int? _isMobileVerified;
  dynamic _address;
  dynamic _bankName;
  dynamic _bankAcNumber;
  dynamic _bankBranchCode;
  dynamic _bankNation;
  dynamic _bankNationId;

  int? get id => _id;
  String? get fullName => _fullName;
  String? get email => _email;
  String? get password => _password;
  String? get mobile => _mobile;
  dynamic get phone => _phone;
  dynamic get userType => _userType;
  dynamic get photo => _photo;
  dynamic get otp => _otp;
  String? get token => _token;
  int? get agreedTerm => _agreedTerm;
  String? get verificationCode => _verificationCode;
  dynamic get isVerified => _isVerified;
  int? get status => _status;
  dynamic get createdUserId => _createdUserId;
  dynamic get updatedUserId => _updatedUserId;
  String? get createdAt => _createdAt;
  String? get updatedAt => _updatedAt;
  int? get resendcodeEmailCount => _resendcodeEmailCount;
  int? get resendcodeSmsCount => _resendcodeSmsCount;
  String? get dob => _dob;
  Device? get device => _device;
  dynamic get ip => _ip;
  dynamic get location => _location;
  dynamic get occupation => _occupation;
  dynamic get postalCode => _postalCode;
  int? get isEmailVerified => _isEmailVerified;
  int? get isMobileVerified => _isMobileVerified;
  dynamic get address => _address;
  dynamic get bankName => _bankName;
  dynamic get bankAcNumber => _bankAcNumber;
  dynamic get bankBranchCode => _bankBranchCode;
  dynamic get bankNation => _bankNation;
  dynamic get bankNationId => _bankNationId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['full_name'] = _fullName;
    map['email'] = _email;
    map['password'] = _password;
    map['mobile'] = _mobile;
    map['phone'] = _phone;
    map['user_type'] = _userType;
    map['photo'] = _photo;
    map['otp'] = _otp;
    map['token'] = _token;
    map['agreed_term'] = _agreedTerm;
    map['verification_code'] = _verificationCode;
    map['is_verified'] = _isVerified;
    map['status'] = _status;
    map['created_user_id'] = _createdUserId;
    map['updated_user_id'] = _updatedUserId;
    map['created_at'] = _createdAt;
    map['updated_at'] = _updatedAt;
    map['resendcode_email_count'] = _resendcodeEmailCount;
    map['resendcode_sms_count'] = _resendcodeSmsCount;
    map['dob'] = _dob;
    if (_device != null) {
      map['device'] = _device?.toJson();
    }
    map['ip'] = _ip;
    map['location'] = _location;
    map['occupation'] = _occupation;
    map['postal_code'] = _postalCode;
    map['is_email_verified'] = _isEmailVerified;
    map['is_mobile_verified'] = _isMobileVerified;
    map['address'] = _address;
    map['bank_name'] = _bankName;
    map['bank_ac_number'] = _bankAcNumber;
    map['bank_branch_code'] = _bankBranchCode;
    map['bank_nation'] = _bankNation;
    map['bank_nation_id'] = _bankNationId;
    return map;
  }
}

class Device {
  Device({
    String? status,
    String? country,
    String? countryCode,
    String? region,
    String? regionName,
    String? city,
    String? zip,
    double? lat,
    double? lon,
    String? timezone,
    String? isp,
    String? org,
    String? as,
    String? query,
  }) {
    _status = status;
    _country = country;
    _countryCode = countryCode;
    _region = region;
    _regionName = regionName;
    _city = city;
    _zip = zip;
    _lat = lat;
    _lon = lon;
    _timezone = timezone;
    _isp = isp;
    _org = org;
    _as = as;
    _query = query;
  }

  Device.fromJson(dynamic json) {
    _status = json['status'];
    _country = json['country'];
    _countryCode = json['countryCode'];
    _region = json['region'];
    _regionName = json['regionName'];
    _city = json['city'];
    _zip = json['zip'];
    _lat = json['lat'];
    _lon = json['lon'];
    _timezone = json['timezone'];
    _isp = json['isp'];
    _org = json['org'];
    _as = json['as'];
    _query = json['query'];
  }
  String? _status;
  String? _country;
  String? _countryCode;
  String? _region;
  String? _regionName;
  String? _city;
  String? _zip;
  double? _lat;
  double? _lon;
  String? _timezone;
  String? _isp;
  String? _org;
  String? _as;
  String? _query;

  String? get status => _status;
  String? get country => _country;
  String? get countryCode => _countryCode;
  String? get region => _region;
  String? get regionName => _regionName;
  String? get city => _city;
  String? get zip => _zip;
  double? get lat => _lat;
  double? get lon => _lon;
  String? get timezone => _timezone;
  String? get isp => _isp;
  String? get org => _org;
  String? get as => _as;
  String? get query => _query;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['country'] = _country;
    map['countryCode'] = _countryCode;
    map['region'] = _region;
    map['regionName'] = _regionName;
    map['city'] = _city;
    map['zip'] = _zip;
    map['lat'] = _lat;
    map['lon'] = _lon;
    map['timezone'] = _timezone;
    map['isp'] = _isp;
    map['org'] = _org;
    map['as'] = _as;
    map['query'] = _query;
    return map;
  }
}
