class TrainingViewResponse {
  List<TrainingViewResponseModel>? value;
  TrainingViewResponse({this.value});

  factory TrainingViewResponse.fromJson(json) {
    return TrainingViewResponse(
        value: List<TrainingViewResponseModel>.from(json.map((x) {
      return TrainingViewResponseModel.fromJson(x);
    })));
  }
}

class TrainingViewResponseModel {
  TrainingViewResponseModel({
    this.displayOrder,
    this.id,
    this.title,
    this.body,
    this.createdAt,
    this.updatedAt,
    this.publishedAt,
    this.url,
    this.media,
    this.thumbnail,
    this.trainingCategory,
    this.membershipTypes,
  });

  int? displayOrder;
  int? id;
  String? title;
  String? body;
  DateTime? createdAt;
  DateTime? updatedAt;
  DateTime? publishedAt;
  String? url;
  Media? media;
  Thumbnail? thumbnail;
  TrainingCategory? trainingCategory;
  List<MembershipTypes>? membershipTypes;

  factory TrainingViewResponseModel.fromJson(json) {
    return TrainingViewResponseModel(
      displayOrder: json["display_order"],
      id: json["id"],
      title: json["Title"],
      body: json["Body"],
      createdAt:
          json["createdAt"] != null ? DateTime.parse(json["createdAt"]) : null,
      updatedAt:
          json["updatedAt"] != null ? DateTime.parse(json["updatedAt"]) : null,
      publishedAt: json["publishedAt"] != null
          ? DateTime.parse(json["publishedAt"])
          : null,
      url: json["url"],
      media: json["Media"] != null ? Media.fromJson(json["Media"]) : null,
      thumbnail: json['Thumbnail'] != null
          ? Thumbnail.fromJson(json['Thumbnail'])
          : null,
      trainingCategory: TrainingCategory.fromJson(json["training_category"]),
      membershipTypes: List<MembershipTypes>.from(
          json["membership_types"].map((x) => MembershipTypes.fromJson(x))),
    );
  }
}

class TrainingCategory {
  TrainingCategory({
    this.id,
    this.name,
    this.createdAt,
    this.updatedAt,
    this.publishedAt,
    this.category,
  });

  int? id;
  String? name;
  DateTime? createdAt;
  DateTime? updatedAt;
  DateTime? publishedAt;
  String? category;

  factory TrainingCategory.fromJson(json) => TrainingCategory(
        id: json["id"],
        name: json["name"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        publishedAt: DateTime.parse(json["publishedAt"]),
        category: json["category"],
      );
}

class MembershipTypes {
  MembershipTypes({
    this.id,
    this.name,
    this.createdAt,
    this.updatedAt,
    this.publishedAt,
    this.category,
  });

  int? id;
  String? name;
  DateTime? createdAt;
  DateTime? updatedAt;
  DateTime? publishedAt;
  String? category;

  factory MembershipTypes.fromJson(Map<String, dynamic> json) =>
      MembershipTypes(
        id: json["id"],
        name: json["name"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        publishedAt: DateTime.parse(json["publishedAt"]),
        category: json["category"],
      );
}

class Media {
  int? id;
  String? name;
  String? alternativeText;
  String? caption;
  String? hash;
  String? ext;
  String? mime;
  double? size;
  String? url;
  String? provider;
  DateTime? createdAt;
  DateTime? updatedAt;

  Media(
      {this.id,
      this.name,
      this.alternativeText,
      this.caption,
      this.hash,
      this.ext,
      this.mime,
      this.size,
      this.url,
      this.provider,
      this.createdAt,
      this.updatedAt});

  factory Media.fromJson(Map<String, dynamic> json) => Media(
        id: json["id"],
        name: json["name"],
        alternativeText: json["alternativeText"],
        caption: json["caption"],
        hash: json["hash"],
        ext: json["ext"],
        mime: json["mime"],
        size: json["size"],
        url: json["url"],
        provider: json["provider"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
      );
}

class Thumbnail {
  Thumbnail({
    this.formats,
  });
  Formats? formats;

  factory Thumbnail.fromJson(Map<String, dynamic> json) => Thumbnail(
        formats: Formats.fromJson(json["formats"]),
      );
}

class Formats {
  Formats({
    // this.small,
    // this.medium,
    this.thumbnail,
  });

  // Medium? small;
  // Medium? medium;
  Medium? thumbnail;

  factory Formats.fromJson(Map<String, dynamic> json) => Formats(
        // small: json['small'] != null ? Medium.fromJson(json["small"]) : null,
        // medium: json['medium'] != null ? Medium.fromJson(json["medium"]) : null,
        thumbnail: json['thumbnail'] != null
            ? Medium.fromJson(json["thumbnail"])
            : null,
      );

  // Map<String, dynamic> toJson() => {
  //       "small": small!.toJson(),
  //       "medium": medium!.toJson(),
  //       "thumbnail": thumbnail!.toJson(),
  //     };
}

class Medium {
  Medium({
    this.ext,
    this.url,
    this.hash,
    this.mime,
    this.name,
    this.path,
    this.size,
    this.width,
    this.height,
  });

  String? ext;
  String? url;
  String? hash;
  String? mime;
  String? name;
  dynamic path;
  double? size;
  int? width;
  int? height;

  factory Medium.fromJson(Map<String, dynamic> json) => Medium(
        ext: json["ext"],
        url: json["url"],
        hash: json["hash"],
        mime: json["mime"],
        name: json["name"],
        path: json["path"],
        size: json["size"].toDouble(),
        width: json["width"],
        height: json["height"],
      );

  // Map<String, dynamic> toJson() => {
  //       "ext": ext,
  //       "url": url,
  //       "hash": hash,
  //       "mime": mime,
  //       "name": name,
  //       "path": path,
  //       "size": size,
  //       "width": width,
  //       "height": height,
  //     };
}
