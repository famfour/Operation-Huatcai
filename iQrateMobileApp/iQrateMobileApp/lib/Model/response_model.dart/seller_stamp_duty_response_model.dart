// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

SellerStampDutyResponseModel welcomeFromJson(String str) =>
    SellerStampDutyResponseModel.fromJson(json.decode(str));

String welcomeToJson(SellerStampDutyResponseModel data) =>
    json.encode(data.toJson());

class SellerStampDutyResponseModel {
  SellerStampDutyResponseModel({
    this.sellerStampDuty,
  });

  String? sellerStampDuty;

  factory SellerStampDutyResponseModel.fromJson(Map<String, dynamic> json) =>
      SellerStampDutyResponseModel(
        sellerStampDuty: json["seller_stamp_duty"],
      );

  Map<String, dynamic> toJson() => {
        "seller_stamp_duty": sellerStampDuty,
      };
}
