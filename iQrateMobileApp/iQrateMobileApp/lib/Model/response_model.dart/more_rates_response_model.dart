import 'package:iqrate/Model/response_model.dart/refinance_new_lead_package_response_model_v2.dart';

class MoreRatesResponseModel {
  MoreRatesResponseModel({
      this.count, 
      this.next, 
      this.previous, 
      this.results,});

  MoreRatesResponseModel.fromJson(dynamic json) {
    count = json['count'];
    next = json['next'];
    previous = json['previous'];
    if (json['results'] != null) {
      results = [];
      json['results'].forEach((v) {
        results?.add(Fixed.fromJson(v));
      });
    }
  }
  int? count;
  String? next;
  dynamic previous;
  List<Fixed>? results;
MoreRatesResponseModel copyWith({  int? count,
  String? next,
  dynamic previous,
  List<Fixed>? results,
}) => MoreRatesResponseModel(  count: count ?? this.count,
  next: next ?? this.next,
  previous: previous ?? this.previous,
  results: results ?? this.results,
);
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['count'] = count;
    map['next'] = next;
    map['previous'] = previous;
    if (results != null) {
      map['results'] = results?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

class Results {
  Results({
      this.id,
      this.bank,
      this.mask,
      this.rateCategory,
      this.rateType,
      this.propertyTypes,
      this.propertyStatus,
      this.loanCategory,
      this.lockInPeriod,
      this.minLoanAmount,
      this.depositToPlace,
      this.valuationSubsidy,
      this.fireInsuranceSubsidy,
      this.cashRebateLegalSubsidy,
      this.cashRebateSubsidyClawbackPeriodYears,
      this.partialRepaymentPenalty,
      this.partialRepaymentPenaltyRemarks,
      this.fullRepaymentPenalty,
      this.fullRepaymentPenaltyRemarks,
      this.rates,
      this.cancellationFee,
      this.depositToPlaceRemarks,
      this.remarksForClient,
      this.interestOffsetting,
      this.interestResetDate,
      this.processingFee,
      this.remarksForBroker,
      this.newPurchaseReferralFee,
      this.refinanceReferralFee,
      this.publish,
      this.createdBy,
      this.updatedBy,
      this.created,
      this.updated,});

  Results.fromJson(dynamic json) {
    id = json['id'];
    bank = json['bank'] != null ? Bank.fromJson(json['bank']) : null;
    mask = json['mask'];
    rateCategory = json['rate_category'];
    rateType = json['rate_type'];
    propertyTypes = json['property_types'] != null ? json['property_types'].cast<String>() : [];
    propertyStatus = json['property_status'] != null ? json['property_status'].cast<String>() : [];
    loanCategory = json['loan_category'] != null ? json['loan_category'].cast<String>() : [];
    lockInPeriod = json['lock_in_period'];
    minLoanAmount = json['min_loan_amount'];
    depositToPlace = json['deposit_to_place'];
    valuationSubsidy = json['valuation_subsidy'] != null ? json['valuation_subsidy'].cast<String>() : [];
    fireInsuranceSubsidy = json['fire_insurance_subsidy'];
    cashRebateLegalSubsidy = json['cash_rebate_legal_subsidy'] != null ? json['cash_rebate_legal_subsidy'].cast<String>() : [];
    cashRebateSubsidyClawbackPeriodYears = json['cash_rebate_subsidy_clawback_period_years'];
    partialRepaymentPenalty = json['partial_repayment_penalty'];
    partialRepaymentPenaltyRemarks = json['partial_repayment_penalty_remarks'];
    fullRepaymentPenalty = json['full_repayment_penalty'];
    fullRepaymentPenaltyRemarks = json['full_repayment_penalty_remarks'];
    if (json['rates'] != null) {
      rates = [];
      json['rates'].forEach((v) {
        rates?.add(Rates.fromJson(v));
      });
    }
    cancellationFee = json['cancellation_fee'];
    depositToPlaceRemarks = json['deposit_to_place_remarks'];
    remarksForClient = json['remarks_for_client'] != null ? json['remarks_for_client'].cast<String>() : [];
    interestOffsetting = json['interest_offsetting'];
    interestResetDate = json['interest_reset_date'];
    processingFee = json['processing_fee'];
    remarksForBroker = json['remarks_for_broker'];
    newPurchaseReferralFee = json['new_purchase_referral_fee'];
    refinanceReferralFee = json['refinance_referral_fee'];
    publish = json['publish'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    created = json['created'];
    updated = json['updated'];
  }
  int? id;
  bool isChecked = false;
  Bank? bank;
  bool? mask;
  String? rateCategory;
  String? rateType;
  List<String>? propertyTypes;
  List<String>? propertyStatus;
  List<String>? loanCategory;
  int? lockInPeriod;
  double? minLoanAmount;
  double? depositToPlace;
  List<String>? valuationSubsidy;
  String? fireInsuranceSubsidy;
  List<String>? cashRebateLegalSubsidy;
  int? cashRebateSubsidyClawbackPeriodYears;
  double? partialRepaymentPenalty;
  String? partialRepaymentPenaltyRemarks;
  double? fullRepaymentPenalty;
  String? fullRepaymentPenaltyRemarks;
  List<Rates>? rates;
  String? cancellationFee;
  String? depositToPlaceRemarks;
  List<String>? remarksForClient;
  String? interestOffsetting;
  bool? interestResetDate;
  bool? processingFee;
  dynamic remarksForBroker;
  double? newPurchaseReferralFee;
  double? refinanceReferralFee;
  bool? publish;
  dynamic createdBy;
  dynamic updatedBy;
  String? created;
  String? updated;
Results copyWith({  int? id,
  Bank? bank,
  bool? mask,
  String? rateCategory,
  String? rateType,
  List<String>? propertyTypes,
  List<String>? propertyStatus,
  List<String>? loanCategory,
  int? lockInPeriod,
  double? minLoanAmount,
  double? depositToPlace,
  List<String>? valuationSubsidy,
  String? fireInsuranceSubsidy,
  List<String>? cashRebateLegalSubsidy,
  int? cashRebateSubsidyClawbackPeriodYears,
  double? partialRepaymentPenalty,
  String? partialRepaymentPenaltyRemarks,
  double? fullRepaymentPenalty,
  String? fullRepaymentPenaltyRemarks,
  List<Rates>? rates,
  String? cancellationFee,
  String? depositToPlaceRemarks,
  List<String>? remarksForClient,
  String? interestOffsetting,
  bool? interestResetDate,
  bool? processingFee,
  dynamic remarksForBroker,
  double? newPurchaseReferralFee,
  double? refinanceReferralFee,
  bool? publish,
  dynamic createdBy,
  dynamic updatedBy,
  String? created,
  String? updated,
}) => Results(  id: id ?? this.id,
  bank: bank ?? this.bank,
  mask: mask ?? this.mask,
  rateCategory: rateCategory ?? this.rateCategory,
  rateType: rateType ?? this.rateType,
  propertyTypes: propertyTypes ?? this.propertyTypes,
  propertyStatus: propertyStatus ?? this.propertyStatus,
  loanCategory: loanCategory ?? this.loanCategory,
  lockInPeriod: lockInPeriod ?? this.lockInPeriod,
  minLoanAmount: minLoanAmount ?? this.minLoanAmount,
  depositToPlace: depositToPlace ?? this.depositToPlace,
  valuationSubsidy: valuationSubsidy ?? this.valuationSubsidy,
  fireInsuranceSubsidy: fireInsuranceSubsidy ?? this.fireInsuranceSubsidy,
  cashRebateLegalSubsidy: cashRebateLegalSubsidy ?? this.cashRebateLegalSubsidy,
  cashRebateSubsidyClawbackPeriodYears: cashRebateSubsidyClawbackPeriodYears ?? this.cashRebateSubsidyClawbackPeriodYears,
  partialRepaymentPenalty: partialRepaymentPenalty ?? this.partialRepaymentPenalty,
  partialRepaymentPenaltyRemarks: partialRepaymentPenaltyRemarks ?? this.partialRepaymentPenaltyRemarks,
  fullRepaymentPenalty: fullRepaymentPenalty ?? this.fullRepaymentPenalty,
  fullRepaymentPenaltyRemarks: fullRepaymentPenaltyRemarks ?? this.fullRepaymentPenaltyRemarks,
  rates: rates ?? this.rates,
  cancellationFee: cancellationFee ?? this.cancellationFee,
  depositToPlaceRemarks: depositToPlaceRemarks ?? this.depositToPlaceRemarks,
  remarksForClient: remarksForClient ?? this.remarksForClient,
  interestOffsetting: interestOffsetting ?? this.interestOffsetting,
  interestResetDate: interestResetDate ?? this.interestResetDate,
  processingFee: processingFee ?? this.processingFee,
  remarksForBroker: remarksForBroker ?? this.remarksForBroker,
  newPurchaseReferralFee: newPurchaseReferralFee ?? this.newPurchaseReferralFee,
  refinanceReferralFee: refinanceReferralFee ?? this.refinanceReferralFee,
  publish: publish ?? this.publish,
  createdBy: createdBy ?? this.createdBy,
  updatedBy: updatedBy ?? this.updatedBy,
  created: created ?? this.created,
  updated: updated ?? this.updated,
);
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    if (bank != null) {
      map['bank'] = bank?.toJson();
    }
    map['mask'] = mask;
    map['rate_category'] = rateCategory;
    map['rate_type'] = rateType;
    map['property_types'] = propertyTypes;
    map['property_status'] = propertyStatus;
    map['loan_category'] = loanCategory;
    map['lock_in_period'] = lockInPeriod;
    map['min_loan_amount'] = minLoanAmount;
    map['deposit_to_place'] = depositToPlace;
    map['valuation_subsidy'] = valuationSubsidy;
    map['fire_insurance_subsidy'] = fireInsuranceSubsidy;
    map['cash_rebate_legal_subsidy'] = cashRebateLegalSubsidy;
    map['cash_rebate_subsidy_clawback_period_years'] = cashRebateSubsidyClawbackPeriodYears;
    map['partial_repayment_penalty'] = partialRepaymentPenalty;
    map['partial_repayment_penalty_remarks'] = partialRepaymentPenaltyRemarks;
    map['full_repayment_penalty'] = fullRepaymentPenalty;
    map['full_repayment_penalty_remarks'] = fullRepaymentPenaltyRemarks;
    if (rates != null) {
      map['rates'] = rates?.map((v) => v.toJson()).toList();
    }
    map['cancellation_fee'] = cancellationFee;
    map['deposit_to_place_remarks'] = depositToPlaceRemarks;
    map['remarks_for_client'] = remarksForClient;
    map['interest_offsetting'] = interestOffsetting;
    map['interest_reset_date'] = interestResetDate;
    map['processing_fee'] = processingFee;
    map['remarks_for_broker'] = remarksForBroker;
    map['new_purchase_referral_fee'] = newPurchaseReferralFee;
    map['refinance_referral_fee'] = refinanceReferralFee;
    map['publish'] = publish;
    map['created_by'] = createdBy;
    map['updated_by'] = updatedBy;
    map['created'] = created;
    map['updated'] = updated;
    return map;
  }

}

class Rates {
  Rates({
      this.year,
      this.referenceRate,
      this.bankSpread,});

  Rates.fromJson(dynamic json) {
    year = json['year'];
    referenceRate = json['reference_rate'];
    bankSpread = json['bank_spread'];
  }
  String? year;
  int? referenceRate;
  double? bankSpread;
Rates copyWith({  String? year,
  int? referenceRate,
  double? bankSpread,
}) => Rates(  year: year ?? this.year,
  referenceRate: referenceRate ?? this.referenceRate,
  bankSpread: bankSpread ?? this.bankSpread,
);
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['year'] = year;
    map['reference_rate'] = referenceRate;
    map['bank_spread'] = bankSpread;
    return map;
  }

}

class Bank {
  Bank({
      this.id,
      this.name,
      this.nameMasked,
      this.status,
      this.logo,});

  Bank.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    nameMasked = json['name_masked'];
    status = json['status'];
    logo = json['logo'];
  }
  int? id;
  String? name;
  String? nameMasked;
  bool? status;
  String? logo;
Bank copyWith({  int? id,
  String? name,
  String? nameMasked,
  bool? status,
  String? logo,
}) => Bank(  id: id ?? this.id,
  name: name ?? this.name,
  nameMasked: nameMasked ?? this.nameMasked,
  status: status ?? this.status,
  logo: logo ?? this.logo,
);
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    map['name_masked'] = nameMasked;
    map['status'] = status;
    map['logo'] = logo;
    return map;
  }

}