// ignore_for_file: file_names

class BucMortgareCompareModel {
  dynamic totalPayment;
  dynamic totalPrincipal;
  dynamic totalInterest;
  List<dynamic>? yearlyInterest;

  BucMortgareCompareModel(
      {this.totalPayment,
      this.totalPrincipal,
      this.totalInterest,
      this.yearlyInterest});

  BucMortgareCompareModel.fromJson(Map<String, dynamic> json) {
    totalPayment = json['Total Payment'];
    totalPrincipal = json['Total Principal'];
    totalInterest = json['Total Interest'];
    yearlyInterest = json['Yearly Interest'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['Total Payment'] = totalPayment;
    data['Total Principal'] = totalPrincipal;
    data['Total Interest'] = totalInterest;
    data['Yearly Interest'] = yearlyInterest;
    return data;
  }
}

class RatePackageComparisonModel {
  String? name;
  BucMortgareCompareModel? model;
  RatePackageComparisonModel({this.name, this.model});

  RatePackageComparisonModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    model = json['model'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['model'] = model;
    return data;
  }
}
