// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

UpdateClientDetailsResponseModel welcomeFromJson(String str) =>
    UpdateClientDetailsResponseModel.fromJson(json.decode(str));

String welcomeToJson(UpdateClientDetailsResponseModel data) =>
    json.encode(data.toJson());

class UpdateClientDetailsResponseModel {
  UpdateClientDetailsResponseModel({
    this.id,
    this.lead,
    this.name,
    this.countryCode,
    this.phoneNumber,
    this.mainApplicant,
    this.pdpaStatus,
    this.email,
    this.annualIncome,
    this.dob,
    this.nationality,
    this.streetName,
    this.blockNumber,
    this.unitNumber,
    this.projectName,
    this.postalCode,
    this.createdBy,
    this.updatedBy,
    this.created,
    this.updated,
  });

  int? id;
  int? lead;
  String? name;
  int? countryCode;
  int? phoneNumber;
  bool? mainApplicant;
  bool? pdpaStatus;
  String? email;
  double? annualIncome;
  DateTime? dob;
  String? nationality;
  String? streetName;
  String? blockNumber;
  String? unitNumber;
  String? projectName;
  String? postalCode;
  String? createdBy;
  String? updatedBy;
  DateTime? created;
  DateTime? updated;

  factory UpdateClientDetailsResponseModel.fromJson(
          Map<String, dynamic> json) =>
      UpdateClientDetailsResponseModel(
        id: json["id"],
        lead: json["lead"],
        name: json["name"],
        countryCode: json["country_code"],
        phoneNumber: json["phone_number"],
        mainApplicant: json["main_applicant"],
        pdpaStatus: json["pdpa_status"],
        email: json["email"],
        annualIncome: json["annual_income"].toString()=="null" ? 0 : json["annual_income"].toDouble(),
        dob: json["dob"].toString()=="null" ? DateTime.now() : DateTime.parse(json["dob"]),
        nationality: json["nationality"],
        streetName: json["street_name"],
        blockNumber: json["block_number"],
        unitNumber: json["unit_number"],
        projectName: json["project_name"],
        postalCode: json["postal_code"],
        createdBy: json["created_by"],
        updatedBy: json["updated_by"],
        created: DateTime.parse(json["created"]),
        updated: DateTime.parse(json["updated"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "lead": lead,
        "name": name,
        "country_code": countryCode,
        "phone_number": phoneNumber,
        "main_applicant": mainApplicant,
        "pdpa_status": pdpaStatus,
        "email": email,
        "annual_income": annualIncome,
        "dob":
            "${dob!.year.toString().padLeft(4, '0')}-${dob!.month.toString().padLeft(2, '0')}-${dob!.day.toString().padLeft(2, '0')}",
        "nationality": nationality,
        "street_name": streetName,
        "block_number": blockNumber,
        "unit_number": unitNumber,
        "project_name": projectName,
        "postal_code": postalCode,
        "created_by": createdBy,
        "updated_by": updatedBy,
        "created": created!.toIso8601String(),
        "updated": updated!.toIso8601String(),
      };
}
