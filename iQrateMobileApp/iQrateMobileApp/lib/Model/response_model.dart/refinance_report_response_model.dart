// ignore_for_file: constant_identifier_names

import 'dart:convert';

RefinanceReportResponseModel welcomeFromJson(String str) =>
    RefinanceReportResponseModel.fromJson(json.decode(str));

class RefinanceReportResponseModel {
  RefinanceReportResponseModel({
    this.loanDetails,
    this.yourSavingFromRefinancing,
    this.packageDetails,
  });

  LoanDetails? loanDetails;
  YourSavingFromRefinancing? yourSavingFromRefinancing;
  List<PackageDetail>? packageDetails;

  factory RefinanceReportResponseModel.fromJson(Map<String, dynamic> json) =>
      RefinanceReportResponseModel(
        loanDetails: LoanDetails.fromJson(json["loan_details"]),
        yourSavingFromRefinancing: YourSavingFromRefinancing.fromJson(
            json["your_saving_from_refinancing"]),
        packageDetails: List<PackageDetail>.from(
            json["Package_details"].map((x) => PackageDetail.fromJson(x))),
      );
}

class LoanDetails {
  LoanDetails({
    this.preparedFor,
    this.outstandingLoanAmount,
    this.maximumLoanTenure,
    this.preferedLoanTenure,
    this.existingBank,
    this.existingInterestRate,
  });

  String? preparedFor;
  int? outstandingLoanAmount;
  int? maximumLoanTenure;
  int? preferedLoanTenure;
  String? existingBank;
  double? existingInterestRate;

  factory LoanDetails.fromJson(Map<String, dynamic> json) => LoanDetails(
        preparedFor: json["Prepared_for"],
        outstandingLoanAmount: json["Outstanding_Loan_Amount"],
        maximumLoanTenure: json["Maximum_Loan_Tenure"],
        preferedLoanTenure: json["Prefered_Loan_Tenure"],
        existingBank: json["Existing_Bank"],
        existingInterestRate: json["Existing_Interest_Rate"].toDouble(),
      );
}

class PackageDetail {
  PackageDetail({
    this.name,
    this.maximumLoanTenure,
    this.preferedLoanTenure,
    this.package,
    this.selectedPackageDetails,
  });

  String? name;
  LoanTenure? maximumLoanTenure;
  LoanTenure? preferedLoanTenure;
  int? package;
  SelectedPackageDetails? selectedPackageDetails;

  factory PackageDetail.fromJson(Map<String, dynamic> json) => PackageDetail(
        name: json["name"],
        maximumLoanTenure: LoanTenure.fromJson(json["maximum_loan_tenure"]),
        preferedLoanTenure: LoanTenure.fromJson(json["prefered_loan_tenure"]),
        package: json["Package"],
        selectedPackageDetails:
            SelectedPackageDetails.fromJson(json["selected_package_details"]),
      );
}

class LoanTenure {
  LoanTenure({
    this.monthlyInstallment,
    this.totalPrincipal,
    this.totalInterest,
    this.cashSubsidy,
    this.savings,
    this.rate,
    this.mortgagePaymentList,
  });

  int? monthlyInstallment;
  int? totalPrincipal;
  int? totalInterest;
  int? cashSubsidy;
  int? savings;
  double? rate;
  List<MortgagePaymentList>? mortgagePaymentList;

  factory LoanTenure.fromJson(Map<String, dynamic> json) => LoanTenure(
        monthlyInstallment: json["Monthly_Installment"],
        totalPrincipal: json["Total_Principal"],
        totalInterest: json["Total_Interest"],
        cashSubsidy: json["Cash_Subsidy"],
        savings: json["Savings"],
        rate: json["Rate"].toDouble(),
        mortgagePaymentList: List<MortgagePaymentList>.from(
            json["Mortgage_Payment_List"]
                .map((x) => MortgagePaymentList.fromJson(x))),
      );
}

class MortgagePaymentList {
  MortgagePaymentList({
    this.emiNo,
    this.year,
    this.month,
    this.interestRate,
    this.installmentAmount,
    this.interestRepayment,
    this.principalRepayment,
    this.outstandingAmount,
    this.cumInterestPaid,
  });

  int? emiNo;
  int? year;
  int? month;
  double? interestRate;
  int? installmentAmount;
  double? interestRepayment;
  double? principalRepayment;
  double? outstandingAmount;
  double? cumInterestPaid;

  factory MortgagePaymentList.fromJson(Map<String, dynamic> json) =>
      MortgagePaymentList(
        emiNo: json["emi_no"],
        year: json["year"],
        month: json["month"],
        interestRate: json["interest_rate"].toDouble(),
        installmentAmount: json["installment_amount"],
        interestRepayment: json["interest_repayment"].toDouble(),
        principalRepayment: json["principal_repayment"].toDouble(),
        outstandingAmount: json["outstanding_amount"].toDouble(),
        cumInterestPaid: json["cum_interest_paid"].toDouble(),
      );
}

class SelectedPackageDetails {
  SelectedPackageDetails({
    this.id,
    this.bank,
    this.mask,
    this.rateCategory,
    this.rateType,
    this.propertyTypes,
    this.propertyStatus,
    this.loanCategory,
    this.lockInPeriod,
    this.minLoanAmount,
    this.depositToPlace,
    this.valuationSubsidy,
    this.fireInsuranceSubsidy,
    this.cashRebateLegalSubsidy,
    this.cashRebateSubsidyClawbackPeriodYears,
    this.partialRepaymentPenalty,
    this.partialRepaymentPenaltyRemarks,
    this.fullRepaymentPenalty,
    this.fullRepaymentPenaltyRemarks,
    this.rates,
    this.cancellationFee,
    this.depositToPlaceRemarks,
    this.remarksForClient,
    this.interestOffsetting,
    this.interestResetDate,
    this.processingFee,
    this.remarksForBroker,
    this.newPurchaseReferralFee,
    this.refinanceReferralFee,
    this.createdBy,
    this.updatedBy,
    this.created,
    this.updated,
  });

  int? id;
  Bank? bank;
  bool? mask;
  String? rateCategory;
  RateType? rateType;
  List<String>? propertyTypes;
  List<PropertyStatus>? propertyStatus;
  List<String>? loanCategory;
  int? lockInPeriod;
  int? minLoanAmount;
  int? depositToPlace;
  List<String>? valuationSubsidy;
  String? fireInsuranceSubsidy;
  List<String>? cashRebateLegalSubsidy;
  int? cashRebateSubsidyClawbackPeriodYears;
  double? partialRepaymentPenalty;
  String? partialRepaymentPenaltyRemarks;
  double? fullRepaymentPenalty;
  String? fullRepaymentPenaltyRemarks;
  List<Rate>? rates;
  String? cancellationFee;
  String? depositToPlaceRemarks;
  List<String>? remarksForClient;
  String? interestOffsetting;
  bool? interestResetDate;
  bool? processingFee;
  String? remarksForBroker;
  double? newPurchaseReferralFee;
  double? refinanceReferralFee;
  dynamic createdBy;
  String? updatedBy;
  DateTime? created;
  DateTime? updated;

  factory SelectedPackageDetails.fromJson(Map<String, dynamic> json) =>
      SelectedPackageDetails(
        id: json["id"],
        bank: Bank.fromJson(json["bank"]),
        mask: json["mask"],
        rateCategory: json["rate_category"],
        rateType: rateTypeValues.map[json["rate_type"]],
        propertyTypes: List<String>.from(json["property_types"].map((x) => x)),
        propertyStatus: List<PropertyStatus>.from(
            json["property_status"].map((x) => propertyStatusValues.map[x])),
        loanCategory: List<String>.from(json["loan_category"].map((x) => x)),
        lockInPeriod: json["lock_in_period"],
        minLoanAmount: json["min_loan_amount"],
        depositToPlace: json["deposit_to_place"],
        valuationSubsidy:
            List<String>.from(json["valuation_subsidy"].map((x) => x)),
        fireInsuranceSubsidy: json["fire_insurance_subsidy"],
        cashRebateLegalSubsidy:
            List<String>.from(json["cash_rebate_legal_subsidy"].map((x) => x)),
        cashRebateSubsidyClawbackPeriodYears:
            json["cash_rebate_subsidy_clawback_period_years"],
        partialRepaymentPenalty: json["partial_repayment_penalty"].toDouble(),
        partialRepaymentPenaltyRemarks:
            json["partial_repayment_penalty_remarks"],
        fullRepaymentPenalty: json["full_repayment_penalty"].toDouble(),
        fullRepaymentPenaltyRemarks: json["full_repayment_penalty_remarks"],
        rates: List<Rate>.from(json["rates"].map((x) => Rate.fromJson(x))),
        cancellationFee: json["cancellation_fee"],
        depositToPlaceRemarks: json["deposit_to_place_remarks"],
        remarksForClient:
            List<String>.from(json["remarks_for_client"].map((x) => x)),
        interestOffsetting: json["interest_offsetting"],
        interestResetDate: json["interest_reset_date"],
        processingFee: json["processing_fee"],
        remarksForBroker: json["remarks_for_broker"],
        newPurchaseReferralFee: json["new_purchase_referral_fee"].toDouble(),
        refinanceReferralFee: json["refinance_referral_fee"].toDouble(),
        createdBy: json["created_by"],
        updatedBy: json["updated_by"],
        created: DateTime.parse(json["created"]),
        updated: DateTime.parse(json["updated"]),
      );
}

class Bank {
  Bank({
    this.id,
    this.name,
    this.logo,
    this.bankForm,
  });

  int? id;
  String? name;
  String? logo;
  String? bankForm;

  factory Bank.fromJson(Map<String, dynamic> json) => Bank(
        id: json["id"],
        name: json["name"],
        logo: json["logo"],
        bankForm: json["bank_form"],
      );
}

enum PropertyStatus {
  UNDER_CONSTRUCTION_TO_OBTAIN_TOP_MORE_THAN_2_YEARS,
  UNDER_CONSTRUCTION_TO_OBTAIN_TOP_WITHIN_2_YEARS,
  COMPLETED
}

final propertyStatusValues = EnumValues({
  "completed": PropertyStatus.COMPLETED,
  "under_construction_to_obtain_top_more_than_2_years":
      PropertyStatus.UNDER_CONSTRUCTION_TO_OBTAIN_TOP_MORE_THAN_2_YEARS,
  "under_construction_to_obtain_top_within_2_years":
      PropertyStatus.UNDER_CONSTRUCTION_TO_OBTAIN_TOP_WITHIN_2_YEARS
});

enum RateType { BOARD, FIXED_DEPOSIT, SORA }

final rateTypeValues = EnumValues({
  "board": RateType.BOARD,
  "fixed_deposit": RateType.FIXED_DEPOSIT,
  "sora": RateType.SORA
});

class Rate {
  Rate({
    this.year,
    this.referenceRate,
    this.bankSpread,
    this.totalInterestRate,
  });

  String? year;
  ReferenceRate? referenceRate;
  double? bankSpread;
  double? totalInterestRate;

  factory Rate.fromJson(Map<String, dynamic> json) => Rate(
        year: json["year"],
        referenceRate: ReferenceRate.fromJson(json["reference_rate"]),
        bankSpread: json["bank_spread"].toDouble(),
        totalInterestRate: json["total_interest_rate"].toDouble(),
      );
}

class ReferenceRate {
  ReferenceRate({
    this.rateType,
    this.reference,
    this.interestRate,
    this.equation,
    this.remarks,
  });

  RateType? rateType;
  Reference? reference;
  double? interestRate;
  Equation? equation;
  dynamic remarks;

  factory ReferenceRate.fromJson(Map<String, dynamic> json) => ReferenceRate(
        rateType: rateTypeValues.map[json["rate_type"]],
        reference: referenceValues.map[json["reference"]],
        interestRate: json["interest_rate"].toDouble(),
        equation: equationValues.map[json["equation"]],
        remarks: json["remarks"],
      );
}

enum Equation { EMPTY, EQUATION, PURPLE }

final equationValues = EnumValues(
    {"-": Equation.EMPTY, "+": Equation.EQUATION, "=": Equation.PURPLE});

enum Reference { BOARD_787974, FIXED_DEPOSIT_176117, SORA_770387, BOARD_269235 }

final referenceValues = EnumValues({
  "BOARD-269235": Reference.BOARD_269235,
  "BOARD-787974": Reference.BOARD_787974,
  "FIXED_DEPOSIT-176117": Reference.FIXED_DEPOSIT_176117,
  "SORA-770387": Reference.SORA_770387
});

class YourSavingFromRefinancing {
  YourSavingFromRefinancing({
    this.legalFees,
    this.valuationFees,
  });

  int? legalFees;
  int? valuationFees;

  factory YourSavingFromRefinancing.fromJson(Map<String, dynamic> json) =>
      YourSavingFromRefinancing(
        legalFees: json["Legal_Fees"],
        valuationFees: json["Valuation_Fees"],
      );
}

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String>? reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    reverseMap;
    return reverseMap!;
  }
}
