import 'dart:convert';

BucMortgagePdfResponseModel welcomeFromJson(String str) =>
    BucMortgagePdfResponseModel.fromJson(json.decode(str));

String welcomeToJson(BucMortgagePdfResponseModel data) =>
    json.encode(data.toJson());

class BucMortgagePdfResponseModel {
  BucMortgagePdfResponseModel({
    this.url,
  });

  String? url;

  factory BucMortgagePdfResponseModel.fromJson(Map<String, dynamic> json) =>
      BucMortgagePdfResponseModel(
        url: json["url"],
      );

  Map<String, dynamic> toJson() => {
        "url": url,
      };
}
