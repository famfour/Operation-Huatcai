// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

RefinanceReportPdfResponseModel welcomeFromJson(String str) =>
    RefinanceReportPdfResponseModel.fromJson(json.decode(str));

String welcomeToJson(RefinanceReportPdfResponseModel data) =>
    json.encode(data.toJson());

class RefinanceReportPdfResponseModel {
  RefinanceReportPdfResponseModel({
    this.url,
  });

  String? url;

  factory RefinanceReportPdfResponseModel.fromJson(Map<String, dynamic> json) =>
      RefinanceReportPdfResponseModel(
        url: json["url"],
      );

  Map<String, dynamic> toJson() => {
        "url": url,
      };
}
