class BankSubmissionBanksListResponseModel {
  List<BankSubmissionOneBank>? bankSubmissionOneBank;
  BankSubmissionBanksListResponseModel({this.bankSubmissionOneBank});

  factory BankSubmissionBanksListResponseModel.fromJson(json) {
    return BankSubmissionBanksListResponseModel(
        bankSubmissionOneBank: List<BankSubmissionOneBank>.from(
            json.map((value) => BankSubmissionOneBank.fromJson(value))));
  }
}

class BankSubmissionOneBank {
  BankSubmissionOneBank(
      {this.bankDetails,
      this.emailToBankTask,
      this.emailToLeasTask,
      this.displayDocumentDrawer});
  BankSubmissionBankDetails? bankDetails;
  bool? emailToBankTask;
  bool? emailToLeasTask;
  bool? displayDocumentDrawer;

  factory BankSubmissionOneBank.fromJson(Map<String, dynamic> json) =>
      BankSubmissionOneBank(
          bankDetails: BankSubmissionBankDetails.fromJson(json["bank"]),
          emailToBankTask: json["email_to_bank"],
          emailToLeasTask: json["email_to_lead"],
          displayDocumentDrawer: json["display_document_drawer"]);
}

class BankSubmissionBankDetails {
  BankSubmissionBankDetails({
    this.id,
    this.bankName,
    this.logo,
    this.bankForm,
  });
  int? id;
  String? bankName;
  String? logo;
  String? bankForm;
  factory BankSubmissionBankDetails.fromJson(Map<String, dynamic> json) =>
      BankSubmissionBankDetails(
        id: json["id"],
        bankName: json["name"],
        logo: json["logo"],
        bankForm: json["bank_form"],
      );
}
