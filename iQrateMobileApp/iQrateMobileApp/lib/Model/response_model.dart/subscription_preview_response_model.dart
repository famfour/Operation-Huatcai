// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

SubscriptionsPreview welcomeFromJson(String str) =>
    SubscriptionsPreview.fromJson(json.decode(str));

String welcomeToJson(SubscriptionsPreview data) => json.encode(data.toJson());

class SubscriptionsPreview {
  SubscriptionsPreview({
    this.object,
    this.accountCountry,
    this.accountName,
    this.accountTaxIds,
    this.amountDue,
    this.amountPaid,
    this.amountRemaining,
    this.applicationFeeAmount,
    this.attemptCount,
    this.attempted,
    this.automaticTax,
    this.billingReason,
    this.charge,
    this.collectionMethod,
    this.created,
    this.currency,
    this.customFields,
    this.customer,
    this.customerAddress,
    this.customerEmail,
    this.customerName,
    this.customerPhone,
    this.customerShipping,
    this.customerTaxExempt,
    this.customerTaxIds,
    this.defaultPaymentMethod,
    this.defaultSource,
    this.defaultTaxRates,
    this.description,
    this.discount,
    this.discounts,
    this.dueDate,
    this.endingBalance,
    this.footer,
    this.lastFinalizationError,
    this.lines,
    this.livemode,
    this.metadata,
    this.nextPaymentAttempt,
    this.number,
    this.onBehalfOf,
    this.paid,
    this.paidOutOfBand,
    this.paymentIntent,
    this.paymentSettings,
    this.periodEnd,
    this.periodStart,
    this.postPaymentCreditNotesAmount,
    this.prePaymentCreditNotesAmount,
    this.quote,
    this.receiptNumber,
    this.startingBalance,
    this.statementDescriptor,
    this.status,
    this.statusTransitions,
    this.subscription,
    this.subscriptionProrationDate,
    this.subtotal,
    this.tax,
    this.testClock,
    this.total,
    this.totalDiscountAmounts,
    this.totalTaxAmounts,
    this.transferData,
    this.webhooksDeliveredAt,
  });

  String? object;
  String? accountCountry;
  String? accountName;
  dynamic accountTaxIds;
  int? amountDue;
  int? amountPaid;
  int? amountRemaining;
  dynamic applicationFeeAmount;
  int? attemptCount;
  bool? attempted;
  AutomaticTax? automaticTax;
  String? billingReason;
  dynamic charge;
  String? collectionMethod;
  int? created;
  String? currency;
  dynamic customFields;
  String? customer;
  dynamic customerAddress;
  String? customerEmail;
  String? customerName;
  dynamic customerPhone;
  dynamic customerShipping;
  String? customerTaxExempt;
  List<dynamic>? customerTaxIds;
  dynamic defaultPaymentMethod;
  dynamic defaultSource;
  List<dynamic>? defaultTaxRates;
  dynamic description;
  dynamic discount;
  List<dynamic>? discounts;
  dynamic dueDate;
  int? endingBalance;
  dynamic footer;
  dynamic lastFinalizationError;
  Lines? lines;
  bool? livemode;
  Metadata? metadata;
  int? nextPaymentAttempt;
  dynamic number;
  dynamic onBehalfOf;
  bool? paid;
  bool? paidOutOfBand;
  dynamic paymentIntent;
  PaymentSettings? paymentSettings;
  int? periodEnd;
  int? periodStart;
  int? postPaymentCreditNotesAmount;
  int? prePaymentCreditNotesAmount;
  dynamic quote;
  dynamic receiptNumber;
  int? startingBalance;
  dynamic statementDescriptor;
  String? status;
  StatusTransitions? statusTransitions;
  String? subscription;
  int? subscriptionProrationDate;
  int? subtotal;
  dynamic tax;
  dynamic testClock;
  int? total;
  List<dynamic>? totalDiscountAmounts;
  List<dynamic>? totalTaxAmounts;
  dynamic transferData;
  dynamic webhooksDeliveredAt;

  factory SubscriptionsPreview.fromJson(Map<String, dynamic> json) =>
      SubscriptionsPreview(
        object: json["object"],
        accountCountry: json["account_country"],
        accountName: json["account_name"],
        accountTaxIds: json["account_tax_ids"],
        amountDue: json["amount_due"],
        amountPaid: json["amount_paid"],
        amountRemaining: json["amount_remaining"],
        applicationFeeAmount: json["application_fee_amount"],
        attemptCount: json["attempt_count"],
        attempted: json["attempted"],
        automaticTax: AutomaticTax.fromJson(json["automatic_tax"]),
        billingReason: json["billing_reason"],
        charge: json["charge"],
        collectionMethod: json["collection_method"],
        created: json["created"],
        currency: json["currency"],
        customFields: json["custom_fields"],
        customer: json["customer"],
        customerAddress: json["customer_address"],
        customerEmail: json["customer_email"],
        customerName: json["customer_name"],
        customerPhone: json["customer_phone"],
        customerShipping: json["customer_shipping"],
        customerTaxExempt: json["customer_tax_exempt"],
        customerTaxIds:
            List<dynamic>.from(json["customer_tax_ids"].map((x) => x)),
        defaultPaymentMethod: json["default_payment_method"],
        defaultSource: json["default_source"],
        defaultTaxRates:
            List<dynamic>.from(json["default_tax_rates"].map((x) => x)),
        description: json["description"],
        discount: json["discount"],
        discounts: List<dynamic>.from(json["discounts"].map((x) => x)),
        dueDate: json["due_date"],
        endingBalance: json["ending_balance"],
        footer: json["footer"],
        lastFinalizationError: json["last_finalization_error"],
        lines: Lines.fromJson(json["lines"]),
        livemode: json["livemode"],
        metadata: Metadata.fromJson(json["metadata"]),
        nextPaymentAttempt: json["next_payment_attempt"],
        number: json["number"],
        onBehalfOf: json["on_behalf_of"],
        paid: json["paid"],
        paidOutOfBand: json["paid_out_of_band"],
        paymentIntent: json["payment_intent"],
        paymentSettings: PaymentSettings.fromJson(json["payment_settings"]),
        periodEnd: json["period_end"],
        periodStart: json["period_start"],
        postPaymentCreditNotesAmount: json["post_payment_credit_notes_amount"],
        prePaymentCreditNotesAmount: json["pre_payment_credit_notes_amount"],
        quote: json["quote"],
        receiptNumber: json["receipt_number"],
        startingBalance: json["starting_balance"],
        statementDescriptor: json["statement_descriptor"],
        status: json["status"],
        statusTransitions:
            StatusTransitions.fromJson(json["status_transitions"]),
        subscription: json["subscription"],
        subscriptionProrationDate: json["subscription_proration_date"],
        subtotal: json["subtotal"],
        tax: json["tax"],
        testClock: json["test_clock"],
        total: json["total"],
        totalDiscountAmounts:
            List<dynamic>.from(json["total_discount_amounts"].map((x) => x)),
        totalTaxAmounts:
            List<dynamic>.from(json["total_tax_amounts"].map((x) => x)),
        transferData: json["transfer_data"],
        webhooksDeliveredAt: json["webhooks_delivered_at"],
      );

  Map<String, dynamic> toJson() => {
        "object": object,
        "account_country": accountCountry,
        "account_name": accountName,
        "account_tax_ids": accountTaxIds,
        "amount_due": amountDue,
        "amount_paid": amountPaid,
        "amount_remaining": amountRemaining,
        "application_fee_amount": applicationFeeAmount,
        "attempt_count": attemptCount,
        "attempted": attempted,
        "automatic_tax": automaticTax!.toJson(),
        "billing_reason": billingReason,
        "charge": charge,
        "collection_method": collectionMethod,
        "created": created,
        "currency": currency,
        "custom_fields": customFields,
        "customer": customer,
        "customer_address": customerAddress,
        "customer_email": customerEmail,
        "customer_name": customerName,
        "customer_phone": customerPhone,
        "customer_shipping": customerShipping,
        "customer_tax_exempt": customerTaxExempt,
        "customer_tax_ids": List<dynamic>.from(customerTaxIds!.map((x) => x)),
        "default_payment_method": defaultPaymentMethod,
        "default_source": defaultSource,
        "default_tax_rates": List<dynamic>.from(defaultTaxRates!.map((x) => x)),
        "description": description,
        "discount": discount,
        "discounts": List<dynamic>.from(discounts!.map((x) => x)),
        "due_date": dueDate,
        "ending_balance": endingBalance,
        "footer": footer,
        "last_finalization_error": lastFinalizationError,
        "lines": lines!.toJson(),
        "livemode": livemode,
        "metadata": metadata!.toJson(),
        "next_payment_attempt": nextPaymentAttempt,
        "number": number,
        "on_behalf_of": onBehalfOf,
        "paid": paid,
        "paid_out_of_band": paidOutOfBand,
        "payment_intent": paymentIntent,
        "payment_settings": paymentSettings!.toJson(),
        "period_end": periodEnd,
        "period_start": periodStart,
        "post_payment_credit_notes_amount": postPaymentCreditNotesAmount,
        "pre_payment_credit_notes_amount": prePaymentCreditNotesAmount,
        "quote": quote,
        "receipt_number": receiptNumber,
        "starting_balance": startingBalance,
        "statement_descriptor": statementDescriptor,
        "status": status,
        "status_transitions": statusTransitions!.toJson(),
        "subscription": subscription,
        "subscription_proration_date": subscriptionProrationDate,
        "subtotal": subtotal,
        "tax": tax,
        "test_clock": testClock,
        "total": total,
        "total_discount_amounts":
            List<dynamic>.from(totalDiscountAmounts!.map((x) => x)),
        "total_tax_amounts": List<dynamic>.from(totalTaxAmounts!.map((x) => x)),
        "transfer_data": transferData,
        "webhooks_delivered_at": webhooksDeliveredAt,
      };
}

class AutomaticTax {
  AutomaticTax({
    this.enabled,
    this.status,
  });

  bool? enabled;
  dynamic status;

  factory AutomaticTax.fromJson(Map<String, dynamic> json) => AutomaticTax(
        enabled: json["enabled"],
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "enabled": enabled,
        "status": status,
      };
}

class Lines {
  Lines({
    this.object,
    this.data,
    this.hasMore,
    this.totalCount,
    this.url,
  });

  String? object;
  List<Datum>? data;
  bool? hasMore;
  int? totalCount;
  String? url;

  factory Lines.fromJson(Map<String, dynamic> json) => Lines(
        object: json["object"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        hasMore: json["has_more"],
        totalCount: json["total_count"],
        url: json["url"],
      );

  Map<String, dynamic> toJson() => {
        "object": object,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
        "has_more": hasMore,
        "total_count": totalCount,
        "url": url,
      };
}

class Datum {
  Datum({
    this.id,
    this.object,
    this.amount,
    this.currency,
    this.description,
    this.discountAmounts,
    this.discountable,
    this.discounts,
    this.livemode,
    this.metadata,
    this.period,
    this.plan,
    this.price,
    this.proration,
    this.prorationDetails,
    this.quantity,
    this.subscription,
    this.subscriptionItem,
    this.taxAmounts,
    this.taxRates,
    this.type,
  });

  String? id;
  String? object;
  int? amount;
  String? currency;
  String? description;
  List<dynamic>? discountAmounts;
  bool? discountable;
  List<dynamic>? discounts;
  bool? livemode;
  Metadata? metadata;
  Period? period;
  Plan? plan;
  Price? price;
  bool? proration;
  ProrationDetails? prorationDetails;
  int? quantity;
  String? subscription;
  String? subscriptionItem;
  List<dynamic>? taxAmounts;
  List<dynamic>? taxRates;
  String? type;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        object: json["object"],
        amount: json["amount"],
        currency: json["currency"],
        description: json["description"],
        discountAmounts:
            List<dynamic>.from(json["discount_amounts"].map((x) => x)),
        discountable: json["discountable"],
        discounts: List<dynamic>.from(json["discounts"].map((x) => x)),
        livemode: json["livemode"],
        metadata: Metadata.fromJson(json["metadata"]),
        period: Period.fromJson(json["period"]),
        plan: Plan.fromJson(json["plan"]),
        price: Price.fromJson(json["price"]),
        proration: json["proration"],
        prorationDetails: ProrationDetails.fromJson(json["proration_details"]),
        quantity: json["quantity"],
        subscription: json["subscription"],
        subscriptionItem: json["subscription_item"],
        taxAmounts: List<dynamic>.from(json["tax_amounts"].map((x) => x)),
        taxRates: List<dynamic>.from(json["tax_rates"].map((x) => x)),
        type: json["type"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "object": object,
        "amount": amount,
        "currency": currency,
        "description": description,
        "discount_amounts": List<dynamic>.from(discountAmounts!.map((x) => x)),
        "discountable": discountable,
        "discounts": List<dynamic>.from(discounts!.map((x) => x)),
        "livemode": livemode,
        "metadata": metadata!.toJson(),
        "period": period!.toJson(),
        "plan": plan!.toJson(),
        "price": price!.toJson(),
        "proration": proration,
        "proration_details": prorationDetails!.toJson(),
        "quantity": quantity,
        "subscription": subscription,
        "subscription_item": subscriptionItem,
        "tax_amounts": List<dynamic>.from(taxAmounts!.map((x) => x)),
        "tax_rates": List<dynamic>.from(taxRates!.map((x) => x)),
        "type": type,
      };
}

class Metadata {
  Metadata();

  factory Metadata.fromJson(Map<String, dynamic> json) => Metadata();

  Map<String, dynamic> toJson() => {};
}

class Period {
  Period({
    this.end,
    this.start,
  });

  int? end;
  int? start;

  factory Period.fromJson(Map<String, dynamic> json) => Period(
        end: json["end"],
        start: json["start"],
      );

  Map<String, dynamic> toJson() => {
        "end": end,
        "start": start,
      };
}

class Plan {
  Plan({
    this.id,
    this.object,
    this.active,
    this.aggregateUsage,
    this.amount,
    this.amountDecimal,
    this.billingScheme,
    this.created,
    this.currency,
    this.interval,
    this.intervalCount,
    this.livemode,
    this.metadata,
    this.nickname,
    this.product,
    this.tiersMode,
    this.transformUsage,
    this.trialPeriodDays,
    this.usageType,
  });

  String? id;
  String? object;
  bool? active;
  dynamic aggregateUsage;
  int? amount;
  String? amountDecimal;
  String? billingScheme;
  int? created;
  String? currency;
  String? interval;
  int? intervalCount;
  bool? livemode;
  Metadata? metadata;
  dynamic nickname;
  String? product;
  dynamic tiersMode;
  dynamic transformUsage;
  dynamic trialPeriodDays;
  String? usageType;

  factory Plan.fromJson(Map<String, dynamic> json) => Plan(
        id: json["id"],
        object: json["object"],
        active: json["active"],
        aggregateUsage: json["aggregate_usage"],
        amount: json["amount"],
        amountDecimal: json["amount_decimal"],
        billingScheme: json["billing_scheme"],
        created: json["created"],
        currency: json["currency"],
        interval: json["interval"],
        intervalCount: json["interval_count"],
        livemode: json["livemode"],
        metadata: Metadata.fromJson(json["metadata"]),
        nickname: json["nickname"],
        product: json["product"],
        tiersMode: json["tiers_mode"],
        transformUsage: json["transform_usage"],
        trialPeriodDays: json["trial_period_days"],
        usageType: json["usage_type"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "object": object,
        "active": active,
        "aggregate_usage": aggregateUsage,
        "amount": amount,
        "amount_decimal": amountDecimal,
        "billing_scheme": billingScheme,
        "created": created,
        "currency": currency,
        "interval": interval,
        "interval_count": intervalCount,
        "livemode": livemode,
        "metadata": metadata!.toJson(),
        "nickname": nickname,
        "product": product,
        "tiers_mode": tiersMode,
        "transform_usage": transformUsage,
        "trial_period_days": trialPeriodDays,
        "usage_type": usageType,
      };
}

class Price {
  Price({
    this.id,
    this.object,
    this.active,
    this.billingScheme,
    this.created,
    this.currency,
    this.livemode,
    this.lookupKey,
    this.metadata,
    this.nickname,
    this.product,
    this.recurring,
    this.taxBehavior,
    this.tiersMode,
    this.transformQuantity,
    this.type,
    this.unitAmount,
    this.unitAmountDecimal,
  });

  String? id;
  String? object;
  bool? active;
  String? billingScheme;
  int? created;
  String? currency;
  bool? livemode;
  dynamic lookupKey;
  Metadata? metadata;
  dynamic nickname;
  String? product;
  Recurring? recurring;
  String? taxBehavior;
  dynamic tiersMode;
  dynamic transformQuantity;
  String? type;
  int? unitAmount;
  String? unitAmountDecimal;

  factory Price.fromJson(Map<String, dynamic> json) => Price(
        id: json["id"],
        object: json["object"],
        active: json["active"],
        billingScheme: json["billing_scheme"],
        created: json["created"],
        currency: json["currency"],
        livemode: json["livemode"],
        lookupKey: json["lookup_key"],
        metadata: Metadata.fromJson(json["metadata"]),
        nickname: json["nickname"],
        product: json["product"],
        recurring: Recurring.fromJson(json["recurring"]),
        taxBehavior: json["tax_behavior"],
        tiersMode: json["tiers_mode"],
        transformQuantity: json["transform_quantity"],
        type: json["type"],
        unitAmount: json["unit_amount"],
        unitAmountDecimal: json["unit_amount_decimal"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "object": object,
        "active": active,
        "billing_scheme": billingScheme,
        "created": created,
        "currency": currency,
        "livemode": livemode,
        "lookup_key": lookupKey,
        "metadata": metadata!.toJson(),
        "nickname": nickname,
        "product": product,
        "recurring": recurring!.toJson(),
        "tax_behavior": taxBehavior,
        "tiers_mode": tiersMode,
        "transform_quantity": transformQuantity,
        "type": type,
        "unit_amount": unitAmount,
        "unit_amount_decimal": unitAmountDecimal,
      };
}

class Recurring {
  Recurring({
    this.aggregateUsage,
    this.interval,
    this.intervalCount,
    this.trialPeriodDays,
    this.usageType,
  });

  dynamic aggregateUsage;
  String? interval;
  int? intervalCount;
  dynamic trialPeriodDays;
  String? usageType;

  factory Recurring.fromJson(Map<String, dynamic> json) => Recurring(
        aggregateUsage: json["aggregate_usage"],
        interval: json["interval"],
        intervalCount: json["interval_count"],
        trialPeriodDays: json["trial_period_days"],
        usageType: json["usage_type"],
      );

  Map<String, dynamic> toJson() => {
        "aggregate_usage": aggregateUsage,
        "interval": interval,
        "interval_count": intervalCount,
        "trial_period_days": trialPeriodDays,
        "usage_type": usageType,
      };
}

class ProrationDetails {
  ProrationDetails({
    this.creditedItems,
  });

  dynamic creditedItems;

  factory ProrationDetails.fromJson(Map<String, dynamic> json) =>
      ProrationDetails(
        creditedItems: json["credited_items"],
      );

  Map<String, dynamic> toJson() => {
        "credited_items": creditedItems,
      };
}

class PaymentSettings {
  PaymentSettings({
    this.paymentMethodOptions,
    this.paymentMethodTypes,
  });

  dynamic paymentMethodOptions;
  dynamic paymentMethodTypes;

  factory PaymentSettings.fromJson(Map<String, dynamic> json) =>
      PaymentSettings(
        paymentMethodOptions: json["payment_method_options"],
        paymentMethodTypes: json["payment_method_types"],
      );

  Map<String, dynamic> toJson() => {
        "payment_method_options": paymentMethodOptions,
        "payment_method_types": paymentMethodTypes,
      };
}

class StatusTransitions {
  StatusTransitions({
    this.finalizedAt,
    this.markedUncollectibleAt,
    this.paidAt,
    this.voidedAt,
  });

  dynamic finalizedAt;
  dynamic markedUncollectibleAt;
  dynamic paidAt;
  dynamic voidedAt;

  factory StatusTransitions.fromJson(Map<String, dynamic> json) =>
      StatusTransitions(
        finalizedAt: json["finalized_at"],
        markedUncollectibleAt: json["marked_uncollectible_at"],
        paidAt: json["paid_at"],
        voidedAt: json["voided_at"],
      );

  Map<String, dynamic> toJson() => {
        "finalized_at": finalizedAt,
        "marked_uncollectible_at": markedUncollectibleAt,
        "paid_at": paidAt,
        "voided_at": voidedAt,
      };
}
