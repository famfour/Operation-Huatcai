import 'dart:convert';

RefinanceTotalResponseModel welcomeFromJson(String str) =>
    RefinanceTotalResponseModel.fromJson(json.decode(str));

String welcomeToJson(RefinanceTotalResponseModel data) =>
    json.encode(data.toJson());

class RefinanceTotalResponseModel {
  RefinanceTotalResponseModel({
    this.totalMonthlyIncome,
  });

  dynamic totalMonthlyIncome;

  factory RefinanceTotalResponseModel.fromJson(Map<String, dynamic> json) =>
      RefinanceTotalResponseModel(
        totalMonthlyIncome: json["total_monthly_income"],
      );

  Map<String, dynamic> toJson() => {
        "total_monthly_income": totalMonthlyIncome,
      };
}
