// ignore_for_file: file_names, unnecessary_this

import 'package:iqrate/Model/response_model.dart/safe_convert.dart';

class MortgageRepaymentTableModel {
  int year = 0;
  int month = 0;
  double interestRate = 0;
  double installmentAmount = 0;
  double interestRepayment = 0;
  double principalRepayment = 0;
  double outstandingAmount = 0;
  double cumInterestPaid = 0;

  MortgageRepaymentTableModel({
    this.year = 0,
    this.month = 0,
    this.interestRate = 0,
    this.installmentAmount = 0,
    this.interestRepayment = 0,
    this.principalRepayment = 0,
    this.outstandingAmount = 0,
    this.cumInterestPaid = 0,
  });

  MortgageRepaymentTableModel.fromJson(Map<String, dynamic> json)
      : year = SafeManager.parseInt(json, 'year'),
        month = SafeManager.parseInt(json, 'month'),
        interestRate = SafeManager.parseDouble(json, 'interest_rate'),
        installmentAmount = SafeManager.parseDouble(json, 'installment_amount'),
        interestRepayment = SafeManager.parseDouble(json, 'interest_repayment'),
        principalRepayment =
            SafeManager.parseDouble(json, 'principal_repayment'),
        outstandingAmount = SafeManager.parseDouble(json, 'outstanding_amount'),
        cumInterestPaid = SafeManager.parseDouble(json, 'cum_interest_paid');

  Map<String, dynamic> toJson() => {
        'year': this.year,
        'month': this.month,
        'interest_rate': this.interestRate,
        'installment_amount': this.installmentAmount,
        'interest_repayment': this.interestRepayment,
        'principal_repayment': this.principalRepayment,
        'outstanding_amount': this.outstandingAmount,
        'cum_interest_paid': this.cumInterestPaid,
      };
}
