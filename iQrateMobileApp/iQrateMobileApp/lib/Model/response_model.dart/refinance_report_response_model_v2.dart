// ignore_for_file: constant_identifier_names

class RefinanceReportResponseModelV2 {
  RefinanceReportResponseModelV2({
    this.loanDetails,
    this.yourSavingFromRefinancing,
    this.packageDetails,
  });

  LoanDetails? loanDetails;
  YourSavingFromRefinancing? yourSavingFromRefinancing;
  List<PackageDetails>? packageDetails;

  RefinanceReportResponseModelV2.fromJson(dynamic json) {
    loanDetails = json['loan_details'] != null
        ? LoanDetails.fromJson(json['loan_details'])
        : null;
    yourSavingFromRefinancing = json['your_saving_from_refinancing'] != null
        ? YourSavingFromRefinancing.fromJson(
            json['your_saving_from_refinancing'])
        : null;
    packageDetails = List<PackageDetails>.from(
        json["Package_details"].map((x) => PackageDetails.fromJson(x)));
  }

  RefinanceReportResponseModelV2 copyWith({
    LoanDetails? loanDetails,
    YourSavingFromRefinancing? yourSavingFromRefinancing,
    List<PackageDetails>? packageDetails,
  }) =>
      RefinanceReportResponseModelV2(
        loanDetails: loanDetails ?? this.loanDetails,
        yourSavingFromRefinancing:
            yourSavingFromRefinancing ?? this.yourSavingFromRefinancing,
        packageDetails: packageDetails ?? this.packageDetails,
      );
}

class MortgagePaymentList {
  MortgagePaymentList({
    this.emiNo,
    this.year,
    this.month,
    this.interestRate,
    this.installmentAmount,
    this.interestRepayment,
    this.principalRepayment,
    this.outstandingAmount,
    this.cumInterestPaid,
  });

  int? emiNo;
  int? year;
  int? month;
  double? interestRate;
  dynamic installmentAmount;
  double? interestRepayment;
  double? principalRepayment;
  double? outstandingAmount;
  double? cumInterestPaid;

  factory MortgagePaymentList.fromJson(Map<String, dynamic> json) =>
      MortgagePaymentList(
        emiNo: json["emi_no"],
        year: json["year"],
        month: json["month"],
        interestRate: json["interest_rate"].toDouble(),
        installmentAmount: json["installment_amount"],
        interestRepayment: json["interest_repayment"].toDouble(),
        principalRepayment: json["principal_repayment"].toDouble(),
        outstandingAmount: json["outstanding_amount"].toDouble(),
        cumInterestPaid: json["cum_interest_paid"].toDouble(),
      );
}

class PackageDetails {
  PackageDetails({
    this.name,
    this.maximumLoanTenure,
    this.preferedLoanTenure,
    this.package,
  });

  int? package;
  String? name;
  MaximumLoanTenure? maximumLoanTenure;
  PreferedLoanTenure? preferedLoanTenure;
  SelectedPackageDetails? selectedPackageDetails;

  PackageDetails.fromJson(dynamic json) {
    name = json['name'];
    package = json['Package'];
    maximumLoanTenure = json['maximum_loan_tenure'] != null
        ? MaximumLoanTenure.fromJson(json['maximum_loan_tenure'])
        : null;
    preferedLoanTenure =
        json['prefered_loan_tenure'].toString() != [].toString()
            ? PreferedLoanTenure.fromJson(json['prefered_loan_tenure'])
            : null;
    selectedPackageDetails = json['selected_package_details'] != null
        ? SelectedPackageDetails.fromJson(json['selected_package_details'])
        : null;
  }

  PackageDetails copyWith({
    String? name,
    MaximumLoanTenure? maximumLoanTenure,
    PreferedLoanTenure? preferedLoanTenure,
    int? package,
  }) =>
      PackageDetails(
        name: name ?? this.name,
        maximumLoanTenure: maximumLoanTenure ?? this.maximumLoanTenure,
        preferedLoanTenure: preferedLoanTenure ?? this.preferedLoanTenure,
        package: package ?? this.package,
      );
}

class SelectedPackageDetails {
  SelectedPackageDetails({
    this.id,
    this.bank,
    this.mask,
    this.rateCategory,
    this.rateType,
    this.propertyTypes,
    this.propertyStatus,
    this.loanCategory,
    this.lockInPeriod,
    this.minLoanAmount,
    this.depositToPlace,
    this.valuationSubsidy,
    this.fireInsuranceSubsidy,
    this.cashRebateLegalSubsidy,
    this.cashRebateSubsidyClawbackPeriodYears,
    this.partialRepaymentPenalty,
    this.partialRepaymentPenaltyRemarks,
    this.fullRepaymentPenalty,
    this.fullRepaymentPenaltyRemarks,
    this.rates,
    this.cancellationFee,
    this.cancellationFeeRemarks,
    this.depositToPlaceRemarks,
    this.remarksForClient,
    this.interestOffsetting,
    this.interestResetDate,
    this.processingFee,
    this.remarksForBroker,
    this.newPurchaseReferralFee,
    this.refinanceReferralFee,
    this.createdBy,
    this.updatedBy,
    this.created,
    this.updated,
  });

  int? id;
  Bank? bank;
  bool? mask;
  String? rateCategory;
  String? rateType;
  List<String>? propertyTypes;
  List<String>? propertyStatus;
  List<String>? loanCategory;
  int? lockInPeriod;
  int? minLoanAmount;
  int? depositToPlace;
  List<String>? valuationSubsidy;
  String? fireInsuranceSubsidy;
  List<String>? cashRebateLegalSubsidy;
  int? cashRebateSubsidyClawbackPeriodYears;
  double? partialRepaymentPenalty;
  String? partialRepaymentPenaltyRemarks;
  double? fullRepaymentPenalty;
  String? fullRepaymentPenaltyRemarks;
  List<Rate>? rates;
  String? cancellationFee;
  String? cancellationFeeRemarks;
  String? depositToPlaceRemarks;
  List<String>? remarksForClient;
  String? interestOffsetting;
  dynamic interestResetDate;
  dynamic processingFee;
  String? remarksForBroker;
  double? newPurchaseReferralFee;
  double? refinanceReferralFee;
  dynamic createdBy;
  String? updatedBy;
  DateTime? created;
  DateTime? updated;

  factory SelectedPackageDetails.fromJson(Map<String, dynamic> json) {
    return SelectedPackageDetails(
      id: json["id"],
      bank: Bank.fromJson(json["bank"]),
      mask: json["mask"],
      rateCategory: json["rate_category"],
      rateType: json["rate_type"],
      propertyTypes: List<String>.from(json["property_types"].map((x) => x)),
      propertyStatus: List<String>.from(json["property_status"].map((x) => x)),
      loanCategory: List<String>.from(json["loan_category"].map((x) => x)),
      lockInPeriod: json["lock_in_period"],
      minLoanAmount: json["min_loan_amount"],
      depositToPlace: json["deposit_to_place"],
      valuationSubsidy:
          List<String>.from(json["valuation_subsidy"].map((x) => x)),
      fireInsuranceSubsidy: json["fire_insurance_subsidy"],
      cashRebateLegalSubsidy:
          List<String>.from(json["cash_rebate_legal_subsidy"].map((x) => x)),
      cashRebateSubsidyClawbackPeriodYears:
          json["cash_rebate_subsidy_clawback_period_years"],
      partialRepaymentPenalty: json["partial_repayment_penalty"].toDouble(),
      partialRepaymentPenaltyRemarks: json["partial_repayment_penalty_remarks"],
      fullRepaymentPenalty: json["full_repayment_penalty"].toDouble(),
      fullRepaymentPenaltyRemarks: json["full_repayment_penalty_remarks"],
      rates: List<Rate>.from(json["rates"].map((x) => Rate.fromJson(x))),
      cancellationFee: json["cancellation_fee"],
      cancellationFeeRemarks: json["cancellation_fee_remarks"] ?? "NA",
      depositToPlaceRemarks: json["deposit_to_place_remarks"],
      remarksForClient:
          List<String>.from(json["remarks_for_client"].map((x) => x)),
      interestOffsetting: json["interest_offsetting"],
      interestResetDate: json["interest_reset_date"],
      processingFee: json["processing_fee"],
      remarksForBroker: json["remarks_for_broker"],
      newPurchaseReferralFee: json["new_purchase_referral_fee"].toDouble(),
      refinanceReferralFee: json["refinance_referral_fee"].toDouble(),
      createdBy: json["created_by"],
      updatedBy: json["updated_by"],
      created: DateTime.parse(json["created"]),
      updated: DateTime.parse(json["updated"]),
    );
  }
}

class Bank {
  Bank({
    this.id,
    this.name,
    this.logo,
    this.bankForm,
  });

  int? id;
  String? name;
  String? logo;
  String? bankForm;

  factory Bank.fromJson(Map<String, dynamic> json) => Bank(
        id: json["id"],
        name: json["name"],
        logo: json["logo"],
        bankForm: json["bank_form"],
      );
}

class Rate {
  Rate({
    this.year,
    this.referenceRate,
    this.bankSpread,
    this.totalInterestRate,
  });

  String? year;
  ReferenceRate? referenceRate;
  double? bankSpread;
  double? totalInterestRate;

  factory Rate.fromJson(Map<String, dynamic> json) => Rate(
        year: json["year"],
        referenceRate: ReferenceRate.fromJson(json["reference_rate"]),
        bankSpread: json["bank_spread"].toDouble(),
        totalInterestRate: json["total_interest_rate"].toDouble(),
      );
}

class ReferenceRate {
  ReferenceRate({
    this.rateType,
    this.reference,
    this.interestRate,
    this.equation,
    this.remarks,
  });

  RateType? rateType;
  String? reference;
  double? interestRate;
  Equation? equation;
  dynamic remarks;

  factory ReferenceRate.fromJson(Map<String, dynamic> json) => ReferenceRate(
        rateType: rateTypeValues.map[json["rate_type"]],
        reference: json["reference"],
        interestRate: json["interest_rate"].toDouble(),
        equation: equationValues.map[json["equation"]],
        remarks: json["remarks"],
      );
}

enum Equation { EMPTY, EQUATION, PURPLE }

final equationValues = EnumValues(
    {"-": Equation.EMPTY, "+": Equation.EQUATION, "=": Equation.PURPLE});

final referenceValues = EnumValues({
  "BOARD-269235": Reference.BOARD_269235,
  "BOARD-787974": Reference.BOARD_787974,
  "FIXED_DEPOSIT-176117": Reference.FIXED_DEPOSIT_176117,
  "SORA-770387": Reference.SORA_770387
});

enum Reference { BOARD_787974, FIXED_DEPOSIT_176117, SORA_770387, BOARD_269235 }

final propertyStatusValues = EnumValues({
  "completed": PropertyStatus.COMPLETED,
  "under_construction_to_obtain_top_more_than_2_years":
      PropertyStatus.UNDER_CONSTRUCTION_TO_OBTAIN_TOP_MORE_THAN_2_YEARS,
  "under_construction_to_obtain_top_within_2_years":
      PropertyStatus.UNDER_CONSTRUCTION_TO_OBTAIN_TOP_WITHIN_2_YEARS
});

final rateTypeValues = EnumValues({
  "board": RateType.BOARD,
  "fixed_deposit": RateType.FIXED_DEPOSIT,
  "sora": RateType.SORA
});

enum RateType { BOARD, FIXED_DEPOSIT, SORA }

enum PropertyStatus {
  UNDER_CONSTRUCTION_TO_OBTAIN_TOP_MORE_THAN_2_YEARS,
  UNDER_CONSTRUCTION_TO_OBTAIN_TOP_WITHIN_2_YEARS,
  COMPLETED
}

class PreferedLoanTenure {
  PreferedLoanTenure({
    this.monthlyInstallment,
    this.totalPrincipal,
    this.totalInterest,
    this.cashSubsidy,
    this.savings,
    this.rate,
    this.mortgagePaymentList,
  });

  dynamic monthlyInstallment;
  int? totalPrincipal;
  int? totalInterest;
  int? cashSubsidy;
  int? savings;
  dynamic rate;
  List<MortgagePaymentList>? mortgagePaymentList;

  PreferedLoanTenure.fromJson(dynamic json) {
    monthlyInstallment = json['Monthly_Installment'];
    totalPrincipal = json['Total_Principal'];
    totalInterest = json['Total_Interest'];
    cashSubsidy = json['Cash_Subsidy'];
    savings = json['Savings'];
    rate = json['Rate'];
    if (json['Mortgage_Payment_List'] != null) {
      mortgagePaymentList = List<MortgagePaymentList>.from(
          json["Mortgage_Payment_List"]
              .map((x) => MortgagePaymentList.fromJson(x)));
    }
  }

  PreferedLoanTenure copyWith({
    int? monthlyInstallment,
    int? totalPrincipal,
    int? totalInterest,
    int? cashSubsidy,
    int? savings,
    dynamic rate,
    List<MortgagePaymentList>? mortgagePaymentList,
  }) =>
      PreferedLoanTenure(
        monthlyInstallment: monthlyInstallment ?? this.monthlyInstallment,
        totalPrincipal: totalPrincipal ?? this.totalPrincipal,
        totalInterest: totalInterest ?? this.totalInterest,
        cashSubsidy: cashSubsidy ?? this.cashSubsidy,
        savings: savings ?? this.savings,
        rate: rate ?? this.rate,
        mortgagePaymentList: mortgagePaymentList ?? this.mortgagePaymentList,
      );
}

class MaximumLoanTenure {
  MaximumLoanTenure({
    this.monthlyInstallment,
    this.totalPrincipal,
    this.totalInterest,
    this.cashSubsidy,
    this.savings,
    this.rate,
    this.mortgagePaymentList,
  });

  int? monthlyInstallment;
  int? totalPrincipal;
  int? totalInterest;
  int? cashSubsidy;
  int? savings;
  dynamic rate;
  List<MortgagePaymentList>? mortgagePaymentList;

  MaximumLoanTenure.fromJson(dynamic json) {
    monthlyInstallment = json['Monthly_Installment'];
    totalPrincipal = json['Total_Principal'];
    totalInterest = json['Total_Interest'];
    cashSubsidy = json['Cash_Subsidy'];
    savings = json['Savings'];
    rate = json['Rate'];
    if (json['Mortgage_Payment_List'] != null) {
      mortgagePaymentList = List<MortgagePaymentList>.from(
          json["Mortgage_Payment_List"]
              .map((x) => MortgagePaymentList.fromJson(x)));
    }
  }

  MaximumLoanTenure copyWith({
    int? monthlyInstallment,
    int? totalPrincipal,
    int? totalInterest,
    int? cashSubsidy,
    int? savings,
    dynamic rate,
    List<MortgagePaymentList>? mortgagePaymentList,
  }) =>
      MaximumLoanTenure(
        monthlyInstallment: monthlyInstallment ?? this.monthlyInstallment,
        totalPrincipal: totalPrincipal ?? this.totalPrincipal,
        totalInterest: totalInterest ?? this.totalInterest,
        cashSubsidy: cashSubsidy ?? this.cashSubsidy,
        savings: savings ?? this.savings,
        rate: rate ?? this.rate,
        mortgagePaymentList: mortgagePaymentList ?? this.mortgagePaymentList,
      );
}

class YourSavingFromRefinancing {
  YourSavingFromRefinancing({
    this.legalFees,
    this.valuationFees,
  });

  YourSavingFromRefinancing.fromJson(dynamic json) {
    legalFees = json['Legal_Fees'];
    valuationFees = json['Valuation_Fees'];
  }
  int? legalFees;
  int? valuationFees;
  YourSavingFromRefinancing copyWith({
    int? legalFees,
    int? valuationFees,
  }) =>
      YourSavingFromRefinancing(
        legalFees: legalFees ?? this.legalFees,
        valuationFees: valuationFees ?? this.valuationFees,
      );
}

class LoanDetails {
  LoanDetails({
    this.preparedFor,
    this.outstandingLoanAmount,
    this.maximumLoanTenure,
    this.preferedLoanTenure,
    this.existingBank,
    this.existingInterestRate,
  });

  String? preparedFor;
  int? outstandingLoanAmount;
  int? maximumLoanTenure;
  int? preferedLoanTenure;
  String? existingBank;
  dynamic existingInterestRate;

  LoanDetails.fromJson(dynamic json) {
    preparedFor = json['Prepared_for'];
    outstandingLoanAmount = json['Outstanding_Loan_Amount'];
    maximumLoanTenure = json['Maximum_Loan_Tenure'];
    preferedLoanTenure = json['Prefered_Loan_Tenure'];
    existingBank = json['Existing_Bank'];
    existingInterestRate = json['Existing_Interest_Rate'];
  }

  LoanDetails copyWith({
    String? preparedFor,
    int? outstandingLoanAmount,
    int? maximumLoanTenure,
    int? preferedLoanTenure,
    String? existingBank,
    int? existingInterestRate,
  }) =>
      LoanDetails(
        preparedFor: preparedFor ?? this.preparedFor,
        outstandingLoanAmount:
            outstandingLoanAmount ?? this.outstandingLoanAmount,
        maximumLoanTenure: maximumLoanTenure ?? this.maximumLoanTenure,
        preferedLoanTenure: preferedLoanTenure ?? this.preferedLoanTenure,
        existingBank: existingBank ?? this.existingBank,
        existingInterestRate: existingInterestRate ?? this.existingInterestRate,
      );
}

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String>? reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    reverseMap;
    return reverseMap!;
  }
}
