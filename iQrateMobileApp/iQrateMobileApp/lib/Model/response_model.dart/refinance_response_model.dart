import 'dart:convert';

RefinanceResponseModel welcomeFromJson(String str) =>
    RefinanceResponseModel.fromJson(json.decode(str));

String welcomeToJson(RefinanceResponseModel data) => json.encode(data.toJson());

class RefinanceResponseModel {
  RefinanceResponseModel({
    this.earlyRepaymentPenaltyAmount,
    this.cancellationPenaltyAmount,
  });

  dynamic earlyRepaymentPenaltyAmount;
  dynamic cancellationPenaltyAmount;

  factory RefinanceResponseModel.fromJson(Map<String, dynamic> json) =>
      RefinanceResponseModel(
        earlyRepaymentPenaltyAmount: json["early_repayment_penalty_amount"],
        cancellationPenaltyAmount: json["cancellation_penalty_amount"],
      );

  Map<String, dynamic> toJson() => {
        "early_repayment_penalty_amount": earlyRepaymentPenaltyAmount,
        "cancellation_penalty_amount": cancellationPenaltyAmount,
      };
}
