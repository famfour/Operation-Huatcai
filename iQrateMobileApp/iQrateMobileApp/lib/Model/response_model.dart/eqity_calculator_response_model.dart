import 'dart:convert';

EquityCalculatorResponseModel welcomeFromJson(String str) =>
    EquityCalculatorResponseModel.fromJson(json.decode(str));

String welcomeToJson(EquityCalculatorResponseModel data) =>
    json.encode(data.toJson());

class EquityCalculatorResponseModel {
  EquityCalculatorResponseModel({
    this.maximumEquityLoan,
  });

  String? maximumEquityLoan;

  factory EquityCalculatorResponseModel.fromJson(Map<String, dynamic> json) =>
      EquityCalculatorResponseModel(
        maximumEquityLoan: json["maximum_equity_loan"],
      );

  Map<String, dynamic> toJson() => {
        "maximum_equity_loan": maximumEquityLoan,
      };
}
