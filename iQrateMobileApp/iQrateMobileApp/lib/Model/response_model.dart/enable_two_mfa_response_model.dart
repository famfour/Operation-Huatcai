// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

EnableTwoMfaResponseModel welcomeFromJson(String str) =>
    EnableTwoMfaResponseModel.fromJson(json.decode(str));

String welcomeToJson(EnableTwoMfaResponseModel data) =>
    json.encode(data.toJson());

class EnableTwoMfaResponseModel {
  EnableTwoMfaResponseModel({
    this.secret,
    this.otpauthUrl,
  });

  String? secret;
  String? otpauthUrl;

  factory EnableTwoMfaResponseModel.fromJson(Map<String, dynamic> json) =>
      EnableTwoMfaResponseModel(
        secret: json["secret"],
        otpauthUrl: json["otpauthUrl"],
      );

  Map<String, dynamic> toJson() => {
        "secret": secret,
        "otpauthUrl": otpauthUrl,
      };
}
