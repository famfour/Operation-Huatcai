import '../../Model/response_model.dart/safe_convert.dart';

class BucProgressiveRepaymentScheduleModelSent {
	String disbursementName = "";
	String percentage = "";
	String month = "";

	BucProgressiveRepaymentScheduleModelSent({
		this.disbursementName = "",
		this.percentage = "",
		this.month = "",
	});

	BucProgressiveRepaymentScheduleModelSent.fromJson(Map<String, dynamic> json)
			:	disbursementName = SafeManager.parseString(json, 'disbursement_name'),
	percentage = SafeManager.parseString(json, 'percentage'),
	month = SafeManager.parseString(json, 'month');

	Map<String, dynamic> toJson() => {
				'disbursement_name': disbursementName,
				'percentage': percentage,
				'month': month,
			};
}
