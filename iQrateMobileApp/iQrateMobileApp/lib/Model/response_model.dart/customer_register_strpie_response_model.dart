// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

CustomerRegisterStripeResponseModel welcomeFromJson(String str) =>
    CustomerRegisterStripeResponseModel.fromJson(json.decode(str));

String welcomeToJson(CustomerRegisterStripeResponseModel data) =>
    json.encode(data.toJson());

class CustomerRegisterStripeResponseModel {
  CustomerRegisterStripeResponseModel({
    this.stripeId,
  });

  String? stripeId;

  factory CustomerRegisterStripeResponseModel.fromJson(
          Map<String, dynamic> json) =>
      CustomerRegisterStripeResponseModel(
        stripeId: json["stripe_id"],
      );

  Map<String, dynamic> toJson() => {
        "stripe_id": stripeId,
      };
}
