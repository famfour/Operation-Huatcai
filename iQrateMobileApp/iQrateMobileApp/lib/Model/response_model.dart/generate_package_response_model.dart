// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

GeneratePackageResponseModel welcomeFromJson(String str) =>
    GeneratePackageResponseModel.fromJson(json.decode(str));

String welcomeToJson(GeneratePackageResponseModel data) =>
    json.encode(data.toJson());

class GeneratePackageResponseModel {

  GeneratePackageResponseModel({
    this.id,
    this.bank,
    this.mask,
    this.rateCategory,
    this.rateType,
    this.propertyTypes,
    this.propertyStatus,
    this.loanCategory,
    this.lockInPeriod,
    this.minLoanAmount,
    this.depositToPlace,
    this.valuationSubsidy,
    this.fireInsuranceSubsidy,
    this.cashRebateLegalSubsidy,
    this.cashRebateSubsidyClawbackPeriodYears,
    this.partialRepaymentPenalty,
    this.partialRepaymentPenaltyRemarks,
    this.fullRepaymentPenalty,
    this.fullRepaymentPenaltyRemarks,
    this.rates,
    this.cancellationFee,
    this.depositToPlaceRemarks,
    this.remarksForClient,
    this.interestOffsetting,
    this.interestResetDate,
    this.processingFee,
    this.remarksForBroker,
    this.newPurchaseReferralFee,
    this.refinanceReferralFee,
    this.publish,
    this.createdBy,
    this.updatedBy,
    this.created,
    this.updated,
  });

  int? id;
  Bank? bank;
  bool? mask;
  String? rateCategory;
  String? rateType;
  List<String>? propertyTypes;
  List<String>? propertyStatus;
  List<String>? loanCategory;
  int? lockInPeriod;
  int? minLoanAmount;
  int? depositToPlace;
  List<String>? valuationSubsidy;
  String? fireInsuranceSubsidy;
  List<String>? cashRebateLegalSubsidy;
  int? cashRebateSubsidyClawbackPeriodYears;
  int? partialRepaymentPenalty;
  String? partialRepaymentPenaltyRemarks;
  int? fullRepaymentPenalty;
  String? fullRepaymentPenaltyRemarks;
  List<Rate>? rates;
  String? cancellationFee;
  String? depositToPlaceRemarks;
  List<String>? remarksForClient;
  String? interestOffsetting;
  bool? interestResetDate;
  bool? processingFee;
  String? remarksForBroker;
  int? newPurchaseReferralFee;
  int? refinanceReferralFee;
  bool? publish;
  String? createdBy;
  String? updatedBy;
  DateTime? created;
  DateTime? updated;

  factory GeneratePackageResponseModel.fromJson(Map<String, dynamic> json) =>
      GeneratePackageResponseModel(
        id: json["id"],
        bank: Bank.fromJson(json["bank"]),
        mask: json["mask"],
        rateCategory: json["rate_category"],
        rateType: json["rate_type"],
        propertyTypes: List<String>.from(json["property_types"].map((x) => x)),
        propertyStatus:
            List<String>.from(json["property_status"].map((x) => x)),
        loanCategory: List<String>.from(json["loan_category"].map((x) => x)),
        lockInPeriod: json["lock_in_period"],
        minLoanAmount: json["min_loan_amount"],
        depositToPlace: json["deposit_to_place"],
        valuationSubsidy:
            List<String>.from(json["valuation_subsidy"].map((x) => x)),
        fireInsuranceSubsidy: json["fire_insurance_subsidy"],
        cashRebateLegalSubsidy:
            List<String>.from(json["cash_rebate_legal_subsidy"].map((x) => x)),
        cashRebateSubsidyClawbackPeriodYears:
            json["cash_rebate_subsidy_clawback_period_years"],
        partialRepaymentPenalty: json["partial_repayment_penalty"],
        partialRepaymentPenaltyRemarks:
            json["partial_repayment_penalty_remarks"],
        fullRepaymentPenalty: json["full_repayment_penalty"],
        fullRepaymentPenaltyRemarks: json["full_repayment_penalty_remarks"],
        rates: List<Rate>.from(json["rates"].map((x) => Rate.fromJson(x))),
        cancellationFee: json["cancellation_fee"],
        depositToPlaceRemarks: json["deposit_to_place_remarks"],
        remarksForClient:
            List<String>.from(json["remarks_for_client"].map((x) => x)),
        interestOffsetting: json["interest_offsetting"],
        interestResetDate: json["interest_reset_date"],
        processingFee: json["processing_fee"],
        remarksForBroker: json["remarks_for_broker"],
        newPurchaseReferralFee: json["new_purchase_referral_fee"],
        refinanceReferralFee: json["refinance_referral_fee"],
        publish: json["publish"],
        createdBy: json["created_by"],
        updatedBy: json["updated_by"],
        created: DateTime.parse(json["created"]),
        updated: DateTime.parse(json["updated"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "bank": bank!.toJson(),
        "mask": mask,
        "rate_category": rateCategory,
        "rate_type": rateType,
        "property_types": List<dynamic>.from(propertyTypes!.map((x) => x)),
        "property_status": List<dynamic>.from(propertyStatus!.map((x) => x)),
        "loan_category": List<dynamic>.from(loanCategory!.map((x) => x)),
        "lock_in_period": lockInPeriod,
        "min_loan_amount": minLoanAmount,
        "deposit_to_place": depositToPlace,
        "valuation_subsidy":
            List<dynamic>.from(valuationSubsidy!.map((x) => x)),
        "fire_insurance_subsidy": fireInsuranceSubsidy,
        "cash_rebate_legal_subsidy":
            List<dynamic>.from(cashRebateLegalSubsidy!.map((x) => x)),
        "cash_rebate_subsidy_clawback_period_years":
            cashRebateSubsidyClawbackPeriodYears,
        "partial_repayment_penalty": partialRepaymentPenalty,
        "partial_repayment_penalty_remarks": partialRepaymentPenaltyRemarks,
        "full_repayment_penalty": fullRepaymentPenalty,
        "full_repayment_penalty_remarks": fullRepaymentPenaltyRemarks,
        "rates": List<dynamic>.from(rates!.map((x) => x.toJson())),
        "cancellation_fee": cancellationFee,
        "deposit_to_place_remarks": depositToPlaceRemarks,
        "remarks_for_client":
            List<dynamic>.from(remarksForClient!.map((x) => x)),
        "interest_offsetting": interestOffsetting,
        "interest_reset_date": interestResetDate,
        "processing_fee": processingFee,
        "remarks_for_broker": remarksForBroker,
        "new_purchase_referral_fee": newPurchaseReferralFee,
        "refinance_referral_fee": refinanceReferralFee,
        "publish": publish,
        "created_by": createdBy,
        "updated_by": updatedBy,
        "created": created!.toIso8601String(),
        "updated": updated!.toIso8601String(),
      };
}

class Bank {
  Bank({
    this.id,
    this.name,
    this.nameMasked,
    this.status,
    this.logo,
  });

  int? id;
  String? name;
  String? nameMasked;
  bool? status;
  String? logo;

  factory Bank.fromJson(Map<String, dynamic> json) => Bank(
        id: json["id"],
        name: json["name"],
        nameMasked: json["name_masked"],
        status: json["status"],
        logo: json["logo"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "name_masked": nameMasked,
        "status": status,
        "logo": logo,
      };
}

class Rate {
  Rate({
    this.year,
    this.referenceRate,
    this.bankSpread,
  });

  String? year;
  int? referenceRate;
  int? bankSpread;

  factory Rate.fromJson(Map<String, dynamic> json) => Rate(
        year: json["year"],
        referenceRate: json["reference_rate"],
        bankSpread: json["bank_spread"],
      );

  Map<String, dynamic> toJson() => {
        "year": year,
        "reference_rate": referenceRate,
        "bank_spread": bankSpread,
      };
}
