// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

VerifyPdpaResponseModel welcomeFromJson(String str) =>
    VerifyPdpaResponseModel.fromJson(json.decode(str));

String welcomeToJson(VerifyPdpaResponseModel data) =>
    json.encode(data.toJson());

class VerifyPdpaResponseModel {
  VerifyPdpaResponseModel({
    this.client,
    this.message,
    this.status,
  });

  int? client;
  String? message;
  bool? status;

  factory VerifyPdpaResponseModel.fromJson(Map<String, dynamic> json) =>
      VerifyPdpaResponseModel(
        client: json["client"],
        message: json["message"],
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "client": client,
        "message": message,
        "status": status,
      };
}
