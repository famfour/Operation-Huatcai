class SubscriptionPlansModel {
  List<OneSubscriptionPlan> subscriptionPlans;

  SubscriptionPlansModel({required this.subscriptionPlans});

  factory SubscriptionPlansModel.fromJson(json) {
    return SubscriptionPlansModel(
        subscriptionPlans: List<OneSubscriptionPlan>.from(
            json.map((value) => OneSubscriptionPlan.fromJson(value))));
  }
}

class OneSubscriptionPlan {
  String product;
  String id;
  List<PlanDetails> plan;

  OneSubscriptionPlan(
      {required this.product, required this.id, required this.plan});

  factory OneSubscriptionPlan.fromJson(json) {
    return OneSubscriptionPlan(
        product: json["product"],
        id: json["id"],
        plan: List<PlanDetails>.from(
            json["plan"].map((value) => PlanDetails.fromJson(value))));
  }
}

class PlanDetails {
  String planId;
  String planDuration;
  int amount;

  PlanDetails(
      {required this.planId, required this.planDuration, required this.amount});

  factory PlanDetails.fromJson(json) {
    return PlanDetails(
        planId: json["plan_id"],
        planDuration: json["plan"],
        amount: (json["amount"]));
  }
}
