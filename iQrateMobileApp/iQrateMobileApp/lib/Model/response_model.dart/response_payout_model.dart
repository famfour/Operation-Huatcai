
import 'package:iqrate/Model/response_model.dart/banker_model.dart';
import 'package:iqrate/Model/response_model.dart/documents_drawer_response_model.dart';
import 'package:iqrate/Model/send_model.dart/safe_convert.dart';
import 'package:iqrate/Model/send_model.dart/send_create_payout_v2.dart';

class ResponsePayoutModel {
  final int id;
  final Bank bank;
  final BankerModel banker;
  final List<LoansItem> loans;
  final List<Documents> documents;
  final String loanAcceptanceDate;
  final String loReferenceNumber;
  final int bankReferralFee;
  final String ownLawFirmName;
  final int legalFee;
  final String status;
  final String remarks;
  final int lead;
  final int lawFirm;

  ResponsePayoutModel({
    this.id = 0,
    required this.bank,
    required this.banker,
    required this.loans,
    required this.documents,
    this.loanAcceptanceDate = "",
    this.loReferenceNumber = "",
    this.bankReferralFee = 0,
    this.ownLawFirmName = "",
    this.legalFee = 0,
    this.status = "",
    this.remarks = "",
    this.lead = 0,
    this.lawFirm = 0,
  });

  factory ResponsePayoutModel.fromJson(Map<String, dynamic>? json) => ResponsePayoutModel(
    id: asInt(json, 'id'),
    bank: Bank.fromJson(asMap(json, 'bank')),
    banker: BankerModel.fromJson(asMap(json, 'banker')),
    loans: asList(json, 'loans').map((e) => LoansItem.fromJson(e)).toList(),
    documents: asList(json, 'documents').map((e) => Documents.fromJson(e)).toList(),
    loanAcceptanceDate: asString(json, 'loan_acceptance_date'),
    loReferenceNumber: asString(json, 'lo_reference_number'),
    bankReferralFee: asInt(json, 'bank_referral_fee'),
    ownLawFirmName: asString(json, 'own_law_firm_name'),
    legalFee: asInt(json, 'legal_fee'),
    status: asString(json, 'status'),
    remarks: asString(json, 'remarks'),
    lead: asInt(json, 'lead'),
    lawFirm: asInt(json, 'law_firm'),
  );

  Map<String, dynamic> toJson() => {
    'id': id,
    'bank': bank.toJson(),
    'banker': banker.toJson(),
    'loans': loans.map((e) => e.toJson()).toList(),
    'documents': documents.map((e) => e.toJson()).toList(),
    'loan_acceptance_date': loanAcceptanceDate,
    'lo_reference_number': loReferenceNumber,
    'bank_referral_fee': bankReferralFee,
    'own_law_firm_name': ownLawFirmName,
    'legal_fee': legalFee,
    'status': status,
    'remarks': remarks,
    'lead': lead,
    'law_firm': lawFirm,
  };
}

class Bank {
  final int id;
  final String name;
  final String nameMasked;
  final String logo;
  final List<FormsItem> forms;

  Bank({
    this.id = 0,
    this.name = "",
    this.nameMasked = "",
    this.logo = "",
    required this.forms,
  });

  factory Bank.fromJson(Map<String, dynamic>? json) => Bank(
    id: asInt(json, 'id'),
    name: asString(json, 'name'),
    nameMasked: asString(json, 'name_masked'),
    logo: asString(json, 'logo'),
    forms: asList(json, 'forms').map((e) => FormsItem.fromJson(e)).toList(),
  );

  Map<String, dynamic> toJson() => {
    'id': id,
    'name': name,
    'name_masked': nameMasked,
    'logo': logo,
    'forms': forms.map((e) => e.toJson()).toList(),
  };
}

class FormsItem {
  final String name;
  final String url;

  FormsItem({
    this.name = "",
    this.url = "",
  });

  factory FormsItem.fromJson(Map<String, dynamic>? json) => FormsItem(
    name: asString(json, 'name'),
    url: asString(json, 'url'),
  );

  Map<String, dynamic> toJson() => {
    'name': name,
    'url': url,
  };
}

class RatesItem {
  final String year;
  final int bankSpread;
  final int totalInterestRate;
  final int referenceRate;

  RatesItem({
    this.year = "",
    this.bankSpread = 0,
    this.totalInterestRate = 0,
    this.referenceRate = 0,
  });

  factory RatesItem.fromJson(Map<String, dynamic>? json) => RatesItem(
    year: asString(json, 'year'),
    bankSpread: asInt(json, 'bank_spread'),
    totalInterestRate: asInt(json, 'total_interest_rate'),
    referenceRate: asInt(json, 'reference_rate'),
  );

  Map<String, dynamic> toJson() => {
    'year': year,
    'bank_spread': bankSpread,
    'total_interest_rate': totalInterestRate,
    'reference_rate': referenceRate,
  };
}


/*class Documents {
  final String id;
  final String filename;
  final String docType;
  final String created;
  final String updated;

  Documents({
    this.id = "",
    this.filename = "",
    this.docType = "",
    this.created = "",
    this.updated = "",
  });

  factory Documents.fromJson(Map<String, dynamic>? json) => Documents(
    id: asString(json, 'id'),
    filename: asString(json, 'filename'),
    docType: asString(json, 'doc_type'),
    created: asString(json, 'created'),
    updated: asString(json, 'updated'),
  );

  Map<String, dynamic> toJson() => {
    'id': id,
    'filename': filename,
    'doc_type': docType,
    'created': created,
    'updated': updated,
  };
}*/

