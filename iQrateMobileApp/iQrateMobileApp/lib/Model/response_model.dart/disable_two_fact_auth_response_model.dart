// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

DisableTwoFaResponseModel welcomeFromJson(String str) =>
    DisableTwoFaResponseModel.fromJson(json.decode(str));

String welcomeToJson(DisableTwoFaResponseModel data) =>
    json.encode(data.toJson());

class DisableTwoFaResponseModel {
  DisableTwoFaResponseModel({
    this.message,
  });

  String? message;

  factory DisableTwoFaResponseModel.fromJson(Map<String, dynamic> json) =>
      DisableTwoFaResponseModel(
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "message": message,
      };
}
