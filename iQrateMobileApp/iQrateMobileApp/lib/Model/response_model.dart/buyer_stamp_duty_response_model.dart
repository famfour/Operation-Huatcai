// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

BuyerStampDutyResponseModel welcomeFromJson(String str) =>
    BuyerStampDutyResponseModel.fromJson(json.decode(str));

String welcomeToJson(BuyerStampDutyResponseModel data) =>
    json.encode(data.toJson());

class BuyerStampDutyResponseModel {
  BuyerStampDutyResponseModel({
    this.buyerStampDuty,
    this.additionalBuyerStampDuty,
    this.totalBuyerStampDuty,
  });

  String? buyerStampDuty;
  String? additionalBuyerStampDuty;
  String? totalBuyerStampDuty;

  factory BuyerStampDutyResponseModel.fromJson(Map<String, dynamic> json) =>
      BuyerStampDutyResponseModel(
        buyerStampDuty: json["buyer_stamp_duty"],
        additionalBuyerStampDuty: json["additional_buyer_stamp_duty"],
        totalBuyerStampDuty: json["total_buyer_stamp_duty"],
      );

  Map<String, dynamic> toJson() => {
        "buyer_stamp_duty": buyerStampDuty,
        "additional_buyer_stamp_duty": additionalBuyerStampDuty,
        "total_buyer_stamp_duty": totalBuyerStampDuty,
      };
}
