// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

UpdatePasswordResponseModel welcomeFromJson(String str) =>
    UpdatePasswordResponseModel.fromJson(json.decode(str));

String welcomeToJson(UpdatePasswordResponseModel data) =>
    json.encode(data.toJson());

class UpdatePasswordResponseModel {
  UpdatePasswordResponseModel({
    this.message,
  });

  String? message;

  factory UpdatePasswordResponseModel.fromJson(Map<String, dynamic> json) =>
      UpdatePasswordResponseModel(
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "message": message,
      };
}
