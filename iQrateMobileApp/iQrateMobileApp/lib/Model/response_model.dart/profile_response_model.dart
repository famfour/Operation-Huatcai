// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

// ignore_for_file: constant_identifier_names

import 'dart:convert';

ProfileResponseModel welcomeFromJson(String str) =>
    ProfileResponseModel.fromJson(json.decode(str));

String welcomeToJson(ProfileResponseModel data) => json.encode(data.toJson());

class ProfileResponseModel {
  ProfileResponseModel({
    this.id,
    this.fullName,
    this.email,
    this.stripeCustomerId,
    this.stripePaymentMethodId,
    this.stripeSubscriptionId,
    this.stripePriceId,
    this.countryCode,
    this.mobile,
    this.wholeNumber,
    this.phone,
    this.userType,
    this.membershipType,
    this.photo,
    this.verificationCodeSendDatetime,
    this.status,
    this.createdUserId,
    this.updatedUserId,
    this.createdAt,
    this.updatedAt,
    this.dob,
    this.device,
    this.ip,
    this.location,
    this.occupation,
    this.postalCode,
    this.isEmailVerified,
    this.isMobileVerified,
    this.address,
    this.twoFactorAuthenticationSecret,
    this.isTwoFactorAuthenticationEnabled,
    this.registerType,
    this.countryCodeLabel,
    this.refreshToken,
    this.referalCode,
    this.cobrokeStatus,
    this.bankers,
    this.aclData,
  });

  String? id;
  String? fullName;
  String? email;
  String? stripeCustomerId;
  String? stripePaymentMethodId;
  String? stripeSubscriptionId;
  String? stripePriceId;
  String? countryCode;
  String? mobile;
  String? wholeNumber;
  dynamic phone;
  String? userType;
  String? membershipType;
  dynamic photo;
  dynamic verificationCodeSendDatetime;
  int? status;
  String? createdUserId;
  String? updatedUserId;
  DateTime? createdAt;
  DateTime? updatedAt;
  String? dob;
  dynamic device;
  dynamic ip;
  dynamic location;
  String? occupation;
  dynamic postalCode;
  int? isEmailVerified;
  int? isMobileVerified;
  dynamic address;
  String? twoFactorAuthenticationSecret;
  bool? isTwoFactorAuthenticationEnabled;
  String? registerType;
  String? countryCodeLabel;
  String? refreshToken;
  String? referalCode;
  int? cobrokeStatus;
  List<dynamic>? bankers;
  List<Map<String, AclDatum>>? aclData;

  factory ProfileResponseModel.fromJson(Map<String, dynamic> json) =>
      ProfileResponseModel(
        id: json["id"],
        fullName: json["full_name"],
        email: json["email"],
        stripeCustomerId: json["stripe_customer_id"],
        stripePaymentMethodId: json["stripe_payment_method_id"],
        stripeSubscriptionId: json["stripe_subscription_id"],
        stripePriceId: json["stripe_price_id"],
        countryCode: json["country_code"],
        mobile: json["mobile"],
        wholeNumber: json["whole_number"],
        phone: json["phone"],
        userType: json["user_type"],
        membershipType: json["membership_type"],
        photo: json["photo"],
        verificationCodeSendDatetime: json["verification_code_send_datetime"],
        status: json["status"],
        createdUserId: json["created_user_id"],
        updatedUserId: json["updated_user_id"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        dob: json["dob"],
        device: json["device"],
        ip: json["ip"],
        location: json["location"],
        occupation: json["occupation"],
        postalCode: json["postal_code"],
        isEmailVerified: json["is_email_verified"],
        isMobileVerified: json["is_mobile_verified"],
        address: json["address"],
        twoFactorAuthenticationSecret: json["twoFactorAuthenticationSecret"],
        isTwoFactorAuthenticationEnabled:
            json["isTwoFactorAuthenticationEnabled"],
        registerType: json["register_type"],
        countryCodeLabel: json["country_code_label"],
        refreshToken: json["refresh_token"],
        referalCode: json["referal_code"],
        cobrokeStatus: json["cobroke_status"],
        bankers: List<dynamic>.from(json["bankers"].map((x) => x)),
        aclData: List<Map<String, AclDatum>>.from(json["acl_data"].map((x) =>
            Map.from(x).map((k, v) =>
                MapEntry<String, AclDatum>(k, AclDatum.fromJson(v))))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "full_name": fullName,
        "email": email,
        "stripe_customer_id": stripeCustomerId,
        "stripe_payment_method_id": stripePaymentMethodId,
        "stripe_subscription_id": stripeSubscriptionId,
        "stripe_price_id": stripePriceId,
        "country_code": countryCode,
        "mobile": mobile,
        "whole_number": wholeNumber,
        "phone": phone,
        "user_type": userType,
        "membership_type": membershipType,
        "photo": photo,
        "verification_code_send_datetime": verificationCodeSendDatetime,
        "status": status,
        "created_user_id": createdUserId,
        "updated_user_id": updatedUserId,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
        "dob": dob,
        "device": device,
        "ip": ip,
        "location": location,
        "occupation": occupation,
        "postal_code": postalCode,
        "is_email_verified": isEmailVerified,
        "is_mobile_verified": isMobileVerified,
        "address": address,
        "twoFactorAuthenticationSecret": twoFactorAuthenticationSecret,
        "isTwoFactorAuthenticationEnabled": isTwoFactorAuthenticationEnabled,
        "register_type": registerType,
        "country_code_label": countryCodeLabel,
        "refresh_token": refreshToken,
        "referal_code": referalCode,
        "cobroke_status": cobrokeStatus,
        "bankers": List<dynamic>.from(bankers!.map((x) => x)),
        "acl_data": List<dynamic>.from(aclData!.map((x) => Map.from(x)
            .map((k, v) => MapEntry<String, dynamic>(k, v.toJson())))),
      };
}

class AclDatum {
  AclDatum({
    this.permission,
    this.ratelimit,
    this.accesstype,
    this.microservice,
  });

  Permission? permission;
  String? ratelimit;
  Accesstype? accesstype;
  Microservice? microservice;

  factory AclDatum.fromJson(Map<String, dynamic> json) => AclDatum(
        permission: permissionValues.map![json["permission"]],
        ratelimit: json["ratelimit"],
        accesstype: accesstypeValues.map![json["accesstype"]],
        microservice: microserviceValues.map![json["microservice"]],
      );

  Map<String, dynamic> toJson() => {
        "permission": permissionValues.reverse[permission],
        "ratelimit": ratelimit,
        "accesstype": accesstypeValues.reverse[accesstype],
        "microservice": microserviceValues.reverse[microservice],
      };
}

enum Accesstype { EMPTY, DELETE, GET, PUT, POST, SELF }

final accesstypeValues = EnumValues({
  "DELETE": Accesstype.DELETE,
  "*": Accesstype.EMPTY,
  "GET": Accesstype.GET,
  "POST": Accesstype.POST,
  "PUT": Accesstype.PUT,
  "self": Accesstype.SELF
});

enum Microservice { CUSTOMER, NOTIFICATION, USER, CONFIG }

final microserviceValues = EnumValues({
  "config": Microservice.CONFIG,
  "customer": Microservice.CUSTOMER,
  "notification": Microservice.NOTIFICATION,
  "user": Microservice.USER
});

enum Permission { ALLOW }

final permissionValues = EnumValues({"ALLOW": Permission.ALLOW});

class EnumValues<T> {
  Map<String, T>? map;
  Map<T, String>? reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    reverseMap ??= map!.map((k, v) => MapEntry(v, k));
    return reverseMap!;
  }
}
