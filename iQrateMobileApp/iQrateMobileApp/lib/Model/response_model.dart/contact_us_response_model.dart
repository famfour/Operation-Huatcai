// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

ContactUsResponseModel welcomeFromJson(String str) =>
    ContactUsResponseModel.fromJson(json.decode(str));

String welcomeToJson(ContactUsResponseModel data) => json.encode(data.toJson());

class ContactUsResponseModel {
  ContactUsResponseModel({
    this.message,
    this.data,
  });

  String? message;
  Data? data;

  factory ContactUsResponseModel.fromJson(Map<String, dynamic> json) =>
      ContactUsResponseModel(
        message: json["message"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "data": data!.toJson(),
      };
}

class Data {
  Data({
    this.name,
    this.email,
    this.message,
    this.createdAt,
    this.createdUserId,
    this.updatedUserId,
    this.updatedAt,
    this.id,
  });

  String? name;
  String? email;
  String? message;
  DateTime? createdAt;
  String? createdUserId;
  dynamic updatedUserId;
  dynamic updatedAt;
  int? id;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        name: json["name"],
        email: json["email"],
        message: json["message"],
        createdAt: DateTime.parse(json["created_at"]),
        createdUserId: json["created_user_id"],
        updatedUserId: json["updated_user_id"],
        updatedAt: json["updated_at"],
        id: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "email": email,
        "message": message,
        "created_at": createdAt!.toIso8601String(),
        "created_user_id": createdUserId,
        "updated_user_id": updatedUserId,
        "updated_at": updatedAt,
        "id": id,
      };
}
