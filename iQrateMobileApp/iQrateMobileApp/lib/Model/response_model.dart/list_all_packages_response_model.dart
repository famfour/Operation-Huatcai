// class ListAllPackagesResponseModel {
//   int? count;
//   String? next;
//   String? previous;
//   List<PackageModel>? results;
//
//   ListAllPackagesResponseModel({this.count, this.next, this.previous, this.results});
//
//   ListAllPackagesResponseModel.fromJson(Map<String, dynamic> json) {
//     count = json['count'];
//     next = json['next'];
//     previous = json['previous'];
//     if (json['results'] != null) {
//       results = <PackageModel>[];
//       json['results'].forEach((v) {
//         results!.add(new PackageModel.fromJson(v));
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['count'] = this.count;
//     data['next'] = this.next;
//     data['previous'] = this.previous;
//     if (this.results != null) {
//       data['results'] = this.results!.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }
//
// class PackageModel {
//   int? id;
//   Bank? bank;
//   bool? mask;
//   String? rateCategory;
//   String? rateType;
//   List<String>? propertyTypes;
//   List<String>? propertyStatus;
//   List<String>? loanCategory;
//   double? lockInPeriod;
//   double? minLoanAmount;
//   double? depositToPlace;
//   List<String>? valuationSubsidy;
//   String? fireInsuranceSubsidy;
//   List<String>? cashRebateLegalSubsidy;
//   double? cashRebateSubsidyClawbackPeriodYears;
//   double? partialRepaymentPenalty;
//   String? partialRepaymentPenaltyRemarks;
//   double? fullRepaymentPenalty;
//   String? fullRepaymentPenaltyRemarks;
//   List<Rates>? rates;
//   String? cancellationFee;
//   String? depositToPlaceRemarks;
//   List<String>? remarksForClient;
//   String? interestOffsetting;
//   bool? interestResetDate;
//   bool? processingFee;
//   String? remarksForBroker;
//   double? newPurchaseReferralFee;
//   double? refinanceReferralFee;
//   String? createdBy;
//   String? updatedBy;
//   String? created;
//   String? updated;
//
//   bool? isChecked;
//
//   PackageModel(
//       {this.id,
//         this.bank,
//         this.mask,
//         this.rateCategory,
//         this.rateType,
//         this.propertyTypes,
//         this.propertyStatus,
//         this.loanCategory,
//         this.lockInPeriod,
//         this.minLoanAmount,
//         this.depositToPlace,
//         this.valuationSubsidy,
//         this.fireInsuranceSubsidy,
//         this.cashRebateLegalSubsidy,
//         this.cashRebateSubsidyClawbackPeriodYears,
//         this.partialRepaymentPenalty,
//         this.partialRepaymentPenaltyRemarks,
//         this.fullRepaymentPenalty,
//         this.fullRepaymentPenaltyRemarks,
//         this.rates,
//         this.isChecked,
//         this.cancellationFee,
//         this.depositToPlaceRemarks,
//         this.remarksForClient,
//         this.interestOffsetting,
//         this.interestResetDate,
//         this.processingFee,
//         this.remarksForBroker,
//         this.newPurchaseReferralFee,
//         this.refinanceReferralFee,
//         this.createdBy,
//         this.updatedBy,
//         this.created,
//         this.updated});
//
//   PackageModel.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     bank = json['bank'] != null ? new Bank.fromJson(json['bank']) : null;
//     mask = json['mask'];
//     rateCategory = json['rate_category'];
//     rateType = json['rate_type'];
//     propertyTypes = json['property_types'].cast<String>();
//     propertyStatus = json['property_status'].cast<String>();
//     loanCategory = json['loan_category'].cast<String>();
//     lockInPeriod = json['lock_in_period'];
//     minLoanAmount = json['min_loan_amount'];
//     depositToPlace = json['deposit_to_place'];
//     isChecked = json['isChecked'] ?? false;
//     valuationSubsidy = json['valuation_subsidy'].cast<String>();
//     fireInsuranceSubsidy = json['fire_insurance_subsidy'];
//     cashRebateLegalSubsidy = json['cash_rebate_legal_subsidy'].cast<String>();
//     cashRebateSubsidyClawbackPeriodYears =
//     json['cash_rebate_subsidy_clawback_period_years'];
//     partialRepaymentPenalty = json['partial_repayment_penalty'];
//     partialRepaymentPenaltyRemarks = json['partial_repayment_penalty_remarks'];
//     fullRepaymentPenalty = json['full_repayment_penalty'];
//     fullRepaymentPenaltyRemarks = json['full_repayment_penalty_remarks'];
//     if (json['rates'] != null) {
//       rates = <Rates>[];
//       json['rates'].forEach((v) {
//         rates!.add(new Rates.fromJson(v));
//       });
//     }
//     cancellationFee = json['cancellation_fee'];
//     depositToPlaceRemarks = json['deposit_to_place_remarks'];
//     remarksForClient = json['remarks_for_client'].cast<String>();
//     interestOffsetting = json['interest_offsetting'];
//     interestResetDate = json['interest_reset_date'];
//     processingFee = json['processing_fee'];
//     remarksForBroker = json['remarks_for_broker'];
//     newPurchaseReferralFee = json['new_purchase_referral_fee'];
//     refinanceReferralFee = json['refinance_referral_fee'];
//     createdBy = json['created_by'];
//     updatedBy = json['updated_by'];
//     created = json['created'];
//     updated = json['updated'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     if (this.bank != null) {
//       data['bank'] = this.bank!.toJson();
//     }
//     data['isChecked'] = this.isChecked;
//     data['mask'] = this.mask;
//     data['rate_category'] = this.rateCategory;
//     data['rate_type'] = this.rateType;
//     data['property_types'] = this.propertyTypes;
//     data['property_status'] = this.propertyStatus;
//     data['loan_category'] = this.loanCategory;
//     data['lock_in_period'] = this.lockInPeriod;
//     data['min_loan_amount'] = this.minLoanAmount;
//     data['deposit_to_place'] = this.depositToPlace;
//     data['valuation_subsidy'] = this.valuationSubsidy;
//     data['fire_insurance_subsidy'] = this.fireInsuranceSubsidy;
//     data['cash_rebate_legal_subsidy'] = this.cashRebateLegalSubsidy;
//     data['cash_rebate_subsidy_clawback_period_years'] =
//         this.cashRebateSubsidyClawbackPeriodYears;
//     data['partial_repayment_penalty'] = this.partialRepaymentPenalty;
//     data['partial_repayment_penalty_remarks'] =
//         this.partialRepaymentPenaltyRemarks;
//     data['full_repayment_penalty'] = this.fullRepaymentPenalty;
//     data['full_repayment_penalty_remarks'] = this.fullRepaymentPenaltyRemarks;
//     if (this.rates != null) {
//       data['rates'] = this.rates!.map((v) => v.toJson()).toList();
//     }
//     data['cancellation_fee'] = this.cancellationFee;
//     data['deposit_to_place_remarks'] = this.depositToPlaceRemarks;
//     data['remarks_for_client'] = this.remarksForClient;
//     data['interest_offsetting'] = this.interestOffsetting;
//     data['interest_reset_date'] = this.interestResetDate;
//     data['processing_fee'] = this.processingFee;
//     data['remarks_for_broker'] = this.remarksForBroker;
//     data['new_purchase_referral_fee'] = this.newPurchaseReferralFee;
//     data['refinance_referral_fee'] = this.refinanceReferralFee;
//     data['created_by'] = this.createdBy;
//     data['updated_by'] = this.updatedBy;
//     data['created'] = this.created;
//     data['updated'] = this.updated;
//     return data;
//   }
// }
//
// class Bank {
//   int? id;
//   String? name;
//   String? logo;
//   String? bankForm;
//
//   Bank({this.id, this.name, this.logo, this.bankForm});
//
//   Bank.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     name = json['name'];
//     logo = json['logo'];
//     bankForm = json['bank_form'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['name'] = this.name;
//     data['logo'] = this.logo;
//     data['bank_form'] = this.bankForm;
//     return data;
//   }
// }
//
// class Rates {
//   String? year;
//   ReferenceRate? referenceRate;
//   double? bankSpread;
//   String? totalInterestRate;
//
//   Rates(
//       {this.year, this.referenceRate, this.bankSpread, this.totalInterestRate});
//
//   Rates.fromJson(Map<String, dynamic> json) {
//     year = json['year'];
//     referenceRate = json['reference_rate'] != null
//         ? new ReferenceRate.fromJson(json['reference_rate'])
//         : null;
//     bankSpread = json['bank_spread'];
//     totalInterestRate = json['total_interest_rate'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['year'] = this.year;
//     if (this.referenceRate != null) {
//       data['reference_rate'] = this.referenceRate!.toJson();
//     }
//     data['bank_spread'] = this.bankSpread;
//     data['total_interest_rate'] = this.totalInterestRate;
//     return data;
//   }
// }
//
// class ReferenceRate {
//   String? rateType;
//   String? reference;
//   double? interestRate;
//   String? equation;
//   String? remarks;
//
//   ReferenceRate(
//       {this.rateType,
//         this.reference,
//         this.interestRate,
//         this.equation,
//         this.remarks});
//
//   ReferenceRate.fromJson(Map<String, dynamic> json) {
//     rateType = json['rate_type'];
//     reference = json['reference'];
//     interestRate = json['interest_rate'];
//     equation = json['equation'];
//     remarks = json['remarks'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['rate_type'] = this.rateType;
//     data['reference'] = this.reference;
//     data['interest_rate'] = this.interestRate;
//     data['equation'] = this.equation;
//     data['remarks'] = this.remarks;
//     return data;
//   }
// }