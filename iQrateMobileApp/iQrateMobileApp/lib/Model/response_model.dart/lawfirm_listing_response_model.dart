class LawFirmListingsResponseModel {
  int? count;
  String? next;
  String? previous;
  List<OneResult>? result;

  LawFirmListingsResponseModel(
      {this.count, this.next, this.previous, this.result});

  factory LawFirmListingsResponseModel.fromJson(json) {
    return LawFirmListingsResponseModel(
        count: json['count'],
        next: json['next'],
        previous: json['previous'],
        result: List<OneResult>.from(
            json['results'].map((data) => OneResult.fromJson(data))));
  }
}

class OneResult {
  int? id;
  String? name;
  String? email;
  int? priority;
  String? remarks;
  List<OneProduct>? productList;

  OneResult(
      {this.id,
      this.name,
      this.email,
      this.priority,
      this.remarks,
      this.productList});

  factory OneResult.fromJson(json) {
    return OneResult(
        id: json['id'],
        name: json['name'],
        email: json['email'],
        priority: json['priority'],
        remarks: json['remarks'],
        productList: List<OneProduct>.from(
            json['products'].map((data) => OneProduct.fromJson(data))));
  }
}

class OneProduct {
  int? id;
  List? loanCategory;
  List? propertyType;
  double? loanRangesFrom;
  double? loanRangesTo;
  bool? fixedLeaglFee;
  double? legalFees;

  OneProduct(
      {this.id,
      this.loanCategory,
      this.propertyType,
      this.loanRangesFrom,
      this.loanRangesTo,
      this.fixedLeaglFee,
      this.legalFees});

  factory OneProduct.fromJson(json) {
    return OneProduct(
        id: json['id'],
        loanCategory: json['loan_category'],
        propertyType: json['property_types'],
        loanRangesFrom: json['loan_range_from'],
        loanRangesTo: json['loan_range_to'],
        fixedLeaglFee: json['fixed_legal_fee'],
        legalFees: json['legal_fee']);
  }
}
