// ignore_for_file: non_constant_identifier_names, file_names

import '../../Model/response_model.dart/safe_convert.dart';

class BucMortgageRepaymentTableModel {
  int year = 0;
  int month = 0;
  String interestRate = "";
  double disbursement_ratio = 0;
  double outstanding_principal_start = 0;
  double monthly_installment = 0;
  String interest_payment = "";
  String principal_payment = "";
  String outstanding_principal_end = "";
  //outstanding_principal_end

  BucMortgageRepaymentTableModel(
      {this.year = 0,
      this.month = 0,
      this.interestRate = "",
      this.disbursement_ratio = 0,
      this.outstanding_principal_start = 0,
      this.monthly_installment = 0,
      this.interest_payment = "",
      this.principal_payment = "",
      this.outstanding_principal_end = ""});

  BucMortgageRepaymentTableModel.fromJson(Map<String, dynamic> json)
      : year = SafeManager.parseInt(json, 'year'),
        month = SafeManager.parseInt(json, 'month'),
        interestRate = SafeManager.parseString(json, 'interest_rate'),
        disbursement_ratio =
            SafeManager.parseDouble(json, 'disbursement_ratio'),
        outstanding_principal_start =
            SafeManager.parseDouble(json, 'outstanding_principal_start'),
        monthly_installment =
            SafeManager.parseDouble(json, 'monthly_installment'),
        interest_payment = SafeManager.parseString(json, 'interest_payment'),
        principal_payment = SafeManager.parseString(json, 'principal_payment'),
        outstanding_principal_end =
            SafeManager.parseString(json, 'outstanding_principal_end');

  Map<String, dynamic> toJson() => {
        'year': year,
        'month': month,
        'interest_rate': interestRate,
        'installment_amount': disbursement_ratio,
        'outstanding_principal_start': outstanding_principal_start,
        'monthly_installment': monthly_installment,
        'interest_payment': interest_payment,
        'principal_payment': principal_payment,
        'outstanding_principal_end': outstanding_principal_end,
      };
}
