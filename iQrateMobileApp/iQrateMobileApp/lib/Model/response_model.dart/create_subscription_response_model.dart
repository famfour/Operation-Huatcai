// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

CreateSubscriptionResponseModel welcomeFromJson(String str) =>
    CreateSubscriptionResponseModel.fromJson(json.decode(str));

String welcomeToJson(CreateSubscriptionResponseModel data) =>
    json.encode(data.toJson());

class CreateSubscriptionResponseModel {
  CreateSubscriptionResponseModel({
    this.id,
    this.object,
    this.applicationFeePercent,
    this.automaticTax,
    this.billingCycleAnchor,
    this.billingThresholds,
    this.cancelAt,
    this.cancelAtPeriodEnd,
    this.canceledAt,
    this.collectionMethod,
    this.created,
    this.currentPeriodEnd,
    this.currentPeriodStart,
    this.customer,
    this.daysUntilDue,
    this.defaultPaymentMethod,
    this.defaultSource,
    this.defaultTaxRates,
    this.discount,
    this.endedAt,
    this.items,
    this.latestInvoice,
    this.livemode,
    this.metadata,
    this.nextPendingInvoiceItemInvoice,
    this.pauseCollection,
    this.paymentSettings,
    this.pendingInvoiceItemInterval,
    this.pendingSetupIntent,
    this.pendingUpdate,
    this.plan,
    this.quantity,
    this.schedule,
    this.startDate,
    this.status,
    this.testClock,
    this.transferData,
    this.trialEnd,
    this.trialStart,
  });

  String? id;
  String? object;
  dynamic applicationFeePercent;
  AutomaticTax? automaticTax;
  int? billingCycleAnchor;
  dynamic billingThresholds;
  dynamic cancelAt;
  bool? cancelAtPeriodEnd;
  dynamic canceledAt;
  String? collectionMethod;
  int? created;
  int? currentPeriodEnd;
  int? currentPeriodStart;
  String? customer;
  dynamic daysUntilDue;
  dynamic defaultPaymentMethod;
  dynamic defaultSource;
  List<dynamic>? defaultTaxRates;
  dynamic discount;
  dynamic endedAt;
  Items? items;
  String? latestInvoice;
  bool? livemode;
  Metadata? metadata;
  dynamic nextPendingInvoiceItemInvoice;
  dynamic pauseCollection;
  PaymentSettings? paymentSettings;
  dynamic pendingInvoiceItemInterval;
  dynamic pendingSetupIntent;
  dynamic pendingUpdate;
  Plan? plan;
  int? quantity;
  dynamic schedule;
  int? startDate;
  String? status;
  dynamic testClock;
  dynamic transferData;
  dynamic trialEnd;
  dynamic trialStart;

  factory CreateSubscriptionResponseModel.fromJson(Map<String, dynamic> json) =>
      CreateSubscriptionResponseModel(
        id: json["id"],
        object: json["object"],
        applicationFeePercent: json["application_fee_percent"],
        automaticTax: AutomaticTax.fromJson(json["automatic_tax"]),
        billingCycleAnchor: json["billing_cycle_anchor"],
        billingThresholds: json["billing_thresholds"],
        cancelAt: json["cancel_at"],
        cancelAtPeriodEnd: json["cancel_at_period_end"],
        canceledAt: json["canceled_at"],
        collectionMethod: json["collection_method"],
        created: json["created"],
        currentPeriodEnd: json["current_period_end"],
        currentPeriodStart: json["current_period_start"],
        customer: json["customer"],
        daysUntilDue: json["days_until_due"],
        defaultPaymentMethod: json["default_payment_method"],
        defaultSource: json["default_source"],
        defaultTaxRates:
            List<dynamic>.from(json["default_tax_rates"].map((x) => x)),
        discount: json["discount"],
        endedAt: json["ended_at"],
        items: Items.fromJson(json["items"]),
        latestInvoice: json["latest_invoice"],
        livemode: json["livemode"],
        metadata: Metadata.fromJson(json["metadata"]),
        nextPendingInvoiceItemInvoice:
            json["next_pending_invoice_item_invoice"],
        pauseCollection: json["pause_collection"],
        paymentSettings: PaymentSettings.fromJson(json["payment_settings"]),
        pendingInvoiceItemInterval: json["pending_invoice_item_interval"],
        pendingSetupIntent: json["pending_setup_intent"],
        pendingUpdate: json["pending_update"],
        plan: Plan.fromJson(json["plan"]),
        quantity: json["quantity"],
        schedule: json["schedule"],
        startDate: json["start_date"],
        status: json["status"],
        testClock: json["test_clock"],
        transferData: json["transfer_data"],
        trialEnd: json["trial_end"],
        trialStart: json["trial_start"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "object": object,
        "application_fee_percent": applicationFeePercent,
        "automatic_tax": automaticTax!.toJson(),
        "billing_cycle_anchor": billingCycleAnchor,
        "billing_thresholds": billingThresholds,
        "cancel_at": cancelAt,
        "cancel_at_period_end": cancelAtPeriodEnd,
        "canceled_at": canceledAt,
        "collection_method": collectionMethod,
        "created": created,
        "current_period_end": currentPeriodEnd,
        "current_period_start": currentPeriodStart,
        "customer": customer,
        "days_until_due": daysUntilDue,
        "default_payment_method": defaultPaymentMethod,
        "default_source": defaultSource,
        "default_tax_rates": List<dynamic>.from(defaultTaxRates!.map((x) => x)),
        "discount": discount,
        "ended_at": endedAt,
        "items": items!.toJson(),
        "latest_invoice": latestInvoice,
        "livemode": livemode,
        "metadata": metadata!.toJson(),
        "next_pending_invoice_item_invoice": nextPendingInvoiceItemInvoice,
        "pause_collection": pauseCollection,
        "payment_settings": paymentSettings!.toJson(),
        "pending_invoice_item_interval": pendingInvoiceItemInterval,
        "pending_setup_intent": pendingSetupIntent,
        "pending_update": pendingUpdate,
        "plan": plan!.toJson(),
        "quantity": quantity,
        "schedule": schedule,
        "start_date": startDate,
        "status": status,
        "test_clock": testClock,
        "transfer_data": transferData,
        "trial_end": trialEnd,
        "trial_start": trialStart,
      };
}

class AutomaticTax {
  AutomaticTax({
    this.enabled,
  });

  bool? enabled;

  factory AutomaticTax.fromJson(Map<String, dynamic> json) => AutomaticTax(
        enabled: json["enabled"],
      );

  Map<String, dynamic> toJson() => {
        "enabled": enabled,
      };
}

class Items {
  Items({
    this.object,
    this.data,
    this.hasMore,
    this.totalCount,
    this.url,
  });

  String? object;
  List<Datum>? data;
  bool? hasMore;
  int? totalCount;
  String? url;

  factory Items.fromJson(Map<String, dynamic> json) => Items(
        object: json["object"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        hasMore: json["has_more"],
        totalCount: json["total_count"],
        url: json["url"],
      );

  Map<String, dynamic> toJson() => {
        "object": object,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
        "has_more": hasMore,
        "total_count": totalCount,
        "url": url,
      };
}

class Datum {
  Datum({
    this.id,
    this.object,
    this.billingThresholds,
    this.created,
    this.metadata,
    this.plan,
    this.price,
    this.quantity,
    this.subscription,
    this.taxRates,
  });

  String? id;
  String? object;
  dynamic billingThresholds;
  int? created;
  Metadata? metadata;
  Plan? plan;
  Price? price;
  int? quantity;
  String? subscription;
  List<dynamic>? taxRates;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        object: json["object"],
        billingThresholds: json["billing_thresholds"],
        created: json["created"],
        metadata: Metadata.fromJson(json["metadata"]),
        plan: Plan.fromJson(json["plan"]),
        price: Price.fromJson(json["price"]),
        quantity: json["quantity"],
        subscription: json["subscription"],
        taxRates: List<dynamic>.from(json["tax_rates"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "object": object,
        "billing_thresholds": billingThresholds,
        "created": created,
        "metadata": metadata!.toJson(),
        "plan": plan!.toJson(),
        "price": price!.toJson(),
        "quantity": quantity,
        "subscription": subscription,
        "tax_rates": List<dynamic>.from(taxRates!.map((x) => x)),
      };
}

class Metadata {
  Metadata();

  factory Metadata.fromJson(Map<String, dynamic> json) => Metadata();

  Map<String, dynamic> toJson() => {};
}

class Plan {
  Plan({
    this.id,
    this.object,
    this.active,
    this.aggregateUsage,
    this.amount,
    this.amountDecimal,
    this.billingScheme,
    this.created,
    this.currency,
    this.interval,
    this.intervalCount,
    this.livemode,
    this.metadata,
    this.nickname,
    this.product,
    this.tiersMode,
    this.transformUsage,
    this.trialPeriodDays,
    this.usageType,
  });

  String? id;
  String? object;
  bool? active;
  dynamic aggregateUsage;
  int? amount;
  String? amountDecimal;
  String? billingScheme;
  int? created;
  String? currency;
  String? interval;
  int? intervalCount;
  bool? livemode;
  Metadata? metadata;
  dynamic nickname;
  String? product;
  dynamic tiersMode;
  dynamic transformUsage;
  dynamic trialPeriodDays;
  String? usageType;

  factory Plan.fromJson(Map<String, dynamic> json) => Plan(
        id: json["id"],
        object: json["object"],
        active: json["active"],
        aggregateUsage: json["aggregate_usage"],
        amount: json["amount"],
        amountDecimal: json["amount_decimal"],
        billingScheme: json["billing_scheme"],
        created: json["created"],
        currency: json["currency"],
        interval: json["interval"],
        intervalCount: json["interval_count"],
        livemode: json["livemode"],
        metadata: Metadata.fromJson(json["metadata"]),
        nickname: json["nickname"],
        product: json["product"],
        tiersMode: json["tiers_mode"],
        transformUsage: json["transform_usage"],
        trialPeriodDays: json["trial_period_days"],
        usageType: json["usage_type"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "object": object,
        "active": active,
        "aggregate_usage": aggregateUsage,
        "amount": amount,
        "amount_decimal": amountDecimal,
        "billing_scheme": billingScheme,
        "created": created,
        "currency": currency,
        "interval": interval,
        "interval_count": intervalCount,
        "livemode": livemode,
        "metadata": metadata!.toJson(),
        "nickname": nickname,
        "product": product,
        "tiers_mode": tiersMode,
        "transform_usage": transformUsage,
        "trial_period_days": trialPeriodDays,
        "usage_type": usageType,
      };
}

class Price {
  Price({
    this.id,
    this.object,
    this.active,
    this.billingScheme,
    this.created,
    this.currency,
    this.livemode,
    this.lookupKey,
    this.metadata,
    this.nickname,
    this.product,
    this.recurring,
    this.taxBehavior,
    this.tiersMode,
    this.transformQuantity,
    this.type,
    this.unitAmount,
    this.unitAmountDecimal,
  });

  String? id;
  String? object;
  bool? active;
  String? billingScheme;
  int? created;
  String? currency;
  bool? livemode;
  dynamic lookupKey;
  Metadata? metadata;
  dynamic nickname;
  String? product;
  Recurring? recurring;
  String? taxBehavior;
  dynamic tiersMode;
  dynamic transformQuantity;
  String? type;
  int? unitAmount;
  String? unitAmountDecimal;

  factory Price.fromJson(Map<String, dynamic> json) => Price(
        id: json["id"],
        object: json["object"],
        active: json["active"],
        billingScheme: json["billing_scheme"],
        created: json["created"],
        currency: json["currency"],
        livemode: json["livemode"],
        lookupKey: json["lookup_key"],
        metadata: Metadata.fromJson(json["metadata"]),
        nickname: json["nickname"],
        product: json["product"],
        recurring: Recurring.fromJson(json["recurring"]),
        taxBehavior: json["tax_behavior"],
        tiersMode: json["tiers_mode"],
        transformQuantity: json["transform_quantity"],
        type: json["type"],
        unitAmount: json["unit_amount"],
        unitAmountDecimal: json["unit_amount_decimal"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "object": object,
        "active": active,
        "billing_scheme": billingScheme,
        "created": created,
        "currency": currency,
        "livemode": livemode,
        "lookup_key": lookupKey,
        "metadata": metadata!.toJson(),
        "nickname": nickname,
        "product": product,
        "recurring": recurring!.toJson(),
        "tax_behavior": taxBehavior,
        "tiers_mode": tiersMode,
        "transform_quantity": transformQuantity,
        "type": type,
        "unit_amount": unitAmount,
        "unit_amount_decimal": unitAmountDecimal,
      };
}

class Recurring {
  Recurring({
    this.aggregateUsage,
    this.interval,
    this.intervalCount,
    this.trialPeriodDays,
    this.usageType,
  });

  dynamic aggregateUsage;
  String? interval;
  int? intervalCount;
  dynamic trialPeriodDays;
  String? usageType;

  factory Recurring.fromJson(Map<String, dynamic> json) => Recurring(
        aggregateUsage: json["aggregate_usage"],
        interval: json["interval"],
        intervalCount: json["interval_count"],
        trialPeriodDays: json["trial_period_days"],
        usageType: json["usage_type"],
      );

  Map<String, dynamic> toJson() => {
        "aggregate_usage": aggregateUsage,
        "interval": interval,
        "interval_count": intervalCount,
        "trial_period_days": trialPeriodDays,
        "usage_type": usageType,
      };
}

class PaymentSettings {
  PaymentSettings({
    this.paymentMethodOptions,
    this.paymentMethodTypes,
  });

  dynamic paymentMethodOptions;
  dynamic paymentMethodTypes;

  factory PaymentSettings.fromJson(Map<String, dynamic> json) =>
      PaymentSettings(
        paymentMethodOptions: json["payment_method_options"],
        paymentMethodTypes: json["payment_method_types"],
      );

  Map<String, dynamic> toJson() => {
        "payment_method_options": paymentMethodOptions,
        "payment_method_types": paymentMethodTypes,
      };
}
