// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

EmailVerificationResponseModel welcomeFromJson(String str) =>
    EmailVerificationResponseModel.fromJson(json.decode(str));

String welcomeToJson(EmailVerificationResponseModel data) =>
    json.encode(data.toJson());

class EmailVerificationResponseModel {
  EmailVerificationResponseModel({
    this.message,
  });

  String? message;

  factory EmailVerificationResponseModel.fromJson(Map<String, dynamic> json) =>
      EmailVerificationResponseModel(
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "message": message,
      };
}
