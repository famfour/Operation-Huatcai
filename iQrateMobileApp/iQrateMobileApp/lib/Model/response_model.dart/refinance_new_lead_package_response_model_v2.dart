import 'package:iqrate/Model/response_model.dart/safe_convert.dart';

class RefinanceNewLeadPackageResponseModelV2 {
  RefinanceNewLeadPackageResponseModelV2({
    this.fixed,
    this.floating,
  });

  RefinanceNewLeadPackageResponseModelV2.fromJson(dynamic json) {
    fixed = json['fixed'] != null ? Fixed.fromJson(json['fixed']) : null;
    floating =
        json['floating'] != null ? Fixed.fromJson(json['floating']) : null;
  }
  Fixed? fixed;
  Fixed? floating;
  RefinanceNewLeadPackageResponseModelV2 copyWith({
    Fixed? fixed,
    Fixed? floating,
  }) =>
      RefinanceNewLeadPackageResponseModelV2(
        fixed: fixed ?? this.fixed,
        floating: floating ?? this.floating,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (fixed != null) {
      map['fixed'] = fixed?.toJson();
    }
    if (floating != null) {
      map['floating'] = floating?.toJson();
    }
    return map;
  }
}

class GeneratePackageResponseModelV2 {
  List<Fixed>? fixed = [];
  List<Fixed>? floating = [];

  GeneratePackageResponseModelV2({
    this.fixed,
    this.floating,
  });

  GeneratePackageResponseModelV2.fromJson(Map<String, dynamic> json)
      : fixed = SafeManager.parseList(json, 'fixed')
            .map((e) => Fixed.fromJson(e))
            .toList(),
        floating = SafeManager.parseList(json, 'floating')
            .map((e) => Fixed.fromJson(e))
            .toList();

  Map<String, dynamic> toJson() => {
        'fixed': fixed?.map((e) => e.toJson()).toList(),
        'floating': floating?.map((e) => e.toJson()).toList(),
      };
}

class Fixed {
  Fixed({
    this.id,
    this.bank,
    this.mask,
    this.rateCategory,
    this.rateType,
    this.propertyTypes,
    this.propertyStatus,
    this.loanCategory,
    this.lockInPeriod,
    this.minLoanAmount,
    this.depositToPlace,
    this.valuationSubsidy,
    this.fireInsuranceSubsidy,
    this.cashRebateLegalSubsidy,
    this.cashRebateSubsidyClawbackPeriodYears,
    this.partialRepaymentPenalty,
    this.partialRepaymentPenaltyRemarks,
    this.fullRepaymentPenalty,
    this.fullRepaymentPenaltyRemarks,
    this.rates,
    this.cancellationFee,
    this.cancellationFeeRemarks,
    this.depositToPlaceRemarks,
    this.remarksForClient,
    this.interestOffsetting,
    this.interestResetDate,
    this.processingFee,
    this.remarksForBroker,
    this.newPurchaseReferralFee,
    this.refinanceReferralFee,
    this.isPublished,
    this.createdBy,
    this.updatedBy,
    this.created,
    this.updated,
    this.isChecked = false,
  });

  Fixed.fromJson(dynamic json) {
    id = json['id'];
    bank = json['bank'] != null ? BankFixed.fromJson(json['bank']) : null;
    mask = json['mask'];
    rateCategory = json['rate_category'];
    rateType = json['rate_type'];
    propertyTypes = json['property_types'] != null
        ? json['property_types'].cast<String>()
        : [];
    propertyStatus = json['property_status'] != null
        ? json['property_status'].cast<String>()
        : [];
    loanCategory = json['loan_category'] != null
        ? json['loan_category'].cast<String>()
        : [];
    lockInPeriod = json['lock_in_period'];
    minLoanAmount = json['min_loan_amount'];
    depositToPlace = json['deposit_to_place'];
    valuationSubsidy = json['valuation_subsidy'] != null
        ? json['valuation_subsidy'].cast<String>()
        : [];
    fireInsuranceSubsidy = json['fire_insurance_subsidy'];
    cashRebateLegalSubsidy = json['cash_rebate_legal_subsidy'] != null
        ? json['cash_rebate_legal_subsidy'].cast<String>()
        : [];
    cashRebateSubsidyClawbackPeriodYears =
        json['cash_rebate_subsidy_clawback_period_years'];
    partialRepaymentPenalty = json['partial_repayment_penalty'];
    partialRepaymentPenaltyRemarks = json['partial_repayment_penalty_remarks'];
    fullRepaymentPenalty = json['full_repayment_penalty'];
    fullRepaymentPenaltyRemarks = json['full_repayment_penalty_remarks'];
    if (json['rates'] != null) {
      rates = [];
      json['rates'].forEach((v) {
        rates?.add(RatesFixed.fromJson(v));
      });
    }
    cancellationFee = json['cancellation_fee'];
    cancellationFeeRemarks = json['cancellation_fee_remarks'] ?? "NA";
    depositToPlaceRemarks = json['deposit_to_place_remarks'];
    remarksForClient = json['remarks_for_client'] != null
        ? json['remarks_for_client'].cast<String>()
        : [];
    interestOffsetting = json['interest_offsetting'];
    interestResetDate = json['interest_reset_date'];
    processingFee = json['processing_fee'];
    remarksForBroker = json['remarks_for_broker'];
    newPurchaseReferralFee = json['new_purchase_referral_fee'];
    refinanceReferralFee = json['refinance_referral_fee'];
    isPublished = json['publish'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    created = json['created'];
    updated = json['updated'];
  }
  int? id;
  BankFixed? bank;
  bool? mask;
  String? rateCategory;
  String? rateType;
  List<String>? propertyTypes;
  List<String>? propertyStatus;
  List<String>? loanCategory;
  int? lockInPeriod;
  double? minLoanAmount;
  double? depositToPlace;
  List<String>? valuationSubsidy;
  String? fireInsuranceSubsidy;
  List<String>? cashRebateLegalSubsidy;
  int? cashRebateSubsidyClawbackPeriodYears;
  double? partialRepaymentPenalty;
  String? partialRepaymentPenaltyRemarks;
  double? fullRepaymentPenalty;
  String? fullRepaymentPenaltyRemarks;
  List<RatesFixed>? rates;
  String? cancellationFee;
  String? cancellationFeeRemarks;
  String? depositToPlaceRemarks;
  List<String>? remarksForClient;
  String? interestOffsetting;
  dynamic interestResetDate;
  dynamic processingFee;
  dynamic remarksForBroker;
  double? newPurchaseReferralFee;
  double? refinanceReferralFee;
  bool? isPublished;
  dynamic createdBy;
  dynamic updatedBy;
  String? created;
  String? updated;
  bool isChecked = false;
  Fixed copyWith({
    int? id,
    BankFixed? bank,
    bool? mask,
    String? rateCategory,
    String? rateType,
    List<String>? propertyTypes,
    List<String>? propertyStatus,
    List<String>? loanCategory,
    int? lockInPeriod,
    double? minLoanAmount,
    double? depositToPlace,
    List<String>? valuationSubsidy,
    String? fireInsuranceSubsidy,
    List<String>? cashRebateLegalSubsidy,
    int? cashRebateSubsidyClawbackPeriodYears,
    double? partialRepaymentPenalty,
    String? partialRepaymentPenaltyRemarks,
    double? fullRepaymentPenalty,
    String? fullRepaymentPenaltyRemarks,
    List<RatesFixed>? rates,
    String? cancellationFee,
    String? depositToPlaceRemarks,
    List<String>? remarksForClient,
    String? interestOffsetting,
    bool? interestResetDate,
    bool? processingFee,
    dynamic remarksForBroker,
    double? newPurchaseReferralFee,
    double? refinanceReferralFee,
    bool? isPublished,
    dynamic createdBy,
    dynamic updatedBy,
    String? created,
    String? updated,
  }) =>
      Fixed(
        id: id ?? this.id,
        bank: bank ?? this.bank,
        mask: mask ?? this.mask,
        rateCategory: rateCategory ?? this.rateCategory,
        rateType: rateType ?? this.rateType,
        propertyTypes: propertyTypes ?? this.propertyTypes,
        propertyStatus: propertyStatus ?? this.propertyStatus,
        loanCategory: loanCategory ?? this.loanCategory,
        lockInPeriod: lockInPeriod ?? this.lockInPeriod,
        minLoanAmount: minLoanAmount ?? this.minLoanAmount,
        depositToPlace: depositToPlace ?? this.depositToPlace,
        valuationSubsidy: valuationSubsidy ?? this.valuationSubsidy,
        fireInsuranceSubsidy: fireInsuranceSubsidy ?? this.fireInsuranceSubsidy,
        cashRebateLegalSubsidy:
            cashRebateLegalSubsidy ?? this.cashRebateLegalSubsidy,
        cashRebateSubsidyClawbackPeriodYears:
            cashRebateSubsidyClawbackPeriodYears ??
                this.cashRebateSubsidyClawbackPeriodYears,
        partialRepaymentPenalty:
            partialRepaymentPenalty ?? this.partialRepaymentPenalty,
        partialRepaymentPenaltyRemarks: partialRepaymentPenaltyRemarks ??
            this.partialRepaymentPenaltyRemarks,
        fullRepaymentPenalty: fullRepaymentPenalty ?? this.fullRepaymentPenalty,
        fullRepaymentPenaltyRemarks:
            fullRepaymentPenaltyRemarks ?? this.fullRepaymentPenaltyRemarks,
        rates: rates ?? this.rates,
        cancellationFee: cancellationFee ?? this.cancellationFee,
        cancellationFeeRemarks:
            cancellationFeeRemarks ?? cancellationFeeRemarks,
        depositToPlaceRemarks:
            depositToPlaceRemarks ?? this.depositToPlaceRemarks,
        remarksForClient: remarksForClient ?? this.remarksForClient,
        interestOffsetting: interestOffsetting ?? this.interestOffsetting,
        interestResetDate: interestResetDate ?? this.interestResetDate,
        processingFee: processingFee ?? this.processingFee,
        remarksForBroker: remarksForBroker ?? this.remarksForBroker,
        newPurchaseReferralFee:
            newPurchaseReferralFee ?? this.newPurchaseReferralFee,
        refinanceReferralFee: refinanceReferralFee ?? this.refinanceReferralFee,
        isPublished: isPublished ?? this.isPublished,
        createdBy: createdBy ?? this.createdBy,
        updatedBy: updatedBy ?? this.updatedBy,
        created: created ?? this.created,
        updated: updated ?? this.updated,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    if (bank != null) {
      map['bank'] = bank?.toJson();
    }
    map['mask'] = mask;
    map['rate_category'] = rateCategory;
    map['rate_type'] = rateType;
    map['property_types'] = propertyTypes;
    map['property_status'] = propertyStatus;
    map['loan_category'] = loanCategory;
    map['lock_in_period'] = lockInPeriod;
    map['min_loan_amount'] = minLoanAmount;
    map['deposit_to_place'] = depositToPlace;
    map['valuation_subsidy'] = valuationSubsidy;
    map['fire_insurance_subsidy'] = fireInsuranceSubsidy;
    map['cash_rebate_legal_subsidy'] = cashRebateLegalSubsidy;
    map['cash_rebate_subsidy_clawback_period_years'] =
        cashRebateSubsidyClawbackPeriodYears;
    map['partial_repayment_penalty'] = partialRepaymentPenalty;
    map['partial_repayment_penalty_remarks'] = partialRepaymentPenaltyRemarks;
    map['full_repayment_penalty'] = fullRepaymentPenalty;
    map['full_repayment_penalty_remarks'] = fullRepaymentPenaltyRemarks;
    if (rates != null) {
      map['rates'] = rates?.map((v) => v.toJson()).toList();
    }
    map['cancellation_fee'] = cancellationFee;
    map['deposit_to_place_remarks'] = depositToPlaceRemarks;
    map['remarks_for_client'] = remarksForClient;
    map['interest_offsetting'] = interestOffsetting;
    map['interest_reset_date'] = interestResetDate;
    map['processing_fee'] = processingFee;
    map['remarks_for_broker'] = remarksForBroker;
    map['new_purchase_referral_fee'] = newPurchaseReferralFee;
    map['refinance_referral_fee'] = refinanceReferralFee;
    map['publish'] = isPublished;
    map['created_by'] = createdBy;
    map['updated_by'] = updatedBy;
    map['created'] = created;
    map['updated'] = updated;
    return map;
  }
}

class RatesFixed {
  RatesFixed({
    this.year,
    this.referenceRate,
    this.bankSpread,
    this.totalInterestRate,
  });

  RatesFixed.fromJson(dynamic json) {
    year = json['year'];
    referenceRate = json['reference_rate'] != null
        ? ReferenceRateFixd.fromJson(json['reference_rate'])
        : null;
    bankSpread = json['bank_spread'];
    totalInterestRate = json['total_interest_rate'];
  }
  String? year;
  ReferenceRateFixd? referenceRate;
  double? bankSpread;
  double? interestRate;
  double? totalInterestRate;
  RatesFixed copyWith({
    String? year,
    ReferenceRateFixd? referenceRate,
    double? bankSpread,
    double? interestRate,
    double? totalInterestRate,
  }) =>
      RatesFixed(
        year: year ?? this.year,
        referenceRate: referenceRate ?? this.referenceRate,
        bankSpread: bankSpread ?? this.bankSpread,
        totalInterestRate: totalInterestRate ?? this.totalInterestRate,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['year'] = year;
    if (referenceRate != null) {
      map['reference_rate'] = referenceRate?.toJson();
    }
    map['bank_spread'] = bankSpread;
    map['total_interest_rate'] = totalInterestRate;
    map['interest_rate'] = interestRate;
    return map;
  }
}

class ReferenceRateFixd {
  ReferenceRateFixd({
    this.rateType,
    this.reference,
    this.interestRate,
    this.equation,
    this.remarks,
  });

  ReferenceRateFixd.fromJson(dynamic json) {
    rateType = json['rate_type'];
    reference = json['reference'];
    interestRate = json['interest_rate'];
    equation = json['equation'];
    remarks = json['remarks'];
  }
  String? rateType;
  String? reference;
  double? interestRate;
  String? equation;
  dynamic remarks;
  ReferenceRateFixd copyWith({
    String? rateType,
    String? reference,
    double? interestRate,
    String? equation,
    dynamic remarks,
  }) =>
      ReferenceRateFixd(
        rateType: rateType ?? this.rateType,
        reference: reference ?? this.reference,
        interestRate: interestRate ?? this.interestRate,
        equation: equation ?? this.equation,
        remarks: remarks ?? this.remarks,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['rate_type'] = rateType;
    map['reference'] = reference;
    map['interest_rate'] = interestRate;
    map['equation'] = equation;
    map['remarks'] = remarks;
    return map;
  }
}

class BankFixed {
  BankFixed({
    this.id,
    this.name,
    this.logo,
    this.bankForm,
  });

  BankFixed.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    logo = json['logo'];
    bankForm = json['bank_form'];
  }
  int? id;
  String? name;
  String? logo;
  String? bankForm;
  BankFixed copyWith({
    int? id,
    String? name,
    String? logo,
    String? bankForm,
  }) =>
      BankFixed(
        id: id ?? this.id,
        name: name ?? this.name,
        logo: logo ?? this.logo,
        bankForm: bankForm ?? this.bankForm,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    map['logo'] = logo;
    map['bank_form'] = bankForm;
    return map;
  }
}
