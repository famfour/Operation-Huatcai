// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

// ignore_for_file: non_constant_identifier_names

import 'dart:convert';

FetchLeadListResponseModel welcomeFromJson(String str) =>
    FetchLeadListResponseModel.fromJson(json.decode(str));

String welcomeToJson(FetchLeadListResponseModel data) =>
    json.encode(data.toJson());

class FetchLeadListResponseModel {
  FetchLeadListResponseModel({
    this.count,
    this.next,
    this.previous,
    this.results,
  });

  int? count;
  dynamic next;
  dynamic previous;
  List<Result>? results;

  factory FetchLeadListResponseModel.fromJson(Map<String, dynamic> json) =>
      FetchLeadListResponseModel(
        count: json["count"],
        next: json["next"],
        previous: json["previous"],
        results:
            List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "count": count,
        "next": next,
        "previous": previous,
        "results": List<dynamic>.from(results!.map((x) => x.toJson())),
      };
}

class Result {
  Result({
    this.id,
    this.leadType,
    this.leadStatus,
    this.agentApplied,
    this.coBrokeAgent,
    this.sellWithinThreeYears,
    this.has200KDown,
    this.loanCategory,
    this.loanAmount,
    this.outstanding_loan_amount,
    this.existingBank,
    this.currentInterestRate,
    this.clients,
    this.packages,
    this.wonLead,
    this.property,
    this.canEdit,
    this.createdBy,
    this.updatedBy,
    this.created,
    this.updated,
    this.document_drawer_id,
    this.document_drawer_url,
    this.coBrokeStatus,
  });
  String? coBrokeStatus;
  int? id;
  String? document_drawer_id;
  String? document_drawer_url;
  String? leadType;
  String? leadStatus;
  String? agentApplied;
  CoBrokeAgent? coBrokeAgent;
  bool? sellWithinThreeYears;
  bool? has200KDown;
  dynamic loanCategory;
  dynamic loanAmount;
  dynamic outstanding_loan_amount;
  dynamic existingBank;
  dynamic currentInterestRate;
  List<Client>? clients;
  List<dynamic>? packages;
  dynamic wonLead;
  dynamic property;
  bool? canEdit;
  dynamic createdBy;
  dynamic updatedBy;
  DateTime? created;
  DateTime? updated;
  bool isExpanded = false;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        coBrokeStatus: json["co_broke_status"],
        document_drawer_id: json["document_drawer_id"],
        document_drawer_url: json["document_drawer_url"],
        leadType: json["lead_type"],
        leadStatus: json["lead_status"],
        agentApplied: json["agent_applied"],
        coBrokeAgent: CoBrokeAgent.fromJson(json["co_broke_agent"]),
        sellWithinThreeYears: json["sell_within_three_years"],
        has200KDown: json["has_200k_down"],
        loanCategory: json["loan_category"],
        loanAmount: json["loan_amount"],
        outstanding_loan_amount: json["outstanding_loan_amount"],
        existingBank: json["existing_bank"],
        currentInterestRate: json["current_interest_rate"],
        clients:
            List<Client>.from(json["clients"].map((x) => Client.fromJson(x))),
        packages: List<dynamic>.from(json["packages"].map((x) => x)),
        wonLead: json["won_lead"],
        property: json["property"],
        canEdit: json["can_edit"],
        createdBy: json["created_by"],
        updatedBy: json["updated_by"],
        created: DateTime.parse(json["created"]),
        updated: DateTime.parse(json["updated"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "lead_type": leadType,
        "lead_status": leadStatus,
        "agent_applied": agentApplied,
        "co_broke_agent": coBrokeAgent,
        "sell_within_three_years": sellWithinThreeYears,
        "has_200k_down": has200KDown,
        "loan_category": loanCategory,
        "loan_amount": loanAmount,
        "existing_bank": existingBank,
        "current_interest_rate": currentInterestRate,
        "clients": List<dynamic>.from(clients!.map((x) => x.toJson())),
        "packages": List<dynamic>.from(packages!.map((x) => x)),
        "won_lead": wonLead,
        "property": property,
        "can_edit": canEdit,
        "created_by": createdBy,
        "updated_by": updatedBy,
        "created": created!.toIso8601String(),
        "updated": updated!.toIso8601String(),
        "co_broke_status": coBrokeStatus,
      };
}

class Client {
  Client({
    this.id,
    this.lead,
    this.name,
    this.countryCode,
    this.phoneNumber,
    this.mainApplicant,
    this.pdpaStatus,
    this.email,
    this.annualIncome,
    this.dob,
    this.nationality,
    this.streetName,
    this.blockNumber,
    this.unitNumber,
    this.projectName,
    this.postalCode,
    this.createdBy,
    this.updatedBy,
    this.created,
    this.updated,
  });

  int? id;
  int? lead;
  String? name;
  int? countryCode;
  int? phoneNumber;
  bool? mainApplicant;
  bool? pdpaStatus;
  String? email;
  dynamic annualIncome;
  dynamic dob;
  String? nationality;
  dynamic streetName;
  dynamic blockNumber;
  dynamic unitNumber;
  dynamic projectName;
  dynamic postalCode;
  dynamic createdBy;
  dynamic updatedBy;
  DateTime? created;
  DateTime? updated;

  factory Client.fromJson(Map<String, dynamic> json) => Client(
        id: json["id"],
        lead: json["lead"],
        name: json["name"],
        countryCode: json["country_code"],
        phoneNumber: json["phone_number"],
        mainApplicant: json["main_applicant"],
        pdpaStatus: json["pdpa_status"],
        email: json["email"],
        annualIncome: json["annual_income"],
        dob: json["dob"],
        nationality: json["nationality"],
        streetName: json["street_name"],
        blockNumber: json["block_number"],
        unitNumber: json["unit_number"],
        projectName: json["project_name"],
        postalCode: json["postal_code"],
        createdBy: json["created_by"],
        updatedBy: json["updated_by"],
        created: DateTime.parse(json["created"]),
        updated: DateTime.parse(json["updated"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "lead": lead,
        "name": name,
        "country_code": countryCode,
        "phone_number": phoneNumber,
        "main_applicant": mainApplicant,
        "pdpa_status": pdpaStatus,
        "email": email,
        "annual_income": annualIncome,
        "dob": dob,
        "nationality": nationality,
        "street_name": streetName,
        "block_number": blockNumber,
        "unit_number": unitNumber,
        "project_name": projectName,
        "postal_code": postalCode,
        "created_by": createdBy,
        "updated_by": updatedBy,
        "created": created!.toIso8601String(),
        "updated": updated!.toIso8601String(),
      };
}

class CoBrokeAgent {
  CoBrokeAgent({this.name, this.phoneNumber});

  String? name;
  String? phoneNumber;

  factory CoBrokeAgent.fromJson(Map<String, dynamic> json) => CoBrokeAgent(
        name: json["name"],
        phoneNumber: json["phone_number"],
      );
}
