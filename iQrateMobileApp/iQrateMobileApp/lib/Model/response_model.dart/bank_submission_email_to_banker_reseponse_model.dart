class BankSubmissionEmailToBankerResponseModel {
  String? banker;
  String? email;
  String? subject;
  String? content;
  List? attachments;

  BankSubmissionEmailToBankerResponseModel(
      {this.banker, this.email, this.subject, this.content, this.attachments});

  factory BankSubmissionEmailToBankerResponseModel.fromJson(json) =>
      BankSubmissionEmailToBankerResponseModel(
          banker: json["banker"],
          email: json["email"],
          subject: json["subject"],
          content: json["content"],
          attachments: json["attachments"]);
}
