// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

List<GetInvoicesResponseModel> welcomeFromJson(String str) =>
    List<GetInvoicesResponseModel>.from(
        json.decode(str).map((x) => GetInvoicesResponseModel.fromJson(x)));

String welcomeToJson(List<GetInvoicesResponseModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class GetInvoicesResponseModel {
  GetInvoicesResponseModel({
    this.id,
    this.amountPaid,
    this.amountRemaining,
    this.collectionMethod,
    this.invoicePdf,
    this.number,
    this.periodStart,
    this.periodEnd,
    this.paid,
    this.paidAt,
    this.status,
    this.total,
  });

  String? id;
  int? amountPaid;
  int? amountRemaining;
  String? collectionMethod;
  String? invoicePdf;
  String? number;
  int? periodStart;
  int? periodEnd;
  bool? paid;
  int? paidAt;
  String? status;
  int? total;

  factory GetInvoicesResponseModel.fromJson(Map<String, dynamic> json) =>
      GetInvoicesResponseModel(
        id: json["id"],
        amountPaid: json["amount_paid"],
        amountRemaining: json["amount_remaining"],
        collectionMethod: json["collection_method"],
        invoicePdf: json["invoice_pdf"],
        number: json["number"],
        periodStart: json["period_start"],
        periodEnd: json["period_end"],
        paid: json["paid"],
        paidAt: json["paid_at"],
        status: json["status"],
        total: json["total"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "amount_paid": amountPaid,
        "amount_remaining": amountRemaining,
        "collection_method": collectionMethod,
        "invoice_pdf": invoicePdf,
        "number": number,
        "period_start": periodStart,
        "period_end": periodEnd,
        "paid": paid,
        "paid_at": paidAt,
        "status": status,
        "total": total,
      };
}
