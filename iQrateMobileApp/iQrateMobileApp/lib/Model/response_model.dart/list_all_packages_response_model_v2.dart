import 'safe_convert.dart';

class ListAllPackagesResponseModel {
  int? count = 0;
  String? next = "";
  String? previous = "";
  List<PackageModel>? results = [];

  ListAllPackagesResponseModel({
    this.count,
    this.next,
    this.previous,
    this.results,
  });

  getCountItemSelected() {
    var a = 0;
    for (var i = 0; i < results!.length; i++) {
      var count = results![i].isChecked ?? false ? 1 : 0;
      a += count;
    }
    return a;
  }

  ListAllPackagesResponseModel.fromJson(Map<String, dynamic> json)
      : count = SafeManager.parseInt(json, 'count'),
        next = SafeManager.parseString(json, 'next'),
        previous = json['previous'],
        results = SafeManager.parseList(json, 'results')
            .map((e) => PackageModel.fromJson(e))
            .toList();

  Map<String, dynamic> toJson() => {
        'count': count,
        'next': next,
        'previous': previous,
        'results': results?.map((e) => e.toJson()).toList(),
      };
}

class PackageModel {
  int? id = 0;
  Bank? bank;
  bool? mask = false;
  //isChecked
  bool? isChecked = false;

  String? rateCategory = "";
  String? rateType = "";
  List<String>? propertyTypes = [];
  List<String>? propertyStatus = [];
  List<String>? loanCategory = [];
  int? lockInPeriod = 0;
  double? minLoanAmount = 0.0;
  double? depositToPlace = 0.0;
  List<String>? valuationSubsidy = [];
  String? fireInsuranceSubsidy = "";
  List<String>? cashRebateLegalSubsidy = [];
  int? cashRebateSubsidyClawbackPeriodYears = 0;
  double? partialRepaymentPenalty = 0.0;
  String? partialRepaymentPenaltyRemarks = "";
  double? fullRepaymentPenalty = 0.0;
  String? fullRepaymentPenaltyRemarks = "";
  List<Rates>? rates = [];
  String? cancellationFee = "";
  String? cancellationFeeRemarks = "";
  String? depositToPlaceRemarks = "";
  List<String>? remarksForClient = [];
  String? interestOffsetting = "";
  bool? interestResetDate = false;
  bool? processingFee = false;
  String? remarksForBroker = "";
  double? newPurchaseReferralFee = 0.0;
  double? refinanceReferralFee = 0.0;
  String? createdBy = "";
  String? updatedBy = "";
  String? created = "";
  String? updated = "";

  getremarksForClientString(){
    var a = remarksForClient;
    var b = a.toString();// [a, b, c]
    var c = b.replaceAll("[", "");
    var d = c.replaceAll("]", "");
    return d;

  }

  PackageModel({
    this.id,
    this.bank,
    this.mask,
    this.isChecked,
    this.rateCategory,
    this.rateType,
    this.propertyTypes,
    this.propertyStatus,
    this.loanCategory,
    this.lockInPeriod,
    this.minLoanAmount,
    this.depositToPlace,
    this.valuationSubsidy,
    this.fireInsuranceSubsidy,
    this.cashRebateLegalSubsidy,
    this.cashRebateSubsidyClawbackPeriodYears,
    this.partialRepaymentPenalty,
    this.partialRepaymentPenaltyRemarks,
    this.fullRepaymentPenalty,
    this.fullRepaymentPenaltyRemarks,
    this.rates,
    this.cancellationFee,
    this.cancellationFeeRemarks,
    this.depositToPlaceRemarks,
    this.remarksForClient,
    this.interestOffsetting,
    this.interestResetDate,
    this.processingFee,
    this.remarksForBroker,
    this.newPurchaseReferralFee,
    this.refinanceReferralFee,
    this.createdBy,
    this.updatedBy,
    this.created,
    this.updated,
  });

  PackageModel.fromJson(Map<String, dynamic> json)
      : id = SafeManager.parseInt(json, 'id'),
        bank = Bank.fromJson(
          SafeManager.parseObject(json, 'bank'),
        ),
        mask = SafeManager.parseBoolean(json, 'mask'),
        //isChecked
        isChecked = SafeManager.parseBoolean(json, 'isChecked'),
        rateCategory = SafeManager.parseString(json, 'rate_category'),
        rateType = SafeManager.parseString(json, 'rate_type'),
        propertyTypes = List<String>.from(json["property_types"]),
        propertyStatus = List<String>.from(json["property_status"]),
        loanCategory = List<String>.from(json["loan_category"]),
        lockInPeriod = SafeManager.parseInt(json, 'lock_in_period'),
        minLoanAmount = SafeManager.parseDouble(json, 'min_loan_amount'),
        depositToPlace = SafeManager.parseDouble(json, 'deposit_to_place'),
        valuationSubsidy = List<String>.from(json["valuation_subsidy"]),
        fireInsuranceSubsidy =
            SafeManager.parseString(json, 'fire_insurance_subsidy'),
        cashRebateLegalSubsidy =
            List<String>.from(json["cash_rebate_legal_subsidy"]),
        cashRebateSubsidyClawbackPeriodYears = SafeManager.parseInt(
            json, 'cash_rebate_subsidy_clawback_period_years'),
        partialRepaymentPenalty =
            SafeManager.parseDouble(json, 'partial_repayment_penalty'),
        partialRepaymentPenaltyRemarks =
            SafeManager.parseString(json, 'partial_repayment_penalty_remarks'),
        fullRepaymentPenalty =
            SafeManager.parseDouble(json, 'full_repayment_penalty'),
        fullRepaymentPenaltyRemarks =
            SafeManager.parseString(json, 'full_repayment_penalty_remarks'),
        rates = SafeManager.parseList(json, 'rates')
            .map((e) => Rates.fromJson(e))
            .toList(),
        cancellationFee = SafeManager.parseString(json, 'cancellation_fee'),
        cancellationFeeRemarks = SafeManager.parseString(json, 'cancellation_fee_remarks'),
        depositToPlaceRemarks =
            SafeManager.parseString(json, 'deposit_to_place_remarks'),
        remarksForClient = List<String>.from(json["remarks_for_client"]),
        interestOffsetting =
            SafeManager.parseString(json, 'interest_offsetting'),
        interestResetDate =
            SafeManager.parseBoolean(json, 'interest_reset_date'),
        processingFee = SafeManager.parseBoolean(json, 'processing_fee'),
        remarksForBroker = SafeManager.parseString(json, 'remarks_for_broker'),
        newPurchaseReferralFee =
            SafeManager.parseDouble(json, 'new_purchase_referral_fee'),
        refinanceReferralFee =
            SafeManager.parseDouble(json, 'refinance_referral_fee'),
        createdBy = SafeManager.parseString(json, 'created_by'),
        updatedBy = SafeManager.parseString(json, 'updated_by'),
        created = SafeManager.parseString(json, 'created'),
        updated = SafeManager.parseString(json, 'updated');

  Map<String, dynamic> toJson() => {
        'id': id,
        'bank': bank?.toJson(),
        'mask': mask,
        'isChecked': isChecked,
        'rate_category': rateCategory,
        'rate_type': rateType,
        'property_types': propertyTypes,
        'property_status': propertyStatus,
        'loan_category': loanCategory,
        'lock_in_period': lockInPeriod,
        'min_loan_amount': minLoanAmount,
        'deposit_to_place': depositToPlace,
        'valuation_subsidy': valuationSubsidy,
        'fire_insurance_subsidy': fireInsuranceSubsidy,
        'cash_rebate_legal_subsidy': cashRebateLegalSubsidy,
        'cash_rebate_subsidy_clawback_period_years':
            cashRebateSubsidyClawbackPeriodYears,
        'partial_repayment_penalty': partialRepaymentPenalty,
        'partial_repayment_penalty_remarks': partialRepaymentPenaltyRemarks,
        'full_repayment_penalty': fullRepaymentPenalty,
        'full_repayment_penalty_remarks': fullRepaymentPenaltyRemarks,
        'rates': rates?.map((e) => e.toJson()).toList(),
        'cancellation_fee': cancellationFee,
        'cancellation_fee_remarks': cancellationFeeRemarks,
        'deposit_to_place_remarks': depositToPlaceRemarks,
        'remarks_for_client': remarksForClient,
        'interest_offsetting': interestOffsetting,
        'interest_reset_date': interestResetDate,
        'processing_fee': processingFee,
        'remarks_for_broker': remarksForBroker,
        'new_purchase_referral_fee': newPurchaseReferralFee,
        'refinance_referral_fee': refinanceReferralFee,
        'created_by': createdBy,
        'updated_by': updatedBy,
        'created': created,
        'updated': updated,
      };
}

class Rates {
  String? year = "";
  ReferenceRate? referenceRate;
  double? bankSpread = 0.0;
  double? totalInterestRate = 0.0;

  Rates({
    this.year,
    this.referenceRate,
    this.bankSpread,
    this.totalInterestRate,
  });

  Rates.fromJson(Map<String, dynamic> json)
      : year = SafeManager.parseString(json, 'year'),
        referenceRate = ReferenceRate.fromJson(
          SafeManager.parseObject(json, 'reference_rate'),
        ),
        bankSpread = SafeManager.parseDouble(json, 'bank_spread'),
        totalInterestRate =
            SafeManager.parseDouble(json, 'total_interest_rate');

  Map<String, dynamic> toJson() => {
        'year': year,
        'reference_rate': referenceRate?.toJson(),
        'bank_spread': bankSpread,
        'total_interest_rate': totalInterestRate,
      };
}

class ReferenceRate {
  String? rateType = "";
  String? reference = "";
  double? interestRate = 0.0;
  String? equation = "";
  String? remarks = "";

  ReferenceRate({
    this.rateType,
    this.reference,
    this.interestRate,
    this.equation,
    this.remarks,
  });

  ReferenceRate.fromJson(Map<String, dynamic> json)
      : rateType = SafeManager.parseString(json, 'rate_type'),
        reference = SafeManager.parseString(json, 'reference'),
        interestRate = SafeManager.parseDouble(json, 'interest_rate'),
        equation = SafeManager.parseString(json, 'equation'),
        remarks = SafeManager.parseString(json, 'remarks');

  Map<String, dynamic> toJson() => {
        'rate_type': rateType,
        'reference': reference,
        'interest_rate': interestRate,
        'equation': equation,
        'remarks': remarks,
      };
}

class Bank {
  int? id = 0;
  String? name = "";
  String? logo = "";
  String? bankForm = "";

  Bank({
    this.id,
    this.name,
    this.logo,
    this.bankForm,
  });

  Bank.fromJson(Map<String, dynamic> json)
      : id = SafeManager.parseInt(json, 'id'),
        name = SafeManager.parseString(json, 'name'),
        logo = SafeManager.parseString(json, 'logo'),
        bankForm = SafeManager.parseString(json, 'bank_form');

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'logo': logo,
        'bank_form': bankForm,
      };
}
