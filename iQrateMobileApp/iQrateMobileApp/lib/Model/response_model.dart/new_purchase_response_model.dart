// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

NewPurchaseResponseModel welcomeFromJson(String str) =>
    NewPurchaseResponseModel.fromJson(json.decode(str));

String welcomeToJson(NewPurchaseResponseModel data) =>
    json.encode(data.toJson());

class NewPurchaseResponseModel {
  NewPurchaseResponseModel({
    this.monthlyFixedIncome,
    this.annualIncome,
    this.monthlyRentalIncome,
    this.totalHouseInstallment,
    this.totalCarInstallment,
    this.totalPersonalInstallment,
    this.totalCreditInstallment,
    this.totalGurantorInstallment,
    this.totalPropertyInstallment,
    this.totalPropertyCompanyInstallment,
    this.noOfHousingLoan,
    this.noOfOwnProperties,
    this.loanToValue,
    this.maximumLoanTenure,
    this.maximumTenureBasedAffordability,
    this.preferedTenureBasedAffordability,
    this.buyerStampDuty,
    this.additionalBuyerStampDuty,
    this.legalFees,
    this.valuationFees,
  });

  int? monthlyFixedIncome;
  int? annualIncome;
  int? monthlyRentalIncome;
  int? totalHouseInstallment;
  int? totalCarInstallment;
  int? totalPersonalInstallment;
  int? totalCreditInstallment;
  int? totalGurantorInstallment;
  int? totalPropertyInstallment;
  int? totalPropertyCompanyInstallment;
  int? noOfHousingLoan;
  int? noOfOwnProperties;
  String? loanToValue;
  String? maximumLoanTenure;
  TenureBasedAffordability? maximumTenureBasedAffordability;
  TenureBasedAffordability? preferedTenureBasedAffordability;
  String? buyerStampDuty;
  String? additionalBuyerStampDuty;
  String? legalFees;
  String? valuationFees;

  factory NewPurchaseResponseModel.fromJson(Map<String, dynamic> json) {
    return NewPurchaseResponseModel(
      monthlyFixedIncome: json["monthly_fixed_income"],
      annualIncome: json["annual_income"],
      monthlyRentalIncome: json["monthly_rental_income"],
      totalHouseInstallment: json["total_house_installment"],
      totalCarInstallment: json["total_car_installment"],
      totalPersonalInstallment: json["total_personal_installment"],
      totalCreditInstallment: json["total_credit_installment"],
      totalGurantorInstallment: json["total_gurantor_installment"],
      totalPropertyInstallment: json["total_property_installment"],
      totalPropertyCompanyInstallment:
          json["total_property_company_installment"],
      noOfHousingLoan: json["no_of_housing_loan"],
      noOfOwnProperties: json["no_of_own_properties"],
      loanToValue: json["Loan To Value"],
      maximumLoanTenure: json["Maximum Loan Tenure"],
      maximumTenureBasedAffordability: TenureBasedAffordability.fromJson(
          json["Maximum Tenure based Affordability"]),
      preferedTenureBasedAffordability:
          json["Prefered Tenure based Affordability"].toString() ==
                  [].toString()
              ? null
              : TenureBasedAffordability.fromJson(
                  json["Prefered Tenure based Affordability"]),
      buyerStampDuty: json["Buyer Stamp Duty"],
      additionalBuyerStampDuty: json["Additional Buyer Stamp Duty"],
      legalFees: json["Legal Fees"],
      valuationFees: json["Valuation Fees"],
    );
  }

  Map<String, dynamic> toJson() => {
        "monthly_fixed_income": monthlyFixedIncome,
        "annual_income": annualIncome,
        "monthly_rental_income": monthlyRentalIncome,
        "total_house_installment": totalHouseInstallment,
        "total_car_installment": totalCarInstallment,
        "total_personal_installment": totalPersonalInstallment,
        "total_credit_installment": totalCreditInstallment,
        "total_gurantor_installment": totalGurantorInstallment,
        "total_property_installment": totalPropertyInstallment,
        "total_property_company_installment": totalPropertyCompanyInstallment,
        "no_of_housing_loan": noOfHousingLoan,
        "no_of_own_properties": noOfOwnProperties,
        "Loan To Value": loanToValue,
        "Maximum Loan Tenure": maximumLoanTenure,
        "Maximum Tenure based Affordability":
            maximumTenureBasedAffordability!.toJson(),
        "Prefered Tenure based Affordability":
            preferedTenureBasedAffordability!.toJson(),
        "Buyer Stamp Duty": buyerStampDuty,
        "Additional Buyer Stamp Duty": additionalBuyerStampDuty,
        "Legal Fees": legalFees,
        "Valuation Fees": valuationFees,
      };
}

class TenureBasedAffordability {
  TenureBasedAffordability(
      {this.maximumQualifiedLoan,
      this.maximumPropertyPrice,
      this.msr,
      this.msrStatus,
      this.tdsr,
      this.tdsrStatus,
      this.fullFludgeAmount,
      this.fullUnFludgeAmount,
      this.loanTenure,
      this.monthlyInstallment});

  String? maximumQualifiedLoan;
  String? maximumPropertyPrice;
  dynamic msr;
  dynamic tdsr;
  String? msrStatus;
  String? tdsrStatus;
  String? fullFludgeAmount;
  String? fullUnFludgeAmount;
  int? loanTenure;
  String? monthlyInstallment;

  factory TenureBasedAffordability.fromJson(Map<String, dynamic> json) =>
      TenureBasedAffordability(
          maximumQualifiedLoan: json["Maximum Qualified Loan"],
          maximumPropertyPrice: json["Maximum Property Price"],
          msr: json["MSR"],
          msrStatus: json["MSR Status"],
          tdsr: json["TDSR"],
          tdsrStatus: json["TDSR Status"],
          fullFludgeAmount: json["Full Fludge Amount"],
          fullUnFludgeAmount: json["Full UnFludge Amount"],
          loanTenure: json["Loan Tenure"],
          monthlyInstallment: json["Monthly Installment"]);

  Map<String, dynamic> toJson() => {
        "Maximum Qualified Loan": maximumQualifiedLoan,
        "Maximum Property Price": maximumPropertyPrice,
        "MSR": msr,
        "MSR Status": msrStatus,
        "TDSR Status": tdsrStatus,
        "TDSR": tdsr,
        "Full Fludge Amount": fullFludgeAmount,
        "Full UnFludge Amount": fullUnFludgeAmount,
        "Loan Tenure": loanTenure,
        "Monthly Installment": monthlyInstallment
      };
}
