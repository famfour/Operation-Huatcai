class EmailLogResponseModel {
  int? id;
  String? emailType;
  String? name;
  String? subject;
  String? toEmail;
  String? content;
  bool? sent;
  List<Attachments>? attachments;
  String? created;
  String? updated;

  EmailLogResponseModel(
      {this.id,
      this.emailType,
      this.name,
      this.subject,
      this.toEmail,
      this.content,
      this.sent,
      this.attachments,
      this.created,
      this.updated});

  EmailLogResponseModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    emailType = json['email_type'];
    name = json['name'];
    subject = json['subject'];
    toEmail = json['to_email'];
    content = json['content'];
    sent = json['sent'];
    if (json['attachments'] != null) {
      attachments = <Attachments>[];
      json['attachments'].forEach((v) {
        attachments!.add(Attachments.fromJson(v));
      });
    }
    created = json['created'];
    updated = json['updated'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['email_type'] = emailType;
    data['name'] = name;
    data['subject'] = subject;
    data['to_email'] = toEmail;
    data['content'] = content;
    data['sent'] = sent;
    if (attachments != null) {
      data['attachments'] = attachments!.map((v) => v.toJson()).toList();
    }
    data['created'] = created;
    data['updated'] = updated;
    return data;
  }
}

class Attachments {
  String? name;
  String? file;

  Attachments({this.name, this.file});

  Attachments.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    file = json['file'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['file'] = file;
    return data;
  }
}
