import 'dart:convert';

List<NewPurchaseTotalResponseModel> welcomeFromJson(String str) =>
    List<NewPurchaseTotalResponseModel>.from(
        json.decode(str).map((x) => NewPurchaseTotalResponseModel.fromJson(x)));

String welcomeToJson(List<NewPurchaseTotalResponseModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class NewPurchaseTotalResponseModel {
  NewPurchaseTotalResponseModel({
    this.totalMonthlyIncome,
    this.totalFinancialCommitments,
  });

  double? totalMonthlyIncome;
  double? totalFinancialCommitments;

  factory NewPurchaseTotalResponseModel.fromJson(Map<String, dynamic> json) =>
      NewPurchaseTotalResponseModel(
        totalMonthlyIncome: (json["total_monthly_income"]).toDouble(),
        totalFinancialCommitments:
            (json["total_financial_commitments"]).toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "total_monthly_income": totalMonthlyIncome,
        "total_financial_commitments": totalFinancialCommitments,
      };
}
