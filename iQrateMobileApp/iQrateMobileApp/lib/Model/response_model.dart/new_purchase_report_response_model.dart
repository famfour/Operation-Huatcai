import 'dart:convert';

NewPurchaseReportResponseModel welcomeFromJson(String str) =>
    NewPurchaseReportResponseModel.fromJson(json.decode(str));

// String welcomeToJson(NewPurchaseReportResponseModel data) =>
//     json.encode(data.toJson());

class NewPurchaseReportResponseModel {
  NewPurchaseReportResponseModel({
    this.summary,
    this.loanEligiblityMaximumLoanTenureBased,
    this.lowestFixed,
    this.lowestFloating,
    this.loanPackageMaximumLoanTenureBased,
    this.loanEligiblityPreferredLoanTenureBased,
    this.loanPackagePreferredLoanTenureBased,
  });

  Summary? summary;
  LoanEligibilityMaximumLoanTenure? loanEligiblityMaximumLoanTenureBased;
  LowestFixed? lowestFixed;
  LowestFloating? lowestFloating;
  LoanPackageLoanTenureBased? loanPackageMaximumLoanTenureBased;
  LoanEligibilityPreferredLoanTenure? loanEligiblityPreferredLoanTenureBased;
  LoanPackageLoanTenureBased? loanPackagePreferredLoanTenureBased;

  factory NewPurchaseReportResponseModel.fromJson(Map<String, dynamic> json) {
    return NewPurchaseReportResponseModel(
      summary: Summary.fromJson(json["Summary"]),
      loanEligiblityMaximumLoanTenureBased:
          LoanEligibilityMaximumLoanTenure.fromJson(
              json["Loan_Eligiblity_Maximum_Loan_Tenure_Based"]),
      lowestFixed: json["Lowest_fixed"].toString() == [].toString()
          ? null
          : LowestFixed.fromJson(json["Lowest_fixed"]),
      lowestFloating: json["Lowest_floating"].toString() == [].toString()
          ? null
          : LowestFloating.fromJson(json["Lowest_floating"]),
      loanPackageMaximumLoanTenureBased: LoanPackageLoanTenureBased.fromJson(
          json["Loan_Package_Maximum_Loan_Tenure_Based"]),
      loanEligiblityPreferredLoanTenureBased:
          json["Loan_Eligiblity_Preferred_Loan_Tenure_Based"].toString() ==
                  [].toString()
              ? null
              : LoanEligibilityPreferredLoanTenure.fromJson(
                  json["Loan_Eligiblity_Preferred_Loan_Tenure_Based"]),
      loanPackagePreferredLoanTenureBased:
          json["Loan_Package_Preferred_Loan_Tenure_Based"].toString() ==
                  [].toString()
              ? null
              : LoanPackageLoanTenureBased.fromJson(
                  json["Loan_Package_Preferred_Loan_Tenure_Based"]),
    );
  }

// Map<String, dynamic> toJson() => {
//       "Summary": summary!.toJson(),
//       "Loan_Eligiblity_Maximum_Loan_Tenure_Based":
//           Map.from(loanEligiblityMaximumLoanTenureBased!)
//               .map((k, v) => MapEntry<String, dynamic>(k, v)),
//       "Lowest_fixed": lowestFixed!.toJson(),
//       "Lowest_floating": lowestFloating!.toJson(),
//       "Loan_Package_Maximum_Loan_Tenure_Based":
//           loanPackageMaximumLoanTenureBased!.toJson(),
//       "Loan_Eligiblity_Preferred_Loan_Tenure_Based":
//           Map.from(loanEligiblityPreferredLoanTenureBased!)
//               .map((k, v) => MapEntry<String, dynamic>(k, v)),
//       "Loan_Package_Preferred_Loan_Tenure_Based":
//           loanPackagePreferredLoanTenureBased!.toJson(),
//     };
}

class LoanEligibilityMaximumLoanTenure {
  LoanEligibilityMaximumLoanTenure({
    this.maximumLoanQualified,
    this.maximumPropertyPrice,
    this.msr,
    this.tdsr,
    this.maximumLoanTenure,
    this.fullPledgeAmount,
    this.fullUnpledgeAmount,
    this.preferredPledgeAmount,
    this.unPledgeAmount,
    this.preferredUnpledgeAmount,
    this.pledgeAmount,
  });

  int? maximumLoanQualified;
  int? maximumPropertyPrice;
  double? msr;
  double? tdsr;
  int? maximumLoanTenure;
  int? fullPledgeAmount;
  int? fullUnpledgeAmount;
  int? preferredPledgeAmount;
  int? unPledgeAmount;
  int? preferredUnpledgeAmount;
  int? pledgeAmount;

  factory LoanEligibilityMaximumLoanTenure.fromJson(
          Map<String, dynamic> json) =>
      LoanEligibilityMaximumLoanTenure(
        maximumLoanQualified: json["Maximum_Loan_Qualified"],
        maximumPropertyPrice: json["Maximum_Property_Price"],
        msr: double.tryParse(json["MSR"].toString()),
        tdsr: double.tryParse(json["TDSR"].toString()),
        maximumLoanTenure: json["Maximum_Loan_Tenure"],
        fullPledgeAmount: json["Full_Pledge_Amount"],
        fullUnpledgeAmount: json["Full_Unpledge_Amount"],
        preferredPledgeAmount: json["Preferred_Pledge_Amount"],
        unPledgeAmount: json["UnPledge_Amount"],
        preferredUnpledgeAmount: json["Preferred_UnPledge_Amount"],
        pledgeAmount: json["Pledge_Amount"],
      );

// Map<String, dynamic> toJson() => {
//       "Maximum Loan Qualified": maximumLoanQualified,
//       "Maximum Property Price": maximumPropertyPrice,
//       "MSR": msr,
//       "TDSR": tdsr,
//       "Maximum Loan Tenure": maximumLoanTenure,
//       "Full Pledge Amount": fullPledgeAmount,
//       "Full Unpledge Amount": fullUnpledgeAmount,
//       "Preferred Pledge Amount": preferredPledgeAmount,
//       "UnPledge Amount": unPledgeAmount,
//       "Preferred UnPledge Amount": preferredUnpledgeAmount,
//       "Pledge Amount": pledgeAmount,
//     };
}

class LoanEligibilityPreferredLoanTenure {
  LoanEligibilityPreferredLoanTenure({
    this.preferredLoanQualified,
    this.maximumPropertyPrice,
    this.msr,
    this.tdsr,
    this.preferredLoanTenure,
    this.fullPledgeAmount,
    this.fullUnpledgeAmount,
    this.preferredPledgeAmount,
    this.unPledgeAmount,
    this.preferredUnpledgeAmount,
    this.pledgeAmount,
  });

  int? preferredLoanQualified;
  int? maximumPropertyPrice;
  double? msr;
  double? tdsr;
  int? preferredLoanTenure;
  int? fullPledgeAmount;
  int? fullUnpledgeAmount;
  int? preferredPledgeAmount;
  int? unPledgeAmount;
  int? preferredUnpledgeAmount;
  int? pledgeAmount;

  factory LoanEligibilityPreferredLoanTenure.fromJson(
          Map<String, dynamic> json) =>
      LoanEligibilityPreferredLoanTenure(
        preferredLoanQualified: json["Preffered_Loan_Qualified"],
        maximumPropertyPrice: json["Maximum_Property_Price"],
        msr: double.tryParse(json["MSR"].toString()),
        tdsr: double.tryParse(json["TDSR"].toString()),
        preferredLoanTenure: json["Preferred_Loan_Tenure"],
        fullPledgeAmount: json["Full_Pledge_Amount"],
        fullUnpledgeAmount: json["Full_Unpledge_Amount"],
        preferredPledgeAmount: json["Preferred_Pledge_Amount"],
        unPledgeAmount: json["UnPledge_Amount"],
        preferredUnpledgeAmount: json["Preferred_UnPledge_Amount"],
        pledgeAmount: json["Pledge_Amount"],
      );

// Map<String, dynamic> toJson() => {
//       "Maximum Loan Qualified": maximumLoanQualified,
//       "Maximum Property Price": maximumPropertyPrice,
//       "MSR": msr,
//       "TDSR": tdsr,
//       "Maximum Loan Tenure": maximumLoanTenure,
//       "Full Pledge Amount": fullPledgeAmount,
//       "Full Unpledge Amount": fullUnpledgeAmount,
//       "Preferred Pledge Amount": preferredPledgeAmount,
//       "UnPledge Amount": unPledgeAmount,
//       "Preferred UnPledge Amount": preferredUnpledgeAmount,
//       "Pledge Amount": pledgeAmount,
//     };
}

class LoanPackageLoanTenureBased {
  LoanPackageLoanTenureBased({
    this.lowestFloating,
    this.lowestFixed,
  });

  LowestF? lowestFloating;
  LowestF? lowestFixed;

  factory LoanPackageLoanTenureBased.fromJson(Map<String, dynamic> json) =>
      LoanPackageLoanTenureBased(
        lowestFloating: json["lowest_floating"] == null
            ? null
            : LowestF.fromJson(json["lowest_floating"]),
        lowestFixed: LowestF.fromJson(json["lowest_fixed"]),
      );

  Map<String, dynamic> toJson() => {
        "lowest_floating": lowestFloating!.toJson(),
        "lowest_fixed": lowestFixed!.toJson(),
      };
}

class LowestF {
  LowestF({
    this.monthlyInstallment,
    this.totalPayment,
    this.totalPrincipal,
    this.totalInterest,
    this.rate,
    this.mortgageRepaymentTable,
  });

  int? monthlyInstallment;
  dynamic
      totalPayment; //! changed to dynamic dataType as from backend we are not certain if it is int or double dataType
  int? totalPrincipal;
  dynamic totalInterest;
  dynamic rate;
  List<MortgageRepaymentTable>? mortgageRepaymentTable;

  factory LowestF.fromJson(Map<String, dynamic> json) => LowestF(
        monthlyInstallment: json["Monthly_Installment"],
        totalPayment: json["Total_Payment"],
        totalPrincipal: json["Total_Principal"],
        totalInterest: json["Total_Interest"],
        rate: json["rate"].toDouble(),
        mortgageRepaymentTable: List<MortgageRepaymentTable>.from(
            json["Mortgage_Repayment_Table"]
                .map((x) => MortgageRepaymentTable.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Monthly_Installment": monthlyInstallment,
        "Total_Payment": totalPayment,
        "Total_Principal": totalPrincipal,
        "Total_Interest": totalInterest,
        "rate": rate,
        "Mortgage_Repayment_Table":
            List<dynamic>.from(mortgageRepaymentTable!.map((x) => x.toJson())),
      };
}

class MortgageRepaymentTable {
  MortgageRepaymentTable({
    this.emiNo,
    this.year,
    this.month,
    this.interestRate,
    this.installmentAmount,
    this.interestRepayment,
    this.principalRepayment,
    this.outstandingAmount,
    this.cumInterestPaid,
  });

  int? emiNo;
  int? year;
  int? month;
  dynamic interestRate;
  dynamic installmentAmount;
  dynamic interestRepayment;
  dynamic principalRepayment;
  dynamic outstandingAmount;
  dynamic cumInterestPaid;

  factory MortgageRepaymentTable.fromJson(Map<String, dynamic> json) =>
      MortgageRepaymentTable(
        emiNo: json["emi_no"],
        year: json["year"],
        month: json["month"],
        interestRate: json["interest_rate"].toDouble(),
        installmentAmount: json["installment_amount"],
        interestRepayment: json["interest_repayment"],
        principalRepayment: json["principal_repayment"],
        outstandingAmount: json["outstanding_amount"],
        cumInterestPaid: json["cum_interest_paid"],
      );

  Map<String, dynamic> toJson() => {
        "emi_no": emiNo,
        "year": year,
        "month": month,
        "interest_rate": interestRate,
        "installment_amount": installmentAmount,
        "interest_repayment": interestRepayment,
        "principal_repayment": principalRepayment,
        "outstanding_amount": outstandingAmount,
        "cum_interest_paid": cumInterestPaid,
      };
}

class LowestFixed {
  LowestFixed({
    this.bankName,
    this.rateType,
    this.bankLogo,
    this.rateList,
    this.rateCategory,
    this.details,
  });

  String? bankName;
  String? rateType;
  String? bankLogo;
  String? rateCategory;
  List<double>? rateList;
  Details? details;

  factory LowestFixed.fromJson(Map<String, dynamic> json) => LowestFixed(
        bankName: json["bank_name"],
        rateType: json["rate_type"],
        bankLogo: json["bank_logo"],
        rateList: List<double>.from(json["rate_list"].map((x) => x.toDouble())),
        details: Details.fromJson(json["details"]),
        rateCategory: json["rate_category"],
      );

  Map<String, dynamic> toJson() => {
        "bank_name": bankName,
        "rate_type": rateType,
        "bank_logo": bankLogo,
        "rate_list": List<dynamic>.from(rateList!.map((x) => x)),
        "details": details!.toJson(),
        "rate_category": rateCategory,
      };
}

class Details {
  Details({
    this.id,
    this.bank,
    this.mask,
    this.rateCategory,
    this.rateType,
    this.propertyTypes,
    this.propertyStatus,
    this.loanCategory,
    this.lockInPeriod,
    this.minLoanAmount,
    this.depositToPlace,
    this.valuationSubsidy,
    this.fireInsuranceSubsidy,
    this.cashRebateLegalSubsidy,
    this.cashRebateSubsidyClawbackPeriodYears,
    this.partialRepaymentPenalty,
    this.partialRepaymentPenaltyRemarks,
    this.fullRepaymentPenalty,
    this.fullRepaymentPenaltyRemarks,
    this.rates,
    this.cancellationFee,
    this.cancellationFeeRemarks,
    this.depositToPlaceRemarks,
    this.remarksForClient,
    this.interestOffsetting,
    this.interestResetDate,
    this.processingFee,
    this.remarksForBroker,
    this.newPurchaseReferralFee,
    this.refinanceReferralFee,
    this.createdBy,
    this.updatedBy,
    this.created,
    this.updated,
  });

  int? id;
  Bank? bank;
  bool? mask;
  String? rateCategory;
  String? rateType;
  List<String>? propertyTypes;
  List<String>? propertyStatus;
  List<String>? loanCategory;
  int? lockInPeriod;
  int? minLoanAmount;
  int? depositToPlace;
  List<String>? valuationSubsidy;
  String? fireInsuranceSubsidy;
  List<String>? cashRebateLegalSubsidy;
  int? cashRebateSubsidyClawbackPeriodYears;
  double? partialRepaymentPenalty;
  String? partialRepaymentPenaltyRemarks;
  double? fullRepaymentPenalty;
  String? fullRepaymentPenaltyRemarks;
  List<Rate>? rates;
  String? cancellationFee;
  String? cancellationFeeRemarks = "";
  String? depositToPlaceRemarks;
  List<String>? remarksForClient;
  String? interestOffsetting;
  String? interestResetDate;
  String? processingFee;
  String? remarksForBroker;
  double? newPurchaseReferralFee;
  double? refinanceReferralFee;
  String? createdBy;
  String? updatedBy;
  DateTime? created;
  DateTime? updated;

  factory Details.fromJson(Map<String, dynamic> json) => Details(
        id: json["id"],
        bank: Bank.fromJson(json["bank"]),
        mask: json["mask"],
        rateCategory: json["rate_category"],
        rateType: json["rate_type"],
        propertyTypes: List<String>.from(json["property_types"].map((x) => x)),
        propertyStatus:
            List<String>.from(json["property_status"].map((x) => x)),
        loanCategory: List<String>.from(json["loan_category"].map((x) => x)),
        lockInPeriod: json["lock_in_period"],
        minLoanAmount: json["min_loan_amount"],
        depositToPlace: json["deposit_to_place"],
        valuationSubsidy:
            List<String>.from(json["valuation_subsidy"].map((x) => x)),
        fireInsuranceSubsidy: json["fire_insurance_subsidy"],
        cashRebateLegalSubsidy:
            List<String>.from(json["cash_rebate_legal_subsidy"].map((x) => x)),
        cashRebateSubsidyClawbackPeriodYears:
            json["cash_rebate_subsidy_clawback_period_years"],
        partialRepaymentPenalty: json["partial_repayment_penalty"].toDouble(),
        partialRepaymentPenaltyRemarks:
            json["partial_repayment_penalty_remarks"],
        fullRepaymentPenalty: json["full_repayment_penalty"].toDouble(),
        fullRepaymentPenaltyRemarks: json["full_repayment_penalty_remarks"],
        rates: List<Rate>.from(json["rates"].map((x) => Rate.fromJson(x))),
        cancellationFee: json["cancellation_fee"],
        cancellationFeeRemarks: json["cancellation_fee_remarks"] ?? "",
        depositToPlaceRemarks: json["deposit_to_place_remarks"],
        remarksForClient:
            List<String>.from(json["remarks_for_client"].map((x) => x)),
        interestOffsetting: json["interest_offsetting"],
        interestResetDate: json["interest_reset_date"],
        processingFee: json["processing_fee"],
        remarksForBroker: json["remarks_for_broker"],
        newPurchaseReferralFee: json["new_purchase_referral_fee"].toDouble(),
        refinanceReferralFee: json["refinance_referral_fee"].toDouble(),
        createdBy: json["created_by"],
        updatedBy: json["updated_by"],
        created: DateTime.parse(json["created"]),
        updated: DateTime.parse(json["updated"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "bank": bank!.toJson(),
        "mask": mask,
        "rate_category": rateCategory,
        "rate_type": rateType,
        "property_types": List<dynamic>.from(propertyTypes!.map((x) => x)),
        "property_status": List<dynamic>.from(propertyStatus!.map((x) => x)),
        "loan_category": List<dynamic>.from(loanCategory!.map((x) => x)),
        "lock_in_period": lockInPeriod,
        "min_loan_amount": minLoanAmount,
        "deposit_to_place": depositToPlace,
        "valuation_subsidy":
            List<dynamic>.from(valuationSubsidy!.map((x) => x)),
        "fire_insurance_subsidy": fireInsuranceSubsidy,
        "cash_rebate_legal_subsidy":
            List<dynamic>.from(cashRebateLegalSubsidy!.map((x) => x)),
        "cash_rebate_subsidy_clawback_period_years":
            cashRebateSubsidyClawbackPeriodYears,
        "partial_repayment_penalty": partialRepaymentPenalty,
        "partial_repayment_penalty_remarks": partialRepaymentPenaltyRemarks,
        "full_repayment_penalty": fullRepaymentPenalty,
        "full_repayment_penalty_remarks": fullRepaymentPenaltyRemarks,
        "rates": List<dynamic>.from(rates!.map((x) => x.toJson())),
        "cancellation_fee": cancellationFee,
        "cancellation_fee_remarks": cancellationFeeRemarks,
        "deposit_to_place_remarks": depositToPlaceRemarks,
        "remarks_for_client":
            List<dynamic>.from(remarksForClient!.map((x) => x)),
        "interest_offsetting": interestOffsetting,
        "interest_reset_date": interestResetDate,
        "processing_fee": processingFee,
        "remarks_for_broker": remarksForBroker,
        "new_purchase_referral_fee": newPurchaseReferralFee,
        "refinance_referral_fee": refinanceReferralFee,
        "created_by": createdBy,
        "updated_by": updatedBy,
        "created": created!.toIso8601String(),
        "updated": updated!.toIso8601String(),
      };
}

class Bank {
  Bank({
    this.id,
    this.name,
    this.logo,
    this.bankForm,
  });

  int? id;
  String? name;
  String? logo;
  dynamic bankForm;

  factory Bank.fromJson(Map<String, dynamic> json) => Bank(
        id: json["id"],
        name: json["name"],
        logo: json["logo"],
        bankForm: json["bank_form"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "logo": logo,
        "bank_form": bankForm,
      };
}

class Rate {
  Rate({
    this.year,
    this.referenceRate,
    this.bankSpread,
    this.totalInterestRate,
  });

  String? year;
  ReferenceRate? referenceRate;
  dynamic bankSpread;
  double? totalInterestRate;

  factory Rate.fromJson(Map<String, dynamic> json) => Rate(
        year: json["year"],
        referenceRate: ReferenceRate.fromJson(json["reference_rate"]),
        bankSpread: json["bank_spread"],
        totalInterestRate: json["total_interest_rate"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "year": year,
        "reference_rate": referenceRate!.toJson(),
        "bank_spread": bankSpread,
        "total_interest_rate": totalInterestRate,
      };
}

class ReferenceRate {
  ReferenceRate({
    this.rateType,
    this.reference,
    this.interestRate,
    this.equation,
    this.remarks,
  });

  String? rateType;
  String? reference;
  double? interestRate;
  String? equation;
  String? remarks;

  factory ReferenceRate.fromJson(Map<String, dynamic> json) => ReferenceRate(
        rateType: json["rate_type"],
        reference: json["reference"],
        interestRate: json["interest_rate"].toDouble(),
        equation: json["equation"],
        remarks: json["remarks"],
      );

  Map<String, dynamic> toJson() => {
        "rate_type": rateType,
        "reference": reference,
        "interest_rate": interestRate,
        "equation": equation,
        "remarks": remarks,
      };
}

class LowestFloating {
  LowestFloating({
    this.bankName,
    this.rateType,
    this.bankLogo,
    this.rateList,
    this.rateCategory,
    this.details,
  });

  String? bankName;
  String? rateType;
  String? rateCategory;
  String? bankLogo;
  List<double>? rateList;
  Details? details;

  factory LowestFloating.fromJson(Map<String, dynamic> json) => LowestFloating(
        bankName: json["bank_name"],
        rateType: json["rate_type"],
        bankLogo: json["bank_logo"],
        rateList: List<double>.from(json["rate_list"].map((x) => x.toDouble())),
        details: Details.fromJson(json["details"]),
        rateCategory: json["rate_category"],
      );

  Map<String, dynamic> toJson() => {
        "bank_name": bankName,
        "rate_type": rateType,
        "bank_logo": bankLogo,
        "rate_list": List<dynamic>.from(rateList!.map((x) => x)),
        "details": details!.toJson(),
        "rate_category": rateCategory,
      };
}

class Summary {
  Summary({
    this.preparedFor,
    this.propertyPurchasePrice,
    this.cashDownPayment,
    this.cpfOrCashDownPayment,
    this.loanAmount,
    this.buyerStampDuty,
    this.additionalBuyerStampDuty,
    this.legalFees,
    this.valuationFees,
    this.otherFees,
  });

  String? preparedFor;
  int? propertyPurchasePrice;
  String? cashDownPayment;
  String? cpfOrCashDownPayment;
  int? loanAmount;
  int? buyerStampDuty;
  int? additionalBuyerStampDuty;
  int? legalFees;
  int? valuationFees;
  int? otherFees;

  factory Summary.fromJson(Map<String, dynamic> json) => Summary(
        preparedFor: json["Prepared_for"],
        propertyPurchasePrice: json["Property_Purchase_Price"],
        cashDownPayment: json["Cash_Down_Payment"],
        cpfOrCashDownPayment: json["CPF_or_Cash_Down_Payment"],
        loanAmount: json["Loan_Amount"],
        buyerStampDuty: json["Buyer_Stamp_Duty"],
        additionalBuyerStampDuty: json["Additional_Buyer_Stamp_Duty"],
        legalFees: json["Legal_Fees"],
        valuationFees: json["Valuation_Fees"],
        otherFees: json["Other_Fees"],
      );

  Map<String, dynamic> toJson() => {
        "Prepared_for": preparedFor,
        "Property_Purchase_Price": propertyPurchasePrice,
        "Cash_Down_Payment": cashDownPayment,
        "CPF_or_Cash_Down_Payment": cpfOrCashDownPayment,
        "Loan_Amount": loanAmount,
        "Buyer_Stamp_Duty": buyerStampDuty,
        "Additional_Buyer_Stamp_Duty": additionalBuyerStampDuty,
        "Legal_Fees": legalFees,
        "Valuation_Fees": valuationFees,
        "Other_Fees": otherFees,
      };
}
