// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString?);

import 'dart:convert';

DeleteProfileImageResponseModel welcomeFromJson(String? str) =>
    DeleteProfileImageResponseModel.fromJson(json.decode(str!));

String? welcomeToJson(DeleteProfileImageResponseModel data) =>
    json.encode(data.toJson());

class DeleteProfileImageResponseModel {
  DeleteProfileImageResponseModel({
    this.resultcode,
    this.data,
  });

  int? resultcode;
  ResponseData? data;

  factory DeleteProfileImageResponseModel.fromJson(
          Map<String?, dynamic> json) =>
      DeleteProfileImageResponseModel(
        resultcode: json["resultcode"],
        data: ResponseData.fromJson(json["data"]),
      );

  Map<String?, dynamic> toJson() => {
        "resultcode": resultcode,
        "data": data!.toJson(),
      };
}

class ResponseData {
  ResponseData({
    this.id,
    this.fullName,
    this.email,
    this.password,
    this.countryCode,
    this.mobile,
    this.wholeNumber,
    this.phone,
    this.userType,
    this.membershipType,
    this.photo,
    this.otp,
    this.token,
    this.agreedTerm,
    this.verificationCode,
    this.verificationCodeSendDatetime,
    this.isVerified,
    this.status,
    this.createdUserId,
    this.updatedUserId,
    this.createdAt,
    this.updatedAt,
    this.resendcodeEmailCount,
    this.resendcodeSmsCount,
    this.dob,
    this.device,
    this.ip,
    this.location,
    this.occupation,
    this.postalCode,
    this.isEmailVerified,
    this.isMobileVerified,
    this.address,
    this.twoFactorAuthenticationSecret,
    this.isTwoFactorAuthenticationEnabled,
  });

  String? id;
  String? fullName;
  String? email;
  dynamic password;
  String? countryCode;
  String? mobile;
  String? wholeNumber;
  dynamic phone;
  String? userType;
  String? membershipType;
  String? photo;
  dynamic otp;
  dynamic token;
  int? agreedTerm;
  String? verificationCode;
  dynamic verificationCodeSendDatetime;
  dynamic isVerified;
  int? status;
  String? createdUserId;
  String? updatedUserId;
  DateTime? createdAt;
  DateTime? updatedAt;
  int? resendcodeEmailCount;
  int? resendcodeSmsCount;
  String? dob;
  dynamic device;
  dynamic ip;
  dynamic location;
  dynamic occupation;
  dynamic postalCode;
  int? isEmailVerified;
  int? isMobileVerified;
  dynamic address;
  dynamic twoFactorAuthenticationSecret;
  bool? isTwoFactorAuthenticationEnabled;

  factory ResponseData.fromJson(Map<String?, dynamic> json) => ResponseData(
        id: json["id"],
        fullName: json["full_name"],
        email: json["email"],
        password: json["password"],
        countryCode: json["country_code"],
        mobile: json["mobile"],
        wholeNumber: json["whole_number"],
        phone: json["phone"],
        userType: json["user_type"],
        membershipType: json["membership_type"],
        photo: json["photo"],
        otp: json["otp"],
        token: json["token"],
        agreedTerm: json["agreed_term"],
        verificationCode: json["verification_code"],
        verificationCodeSendDatetime: json["verification_code_send_datetime"],
        isVerified: json["is_verified"],
        status: json["status"],
        createdUserId: json["created_user_id"],
        updatedUserId: json["updated_user_id"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        resendcodeEmailCount: json["resendcode_email_count"],
        resendcodeSmsCount: json["resendcode_sms_count"],
        dob: json["dob"],
        device: json["device"],
        ip: json["ip"],
        location: json["location"],
        occupation: json["occupation"],
        postalCode: json["postal_code"],
        isEmailVerified: json["is_email_verified"],
        isMobileVerified: json["is_mobile_verified"],
        address: json["address"],
        twoFactorAuthenticationSecret: json["twoFactorAuthenticationSecret"],
        isTwoFactorAuthenticationEnabled:
            json["isTwoFactorAuthenticationEnabled"],
      );

  Map<String?, dynamic> toJson() => {
        "id": id,
        "full_name": fullName,
        "email": email,
        "password": password,
        "country_code": countryCode,
        "mobile": mobile,
        "whole_number": wholeNumber,
        "phone": phone,
        "user_type": userType,
        "membership_type": membershipType,
        "photo": photo,
        "otp": otp,
        "token": token,
        "agreed_term": agreedTerm,
        "verification_code": verificationCode,
        "verification_code_send_datetime": verificationCodeSendDatetime,
        "is_verified": isVerified,
        "status": status,
        "created_user_id": createdUserId,
        "updated_user_id": updatedUserId,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
        "resendcode_email_count": resendcodeEmailCount,
        "resendcode_sms_count": resendcodeSmsCount,
        //"dob": "${dob!.year.toString().padLeft(4, '0')}-${dob!.month.toString().padLeft(2, '0')}-${dob!.day.toString().padLeft(2, '0')}",
        "device": device,
        "ip": ip,
        "location": location,
        "occupation": occupation,
        "postal_code": postalCode,
        "is_email_verified": isEmailVerified,
        "is_mobile_verified": isMobileVerified,
        "address": address,
        "twoFactorAuthenticationSecret": twoFactorAuthenticationSecret,
        "isTwoFactorAuthenticationEnabled": isTwoFactorAuthenticationEnabled,
      };
}
