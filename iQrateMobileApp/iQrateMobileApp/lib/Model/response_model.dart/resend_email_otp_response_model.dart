// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

ResendEmailOtpResponseModel welcomeFromJson(String str) =>
    ResendEmailOtpResponseModel.fromJson(json.decode(str));

String welcomeToJson(ResendEmailOtpResponseModel data) =>
    json.encode(data.toJson());

class ResendEmailOtpResponseModel {
  ResendEmailOtpResponseModel({
    this.message,
  });

  String? message;

  factory ResendEmailOtpResponseModel.fromJson(Map<String, dynamic> json) =>
      ResendEmailOtpResponseModel(
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "message": message,
      };
}
