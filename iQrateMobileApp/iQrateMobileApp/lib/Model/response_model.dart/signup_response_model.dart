// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

SignUpResponseModel welcomeFromJson(String str) =>
    SignUpResponseModel.fromJson(json.decode(str));

String welcomeToJson(SignUpResponseModel data) => json.encode(data.toJson());

class SignUpResponseModel {
  SignUpResponseModel({
    this.id,
    this.fullName,
    this.email,
    this.countryCode,
    this.countryCodeLabel,
    this.mobile,
    this.dob,
    this.agreedTerm,
    this.smsToken,
  });

  String? id;
  String? fullName;
  String? email;
  String? countryCode;
  String? countryCodeLabel;
  String? mobile;
  String? dob;
  int? agreedTerm;
  String? smsToken;

  factory SignUpResponseModel.fromJson(Map<String, dynamic> json) =>
      SignUpResponseModel(
        id: json["id"],
        fullName: json["full_name"],
        email: json["email"],
        countryCode: json["country_code"],
        countryCodeLabel: json["country_code_label"],
        mobile: json["mobile"],
        dob: json["dob"],
        agreedTerm: json["agreed_term"],
        smsToken: json["SMSToken"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "full_name": fullName,
        "email": email,
        "country_code": countryCode,
        "country_code_label": countryCodeLabel,
        "mobile": mobile,
        "dob": dob,
        "agreed_term": agreedTerm,
        "SMSToken": smsToken,
      };
}
