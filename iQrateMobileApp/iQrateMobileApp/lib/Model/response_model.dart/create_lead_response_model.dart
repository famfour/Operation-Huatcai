// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

CreateLeadResponseModel welcomeFromJson(String str) =>
    CreateLeadResponseModel.fromJson(json.decode(str));

String welcomeToJson(CreateLeadResponseModel data) =>
    json.encode(data.toJson());

class CreateLeadResponseModel {
  CreateLeadResponseModel(
      {this.id,
      this.leadType,
      this.agentApplied,
      this.createdBy,
      this.updatedBy,
      //this.created,
      //this.updated,
      this.pdpaStatus,
      this.name,
      this.number,
      this.email,
      this.dob,
      this.nationality,
      this.annualIncome});

  int? id;
  String? leadType;
  String? agentApplied;
  dynamic createdBy;
  dynamic updatedBy;
  bool? pdpaStatus;
  String? name;
  String? number;
  String? email;
  String? dob;
  String? nationality;
  double? annualIncome;
  //DateTime? created;
  //DateTime? updated;

  factory CreateLeadResponseModel.fromJson(Map<String, dynamic> json) =>
      CreateLeadResponseModel(
          id: json["id"],
          leadType: json["lead_type"],
          agentApplied: json["agent_applied"],
          createdBy: json["created_by"],
          updatedBy: json["updated_by"],
          pdpaStatus: json['pdpa_status'],
          name: json['name'],
          number: json['phone_number'].toString(),
          email: json['email'],
          dob: json['dob'],
          nationality: json['nationality'],
          annualIncome: json['annual_income']
          //created: DateTime.parse(json["created"]),
          //updated: DateTime.parse(json["updated"]),
          );

  Map<String, dynamic> toJson() => {
        "id": id,
        "lead_type": leadType,
        "agent_applied": agentApplied,
        "created_by": createdBy,
        "updated_by": updatedBy,
        //"created": created!.toIso8601String(),
        //"updated": updated!.toIso8601String(),
      };
}
