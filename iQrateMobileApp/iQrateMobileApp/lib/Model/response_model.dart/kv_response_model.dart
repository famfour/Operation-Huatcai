// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

List<KvValueStoreResponseModel> welcomeFromJson(String str) =>
    List<KvValueStoreResponseModel>.from(
        json.decode(str).map((x) => KvValueStoreResponseModel.fromJson(x)));

String welcomeToJson(List<KvValueStoreResponseModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class KvValueStoreResponseModel {
  KvValueStoreResponseModel({
    this.key,
    this.code,
    this.valueType,
    this.value,
  });

  String? key;
  String? code;
  String? valueType;
  dynamic value;

  factory KvValueStoreResponseModel.fromJson(Map<String, dynamic> json) =>
      KvValueStoreResponseModel(
        key: json["key"],
        code: json["code"],
        valueType: json["value_type"],
        value: json["value"],
      );

  Map<String, dynamic> toJson() => {
        "key": key,
        "code": code,
        "value_type": valueType,
        "value": value,
      };
}

class ValueClass {
  ValueClass({
    this.rework,
    this.pending,
    this.submitted,
    this.lost,
    this.won,
    this.others,
    this.coBroke,
    this.yourself,
    this.the1,
    this.the2,
    this.the3,
    this.the4,
    this.the5,
    this.the6,
    this.the7,
    this.the8,
    this.the9,
    this.the10,
    this.refinanceCashOutTermLoan,
    this.partPurchaseDecouplingDivorce,
    this.newPurchaseApprovalInPrinciple,
    this.newPurchaseWithOptionToPurchase,
    this.completed,
    this.underConstructionToObtainTopWithin2Years,
    this.underConstructionToObtainTopMoreThan2Years,
    this.hdb,
    this.landed,
    this.ecResaleOutOfMop,
    this.condominiumApartment,
    this.strataHousingTownhouses,
    this.ecFromDeveloperWithinMop,
    this.standard,
    this.exclusive,
    this.sora,
    this.board,
    this.fixed,
    this.sibor,
    this.fixedDeposit,
  });

  String? rework;
  String? pending;
  String? submitted;
  String? lost;
  String? won;
  String? others;
  String? coBroke;
  String? yourself;
  String? the1;
  String? the2;
  String? the3;
  String? the4;
  String? the5;
  String? the6;
  String? the7;
  String? the8;
  String? the9;
  String? the10;
  String? refinanceCashOutTermLoan;
  String? partPurchaseDecouplingDivorce;
  String? newPurchaseApprovalInPrinciple;
  String? newPurchaseWithOptionToPurchase;
  String? completed;
  String? underConstructionToObtainTopWithin2Years;
  String? underConstructionToObtainTopMoreThan2Years;
  String? hdb;
  String? landed;
  String? ecResaleOutOfMop;
  String? condominiumApartment;
  String? strataHousingTownhouses;
  String? ecFromDeveloperWithinMop;
  String? standard;
  String? exclusive;
  String? sora;
  String? board;
  String? fixed;
  String? sibor;
  String? fixedDeposit;

  factory ValueClass.fromJson(Map<String, dynamic> json) => ValueClass(
        rework: json["rework"],
        pending: json["pending"],
        submitted: json["submitted"],
        lost: json["lost"],
        won: json["won"],
        others: json["others"],
        coBroke: json["co_broke"],
        yourself: json["yourself"],
        the1: json["1"],
        the2: json["2"],
        the3: json["3"],
        the4: json["4"],
        the5: json["5"],
        the6: json["6"],
        the7: json["7"],
        the8: json["8"],
        the9: json["9"],
        the10: json["10"],
        refinanceCashOutTermLoan: json["refinance_cash_out_term_loan"],
        partPurchaseDecouplingDivorce: json["part_purchase_decoupling_divorce"],
        newPurchaseApprovalInPrinciple:
            json["new_purchase_approval_in_principle"],
        newPurchaseWithOptionToPurchase:
            json["new_purchase_with_option_to_purchase"],
        completed: json["completed"],
        underConstructionToObtainTopWithin2Years:
            json["under_construction_to_obtain_top_within_2_years"],
        underConstructionToObtainTopMoreThan2Years:
            json["under_construction_to_obtain_top_more_than_2_years"],
        hdb: json["hdb"],
        landed: json["landed"],
        ecResaleOutOfMop: json["ec_resale_out_of_mop"],
        condominiumApartment: json["condominium_apartment"],
        strataHousingTownhouses: json["strata_housing_townhouses"],
        ecFromDeveloperWithinMop: json["ec_from_developer_within_mop"],
        standard: json["standard"],
        exclusive: json["exclusive"],
        sora: json["sora"],
        board: json["board"],
        fixed: json["fixed"],
        sibor: json["sibor"],
        fixedDeposit: json["fixed_deposit"],
      );

  Map<String, dynamic> toJson() => {
        "rework": rework,
        "pending": pending,
        "submitted": submitted,
        "lost": lost,
        "won": won,
        "others": others,
        "co_broke": coBroke,
        "yourself": yourself,
        "1": the1,
        "2": the2,
        "3": the3,
        "4": the4,
        "5": the5,
        "6": the6,
        "7": the7,
        "8": the8,
        "9": the9,
        "10": the10,
        "refinance_cash_out_term_loan": refinanceCashOutTermLoan,
        "part_purchase_decoupling_divorce": partPurchaseDecouplingDivorce,
        "new_purchase_approval_in_principle": newPurchaseApprovalInPrinciple,
        "new_purchase_with_option_to_purchase": newPurchaseWithOptionToPurchase,
        "completed": completed,
        "under_construction_to_obtain_top_within_2_years":
            underConstructionToObtainTopWithin2Years,
        "under_construction_to_obtain_top_more_than_2_years":
            underConstructionToObtainTopMoreThan2Years,
        "hdb": hdb,
        "landed": landed,
        "ec_resale_out_of_mop": ecResaleOutOfMop,
        "condominium_apartment": condominiumApartment,
        "strata_housing_townhouses": strataHousingTownhouses,
        "ec_from_developer_within_mop": ecFromDeveloperWithinMop,
        "standard": standard,
        "exclusive": exclusive,
        "sora": sora,
        "board": board,
        "fixed": fixed,
        "sibor": sibor,
        "fixed_deposit": fixedDeposit,
      };
}

// ignore: constant_identifier_names
enum ValueType { JSON, LIST }

final valueTypeValues =
    EnumValues({"json": ValueType.JSON, "list": ValueType.LIST});

class EnumValues<T> {
  Map<String, T>? map;
  Map<T, String>? reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    reverseMap;
    return reverseMap!;
  }
}

/*class KvListTypeModel {
  String? key;
  String? code;
  String? valueType;
  List<String>? value;

  KvListTypeModel({this.key, this.code, this.valueType, this.value});

  KvListTypeModel.fromJson(Map<String, dynamic> json) {
    key = json['key'];
    code = json['code'];
    valueType = json['value_type'];
    value = json['value'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['key'] = key;
    data['code'] = code;
    data['value_type'] = valueType;
    data['value'] = value;
    return data;
  }
}*/

