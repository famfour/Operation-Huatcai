// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

ChangeEmailResponseModel welcomeFromJson(String str) =>
    ChangeEmailResponseModel.fromJson(json.decode(str));

String welcomeToJson(ChangeEmailResponseModel data) =>
    json.encode(data.toJson());

class ChangeEmailResponseModel {
  ChangeEmailResponseModel({
    this.message,
  });

  String? message;

  factory ChangeEmailResponseModel.fromJson(Map<String, dynamic> json) =>
      ChangeEmailResponseModel(
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "message": message,
      };
}
