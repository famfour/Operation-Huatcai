class PdpaTokenResponse {
  PdpaTokenResponse({
      this.client, 
      this.message,
      this.status,});

  PdpaTokenResponse.fromJson(dynamic json) {
    client = json['client'];
    message = json['message'];
    status = json['status'];
  }
  int? client;
  String? message;
  bool? status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['client'] = client;
    map['message'] = message;
    map['status'] = status;
    return map;
  }

}


class PdpaTokenSend {
  PdpaTokenSend({
    this.token});

  PdpaTokenSend.fromJson(dynamic json) {

    token = json['token'];

  }

  String? token;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['token'] = token;
    return map;
  }

}
