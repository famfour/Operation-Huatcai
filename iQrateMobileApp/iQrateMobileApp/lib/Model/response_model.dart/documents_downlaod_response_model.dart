class DocumentsDownlaodResponseModel {
  DocumentsDownlaodResponseModel({
      this.id, 
      this.url, 
      this.filename,});

  DocumentsDownlaodResponseModel.fromJson(dynamic json) {
    id = json['id'];
    url = json['url'];
    filename = json['filename'];
  }
  String? id;
  String? url;
  String? filename;
DocumentsDownlaodResponseModel copyWith({  String? id,
  String? url,
  String? filename,
}) => DocumentsDownlaodResponseModel(  id: id ?? this.id,
  url: url ?? this.url,
  filename: filename ?? this.filename,
);
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['url'] = url;
    map['filename'] = filename;
    return map;
  }

}