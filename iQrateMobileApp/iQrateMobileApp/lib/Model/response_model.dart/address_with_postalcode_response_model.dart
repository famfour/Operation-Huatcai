class AddressWithPostalCodeResponseModel {
  int? found;
  int? totalNumberOfPages;
  int? pageNumber;
  List<OneResult>? results;

  AddressWithPostalCodeResponseModel(
      {this.found, this.totalNumberOfPages, this.pageNumber, this.results});

  factory AddressWithPostalCodeResponseModel.fromJson(json) {
    return AddressWithPostalCodeResponseModel(
        found: json['found'],
        totalNumberOfPages: json['totalNumPages'],
        pageNumber: json['pageNum'],
        results: List<OneResult>.from(
            json['results'].map((data) => OneResult.fromJson(data))));
  }
}

class OneResult {
  String? searchVal;
  String? blkNo;
  String? roadName;
  String? building;
  String? address;
  String? postal;

  OneResult(
      {this.searchVal,
      this.blkNo,
      this.roadName,
      this.building,
      this.address,
      this.postal});

  factory OneResult.fromJson(json) {
    return OneResult(
        searchVal: json['SEARCHVAL'],
        blkNo: json['BLK_NO'],
        roadName: json['ROAD_NAME'],
        building: json['BUILDING'],
        address: json['ADDRESS'],
        postal: json['POSTAL']);
  }
}
