// ignore_for_file: non_constant_identifier_names

class BuyerStampDutySendModel {
  double? purchasePrice;
  int? loanApplicant;
  int? applicant1Property;
  int? applicant2Property;
  int? applicant3Property;
  int? applicant4Property;
  String? applicant1Nationality;
  String? applicant2Nationality;
  String? applicant3Nationality;
  String? applicant4Nationality;

  BuyerStampDutySendModel({
    this.purchasePrice,
    this.loanApplicant,
    this.applicant1Property,
    this.applicant2Property,
    this.applicant3Property,
    this.applicant4Property,
    this.applicant1Nationality,
    this.applicant2Nationality,
    this.applicant3Nationality,
    this.applicant4Nationality,
  });

  BuyerStampDutySendModel.fromJson(Map<String, dynamic> json) {
    purchasePrice = json['purchase_price'];
    loanApplicant = json['loan_applicant'];
    applicant1Property = json['applicant1_property'];
    applicant2Property = json['applicant2_property'];
    applicant3Property = json['applicant3_property'];
    applicant4Property = json['applicant4_property'];
    applicant1Nationality = json['applicant1_nationality'];
    applicant2Nationality = json['applicant2_nationality'];
    applicant3Nationality = json['applicant3_nationality'];
    applicant4Nationality = json['applicant4_nationality'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['purchase_price'] = purchasePrice;
    data['loan_applicant'] = loanApplicant;
    data['applicant1_property'] = applicant1Property;
    data['applicant2_property'] = applicant2Property;
    data['applicant3_property'] = applicant3Property;
    data['applicant4_property'] = applicant4Property;
    data['applicant1_nationality'] = applicant1Nationality;
    data['applicant2_nationality'] = applicant2Nationality;
    data['applicant3_nationality'] = applicant3Nationality;
    data['applicant4_nationality'] = applicant4Nationality;
    return data;
  }
}
