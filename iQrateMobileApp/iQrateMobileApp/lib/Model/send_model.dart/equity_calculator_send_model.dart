class EquityCalculatorSendModel {
  int? existingHousingLoans;
  String? currentValuation;
  String? totalCpf;
  String? outstandingLoan;
  String? loanTagged;

  EquityCalculatorSendModel({
    this.existingHousingLoans,
    this.currentValuation,
    this.totalCpf,
    this.outstandingLoan,
    this.loanTagged,
  });

  EquityCalculatorSendModel.fromJson(Map<String, dynamic> json) {
    existingHousingLoans = json['existing_housing_loans'];
    currentValuation = json['current_valuation'];
    totalCpf = json['total_cpf'];
    outstandingLoan = json['outstanding_loan'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['existing_housing_loans'] = existingHousingLoans;
    data['current_valuation'] = currentValuation;
    data['total_cpf'] = totalCpf;
    data['outstanding_loan'] = outstandingLoan;
    data['loan_tagged'] = loanTagged;
    return data;
  }
}
