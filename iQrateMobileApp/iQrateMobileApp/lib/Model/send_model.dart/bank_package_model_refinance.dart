class BankPackageModel {
  int? id;
  String? bank;
  String? bankName;
  double? interestRate;
  dynamic packageId;

  BankPackageModel({
    this.id,
    this.bank,
    this.bankName,
    this.interestRate,
    this.packageId,
  });

  factory BankPackageModel.fromJson(Map<String, dynamic> json) {
    return BankPackageModel(
      id: json['id'],
      bank: json['bank'],
      bankName: json['bank_name'],
      interestRate: json['interest_rate'],
      packageId: json['package_id'],
    );
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "bank": bank,
        "bank_name": bankName,
        "interest_rate": interestRate,
        "package_id": packageId,
      };
}
