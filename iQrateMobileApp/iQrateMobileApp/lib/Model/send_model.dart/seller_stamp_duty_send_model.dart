class SellerStampDutyCalculatorSendModel {
  String? sellingPrice;
  String? date;

  SellerStampDutyCalculatorSendModel({
    this.sellingPrice,
    this.date,
  });

  SellerStampDutyCalculatorSendModel.fromJson(Map<String, dynamic> json) {
    sellingPrice = json['selling_price'];
    date = json['date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['selling_price'] = sellingPrice;
    data['date'] = date;
    return data;
  }
}
