// ignore_for_file: non_constant_identifier_names

class EmailVerificationSendModel {
  String? verification_code;

  EmailVerificationSendModel(
      {this.verification_code}); //this.deviceId, this.deviceType});

  EmailVerificationSendModel.fromJson(Map<String, dynamic> json) {
    verification_code = json['verification_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['verification_code'] = verification_code;
    return data;
  }
}
