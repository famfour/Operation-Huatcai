class ChangeEmailSendModel {
  String? email;

  ChangeEmailSendModel({
    this.email,
  }); //this.deviceId, this.deviceType});

  ChangeEmailSendModel.fromJson(Map<String, dynamic> json) {
    email = json['email'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['email'] = email;
    return data;
  }
}
