// ignore_for_file: file_names

class GeneratePackageSendModel {
  GeneratePackageSendModel({
    this.lead,
    this.packages,
  });

  GeneratePackageSendModel.fromJson(dynamic json) {
    lead = json['lead'];
    packages = json['packages'] != null ? json['packages'].cast<int>() : [];
  }
  int? lead;
  List<int>? packages;
  GeneratePackageSendModel copyWith({
    int? lead,
    List<int>? packages,
  }) =>
      GeneratePackageSendModel(
        lead: lead ?? this.lead,
        packages: packages ?? this.packages,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['lead'] = lead;
    map['packages'] = packages;
    return map;
  }
}
