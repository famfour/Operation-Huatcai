// ignore_for_file: non_constant_identifier_names

class GoogleDobSendModel {
  String? mobile;
  String? country_code;
  String? country_code_label;


  GoogleDobSendModel({
    this.mobile,
    this.country_code,
    this.country_code_label,
  }); //this.deviceId, this.deviceType});

  GoogleDobSendModel.fromJson(Map<String, dynamic> json) {
    mobile = json['mobile'];
    country_code = json['country_code'];
    country_code_label = json['country_code_label'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['mobile'] = mobile;
    data['country_code'] = country_code;
    data['country_code_label'] = country_code_label;

    return data;
  }
}
