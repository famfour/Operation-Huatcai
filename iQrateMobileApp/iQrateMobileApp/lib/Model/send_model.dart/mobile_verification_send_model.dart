// ignore_for_file: non_constant_identifier_names

class MobileVerificationSendModel {
  String? verification_code;
  String? verification_token;
  String? type;

  MobileVerificationSendModel({
    this.verification_code,
    this.verification_token,
    this.type,
  }); //this.deviceId, this.deviceType});

  MobileVerificationSendModel.fromJson(Map<String, dynamic> json) {
    verification_code = json['verification_code'];
    verification_token = json['verification_token'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['verification_code'] = verification_code;
    data['verification_token'] = verification_token;
    data['type'] = type;
    return data;
  }
}
