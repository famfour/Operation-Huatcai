// ignore_for_file: non_constant_identifier_names

class NewPurchaseGetPledgeAmountSendModel {
  double? loanAmount;
  int? preferredUnfludgeAmount;
  double? totalMonthlyIncome;
  int? loanTenure;
  double? totalFinancialCommitments;
  int? propertyType;

  NewPurchaseGetPledgeAmountSendModel(
      {this.loanAmount,
      this.preferredUnfludgeAmount,
      this.totalMonthlyIncome,
      this.loanTenure,
      this.totalFinancialCommitments,
      this.propertyType});

  NewPurchaseGetPledgeAmountSendModel.fromJson(Map<String, dynamic> json) {
    loanAmount = json['loan_amount'];
    preferredUnfludgeAmount = json['preferred_unfludge_amount'];
    totalMonthlyIncome = json['total_monthly_income'];
    loanTenure = json['loan_tenure'];
    totalFinancialCommitments = json['total_financial_commitments'];
    propertyType = json['property_type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['loan_amount'] = loanAmount;
    data['preferred_unfludge_amount'] = preferredUnfludgeAmount;
    data['total_monthly_income'] = totalMonthlyIncome;
    data['loan_tenure'] = loanTenure;
    data['total_financial_commitments'] = totalFinancialCommitments;
    data['property_type'] = propertyType;
    return data;
  }
}
