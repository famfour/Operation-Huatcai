class CreateLeadSendModel {
  String? leadType;
  String? agentApplied;

  CreateLeadSendModel({
    this.leadType,
    this.agentApplied,
  });

  CreateLeadSendModel.fromJson(Map<String, dynamic> json) {
    leadType = json['lead_type'];
    agentApplied = json['agent_applied'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['lead_type'] = leadType;
    data['agent_applied'] = agentApplied;
    return data;
  }
}
