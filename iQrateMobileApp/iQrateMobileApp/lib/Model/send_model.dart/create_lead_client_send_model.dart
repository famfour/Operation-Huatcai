class CreateLeadClientSendModel {
  CreateLeadClientSendModel({
    this.email,
    this.lead,
    this.name,
    this.nationality,
    this.phoneNumber,
    this.mainApplicant,
    this.annualIncome,
    this.dob,
    this.streetName,
    this.blockNumber,
    this.unitNumber,
    this.projectName,
    this.postalCode,
    this.countryCode,
  });

  CreateLeadClientSendModel.fromJson(dynamic json) {
    email = json['email'];
    lead = json['lead'];
    name = json['name'];
    nationality = json['nationality'];
    phoneNumber = json['phone_number'];
    countryCode = json['country_code'];
    mainApplicant = json['main_applicant'];
    annualIncome = json['annual_income'];
    dob = json['dob'];
    streetName = json['street_name'];
    blockNumber = json['block_number'];
    unitNumber = json['unit_number'];
    projectName = json['project_name'];
    postalCode = json['postal_code'];
  }
  String? email;
  int? lead;
  String? name;
  String? nationality;
  String? phoneNumber;
  String? countryCode;
  bool? mainApplicant;
  double? annualIncome;
  String? dob;
  String? streetName;
  String? blockNumber;
  String? unitNumber;
  String? projectName;
  String? postalCode;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['email'] = email;
    map['lead'] = lead;
    map['name'] = name;
    map['nationality'] = nationality;
    map['phone_number'] = phoneNumber;
    map['country_code'] = countryCode;
    map['main_applicant'] = mainApplicant;
    map['annual_income'] = annualIncome;
    map['dob'] = dob;
/*    map['street_name'] = streetName;
    map['block_number'] = blockNumber;
    map['unit_number'] = unitNumber;
    map['project_name'] = projectName;
    map['postal_code'] = postalCode;*/
    return map;
  }
}
