// ignore_for_file: non_constant_identifier_names

class ChangePasswordSendModel {
  String? oldPassword;
  String? newPassword;
  String? confirmPassword;

  ChangePasswordSendModel({
    this.newPassword,
    this.oldPassword,
    this.confirmPassword,
  }); //this.deviceId, this.deviceType});

  ChangePasswordSendModel.fromJson(Map<String, dynamic> json) {
    oldPassword = json['old_password'];
    newPassword = json['new_password'];
    confirmPassword = json['confirm_password'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['old_password'] = oldPassword;
    data['new_password'] = newPassword;
    data['confirm_password'] = confirmPassword;
    return data;
  }
}
