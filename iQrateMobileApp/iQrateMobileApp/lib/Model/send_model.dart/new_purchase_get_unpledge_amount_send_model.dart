// ignore_for_file: non_constant_identifier_names

class NewPurchaseGetUnpledgeAmountSendModel {
  double? loanAmount;
  double? preferredfludgeAmount;
  double? totalMonthlyIncome;
  int? loanTenure;
  double? totalFinancialCommitments;
  int? propertyType;

  NewPurchaseGetUnpledgeAmountSendModel({
    this.loanAmount,
    this.preferredfludgeAmount,
    this.totalMonthlyIncome,
    this.loanTenure,
    this.totalFinancialCommitments,
    this.propertyType,
  });

  NewPurchaseGetUnpledgeAmountSendModel.fromJson(Map<String, dynamic> json) {
    loanAmount = json['loan_amount'];
    preferredfludgeAmount = json['preferred_fludge_amount'];
    totalMonthlyIncome = json['total_monthly_income'];
    loanTenure = json['loan_tenure'];
    totalFinancialCommitments = json['total_financial_commitments'];
    propertyType = json['property_type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['loan_amount'] = loanAmount;
    data['preferred_fludge_amount'] = preferredfludgeAmount;
    data['total_monthly_income'] = totalMonthlyIncome;
    data['loan_tenure'] = loanTenure;
    data['total_financial_commitments'] = totalFinancialCommitments;
    data['property_type'] = propertyType;
    return data;
  }
}
