class SubscriptionPreviewSendModel {
  String? priceId;

  SubscriptionPreviewSendModel({
    this.priceId,
  }); //this.deviceId, this.deviceType});

  SubscriptionPreviewSendModel.fromJson(Map<String, dynamic> json) {
    priceId = json['price_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['price_id'] = priceId;
    return data;
  }
}
