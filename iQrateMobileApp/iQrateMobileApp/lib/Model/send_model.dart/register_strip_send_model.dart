// ignore_for_file: non_constant_identifier_names

class RegisterStripeSendModel {
  String? name;
  String? email;
  String? user_id;
  String? phone;

  RegisterStripeSendModel({
    this.name,
    this.email,
    this.user_id,
    this.phone,
  });

  RegisterStripeSendModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    email = json['email'];
    user_id = json['user_id'];
    phone = json['phone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['email'] = email;
    data['user_id'] = user_id;
    data['phone'] = phone;
    return data;
  }
}
