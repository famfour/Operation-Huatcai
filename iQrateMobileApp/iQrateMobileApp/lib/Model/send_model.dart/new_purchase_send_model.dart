// ignore_for_file: non_constant_identifier_names

class NewPurchaseCalculatorSendModel {
  int? propertyPrice;
  int? loanAmount;
  String? dob;
  String? extendedLoanTenure;
  int? propertyType;
  int? noOfLoanApplicants;
  List? monthlyFixedIncome;
  List? totalHouseInstallment;
  List? totalCarInstallment;
  List? totalPersonalInstallment;
  List? totalCreditInstallment;
  List? totalPropertyInstallment;
  List? totalPropertyCompanyInstallment;
  List? annualIncome;
  List? monthlyRentalIncome;
  List? totalGuarantorInstallment;
  List? numberOfHousingLoan;
  List? numberOfOwnProperties;
  String? nationality;
  int? preferredLoanTenure;
  String? loanCategory;
  List? dobList;
  List? employemetTypeList;

  NewPurchaseCalculatorSendModel(
      {this.propertyType,
      this.loanAmount,
      this.dob,
      this.extendedLoanTenure,
      this.propertyPrice,
      this.noOfLoanApplicants,
      this.monthlyFixedIncome,
      this.totalHouseInstallment,
      this.totalCarInstallment,
      this.totalPersonalInstallment,
      this.totalCreditInstallment,
      this.totalPropertyInstallment,
      this.totalPropertyCompanyInstallment,
      this.annualIncome,
      this.monthlyRentalIncome,
      this.totalGuarantorInstallment,
      this.numberOfHousingLoan,
      this.numberOfOwnProperties,
      this.nationality,
      this.preferredLoanTenure,
      this.loanCategory,
      this.dobList,
      this.employemetTypeList});

  NewPurchaseCalculatorSendModel.fromJson(Map<String, dynamic> json) {
    propertyPrice = json['property_price'];
    loanAmount = json['loan_amount'];
    dob = json['dob'];
    extendedLoanTenure = json['extend_loan_tenure'];
    propertyType = json['property_type'];
    noOfLoanApplicants = json['no_of_loan_applicants'];
    monthlyFixedIncome = json['monthly_fixed_income'];
    totalHouseInstallment = json['total_house_installment'];
    totalCarInstallment = json['total_car_installment'];
    totalPersonalInstallment = json['total_personal_installment'];
    totalCreditInstallment = json['total_credit_installment'];
    totalPropertyInstallment = json['total_property_installment'];
    totalPropertyCompanyInstallment =
        json['total_property_company_installment'];
    annualIncome = json['annual_income'];
    monthlyRentalIncome = json['monthly_rental_income'];
    totalGuarantorInstallment = json['total_gurantor_installment'];
    numberOfHousingLoan = json['no_of_housing_loan'];
    numberOfOwnProperties = json['no_of_own_properties'];
    nationality = json['nationality'];
    preferredLoanTenure = json['prefered_loan_tenure'];
    loanCategory = json['loan_category'];
    dobList = json['dob_list'];
    employemetTypeList = json['employement_type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['property_price'] = propertyPrice;
    data['loan_amount'] = loanAmount;
    data['dob'] = dob;
    data['extend_loan_tenure'] = extendedLoanTenure;
    data['property_type'] = propertyType;
    data['no_of_loan_applicants'] = noOfLoanApplicants;
    data['monthly_fixed_income'] = monthlyFixedIncome;
    data['total_house_installment'] = totalHouseInstallment;
    data['total_car_installment'] = totalCarInstallment;
    data['total_personal_installment'] = totalPersonalInstallment;
    data['total_credit_installment'] = totalCreditInstallment;
    data['total_property_installment'] = totalPropertyInstallment;
    data['total_property_company_installment'] =
        totalPropertyCompanyInstallment;
    data['annual_income'] = annualIncome;
    data['monthly_rental_income'] = monthlyRentalIncome;
    data['total_gurantor_installment'] = totalGuarantorInstallment;
    data['no_of_housing_loan'] = numberOfHousingLoan;
    data['no_of_own_properties'] = numberOfOwnProperties;
    data['nationality'] = nationality;
    data['prefered_loan_tenure'] = preferredLoanTenure;
    data['loan_category'] = loanCategory;
    data['dob_list'] = dobList;
    data['employement_type'] = employemetTypeList;
    return data;
  }
}
