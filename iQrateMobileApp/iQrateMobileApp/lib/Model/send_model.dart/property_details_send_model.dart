class PropertyDetailsSendModel {
  PropertyDetailsSendModel({
    this.lead,
    this.postalCode,
    this.propertyStatus,
    this.propertyType,
    this.purchasePrice,
    this.streetName,
    this.unitNumber,
    this.projectName,
  });

  PropertyDetailsSendModel.fromJson(dynamic json) {
    lead = json['lead'];
    postalCode = json['postal_code'];
    propertyStatus = json['property_status'];
    propertyType = json['property_type'];
    purchasePrice = json['purchase_price'];
    streetName = json['street_name'];
    unitNumber = json['unit_number'];
    projectName = json['project_name'];
  }
  int? lead;
  String? postalCode;
  String? propertyStatus;
  String? propertyType;
  double? purchasePrice;
  String? streetName;
  String? unitNumber;
  String? projectName;
  PropertyDetailsSendModel copyWith({
    int? lead,
    String? postalCode,
    String? propertyStatus,
    String? propertyType,
    double? purchasePrice,
    String? streetName,
    String? unitNumber,
    String? projectName,
  }) =>
      PropertyDetailsSendModel(
        lead: lead ?? this.lead,
        postalCode: postalCode ?? this.postalCode,
        propertyStatus: propertyStatus ?? this.propertyStatus,
        propertyType: propertyType ?? this.propertyType,
        purchasePrice: purchasePrice ?? this.purchasePrice,
        streetName: streetName ?? this.streetName,
        unitNumber: unitNumber ?? this.unitNumber,
        projectName: projectName ?? this.projectName,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['lead'] = lead;
    map['postal_code'] = postalCode;
    map['property_status'] = propertyStatus;
    map['property_type'] = propertyType;
    map['purchase_price'] = purchasePrice;
    map['street_name'] = streetName;
    map['unit_number'] = unitNumber;
    map['project_name'] = projectName;
    return map;
  }
}
