class RefinanceTotalSendModel {
  RefinanceTotalSendModel({
    this.noOfApplicants,
    this.annualIncome,
    this.monthlyFixedIncome,
    this.monthlyRentalIncome,
    this.token,
    this.employementType,
    this.dob,
  });

  int? noOfApplicants;
  List? annualIncome;
  List? monthlyFixedIncome;
  List? monthlyRentalIncome;
  String? token;
  List? employementType;
  List? dob;

  RefinanceTotalSendModel.fromJson(dynamic json) {
    noOfApplicants = json['no_of_loan_applicants'];
    annualIncome = json['annual_income'];
    monthlyFixedIncome = json['monthly_fixed_income'];
    monthlyRentalIncome = json['monthly_rental_income'];
    token = json['token'];
    employementType = json['employement_type'];
    dob = json['dob_list'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['no_of_loan_applicants'] = noOfApplicants;
    data['annual_income'] = annualIncome;
    data['monthly_fixed_income'] = monthlyFixedIncome;
    data['monthly_rental_income'] = monthlyRentalIncome;
    data['token'] = token;
    data['employement_type'] = employementType;
    data['dob_list'] = dob;
    return data;
  }
}
