// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

class TwoFAuserSendModel {
  TwoFAuserSendModel({
    this.id,
    this.fullName,
    this.email,
    this.countryCode,
    this.mobile,
    this.dob,
    this.agreedTerm,
  });

  String? id;
  String? fullName;
  String? email;
  String? countryCode;
  String? mobile;
  DateTime? dob;
  int? agreedTerm;

  factory TwoFAuserSendModel.fromJson(Map<String, dynamic> json) =>
      TwoFAuserSendModel(
        id: json["id"],
        fullName: json["full_name"],
        email: json["email"],
        countryCode: json["country_code"],
        mobile: json["mobile"],
        dob: DateTime.parse(json["dob"]),
        agreedTerm: json["agreed_term"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "full_name": fullName,
        "email": email,
        "country_code": countryCode,
        "mobile": mobile,
        "dob":
            "${dob!.year.toString().padLeft(4, '0')}-${dob!.month.toString().padLeft(2, '0')}-${dob!.day.toString().padLeft(2, '0')}",
        "agreed_term": agreedTerm,
      };
}
