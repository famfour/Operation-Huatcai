// To parse this JSON data, do
//
//     final signUpSendModel = signUpSendModelFromJson(jsonString);

// ignore_for_file: non_constant_identifier_names

import 'dart:convert';

SignUpSendModel signUpSendModelFromJson(String str) =>
    SignUpSendModel.fromJson(json.decode(str));

String signUpSendModelToJson(SignUpSendModel data) =>
    json.encode(data.toJson());

class SignUpSendModel {
  SignUpSendModel(
      {this.full_name,
      this.password,
      this.email,
      this.mobile,
      this.dob,
      this.agreed_term,
      this.country_code_label,
      this.country_code,
      this. referral_code
      });

  String? full_name;
  String? password;
  String? email;
  String? mobile;
  String? dob;
  int? agreed_term;
  String? country_code;
  String? country_code_label;
  //referal code added
  String? referral_code;

  factory SignUpSendModel.fromJson(Map<String, dynamic> json) =>
      SignUpSendModel(
        full_name: json["full_name"],
        password: json["password"],
        email: json["email"],
        mobile: json["mobile"],
        dob: json["dob"],
        agreed_term: json["agreed_term"],
        country_code: json["country_code"],
        country_code_label: json["country_code_label"],
        referral_code: json["referal_code"],
      );

  Map<String, dynamic> toJson() => {
        "full_name": full_name,
        "password": password,
        "email": email,
        "mobile": mobile,
        "dob": dob,
        "agreed_term": agreed_term,
        "country_code": country_code,
        "country_code_label": country_code_label,
        "referal_code":  referral_code,
      };
}
