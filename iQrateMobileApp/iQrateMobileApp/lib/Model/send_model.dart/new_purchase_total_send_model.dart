// ignore_for_file: non_constant_identifier_names

class NewPurchaseTotalSendModel {
  int? numberOfLoanApplicants;
  List? totalPropertyInstallment;
  List? totalGurantorInstallment;
  List? totalPropertyCompanyInstallment;
  List? totalHouseInstallment;
  List? totalPersonalInstallment;
  List? totalCreditInstallment;
  List? annualIncome;
  List? monthlyFixedIncome;
  List? monthlyRentalIncome;
  List? totalCarInstallment;
  List? employementType;
  String? token;

  NewPurchaseTotalSendModel({
    this.numberOfLoanApplicants,
    this.totalPropertyInstallment,
    this.totalGurantorInstallment,
    this.totalPropertyCompanyInstallment,
    this.totalHouseInstallment,
    this.totalPersonalInstallment,
    this.totalCreditInstallment,
    this.annualIncome,
    this.monthlyFixedIncome,
    this.monthlyRentalIncome,
    this.totalCarInstallment,
    this.employementType,
    this.token,
  });

  NewPurchaseTotalSendModel.fromJson(Map<String, dynamic> json) {
    numberOfLoanApplicants = json['no_of_loan_applicants'];
    totalPropertyInstallment = json['total_property_installment'];
    totalGurantorInstallment = json['total_gurantor_installment'];
    totalPropertyCompanyInstallment =
        json['total_property_company_installment'];
    totalHouseInstallment = json['total_house_installment'];
    totalPersonalInstallment = json['total_personal_installment'];
    totalCreditInstallment = json['total_credit_installment'];
    annualIncome = json['annual_income'];
    monthlyFixedIncome = json['monthly_fixed_income'];
    monthlyRentalIncome = json['monthly_rental_income'];
    totalCarInstallment = json['total_car_installment'];
    employementType = json['emplyement_type'];
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['no_of_loan_applicants'] = numberOfLoanApplicants;
    data['total_property_installment'] = totalPropertyInstallment;
    data['total_gurantor_installment'] = totalGurantorInstallment;
    data['total_property_company_installment'] =
        totalPropertyCompanyInstallment;
    data['total_house_installment'] = totalHouseInstallment;
    data['total_personal_installment'] = totalPersonalInstallment;
    data['total_credit_installment'] = totalCreditInstallment;
    data['annual_income'] = annualIncome;
    data['monthly_fixed_income'] = monthlyFixedIncome;
    data['monthly_rental_income'] = monthlyRentalIncome;
    data['total_car_installment'] = totalCarInstallment;
    data['employement_type'] = employementType;
    data['token'] = token;
    return data;
  }
}
