import 'package:iqrate/Model/response_model.dart/safe_convert.dart';
import 'safe_convert.dart';

class SendCreatePayoutV2 {
  final int lead;
  final int bank;
  final int banker;
  final int lawFirm;
  final List<LoansItem> loans;
  final String loanAcceptanceDate;
  final String loReferenceNumber;
  final double bankReferralFee;
  final String ownLawFirmName;
  final double legalFee;

  SendCreatePayoutV2({
    this.lead = 0,
    this.bank = 0,
    this.banker = 0,
    this.lawFirm = 0,
    required this.loans,
    this.loanAcceptanceDate = "",
    this.loReferenceNumber = "",
    this.bankReferralFee = 0,
    this.ownLawFirmName = "",
    this.legalFee = 0,
  });

  factory SendCreatePayoutV2.fromJson(Map<String, dynamic>? json) =>
      SendCreatePayoutV2(
        lead: SafeManager.parseInt(json!, 'lead'),
        bank: asInt(json, 'bank'),
        banker: asInt(json, 'banker'),
        lawFirm: asInt(json, 'law_firm'),
        loans: asList(json, 'loans').map((e) => LoansItem.fromJson(e)).toList(),
        loanAcceptanceDate: asString(json, 'loan_acceptance_date'),
        loReferenceNumber: asString(json, 'lo_reference_number'),
        bankReferralFee: asDouble(json, 'bank_referral_fee'),
        ownLawFirmName: asString(json, 'own_law_firm_name'),
        legalFee: asDouble(json, 'legal_fee'),
      );

  Map<String, dynamic> toJson() => {
        'lead': lead,
        'bank': bank,
        'banker': banker,
        'law_firm_id': lawFirm,
        'loans': loans.map((e) => e.toJson()).toList(),
        'loan_acceptance_date': loanAcceptanceDate,
        'lo_reference_number': loReferenceNumber,
        'bank_referral_fee': bankReferralFee,
        'own_law_firm_name': ownLawFirmName,
        'legal_fee': legalFee,
      };
}

class LoansItem {
  final String rateType;
  final String lockInPeriod;
  final List<RatesItem> rates;
  final double loanAmount;
  final int clawbackPeriod;
  final double cashRebateSubsidy;
  final String nextReviewDate;

  LoansItem({
    this.rateType = "",
    this.lockInPeriod = "",
    required this.rates,
    this.loanAmount = 0,
    this.clawbackPeriod = 0,
    this.cashRebateSubsidy = 0,
    this.nextReviewDate = "",
  });

  factory LoansItem.fromJson(Map<String, dynamic>? json) => LoansItem(
        rateType: asString(json, 'rate_type'),
        lockInPeriod: asString(json, 'lock_in_period'),
        rates: asList(json, 'rates').map((e) => RatesItem.fromJson(e)).toList(),
        loanAmount: asDouble(json, 'loan_amount'),
        clawbackPeriod: asInt(json, 'clawback_period'),
        cashRebateSubsidy: asDouble(json, 'cash_rebate_subsidy'),
        nextReviewDate: asString(json, 'next_review_date'),
      );

  Map<String, dynamic> toJson() => {
        'rate_type': rateType,
        'lock_in_period': lockInPeriod,
        'rates': rates.map((e) => e.toJson()).toList(),
        'loan_amount': loanAmount,
        'clawback_period': clawbackPeriod,
        'cash_rebate_subsidy': cashRebateSubsidy,
        'next_review_date': nextReviewDate,
      };
}

class RatesItem {
  final String year;
  final int bankSpread;
  final int referenceRate;

  RatesItem({
    this.year = "",
    this.bankSpread = 0,
    this.referenceRate = 0,
  });

  factory RatesItem.fromJson(Map<String, dynamic>? json) => RatesItem(
        year: asString(json, 'year'),
        bankSpread: asInt(json, 'bank_spread'),
        referenceRate: asInt(json, 'reference_rate'),
      );

  Map<String, dynamic> toJson() => {
        'year': year,
        'bank_spread': bankSpread,
        'reference_rate': referenceRate,
      };
}
