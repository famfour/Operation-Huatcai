class PdpaValidateSendModel {
  PdpaValidateSendModel({
    this.token,
  });

  PdpaValidateSendModel.fromJson(dynamic json) {
    token = json['token'];
  }
  String? token;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['token'] = token;
    return map;
  }
}
