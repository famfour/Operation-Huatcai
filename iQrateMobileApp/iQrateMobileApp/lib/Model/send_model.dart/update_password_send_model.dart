// ignore_for_file: non_constant_identifier_names

class UpdatePasswordSendModel {
  String? password;
  String? otp;

  UpdatePasswordSendModel({
    this.password,
    this.otp,
  }); //this.deviceId, this.deviceType});

  UpdatePasswordSendModel.fromJson(Map<String, dynamic> json) {
    password = json['password'];
    otp = json['otp'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['password'] = password;
    data['otp'] = otp;
    return data;
  }
}
