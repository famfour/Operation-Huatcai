// ignore_for_file: file_names

class SendLawFirmSubmit {
  SendLawFirmSubmit({
    this.lawFirm,
    this.ownLawFirmName,
    this.ownLawFirmEmail,
    this.remarks,
    this.documentIds,
  });

  SendLawFirmSubmit.fromJson(dynamic json) {
    lawFirm = json['law_firm'];
    ownLawFirmName = json['own_law_firm_name'];
    ownLawFirmEmail = json['own_law_firm_email'];
    remarks = json['remarks'];
    documentIds =
        json['document_ids'] != null ? json['document_ids'].cast<String>() : [];
  }
  int? lawFirm;
  String? ownLawFirmName;
  String? ownLawFirmEmail;
  String? remarks;
  List<String>? documentIds;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['law_firm'] = lawFirm;
    map['own_law_firm_name'] = ownLawFirmName;
    map['own_law_firm_email'] = ownLawFirmEmail;
    map['remarks'] = remarks;
    map['document_ids'] = documentIds;
    return map;
  }
}
