class UpgradeDowngradeSendModel {
  String? priceId;

  UpgradeDowngradeSendModel({
    this.priceId,
  });

  UpgradeDowngradeSendModel.fromJson(Map<String, dynamic> json) {
    priceId = json['price_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['price_id'] = priceId;
    return data;
  }
}
