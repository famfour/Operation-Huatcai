class RefinanceReportPdfSendModel {
  int? noOfApplicants;
  String? name;
  int? outstandingLoanAmount;
  int? outstandinLoanTenure;
  int? preferredLoanTenure;
  String? existingBank;
  double? existingInterestRate;
  int? propertyType;
  int? yearOfPurchase;
  int? earlyRepaymentPenaltyAmount;
  int? cancellationPenaltyAmount;
  int? existingLoanSubsidy;
  String? dob;
  String? loanCategory;
  List? bankDetails;
  List? exportFields;

  RefinanceReportPdfSendModel({
    this.noOfApplicants,
    this.outstandingLoanAmount,
    this.outstandinLoanTenure,
    this.preferredLoanTenure,
    this.existingBank,
    this.existingInterestRate,
    this.propertyType,
    this.yearOfPurchase,
    this.earlyRepaymentPenaltyAmount,
    this.cancellationPenaltyAmount,
    this.existingLoanSubsidy,
    this.dob,
    this.loanCategory,
    this.bankDetails,
    this.exportFields,
  });

  RefinanceReportPdfSendModel.fromJson(dynamic json) {
    noOfApplicants = json['no_of_loan_applicants'];
    outstandingLoanAmount = json['outstanding_loan_amount'];
    outstandinLoanTenure = json['outstanding_loan_tenure'];
    preferredLoanTenure = json['preferred_loan_tenure'];
    existingBank = json['existing_bank'];
    existingInterestRate = json['existing_interest_rate'];
    propertyType = json['property_type'];
    yearOfPurchase = json['year_of_purchase'];
    earlyRepaymentPenaltyAmount = json['early_repayment_penalty_amount'];
    cancellationPenaltyAmount = json['cancellation_penalty_amount'];
    existingLoanSubsidy = json['existing_loan_subsidy'];
    dob = json['dob'];
    loanCategory = json['loan_category'];
    bankDetails = json['bank_details'];
    exportFields = json['export_fields'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['no_of_applicants'] = noOfApplicants;
    data['outstanding_loan_amount'] = outstandingLoanAmount;
    data['outstanding_loan_tenure'] = outstandinLoanTenure;
    data['preferred_loan_tenure'] = preferredLoanTenure;
    data['existing_bank'] = existingBank;
    data['existing_interest_rate'] = existingInterestRate;
    data['property_type'] = propertyType;
    data['year_of_purchase'] = yearOfPurchase;
    data['early_repayment_penalty_amount'] = earlyRepaymentPenaltyAmount;
    data['cancellation_penalty_amount'] = cancellationPenaltyAmount;
    data['existing_loan_subsidy'] = existingLoanSubsidy;
    data['dob'] = dob;
    data['loan_category'] = loanCategory;
    data['bank_details'] = bankDetails;
    data['export_fields'] = exportFields;
    return data;
  }
}
