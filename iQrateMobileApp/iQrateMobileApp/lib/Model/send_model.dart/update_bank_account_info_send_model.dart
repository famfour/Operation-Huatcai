class UpdateBankAccountInformationSendModel {
  String? bankName;
  String? accountNumber;

  UpdateBankAccountInformationSendModel({this.bankName, this.accountNumber});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['bank_name'] = bankName;
    data['bank_ac_number'] = accountNumber;
    return data;
  }
}
