// ignore_for_file: non_constant_identifier_names

class ContactUsSendModel {
  String? name;
  String? email;
  String? message;

  ContactUsSendModel({
    this.name,
    this.email,
    this.message,
  }); //this.deviceId, this.deviceType});

  ContactUsSendModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    email = json['email'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['email'] = email;
    data['message'] = message;
    return data;
  }
}
