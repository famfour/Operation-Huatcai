class TwoFASendModel {
  TwoFASendModel({
    String? twoFactorAuthenticationCode,
    String? user,
  }) {
    _twoFactorAuthenticationCode = twoFactorAuthenticationCode;
    _user = user;
  }

  TwoFASendModel.fromJson(dynamic json) {
    _twoFactorAuthenticationCode = json['twoFactorAuthenticationCode'];
    _user = json['user'];
  }
  String? _twoFactorAuthenticationCode;
  String? _user;

  String? get twoFactorAuthenticationCode => _twoFactorAuthenticationCode;
  String? get user => _user;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['twoFactorAuthenticationCode'] = _twoFactorAuthenticationCode;
    map['user'] = _user;
    return map;
  }
}
