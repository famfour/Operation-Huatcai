// ignore_for_file: non_constant_identifier_names

class UpdateProfileSendModel {
  String? name;
  String? dob;
  String? mobile;
  String? country_code;
  UpdateProfileSendModel({this.name, this.dob, this.country_code, this.mobile});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['country_code'] = country_code;
    data['mobile'] = mobile;
    data['full_name'] = name;
    data['dob'] = dob;
    return data;
  }
}
