// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

// ignore_for_file: constant_identifier_names

import 'dart:convert';

AllPackagesResponseModel welcomeFromJson(String str) =>
    AllPackagesResponseModel.fromJson(json.decode(str));

String welcomeToJson(AllPackagesResponseModel data) =>
    json.encode(data.toJson());

class AllPackagesResponseModel {
  AllPackagesResponseModel({
    this.count,
    this.next,
    this.previous,
    this.results,
  });

  int? count;
  String? next;
  dynamic previous;
  List<Result>? results;

  factory AllPackagesResponseModel.fromJson(Map<String, dynamic> json) =>
      AllPackagesResponseModel(
        count: json["count"],
        next: json["next"],
        previous: json["previous"],
        results:
            List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "count": count,
        "next": next,
        "previous": previous,
        "results": List<dynamic>.from(results!.map((x) => x.toJson())),
      };
}

class Result {
  Result({
    this.id,
    this.bank,
    this.mask,
    this.rateCategory,
    this.rateType,
    this.propertyTypes,
    this.propertyStatus,
    this.loanCategory,
    this.lockInPeriod,
    this.minLoanAmount,
    this.depositToPlace,
    this.valuationSubsidy,
    this.fireInsuranceSubsidy,
    this.cashRebateLegalSubsidy,
    this.cashRebateSubsidyClawbackPeriodYears,
    this.partialRepaymentPenalty,
    this.partialRepaymentPenaltyRemarks,
    this.fullRepaymentPenalty,
    this.fullRepaymentPenaltyRemarks,
    this.rates,
    this.cancellationFee,
    this.depositToPlaceRemarks,
    this.remarksForClient,
    this.interestOffsetting,
    this.interestResetDate,
    this.processingFee,
    this.remarksForBroker,
    this.newPurchaseReferralFee,
    this.refinanceReferralFee,
    this.publish,
    this.createdBy,
    this.updatedBy,
    this.created,
    this.updated,
  });

  int? id;
  Bank? bank;
  bool? mask;
  RateCategory? rateCategory;
  String? rateType;
  List<PropertyType>? propertyTypes;
  List<PropertyStatus>? propertyStatus;
  List<LoanCategory>? loanCategory;
  int? lockInPeriod;
  int? minLoanAmount;
  int? depositToPlace;
  List<DepositToPlaceRemarks>? valuationSubsidy;
  DepositToPlaceRemarks? fireInsuranceSubsidy;
  List<DepositToPlaceRemarks>? cashRebateLegalSubsidy;
  int? cashRebateSubsidyClawbackPeriodYears;
  double? partialRepaymentPenalty;
  DepositToPlaceRemarks? partialRepaymentPenaltyRemarks;
  double? fullRepaymentPenalty;
  DepositToPlaceRemarks? fullRepaymentPenaltyRemarks;
  List<Rate>? rates;
  CancellationFee? cancellationFee;
  DepositToPlaceRemarks? depositToPlaceRemarks;
  List<DepositToPlaceRemarks>? remarksForClient;
  DepositToPlaceRemarks? interestOffsetting;
  bool? interestResetDate;
  bool? processingFee;
  dynamic remarksForBroker;
  double? newPurchaseReferralFee;
  double? refinanceReferralFee;
  bool? publish;
  dynamic createdBy;
  dynamic updatedBy;
  DateTime? created;
  DateTime? updated;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        bank: Bank.fromJson(json["bank"]),
        mask: json["mask"],
        rateCategory: rateCategoryValues.map![json["rate_category"]],
        rateType: json["rate_type"],
        propertyTypes: List<PropertyType>.from(
            json["property_types"].map((x) => propertyTypeValues.map![x])),
        propertyStatus: List<PropertyStatus>.from(
            json["property_status"].map((x) => propertyStatusValues.map![x])),
        loanCategory: List<LoanCategory>.from(
            json["loan_category"].map((x) => loanCategoryValues.map![x])),
        lockInPeriod: json["lock_in_period"],
        minLoanAmount: json["min_loan_amount"],
        depositToPlace: json["deposit_to_place"],
        valuationSubsidy: List<DepositToPlaceRemarks>.from(
            json["valuation_subsidy"]
                .map((x) => depositToPlaceRemarksValues.map![x])),
        fireInsuranceSubsidy:
            depositToPlaceRemarksValues.map![json["fire_insurance_subsidy"]],
        cashRebateLegalSubsidy: List<DepositToPlaceRemarks>.from(
            json["cash_rebate_legal_subsidy"]
                .map((x) => depositToPlaceRemarksValues.map![x])),
        cashRebateSubsidyClawbackPeriodYears:
            json["cash_rebate_subsidy_clawback_period_years"],
        partialRepaymentPenalty: json["partial_repayment_penalty"].toDouble(),
        partialRepaymentPenaltyRemarks: depositToPlaceRemarksValues
            .map![json["partial_repayment_penalty_remarks"]],
        fullRepaymentPenalty: json["full_repayment_penalty"].toDouble(),
        fullRepaymentPenaltyRemarks: depositToPlaceRemarksValues
            .map![json["full_repayment_penalty_remarks"]],
        rates: List<Rate>.from(json["rates"].map((x) => Rate.fromJson(x))),
        cancellationFee: cancellationFeeValues.map![json["cancellation_fee"]],
        depositToPlaceRemarks:
            depositToPlaceRemarksValues.map![json["deposit_to_place_remarks"]],
        remarksForClient: List<DepositToPlaceRemarks>.from(
            json["remarks_for_client"]
                .map((x) => depositToPlaceRemarksValues.map![x])),
        interestOffsetting:
            depositToPlaceRemarksValues.map![json["interest_offsetting"]],
        interestResetDate: json["interest_reset_date"],
        processingFee: json["processing_fee"],
        remarksForBroker: json["remarks_for_broker"],
        newPurchaseReferralFee: json["new_purchase_referral_fee"].toDouble(),
        refinanceReferralFee: json["refinance_referral_fee"].toDouble(),
        publish: json["publish"],
        createdBy: json["created_by"],
        updatedBy: json["updated_by"],
        created: DateTime.parse(json["created"]),
        updated: DateTime.parse(json["updated"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "bank": bank!.toJson(),
        "mask": mask,
        "rate_category": rateCategoryValues.reverse[rateCategory],
        "rate_type": rateType,
        "property_types": List<dynamic>.from(
            propertyTypes!.map((x) => propertyTypeValues.reverse[x])),
        "property_status": List<dynamic>.from(
            propertyStatus!.map((x) => propertyStatusValues.reverse[x])),
        "loan_category": List<dynamic>.from(
            loanCategory!.map((x) => loanCategoryValues.reverse[x])),
        "lock_in_period": lockInPeriod,
        "min_loan_amount": minLoanAmount,
        "deposit_to_place": depositToPlace,
        "valuation_subsidy": List<dynamic>.from(valuationSubsidy!
            .map((x) => depositToPlaceRemarksValues.reverse[x])),
        "fire_insurance_subsidy":
            depositToPlaceRemarksValues.reverse[fireInsuranceSubsidy],
        "cash_rebate_legal_subsidy": List<dynamic>.from(cashRebateLegalSubsidy!
            .map((x) => depositToPlaceRemarksValues.reverse[x])),
        "cash_rebate_subsidy_clawback_period_years":
            cashRebateSubsidyClawbackPeriodYears,
        "partial_repayment_penalty": partialRepaymentPenalty,
        "partial_repayment_penalty_remarks":
            depositToPlaceRemarksValues.reverse[partialRepaymentPenaltyRemarks],
        "full_repayment_penalty": fullRepaymentPenalty,
        "full_repayment_penalty_remarks":
            depositToPlaceRemarksValues.reverse[fullRepaymentPenaltyRemarks],
        "rates": List<dynamic>.from(rates!.map((x) => x.toJson())),
        "cancellation_fee": cancellationFeeValues.reverse[cancellationFee],
        "deposit_to_place_remarks":
            depositToPlaceRemarksValues.reverse[depositToPlaceRemarks],
        "remarks_for_client": List<dynamic>.from(remarksForClient!
            .map((x) => depositToPlaceRemarksValues.reverse[x])),
        "interest_offsetting":
            depositToPlaceRemarksValues.reverse[interestOffsetting],
        "interest_reset_date": interestResetDate,
        "processing_fee": processingFee,
        "remarks_for_broker": remarksForBroker,
        "new_purchase_referral_fee": newPurchaseReferralFee,
        "refinance_referral_fee": refinanceReferralFee,
        "publish": publish,
        "created_by": createdBy,
        "updated_by": updatedBy,
        "created": created!.toIso8601String(),
        "updated": updated!.toIso8601String(),
      };
}

class Bank {
  Bank({
    this.id,
    this.name,
    this.nameMasked,
    this.status,
    this.logo,
  });

  int? id;
  String? name;
  String? nameMasked;
  bool? status;
  String? logo;

  factory Bank.fromJson(Map<String, dynamic> json) => Bank(
        id: json["id"],
        name: json["name"],
        nameMasked: json["name_masked"],
        status: json["status"],
        logo: json["logo"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "name_masked": nameMasked,
        "status": status,
        "logo": logo,
      };
}

enum CancellationFee { THE_075 }

final cancellationFeeValues = EnumValues({"0.75%": CancellationFee.THE_075});

enum DepositToPlaceRemarks { NA }

final depositToPlaceRemarksValues =
    EnumValues({"NA": DepositToPlaceRemarks.NA});

enum LoanCategory {
  NEW_PURCHASE_WITH_OPTION_TO_PURCHASE,
  NEW_PURCHASE_APPROVAL_IN_PRINCIPLE,
  REFINANCE_CASH_OUT_TERM_LOAN,
  PART_PURCHASE_DECOUPLING_DIVORCE
}

final loanCategoryValues = EnumValues({
  "new_purchase_approval_in_principle":
      LoanCategory.NEW_PURCHASE_APPROVAL_IN_PRINCIPLE,
  "new_purchase_with_option_to_purchase":
      LoanCategory.NEW_PURCHASE_WITH_OPTION_TO_PURCHASE,
  "part_purchase_decoupling_divorce":
      LoanCategory.PART_PURCHASE_DECOUPLING_DIVORCE,
  "refinance_cash_out_term_loan": LoanCategory.REFINANCE_CASH_OUT_TERM_LOAN
});

enum PropertyStatus {
  COMPLETED,
  UNDER_CONSTRUCTION_TO_OBTAIN_TOP_WITHIN_2_YEARS,
  UNDER_CONSTRUCTION_TO_OBTAIN_TOP_MORE_THAN_2_YEARS
}

final propertyStatusValues = EnumValues({
  "completed": PropertyStatus.COMPLETED,
  "under_construction_to_obtain_top_more_than_2_years":
      PropertyStatus.UNDER_CONSTRUCTION_TO_OBTAIN_TOP_MORE_THAN_2_YEARS,
  "under_construction_to_obtain_top_within_2_years":
      PropertyStatus.UNDER_CONSTRUCTION_TO_OBTAIN_TOP_WITHIN_2_YEARS
});

enum PropertyType {
  HDB,
  CONDOMINIUM_APARTMENT,
  STRATA_HOUSING_TOWNHOUSES,
  EC_RESALE_OUT_OF_MOP,
  EC_FROM_DEVELOPER_WITHIN_MOP,
  LANDED
}

final propertyTypeValues = EnumValues({
  "condominium_apartment": PropertyType.CONDOMINIUM_APARTMENT,
  "ec_from_developer_within_mop": PropertyType.EC_FROM_DEVELOPER_WITHIN_MOP,
  "ec_resale_out_of_mop": PropertyType.EC_RESALE_OUT_OF_MOP,
  "hdb": PropertyType.HDB,
  "landed": PropertyType.LANDED,
  "strata_housing_townhouses": PropertyType.STRATA_HOUSING_TOWNHOUSES
});

enum RateCategory { EXCLUSIVE, STANDARD }

final rateCategoryValues = EnumValues(
    {"exclusive": RateCategory.EXCLUSIVE, "standard": RateCategory.STANDARD});

class Rate {
  Rate({
    this.year,
    this.referenceRate,
    this.bankSpread,
  });

  Year? year;
  int? referenceRate;
  double? bankSpread;

  factory Rate.fromJson(Map<String, dynamic> json) => Rate(
        year: yearValues.map![json["year"]],
        referenceRate: json["reference_rate"],
        bankSpread: json["bank_spread"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "year": yearValues.reverse[year],
        "reference_rate": referenceRate,
        "bank_spread": bankSpread,
      };
}

enum Year { THEREAFTER, YEAR_5, YEAR_4, YEAR_3, YEAR_2, YEAR_1 }

final yearValues = EnumValues({
  "thereafter": Year.THEREAFTER,
  "year_1": Year.YEAR_1,
  "year_2": Year.YEAR_2,
  "year_3": Year.YEAR_3,
  "year_4": Year.YEAR_4,
  "year_5": Year.YEAR_5
});

class EnumValues<T> {
  Map<String, T>? map;
  Map<T, String>? reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    reverseMap ??= map!.map((k, v) => MapEntry(v, k));
    return reverseMap!;
  }
}
