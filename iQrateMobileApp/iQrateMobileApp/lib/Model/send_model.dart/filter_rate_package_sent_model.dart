class FilterRatePackageSentModel {
  String? loanAmount;
  int? pageSize;
  List<String>? bankId;
  bool? has200k = true;
  List<String>? loanCategory;
  int? page;
  List<String>? propertyStatus;
  List<String>? propertyTypes;
  List<String>? rateCategory;
  List<String>? rateType;
  bool? sellPropertyIn3Years;

  FilterRatePackageSentModel(
      {this.loanAmount,
      this.bankId,
      this.has200k,
      this.loanCategory,
      this.page,
      this.pageSize,
      this.propertyStatus,
      this.propertyTypes,
      this.rateCategory,
      this.rateType,
      this.sellPropertyIn3Years});

  FilterRatePackageSentModel.fromJson(Map<String, dynamic> json) {
    loanAmount = json['loan_amount'];
    bankId = json['bank_id'].cast<int>();
    has200k = json['has_200k'];
    loanCategory = json['loan_category'].cast<String>();
    page = json['page'];
    pageSize = json['page_size'];
    propertyStatus = json['property_status'].cast<String>();
    propertyTypes = json['property_types'].cast<String>();
    rateCategory = json['rate_category'].cast<String>();
    rateType = json['rate_type'].cast<String>();
    sellPropertyIn3Years = json['sell_property_in_3_years'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    // if (loanAmount != null)  data['loan_amount'] = loanAmount;
    if (loanAmount != null && loanAmount != "") {
      data['loan_amount'] = convertLoanAmountToDoubleString();
    }
    if (bankId != null && bankId!.isNotEmpty) data['bank_id'] = bankId;
    if (has200k != null) data['has_200k'] = has200k.toString();
    if (loanCategory != null)  data['loan_category'] = loanCategory;
    if (page != null) data['page'] = page;
    if (pageSize != null) data['page_size'] = pageSize;
    if (propertyStatus != null) data['property_status'] = propertyStatus;

    if (propertyTypes != null && propertyTypes!.isNotEmpty) {
      data['property_types'] = propertyTypes;
    }
    if (rateCategory != null) data['rate_category'] = rateCategory;
    if (rateType != null) data['rate_type'] = rateType;
    if (sellPropertyIn3Years != null) {
      data['sell_property_in_3_years'] = sellPropertyIn3Years.toString();
    }
    return data;
  }

  convertLoanAmountToDoubleString() {
    var temp = loanAmount!.replaceAll(",", "");
    return temp.toString();
  }
}
