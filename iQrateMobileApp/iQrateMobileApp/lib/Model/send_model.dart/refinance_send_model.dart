class RefinanceSendModel {
  RefinanceSendModel({
    this.noOfApplicants,
    this.outstandingLoanAmount,
    this.earlyRepaymentPenalty,
    this.undisbursedLoanAmount,
    this.cancellationPenalty,
  });

  int? noOfApplicants;
  int? outstandingLoanAmount;
  double? earlyRepaymentPenalty;
  int? undisbursedLoanAmount;
  double? cancellationPenalty;

  RefinanceSendModel.fromJson(dynamic json) {
    noOfApplicants = json['no_of_loan_applicants'];
    outstandingLoanAmount = json['outstanding_loan_amount'];
    earlyRepaymentPenalty = json['early_repayment_penalty'];
    undisbursedLoanAmount = json['undisbursed_loan_amount'];
    cancellationPenalty = json['cancellation_penalty'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['no_of_loan_applicants'] = noOfApplicants;
    data['outstanding_loan_amount'] = outstandingLoanAmount;
    data['early_repayment_penalty'] = earlyRepaymentPenalty;
    data['undisbursed_loan_amount'] = undisbursedLoanAmount;
    data['cancellation_penalty'] = cancellationPenalty;
    return data;
  }
}
