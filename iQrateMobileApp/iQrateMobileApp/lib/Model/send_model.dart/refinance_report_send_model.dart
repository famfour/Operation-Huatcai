import 'package:iqrate/Model/send_model.dart/bank_package_model_refinance.dart';

class RefinanceReportSendModel {
  int? noOfApplicants;
  String? name;
  int? outstandingLoanAmount;
  int? outstandinLoanTenure;
  int? preferredLoanTenure;
  String? existingBank;
  double? existingInterestRate;
  int? propertyType;
  int? yearOfPurchase;
  dynamic earlyRepaymentPenaltyAmount;
  dynamic cancellationPenaltyAmount;
  int? existingLoanSubsidy;
  String? dob;
  String? loanCategory;
  List<BankPackageModel>? bankDetails;
  String? token;
  List? exportFields;
  dynamic cashRebateSubsidy;
  dynamic approximatePropertyPrice;
  String? propertyStatus;

  RefinanceReportSendModel(
      {this.noOfApplicants,
      this.name,
      this.outstandingLoanAmount,
      this.outstandinLoanTenure,
      this.preferredLoanTenure,
      this.existingBank,
      this.existingInterestRate,
      this.propertyType,
      this.yearOfPurchase,
      this.earlyRepaymentPenaltyAmount,
      this.cancellationPenaltyAmount,
      this.existingLoanSubsidy,
      this.dob,
      this.loanCategory,
      this.bankDetails,
      this.token,
      this.exportFields,
      this.cashRebateSubsidy,
      this.approximatePropertyPrice,
      this.propertyStatus});

  RefinanceReportSendModel.fromJson(dynamic json) {
    noOfApplicants = json['no_of_loan_applicants'];
    name = json['name'];
    outstandingLoanAmount = json['outstanding_loan_amount'];
    outstandinLoanTenure = json['outstanding_loan_tenure'];
    preferredLoanTenure = json['prefered_loan_tenure'];
    existingBank = json['existing_bank'];
    existingInterestRate = json['existing_interest_rate'];
    propertyType = json['property_type'];
    yearOfPurchase = json['year_of_purchase'];
    earlyRepaymentPenaltyAmount = json['early_repayment_penalty_amount'];
    cancellationPenaltyAmount = json['cancellation_penalty_amount'];
    existingLoanSubsidy = json['existing_loan_subsidy'];
    dob = json['dob'];
    loanCategory = json['loan_category'];
    bankDetails = List<BankPackageModel>.from(
        json["bank_details"].map((x) => BankPackageModel.fromJson(x)));
    token = json['token'];
    exportFields = json['export_fields'];
    cashRebateSubsidy = json['cash_rebate_subsidy'];
    approximatePropertyPrice = json['approximate_property_price'];
    propertyStatus = json['property_status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['no_of_loan_applicants'] = noOfApplicants;
    data['name'] = name;
    data['outstanding_loan_amount'] = outstandingLoanAmount;
    data['outstanding_loan_tenure'] = outstandinLoanTenure;
    data['prefered_loan_tenure'] = preferredLoanTenure;
    data['existing_bank'] = existingBank;
    data['existing_interest_rate'] = existingInterestRate;
    data['property_type'] = propertyType;
    data['year_of_purchase'] = yearOfPurchase;
    data['early_repayment_penalty_amount'] = earlyRepaymentPenaltyAmount;
    data['cancellation_penalty_amount'] = cancellationPenaltyAmount;
    data['existing_loan_subsidy'] = existingLoanSubsidy;
    data['dob'] = dob;
    data['loan_category'] = loanCategory;
    data['bank_details'] =
        List<dynamic>.from(bankDetails!.map((x) => x.toJson()));
    data['token'] = token;
    data['export_fields'] = exportFields;
    data['cash_rebate_subsidy'] = cashRebateSubsidy;
    data['approximate_property_price'] = approximatePropertyPrice;
    data['property_status'] = propertyStatus;
    return data;
  }
}
