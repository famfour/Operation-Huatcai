class LoginSend {
  String? email;
  String? password;
  String? twoFactorAuthenticationCode;
  // String? deviceId;
  // String? deviceType;

  LoginSend(
      {this.email,
      this.password,
      this.twoFactorAuthenticationCode}); //this.deviceId, this.deviceType});

  LoginSend.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    password = json['password'];
    // deviceId = json['device_id'];
    // deviceType = json['device_type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['email'] = email;
    data['password'] = password;
    if (twoFactorAuthenticationCode != null) {
      data['twoFactorAuthenticationCode'] = twoFactorAuthenticationCode;
    }
    // data['device_id'] = this.deviceId;
    // data['device_type'] = this.deviceType;
    return data;
  }
}
