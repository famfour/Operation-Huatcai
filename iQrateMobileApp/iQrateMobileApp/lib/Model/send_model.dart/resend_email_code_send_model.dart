class ResendEmailCodeSendModel {
  ResendEmailCodeSendModel({
      String? email,}){
    _email = email;
}

  ResendEmailCodeSendModel.fromJson(dynamic json) {
    _email = json['email'];
  }
  String? _email;

  String? get email => _email;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['email'] = _email;
    return map;
  }

}