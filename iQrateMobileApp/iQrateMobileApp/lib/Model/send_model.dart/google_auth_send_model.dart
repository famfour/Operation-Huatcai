class GoogleAuthSendModel {
  String? accessToken;
  String? idToken;
  String? type;

  GoogleAuthSendModel({this.accessToken, this.idToken, this.type});

  Map<String, dynamic> toJson() =>
      {"access_token": accessToken, "idToken": idToken, "type": type};
}


class GoogleAuth2FASendModel {
  String? accessToken;
  String? idToken;
  String? type;
  String? twoFactorAuthenticationCode;

  GoogleAuth2FASendModel({this.accessToken, this.idToken, this.type, this.twoFactorAuthenticationCode});

  Map<String, dynamic> toJson() =>
      {"access_token": accessToken, "idToken": idToken, "type": type, "twoFactorAuthenticationCode": twoFactorAuthenticationCode};
}