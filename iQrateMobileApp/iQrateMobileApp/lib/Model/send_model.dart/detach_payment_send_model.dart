class DetachPaymentSendModel {
  String? customerId;
  String? paymentMethodId;

  DetachPaymentSendModel({
    this.customerId,
    this.paymentMethodId,
  });

  DetachPaymentSendModel.fromJson(Map<String, dynamic> json) {
    customerId = json['customer_id'];
    paymentMethodId = json['payment_method_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['customer_id'] = customerId;
    data['payment_method_id'] = paymentMethodId;
    return data;
  }
}
