class AddNewPropertySendModel {
  int? lead;
  String? postalCode;
  String? propertyStatus;
  String? propertyType;
  int? purchasePrice;
  String? streetName;
  String? unitNumber;
  String? projectName;

  AddNewPropertySendModel({
    this.lead,
    this.postalCode,
    this.propertyStatus,
    this.propertyType,
    this.purchasePrice,
    this.streetName,
    this.unitNumber,
    this.projectName,
  }); //this.deviceId, this.deviceType});

  AddNewPropertySendModel.fromJson(Map<String, dynamic> json) {
    lead = json['lead'];
    postalCode = json['postal_code'];
    propertyStatus = json['property_status'];
    propertyType = json['property_type'];
    purchasePrice = json['purchase_price'];
    streetName = json['street_name'];
    unitNumber = json['unit_number'];
    projectName = json['project_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['lead'] = lead;
    data['postal_code'] = postalCode;
    data['property_status'] = propertyStatus;
    data['property_type'] = propertyType;
    data['purchase_price'] = purchasePrice;
    data['street_name'] = streetName;
    data['unit_number'] = unitNumber;
    data['project_name'] = projectName;
    return data;
  }
}
