// ignore_for_file: non_constant_identifier_names

class RemovePaymentMethodSendModel {
  String? customerId;
  String? paymentMethodId;

  RemovePaymentMethodSendModel({
    this.customerId,
    this.paymentMethodId,
  }); //this.deviceId, this.deviceType});

  RemovePaymentMethodSendModel.fromJson(Map<String, dynamic> json) {
    customerId = json['customer_id'];
    paymentMethodId = json['payment_method_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['customer_id'] = customerId;
    data['payment_method_id'] = paymentMethodId;
    return data;
  }
}
