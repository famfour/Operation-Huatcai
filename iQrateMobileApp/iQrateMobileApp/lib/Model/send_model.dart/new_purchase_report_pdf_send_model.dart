class NewPurchaseReportPdfSendModel {
  int? numberOfLoanApplicants;
  String? name;
  int? propertyPrice;
  int? numberOfHousingLoan;
  double? ltv;
  int? loanAmount;
  int? buyerStampDuty;
  int? additionalBuyerStampDuty;
  int? legalFees;
  int? valuationFees;
  int? otherFees;
  String? extendLoanTenure;
  int? maximumLoanTenure;
  dynamic maximumLoanQualifiedMtl;
  dynamic maximumPriceMlt;
  double? msrMtl;
  double? tdsrMtl;
  int? fullPludgeAmountMlt;
  int? fullUnPludgeAmountMlt;
  int? preferPludgeMlt;
  int? calculatiomnUnpludgeMlt;
  int? preferUnpludgeMlt;
  int? calculationPludgeMlt;
  int? preferredLoanTenure;
  double? maximumLoanQualifiedPlt;
  double? maximumPricePlt;
  double? msrPlt;
  double? tdsrPlt;
  int? fullPludgeAmountPlt;
  int? fullUnPludgeAmountPlt;
  int? preferPludgePlt;
  int? calculatiomnUnpludgePlt;
  int? preferUnpludgePlt;
  int? calculationPludgePlt;
  String? token;
  List? exportFields;
  double? monthlyInstallmentMlt;
  double? monthlyInstallmentPlt;
  int? propertyType;

  NewPurchaseReportPdfSendModel(
      {this.numberOfLoanApplicants,
      this.name,
      this.propertyPrice,
      this.numberOfHousingLoan,
      this.ltv,
      this.loanAmount,
      this.buyerStampDuty,
      this.additionalBuyerStampDuty,
      this.legalFees,
      this.valuationFees,
      this.otherFees,
      this.extendLoanTenure,
      this.maximumLoanTenure,
      this.maximumLoanQualifiedMtl,
      this.maximumPriceMlt,
      this.msrMtl,
      this.tdsrMtl,
      this.fullPludgeAmountMlt,
      this.fullUnPludgeAmountMlt,
      this.preferPludgeMlt,
      this.calculatiomnUnpludgeMlt,
      this.preferUnpludgeMlt,
      this.calculationPludgeMlt,
      this.preferredLoanTenure,
      this.maximumLoanQualifiedPlt,
      this.maximumPricePlt,
      this.msrPlt,
      this.tdsrPlt,
      this.fullPludgeAmountPlt,
      this.fullUnPludgeAmountPlt,
      this.preferPludgePlt,
      this.calculatiomnUnpludgePlt,
      this.preferUnpludgePlt,
      this.calculationPludgePlt,
      this.token,
      this.exportFields,
      this.monthlyInstallmentMlt,
      this.monthlyInstallmentPlt,
      this.propertyType});

  NewPurchaseReportPdfSendModel.fromJson(Map<String, dynamic> json) {
    numberOfLoanApplicants = json['no_of_loan_applicants'];
    name = json['name'];
    propertyPrice = json['property_price'];
    numberOfHousingLoan = json['no_of_housing_loan'];
    ltv = json['ltv'];
    loanAmount = json['loan_amount'];
    buyerStampDuty = json['buyer_stamp_duty'];
    additionalBuyerStampDuty = json['additional_buyer_stamp_duty'];
    legalFees = json['legal_fees'];
    valuationFees = json['valuation_fees'];
    otherFees = json['other_fees'];
    extendLoanTenure = json['extend_loan_tenure'];
    maximumLoanTenure = json['maximum_loan_tenure'];
    maximumLoanQualifiedMtl = json['maximum_loan_qualified_mlt'];
    maximumPriceMlt = json['maximum_price_mlt'];
    msrMtl = json['msr_mlt'];
    tdsrMtl = json['tdsr_mlt'];
    fullPludgeAmountMlt = json['full_pludge_mlt'];
    fullUnPludgeAmountMlt = json['full_unpludge_mlt'];
    preferPludgeMlt = json['prefer_pludge_mlt'];
    calculatiomnUnpludgeMlt = json['calculatiomn_unpludge_mlt'];
    preferUnpludgeMlt = json['prefer_unpludge_mlt'];
    calculationPludgeMlt = json['calculation_pludge_mlt'];
    preferredLoanTenure = json['preferrd_loan_tenure'];
    maximumLoanQualifiedPlt = json['maximum_loan_qualified_plt'];
    maximumPricePlt = json['maximum_price_plt'];
    msrPlt = json['msr_plt'];
    tdsrPlt = json['tdsr_plt'];
    fullPludgeAmountPlt = json['full_pludge_plt'];
    fullUnPludgeAmountPlt = json['full_unpludge_plt'];
    preferPludgePlt = json['prefer_pludge_plt'];
    calculatiomnUnpludgePlt = json['calculatiomn_unpludge_plt'];
    preferUnpludgePlt = json['prefer_unpludge_plt'];
    calculationPludgePlt = json['calculation_pludge_plt'];
    token = json['token'];
    exportFields = json['export_fields'];
    monthlyInstallmentMlt = json['monthly_installment_mlt'];
    monthlyInstallmentPlt = json['monthly_installment_plt'];
    propertyType = json['property_type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['no_of_loan_applicants'] = numberOfLoanApplicants;
    data['name'] = name;
    data['property_price'] = propertyPrice;
    data['no_of_housing_loan'] = numberOfHousingLoan;
    data['ltv'] = ltv;
    data['loan_amount'] = loanAmount;
    data['buyer_stamp_duty'] = buyerStampDuty;
    data['additional_buyer_stamp_duty'] = additionalBuyerStampDuty;
    data['legal_fees'] = legalFees;
    data['valuation_fees'] = valuationFees;
    data['other_fees'] = otherFees;
    data['extend_loan_tenure'] = extendLoanTenure;
    data['maximum_loan_tenure'] = maximumLoanTenure;
    data['maximum_loan_qualified_mlt'] = maximumLoanQualifiedMtl;
    data['maximum_price_mlt'] = maximumPriceMlt;
    data['msr_mlt'] = msrMtl;
    data['tdsr_mlt'] = tdsrMtl;
    data['full_pludge_mlt'] = fullPludgeAmountMlt;
    data['full_unpludge_mlt'] = fullUnPludgeAmountMlt;
    data['prefer_pludge_mlt'] = preferPludgeMlt;
    data['calculatiomn_unpludge_mlt'] = calculatiomnUnpludgeMlt;
    data['prefer_unpludge_mlt'] = preferUnpludgeMlt;
    data['calculation_pludge_mlt'] = calculationPludgeMlt;
    data['preferrd_loan_tenure'] = preferredLoanTenure;
    data['maximum_loan_qualified_plt'] = maximumLoanQualifiedPlt;
    data['maximum_price_plt'] = maximumPricePlt;
    data['msr_plt'] = msrPlt;
    data['tdsr_plt'] = tdsrPlt;
    data['full_pludge_plt'] = fullPludgeAmountPlt;
    data['full_unpludge_plt'] = fullUnPludgeAmountPlt;
    data['prefer_pludge_plt'] = preferPludgePlt;
    data['calculatiomn_unpludge_plt'] = calculatiomnUnpludgePlt;
    data['prefer_unpludge_plt'] = preferUnpludgePlt;
    data['calculation_pludge_plt'] = calculationPludgePlt;
    data['token'] = token;
    data['export_fields'] = exportFields;
    data['monthly_installment_mlt'] = monthlyInstallmentMlt;
    data['monthly_installment_plt'] = monthlyInstallmentPlt;
    data['property_type'] = propertyType;
    return data;
  }
}
