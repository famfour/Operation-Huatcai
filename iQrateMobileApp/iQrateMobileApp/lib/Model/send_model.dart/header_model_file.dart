// To parse this JSON data, do
//
// final onBoardUserModel = onBoardUserModelFromJson(jsonString);

import 'dart:convert';

HeaderModelFile onBoardUserModelFromJson(String str) =>
    HeaderModelFile.fromJson(json.decode(str));

String onBoardUserModelToJson(HeaderModelFile data) =>
    json.encode(data.toJson());

class HeaderModelFile {
  String? authorization;

  HeaderModelFile({
    required this.authorization,
  });

  factory HeaderModelFile.fromJson(Map<String, dynamic> json) =>
      HeaderModelFile(
        authorization: json["Authorization"],
      );

  Map<String, dynamic> toJson() => {
        "Authorization": authorization,
      };
  Map<String, String> toHeader() => {
        "Authorization": authorization ?? " ",
        // "Content-Type": "multipart/form-data"
      };
}
