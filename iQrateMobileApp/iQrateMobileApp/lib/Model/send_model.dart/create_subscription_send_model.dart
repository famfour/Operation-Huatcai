// ignore_for_file: non_constant_identifier_names

class CreateSubscriptionSendModel {
  String? customerId;
  String? priceId;
  String? collectionMethod;
  String? product;
  String? promoCode;

  CreateSubscriptionSendModel({
    this.customerId,
    this.priceId,
    this.collectionMethod,
    this.product,
    this.promoCode,
  }); //this.deviceId, this.deviceType});

  CreateSubscriptionSendModel.fromJson(Map<String, dynamic> json) {
    customerId = json['customer_id'];
    priceId = json['price_id'];
    collectionMethod = json['collection_method'];
    product = json['product'];
    promoCode = json['promo_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['customer_id'] = customerId;
    data['price_id'] = priceId;
    data['collection_method'] = collectionMethod;
    data['product'] = product;
    data['promo_code'] = promoCode;
    return data;
  }
}
