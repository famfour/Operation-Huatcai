class AppleLoginSendModel {
  AppleLoginSendModel({
      this.provider = "apple",
      this.identityToken, 
      this.authorizationCode, 
      this.user, 
      this.givenName, 
      this.type,
      this.twoFactorAuthenticationCode,
  });

  AppleLoginSendModel.fromJson(dynamic json) {
    provider = json['provider'];
    identityToken = json['identityToken'];
    authorizationCode = json['authorizationCode'];
    user = json['user'];
    givenName = json['givenName'];
    type = json['type'];
  }
  String? provider;
  String? identityToken;
  String? authorizationCode;
  String? user;
  String? givenName;
  String? type;
  String? twoFactorAuthenticationCode;
AppleLoginSendModel copyWith({  String? provider,
  String? identityToken,
  String? authorizationCode,
  String? user,
  String? givenName,
  String? type,
}) => AppleLoginSendModel(  provider: provider ?? this.provider,
  identityToken: identityToken ?? this.identityToken,
  authorizationCode: authorizationCode ?? this.authorizationCode,
  user: user ?? this.user,
  givenName: givenName ?? this.givenName,
  type: type ?? this.type,
);
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['provider'] = provider;
    map['identityToken'] = identityToken;
    map['authorizationCode'] = authorizationCode;
    map['user'] = user;
    map['givenName'] = givenName;
    map['type'] = type;
    map['twoFactorAuthenticationCode'] = twoFactorAuthenticationCode;
    return map;
  }

}