<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .main-table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;
            border: 1px solid #ddd;
            /* margin-left: -4%; */
        }

        .main-table th,
        .main-table td {
            text-align: center;
            padding: 8px;
            border: 1px solid #ddd;
            color: #6F767E;
        }

        .main-table th {
            color: #DA3B3E;
            font-size: 1.125rem;
        }

        .PASS{
            color: green;
        }

        .FAIL{
            color: #DA3B3E;
        }

        #rate-table th {
            color: #DA3B3E;
            font-size: 1.125rem;
        }

        #rate-table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 80%;
            border: 1px solid #ddd;
            margin-left: 10%;
        }

        .year-table
        {
            border-collapse: collapse;
        }
        .year-table td{

            /* border-spacing: 0; */

            border: 1px solid #D8E0F0;
            /* margin-left: 10%; */
            line-height: 1.5;
        }

        .yearly_rate-table{
            border-collapse: collapse;
            border-spacing: 0;
            width: 90%;
            border: 1px solid #ddd;
            margin-left: 5%;
        }

        .yearly_rate-table td {
            text-align: left;
            padding: 8px;
            border: 1px solid #ddd;
            /* color: #6F767E; */
        }

        #rate-table th {
            text-align: center;
            padding: 8px;
            border: 1px solid #ddd;
            color: #6F767E;
        }

        #rate-table td {
            text-align: left;
            padding: 8px;
            border: 1px solid #ddd;
            color: #6F767E;
        }

        #rate-table th {
            color: #DA3B3E;
            font-size: 1.125rem;
        }

        #progressive-rate-table th {
            color: #DA3B3E;
            font-size: 1.125rem;
        }

        #progressive-rate-table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 80%;
            border: 1px solid #ddd;
            margin-left: 10%;
        }

        #progressive-rate-table th,
        #progressive-rate-table td {
            text-align: center;
            padding: 8px;
            border: 1px solid #ddd;
            color: #6F767E;
        }

        #progressive-rate-table th {
            color: #DA3B3E;
            font-size: 1.125rem;
        }

        #loan-table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 30%;
        }

        #mybank {
            margin-left: 25px;
        }

        #defaultImg {
            height: 60px;
        }

        .alert {
            position: relative;
            padding: 1rem 1rem;
            margin-bottom: 1rem;
            border: 1px solid transparent;
            border-radius: 0.25rem;
            font-size: 18px;
            font-weight: bold;
        }

        .alert-danger {
            color: #ffffff;
            background-color: #e11c2c;
            border-color: #e11c2c;
        }

        .alert_default {
            position: relative;
            color: #e11c2c;
            padding: 1rem 1rem;
            margin-bottom: 1rem;
            border: 1px solid transparent;
            border-radius: 0.25rem;
            font-size: 16px;
            font-weight: bold;
            background-color: lightgray;
            //border-color: #e11c2c;
        }

        .gray_color{
            color: #767676;
        }

    </style>
</head>

<body>



    <div style="overflow-x:auto;" >

       <center> <img style="margin-left: 25px; margin-top:15px" src="assets/images/logo.png"/></center>


        <div>
            <br>
            <h2 style="color: #e11c2c; font-weight:bold;">New Purchase Report</h2>
        </div>

            @if (in_array("Summary", $export_fields))

                <div class="alert alert-danger" role="alert">
                    Summary
                </div>
                <table id="loan-table" style="width:100%;line-height:2">
                    <tr>
                        <td width="5%"></td>
                        <td width="30%" class="gray_color" >
                            Prepared for
                        </td>
                        <td>
                            {{ $data['Summary']['Prepared_for'] }}
                        </td>
                    </tr>
                    <tr>
                        <td width="5%"></td>
                        <td width="30%" class="gray_color" >
                            Property Purchase Price
                        </td>
                        <td>
                            ${{ number_format($data['Summary']['Property_Purchase_Price'], 2) }}
                        </td>
                    </tr>
                    <tr>
                        <td width="5%"></td>
                        <td width="30%" class="gray_color" >
                            Cash Down Payment
                            </td>
                        <td>
                            ${{ $data['Summary']['Cash_Down_Payment'].".00" }}
                        </td>
                    </tr>

                    <tr>
                        <td width="5%"></td>
                        <td width="30%" class="gray_color" >
                            CPF/Cash Down Payment
                            </td>
                        <td>
                            ${{ $data['Summary']['CPF_or_Cash_Down_Payment'].".00" }}
                        </td>
                    </tr>

                    <tr>
                        <td width="5%"></td>
                        <td width="30%" class="gray_color" >
                            Loan Amount
                            </td>
                        <td>
                            ${{ number_format($data['Summary']['Loan_Amount'],2) }}
                        </td>
                    </tr>
                    <tr>
                        <td width="5%"></td>
                        <td width="30%" class="gray_color" >
                            Buyer`s Stamp Duty
                            </td>
                        <td>
                            ${{ number_format($data['Summary']['Buyer_Stamp_Duty'], 2) }}
                        </td>
                    </tr>
                    <tr>
                        <td width="5%"></td>
                        <td width="30%" class="gray_color" >
                            Additional Buyer`s Stamp Duty
                            </td>
                        <td>
                            ${{ number_format($data['Summary']['Additional_Buyer_Stamp_Duty'], 2) }}
                        </td>
                    </tr>
                    <tr>
                        <td width="5%"></td>
                        <td width="30%" class="gray_color" >
                            Legal Fees
                            </td>
                        <td>
                            ${{ number_format($data['Summary']['Legal_Fees'], 2) }}
                        </td>
                    </tr>
                    <tr>
                        <td width="5%"></td>
                        <td width="30%" class="gray_color" >
                            Valuation Fees
                            </td>
                        <td>
                            ${{ number_format($data['Summary']['Valuation_Fees'], 2) }}
                        </td>
                    </tr>
                    <tr>
                        <td width="5%"></td>
                        <td width="30%" class="gray_color" >
                            Other Fees
                            </td>
                        <td>
                            ${{ number_format($data['Summary']['Other_Fees'], 2) }}
                        </td>
                    </tr>
                </table>
                <br><br>
            @endif


        @if (in_array("Loan_eligibility_maximum", $export_fields))
                <div class="alert alert-danger" role="alert">
                    Loan Eligibility (Maximum Loan Tenure)
                </div>

                <table style="width:100%;">
                    <tr style="padding:10px;">
                        <th colspan="3"><h4 class="{{$data['Loan_Eligiblity_Maximum_Loan_Tenure_Based']['MSR Status']}}">MSR ({{$data['Loan_Eligiblity_Maximum_Loan_Tenure_Based']['MSR Status']}})</h4></th>
                        <th colspan="3"> <h4 class="{{$data['Loan_Eligiblity_Maximum_Loan_Tenure_Based']['TDSR Status']}}">TDSR ({{$data['Loan_Eligiblity_Maximum_Loan_Tenure_Based']['TDSR Status']}})</h4></th>
                    </tr>
                    <tr>
                        <th colspan="3">
                            <?php
                                $msr_style = 'rgba(56,123,45,0.6)';
                                if($data['Loan_Eligiblity_Maximum_Loan_Tenure_Based']['MSR'] > 30)
                                {
                                    $msr_style = 'rgba(245,27,27,0.8)';
                                }

                                $tdsr_style = 'rgba(56,123,45,0.6)';
                                if($data['Loan_Eligiblity_Maximum_Loan_Tenure_Based']['TDSR'] > 55)
                                {
                                    $tdsr_style = 'rgba(245,27,27,0.8)';
                                }
                            ?>
                            <img src="https://quickchart.io/chart?width=60&height=60&c={type:'radialGauge',data:{datasets:[{data:[{{$data['Loan_Eligiblity_Maximum_Loan_Tenure_Based']['MSR']}}],backgroundColor:'{{$msr_style }}'}]}}">

                        </th>
                        <th colspan="3">
                            <img src="https://quickchart.io/chart?width=60&height=60&c={type:'radialGauge',data:{datasets:[{data:[{{$data['Loan_Eligiblity_Maximum_Loan_Tenure_Based']['TDSR']}}],backgroundColor:'{{$tdsr_style}}',fontColor:'rgba(223,83,86)'}],fontColor:'rgba(223,83,86)'}}">
                            </th>
                    </tr>
                    <tr><td colspan="3"><br></td></tr>
                    <tr>
                        <td width="5%"></td>
                        <td width="30%" colspan="2" class="gray_color">Maximum Loan Tenure</td>
                        <td>{{$data['Loan_Eligiblity_Maximum_Loan_Tenure_Based']['Maximum_Loan_Tenure']}} Years</td>
                    </tr>
                    <tr>
                        <td width="5%"></td>
                        <td width="30%" colspan="2" class="gray_color">Monthly Installment</td>
                        <td>${{$data['Loan_Eligiblity_Maximum_Loan_Tenure_Based']['Monthly_Installment']}}</td>
                    </tr>
                </table>
                <table style="width:100%;line-height:2;page-break-inside: avoid;" >

                    <tr>
                        <td width="5%"></td>
                        <td colspan="3"><h3 style="color: #DA3B3E">You can choose Pledge and/or UnPledge Funds to passTDSR/MSR</h3></td>
                    </tr>
                    <tr>
                        <td width="5%">
                        <td width="30%" colspan="2" class="gray_color" >Full Pledge Amount</td>
                        <td>${{ number_format($data['Loan_Eligiblity_Maximum_Loan_Tenure_Based']['Full_Pledge_Amount'], 2)}} </td>
                    </tr>
                    <tr>
                        <td width="5%">
                        <td width="30%" colspan="2"><b>OR</b></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td width="5%">
                        <td width="30%" colspan="2" class="gray_color" >Full UnPledge Amount</td>
                        <td>${{ number_format($data['Loan_Eligiblity_Maximum_Loan_Tenure_Based']['Full_Unpledge_Amount'], 2)}} </td>
                    </tr>
                    <tr>
                        <td width="5%">
                        <td width="30%" colspan="2"><b>OR</b></td>
                        <td></td>
                    </tr>

                    <tr>
                        <td width="5%">
                        <td width="30%"><span class="gray_color">Peferred Pledge Amount</span><br>{{$data['Loan_Eligiblity_Maximum_Loan_Tenure_Based']['Preferred_Pledge_Amount']}}</td>
                        <td width="5%"> +</td>
                        <td><span class="gray_color">Unpledge Amount</span><br>${{ number_format($data['Loan_Eligiblity_Maximum_Loan_Tenure_Based']['UnPledge_Amount'], 2)}} </td>
                    </tr>

                    <tr>
                        <td width="5%">
                        <td width="30%" colspan="2"><b>OR</b></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td width="5%">
                        <td width="30%"><span class="gray_color">Peferred UnPledge Amount</span><br>{{$data['Loan_Eligiblity_Maximum_Loan_Tenure_Based']['Preferred_UnPledge_Amount']}}</td>
                        <td width="5%"> +</td>
                        <td><span class="gray_color">Pledge Amount</span><br>${{number_format($data['Loan_Eligiblity_Maximum_Loan_Tenure_Based']['Pledge_Amount'], 2)}} </td>
                    </tr>


                    <tr>
                        <td width="5%"></td>
                        <td colspan="3"><h3 style="color: #DA3B3E">Based on Your Current Financial Status</h3></td>
                    </tr>
                    <tr>
                        <td width="5%">
                        <td width="30%" colspan="2" class="gray_color" >Maximum Loan Qualified</td>
                        <td>${{ number_format($data['Loan_Eligiblity_Maximum_Loan_Tenure_Based']['Maximum_Loan_Qualified'], 2)}} </td>
                    </tr>
                    <tr>
                        <td width="5%">
                        <td width="30%" colspan="2" class="gray_color" >Maximum Property price that can be purchased</td>
                        <td>${{ number_format($data['Loan_Eligiblity_Maximum_Loan_Tenure_Based']['Maximum_Property_Price'], 2)}} </td>
                    </tr>
                </table>
                <br><br>

            @endif

            @if (in_array("Loan_eligibility_maximum_package", $export_fields))
                <div class="alert alert-danger" role="alert">
                    Loan Eligibility (Maximum Loan Tenure) Loan Package
                </div>

                <table id="rate-table">
                    <?php
                        $header = '';
                        $monthly_installment = '';
                        $total_payment = '';
                        $total_principal = '';
                        $total_interest = '';
                    ?>
                    @foreach ($data['Loan_Package_Maximum_Loan_Tenure_Based'] as $key=> $rate_val)
                    <?php

                        $header                 .= '<th>'.str_replace("_"," ",ucfirst($key)).' '.($rate_val['rate']).'%</th>';
                        $monthly_installment    .= '<td>$'.number_format($rate_val['Monthly_Installment'], 2).'</td>';
                        $total_payment          .= '<td>$'.$rate_val['Total_Payment'].'</td>';
                        $total_principal        .= '<td>$'.number_format($rate_val['Total_Principal'], 2).'</td>';
                        $total_interest         .= '<td>$'.$rate_val['Total_Interest'].'</td>';

                    ?>
                    @endforeach
                    <tr><th></th><?php echo $header; ?></tr>
                    <tr><td>Monthly Installment</td><?php echo $monthly_installment; ?></tr>
                    <tr><td>Total Payment</td><?php echo $total_payment; ?></tr>
                    <tr><td>Total Principal</td><?php echo $total_principal; ?></tr>
                    <tr><td>Total Interest</td><?php echo $total_interest; ?></tr>
                </table>
                <br><br>

                @if (!empty($data['Lowest_fixed']))
            <div class="alert" role="alert" style="color: #e11c2c; font-weight:bold;">
                Lowest Fixed<hr>
            </div>

            <table style="width: 60%;margin:auto;" >
                    <tr>
                        <td>

                            <img style="width:100px;height:100px;" src="{{ $data['Lowest_fixed']['bank_logo'] }}" alt="">

                        </td>
                        <td colspan="2">
                            <h4>{{ $data['Lowest_fixed']['bank_name'] }}</h4>
                            <h4>
                                {{ ucfirst($data['Lowest_fixed']['rate_type']) }}

                                @if (!empty($data['Lowest_fixed']['details']['rate_category']))
                                    ({{ ucfirst($data['Lowest_fixed']['details']['rate_category']) }})
                                @endif
                            </h4>
                        </td>
                    </tr>
            </table>
            <table style="width: 70%;margin:auto;text-align:center" class="year-table">
                    <tr >
                        <td>
                            Year 1 : <b>{{ $data['Lowest_fixed']['rate_list'][0]/0.01 }}%</b>
                        </td>
                        <td>
                            Year 2 :<b>
                            @if (!empty($data['Lowest_fixed']['rate_list'][1]))
                                {{$data['Lowest_fixed']['rate_list'][1]/0.01}}
                            @else
                                {{$data['Lowest_fixed']['rate_list'][0]/0.01}}
                            @endif %</b>
                        </td>

                        <td>
                            Year 3 :<b>
                            @if (!empty($data['Lowest_fixed']['rate_list'][2]))
                                {{$data['Lowest_fixed']['rate_list'][2]/0.01}}
                            @else
                                {{$data['Lowest_fixed']['rate_list'][0]/0.01}}
                            @endif %</b>
                        </td>
                    </tr>
            </table>

            <br>
            <div class="alert_default" role="alert">
                Rates
            </div>

            <table class="yearly_rate-table">
                    <?php
                        $max_key = count($data['Lowest_fixed']['details']['rates']) -1;
                    ?>
                        @for($c = 0; $c <= $max_key; $c++)

                        <?php
                            $header = ($c == 5)? 'Thereafter' : 'Year '.$c+1;
                        ?>

                            <tr><td>{{$header}}</td><td> <span style="font-size: 14px;">{{ $data['Lowest_fixed']['details']['rates'][$max_key - $c]['reference_rate']['reference'] }}</span> <br>
                                @if ( $data['Lowest_fixed']['details']['rates'][$max_key - $c]['reference_rate']['rate_type'] != 'fixed')

                                 ({{ $data['Lowest_fixed']['details']['rates'][$max_key - $c]['reference_rate']['interest_rate'] }}%) {{ $data['Lowest_fixed']['details']['rates'][$max_key - $c]['reference_rate']['equation'] }} ({{ $data['Lowest_fixed']['details']['rates'][$max_key - $c]['bank_spread'] }}%)  =
                                @endif
                                {{ $data['Lowest_fixed']['details']['rates'][$max_key - $c]['total_interest_rate'] }}%</td></tr>
                        @endfor

            </table>

            <br>

            <div class="alert_default" role="alert">
                Key Features
            </div>

            <table class="yearly_rate-table">
                <tr><td>Property Type</td><td>
                @if (is_array($data['Lowest_fixed']['details']['property_types']))
                    @foreach ($data['Lowest_fixed']['details']['property_types'] as $item)

                        @if(!empty(config('calculator.property_types_key')[$item]))
                            {{ config('calculator.property_types_key')[$item] }} <br>
                        @else
                            {{ ucfirst($item) }} <br>
                        @endif
                    @endforeach
                @else
                    {{ ucfirst($data['Lowest_fixed']['details']['property_types']) }}
                @endif
                </td></tr>

                <tr><td>Property Status</td><td>
                    @if (is_array($data['Lowest_fixed']['details']['property_status']))
                        @foreach ($data['Lowest_fixed']['details']['property_status'] as $item)
                            {{ ucfirst($item) }} <br>
                        @endforeach
                    @else
                        {{ ucfirst($data['Lowest_fixed']['details']['property_status']) }}
                    @endif
                    </td></tr>

                <tr><td>Type of Loan</td><td>
                    @if (!empty($data['Lowest_fixed']['details']['loan_category']))
                        @foreach ($data['Lowest_fixed']['details']['loan_category'] as $val)
                        @if(!empty(config('calculator.loan_types_key')[$val]))
                            {{ config('calculator.loan_types_key')[$val] }} <br>
                        @else
                            {{ ucfirst($val) }} <br>
                        @endif
                        @endforeach
                    @endif
                </td></tr>

                <tr><td>Lock in Period</td><td>{{$data['Lowest_fixed']['details']['lock_in_period']}} Years</td></tr>

                <tr><td>Min Loan Amount</td><td>${{$data['Lowest_fixed']['details']['min_loan_amount']}}</td></tr>

                <tr><td>Deposit to Place</td><td>{{$data['Lowest_fixed']['details']['deposit_to_place']}}</td></tr>
                <tr><td>Remarks for Applicants</td><td>
                @if (is_array($data['Lowest_fixed']['details']['remarks_for_client']))
                    @foreach ($data['Lowest_fixed']['details']['remarks_for_client'] as $item)
                        {{$item}} <br>
                    @endforeach
                @else
                    {{$data['Lowest_fixed']['details']['remarks_for_client']}}
                @endif
                </td></tr>
                <tr><td>Remarks for Broker</td><td>
                    @if (is_array($data['Lowest_fixed']['details']['remarks_for_broker']))
                    @foreach ($data['Lowest_fixed']['details']['remarks_for_broker'] as $item)
                        {{$item}} <br>
                    @endforeach
                @else
                    {{$data['Lowest_fixed']['details']['remarks_for_broker']}}
                @endif
                    </td></tr>
            </table>

            <br><br>
            <div class="alert_default" role="alert">
                Bank`s Subsidy
            </div>

            <table class="yearly_rate-table">
                <tr><td>Cash Rebate / Leagal Subsidy</td><td>



                    @if (is_array($data['Lowest_fixed']['details']['cash_rebate_legal_subsidy']))
                    @foreach ($data['Lowest_fixed']['details']['cash_rebate_legal_subsidy'] as $item)

                <?php echo str_replace('&ge;','',htmlentities($item)); ?><br>



                    @endforeach

                @else
                    {{str_replace("?"," ",$data['Lowest_fixed']['details']['cash_rebate_legal_subsidy'])}}
                @endif

                </td></tr>

                <tr><td>Valuation Subsidy</td><td>
                    @if (is_array($data['Lowest_fixed']['details']['valuation_subsidy']))
                    @foreach ($data['Lowest_fixed']['details']['valuation_subsidy'] as $item)
                        {{$item}} <br>
                    @endforeach
                @else
                    {{$data['Lowest_fixed']['details']['valuation_subsidy']}}
                @endif
                </td></tr>

                <tr><td>Cash Rebate / Subsidy Clawback</td><td>{{$data['Lowest_fixed']['details']['cash_rebate_subsidy_clawback_period_years']}} Years</td></tr>

                <tr><td>Fire Insurance Subsidy</td><td>{{$data['Lowest_fixed']['details']['fire_insurance_subsidy']}}</td></tr>
            </table>

            <br>
            <div class="alert_default" role="alert">
                Early Repayment Penalty
            </div>

            <table class="yearly_rate-table">
                <tr><td>Partial Repayment Penalty</td><td>

                    {{$data['Lowest_fixed']['details']['partial_repayment_penalty']}}%

                </td></tr>

                <tr><td>Partial Repayment Penalty Remarks</td><td>

                    {{$data['Lowest_fixed']['details']['partial_repayment_penalty_remarks']}}

                </td></tr>

                <tr><td>Full Repayment Penalty</td><td>
                    {{$data['Lowest_fixed']['details']['full_repayment_penalty']}}%

                </td></tr>

                <tr><td>Full Repayment Penalty Remarks</td><td>
                    {{$data['Lowest_fixed']['details']['full_repayment_penalty_remarks']}}

                </td></tr>

                <tr><td>Cancellation Fees</td><td>{{$data['Lowest_fixed']['details']['cancellation_fee']}}</td></tr>

                <tr><td>Cancellation Fees Remarks</td><td>{{$data['Lowest_fixed']['details']['cancellation_fee_remarks']}}</td></tr>

                <tr><td>New Purchase Referral Fees</td><td>{{$data['Lowest_fixed']['details']['new_purchase_referral_fee']}}</td></tr>
                <tr><td>Refinance Referral Fees</td><td>{{$data['Lowest_fixed']['details']['refinance_referral_fee']}}</td></tr>

            </table>

            <br><br>
            @endif

            @if (!empty($data['Lowest_floating']))
            <div class="alert" role="alert" style="color: #e11c2c; font-weight:bold;">
                Lowest Floating<hr>
            </div>

            <table style="width: 60%;margin:auto;">
                    <tr>
                        <td>

                            <img style="width:100px;height:100px;" src="{{ $data['Lowest_floating']['bank_logo'] }}" alt="">

                        </td>
                        <td colspan="2">
                            <h4>{{ $data['Lowest_floating']['bank_name'] }}</h4>
                            <h4>
                                {{ ucfirst($data['Lowest_floating']['rate_type']) }}
                                @if (!empty($data['Lowest_floating']['details']['rate_category']))
                                    ({{ ucfirst($data['Lowest_floating']['details']['rate_category']) }})
                                @endif

                            </h4>
                        </td>
                    </tr>
            </table>
            <table style="width: 65%;margin:auto;text-align:center" class="year-table">
                    <tr >
                        <td>
                            Year 1 :<b>{{ $data['Lowest_floating']['rate_list'][0]/0.01 }}%</b>
                        </td>
                        <td>
                            Year 2 :<b>
                            @if (!empty($data['Lowest_floating']['rate_list'][1]))
                                {{$data['Lowest_floating']['rate_list'][1]/0.01}}
                            @else
                                {{$data['Lowest_floating']['rate_list'][0]/0.01}}
                            @endif %</b>
                        </td>

                        <td>
                            Year 3 :<b>
                            @if (!empty($data['Lowest_floating']['rate_list'][2]))
                                {{$data['Lowest_floating']['rate_list'][2]/0.01}}
                            @else
                                {{$data['Lowest_floating']['rate_list'][0]/0.01}}
                            @endif %</b>
                        </td>
                    </tr>
            </table>

            <br>
            <div class="alert_default" role="alert">
                Rates
            </div>

            <table class="yearly_rate-table">
                <?php
                        $max_key = count($data['Lowest_floating']['details']['rates']) -1;
                    ?>
                        @for($c = 0; $c <= $max_key; $c++)

                        <?php
                            $header = ($c == 5)? 'Thereafter' : 'Year '.$c+1;
                        ?>

                            <tr><td> {{$header}}</td><td> <span style="font-size: 14px;">{{ $data['Lowest_floating']['details']['rates'][$max_key - $c]['reference_rate']['reference'] }} </span><br>
                                @if ( $data['Lowest_floating']['details']['rates'][$max_key - $c]['reference_rate']['rate_type'] != 'fixed')

                                  ({{ $data['Lowest_floating']['details']['rates'][$max_key - $c]['reference_rate']['interest_rate'] }}%) {{ $data['Lowest_floating']['details']['rates'][$max_key - $c]['reference_rate']['equation'] }} ({{ $data['Lowest_floating']['details']['rates'][$max_key - $c]['bank_spread'] }}%) =
                                @endif
                                {{ $data['Lowest_floating']['details']['rates'][$max_key - $c]['total_interest_rate'] }}%</td></tr>
                        @endfor



            </table>

            <br>

            <div class="alert_default" role="alert">
                Key Features
            </div>

            <table class="yearly_rate-table">
                <tr><td>Property Type</td><td>
                @if (is_array($data['Lowest_floating']['details']['property_types']))
                    @foreach ($data['Lowest_floating']['details']['property_types'] as $item)
                    @if(!empty(config('calculator.property_types_key')[$item]))
                        {{ config('calculator.property_types_key')[$item] }} <br>
                    @else
                        {{ ucfirst($item) }} <br>
                    @endif
                    @endforeach
                @else
                    {{ ucfirst($data['Lowest_floating']['details']['property_types']) }}
                @endif
                </td></tr>

                <tr><td>Property Status</td><td>
                    @if (is_array($data['Lowest_floating']['details']['property_status']))
                        @foreach ($data['Lowest_floating']['details']['property_status'] as $item)
                            {{ ucfirst($item) }} <br>
                        @endforeach
                    @else
                        {{ ucfirst($data['Lowest_floating']['details']['property_status']) }}
                    @endif
                    </td></tr>

                <tr><td>Type of Loan</td><td>
                    @if (!empty($data['Lowest_floating']['details']['loan_category']))
                    @foreach ($data['Lowest_floating']['details']['loan_category'] as $val)
                    @if(!empty(config('calculator.loan_types_key')[$val]))
                            {{ config('calculator.loan_types_key')[$val] }} <br>
                        @else
                            {{ ucfirst($val) }} <br>
                        @endif
                    @endforeach
                @endif
                </td></tr>

                <tr><td>Lock in Period</td><td>{{$data['Lowest_floating']['details']['lock_in_period']}} Years</td></tr>

                <tr><td>Min Loan Amount</td><td>${{$data['Lowest_floating']['details']['min_loan_amount']}}</td></tr>

                <tr><td>Deposit to Place</td><td>{{$data['Lowest_floating']['details']['deposit_to_place']}}</td></tr>
                <tr><td>Remarks for Applicants</td><td>
                @if (is_array($data['Lowest_floating']['details']['remarks_for_client']))
                    @foreach ($data['Lowest_floating']['details']['remarks_for_client'] as $item)
                        {{$item}} <br>
                    @endforeach
                @else
                    {{$data['Lowest_floating']['details']['remarks_for_client']}}
                @endif
                </td></tr>
                <tr><td>Remarks for Broker</td><td>
                    @if (is_array($data['Lowest_floating']['details']['remarks_for_broker']))
                    @foreach ($data['Lowest_floating']['details']['remarks_for_broker'] as $item)
                        {{$item}} <br>
                    @endforeach
                @else
                    {{$data['Lowest_floating']['details']['remarks_for_broker']}}
                @endif
                    </td></tr>
            </table>

            <br>
            <div class="alert_default" role="alert">
                Bank`s Subsidy
            </div>

            <table class="yearly_rate-table">
                <tr><td>Cash Rebate / Leagal Subsidy</td><td>

                    @if (is_array($data['Lowest_floating']['details']['cash_rebate_legal_subsidy']))
                    @foreach ($data['Lowest_floating']['details']['cash_rebate_legal_subsidy'] as $item)
                    <?php echo str_replace('&ge;','',htmlentities($item)); ?><br>

                    @endforeach
                @else
                    {{str_replace("?"," ",$data['Lowest_floating']['details']['cash_rebate_legal_subsidy'])}}
                @endif

                </td></tr>

                <tr><td>Valuation Subsidy</td><td>
                    @if (is_array($data['Lowest_floating']['details']['valuation_subsidy']))
                    @foreach ($data['Lowest_floating']['details']['valuation_subsidy'] as $item)
                        {{$item}} <br>
                    @endforeach
                @else
                    {{$data['Lowest_floating']['details']['valuation_subsidy']}}
                @endif
                </td></tr>

                <tr><td>Cash Rebate / Subsidy Clawback</td><td>{{$data['Lowest_floating']['details']['cash_rebate_subsidy_clawback_period_years']}} Years</td></tr>

                <tr><td>Fire Insurance Subsidy</td><td>{{$data['Lowest_floating']['details']['fire_insurance_subsidy']}}</td></tr>
            </table>

            <br>
            <div class="alert_default" role="alert">
                Early Repayment Penalty
            </div>

            <table class="yearly_rate-table">
                <tr><td>Partial Repayment Penalty</td><td>

                    {{$data['Lowest_floating']['details']['partial_repayment_penalty']}}%

                </td></tr>
                <tr><td>Partial Repayment Penalty Remarks</td><td>

                    {{$data['Lowest_floating']['details']['partial_repayment_penalty_remarks']}}

                </td></tr>

                <tr><td>Full Repayment Penalty</td><td>
                    {{$data['Lowest_floating']['details']['full_repayment_penalty']}}%

                </td></tr>
                <tr><td>Full Repayment Penalty Remarks</td><td>
                    {{$data['Lowest_floating']['details']['full_repayment_penalty_remarks']}}

                </td></tr>

                <tr><td>Cancellation Fees</td><td>{{$data['Lowest_floating']['details']['cancellation_fee']}}</td></tr>
                <tr><td>Cancellation Fees Remarks</td><td>{{$data['Lowest_floating']['details']['cancellation_fee_remarks']}}</td></tr>
                <tr><td>New Purchase Referral Fees</td><td>{{$data['Lowest_floating']['details']['new_purchase_referral_fee']}}</td></tr>
                <tr><td>Refinance Referral Fees</td><td>{{$data['Lowest_floating']['details']['refinance_referral_fee']}}</td></tr>

            </table>

            <br><br>
            @endif

            <br><br>
        @endif

        @if (in_array("Loan_eligibility_maximum_mortgage", $export_fields))
            <div class="alert alert-danger" role="alert">
                Loan Eligibility (Maximum Loan Tenure) Mortgage Repayment For Lowest Fixed
            </div>

                    <table  class="main-table" >
                        <!-- row 1 -->
                        <tr>
                            <th colspan="2">Year</th>
                            <th>Interest Rate</th>
                            <th>Installment Amount</th>
                            <th>Interest Repayment</th>
                            <th>Principal Repayment</th>
                            <th>Outstanding Amount</th>
                            <th>Cum Interest Paid</th>
                        </tr>
                        <?php $y = 0; $j = 1; ?>
                        @foreach ($data['Loan_Package_Maximum_Loan_Tenure_Based']['lowest_fixed']['Mortgage_Repayment_Table'] as $key=> $val)


                                <?php $style = ''; ?>
                                @if ($val['month'] == 12)
                                    <?php $style = 'background:lightgray;'; ?>
                                @endif
                                <tr style="{{$style}}">
                                    <td>@if ($y != $val['year'] )
                                        {{ $val['year']}}
                                    @endif</td>
                                    <td>{{ $val['month'] }}</td>
                                    <td>{{ $val['interest_rate']."%"}}</td>

                                    <td>${{ number_format($val['installment_amount'], 2)}}</td>

                                    <td>{{ "$".number_format($val['interest_repayment'], 2)}}</td>
                                    <td>{{ "$".number_format($val['principal_repayment'], 2)}}</td>
                                    <td>{{ "$".number_format($val['outstanding_amount'], 2)}}</td>
                                    <td>{{ "$".number_format($val['cum_interest_paid'], 2)}}</td>
                                </tr>
                                <?php $y = $val['year']; ?>

                            <?php $j++; ?>
                    @endforeach
                    </table>
                </div>

                <br><br>
            <div class="alert alert-danger" role="alert">
                Loan Eligibility (Maximum Loan Tenure) Mortgage Repayment For Lowest Floating
            </div>

                    <table class="main-table" >
                        <!-- row 1 -->
                        <tr>
                            <th colspan="2">Year</th>
                            <th>Interest Rate</th>
                            <th>Installment Amount</th>
                            <th>Interest Repayment</th>
                            <th>Principal Repayment</th>
                            <th>Outstanding Amount</th>
                            <th>Cum Interest Paid</th>
                        </tr>
                        <?php $y = 0; $j = 1; ?>
                        @foreach ($data['Loan_Package_Maximum_Loan_Tenure_Based']['lowest_floating']['Mortgage_Repayment_Table'] as $key=> $val)

                                <?php $style = ''; ?>
                                @if ($val['month'] == 12)
                                    <?php $style = 'background:lightgray;'; ?>
                                @endif
                                <tr style="{{$style}}">
                                    <td>@if ($y != $val['year'] )
                                        {{ $val['year']}}
                                    @endif</td>
                                    <td>{{ $val['month'] }}</td>
                                    <td>{{ $val['interest_rate']."%"}}</td>

                                    <td>${{ number_format($val['installment_amount'], 2)}}</td>

                                    <td>{{ number_format($val['interest_repayment'], 2)}}</td>
                                    <td>{{ "$".number_format($val['principal_repayment'], 2)}}</td>
                                    <td>{{ "$".number_format($val['outstanding_amount'], 2)}}</td>
                                    <td>{{ "$".number_format($val['cum_interest_paid'], 2)}}</td>
                                </tr>
                                <?php $y = $val['year']; ?>

                            <?php $j++; ?>
                    @endforeach
                    </table>
                </div>
            <br><br>
        @endif


        @if ($preferred_loan_tenure > 0)

            @if (in_array("Loan_eligibility_preferred", $export_fields))
                <table style="width:100%;page-break-inside: avoid;">
                    <tr><td colspan="5"><div class="alert alert-danger" role="alert">
                        Loan Eligibility (Preferred Loan Tenure)
                    </div></td>
                    </tr>
                    <tr style="padding:10px;">

                        <th colspan="3"><h4 class="{{$data['Loan_Eligiblity_Preferred_Loan_Tenure_Based']['MSR Status']}}">MSR ({{$data['Loan_Eligiblity_Preferred_Loan_Tenure_Based']['MSR Status']}})</h4></th>
                        <th colspan="3"> <h4 class="{{$data['Loan_Eligiblity_Preferred_Loan_Tenure_Based']['TDSR Status']}}">TDSR ({{$data['Loan_Eligiblity_Preferred_Loan_Tenure_Based']['TDSR Status']}})</h4></th>
                    </tr>
                    <tr>
                        <th colspan="3">
                            <?php
                                $msr_style = 'rgba(56,123,45,0.6)';
                                if($data['Loan_Eligiblity_Preferred_Loan_Tenure_Based']['MSR'] > 30)
                                {
                                    $msr_style = 'rgba(245,27,27,0.8)';
                                }

                                $tdsr_style = 'rgba(56,123,45,0.6)';
                                if($data['Loan_Eligiblity_Preferred_Loan_Tenure_Based']['TDSR'] > 55)
                                {
                                    $tdsr_style = 'rgba(245,27,27,0.8)';
                                }
                            ?>
                            <img src="https://quickchart.io/chart?width=60&height=60&c={type:'radialGauge',data:{datasets:[{data:[{{$data['Loan_Eligiblity_Preferred_Loan_Tenure_Based']['MSR']}}],backgroundColor:'{{$msr_style }}'}]}}">

                        </th>
                        <th colspan="3">
                            <img src="https://quickchart.io/chart?width=60&height=60&c={type:'radialGauge',data:{datasets:[{data:[{{$data['Loan_Eligiblity_Preferred_Loan_Tenure_Based']['TDSR']}}],backgroundColor:'{{$tdsr_style}}',fontColor:'rgba(223,83,86)'}],fontColor:'rgba(223,83,86)'}}">
                            </th>
                    </tr>
                </tr>
                <tr><td colspan="3"><br></td></tr>
                    <tr>
                        <td width="5%"></td>
                        <td width="30%" colspan="2" class="gray_color">Preferred Loan Tenure</td>
                        <td>{{$data['Loan_Eligiblity_Preferred_Loan_Tenure_Based']['Preferred_Loan_Tenure']}} Years</td>
                    </tr>
                    <tr>
                        <td width="5%"></td>
                        <td width="30%" colspan="2" class="gray_color">Monthly Installment</td>
                        <td>${{$data['Loan_Eligiblity_Preferred_Loan_Tenure_Based']['Monthly_Installment']}} </td>
                    </tr>
                </table>
                <table style="width:100%;line-height:1.7;page-break-inside: avoid;">

                    <tr>
                        <td width="5%"></td>
                        <td colspan="3"><h3 style="color: #DA3B3E">You can choose Pledge and/or UnPledge Funds to passTDSR/MSR</h3></td>
                    </tr>
                    <tr>
                        <td width="5%">
                        <td width="30%" colspan="2" class="gray_color">Full Pledge Amount</td>
                        <td>${{ number_format($data['Loan_Eligiblity_Preferred_Loan_Tenure_Based']['Full_Pledge_Amount'], 2)}} </td>
                    </tr>
                    <tr>
                        <td width="5%">
                        <td width="30%" colspan="2"><b>OR</b></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td width="5%">
                        <td width="30%" colspan="2" class="gray_color">Full UnPledge Amount</td>
                        <td>${{ number_format($data['Loan_Eligiblity_Preferred_Loan_Tenure_Based']['Full_Unpledge_Amount'], 2)}} </td>
                    </tr>
                    <tr>
                        <td width="5%">
                        <td width="30%" colspan="2"><b>OR</b></td>
                        <td></td>
                    </tr>

                    <tr>
                        <td width="5%">
                        <td width="30%"><span class="gray_color">Peferred Pledge Amount</span><br>{{$data['Loan_Eligiblity_Preferred_Loan_Tenure_Based']['Preferred_Pledge_Amount']}}</td>
                        <td width="5%"> +</td>
                        <td><span class="gray_color">UnPledge Amount</span><br>${{ number_format($data['Loan_Eligiblity_Preferred_Loan_Tenure_Based']['UnPledge_Amount'], 2)}} </td>
                    </tr>
                    <tr>
                        <td width="5%">
                        <td width="30%" colspan="2"><b>OR</b></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td width="5%">
                        <td width="30%"><span class="gray_color">Peferred UnPledge Amount</span><br>${{ number_format($data['Loan_Eligiblity_Preferred_Loan_Tenure_Based']['Preferred_UnPledge_Amount'], 2)}}</td>
                        <td width="5%"> +</td>
                        <td><span class="gray_color">Pledge Amount</span><br>${{ number_format($data['Loan_Eligiblity_Preferred_Loan_Tenure_Based']['Pledge_Amount'], 2)}} </td>
                    </tr>

                    <tr>
                        <td width="5%"></td>
                        <td colspan="3"><h3 style="color: #DA3B3E">Based on Your Current Financial Status</h3></td>
                    </tr>
                    <tr>
                        <td width="5%">
                        <td width="30%" colspan="2">Maximum Loan Qualified</td>
                        <td>${{ number_format($data['Loan_Eligiblity_Preferred_Loan_Tenure_Based']['Preffered_Loan_Qualified'], 2)}} </td>
                    </tr>
                    <tr>
                        <td width="5%">
                        <td width="30%" colspan="2">Maximum Property price that can be purchased</td>
                        <td>${{ number_format($data['Loan_Eligiblity_Preferred_Loan_Tenure_Based']['Maximum_Property_Price'], 2)}} </td>
                    </tr>
                </table>
                <br><br>
            @endif

            @if (in_array("Loan_eligibility_preferred_package", $export_fields))
                <div class="alert alert-danger" role="alert">
                    Loan Eligibility (Preferred Loan Tenure) Loan Package
                </div>


                <table id="rate-table">
                    <?php
                        $header = '';
                        $monthly_installment = '';
                        $total_payment = '';
                        $total_principal = '';
                        $total_interest = '';
                    ?>
                    @foreach ($data['Loan_Package_Preferred_Loan_Tenure_Based'] as $key=> $rate_val)
                    <?php
                        $header                 .= '<th>'.str_replace("_"," ",ucfirst($key)).' '.($rate_val['rate']).'%</th>';
                        $monthly_installment    .= '<td>$'.number_format($rate_val['Monthly_Installment'], 2).'</td>';
                        $total_payment          .= '<td>$'.$rate_val['Total_Payment'].'</td>';
                        $total_principal        .= '<td>$'.number_format($rate_val['Total_Principal'], 2).'</td>';
                        $total_interest         .= '<td>$'.$rate_val['Total_Interest'].'</td>';
                    $j = 1;
                    ?>
                    @endforeach
                    <tr><th></th><?php echo $header; ?></tr>
                    <tr><td>Monthly Installment</td><?php echo $monthly_installment; ?></tr>
                    <tr><td>Total Payment</td><?php echo $total_payment; ?></tr>
                    <tr><td>Total Principal</td><?php echo $total_principal; ?></tr>
                    <tr><td>Total Interest</td><?php echo $total_interest; ?></tr>
                </table>
                <br><br>

                @if (!empty($data['Lowest_fixed']))
            <div class="alert" role="alert" style="color: #e11c2c; font-weight:bold;">
                Lowest Fixed<hr>
            </div>

                <table style="width: 60%;margin:auto;" >
                    <tr>
                        <td>

                            <img style="width:100px;height:100px;" src="{{ $data['Lowest_fixed']['bank_logo'] }}" alt="">

                        </td>
                        <td colspan="2">
                            <h4>{{ $data['Lowest_fixed']['bank_name'] }}</h4>
                            <h4>
                                {{ ucfirst($data['Lowest_fixed']['rate_type']) }}
                                @if (!empty($data['Lowest_fixed']['details']['rate_category']))
                                    ({{ ucfirst($data['Lowest_fixed']['details']['rate_category']) }})
                                @endif

                            </h4>
                        </td>
                    </tr>
                </table>
                <table style="width: 70%;margin:auto;text-align:center" class="year-table">
                    <tr>
                        <td>
                            Year 1 :<b>{{ $data['Lowest_fixed']['rate_list'][0]/0.01 }}%</b>
                        </td>
                        <td>
                            Year 2 :<b>
                            @if (!empty($data['Lowest_fixed']['rate_list'][1]))
                                {{$data['Lowest_fixed']['rate_list'][1]/0.01}}
                            @else
                                {{$data['Lowest_fixed']['rate_list'][0]/0.01}}
                            @endif %</b>
                        </td>

                        <td>
                            Year 3 :<b>
                            @if (!empty($data['Lowest_fixed']['rate_list'][2]))
                                {{$data['Lowest_fixed']['rate_list'][2]/0.01}}
                            @else
                                {{$data['Lowest_fixed']['rate_list'][0]/0.01}}
                            @endif %</b>
                        </td>
                    </tr>
            </table>

            <br>
            <div class="alert_default" role="alert">
                Rates
            </div>

            <table class="yearly_rate-table">
                <?php
                        $max_key = count($data['Lowest_fixed']['details']['rates']) -1;
                    ?>
                        @for($c = 0; $c <= $max_key; $c++)
                        <?php
                            $header = ($c == 5)? 'Thereafter' : 'Year '.$c+1;
                        ?>

                            <tr><td>{{$header}}</td><td><span style="font-size: 14px;">{{ $data['Lowest_fixed']['details']['rates'][$max_key - $c]['reference_rate']['reference'] }} </span> <br>
                                @if ( $data['Lowest_fixed']['details']['rates'][$max_key - $c]['reference_rate']['rate_type'] != 'fixed')
                                  ({{ $data['Lowest_fixed']['details']['rates'][$max_key - $c]['reference_rate']['interest_rate'] }}%) {{ $data['Lowest_fixed']['details']['rates'][$max_key - $c]['reference_rate']['equation'] }} ({{ $data['Lowest_fixed']['details']['rates'][$max_key - $c]['bank_spread'] }}%) =
                                @endif
                                {{ $data['Lowest_fixed']['details']['rates'][$max_key - $c]['total_interest_rate'] }}%</td></tr>
                        @endfor

            </table>

            <br>

            <div class="alert_default" role="alert">
                Key Features
            </div>

            <table class="yearly_rate-table">
                <tr><td>Property Type</td><td>
                @if (is_array($data['Lowest_fixed']['details']['property_types']))
                    @foreach ($data['Lowest_fixed']['details']['property_types'] as $item)
                    @if(!empty(config('calculator.property_types_key')[$item]))
                        {{ config('calculator.property_types_key')[$item] }} <br>
                    @else
                        {{ ucfirst($item) }} <br>
                @endif
                    @endforeach
                @else
                    {{ ucfirst($data['Lowest_fixed']['details']['property_types']) }}
                @endif
                </td></tr>

                <tr><td>Property Status</td><td>
                    @if (is_array($data['Lowest_fixed']['details']['property_status']))
                        @foreach ($data['Lowest_fixed']['details']['property_status'] as $item)
                            {{ ucfirst($item) }} <br>
                        @endforeach
                    @else
                        {{ ucfirst($data['Lowest_fixed']['details']['property_status']) }}
                    @endif
                    </td></tr>

                <tr><td>Type of Loan</td><td>
                    @if (!empty($data['Lowest_fixed']['details']['loan_category']))
                    @foreach ($data['Lowest_fixed']['details']['loan_category'] as $val)
                    @if(!empty(config('calculator.loan_types_key')[$val]))
                            {{ config('calculator.loan_types_key')[$val] }} <br>
                        @else
                            {{ ucfirst($val) }} <br>
                        @endif
                    @endforeach
                @endif
                </td></tr>

                <tr><td>Lock in Period</td><td>{{$data['Lowest_fixed']['details']['lock_in_period']}} Years</td></tr>

                <tr><td>Min Loan Amount</td><td>${{$data['Lowest_fixed']['details']['min_loan_amount']}}</td></tr>

                <tr><td>Deposit to Place</td><td>{{$data['Lowest_fixed']['details']['deposit_to_place']}}</td></tr>
                <tr><td>Remarks for Applicants</td><td>
                @if (is_array($data['Lowest_fixed']['details']['remarks_for_client']))
                    @foreach ($data['Lowest_fixed']['details']['remarks_for_client'] as $item)
                        {{$item}} <br>
                    @endforeach
                @else
                    {{$data['Lowest_fixed']['details']['remarks_for_client']}}
                @endif
                </td></tr>
                <tr><td>Remarks for Broker</td><td>
                    @if (is_array($data['Lowest_fixed']['details']['remarks_for_broker']))
                    @foreach ($data['Lowest_fixed']['details']['remarks_for_broker'] as $item)
                        {{$item}} <br>
                    @endforeach
                @else
                    {{$data['Lowest_fixed']['details']['remarks_for_broker']}}
                @endif
                    </td></tr>
            </table>

            <br><br>
            <div class="alert_default" role="alert">
                Bank`s Subsidy
            </div>

            <table class="yearly_rate-table">
                <tr><td>Cash Rebate / Leagal Subsidy</td><td>

                    @if (is_array($data['Lowest_fixed']['details']['cash_rebate_legal_subsidy']))
                    @foreach ($data['Lowest_fixed']['details']['cash_rebate_legal_subsidy'] as $item)
                    <?php echo str_replace('&ge;','',htmlentities($item)); ?><br>
                    @endforeach
                @else
                    {{str_replace("?"," ",$data['Lowest_fixed']['details']['cash_rebate_legal_subsidy'])}}
                @endif

                </td></tr>

                <tr><td>Valuation Subsidy</td><td>
                    @if (is_array($data['Lowest_fixed']['details']['valuation_subsidy']))
                    @foreach ($data['Lowest_fixed']['details']['valuation_subsidy'] as $item)
                        {{$item}} <br>
                    @endforeach
                @else
                    {{$data['Lowest_fixed']['details']['valuation_subsidy']}}
                @endif
                </td></tr>

                <tr><td>Cash Rebate / Subsidy Clawback</td><td>{{$data['Lowest_fixed']['details']['cash_rebate_subsidy_clawback_period_years']}} Years</td></tr>

                <tr><td>Fire Insurance Subsidy</td><td>{{$data['Lowest_fixed']['details']['fire_insurance_subsidy']}}</td></tr>
            </table>

            <br>
            <div class="alert_default" role="alert">
                Early Repayment Penalty
            </div>

            <table class="yearly_rate-table">
                <tr><td>Partial Repayment Penalty</td><td>

                    {{$data['Lowest_fixed']['details']['partial_repayment_penalty']}}%

                </td></tr>

                <tr><td>Partial Repayment Penalty Remarks</td><td>

                    {{$data['Lowest_fixed']['details']['partial_repayment_penalty_remarks']}}

                </td></tr>

                <tr><td>Full Repayment Penalty</td><td>
                    {{$data['Lowest_fixed']['details']['full_repayment_penalty']}}%

                </td></tr>

                <tr><td>Full Repayment Penalty Remarks</td><td>
                    {{$data['Lowest_fixed']['details']['full_repayment_penalty_remarks']}}

                </td></tr>

                <tr><td>Cancellation Fees</td><td>{{$data['Lowest_fixed']['details']['cancellation_fee']}}</td></tr>
                <tr><td>Cancellation Fees Remarks</td><td>{{$data['Lowest_fixed']['details']['cancellation_fee_remarks']}}</td></tr>
                <tr><td>New Purchase Referral Fees</td><td>{{$data['Lowest_fixed']['details']['new_purchase_referral_fee']}}</td></tr>
                <tr><td>Refinance Referral Fees</td><td>{{$data['Lowest_fixed']['details']['refinance_referral_fee']}}</td></tr>

            </table>

            <br><br>
            @endif

            @if (!empty($data['Lowest_floating']))
            <div class="alert" role="alert" style="color: #e11c2c; font-weight:bold;">
                Lowest Floating<hr>
            </div>

                <table style="width: 60%;margin:auto;" >
                    <tr>
                        <td>

                            <img style="width:100px;height:100px;" src="{{ $data['Lowest_floating']['bank_logo'] }}" alt="">

                        </td>
                        <td colspan="2">
                            <h4>{{ $data['Lowest_floating']['bank_name'] }}</h4>
                            <h4>
                                {{ ucfirst($data['Lowest_floating']['details']['rate_type']) }}
                                @if (!empty($data['Lowest_floating']['details']['rate_category']))
                                    ({{ ucfirst($data['Lowest_floating']['details']['rate_category']) }})
                                @endif

                            </h4>
                        </td>
                    </tr>
                </table>
                <table style="width: 70%;margin:auto;text-align:center" class="year-table">
                    <tr>
                        <td>
                            Year 1 :<b>{{ $data['Lowest_floating']['rate_list'][0]/0.01 }}%</b>
                        </td>
                        <td>
                            Year 2 :<b>
                            @if (!empty($data['Lowest_floating']['rate_list'][1]))
                                {{$data['Lowest_floating']['rate_list'][1]/0.01}}
                            @else
                                {{$data['Lowest_floating']['rate_list'][0]/0.01}}
                            @endif %</b>
                        </td>

                        <td>
                            Year 3 :<b>
                            @if (!empty($data['Lowest_floating']['rate_list'][2]))
                                {{$data['Lowest_floating']['rate_list'][2]/0.01}}
                            @else
                                {{$data['Lowest_floating']['rate_list'][0]/0.01}}
                            @endif %</b>
                        </td>
                    </tr>
            </table>

            <br>
            <div class="alert_default" role="alert">
                Rates
            </div>

            <table class="yearly_rate-table">
                <?php
                        $max_key = count($data['Lowest_floating']['details']['rates']) -1;
                    ?>
                        @for($c = 0; $c <= $max_key; $c++)

                        <?php
                            $header = ($c == 5)? 'Thereafter' : 'Year '.$c+1;
                        ?>

                        <tr><td> {{$header}}</td><td> <span style="font-size: 14px;">{{ $data['Lowest_floating']['details']['rates'][$max_key - $c]['reference_rate']['reference'] }} </span><br>
                            @if ( $data['Lowest_floating']['details']['rates'][$max_key - $c]['reference_rate']['rate_type'] != 'fixed')

                              ({{ $data['Lowest_floating']['details']['rates'][$max_key - $c]['reference_rate']['interest_rate'] }}%) {{ $data['Lowest_floating']['details']['rates'][$max_key - $c]['reference_rate']['equation'] }} ({{ $data['Lowest_floating']['details']['rates'][$max_key - $c]['bank_spread'] }}%) =
                            @endif
                            {{ $data['Lowest_floating']['details']['rates'][$max_key - $c]['total_interest_rate'] }}%</td></tr>
                        @endfor



            </table>

            <br>

            <div class="alert_default" role="alert">
                Key Features
            </div>

            <table class="yearly_rate-table">
                <tr><td>Property Type</td><td>
                @if (is_array($data['Lowest_floating']['details']['property_types']))
                    @foreach ($data['Lowest_floating']['details']['property_types'] as $item)
                    @if(!empty(config('calculator.property_types_key')[$item]))
                        {{ config('calculator.property_types_key')[$item] }} <br>
                    @else
                        {{ ucfirst($item) }} <br>
                    @endif
                    @endforeach
                @else
                    {{ ucfirst($data['Lowest_floating']['details']['property_types']) }}
                @endif
                </td></tr>

                <tr><td>Property Status</td><td>
                    @if (is_array($data['Lowest_floating']['details']['property_status']))
                        @foreach ($data['Lowest_floating']['details']['property_status'] as $item)
                            {{ ucfirst($item) }} <br>
                        @endforeach
                    @else
                        {{ ucfirst($data['Lowest_floating']['details']['property_status']) }}
                    @endif
                    </td></tr>

                <tr><td>Type of Loan</td><td>
                    @if (!empty($data['Lowest_floating']['details']['loan_category']))
                    @foreach ($data['Lowest_floating']['details']['loan_category'] as $val)
                    @if(!empty(config('calculator.loan_types_key')[$val]))
                    {{ config('calculator.loan_types_key')[$val] }} <br>
                @else
                    {{ ucfirst($val) }} <br>
                @endif
                    @endforeach
                @endif
                </td></tr>

                <tr><td>Lock in Period</td><td>{{$data['Lowest_floating']['details']['lock_in_period']}} Years</td></tr>

                <tr><td>Min Loan Amount</td><td>${{$data['Lowest_floating']['details']['min_loan_amount']}}</td></tr>

                <tr><td>Deposit to Place</td><td>{{$data['Lowest_floating']['details']['deposit_to_place']}}</td></tr>
                <tr><td>Remarks for Applicants</td><td>
                @if (is_array($data['Lowest_floating']['details']['remarks_for_client']))
                    @foreach ($data['Lowest_floating']['details']['remarks_for_client'] as $item)
                        {{$item}} <br>
                    @endforeach
                @else
                    {{$data['Lowest_floating']['details']['remarks_for_client']}}
                @endif
                </td></tr>
                <tr><td>Remarks for Broker</td><td>
                    @if (is_array($data['Lowest_floating']['details']['remarks_for_broker']))
                    @foreach ($data['Lowest_floating']['details']['remarks_for_broker'] as $item)
                        {{$item}} <br>
                    @endforeach
                @else
                    {{$data['Lowest_floating']['details']['remarks_for_broker']}}
                @endif
                    </td></tr>
            </table>

            <br>
            <div class="alert_default" role="alert">
                Bank`s Subsidy
            </div>

            <table class="yearly_rate-table">
                <tr><td>Cash Rebate / Leagal Subsidy</td><td>



                    @if (is_array($data['Lowest_floating']['details']['cash_rebate_legal_subsidy']))
                    @foreach ($data['Lowest_floating']['details']['cash_rebate_legal_subsidy'] as $item)
                    <?php echo str_replace('&ge;','',htmlentities($item)); ?><br>
                    @endforeach
                @else
                    {{ str_replace("?"," ",$data['Lowest_floating']['details']['cash_rebate_legal_subsidy'])}}
                @endif

                </td></tr>

                <tr><td>Valuation Subsidy</td><td>
                    @if (is_array($data['Lowest_floating']['details']['valuation_subsidy']))
                    @foreach ($data['Lowest_floating']['details']['valuation_subsidy'] as $item)
                        {{$item}} <br>
                    @endforeach
                @else
                    {{$data['Lowest_floating']['details']['valuation_subsidy']}}
                @endif
                </td></tr>

                <tr><td>Cash Rebate / Subsidy Clawback</td><td>{{$data['Lowest_floating']['details']['cash_rebate_subsidy_clawback_period_years']}} Years</td></tr>

                <tr><td>Fire Insurance Subsidy</td><td>{{$data['Lowest_floating']['details']['fire_insurance_subsidy']}}</td></tr>
            </table>

            <br>
            <div class="alert_default" role="alert">
                Early Repayment Penalty
            </div>

            <table class="yearly_rate-table">
                <tr><td>Partial Repayment Penalty</td><td>

                    {{$data['Lowest_floating']['details']['partial_repayment_penalty']}}%

                </td></tr>
                <tr><td>Partial Repayment Penalty Remarks</td><td>

                    {{$data['Lowest_floating']['details']['partial_repayment_penalty_remarks']}}

                </td></tr>

                <tr><td>Full Repayment Penalty</td><td>
                    {{$data['Lowest_floating']['details']['full_repayment_penalty']}}%

                </td></tr>
                <tr><td>Full Repayment Penalty Remarks</td><td>
                    {{$data['Lowest_floating']['details']['full_repayment_penalty_remarks']}}

                </td></tr>

                <tr><td>Cancellation Fees</td><td>{{$data['Lowest_floating']['details']['cancellation_fee']}}</td></tr>

                <tr><td>Cancellation Fees Remarks</td><td>{{$data['Lowest_floating']['details']['cancellation_fee_remarks']}}</td></tr>

                <tr><td>New Purchase Referral Fees</td><td>{{$data['Lowest_floating']['details']['new_purchase_referral_fee']}}</td></tr>
                <tr><td>Refinance Referral Fees</td><td>{{$data['Lowest_floating']['details']['refinance_referral_fee']}}</td></tr>

            </table>

            <br><br>
            @endif

            <br><br>
        @endif


        @if (in_array("Loan_eligibility_preferred_mortgage", $export_fields))
            <div class="alert alert-danger" role="alert">
                Loan Eligibility (Preferred Loan Tenure) Mortgage Repayment For Lowest Fixed
            </div>

                    <table class="main-table" >
                        <!-- row 1 -->
                        <tr>
                            <th colspan="2">Year</th>
                            <th>Interest Rate</th>
                            <th>Installment Amount</th>
                            <th>Interest Repayment</th>
                            <th>Principal Repayment</th>
                            <th>Outstanding Amount</th>
                            <th>Cum Interest Paid</th>
                        </tr>
                        <?php $y = 0; $j = 1; ?>
                        @foreach ($data['Loan_Package_Preferred_Loan_Tenure_Based']['lowest_fixed']['Mortgage_Repayment_Table'] as $key=> $val)


                                <?php $style = ''; ?>
                                @if ($val['month'] == 12)
                                    <?php $style = 'background:lightgray;'; ?>
                                @endif
                                <tr style="{{$style}}">
                                    <td>@if ($y != $val['year'] )
                                        {{ $val['year']}}
                                    @endif</td>
                                    <td>{{ $val['month'] }}</td>
                                    <td>{{ $val['interest_rate']."%"}}</td>

                                    <td>${{ number_format($val['installment_amount'], 2)}}</td>

                                    <td>{{ "$".number_format($val['interest_repayment'], 2)}}</td>
                                    <td>{{ "$".number_format($val['principal_repayment'], 2)}}</td>
                                    <td>{{ "$".number_format($val['outstanding_amount'], 2)}}</td>
                                    <td>{{ "$".number_format($val['cum_interest_paid'], 2)}}</td>
                                </tr>
                                <?php $y = $val['year']; ?>

                            <?php $j++; ?>
                    @endforeach
                    </table>
                </div>

                <br><br>

            <div class="alert alert-danger" role="alert">
                Loan Eligibility (Preferred Loan Tenure) Mortgage Repayment For Lowest Floating
            </div>

                    <table class="main-table" >
                        <!-- row 1 -->
                        <tr>
                            <th colspan="2">Year</th>
                            <th>Interest Rate</th>
                            <th>Installment Amount</th>
                            <th>Interest Repayment</th>
                            <th>Principal Repayment</th>
                            <th>Outstanding Amount</th>
                            <th>Cum Interest Paid</th>
                        </tr>
                        <?php $y = 0; $j = 1; ?>
                        @foreach ($data['Loan_Package_Preferred_Loan_Tenure_Based']['lowest_floating']['Mortgage_Repayment_Table'] as $key=> $val)

                                <?php $style = ''; ?>
                                @if ($val['month'] == 12)
                                    <?php $style = 'background:lightgray;'; ?>
                                @endif
                                <tr style="{{$style}}">
                                    <td>@if ($y != $val['year'] )
                                        {{ $val['year']}}
                                    @endif</td>
                                    <td>{{ $val['month'] }}</td>
                                    <td>{{ $val['interest_rate']."%"}}</td>

                                    <td>${{ number_format($val['installment_amount'], 2)}}</td>

                                    <td>{{ "$".number_format($val['interest_repayment'], 2)}}</td>
                                    <td>{{ "$".number_format($val['principal_repayment'], 2)}}</td>
                                    <td>{{ "$".number_format($val['outstanding_amount'], 2)}}</td>
                                    <td>{{ "$".number_format($val['cum_interest_paid'], 2)}}</td>
                                </tr>
                                <?php $y = $val['year']; ?>

                            <?php $j++; ?>
                    @endforeach
                    </table>
                </div>
            <br><br>
        @endif
    @endif


</body>

</html>
