<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <style>
        .main-table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;
            border: 1px solid #ddd;
            /* margin-left: -4%; */
        }

        .main-table th,
        .main-table td {
            text-align: center;
            padding: 8px;
            border: 1px solid #ddd;
            color: #6F767E;
        }

        .main-table th {
            color: #DA3B3E;
            font-size: 1.125rem;
        }

        #rate-table th {
            color: #DA3B3E;
            font-size: 1.125rem;
        }

        #rate-table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 80%;
            border: 1px solid #ddd;
            margin-left: 10%;
        }
        .year-table
        {
            border-collapse: collapse;
        }
        .year-table td{

            /* border-spacing: 0; */

            border: 1px solid #D8E0F0;
            /* margin-left: 10%; */
            line-height: 1.5;
        }

        .yearly_rate-table{
            border-collapse: collapse;
            border-spacing: 0;
            width: 90%;
            border: 1px solid #ddd;
            margin-left: 5%;
        }

        .yearly_rate-table td {
            text-align: left;
            padding: 8px;
            border: 1px solid #ddd;
            /* color: #6F767E; */
        }

        #rate-table th {
            text-align: center;
            padding: 8px;
            border: 1px solid #ddd;
            color: #6F767E;
        }

        #rate-table td {
            text-align: left;
            padding: 8px;
            border: 1px solid #ddd;
            color: #6F767E;
        }

        #rate-table th {
            color: #DA3B3E;
            font-size: 1.125rem;
        }

        #progressive-rate-table th {
            color: #DA3B3E;
            font-size: 1.125rem;
        }

        #progressive-rate-table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 80%;
            border: 1px solid #ddd;
            margin-left: 10%;
        }

        #progressive-rate-table th,
        #progressive-rate-table td {
            text-align: center;
            padding: 8px;
            border: 1px solid #ddd;
            color: #6F767E;
        }

        #progressive-rate-table th {
            color: #DA3B3E;
            font-size: 1.125rem;
        }

        #loan-table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 30%;
            line-height: 2;
        }

        #mybank {
            margin-left: 25px;
        }

        #defaultImg {
            height: 60px;
        }

        .alert {
            position: relative;
            padding: 1rem 1rem;
            margin-bottom: 1rem;
            border: 1px solid transparent;
            border-radius: 0.25rem;
            font-size: 18px;
            font-weight: bold;
        }

        .alert-danger {
            color: #ffffff;
            background-color: #e11c2c;
            border-color: #e11c2c;
        }

        .alert_default {
            position: relative;
            color: #e11c2c;
            padding: 1rem 1rem;
            margin-bottom: 1rem;
            border: 1px solid transparent;
            border-radius: 0.25rem;
            font-size: 16px;
            font-weight: bold;
            background-color: lightgray;
            //border-color: #e11c2c;
        }

        .gray_color{
            color: #767676;
        }

    </style>
</head>

<body >

    <?php $datas ?>

    <div style="overflow-x:auto;" >

       <center> <img style="margin-left: 25px; margin-top:15px" src="assets/images/logo.png"/></center>

        <div>
            <br>
            <h2 style="color: #e11c2c; font-weight:bold;">Mortgage Repayment Report</h2>
        </div>

        @if (in_array("Loan Details", $export_fields))


            <div class="alert alert-danger" role="alert">
                Loan Details
            </div>
            <table id="loan-table">
                <tr>
                    <td>
                        Prepared for
                    </td>
                    <td>
                        {{ $datas['Loan Details']['Prepared For'] }}
                    </td>
                </tr>
                <tr>
                    <td>
                        Loan Amount
                    </td>
                    <td>
                        ${{ number_format($datas['Loan Details']['Loan Amount'],2) }}
                    </td>
                </tr>
                <tr>
                    <td>
                        Loan Tenure
                    </td>
                    <td>
                        {{ $datas['Loan Details']['Loan Tenure'] }} Years
                    </td>
                </tr>
            </table>
            <br><br>
        @endif

        @if (in_array("Interest Rate Comparison", $export_fields))
            <div class="alert alert-danger" role="alert">
                Rate Package Comparison
            </div>
            <table id="rate-table">
                <?php
                        $header = '';
                        $monthly_installment = '';
                        $total_payment = '';
                        $total_principal = '';
                        $total_interest = '';

                ?>
                @foreach ($datas['Interest Rate Comparison'] as $key=> $rate_val)
                <?php
                        $header                 .= '<th>'.$key.'</th>';
                        $monthly_installment    .= '<td>$'.number_format($rate_val['Monthly Installment'], 2).'</td>';
                        $total_payment          .= '<td>$'.number_format($rate_val['Total Payment'], 2).'</td>';
                        $total_principal        .= '<td>$'.number_format($rate_val['Total Principal'], 2).'</td>';
                        $total_interest         .= '<td>$'.number_format($rate_val['Total Interest'], 2).'</td>';

                ?>
                @endforeach
                <tr><th></th><?php echo $header; ?></tr>
                <tr><td>Monthly Installment</td><?php echo $monthly_installment; ?></tr>
                <tr><td>Total Payment</td><?php echo $total_payment; ?></tr>
                <tr><td>Total Principal</td><?php echo $total_principal; ?></tr>
                <tr><td>Total Interest</td><?php echo $total_interest; ?></tr>
            </table>


            <br><br>
        @endif

        <?php $j = 1; ?>

        @foreach ($datas['Mortgage Repayment'] as $key=> $payment_list)

        @if (in_array("Mortgage Repayment ".$j, $export_fields))
            <div class="alert alert-danger" role="alert">
                Mortgage Repayment Table (Based on Prefered Rate {{$j}} )
            </div>

                    <table style="margin-top: 25px;" class="main-table" >
                        <!-- row 1 -->
                        <tr>
                            <th colspan="2">Year</th>
                            <th>Interest Rate</th>
                            <th>Installment Amount</th>
                            <th>Interest Repayment</th>
                            <th>Principal Repayment</th>
                            <th>Outstanding Amount</th>
                            <th>Cum Interest Paid</th>

                        </tr>
                        <?php $y = 0; ?>
                        @foreach ($payment_list as $val)
                            <?php $style = ''; ?>
                            @if ($val['month'] == 12)
                                <?php $style = 'background:lightgray;'; ?>
                            @endif
                            <tr style="{{$style}}">
                                <td>@if ($y != $val['year'] )
                                    {{ $val['year']}}
                                @endif</td>
                                <td>{{ $val['month'] }}</td>
                                <td>{{ number_format($val['interest_rate'], 2)."%"}}</td>
                                <td>{{ "$".number_format($val['installment_amount'], 2)}}</td>
                                <td>{{ "$".number_format($val['interest_repayment'], 2)}}</td>
                                <td>{{ "$".number_format($val['principal_repayment'], 2)}}</td>
                                <td>{{ "$".number_format($val['outstanding_amount'], 2)}}</td>
                                <td>{{ "$".number_format($val['cum_interest_paid'], 2)}}</td>
                            </tr>
                            <?php $y = $val['year']; ?>
                        @endforeach

                    </table>
                </div>
            <br><br>
            @endif
            <?php $j++; ?>
        @endforeach



</body>

</html>
