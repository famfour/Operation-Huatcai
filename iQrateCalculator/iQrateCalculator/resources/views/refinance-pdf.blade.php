<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .main-table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;
            border: 1px solid #ddd;
            /* margin-left: -4%; */
        }

        .main-table th,
        .main-table td {
            text-align: center;
            padding: 8px;
            border: 1px solid #ddd;
            color: #6F767E;
        }

        .main-table th {
            color: #DA3B3E;
            font-size: 1.125rem;
        }

        #rate-table th {
            color: #DA3B3E;
            font-size: 1.125rem;
        }

        #rate-table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;
            border: 1px solid #ddd;
        }
        .year-table
        {
            border-collapse: collapse;
        }
        .year-table td{

            /* border-spacing: 0; */

            border: 1px solid #D8E0F0;
            /* margin-left: 10%; */
            line-height: 1.5;
        }

        .yearly_rate-table{
            border-collapse: collapse;
            border-spacing: 0;
            width: 90%;
            border: 1px solid #ddd;
            margin-left: 5%;
        }

        .yearly_rate-table td {
            text-align: left;
            padding: 8px;
            border: 1px solid #ddd;
            /* color: #6F767E; */
        }

        #rate-table th {
            text-align: center;
            padding: 8px;
            border: 1px solid #ddd;
            color: #6F767E;
        }

        #rate-table td {
            text-align: left;
            padding: 8px;
            border: 1px solid #ddd;
            color: #6F767E;
        }

        #rate-table th {
            color: #DA3B3E;
            font-size: 1.125rem;
        }

        #progressive-rate-table th {
            color: #DA3B3E;
            font-size: 1.125rem;
        }

        #progressive-rate-table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 80%;
            border: 1px solid #ddd;
            margin-left: 10%;
        }

        #progressive-rate-table th,
        #progressive-rate-table td {
            text-align: center;
            padding: 8px;
            border: 1px solid #ddd;
            color: #6F767E;
        }

        #progressive-rate-table th {
            color: #DA3B3E;
            font-size: 1.125rem;
        }

        #loan-table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 30%;
        }

        #mybank {
            margin-left: 25px;
        }

        #defaultImg {
            height: 60px;
        }

        .alert {
            position: relative;
            padding: 1rem 1rem;
            margin-bottom: 1rem;
            border: 1px solid transparent;
            border-radius: 0.25rem;
            font-size: 18px;
            font-weight: bold;
        }

        .alert-danger {
            color: #ffffff;
            background-color: #e11c2c;
            border-color: #e11c2c;
        }

        .alert_default {
            position: relative;
            color: #e11c2c;
            padding: 1rem 1rem;
            margin-bottom: 1rem;
            border: 1px solid transparent;
            border-radius: 0.25rem;
            font-size: 16px;
            font-weight: bold;
            background-color: lightgray;
            //border-color: #e11c2c;
        }

        .gray_color{
            color: #767676;
        }

    </style>
</head>

<body>



    <div style="overflow-x:auto;" >

       <center> <img style="margin-left: 25px; margin-top:15px" src="assets/images/logo.png"/></center>



        <div>
            <br>
            <h2 style="color: #e11c2c; font-weight:bold;">My Refinancing Report</h2>
        </div>

        @if (in_array("Loan_details", $export_fields))

            <div class="alert alert-danger" role="alert">
                Loan Details
            </div>
                <table id="loan-table" style="width:100%;line-height:2">
                    <tr>
                        <td width="5%"></td>
                        <td width="30%" class="gray_color" >
                            Prepared for
                        </td>
                        <td>
                            {{ $data['loan_details']['Prepared_for'] }}
                        </td>
                    </tr>
                    <tr>
                        <td width="5%"></td>
                        <td width="30%" class="gray_color" >
                            Outstanding Loan Amount
                        </td>
                        <td>
                            ${{ number_format($data['loan_details']['Outstanding_Loan_Amount'],2) }}
                        </td>
                    </tr>
                    <tr>
                        <td width="5%"></td>
                        <td width="30%" class="gray_color" >
                            Maximum Loan Tenure
                            </td>
                        <td>
                            {{ $data['loan_details']['Maximum_Loan_Tenure'] }} Years
                        </td>
                    </tr>
                    <tr>
                        <td width="5%"></td>
                        <td width="30%" class="gray_color" >
                            Preferred Loan Tenure
                            </td>
                        <td>
                            {{ $data['loan_details']['Prefered_Loan_Tenure'] }} Years
                        </td>
                    </tr>
                    <tr>
                        <td width="5%"></td>
                        <td width="30%" class="gray_color" >
                            Existing Bank
                            </td>
                        <td>
                            {{ $data['loan_details']['Existing_Bank'] }}
                        </td>
                    </tr>
                    <tr>
                        <td width="5%"></td>
                        <td width="30%" class="gray_color" >
                            Existing Interest Rate
                            </td>
                        <td>
                            {{ $data['loan_details']['Existing_Interest_Rate'] }}%
                        </td>
                    </tr>

                    <tr>
                        <td width="5%"></td>
                        <td width="30%" class="gray_color" >
                            Legal Fees Payable
                            </td>
                        <td>
                            ${{ number_format($data['your_saving_from_refinancing']['Legal_Fees'], 2) }}
                        </td>
                    </tr>
                    <tr>
                        <td width="5%"></td>
                        <td width="30%" class="gray_color" >
                            Valuation Fees
                            </td>
                        <td>
                            ${{ number_format($data['your_saving_from_refinancing']['Valuation_Fees'], 2) }}
                        </td>
                    </tr>

                </table>

                <br><br>
            @endif

        @if (in_array("Savings_from_refinancing", $export_fields))
            <div class="alert alert-danger" role="alert">
                Your Savings From Refinancing (Over 3 Years)
            </div>

                <br>
                <div class="alert" role="alert" style="color: #e11c2c; font-weight:bold;">
                    Based On Maximum Loan Tenure<hr>
                </div>
                <table id="rate-table">
                    <?php
                        $header = '';
                        $monthly_installment = '';
                        $total_principal = '';
                        $total_interest = '';
                        $subsidy = '';
                        $savings = '';
                    ?>
                    @foreach ($data['Package_details'] as $key=> $rate_val)
                    <?php

                        $header                 .= '<th>'.$rate_val['name'].' '.($rate_val['maximum_loan_tenure']['Rate']).'%</th>';
                        $monthly_installment    .= '<td>'.$rate_val['maximum_loan_tenure']['Monthly_Installment'].'</td>';
                        $total_principal        .= '<td>'.$rate_val['maximum_loan_tenure']['Total_Principal'].'</td>';
                        $total_interest         .= '<td>'.$rate_val['maximum_loan_tenure']['Total_Interest'].'</td>';
                        $subsidy         .= '<td>'.$rate_val['maximum_loan_tenure']['Cash_Subsidy'].'</td>';
                        $savings         .= '<td>'.$rate_val['maximum_loan_tenure']['Savings'].'</td>';
                    $j = 1;
                    ?>
                    @endforeach
                    <tr><th></th><?php echo $header; ?></tr>
                    <tr><td style="text-align: left;">Monthly Installment</td><?php echo $monthly_installment; ?></tr>
                    <tr><td style="text-align: left;">Total Principal</td><?php echo $total_principal; ?></tr>
                    <tr><td style="text-align: left;">Total Interest</td><?php echo $total_interest; ?></tr>
                    <tr><td style="text-align: left;">Cash/Rebate Subsidy</td><?php echo $subsidy; ?></tr>
                    <tr><td style="text-align: left;">Savings</td><?php echo $savings; ?></tr>
                </table>


                <br><br>
                @if ($prefered_tenure > 0)

                <div class="alert" role="alert" style="color: #e11c2c; font-weight:bold;">
                    Based On Preferred Loan Tenure<hr>
                </div>
                <table id="rate-table">
                    <?php

                        $header = '';
                        $monthly_installment = '';
                        $total_principal = '';
                        $total_interest = '';
                        $subsidy = '';
                        $savings = '';
                    ?>
                    @foreach ($data['Package_details'] as $key=> $rate_val)

                    <?php


                            $header                 .= '<th>'.$rate_val['name'].' '.($rate_val['prefered_loan_tenure']['Rate']).'%</th>';
                            $monthly_installment    .= '<td>'.$rate_val['prefered_loan_tenure']['Monthly_Installment'].'</td>';
                            $total_principal        .= '<td>'.$rate_val['prefered_loan_tenure']['Total_Principal'].'</td>';
                            $total_interest         .= '<td>'.$rate_val['prefered_loan_tenure']['Total_Interest'].'</td>';
                            $subsidy         .= '<td>'.$rate_val['prefered_loan_tenure']['Cash_Subsidy'].'</td>';
                            $savings         .= '<td>'.$rate_val['prefered_loan_tenure']['Savings'].'</td>';
                        $j = 1;

                    ?>
                    @endforeach
                    <tr><th></th><?php echo $header; ?></tr>
                    <tr><td style="text-align: left;">Monthly Installment</td><?php echo $monthly_installment; ?></tr>
                    <tr><td style="text-align: left;">Total Principal</td><?php echo $total_principal; ?></tr>
                    <tr><td style="text-align: left;">Total Interest</td><?php echo $total_interest; ?></tr>
                    <tr><td style="text-align: left;">Cash/Rebate Subsidy</td><?php echo $subsidy; ?></tr>
                    <tr><td style="text-align: left;">Savings</td><?php echo $savings; ?></tr>
                </table>
                <br><br>
                @endif

            @endif


            <?php $p = 1;  ?>
            @foreach ($data['Package_details'] as $key =>$val)
                @if ($val['name'] != 'Current' && !empty($val['selected_package_details'])   )
                    <div class="alert alert-danger" role="alert">
                        Selected Loan Package {{$p}}
                    </div>

                    <table style="width: 40%;margin:auto;" >
                        <tr>
                            <td>
                                @if ($val['selected_package_details']['mask'] == false)
                                    <img style="width:100px;height:60px;" src="{{ $val['selected_package_details']['bank']['logo'] }}" alt="">
                                @else
                                <img  src="assets/images/logo.png" alt="">
                                @endif
                            </td>
                            <td colspan="2">
                                @if ($val['selected_package_details']['mask'] == false)
                                    <h4>{{ $val['selected_package_details']['bank']['name'] }}</h4>
                                @else
                                    <h4>{{ $val['selected_package_details']['bank']['name_masked'] }}</h4>
                                @endif

                                <h4>
                                    {{ ucfirst($val['selected_package_details']['rate_type']) }}
                                    @if (!empty($val['selected_package_details']['rate_category']))
                                        ({{ ucfirst($val['selected_package_details']['rate_category']) }})
                                    @endif

                                </h4>
                            </td>
                        </tr>
                    </table>


                    <table style="width: 60%;margin:auto;text-align:center" class="year-table">
                        <?php $max_key = count($val['selected_package_details']['rates']) -1;
                              $year1 = $val['selected_package_details']['rates'][$max_key];
                              $year2 = ($max_key-1 >= 0)? $val['selected_package_details']['rates'][$max_key-1] : $val['selected_package_details']['rates'][$max_key];

                              $year3 = ($max_key-2 >= 0)? $val['selected_package_details']['rates'][$max_key-2] : $val['selected_package_details']['rates'][$max_key];

                        ?>
                        <tr >
                            <td>
                                Year 1 : <b>{{$year1['total_interest_rate']}}%</b>
                            </td>
                            <td>
                                Year 2 :<b>{{$year2['total_interest_rate']}}%</b>
                            </td>
                            <td>
                                Year 3 :<b>{{$year3['total_interest_rate']}} %</b>
                            </td>
                        </tr>
                    </table>


                    <br>
                    <div class="alert_default" role="alert">
                        Rates
                    </div>

                    <table class="yearly_rate-table">
                            <?php
                                $max_key = count($val['selected_package_details']['rates']) -1;
                            ?>
                                @for($c = 0; $c <= $max_key; $c++)

                                <?php
                                $header = ($c == 5)? 'Thereafter' : 'Year '.$c+1;
                            ?>

                                    <tr><td>{{$header}}</td><td> <span style="font-size: 14px;">{{ $val['selected_package_details']['rates'][$max_key-$c]['reference_rate']['reference'] }} </span> <br>

                                        @if ( $val['selected_package_details']['rates'][$max_key - $c]['reference_rate']['rate_type'] != 'fixed')


                                          ({{ $val['selected_package_details']['rates'][$max_key-$c]['reference_rate']['interest_rate'] }}%) {{ $val['selected_package_details']['rates'][$max_key-$c]['reference_rate']['equation'] }} ({{ $val['selected_package_details']['rates'][$max_key-$c]['bank_spread'] }}%) =
                                        @endif

                                        {{ $val['selected_package_details']['rates'][$max_key-$c]['total_interest_rate'] }}%</td></tr>
                                @endfor
                    </table>
                    <br>
                    <div class="alert_default" role="alert">
                        Key Features
                    </div>
                    <table class="yearly_rate-table">
                        <tr><td>Property Type</td><td>
                        @if (is_array($val['selected_package_details']['property_types']))
                            @foreach ($val['selected_package_details']['property_types'] as $item)
                                @if(!empty(config('calculator.property_types_key')[$item]))
                                {{ config('calculator.property_types_key')[$item] }} <br>
                            @else
                                {{ ucfirst($item) }} <br>
                            @endif
                            @endforeach
                        @else
                            {{ ucfirst($val['selected_package_details']['property_types']) }}
                        @endif
                        </td></tr>

                        <tr><td>Property Status</td><td>
                            @if (is_array($val['selected_package_details']['property_status']))
                                @foreach ($val['selected_package_details']['property_status'] as $item)
                                    {{ ucfirst($item) }} <br>
                                @endforeach
                            @else
                                {{ ucfirst($val['selected_package_details']['property_status']) }}
                            @endif
                            </td></tr>

                        <tr><td>Type of Loan</td><td>
                            @if (!empty($data['selected_package_details']['loan_category']))
                            @foreach ($data['selected_package_details']['loan_category'] as $val)
                            @if(!empty(config('calculator.loan_types_key')[$val]))
                            {{ config('calculator.loan_types_key')[$val] }} <br>
                        @else
                            {{ ucfirst($val) }} <br>
                        @endif
                            @endforeach
                        @endif

                        </td></tr>

                        <tr><td>Lock in Period</td><td>{{$val['selected_package_details']['lock_in_period']}} Years</td></tr>

                        <tr><td>Min Loan Amount</td><td>${{ number_format($val['selected_package_details']['min_loan_amount'], 2)}}</td></tr>

                        <tr><td>Deposit to Place</td><td>{{$val['selected_package_details']['deposit_to_place']}}</td></tr>
                        <tr><td>Remarks for Applicants</td><td>
                        @if (is_array($val['selected_package_details']['remarks_for_client']))
                            @foreach ($val['selected_package_details']['remarks_for_client'] as $item)
                                {{$item}} <br>
                            @endforeach
                        @else
                            {{$val['selected_package_details']['remarks_for_client']}}
                        @endif
                        </td></tr>
                        <tr><td>Remarks for Broker</td><td>
                            @if (is_array($val['selected_package_details']['remarks_for_broker']))
                            @foreach ($val['selected_package_details']['remarks_for_broker'] as $item)
                                {{$item}} <br>
                            @endforeach
                        @else
                            {{$val['selected_package_details']['remarks_for_broker']}}
                        @endif
                            </td></tr>
                    </table>

                    <br><br>
                    <div class="alert_default" role="alert">
                        Bank`s Subsidy
                    </div>

                    <table class="yearly_rate-table">
                        <tr><td>Cash Rebate / Leagal Subsidy</td><td>

                            @if (is_array($val['selected_package_details']['cash_rebate_legal_subsidy']))
                            @foreach ($val['selected_package_details']['cash_rebate_legal_subsidy'] as $item)
                            <?php echo str_replace('&ge;','',htmlentities($item)); ?><br>
                            @endforeach
                        @else
                            {{str_replace('&ge;','',htmlentities($val['selected_package_details']['cash_rebate_legal_subsidy'])) }}
                        @endif

                        </td></tr>

                        <tr><td>Valuation Subsidy</td><td>
                            @if (is_array($val['selected_package_details']['valuation_subsidy']))
                            @foreach ($val['selected_package_details']['valuation_subsidy'] as $item)
                                {{$item}} <br>
                            @endforeach
                        @else
                            {{$val['selected_package_details']['valuation_subsidy']}}
                        @endif
                        </td></tr>

                        <tr><td>Cash Rebate / Subsidy Clawback</td><td>{{$val['selected_package_details']['cash_rebate_subsidy_clawback_period_years']}} Years</td></tr>

                        <tr><td>Fire Insurance Subsidy</td><td>{{$val['selected_package_details']['fire_insurance_subsidy']}}</td></tr>
                    </table>

                    <br>
                    <div class="alert_default" role="alert">
                        Early Repayment Penalty
                    </div>

                    <table class="yearly_rate-table">
                        <tr><td>Partial Repayment Penalty</td><td>

                            {{$val['selected_package_details']['partial_repayment_penalty']}}%

                        </td></tr>
                        <tr><td>Partial Repayment Penalty Remarks</td><td>

                            {{$val['selected_package_details']['partial_repayment_penalty_remarks']}}

                        </td></tr>

                        <tr><td>Full Repayment Penalty</td><td>
                            {{$val['selected_package_details']['full_repayment_penalty']}}%

                        </td></tr>
                        <tr><td>Full Repayment Penalty Remarks</td><td>
                            {{$val['selected_package_details']['full_repayment_penalty_remarks']}}

                        </td></tr>

                        <tr><td>Cancellation Fees</td><td>{{$val['selected_package_details']['cancellation_fee']}}</td></tr>

                        <tr><td>Cancellation Fees Remarks</td><td>{{$val['selected_package_details']['cancellation_fee_remarks']}}</td></tr>
                        <tr><td>New Purchase Referral Fees</td><td>{{$val['selected_package_details']['new_purchase_referral_fee']}}</td></tr>
                        <tr><td>Refinance Referral Fees</td><td>{{$val['selected_package_details']['refinance_referral_fee']}}</td></tr>

                    </table>

                    <br><br>
                    <div class="alert" role="alert" style="color: #e11c2c; font-weight:bold;">
                        Mortgage Repayment (Based On Maximum Loan Tenure)<hr>
                    </div>

                    <table  class="main-table" >
                        <tr>
                            <th colspan="2">Year</th>
                            <th>Interest Rate</th>
                            <th>Installment Amount</th>
                            <th>Interest Repayment</th>
                            <th>Principal Repayment</th>
                            <th>Outstanding Amount</th>
                            <th>Cum Interest Paid</th>
                        </tr>
                        <?php $y = 0; $j = 1; ?>

                        @foreach ($val['maximum_loan_tenure']['Mortgage_Payment_List'] as $key=> $mort_val)
                                <?php $style = ''; ?>
                                @if ($mort_val['month'] == 12)
                                    <?php $style = 'background:lightgray;'; ?>
                                @endif
                                <tr style="{{$style}}">
                                    <td>@if ($y != $mort_val['year'] )
                                        {{ $mort_val['year']}}
                                    @endif</td>
                                    <td>{{ $mort_val['month'] }}</td>
                                    <td>{{ $mort_val['interest_rate']."%"}}</td>

                                    <td>${{ number_format($mort_val['installment_amount'], 2)}}</td>

                                    <td>{{ "$".number_format($mort_val['interest_repayment'], 2)}}</td>
                                    <td>{{ "$".number_format($mort_val['principal_repayment'], 2)}}</td>
                                    <td>{{ "$".number_format($mort_val['outstanding_amount'], 2)}}</td>
                                    <td>{{ "$".number_format($mort_val['cum_interest_paid'], 2)}}</td>
                                </tr>
                                <?php $y = $mort_val['year']; ?>
                            <?php $j++; ?>
                    @endforeach
                    </table>

                    @if ($prefered_tenure > 0)
                    <br><br>
                    <div class="alert" role="alert" style="color: #e11c2c; font-weight:bold;">
                        Mortgage Repayment (Based On Preferred Loan Tenure)<hr>
                    </div>

                    <table  class="main-table" >
                        <tr>
                            <th colspan="2">Year</th>
                            <th>Interest Rate</th>
                            <th>Installment Amount</th>
                            <th>Interest Repayment</th>
                            <th>Principal Repayment</th>
                            <th>Outstanding Amount</th>
                            <th>Cum Interest Paid</th>
                        </tr>
                        <?php $y = 0; $j = 1; ?>
                        @foreach ($val['prefered_loan_tenure']['Mortgage_Payment_List'] as $key=> $mort_val)
                                <?php $style = ''; ?>
                                @if ($mort_val['month'] == 12)
                                    <?php $style = 'background:lightgray;'; ?>
                                @endif
                                <tr style="{{$style}}">
                                    <td>@if ($y != $mort_val['year'] )
                                        {{ $mort_val['year']}}
                                    @endif</td>
                                    <td>{{ $mort_val['month'] }}</td>
                                    <td>{{ $mort_val['interest_rate']."%"}}</td>

                                    <td>${{ number_format($mort_val['installment_amount'], 2)}}</td>

                                    <td>{{ "$".number_format($mort_val['interest_repayment'],2)}}</td>
                                    <td>{{ "$".number_format($mort_val['principal_repayment'], 2)}}</td>
                                    <td>{{ "$".number_format($mort_val['outstanding_amount'], 2)}}</td>
                                    <td>{{ "$".number_format($mort_val['cum_interest_paid'], 2)}}</td>
                                </tr>
                                <?php $y = $mort_val['year']; ?>
                            <?php $j++; ?>
                    @endforeach
                    </table>
                    @endif

                    <br><br>

                @endif

                @if ($val['name'] != 'Current')
                    <?php $p++; ?>
                @endif
            @endforeach
</body>

</html>
