<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <style>
        .main-table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;
            border: 1px solid #ddd;
            /* margin-left: -4%; */
        }

        .main-table th,
        .main-table td {
            text-align: center;
            /* padding: 8px; */
            border: 1px solid #ddd;
            color: #6F767E;
        }

        .main-table th {
            color: #DA3B3E;
            font-size: 1.125rem;
        }

        #rate-table th {
            color: #DA3B3E;
            font-size: 1.125rem;
        }

        #rate-table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;
            border: 1px solid #ddd;
            /* margin:auto; */
            /* margin-left: 10%; */
        }
        .year-table
        {
            border-collapse: collapse;
        }
        .year-table td{

            /* border-spacing: 0; */

            border: 1px solid #D8E0F0;
            /* margin-left: 10%; */
            line-height: 1.5;
        }

        .yearly_rate-table{
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;
            border-bottom: 1px solid #ddd;
            /* margin-left: 5%; */
        }

        .yearly_rate-table td {
            text-align: left;
            padding: 8px;
            border-bottom: 1px solid #ddd;
            /* color: #6F767E; */
        }

        #rate-table th {
            text-align: center;
            padding: 8px;
            border: 1px solid #ddd;
            color: #6F767E;
        }

        #rate-table td {
            text-align: left;
            padding: 8px;
            border: 1px solid #ddd;
            color: #6F767E;
        }

        #rate-table th {
            color: #DA3B3E;
            font-size: 1.125rem;
        }

        #progressive-rate-table th {
            color: #DA3B3E;
            font-size: 1.125rem;
        }

        #progressive-rate-table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;
            /* border: 1px solid #ddd; */
            /* margin-left: 10%; */
        }

        #progressive-rate-table th,
        #progressive-rate-table td {
            text-align: center;
            padding: 8px;
            border: 1px solid #ddd;
            color: #6F767E;
        }

        #progressive-rate-table th {
            color: #DA3B3E;
            font-size: 1.125rem;
        }

        #loan-table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;
            line-height: 2;
        }

        #mybank {
            margin-left: 25px;
        }

        #defaultImg {
            height: 60px;
        }

        .alert {
            position: relative;
            padding: 1rem 1rem;
            margin-bottom: 1rem;
            border: 1px solid transparent;
            border-radius: 0.25rem;
            font-size: 18px;
            font-weight: bold;
        }

        .alert-danger {
            color: #ffffff;
            background-color: #e11c2c;
            border-color: #e11c2c;
        }

        .alert_default {
            position: relative;
            color: #e11c2c;
            padding: 1rem 1rem;
            margin-bottom: 1rem;
            border: 1px solid transparent;
            border-radius: 0.25rem;
            font-size: 16px;
            font-weight: bold;
            background-color: lightgray;
            //border-color: #e11c2c;
        }

        .gray_color{
            color: #767676;
        }

    </style>
</head>

<body >

    <div style="overflow-x:auto;" >

       <center> <img style="margin-left: 25px; margin-top:15px" src="assets/images/logo.png"/></center>

        <div>
            <br>
            <h2 style="color: #e11c2c; font-weight:bold;">Buc Mortgage Repayment Report</h2>
        </div>

        @if (in_array("Loan Details", $export_fields))
            <div class="alert alert-danger" role="alert">
                Loan Details
            </div>
            <table id="loan-table">
                <tr>
                    <td width="5%"></td>
                    <td width="30%" class="gray_color" >
                        Prepared for
                    </td>
                    <td>
                    {{ $datas['Loan Details']['Prepared For'] }}
                    </td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td width="30%" class="gray_color" >
                        Property Price
                    </td>
                    <td>
                        ${{ number_format($datas['Loan Details']['Property Price'], 2) }}
                    </td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td width="30%" class="gray_color" >
                        Loan-To-Value
                        </td>
                    <td>
                        {{ $datas['Loan Details']['Loan-To-Value'] }}%
                    </td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td width="30%" class="gray_color" >
                        CPF Available
                        </td>
                    <td>
                        {{ $datas['Loan Details']['CPF Available'] }}
                    </td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td width="30%" class="gray_color" >
                        Downpayment
                        </td>
                    <td>
                        ${{ number_format($datas['Loan Details']['Downpayment'], 2) }}
                    </td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td width="30%" class="gray_color" >
                        Loan Amount
                        </td>
                    <td>
                        ${{ number_format($datas['Loan Details']['Loan Amount'], 2) }}
                    </td>
                </tr>
            </table>

            <br><br><br>
        @endif

        @if (in_array("Rate Comparison", $export_fields))
            <div class="alert alert-danger" role="alert">Rate Package Comparison</div>
            <table id="rate-table">
                <?php
                    $header = '';
                    $monthly_installment = '';
                    $total_payment = '';
                    $total_principal = '';
                    $total_interest = '';
                    $year1 = '';
                    $year2 = '';
                    $year3 = '';
                    $year4 = '';
                    $year5 = '';
                    $year6 = '';

                ?>
                @foreach ($datas['Rate Package Comparison'] as $key=> $rate_val)
                <?php

                    $header                 .= '<th>'.$key.'</th>';
                    $total_payment          .= '<td>$'.$rate_val['Total Payment'].'</td>';
                    $total_principal        .= '<td>$'.$rate_val['Total Principal'].'</td>';
                    $total_interest         .= '<td>$'.$rate_val['Total Interest'].'</td>';
                    $default = 1;
                    if(!empty($rate_val['Yearly Interest'][0]))
                    {
                        $year1                  .= '<td>'.$rate_val['Yearly Interest'][0].'%</td>';
                        $default = $rate_val['Yearly Interest'][0];
                    }else{
                        $year1                  .= '<td>'.$default.'%</td>';
                    }
                    if(!empty($rate_val['Yearly Interest'][1]))
                    {
                        $year2                  .= '<td>'.$rate_val['Yearly Interest'][1].'%</td>';
                        $default = $rate_val['Yearly Interest'][1];
                    }else{
                        $year2                  .= '<td>'.$default.'%</td>';
                    }
                    if(!empty($rate_val['Yearly Interest'][2]))
                    {
                        $year3                  .= '<td>'.$rate_val['Yearly Interest'][2].'%</td>';
                        $default = $rate_val['Yearly Interest'][2];
                    }else{
                        $year3                  .= '<td>'.$default.'%</td>';
                    }
                    if(!empty($rate_val['Yearly Interest'][3]))
                    {
                        $year4                  .= '<td>'.$rate_val['Yearly Interest'][3].'%</td>';
                    }else{
                        $year4                  .= '<td>'.$default.'%</td>';
                    }
                    if(!empty($rate_val['Yearly Interest'][4]))
                    {
                        $year5                  .= '<td>'.$rate_val['Yearly Interest'][4].'%</td>';
                    }else{
                        $year5                  .= '<td>'.$default.'%</td>';
                    }
                    if(!empty($rate_val['Yearly Interest'][5]))
                    {
                        $year6                  .= '<td>'.$rate_val['Yearly Interest'][5].'%</td>';
                    }else{
                        $year6                  .= '<td>'.$default.'%</td>';
                    }
                ?>
                @endforeach
                <tr><th></th><?php echo $header; ?></tr>
                <tr><td>Total Payment</td><?php echo $total_payment; ?></tr>
                <tr><td>Total Principal</td><?php echo $total_principal; ?></tr>
                <tr><td>Total Interest</td><?php echo $total_interest; ?></tr>
                    <tr><td>Year 1</td><?php echo $year1; ?></tr>

                    <tr><td>Year 2</td><?php echo $year2; ?></tr>
                    <tr><td>Year 3</td><?php echo $year3; ?></tr>
                    <tr><td>Year 4</td><?php echo $year4; ?></tr>
                    <tr><td>Year 5</td><?php echo $year5; ?></tr>

                    <tr><td>Thereafter</td><?php echo $year6; ?></tr>
            </table>
            <br><br><br>

            @if (!empty($datas['IQrate Lowest Buc Package']))
            <div class="alert" role="alert" style="color: #e11c2c; font-weight:bold;">IQrate Lowest BUC Package<hr>
            </div>
            <table style="width: 60%;margin:auto;" >
                <tr>
                    <td>
                        @if ($datas['IQrate Lowest Buc Package']['details']->mask == false)
                                <img style="width: 100px;height:50px;" src="{{ $datas['IQrate Lowest Buc Package']['details']->bank->logo }}" alt="">
                            @else
                                <img  src="assets/images/logo.png" alt="">
                        @endif


                    </td>
                    <td colspan="2">
                        @if ($datas['IQrate Lowest Buc Package']['details']->mask == false)
                                <h4>{{ $datas['IQrate Lowest Buc Package']['details']->bank->name }}</h4>
                            @else
                                <h4>{{ $datas['IQrate Lowest Buc Package']['details']->bank->name_masked }}</h4>
                        @endif
                    </td>
                </tr>
            </table>

            <table style="width: 70%;margin:auto;text-align:center" class="year-table">
                <tr >
                    <td>
                        Year 1 :  <b>{{ $datas['IQrate Lowest Buc Package']['year1'] }}%</b>
                    </td>
                    <td>
                        Year 2 :  <b>{{ $datas['IQrate Lowest Buc Package']['year2'] }}%</b>
                    </td>

                    <td>
                        Year 3 :  <b>{{ $datas['IQrate Lowest Buc Package']['year3'] }}% </b>
                    </td>
                </tr>
            </table>

            <br>
            <div class="alert_default" role="alert">
                Rates
            </div>

            <table class="yearly_rate-table">


                <?php
                        $max_key = count($datas['IQrate Lowest Buc Package']['details']->rates) -1;
                    ?>
                        @for($c = 0; $c <= $max_key; $c++)

                            <?php
                                $header = ($c == 5)? 'Thereafter' : 'Year '.$c+1;
                            ?>

                            <tr><td>{{$header}}</td><td><span style="font-size: 14px;">{{ $datas['IQrate Lowest Buc Package']['details']->rates[$max_key - $c]->reference_rate->reference }}</span><br>
                                @if ($datas['IQrate Lowest Buc Package']['details']->rates[$max_key - $c]->reference_rate->rate_type != 'fixed')
                                ({{ number_format($datas['IQrate Lowest Buc Package']['details']->rates[$max_key - $c]->bank_spread, 2) }})%{{$datas['IQrate Lowest Buc Package']['details']->rates[$max_key - $c]->reference_rate->equation}}{{ number_format($datas['IQrate Lowest Buc Package']['details']->rates[$max_key - $c]->reference_rate->interest_rate, 2) }}%=
                                @endif
                                {{ number_format($datas['IQrate Lowest Buc Package']['details']->rates[$max_key - $c]->total_interest_rate, 2) }}%</td></tr>
                        @endfor
            </table>
            <br>
            <div class="alert_default" role="alert">
                Key Features
            </div>

            <table class="yearly_rate-table">
                <tr><td>Property Type</td><td>
                @if (is_array($datas['IQrate Lowest Buc Package']['details']->property_types))
                    @foreach ($datas['IQrate Lowest Buc Package']['details']->property_types as $item)
                        {{ ucfirst(str_replace('_',' ',$item)) }} <br>
                    @endforeach
                @else
                    {{ ucfirst(str_replace('_',' ',$datas['IQrate Lowest Buc Package']['details']->property_types)) }}
                @endif
                </td></tr>



                <tr><td>Type of Loan</td><td>New Purcahse / Refinance</td></tr>

                <tr><td>Lock in Period</td><td>{{$datas['IQrate Lowest Buc Package']['details']->lock_in_period}}</td></tr>

                <tr><td>Min Loan Amount</td><td>{{$datas['IQrate Lowest Buc Package']['details']->min_loan_amount}}</td></tr>

                <tr><td>Deposit to Place</td><td>{{$datas['IQrate Lowest Buc Package']['details']->deposit_to_place}}</td></tr>
                <tr><td>Remarks for Applicants</td><td>
                @if (is_array($datas['IQrate Lowest Buc Package']['details']->remarks_for_client))
                    @foreach ($datas['IQrate Lowest Buc Package']['details']->remarks_for_client as $item)
                        {{$item}} <br>
                    @endforeach
                @else
                    {{$datas['IQrate Lowest Buc Package']['details']->remarks_for_client}}
                @endif
                </td></tr>
                <tr><td>Remarks for Broker</td><td>
                    @if (is_array($datas['IQrate Lowest Buc Package']['details']->remarks_for_broker))
                    @foreach ($datas['IQrate Lowest Buc Package']['details']->remarks_for_broker as $item)
                        {{$item}} <br>
                    @endforeach
                @else
                    {{$datas['IQrate Lowest Buc Package']['details']->remarks_for_broker}}
                @endif
                    </td></tr>
            </table>

            <br><br>
            <div class="alert_default" role="alert">
                Bank`s Subsidy
            </div>

            <table class="yearly_rate-table">
                <tr><td>Cash Rebate / Leagal Subsidy</td><td>

                    @if (is_array($datas['IQrate Lowest Buc Package']['details']->cash_rebate_legal_subsidy))
                    @foreach ($datas['IQrate Lowest Buc Package']['details']->cash_rebate_legal_subsidy as $item)
                        {{$item}} <br>
                    @endforeach
                @else
                    {{$datas['IQrate Lowest Buc Package']['details']->cash_rebate_legal_subsidy}}
                @endif

                </td></tr>

                <tr><td>Valuation Subsidy</td><td>
                    @if (is_array($datas['IQrate Lowest Buc Package']['details']->valuation_subsidy))
                    @foreach ($datas['IQrate Lowest Buc Package']['details']->valuation_subsidy as $item)
                        {{$item}} <br>
                    @endforeach
                @else
                    {{$datas['IQrate Lowest Buc Package']['details']->valuation_subsidy}}
                @endif
                </td></tr>

                <tr><td>Cash Rebate / Subsidy Clawback</td><td>{{$datas['IQrate Lowest Buc Package']['details']->cash_rebate_subsidy_clawback_period_years}}</td></tr>

                <tr><td>Fire Insurance Subsidy</td><td>{{$datas['IQrate Lowest Buc Package']['details']->fire_insurance_subsidy}}</td></tr>
            </table>

            <br>
            <div class="alert_default" role="alert">
                Early Repayment Penality
            </div>

            <table class="yearly_rate-table">
                <tr><td>Partial Repayment Penality</td><td>

                    {{$datas['IQrate Lowest Buc Package']['details']->partial_repayment_penalty}}%
                    <br>
                    {{$datas['IQrate Lowest Buc Package']['details']->partial_repayment_penalty_remarks}}

                </td></tr>

                <tr><td>Full Repayment Penality</td><td>
                    {{$datas['IQrate Lowest Buc Package']['details']->full_repayment_penalty}}%
                    <br>
                    {{$datas['IQrate Lowest Buc Package']['details']->full_repayment_penalty_remarks}}
                </td></tr>

                <tr><td>Cancellation Fees</td><td>{{$datas['IQrate Lowest Buc Package']['details']->cancellation_fee}}
                    <br>

                </td></tr>

            </table>

            <br><br>
            @endif

    @endif



    @if (in_array("Disbursement Schedule", $export_fields))
        <div class="alert alert-danger" role="alert">
        Progressive Repayment Schedule
        </div>
        <table id="progressive-rate-table">
              <tr><th>Disbursement Stage</th><th>Disbursement Range</th><th>%</th><th>Disbursement Amount</th><th>Payment Mode</th><th>Monthly Installment (IQrate Exclusive Buc rate)</th>
                @if ($datas['prefered_rate_enable'] == 'yes')
                    <th>Monthly Installment (Your Prefered Interest Rate)</th>
                @endif
                </tr>
              @foreach ($datas['Disbursement Schedule'] as $key=> $disbursement)

              <tr>
                <td>{{ $disbursement['Disbursement Stage'] }}</td>
                <td>{{ $disbursement['Disbursement Range'] }}</td>
                <td>{{ $disbursement['%'] }}%</td>
                @if (is_array($disbursement['Disbursement Amount']))
                    <td><div>${{ $disbursement['Disbursement Amount'][0] }}</div><div>${{ $disbursement['Disbursement Amount'][1] }}</div></td>
                @else
                    <td>${{ $disbursement['Disbursement Amount'] }}</td>
                @endif


                @if (is_array($disbursement['Payment Mode']))
                    <td>
                        @foreach ($disbursement['Payment Mode'] as $pay_name=>$amount)
                        <div>{{ $pay_name.' - '.$amount }}</div>
                        @endforeach
                    </td>
                @else
                    <td>{{ $disbursement['Payment Mode'] }}</td>
                @endif

                @if (is_array($disbursement['Monthly Installment based on IQrate']))
                    <td><div>${{ $disbursement['Monthly Installment based on IQrate'][0] }}</div><div>${{ $disbursement['Monthly Installment based on IQrate'][1] }}</div></td>
                @else
                    <td>${{ $disbursement['Monthly Installment based on IQrate'] }}</td>
                @endif

                @if ($datas['prefered_rate_enable'] == 'yes')
                    @if (is_array($disbursement['Monthly Installment based on Prefered']))
                        <td><div>${{ $disbursement['Monthly Installment based on Prefered'][0] }}</div><div>${{ $disbursement['Monthly Installment based on Prefered'][1] }}</div></td>
                    @else
                        <td>${{ $disbursement['Monthly Installment based on Prefered'] }}</td>
                    @endif
                @endif
              </tr>
              @endforeach
        </table>

        <br><br>
    @endif



    @if (in_array("Progressive Repayment", $export_fields))
        <div class="alert alert-danger" role="alert">
            Buc Mortgage Repayment Table (IQrate Exclusive BUC Rate )
        </div>

        <table style="margin-top: 25px;" class="main-table" >
            <!-- row 1 -->
            <tr>
                <th>Year</th>
                <th>Month</th>
                <th>Interest Rate</th>
                <th>Disbursement Ratio (Property Price (%))</th>
                <th>Outstanding Principal (Start of month)</th>
                <th>Monthly Installment</th>
                <th>Interest Payment</th>
                <th>Principal Payment</th>
                <th>Outstanding Principal (End of Month)</th>

            </tr>
            <?php $y = 0; $disbursement = 0; ?>
            @foreach ($datas['IQrate Exclusive BUC Rate'] as $key=> $payment_list)

                @foreach ($payment_list as $val)

                    <?php $style = ''; ?>
                    @if ($val['month'] == 12)
                        <?php $style = 'background:lightgray;'; ?>
                    @endif
                    <tr style="{{$style}}">
                        <td>@if ($y != $val['year'] )
                            {{ $val['year']}}
                        @endif</td>
                        <td>{{ $val['month'] }}</td>
                        <td>{{ $val['interest_rate']."%"}}</td>

                        <td>@if ($disbursement != $val['disbursement_name'] )
                            {{ $val['disbursement_name'].' - '.$val['disbursement_ratio']}}
                        @endif</td>

                        <td>{{ "$".number_format($val['outstanding_principal_start'], 2)}}</td>
                        <td>{{ "$".number_format($val['monthly_installment'], 2)}}</td>
                        <td>{{ "$".$val['interest_payment']}}</td>
                        <td>{{ "$".$val['principal_payment']}}</td>
                        <td>{{ "$".$val['outstanding_principal_end']}}</td>
                    </tr>
                    <?php $y = $val['year']; $disbursement = $val['disbursement_name']; ?>

                @endforeach
            @endforeach
        </table>
        <br><br>

    @endif

        @if ($datas['prefered_rate_enable'] == 'yes')

        @if (in_array("Preferred Repayment", $export_fields))
        <div class="alert alert-danger" role="alert">
            Buc Mortgage Repayment Table (Preferred Exclusive BUC Rate )
        </div>

        <table style="margin-top: 25px;" class="main-table" >
            <!-- row 1 -->
            <tr>
                <th>Year</th>
                <th>Month</th>
                <th>Interest Rate</th>
                <th>Disbursement Ratio (Property Price (%))</th>
                <th>Outstanding Principal (Start of month)</th>
                <th>Monthly Installment</th>
                <th>Interest Payment</th>
                <th>Principal Payment</th>
                <th>Outstanding Principal (End of Month)</th>

            </tr>
            <?php $y = 0; $disbursement = 0; ?>
            @foreach ($datas['Prefered BUC Rate'] as $key=> $payment_list)

                @foreach ($payment_list as $val)

                    <?php $style = ''; ?>
                    @if ($val['month'] == 12)
                        <?php $style = 'background:lightgray;'; ?>
                    @endif
                    <tr style="{{$style}}">
                        <td>@if ($y != $val['year'] )
                            {{ $val['year']}}
                        @endif</td>
                        <td>{{ $val['month'] }}</td>
                        <td>{{ $val['interest_rate']."%"}}</td>

                        <td>@if ($disbursement != $val['disbursement_name'] )
                            {{ $val['disbursement_name'].' - '.$val['disbursement_ratio']}}
                        @endif</td>

                        <td>{{ "$".number_format($val['outstanding_principal_start'], 2)}}</td>
                        <td>{{ "$".number_format($val['monthly_installment'], 2)}}</td>
                        <td>{{ "$".$val['interest_payment']}}</td>
                        <td>{{ "$".$val['principal_payment']}}</td>
                        <td>{{ "$".$val['outstanding_principal_end']}}</td>
                    </tr>
                    <?php $y = $val['year']; $disbursement = $val['disbursement_name']; ?>

                @endforeach
            @endforeach
        </table>
        <br><br>

        @endif
        @endif





</body>

</html>
