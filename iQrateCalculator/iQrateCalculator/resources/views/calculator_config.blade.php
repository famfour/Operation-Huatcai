<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<body>

    <div class="container">
        <h3>Calculator Configuration</h3>
        <button type="button" class="pull-right btn btn-info btn-md" data-toggle="modal" data-target="#calculatorConfig">Add</button>

    <table border="1" class="table">
        <thead>
            <tr>
                <th>Key</th>
                <th>Value</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($calculator_config as $key => $value)
                <tr>
                    <td>{{ $value->key }}</td>
                    <td>{{ $value->value }}</td>
                    <td><a href="#" class="update" edit_id="{{ $value->id }}" href="{{ url('calculator-config-update') }}">Edit</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    </div>

    <!-- Modal -->
    <div id="calculatorConfig" class="modal fade" role="dialog">
        <div class="modal-dialog">
    
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Calculator Configuration</h4>
            </div>
            <div class="modal-body">
                <form class="form" method="POST" action="{{ url('calculator-config') }}">
                    {{-- @csrf --}}
                    <div class="form-group">
                        <label>Key</label>
                        <input type="input" name="key" class="form-control" id="key" placeholder="Key" required>
                    </div>
                    <div class="form-group">
                        <label>Value</label>
                        <input type="input" name="value" class="form-control" id="value" placeholder="value" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    
        </div>
    </div>

<script>
    $('.update').click(function(){
    
        var edit_id = $(this).attr('edit_id');
        
        $.ajax
        ({ 
            url: '{{ url("calculator-config-edit") }}/'+edit_id,
            // data: {"bookID": book_id},
            type: 'post',
            success: function(result)
            {
                console.log(result);
                $('#key').val(result.key)
                $('#value').val(result.value)
                $('<input>').attr({
                    type: 'hidden',
                    id: 'edit_id',
                    name: 'edit_id'
                }).appendTo('form').val(result.id);

                $('#calculatorConfig').modal('show');
                // $('.modal-box').text(result).fadeIn(700, function() 
                // {
                //     setTimeout(function() 
                //     {
                //         $('.modal-box').fadeOut();
                //     }, 2000);
                // });
            }
        });
    });
    </script>
</body>
</html>
