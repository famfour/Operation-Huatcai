<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->get('/key', function() {
    return \Illuminate\Support\Str::random(32);
});
$router->get('/calculator-config', ['uses' => 'CalculatorConfigController@index']);
$router->post('/calculator-config', ['uses' => 'CalculatorConfigController@createUpdate']);
$router->post('/calculator-config-edit/{id}', ['uses' => 'CalculatorConfigController@edit']);

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->post('calculator/equity-loan',  ['uses' => 'EquityLoanController@equityLoan']);
    $router->post('calculator/seller-stamp-duty',  ['uses' => 'SellerStampDutyController@sellerStampDuty']);
    $router->post('calculator/buyer-stamp-duty',  ['uses' => 'BuyerStampDutyController@buyerStampDuty']);
    
    $router->post('calculator/new-purchase',  ['uses' => 'CalculatorController@newPurchaseFull']);
    $router->post('calculator/new-purchase-report',  ['uses' => 'CalculatorController@newPurchaseReport']);
    $router->post('calculator/new-purchase-pdf',  ['uses' => 'CalculatorController@newPurchasePdf']);
    $router->post('calculator/new-purchase-total',  ['uses' => 'CalculatorController@newPurchaseTotal']);
    $router->post('calculator/get-unfludge-amount',  ['uses' => 'CalculatorController@getUnpludge']);
    $router->post('calculator/get-fludge-amount',  ['uses' => 'CalculatorController@getPludge']);

    $router->post('calculator/refinance',  ['uses' => 'CalculatorController@getRefinance']);
    $router->post('calculator/refinance-report',  ['uses' => 'CalculatorController@getRefinanceReport']);
    $router->post('calculator/refinance-pdf',  ['uses' => 'CalculatorController@refinancePdf']);
    $router->post('calculator/refinance-total',  ['uses' => 'CalculatorController@refinanceTotal']);
    $router->post('calculator/mortgage-repayment', ['uses' => 'CalculatorController@mortageCalculator']);

    $router->post('calculator/mortgage-repayment-pdf', ['uses' => 'CalculatorController@mortageCalculatorPdf']);

    $router->post('calculator/buc-mortgage-downpayment', ['uses' => 'CalculatorController@bucMortgageDownpayment']);
    $router->post('calculator/buc-mortgage-report', ['uses' => 'CalculatorController@bucMortgageReport']);

    $router->post('calculator/buc-mortgage-report-pdf', ['uses' => 'CalculatorController@bucMortgageReportPdf']);
});


