#!/bin/sh

cd /var/www

# php artisan migrate:fresh --seed
php artisan config:clear
php artisan cache:clear
php artisan route:cache
php artisan swagger-lume:generate 

/usr/bin/supervisord -c /etc/supervisord.conf