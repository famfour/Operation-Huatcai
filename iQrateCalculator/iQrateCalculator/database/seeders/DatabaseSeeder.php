<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CalculatorConfig;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['key' => 'loan_amount0','value' => '75','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'loan_amount1_yes','value' => '75','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'loan_amount1_no','value' => '45','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'loan_amount2','value' => '45','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'purchase_value1','value' => '12','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'purchase_value2','value' => '8','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'purchase_value3','value' => '4','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'purchase_value4','value' => '0','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'property_nationality','value' => '[{"Singapore Citizens":0,"Iceland Citizens \/ Permanent Residents":0,"Liechtenstein Citizens \/ Permanent Residents":0,"Norway Citizens \/ Permanent Residents":0,"Switzerland Citizens \/ Permanent Residents":0,"USA Citizens":0,"Singapore Permanent Residents":5,"Others":30},{"Singapore Citizens":17,"Iceland Citizens \/ Permanent Residents":17,"Liechtenstein Citizens \/ Permanent Residents":17,"Norway Citizens \/ Permanent Residents":17,"Switzerland Citizens \/ Permanent Residents":17,"USA Citizens":17,"Singapore Permanent Residents":25,"Others":30},{"Singapore Citizens":25,"Iceland Citizens \/ Permanent Residents":25,"Liechtenstein Citizens \/ Permanent Residents":25,"Norway Citizens \/ Permanent Residents":25,"Switzerland Citizens \/ Permanent Residents":25,"USA Citizens":25,"Singapore Permanent Residents":30,"Others":30},{"Singapore Citizens":25,"Iceland Citizens \/ Permanent Residents":25,"Liechtenstein Citizens \/ Permanent Residents":25,"Norway Citizens \/ Permanent Residents":25,"Switzerland Citizens \/ Permanent Residents":25,"USA Citizens":25,"Singapore Permanent Residents":30,"Others":30}]','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'maximum_loan_tenure','value' => '30','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'monthly_variable','value' => '0.7','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'lowest_intrest_rate','value' => '0.7','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'company_commertial_rate','value' => '0.2','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'monthly_installment','value' => '3.5','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'msr','value' => '30','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'tdsr','value' => '55','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'default_interest','value' => '3.5','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'fledge_amt','value' => '48','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'valuation1','value' => '170','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'valuation2','value' => '220','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'valuation3','value' => '250','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'valuation4','value' => '300','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'valuation5','value' => '350','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'valuation6','value' => '400','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'valuation7','value' => '450','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'valuation8','value' => '500','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'valuation9','value' => '550','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'valuation10','value' => '600','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'valuation11','value' => '650','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'valuation12','value' => '700','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'valuation_exces','value' => '0.02','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'legal_fees_url','value' => 'https://dev-api.iqrate.io/customer/lawfirm/all','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'rates_url','value' => 'https://dev-api.iqrate.io/customer/bank/packages','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'default_down_payment','value' => '0.25','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'emi','value' => '0.013','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'refinance_subsidy','value' => '0.004','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'cimb_subsidy','value' => '0.003','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'citi_subsidy','value' => '0.002','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'lowest_floating','value' => '0.013','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'lowest_fixed','value' => '0.014','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'buc_interest_year1','value' => '1.1','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'buc_interest_year2','value' => '1.2','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'buc_interest_year3','value' => '1.3','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'buc_interest_year4','value' => '1.4','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'buc_interest_year5','value' => '1.5','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'property1','value' => '180000','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'property2','value' => '180000','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'property3','value' => '640000','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'property4','value' => '1000000','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'property1_percentage','value' => '1','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'property2_percentage','value' => '2','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'property3_percentage','value' => '3','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'property4_percentage','value' => '4','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'nationality','value' => '["Singapore Citizens","Iceland Citizens \/ Permanent Residents","Liechtenstein Citizens \/ Permanent Residents","Norway Citizens \/ Permanent Residents","Switzerland Citizens \/ Permanent Residents","USA Citizens","Singapore Permanent Residents","Others"]','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'disbursement_name','value' => '["Completion of Foundation","Reinforcement Concrete","Brick Walls","Roofing\/ Ceiling","Electrical Wiring","Car Park, Roads, Drains","Temporary Occupation Permit (TOP)","Certificate of Statutory Completion (CSC)"]','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['key' => 'yearly_interest','value' => '{"lowest_fixed":[0.014],"lowest_floating":[0.013,0.014]}','created_at' => Carbon::now(),'updated_at' => Carbon::now()],

        ];

        CalculatorConfig::insert($data);
    }
}
