<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->get('/');

        $this->assertEquals(
            $this->app->version(), $this->response->getContent()
        );
    }

    public function testGetCalculatorDetails(){

        $parameters = [
            'property_price'        => 1500000,
            'loan_amount'           => 360000,
            'dob'                   => '06-03-1992',
            'property_type'         => 2,
            'no_of_loan_applicants' => 1,
            'prefered_loan_tenure'  => 24,
            'extend_loan_tenure'    => 'no',
            'monthly_fixed_income'      => 4883,
            'annual_income'             => 60000,
            'monthly_rental_income'     => 1000,
            'total_house_installment'           => 1000,
            'total_car_installment'             => 0,
            'total_personal_installment'        => 0,
            'total_credit_installment'          => 0,
            'total_gurantor_installment'        => 0,
            'total_property_installment'        => 0,
            'total_property_company_installment'=> 0,
            'no_of_housing_loan'                => 1,
            'no_of_own_properties'              => 1,
            'nationality'                       => 'Others',
            'loan_category'                     => 'new_purchase_with_option_to_purchase',
        ];

        $this->post("api/calculator/new-purchase", $parameters, []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            [
                "success",
                "calculator_response" => [
                    "Loan To Value",
                    "Maximum Loan Tenure",
                    "Maximum Tenure based Affordability" => [
                        "Maximum Qualified Loan",
                        "Maximum Property Price",
                        "MSR",
                        "TDSR",
                        "Full Fludge Amount",
                        "Full UnFludge Amount",
                        "Loan Tenure"
                    ],
                    "Prefered Tenure based Affordability" => [
                        "Maximum Qualified Loan",
                        "Maximum Property Price",
                        "MSR",
                        "TDSR",
                        "Full Fludge Amount",
                        "Full UnFludge Amount",
                        "Loan Tenure"
                    ],
                    "Buyer Stamp Duty",
                    "Additional Buyer Stamp Duty",
                    "Legal Fees",
                    "Valuation Fees"
                ],
                "datetime"
            ]    
        );
        
    }

}
