<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class CalculatorTest extends TestCase
{

    public function testEquityLoan()
    {
        $this->json('POST', '/api/calculator/equity-loan', ['existing_housing_loans' => 1,'current_valuation' => 1000000,'total_cpf' => 100000,'outstanding_loan' => 400000])
        ->seeJson([
           'maximum_equity_loan' => '250,000.00',

        ]);
    }

    public function testSellerStampDuty()
    {
        $this->json('POST', '/api/calculator/seller-stamp-duty', ['selling_price' => 100000,'date' => date('Y-m-d', strtotime('-1 day'))])
        ->seeJson([
           'seller_stamp_duty' => "12,000.00",

        ]);
    }

    public function testBuyerStampDuty()
    {
        $this->json('POST', '/api/calculator/buyer-stamp-duty', ['purchase_price' => 1800000,'loan_applicant' => 3,'applicant1_property' => '1','applicant1_nationality'=> 'Singapore Citizens','applicant2_property'=> '2','applicant2_nationality'=> 'Norway Citizens / Permanent Residents','applicant3_property'=> '3','applicant3_nationality'=> 'Others'])
        ->seeStatusCode(200)
        ->seeJson([
            "buyer_stamp_duty" => "56,600.00",
            "additional_buyer_stamp_duty" => "540,000.00",
            "total_buyer_stamp_duty" => "596,600.00",
        ]);
    }

    public function testNewPurchase()
    {
        $this->json('POST', '/api/calculator/newPurchaseReport', ['name' => "James Lin",'property_price' => "1500000",'no_of_housing_loan' => "1",'ltv' => "24",'loan_amount' => "360000",'buyer_stamp_duty' => "24600",'additional_buyer_stamp_duty' => "255001",'legal_fees' => "1000",'valuation_fees' => "170",'other_fees' => "0",'extend_loan_tenure' => "yes",'maximum_loan_tenure' => "2",'maximum_loan_qualified_mlt' => "352303",'maximum_price_mlt' => "7828",'msr_mlt' => "32",'tdsr_mlt' => "59",'full_pludge_mlt' => "15504",'full_unpludge_mlt' => "51680",'prefer_pludge_mlt' => "15504",'calculatiomn_unpludge_mlt' => "51680",'prefer_unpludge_mlt' => "51680",'calculation_pludge_mlt' => "15504",'preferrd_loan_tenure' => "1",'maximum_loan_qualified_plt' => "307953",'maximum_price_plt' => "6843",'msr_plt' => "36",'tdsr_plt' => "63",'full_pludge_plt' => "35232",'full_unpludge_plt' => "117440",'prefer_pludge_plt' => "35232",'calculatiomn_unpludge_plt' => "117440",'prefer_unpludge_plt' => "117440",'calculation_pludge_plt' => "35232",'no_of_loan_applicants' => "1"])
        ->seeStatusCode(200)
        ->seeJson([

        ]);
    }
}



