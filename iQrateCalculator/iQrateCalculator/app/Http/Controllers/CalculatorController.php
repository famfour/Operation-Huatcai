<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Http;
use Barryvdh\DomPDF\Facade\Pdf;
use Exception;
use Illuminate\Support\Facades\Log;
use Storage;
use App\Models\CalculatorConfig;
use Config;
use App\Models\Console;



class CalculatorController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->calculator_config = CalculatorConfig::select('key','value')->get();
        // foreach($this->calculator_config as $cal_key => $cal_value)
        // {
        //     if($this->isJson($cal_value->value) == 1)
        //     {
        //         $value = json_decode($cal_value->value,true);
        //     }
        //     else
        //     {
        //         $value = $cal_value->value;
        //     }

        //     config(['calculator.'.$cal_value->key => $value]);
        // }

    }

    function isJson($string) {
        return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? 1 : 0;

    }


        /**
     * @OA\Post(
     *     path="/api/calculator/new-purchase",
     *     operationId="/api/calculator/new-purchase",
     *     tags={"New Purchase Calculator"},
     *  @OA\Parameter(
     *         name="property_price",
     *         in="query",
     *         description="property_price",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     * @OA\Parameter(
     *         name="loan_amount",
     *         in="query",
     *         description="loan_amount",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     * @OA\Parameter(
     *         name="dob",
     *         in="query",
     *         description="dob",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="string"))
     *     ),
     *
     * @OA\Parameter(
     *         name="property_type",
     *         in="query",
     *         description="property_type",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     * @OA\Parameter(
     *         name="no_of_loan_applicants",
     *         in="query",
     *         description="no_of_loan_applicants",
     *         required=true,
     *         @OA\Schema(type="integer", default="2")
     *     ),
     *
     * @OA\Parameter(
     *         name="prefered_loan_tenure",
     *         in="query",
     *         description="prefered_loan_tenure",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     * @OA\Parameter(
     *         name="extend_loan_tenure",
     *         in="query",
     *         description="extend_loan_tenure",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="string"))
     *     ),
     *
     * @OA\Parameter(
     *         name="monthly_fixed_income",
     *         in="query",
     *         description="monthly_fixed_income",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     * @OA\Parameter(
     *         name="annual_income",
     *         in="query",
     *         description="annual_income",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     * @OA\Parameter(
     *         name="monthly_rental_income",
     *         in="query",
     *         description="monthly_rental_income",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     * @OA\Parameter(
     *         name="total_house_installment",
     *         in="query",
     *         description="total_house_installment",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     * @OA\Parameter(
     *         name="total_car_installment",
     *         in="query",
     *         description="total_car_installment",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     * @OA\Parameter(
     *         name="total_personal_installment",
     *         in="query",
     *         description="total_personal_installment",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     * @OA\Parameter(
     *         name="total_credit_installment",
     *         in="query",
     *         description="total_credit_installment",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     * @OA\Parameter(
     *         name="total_gurantor_installment",
     *         in="query",
     *         description="total_gurantor_installment",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     * @OA\Parameter(
     *         name="total_property_installment",
     *         in="query",
     *         description="total_property_installment",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     * @OA\Parameter(
     *         name="total_property_company_installment",
     *         in="query",
     *         description="total_property_company_installment",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     * @OA\Parameter(
     *         name="no_of_housing_loan",
     *         in="query",
     *         description="no_of_housing_loan",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     * @OA\Parameter(
     *         name="no_of_own_properties",
     *         in="query",
     *         description="no_of_own_properties",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     * @OA\Parameter(
     *         name="nationality",
     *         in="query",
     *         description="nationality",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="string"))
     *     ),
     *
     * @OA\Parameter(
     *         name="loan_category",
     *         in="query",
     *         description="loan_category",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="string"))
     *     ),
     *
     *  @OA\Response(
     *         response="200",
     *         description="New Purchase Calculator",
     *     )
     * )
     */

    public function getTotalMonthlyIncome($request)
    {
        $total_monthly_array = [];
        $datas = $request;
        $i = 0;
        $net_rental_percentage = config('calculator.lowest_intrest_rate');
        foreach($request['annual_income'] as $annual_income){
            $monthly_variable_income    = $request['annual_income'][$i] - (12 * $request['monthly_fixed_income'][$i]);
            $monthly_variable_income    = $monthly_variable_income*(config('calculator.monthly_variable'));
            $monthly_variable_income    = ($monthly_variable_income/12);
            $monthly_variable_income    = ($monthly_variable_income < 0)? 0 : $monthly_variable_income;

            $net_rental_income          = ($request['monthly_rental_income'][$i] * $net_rental_percentage);
            $request['monthly_variable_income'] = $monthly_variable_income;
            $request['net_rental_income']       = $net_rental_income;
            $total_monthly_income               = $request['monthly_fixed_income'][$i] + $monthly_variable_income+$net_rental_income;

            if(strtolower($request['employement_type'][$i]) == 'self-employed'){

                $monthly_variable_income    = $request['annual_income'][$i];
                $monthly_variable_income    = $monthly_variable_income*(config('calculator.monthly_variable'));
                $monthly_variable_income    = ($monthly_variable_income/12);
                $monthly_variable_income    = ($monthly_variable_income < 0)? 0 : $monthly_variable_income;

                $total_monthly_income               = $monthly_variable_income+$net_rental_income;
            }else if(strtolower($request['employement_type'][$i]) != 'self-employed' && strtolower($request['employement_type'][$i]) != 'salaried'){
                $total_monthly_income  = $net_rental_income;
            }

            $total_monthly_array[$i] = $total_monthly_income;
            $i++;
        }
        return $total_monthly_array;
    }

    public function getweightedAge($total_income, $dob_list)
    {
        $date2          = date('Y-m-d');
        $age_array      = [];
        foreach($dob_list as $key1 => $date1){
            $date1          = date('Y-m-d',strtotime($date1));
            $diff           = strtotime($date2) - strtotime($date1);
            $age            =  number_format((int)($diff / (365*60*60*24)));
            $age_array[$key1] = $age;
        }

        $total = array();
        foreach ($total_income as $key=>$price) {
            $total[$key] = $price * $age_array[$key];
        }

        $weight_age = ceil(array_sum($total)/array_sum($total_income));
        return $weight_age;
    }

    public function newPurchaseFull(Request $request)
    {
        $request['prefered_loan_tenure'] = ($request['prefered_loan_tenure'] == '')? 0 : $request['prefered_loan_tenure'];

        Console::log("NEW_PURCHASE: entering");
        $validator = Validator::make($request->all(), [

            'no_of_loan_applicants' => 'required|numeric|max:4',
            'property_price'        => 'required|numeric|gt:loan_amount|min:50',
            'loan_amount'           => 'required|numeric|min:50',
            'property_type'         => 'required|numeric|max:10',
            'prefered_loan_tenure'  => 'required|numeric|min:0|max:35',
            'extend_loan_tenure'    => 'required|in:yes,no',
            'employement_type'      => 'required|array|min:1',
        ]
        );

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->all()], 400);
        }

        $variables = [
        'monthly_fixed_income' ,'annual_income' ,'monthly_rental_income','total_house_installment','total_car_installment', 'total_personal_installment', 'total_credit_installment','total_gurantor_installment','total_property_installment',
        'total_property_company_installment','no_of_housing_loan','no_of_own_properties'];

        for($i = 0; $i < $request['no_of_loan_applicants']; $i++){

            foreach($variables as $var)
            {
                if(!isset($request[$var][$i]) ){
                    return sendResponse($var.' value missing for '.($i+1).' applicant !',400, false);
                }
            }

            if(strtolower($request['employement_type'][$i]) == 'salaried' && $request['monthly_fixed_income'][$i] == 0){
                return response()->json([
                    'message' => ['As per employement type must need Fixed income  for applicant '.$i+1]], 400);
            }

            if(strtolower($request['employement_type'][$i]) == 'self-employed' && $request['annual_income'][$i] == 0){
                return response()->json([
                    'message' => ['As per employement type must need Annual income for applicant '.$i+1]], 400);
            }
        }
        $weighted_age = 0;
        $monthly_variable_income = $this->getVariableIncome($request);
        $all_total_monthly_income = $this->getTotalMonthlyIncome($request);
        if($request['no_of_loan_applicants'] > 1){
            $weighted_age = $this->getweightedAge($all_total_monthly_income, $request['dob_list']);
        }


        $req_data = array(
            'property_price'        => $request['property_price'],
            'loan_amount'           => $request['loan_amount'],
            'dob'                   => $request['dob'],
            'weighted_age'          => $weighted_age,
            'property_type'         => $request['property_type'],
            'no_of_loan_applicants' => $request['no_of_loan_applicants'],
            'prefered_loan_tenure'  => $request['prefered_loan_tenure'],
            'extend_loan_tenure'    => $request['extend_loan_tenure'],
            'monthly_fixed_income'      => array_sum($request['monthly_fixed_income']),
            'annual_income'             => array_sum($request['annual_income']),
            'monthly_rental_income'     => array_sum($request['monthly_rental_income']),
            'total_house_installment'           => array_sum($request['total_house_installment']),
            'total_car_installment'             => array_sum($request['total_car_installment']),
            'total_personal_installment'        => array_sum($request['total_personal_installment']),
            'total_credit_installment'          => array_sum($request['total_credit_installment']),
            'total_gurantor_installment'        => array_sum($request['total_gurantor_installment']),
            'total_property_installment'        => array_sum($request['total_property_installment']),
            'total_property_company_installment'=> array_sum($request['total_property_company_installment']),
            'no_of_housing_loan'                => array_sum($request['no_of_housing_loan']),
            'no_of_own_properties'              => array_sum($request['no_of_own_properties']),
            'nationality'                       => $request['nationality'],
            'loan_category'                     => $request['loan_category'],
            'monthly_variable_income'           => $monthly_variable_income,
            'total_monthly_income'              => array_sum($all_total_monthly_income),
            'token'                             => !empty($request['token'])? $request['token'] : ""
        );
        $result = $this->newPurchase($req_data);


        return $result;
    }

    public function newPurchase($request){

        try{
            $validator = Validator::make($request, [
                'property_price'        => 'required|numeric|gt:loan_amount|min:50',
                'loan_amount'           => 'required|numeric|min:50',
                'dob'                   => 'required|date|before:now',
                'property_type'         => 'required|numeric|max:10',
                'no_of_loan_applicants' => 'required|numeric|max:4',
                'prefered_loan_tenure'  => 'required|numeric|min:0|max:35',
                'extend_loan_tenure'    => 'required|in:yes,no',
                'monthly_fixed_income'      => 'required|numeric|min:0',
                'annual_income'             => 'required|numeric',
                'monthly_rental_income'     => 'required|numeric',
                'total_house_installment'           => 'required|numeric',
                'total_car_installment'             => 'required|numeric',
                'total_personal_installment'        => 'required|numeric',
                'total_credit_installment'          => 'required|numeric',
                'total_gurantor_installment'        => 'required|numeric',
                'total_property_installment'        => 'required|numeric',
                'total_property_company_installment'=> 'required|numeric',
                'no_of_housing_loan'                => 'required|numeric',
                'no_of_own_properties'              => 'required|numeric',
                'nationality'                       => 'required|in:'.implode(",",config('calculator.nationality')),
                'loan_category'                     => 'required',
            ]
            );

            $net_rental_percentage = config('calculator.lowest_intrest_rate');
            $property_type  =  $request['property_type'];

            if ($validator->fails()) {
                return response()->json([
                    'message' => $validator->messages()->all()], 400);
            }
            if($request['extend_loan_tenure'] == 'no' && $request['prefered_loan_tenure'] > 25 && $property_type == 1){

                return response()->json([
                    'message' => ['Loan Tenure more than 25.']], 400);
            }

            if($request['prefered_loan_tenure'] != 0 && $request['prefered_loan_tenure'] < 5){
                return response()->json([
                    'message' => ["Minimum required preferred loan tenure is 5 year."]], 400);
            }

            if($request['extend_loan_tenure'] == 'no' && $request['prefered_loan_tenure'] > 30 && $property_type != 1){

                return response()->json([
                    'message' => ['Loan Tenure more than 30.']], 400);
            }

            // Calculate LTV
            $ltv            = round(($request['loan_amount']/$request['property_price']) * 100, 2);
            //echo $ltv; die;

            $date1          = date('Y-m-d',strtotime($request['dob']));
            $date2          = date('Y-m-d');
            $diff           = strtotime($date2) - strtotime($date1);
            $age            =  number_format((int)($diff / (365*60*60*24)));
            if($request['no_of_loan_applicants'] > 1){
                $age = $request['weighted_age'];
            }

            $extend         =  $request['extend_loan_tenure'];


            // Calculate maximum loan tenure
            //if($ltv > 55 && $ltv < 75 && $property_type == 1 && $age >= 25 && $age <= 65 && $request['prefered_loan_tenure'] > 25 )
            if($ltv > 55 && $ltv < 75 && $property_type == 1 &&  $age <= 65 && $request['prefered_loan_tenure'] > 25 )
            {
                return response()->json([
                    'message' => ['Loan tenure is more than 25.']], 400);
            }

            if($ltv > 55 && $request['prefered_loan_tenure'] > 30){
                return response()->json([
                    'message' => ["Maximum loan tenure for 30 years."]], 400);
            }

            // Check ltv based conditions
            if($request['no_of_housing_loan'] == 0 && $ltv > 75  && $request['extend_loan_tenure'] == 'no'){
                return response()->json([
                    'message' => ['Ltv is more than 75%.']], 400);
            } elseif($request['no_of_housing_loan'] == 1 && $ltv > 45  && $request['extend_loan_tenure'] == 'no'){
                return response()->json([
                    'message' => ['Ltv is more than 45%.']], 400);
            } elseif($request['no_of_housing_loan'] > 1 && $ltv > 35  && $request['extend_loan_tenure'] == 'no'){
                return response()->json([
                    'message' => ['Ltv is more than 35%.']], 400);
            } elseif($request['no_of_housing_loan'] == 0 && $ltv > 55  && $request['extend_loan_tenure'] == 'yes'){
                //echo $ltv;  die;
                return response()->json([
                    'message' => ['Ltv is more than 55%.']], 400);
            }
            elseif($request['no_of_housing_loan'] > 0 && $ltv > 25  && $request['extend_loan_tenure'] == 'yes'){
                return response()->json([
                    'message' => ['Ltv is more than 25%.']], 400);
            }

            // Calculate Maximum loan tenure
            $maximum_loan_tenure =  config('calculator.maximum_loan_tenure');
            if($property_type == 1 && $extend == 'yes')
            {
                $maximum_loan_tenure = ((75-$age) < 30 )? (75-$age) : 30;
            }else if($property_type == 1){
                $maximum_loan_tenure = ((65-$age) < 25 )? (65-$age) : 25;
            }else if ($property_type > 1 && $extend == 'yes'){
                $maximum_loan_tenure = ((75-$age) < 35 )? (75-$age) : 35;
            }else if($property_type > 1){
                $maximum_loan_tenure = ((65-$age) < 30 )? (65-$age) : 30;
            }

            //echo $maximum_loan_tenure; die;

            if($maximum_loan_tenure < 5){
                return response()->json([
                    'message' => ['Your Age or weighted age is '.$age.' So you are not eligible for the loan.']], 400);
            }

            if($maximum_loan_tenure < $request['prefered_loan_tenure']){
                return response()->json([
                    'message' => ['Your maximum loan tenure is '.$maximum_loan_tenure.', so your preferred loan tenure should be less than '.$maximum_loan_tenure]], 400);
            }

            // Calculate maximum loan qualified
            $monthly_variable_income    = $request['monthly_variable_income'];

            $net_rental_income          = floor($request['monthly_rental_income']* $net_rental_percentage);
            $request['monthly_variable_income'] = $monthly_variable_income;
            $request['net_rental_income']       = $net_rental_income;
            //$total_monthly_income               = $request['monthly_fixed_income']+$monthly_variable_income+$net_rental_income;

            //$request['total_monthly_income']    = $total_monthly_income;
            $total_monthly_income =  $request['total_monthly_income'];

            $monthly_intrst         = (config('calculator.monthly_installment')*0.01)/12;
            $n                      = $maximum_loan_tenure*12;
            $m                      = $request['prefered_loan_tenure']*12;
            $monthly_installment    = ceil($request['loan_amount'] * (($monthly_intrst*pow(1+$monthly_intrst,$n)) / ( pow(1+$monthly_intrst,$n)-1  )));

            $pre_monthly_installment = 0;
            if($request['prefered_loan_tenure'] > 0){
                $pre_monthly_installment= ceil($request['loan_amount'] * (($monthly_intrst*pow(1+$monthly_intrst,$m)) / ( pow(1+$monthly_intrst,$m)-1  )));
            }

            //echo $request['total_house_installment']." + ".$request['total_car_installment']." + ".$request['total_personal_installment']." + ".$request['total_credit_installment']." + ".$request['total_gurantor_installment']." + ".$request['total_property_installment']."+ ".($request['total_property_company_installment']* config('calculator.company_commertial_rate')); die;

            $total_financial_commitments = ($request['total_house_installment'] + $request['total_car_installment'] + $request['total_personal_installment'] + $request['total_credit_installment'] + $request['total_gurantor_installment'] + $request['total_property_installment']+ ($request['total_property_company_installment']* config('calculator.company_commertial_rate')));

            //echo $total_financial_commitments; die;

            // Calculate msr and tdsr values
            $request['total_financial_commitments'] = $total_financial_commitments;
            $msr = 0;
            $pre_msr = 0;
            $tdsr = 0;
            $pre_tdsr = 0;
            if($total_monthly_income > 0){
                $msr        = round((($monthly_installment+$request['total_property_installment'])/$total_monthly_income)*100, 2);
                $pre_msr = 0;
                if($request['prefered_loan_tenure'] > 0){
                    $pre_msr    = round((($pre_monthly_installment+$request['total_property_installment'])/$total_monthly_income)*100, 2);
                }
                $tdsr       = round((($monthly_installment+$total_financial_commitments)/$total_monthly_income)*100, 2);
                $pre_tdsr = 0;
                if($request['prefered_loan_tenure'] > 0){
                    $pre_tdsr   = round((($pre_monthly_installment+$total_financial_commitments)/$total_monthly_income)*100, 2);
                }
            }

            // Calculate maximum loan qualified
            $request['maximum_loan_tenure'] = $maximum_loan_tenure;
            $request['loan_tenure']         = $maximum_loan_tenure;
            $max_loan_qualified             = $this->maxLoanQualified($request);
            $msr_based_val = $max_loan_qualified['msr'];
            $tdsr_based_val = $max_loan_qualified['tdsr'];

            $max_loan_qualified = $max_loan_qualified['maximum_loan_qualified'];

            if($request['prefered_loan_tenure'] > 0){
                $request['loan_tenure']         = $request['prefered_loan_tenure'];
                $pre_max_loan_qualified         = $this->maxLoanQualified($request);
                $pre_max_loan_qualified         = $pre_max_loan_qualified['maximum_loan_qualified'];
            }

            // Calculate Maximum Property Price
            $mltv = $ltv;

            if($request['no_of_housing_loan'] == 0 && $request['extend_loan_tenure'] == 'no'){

                $mltv = 75;
            } elseif($request['no_of_housing_loan'] == 1 && $request['extend_loan_tenure'] == 'no'){

                $mltv = 45;
            } elseif($request['no_of_housing_loan'] > 1 && $request['extend_loan_tenure'] == 'no'){

                $mltv = 35;
            } elseif($request['extend_loan_tenure'] == 'yes' && $request['no_of_housing_loan'] == 0)
            {
                $mltv = 55;
            } elseif($request['extend_loan_tenure'] == 'yes' && $request['no_of_housing_loan'] == 1 )
            {
                $mltv = 25;
            }elseif($request['extend_loan_tenure'] == 'yes' &&  $request['no_of_housing_loan'] > 1)
            {
                $mltv = 15;
            }

            $ltv_for_property_price = $ltv;

            //echo $ltv." ".$max_loan_qualified; die;

            $maximum_property_price         = floor(($max_loan_qualified/$mltv)*100);
            $maximum_property_price         = ($maximum_property_price < 0)? 0 : $maximum_property_price;
            $maximum_property_price_tdsr    = floor(($tdsr_based_val/$mltv)*100);
            $maximum_property_price_msr     = floor(($msr_based_val/$mltv)*100);
            if($request['prefered_loan_tenure'] > 0){
                $pre_maximum_property_price     = floor(($pre_max_loan_qualified/$mltv)*100);
                $pre_maximum_property_price     = ($pre_maximum_property_price < 0)? 0 : $pre_maximum_property_price;
            }

            // Calculate Buyer Stamp Duty
            $stamp_arr                      = array("loan_applicant" => $request['no_of_loan_applicants'], "purchase_price" => $request['property_price']);
            $buyer_stamp                    = $this->buyerStampDutySingle($stamp_arr);

            // Calculate Additional Buyer Stamp Duty
            $additional_buyer_stamp_duty    = $this->getAdditionalStampDuty($request['nationality'], $request['no_of_own_properties'], $request['property_price']);

            // Calculate Fludge Amount
            $request['loan_tenure']     = $maximum_loan_tenure;
            $fludge_amount              = $this->getFludgeAmount($request);
            $unfludge_amount            = $this->getUnFludgeAmount($request);

            if($request['prefered_loan_tenure'] > 0){
                $request['loan_tenure']     = $request['prefered_loan_tenure'];
                $pre_fludge_amount          = $this->getFludgeAmount($request);
                $pre_unfludge_amount        = $this->getUnFludgeAmount($request);
            }



            // Calculate Valuation Fees
            $valuation_fees = $this->getValuationFees($request['property_price'], $property_type, 'completed');

            // Calculate legal fees
            $legal_fees = 0;
            if(!empty($request['token'])){
                $token = $request['token'];
                $profile_details = Http::withToken($token)->get(config('calculator.legal_fees_url'));

                $profile_details = $profile_details->json();
                //dd($profile_details);
                $profile_details = (!empty($profile_details['results']))? $profile_details['results'] : array();
                if(!empty($profile_details)){
                    foreach($profile_details as $row)
                    {
                        if(!empty($row['products'])){
                            foreach($row['products'] as $row1)
                            {
                                if($row1['loan_category'] == $request['loan_category'] && $row1['loan_range_from'] <= $request['loan_amount'] && $row1['loan_range_to'] >= $request['loan_amount']){
                                    $legal_fees = $row1['legal_fee'];
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            $msr_status = "FAIL";
            $pre_msr_status = "FAIL";
            if(config('calculator.msr') >= $msr && $msr > 0)
            {
                $msr_status = "PASS";
            }
            if(config('calculator.msr') >= $pre_msr && $pre_msr > 0)
            {
                $pre_msr_status = "PASS";
            }
            $tdsr_status = "FAIL";
            $pre_tdsr_status = "FAIL";
            if(config('calculator.tdsr') >= $tdsr && $tdsr > 0)
            {
                $tdsr_status = "PASS";
            }
            if(config('calculator.tdsr') >= $pre_tdsr && $pre_tdsr > 0)
            {
                $pre_tdsr_status = "PASS";
            }

            $maximum_tenure_based = [
                'Maximum Qualified Loan' => number_format($max_loan_qualified, 2),
                'Maximum Property Price' => number_format($maximum_property_price, 2),
                'MSR' => $msr,
                'MSR Status' => $msr_status,
                'TDSR' => $tdsr,
                'TDSR Status' => $tdsr_status,
                'Full Fludge Amount' => number_format($fludge_amount),
                'Full UnFludge Amount' => number_format($unfludge_amount),
                'Loan Tenure' => $maximum_loan_tenure,
                'Monthly Installment' => number_format($monthly_installment),
            ];

            $prefered_tenure_based = [];
            if($request['prefered_loan_tenure'] > 0){
                $prefered_tenure_based = [
                    'Maximum Qualified Loan' => number_format($pre_max_loan_qualified, 2),
                    'Maximum Property Price' => number_format($pre_maximum_property_price, 2),
                    'MSR' => $pre_msr,
                    'MSR Status' => $pre_msr_status,
                    'TDSR' => $pre_tdsr,
                    'TDSR Status' => $pre_tdsr_status,
                    'Full Fludge Amount' => number_format($pre_fludge_amount),
                    'Full UnFludge Amount' => number_format($pre_unfludge_amount),
                    'Loan Tenure' => (int)$request['prefered_loan_tenure'],
                    'Monthly Installment' => number_format($pre_monthly_installment),
                ];
            }

            $response = [
                'monthly_fixed_income' => $request['monthly_fixed_income'],
                'annual_income' => $request['annual_income'],
                'monthly_rental_income' => $request['monthly_rental_income'],
                'total_house_installment' => $request['total_house_installment'],
                'total_car_installment' => $request['total_car_installment'],
                'total_personal_installment' => $request['total_personal_installment'],
                'total_credit_installment' => $request['total_credit_installment'],
                'total_gurantor_installment' => $request['total_gurantor_installment'],
                'total_property_installment' => $request['total_property_installment'],
                'total_property_company_installment' => $request['total_property_company_installment'],
                'total_monthly_income' => $total_monthly_income,
                'monthly_installment_mlt' => $monthly_installment,
                'monthly_installment_plt' => $pre_monthly_installment,
                'maximum_ltv' => $mltv,
                'msr_based_val' => $msr_based_val,
                'tdsr_based_val' => $tdsr_based_val,
                'property_price_msr' => $maximum_property_price_msr,
                'property_price_tdsr' => $maximum_property_price_tdsr,
                'no_of_housing_loan' => $request['no_of_housing_loan'],
                'no_of_own_properties' => $request['no_of_own_properties'],
                'Loan To Value' => number_format($ltv,2)." %",
                'Maximum Loan Tenure' => $maximum_loan_tenure." years",
                'Maximum Tenure based Affordability' => $maximum_tenure_based,
                'Prefered Tenure based Affordability' => $prefered_tenure_based,
                'Buyer Stamp Duty' => number_format($buyer_stamp),
                'Additional Buyer Stamp Duty' => number_format($additional_buyer_stamp_duty),
                'Legal Fees' => number_format($legal_fees),
                'Valuation Fees' => number_format($valuation_fees),
            ];
            //return sendResponse($response, 200, true);
            return $response;
        } catch (Exception $e) {
            Console::log("NEW_PURCHASE Error: ".$e->getMessage());
            return sendResponse(config('common.message.error_response_500.message'),500, false);
        }
    }

    public function getLowInterest($token, $request)
    {
        $res['lowest_fixed'] = [];
        $res['lowest_floating'] = [];
        if($token != "")
        {

            //$rates_details = Http::withToken($token)->get(config('calculator.rates_url'));
            $rates_details = Http::withToken($token)->get(config('calculator.new_purchase_rates_url').'?loan_amount='.$request['loan_amount'].'&property_status=completed&property_type='.config('calculator.property_type_name')[$request['property_type']]);
            $rates_details =json_decode($rates_details, true);



            if(!empty($rates_details['fixed'])){

                $rates = $rates_details['fixed'];
                //dd($rates);
                $max_key = count($rates['rates']) -1;
                $bank_details = [];
                for($c = 0; $c <= $max_key; $c++)
                {
                    $bank_details[]   = $rates['rates'][$max_key-$c]['total_interest_rate'] * 0.01;
                }


                $res['lowest_fixed'] = $bank_details;
                $res['lowest_fixed_details']['bank_name'] = ($rates['mask'] == false)? $rates['bank']['name'] : $rates['bank']['name_masked'];
                $res['lowest_fixed_details']['rate_type'] = $rates['rate_type'];
                $res['lowest_fixed_details']['bank_logo'] = ($rates['mask'] == false)? $rates['bank']['logo'] : "assets/images/logo.png";
                $res['lowest_fixed_details']['rate_list'] = $bank_details;
                $res['lowest_fixed_details']['details']   = $rates;
            }

            if(!empty($rates_details['floating'])){

                $rates = $rates_details['floating'];
                $max_key = count($rates['rates']) -1;
                $bank_details = [];
                for($c = 0; $c <= $max_key; $c++)
                {
                    $bank_details[]   = $rates['rates'][$max_key-$c]['total_interest_rate'] * 0.01;
                }
                $res['lowest_floating'] = $bank_details;
                $res['lowest_floating_details']['bank_name'] = ($rates['mask'] == false)? $rates['bank']['name'] : $rates['bank']['name_masked'];
                $res['lowest_floating_details']['rate_type'] = $rates['rate_type'];
                $res['lowest_floating_details']['bank_logo'] = ($rates['mask'] == false)? $rates['bank']['logo'] : "assets/images/logo.png";
                $res['lowest_floating_details']['rate_list'] = $bank_details;
                $res['lowest_floating_details']['details']   = $rates;
            }


        }
        return $res;
    }



    // public function getLowInterest($token)
    // {
    //     $res['lowest_fixed'] = [];
    //     $res['lowest_floating'] = [];
    //     if($token != "")
    //     {
    //         $rates_details = Http::withToken($token)->get(config('calculator.rates_url'));
    //         $rates_details = $rates_details->json();
    //         $rates_details = (!empty($rates_details['results']))? $rates_details['results'] : array();

    //         if(!empty($rates_details)){
    //             $lowest_interest = 30;
    //             $lowest_floating_interest = 30;
    //             foreach($rates_details as $rates)
    //             {
    //                 $max_key = count($rates['rates']) -1;
    //                 if($lowest_interest > $rates['rates'][$max_key]['total_interest_rate'] && $rates['rate_type'] == 'fixed')
    //                 {
    //                     $bank_details = [];
    //                     for($c = 0; $c <= $max_key; $c++)
    //                     {
    //                         $bank_details[]   = $rates['rates'][$max_key-$c]['total_interest_rate'] * 0.01;
    //                         $lowets_year[$c] = $rates['rates'][$max_key-$c]['total_interest_rate'];
    //                     }
    //                     if(count(array_unique($bank_details)) === 1) {
    //                         $res['lowest_fixed'] = $bank_details;
    //                         $res['lowest_fixed_details']['bank_name'] = $rates['bank']['name'];
    //                         $res['lowest_fixed_details']['rate_type'] = $rates['rate_type'];
    //                         $res['lowest_fixed_details']['bank_logo'] = $rates['bank']['logo'];
    //                         $res['lowest_fixed_details']['rate_list'] = $bank_details;
    //                         $res['lowest_fixed_details']['details'] = $rates;
    //                         $lowest_interest = $rates['rates'][$max_key]['total_interest_rate'];
    //                     }
    //                 }

    //                 if($lowest_floating_interest > $rates['rates'][$max_key]['total_interest_rate']  && $rates['rate_type'] != 'fixed')
    //                 {
    //                     $bank_details = [];
    //                     for($c = 0; $c <= $max_key; $c++)
    //                     {
    //                         $bank_details[]   = $rates['rates'][$max_key-$c]['total_interest_rate'] * 0.01;
    //                         $lowets_year[$c] = $rates['rates'][$max_key-$c]['total_interest_rate'];
    //                     }
    //                     if(count(array_unique($bank_details)) > 1) {
    //                         $res['lowest_floating'] = $bank_details;
    //                         $res['lowest_floating_details']['bank_name'] = $rates['bank']['name'];
    //                         $res['lowest_floating_details']['rate_type'] = $rates['rate_type'];
    //                         $res['lowest_floating_details']['bank_logo'] = $rates['bank']['logo'];
    //                         $res['lowest_floating_details']['rate_list'] = $bank_details;
    //                         $res['lowest_floating_details']['details']   = $rates;
    //                         $lowest_floating_interest = $rates['rates'][$max_key]['total_interest_rate'];
    //                     }
    //                 }
    //             }
    //             if(empty($res['lowest_floating']))
    //             {
    //                 $res['lowest_floating'] = $res['lowest_fixed'];
    //                 $res['lowest_floating_details'] = $res['lowest_fixed_details'];
    //             }
    //         }
    //     }
    //     return $res;
    // }

    public function getPrincipleAmount($emi, $n, $interest){

        $x      = $emi/$interest;
        $y      = pow(1+$interest, $n);
        $z      = $y-1;
        $res    = ($z/$y);
        $res    = $res * $x;

        return $res;
    }

    public function getValuationFees($amount, $property_type, $property_status){

        $fees = 0;
        if($property_type == 1 && strtolower($property_status) == 'completed')
        {
            $fees = 250;
        }else if ($property_type == 1 && strtolower($property_status) != 'completed'){
            $fees = 0;
        }else if($property_type != 1 && strtolower($property_status) == 'completed'){

            if($amount <= 500000){
                $fees = config('calculator.valuation1');
            }else if($amount > 500000 && $amount <= 750000 ){
                $fees = config('calculator.valuation2');
            }else if($amount > 750000 && $amount <= 1000000 ){
                $fees = config('calculator.valuation3');
            }else if($amount > 1000000 && $amount <= 1500000 ){
                $fees = config('calculator.valuation4');
            }else if($amount > 1500000 && $amount <= 2000000 ){
                $fees = config('calculator.valuation5');
            }else if($amount > 2000000 && $amount <= 2500000 ){
                $fees = config('calculator.valuation6');
            }else if($amount > 2500000 && $amount <= 3000000 ){
                $fees = config('calculator.valuation7');
            }else if($amount > 3000000 && $amount <= 3500000 ){
                $fees = config('calculator.valuation8');
            }else if($amount > 3500000 && $amount <= 4000000 ){
                $fees = config('calculator.valuation9');
            }else if($amount > 4000000 && $amount <= 4500000 ){
                $fees = config('calculator.valuation10');
            }else if($amount > 4500000 && $amount <= 5000000 ){
                $fees = config('calculator.valuation11');
            }else if($amount > 5000000 ){

                $fees = config('calculator.valuation12');
                $remaining_amount = ($amount - 5000000);
                $res = $remaining_amount * (config('calculator.valuation_exces')/100);
                $fees = $fees+$res;
            }
        }else if($property_type != 1 && strtolower($property_status) != 'completed'){
            $fees = 200;
        }
        return $fees;
    }

    public function getUnpludge(Request $request)
    {
        $validator = Validator::make($request->all(), [

            'loan_amount'               => 'required|numeric|min:50',
            //'preferred_fludge_amount'   => 'required|numeric|lt:loan_amount',
            'preferred_fludge_amount'   => 'required|numeric',
            'total_monthly_income'      => 'required|numeric',
            'loan_tenure'               => 'required|numeric|min:1|max:35',
            'total_financial_commitments'   => 'required|numeric',
            'property_type'             => 'required|numeric|min:1|max:10',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->all()], 400);
        }

        $full_fludge = $this->getFludgeAmount($request);

        //echo $full_fludge; die;

        if($full_fludge > $request['preferred_fludge_amount'])
        {
           $unpludhge = number_format(($full_fludge - $request['preferred_fludge_amount'])/(30/100));
        }else{
            $unpludhge = "0";
        }

        //echo $unpludhge; die;

        $res['unfludge_amount'] = $unpludhge;
        return $res;
    }

    public function getFludgeAmount($request){
        $maximum_loan_tenure        = $request['loan_tenure'];
        $total_monthly_income       = $request['total_monthly_income'];
        //if($total_monthly_income > 0){

            //$monthly_installment = $total_monthly_income*config('calculator.msr');
            $monthly_intrst         = (config('calculator.monthly_installment')*0.01)/12;
            $n                      = $request['loan_tenure']*12;
            $monthly_installment    = ceil($request['loan_amount'] * (($monthly_intrst*pow(1+$monthly_intrst,$n)) / ( pow(1+$monthly_intrst,$n)-1  )));


            $total_financial_commitments = $request['total_financial_commitments'];

            $msr = 0;
            $tdsr = 0;
            if($total_monthly_income > 0){
                $msr    = round((($monthly_installment+$request['total_property_installment'])/$total_monthly_income)*100,2);
                $tdsr   = round((($monthly_installment+$total_financial_commitments)/$total_monthly_income)*100,2);
            }

            //echo $msr.' - '.$tdsr; die;
            $fludge = 0;
            // If msr faild then calculate fludge amount
            if($request['property_type'] == 1 || $request['property_type'] == 4){

                if(($msr > config('calculator.msr') || $msr == 0)  && ($tdsr > 0 && $tdsr <= config('calculator.tdsr'))){
                    $fludge = ceil(($monthly_installment+$request['total_property_installment'])/ (config('calculator.msr')/100));
                    $fludge = ceil($fludge - $total_monthly_income);
                    $fludge = ceil($fludge * config('calculator.fledge_amt'));
                }else if(($tdsr > config('calculator.tdsr') || $tdsr == 0 ) && ( $msr > 0 && $msr <= config('calculator.msr'))){
                    $fludge = ceil(($monthly_installment + $total_financial_commitments)/ (config('calculator.tdsr')/100));
                    $fludge = ceil($fludge - $total_monthly_income);
                    $fludge = ceil($fludge * config('calculator.fledge_amt'));
                }else if (($msr > config('calculator.msr') || $msr == 0) && ($tdsr == 0 || $tdsr > config('calculator.tdsr'))){

                    $msr_fludge = ceil(($monthly_installment+$request['total_property_installment'])/ (config('calculator.msr')/100));
                    $msr_fludge = ceil($msr_fludge - $total_monthly_income);
                    $msr_fludge = ceil($msr_fludge * config('calculator.fledge_amt'));

                    $tdsr_fludge = ceil(($monthly_installment + $total_financial_commitments)/ (config('calculator.tdsr')/100));
                    $tdsr_fludge = ceil($tdsr_fludge - $total_monthly_income);
                    $tdsr_fludge = ceil($tdsr_fludge * config('calculator.fledge_amt'));


                    $fludge = ($msr_fludge < $tdsr_fludge)? $tdsr_fludge : $msr_fludge;
                }
            }else{
                if($tdsr > config('calculator.tdsr') || $tdsr == 0 ){
                    $fludge = ceil(($monthly_installment + $total_financial_commitments)/ (config('calculator.tdsr')/100));
                    $fludge = ceil($fludge - $total_monthly_income);
                    $fludge = ceil($fludge * config('calculator.fledge_amt'));
                }
            }
        // }else{
        //     $fludge = "0";
        // }
        return $fludge;
    }

    public function getPludge(Request $request)
    {
        $validator = Validator::make($request->all(), [

            'loan_amount'           => 'required|numeric|min:50',
            //'preferred_unfludge_amount' => 'required|numeric|lt:loan_amount',
            'preferred_unfludge_amount' => 'required|numeric',
            'total_monthly_income'  => 'required|numeric',
            'loan_tenure'           => 'required|numeric|min:1|max:35',
            'total_financial_commitments'   => 'required|numeric',
            'property_type'             => 'required|numeric|min:1|max:10',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->all()], 400);
        }

        $full_fludge = $this->getUnFludgeAmount($request);

        if($full_fludge > $request['preferred_unfludge_amount']){
            $pludhge = number_format(($full_fludge - $request['preferred_unfludge_amount'])*0.3);
        }else{
            $pludhge = "0";
        }
        //echo $pludhge; die;
        $res['fludge_amount'] = $pludhge;
        return $res;
    }

    public function getUnFludgeAmount($request){
        $maximum_loan_tenure        = $request['loan_tenure'];
        $total_monthly_income       = $request['total_monthly_income'];

        $monthly_intrst         = (config('calculator.monthly_installment')*0.01)/12;
        $n                      = $request['loan_tenure']*12;
        $monthly_installment    = ceil($request['loan_amount'] * (($monthly_intrst*pow(1+$monthly_intrst,$n)) / ( pow(1+$monthly_intrst,$n)-1  )));

        $total_financial_commitments = $request['total_financial_commitments'];
        //if($total_monthly_income > 0){

            $msr = 0;
            $tdsr = 0;
            if($total_monthly_income > 0){
                $msr    = round((($monthly_installment+$request['total_property_installment'])/$total_monthly_income)*100,2);
                $tdsr   = round((($monthly_installment+$total_financial_commitments)/$total_monthly_income)*100,2);
            }
            //echo $msr.' - '.$tdsr; die;


            $fludge = 0;
            if($request['property_type'] == 1 || $request['property_type'] == 4){
                // calculate un pludge for msr failed
                if(($msr > config('calculator.msr') || $msr == 0)  && ($tdsr > 0 && $tdsr <= config('calculator.tdsr'))){
                    $fludge = ceil(($monthly_installment+$request['total_property_installment'])/ (config('calculator.msr')/100));

                    $fludge = ceil($fludge - $total_monthly_income);
                    $fludge = ceil($fludge * config('calculator.fledge_amt')/(30/100));
                }else if(($tdsr > config('calculator.tdsr') || $tdsr == 0 ) && ( $msr > 0 && $msr <= config('calculator.msr'))){
                    $fludge = ceil(($monthly_installment + $total_financial_commitments)/ (config('calculator.tdsr')/100));
                    $fludge = ceil($fludge - $total_monthly_income);
                    $fludge = ceil($fludge * config('calculator.fledge_amt')/(30/100));
                }else if (($msr > config('calculator.msr') || $msr == 0) && ($tdsr == 0 || $tdsr > config('calculator.tdsr'))){

                    $msr_fludge = ceil(($monthly_installment+$request['total_property_installment'])/ (config('calculator.msr')/100));

                    $msr_fludge = ceil($msr_fludge - $total_monthly_income);
                    $msr_fludge = ceil($msr_fludge * config('calculator.fledge_amt')/(30/100));


                    $tdsr_fludge = ceil(($monthly_installment + $total_financial_commitments)/ (config('calculator.tdsr')/100));
                    $tdsr_fludge = ceil($tdsr_fludge - $total_monthly_income);

                    $tdsr_fludge = ceil($tdsr_fludge * config('calculator.fledge_amt')/(30/100));

                    $fludge = ($msr_fludge < $tdsr_fludge)? $tdsr_fludge : $msr_fludge;

                }
            }else{
                if($tdsr > config('calculator.tdsr') || $tdsr == 0){
                    $fludge = ceil(($monthly_installment + $total_financial_commitments)/ (config('calculator.tdsr')/100));
                    $fludge = ceil($fludge - $total_monthly_income);
                    $fludge = ceil($fludge * config('calculator.fledge_amt')/(30/100));
                }
            }
        // }else{
        //     $fludge = "0";
        // }

        //echo $fludge; die;
        return $fludge;
    }

    public function maxLoanQualified($request){
        $maximum_loan_tenure        = $request['loan_tenure'];
        $monthly_variable_income    = $request['monthly_variable_income'];
        $lowest_interest = config('calculator.monthly_installment');

        $net_rental_income          = floor($request['monthly_rental_income']* $lowest_interest);
        $total_monthly_income       = $request['total_monthly_income'];
        //if($total_monthly_income > 0){

            $monthly_intrst             = (config('calculator.monthly_installment')*0.01)/12;
            $n = $request['loan_tenure']*12;
            $monthly_installment = floor($request['loan_amount'] * (($monthly_intrst*pow(1+$monthly_intrst,$n)) / ( pow(1+$monthly_intrst,$n)-1  )));

            $total_financial_commitments = $request['total_financial_commitments'];


            $msr = 0;
            $tdsr = 0;
            if($total_monthly_income > 0){
                $msr = round((($monthly_installment+$request['total_property_installment'])/$total_monthly_income)*100,2);
                $tdsr = round((($monthly_installment+$total_financial_commitments)/$total_monthly_income)*100,2);
            }
            $max_loan_qualified = 0;
            $msr_loan = 0;
            $tdsr_loan = 0;

            // calculate maximum loan qualified for msr and tdsr pass
            if($tdsr <= config('calculator.tdsr') && $tdsr > 0 && $msr > 0 && $msr <= config('calculator.msr')){

                $msr_wise_val  = floor($total_monthly_income * (config('calculator.msr')/100)) - $request['total_property_installment'];
                $tdsr_wise_val = floor(($total_monthly_income * (config('calculator.tdsr')/100)) - $total_financial_commitments);

                $msr_loan = $this->getPrincipleAmount($msr_wise_val, $maximum_loan_tenure * 12, (config('calculator.default_interest')*0.01)/12 );
                $tdsr_loan = $this->getPrincipleAmount($tdsr_wise_val, $maximum_loan_tenure * 12, (config('calculator.default_interest')*0.01)/12 );

                if($request['property_type'] == 1 || $request['property_type'] == 4){
                    $max_loan_qualified = ($msr_loan < $tdsr_loan)? $msr_loan : $tdsr_loan;
                }else{
                    $max_loan_qualified = $tdsr_loan;
                }

                // if tdsr pass and msr fail
            }else  if(($tdsr <= config('calculator.tdsr') && $tdsr > 0) && ($msr > config('calculator.msr') || $msr == 0 )){

                $msr_wise_val  = floor($total_monthly_income * (config('calculator.msr')/100)) - $request['total_property_installment'];
                $msr_loan = $this->getPrincipleAmount($msr_wise_val, $maximum_loan_tenure * 12, (config('calculator.default_interest')*0.01)/12 );


                $tdsr_wise_val = floor(($total_monthly_income * (config('calculator.tdsr')/100)) - $total_financial_commitments);
                $max_loan_qualified = floor($this->getPrincipleAmount($tdsr_wise_val, $maximum_loan_tenure * 12, (config('calculator.default_interest')*0.01)/12 ));
                $tdsr_loan = $max_loan_qualified;

                if($request['property_type'] == 1 || $request['property_type'] == 4){
                    $max_loan_qualified = ($msr_loan < $tdsr_loan)? $msr_loan : $tdsr_loan;
                }else{
                    $max_loan_qualified = $tdsr_loan;
                }

                // if msr pass and tdsr fail
            }else  if(($msr <= config('calculator.msr') && $msr > 0) && ($tdsr > config('calculator.tdsr') || $tdsr == 0)){

                $msr_wise_val  = floor($total_monthly_income * (config('calculator.msr')/100)) - $request['total_property_installment'];
                $msr_loan = $this->getPrincipleAmount($msr_wise_val, $maximum_loan_tenure * 12, (config('calculator.default_interest')*0.01)/12 );


                $tdsr_wise_val = floor(($total_monthly_income * (config('calculator.tdsr')/100)) - $total_financial_commitments);
                $max_loan_qualified = floor($this->getPrincipleAmount($tdsr_wise_val, $maximum_loan_tenure * 12, (config('calculator.default_interest')*0.01)/12 ));
                $tdsr_loan = $max_loan_qualified;

                if($request['property_type'] == 1 || $request['property_type'] == 4){
                    $max_loan_qualified = ($msr_loan < $tdsr_loan)? $msr_loan : $tdsr_loan;
                }else{
                    $max_loan_qualified = $tdsr_loan;
                }

                // if msr and tdsr fail
            }else if(($tdsr > config('calculator.tdsr') || $tdsr == 0) && ($msr > config('calculator.msr') || $msr == 0)){
                $msr_wise_val  = floor($total_monthly_income * (config('calculator.msr')/100)) - $request['total_property_installment'];
                $tdsr_wise_val = floor(($total_monthly_income * (config('calculator.tdsr')/100)) - $total_financial_commitments);
                $msr_loan = $this->getPrincipleAmount($msr_wise_val, $maximum_loan_tenure * 12, (config('calculator.default_interest')*0.01)/12 );
                $tdsr_loan = $this->getPrincipleAmount($tdsr_wise_val, $maximum_loan_tenure * 12, (config('calculator.default_interest')*0.01)/12 );

                //  echo "<br>\n".$msr_loan."#".$tdsr_loan;
                if($request['property_type'] == 1 || $request['property_type'] == 4){
                    $max_loan_qualified = ($msr_loan < $tdsr_loan)? $msr_loan : $tdsr_loan;
                    //echo $max_loan_qualified.'<br>';
                }else{
                    $max_loan_qualified = $tdsr_loan;
                }
            }
        // }else{
        //     $msr_loan = 0;
        //     $tdsr_loan = 0;
        //     $max_loan_qualified = 0;
        // }
        $res['msr'] = $msr_loan;
        $res['tdsr'] = $tdsr_loan;
        $res['maximum_loan_qualified'] = ($max_loan_qualified > 0)? $max_loan_qualified : 0;
        return $res;
    }

    public function getAdditionalStampDuty($nationality, $no_of_prop, $amt){

        $no_of_prop = ($no_of_prop > 3)? 3 : $no_of_prop;
        $additional_percentage =  config('calculator.property_nationality')[$no_of_prop][$nationality];
        $additional_buyer_stamp_duty = ($additional_percentage != 0)? $amt * ($additional_percentage/100) : 0;

        return round($additional_buyer_stamp_duty, 2);
    }

    public function buyerStampDutySingle($request)
    {
        try {

            $property1 = config('calculator.property1');
            $property2 = config('calculator.property2');
            $property3 = config('calculator.property3');
            $property4 = config('calculator.property4');

            $property1_percentage = config('calculator.property1_percentage');
            $property2_percentage = config('calculator.property2_percentage');
            $property3_percentage = config('calculator.property3_percentage');
            $property4_percentage = config('calculator.property4_percentage');

            $purchase_price = $request['purchase_price'];

            if($purchase_price > $property1){
                $new_purchase_price = $property1;
                $purchase_price = $purchase_price -  $property1;
                $buyer_stamp_duty1 = ($new_purchase_price * $property1_percentage) / 100;
            }
            else{
                $new_purchase_price = $purchase_price;
                $buyer_stamp_duty1 = ($new_purchase_price * $property1_percentage) / 100;
            }

            if($purchase_price > $property2){
                $new_purchase_price = $property2;
                $purchase_price = $purchase_price -  $property2;
                $buyer_stamp_duty2 = ($new_purchase_price * $property2_percentage) / 100;
            }
            else{
                $new_purchase_price = $purchase_price;
                $buyer_stamp_duty2 = ($new_purchase_price * $property2_percentage) / 100;
            }

            if($purchase_price > $property3){
                $new_purchase_price = $property3;
                $purchase_price = $purchase_price -  $property3;
                $buyer_stamp_duty3 = ($new_purchase_price * $property3_percentage) / 100;
            }
            else{
                $new_purchase_price = $purchase_price;
                $buyer_stamp_duty3 = ($new_purchase_price * $property3_percentage) / 100;

            }
            if($purchase_price >= $property4){
                // $new_purchase_price = $property4;
                // $purchase_price = $purchase_price -  $property4;
                $buyer_stamp_duty4 = ($purchase_price * $property4_percentage) / 100;
            }else{
                $buyer_stamp_duty4 = 0;
            }

            $total_buyer_stamp_duty = $buyer_stamp_duty1 + $buyer_stamp_duty2 + $buyer_stamp_duty3 + $buyer_stamp_duty4;

            // Response for this function
            return $total_buyer_stamp_duty;
        } catch (Exception $e) {
            // $e->getMessage();
            Console::log("BUYER_STAMP_DUTY: Error: ".$e->getMessage());
            return sendResponse(config('common.message.error_response_500.message'),500, false);
        }
    }

     /**
     * @OA\Post(
     *     path="/api/calculator/new-purchase-report",
     *     operationId="/api/calculator/new-purchase-report",
     *     tags={"New Purchase Report"},
     *
     * @OA\Parameter(
     *         name="no_of_loan_applicants",
     *         in="query",
     *         description="no_of_loan_applicants",
     *         required=true,
     *         @OA\Schema(type="integer", default="2")
     *     ),
     *
     * @OA\Parameter(
     *         name="property_price",
     *         in="query",
     *         description="property_price",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *  @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="name",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="string"))
     *     ),
     * @OA\Parameter(
     *         name="no_of_housing_loan",
     *         in="query",
     *         description="no_of_housing_loan",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),@OA\Parameter(
     *         name="ltv",
     *         in="query",
     *         description="ltv",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),@OA\Parameter(
     *         name="loan_amount",
     *         in="query",
     *         description="loan_amount",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),@OA\Parameter(
     *         name="buyer_stamp_duty",
     *         in="query",
     *         description="buyer_stamp_duty",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),@OA\Parameter(
     *         name="additional_buyer_stamp_duty",
     *         in="query",
     *         description="additional_buyer_stamp_duty",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),@OA\Parameter(
     *         name="legal_fees",
     *         in="query",
     *         description="legal_fees",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),@OA\Parameter(
     *         name="valuation_fees",
     *         in="query",
     *         description="valuation_fees",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),@OA\Parameter(
     *         name="other_fees",
     *         in="query",
     *         description="other_fees",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),@OA\Parameter(
     *         name="extend_loan_tenure",
     *         in="query",
     *         description="extend_loan_tenure",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="string"))
     *     ),@OA\Parameter(
     *         name="maximum_loan_tenure",
     *         in="query",
     *         description="maximum_loan_tenure",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),@OA\Parameter(
     *         name="maximum_loan_qualified_mlt",
     *         in="query",
     *         description="maximum_loan_qualified_mlt",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),@OA\Parameter(
     *         name="maximum_price_mlt",
     *         in="query",
     *         description="maximum_price_mlt",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),@OA\Parameter(
     *         name="msr_mlt",
     *         in="query",
     *         description="msr_mlt",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),@OA\Parameter(
     *         name="tdsr_mlt",
     *         in="query",
     *         description="tdsr_mlt",
     *         required=true,
     *        @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),@OA\Parameter(
     *         name="full_pludge_mlt",
     *         in="query",
     *         description="full_pludge_mlt",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),@OA\Parameter(
     *         name="full_unpludge_mlt",
     *         in="query",
     *         description="full_unpludge_mlt",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),@OA\Parameter(
     *         name="prefer_pludge_mlt",
     *         in="query",
     *         description="prefer_pludge_mlt",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),@OA\Parameter(
     *         name="calculatiomn_unpludge_mlt",
     *         in="query",
     *         description="calculatiomn_unpludge_mlt",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),@OA\Parameter(
     *         name="prefer_unpludge_mlt",
     *         in="query",
     *         description="prefer_unpludge_mlt",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),@OA\Parameter(
     *         name="calculation_pludge_mlt",
     *         in="query",
     *         description="calculation_pludge_mlt",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),@OA\Parameter(
     *         name="preferrd_loan_tenure",
     *         in="query",
     *         description="preferrd_loan_tenure",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),@OA\Parameter(
     *         name="maximum_loan_qualified_plt",
     *         in="query",
     *         description="maximum_loan_qualified_plt",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),@OA\Parameter(
     *         name="maximum_price_plt",
     *         in="query",
     *         description="maximum_price_plt",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),@OA\Parameter(
     *         name="msr_plt",
     *         in="query",
     *         description="msr_plt",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),@OA\Parameter(
     *         name="tdsr_plt",
     *         in="query",
     *         description="tdsr_plt",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),@OA\Parameter(
     *         name="full_pludge_plt",
     *         in="query",
     *         description="full_pludge_plt",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),@OA\Parameter(
     *         name="full_unpludge_plt",
     *         in="query",
     *         description="full_unpludge_plt",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),@OA\Parameter(
     *         name="prefer_pludge_plt",
     *         in="query",
     *         description="prefer_pludge_plt",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),@OA\Parameter(
     *         name="calculatiomn_unpludge_plt",
     *         in="query",
     *         description="calculatiomn_unpludge_plt",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),@OA\Parameter(
     *         name="prefer_unpludge_plt",
     *         in="query",
     *         description="prefer_unpludge_plt",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),@OA\Parameter(
     *         name="calculation_pludge_plt",
     *         in="query",
     *         description="calculation_pludge_plt",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     *   @OA\Response(
     *         response="200",
     *         description="New Purchase Report"
     *    )
     * )
     */

    public function newPurchaseReport(Request $request){

        try{
            $request['preferrd_loan_tenure'] = ($request['preferrd_loan_tenure'] == '')? 0 : $request['preferrd_loan_tenure'];
            $validator = Validator::make($request->all(), [
                'name'                  => 'required',
                'property_price'        => 'required|numeric|gt:loan_amount|min:50',
                'property_type'         => 'required|numeric|max:10',
                'no_of_housing_loan'    => 'required|numeric',
                'ltv'                   => 'required|numeric|max:75',
                'loan_amount'           => 'required|numeric|min:50',
                'buyer_stamp_duty'      => 'required|numeric',
                'additional_buyer_stamp_duty'  => 'required|numeric',
                'legal_fees'            => 'required|numeric',
                'valuation_fees'        => 'required|numeric',
                'extend_loan_tenure'    => 'required|in:yes,no',

                'maximum_loan_tenure'           => 'required|numeric',
                'maximum_loan_qualified_mlt'    => 'required|numeric',
                'maximum_price_mlt'             => 'required|numeric',
                'msr_mlt'                       => 'required|numeric',
                'tdsr_mlt'                      => 'required|numeric',
                'full_pludge_mlt'               => 'required|numeric',
                'full_unpludge_mlt'             => 'required|numeric',
                'prefer_pludge_mlt'             => 'required|numeric',
                'calculatiomn_unpludge_mlt'     => 'required|numeric',
                'prefer_unpludge_mlt'           => 'required|numeric',
                'calculation_pludge_mlt'        => 'required|numeric',
                //'monthly_installment_mlt'       => 'required|numeric',

                'preferrd_loan_tenure'          => 'required|numeric|min:0|max:35',
                'maximum_loan_qualified_plt'    => 'required|numeric',
                'maximum_price_plt'             => 'required|numeric',
                'msr_plt'                       => 'required|numeric',
                'tdsr_plt'                      => 'required|numeric',
                'full_pludge_plt'               => 'required|numeric',
                'full_unpludge_plt'             => 'required|numeric',
                'prefer_pludge_plt'             => 'required|numeric',
                'calculatiomn_unpludge_plt'     => 'required|numeric',
                'prefer_unpludge_plt'           => 'required|numeric',
                'calculation_pludge_plt'        => 'required|numeric',
                //'monthly_installment_plt'       => 'required|numeric',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'message' => $validator->messages()->all()], 400);
            }

            if($request['preferrd_loan_tenure'] != 0 && $request['preferrd_loan_tenure'] < 5){
                return response()->json([
                    'message' => ["Minimum required preferred loan tenure is 5 year."]], 400);
            }

            // Calculate downpayment based on ltv
            $down_percentage = 25;
            if($request['no_of_housing_loan'] == 0 && $request['ltv'] == 75 ){
                $down_percentage = 5;
            }

            if($request['no_of_housing_loan'] == 0 && $request['ltv'] == 55 && $request['extend_loan_tenure'] == 'no'){
                $down_percentage = 5;
            }

            if($request['no_of_housing_loan'] == 0 && $request['ltv'] == 55 && $request['extend_loan_tenure'] == 'yes' ){
                $down_percentage = 10;
            }

            // Log::info("New purchase ltv calculation success");
            $cash_payment = ($down_percentage*0.01)*$request['property_price'];
            $cpf = number_format($request['property_price'] - $cash_payment - $request['loan_amount']);

            $msr_status = "FAIL";
            $pre_msr_status = "FAIL";
            if(config('calculator.msr') >= $request['msr_mlt'] && $request['msr_mlt'] > 0)
            {
                $msr_status = "PASS";
            }
            if(config('calculator.msr') >= $request['msr_plt'] && $request['msr_plt'] > 0)
            {
                $pre_msr_status = "PASS";
            }
            $tdsr_status = "FAIL";
            $pre_tdsr_status = "FAIL";
            if(config('calculator.tdsr') >= $request['tdsr_mlt'] && $request['tdsr_mlt'] > 0)
            {
                $tdsr_status = "PASS";
            }
            if(config('calculator.tdsr') >= $request['tdsr_plt'] && $request['tdsr_plt'] > 0)
            {
                $pre_tdsr_status = "PASS";
            }
            // Log::info("New purchase msr,tdsr status success");
            $summary = ['Prepared_for'              => $request['name'],
                        'Property_Purchase_Price'   => (int)$request['property_price'],
                        'Cash_Down_Payment'         => number_format($cash_payment),
                        'CPF_or_Cash_Down_Payment'     => $cpf,
                        'Loan_Amount'               => (int)$request['loan_amount'],
                        'Buyer_Stamp_Duty'          => (int)$request['buyer_stamp_duty'],
                        'Additional_Buyer_Stamp_Duty'  => (int)$request['additional_buyer_stamp_duty'],
                        'Legal_Fees'                => (int)$request['legal_fees'],
                        'Valuation_Fees'            => (int)$request['valuation_fees'],
                        'Other_Fees'                => (!empty($request['other_fees']))? (int)$request['other_fees'] : 0,
            ];
            // Log::info("New purchase summary details success");
            $loan_eligibility_mlt  = [
                'Maximum_Loan_Qualified'    => (int)$request['maximum_loan_qualified_mlt'],
                'Maximum_Property_Price'    => (int)$request['maximum_price_mlt'],
                'MSR'                       => round($request['msr_mlt'],2),
                'MSR Status'                => $msr_status,
                'TDSR'                      => round($request['tdsr_mlt'],2),
                'TDSR Status'               => $tdsr_status,
                'Maximum_Loan_Tenure'       => (int)$request['maximum_loan_tenure'],
                'Full_Pledge_Amount'        => (int)$request['full_pludge_mlt'],
                'Full_Unpledge_Amount'      => (int)$request['full_unpludge_mlt'],
                'Preferred_Pledge_Amount'   => (int)$request['prefer_pludge_mlt'],
                'UnPledge_Amount'           => (int)$request['calculatiomn_unpludge_mlt'],
                'Preferred_UnPledge_Amount' => (int)$request['prefer_unpludge_mlt'],
                'Pledge_Amount'             => (int)$request['calculation_pludge_mlt'],
                'Monthly_Installment'       => !empty($request['monthly_installment_mlt'])? (int)$request['monthly_installment_mlt'] : 0,
            ];

            $interest_types = ['lowest_floating','lowest_fixed'];
            //$lowest_interest = ['lowest_floating' => 0.013, 'lowest_fixed' => 0.014];
            $lowest_interest = [];
            //$yearly_interest = ['lowest_fixed' => [0.014], 'lowest_floating' => [0.013, 0.014]];
            $yearly_interest = [];

            $lowest_floating_details = [];
            $lowest_fixed_details = [];
            // Get the lowest floating and fixed interest
            if(!empty($request['token']))
            {
                $low_interest = $this->getLowInterest($request['token'], $request);

                if(empty($low_interest['lowest_floating']) && empty($low_interest['lowest_fixed'])){
                    return response()->json([
                        'message' => 'Packages not available.'], 400);
                }

                $lowest_interest['lowest_floating'] = !empty($low_interest['lowest_floating'])? $low_interest['lowest_floating'][0] : [];
                $yearly_interest['lowest_floating'] = !empty($low_interest['lowest_floating'])? $low_interest['lowest_floating'] : [];
                $lowest_interest['lowest_fixed'] = !empty($low_interest['lowest_fixed'])? $low_interest['lowest_fixed'][0] : [];
                $yearly_interest['lowest_fixed'] = !empty($low_interest['lowest_fixed'])? $low_interest['lowest_fixed'] : [];

                $lowest_fixed_details = !empty($low_interest['lowest_fixed_details'])? $low_interest['lowest_fixed_details'] : [];
                $lowest_floating_details = !empty($low_interest['lowest_floating_details'])? $low_interest['lowest_floating_details'] : [];
            }


            // Calculate the mortgage list based on maximum loan tenure and preferred loan tenure for lowest fixed and floating interest rate
            $loan_packages_mlt = [];
            $loan_packages_plt = [];
            if(!empty($lowest_interest)){
                foreach($interest_types as $intr_type){
                    if(!empty($lowest_interest[$intr_type])){

                        $monthly_installment_mlt= round($this->emiCalculator($request['maximum_loan_qualified_mlt'], $lowest_interest[$intr_type], $request['maximum_loan_tenure']));
                        $total_payment_mlt  = ($request['maximum_loan_tenure']*12)*$monthly_installment_mlt;
                        $total_interest     = $total_payment_mlt - $request['maximum_loan_qualified_mlt'];
                        $loan_package_mlt = [
                            'Monthly_Installment'    => (int)$monthly_installment_mlt,
                            'Total_Payment'          => (int)$total_payment_mlt,
                            'Total_Principal'        => (int)$request['maximum_loan_qualified_mlt'],
                            'Total_Interest'         => $total_interest,
                            'rate'                   => ($lowest_interest[$intr_type]*100),
                        ];
                        $loan_packages_mlt[$intr_type] = $loan_package_mlt;
                        $payment_mlt = $this->getEmiListNew($request['maximum_loan_qualified_mlt'], $lowest_interest[$intr_type], $request['maximum_loan_tenure']*12, $monthly_installment_mlt, $yearly_interest[$intr_type]);
                        $loan_packages_mlt[$intr_type]['Mortgage_Repayment_Table'] = $payment_mlt['emi_list'];
                        $loan_packages_mlt[$intr_type]['Total_Interest'] = $payment_mlt['total_interest'];
                        $loan_packages_mlt[$intr_type]['Total_Payment'] = $payment_mlt['total_interest']+$request['maximum_loan_qualified_mlt'];

                        if($request['preferrd_loan_tenure'] > 0){

                            $monthly_installment_plt= round($this->emiCalculator($request['maximum_loan_qualified_plt'], $lowest_interest[$intr_type], $request['preferrd_loan_tenure']));
                            $total_payment_plt  = ($request['preferrd_loan_tenure']*12)*$monthly_installment_plt;
                            $total_interest     = $total_payment_plt - $request['maximum_loan_qualified_plt'];
                            $loan_package_plt = [
                                'Monthly_Installment'    => (int)$monthly_installment_plt,
                                'Total_Payment'          => (int)$total_payment_plt,
                                'Total_Principal'        => (int)$request['maximum_loan_qualified_plt'],
                                'Total_Interest'         => $total_interest,
                                'rate'                   => ($lowest_interest[$intr_type]*100),
                            ];
                            $loan_packages_plt[$intr_type] = $loan_package_plt;
                            $payment_plt = $this->getEmiListNew($request['maximum_loan_qualified_plt'], $lowest_interest[$intr_type], $request['preferrd_loan_tenure']*12, $monthly_installment_plt, $yearly_interest[$intr_type]);
                            $loan_packages_plt[$intr_type]['Mortgage_Repayment_Table'] = $payment_plt['emi_list'];
                            $loan_packages_plt[$intr_type]['Total_Interest'] = $payment_plt['total_interest'];
                            $loan_packages_plt[$intr_type]['Total_Payment'] = $payment_plt['total_interest']+$request['maximum_loan_qualified_plt'];
                        }
                    }
                }
            }

            $loan_eligibility_plt = [];
            if($request['preferrd_loan_tenure'] > 0){
                $loan_eligibility_plt  = [
                    'Preffered_Loan_Qualified'  => (int)$request['maximum_loan_qualified_plt'],
                    'Maximum_Property_Price'    => (int)$request['maximum_price_plt'],
                    'MSR'                       => round($request['msr_plt'],2),
                    'MSR Status'                => $pre_msr_status,
                    'TDSR'                      => round($request['tdsr_plt'],2),
                    'TDSR Status'               => $pre_tdsr_status,
                    'Preferred_Loan_Tenure'     => (int)$request['preferrd_loan_tenure'],
                    'Full_Pledge_Amount'        => (int)$request['full_pludge_plt'],
                    'Full_Unpledge_Amount'      => (int)$request['full_unpludge_plt'],
                    'Preferred_Pledge_Amount'   => (int)$request['prefer_pludge_plt'],
                    'UnPledge_Amount'           => (int)$request['calculatiomn_unpludge_plt'],
                    'Preferred_UnPledge_Amount' => (int)$request['prefer_unpludge_mlt'],
                    'Pledge_Amount'             => (int)$request['calculation_pludge_plt'],
                    'Monthly_Installment'       => !empty($request['monthly_installment_plt'])? (int)$request['monthly_installment_plt'] : 0,
                ];
            }

            $response = [
                'Summary' => $summary,
                'Loan_Eligiblity_Maximum_Loan_Tenure_Based' => $loan_eligibility_mlt,
                'Lowest_fixed' => $lowest_fixed_details,
                'Lowest_floating' => $lowest_floating_details,
                'Loan_Package_Maximum_Loan_Tenure_Based' => $loan_packages_mlt,

                'Loan_Eligiblity_Preferred_Loan_Tenure_Based' => $loan_eligibility_plt,
                'Loan_Package_Preferred_Loan_Tenure_Based' => $loan_packages_plt,
            ];
            // Log::info("New purchase response success");

            return $response;
        } catch (Exception $e) {
            Console::log("NEW_PURCHASE_REPORT: Error: ".$e->getMessage());
            return sendResponse(config('common.message.error_response_500.message'),500, false);
        }
    }

    // Calculate list of Emi based on amount, interest rate, tenure, emi amount
    public function getEmiList_old($principal, $interestRate, $term, $emi, $yearly_interest){

        $emi = round($this->emiCalculator($principal, $interestRate, $term/12),2);
        $real_principal = $principal;
        $time = 1 / 12;     // Getting the "t" for the formula
        $result = [];
        $j = 0;
        $cum_total = 0;
        $year = 0;
        for ($i = 1; $i <= $term; $i++) { // Run the loop for the number of terms

            if($j < 12)
            {
                $j++;
            }else{
                if(!empty($yearly_interest)){
                    $interestRate = !empty($yearly_interest[$year])? $yearly_interest[$year] : $yearly_interest[count($yearly_interest)-1];
                    $temp_term = $term - ($year*12);
                    //$emi = round($this->emiCalculator($principal, $interestRate, $temp_term/12), 2);
                    $emi = round($this->emiCalculator($real_principal, $interestRate, $term/12), 2);
                }
                $j = 1;
            }

            $total          = round(($principal * $interestRate * $time), 2);
            $principal_pay  = $emi - $total;
            $principal      -= $principal_pay;
            if($emi > $principal)
            {
                $principal_pay  = round(($principal * $interestRate * $time), 2);
                $emi = $principal+$principal_pay;
            }
            if($principal < 200 && $emi > 200){
                $emi = ($principal > 0)? $emi+$principal : $emi;
                if($principal > 0)
                {
                    $principal_pay = $principal_pay+$principal;
                }

                if($principal < 0)
                {
                    $total = $total-$principal;
                    $principal_pay = $principal_pay+$principal;
                }
                $principal_pay = ($principal > 0)? $principal_pay+$principal : $principal_pay;
                $principal = 0;
            }
            $year           = ceil($i/12);
            $month          = $j;
            $cum_total      = round(($cum_total + $total),2);
            $result['emi_list'][$i]     = [
                'year'                  => $year,
                'month'                 => $month,
                'interest_rate'         => round($interestRate*100, 2),
                'installment_amount'    => $emi,
                'interest_repayment'    => round($total, 2),
                'principal_repayment'   => round($principal_pay, 2),
                'outstanding_amount'    => round($principal, 2),
                'cum_interest_paid'     => round($cum_total, 2),
            ];
        }

        $result['total_interest']     = round($cum_total, 2);
        return $result;
    }


    // Calculate list of Emi based on amount, interest rate, tenure, emi amount
    public function getEmiList($principal, $interestRate, $term, $emi, $yearly_interest){

        $emi = ceil($this->emiCalculator($principal, $interestRate, $term/12));
        $real_principal = $principal;
        $time = 1 / 12;     // Getting the "t" for the formula
        $result = [];
        $j = 0;
        $cum_total = 0;
        $year = 0;
        $emi_loop = true;
        $i = 1;
        //for ($i = 1; $i <= $term+1; $i++) { // Run the loop for the number of terms
        while($emi_loop == true){

            if($j < 12)
            {
                $j++;
            }else{
                if(!empty($yearly_interest)){
                    $interestRate = !empty($yearly_interest[$year])? $yearly_interest[$year] : $yearly_interest[count($yearly_interest)-1];
                    $temp_term = $term - ($year*12);
                    //$emi = round($this->emiCalculator($principal, $interestRate, $temp_term/12), 2);
                    $emi = ceil($this->emiCalculator($real_principal, $interestRate, $term/12));
                }
                $j = 1;
            }

            $total          = round(($principal * $interestRate * $time), 2);
            $principal_pay  = $emi - $total;


            $year           = ceil($i/12);
            $month          = $j;
            $cum_total      = round(($cum_total + $total),2);
            $result['emi_list'][$i]     = [
                'year'                  => $year,
                'month'                 => $month,
                'interest_rate'         => round($interestRate*100, 2),
                'installment_amount'    => $emi,
                'interest_repayment'    => round($total, 2),
                'principal_repayment'   => round($principal_pay, 2),
                'outstanding_amount'    => round($principal, 2),
                'cum_interest_paid'     => round($cum_total, 2),
            ];

            $principal      -= $principal_pay;
            if($emi > $principal)
            {
                $principal_pay  = round(($principal * $interestRate * $time), 2);
                //$emi = round($principal+$principal_pay, 2);
            }

            if($principal <= 0){
                $emi_loop = false;
                $principal = 0;
                $principal_pay = 0;
                $emi = $total;
            }
            $i++;
        }

        $result['total_interest']     = round($cum_total, 2);
        return $result;
    }

    // Calculate list of Emi based on amount, interest rate, tenure, emi amount
    public function getEmiListNew($principal, $interestRate, $term, $emi, $yearly_interest){

        $emi = ceil($this->emiCalculator($principal, $interestRate, $term/12));
        $real_principal = $principal;
        $time = 1 / 12;     // Getting the "t" for the formula
        $result = [];
        $j = 0;
        $cum_total = 0;
        $cum_total_limit = 0;
        $year = 0;
        $emi_loop = true;
        $i = 1;
        //for ($i = 1; $i <= $term+1; $i++) { // Run the loop for the number of terms
        while($emi_loop == true){

            if($j < 12)
            {
                $j++;
            }else{
                if(!empty($yearly_interest)){
                    $interestRate = !empty($yearly_interest[$year])? $yearly_interest[$year] : $yearly_interest[count($yearly_interest)-1];
                    $temp_term = $term - ($year*12);
                    $emi = ceil($this->emiCalculator($real_principal, $interestRate, $term/12));
                    //$emi = round($this->emiCalculator($principal, $interestRate, $temp_term/12), 2);
                }
                $j = 1;
            }

            $total          = round(($principal * $interestRate * $time), 2);
            $principal_pay  = $emi - $total;

            $year           = ceil($i/12);
            $month          = $j;
            $cum_total      = round(($cum_total + $total), 2);
            if($year <= 3){
                $cum_total_limit      = round(($cum_total_limit + $total), 2);
            }
            $result['emi_list'][]     = [
                'emi_no'                => $i,
                'year'                  => $year,
                'month'                 => $month,
                'interest_rate'         => round($interestRate*100, 2),
                'installment_amount'    => $emi,
                'interest_repayment'    => round($total, 2),
                'principal_repayment'   => round($principal_pay, 2),
                'outstanding_amount'    => round($principal, 2),
                'cum_interest_paid'     => round($cum_total, 2),
            ];

            $principal      -= $principal_pay;
            if($emi > $principal)
            {
                $principal_pay  = round(($principal * $interestRate * $time), 2);
                //echo $emi.' - '.$principal+$principal_pay; die;
                //$emi = round($principal+$principal_pay, 2);
            }

            if($principal <= 0){
                $emi_loop = false;
                $principal = 0;
                $principal_pay = 0;
                $emi = 0;
            }
            $i++;
        }
        //echo $cum_total; die;
        $result['total_interest'] = round($cum_total, 2);
        $result['total_interest_for_3_months'] = round($cum_total_limit, 2);
        return $result;
    }

    public function emiCalculator($principal, $interest, $year){

        $interest   = $interest/12;
        $n          = $year * 12;
        $tot        = ($principal * $interest);
        $x          = pow(1+$interest,$n);
        $y          = pow(1+$interest,$n) - 1;
        $emi        = $x/$y;
        $emi        = round($tot*$emi, 2);
        return $emi;
    }


    public function emiCalculator_buc($principal, $interest, $year){

        $interest   = $interest/12;
        $n          = $year;
        $tot        = ($principal * $interest);
        $x          = pow(1+$interest,$n);
        $y          = pow(1+$interest,$n) - 1;
        $emi        = $x/$y;
        $emi        = round($tot*$emi, 2);
        return $emi;
    }

    // Calculate list of Buc based Emi based on amount, interest rate, tenure, emi amount
    public function getBucEmiList($principal, $interestRate, $term, $emi, $def_month, $real_tenure, $disburse_name, $ratio, $yearly_interest){


        //echo $principal; die;
        $time = 1 / 12;     // Getting the "t" for the formula
        $result = [];
        $j = 1;
        $m = 0+$def_month;
        $cum_total = 0;
        $full_emi = 0;
        $full_principal = 0;
        if($disburse_name == 'Certificate of Statutory Completion' ){

            $real_tenure = $term;
            $term = $real_tenure;
        }
        $main_principal = $principal;

        $emi = $this->emiCalculator($principal, $interestRate, $term/12);

        for ($i = 1; $i <= $real_tenure; $i++) { // Run the loop for the number of terms
            $m++;
            $j = (fmod($m, 12) == 0)? 12 : fmod($m, 12);

            $year           = ceil($m/12);
            $month          = $j;
            if(!empty($yearly_interest[$year-1])){
                $interestRate =  $yearly_interest[$year-1]*0.01;


                $emi = $this->emiCalculator($main_principal, $interestRate, $term/12);


            }

            $total          = round(($principal * $interestRate * $time), 2);
            $principal_pay  = $emi - $total;
            $old_principal  = $principal;
            $principal      -= $principal_pay;



            if($principal < $emi){

                $total  = round(($principal * $interestRate * $time), 2);
                $emi = $principal +$total;

                $full_emi       = $full_emi + $emi;
                $full_principal = $full_principal + $principal_pay;
                $cum_total      = round($cum_total + $total, 2);
            }else{
                $full_emi       = $full_emi + $emi;
                $full_principal = $full_principal + $principal_pay;
                $cum_total      = round($cum_total + $total, 2);
            }



            $result[$i]     = [
                'year'                  => $year,
                'month'                 => $month,
                'interest_rate'         => number_format($interestRate*100, 2),
                'disbursement_ratio'    => $ratio,
                'disbursement_name'    => $disburse_name,
                'outstanding_principal_start'    => round($old_principal, 2),
                'monthly_installment'   => $emi,
                'interest_payment'      => number_format($total, 2),
                'principal_payment'     => number_format($principal_pay, 2),
                'outstanding_principal_end'    => number_format($principal, 2),
            ];


        }
        //echo $full_emi.' - '.$cum_total.' - '.$full_principal.'<br>';

        $result['month'] = $m;
        $result['previous_outstanding'] = $principal;
        $result['previous_total_interest'] = $cum_total;
        $result['previous_total_payment'] = $full_emi;
        $result['previous_total_principal_pay'] = $full_principal;
        return $result;
    }

    /**
     * @OA\Post(
     *     path="/api/calculator/new-purchase-total",
     *     operationId="/api/calculator/new-purchase-total",
     *     tags={"New Purchase Total"},
     *
     *  @OA\Parameter(
     *         name="no_of_loan_applicants",
     *         in="query",
     *         description="no_of_loan_applicants",
     *         required=true,
     *         @OA\Schema(type="integer", default="2")
     *     ),
     *
     * @OA\Parameter(
     *         name="annual_income",
     *         in="query",
     *         description="annual_income",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     * @OA\Parameter(
     *         name="monthly_fixed_income",
     *         in="query",
     *         description="monthly_fixed_income",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     * @OA\Parameter(
     *         name="monthly_rental_income",
     *         in="query",
     *         description="monthly_rental_income",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     * @OA\Parameter(
     *         name="total_house_installment",
     *         in="query",
     *         description="total_house_installment",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     * @OA\Parameter(
     *         name="total_car_installment",
     *         in="query",
     *         description="total_car_installment",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     * @OA\Parameter(
     *         name="total_personal_installment",
     *         in="query",
     *         description="total_personal_installment",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     * @OA\Parameter(
     *         name="total_credit_installment",
     *         in="query",
     *         description="total_credit_installment",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     * @OA\Parameter(
     *         name="total_gurantor_installment",
     *         in="query",
     *         description="total_gurantor_installment",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     * @OA\Parameter(
     *         name="total_property_installment",
     *         in="query",
     *         description="total_property_installment",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     * @OA\Parameter(
     *         name="total_property_company_installment",
     *         in="query",
     *         description="total_property_company_installment",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     *
     *  @OA\Response(
     *         response="200",
     *         description="New Purchase Total",
     *     )
     * )
     */
    public function newPurchaseTotal(Request $request)
    {
        $validator = Validator::make($request->all(), [

            'no_of_loan_applicants' => 'required|numeric|max:4',
        ]);

        if ($validator->fails()) {

            return response()->json([
                'message' => $validator->messages()->all()], 400);
        }

        $variables = ['annual_income','monthly_fixed_income','monthly_rental_income','total_house_installment', 'total_car_installment', 'total_personal_installment', 'total_credit_installment', 'total_gurantor_installment', 'total_property_installment', 'total_property_company_installment','employement_type'];
        for($i = 0; $i < $request['no_of_loan_applicants']; $i++){
            foreach($variables as $var)
            {
                if(!isset($request[$var][$i]) ){
                    //return sendResponse($var.' value missing for '.($i+1).' applicant !',401, false);
                    return response()->json([
                        'message' => [$var.' value missing for '.($i+1).' applicant !']], 400);
                }
            }
        }
        $monthly_variable_income = $this->getVariableIncome($request);
        $all_total_monthly_income = $this->getTotalMonthlyIncome($request);

        $req_data = array(
            'annual_income'             =>array_sum( $request['annual_income']),
            'monthly_variable_income'   => $monthly_variable_income,
            'total_monthly_income'      => array_sum($all_total_monthly_income),
            'monthly_fixed_income'      =>array_sum( $request['monthly_fixed_income']),
            'monthly_rental_income'     =>array_sum( $request['monthly_rental_income']),
            'total_house_installment'           =>array_sum( $request['total_house_installment']),
            'total_car_installment'             =>array_sum( $request['total_car_installment']),
            'total_personal_installment'        =>array_sum( $request['total_personal_installment']),
            'total_credit_installment'          =>array_sum( $request['total_credit_installment']),
            'total_gurantor_installment'        =>array_sum( $request['total_gurantor_installment']),
            'total_property_installment'        =>array_sum( $request['total_property_installment']),
            'total_property_company_installment'=>array_sum( $request['total_property_company_installment']),
            'token' => !empty($request['token'])? $request['token'] : "",

        );
        $result = $this->calculateTotal($req_data);

        return $result;
    }

    public function calculateTotal($request){
        $res = [];
        $validator = Validator::make($request, [
            'annual_income'             => 'required|numeric',
            'monthly_fixed_income'      => 'required|numeric',
            'monthly_rental_income'     => 'required|numeric',
            'total_house_installment'           => 'required|numeric',
            'total_car_installment'             => 'required|numeric',
            'total_personal_installment'        => 'required|numeric',
            'total_credit_installment'          => 'required|numeric',
            'total_gurantor_installment'        => 'required|numeric',
            'total_property_installment'        => 'required|numeric',
            'total_property_company_installment'=> 'required|numeric',
        ]);

        if ($validator->fails()) {
            // return $response = array(
            //     "statusCode" => 401,
            //     "message" => $validator->messages()->all(),
            //     "error" => "Bad Request",
            // );
            return response()->json([
                'message' => $validator->messages()->all()], 400);
        }

        $lowest_interest = config('calculator.lowest_intrest_rate');

        $monthly_variable_income    = $request['monthly_variable_income'];
        $net_rental_income          = floor($request['monthly_rental_income']*$lowest_interest);


        //$total_monthly_income           = $request['monthly_fixed_income']+$monthly_variable_income+$net_rental_income;
       // $res['total_monthly_income']    = number_format($total_monthly_income, 2);

        $res['total_monthly_income']    = round($request['total_monthly_income'], 2);
        $res['total_financial_commitments'] = round(($request['total_house_installment'] + $request['total_car_installment'] + $request['total_personal_installment'] + $request['total_credit_installment'] + $request['total_gurantor_installment'] + $request['total_property_installment']+ ($request['total_property_company_installment']* config('calculator.company_commertial_rate'))), 2);

        return $res;
    }


    /**
     * @OA\Post(
     *     path="/api/calculator/refinance-total",
     *     operationId="/api/calculator/refinance-total",
     *     tags={"Refinance Total"},
     *
     *  @OA\Parameter(
     *         name="no_of_loan_applicants",
     *         in="query",
     *         description="no_of_loan_applicants",
     *         required=true,
     *         @OA\Schema(type="integer", default="2")
     *     ),
     *
     * @OA\Parameter(
     *         name="annual_income",
     *         in="query",
     *         description="annual_income",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     * @OA\Parameter(
     *         name="monthly_fixed_income",
     *         in="query",
     *         description="monthly_fixed_income",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     * @OA\Parameter(
     *         name="monthly_rental_income",
     *         in="query",
     *         description="monthly_rental_income",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     *
     *  @OA\Response(
     *         response="200",
     *         description="Refinance Total",
     *     )
     * )
     */
    public function refinanceTotal(Request $request)
    {

        $validator = Validator::make($request->all(), [

            'no_of_loan_applicants' => 'required|numeric|max:4',
            'employement_type'      => 'required|array|min:1',
        ]);

        if ($validator->fails()) {

            return response()->json([
                'message' => $validator->messages()->all()], 400);
        }

        $variables = ['annual_income','monthly_fixed_income','monthly_rental_income'];
        for($i = 0; $i < $request['no_of_loan_applicants']; $i++){

            foreach($variables as $var)
            {
                if(!isset($request[$var][$i]) ){
                    return response()->json([
                        'message' => [$var.' value missing for '.($i+1).' applicant !']], 400);
                }
            }

            if(strtolower($request['employement_type'][$i]) == 'salaried' && $request['monthly_fixed_income'][$i] == 0){
                return response()->json([
                    'message' => ['As per employement type must need Fixed income  for applicant '.$i+1]], 400);
            }

            if(strtolower($request['employement_type'][$i]) == 'self-employed' && $request['annual_income'][$i] == 0){
                return response()->json([
                    'message' => ['As per employement type must need Annual income for applicant '.$i+1]], 400);
            }
        }

        $weighted_age = 0;
        $monthly_variable_income = $this->getVariableIncome($request);
        $all_total_monthly_income = $this->getTotalMonthlyIncome($request);
        if($request['no_of_loan_applicants'] > 1){

            //dd($all_total_monthly_income);
            $weighted_age = $this->getweightedAge($all_total_monthly_income, $request['dob_list']);
        }

        $req_data = [
            'annual_income'             => array_sum($request['annual_income']),
            'monthly_fixed_income'      => array_sum($request['monthly_fixed_income']),
            'monthly_rental_income'     => array_sum($request['monthly_rental_income']),
            'no_of_loan_applicants'     => $request['no_of_loan_applicants'],
            'weighted_age'              => $weighted_age,
            'monthly_variable_income'   => $monthly_variable_income,
            'total_monthly_income'      => array_sum($all_total_monthly_income),
            'token' => !empty($request['token'])? $request['token'] : "",
        ];


        $result = $this->calculateRefinanceTotal($req_data);

        return $result;
    }

    public function calculateRefinanceTotal($request){
        $res = [];
        $validator = Validator::make($request, [
            'annual_income'             => 'required|numeric',
            'monthly_fixed_income'      => 'required|numeric',
            'monthly_rental_income'     => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->all()], 400);
        }

        $net_rental_percentage      = config('calculator.lowest_intrest_rate');
        $monthly_variable_income    = $request['monthly_variable_income'];
        $net_rental_income          = ($request['monthly_rental_income']*$net_rental_percentage);

        //$total_monthly_income           = $request['monthly_fixed_income']+$monthly_variable_income+$net_rental_income;
        $res['total_monthly_income']    = round($request['total_monthly_income'], 2);


        $res['weighted_age']    = $request['weighted_age'];
        return $res;
    }

    public function getVariableIncome($request){

        $monthly_var_income = 0;

        for($i = 0; $i < $request['no_of_loan_applicants']; $i++){

            $monthly_variable_income    = $request['annual_income'][$i] - (12 * $request['monthly_fixed_income'][$i]);
            $monthly_variable_income    = $monthly_variable_income*(config('calculator.monthly_variable'));
            $monthly_variable_income    = ($monthly_variable_income/12);
            $monthly_variable_income    = ($monthly_variable_income < 0)? 0 : $monthly_variable_income;

            $monthly_var_income = $monthly_var_income+$monthly_variable_income;

        }

        return $monthly_var_income;

    }

    /**
     * @OA\Post(
     *     path="/api/calculator/refinance",
     *     operationId="/api/calculator/refinance",
     *     tags={"Refinance"},
     *
     *  @OA\Parameter(
     *         name="no_of_loan_applicants",
     *         in="query",
     *         description="no_of_loan_applicants",
     *         required=true,
     *         @OA\Schema(type="integer", default="2")
     *     ),
     *
     * @OA\Parameter(
     *         name="outstanding_loan_amount",
     *         in="query",
     *         description="outstanding_loan_amount",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     * @OA\Parameter(
     *         name="early_repayment_penalty",
     *         in="query",
     *         description="early_repayment_penalty",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     *  @OA\Parameter(
     *         name="undisbursed_loan_amount",
     *         in="query",
     *         description="undisbursed_loan_amount",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     * @OA\Parameter(
     *         name="cancellation_penalty",
     *         in="query",
     *         description="cancellation_penalty",
     *         required=true,
     *         @OA\Schema(type="array", @OA\Items(type="integer"))
     *     ),
     *
     *
     *  @OA\Response(
     *         response="200",
     *         description="Refinance",
     *     )
     * )
     */
    public function refinanceFull(Request $request)
    {
        $validator = Validator::make($request->all(), [

            'no_of_loan_applicants' => 'required|numeric|max:4',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->all()], 400);
        }

        $variables = ['outstanding_loan_amount','early_repayment_penalty', 'undisbursed_loan_amount', 'cancellation_penalty'];
        for($i = 0; $i < $request['no_of_loan_applicants']; $i++){

            foreach($variables as $var)
            {
                if(!isset($request[$var][$i]) ){
                    return response()->json([
                        'message' => [$var.' value missing for '.($i+1).' applicant !']], 400);
                }
            }
            $req_data = array(
                'outstanding_loan_amount'      => $request['outstanding_loan_amount'][$i],
                'early_repayment_penalty'      => $request['early_repayment_penalty'][$i],
                'undisbursed_loan_amount'      => $request['undisbursed_loan_amount'][$i],
                'cancellation_penalty'         => $request['cancellation_penalty'][$i],
            );
            $result[$i] = $this->getRefinance($req_data);
        }
        return $result;
    }

    public function getRefinance(Request $request){
        $res = [];
        $validator = Validator::make($request->all(), [
            'outstanding_loan_amount'     => 'required|numeric',
            'early_repayment_penalty'     => 'required|numeric|max:30',
            'undisbursed_loan_amount'     => 'required|numeric|lt:outstanding_loan_amount',
            'cancellation_penalty'        => 'required|numeric|max:30',
        ], ['undisbursed_loan_amount.lt' => "The undisbursed loan amount must be less than outstanding loan amount"]);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->all()], 400);
        }

        $res['early_repayment_penalty_amount'] = $request['outstanding_loan_amount']*($request['early_repayment_penalty']/100);
        $res['cancellation_penalty_amount']    = $request['undisbursed_loan_amount']*($request['cancellation_penalty']/100);
        return $res;
    }


    /**
     * @OA\Post(
     *     path="/api/calculator/refinance-report",
     *     operationId="/api/calculator/refinance-report",
     *     tags={"Refinance Report"},
     *
     *  @OA\Parameter(
     *         name="no_of_loan_applicants",
     *         in="query",
     *         description="no_of_loan_applicants",
     *         required=true,
     *         @OA\Schema(type="integer", default="2")
     *     ),
     *
     *
     *
     *
     *  @OA\Response(
     *         response="200",
     *         description="Refinance Report",
     *     )
     * )
     */

    public function getRefinanceReport(Request $request){

        $res = [];
        $validator = Validator::make($request->all(), [
            'name'                        => 'required',
            'dob'                         => 'required|date|before:now',
            'approximate_property_price'  => 'required|numeric',
            'outstanding_loan_amount'     => 'required|numeric|lt:approximate_property_price',
            'outstanding_loan_tenure'     => 'required|numeric|min:1|max:35',
            'prefered_loan_tenure'        => 'required|numeric|min:0|max:35',
            'early_repayment_penalty_amount' => 'required|numeric|lt:outstanding_loan_amount',
            'cancellation_penalty_amount' => 'required|numeric|lt:outstanding_loan_amount|min:0',
            'existing_loan_subsidy'       => 'required|numeric|lt:outstanding_loan_amount|min:0',
            'existing_bank'               => 'required',
            'existing_interest_rate'      => 'required|numeric|max:30',
            'property_type'               => 'required|numeric|max:10',
            'property_status'             => 'required',
            'year_of_purchase'            => 'required|digits:4|numeric|min:'.(date("Y")-30).'|max:'.date("Y"),
            'loan_category'               => 'required',
            'cash_rebate_subsidy'         => 'required|numeric',
            'bank_details'                => 'required|array|between:1,4',
            'bank_details.*.id'           => 'required|numeric',
            //'bank_details.*.bank_name'    => 'required',
            'bank_details.*.interest_rate'=> 'required|numeric|max:30',
            'bank_details.*.bank'         => 'required',
            'bank_details.*.package_id'   => 'required|numeric',
        ],[
            'bank_details.required' => 'Please select atleast one package.',
            'year_of_purchase.digits' => 'Please enter valid year',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->all()], 400);
        }

        // Calculate maximum loan tenure
        $date1          = date('Y-m-d',strtotime($request['dob']));
        $date2          = date('Y-m-d');
        $diff           = strtotime($date2) - strtotime($date1);
        $age            =  number_format((int)($diff / (365*60*60*24)));
        if($request['no_of_loan_applicants'] > 1 && !empty($request['weighted_age']) && $request['weighted_age'] > 0){
            $age = $request['weighted_age'];
        }
        $maximum_loan_tenure =  config('calculator.maximum_loan_tenure');
        $property_type       = $request['property_type'];
        $no_of_purchased_year= date("Y") - $request['year_of_purchase'];
        $exclude_DBS         = "yes";
        foreach($request['bank_details'] as $banks){
            $bank_name = explode(" ", $banks['bank']);
            $bank_name = $bank_name[0];
            if(strtolower($bank_name) == "dbs"){
                $exclude_DBS = 'no';
            }
        }

        if($property_type == 1 && $exclude_DBS == 'yes')
        {
            $maximum_loan_tenure = ((30-$no_of_purchased_year-1) < (75-$age) )? (30-$no_of_purchased_year-1) : (75-$age);
        }else if($property_type == 1){
            $maximum_loan_tenure = ((30-$no_of_purchased_year-2) < (75-$age) )? (30-$no_of_purchased_year-2) : (75-$age);
        }else if ($property_type > 1 && $exclude_DBS == 'yes'){
            $maximum_loan_tenure = ((35-$no_of_purchased_year-1) < (75-$age) )? (35-$no_of_purchased_year-1) : (75-$age);
        }else if($property_type > 1){
            $maximum_loan_tenure = ((35-$no_of_purchased_year-2) < (75-$age) )? (35-$no_of_purchased_year-2) : (75-$age);
        }

        if($request['prefered_loan_tenure'] != 0 && $request['prefered_loan_tenure'] < 5){
            return response()->json([
                'message' => ["Minimum required preferred loan tenure is 5 year."]], 400);
        }

        if($request['prefered_loan_tenure'] > $maximum_loan_tenure){
            return response()->json([
                'message' => ["Preferred loan tenure is greater than maximum loan tenure"]], 400);
        }

        if($maximum_loan_tenure < 5){
            return response()->json([
                'message' => [" Your Age or weighted age is ".$age." So you are not eligible for the refinance."]], 400);
        }

        // Calculate Valuation Fees
        $valuation_fees = $this->getValuationFees($request['approximate_property_price'], $property_type, $request['property_status']);

        // Calculate legal fees
        $legal_fees = 0;
        if(!empty($request['token'])){
            $token = $request['token'];
            $profile_details = Http::withToken($token)->get(config('calculator.legal_fees_url'));
            $profile_details = $profile_details->json();
            $profile_details = (!empty($profile_details['results']))? $profile_details['results'] : array();
            if(!empty($profile_details)){
                foreach($profile_details as $row)
                {
                    if(!empty($row['products'])){
                        foreach($row['products'] as $row1)
                        {
                            if($row1['loan_category'] == $request['loan_category'] && $row1['loan_range_from'] <= $request['outstanding_loan_amount'] && $row1['loan_range_to'] >= $request['outstanding_loan_amount']){
                                $legal_fees = $row1['legal_fee'];
                                break;
                            }
                        }
                    }
                }
            }
        }
        //$maximum_loan_tenure = 2;
        $res['loan_details']    = [
            'Prepared_for'              => $request['name'],
            'Outstanding_Loan_Amount'   => $request['outstanding_loan_amount'],
            'Maximum_Loan_Tenure'       => $maximum_loan_tenure,
            'Prefered_Loan_Tenure'      => $request['prefered_loan_tenure'],
            'Existing_Bank'             => $request['existing_bank'],
            'Existing_Interest_Rate'    => $request['existing_interest_rate'],
        ];
        $res['your_saving_from_refinancing'] =[
            'Legal_Fees' => $legal_fees,
            'Valuation_Fees' => $valuation_fees
        ];

        // Calculate Current details
        $package_basedon_mlt       = $this->getPackageValues($request['outstanding_loan_tenure'],$request['outstanding_loan_amount'], $request['existing_interest_rate'], $request['existing_bank'], $request['property_type'], 'current', $request['existing_interest_rate'], [],'', $request['cash_rebate_subsidy']);

        $monthly_intrst         = ($request['existing_interest_rate']*0.01)/12;
        $n                      = $request['outstanding_loan_tenure']*12;
        $current_installment    = floor($request['outstanding_loan_amount'] * (($monthly_intrst*pow(1+$monthly_intrst,$n)) / ( pow(1+$monthly_intrst,$n)-1  )));
        $tot_interest_savings   = $this->getEmiListNew($request['outstanding_loan_amount'], ($request['existing_interest_rate']/100), $request['outstanding_loan_tenure']*12, $current_installment,[]);
        //dd($package_basedon_mlt);
        $tot_interest_savings   = $tot_interest_savings['total_interest_for_3_months'];

        $package_basedon_plt = [];
        $tot_interest_savings_plt = '';
        if($request['prefered_loan_tenure']){
            $package_basedon_plt       = $this->getPackageValues($request['outstanding_loan_tenure'], $request['outstanding_loan_amount'],  $request['existing_interest_rate'], $request['existing_bank'], $request['property_type'], 'current', $request['existing_interest_rate'], [],'', $request['cash_rebate_subsidy']);

            $n                      = $request['outstanding_loan_tenure']*12;
            $current_installment    = floor($request['outstanding_loan_amount'] * (($monthly_intrst*pow(1+$monthly_intrst,$n)) / ( pow(1+$monthly_intrst,$n)-1  )));
            $tot_interest_savings_plt   = $this->getEmiListNew($request['outstanding_loan_amount'], ($request['existing_interest_rate']/100), $request['outstanding_loan_tenure']*12, $current_installment,[]);
            $tot_interest_savings_plt   = $tot_interest_savings_plt['total_interest_for_3_months'];
        }

        $res['Package_details'][0]['name'] = "Current";
        $res['Package_details'][0]['maximum_loan_tenure'] = $package_basedon_mlt;
        $res['Package_details'][0]['prefered_loan_tenure'] = $package_basedon_plt;
        // Calculate Package values
        $saving_amount = $legal_fees - $valuation_fees - $request['early_repayment_penalty_amount'] - $request['cancellation_penalty_amount'] - $request['existing_loan_subsidy'] + $request['cash_rebate_subsidy'];
        $package_full_details = [];
        $m = 1;
        foreach($request['bank_details'] as $banks){

            $bank_name_details = Http::withToken($token)->get(config('calculator.bank_names_api'));
            $bank_name_details = $bank_name_details->json();
            $bank_names = [];
            if(!empty($bank_name_details['results'])){
                foreach($bank_name_details['results'] as $val){
                    $bank_names[$val['id']] = $val['name'];
                }
            }
            //dd($bank_names);

            $token = $request['token'];
            $package_yearly_rate = [];
            $package_details = Http::withToken($token)->get(config('calculator.rates_url').'?bank_id='.$banks['id']);
            $package_details = $package_details->json();

            //dd($package_details);
            $bank_real_name = !empty($bank_names[$banks['id']])? $bank_names[$banks['id']] :  $banks['bank'];

            //echo $bank_real_name; die;
            if(!empty($package_details['results'][0])){
                $bank_packages = $package_details['results'];
                foreach($bank_packages as $pack){
                    if($pack['id'] == $banks['package_id']){
                        $package_details = $pack;
                        break;
                    }
                }
            }else{
                $package_details = array();
            }
            //$package_details = (!empty($package_details['results'][0]))? $package_details['results'][0] : array();

            //dd($package_details);

            if(!empty($package_details))
            {
                $max_key = count($package_details['rates']) -1;
                for($c = 0; $c <= $max_key; $c++)
                {
                    $package_yearly_rate[]   = $package_details['rates'][$max_key-$c]['total_interest_rate'] *0.01;
                }

                if(count($package_yearly_rate) < 6){
                    for($i = 0; $i < 6; $i++){
                        if(empty($package_yearly_rate[$i])){
                            $package_yearly_rate[$i] = !empty($package_details['rates'][$max_key]['total_interest_rate'])? $package_details['rates'][$max_key]['total_interest_rate'] * 0.01 : 0.01;
                        }
                    }
                }
            }

            $package_basedon_mlt            = $this->getPackageValues($maximum_loan_tenure,$request['outstanding_loan_amount'], $banks['interest_rate'], $bank_real_name, $request['property_type'], 'packege', $request['existing_interest_rate'], $package_yearly_rate,$tot_interest_savings,0);
            $package_basedon_mlt['Savings'] = ($package_basedon_mlt['Savings'] - $saving_amount > 0)? $package_basedon_mlt['Savings'] - $saving_amount : 0;

            $package_basedon_plt = [];
            if($request['prefered_loan_tenure']){
                $package_basedon_plt            = $this->getPackageValues($request['prefered_loan_tenure'], $request['outstanding_loan_amount'],  $banks['interest_rate'], $bank_real_name, $request['property_type'], 'package', $request['existing_interest_rate'], $package_yearly_rate, $tot_interest_savings_plt, 0);
                $package_basedon_plt['Savings'] = (($package_basedon_plt['Savings'] - $saving_amount) > 0)? ($package_basedon_plt['Savings'] - $saving_amount) : 0;
            }


            $res['Package_details'][$m]['Package'] = $m;
            $res['Package_details'][$m]['name'] = $bank_real_name;
            $res['Package_details'][$m]['selected_package_details'] = $package_details;
            $res['Package_details'][$m]['maximum_loan_tenure'] = $package_basedon_mlt;
            $res['Package_details'][$m]['prefered_loan_tenure'] = $package_basedon_plt;


            $m++;
        }
        return $res;
    }

    public function getPackageValues($tenure, $loan_amount, $interest, $bank, $type, $category, $current_interest,$yearly_rate, $tot_interest_savings, $current_rebate_subsidy){

        $monthly_intrst         = ($interest*0.01)/12;
        $n                      = $tenure*12;
        $monthly_installment    = floor($loan_amount * (($monthly_intrst*pow(1+$monthly_intrst,$n)) / ( pow(1+$monthly_intrst,$n)-1  )));
        //$principal_amount = $this->getPrincipleAmount($monthly_installment, $n, $monthly_intrst );
        $principal_amount       = $loan_amount;
        //$total_interest         = ($n*$monthly_installment)-$principal_amount;

        $monthly_intrst         = ($current_interest*0.01)/12;
        $n                      = $tenure*12;
        $current_installment    = floor($loan_amount * (($monthly_intrst*pow(1+$monthly_intrst,$n)) / ( pow(1+$monthly_intrst,$n)-1  )));
        //$amount           = $this->getPrincipleAmount($current_installment, $n, $monthly_intrst );
        $amount                 = $loan_amount;
        $tot_interest           = ($n*$current_installment)-$amount;
        //$tot_interest_savings   = ($loan_amount*(1+(36*$monthly_intrst))) - $loan_amount;
        // $tot_interest_savings_old   = $this->getEmiListNew($loan_amount, ($current_interest/100), $tenure*12, $current_installment,[]);
        // $tot_interest_savings_old   = $tot_interest_savings_old['total_interest_for_3_months'];
        $cash_subsidy           = 0;
        $savings                = 0;
        $mortage_list           = [];
        $total_interest_savings = 0;
        if($category != 'current'){
            //$yearly_rate = [0.01,0.02,0.03,0.01,0.01,0.01];
            $cash_subsidy   = $this->calculateCashSubsidy($bank, $loan_amount, $type);
            $mortage_list   = $this->getEmiListNew($loan_amount, ($interest/100), $tenure*12, $monthly_installment,$yearly_rate);
            $total_interest_savings   = $mortage_list['total_interest_for_3_months'];
            //dd($yearly_rate);
            //echo $tot_interest_savings.' - '.$total_interest_savings.'<br>'; //die;
            $savings        = ($tot_interest_savings-$total_interest_savings)+$cash_subsidy;
            $tot_interest   = $mortage_list['total_interest'];

            $mortage_list   = $mortage_list['emi_list'];
        }else{
            $mortage_list   = $this->getEmiListNew($loan_amount, ($interest/100), $tenure*12, $monthly_installment,$yearly_rate);
            $tot_interest   = $mortage_list['total_interest'];
            $cash_subsidy   = $current_rebate_subsidy;

            $mortage_list   = $mortage_list['emi_list'];
        }


        return [
            'Monthly_Installment' => round($monthly_installment),
            'Total_Principal' => round($principal_amount),
            'Total_Interest' => round($tot_interest),
            'Interest_for_3_years_current' => $tot_interest_savings,
            'Interest_for_3_years' => $total_interest_savings,
            'Cash_Subsidy' => round($cash_subsidy),
            'Savings' => round($savings),
            'Rate' => $interest,
            'Mortgage_Payment_List' => $mortage_list,
        ];
    }

    public function calculateCashSubsidy($bank, $amount, $type){

        $bank = explode(" ", $bank);
        $bank = $bank[0];


        $subsidy = 0;
        if(strtolower($bank) == "dbs" && $type == 1){
            if($amount >= 250000){
                $subsidy = 2000;
            }
        }else if(strtolower($bank) == "dbs" && $type != 1){
            if($amount >= 1500000){
                $subsidy = 2800;
            }else if($amount >= 1000000){
                $subsidy = 2500;
            }else if($amount >= 500000){
                $subsidy = 2000;
            }
        }else if(strtolower($bank) == "uob" && $type == 1){
            if($amount >= 300000){
                $subsidy = 2000;
            }else{
                $subsidy = (config('calculator.refinance_subsidy')*$amount <= 1500)? config('calculator.refinance_subsidy')*$amount : 1500;
            }
        }else if(strtolower($bank) == "uob" && $type != 1){
            if($amount >= 450000){
                $subsidy = 2200;
            }else{
                $subsidy = (config('calculator.refinance_subsidy')*$amount <= 1800)? config('calculator.refinance_subsidy')*$amount : 1800;
            }
        }else if(strtolower($bank) == "ocbc" && $type == 1){
            if($amount >= 300000){
                $subsidy = 1800;
            }
        }else if(strtolower($bank) == "ocbc" && $type != 1){
            if($amount >= 1500000){
                $subsidy = 2500;
            }else if($amount >= 500000){
                $subsidy = 2000;
            }
        }else if(strtolower($bank) == "hsbc"){
            if($amount >= 1500000){
                $subsidy = 2500;
            }else if($amount >= 500000){
                $subsidy = 2000;
            }else if($amount >= 200000){
                $subsidy = 1000;
            }
        }else if(strtolower($bank) == "maybank"){
            $subsidy = (config('calculator.refinance_subsidy')*$amount < 2000)? config('calculator.refinance_subsidy')*$amount : 2000;
        }else if(strtolower($bank) == "scb" && $type != 1){
            if($amount >= 1000000){
                $subsidy = (config('calculator.refinance_subsidy')*$amount < 2000)? config('calculator.refinance_subsidy')*$amount : 2000;
            }else if($amount >= 500000){
                $subsidy = (config('calculator.refinance_subsidy')*$amount < 1800)? config('calculator.refinance_subsidy')*$amount : 1800;
            }
        }else if(strtolower($bank) == "cimb" && $type != 1){
            $subsidy = (config('calculator.cimb_subsidy')*$amount < 2000)? config('calculator.cimb_subsidy')*$amount : 2000;
        }else if(strtolower($bank) == "boc" && $type != 1){
            $subsidy = (config('calculator.refinance_subsidy')*$amount < 1800)? config('calculator.refinance_subsidy')*$amount : 1800;
        }else if(strtolower($bank) == "citibank" && $type == 1){
            if($amount <= 250000){
                $subsidy = (config('calculator.refinance_subsidy')*$amount < 2500)? config('calculator.refinance_subsidy')*$amount : 2500;
            }else{
                $subsidy = (config('calculator.refinance_subsidy')*$amount < 5000)? config('calculator.refinance_subsidy')*$amount : 5000;
            }
        }else if(strtolower($bank) == "citibank" && $type != 1){
            if($amount <= 800000){
                if($amount < 250000){
                    $subsidy = (config('calculator.citi_subsidy')*$amount < 2500)? config('calculator.citi_subsidy')*$amount : 2500;
                }else{
                    $subsidy = (config('calculator.citi_subsidy')*$amount < 5000)? config('calculator.citi_subsidy')*$amount : 5000;
                }
            }else{
                $subsidy = (config('calculator.refinance_subsidy')*$amount < 5000)? config('calculator.refinance_subsidy')*$amount : 5000;
            }
        }else if(strtolower($bank) == "hlf"){
            if($amount >= 300000){
                $subsidy = 2000;
            }else if($amount >= 200000){
                $subsidy = 1500;
            }
        }else if(strtolower($bank) == "sbi"){
            if($amount >= 250000){
                if($amount > 2000000){
                    $subsidy = (config('calculator.refinance_subsidy')*$amount < 1500)? config('calculator.refinance_subsidy')*$amount : 1500;
                }else if($amount >= 750000){
                    $subsidy = (config('calculator.refinance_subsidy')*$amount < 1250)? config('calculator.refinance_subsidy')*$amount : 1250;
                }else{
                    $subsidy = (config('calculator.refinance_subsidy')*$amount < 1000)? config('calculator.refinance_subsidy')*$amount : 1000;
                }
            }
        }else if(strtolower($bank) == "rhb"){
            if($amount >= 500000){
                $subsidy = (config('calculator.refinance_subsidy')*$amount < 1800)? config('calculator.refinance_subsidy')*$amount : 1800;
            }
        }
        return round($subsidy);
    }

    /**
     * @OA\Post(
     *     path="/api/calculator/mortage-repayment",
     *     operationId="/api/calculator/mortage-repayment",
     *     tags={"Mortgage Repayment"},
     *
     *  @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="name",
     *         required=true,
     *         @OA\Schema(type="string", default="David")
     *     ),
     *
     * @OA\Parameter(
     *         name="loan_amount",
     *         in="query",
     *         description="loan_amount",
     *         required=true,
     *         @OA\Schema(type="integer", default="500000")
     *     ),
     * @OA\Parameter(
     *         name="loan_tenure",
     *         in="query",
     *         description="loan_tenure",
     *         required=true,
     *         @OA\Schema(type="integer", default="5")
     *     ),
     * @OA\Parameter(
     *         name="prefered_loan_tenure",
     *         in="query",
     *         description="prefered_loan_tenure",
     *         required=true,
     *         @OA\Schema(type="integer", default="10")
     *     ),
     * @OA\Parameter(
     *         name="existing_bank",
     *         in="query",
     *         description="existing_bank",
     *         required=true,
     *         @OA\Schema(type="string", default="DBS")
     *     ),
     * @OA\Parameter(
     *         name="existing_bank_interest",
     *         in="query",
     *         description="existing_bank_interest",
     *         required=true,
     *         @OA\Schema(type="number", default="7.5")
     *     ),
     * @OA\Parameter(
     *         name="prefered_rate1_year1",
     *         in="query",
     *         description="prefered_rate1_year1",
     *         required=true,
     *         @OA\Schema(type="number", default="2.5")
     *     ),
     * @OA\Parameter(
     *         name="prefered_rate1_year2",
     *         in="query",
     *         description="prefered_rate1_year2",
     *         required=true,
     *         @OA\Schema(type="number", default="3.5")
     *     ),
     * @OA\Parameter(
     *         name="prefered_rate1_year3",
     *         in="query",
     *         description="prefered_rate1_year3",
     *         required=true,
     *         @OA\Schema(type="number", default="4.5")
     *     ),
     * @OA\Parameter(
     *         name="prefered_rate1_year4",
     *         in="query",
     *         description="prefered_rate1_year4",
     *         required=true,
     *         @OA\Schema(type="number", default="5.5")
     *     ),
     * @OA\Parameter(
     *         name="prefered_rate1_year5",
     *         in="query",
     *         description="prefered_rate1_year5",
     *         required=true,
     *         @OA\Schema(type="number", default="6.5")
     *     ),
     * @OA\Parameter(
     *         name="prefered_rate2_enable",
     *         in="query",
     *         description="prefered_rate2_enable",
     *         required=true,
     *         @OA\Schema(type="string", default="yes")
     *     ),
     * @OA\Parameter(
     *         name="prefered_rate2_year1",
     *         in="query",
     *         description="prefered_rate2_year1",
     *         required=false,
     *         @OA\Schema(type="number", default="1.7")
     *     ),
     * @OA\Parameter(
     *         name="prefered_rate2_year2",
     *         in="query",
     *         description="prefered_rate2_year2",
     *         required=false,
     *         @OA\Schema(type="number", default="2.4")
     *     ),
     * @OA\Parameter(
     *         name="prefered_rate2_year3",
     *         in="query",
     *         description="prefered_rate2_year3",
     *         required=false,
     *         @OA\Schema(type="number", default="3.4")
     *     ),
     * @OA\Parameter(
     *         name="prefered_rate2_year4",
     *         in="query",
     *         description="prefered_rate2_year4",
     *         required=false,
     *         @OA\Schema(type="number", default="4.5")
     *     ),
     * @OA\Parameter(
     *         name="prefered_rate2_year5",
     *         in="query",
     *         description="prefered_rate2_year5",
     *         required=false,
     *         @OA\Schema(type="integer", default="5")
     *     ),
     *
     *
     *
     *
     *  @OA\Response(
     *         response="200",
     *         description="Mortgage Repayment",
     *     )
     * )
     */

    public function mortageCalculator(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'name'                 => 'required',
                'loan_amount'          => 'required|numeric|min:50',
                'loan_tenure'          => 'required|numeric|min:1|max:35',
                'prefered_rate1_year1' => 'required|numeric|max:30',
                'prefered_rate1_year2' => 'required|numeric|max:30',
                'prefered_rate1_year3' => 'required|numeric|max:30',
                'prefered_rate1_year4' => 'required|numeric|max:30',
                'prefered_rate1_year5' => 'required|numeric|max:30',
                'prefered_rate2_enable'=> 'required|in:yes,no',
                'prefered_rate2_year1' => 'nullable|required_if:prefered_rate2_enable,==,yes|numeric|max:30',
                'prefered_rate2_year2' => 'nullable|required_if:prefered_rate2_enable,==,yes|numeric|max:30',
                'prefered_rate2_year3' => 'nullable|required_if:prefered_rate2_enable,==,yes|numeric|max:30',
                'prefered_rate2_year4' => 'nullable|required_if:prefered_rate2_enable,==,yes|numeric|max:30',
                'prefered_rate2_year5' => 'nullable|required_if:prefered_rate2_enable,==,yes|numeric|max:30',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'message' => $validator->messages()->all()], 400);
            }
            // Get Loan Details;
            $loan_details = [
                "Prepared For" => $request['name'],
                "Loan Amount" => $request['loan_amount'],
                "Loan Tenure" => $request['loan_tenure'],
            ];
            // Get Interest rate comparison
            $interest_types = [$request['prefered_rate1_year1']];
            if($request['prefered_rate2_enable'] == 'yes'){
                $interest_types = [$request['prefered_rate1_year1'],$request['prefered_rate2_year1']];
            }
            $interest_details = [];
            $mortage_details = [];
            $i = 1;
            foreach($interest_types as $intrst_type)
            {
                $monthly_intrst         = ($intrst_type*0.01)/12;
                $n                      = $request['loan_tenure']*12;
                $monthly_installment    = round($this->emiCalculator($request['loan_amount'], ($intrst_type*0.01), $request['loan_tenure']), 2);

                $total_payment = round(round($monthly_installment) * $n);
                $total_interest= $total_payment - $request['loan_amount'];

                $interest_rate_comparison = [
                    "Monthly Installment" => $monthly_installment,
                    "Total Payment" => $total_payment,
                    "Total Principal" => $request['loan_amount'],
                    "Total Interest" => $total_interest,
                ];
                $interest_details['Interest Rate '.$i] = $interest_rate_comparison;

                $yearly_rate = [($request['prefered_rate'.$i.'_year1']/100),($request['prefered_rate'.$i.'_year2']/100),($request['prefered_rate'.$i.'_year3']/100),($request['prefered_rate'.$i.'_year4']/100),($request['prefered_rate'.$i.'_year5']/100)];

                $list_of_emi = $this->getEmiList($request['loan_amount'], ($intrst_type*0.01), $request['loan_tenure']*12, $monthly_installment,$yearly_rate);
                $mortage_details['Mortgage list for '.$i] = $list_of_emi['emi_list'];
                $interest_details['Interest Rate '.$i]['Total Interest'] = $list_of_emi['total_interest'];
                $interest_details['Interest Rate '.$i]['Total Payment'] = $list_of_emi['total_interest']+$request['loan_amount'];

                $i++;
            }
            return $response = [
                'Loan Details' => $loan_details,
                'Interest Rate Comparison' => $interest_details,
                'Mortgage Repayment' => $mortage_details,
            ];
        } catch (Exception $e) {
            return sendResponse(config('common.message.error_response_500.message'),500, false);
        }
    }

    public function newPurchasePdf(Request $request)
    {
        try{
            $request['preferred_loan_tenure'] = ($request['preferred_loan_tenure'] == '')? 0 : $request['preferred_loan_tenure'];
            ini_set('memory_limit', '2000M');
            $validator = Validator::make($request->all(), [
                'export_fields'  => 'required|array|min:1',
                'export_fields.*' => 'string|in:Summary,Loan_eligibility_maximum,Loan_eligibility_maximum_package,Loan_eligibility_maximum_mortgage,Loan_eligibility_preferred,Loan_eligibility_preferred_package,Loan_eligibility_preferred_mortgage'
            ],[
                'export_fields.required' => 'Please select atleast one checkbox.'
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'message' => $validator->messages()->all()], 400);
            }

            $res = $this->newPurchaseReport($request);

            if(!is_array($res))
            {
                return $res;
            }
            $pdf = PDF::loadView('new-purchase-pdf',['data'=>$res,  'export_fields' => $request['export_fields'], 'preferred_loan_tenure' => $request['preferrd_loan_tenure']]);
            //return $pdf->download('purchase_report');
            $imageName = 'calculator-pdf/new-purchase-report'.strtotime('now').'.pdf';
            Storage::disk('s3')->put($imageName, $pdf->output(), [
                'ContentDisposition' => 'attachment'
           ]);
            $imageName = Storage::disk('s3')->url($imageName);
            return $imageName;
        } catch (Exception $e) {
            Console::log("NEW_PURCHASE_PDF: Error: ".$e->getMessage());
            return $e;
        }
    }

    public function refinancePdf(Request $request)
    {
        try{
            ini_set('memory_limit', '2000M');
            $validator = Validator::make($request->all(), [
                'export_fields'  => 'required|array|min:1',
                'export_fields.*' => 'string|in:Loan_details,Savings_from_refinancing,Package_1,Package_2,Package_3,Package_4'
            ],[
                'export_fields.required' => 'Please select atleast one checkbox.'
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'message' => $validator->messages()->all()], 400);
            }
            $res = $this->getRefinanceReport($request);

            if(!is_array($res))
            {
                return $res;
            }

            //return view('refinance-pdf',['datas'=>$res]);
            $pdf = PDF::loadView('refinance-pdf',['data'=>$res, 'prefered_tenure' => $request['prefered_loan_tenure'],  'export_fields' => $request['export_fields']]);
            //return $pdf->download('purchase_report');
            $imageName = 'calculator-pdf/refinance-report'.strtotime('now').'.pdf';
            Storage::disk('s3')->put($imageName, $pdf->output(), [
                'ContentDisposition' => 'attachment'
           ]);
            $imageName = Storage::disk('s3')->url($imageName);
            return $imageName;
        } catch (Exception $e) {
            Console::log("REFINANCE_PDF: Error: ".$e->getMessage());
            return $e;
        }
    }

    public function mortageCalculatorPdf(Request $request)
    {
        try{

            ini_set('memory_limit', '2000M');
            $validator = Validator::make($request->all(), [
                'export_fields'  => 'required|array|min:1',
                'export_fields.*' => 'string|in:Loan Details,Interest Rate Comparison,Mortgage Repayment 1,Mortgage Repayment 2'
            ],[
                'export_fields.required' => 'Please select atleast one checkbox.'
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'message' => $validator->messages()->all()], 400);
            }

            $res = $this->mortageCalculator($request);
            if(!empty($res['statusCode'])){
                return $res;
            }
            $pdf = PDF::loadView('pdf',['datas'=>$res, 'export_fields' => $request['export_fields']]);
            //return $pdf->download('purchase_report');
            $imageName = 'calculator-pdf/mortgage-report'.strtotime('now').'.pdf';
            Storage::disk('s3')->put($imageName, $pdf->output(), [
                'ContentDisposition' => 'attachment'
           ]);
            $imageName = Storage::disk('s3')->url($imageName);
            return $imageName;
        } catch (Exception $e) {
            Console::log("MORTGAGE_PDF: Error: ".$e->getMessage());
            return $e;
        }
    }

    public function bucMortgageReportPdf(Request $request)
    {
        try{
            ini_set('memory_limit', '2000M');
            $validator = Validator::make($request->all(), [
                'export_fields'  => 'required|array|min:1',
                'export_fields.*' => 'string|in:Loan Details,Rate Comparison,Disbursement Schedule,Progressive Repayment,Preferred Repayment'
            ],[
                'export_fields.required' => 'Please select atleast one checkbox.'
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'message' => $validator->messages()->all()], 400);
            }

            $res = $this->bucMortgageReport($request);
            if(!empty($res['statusCode'])){
                return $res;
            }
            $res['prefered_rate_enable'] = $request['prefered_rate_enable'];
            $pdf = PDF::loadView('buc-pdf',['datas'=>$res, 'export_fields' => $request['export_fields']]);
            //return $pdf->download('purchase_report');
            $imageName = 'calculator-pdf/buc-mortgage-report'.strtotime('now').'.pdf';
            Storage::disk('s3')->put($imageName, $pdf->output(), [
                'ContentDisposition' => 'attachment'
           ]);
            $imageName = Storage::disk('s3')->url($imageName);
            return $imageName;
        } catch (Exception $e) {
            Console::log("BUC_MORTGAGE_PDF: Error: ".$e->getMessage());
            return $e;
        }
    }

    public function bucMortgageDownpayment(Request $request){

        try {
            $validator = Validator::make($request->all(), [
                'property_price'       => 'required|numeric|min:50',
                'ltv'                  => 'required|numeric|min:5|max:75',
                'cpf_available_amount' => 'nullable|numeric|lt:property_price',
                'loan_tenure'          => 'required|numeric|min:5|max:35',
            ]);

            if ($validator->fails()) {
                return response()->json([
                'message' => $validator->messages()->all()], 400);
            }

            if($request['ltv'] > 55 && $request['loan_tenure'] > 30){
                return response()->json([
                    'message' => ["Maximum loan tenure for 30 years."]], 400);
            }

            if(empty($request['cpf_available_amount']) || $request['cpf_available_amount'] == '' || $request['cpf_available_amount'] == null){

                $request['cpf_available_amount'] = 0;
            }

            $down_payment   = round(((100 - $request['ltv'])*0.01) * $request['property_price'], 2);
            $loan_amount    = round(($request['ltv']*0.01) * $request['property_price'], 2);
            $real_loan_amount    = $loan_amount;

            $cash_downpayment   = $down_payment - $request['cpf_available_amount'];
            $cpf_payment        = $request['cpf_available_amount'];

            $disbursement_schedule = $this->getDisbursementSchedule($request['property_price'], $cash_downpayment, $cpf_payment, $loan_amount, []);

            return $response = [
                'Down Payment' => $down_payment,
                'Loan Amount' => $real_loan_amount,
                'disbursement_schedule' => $disbursement_schedule
            ];
        } catch (Exception $e) {
            Console::log("BUC_MORTGAGE_DOWNPAYMENT: Error: ".$e->getMessage());
            return sendResponse(config('common.message.error_response_500.message'),500, false);
        }
    }

    public function getDisbursementSchedule($property_price, $cash_downpayment, $cpf_payment, $loan_amount, $disbursement_month){

        $disbursement_rate = config('calculator.disbursement_ratio');
        $disbursement_schedule = [];
        $i = 0;
        foreach($disbursement_rate as $detail){

            $payment = [];
            $disbursement_name = [];
            $percentage_value = $property_price * $detail['percentage'] * 0.01;
            if($cash_downpayment > $percentage_value){
                $payment['cash'] = round($percentage_value, 2);
                $disbursement_name[] = "Cash";
                $cash_downpayment = $cash_downpayment-$percentage_value;
            }else{
                $cash_percentage = 0;
                if($cash_downpayment > 0)
                {
                    $payment['cash'] = round($cash_downpayment, 2);
                    $disbursement_name[] = "Cash";
                    $cash_percentage = ($cash_downpayment/$property_price)*100;
                    $cash_downpayment = 0;
                }

                $cpf_percentage = $detail['percentage'] - $cash_percentage;
                $percentage_value = $property_price * $cpf_percentage * 0.01;
                if($cpf_payment > $percentage_value ){
                    if($percentage_value > 0){
                        $payment['cpf'] = round($percentage_value, 2);
                        $disbursement_name[] = "Cpf";
                        $cpf_payment = $cpf_payment-$percentage_value;
                    }
                }else{

                    $cpf_percentage = 0;
                    if($cpf_payment > 0)
                    {
                        $payment['cpf'] = round($cpf_payment, 2);
                        $disbursement_name[] = "Cpf";
                        $cpf_percentage = ($cpf_payment/$property_price)*100;
                        $cpf_payment = 0;
                    }

                    $bank_percentage = $detail['percentage'] - $cpf_percentage - $cash_percentage;
                    $percentage_value = $property_price * $bank_percentage * 0.01;
                    if($loan_amount > $percentage_value){
                        if($percentage_value > 0){
                            $payment['bank_loan'] = round($percentage_value, 2);
                            $disbursement_name[] = "Bank Loan";
                            $loan_amount = $loan_amount-$percentage_value;
                        }
                    }else{
                        $disbursement_name[] = "Bank Loan";
                        $payment['bank_loan'] = round($loan_amount, 2);
                    }
                }
            }


            $disbursement_schedule[$i]['month'] = !empty($disbursement_month[$detail['description']])? $disbursement_month[$detail['description']] : 6;
            $disbursement_schedule[$i]['name'] = $detail['description'];
            $disbursement_schedule[$i]['percentage'] = $detail['percentage'];
            $disbursement_schedule[$i]['amount'] = $payment;
            $disbursement_schedule[$i]['disbursement_name'] = $disbursement_name;
            $disbursement_schedule[$i]['total_amount'] = array_sum($payment);
            $i++;
        }
        return $disbursement_schedule;
    }

    public function bucMortgageReport(Request $request){

        try {
            $validator = Validator::make($request->all(), [
                'name'                 => 'required',
                'property_price'       => 'required|numeric',
                'ltv'                  => 'required|numeric|min:5|max:75',
                'loan_tenure'          => 'required|numeric|min:5|max:35',
                'downpayment'          => 'required|numeric|lt:property_price',
                'cpf_available_amount' => 'nullable|numeric|lt:downpayment',
                'loan_amount'          => 'required|numeric|lt:property_price',
                'token'                => 'required',
                'prefered_rate_enable'=> 'required|in:yes,no',
                'prefered_rate_year1' => 'nullable|required_if:prefered_rate_enable,==,yes|numeric|max:30',
                'prefered_rate_year2' => 'nullable|required_if:prefered_rate_enable,==,yes|numeric|max:30',
                'prefered_rate_year3' => 'nullable|required_if:prefered_rate_enable,==,yes|numeric|max:30',
                'prefered_rate_year4' => 'nullable|required_if:prefered_rate_enable,==,yes|numeric|max:30',
                'prefered_rate_year5' => 'nullable|required_if:prefered_rate_enable,==,yes|numeric|max:30',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'message' => $validator->messages()->all()], 400);
            }

            if($request['ltv'] > 55 && $request['loan_tenure'] > 30){
                return response()->json([
                    'message' => ["Maximum loan tenure for 30 years."]], 400);
            }
            if(empty($request['cpf_available_amount']) || $request['cpf_available_amount'] == '' || $request['cpf_available_amount'] == null){

                $request['cpf_available_amount'] = 0;
            }

            $loan_details = [
                "Prepared For"      => $request['name'],
                "Property Price"    => $request['property_price'],
                "Loan-To-Value"     => $request['ltv'],
                "Loan Tenure"       => $request['loan_tenure'],
                "CPF Available"     => $request['cpf_available_amount'],
                "Downpayment"       => $request['downpayment'],
                "Loan Amount"       => $request['loan_amount'],
            ];

            // Get lowest interest packages
            $lowets_year[0] = 1;
            $yearly_interest = [];
            $pre_yearly_interest = [];
            $bank_details = [];
            if(!empty($request['token'])){
                $package = $this->getLowestPackages($request);
                if(!empty($package)){
                    $lowest_interest    = $package['lowest_interest'];
                    $bank_details       = $package['bank_details'];
                    $yearly_interest    = $package['yearly_interest'];
                    $lowets_year        = $package['yearly_interest'];
                }
            }
            //dd($bank_details);


            // Rate Package Comparison
            $monthly_intrst         = ($lowets_year[0]*0.01)/12;
            $n                      = $request['loan_tenure']*12;
            $monthly_installment    = round($this->emiCalculator($request['loan_amount'], ($lowets_year[0]*0.01), $request['loan_tenure']));

            if($request['prefered_rate_enable'] == 'yes'){
                $monthly_intrst         = ($request['prefered_rate_year1']*0.01)/12;
                $n                      = $request['loan_tenure']*12;
                $monthly_installment    = ceil($request['loan_amount'] * (($monthly_intrst*pow(1+$monthly_intrst,$n)) / ( pow(1+$monthly_intrst,$n)-1  )));

                $pre_yearly_interest = [$request['prefered_rate_year1'],$request['prefered_rate_year2'],$request['prefered_rate_year3'],$request['prefered_rate_year4'],$request['prefered_rate_year5'],$request['prefered_rate_year5'] ];
            }

            //Progressive Repayment Table
            $cash_downpayment = $request['downpayment'] - $request['cpf_available_amount'];
            $cpf_payment = $request['cpf_available_amount'];
            $loan_amount = $request['loan_amount'];

            $disbursement_month = !empty($request['disbursement_month'])? $request['disbursement_month'] : [];

            $disbursement_schedule = $this->getDisbursementSchedule($request['property_price'], $cash_downpayment, $cpf_payment, $loan_amount, $disbursement_month);

            //echo '<pre>'; print_r($disbursement_schedule); die;
            $progressive_repayment_table = [];
            $iQrate_progressive_emi_list = [];
            $prefered_progressive_emi_list = [];
            $iqrate_principal = 0;
            $prefered_principal = 0;
            $year = 1;
            $list_year = 1;
            $total_month = 0;
            $list_total_month = 0;
            $i = 1;
            $default_month = 0;
            $emi_IQrate = 0;
            $emi_prefered = 0;

            $buc_total_interest = 0;
            $buc_total_payment = 0;
            $buc_total_principal = 0;

            $buc_pre_total_interest = 0;
            $buc_pre_total_payment = 0;
            $buc_pre_total_principal = 0;

            //dd($bank_details);
            if(!empty($bank_details)){
                foreach($disbursement_schedule as $val){

                    $total_month    = $val['month'] + $total_month;
                    $year           = ceil($total_month/12);

                    $max_interest = $bank_details['year1'];
                    if(!empty($bank_details['year2'])){
                        $max_interest = $bank_details['year2'];
                    } elseif(!empty($bank_details['year3'])){
                        $max_interest = $bank_details['year3'];
                    } elseif(!empty($bank_details['year4'])){
                        $max_interest = $bank_details['year4'];
                    } elseif(!empty($bank_details['year5'])){
                        $max_interest = $bank_details['year5'];
                    } elseif(!empty($bank_details['year6'])){
                        $max_interest = $bank_details['year6'];
                    }

                    $interestRate = !empty($bank_details['year'.$year])? $bank_details['year'.$year] * 0.01 : $max_interest;

                    if(!empty($val['amount']['bank_loan'])){

                        $list_total_month = $val['month'] + $list_total_month;
                        $list_year      = ceil($list_total_month/12);

                        $interestRate = !empty($bank_details['year'.$list_year])? $bank_details['year'.$list_year] * 0.01 : $max_interest;
                        $percentage     = ($val['amount']['bank_loan']/$request['property_price'])*100;
                        $principal      = round(($request['property_price']*$percentage * 0.01), 2);
                        $emi_IQrate     = $this->emiCalculator_buc(($principal+$iqrate_principal), $interestRate, ($request['loan_tenure']*12)-$default_month);



                        if($request['prefered_rate_enable'] == 'yes'){
                            $interestRate_pre = $request['prefered_rate_year'.$list_year] * 0.01;
                            $emi_prefered = round($this->emiCalculator_buc(($principal+$prefered_principal), $interestRate_pre, ($request['loan_tenure']*12)-$default_month ), 2);
                        }

                        $iq_emi = $this->getBucEmiList($principal+$iqrate_principal, $interestRate, ($request['loan_tenure']*12)-$default_month, $emi_IQrate, $default_month, $val['month'], $val['name'], $percentage, $yearly_interest);

                        //dd($iq_emi);

                        $buc_total_interest = $buc_total_interest + $iq_emi['previous_total_interest'];
                        $buc_total_payment = $buc_total_payment + $iq_emi['previous_total_payment'];
                        $buc_total_principal = $buc_total_principal + $iq_emi['previous_total_principal_pay'];

                        if($request['prefered_rate_enable'] == 'yes'){

                            $pre_emi = $this->getBucEmiList($principal+$prefered_principal, ($request['prefered_rate_year'.$list_year] * 0.01), ($request['loan_tenure']*12)- $default_month, $emi_prefered, $default_month, $val['month'], $val['name'], $percentage, $pre_yearly_interest);

                            $buc_pre_total_interest = $buc_pre_total_interest + $pre_emi['previous_total_interest'];
                            $buc_pre_total_payment = $buc_pre_total_payment + $pre_emi['previous_total_payment'];
                            $buc_pre_total_principal = $buc_pre_total_principal + $pre_emi['previous_total_principal_pay'];

                            $prefered_principal = $pre_emi['previous_outstanding'];
                            unset($pre_emi['month']);
                            unset($pre_emi['previous_outstanding']);
                            unset($pre_emi['previous_total_interest']);
                            unset($pre_emi['previous_total_payment']);
                            unset($pre_emi['previous_total_principal_pay']);
                            $prefered_progressive_emi_list[$val['name']." - ".$percentage] =  $pre_emi;
                        }

                        //dd($iq_emi);
                        //$default_month = $iq_emi['month'];
                        $iqrate_principal = $iq_emi['previous_outstanding'];
                        unset($iq_emi['month']);
                        unset($iq_emi['previous_outstanding']);
                        unset($iq_emi['previous_total_interest']);
                        unset($iq_emi['previous_total_payment']);
                        unset($iq_emi['previous_total_principal_pay']);
                        $iQrate_progressive_emi_list[$val['name']." - ".$percentage] =  $iq_emi;
                        $default_month = $default_month+$val['month'];
                        $i++;
                    }




                    $disbursement_principal = round(($request['property_price']*$val['percentage'] * 0.01), 2);
                    $progressive_repayment_table[] = [
                        "Disbursement Stage" => $val['name'],
                        "Disbursement Range" => $val['month'],
                        "%" => $val['percentage'],
                        "Disbursement Amount" => $disbursement_principal,
                        "Disbursement Name" => $val['disbursement_name'],
                        "Payment Mode" => $val['amount'],
                        "Monthly Installment based on IQrate" => $emi_IQrate,
                        "Monthly Installment based on Prefered" => $emi_prefered,
                    ];

                }
            }

            //echo $buc_pre_total_payment.' = '.$buc_pre_total_principal.' + '.$buc_pre_total_interest; die;

            $rate_package_comparison['IQrate Lowest BUC Package ('.$lowets_year[0].'%)'] = [
                'Total Payment' => number_format($buc_total_payment, 2),
                'Total Principal' => number_format($request['loan_amount']),
                'Total Interest' => number_format($buc_total_payment - $request['loan_amount'], 2 ),
                'Yearly Interest' => $lowets_year,
            ];

            if($request['prefered_rate_enable'] == 'yes'){

                $rate_package_comparison['Your Prefered interest rate ('.$request['prefered_rate_year1'].'%)'] = [
                    'Total Payment' => number_format($buc_pre_total_payment, 2),
                    'Total Principal' => number_format($request['loan_amount']),
                    'Total Interest' => number_format($buc_pre_total_payment - $request['loan_amount'], 2 ),
                    'Yearly Interest' => [$request['prefered_rate_year1'],$request['prefered_rate_year2'],$request['prefered_rate_year3'],$request['prefered_rate_year4'],$request['prefered_rate_year5'],$request['prefered_rate_year5'] ]
                ];

            }
            return $response = [
                'Loan Details' => $loan_details,
                'Rate Package Comparison' => $rate_package_comparison,
                'IQrate Lowest Buc Package' => $bank_details,
                'Disbursement Schedule' => $progressive_repayment_table,
                'IQrate Exclusive BUC Rate' => $iQrate_progressive_emi_list,
                'Prefered BUC Rate' => $prefered_progressive_emi_list,
            ];
        } catch (Exception $e) {
            Console::log("BUC_MORTGAGE_REPORT: Error: ".$e->getMessage());
            return sendResponse(config('common.message.error_response_500.message'),500, false);
        }
    }

    public function getLowestPackages($request)
    {
        $token = $request['token'];
        $rates_details = Http::withToken($token)->get(config('calculator.buc_rates_url').'?loan_amount='.$request['loan_amount']);
        $res = [];
        //dd(json_decode($rates_details)); die;
        if(!empty($rates_details['rates'])){

            $max_key = count($rates_details['rates']) -1;

            $bank_details['name']       = $rates_details['bank']['name'];
            $bank_details['interest']   = $rates_details['rates'][$max_key]['total_interest_rate'];
            $bank_details['details']    = json_decode($rates_details);

            for($c = 0; $c <= $max_key; $c++)
            {
                $yearly_interest[$c] = $rates_details['rates'][$max_key-$c]['total_interest_rate'];
                $bank_details['year'.$c+1]   = $rates_details['rates'][$max_key-$c]['total_interest_rate'];
                $lowets_year[$c] = $rates_details['rates'][$max_key-$c]['total_interest_rate'];
            }
            if(count($yearly_interest) < 6){
                for($i = 0; $i < 6; $i++){
                    if(empty($yearly_interest[$i])){
                        $yearly_interest[$i] = !empty($rates_details['rates'][$max_key]['total_interest_rate'])? $rates_details['rates'][$max_key]['total_interest_rate'] : 1;
                    }
                }
            }
            //echo count($yearly_interest); die;

            $lowest_interest = $rates_details['rates'][$max_key]['total_interest_rate'];

            $res['lowest_interest'] = $lowest_interest;
            $res['bank_details']    = $bank_details;
            $res['yearly_interest'] = $yearly_interest;
        }
        //dd($res);

        return $res;
    }


    // public function getLowestPackages($request)
    // {
    //     $token = $request['token'];
    //     $rates_details = Http::withToken($token)->get(config('calculator.rates_url').'?property_types='.implode(',', config('calculator.buc_filter_property_types')).'&property_status='.implode(',', config('calculator.buc_filter_property_status')));
    //     //$rates_details = Http::withToken($token)->get(config('calculator.rates_url'));
    //     $rates_details = $rates_details->json();
    //     $rates_details = (!empty($rates_details['results']))? $rates_details['results'] : array();
    //     $res = [];
    //     $same_package = [];

    //     if(!empty($rates_details)){

    //         $lowest_interest = 100;
    //         foreach($rates_details as $rates)
    //         {
    //             if($rates['rate_type'] != 'fixed')
    //             {
    //                 $max_key = count($rates['rates']) -1;
    //                 if($lowest_interest > $rates['rates'][$max_key]['total_interest_rate'])
    //                 {
    //                     $same_package = [];
    //                     $bank_details['name']       = $rates['bank']['name'];
    //                     $bank_details['interest']   = $rates['rates'][$max_key]['total_interest_rate'];
    //                     $bank_details['details']   = $rates;
    //                     $same_package[] = $rates;

    //                     for($c = 0; $c <= $max_key; $c++)
    //                     {
    //                         $yearly_interest[$c] = $rates['rates'][$max_key-$c]['total_interest_rate'];
    //                         $bank_details['year'.$c+1]   = $rates['rates'][$max_key-$c]['total_interest_rate'];
    //                         $lowets_year[$c] = $rates['rates'][$max_key-$c]['total_interest_rate'];
    //                     }
    //                     $lowest_interest = $rates['rates'][$max_key]['total_interest_rate'];
    //                 }else if($lowest_interest == $rates['rates'][$max_key]['reference_rate']    ['interest_rate']){
    //                     $same_package[] = $rates;

    //                 }
    //             }
    //         }
    //         if(count($same_package) > 1){
    //             $temp = $same_package[0];
    //             foreach($same_package as $pack){
    //                 for($i = 0; $i< 6; $i++){
    //                     $a = !empty($pack['rates'][$i])? $pack['rates'][$i] : 100;
    //                     $b = !empty($temp['rates'][$i])? $temp['rates'][$i] : 0;
    //                     if($a < $b){
    //                         $temp = $pack;
    //                     }
    //                 }
    //             }
    //             $max_key = count($temp['rates']) -1;
    //             $bank_details = [];
    //             $yearly_interest = [];
    //             $bank_details['name']       = $temp['bank']['name'];
    //             $bank_details['interest']   = $temp['rates'][$max_key]['total_interest_rate'];
    //             $bank_details['details']   = $rates;
    //             for($c = 0; $c <= $max_key; $c++)
    //             {
    //                 $yearly_interest[$c] = $temp['rates'][$max_key-$c]['total_interest_rate'];
    //                 $bank_details['year'.$c+1]   = $temp['rates'][$max_key-$c]['total_interest_rate'];
    //             }
    //             $lowest_interest = $temp['rates'][$max_key]['total_interest_rate'];
    //         }

    //         $res['lowest_interest'] = $lowest_interest;
    //         $res['bank_details']    = $bank_details;
    //         $res['yearly_interest'] = $yearly_interest;
    //     }

    //     return $res;
    // }



}
