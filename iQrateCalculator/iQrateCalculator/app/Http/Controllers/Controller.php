<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use App\Models\CalculatorConfig;

class Controller extends BaseController
{
   /**
     * @OA\Info(
     *   title="Calculator API",
     *   version="1.0",
     *   description="This is an API that calculate data and response in JSON format",
     * )
     */

    public function __construct()
    {
        // $this->calculator_config = CalculatorConfig::select('key','value')->get();
        // foreach($this->calculator_config as $cal_key => $cal_value)
        // {
        //     if($this->isJson($cal_value->value) == 1)
        //     {
        //         $value = json_decode($cal_value->value,true);
        //     }
        //     else
        //     {
        //         $value = $cal_value->value;
        //     }
            
        //     config(['calculator.'.$cal_value->key => $value]);
        // }

    }

    // function isJson($string) {
    //     return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? 1 : 0;
    // }
    
}