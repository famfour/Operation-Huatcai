<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Http;
use Barryvdh\DomPDF\Facade\Pdf;
use Exception;
use Storage;


class SellerStampDutyController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @OA\Post(
     *     path="/api/calculator/seller-stamp-duty",
     *     operationId="/api/calculator/seller-stamp-duty",
     *     tags={"Sellers stamp duty"},
     *  @OA\Parameter(
     *         name="date",
     *         in="query",
     *         description="date",
     *         required=true,
     *         @OA\Schema(type="string", default="")
     *     ),
     *  @OA\Parameter(
     *         name="selling_price",
     *         in="query",
     *         description="selling_price",
     *         required=true,
     *         @OA\Schema(type="integer", default="0")
     *     ),
     *  @OA\Response(
     *         response="200",
     *         description="Sellers stamp duty",
     *     )
     * )
     */
    public function sellerStampDuty(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'selling_price' => 'required|numeric',
                'date'         => 'required|date|before_or_equal:today',
            ]);

            if ($validator->fails()) {
                return $response = array(
                    "statusCode" => 401,
                    "message" => $validator->messages()->all(),
                    "error" => "Bad Request",
                );
            }
            $date1 = new \DateTime(date('Y-m-d',strtotime($request['date'])));
            
            $date1->modify("-1 day");
            $date1->format("Y-m-d H:i:s");

            $date2 = new \DateTime(date('Y-m-d'));
            $interval = $date1->diff($date2);
            
            
            //dd($interval);

            $year = $interval->y.'.'.$interval->d;;
            $month = $interval->m;
            $day = $interval->d;
         
            if($year <= 1)
            {
                $purchase_value = config('calculator.purchase_value1');
            }
            else if($year > 1 && $year <= 2) // more than 1 and below or equal 2 year
            {
                $purchase_value = config('calculator.purchase_value2');
            }
            else if($year > 2 && $year <= 3) // more than 2 and below or equal 3 year
            {
                $purchase_value = config('calculator.purchase_value3');
            }
            else if($year > 3) // more than 3
            {
                $purchase_value = config('calculator.purchase_value4');
            }
            
            $seller_stamp_duty = ($purchase_value * filterNumber($request['selling_price'])) / 100;
            return $response = [
                'seller_stamp_duty' => number_format($seller_stamp_duty,2),
            ];
        } catch (Exception $e) {
            return $response = array(
                "statusCode" => 500,
                "message" => $e->getMessage(),
                "error" => "Bad Request",
            );
            return sendResponse(config('common.message.error_response_500.message'),500, false);
        }
    }
}