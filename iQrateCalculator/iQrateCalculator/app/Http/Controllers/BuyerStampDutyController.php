<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Http;
use Barryvdh\DomPDF\Facade\Pdf;
use Exception;
use Storage;

class BuyerStampDutyController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @OA\Post(
     *     path="/api/calculator/buyer-stamp-duty",
     *     operationId="/api/calculator/buyer-stamp-duty",
     *     tags={"Buyer stamp duty"},
     *     @OA\Parameter(
     *         name="loan_applicant",
     *         in="query",
     *         description="loan_applicant",
     *         required=false,
     *         @OA\Schema(type="integer", default="1")
     *     ),
     *    @OA\Parameter(
     *         name="purchase_price",
     *         in="query",
     *         description="purchase_price",
     *         required=true,
     *         @OA\Schema(type="integer", default="0")
     *     ),
     *   @OA\Parameter(
     *         name="applicant1_property",
     *         in="query",
     *         description="applicant1_property",
     *         required=true,
     *         @OA\Schema(type="integer", default="0")
     *     ),
     *   @OA\Parameter(
     *         name="applicant1_nationality",
     *         in="query",
     *         description="applicant1_nationality",
     *         required=true,
     *         @OA\Schema(type="string", default="0")
     *     ),
     *   @OA\Parameter(
     *         name="applicant2_property",
     *         in="query",
     *         description="applicant2_property",
     *         required=true,
     *         @OA\Schema(type="integer", default="0")
     *     ),
     *   @OA\Parameter(
     *         name="applicant2_nationality",
     *         in="query",
     *         description="applicant2_nationality",
     *         required=true,
     *         @OA\Schema(type="string", default="0")
     *     ),
     *   @OA\Parameter(
     *         name="applicant3_property",
     *         in="query",
     *         description="applicant3_property",
     *         required=true,
     *         @OA\Schema(type="integer", default="0")
     *     ),
     *   @OA\Parameter(
     *         name="applicant3_nationality",
     *         in="query",
     *         description="applicant3_nationality",
     *         required=true,
     *         @OA\Schema(type="string", default="0")
     *     ),
     *   @OA\Response(
     *         response="200",
     *         description="Response",
     *    )
     * )
     */
    public function buyerStampDuty(Request $request)
    {
        try {

            $rule = [];
            $rule['purchase_price'] = 'required|numeric';
            $rule['loan_applicant']  = ['required','numeric',Rule::in([1, 2, 3, 4])];

            if($request['loan_applicant'] >= 1)
            {
                $rule['applicant1_property'] = 'required_if:loan_applicant,==,1|required_if:loan_applicant,==,2|required_if:loan_applicant,==,3|required_if:loan_applicant,==,4|numeric';

                $rule['applicant1_nationality'] = ['required_if:loan_applicant,==,1','required_if:loan_applicant,==,2','required_if:loan_applicant,==,3','required_if:loan_applicant,==,4',Rule::in(config('calculator.nationality'))];
            }

            if($request['loan_applicant'] >= 2)
            {
                $rule['applicant2_property'] = 'required_if:loan_applicant,==,2|required_if:loan_applicant,==,3|required_if:loan_applicant,==,4|numeric';

                $rule['applicant2_nationality'] = ['required_if:loan_applicant,==,2','required_if:loan_applicant,==,3','required_if:loan_applicant,==,4',Rule::in(config('calculator.nationality'))];
            }

            if($request['loan_applicant'] >= 3)
            {
                $rule['applicant3_property'] = 'required_if:loan_applicant,==,3|required_if:loan_applicant,==,4|numeric';

                $rule['applicant3_nationality'] = ['required_if:loan_applicant,==,3','required_if:loan_applicant,==,4',Rule::in(config('calculator.nationality'))];
            }

            if($request['loan_applicant'] == 4)
            {
                $rule['applicant4_property'] = 'required_if:loan_applicant,==,4|numeric';

                $rule['applicant4_nationality'] = ['required_if:loan_applicant,==,4',Rule::in(config('calculator.nationality'))];
            }

            $validator = Validator::make($request->all(), $rule);

            if ($validator->fails()) {
                return $response = array(
                    "statusCode" => 401,
                    "message" => $validator->messages()->all(),
                    "error" => "Bad Request",
                );
            }
            $property1 = config('calculator.property1');
            $property2 = config('calculator.property2');
            $property3 = config('calculator.property3');
            $property4 = config('calculator.property4');

            $property1_percentage = config('calculator.property1_percentage');
            $property2_percentage = config('calculator.property2_percentage');
            $property3_percentage = config('calculator.property3_percentage');
            $property4_percentage = config('calculator.property4_percentage');

            $purchase_price = $request['purchase_price'];

            $max_purchanse_price = false;
            $buyer_stamp_duty1 = 0;
            $buyer_stamp_duty2 = 0;
            $buyer_stamp_duty3 = 0;
            $buyer_stamp_duty4 = 0;

            if($purchase_price > $property1){
                $new_purchase_price = $property1;
                $purchase_price = $purchase_price -  $property1;
                $buyer_stamp_duty1 = ($new_purchase_price * $property1_percentage) / 100;
            }
            else{
                $new_purchase_price = $purchase_price;
                $buyer_stamp_duty1 = ($new_purchase_price * $property1_percentage) / 100;
                $max_purchanse_price = true;
            }

            // return $buyer_stamp_duty1;

            if($max_purchanse_price == false)
            {
                if($purchase_price > $property2){
                    $new_purchase_price = $property2;
                    $purchase_price = $purchase_price -  $property2;
                    $buyer_stamp_duty2 = ($new_purchase_price * $property2_percentage) / 100;
                }
                else{
                    $new_purchase_price = $purchase_price;
                    $buyer_stamp_duty2 = ($new_purchase_price * $property2_percentage) / 100;
                    $max_purchanse_price = true;
                }
            }

            if($max_purchanse_price == false)
            {
                if($purchase_price > $property3){
                    $new_purchase_price = $property3;
                    $purchase_price = $purchase_price -  $property3;
                    $buyer_stamp_duty3 = ($new_purchase_price * $property3_percentage) / 100;
                }
                else{
                    $new_purchase_price = $purchase_price;
                    $buyer_stamp_duty3 = ($new_purchase_price * $property3_percentage) / 100;
                    $max_purchanse_price = true;
                }
            }

            if($max_purchanse_price == false)
            {
                //if($purchase_price > $property4){
                    //$new_purchase_price = $property4;
                    //$purchase_price = $purchase_price -  $property4;
                    $buyer_stamp_duty4 = ($purchase_price * $property4_percentage) / 100;
                //}
            }
            // return $buyer_stamp_duty4;
            $total_buyer_stamp_duty = $buyer_stamp_duty1 + $buyer_stamp_duty2 + $buyer_stamp_duty3 + $buyer_stamp_duty4;

            if($request['applicant1_property'] >= 3){
                $request['applicant1_property'] = 3;
            }
            if($request['applicant2_property'] >= 3){
                $request['applicant2_property'] = 3;
            }
            if($request['applicant3_property'] >= 3){
                $request['applicant3_property'] = 3;
            }

            if($request['applicant1_property'] > 3)
            {
                $request['applicant1_property'] = 3;
            }
            if($request['applicant2_property'] > 3)
            {
                $request['applicant2_property'] = 3;
            }
            if($request['applicant3_property'] > 3)
            {
                $request['applicant3_property'] = 3;
            }
            if($request['applicant4_property'] > 3)
            {
                $request['applicant4_property'] = 3;
            }

            $applicant1_percantage = 0;$applicant2_percantage = 0;$applicant3_percantage = 0;$applicant4_percantage = 0;

            $applicant1_percantage =  config('calculator.property_nationality')[$request['applicant1_property']][$request['applicant1_nationality']];

            if($request['loan_applicant'] >= 2)
            {
                if(isset($request['applicant2_property']) && $request['applicant2_property'] != '')
                {
                    $applicant2_percantage =  config('calculator.property_nationality')[$request['applicant2_property']][$request['applicant2_nationality']];
                }
            }

            if($request['loan_applicant'] >= 3)
            {
                if(isset($request['applicant3_property']) && $request['applicant3_property'] != '')
                {
                    $applicant3_percantage =  config('calculator.property_nationality')[$request['applicant3_property']][$request['applicant3_nationality']];
                }
            }
            if($request['loan_applicant'] >= 4)
            {
                if(isset($request['applicant4_property']) && $request['applicant4_property'] != '')
                {
                    $applicant4_percantage =  config('calculator.property_nationality')[$request['applicant4_property']][$request['applicant4_nationality']];
                }
            }

            $percantage = max($applicant1_percantage,$applicant2_percantage,$applicant3_percantage,$applicant4_percantage);

            $additional_buyer_stamp_duty = ($request['purchase_price'] * $percantage) / 100;

            return $response = [
                'buyer_stamp_duty' => number_format($total_buyer_stamp_duty,2),
                'additional_buyer_stamp_duty' => number_format($additional_buyer_stamp_duty,2),
                'total_buyer_stamp_duty' => number_format(($total_buyer_stamp_duty + $additional_buyer_stamp_duty),2),
            ];

            // Response for this function
            return sendResponse($response, 200, true);
        } catch (Exception $e) {
            return $response = array(
                "statusCode" => 500,
                "message" => $e->getMessage(),
                "error" => "Bad Request",
            );
            // return sendResponse(config('common.message.error_response_500.message'),500, false);
        }
    }

}
