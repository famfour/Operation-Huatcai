<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Http;
use Barryvdh\DomPDF\Facade\Pdf;
use Exception;
use Storage;


class EquityLoanController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @OA\Post(
     *     path="/api/calculator/equity-loan",
     *     operationId="/api/calculator/equity-loan",
     *     tags={"Equity Loan"},
     *     @OA\Parameter(
     *         name="existing_housing_loans",
     *         in="query",
     *         description="existing_housing_loans",
     *         required=true,
     *         @OA\Schema(type="integer", default="1")
     *     ),
     *  @OA\Parameter(
     *         name="current_valuation",
     *         in="query",
     *         description="current_valuation",
     *         required=true,
     *         @OA\Schema(type="integer", default="500")
     *     ),
     *  @OA\Parameter(
     *         name="total_cpf",
     *         in="query",
     *         description="total_cpf",
     *         required=true,
     *         @OA\Schema(type="integer", default="2")
     *     ),
     *  @OA\Parameter(
     *         name="outstanding_loan",
     *         in="query",
     *         description="outstanding_loan",
     *         required=true,
     *         @OA\Schema(type="integer", default="1")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Equity loan",
     *     )
     * )
     */
    public function equityLoan(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'existing_housing_loans' => 'required|numeric',
                'current_valuation'         => 'required|numeric',
                'loan_tagged'         => ['required_if:existing_housing_loans,==,1',Rule::in(['yes','no'])],
                'total_cpf'         => 'numeric|nullable',
                'outstanding_loan'      => 'numeric|nullable',
            ],['loan_tagged.in' => "The selected loan tagged is invalid. It's value either yes or no"]);

            if ($validator->fails()) {
                return $response = array(
                    "statusCode" => 401,
                    "message" => $validator->messages()->all(),
                    "error" => "Bad Request",
                );
            }

            if($request['total_cpf'] == '')
            {
                $request['total_cpf'] = 0;
            }

            if($request['outstanding_loan'] == '')
            {
                $request['outstanding_loan'] = 0;
            }

            if($request['existing_housing_loans'] == 0)
            {
                $max_loan_amount = (config('calculator.loan_amount0') * filterNumber($request['current_valuation'])) / 100;
            }
            else if($request['existing_housing_loans'] == 1)
            {
                if($request['loan_tagged'] == 'yes')
                {
                    $max_loan_amount = (config('calculator.loan_amount1_yes') * filterNumber($request['current_valuation'])) / 100;
                }
                else
                {
                    $max_loan_amount = (config('calculator.loan_amount1_no') * filterNumber($request['current_valuation'])) / 100;
                }

            }
            else if($request['existing_housing_loans'] >= 2)
            {
                $max_loan_amount = (config('calculator.loan_amount2') * filterNumber($request['current_valuation'])) / 100;
            }
            $max_equity_load = $max_loan_amount - filterNumber($request['total_cpf']) - filterNumber($request['outstanding_loan']);
            $max_equity_load = ($max_equity_load < 0)? 0 : $max_equity_load;

            return $response = [
                'maximum_equity_loan' => number_format($max_equity_load,2),
            ];
            // Response for this function
            //return sendResponse($response, 200, true);
        } catch (Exception $e) {
            return $response = array(
                "statusCode" => 500,
                "message" => $e->getMessage(),
                "error" => "Bad Request",
            );
            // return sendResponse(config('common.message.error_response_500.message'),500, false);
        }

    }
}
