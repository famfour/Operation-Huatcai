<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Http;
use App\Models\CalculatorConfig;


class CalculatorConfigController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        $data['calculator_config'] = CalculatorConfig::get();
        return view('calculator_config',$data);
    }

    public function createUpdate(Request $request)
    {
        // return $request->all();
        if(isset($request->edit_id))
        {
            CalculatorConfig::where('id',$request['edit_id'])->update(['key' => $request['key'],'value' => $request['value']]);
        }
        else
        {
            CalculatorConfig::create([
                'key' => $request['key'],
                'value' => $request['value'],
            ]);
        }

        return redirect('/calculator-config');
    }

    public function edit($id)
    {
        return $cal_config = CalculatorConfig::find($id);
    }
}