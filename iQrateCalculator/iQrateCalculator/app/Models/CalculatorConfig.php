<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CalculatorConfig extends Model
{
    use HasFactory;
    protected $table = "calculator_config";
    protected $fillable = [
        'key','value'
    ];

}
