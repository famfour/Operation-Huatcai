<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\Console\Output\ConsoleOutput;
class Console extends Model
{
    
     /**
     * To log the messages in console or terminal
     *
     * @var string
     */
    public static function log($str) {
        //$output = new ConsoleOutput();
        //$output->writeln($str);
        error_log($str);
    }

}
