<?php

if (! function_exists('filterNumber')) {
    function filterNumber($str) {
        return (float) filter_var($str, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );
    }
}

/**
 * Display a response listing request.
 *
 * @param  $code  (number) API response code
 * @param  $status  (boolean) true/false
 * @param  $data  (any) response data
 * @param  $message  (string) API response message details
 * @return \Illuminate\Http\Response
 */
if (! function_exists('sendResponse')) {
    function sendResponse($title, $code = 200, $status = false,$response_separate = true)
    {
        // Response common message's
        if ($status) {
            $response['success'] = config('api-response.message.success_response')['success'] ;
        } else {
            $response['success']  = config('api-response.message.error_response_'.$code)['success'] ;
        }
        // If Data data then pass it otherwise default value
        // if ($response_separate) {
        //     $response['data'] = $data;
        // } else {
        //     $response = $data;
        // }

        // dd($response);

        // $response['program']    = config('api-response.app_name');
        $response['calculator_response']      = $title;
        // $response['version']    = '1';        
        $response['datetime']   = date('Y-m-d H:i:s');       
        
        // Replay to response corresponding URL
        return response($response, $code)->header('Content-Type', config('api-response.api_content_type'));
    }
}

