<?php

return [

    'app_name'  => 'iQrateCalculator',
    'api_content_type' => 'application/json',
    // Custom Error Messages
    'message' => [
        'success_response' => [
            'success'     => true,
            'data'        => null,
            'message'     => 'Success',
            'code'        => 200,
        ],
        'error_response_403' => [
            'success' => false,
            'data'    => null,
            'message' => 'Permission denied',
            'code'    => 403,
        ],
        'error_response_401' => [
            'success' => false,
            'data'    => null,
            'message' => 'Access Denied',
            'code'    => 401,
        ],
        'error_response_404' => [
            'success' => false,
            'data'    => null,
            'message' => 'API not found',
            'code'    => 404,
        ],
        'error_response_500' => [
            'success' => false,
            'data'    => null,
            'message' => 'Whoops, looks like something went wrong',
            'code'    => 500,
        ],
    ],
];
