<?php

return [
    'loan_amount0' => 75,
    'loan_amount1_yes' => 75,
    'loan_amount1_no' => 45,
    'loan_amount2' => 45,
    'purchase_value1' => 12,
    'purchase_value2' => 8,
    'purchase_value3' => 4,
    'purchase_value4' => 0,

    'maximum_loan_tenure' => 30,
    'monthly_variable' => 0.7,
    'lowest_intrest_rate' => 0.7,
    'company_commertial_rate' => 0.2,
    'default_installment' => 0.3,
    'monthly_installment' => 4,
    'msr' => 30,
    'tdsr' => 55,
    'default_interest' => 4,
    'fledge_amt' => 48,
    'valuation1' => 325,
    'valuation2' => 325,
    'valuation3' => 325,
    'valuation4' => 400,
    'valuation5' => 450,
    'valuation6' => 550,
    'valuation7' => 550,
    'valuation8' => 625,
    'valuation9' => 625,
    'valuation10' => 700,
    'valuation11' => 700,
    'valuation12' => 700,
    'valuation_exces' => 0.02,
    'property_type_name' => [1 => "hdb", 2 => "condominium_apartment", 3 => "strata_housing_townhouses", 4 => "ec_resale_out_of_mop", 5 => "ec_from_developer_within_mop", 6 =>  "landed"],
    'legal_fees_url' => env("BASE_URL").'/customer/lawfirm/all',
    //'legal_fees_url' => 'https://dev-api.iqrate.io/customer/lawfirm/all',
    'rates_url' => env("BASE_URL").'/customer/bank/packages',
    //'rates_url' => 'https://dev-api.iqrate.io/customer/bank/packages',

    'bank_names_api' => env("BASE_URL").'/customer/bank/banks',
    //'bank_names_api' => 'https://dev-api.iqrate.io/customer/bank/banks',
    'buc_rates_url' => env("BASE_URL").'/customer/bank/packages/buc_calculator',
    //'buc_rates_url' => 'https://dev-api.iqrate.io/customer/bank/packages/buc_calculator',
    'new_purchase_rates_url' => env("BASE_URL").'/customer/bank/packages/new_purchase_calculator',
    //'new_purchase_rates_url' => 'https://dev-api.iqrate.io/customer/bank/packages/new_purchase_calculator',

    'default_down_payment' => 0.25,
    'emi' =>0.013,
    'refinance_subsidy' =>0.004,
    'cimb_subsidy' =>0.003,
    'citi_subsidy' =>0.002,
    'lowest_floating' =>0.013,
    'lowest_fixed' =>0.014,
    'yearly_interest' => ['lowest_fixed' => [0.014], 'lowest_floating' => [0.013, 0.014]],
    'buc_interest_year1' => 1.1,
    'buc_interest_year2' => 1.2,
    'buc_interest_year3' => 1.3,
    'buc_interest_year4' => 1.4,
    'buc_interest_year5' => 1.5,

    'disbursement_ratio' => [
        [
            'name' => 'option_to_purchase',
            'description' => 'Option To Purchase',
            'percentage' => 5,
            'cash_downpayment' => 0,
            'cpf_downpayment' => 0,
            'loan_amount' => 0,
            'month' => 6
        ],
        [
            'name' => 'downpayment',
            'description' => 'Downpayment',
            'percentage' => 15,
            'cash_downpayment' => 0,
            'cpf_downpayment' => 0,
            'loan_amount' => 0,
            'month' => 6
        ],
        [
            'name' => 'foundation',
            'description' => 'Foundation',
            'percentage' => 10,
            'cash_downpayment' => 0,
            'cpf_downpayment' => 0,
            'loan_amount' => 0,
            'month' => 6
        ],
        [
            'name' => 'concrete_framework',
            'description' => 'Concrete Framework',
            'percentage' => 10,
            'cash_downpayment' => 0,
            'cpf_downpayment' => 0,
            'loan_amount' => 0,
            'month' => 6
        ],
        [
            'name' => 'walls',
            'description' => 'Walls',
            'percentage' => 5,
            'cash_downpayment' => 0,
            'cpf_downpayment' => 0,
            'loan_amount' => 0,
            'month' => 6
        ],
        [
            'name' => 'ceiling',
            'description' => 'Ceiling',
            'percentage' => 5,
            'cash_downpayment' => 0,
            'cpf_downpayment' => 0,
            'loan_amount' => 0,
            'month' => 6
        ],
        [
            'name' => 'doors_and_windows',
            'description' => 'Doors and Windows',
            'percentage' => 5,
            'cash_downpayment' => 0,
            'cpf_downpayment' => 0,
            'loan_amount' => 0,
            'month' => 6
        ],
        [
            'name' => 'car_park',
            'description' => 'Car Park',
            'percentage' => 5,
            'cash_downpayment' => 0,
            'cpf_downpayment' => 0,
            'loan_amount' => 0,
            'month' => 6
        ],
        [
            'name' => 'temporary_occupation_permit',
            'description' => 'Temporary Occupation Permit',
            'percentage' => 25,
            'cash_downpayment' => 0,
            'cpf_downpayment' => 0,
            'loan_amount' => 0,
            'month' => 6
        ],
        [
            'name' => 'certificate_of_statutory_completion',
            'description' => 'Certificate of Statutory Completion',
            'percentage' => 15,
            'cash_downpayment' => 0,
            'cpf_downpayment' => 0,
            'loan_amount' => 0,
            'month' => 6
        ]
        ],

    'property_types_key' => [
        "condominium_apartment" => 'Condominium / Apartment',
        "landed" => 'Landed',
        "hdb" => 'HDB',
        "ec_resale_out_of_mop" => 'EC (Resale / Out of MOP)',
        "ec_from_developer_within_mop" => 'EC (From Developer / With oin MOP)',
        "strata_housing_townhouses" => 'Starta Housing / Townhouses'
    ],

    'loan_types_key' => [
        "refinance" => 'Refinance',
        "new_purchase_with_option_to_purchase" => 'New Purchase (With Option To Purchase)',
        "part_purchase_decoupling_divorce" => 'Part Purchase / Decoupling / Divorce',
        "cash_out_term_loan" => 'Cash Out (Term Loan)',
        "new_purchase_approval_in_principle" => 'New Purchase (Approval in Principle)'
      ],

    'property1' => 180000,
    'property2' => 180000,
    'property3' => 640000,
    'property4' => 1000000,

    'property1_percentage' => 1,
    'property2_percentage' => 2,
    'property3_percentage' => 3,
    'property4_percentage' => 4,

    'nationality' => array(
        'Singapore Citizens', 'Iceland Citizens / Permanent Residents', 'Liechtenstein Citizens / Permanent Residents', 'Norway Citizens / Permanent Residents', 'Switzerland Citizens / Permanent Residents', 'USA Citizens', 'Singapore Permanent Residents', 'Others',
    ),

    'disbursement_name' => array(
        'Completion of Foundation', 'Reinforcement Concrete', 'Brick Walls', 'Roofing/ Ceiling', 'Electrical Wiring', 'Car Park, Roads, Drains', 'Temporary Occupation Permit (TOP)', 'Certificate of Statutory Completion (CSC)'
    ),

    'buc_filter_property_types' => array("condominium_apartment", "landed", "ec_resale_out_of_mop", "ec_from_developer_within_mop","strata_housing_townhouses"),
    'buc_filter_property_status' => array("under_construction_to_obtain_top_within_2_years",
    "under_construction_to_obtain_top_more_than_2_years"),

    'property_nationality' => array(
        0 => array(
            'Singapore Citizens' => 0,
            'Iceland Citizens / Permanent Residents' => 0,
            'Liechtenstein Citizens / Permanent Residents' => 0,
            'Norway Citizens / Permanent Residents' => 0,
            'Switzerland Citizens / Permanent Residents' => 0,
            'USA Citizens' => 0,
            'Singapore Permanent Residents' => 5,
            'Others' => 30,
        ),
        1 => array(
            'Singapore Citizens' => 17,
            'Iceland Citizens / Permanent Residents' => 17,
            'Liechtenstein Citizens / Permanent Residents' => 17,
            'Norway Citizens / Permanent Residents' => 17,
            'Switzerland Citizens / Permanent Residents' => 17,
            'USA Citizens' => 17,
            'Singapore Permanent Residents' => 25,
            'Others' => 30,
        ),
        2 => array(
            'Singapore Citizens' => 25,
            'Iceland Citizens / Permanent Residents' => 25,
            'Liechtenstein Citizens / Permanent Residents' => 25,
            'Norway Citizens / Permanent Residents' => 25,
            'Switzerland Citizens / Permanent Residents' => 25,
            'USA Citizens' => 25,
            'Singapore Permanent Residents' => 30,
            'Others' => 30,
        ),
        3 => array(
            'Singapore Citizens' => 25,
            'Iceland Citizens / Permanent Residents' => 25,
            'Liechtenstein Citizens / Permanent Residents' => 25,
            'Norway Citizens / Permanent Residents' => 25,
            'Switzerland Citizens / Permanent Residents' => 25,
            'USA Citizens' => 25,
            'Singapore Permanent Residents' => 30,
            'Others' => 30,
        ),
    ),

];
