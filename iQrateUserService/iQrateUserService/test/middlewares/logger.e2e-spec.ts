import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, LoggerService } from '@nestjs/common';
import * as request from 'supertest';
import { WINSTON_MODULE_NEST_PROVIDER } from 'nest-winston';
import { AppModule } from '../../src/app.module';

describe('Logger (e2e)', () => {
  let app: INestApplication;
  let logger: LoggerService;
  let loggerSpy: any;
  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();

    logger = app.get(WINSTON_MODULE_NEST_PROVIDER);
    loggerSpy = jest.spyOn(logger, 'log');
  });

  it('should log when there is no error in the API', async () => {
    await request(app.getHttpServer()).get('/healthCheck').expect(200);

    // logged twice - one for request and one for response.
    expect(loggerSpy).toHaveBeenCalledTimes(2);
  });

  it('should log when there is an error in the API', async () => {
    await request(app.getHttpServer())
      .post('/unknownPath')
      .send({ test: 'test' })
      .expect(404);

    // logged twice - one for request and one for response.
    expect(loggerSpy).toHaveBeenCalledTimes(2);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });
});
