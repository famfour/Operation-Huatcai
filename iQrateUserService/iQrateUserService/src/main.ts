import * as http from 'http';
import * as express from 'express';
import * as helmet from 'helmet';
import { NestFactory, Reflector } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import * as cookieParser from 'cookie-parser';
import { ClassSerializerInterceptor, ValidationPipe } from '@nestjs/common';
import { ExpressAdapter } from '@nestjs/platform-express';
import { WINSTON_MODULE_NEST_PROVIDER } from 'nest-winston';
import { ConfigService } from './config/config.service';
import { AppModule } from './app.module';

async function bootstrap() {
  const server = express();
  const configService = new ConfigService();
  const app = await NestFactory.create(AppModule, new ExpressAdapter(server));

  app.useGlobalInterceptors(
    new ClassSerializerInterceptor(app.get(Reflector))
  );
  app.useLogger(app.get(WINSTON_MODULE_NEST_PROVIDER));
  app.useGlobalPipes(new ValidationPipe());
  app.use(cookieParser());
  app.use(helmet());
  app.enableCors();
  // Swagger UI hosted at /api path
  const swaggerDoc = getSwaggerDoc(app);
  const SWAGGER_ENDPOINT = configService.envConfig.SWAGGER_ENDPOINT || 'user-service-swagger';
  SwaggerModule.setup(SWAGGER_ENDPOINT, app, swaggerDoc);

  // Load ACL data into memory for better performance and avoid db hit for acl check
  await configService.loadAllAclDataIntoMemoryViaAppVariable(app);

  await app.listen(configService.envConfig.HTTP_PORT);
}

function getSwaggerDoc(app) {
  const config = new DocumentBuilder()
    .setTitle('User Service')
    .setDescription('Wrapper for User Service APIs')
    .setVersion('1.0')
    .addBearerAuth()
    .build();

  return SwaggerModule.createDocument(app, config);
}

bootstrap();
