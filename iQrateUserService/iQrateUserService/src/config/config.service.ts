import * as dotenv from 'dotenv';
import * as fs from 'fs';
import { Injectable } from '@nestjs/common';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import * as path from 'path';
import { LoggerOptions } from 'winston';
import { loggerOptions } from './logger-options';

import Debug from 'debug';
import {MembershipAcl} from "../entities/membership_acl";
import {AdminAcl} from "../entities/admin_acl";

const debug = Debug('user-service:ConfigService');

interface IEnvConfigInterface {
  [key: string]: string;
}

@Injectable()
export class ConfigService {
  [x: string]: any;
  public readonly envConfig: IEnvConfigInterface;
  public membershipAcls: MembershipAcl[];
  public adminAcls: AdminAcl[];

  constructor() {
    if (
      !['prod', 'production'].includes(
        (process.env.NODE_ENV || '').toLowerCase(),
      )
    ) {
      const filePath = `${process.env.NODE_ENV || 'dev'}.env`;
      const config = dotenv.parse(fs.readFileSync(filePath));
      ConfigService.validateInput(config);
      this.envConfig = config;
    } else {
      this.envConfig = process.env;
    }

    debug('getTypeORMConfig() : ',JSON.stringify(this.getTypeORMConfig()));

  }

  public getMicroServiceUrl(microservice:string) {
    switch(microservice) {
      case 'customer':
        return this.envConfig.CUSTOMER_SERVICE_BASEURL;
        break;
      case 'strapi':
        return this.envConfig.STRAPI_SERVICE_BASEURL;
        break;
      default:
        return undefined;
        break;
    }
  }

  public getTypeORMConfig(): TypeOrmModuleOptions {
    const baseDir = path.join(__dirname, '../');
    const entitiesPath = `${baseDir}entities/*{.ts,.js}`;
    const TYPEORM_ENTITY_SYNC = this.envConfig.TYPEORM_ENTITY_SYNC==='1'?true:false;
    debug('this.envConfig.TYPEORM_ENTITY_SYNC :', this.envConfig.TYPEORM_ENTITY_SYNC);
    debug('TYPEORM_ENTITY_SYNC :', TYPEORM_ENTITY_SYNC);
    return {
      type: 'postgres',
      host: this.envConfig.TYPEORM_HOST,
      username: this.envConfig.TYPEORM_USERNAME,
      password: this.envConfig.TYPEORM_PASSWORD,
      database: this.envConfig.TYPEORM_DATABASE,
      port: Number.parseInt(this.envConfig.TYPEORM_PORT, 10),
      logging: false,
      synchronize: TYPEORM_ENTITY_SYNC,
      entities: [entitiesPath],
    };
  }

  public getKeyCloakConfig(): any {
  return {
    authServerUrl: this.envConfig.KEYCLOAK_BASE_URL,
    realm:this.envConfig.KEYCLOAK_RELAM,
    clientId:this.envConfig.KEYCLOAK_CLIENT,
    secret:this.envConfig.KEYCLOAK_SECRET
  };
}

  public getSmtpConfig() {
    return {
      host: this.envConfig.SMTP_HOST || 'smtp.gmail.com',
      port: this.envConfig.SMTP_PORT || 465,
      secure: this.envConfig.SMTP_ISSECURE || true,
      requireTLS: this.envConfig.SMTP_ISREQUIRETLS || false,
      logger: true,
    };
  }

  public getLoggerConfig(): LoggerOptions {
    return loggerOptions;
  }

  private static validateInput(envConfig: IEnvConfigInterface) {
    this.validateDbConfig(envConfig);
  }

  private static validateDbConfig(envConfig: IEnvConfigInterface) {
    const {
      TYPEORM_HOST,
      TYPEORM_USERNAME,
      TYPEORM_PASSWORD,
      TYPEORM_DATABASE,
      TYPEORM_PORT,
    } = envConfig;

    if (
      !TYPEORM_HOST ||
      !TYPEORM_USERNAME ||
      !TYPEORM_PASSWORD ||
      !TYPEORM_DATABASE ||
      !TYPEORM_PORT
    ) {
      throw new Error('Missing TYPE_ORM Config');
    }
  }

  // Load Acl data into memory
  public async loadAllAclDataIntoMemoryViaAppVariable(app) {
    this.adminAcls = await AdminAcl.find();
    this.membershipAcls = await MembershipAcl.find();
    GlobalService.adminAcls = this.adminAcls;
    GlobalService.membershipAcls = this.membershipAcls;
    // app.set('membershipAcls', this.membershipAcls);
    // app.set('adminAcls', this.adminAcls);
  }

  public getJWTCredentials() {
    return {
      JWT_SECRET_KEY: process.env.JWT_SECRET_KEY,
    };
  }
}

export class GlobalService{
  static adminAcls: AdminAcl[];
  static membershipAcls:MembershipAcl[];
}
