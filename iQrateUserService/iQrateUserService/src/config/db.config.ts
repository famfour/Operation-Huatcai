import * as path from 'path';

const baseDir = path.join(__dirname, '../');
const entitiesPath = `${baseDir}${process.env.TYPEORM_ENTITIES}`;

const TYPEORM_SYNCHRONISE = process.env.TYPEORM_SYNCHRONISE==='1'?true:false;

// debug('TYPEORM_SYNCHRONISE :', TYPEORM_SYNCHRONISE);

export default {
  type: process.env.TYPEORM_CONNECTION,
  host: process.env.TYPEORM_HOST,
  username: process.env.TYPEORM_USERNAME,
  password: process.env.TYPEORM_PASSWORD,
  database: process.env.TYPEORM_DATABASE,
  // TODO: Delete this once the schemas are stable
  synchronise: TYPEORM_SYNCHRONISE,
  port: Number.parseInt(process.env.TYPEORM_PORT, 10),
  entities: [entitiesPath],
  seeds: [`src/db/seeds/*.seed.ts`],
  cli: {
    entitiesDir: 'src/db/entities',
  },
};
