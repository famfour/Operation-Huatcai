import { Client } from 'pg';

import { ConfigService } from '../config/config.service';

import * as Debug from 'debug';

const debug = Debug('pg-events:all');

const config = new ConfigService();

let client = new Client({
    host: config.envConfig.TYPEORM_HOST,
    port: config.envConfig.TYPEORM_PORT || 5432,
    user: config.envConfig.TYPEORM_USERNAME,
    password: config.envConfig.TYPEORM_PASSWORD,
    application_name: 'EventsListening',
    keepAlive: true
});

init(client);

async function init(client) {
    await client.connect();
    debug('pg client connected for events');
    const eventName = 'acl_inserts_update_notification';
    const query = await client.query(`LISTEN ${eventName}`);
    debug('query :', query);
    debug(`pg client started listening for ${eventName} event`);
    client.on('notification', async (msg) => {
        debug('message received from ${eventName} : ', msg);
    });
}

// client.connect(async (err) => {
//    if(err) {
//        console.error(' client.connect : ', err);
//    }
//    debug('pg client connected for events');
//     // var query = client.query('LISTEN search');
//
//     const eventName = 'acl_inserts_update_notification';
//
//     const query = await client.query(`LISTEN ${eventName}`);
//
//     debug('query :', query);
//
//     debug(`pg client started listening for ${eventName} event`);
//     client.on('notification', async (msg) => {
//         debug('message received from ${eventName} : ', msg);
//     });
// });