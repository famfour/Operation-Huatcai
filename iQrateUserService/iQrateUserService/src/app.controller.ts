import { Controller, Get, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AppService } from './app.service';
import {Req} from "@nestjs/common";
import {Request} from "express";
import Debug from 'debug';
const debug = Debug('acl-check:app-controller');

@ApiTags('Utils')
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) { }

  @Get('healthCheck')
  getDate(): string {
    return this.appService.getDate();
  }

  @Post('introspection')
  async introspection(@Req() req: Request):Promise<any> {
    debug('Headers : ', JSON.stringify(req.headers));
    debug('Body : ', JSON.stringify(req.body));
    return await this.appService.introspection(req);
    // return true;
  }

  @Post('authorise')
  async authorise(@Req() req: Request):Promise<any> {
    debug('Headers : ', JSON.stringify(req.headers));
    // debug('Body : ', JSON.stringify(req.body));
    return await this.appService.authorise(req);
    // return true;
  }

  @Post('adminintrospection')
  async adminintrospection(@Req() req: Request):Promise<any> {
    debug('Headers : ', JSON.stringify(req.headers));
    debug('Body : ', JSON.stringify(req.body));
    return await this.appService.adminIntrospection(req);
    // return true;
  }

  @Get('mytest')
  getSDate(): string {
    return "welcome test";
  }
  
}
