import common_1, {HttpException, HttpStatus, Injectable} from '@nestjs/common';
import RedisClient from './usecases/utils/redis';
import { ConfigService } from './config/config.service';
import {Request} from "express";
import {MemberShipAclService} from "./middlewares/acl_module/membership_acl_service";
import Debug from 'debug';
import {JWT} from "./usecases/utils/jwt";
import {KcTokenData} from "./middlewares/KcTokenData.dto";
import {AdminAclService} from "./middlewares/acl_module/admin_acl_service";
const debug = Debug('acl-check-service:app-controller');

@Injectable()
export class AppService {
  private config;
  constructor(config: ConfigService) {
    this.config=config;
    RedisClient.init(config);

    // const pgEventInst = new PgEvents();
    // debug('pgEventInst : ', pgEventInst.client.toString());
  }

  async verifyAndDecode(kcToken: string):Promise<any> {
    const publicKeyB64 = this.config.envConfig.KC_JWT_PUBLIC_KEY_BASE64;
    return JWT.decodeAndVerify<KcTokenData>(publicKeyB64, kcToken);
  }

  async adminTokenVerifyAndDecode(kcToken: string):Promise<any> {
    const publicKeyB64 = this.config.envConfig.KC_ADMIN_JWT_PUBLIC_KEY_BASE64;
    return JWT.decodeAndVerify<KcTokenData>(publicKeyB64, kcToken);
  }

  async adminIntrospection(req: Request): Promise<any> {

    const auth_token = req.body.headers.authorization || req.body.headers.strapiauth;

    // Return 401 if authorization is missing
    if (!req || !req.body || !req.body.headers || !auth_token) {
      throw new HttpException(
          {
            status: HttpStatus.UNAUTHORIZED,
            message: 'Unauthorized Access. Please provide the valid token to access this api',
            name: 'AuthMiddleware_TOKEN_MISSING',
          },
          HttpStatus.UNAUTHORIZED,
      );
    }

    try {
      // let kcToken = req.body.headers.authorization.split('Bearer ')[1];
      //
      // if(req.body.headers.strapiauth) {
      //   kcToken = req.body.headers.strapiauth.split('Bearer ')[1];
      // }

      const kcToken = auth_token.split('Bearer ')[1];

      const verifiedDecodedTokenData = await this.adminTokenVerifyAndDecode(
          kcToken,
      );
      debug('verifiedDecodedTokenData : ', JSON.stringify(verifiedDecodedTokenData));


      req.headers.admin_userid = verifiedDecodedTokenData.sub;
      req.headers.usertype = verifiedDecodedTokenData.azp;
      debug('req.body.headers : ', req.body.headers);
      debug(' realm_access : ', verifiedDecodedTokenData.realm_access.roles);

      const roles =
          verifiedDecodedTokenData &&
          verifiedDecodedTokenData.realm_access &&
          verifiedDecodedTokenData.realm_access.roles
              ? verifiedDecodedTokenData.realm_access.roles
              : [];

      debug('user roles :', JSON.stringify(roles));
      const adminacl = {
        roles,
        api_endpoint: req.body.headers.target_uri,
        accesstype: req.body.headers.target_method,
        userid: verifiedDecodedTokenData.sub
      };

      debug(' adminacl : ', JSON.stringify(adminacl));

      const adminaclcheck = await AdminAclService.verify_admin_acl(adminacl);

      debug('adminaclcheck : ', adminaclcheck);
      if (adminaclcheck.permission === true) {
        return req.body;
      } else {
        throw new HttpException(
            {
              name: "Unauthorized Access",
              status: HttpStatus.FORBIDDEN,
              message: 'Unauthorized Access. You do not have permission to access this api',
              errorcode: 'AdminIntrospection_003',
            },
            HttpStatus.FORBIDDEN,
        );
      }
    } catch (err) {
      debug('admin introspection error :',err);
      if(err.expiredAt) {
        throw new HttpException({
          status: HttpStatus.UNAUTHORIZED,
          name: 'TOKEN_EXPIRED',
          // message: `jwt token2 has expired at ${err.expiredAt}`,
          message: `Session expired`,
          errorcode: 'AdminIntrospection_002',
        }, HttpStatus.UNAUTHORIZED);
      } 
      // else if(err.name) {
      //   throw err;
      // }
      else if (err.name) {
        debug('has err message');
        const mainString = err.message;
        const theWord  = "ES256";
        if (mainString.indexOf(theWord) !== -1 || err.name === 'TypeError' || err.name === 'JsonWebTokenError') {
          console.log('The word "' + theWord + '" exists in given string.');
          debug('wrong token');
              throw new HttpException({
                name : 'INVALID_TOKEN',
                message :  'Invalid admin token'
              },HttpStatus.BAD_REQUEST);
        } else {
          throw err;
        }
      }

      throw new HttpException({
        status: HttpStatus.UNAUTHORIZED,
        name: 'INVALID_TOKEN',
        message: `Invalid token detected.  Please contact system support`,
        errorcode: 'AdminIntrospection_001',
      }, HttpStatus.UNAUTHORIZED);
    }

  }

  async authorise(req: Request): Promise<any> {
    if (!req || !req.headers || !req.headers.authorization) {
      throw new HttpException(
          {
            status: HttpStatus.UNAUTHORIZED,
            message: 'Unauthorized Access. Please provide the valid token to access this api',
            name: 'AuthMiddleware_TOKEN_MISSING',
          },
          HttpStatus.UNAUTHORIZED,
      );
    }
    try {
      const kcToken = req.headers.authorization.split('Bearer ')[1];
      const verifiedDecodedTokenData = await this.verifyAndDecode(
          kcToken,
      );
      debug('verifiedDecodedTokenData : ', verifiedDecodedTokenData);
      return verifiedDecodedTokenData;
    } catch (err) {
      throw err;
    }
  }

  async introspection(req: Request): Promise<any> {


    const auth_token = req.body.headers.authorization || req.body.headers.strapiauth;

    // Return 401 if authorization is missing
    if (!req || !req.body || !req.body.headers || !auth_token) {
      throw new HttpException(
          {
            status: HttpStatus.UNAUTHORIZED,
            message: 'Unauthorized Access. Please provide the valid token to access this api',
            name: 'AuthMiddleware_TOKEN_MISSING',
          },
          HttpStatus.UNAUTHORIZED,
      );
    }

    try {

      const kcToken = auth_token.split('Bearer ')[1];
      const verifiedDecodedTokenData = await this.verifyAndDecode(
          kcToken,
      );
      debug('verifiedDecodedTokenData : ', verifiedDecodedTokenData);

      // Populate both body header and actual header for testing

      req.body.headers.userid = verifiedDecodedTokenData.sub;
      req.body.headers.membershiptype = verifiedDecodedTokenData.membershiptype;
      req.body.headers.usertype = verifiedDecodedTokenData.azp;

      req.headers.userid = verifiedDecodedTokenData.sub;
      req.headers.membershiptype = verifiedDecodedTokenData.membershiptype;
      req.headers.usertype = verifiedDecodedTokenData.azp;

      const aclcheck = {
        membershiptype: verifiedDecodedTokenData.membershiptype,
        api_endpoint: req.body.headers.target_uri,
        userid: verifiedDecodedTokenData.sub,
        accesstype: req.body.headers.target_method
      };

      debug('aclcheck : ', JSON.stringify(aclcheck));

      const acl_verification_flag = await MemberShipAclService.verify_proxy_acl(aclcheck);

      if(acl_verification_flag.access===true) {
        return req.body; //acl_verification_flag.access;
      } else {
        throw new HttpException({
          name:"MEMBERSHIP_ACL_CHECK",
          message:`access denied, your membership package ${aclcheck.membershiptype} do not have access to this feature. Please upgrade your membership`
        }, HttpStatus.FORBIDDEN);
      }
    } catch (err) {
      debug('introspection error :',err);
      if(err.expiredAt) {
        throw new HttpException({
          status: HttpStatus.UNAUTHORIZED,
          name: 'TOKEN_EXPIRED',
          // message: `jwt token1 has expired at ${err.expiredAt}`,
          message: 'Session expired',
          errorcode: 'Introspection_001',
        }, HttpStatus.UNAUTHORIZED);
      } else if (err.name) {
        debug('has err message');
        const mainString = err.message;
        const theWord  = "ES256";
        if (mainString.indexOf(theWord) !== -1 || err.name === 'TypeError' || err.name === 'JsonWebTokenError') {
          console.log('The word "' + theWord + '" exists in given string.');
          debug('wrong token');
              throw new HttpException({
                name : 'INVALID_TOKEN',
                message :  'Invalid token'
              },HttpStatus.BAD_REQUEST);
        } else {
          throw err;
        }
      }
      throw new HttpException({
        status: HttpStatus.UNAUTHORIZED,
        name: 'INVALID_TOKEN',
        message: `Invalid token detected.  Please contact system support`,
        errorcode: 'AdminIntrospection_001',
      }, HttpStatus.UNAUTHORIZED);
    }

  }

  getDate(): string {
    return new Date().toString();
  }
}
