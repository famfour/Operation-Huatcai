import { Controller } from '@nestjs/common';
import { Crud, CrudController } from '@nestjsx/crud';
import { ApiTags } from '@nestjs/swagger';
import Enum from '../../entities/enum';
import { EnumService } from './enum.service';

@ApiTags('Enum')
@Crud({
  model: { type: Enum },
  routes: {
    only: ['getOneBase', 'getManyBase'],
  },
})
@Controller('enum')
export class EnumController implements CrudController<Enum> {
  constructor(public service: EnumService) {}
}
