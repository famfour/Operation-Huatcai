import {
  Controller,
  Get,
  Post,
  Req,
  Body,
  UseGuards,
  Patch,
  Headers,
  HttpException,
  HttpStatus,
  Put,
  Param,
  Res
} from '@nestjs/common';
import { LoginService } from './login.service';
import { AdminFrgetResDto, AdminUpdateResDto, AppleDto, AuthUpdatePassResDto, ChkEmailStatusResDto, ChkUserTypeDto, GenerateRefrlCodeResDto, GmailDto, LoginDto,LoginResDto, ResendEmailCodeResDto, SendResetPassResDto } from './login.dto';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiExcludeEndpoint,
  ApiForbiddenResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger';
import {
  Public,
  Roles,
  Unprotected,
} from 'nest-keycloak-connect';
import { DefaultPassword, DefaultVerificationCode, LogoutDto, UserDto, TwoFactorAuthenticationCodeDto, DefaultReferalDto, RegisterResDto, VerifyCodeResDto, UserMobileResDto, DefaultReferralResDto, UserRefreshTokenResDto, RefreshDto, SendPushNotifyDto } from '../user/dto/user.dto';
import { UserService } from '../user/user.service';

import Debug from 'debug';
import { AdminService } from '../admin/admin.service';
import { SubscriptionService } from '../subscription/subscription.service';
import { TwoFactorAuthenticationService } from "../user/twoFactorAuthentication.service";
import { UserEntity } from "../../entities/users";
import { urlencoded } from "express";
import { ConfigService } from '../../config/config.service';
const debug = Debug('user-service:LoginController');

@ApiTags('Login')
@Controller('auth')
export class LoginController {
  constructor(
    private readonly loginService: LoginService,
    private readonly userService: UserService,
    private readonly adminService: AdminService,
    private readonly twoFactorAuthenticationService: TwoFactorAuthenticationService,
    private readonly subscriptionService: SubscriptionService,
    private config: ConfigService,
    
  ) { }

  @Post('/register')
  @Unprotected()
  @ApiCreatedResponse({
    type: RegisterResDto,
  })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async register(@Body() content: UserDto) {
    console.log('Entering Register controller...');
    console.log('User Register inputs: ', content);
    // console.log(content);
    return this.userService.register(content, 0, 'manual');
    // return true;
  }

  @Get('verify_email/:i')
  async verify_email(@Res() res, @Param('i') i: string) {
    // const { otpauthUrl } = await this.twoFactorAuthenticationService.generateTwoFactorAuthenticationSecret(user);
    // return this.twoFactorAuthenticationService.pipeQrCodeStream(response, otpauthUrl);
    const i_token_str = Buffer.from(i, 'base64').toString('ascii');
    debug('i_token_str : ', i_token_str);
    const i_token: any = JSON.parse(i_token_str);
    i_token.type = "Email";

    try {
      const verifyRes = await this.userService.verifyWithCode(i_token);

      const REDIRECT_TO_FE_URL = process.env.REDIRECT_TO_FE_URL;
      const REDIRECT_TO_EMAIL_VERIFY_SUCCESS = this.config.envConfig.REDIRECT_TO_EMAIL_VERIFY_SUCCESS;
      

      debug('REDIRECT_TO_FE_URL : ', REDIRECT_TO_FE_URL);
      debug('verifyRes : ', verifyRes);

      if (REDIRECT_TO_FE_URL) {
        const encodedVal = encodeURI(JSON.stringify(verifyRes));
        res.redirect(HttpStatus.MOVED_PERMANENTLY, `${REDIRECT_TO_FE_URL}?r=${encodedVal}`);
      } 
      // else {
      //   res.send(verifyRes);
      // }
      else {
        debug('REDIRECT_TO_EMAIL_VERIFY_SUCCESS : ', REDIRECT_TO_EMAIL_VERIFY_SUCCESS);
        debug('email verified successfully');
        if(verifyRes.message === 'Congratulations ! Your email address has been successfully verified.')
        {
          debug('entered if');
          return res.redirect(REDIRECT_TO_EMAIL_VERIFY_SUCCESS);
        } else {
          res.send(verifyRes);
        }
      }
    } catch (err) {
      const REDIRECT_TO_PAGE_NT_FOUND = this.config.envConfig.REDIRECT_TO_PAGE_NT_FOUND;
      debug('REDIRECT_TO_PAGE_NT_FOUND : ', REDIRECT_TO_PAGE_NT_FOUND);
      debug('expired token:', err);
      debug('status:',err.status);
      debug("email link err msg: ", err.message); // Invalid data: No property: name
      debug("email link err name: ",err.name); // PropertyRequired
      
      if (err.message || err.status) {
        debug('has err message');
        const emailMainString = err.message;
        const emailtheWord  = "Redis value is not found for";
        if (emailMainString.indexOf(emailtheWord) !== -1 || err.name === 'Error' || err.status === 401) {
          console.log('The word "' + emailtheWord + '" exists in given string.');
          debug('email link expired');
          return res.redirect(REDIRECT_TO_PAGE_NT_FOUND);
        }
      }
      throw err;
    }
  }

  @Post('turn-on')
  async turnOnTwoFactorAuthentication(
    @Body() twoFactorAuthenticationCode: TwoFactorAuthenticationCodeDto
  ) {
    const isCodeValid = await this.twoFactorAuthenticationService.isTwoFactorAuthenticationCodeValid(
      twoFactorAuthenticationCode.twoFactorAuthenticationCode, twoFactorAuthenticationCode.user
    );
    if (!isCodeValid) {
      throw new HttpException({
        message: "Wrong authentication code"
      }, HttpStatus.UNAUTHORIZED);
      // UnauthorizedException('Wrong authentication code');
    }
    // )  this.usersService.turnOnTwoFactorAuthentication(
    await this.twoFactorAuthenticationService.turnOnTwoFactorAuthentication(twoFactorAuthenticationCode.user.id);
  }

  @Post('/adminlogin')
  @Unprotected()
  @ApiCreatedResponse({
    type: LoginResDto,
  })
  async adminlogin(@Body() loginDto: LoginDto): Promise<any> {
    return await this.adminService.login(loginDto);
  }

  // send email link to reset password
  @Get('adminForgetPassword/:email')
  @ApiCreatedResponse({
    type: AdminFrgetResDto,
  })
  @ApiOkResponse({ description: 'sent email to reset password' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async sendEmailForgetPass(@Param('email') email: string) {
    return this.adminService.sendEmailForgetPass(email, 'ResetPassword');
  }

  // admin update password as per the email response of specific email id
  @Put('adminupdatePassword')
  @ApiCreatedResponse({
    type: AdminUpdateResDto,
  })
  @ApiOkResponse({ description: 'password has been updated' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async updateResetPasswordWithToken(
    @Body() defaultVal: DefaultPassword,
  ) {
    debug(' updatePassword defaultVal :', defaultVal)
    return this.adminService.updateResetPasswordWithToken(defaultVal);
  }

  @Post('/login')
  @Unprotected()
  @ApiCreatedResponse({
    type: LoginResDto,
  })
  async login(@Body() loginDto: LoginDto): Promise<any> {
    return await this.loginService.login(loginDto);
  }

  @Post('/google_verify')
  @Unprotected()
  async googleVerify(@Body() gmail: GmailDto): Promise<any> {
    // https://oauth2.googleapis.com/tokeninfo?id_token=XYZ123

    return await this.loginService.googleVerify(gmail);

    // return gmail;
  }

  @Post('/apple_verify')
  @Unprotected()
  async appleVerify(@Body() apple: AppleDto): Promise<any> {
    return await this.loginService.appleVerify(apple);
  }


  @Patch('/logout')
  @ApiBearerAuth()
  @Unprotected()
  async logout(@Body() body: LogoutDto, @Headers('Authorization') bearerToken: string) {
    debug('bearerToken : ', bearerToken);
    debug('body : ', body);

    if (!bearerToken || !body) {
      throw new HttpException({
        name: "UNAUTHORIZED_LOGOUT",
        message: "Unauthorized logout request. Either access token or refresh token is missing"
      }, HttpStatus.UNAUTHORIZED);
    }

    return this.loginService.logout(bearerToken, body.refresh_token);
  }

  // admin logout
  @Patch('/adminlogout')
  @ApiBearerAuth()
  @Unprotected()
  async adminlogout(@Body() body: LogoutDto, @Headers('Authorization') bearerToken: string) {
    debug('bearerToken : ', bearerToken);
    debug('body : ', body);

    if (!bearerToken || !body) {
      throw new HttpException({
        name: "UNAUTHORIZED_LOGOUT",
        message: "Unauthorized logout request. Either access token or refresh token is missing"
      }, HttpStatus.UNAUTHORIZED);
    }
    return this.adminService.adminlogout(bearerToken, body.refresh_token);
  }

  // test verification code which was sent to an email
  @Put('verifyWithCode')
  @ApiCreatedResponse({
    type: VerifyCodeResDto,
  })
  @ApiOkResponse({ description: 'verification completed successfully' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async verifyWithCode(@Body() defaultCode: DefaultVerificationCode) {
    return this.userService.verifyWithCode(defaultCode);
  }

  // test verification code which was sent to mobile
  @Put('verifySmsCode/:mobile')
  @ApiOkResponse({ description: 'user phone has been verified' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async verifySmsCode(
    @Param('mobile') mobile: string,
    @Body() defaultCode: DefaultVerificationCode,
  ) {
    return this.userService.verifySmsCode(mobile, defaultCode);
  }

  // resend verification code to specific email id
  @Put('resendEmailCode/:email')
  @ApiCreatedResponse({
    type: ResendEmailCodeResDto,
  })
  @ApiOkResponse({ description: 'resent verification code to an email' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async resendEmailWithCode(@Param('email') email: string) {
    const regexEmail = /^\w+([+\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (email.match(regexEmail)) {
      return this.userService.resendEmailWithCode(email, 'VerifyEmail');
    } else {
      throw new HttpException({ message: "Invalid email id", name: "RESET_INVALID_EMAIL" }, 427);
    }

    //
  }

  // resend verification code to specific mobile number
  @Put('resendSmsCode/:mobile')
  @ApiCreatedResponse({
    type: UserMobileResDto,
  })
  @ApiOkResponse({ description: 'resent verification code to mobile' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async resendSmsWithCode(@Param('mobile') mobile: string) {
    return this.userService.resendSmsWithCode(mobile);
  }

  // send email link to reset password
  @Get('sendResetPassword/:email')
  @ApiCreatedResponse({
    type: SendResetPassResDto,
  })
  @ApiOkResponse({ description: 'sent email to reset password' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async resetPassword(@Param('email') email: string) {
    return this.userService.resendEmailWithCode(email, 'ResetPassword');
  }

  // update password as per the email response of specific email id
  @Put('updatePassword')
  @ApiCreatedResponse({
    type: AuthUpdatePassResDto,
  })
  @ApiOkResponse({ description: 'password has been updated' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async updatePassword(
    @Body() defaultVal: DefaultPassword,
  ) {
    debug(' updatePassword defaultVal :', defaultVal)
    return this.userService.updatePasswordWithToken(defaultVal);
  }

  // register with google
  @Get('/gmailRegister')
  @ApiOperation({
    summary: 'Open "http://localhost:3001/user/gmailRegister" API in new tab',
  })
  @UseGuards(AuthGuard('googleregister'))
  async googleAuth1(@Req() req) { }

  @Get('google/callback')
  @ApiExcludeEndpoint()
  @UseGuards(AuthGuard('googleregister'))
  googleAuthRedirect1(@Req() req) {
    return this.userService.googleLogin(req);
  }

  // chk email status
  @Get('checkEmailStatus/:email')
  @ApiCreatedResponse({
    type: ChkEmailStatusResDto,
  })
  @ApiOkResponse({ description: 'Email status' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async chkEmailStatus(@Param('email') email: string) {
    const regexEmail = /^\w+([+\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (email.match(regexEmail)) {
      return this.userService.chkEmailStatus(email);
    } else {
      throw new HttpException({ message: "Invalid email id", name: "RESET_INVALID_EMAIL" }, 427);
    }
  }

  // chk user type
  @Get('checkUserType/:email')
  @ApiCreatedResponse({
    type: ChkUserTypeDto,
  })
  @ApiOkResponse({ description: 'User type' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async checkUserType(@Param('email') email: string) {
    const regexEmail = /^\w+([+\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (email.match(regexEmail)) {
      return this.userService.checkUserType(email);
    } else {
      throw new HttpException({ message: "Invalid email id", name: "RESET_INVALID_EMAIL" }, 427);
    }
  }

  // update referal code based on user email
  @Put('updateReferalCode/:email')
  @ApiCreatedResponse({
    type: DefaultReferralResDto,
  })
  @ApiOkResponse({ description: 'Email status' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async updateReferalCode(@Param('email') email: string,
  @Body() defaultVal: DefaultReferalDto,
  ) {
    const regexEmail = /^\w+([+\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (email.match(regexEmail)) {
      return this.userService.updateReferalCode(email, defaultVal);
    } else {
      throw new HttpException({ message: "Invalid email id", name: "RESET_INVALID_EMAIL" }, 427);
    }
  }

  /**
  * Get transaction response from stripe using webhook
  * @param content 
  * @returns 
  */
  @Post('webhook')
  @ApiCreatedResponse({ description: 'Webhook' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async webhook(@Body() content: any) {

    debug("Subscription webhook response ", content);
    console.log("Subscription webhook response ");
    //console.log(content);
    return this.subscriptionService.webhook(content);
  }

  @Get('/autoGenerate-referalCode')
  @ApiCreatedResponse({
    type: GenerateRefrlCodeResDto,
  })
  @ApiCreatedResponse({ description: 'Auto generate referal code' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async autoGenPass() {
    return this.adminService.autoGenCode();
  }

  // assign cobroke id
   // resend verification code to specific mobile number
   @Put('assign_coBroke_id')
  //  @ApiCreatedResponse({
  //    type: UserMobileResDto,
  //  })
   @ApiOkResponse({ description: 'assign co broke id' })
   @ApiForbiddenResponse({ description: 'forbidden' })
   async assign_coBrokeId() {
     return this.userService.assign_coBrokeId();
   }

   // user refrsh token
  @Post('getRefreshToken')
  @ApiCreatedResponse({
    type: UserRefreshTokenResDto,
  })
  @ApiOkResponse({ description: 'Get refresh token' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async getRefreshToken( @Body() defaultVal: RefreshDto) {
    return this.userService.getRefreshToken(defaultVal);
  }
}