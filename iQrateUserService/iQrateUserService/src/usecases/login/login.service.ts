import { HttpException, HttpService, HttpStatus, Inject, Injectable, Res, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserEntity } from '../../entities/users';
import { LoginSession } from '../../entities/login_session';
import { ConfigService } from '../../config/config.service';
import { UserService } from '../user/user.service';
import { JwtPayload } from './jwt.payload';
import DeviceDetector = require('device-detector-js');
import { AppleDto, GmailDto, LoginDto } from './login.dto';
import { Auth } from '../../middlewares/auth'
const deviceDetector = new DeviceDetector();
const userAgent =
  'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Safari/537.36';
const device = deviceDetector.parse(userAgent);
const deviceinfo = { "OS_name": "${device.os.name}", "OS_version": "${device.os.version}", "client_type": "${device.client.type}", "client_name": "${device.client.name}", "client_version": "${device.client.version}" };

const { OAuth2Client } = require('google-auth-library');

import Debug from 'debug';
import { TwoFactorAuthenticationService } from "../user/twoFactorAuthentication.service";
import { randomStringGenerator } from "@nestjs/common/utils/random-string-generator.util";
import * as jwt from 'jsonwebtoken';

const debug = Debug('user-service:LoginService');
const jwksClient = require('jwks-rsa');


const client = jwksClient({
  jwksUri: 'https://appleid.apple.com/auth/keys'
})


@Injectable()
export class LoginService {
  userRepo: any;

  constructor(
    private readonly usersService: UserService,
    private readonly jwtService: JwtService,
    private config: ConfigService,
    private readonly http: HttpService,
    private readonly twoFactorAuthenticationService: TwoFactorAuthenticationService
    // @Inject(KEYCLOAK_INSTANCE) private keycloak: Keycloak,
  ) {

  }

  public async googleVerify(gmail: GmailDto): Promise<any> {
    try {
      const GOOGLE_TOKENINFO_URL = this.config.envConfig.GOOGLE_TOKENINFO_URL || 'https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=';
      const G_AUTO_PASS = this.config.envConfig.G_AUTO_PASS;
      const gResponse = await this.http.get(`${GOOGLE_TOKENINFO_URL}${gmail.access_token}`).toPromise();
      console.log('userdata : ', gResponse.data);

      //const gResponse1 = await this.http.get(`https://content-people.googleapis.com/v1/people/me?sources=READ_SOURCE_TYPE_PROFILE&personFields=birthdays&key=${gmail.access_token}`).toPromise();

      // console.log('birthday : ', gResponse1.data);

      if (gResponse.data.email && gResponse.data.expires_in) {
        const jsonPayload = JSON.parse(JSON.stringify(this.jwtService.decode(gmail.idToken)));
        debug('jsonPayload : ', jsonPayload);

        if (gmail.type === 'Login') {
          const user: UserEntity = await UserEntity.findOne({ email: jsonPayload.email });

          if (!user) {
            throw new HttpException({
              message: "This user is not registered.  Please signup with app",
              name: "User_Not_Registered"
            }, HttpStatus.UNAUTHORIZED);
          }
          if (user.password === null) {
            user.password = G_AUTO_PASS;
          }
          if (user && user.email) {
            return await this.login({ password: user.password, email: user.email, twoFactorAuthenticationCode: gmail.twoFactorAuthenticationCode });
          } else {
            throw new HttpException({
              message: "This user is not registered.  Please signup with app",
              name: "User_Not_Registered"
            }, HttpStatus.UNAUTHORIZED);
          }
        } else if (gmail.type === 'Register') {

          const userdata = {
            "full_name": jsonPayload.name,
            "email": jsonPayload.email,
            "country_code": "91",
            "country_code_label": "IN",
            "mobile": "1234123456",
            "is_email_verified": jsonPayload.email_verified,
            "password": randomStringGenerator().password,
            "dob": "01-01-1900",
            "agreed_term": 1
          };
          return await this.usersService.register(userdata, 1, 'gmail');
        }
      }
    } catch (err) {
      debug('googleVerify err : ', err) //.response.data);
      if (err && err.response && err.response.data) {
        throw new HttpException(err.response.data, HttpStatus.UNAUTHORIZED);
      }
      throw err;
    }
  }


  async getAppleSignKey(kid) {
    return new Promise((resolve) => {
      client.getSigningKey(kid, (err, key) => {
        if(err){
          console.log('getAppleSignKey err')
          console.error(err)
          resolve(null)
          return
        }
        const signingKey = key.publicKey;
        resolve(signingKey);
      })
    })
  }

  async verifyJWT(json, publicKey) {
    return new Promise((resolve) => {
      jwt.verify(json, publicKey, (err, payload) => {
        if(err) {
          if(err.expiredAt) {
            throw new HttpException({
              name: 'TOKEN_EXPIRED',
              message: `Token expired`,
            }, HttpStatus.UNAUTHORIZED);
          }
          console.error(err)
          return resolve(null)
        }
        resolve(payload)
      })
    })
  }

  // apple signup/signin
  public async appleVerify(apple: AppleDto): Promise<any> {
    try {
      const G_AUTO_PASS = this.config.envConfig.G_AUTO_PASS;
      if(apple.provider !== 'apple'){
        throw new HttpException({
          name : 'INVALID_PROVIDER',
          message :  'Provider should be apple'
        },HttpStatus.BAD_REQUEST);
      }
      if(apple.provider === 'apple'){
        const decodeAppleToken = jwt.decode(apple.identityToken, { complete: true})
        debug('decodeAppleToken : ', decodeAppleToken);

        const kid = decodeAppleToken.header.kid
        const appleKeys = await this.getAppleSignKey(kid)

        if(!appleKeys){
          console.error('something went wrong')
          // newly added err throw option
          throw new HttpException({ 
            name : 'APPLE_PROCESS_FAILED',
            message :  'Provide valid token'
          },HttpStatus.BAD_REQUEST);
          return
        }
        debug('appleKeys:', appleKeys);
        const payload = await this.verifyJWT(apple.identityToken, appleKeys)
        debug('payload:', payload);
        if(!payload) {
          console.error("something went wrong in apple signin");
          throw new HttpException({
            name : 'APPLE_PROCESS_FAILED',
            message :  'Provide valid token'
          },HttpStatus.BAD_REQUEST);
        }
        debug('sign in with apple success', payload)
        if(payload['aud'] === 'com.iqrate' || payload['aud'] === 'com.digitalprizm.iqrate' || payload['aud'] === 'com.iqrate.dp.io') { // if all are match then its authorized apple user 
          debug('successfully  fetched info frm apple', payload)     
        } else {
          throw new HttpException({
            name : 'APPLE_PROCESS_FAILED',
            message :  'Provide valid details'
          },HttpStatus.BAD_REQUEST);
        }

       // login OR register
        if (apple.type === 'Login') {  
          const user: UserEntity = await UserEntity.findOne({ email: decodeAppleToken.payload['email'] });

          if (!user) {
            throw new HttpException({
              message: "This user is not registered.  Please signup with app",
              name: "User_Not_Registered"
            }, HttpStatus.UNAUTHORIZED);
          }
          if(user){
            if(user.register_type !== 'apple') {
              throw new HttpException({
                message: "This user is not registered through apple signup",
                name: "Not_Apple_User"
              }, HttpStatus.UNAUTHORIZED);
            }
          }
          if (user.password === null) {
            user.password = G_AUTO_PASS;
          }
          if (user && user.email) {
            return await this.login({ password: user.password, email: user.email, twoFactorAuthenticationCode: apple.twoFactorAuthenticationCode });
          } else {
            throw new HttpException({
              message: "This user is not registered.  Please signup with app",
              name: "User_Not_Registered"
            }, HttpStatus.UNAUTHORIZED);
          }
        } else if (apple.type === 'Register') {
          if(payload['sub'] === apple.user) { // if its match then its authorized apple user 
            debug('successfully  fetched info frm apple', payload)     
          } else {
            throw new HttpException({
              name : 'APPLE_PROCESS_FAILED',
              message :  'Provide valid user'
            },HttpStatus.BAD_REQUEST);
          }
          debug('apple.givenName:', apple.givenName);
          if(!apple.givenName) {
            throw new HttpException({
              message: "Given name should not be empty",
              name: "GIVEN_NAME_EMPTY"
            }, HttpStatus.UNAUTHORIZED);
          }
          const userdata = {
            "full_name": apple.givenName,
            "email": decodeAppleToken.payload['email'],
            "country_code": "91",
            "country_code_label": "IN",
            "mobile": "1234123456",
            "is_email_verified": decodeAppleToken.payload['email_verified'],
            "password": randomStringGenerator().password,
            "dob": "01-01-1900",
            "agreed_term": 1
          };
          return await this.usersService.register(userdata, 1, 'apple');
        }
      }
    } catch (err) {
      debug('appleVerify err : ', err) //.response.data);
      if (err && err.response && err.response.data) {
        throw new HttpException(err.response.data, HttpStatus.UNAUTHORIZED);
      }
      throw err;
    }
  }


  public async login(
    loginDto: LoginDto
  ): Promise<any> {

    const KEYCLOAK_BASE_URL = this.config.envConfig.KEYCLOAK_BASE_URL;
    const KEYCLOAK_REALM_AGENT = this.config.envConfig.KEYCLOAK_REALM_AGENT;

    debug("grant", this.config.envConfig.KEYCLOAK_GRANT);
    const keyCloakLoginRequestPayload = {
      client_id: this.config.envConfig.KEYCLOCK_CLIENT_NAME_AGENT,
      grant_type: this.config.envConfig.KEYCLOCK_GRANT_TYPE,
      username: loginDto.email,
      password: loginDto.password,
    };
    debug("keyCloakLoginRequestPayload : ", keyCloakLoginRequestPayload);

    const keyCloakLoginRequestFormBody = Object.keys(keyCloakLoginRequestPayload).map((key) => {
      return encodeURIComponent(key) + '=' + encodeURIComponent(keyCloakLoginRequestPayload[key]);
    }).join('&');

    debug("KEYCLOAK_BASE_URL :", KEYCLOAK_BASE_URL);

    //Not sure the purpose of this calling. Please explain the rational as per the logic so we can enable back and apply in the response accordingly
    // const device = await this.http.get('http://ip-api.com/json/').toPromise();
    // debug('device : ', device.data);

    const fullUrl = `${KEYCLOAK_BASE_URL}/realms/${KEYCLOAK_REALM_AGENT}/protocol/openid-connect/token`;
    debug("keyCloakLoginRequestFormBody : ", keyCloakLoginRequestFormBody);
    debug("fullUrl : ", fullUrl);
    try {
      const tokenRes = await this.http.post(
        fullUrl, keyCloakLoginRequestFormBody, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      }).toPromise();

      const userFromToken = await (new Auth()).verifyAndDecode(tokenRes.data.access_token);

      debug('userFromToken : ', userFromToken);
      const user = await UserEntity.findOne(userFromToken.sub);

      debug('Login user  : ', user);

      if (!user) {
        throw new HttpException({ message: 'User account is not setup properly.  Please contact your system support team.', name: 'RECORD_NOT_EXISTS_IN_USER' }, HttpStatus.BAD_REQUEST);
      }

      if (user.isTwoFactorAuthenticationEnabled) {

        if (!loginDto.twoFactorAuthenticationCode) {
          throw new HttpException({ message: "Please enter valid otp code.", name: "OTP_ERROR" }, HttpStatus.UNAUTHORIZED);
        }

        const isOtpVerified = await this.twoFactorAuthenticationService.isTwoFactorAuthenticationCodeValid(loginDto.twoFactorAuthenticationCode.toString(), user);

        if (isOtpVerified === true) {
          // // update refresh token in db
          // await UserEntity.update(
          //   { id: user.id },
          //   { refresh_token: tokenRes.data.refresh_token, updated_at: new Date(), updated_user_id: user.id },
          // );
          // return tokenRes.data;
        } else {
          throw new HttpException({ message: "OTP code is invalid. Please enter valid otp code.", name: "OTP_ERROR" }, HttpStatus.UNAUTHORIZED);
        }
      }

      // START based on membership type allow only minimal device access for a user
      // get keycloak admin token
      const keycloakToken = (await this.usersService.getKeycloak()).token;
      // END

      const headers = {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${keycloakToken}`,
      };
    
      // get user membership type(basic, premium, premium+) frm keycloak based on user id
      const getuser = await this.http
        .get(
          // 'http://54.255.196.41:8080/auth/admin/realms/master/users',
          `${KEYCLOAK_BASE_URL}/admin/realms/${KEYCLOAK_REALM_AGENT}/users/${userFromToken.sub}`,
          {
            headers,
          },
        )
        .toPromise();
        debug('getuser.data:', getuser.data.attributes.membershiptype[0]);

      // get no. of device can access for this membershiptype from login_session tbl
      const userMembership = await LoginSession.findOne({user_membershiptype: getuser.data.attributes.membershiptype[0]});
      const deviceAllows = userMembership.allow_device;

      // get a user all sessions(all login lists) based on user id
      const getAllSessions = await this.http
        .get(
          // 'http://54.255.196.41:8080/auth/admin/realms/master/users',
          `${KEYCLOAK_BASE_URL}/admin/realms/${KEYCLOAK_REALM_AGENT}/users/${userFromToken.sub}/sessions`,
          {
            headers,
          },
        )
        .toPromise();
        debug('getAllSessions:', getAllSessions.data);
        const sessionArr = getAllSessions.data;

        // sort the session list in descending order based on  starttime. so that last login session comes at first. so that keep top list and delete rest of things
        // sessionArr.sort( function ( a, b ) { return b.lastAccess - a.lastAccess; });
        sessionArr.sort( function ( a, b ) { return b.start - a.start; }); //i.e it list out last or latest login session at top based on start time
        debug('sessionArr:', sessionArr);
        for(var i = deviceAllows; i < sessionArr.length; i++) // deviceAllows =1 i.e keep latest one and logout all sessions. deviceAllows = 2 keep latest 2 and logout all other session
        {
          debug('entered for', i);
          debug('sessionArr.id:', sessionArr[i].id);
          
          // delete session based on session id. for basic&premium skip latest 1 AND premium+ skip latest 2
          const delSession = await this.http
          .delete(
            // http://13.229.205.94:8080/auth/admin/realms/Agents/sessions/497a2ca1-e319-4f1c-b3ef-e420514d04a7
            `${KEYCLOAK_BASE_URL}/admin/realms/${KEYCLOAK_REALM_AGENT}/sessions/${sessionArr[i].id}`,
            {
              headers,
            },
          )
          .toPromise();    
        }
        // END

        // update refresh token in db
        let dbRefreshTokenArr = []
        dbRefreshTokenArr = user.refresh_token;
        debug('tokenLen:', dbRefreshTokenArr); 
        if(user.membership_type === 'premium+'){
          if(dbRefreshTokenArr.length<2)
          {
            debug('less than 2')
            debug('premium+ if beforepush:', dbRefreshTokenArr);
            dbRefreshTokenArr.unshift(tokenRes.data.refresh_token); //add new token at the beginnig of the array
            debug('premium+ if afterpush:', dbRefreshTokenArr);
          } else {
            debug('premium+ else beforepush:', dbRefreshTokenArr);
            const removedElement = dbRefreshTokenArr.pop(); // remove last element of the arrray
            dbRefreshTokenArr.unshift(tokenRes.data.refresh_token)  //add new token at the beginnig of the array
            debug('premium+ else afterpush:', dbRefreshTokenArr);
          }
        } else {
          debug('basic if beforepush:', dbRefreshTokenArr);
          // const removedElement = dbRefreshTokenArr.pop(); // remove last element of the arrray
          dbRefreshTokenArr.length = 0;// empty arry
          dbRefreshTokenArr.unshift(tokenRes.data.refresh_token)  //add new token at the beginnig of the array
          debug('basic if afterepush:', dbRefreshTokenArr);
        }
        debug('db token array:', dbRefreshTokenArr);
        await UserEntity.update(
          { id: user.id },
          { refresh_token: dbRefreshTokenArr, updated_at: new Date(), updated_user_id: user.id },
        );
        return tokenRes.data;
    } catch (err) {
      if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
        // throw new HttpException(err.response.data, err.response.status);
        if(err.response.data.error_description === 'Account disabled')
        {
          const userRec = await UserEntity.findOne({email: loginDto.email});
          if(userRec.is_email_verified === 0 && userRec.register_type === 'manual'){
            throw new HttpException({
              message :  'Please verify your email to continue logging in',
            },HttpStatus.BAD_REQUEST);
          } else if(userRec.deleted === 1){
            throw new HttpException({
              message :  'Login Failed : Your account is deleted',
            },HttpStatus.BAD_REQUEST);
          } else {
            throw new HttpException({
              message :  'Login Failed : Your account has been locked',
            },HttpStatus.BAD_REQUEST);
          }
        } else {
          throw new HttpException({
            message :  'Login Failed : Your email or password is incorrect',
          },HttpStatus.BAD_REQUEST);
        }
      } else {
        console.error('login err : ', err);
        throw err;
      }
    }
  }

  public async logout(bearerToken: string, refresh_token: string) {
    const KEYCLOAK_BASE_URL = this.config.envConfig.KEYCLOAK_BASE_URL;
    const KEYCLOAK_REALM_AGENT = this.config.envConfig.KEYCLOAK_REALM_AGENT;
    debug("logout KEYCLOAK_BASE_URL :", KEYCLOAK_BASE_URL);
    bearerToken
    const fullUrl = `${KEYCLOAK_BASE_URL}/realms/${KEYCLOAK_REALM_AGENT}/protocol/openid-connect/logout`;
    const logoutRequestBody = `client_id=${this.config.envConfig.KEYCLOCK_CLIENT_NAME_AGENT}&refresh_token=${refresh_token}`;
    try {
      const tokenRes = await this.http.post(
        fullUrl, logoutRequestBody, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': bearerToken
        }
      }).toPromise();
      debug('logout:', tokenRes);
      return tokenRes.data;
    } catch (err) {
      if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('login err : ', err);
        throw err;
      }
    }

  }

  public async validateUserByJwt(payload: JwtPayload) {
    // This will be used when the user has already logged in and has a JWT
    const user = await this.usersService.findByEmail(payload.email);

    if (!user) {
      throw new UnauthorizedException();
    }
    return this.createJwtPayload(user);
  }

  protected createJwtPayload(user: UserEntity) {
    /*
    const data: JwtPayload = {
      email: user.email,
    }; */

    const data = {
      id: user.id,
      email: user.email,
      full_name: user.full_name,
      mobile: user.mobile,
      user_type: 'agent' //user.user_type
    };

    const jwt = this.jwtService.sign(data);

    //return data;

    return {
      expiresIn: 3600,
      token: jwt,
    };
  }
}
