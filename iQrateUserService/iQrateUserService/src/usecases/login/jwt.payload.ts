export interface JwtPayload {
    readonly email: string;
    readonly id: string;
    readonly full_name: string;
    readonly mobile: string;
    readonly user_type: string;


}