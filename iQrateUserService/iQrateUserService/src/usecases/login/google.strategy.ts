import { PassportStrategy } from '@nestjs/passport';
import { Strategy, VerifyCallback } from 'passport-google-oauth20';
import { config } from 'dotenv';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '../../config/config.service';

config();

@Injectable()
export class GoogleStrategy extends PassportStrategy(Strategy, 'googlelogin') {
  constructor(private config: ConfigService) {
    super({
      clientID: config.envConfig.GOOGLE_LOGIN_CLIENTID,
      clientSecret: config.envConfig.GOOGLE_LOGIN_SECRETID,
      callbackURL: config.envConfig.GOOGLE_REDIRECT_URL,
      scope: ['email', 'profile'],
    });
  }

  async validate(
    accessToken: string,
    refreshToken: string,
    profile: any,
    done: VerifyCallback,
  ): Promise<any> {
    const { name, emails, photos } = profile;
    const user = {
      email: emails[0].value,
      firstName: name.givenName,
      lastName: name.familyName,
      picture: photos[0].value,
      accessToken,
    };
    done(null, user);
  }
}
