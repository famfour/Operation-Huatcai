import { ApiProperty } from '@nestjs/swagger';
import {IsString, IsEmail, IsNotEmpty, IsEnum} from 'class-validator';

export class LoginDto {
  @ApiProperty({
    type: String,
    description: 'Email',
    default: 'roy@gmail.com',
  })
  @IsString()
  @IsEmail({}, { message: 'Please enter a valid email id' })
  email: string;

  @ApiProperty({
    type: String,
    description: 'password',
    default: 'pass',
  })
  @IsString()
  password: string;

  twoFactorAuthenticationCode?:number;
}

export class LoginResDto {
  @ApiProperty({
    type: String,
    description: 'access_token',
    default: 'string',
  })
  access_token: string;

  @ApiProperty({
    type: String,
    description: 'expires_in',
    default: 'number',
  })
  expires_in: string;

  @ApiProperty({
    type: String,
    description: 'refresh_expires_in',
    default: 'number'
  })
  refresh_expires_in: string;

  @ApiProperty({
    type: String,
    description: 'refresh_token',
    default: 'string'
  })
  refresh_token: string;

  @ApiProperty({
    type: String,
    description: 'token_type',
    default: 'string'
  })
  token_type: string;

  @ApiProperty({
    type: String,
    description: 'not-before-policy',
    default: 'number'
  })
  "not-before-policy": string;

  @ApiProperty({
    type: String,
    description: 'session_state',
    default: 'string'
  })
  session_state: string;

  @ApiProperty({
    type: String,
    description: 'scope',
    default: 'string'
  })
  scope: string;

}

export enum AccessTypeEnum {
  REGISTER = 'Register',
  LOGIN = 'Login'
}



export class GmailDto {
  @ApiProperty({
    type: String,
    description: 'ID Token',
  })
  @IsString()
  @IsNotEmpty()
  idToken: string;

  @ApiProperty({
    type: String,
    description: 'Access Token',
  })
  @IsString()
  @IsNotEmpty()
  access_token: string;

  @ApiProperty({
    type: String,
    description: 'TYPE',
    default: 'Login',
  })
  @IsString()
  @IsEnum(AccessTypeEnum)
  @IsNotEmpty()
  type: string;

  twoFactorAuthenticationCode?:number;
}

export class AppleDto {
  @ApiProperty({
    type: String,
    description: 'Provider',
  })
  @IsNotEmpty()
  provider: string;

  @ApiProperty({
    type: String,
    description: 'Identity Token',
  })
  @IsString()
  @IsNotEmpty()
  identityToken: string;

  @ApiProperty({
    type: String,
    description: 'Authorization Code',
  })
  @IsString()
  @IsNotEmpty()
  authorizationCode: string;

  @ApiProperty({
    type: String,
    description: 'User',
    default: '',
  })
  user?: string;

  @ApiProperty({
    type: String,
    description: 'TYPE',
    default: 'Login',
  })
  @IsString()
  @IsEnum(AccessTypeEnum)
  @IsNotEmpty()
  type: string;

  givenName?:string;
  twoFactorAuthenticationCode?:number;
}

export class AdminFrgetResDto {
  @ApiProperty({
    type: String,
    description: 'message',
    default: 'ResetPassword admin Verification code has been sent to your email'
  })
  message: string;
}
export class AdminUpdateResDto {
  @ApiProperty({
    type: String,
    description: 'message',
    default: 'pasword updated successfully'
  })
  message: string;
}

export class ResendEmailCodeResDto {
  @ApiProperty({
    type: String,
    description: 'message',
    default: 'Verify email link has been sent to your email'
  })
  message: string;
}

export class SendResetPassResDto {
  @ApiProperty({
    type: String,
    description: 'message',
    default: 'We have sent a password recover instructions to your email address.'
  })
  message: string;
}

export class AuthUpdatePassResDto {
  @ApiProperty({
    type: String,
    description: 'message',
    default: 'Congratulations ! Password has been updated successfully.'
  })
  message: string;
}

export class ChkEmailStatusResDto {
  @ApiProperty({
    type: Number,
    description: 'is_email_verified',
    default: 1
  })
  is_email_verified: number
}

export class ChkUserTypeDto {
  @ApiProperty({
    type: String,
    description: 'membership_type',
    default: 'basic'
  })
  membership_type: string
}

export class GenerateRefrlCodeResDto {
  @ApiProperty({
    type: String,
    description: 'code',
    default: 'S3WDEF'
  })
  code: string
}