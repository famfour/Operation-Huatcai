import { HttpModule, Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule } from '../../config/config.module';
import { LoginService } from './login.service';
import { LoginController } from './login.controller';
import { ConfigService } from '../../config/config.service';
import { UserService } from '../user/user.service';
import { JwtStrategy } from './jwt.strategy';
import { GoogleStrategy } from './google.strategy';
import { AdminService } from '../admin/admin.service';
import { SubscriptionService } from '../subscription/subscription.service';
import { TwoFactorAuthenticationService } from "../user/twoFactorAuthentication.service";
import { RedisController } from "../redis/redis.controller";

@Module({
  imports: [
    // keycloakModule,
    ConfigModule,
    HttpModule,
    // TypeOrmModule.forFeature([UserEntity]),
    PassportModule.register({ defaultStrategy: 'jwt', session: false }),
    JwtModule.register({
      // eslint-disable-next-line global-require
      // secret: require('dotenv').parse(fs.readFileSync('dev.env'))
      //   .JWT_SECRET_KEY,

      secret: "secretOrKey",

      //secret: async (config: ConfigService) => config.getJWTCredentials().JWT_SECRET_KEY,

      //secret: "secretOrKey",
      signOptions: {
        expiresIn: 3600,
        //  algorithm: 'RS256'
      },
    }),
  ],
  providers: [LoginService, ConfigService, UserService, AdminService, SubscriptionService, JwtStrategy, GoogleStrategy, ConfigService, TwoFactorAuthenticationService
  ],
  controllers: [LoginController, RedisController],
  exports: [ConfigService],

})
export class LoginModule {
  public config: ConfigService;
}
