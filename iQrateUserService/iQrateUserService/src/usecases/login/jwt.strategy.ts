import { ExtractJwt, Strategy } from 'passport-jwt';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import * as fs from 'fs';
import { ConfigService } from '../../config/config.service';
import { LoginService } from './login.service';
import { JwtPayload } from './jwt.payload';

// import { ConfigModule, ConfigService } from '@nestjs/config';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  // public secret = JwtStrategy.prototype.config.envConfig.JWT_SECRET_KEY;
  // public secret = ConfigService.prototype.getJWTCredentials().JWT_SECRET_KEY;
  // public secret = process.env.JWT_SECRET_KEY;

  constructor(
    private readonly loginService: LoginService,
    private config: ConfigService,

  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: config.envConfig.JWT_SECRET_KEY,
      publickey: config.envConfig.JWT_PUBLIC_KEY,
      privatekey: config.envConfig.JWT_PRIVATE_KEY,
      // eslint-disable-next-line global-require
      //secretOrKey: require('dotenv').parse(fs.readFileSync('dev.env')).JWT_SECRET_KEY,
      // secretOrKey:fs.readFileSync(config.get('process.env.JWT_SECRET_KEY'))
    });

  }


  async validate(payload: JwtPayload) {
    // eslint-disable-next-line @typescript-eslint/no-unused-expressions
    //  this.config.envConfig.SECRET_KEY_JWT;
    console.log("payload");
    console.log(payload);
    const user = await this.loginService.validateUserByJwt(payload);
    console.log("user validate");
    console.log(user);
    if (!user) {
      throw new UnauthorizedException();
    }

    return user;
  }
}