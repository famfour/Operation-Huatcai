import { HttpException, Injectable, Req, HttpStatus, ParseArrayPipe } from '@nestjs/common';
import Debug from 'debug';
import * as bcrypt from 'bcrypt';
import { getConnection } from 'typeorm';
import { HttpService } from '@nestjs/axios';
import { ConfigModule } from 'src/config/config.module';
import { ConfigService } from '../../config/config.service';
import { UserEntity } from '../../entities/users';
import { SubscriptionEntity } from '../../entities/subscriptions';
import { CardEntity } from '../../entities/card';
import { UserService } from '../user/user.service';
import {
  CreateCustomerDto, UpdateCustomerDto,
  CreatePaymentMethod, DetachPaymentMethod,
  CreateSubscription,
  SetPaymentMethod, UpdatePaymentMethod, SetUpgradeDowngrade, SubscriptionPreview

} from './subscription.dto';

import RedisClient from '../utils/redis';
import { BroadcasterResult } from 'typeorm/subscriber/BroadcasterResult';

const debug = Debug('Subscription-service:UserService');

@Injectable()
export class SubscriptionService {
  constructor(
    private config: ConfigService,
    private readonly http: HttpService,
    private readonly usersService: UserService,
  ) { }

  /**
   * Creating customer in stripe
   * @param CreateCustomerDto
   * @returns
   */
  public async createCustomer(param: CreateCustomerDto) {
    const name = param.name;
    const email = param.email;
    const user_id = param.user_id;

    try {
      const customer = await this.http
        .post(`${this.config.envConfig.PAYMENT_SERVICE_URL}/customer`, {
          email: email,
          name: name,
          user_id: user_id
        }).toPromise();

      debug('customer: ', customer);
      // Updating stripe customer in local database

      const userRepo = getConnection().getRepository(UserEntity);
      const user = new UserEntity();
      user.stripe_customer_id = customer.data.stripe_id;

      await userRepo.update({ id: user_id }, user);

      const result = {
        stripe_id: customer.data.stripe_id,
      };

      return result;
    } catch (err) {
      debug('err:', err);
      if (
        err &&
        err.response &&
        err.response.status >= 400 &&
        err.response.status < 500
      ) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Create Customer err : ', err);
        throw err;
      }
    }
  }

  /**
   * Get the customer
   * @param customerId 
   * @returns 
   */
  public async getCustomer(customer_id: string) {
    try {
      debug('Customer ID:', customer_id);

      const res = await this.http
        .get(
          `${this.config.envConfig.PAYMENT_SERVICE_URL}/customer/${customer_id}`)
        .toPromise();

      // debug('customer: ', res);
      console.log("Output...");
      console.log(res);
      return res.data;

    } catch (err) {
      debug('err:', err);
      if (
        err &&
        err.response &&
        err.response.status >= 400 &&
        err.response.status < 500
      ) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }
  }

  /**
   * Update Customer
   * @param customer_id 
   * @param UpdateCustomerDto 
   * @returns 
   */
  public async updateCustomer(customer_id: string, param: UpdateCustomerDto) {
    console.log("entering update customer in stripe");
    console.log(param);
    const phone = param.phone;
    const name = param.name;
    const email = param.email;
    try {

      const res = await this.http
        .put(
          `${this.config.envConfig.PAYMENT_SERVICE_URL}/customer/${customer_id}`,
          {
            phone: phone,
            email: email,
            name: name
          },
        )
        .toPromise();

      const userRepo = getConnection().getRepository(UserEntity);
      const user = new UserEntity();
      user.phone = phone;
      user.full_name = name;
      user.email = email;
      user.updated_at = new Date();

      await userRepo.update({ stripe_customer_id: customer_id }, user);

      return res.data;

    } catch (err) {
      debug('err:', err);
      if (
        err &&
        err.response &&
        err.response.status >= 400 &&
        err.response.status < 500
      ) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }

  }

  /**
   * List All Products
   * @returns 
   */
  public async listProducts() {
    try {
      //  const resProducts = await this.stripe.products.list();
      //  const stripe = this.stripe;

      const result = await this.http
        .get(`${this.config.envConfig.PAYMENT_SERVICE_URL}/products`)
        .toPromise();
      console.log(result);
      return result.data;
    } catch (err) {
      debug('err:', err);
      if (
        err &&
        err.response &&
        err.response.status >= 400 &&
        err.response.status < 500
      ) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }
  }

  /**
   * List All subscriptions
   * @returns 
   */
  public async listSubscriptions(customer_id) {
    try {
      const res = await this.http
        .get(
          `${this.config.envConfig.PAYMENT_SERVICE_URL}/subscriptions/${customer_id}`,
        )
        .toPromise();

      return res.data;
    } catch (err) {
      debug('err:', err);
      if (
        err &&
        err.response &&
        err.response.status >= 400 &&
        err.response.status < 500
      ) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Subscriptions err : ', err);
        throw err;
      }
    }
  }

  /**
  * Create payment method
  * @param createPaymentMethod
  * @returns
  */
  public async createPaymentMethod(param: CreatePaymentMethod) {
    try {
      const type = param.type;
      const customer_id = param.customer_id;
      const payment_method_token = param.payment_method_token;
      const res = await this.http
        .post(
          `${this.config.envConfig.PAYMENT_SERVICE_URL}/payment-method`,
          {
            type: type,
            customer_id: customer_id,
            payment_method_token: payment_method_token,
          },
        )
        .toPromise();

      const userRepo = getConnection().getRepository(UserEntity);
      const user = new UserEntity();
      user.stripe_payment_method_id = res.data.invoice_settings.default_payment_method;
      await userRepo.update({ stripe_customer_id: customer_id }, user);
      return res.data;

    } catch (err) {
      debug('err:', err);
      if (
        err &&
        err.response &&
        err.response.status >= 400 &&
        err.response.status < 500
      ) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }
  }

  /**
    * Update payment method
   * @param paymentMethodId
   * @param content
   * @returns
    */
  public async updatePaymentMethod(
    user_id,
    payment_method_id: string,
    param: UpdatePaymentMethod,
  ) {
    try {
      const payment_method_token = param.payment_method_token;
      const type = param.type;

      const userRepo = getConnection().getRepository(UserEntity);
      const userRes = await userRepo.findOne({
        id: user_id,
      });

      const customer_id = userRes.stripe_customer_id;

      const res = await this.http
        .post(
          `${this.config.envConfig.PAYMENT_SERVICE_URL}/payment-method/${payment_method_id}`,
          {
            type: type,
            customer_id: customer_id,
            payment_method_token: payment_method_token,
          },
        )
        .toPromise();
      return res.data;
    } catch (err) {
      debug('err:', err);
      if (
        err &&
        err.response &&
        err.response.status >= 400 &&
        err.response.status < 500
      ) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }
  }

  /**
   * Detach payment method to customer
   * @param paymentMethodId
   * @param customerId 
   * @returns 
   */
  public async detachPaymentMethod(param: DetachPaymentMethod) {
    const customer_id = param.customer_id;
    const payment_method_id = param.payment_method_id;
    try {
      const res = await this.http
        .post(
          `${this.config.envConfig.PAYMENT_SERVICE_URL}/remove-payment-method`,
          {
            customer_id: customer_id,
            payment_method_id: payment_method_id,
          },
        )
        .toPromise();

      const userRepo = getConnection().getRepository(UserEntity);
      const user = new UserEntity();
      user.stripe_payment_method_id = '';
      await userRepo.update({ stripe_customer_id: customer_id }, user);
      return res.data;

    } catch (err) {
      debug('err:', err);
      if (
        err &&
        err.response &&
        err.response.status >= 400 &&
        err.response.status < 500
      ) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }
  }

  /**
   * Set payment method to customer
   * @param paymentMethodId
   * @param customerId 
   * @returns 
   */
  public async setPaymentMethod(param: SetPaymentMethod) {
    const customer_id = param.customer_id;
    const payment_method_id = param.payment_method_id;
    try {
      const res = await this.http
        .post(
          `${this.config.envConfig.PAYMENT_SERVICE_URL}/set-payment-method`,
          {
            customer_id,
            payment_method_id,
          },
        )
        .toPromise();

      const userRepo = getConnection().getRepository(UserEntity);
      const user = new UserEntity();
      user.stripe_payment_method_id = payment_method_id;
      await userRepo.update({ stripe_customer_id: customer_id }, user);
      return res.data;

    } catch (err) {
      debug('err:', err);
      if (
        err &&
        err.response &&
        err.response.status >= 400 &&
        err.response.status < 500
      ) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }
  }

  /**
   * Get payment methods
   * @param customerId 
   * @returns 
   */
  public async getPaymentMethods(user_id) {

    try {
      const userRepo = getConnection().getRepository(UserEntity);
      const userRes = await userRepo.findOne({
        id: user_id,
      });

      const stripe_customer_id = userRes.stripe_customer_id;

      const res = await this.http
        .post(`${this.config.envConfig.PAYMENT_SERVICE_URL}/payment-methods`, {
          customer_id: stripe_customer_id,
        })
        .toPromise();
      // const res = null;
      return res.data;

    } catch (err) {
      debug('err:', err);
      if (
        err &&
        err.response &&
        err.response.status >= 400 &&
        err.response.status < 500
      ) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }
  }
  /**
     * Get cards
     * @param customerId 
     * @returns 
     */
  public async listCards(user_id) {

    try {
      const userRepo = getConnection().getRepository(UserEntity);
      const userRes = await userRepo.findOne({
        id: user_id,
      });

      const stripe_customer_id = userRes.stripe_customer_id;

      const res = await this.http
        .post(`${this.config.envConfig.PAYMENT_SERVICE_URL}/cards`, {
          customer_id: stripe_customer_id,
        })
        .toPromise();
      // const res = null;
      return res.data;

    } catch (err) {
      debug('err:', err);
      if (
        err &&
        err.response &&
        err.response.status >= 400 &&
        err.response.status < 500
      ) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }
  }
  /**
   * Creating subscriptions
   * @param createSubscription 
   * @returns 
   */
  public async createSubscription(param: CreateSubscription) {
    try {


      const userExistRepo = getConnection().getRepository(UserEntity);
      const userExistRes = await userExistRepo.findOne({
        stripe_customer_id: param.customer_id,
      });

      if (
        // eslint-disable-next-line eqeqeq
        param.product!='basic' &&
        userExistRes.stripe_subscription_id !== '' &&
        userExistRes.stripe_subscription_id != null &&
        userExistRes.stripe_subscription_id !== 'undefined'
      ) {
        return new HttpException({
          name: 'SUBSCRIPTION_ALREADY_CREATED',
          message: 'Subscription already created',
        },
          HttpStatus.UNAUTHORIZED,
        );
      }

      const res = await this.http
        .post(`${this.config.envConfig.PAYMENT_SERVICE_URL}/subscription`, {
          customer_id: param.customer_id,
          price_id: param.price_id,
          promo_code: param.promo_code,
          collection_method: param.collection_method,
          product: param.product
        })
        .toPromise();
        debug('origin priceid:', param.price_id);
      
      // get subscription amt
      const pricid = res.data.items.data[0].price.id;
      debug('pricid:', pricid);
      const getSubDet = await this.getSubscriptionAmt(pricid, param.product);

      const subRepo = getConnection().getRepository(SubscriptionEntity);
      const sub = new SubscriptionEntity();
      sub.id = res.data.id;
      sub.stripe_subscription_id = res.data.id;
      sub.stripe_customer_id = res.data.customer.toString();
      sub.collection_method = res.data.collection_method;
      sub.stripe_price_id = res.data.items.data[0].price.id;
      sub.stripe_product_id = res.data.items.data[0].plan.product.toString();
      sub.status = res.data.status;
      sub.amount = getSubDet.amt;
      sub.plan = getSubDet.pln;
      sub.plan_start_date = new Date(res.data.start_date);
      sub.created_at = new Date();
      await subRepo.save(sub);

      const userRepo = getConnection().getRepository(UserEntity);
      const user = new UserEntity();
      user.stripe_subscription_id = res.data.id;
      user.membership_type = param.product;
      user.stripe_price_id = param.price_id;
      await userRepo.update({ stripe_customer_id: param.customer_id }, user);

      // Update membership type in keycloak

      const datas = {
        attributes: {
          membershiptype: param.product,
        }
      };

      const keycloakToken = (await this.usersService.getKeycloak()).token;
      debug('keycloakToken : ', keycloakToken);

      const headers = {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${keycloakToken}`,
      };

      const userData = await UserEntity.findOne({
        stripe_customer_id: param.customer_id,
      });
      const addRes = await this.http
        .put(
          `${(await this.usersService.getKeycloak()).ip}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_AGENT}/users/${userData.id}`,
          datas,
          {
            headers,
          },
        )
        .toPromise();


      /*
      const subItemRepo = getConnection().getRepository(SubscriptionItemEntity);

      for (const item of res.items.data) {
        const subItem = new SubscriptionItemEntity();
        subItem.subscription_id = res.id;
        subItem.subscription_item_id = item.id;
        subItem.stripe_id = res.customer.toString();
        subItem.quantity = item.quantity;
        subItem.plan_id = item.plan.id;
        subItem.price = item.plan.amount;
        subItem.product_id = item.plan.product.toString();
        subItem.created_at = new Date();
        await subItemRepo.save(subItem);
      }*/

      return res.data;

    } catch (err) {
      debug('err:', err);
      if (
        err &&
        err.response &&
        err.response.status >= 400 &&
        err.response.status < 500
      ) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Subscription err : ', err);
        throw err;
      }
    }
  }

  /**
   * Get subscriptions
   * @param customerId 
   * @returns 
   */
  public async getSubscription(user_id) {
    try {

      const userRepo = getConnection().getRepository(UserEntity);
      const userRes = await userRepo.findOne({
        id: user_id,
      });

      const subscription_id = userRes.stripe_subscription_id;

      const res = await this.http
        .get(
          `${this.config.envConfig.PAYMENT_SERVICE_URL}/subscription/${subscription_id}`,
        )
        .toPromise();
      // const res = null;
      return res.data;

    } catch (err) {
      debug('err:', err);
      if (
        err &&
        err.response &&
        err.response.status >= 400 &&
        err.response.status < 500
      ) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }
  }

  /**
   * Get Invoices
   * @returns 
   */
  public async getInvoices(customer_id, req) {
    try {
      const res = await this.http
        .get(
          `${this.config.envConfig.PAYMENT_SERVICE_URL}/invoices/${customer_id}/?start_date=${req.query.start_date}&end_date=${req.query.end_date}&limit=${req.query.limit}`,
        )
        .toPromise();
      return res.data;
    } catch (err) {
      debug('err:', err);
      if (
        err &&
        err.response &&
        err.response.status >= 400 &&
        err.response.status < 500
      ) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }
  }

  /**
   * Get Invoice Details
   * @returns 
   */
  public async getInvoiceDetails(invoice_id) {
    try {
      const res = await this.http
        .get(
          `${this.config.envConfig.PAYMENT_SERVICE_URL}/invoice/${invoice_id}`,
        )
        .toPromise();
      return res.data;
    } catch (err) {
      debug('err:', err);
      if (
        err &&
        err.response &&
        err.response.status >= 400 &&
        err.response.status < 500
      ) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }
  }

  /**
   * Get Payments
   * @returns
   */
  public async getPayments(customer_id, req) {
    try {
      const res = await this.http
        .get(
          `${this.config.envConfig.PAYMENT_SERVICE_URL}/payments/${customer_id}/?start_date=${req.query.start_date}&end_date=${req.query.end_date}&limit=${req.query.limit}`,
        )
        .toPromise();
      return res.data;
    } catch (err) {
      debug('err:', err);
      if (
        err &&
        err.response &&
        err.response.status >= 400 &&
        err.response.status < 500
      ) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Payments err : ', err);
        throw err;
      }
    }

  }

  /**
   * Get Payment details
   * @returns 
   */
  public async getPaymentDetails(payment_id) {
    try {
      const res = await this.http
        .get(
          `${this.config.envConfig.PAYMENT_SERVICE_URL}/payment/${payment_id}`,
        )
        .toPromise();
      return res.data;
    } catch (err) {
      debug('err:', err);
      if (
        err &&
        err.response &&
        err.response.status >= 400 &&
        err.response.status < 500
      ) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }
  }

  /**
   * Get Transaction details
   * @returns 
   */
  public async getTransaction(transaction_id) {
    try {
      const res = await this.http
        .get(
          `${this.config.envConfig.PAYMENT_SERVICE_URL}/transaction/${transaction_id}`,
        )
        .toPromise();
      return res.data;
    } catch (err) {
      debug('err:', err);
      if (
        err &&
        err.response &&
        err.response.status >= 400 &&
        err.response.status < 500
      ) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }
  }

  /**
   * Creating subscriptions
   * @param setUpgradedowngrade 
   * @returns 
   */
  public async setUpgradeDowngrade(customer_id, param: SetUpgradeDowngrade) {

    try {
      const price_id = param.price_id;

      const subRepo = getConnection().getRepository(SubscriptionEntity);
      const subRes = await subRepo.findOne({
        stripe_customer_id: customer_id,
      });
      if (!subRes) {
        return new HttpException({
          name: 'SUBSCRIPTION_NOT_FOUND',
          message: 'Invalid subscription id',
        },
          HttpStatus.UNAUTHORIZED,
        );
      }
      const { stripe_subscription_id } = subRes;
      debug('subscription id: ', stripe_subscription_id);
      const res = await this.http
        .post(
          `${this.config.envConfig.PAYMENT_SERVICE_URL}/upgrade-downgrade/${customer_id}`,
          {
            subscription_id: subRes.stripe_subscription_id,
            price_id: price_id
          }
        )
        .toPromise();

      // Update membership type in keycloak

      const datas = {
        attributes: {
          membershiptype: param.product,
        }
      };

      //datas['attributes'] = {
      //membershiptype: (this.config.envConfig.MEMBERSHIPTYPE || 'basic' ),
      // };
      const keycloakToken = (await this.usersService.getKeycloak()).token;
      debug('keycloakToken : ', keycloakToken);

      const headers = {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${keycloakToken}`,
      };

      const userData = await UserEntity.findOne({
        stripe_customer_id: customer_id,
      });
      const addRes = await this.http
        .put(
          `${(await this.usersService.getKeycloak()).ip}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_AGENT}/users/${userData.id}`,
          datas,
          {
            headers,
          },
        )
        .toPromise();

      const userRepo = getConnection().getRepository(UserEntity);
      const user = new UserEntity();
      user.stripe_subscription_id = res.data.id;
      user.membership_type = param.product;
      user.stripe_price_id = param.price_id;
      await userRepo.update({ stripe_customer_id: customer_id }, user);

      // get subscription amt
      const getSubDet = await this.getSubscriptionAmt(param.price_id, param.product);

      // update subscription amt in subscription tbl
      await subRepo.update(
        { stripe_customer_id: customer_id },
        { updated_at: new Date(), amount: getSubDet.amt, plan: getSubDet.pln },
      );

      return res.data;
    } catch (err) {
      debug('err:', err);
      if (
        err &&
        err.response &&
        err.response.status >= 400 &&
        err.response.status < 500
      ) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }
  }

  /**
   * Get Webhook stripe response
   * @returns 
   */
  public async webhook(res: any) {
    try {
      console.log("Entering webhook activities");
      console.log("type=",res.type);
      switch (res.type) {
        case 'charge.failed':
        case 'invoice.payment_failed':
        case 'payment_intent.payment_failed':  
          this.getFailureTransaction(res.data.object);
          break;
        case 'customer.subscription.updated':
          this.webhookUpdateSubscription(res);
          break;  
        case 'customer.subscription.deleted':
          this.webhookBasicSubscription(res);
          break;
        case 'price.updated':
          const price = res.data.object;
          // Then define and call a function to handle the event price.updated
          break;
        case 'subscription_schedule.completed':
          // const subscriptionSchedule = event.data.object;
          // Then define and call a function to handle the event subscription_schedule.completed
          break;
        case 'subscription_schedule.created':
          const subscriptionSchedule = res.data.object;
          // Then define and call a function to handle the event subscription_schedule.created
          break;
        // ... handle other event types
        default:
          console.log(`Unhandled event type ${res.type}`);
      }

      return null;


    } catch (err) {
      debug('err:', err);
      if (
        err &&
        err.response &&
        err.response.status >= 400 &&
        err.response.status < 500
      ) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Webhook err : ', err);
        throw err;
      }
    }
  }

  /**
   * Get Failure transactions
   * @returns 
   */
  public async getFailureTransaction(data) {
    console.log("Entering getFailureTrasaction");
    console.log(data);
    const message = 'Error: ' + data.object.failure_code + ' ' + data.object.failure_message;
    const amount = data.amount;
    const customer_id = data.customer;

    const userRepo = getConnection().getRepository(UserEntity);
    const userRes = await userRepo.findOne({
      stripe_customer_id: customer_id
    });

    if (userRes) {

      const email = userRes.email;
      const mobile = userRes.mobile;
      const mail = await this.http
        .post(`${this.config.envConfig.NOTIFY_SERVICE_URL}/notify/sendPaymentFailure`, {
          email: email,
          phone: mobile,
          message: message,
          amount: amount,
          subject: 'Payment Failure',
          template: 'PaymentFailure'
        }).toPromise();

    }
  }

  /**
   * Get Invoices
   * @returns 
   */
  public async getCardLogo(card_type) {
    try {

      const cardRepo = getConnection().getRepository(CardEntity);
      const cardRes = await cardRepo.findOne({
        card_type
      });

      return cardRes.logo;

    } catch (err) {
      debug('err:', err);
      if (
        err &&
        err.response &&
        err.response.status >= 400 &&
        err.response.status < 500
      ) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }
  }

  /**
   * Preview subscriptions
   * @param SubscriptionPreview
   * @returns 
   */
  public async preview(user_id, param: SubscriptionPreview) {

    try {
      const price_id = param.price_id;

      const userRepo = getConnection().getRepository(UserEntity);
      const userRes = await userRepo.findOne({
        id: user_id,
      });
      if (!userRes) {
        return new HttpException({
          name: 'USER_NOT_FOUND',
          message: 'Invalid User',
        },
          HttpStatus.UNAUTHORIZED,
        );
      }
      if (userRes.stripe_subscription_id == "" || userRes.stripe_subscription_id == null) {
        return new HttpException({
          name: 'SUBSCRIPTION_NOT_FOUND',
          message: 'Invalid Subscription',
        },
          HttpStatus.UNAUTHORIZED,
        );
      }
      const { stripe_subscription_id, stripe_customer_id } = userRes;
      debug('subscription id: ', stripe_subscription_id);
      debug('customer id: ', stripe_customer_id);
      const res = await this.http
        .post(
          `${this.config.envConfig.PAYMENT_SERVICE_URL}/preview`,
          {
            customer_id: stripe_customer_id,
            subscription_id: stripe_subscription_id,
            price_id: param.price_id,
          }
        )
        .toPromise();

      return res.data;
    } catch (err) {
      debug('err:', err);
      if (
        err &&
        err.response &&
        err.response.status >= 400 &&
        err.response.status < 500
      ) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }
  }
/**
   * Webhook Update subscription
   * @returns 
   */
 public async webhookUpdateSubscription(data) {
  console.log("webhook update subscription");
  console.log(data);
  //const message = 'Error: ' + data.failure_code + ' ' + data.failure_message;
  
  const product_id = data.data.object.plan.product;
  const customer_id = data.data.object.customer;

  console.log("product id, customer id");
  console.log(product_id);
  console.log(customer_id);

  const productRes = await this.http
  .get(
    `${this.config.envConfig.PAYMENT_SERVICE_URL}/get-product-name/${product_id}`,
  )
  .toPromise();

  console.log("get product name api,",productRes);
  const userRepo = getConnection().getRepository(UserEntity);
  const userRes = await userRepo.findOne({
    stripe_customer_id: customer_id
  });

  console.log("product name/membership_type", productRes.data);
  
  if (userRes) {
    if(productRes.data!="" && productRes.data!=null && productRes.data!='undefined' && productRes.data!=undefined) {
      const user = new UserEntity();
      user.membership_type = productRes.data.toLowerCase();
      await userRepo.update({ stripe_customer_id: customer_id }, user);
      console.log("product/membership type updated to",productRes.data.toLowerCase());
      console.log("for the customer ",customer_id);

    }
    const message = 'Your subscription updated to '+productRes.data;
    const email = userRes.email;
    const mobile = userRes.mobile;
    const mail = await this.http
      .post(`${this.config.envConfig.NOTIFY_SERVICE_URL}/notify/sendSubscriptionStatus`, {
        email: email,
        phone: mobile,
        message: message,
        subject: 'Subscription Status',
        template: 'SubscriptionStatus'
      }).toPromise();

  }
}
/**
   * Webhook change to basic subscription
   * @returns 
   */
 public async webhookBasicSubscription(data) {
  console.log("webhook change to basic subscription");
  console.log(data);
  //const message = 'Error: ' + data.failure_code + ' ' + data.failure_message;
  const message = 'Your subscription status changed to Basic.';
  const product_id = data.data.object.plan.product;
  const customer_id = data.data.object.customer;

  console.log("product id, customer id");
  console.log(product_id);
  console.log(customer_id);


  const userRepo = getConnection().getRepository(UserEntity);
  const userRes = await userRepo.findOne({
    stripe_customer_id: customer_id
  });

  
  if (userRes) {
    
      const user = new UserEntity();
      user.membership_type = "basic";
      await userRepo.update({ stripe_customer_id: customer_id }, user);
      console.log("product/membership type updated to","basic");
      console.log("for the customer ",customer_id);
   
      var param = new CreateSubscription();
      param.customer_id = customer_id;
      param.product = "basic";
      param.price_id="price_1L2ZuSFSgVYpzNjn3DenTtpu";
      param.collection_method="charge_automatically";
      this.createSubscription(param);


    const email = userRes.email;
    const mobile = userRes.mobile;
    const mail = await this.http
      .post(`${this.config.envConfig.NOTIFY_SERVICE_URL}/notify/sendSubscriptionStatus`, {
        email: email,
        phone: mobile,
        message: message,
        subject: 'Subscription Status',
        template: 'SubscriptionStatus'
      }).toPromise();

  }
}

  // get amount from subscription/products api and insert into subscription tbl
  public async getSubscriptionAmt(priceid, prdct) {
  let amt, pln;
  const getAllPrdct = await this.listProducts();
  debug('getAllPrdct:', getAllPrdct);
  debug('hai');
  debug('priceid:', priceid);
  debug('prdct:', prdct);

  const planName = prdct;
  const properPlan = planName.charAt(0).toUpperCase() + planName.slice(1); 
  debug('properPlan:', properPlan);

  for(let i = 0; i < getAllPrdct.length; i++)
  {
    debug('entered for-1');
    debug('getAllPrdct[i].product:', getAllPrdct[i].product);
    if(getAllPrdct[i].product === properPlan){
      debug('length:', getAllPrdct[i].plan.length);
      for(let j = 0; j < getAllPrdct[i].plan.length; j++)
      {
        debug('param.price_id:', priceid);
        if(getAllPrdct[i].plan[j].plan_id === priceid){
          debug('entered if-2');
          amt = getAllPrdct[i].plan[j].amount;
          pln = getAllPrdct[i].plan[j].plan;
        }
      }
    }
  }
  debug('getAmt:', amt)
  debug('getPlan:', pln)
   return {amt, pln};
  }

}