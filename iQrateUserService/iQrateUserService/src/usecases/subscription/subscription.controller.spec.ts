import { HttpService } from '@nestjs/axios';
import { ConfigService } from '../../config/config.service';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserService } from '../user/user.service';
import { SubscriptionController } from '../subscription/subscription.controller';
import { SubscriptionService } from './subscription.service';
import { ConfigModule } from '../../config/config.module';
import { HttpStatus } from '@nestjs/common';
import Debug from 'debug';

const debug = Debug('connector:SubscriptionController:Spec');

describe('SubscriptionController', () => {
  let subController: SubscriptionController;
  // let subService: SubscriptionService;
 let subService = new SubscriptionService(new ConfigService(), new HttpService(), new UserService(new ConfigService(), new HttpService()));
//  const userService = new UserService(new ConfigService(), new HttpService());


  beforeAll(async () => {
    
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRootAsync({
          imports: [ConfigModule],
          useFactory: (config: ConfigService) => config.getTypeORMConfig(),
          inject: [ConfigService, SubscriptionService],
        }),
      ],
      controllers: [SubscriptionController],
      providers: [SubscriptionService, UserService],
    })
    .overrideProvider(SubscriptionService)
    .useValue(subService)
    .compile();

    subController = module.get<SubscriptionController>(SubscriptionController);
    subService = module.get<SubscriptionService>(SubscriptionService);
  });

  it('should be defined', () => {
    expect(SubscriptionController).toBeDefined();
  });

  it('Resend sms', async () => {
    debug('TEST RESULT 3:');
    const testRes = await subService.getCustomer('cus_sasasu4');
    debug('testRes mobile:', testRes);
    // expect(testRes.message).toEqual('Please verify the mobile');
  });
}); // Main end
