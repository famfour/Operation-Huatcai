import { ApiProperty } from '@nestjs/swagger';
import {
  IsDefined,
  IsEmail, IsIn,
  IsInt,
  IsNotEmpty,
  IsString,
  Length,
} from 'class-validator';
export class CreateCustomerDto {
  [x: string]: any;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    type: String,
    description: 'name',
    default: 'Tamilstripe'
  })
  name: string;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'email',
    default: 'tamilselvan@digitalprizm.net'
  })
  email: string;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'user_id',
    default: '1'
  })
  user_id: string;

  @ApiProperty({
    description: 'phone',
    default: '9962679318'
  })
  phone: string;

}
export class UpdateCustomerDto {
  [x: string]: any;

  @ApiProperty({
    type: String,
    description: 'name',
    default: 'Tamilstripe'
  })
  name: string;

  @ApiProperty({
    description: 'email',
    default: 'tamilselvan@digitalprizm.net'
  })
  email: string;

  @ApiProperty({
    description: 'phone',
    default: '919962679318'
  })
  phone: string;


}

export class CustomerDto {
  [x: string]: any;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    type: String,
    description: 'customer_id',
    default: 'cus_L5ChpsVGQGoGTL'
  })
  customer_id: string;

}

export class ChargeCustomerDto {
  [x: string]: any;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    type: Number,
    description: 'amount',
    default: '1'
  })
  amount: number;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'customer_id',
    default: 'cus_L5ChpsVGQGoGTL'
  })
  customer_id: string;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'payment_method_id',
    default: 'testpay12'
  })
  payment_method_id: string;


  @ApiProperty({
    description: 'confirm',
    default: true,
  })
  confirm: boolean;

}

export class CreatePaymentMethod {
  [x: string]: any;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'type',
    default: 'card',
  })
  type: string;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'customer_id',
    default: 'cus_L5ChpsVGQGoGTL'
  })
  customer_id: string;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'payment_method_token',
    default: ''
  })
  payment_method_token: string;

}

export class UpdatePaymentMethod {
  [x: string]: any;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'type',
    default: 'card'
  })
  type: string;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'customer_id',
    default: 'cus_L5ChpsVGQGoGTL'
  })
  customer_id: string;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'payment_method_token',
    default: ''
  })
  payment_method_token: string;


}

export class SetPaymentMethod {
  [x: string]: any;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'customer_id',
    default: 'cus_L5ChpsVGQGoGTL'
  })
  customer_id: string;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'payment_method_id',
    default: 'pm_1KQVgtB9aacV2G5wr96Z9Pyg'
  })
  payment_method_id: string;

}

export class DetachPaymentMethod {
  [x: string]: any;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'customer_id',
    default: 'cus_L5ChpsVGQGoGTL'
  })
  customer_id: string;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'payment_method_id',
    default: 'pm_1KQVgtB9aacV2G5wr96Z9Pyg'
  })
  payment_method_id: string;

}

export class CreateSubscription {
  [x: string]: any;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'customer_id',
    default: 'cus_L5ZbSNa8fBeMIm'
  })
  customer_id: string;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'price_id',
    default: 'price_1KOzL6B9aacV2G5wfgJWIwEJ'
  })
  price_id: string;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'collection_method',
    default: 'charge_automatically'
  })
  collection_method: string;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'product',
    default: 'Basic'
  })
  product: string;

  @ApiProperty({
    description: 'promo_code',
    default: "FEST"
  })
  promo_code: string;


}
export class SetUpgradeDowngrade {
  [x: string]: any;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'price_id',
    default: 'price_1KRC91B9aacV2G5wcNxyXxht'
  })
  price_id: string;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'product',
    default: 'Premium'
  })
  product: string;

}
export class SubscriptionPreview {
  [x: string]: any;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'price_id',
    default: 'price_1KRC91B9aacV2G5wcNxyXxht'
  })
  price_id: string;

}

