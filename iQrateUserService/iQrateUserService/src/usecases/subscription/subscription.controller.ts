import {
  Controller,
  Get,
  Post,
  Body,
  Headers,
  Put,
  Param,
  Patch,
  Req,
  HttpException,
  BadRequestException,
  HttpStatus,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiForbiddenResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiCreatedResponse,
  ApiTags,
} from '@nestjs/swagger';
import Debug from 'debug';
// import { conditionalExpression } from '@babel/types';
import { Request } from 'express';
// import { Roles, Unprotected } from 'nest-keycloak-connect';
import { SubscriptionService } from './subscription.service';
import {
  CreateCustomerDto, UpdateCustomerDto,
  CreatePaymentMethod, DetachPaymentMethod,
  CreateSubscription,
  SetPaymentMethod,
  UpdatePaymentMethod,
  SetUpgradeDowngrade,
  SubscriptionPreview
} from './subscription.dto';
const debug = Debug('user-ctrlr:UserController');

@ApiTags('User/Subscription')
@Controller('user/subscription')
export class SubscriptionController {
  constructor(private readonly subscriptionService: SubscriptionService) { }

  @Post('customer')
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'create customer in stripe' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  @ApiNotFoundResponse({ description: 'not found' })
  async createCustomer(
    @Headers() headers: Headers,
    @Body() content: CreateCustomerDto,
  ) {
    const userId = headers['userid'];
    if (!userId) {
      new HttpException({
        name: 'USERID_NOT_FOUND',
        message: 'Token is invalid',
      },
        HttpStatus.UNAUTHORIZED,
      );
    }
    debug('create customer: ', content);
    return this.subscriptionService.createCustomer(content);
  }

  /**
   * Get customer details
   * @param customer_id 
   * @returns 
   */
  @Get('customer/:id')
  @ApiBearerAuth()
  @ApiCreatedResponse({ description: 'Get Customer' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  @ApiNotFoundResponse({ description: 'not found' })
  async getCustomer(@Headers() headers: Headers, @Param('id') customer_id: string) {
    debug('Get customer inputs: ', customer_id);
    const userId = headers['userid'];
    if (!userId) {
      new HttpException({
        name: 'USERID_NOT_FOUND',
        message: 'Token is invalid',
      },
        HttpStatus.UNAUTHORIZED,
      );
    }
    return this.subscriptionService.getCustomer(customer_id);
  }

  /**
  * Update customer details
   * @param updateCustomerDto
   * @returns
  */
  @Put('customer/:id')
  @ApiBearerAuth()
  @ApiCreatedResponse({ description: 'Update Customer' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  @ApiNotFoundResponse({ description: 'not found' })
  async updateCustomer(@Headers() headers: Headers,
    @Param('id') customer_id: string,
    @Body() content: UpdateCustomerDto,
  ) {
    debug('Update customer inputs: ', customer_id);
    const userId = headers['userid'];
    if (!userId) {
      new HttpException({
        name: 'USERID_NOT_FOUND',
        message: 'Token is invalid',
      },
        HttpStatus.UNAUTHORIZED,
      );
    }
    return this.subscriptionService.updateCustomer(customer_id, content);
  }

  /**
   * List All Products
   * @returns
   */
  @Get('products')
  @ApiBearerAuth()
  @ApiCreatedResponse({ description: 'List products' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  @ApiNotFoundResponse({ description: 'not found' })
  async listProducts(@Headers() headers: Headers) {
    const userId = headers['userid'];
    if (!userId) {
      new HttpException({
        name: 'USERID_NOT_FOUND',
        message: 'Token is invalid',
      },
        HttpStatus.UNAUTHORIZED,
      );
    }
    return this.subscriptionService.listProducts();
  }

  /**
  * Create Payment method
   * @param content
   * @returns
   */
  @Post('payment-method')
  @ApiBearerAuth()
  @ApiCreatedResponse({ description: 'Create payment method' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async createPaymentMethod(@Headers() headers: Headers, @Body() content: CreatePaymentMethod) {
    const userId = headers['userid'];
    if (!userId) {
      new HttpException({
        name: 'USERID_NOT_FOUND',
        message: 'Token is invalid',
      },
        HttpStatus.UNAUTHORIZED,
      );
    }
    return this.subscriptionService.createPaymentMethod(content);
  }

  /**
   * Remove Payment method to customer
   * @param content 
   * @returns 
   */
  @Post('remove-payment-method')
  @ApiBearerAuth()
  @ApiCreatedResponse({ description: 'Set payment method' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async detachPaymentMethod(@Headers() headers: Headers, @Body() content: DetachPaymentMethod) {
    const userId = headers['userid'];
    if (!userId) {
      new HttpException({
        name: 'USERID_NOT_FOUND',
        message: 'Token is invalid',
      },
        HttpStatus.UNAUTHORIZED,
      );
    }
    return this.subscriptionService.detachPaymentMethod(content);
  }

  /**
    * Update Payment method
   * @param content
   * @returns
    */
  @Post('payment-method/:id')
  @ApiBearerAuth()
  @ApiCreatedResponse({ description: 'Create payment method' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async updatePaymentMethod(
    @Headers() headers: Headers,
    @Param('id') payment_method_id: string,
    @Body() content: UpdatePaymentMethod,
  ) {
    const userId = headers['userid'];
    if (!userId) {
      new HttpException({
        name: 'USERID_NOT_FOUND',
        message: 'Token is invalid',
      },
        HttpStatus.UNAUTHORIZED,
      );
    }
    return this.subscriptionService.updatePaymentMethod(
      userId,
      payment_method_id,
      content,
    );
  }

  /**
   * Set Payment method to customer
   * @param content 
   * @returns 
   */
  @Post('set-payment-method')
  @ApiBearerAuth()
  @ApiCreatedResponse({ description: 'Set payment method' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async setPaymentMethod(
    @Headers() headers: Headers,
    @Body() content: SetPaymentMethod,
  ) {
    try {
      const userId = headers['userid'];
      if (!userId) {
        new HttpException({
          name: 'USERID_NOT_FOUND',
          message: 'Token is invalid',
        },
          HttpStatus.UNAUTHORIZED,
        );
      }
      return this.subscriptionService.setPaymentMethod(content);
    } catch (err) {
      if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('login err : ', err);
        throw err;
      }
    }
  }

  /**
   * List payment methods
   * @param content
   * @returns 
   */
  @Get('payment-methods')
  @ApiBearerAuth()
  @ApiCreatedResponse({ description: 'Get all payment methods' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async getPaymentMethods(@Headers() headers: Headers) {
    const userId = headers['userid'];
    if (!userId) {
      new HttpException({
        name: 'USERID_NOT_FOUND',
        message: 'Token is invalid',
      },
        HttpStatus.UNAUTHORIZED,
      );
    }
    return this.subscriptionService.getPaymentMethods(userId);
  }
  /**
    * List cards
    * @param content
    * @returns 
    */
  @Get('cards')
  @ApiBearerAuth()
  @ApiCreatedResponse({ description: 'Get all cards' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async listCards(@Headers() headers: Headers) {
    const userId = headers['userid'];
    if (!userId) {
      new HttpException({
        name: 'USERID_NOT_FOUND',
        message: 'Token is invalid',
      },
        HttpStatus.UNAUTHORIZED,
      );
    }
    return this.subscriptionService.listCards(userId);
  }
  /**
   * Create Subscriptions
   * @param content 
   * @returns 
  */
  @Post('create')
  @ApiBearerAuth()
  @ApiCreatedResponse({ description: 'Create subscription' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async createSubscription(@Headers() headers: Headers, @Body() content: CreateSubscription) {
    const userId = headers['userid'];
    if (!userId) {
      new HttpException({
        name: 'USERID_NOT_FOUND',
        message: 'Token is invalid',
      },
        HttpStatus.UNAUTHORIZED,
      );
    }
    return this.subscriptionService.createSubscription(content);
  }

  /**
   * Get Subscriptions
   * @param content
   * @returns 
   */
  @Get('')
  @ApiBearerAuth()
  @ApiCreatedResponse({ description: 'Get subscription' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async getSubscription(@Headers() headers: Headers) {
    const userId = headers['userid'];
    if (!userId) {
      new HttpException({
        name: 'USERID_NOT_FOUND',
        message: 'Token is invalid',
      },
        HttpStatus.UNAUTHORIZED,
      );
    }
    return this.subscriptionService.getSubscription(userId);
  }

  /**
   * List All Subscriptions
   * @returns 
   */
  @Get('all/:id')
  @ApiBearerAuth()
  @ApiCreatedResponse({ description: 'List subscriptions' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async listSubscriptions(@Headers() headers: Headers, @Param('id') customer_id: string) {
    return this.subscriptionService.listSubscriptions(customer_id);
  }

  /**
   * Get Invoices
   * @returns 
   */
  @Get('invoices/:id')
  @ApiBearerAuth()
  @ApiCreatedResponse({ description: 'Get Invoices' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async getInvoices(@Headers() headers: Headers, @Param('id') customer_id: string, @Req() req: Request) {
    const userId = headers['userid'];
    if (!userId) {
      new HttpException({
        name: 'USERID_NOT_FOUND',
        message: 'Token is invalid',
      },
        HttpStatus.UNAUTHORIZED,
      );
    }
    return this.subscriptionService.getInvoices(customer_id, req);
  }

  /**
   * Get Invoices Details
   * @returns 
   */
  @Get('invoice/:id')
  @ApiBearerAuth()
  @ApiCreatedResponse({ description: 'Get Invoice details' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async getInvoiceDetails(@Headers() headers: Headers, @Param('id') invoice_id: string) {
    const userId = headers['userid'];
    if (!userId) {
      new HttpException({
        name: 'USERID_NOT_FOUND',
        message: 'Token is invalid',
      },
        HttpStatus.UNAUTHORIZED,
      );
    }
    return this.subscriptionService.getInvoiceDetails(invoice_id);
  }

  /**
   * Get Payments
   * @param customer_id
   * @returns 
   */
  @Get('payments/:id')
  @ApiBearerAuth()
  @ApiCreatedResponse({ description: 'Get Invoices' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async getPayments(@Headers() headers: Headers, @Param('id') customer_id: string, @Req() req: Request) {
    const userId = headers['userid'];
    if (!userId) {
      new HttpException({
        name: 'USERID_NOT_FOUND',
        message: 'Token is invalid',
      },
        HttpStatus.UNAUTHORIZED,
      );
    }
    return this.subscriptionService.getPayments(customer_id, req);
  }

  /**
   * Get payment details
   * @param payment_id
   * @returns 
   */
  @Get('payment/:id')
  @ApiBearerAuth()
  @ApiCreatedResponse({ description: 'Get Payment details' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async getPaymentDetails(@Headers() headers: Headers, @Param('id') payment_id: string) {
    const userId = headers['userid'];
    if (!userId) {
      new HttpException({
        name: 'USERID_NOT_FOUND',
        message: 'Token is invalid',
      },
        HttpStatus.UNAUTHORIZED,
      );
    }
    return this.subscriptionService.getPaymentDetails(payment_id);
  }

  /**
   * Get transactions
   * @returns 
   */
  @Get('transaction/:id')
  @ApiBearerAuth()
  @ApiCreatedResponse({ description: 'Get Transactions' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async getTransaction(@Headers() headers: Headers, @Param('id') transaction_id: string) {
    const userId = headers['userid'];
    if (!userId) {
      new HttpException({
        name: 'USERID_NOT_FOUND',
        message: 'Token is invalid',
      },
        HttpStatus.UNAUTHORIZED,
      );
    }
    return this.subscriptionService.getTransaction(transaction_id);
  }

  /**
   * Upgrade & downgrade Subscriptions
   * @param content 
   * @returns 
   */
  @Post('upgrade-downgrade/:id')
  @ApiBearerAuth()
  @ApiCreatedResponse({ description: 'Upgrade / downgrade subscription' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async setUpgradedowngrade(
    @Headers() headers: Headers,
    @Param('id') customer_id: string,
    @Body() content: SetUpgradeDowngrade,
  ) {
    const userId = headers['userid'];
    if (!userId) {
      new HttpException({
        name: 'USERID_NOT_FOUND',
        message: 'Token is invalid',
      },
        HttpStatus.UNAUTHORIZED,
      );
    }
    return this.subscriptionService.setUpgradeDowngrade(customer_id, content);
  }

  /**
 * Get Card logo
 * @returns 
 */
  @Get('cardtype_logo/:card_type')
  @ApiBearerAuth()
  @ApiCreatedResponse({ description: 'Get card logo' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async getCardLogo(@Headers() headers: Headers, @Param('card_type') card_type: string) {
    const userId = headers['userid'];
    if (!userId) {
      new HttpException({
        name: 'USERID_NOT_FOUND',
        message: 'Token is invalid',
      },
        HttpStatus.UNAUTHORIZED,
      );
    }
    return this.subscriptionService.getCardLogo(card_type);
  }


  /**
   * Preview Subscriptions
   * @param content 
   * @returns 
   */
  @Post('preview')
  @ApiBearerAuth()
  @ApiCreatedResponse({ description: 'Preview subscription' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async preview(
    @Headers() headers: Headers,
    @Body() content: SubscriptionPreview,
  ) {
    const userId = headers['userid'];
    if (!userId) {
      new HttpException({
        name: 'USERID_NOT_FOUND',
        message: 'Token is invalid',
      },
        HttpStatus.UNAUTHORIZED,
      );
    }
    return this.subscriptionService.preview(userId, content);
  }



} // Main End
