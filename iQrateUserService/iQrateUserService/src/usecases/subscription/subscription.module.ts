import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { SubscriptionController } from './subscription.controller';
import { UserService } from '../user/user.service';
import { SubscriptionService } from './subscription.service';
import { ConfigModule } from '../../config/config.module';
import { GoogleStrategy } from '../user/google.strategy';
import { UserModule } from '../user/user.module';
import { AdminService } from '../admin/admin.service';

@Module({
  imports: [ConfigModule, HttpModule],
  controllers: [SubscriptionController],
  providers: [SubscriptionService,UserService, GoogleStrategy, AdminService],
  exports: [SubscriptionService],
})
export class SubscriptionModule { }
