import { Body, Controller, HttpStatus, Param, Patch, Post, Put, Res, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AddressDto } from './address.dto';
import { AddressService } from './address.service';
import { FileInterceptor, FilesInterceptor, MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { editFileName, imageFileFilter } from './utils/file-upload.utils';
import {
  ApiCreatedResponse,
  ApiExcludeEndpoint,
  ApiForbiddenResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { extname } from 'path';
import {
  Public,
  Roles,
  Unprotected,
} from 'nest-keycloak-connect';
//@UseGuards(AuthGuard("jwt"))
// @UseGuards(AuthGuard())
@Controller('auth')
export class AddressController {
  constructor(private readonly addressService: AddressService) { }

  //   @Post()
  //   async address(@Body() addressDto: AddressDto): Promise<any> {
  //     return await this.addressService.address(addressDto);
  //   }
  @Post("/:id/address")
   @Roles({ roles: ['user'] })
  public async updateAddress(
   // @Res() res,
    @Param('id') id: string,
    @Body() addressDto: AddressDto
  ): Promise<any> {
    console.log("Testing", this.addressService.updateAddress(id, addressDto));
    return this.addressService.updateAddress(id,addressDto);
  }
  // @Post('file-upload')
  // @UseInterceptors(FileInterceptor('file'))
  // uploadFile(@UploadedFile() file: MulterModule) {
  //   console.log(file);
  // }
  @Post(':id/photo')
    @UseInterceptors(FileInterceptor('file',
      {
        storage: diskStorage({
          destination: './photo', 
          filename: (req, file, cb) => {
          const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
          return cb(null, `${randomName}${extname(file.originalname)}`)
        }
        })
      }
    )
    )
    async upload( @UploadedFile() file) {
      console.log(file)
    }
    
  @Patch('deleteAddress/:id')
  @ApiOkResponse({ description: 'Delete Address' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async deleteAddress(@Param('id') id: string) {
    return this.addressService.deleteAddress(id);
  }



}

