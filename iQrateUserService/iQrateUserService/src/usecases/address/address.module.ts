import { HttpService, Module,HttpModule } from '@nestjs/common';
import { AddressController } from './address.controller';
import { AddressService } from './address.service';
import { ConfigService } from '../../config/config.service';
import { UserService } from '../user/user.service';
import { AdminService } from '../admin/admin.service';


@Module({imports: [ HttpModule ],  
  providers: [AddressService,ConfigService, UserService, AdminService],
  controllers: [AddressController],
 })
export class AddressModule {}
