import { HttpService } from '@nestjs/axios';
import { ConfigService } from '../../config/config.service';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserService } from '../user/user.service';
import { AddressController } from './address.controller';
import { AddressDto } from './address.dto';
import { AddressService } from './address.service';
import { ConfigModule } from '../../config/config.module';
import { HttpStatus } from '@nestjs/common';

describe('AddressController', () => {
  //let controller: AddressController;
  let addressController:AddressController;
//   const userService = new UserService(new ConfigService(), new HttpService());
  const addressService=new AddressService(new UserService(new ConfigService(),new HttpService()));
  beforeAll(async () => {
    
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRootAsync({
          imports: [ConfigModule],
          useFactory: (config: ConfigService) => config.getTypeORMConfig(),
          inject: [ConfigService],
        }),
      ],
      controllers: [AddressController],
      providers: [AddressService],
    })
    .overrideProvider(AddressService)
    .useValue(addressService)
    .compile();

    addressController = module.get<AddressController>(AddressController);
  });

  it('should be defined', () => {
    expect(addressController).toBeDefined();
  });
  it('Update address', async () => {
    console.log('Update address testing...');
    const dto = {
        address: '1/1 East Street',
        postal_code:'MA1250'
    };
    const output = await addressController.updateAddress('8', dto);

    console.log(output);
    // const obj = JSON.stringify(output);
    // const result = JSON.parse(obj);
    //console.log(result);

    expect(output.postal_code).toEqual(dto.postal_code);

});
// it('Delete address', async () => {
//   console.log('Delete address testing...');
//   const dto = {
//       address: '',
//       postal_code:''
//   };
//   const output = await addressController.deleteAddress(10);

//   console.log(output);
//   // const obj = JSON.stringify(output);
//   // const result = JSON.parse(obj);
//   //console.log(result);

//   //expect(output).toEqual(dto.postal_code);

// });
});

