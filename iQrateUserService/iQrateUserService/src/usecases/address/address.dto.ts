import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsEmail, IsInt, IsNotEmpty, Length } from 'class-validator';


export class AddressDto {
    @ApiProperty({
        type: String,
        description: 'Address',
        default: 'Address',
      })
      @IsNotEmpty({ message: 'Should not be empty' })
      @Length(2, 255)
      address: string;

      @ApiProperty({
        type: String,
        description: 'Postalcode',
      })
      @IsNotEmpty({ message: 'Should not be empty' })
      @Length(2, 255)
      postal_code: string;
      
}