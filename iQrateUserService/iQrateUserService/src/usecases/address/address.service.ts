import { HttpException, HttpStatus, Injectable, UploadedFile } from '@nestjs/common';
import { UserEntity } from '../../entities/users';
import { getConnection } from 'typeorm';
import { UserService } from '../user/user.service';
import { AddressDto } from './address.dto';
import { UserDto } from '../user/dto/user.dto';
@Injectable()
export class AddressService {
    UserDto: any;
    userRepo: any;
    constructor(
        private readonly usersService: UserService,
      ) { }
      public async updateAddress(id: string, addressdto: AddressDto): Promise<UserDto> {
        const userRepo = getConnection().getRepository(UserEntity);
        try {
          const user = await this.usersService.findById(id);
          user.address = addressdto.address;
          user.postal_code = addressdto.postal_code;
          
          
           return await userRepo.save(user);
        } catch (err) {
          throw new HttpException(err, HttpStatus.BAD_REQUEST);
        }
      }
      // public async uploadedFile(@UploadedFile() file) {
      //   const response = {
      //     originalname: file.originalname,
      //     filename: file.filename,
      //   };
      //   return response;
      // }
      public async setPhoto(id: string, photo: string){
        const userRepo = getConnection().getRepository(UserEntity);
        this.userRepo.update(id, {photo: photo});
      }
      public async deleteAddress(id: string){
        const userRepo = getConnection().getRepository(UserEntity);
        try {
          console.log('Delete Address');
    
          const userExist = await userRepo.findOne({ id });
          if (!userExist) {
            console.log('Record doesnot exist');
            return 'Record doesnot exist';
          }
    
          await userRepo.update(
            { id },
            {
              address: '',
              postal_code:'',
              updated_at: new Date(),
            },
          );
          return {
            resultcode: 200,
            data: await userRepo.findOne({ id }),
          };
        } catch (err) {
          console.error('delete address detail Err:', err);
          throw err;
        }
      }
      
}
