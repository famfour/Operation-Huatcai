import * as redis from 'redis';
import { HttpException, HttpStatus } from '@nestjs/common';
import Debug from 'debug';
import { ConfigService } from '../../config/config.service';

const RedisCluster = require('redis-clustr');

const debug = Debug('user-service:redis');

export default class RedisClient {
  private static redisClient;

  private static redisExpiry;

  static init(config: ConfigService) {
    if (!config.envConfig.REDIS_URL || !config.envConfig.REDIS_EXPIRY) {
      // throw new Error('Redis not configured');
      debug('Redis not configured');
      return;
    } else {
      debug('REDIS_URL : ', config.envConfig.REDIS_URL);
      debug('REDIS_EXPIRY : ', config.envConfig.REDIS_EXPIRY);
    }

    try {
      this.redisExpiry = Number(config.envConfig.REDIS_EXPIRY);
      if (
          !config.envConfig.REDIS_CLUSTERING_ENABLED ||
          config.envConfig.REDIS_CLUSTERING_ENABLED !== 'true'
      ) {
        this.redisClient = redis.createClient(config.envConfig.REDIS_URL);
        return;
      }

      this.redisClient = new RedisCluster({
        servers: [
          {
            host: config.envConfig.REDIS_URL,
            port: 6379,
          },
        ],
        createClient(port, host) {
          // this is the default behaviour
          return redis.createClient(port, host);
        },
      });

    } catch(err) {
      console.error('redis init error : ', err);
      return;
    }

    this.redisClient.on("error", function (err) {
      console.error("Error " + err);
    });

  }

  static async get(redisKey: string): Promise<string | null> {
    return new Promise((resolve, reject) => {
      this.redisClient.get(redisKey, (error, data) => {
        if (!data || error) {
          console.log('Redis Retrieving Error:', error);

          error = new HttpException(
            {
              status: HttpStatus.UNAUTHORIZED,
              message: `Redis value is not found for the key ${redisKey}`,
              errorcode: 'RedisClient_KEY_NOT_FOUND_100',
            },
            HttpStatus.UNAUTHORIZED,
          );
          return reject(error);
        }
        return resolve(JSON.parse(data));
      });
    });
  }

  static async set(redisKey: string, data: any): Promise<boolean> {
    const strData = JSON.stringify(data);

    return new Promise((resolve, reject) => {
      this.redisClient.set(
        redisKey,
        strData,
        'PX',
        this.redisExpiry,
        (error) => {
          if (error) {
            console.log('Redis Set Error:', error);
            return reject(error);
          }

          return resolve(true);
        },
      );
    });
  }
}
