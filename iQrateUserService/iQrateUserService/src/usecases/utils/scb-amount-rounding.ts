export function roundAmount(num: number): number {
  // Fix to three decimal places: 2.1249 => 2.124
  const fixedToThreeDecimals = Math.floor(num * 1000) / 1000;

  // Round to two decimal places: 2.129 => 2.13
  const roundToTwoDecimals = Math.round(fixedToThreeDecimals * 100) / 100;

  // Round to five's multiple: 2.16 => 2.15, 2.18 => 2.20
  return (Math.round((roundToTwoDecimals * 100) / 5) * 5) / 100;
}
