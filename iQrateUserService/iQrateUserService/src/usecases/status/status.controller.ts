import { Body, Controller, Param, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { StatusDto } from './status.dto';
import { StatusService } from './status.service';
import {
  Public,
  Roles,
  Unprotected,
} from 'nest-keycloak-connect';
//@UseGuards(AuthGuard("jwt"))
@Controller('auth')
export class StatusController {
  constructor(private readonly statusService: StatusService) { }

  @Post("/:id/status")
  @Roles({ roles: ['user'] })
  public async setStatus(
    @Param('id') id: string,
    @Body() statusDto: StatusDto
  ): Promise<any> {
    console.log("Testing", this.statusService.setStatus(id, statusDto));
    return this.statusService.setStatus(id, statusDto);
  }
}
