import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { UserEntity } from '../../entities/users';
import { getConnection } from 'typeorm';
import { UserDto } from '../user/dto/user.dto';
import { UserService } from '../user/user.service';
import { StatusDto } from './status.dto';

@Injectable()
export class StatusService {
    constructor(
        private readonly usersService: UserService,
      ) { }
      public async setStatus(id: string, statusdto: StatusDto): Promise<UserDto> {
        const userRepo = getConnection().getRepository(UserEntity);
        try {
          const user = await this.usersService.findById(id);
          user.status = statusdto.status;
          
          
           return await userRepo.save(user);
        } catch (err) {
          throw new HttpException(err, HttpStatus.BAD_REQUEST);
        }
      }
}
