import { HttpService, Module,HttpModule } from '@nestjs/common';
import { StatusController } from './status.controller';
import { StatusService } from './status.service';
import { ConfigService } from '../../config/config.service';
import { UserService } from '../user/user.service';
import { AdminService } from '../admin/admin.service';

@Module({imports: [ HttpModule ],  
    providers: [StatusService,ConfigService, UserService, AdminService],
    controllers: [StatusController],
   })
export class StatusModule {}
