import { HttpService } from '@nestjs/axios';
import { ConfigService } from '../../config/config.service';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserService } from '../user/user.service';
import { StatusController } from './status.controller';
import { StatusDto } from './status.dto';
import { StatusService } from './status.service';
import { ConfigModule } from '../../config/config.module';
import { HttpStatus } from '@nestjs/common';

describe('StatusController', () => {
  //let controller: AddressController;
  let statusController:StatusController;
//   const userService = new UserService(new ConfigService(), new HttpService());
  const statusService=new StatusService(new UserService(new ConfigService(),new HttpService()));
  beforeAll(async () => {
    
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRootAsync({
          imports: [ConfigModule],
          useFactory: (config: ConfigService) => config.getTypeORMConfig(),
          inject: [ConfigService],
        }),
      ],
      controllers: [StatusController],
      providers: [StatusService],
    })
    .overrideProvider(StatusService)
    .useValue(statusService)
    .compile();

    statusController = module.get<StatusController>(StatusController);
  });

  it('should be defined', () => {
    expect(statusController).toBeDefined();
  });
  it('Set status', async () => {
    console.log('Set status testing...');
    const dto = {
        status: 1,
        
    };
    const output = await statusController.setStatus('8', dto);

    console.log(output);
    // const obj = JSON.stringify(output);
    // const result = JSON.parse(obj);
    //console.log(result);

    //expect(output.postal_code).toEqual(dto.postal_code);

});
});