import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsEmail, IsInt, IsNotEmpty, Length } from 'class-validator';


export class StatusDto {
    @ApiProperty({
        type: Number,
        description: 'Status',
        default: 'Status',
      })
      @IsNotEmpty({ message: 'Should not be empty' })
      status: number;

     
      
}