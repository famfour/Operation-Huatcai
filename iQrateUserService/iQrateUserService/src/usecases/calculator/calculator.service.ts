import {HttpException, Injectable, HttpStatus} from '@nestjs/common';
import Debug from 'debug';
import DeviceDetector = require('device-detector-js');
import { HttpService } from '@nestjs/axios';
import { ConfigService } from '../../config/config.service';
import { SellersStamp, EquityDto, BuyerStamp, NewPurchase, RefinanceTotal, Refinance, RefinanceReport, NewPurchaseReportDto, NewPurchaseTotalDto, MortgageRepaymentDto, BucMortgageDownpaymentDto, BucMortgageReportDto, GetUnpludgeAmtDto, GetPludgeAmtDto } from './calculator.dto';

const debug = Debug('user-service:CalciService');

@Injectable()
export class CalculatorService {
  constructor(
    private config: ConfigService,
    private readonly http: HttpService,
  ) {}

  // calculator equity
  async equityPlan(bodyParams: EquityDto) {
    try {
      debug('Entered equity plan service');
      const output = await this.http
          // http://13.212.62.173/api/calculator/equity-loan
          .post(`${this.config.envConfig.CALCULATOR_SERVICE_URL}/api/calculator/equity-loan`, {
            existing_housing_loans: bodyParams.existing_housing_loans,
            current_valuation: bodyParams.current_valuation,
            total_cpf: bodyParams.total_cpf,
            outstanding_loan: bodyParams.outstanding_loan,
            loan_tagged: bodyParams.loan_tagged,
          }).toPromise();
      debug(output.data);
      return output.data;
    } catch (err) {
      if(err && err.response && err.response.status>=400 && err.response.status<500) {
        throw new HttpException(err.response.data,err.response.status);
      } else {
        console.error('equity calci err: ', err);
        throw err;
      }
    }
  }

  // seller stamp duty
  async sellerStampDuty(bodyParams: SellersStamp) {
    try {
      debug('Entered seller stamp duty service');
      const output = await this.http
          // http://13.212.62.173/api/calculator/seller-stamp-duty
          .post(`${this.config.envConfig.CALCULATOR_SERVICE_URL}/api/calculator/seller-stamp-duty`, {
            selling_price: bodyParams.selling_price,
            date: bodyParams.date,
          }).toPromise();
      debug(output.data);
      return output.data;
    } catch (err) {
      if(err && err.response && err.response.status>=400 && err.response.status<500) {
        throw new HttpException(err.response.data,err.response.status);
      } else {
        console.error('seller stamp err : ', err);
        throw err;
      }
    }
  }

  // buyer stamp duty
  async buyerStampDuty(bodyParams: BuyerStamp) {
    try {
      debug('Entered buyer stamp duty service');
      const output = await this.http
          // http://13.212.62.173/api/calculator/seller-stamp-duty
          .post(`${this.config.envConfig.CALCULATOR_SERVICE_URL}/api/calculator/buyer-stamp-duty`, {
            purchase_price: bodyParams.purchase_price,
            loan_applicant: bodyParams.loan_applicant,
            applicant1_property: bodyParams.applicant1_property,
            applicant1_nationality: bodyParams.applicant1_nationality,
            applicant2_property: bodyParams.applicant2_property,
            applicant2_nationality: bodyParams.applicant2_nationality,
            applicant3_property: bodyParams.applicant3_property,
            applicant3_nationality: bodyParams.applicant3_nationality,
            applicant4_property: bodyParams.applicant4_property,
            applicant4_nationality: bodyParams.applicant4_nationality,
          }).toPromise();
      debug(output.data);
      return output.data;
    } catch (err) {
      if(err && err.response && err.response.status>=400 && err.response.status<500) {
        throw new HttpException(err.response.data,err.response.status);
      } else {
        console.error('seller stamp err : ', err);
        throw err;
      }
    }
  }

  // new purchase
  async newPurchase(bodyParams: NewPurchase) {
    try {
      debug('Entered new purchase service');
      const output = await this.http
          // http://13.212.62.173/api/calculator/seller-stamp-duty
          .post(`${this.config.envConfig.CALCULATOR_SERVICE_URL}/api/calculator/new-purchase`, {
            property_price: bodyParams.property_price,
            loan_amount: bodyParams.loan_amount,
            dob: bodyParams.dob,
            extend_loan_tenure: bodyParams.extend_loan_tenure,
            property_type: bodyParams.property_type,
            no_of_loan_applicants: bodyParams.no_of_loan_applicants,
            monthly_fixed_income: bodyParams.monthly_fixed_income,
            total_house_installment: bodyParams.total_house_installment,
            total_car_installment: bodyParams.total_car_installment,
            total_personal_installment: bodyParams.total_personal_installment,
            total_credit_installment: bodyParams.total_credit_installment,
            total_property_installment: bodyParams.total_property_installment,
            total_property_company_installment: bodyParams.total_property_company_installment,
            annual_income: bodyParams.annual_income,
            monthly_rental_income: bodyParams.monthly_rental_income,
            total_gurantor_installment: bodyParams.total_gurantor_installment,
            no_of_housing_loan: bodyParams.no_of_housing_loan,
            no_of_own_properties: bodyParams.no_of_own_properties,
            nationality: bodyParams.nationality,
            prefered_loan_tenure: bodyParams.prefered_loan_tenure,
            loan_category: bodyParams.loan_category,
            dob_list: bodyParams.dob_list,
            employement_type: bodyParams.employement_type,
          }).toPromise();
      debug(output.data);
      return output.data;
    } catch (err) {
      if(err && err.response && err.response.status>=400 && err.response.status<500) {
        throw new HttpException(err.response.data,err.response.status);
      } else {
        console.error('new purchase err : ', err);
        throw err;
      }
    }
  }

  // refinanceTotal
  async refinanceTotal(bodyParams: RefinanceTotal) {
    try {
      debug('Entered RefinanceTotal service');
      const output = await this.http
          // http://13.212.62.173/api/calculator/seller-stamp-duty
          .post(`${this.config.envConfig.CALCULATOR_SERVICE_URL}/api/calculator/refinance-total`, {
            no_of_loan_applicants: bodyParams.no_of_loan_applicants,
            annual_income: bodyParams.annual_income,
            monthly_fixed_income: bodyParams.monthly_fixed_income,
            monthly_rental_income: bodyParams.monthly_rental_income,
            token: bodyParams.token,
            employement_type: bodyParams.employement_type,
            dob_list: bodyParams.dob_list,
          }).toPromise();
      debug(output.data);
      return output.data;
    } catch (err) {
      if(err && err.response && err.response.status>=400 && err.response.status<500) {
        throw new HttpException(err.response.data,err.response.status);
      } else {
        console.error('RefinanceTotal err : ', err);
        throw err;
      }
    }
  }

  // refinanceTotal
  async refinance(bodyParams: Refinance) {
    try {
      debug('Entered Refinance service');
      const output = await this.http
          // http://13.212.62.173/api/calculator/seller-stamp-duty
          .post(`${this.config.envConfig.CALCULATOR_SERVICE_URL}/api/calculator/refinance`, {
            no_of_loan_applicants: bodyParams.no_of_loan_applicants,
            outstanding_loan_amount: bodyParams.outstanding_loan_amount,
            early_repayment_penalty: bodyParams.early_repayment_penalty,
            undisbursed_loan_amount: bodyParams.undisbursed_loan_amount,
            cancellation_penalty: bodyParams.cancellation_penalty,
          }).toPromise();
      debug(output.data);
      return output.data;
    } catch (err) {
      if(err && err.response && err.response.status>=400 && err.response.status<500) {
        throw new HttpException(err.response.data,err.response.status);
      } else {
        console.error('Refinance err : ', err);
        throw err;
      }
    }
  }

  // refinance Report
  async refinanceReport(bodyParams: RefinanceReport) {
    try {
      debug('Entered Refinance Report service');
      const output = await this.http
          // http://13.212.62.173/api/calculator/seller-stamp-duty
          .post(`${this.config.envConfig.CALCULATOR_SERVICE_URL}/api/calculator/refinance-report`, {
            no_of_loan_applicants: bodyParams.no_of_loan_applicants,
            name: bodyParams.name,
            outstanding_loan_amount: bodyParams.outstanding_loan_amount,
            outstanding_loan_tenure: bodyParams.outstanding_loan_tenure,
            prefered_loan_tenure: bodyParams.prefered_loan_tenure,
            existing_bank: bodyParams.existing_bank,
            existing_interest_rate: bodyParams.existing_interest_rate,
            property_type: bodyParams.property_type,
            year_of_purchase: bodyParams.year_of_purchase,
            early_repayment_penalty_amount: bodyParams.early_repayment_penalty_amount,
            cancellation_penalty_amount: bodyParams.cancellation_penalty_amount,
            existing_loan_subsidy: bodyParams.existing_loan_subsidy,
            dob: bodyParams.dob,
            loan_category: bodyParams.loan_category,
            bank_details: bodyParams.bank_details,
            token: bodyParams.token,
            cash_rebate_subsidy: bodyParams.cash_rebate_subsidy,
            approximate_property_price: bodyParams.approximate_property_price,
            weighted_age: bodyParams.weighted_age,
            property_status: bodyParams.property_status,
          }).toPromise();
      debug(output.data);
      return output.data;
    } catch (err) {
      if(err && err.response && err.response.status>=400 && err.response.status<500) {
        throw new HttpException(err.response.data,err.response.status);
      } else {
        console.error('refinanceReport err : ', err);
        throw err;
      }
    }
  }

  // refinance Pdf
  async refinancePdf(bodyParams: RefinanceReport, res) {
    try {
      debug('Entered Refinance Pdf service');
      const a = bodyParams.export_fields;
      const arrayLen = a.length;
      debug('lengt:', arrayLen);
      if(arrayLen === 0){
        throw new HttpException({
          name : 'EXPORT_FIELD_EMPTY',
          message : 'Please select atleast one checkbox'
        },HttpStatus.BAD_REQUEST);
      }
      const output = await this.http
          // http://13.212.62.173/api/calculator/seller-stamp-duty
          .post(`${this.config.envConfig.CALCULATOR_SERVICE_URL}/api/calculator/refinance-pdf`, {
            no_of_loan_applicants: bodyParams.no_of_loan_applicants,
            name: bodyParams.name,
            outstanding_loan_amount: bodyParams.outstanding_loan_amount,
            outstanding_loan_tenure: bodyParams.outstanding_loan_tenure,
            prefered_loan_tenure: bodyParams.prefered_loan_tenure,
            existing_bank: bodyParams.existing_bank,
            existing_interest_rate: bodyParams.existing_interest_rate,
            property_type: bodyParams.property_type,
            year_of_purchase: bodyParams.year_of_purchase,
            early_repayment_penalty_amount: bodyParams.early_repayment_penalty_amount,
            cancellation_penalty_amount: bodyParams.cancellation_penalty_amount,
            existing_loan_subsidy: bodyParams.existing_loan_subsidy,
            dob: bodyParams.dob,
            loan_category: bodyParams.loan_category,
            bank_details: bodyParams.bank_details,
            token: bodyParams.token,
            export_fields: bodyParams.export_fields,
            cash_rebate_subsidy: bodyParams.cash_rebate_subsidy,
            approximate_property_price: bodyParams.approximate_property_price,
            weighted_age: bodyParams.weighted_age,
            property_status: bodyParams.property_status,
          }).toPromise();
      debug(output.data);
      // return output.data;
      res.send({ url: output.data});
    } catch (err) {
      if(err && err.response && err.response.status>=400 && err.response.status<500) {
        throw new HttpException(err.response.data,err.response.status);
      } else {
        console.error('refinanceReport err : ', err);
        throw err;
      }
    }
  }

  // NewPurchaseReport
  async NewPurchaseReport(bodyParams: NewPurchaseReportDto) {
    try {
      debug('NewPurchase Report service');
      const output = await this.http
          // http://13.212.62.173/api/calculator/seller-stamp-duty
          .post(`${this.config.envConfig.CALCULATOR_SERVICE_URL}/api/calculator/new-purchase-report`, {
            no_of_loan_applicants: bodyParams.no_of_loan_applicants,
            name: bodyParams.name,
            property_price: bodyParams.property_price,
            no_of_housing_loan: bodyParams.no_of_housing_loan,
            ltv: bodyParams.ltv,
            loan_amount: bodyParams.loan_amount,
            buyer_stamp_duty: bodyParams.buyer_stamp_duty,
            additional_buyer_stamp_duty: bodyParams.additional_buyer_stamp_duty,
            legal_fees: bodyParams.legal_fees,
            valuation_fees: bodyParams.valuation_fees,
            other_fees: bodyParams.other_fees,
            extend_loan_tenure: bodyParams.extend_loan_tenure,
            maximum_loan_tenure: bodyParams.maximum_loan_tenure,
            maximum_loan_qualified_mlt: bodyParams.maximum_loan_qualified_mlt,
            maximum_price_mlt: bodyParams.maximum_price_mlt,
            msr_mlt: bodyParams.msr_mlt,
            tdsr_mlt: bodyParams.tdsr_mlt,
            full_pludge_mlt: bodyParams.full_pludge_mlt,
            full_unpludge_mlt: bodyParams.full_unpludge_mlt,
            prefer_pludge_mlt: bodyParams.prefer_pludge_mlt,
            calculatiomn_unpludge_mlt: bodyParams.calculatiomn_unpludge_mlt,
            prefer_unpludge_mlt: bodyParams.prefer_unpludge_mlt,
            calculation_pludge_mlt: bodyParams.calculation_pludge_mlt,
            preferrd_loan_tenure: bodyParams.preferrd_loan_tenure,
            maximum_loan_qualified_plt: bodyParams.maximum_loan_qualified_plt,
            maximum_price_plt: bodyParams.maximum_price_plt,
            msr_plt: bodyParams.msr_plt,
            tdsr_plt: bodyParams.tdsr_plt,
            full_pludge_plt: bodyParams.full_pludge_plt,
            full_unpludge_plt: bodyParams.full_unpludge_plt,
            prefer_pludge_plt: bodyParams.prefer_pludge_plt,
            calculatiomn_unpludge_plt: bodyParams.calculatiomn_unpludge_plt,
            prefer_unpludge_plt: bodyParams.prefer_unpludge_plt,
            calculation_pludge_plt: bodyParams.calculation_pludge_plt,
            token: bodyParams.token,
            monthly_installment_plt: bodyParams.monthly_installment_plt,
            monthly_installment_mlt: bodyParams.monthly_installment_mlt,
            property_type: bodyParams.property_type,
            
          }).toPromise();
      debug(output.data);
      return output.data;
    } catch (err) {
      if(err && err.response && err.response.status>=400 && err.response.status<500) {
        throw new HttpException(err.response.data,err.response.status);
      } else {
        console.error('NewPurchaseReport err : ', err);
        throw err;
      }
    }
  }

  // NewPurchaseReport PDF
  async NewPurchaseReportPdf(bodyParams: NewPurchaseReportDto, res) {
    try {
      debug('NewPurchase Report PDF service');
      const a = bodyParams.export_fields;
      const arrayLen = a.length;
      debug('lengt:', arrayLen);
      if(arrayLen === 0){
        throw new HttpException({
          name : 'EXPORT_FIELD_EMPTY',
          message : 'Please select atleast one checkbox'
        },HttpStatus.BAD_REQUEST);
      }

      const output = await this.http
          // http://13.212.62.173/api/calculator/seller-stamp-duty
          .post(`${this.config.envConfig.CALCULATOR_SERVICE_URL}/api/calculator/new-purchase-pdf`, {
            no_of_loan_applicants: bodyParams.no_of_loan_applicants,
            name: bodyParams.name,
            property_price: bodyParams.property_price,
            no_of_housing_loan: bodyParams.no_of_housing_loan,
            ltv: bodyParams.ltv,
            loan_amount: bodyParams.loan_amount,
            buyer_stamp_duty: bodyParams.buyer_stamp_duty,
            additional_buyer_stamp_duty: bodyParams.additional_buyer_stamp_duty,
            legal_fees: bodyParams.legal_fees,
            valuation_fees: bodyParams.valuation_fees,
            other_fees: bodyParams.other_fees,
            extend_loan_tenure: bodyParams.extend_loan_tenure,
            maximum_loan_tenure: bodyParams.maximum_loan_tenure,
            maximum_loan_qualified_mlt: bodyParams.maximum_loan_qualified_mlt,
            maximum_price_mlt: bodyParams.maximum_price_mlt,
            msr_mlt: bodyParams.msr_mlt,
            tdsr_mlt: bodyParams.tdsr_mlt,
            full_pludge_mlt: bodyParams.full_pludge_mlt,
            full_unpludge_mlt: bodyParams.full_unpludge_mlt,
            prefer_pludge_mlt: bodyParams.prefer_pludge_mlt,
            calculatiomn_unpludge_mlt: bodyParams.calculatiomn_unpludge_mlt,
            prefer_unpludge_mlt: bodyParams.prefer_unpludge_mlt,
            calculation_pludge_mlt: bodyParams.calculation_pludge_mlt,
            preferrd_loan_tenure: bodyParams.preferrd_loan_tenure,
            maximum_loan_qualified_plt: bodyParams.maximum_loan_qualified_plt,
            maximum_price_plt: bodyParams.maximum_price_plt,
            msr_plt: bodyParams.msr_plt,
            tdsr_plt: bodyParams.tdsr_plt,
            full_pludge_plt: bodyParams.full_pludge_plt,
            full_unpludge_plt: bodyParams.full_unpludge_plt,
            prefer_pludge_plt: bodyParams.prefer_pludge_plt,
            calculatiomn_unpludge_plt: bodyParams.calculatiomn_unpludge_plt,
            prefer_unpludge_plt: bodyParams.prefer_unpludge_plt,
            calculation_pludge_plt: bodyParams.calculation_pludge_plt,
            token: bodyParams.token,
            export_fields: bodyParams.export_fields,
            monthly_installment_mlt: bodyParams.monthly_installment_mlt,
            monthly_installment_plt: bodyParams.monthly_installment_plt,
            property_type: bodyParams.property_type,
          }).toPromise();
      debug(output.data);
      // return output.data;
      res.send({ url: output.data});
    } catch (err) {
      if(err && err.response && err.response.status>=400 && err.response.status<500) {
        throw new HttpException(err.response.data,err.response.status);
      } else {
        console.error('NewPurchaseReport err : ', err);
        throw err;
      }
    }
  }

  // NewPurchaseTotal api
  async NewPurchaseTotal(bodyParams: NewPurchaseTotalDto) {
    try {
      debug('Entered Refinance Report service');
      const output = await this.http
          // http://13.212.62.173/api/calculator/seller-stamp-duty
          .post(`${this.config.envConfig.CALCULATOR_SERVICE_URL}/api/calculator/new-purchase-total`, {
            no_of_loan_applicants: bodyParams.no_of_loan_applicants,
            total_property_installment: bodyParams.total_property_installment,
            total_gurantor_installment: bodyParams.total_gurantor_installment,
            total_property_company_installment: bodyParams.total_property_company_installment,
            total_house_installment: bodyParams.total_house_installment,
            total_personal_installment: bodyParams.total_personal_installment,
            total_credit_installment: bodyParams.total_credit_installment,
            annual_income: bodyParams.annual_income,
            monthly_fixed_income: bodyParams.monthly_fixed_income,
            monthly_rental_income: bodyParams.monthly_rental_income,
            total_car_installment: bodyParams.total_car_installment,
            token: bodyParams.token,
            employement_type: bodyParams.employement_type,
          }).toPromise();
      debug(output.data);
      return output.data;
    } catch (err) {
      if(err && err.response && err.response.status>=400 && err.response.status<500) {
        throw new HttpException(err.response.data,err.response.status);
      } else {
        console.error('NewPurchaseTotal err : ', err);
        throw err;
      }
    }
  }

  // Mortage Repayment
  async mortgageRepayment(bodyParams: MortgageRepaymentDto) {
    try {
      debug('Entered Refinance Report service');
      if(bodyParams.prefered_rate2_year1 !== null || bodyParams.prefered_rate2_year2 !== null || bodyParams.prefered_rate2_year3 !== null || bodyParams.prefered_rate2_year4 !== null || bodyParams.prefered_rate2_year5 !== null) {
        if(bodyParams.prefered_rate2_year1 === null || bodyParams.prefered_rate2_year2 === null || bodyParams.prefered_rate2_year3 === null || bodyParams.prefered_rate2_year4 === null || bodyParams.prefered_rate2_year5 === null) {
          throw new HttpException({
            name : 'ALL_MUST',
            message : 'All field should be must'
          },HttpStatus.BAD_REQUEST);
        }
      }
      const output = await this.http
          // http://13.212.62.173/api/calculator/seller-stamp-duty
          .post(`${this.config.envConfig.CALCULATOR_SERVICE_URL}/api/calculator/mortgage-repayment`, {
            name: bodyParams.name,
            loan_amount: bodyParams.loan_amount,
            prefered_rate1_year1: bodyParams.prefered_rate1_year1,
            prefered_rate1_year2: bodyParams.prefered_rate1_year2,
            prefered_rate1_year3: bodyParams.prefered_rate1_year3,
            prefered_rate1_year4: bodyParams.prefered_rate1_year4,
            prefered_rate1_year5: bodyParams.prefered_rate1_year5,
            prefered_rate2_year1: bodyParams.prefered_rate2_year1,
            prefered_rate2_year2: bodyParams.prefered_rate2_year2,
            prefered_rate2_year3: bodyParams.prefered_rate2_year3,
            prefered_rate2_year4: bodyParams.prefered_rate2_year4,
            prefered_rate2_year5: bodyParams.prefered_rate2_year5,
            loan_tenure: bodyParams.loan_tenure,
            prefered_rate2_enable: bodyParams.prefered_rate2_enable,
          }).toPromise();
      debug(output.data);
      return output.data;
    } catch (err) {
      if(err && err.response && err.response.status>=400 && err.response.status<500) {
        throw new HttpException(err.response.data,err.response.status);
      } else {
        console.error('mortgageRepayment err : ', err);
        throw err;
      }
    }
  }

  // Mortage Repayment PDF
  async mortgageRepaymentPdf(bodyParams: MortgageRepaymentDto, res) {
    try {
      debug('Entered mortgage repayment pdf service');
      if(bodyParams.prefered_rate2_year1 !== null || bodyParams.prefered_rate2_year2 !== null || bodyParams.prefered_rate2_year3 !== null || bodyParams.prefered_rate2_year4 !== null || bodyParams.prefered_rate2_year5 !== null) {
        if(bodyParams.prefered_rate2_year1 === null || bodyParams.prefered_rate2_year2 === null || bodyParams.prefered_rate2_year3 === null || bodyParams.prefered_rate2_year4 === null || bodyParams.prefered_rate2_year5 === null) {
          throw new HttpException({
            name : 'ALL_MUST',
            message : 'All field should be must'
          },HttpStatus.BAD_REQUEST);
        }
      }
      
      const a = bodyParams.export_fields;
      const arrayLen = a.length;
      debug('lengt:', arrayLen);
      if(arrayLen === 0){
        throw new HttpException({
          name : 'EXPORT_FIELD_EMPTY',
          message : 'Please select atleast one checkbox'
        },HttpStatus.BAD_REQUEST);
      }
      const output = await this.http
          // http://13.212.62.173/api/calculator/seller-stamp-duty
          .post(`${this.config.envConfig.CALCULATOR_SERVICE_URL}/api/calculator/mortgage-repayment-pdf`, {
            name: bodyParams.name,
            loan_amount: bodyParams.loan_amount,
            prefered_rate1_year1: bodyParams.prefered_rate1_year1,
            prefered_rate1_year2: bodyParams.prefered_rate1_year2,
            prefered_rate1_year3: bodyParams.prefered_rate1_year3,
            prefered_rate1_year4: bodyParams.prefered_rate1_year4,
            prefered_rate1_year5: bodyParams.prefered_rate1_year5,
            prefered_rate2_year1: bodyParams.prefered_rate2_year1,
            prefered_rate2_year2: bodyParams.prefered_rate2_year2,
            prefered_rate2_year3: bodyParams.prefered_rate2_year3,
            prefered_rate2_year4: bodyParams.prefered_rate2_year4,
            prefered_rate2_year5: bodyParams.prefered_rate2_year5,
            loan_tenure: bodyParams.loan_tenure,
            prefered_rate2_enable: bodyParams.prefered_rate2_enable,
            export_fields: bodyParams.export_fields,
          }).toPromise();
      debug(output.data);
      res.send({ url: output.data}); 
      //res.send(output.data);
    } catch (err) {
      if(err && err.response && err.response.status>=400 && err.response.status<500) {
        throw new HttpException(err.response.data,err.response.status);
      } else {
        console.error('mortgageRepayment err : ', err);
        throw err;
      }
    }
  }

  // buc-mortgage-downpayment
  async bucMortgageDownpayment(bodyParams: BucMortgageDownpaymentDto) {
    try {
      debug('Entered Buc Mortgage Downpayment service');
      const output = await this.http
          // http://13.212.62.173/api/calculator/seller-stamp-duty
          .post(`${this.config.envConfig.CALCULATOR_SERVICE_URL}/api/calculator/buc-mortgage-downpayment`, {
            property_price: bodyParams.property_price,
            ltv: bodyParams.ltv,
            loan_tenure: bodyParams.loan_tenure,
            cpf_available_amount: bodyParams.cpf_available_amount,
          }).toPromise();
      debug(output.data);
      return output.data;
    } catch (err) {
      if(err && err.response && err.response.status>=400 && err.response.status<500) {
        throw new HttpException(err.response.data,err.response.status);
      } else {
        console.error('bucMortgageDownpayment err : ', err);
        throw err;
      }
    }
  }
  
  // buc-mortgage-downpayment
  async bucMortgageReport(bodyParams: BucMortgageReportDto) {
    try {
      debug('Entered Buc Mortgage Report service');
      const output = await this.http
          // http://13.212.62.173/api/calculator/seller-stamp-duty
          .post(`${this.config.envConfig.CALCULATOR_SERVICE_URL}/api/calculator/buc-mortgage-report`, {
            name: bodyParams.name,
            property_price: bodyParams.property_price,
            ltv: bodyParams.ltv,
            // cpf_available: bodyParams.cpf_available,
            cpf_available_amount: bodyParams.cpf_available_amount,
            downpayment: bodyParams.downpayment,
            loan_amount: bodyParams.loan_amount,
            loan_tenure: bodyParams.loan_tenure,
            prefered_rate_enable: bodyParams.prefered_rate_enable,
            prefered_rate_year1: bodyParams.prefered_rate_year1,
            prefered_rate_year2: bodyParams.prefered_rate_year2,
            prefered_rate_year3: bodyParams.prefered_rate_year3,
            prefered_rate_year4: bodyParams.prefered_rate_year4,
            prefered_rate_year5: bodyParams.prefered_rate_year5,
            token: bodyParams.token,
            disbursement_month: bodyParams.disbursement_month,
          }).toPromise();
      debug(output.data);
      return output.data;
    } catch (err) {
      if(err && err.response && err.response.status>=400 && err.response.status<500) {
        throw new HttpException(err.response.data,err.response.status);
      } else {
        console.error('bucMortgageReport err : ', err);
        throw err;
      }
    }
  }

  // buc-mortgage-downpayment-pdf
  async bucMortgageReportPdf(bodyParams: BucMortgageReportDto, res) {
    try {
      debug('Entered Buc Mortgage Report Pdf service');
      const a = bodyParams.export_fields;
      const arrayLen = a.length;
      debug('lengt:', arrayLen);
      if(arrayLen === 0){
        throw new HttpException({
          name : 'EXPORT_FIELD_EMPTY',
          message : 'Please select atleast one checkbox'
        },HttpStatus.BAD_REQUEST);
      }
      const output = await this.http
          // http://13.212.62.173/api/calculator/seller-stamp-duty
          .post(`${this.config.envConfig.CALCULATOR_SERVICE_URL}/api/calculator/buc-mortgage-report-pdf`, {
            name: bodyParams.name,
            property_price: bodyParams.property_price,
            ltv: bodyParams.ltv,
            // cpf_available: bodyParams.cpf_available,
            cpf_available_amount: bodyParams.cpf_available_amount,
            downpayment: bodyParams.downpayment,
            loan_amount: bodyParams.loan_amount,
            loan_tenure: bodyParams.loan_tenure,
            prefered_rate_enable: bodyParams.prefered_rate_enable,
            prefered_rate_year1: bodyParams.prefered_rate_year1,
            prefered_rate_year2: bodyParams.prefered_rate_year2,
            prefered_rate_year3: bodyParams.prefered_rate_year3,
            prefered_rate_year4: bodyParams.prefered_rate_year4,
            prefered_rate_year5: bodyParams.prefered_rate_year5,
            token: bodyParams.token,
            export_fields: bodyParams.export_fields,
            disbursement_month: bodyParams.disbursement_month,
          }).toPromise();
      debug(output.data);
      // return output.data;
      // res.send(output.data);
      // res.send({ url: `${output.data}?response-content-disposition=attachment&filename=iqrate-calculator.pdf` }); 
      res.send({ url: output.data});
    } catch (err) {
      if(err && err.response && err.response.status>=400 && err.response.status<500) {
        throw new HttpException(err.response.data,err.response.status);
      } else {
        console.error('bucMortgageReport pdf err : ', err);
        throw err;
      }
    }
  }

  // get Unpludge Amt
  async getUnpludgeAmt(bodyParams: GetUnpludgeAmtDto) {
    try {
      debug('Get Unpludge Amt service');

      const output = await this.http
          // http://13.212.62.173/api/calculator/seller-stamp-duty
          .post(`${this.config.envConfig.CALCULATOR_SERVICE_URL}/api/calculator/get-unfludge-amount`, {
            loan_amount: bodyParams.loan_amount,
            preferred_fludge_amount: bodyParams.preferred_fludge_amount,
            total_monthly_income: bodyParams.total_monthly_income,
            loan_tenure: bodyParams.loan_tenure,
            total_financial_commitments: bodyParams.total_financial_commitments,
            property_type: bodyParams.property_type,
          }).toPromise();
      debug(output.data);
      return output.data;
    } catch (err) {
      if(err && err.response && err.response.status>=400 && err.response.status<500) {
        throw new HttpException(err.response.data,err.response.status);
      } else {
        console.error('get unpludge amt err: ', err);
        throw err;
      }
    }
  }

  // get pludge Amt
  async getPludgeAmt(bodyParams: GetPludgeAmtDto) {
    try {
      debug('Get Pludge Amt service');

      const output = await this.http
          // http://13.212.62.173/api/calculator/seller-stamp-duty
          .post(`${this.config.envConfig.CALCULATOR_SERVICE_URL}/api/calculator/get-fludge-amount`, {
            loan_amount: bodyParams.loan_amount,
            preferred_unfludge_amount: bodyParams.preferred_unfludge_amount,
            total_monthly_income: bodyParams.total_monthly_income,
            loan_tenure: bodyParams.loan_tenure,
            total_financial_commitments: bodyParams.total_financial_commitments,
            property_type: bodyParams.property_type,
          }).toPromise();
      debug(output.data);
      return output.data;
    } catch (err) {
      if(err && err.response && err.response.status>=400 && err.response.status<500) {
        throw new HttpException(err.response.data,err.response.status);
      } else {
        console.error('Get pludge amt err : ', err);
        throw err;
      }
    }
  }

  async downloadPdf(resp) {
    try {
      // step - 1
      /*
      const filePath = '04a460cd-62dc-4872-8674-5d96c5e35098.pdf';
      const bucketName = 'iqrate-loan-files';
      const key = 'submission_files/04a460cd-62dc-4872-8674-5d96c5e35098.pdf';
      const AWS_ACCESS_KEY_ID = this.config.envConfig.AWS_ACCESS_KEY_ID || 'AKIARMFLUTLWW52WJ2CB';
      const AWS_SECRET_ACCESS_KEY = this.config.envConfig.AWS_SECRET_ACCESS_KEY || '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';

      const s3 = new AWS.S3({
        accessKeyId: AWS_ACCESS_KEY_ID,
          secretAccessKey: AWS_SECRET_ACCESS_KEY
        });
      const downloadFile = (filePath: number | fs.PathLike, bucketName: string, key: string) => {
        const params = {
          Bucket: bucketName,
          Key: key
        };
        s3.getObject(params, (err, data) => {
          if (err) console.error(err);
          fs.writeFileSync(filePath, data.Body.toString());
          console.log(`${filePath} has been created!`);
        });
      };
      downloadFile(filePath, bucketName, key);
      */
      // return fs.createReadStream('https://iqrate-images.s3.ap-southeast-1.amazonaws.com/calculator-pdf/contract.pdf');
      
      // res.attachment('https://iqrate-images.s3.ap-southeast-1.amazonaws.com/calculator-pdf/contract.pdf');
      // URL of the image

      return resp.attachment('https://iqrate-images.s3.ap-southeast-1.amazonaws.com/calculator-pdf/contract.pdf');
 
    } catch (err) {
      
    }
  }  
  
} // end of main export class