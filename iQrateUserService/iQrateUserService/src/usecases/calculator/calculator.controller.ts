import {
  Controller,
  Headers,
  Put,
  HttpException,
  HttpStatus,
  Post,
  Body,
  Res,
  Header,
  HttpCode,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiForbiddenResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import Debug from 'debug';
import { createReadStream } from 'fs';
import { SellersStamp, EquityDto, BuyerStamp, NewPurchase, RefinanceTotal, Refinance, RefinanceReport, NewPurchaseReportDto, NewPurchaseTotalDto, MortgageRepaymentDto, BucMortgageDownpaymentDto, BucMortgageReportDto, GetUnpludgeAmtDto, GetPludgeAmtDto } from './calculator.dto';
import { CalculatorService } from './calculator.service';

const debug = Debug('user-service:CalaciController');

@ApiTags('Users/Calculators')
@Controller('user/api/calculator')
export class CalculatorController {
  constructor(private readonly calculatorService: CalculatorService) {}

  // Equity Loan
  @Post('equity-loan')
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Caluculator equity plan' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async equityPlan(
    @Body() defaultVal: EquityDto,
    @Headers() headers: Headers,
  ) {
    const userId = headers['userid'];
    if(!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message:'Token is invalid'
      },HttpStatus.UNAUTHORIZED);
    }
    return this.calculatorService.equityPlan(defaultVal);
  }

  // Seller Stamp Duty
  @Post('seller-stamp-duty')
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Caluculator seller stamp duty' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async sellerStampDuty(
    @Body() defaultVal: SellersStamp,
    @Headers() headers: Headers,
  ) {
    const userId = headers['userid'];
    if(!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message:'Token is invalid'
      },HttpStatus.UNAUTHORIZED);
    }
    //  debug("Entering Register controller...");
    return this.calculatorService.sellerStampDuty(defaultVal);
  }

  // buyer Stamp Duty
  @Post('buyer-stamp-duty')
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Caluculator buyer stamp duty' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async buyerStampDuty(
    @Body() defaultVal: BuyerStamp,
    @Headers() headers: Headers,
  ) {
    const userId = headers['userid'];
    if(!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message:'Token is invalid'
      },HttpStatus.UNAUTHORIZED);
    }
    //  debug("Entering Register controller...");
    return this.calculatorService.buyerStampDuty(defaultVal);
  }

  // new purchase
  @Post('new-purchase')
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Caluculator new purchase' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async newPurchase(
    @Body() defaultVal: NewPurchase,
    @Headers() headers: Headers,
  ) {
    const userId = headers['userid'];
    if(!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message:'Token is invalid'
      },HttpStatus.UNAUTHORIZED);
    }
    //  debug("Entering Register controller...");
    return this.calculatorService.newPurchase(defaultVal);
  }

  // refinance-total
  @Post('refinance-total')
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Caluculator refinance total' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async refinanceTotal(
    @Body() defaultVal: RefinanceTotal,
    @Headers() headers: Headers,
  ) {
    const userId = headers['userid'];
    if(!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message:'Token is invalid'
      },HttpStatus.UNAUTHORIZED);
    }
    //  debug("Entering Register controller...");
    return this.calculatorService.refinanceTotal(defaultVal);
  }

  // refinance
  @Post('refinance')
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Caluculator refinance' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async refinance(
    @Body() defaultVal: Refinance,
    @Headers() headers: Headers,
  ) {
    const userId = headers['userid'];
    if(!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message:'Token is invalid'
      },HttpStatus.UNAUTHORIZED);
    }
    //  debug("Entering Register controller...");
    return this.calculatorService.refinance(defaultVal);
  }

  // refinance report
  @Post('refinance-report')
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Caluculator refinance report' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async refinanceReport(
    @Body() defaultVal: RefinanceReport,
    @Headers() headers: Headers,
  ) {
    const userId = headers['userid'];
    if(!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message:'Token is invalid'
      },HttpStatus.UNAUTHORIZED);
    }
    //  debug("Entering Register controller...");
    return this.calculatorService.refinanceReport(defaultVal);
  }

  // refinance report ped
  @Post('refinance-pdf')
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Caluculator refinance pdf' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async refinancePdf(@Res() res,
    @Body() defaultVal: RefinanceReport,
    @Headers() headers: Headers,
  ) {
    const userId = headers['userid'];
    if(!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message:'Token is invalid'
      },HttpStatus.UNAUTHORIZED);
    }
    //  debug("Entering Register controller...");
    return this.calculatorService.refinancePdf(defaultVal, res);
  }

  // new purchase report
  @Post('new-purchase-report')
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Caluculator new purchase report' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async NewPurchaseReport(
    @Body() defaultVal: NewPurchaseReportDto,
    @Headers() headers: Headers,
  ) {
    const userId = headers['userid'];
    if(!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message:'Token is invalid'
      },HttpStatus.UNAUTHORIZED);
    }
    //  debug("Entering Register controller...");
    return this.calculatorService.NewPurchaseReport(defaultVal);
  }

  // new purchase report
  @Post('new-purchase-report-pdf')
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Caluculator new purchase report pdf' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async NewPurchaseReportPdf(@Res() res,
    @Body() defaultVal: NewPurchaseReportDto,
    @Headers() headers: Headers,
  ) {
    const userId = headers['userid'];
    if(!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message:'Token is invalid'
      },HttpStatus.UNAUTHORIZED);
    }
    //  debug("Entering Register controller...");
    return this.calculatorService.NewPurchaseReportPdf(defaultVal, res);
  }

  // new purchase total
  @Post('new-purchase-total')
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Caluculator new purchase total' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async NewPurchaseTotal(
    @Body() defaultVal: NewPurchaseTotalDto,
    @Headers() headers: Headers,
  ) {
    const userId = headers['userid'];
    if(!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message:'Token is invalid'
      },HttpStatus.UNAUTHORIZED);
    }
    //  debug("Entering Register controller...");
    return this.calculatorService.NewPurchaseTotal(defaultVal);
  }

  // mortgage-repayment
  @Post('mortgage-repayment')
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Caluculator mortgage repayment' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async mortgageRepayment(
    @Body() defaultVal: MortgageRepaymentDto,
    @Headers() headers: Headers,
  ) {
    const userId = headers['userid'];
    if(!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message:'Token is invalid'
      },HttpStatus.UNAUTHORIZED);
    }
    //  debug("Entering Register controller...");
    return this.calculatorService.mortgageRepayment(defaultVal);
  }

  // mortgage-repayment-pdf
  @Post('mortgage-repayment-pdf')
  @ApiBearerAuth()
  // @Header('Content-Disposition', 'attachment; filename=test.pdf')
  @ApiOkResponse({ description: 'Caluculator mortgage repayment pdf' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async mortgageRepaymentPdf(@Res() res,
    @Body() defaultVal: MortgageRepaymentDto,
    @Headers() headers: Headers,
  ) {
    const userId = headers['userid'];
    if(!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message:'Token is invalid'
      },HttpStatus.UNAUTHORIZED);
    }
  return this.calculatorService.mortgageRepaymentPdf(defaultVal, res);
  }

  // buc-mortgage-downpayment
  @Post('buc-mortgage-downpayment')
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Buc Mortgage Downpayment' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async bucMortgageDownpayment(
    @Body() defaultVal: BucMortgageDownpaymentDto,
    @Headers() headers: Headers,
  ) {
    const userId = headers['userid'];
    if(!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message:'Token is invalid'
      },HttpStatus.UNAUTHORIZED);
    }
    //  debug("Entering Register controller...");
    return this.calculatorService.bucMortgageDownpayment(defaultVal);
  }

  // buc-mortgage-report
  @Post('buc-mortgage-report')
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Buc Mortgage Report' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async bucMortgageReport(
    @Body() defaultVal: BucMortgageReportDto,
    @Headers() headers: Headers,
  ) {
    const userId = headers['userid'];
    if(!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message:'Token is invalid'
      },HttpStatus.UNAUTHORIZED);
    }
    //  debug("Entering Register controller...");
    return this.calculatorService.bucMortgageReport(defaultVal);
  }

  // buc-mortgage-report
  @Post('buc-mortgage-report-pdf')
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Buc Mortgage Report PDF' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async bucMortgageReportPdf(@Res() res,
    @Body() defaultVal: BucMortgageReportDto,
    @Headers() headers: Headers,
  ) {
    const userId = headers['userid'];
    if(!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message:'Token is invalid'
      },HttpStatus.UNAUTHORIZED);
    }
    //  debug("Entering Register controller...");
    return this.calculatorService.bucMortgageReportPdf(defaultVal, res);
  }

  // get-unpludge-amount
  @Post('get-unpledge-amount')
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'get-unpledge-amount' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async getUnpludgeAmt(
    @Body() defaultVal: GetUnpludgeAmtDto,
    @Headers() headers: Headers,
  ) {
    const userId = headers['userid'];
    if(!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message:'Token is invalid'
      },HttpStatus.UNAUTHORIZED);
    }
    //  debug("Entering Register controller...");
    return this.calculatorService.getUnpludgeAmt(defaultVal);
  }

  // get-unpludge-amount
  @Post('get-pledge-amount')
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'get-pledge-amount' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async getPludgeAmt(
    @Body() defaultVal: GetPludgeAmtDto,
    @Headers() headers: Headers,
  ) {
    const userId = headers['userid'];
    if(!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message:'Token is invalid'
      },HttpStatus.UNAUTHORIZED);
    }
    //  debug("Entering Register controller...");
    return this.calculatorService.getPludgeAmt(defaultVal);
  }

//   @Post('downloadPdf')
//   @ApiBearerAuth()
//   @Header('Content-Type', 'application/pdf')
// @Header('Content-Disposition', 'attachment; filename=test.pdf')
//   async downloadPdf(@Res() res: any, 
//     @Headers() headers: Headers,
//   ) {
//     const userId = headers['userid'];
//       //  debug("Entering Register controller...");
//     return this.calculatorService.downloadPdf(res);
//   }
} // Main End