import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { CalculatorController } from './calculator.controller';
import { CalculatorService } from './calculator.service';
import { ConfigModule } from '../../config/config.module';
// import { keycloakModule } from '../login/login.keycloak.module';

// keycloakModule

@Module({
  imports: [ConfigModule, HttpModule],
  controllers: [CalculatorController],
  providers: [CalculatorService],
  // exports: [UserService],
})
export class CalculatorModule { }
