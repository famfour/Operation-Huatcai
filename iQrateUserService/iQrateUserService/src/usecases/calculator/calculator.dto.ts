import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsInt, IsNotEmpty, Max, Min } from 'class-validator';

export enum YesNoEnum {
  YES = 'yes',
  NO = 'no',
}

export class EquityDto {
  @ApiProperty({
    type: Number,
    description: 'Existing housing loan',
    default: 2,
  })
  @IsNotEmpty({ message: 'Existing housing loan should not be empty' })
  existing_housing_loans: number;
  
  @ApiProperty({
    type: Number,
    description: 'current valuation',
    default: 1000000,
  })
  @IsNotEmpty({ message: 'current valuation should not be empty' })
  current_valuation: number;

  @ApiProperty({
    type: Number,
    description: 'total cpf',
    default: null,
  })
  // @IsNotEmpty({ message: 'total cpf should not be empty' })
  total_cpf?: number;

  @ApiProperty({
    type: Number,
    description: 'outstanding loan',
    default: null,
  })
  // @IsNotEmpty({ message: 'outstanding loan should not be empty' })
  outstanding_loan?: number;

  @ApiProperty({
    type: String,
    description: 'loan tagged',
    default: "",
  })
  loan_tagged?: string;
}

export class SellersStamp {
  @ApiProperty({
    type: Number,
    description: 'selling price',
    default: 1000000,
  })
  @IsNotEmpty({ message: 'selling price should not be empty' })
  selling_price: number;

  @ApiProperty({
    type: String,
    description: 'date',
    default: '2020-07-1',
  })
  @IsNotEmpty({ message: 'date should not be empty' })
  date: string;
}

export enum NationalityEnum {
  SG = 'sg',
  SGR = 'spr',
  F = 'f',
}

export enum NationalityFullnameEnum {
  SG = 'singaporean',
  SGR = 'singaporean permanent residents',
  F = 'foreigner',
}

// buyer stamp
export class BuyerStamp {
  @ApiProperty({
    type: Number,
    description: 'purchase price',
    default: 10000,
  })
  @IsNotEmpty({ message: 'Purchase price should not be empty' })
  purchase_price: number;

  @ApiProperty({
    type: Number,
    description: 'Loan applicant',
    default: 1,
  })
  @IsNotEmpty({ message: 'Loan applicant should not be empty' })
  loan_applicant: number;

  @ApiProperty({
    type: Number,
    description: 'Applicant1 property',
    default: 1,
  })
  @IsNotEmpty({ message: 'Applicant1 property should not be empty' })
  applicant1_property: number;

  @ApiProperty({
    type: String,
    description: 'Applicant1 nationality',
    default: 'Singapore Permanent Residents',
  })
  @IsNotEmpty({ message: 'Applicant1 nationality should not be empty' })
  // @IsEnum(NationalityEnum)
  applicant1_nationality: string;

  @ApiProperty({
    type: Number,
    description: 'Applicant2 property',
    default: null,
  })
  applicant2_property?: number;

  @ApiProperty({
    type: String,
    description: 'Applicant2 nationality',
    default: '',
  })
  // @IsEnum(NationalityEnum)
  applicant2_nationality?: string;

  @ApiProperty({
    type: Number,
    description: 'Applicant3 property',
    default: null,
  })
  applicant3_property: number;

  @ApiProperty({
    type: String,
    description: 'Applicant3 nationality',
    default: '',
  })
  // @IsEnum(NationalityEnum)
  applicant3_nationality?: string;

  @ApiProperty({
    type: Number,
    description: 'Applicant4 property',
    default: null,
  })
  applicant4_property: number;

  @ApiProperty({
    type: String,
    description: 'Applicant4 nationality',
    default: '',
  })
  applicant4_nationality?: string;

}

// new purchase
export class NewPurchase {
  @ApiProperty({
    type: Number,
    description: 'property price',
    default: 1500000,
  })
  @IsNotEmpty({ message: 'Property price should not be empty' })
  property_price: number;

  @ApiProperty({
    type: Number,
    description: 'loan amount',
    default: 360000,
  })
  @IsNotEmpty({ message: 'Loan amount should not be empty' })
  loan_amount: number;

  @ApiProperty({
    type: Date,
    description: 'dob',
    default: '06-03-1992',
  })
  @IsNotEmpty({ message: 'DOB should not be empty' })
  dob: Date;

  @ApiProperty({
    type: Date,
    description: 'dob list',
    default: [''],
  })
  dob_list?: [Date];

  @ApiProperty({
    type: String,
    description: 'extend loan tenure',
    default: 'no',
  })
  @IsNotEmpty({ message: 'Extend loan tenure should not be empty' })
  // @IsEnum(YesNoEnum)
  extend_loan_tenure: string;

  @ApiProperty({
    type: Number,
    description: 'Property type',
    default: 2,
  })
  @IsNotEmpty({ message: 'Property type should not be empty' })
  property_type: number;

  @ApiProperty({
    type: Number,
    description: 'No. of loan applicants',
    default: 1,
  })
  @IsNotEmpty({ message: 'Number of applicants should not be empty' })
  no_of_loan_applicants: number;

  @ApiProperty({
    type: [Number],
    description: 'monthly fixed income',
    default: [12000],
  })
  @IsNotEmpty({ message: 'Monthly fixed income should not be empty' })
  monthly_fixed_income: number[];

  @ApiProperty({
    type: [Number],
    description: 'total house installment',
    default: [1500],
  })
  @IsNotEmpty({ message: 'Total house installment should not be empty' })
  total_house_installment: number[];

  @ApiProperty({
    type: [Number],
    description: 'total car installment',
    default: [1000],
  })
  @IsNotEmpty({ message: 'Total car installment should not be empty' })
  total_car_installment: number[];

  @ApiProperty({
    type: [Number],
    description: 'total personal installment',
    default: [1000],
  })
  @IsNotEmpty({ message: 'Total personal installment should not be empty' })
  total_personal_installment: number[];

  @ApiProperty({
    type: [Number],
    description: 'total credit installment',
    default: [0],
  })
  @IsNotEmpty({ message: 'Total credit installment should not be empty' })
  total_credit_installment: number[];

  @ApiProperty({
    type: [Number],
    description: 'total property installment',
    default: [0],
  })
  @IsNotEmpty({ message: 'Total property installment should not be empty' })
  total_property_installment: number[];

  @ApiProperty({
    type: [Number],
    description: 'total property company installment',
    default: [500],
  })
  @IsNotEmpty({ message: 'Total property company installment should not be empty' })
  total_property_company_installment: number[];

  @ApiProperty({
    type: Number,
    description: 'annual income',
    default: [130000],
  })
  @IsNotEmpty({ message: 'Annual income should not be empty' })
  annual_income: number;

  @ApiProperty({
    type: [Number],
    description: 'monthly rental income',
    default: [1000],
  })
  @IsNotEmpty({ message: 'Monthly rental income should not be empty' })
  monthly_rental_income: number[];

  @ApiProperty({
    type: [Number],
    description: 'total gurantor installment',
    default: [0],
  })
  @IsNotEmpty({ message: 'Total gurantor installment should not be empty' })
  total_gurantor_installment: number[];

  @ApiProperty({
    type: [Number],
    description: 'no. of housing loan',
    default: [1],
  })
  @IsNotEmpty({ message: 'No. of housing loan should not be empty' })
  no_of_housing_loan: number[];

  @ApiProperty({
    type: [Number],
    description: 'no. of own properties',
    default: [1],
  })
  @IsNotEmpty({ message: 'No. of own properties should not be empty' })
  no_of_own_properties: number[];

  @ApiProperty({
    type: String,
    description: 'Nationality',
    default: 'Others',
  })
  @IsNotEmpty({ message: 'Nationality should not be empty' })
  // @IsEnum(NationalityFullnameEnum)
  nationality: string;

  @ApiProperty({
    type: Number,
    description: 'prefered loan tenure',
    default: 4,
  })
  @IsNotEmpty({ message: 'Prefered loan tenure should not be empty' })
  prefered_loan_tenure: number;

  @ApiProperty({
    type: String,
    description: 'loan category',
    default: 'new_purchase_with_option_to_purchase',
  })
  @IsNotEmpty({ message: 'Loan category should not be empty' })
  loan_category: string;

  @ApiProperty({
    type: Object,
    description: 'employement type',
    default: '["salaried"]',
  })
  @IsNotEmpty({ message: 'Employement type should not be empty' })
  employement_type: string[];
}

// RefinanceTotal
export class RefinanceTotal {
  @ApiProperty({
    type: String,
    description: 'Token value',
    default: '',
  })
  @IsNotEmpty({ message: 'Token should not be empty' })
  token: string;

  @ApiProperty({
    type: Number,
    description: 'No. of loan applicants',
    default: 1,
  })
  @IsNotEmpty({ message: 'Number of applicants should not be empty' })
  no_of_loan_applicants: number;

  @ApiProperty({
    type: [Number],
    description: 'Annual Income',
    default: [4600],
  })
  @IsNotEmpty({ message: 'Annual Income should not be empty' })
  annual_income: number[];

  @ApiProperty({
    type: [Number],
    description: 'Monthly Fixed Income',
    default: [1000],
  })
  @IsNotEmpty({ message: 'Monthly Fixed Income should not be empty' })
  monthly_fixed_income: number[];

  @ApiProperty({
    type: [Number],
    description: 'Monthly Rental Income',
    default: [1000],
  })
  @IsNotEmpty({ message: 'Monthly Rental Income should not be empty' })
  monthly_rental_income: number[];

  @ApiProperty({
    type: Object,
    description: 'employement type',
    default: '["salaried"]',
  })
  @IsNotEmpty({ message: 'Employement type should not be empty' })
  employement_type: string[];

  @ApiProperty({
    type: Date,
    description: 'dob list',
    default: [''],
  })
  dob_list?: [Date];
}

// Refinance
export class Refinance {
  @ApiProperty({
    type: Number,
    description: 'No. of loan applicants',
    default: 1,
  })
  @IsNotEmpty({ message: 'Number of applicants should not be empty' })
  no_of_loan_applicants: number;

  @ApiProperty({
    type: Number,
    description: 'Outstanding Loan Amount',
    default: 200000,
  })
  @IsNotEmpty({ message: 'Outstanding loan amount should not be empty' })
  outstanding_loan_amount: number;

  @ApiProperty({
    type: Number,
    description: 'Early Repayment Penalty',
    default: 0.75,
  })
  @IsNotEmpty({ message: 'Early Repayment Penalty should not be empty' })
  early_repayment_penalty: number;

  @ApiProperty({
    type: Number,
    description: 'Undisbursed Loan Amount',
    default: 50000,
  })
  @IsNotEmpty({ message: 'Undisbursed Loan Amount should not be empty' })
  undisbursed_loan_amount: number;

  @ApiProperty({
    type: Number,
    description: 'Cancellation Penalty',
    default: 0.75,
  })
  @IsNotEmpty({ message: 'Cancellation Penalty should not be empty' })
  cancellation_penalty: number;
}

// refinance-report
export class RefinanceReport {
  @ApiProperty({
    type: String,
    description: 'Token value',
    default: '',
  })
  @IsNotEmpty({ message: 'Token should not be empty' })
  token: string;

  @ApiProperty({
    type: Number,
    description: 'No. of loan applicants',
    default: 1,
  })
  @IsNotEmpty({ message: 'Number of applicants should not be empty' })
  no_of_loan_applicants: number;

  @ApiProperty({
    type: String,
    description: 'Name',
    default: 'John',
  })
  name: string;

  @ApiProperty({
    type: Number,
    description: 'Outstanding Loan Amount',
    default: 600000,
  })
  @IsNotEmpty({ message: 'Outstanding loan amount should not be empty' })
  outstanding_loan_amount: number;

  @ApiProperty({
    type: Number,
    description: 'Outstanding Loan Tenure',
    default: 1,
  })
  @IsNotEmpty({ message: 'Early Repayment Penalty should not be empty' })
  outstanding_loan_tenure: number;

  @ApiProperty({
    type: Number,
    description: 'Prefered Loan Tenure',
    default: 2,
  })
  @IsNotEmpty({ message: 'Prefered loan tenure should not be empty' })
  prefered_loan_tenure: number;

  @ApiProperty({
    type: String,
    description: 'Existing bank',
    default: 'DBS',
  })
  @IsNotEmpty({ message: 'Cancellation Penalty should not be empty' })
  existing_bank: string;

  @ApiProperty({
    type: Number,
    description: 'Existing interest rate',
    default: 1.3,
  })
  @IsNotEmpty({ message: 'Existing interest rate should not be empty' })
  existing_interest_rate: number;

  @ApiProperty({
    type: Number,
    description: 'Property type',
    default: 1,
  })
  @IsNotEmpty({ message: 'Property type should not be empty' })
  property_type: number;

  @ApiProperty({
    type: Number,
    description: 'Year of purchase',
    default: 2010,
  })
  @IsNotEmpty({ message: 'Year of purchase should not be empty' })
  year_of_purchase: number;

  @ApiProperty({
    type: Number,
    description: 'Early repayment penalty amount',
    default: 1500,
  })
  @IsNotEmpty({ message: 'Early repayment penalty amount should not be empty' })
  early_repayment_penalty_amount: number;

  @ApiProperty({
    type: Number,
    description: 'Cancellation penalty amount',
    default: 375,
  })
  @IsNotEmpty({ message: 'Cancellation penalty amount should not be empty' })
  cancellation_penalty_amount: number;

  @ApiProperty({
    type: Number,
    description: 'Existing loan subsidy',
    default: 500,
  })
  @IsNotEmpty({ message: 'Existing loan subsidy should not be empty' })
  existing_loan_subsidy: number;

  @ApiProperty({
    type: Date,
    description: 'dob',
    default: '06-03-1992',
  })
  @IsNotEmpty({ message: 'DOB should not be empty' })
  dob: Date;

  @ApiProperty({
    type: String,
    description: 'Loan category',
    default: 'new_purchase_with_option_to_purchase',
  })
  @IsNotEmpty({ message: 'Loan category should not be empty' })
  loan_category: string;

  @ApiProperty({
    type: Object,
    description: 'Bank details',
    default: '[{"id":25,"bank":"O*","bank_name":"O*","interest_rate":2.5,"package_id":87},{"id":5,"bank":"SCB","bank_name":"SCB","interest_rate":2.32,"package_id":45}]',
  })
  @IsNotEmpty({ message: 'Bank details should not be empty' })
  bank_details: string[];

  @ApiProperty({
    type: Object,
    description: 'Export Fields',
    default: '["Loan_details","Savings_from_refinancing","Package_1","Package_2","Package_3","Package_4"]',
  })
  // @IsNotEmpty({ message: 'Atleast one field is must' })
  export_fields?: string[];

  @ApiProperty({
    type: Number,
    description: 'cash rebate subsidy',
    default: 0,
  })
  @IsNotEmpty({ message: 'cash rebate subsidy should not be empty' })
  cash_rebate_subsidy: number;

  @ApiProperty({
    type: Number,
    description: 'approximate property price',
    default: 0,
  })
  @IsNotEmpty({ message: 'Approximate property price should not be empty' })
  approximate_property_price: number;

  @ApiProperty({
    type: Number,
    description: 'Weighted age',
    default: 20,
  })
  weighted_age?: number;

  @ApiProperty({
    type: String,
    description: 'property status',
    default: 'completed',
  })
  @IsNotEmpty({ message: 'Property status should not be empty' })
  property_status: string;
}

// new-purchase-report
export class NewPurchaseReportDto {
  @ApiProperty({
    type: String,
    description: 'Token value',
    default: '',
  })
  @IsNotEmpty({ message: 'Token should not be empty' })
  token: string;

  @ApiProperty({
    type: Number,
    description: 'No. of loan applicants',
    default: 1,
  })
  @IsNotEmpty({ message: 'Number of applicants should not be empty' })
  no_of_loan_applicants: number;

  @ApiProperty({
    type: String,
    description: 'Name',
    default: 'John',
  })
  name: string;

  @ApiProperty({
    type: Number,
    description: 'Property price',
    default: 1500000,
  })
  @IsNotEmpty({ message: 'Property price amount should not be empty' })
  property_price: number;

  @ApiProperty({
    type: Number,
    description: 'no_of_housing_loan',
    default: 1,
  })
  @IsNotEmpty({ message: 'no. of housing loan should not be empty' })
  no_of_housing_loan: number;

  @ApiProperty({
    type: Number,
    description: 'ltv',
    default: 24,
  })
  @IsNotEmpty({ message: 'ltv should not be empty' })
  ltv: number;

  @ApiProperty({
    type: Number,
    description: 'loan_amount',
    default: 360000,
  })
  @IsNotEmpty({ message: 'loan amount should not be empty' })
  loan_amount: number;

  @ApiProperty({
    type: Number,
    description: 'buyer stamp duty',
    default: 24600,
  })
  @IsNotEmpty({ message: 'buyer stamp duty should not be empty' })
  buyer_stamp_duty: number;

  @ApiProperty({
    type: Number,
    description: 'additional buyer stamp duty',
    default: 255001,
  })
  @IsNotEmpty({ message: 'additional buyer stamp duty should not be empty' })
  additional_buyer_stamp_duty: number;

  @ApiProperty({
    type: Number,
    description: 'legal fees',
    default: 1000,
  })
  @IsNotEmpty({ message: 'legal fees should not be empty' })
  legal_fees: number;

  @ApiProperty({
    type: Number,
    description: 'valuation fees',
    default: 170,
  })
  @IsNotEmpty({ message: 'valuation fees should not be empty' })
  valuation_fees: number[];

  @ApiProperty({
    type: Number,
    description: 'other fees',
    default: 0,
  })
  @IsNotEmpty({ message: 'other fees should not be empty' })
  other_fees: number;

  @ApiProperty({
    type: [String],
    description: 'extend loan tenure',
    default: 'yes',
  })
  @IsNotEmpty({ message: 'extend loan tenure should not be empty' })
  extend_loan_tenure: string;

  @ApiProperty({
    type: Number,
    description: 'maximum loan tenure',
    default: 3,
  })
  @IsNotEmpty({ message: 'maximum loan tenure should not be empty' })
  maximum_loan_tenure: number[];

  @ApiProperty({
    type: Number,
    description: 'maximum loan qualified mlt',
    default: 352303,
  })
  @IsNotEmpty({ message: 'maximum loan qualified mlt should not be empty' })
  maximum_loan_qualified_mlt: number[];

  @ApiProperty({
    type: Number,
    description: 'maximum price mlt',
    default: 7828,
  })
  @IsNotEmpty({ message: 'maximum price mlt should not be empty' })
  maximum_price_mlt: number;

  @ApiProperty({
    type: Number,
    description: 'msr mlt',
    default: 32,
  })
  @IsNotEmpty({ message: 'msr mlt should not be empty' })
  msr_mlt: number;

  @ApiProperty({
    type: Number,
    description: 'tdsr mlt',
    default: 59,
  })
  @IsNotEmpty({ message: 'tdsr mlt should not be empty' })
  tdsr_mlt: number;

  @ApiProperty({
    type: Number,
    description: 'full pludge mlt',
    default: 15504,
  })
  @IsNotEmpty({ message: 'full pludge mlt should not be empty' })
  full_pludge_mlt: number;

  @ApiProperty({
    type: Number,
    description: 'full unpludge mlt',
    default: 51680,
  })
  @IsNotEmpty({ message: 'full unpludge mlt should not be empty' })
  full_unpludge_mlt: number;

  @ApiProperty({
    type: Number,
    description: 'prefer pludge mlt',
    default: 15504,
  })
  @IsNotEmpty({ message: 'prefer pludge mlt should not be empty' })
  prefer_pludge_mlt: number;

  @ApiProperty({
    type: Number,
    description: 'calculatiomn unpludge mlt',
    default: 51680,
  })
  @IsNotEmpty({ message: 'calculatiomn unpludge mlt should not be empty' })
  calculatiomn_unpludge_mlt: number[];

  @ApiProperty({
    type: Number,
    description: 'prefer unpludge mlt',
    default: 51680,
  })
  @IsNotEmpty({ message: 'prefer unpludge mlt should not be empty' })
  prefer_unpludge_mlt: number;
  
  @ApiProperty({
    type: Number,
    description: 'calculation pludge mlt',
    default: 15504,
  })
  @IsNotEmpty({ message: 'calculation pludge mlt should not be empty' })
  calculation_pludge_mlt: number;

  @ApiProperty({
    type: Number,
    description: 'preferrd loan tenure',
    default: 2,
  })
  @IsNotEmpty({ message: 'preferrd loan tenure should not be empty' })
  preferrd_loan_tenure: number;

  @ApiProperty({
    type: Number,
    description: 'maximum loan qualified plt',
    default: 307953,
  })
  @IsNotEmpty({ message: 'maximum loan qualified plt should not be empty' })
  maximum_loan_qualified_plt: number;

  @ApiProperty({
    type: Number,
    description: 'maximum price plt',
    default: 6843,
  })
  @IsNotEmpty({ message: 'maximum price plt should not be empty' })
  maximum_price_plt: number;

  @ApiProperty({
    type: Number,
    description: 'msr plt',
    default: 36,
  })
  @IsNotEmpty({ message: 'msr plt should not be empty' })
  msr_plt: number;

  @ApiProperty({
    type: Number,
    description: 'tdsr plt',
    default: 63,
  })
  @IsNotEmpty({ message: 'tdsr plt should not be empty' })
  tdsr_plt: number;

  @ApiProperty({
    type: Number,
    description: 'full pludge plt',
    default: 35232,
  })
  @IsNotEmpty({ message: 'full pludge plt should not be empty' })
  full_pludge_plt: number;

  @ApiProperty({
    type: Number,
    description: 'full unpludge plt',
    default: 117440,
  })
  @IsNotEmpty({ message: 'full unpludge plt should not be empty' })
  full_unpludge_plt: number;

  @ApiProperty({
    type: Number,
    description: 'prefer pludge plt',
    default: 35232,
  })
  @IsNotEmpty({ message: 'prefer pludge plt should not be empty' })
  prefer_pludge_plt: number;

  @ApiProperty({
    type: Number,
    description: 'calculatiomn unpludge plt',
    default: 117440,
  })
  @IsNotEmpty({ message: 'calculatiomn unpludge plt should not be empty' })
  calculatiomn_unpludge_plt: number;

  @ApiProperty({
    type: Number,
    description: 'prefer unpludge plt',
    default: 117440,
  })
  @IsNotEmpty({ message: 'prefer unpludge plt should not be empty' })
  prefer_unpludge_plt: number;

  @ApiProperty({
    type: Number,
    description: 'calculation pludge plt',
    default: 35232,
  })
  @IsNotEmpty({ message: 'calculation pludge plt should not be empty' })
  calculation_pludge_plt: number;

  @ApiProperty({
    type: Object,
    description: 'Export Fields',
    default: '["Summary","Loan_eligibility_maximum","Loan_eligibility_maximum_package","Loan_eligibility_maximum_mortgage","Loan_eligibility_preferred","Loan_eligibility_preferred_package","Loan_eligibility_preferred_mortgage"]',
  })
  // @IsNotEmpty({ message: 'Atleast one field is must' })
  export_fields?: string[];

  @ApiProperty({
    type: Number,
    description: 'monthly installment mlt',
    default: 0,
  })
  monthly_installment_mlt?: number;

  @ApiProperty({
    type: Number,
    description: 'monthly installment plt',
    default: 0,
  })
  monthly_installment_plt?: number;

  @ApiProperty({
    type: Number,
    description: 'Property type',
    default: 2,
  })
  @IsNotEmpty({ message: 'Property type should not be empty' })
  property_type: number;
}


// new-purchase-total
export class NewPurchaseTotalDto {
  @ApiProperty({
    type: String,
    description: 'Token value',
    default: '',
  })
  @IsNotEmpty({ message: 'Token should not be empty' })
  token: string;

  @ApiProperty({
    type: Number,
    description: 'No. of loan applicants',
    default: 1,
  })
  @IsNotEmpty({ message: 'Number of applicants should not be empty' })
  no_of_loan_applicants: number;

  @ApiProperty({
    type: [Number],
    description: 'total property installment',
    default: [0],
  })
  @IsNotEmpty({ message: 'total property installmentamount should not be empty' })
  total_property_installment: number[];

  @ApiProperty({
    type: [Number],
    description: 'total gurantor installment',
    default: [0],
  })
  @IsNotEmpty({ message: 'total gurantor installment should not be empty' })
  total_gurantor_installment: number[];

  @ApiProperty({
    type: [Number],
    description: 'total property company installment',
    default: [0],
  })
  @IsNotEmpty({ message: 'total property company installment should not be empty' })
  total_property_company_installment: number[];

  @ApiProperty({
    type: [Number],
    description: 'total house installment',
    default: [1000],
  })
  @IsNotEmpty({ message: 'total house installment should not be empty' })
  total_house_installment: number[];

  @ApiProperty({
    type: [Number],
    description: 'total personal installment',
    default: [0],
  })
  @IsNotEmpty({ message: 'total personal installment should not be empty' })
  total_personal_installment: number[];

  @ApiProperty({
    type: [Number],
    description: 'total credit installment',
    default: [0],
  })
  @IsNotEmpty({ message: 'total credit installment should not be empty' })
  total_credit_installment: number[];

  @ApiProperty({
    type: [Number],
    description: 'annual income',
    default: [60000],
  })
  @IsNotEmpty({ message: 'annual income should not be empty' })
  annual_income: number[];

  @ApiProperty({
    type: [Number],
    description: 'monthly fixed income',
    default: [4600],
  })
  @IsNotEmpty({ message: 'monthly fixed income should not be empty' })
  monthly_fixed_income: number[];

  @ApiProperty({
    type: [Number],
    description: 'monthly rental income',
    default: [1000],
  })
  @IsNotEmpty({ message: 'monthly rental income should not be empty' })
  monthly_rental_income: number[];

  @ApiProperty({
    type: Object,
    description: 'employement type',
    default: '["salaried"]',
  })
  @IsNotEmpty({ message: 'Employement type should not be empty' })
  employement_type: string[];

  @ApiProperty({
    type: [Number],
    description: 'total car installment',
    default: [200],
  })
  @IsNotEmpty({ message: 'total car installment should not be empty' })
  total_car_installment: number[];
}

// MortgageRepayment
export class MortgageRepaymentDto {
  @ApiProperty({
    type: String,
    description: 'name',
    default: 'David',
  })
  @IsNotEmpty({ message: 'Name should not be empty' })
  name: string;

  @ApiProperty({
    type: Number,
    description: 'loan_amount',
    default: 500000,
  })
  @IsNotEmpty({ message: 'Loan amount should not be empty' })
  loan_amount: number;

  @ApiProperty({
    type: Number,
    description: 'prefered rate1 year1',
    default: 10,
  })
  @IsNotEmpty({ message: 'prefered rate1 year1 should not be empty' })
  prefered_rate1_year1: number;

  @ApiProperty({
    type: Number,
    description: 'prefered rate1 year2',
    default: 1.5,
  })
  @IsNotEmpty({ message: 'prefered rate1 year2 should not be empty' })
  prefered_rate1_year2: number;

  @ApiProperty({
    type: Number,
    description: 'prefered rate1 year3',
    default: 2,
  })
  @IsNotEmpty({ message: 'prefered rate1 year3 should not be empty' })
  prefered_rate1_year3: number;

  @ApiProperty({
    type: Number,
    description: 'prefered rate1 year4',
    default: 2.5,
  })
  @IsNotEmpty({ message: 'prefered rate1 year4 should not be empty' })
  prefered_rate1_year4: number;

  @ApiProperty({
    type: Number,
    description: 'prefered rate1 year5',
    default: 3,
  })
  @IsNotEmpty({ message: 'prefered rate1 year5 should not be empty' })
  prefered_rate1_year5: number;

  @ApiProperty({
    type: Number,
    description: 'prefered rate2 year1',
    default: null,
  })
  prefered_rate2_year1?: number;

  @ApiProperty({
    type: Number,
    description: 'prefered rate2 year2',
    default: null,
  })
  prefered_rate2_year2?: number;

  @ApiProperty({
    type: Number,
    description: 'prefered rate2 year3',
    default: null,
  })
  prefered_rate2_year3?: number;

  @ApiProperty({
    type: Number,
    description: 'prefered rate2 year4',
    default: null,
  })
  prefered_rate2_year4?: number;

  @ApiProperty({
    type: Number,
    description: 'prefered rate2 year5',
    default: null,
  })
  prefered_rate2_year5?: number;

  @ApiProperty({
    type: Number,
    description: 'loan tenure',
    default: 5,
  })
  @IsNotEmpty({ message: 'Loan tenure should not be empty' })
  loan_tenure: number;

  @ApiProperty({
    type: String,
    description: 'Prefered rate2 enable',
    default: 'no',
  })
  @IsNotEmpty({ message: 'Prefered rate2 enable should not be empty' })
  @IsEnum(YesNoEnum)
  prefered_rate2_enable: string;

  @ApiProperty({
    type: Object,
    description: 'Export Fields',
    default: '["Loan Details","Interest Rate Comparison"]',
  })
  // @IsNotEmpty({ message: 'Atleast one field is must' })
  export_fields?: string[];
}

// buc-mortgage-downpayment
export class BucMortgageDownpaymentDto {
  @ApiProperty({
    type: Number,
    description: 'property price',
    default: 1000000,
  })
  @IsNotEmpty({ message: 'Property price should not be empty' })
  property_price: number;

  @ApiProperty({
    type: Number,
    description: 'ltv',
    default: 75,
  })
  @IsNotEmpty({ message: 'Ltv should not be empty' })
  ltv: number;

  @ApiProperty({
    type: Number,
    description: 'loan_tenure',
    default: 6,
  })
  @IsNotEmpty({ message: 'Loan tenure should not be empty' })
  @IsInt()
  loan_tenure: number;

  @ApiProperty({
    type: Number,
    description: 'cpf available amount',
    default: 0,
  })
  @IsNotEmpty({ message: 'cpf available amount should not be empty' })
  cpf_available_amount: number;
}

// buc-mortgage-downpayment
export class BucMortgageReportDto {
  @ApiProperty({
    type: String,
    description: 'Token value',
    default: '',
  })
  @IsNotEmpty({ message: 'Token should not be empty' })
  token: string;

  @ApiProperty({
    type: String,
    description: 'name',
    default: 'David',
  })
  @IsNotEmpty({ message: 'Name should not be empty' })
  name: string;

  @ApiProperty({
    type: Number,
    description: 'property price',
    default: 1000000,
  })
  @IsNotEmpty({ message: 'Property price should not be empty' })
  property_price: number;

  @ApiProperty({
    type: Number,
    description: 'ltv',
    default: 75,
  })
  @IsNotEmpty({ message: 'Ltv should not be empty' })
  ltv: number;

  // @ApiProperty({
  //   type: String,
  //   description: 'cpf available',
  //   default: 'yes',
  // })
  // @IsNotEmpty({ message: 'CPF available should not be empty' })
  // @IsEnum(YesNoEnum)
  // cpf_available: string;

  @ApiProperty({
    type: Number,
    description: 'cpf available amount',
    default: 150000,
  })
  @IsNotEmpty({ message: 'CPF available amount should not be empty' })
  cpf_available_amount: number;

  @ApiProperty({
    type: Number,
    description: 'downpayment',
    default: 250000,
  })
  @IsNotEmpty({ message: 'Downpayment should not be empty' })
  downpayment: number;

  @ApiProperty({
    type: Number,
    description: 'loan amount',
    default: 750000,
  })
  @IsNotEmpty({ message: 'Loan amount should not be empty' })
  loan_amount: number;

  @ApiProperty({
    type: Number,
    description: 'loan_tenure',
    default: 6,
  })
  @IsNotEmpty({ message: 'Loan tenure should not be empty' })
  @IsInt()
  loan_tenure: number;

  @ApiProperty({
    type: String,
    description: 'prefered rate enable',
    default: 'no',
  })
  @IsNotEmpty({ message: 'Prefered rate enable should not be empty' })
  @IsEnum(YesNoEnum)
  prefered_rate_enable: string;

  @ApiProperty({
    type: Number,
    description: 'prefered_rate_year1',
    default: null,
  })
  prefered_rate_year1?: number;

  @ApiProperty({
    type: Number,
    description: 'prefered rate year2',
    default: null,
  })
  prefered_rate_year2?: number;

  @ApiProperty({
    type: Number,
    description: 'prefered rate year3',
    default: null,
  })
  prefered_rate_year3?: number;

  @ApiProperty({
    type: Number,
    description: 'prefered rate year4',
    default: null,
  })
  prefered_rate_year4?: number;

  @ApiProperty({
    type: Number,
    description: 'prefered rate year5',
    default: null,
  })
  prefered_rate_year5?: number;

  @ApiProperty({
    type: Object,
    description: 'Export Fields',
    default: '["Loan Details","Rate Comparison","Disbursement Schedule","Progressive Repayment","Preferred Repayment"]',
  })
  // @IsNotEmpty({ message: 'Atleast one field is must' })
  export_fields?: string[];

  @ApiProperty({
    type: Object,
    description: 'Disbursement month',
    default: '{"Option To Purchase":6, "Downpayment":12, "Foundation": 6, "Concrete Framework": 8, "Walls" : 6, "Ceiling":6, "Doors and Windows": 7, "Car Park": 6, "Temporary Occupation Permit": 6, "Certificate of Statutory Completion": 6}',
  })
  disbursement_month?: string[];
}


// get unpludge amount
export class GetUnpludgeAmtDto {
  @ApiProperty({
    type: Number,
    description: 'loan amount',
    default: 300000,
  })
  @IsNotEmpty({ message: 'Loan amount should not be empty' })
  loan_amount: number;

  @ApiProperty({
    type: Number,
    description: 'preferred pledge amount',
    default: 50000,
  })
  @IsNotEmpty({ message: 'Preferred pledge amount should not be empty' })
  preferred_fludge_amount: number;

  @ApiProperty({
    type: Number,
    description: 'total monthly income',
    default: 15000,
  })
  @IsNotEmpty({ message: 'Total monthly income should not be empty' })
  total_monthly_income: number;

  @ApiProperty({
    type: Number,
    description: 'loan tenure',
    default: 4,
  })
  @IsNotEmpty({ message: 'Loan tenure should not be empty' })
  loan_tenure: number;

  @ApiProperty({
    type: Number,
    description: 'total financial commitments',
    default: 4300,
  })
  @IsNotEmpty({ message: 'Total financial commitments should not be empty' })
  total_financial_commitments: number;

  @ApiProperty({
    type: Number,
    description: 'Property type',
    default: 1,
  })
  @IsNotEmpty({ message: 'Property type should not be empty' })
  property_type: number;
}

// get pludge amt
export class GetPludgeAmtDto {
  @ApiProperty({
    type: Number,
    description: 'loan amount',
    default: 300000,
  })
  @IsNotEmpty({ message: 'Loan amount should not be empty' })
  loan_amount: number;

  @ApiProperty({
    type: Number,
    description: 'preferred unpledge amount',
    default: 120000,
  })
  @IsNotEmpty({ message: 'Preferred unpledge amount should not be empty' })
  preferred_unfludge_amount: number;

  @ApiProperty({
    type: Number,
    description: 'total monthly income',
    default: 15000,
  })
  @IsNotEmpty({ message: 'Total monthly income should not be empty' })
  total_monthly_income: number;

  @ApiProperty({
    type: Number,
    description: 'loan tenure',
    default: 4,
  })
  @IsNotEmpty({ message: 'Loan tenure should not be empty' })
  loan_tenure: number;

  @ApiProperty({
    type: Number,
    description: 'total financial commitments',
    default: 4300,
  })
  @IsNotEmpty({ message: 'Total financial commitments should not be empty' })
  total_financial_commitments: number;

  @ApiProperty({
    type: Number,
    description: 'Property type',
    default: 1,
  })
  @IsNotEmpty({ message: 'Property type should not be empty' })
  property_type: number;
}