import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { ConfigModule } from '../../config/config.module';
import { GoogleStrategy } from './google.strategy';
import {TwoFactorAuthenticationService} from "./twoFactorAuthentication.service";
// import { keycloakModule } from '../login/login.keycloak.module';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { MemberShipAclService } from 'src/middlewares/acl_module/membership_acl_service';
import {SubscriptionModule} from "../subscription/subscription.module";
import {SubscriptionService} from "../subscription/subscription.service";
import { ScheduleModule } from '@nestjs/schedule'; // to run cron job
import { AdminService } from '../admin/admin.service';
// keycloakModule

@Module({
  imports: [ConfigModule, HttpModule, EventEmitterModule.forRoot(), ScheduleModule.forRoot()],
  controllers: [UserController],
  providers: [UserService, GoogleStrategy, TwoFactorAuthenticationService, MemberShipAclService,SubscriptionService, AdminService],
  exports: [UserService],
})
export class UserModule { }
