import { PassportStrategy } from '@nestjs/passport';
import { Strategy, VerifyCallback } from 'passport-google-oauth20';
import { config } from 'dotenv';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '../../config/config.service';

config();

@Injectable()
export class GoogleStrategy extends PassportStrategy(
  Strategy,
  'googleregister',
) {
  constructor(config: ConfigService) {
    super({
      clientID: config.envConfig.GOOGLE_REGISTER_CLIENTID,
      clientSecret: config.envConfig.GOOGLE_REGISTER_SECRETID,
      callbackURL: config.envConfig.GOOGLE_CALLBACK_URL,
      scope: ['email', 'profile'],
    });
  }

  async validate(
    accessToken: string,
    refreshToken: string,
    profile: any,
    done: VerifyCallback,
  ): Promise<any> {
    const { name, emails, photos } = profile;
    const user = {
      email: emails[0].value,
      full_name: name.givenName,
      lastName: name.familyName,
      picture: photos[0].value,
      accessToken,

      // extra field required for register
      agreed_term: '1',
      // password: 'Welcome@123',
      // mobile: 9834128741,
      // dob: '20-09-1975',
    };
    done(null, user);
  }
}
