import { ApiProperty } from '@nestjs/swagger';
import { timestamp } from 'aws-sdk/clients/cloudfront';
import { Json } from 'aws-sdk/clients/robomaker';
import {
  IsDefined,
  IsEmail, IsEnum, IsIn,
  IsInt,
  IsNotEmpty,
  IsString,
  Length,
  Validate,
} from 'class-validator';
import { timeStamp } from 'console';
import { Timestamp, Unique } from 'typeorm';
import {isDeepStrictEqual} from "util";
import {UserEntity} from "../../../entities/users";
import {AccessTypeEnum} from "../../login/login.dto";

export class TwoFactorAuthenticationCodeDto {

  @ApiProperty({
    type: String,
    description: 'twoFactorAuthenticationCode',
  })
  @IsNotEmpty({ message: 'Please provide authenticator code' })
  @Length(2, 255)
  twoFactorAuthenticationCode: string;

  @ApiProperty({
    type: UserEntity,
    description: 'User object'
  })
  user: UserEntity;

}

export class UserDto {
  @ApiProperty({
    type: String,
    description: 'Fullname',
  })
  @IsNotEmpty({ message: 'Full name should not be empty' })
  @Length(2, 255)
  full_name: string;

  @ApiProperty({
    type: String,
    description: 'Email',
    default: 'roy@gmail.com',
  })
  @IsEmail({}, { message: 'Please enter a valid email id' })
  @IsNotEmpty({ message: 'Email should not be empty' })
  @Length(2, 255)
  email: string;

  @ApiProperty({
    type: String,
    description: 'country_code',
    default: '65',
  })
  @IsNotEmpty({ message: 'Country code should not be empty' })
  @Length(1, 5, {
    message: 'Enter valid country code',
  })
  country_code: string;

  @ApiProperty({
    type: String,
    description: 'country_code_label',
    default: 'SG',
  })
  @IsNotEmpty({ message: 'Country code label should not be empty' })
  @Length(1, 5, {
    message: 'Enter valid country  label',
  })
  country_code_label: string;

  @ApiProperty({
    type: String,
    description: 'mobile',
    default: '1234123456',
  })
  @IsNotEmpty({ message: 'Mobile should not be empty' })
  @Length(8, 255, {
    message: 'Enter valid phone number with country code',
  })
  mobile: string;

  @ApiProperty({
    type: String,
    description: 'Password',
    default: 'roy',
  })
  @IsNotEmpty({ message: 'Password should not be empty' })
  @Length(8, 255)
  password: string;

  // @ApiProperty({
  //   type: Date,
  //   description: 'dob',
  //   default: '20-09-1960',
  // })
  // @IsNotEmpty({ message: 'Should not be empty' })
  // dob: Date;

  @ApiProperty({
    type: String,
    description: 'dob',
    default: '20-09-1962',
  })
  @IsNotEmpty({ message: 'DOB should not be empty' })
  dob: string;

  @ApiProperty({
    type: Number,
    description: 'Agreed Term',
    default: 1,
  })
  @IsNotEmpty({ message: 'Agree term should not be empty' })
  @IsIn([1])
  agreed_term: number;

  @ApiProperty({
    type: String,
    description: 'Referal Code',
    default: '',
  })
  referal_code?: string;

  // @ApiProperty({
  //   type: String,
  //   description: 'Is Email Verified',
  //   default: '0',
  // })
  // is_email_verified: number;

  // @ApiProperty({
  //   type: String,
  //   description: 'Is Account Enabled'
  // })
  // is_account_enabled: number;

  // @ApiProperty({
  //   type: String,
  //   description: 'User Type',
  //   default: 'agent',
  // })
  //   @IsNotEmpty({ message: 'User type should not be empty' })
  //   user_type: string;

  //   @ApiProperty({
  //     type: String,
  //     description: 'Membership Type',
  //     default: 'basic',
  //   })
  //     @IsNotEmpty({ message: 'Membership type should not be empty' })
  //     membership_type: string;
}

export class DefaultPassword {
  @ApiProperty({
    type: String,
    description: 'Password',
    default: 'Welcome@123',
  })
  @IsNotEmpty({ message: 'Password should not be empty' })
  @Length(8, 255)
  password: string;

  @ApiProperty({
    type: String,
    description: 'Token',
  })
  @IsNotEmpty({ message: 'Token should not be empty' })
  token: string;

}

export enum ChangeTypeEnum {
  MOBILE = 'Mobile',
  EMAIL = 'Email'
}

export enum SetRoleName {
  SET = 'SET',
  UNSET = 'UNSET'
}

export class DefaultVerificationCode {

  @ApiProperty({
    type: String,
    description: 'Verification Token',
    default: '34563',
  })
  @IsNotEmpty({ message: 'Verification Token should not be empty' })
  verification_token: string;

  @ApiProperty({
    type: String,
    description: 'Verification Code',
    default: '34563',
  })
  @IsNotEmpty({ message: 'Please enter a valid otp' })
  // @IsNotEmpty({ message: 'Verification code should not be empty' })
  // @Length(2, 10)
  verification_code: string;

  @ApiProperty({
    type: String,
    description: 'Verification Type',
    default: 'Mobile',
  })
  @IsNotEmpty({ message: 'Verification Type can be either email or mobile' })
  @IsEnum(ChangeTypeEnum)
  type: string;

}

export class VerifyCodeResDto {
  @ApiProperty({
    type: String,
    description: 'message',
    default: 'Congratulations ! Your mobile number has been successfully verified.',
  })
  message: string;

}

export class QueryUserParmDto {
  @IsDefined()
  @IsNotEmpty()
  full_name: string;
}

export class DefaultOccupationDto {
  @ApiProperty({
    type: String,
    description: 'Occupation',
    default: 'Devloper',
  })
  @IsDefined()
  @IsNotEmpty({ message: 'Occupation should not be empty' })
  occupation: string;
}

export class UserIDDto {
  @IsDefined()
  @IsNotEmpty({ message: 'ID should not be empty' })
  @IsInt()
  id: number;
  // @IsString()
  // id: string;
}

export class DefaultBankDto {
  @ApiProperty({
    type: String,
    description: 'Bank name',
    default: 'ICICI Bank',
  })
  @IsDefined()
  @IsNotEmpty({ message: 'Bank name should not be empty' })
  bank_name: string;

  @ApiProperty({
    type: String,
    description: 'Bank A/c number',
    default: 'IC131212312',
  })
  @IsDefined()
  @IsNotEmpty({ message: 'Bank A/c number should not be empty' })
  bank_ac_number: string;

  // @ApiProperty({
  //   type: String,
  //   description: 'Bank branch code',
  //   default: 'IN12123',
  // })
  // @IsDefined()
  // @IsNotEmpty({ message: 'Bank branch code should not be empty' })
  // bank_branch_code: string;

  // @ApiProperty({
  //   type: String,
  //   description: 'Bank nation',
  //   default: 'Delhi',
  // })
  // @IsDefined()
  // @IsNotEmpty({ message: 'Bank nation should not be empty' })
  // bank_nation: string;
}

export class UserEmailDto {
  @ApiProperty({
    type: String,
    description: 'Email ID',
    default: 'chaithra@digitalprizm.net',
  })
  @IsNotEmpty({ message: 'Email should not be empty' })
  @IsEmail({}, { message: 'Please enter a valid email id' })
  @Length(2, 255)
  email: string;
}

export class RedisDto {
  @ApiProperty({
    type: String,
    description: 'Redis Key'
  })
  @IsNotEmpty({ message: 'Redis Key should not be empty' })
  @Length(1, 255)
  redisKey: string;

  @ApiProperty({
    type: String,
    description: 'Redis Value'
  })
  @IsNotEmpty({ message: 'Redis Value should not be empty' })
  redisVal: string;
}

export class UserMobileDto {
  @ApiProperty({
    type: String,
    description: 'country_code',
    default: '65',
  })
  @IsNotEmpty({ message: 'Country code should not be empty' })
  @Length(1, 5, {
    message: 'Enter valid country code',
  })
  country_code: string;

  @ApiProperty({
    type: String,
    description: 'country_code_label',
    default: 'SG',
  })
  @IsNotEmpty({ message: 'Country code label should not be empty' })
  @Length(1, 5, {
    message: 'Enter valid country  label',
  })
  country_code_label: string;
  
  @ApiProperty({
    type: String,
    description: 'Mobile number',
    default: '8773467234',
  })
  @IsNotEmpty({ message: 'Mobile should not be empty' })
  @IsString()
  @Length(8, 255, {
    message: 'Enter valid phone number with country code',
  })
  mobile: string;
}

export class UserMobileResDto {
  @ApiProperty({
    type: String,
    description: 'verification_token',
    default: 'eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bnFyZWRpc2tleSI6IjUyOGQ1MTg4LTRlZTMtNDJlOS04NDFlLTA4MmZiZWUzZTVmNV9Nb2JpbGUiLCJpZCI6IjUyOGQ1MTg4LTRlZTMtNDJlOS04NDFlLTA4MmZiZWUzZTVmNSIsImlhdCI6MTY1MjYwNDI5NH0.b0Ckmjz6p1ZlSGfQfsOqjc3Th51b2gR7OTG9tghOnFYHL1Cg7ETYT4ZB_SRONlxLJYPIuEUOdIBtaoarKlZjGw',
  })
  verification_token: string;
  
  @ApiProperty({
    type: String,
    description: 'message',
    default: 'Please verify the mobile',
  })
  message: string;


}


export enum StatusEnum {
  Active = 1,
  InActive = 0,
}

export class DefaultUserStatusDto {
  @ApiProperty({
    type: Number,
    description: 'Status (0=blocked, 1=active)',
    default: '1',
  })
  @IsNotEmpty({ message: 'Status should not be empty' })
  @IsEnum(StatusEnum)
  @IsInt()
  status: number;
}

export class LogoutDto {
  @IsNotEmpty({ message: 'Please provide the refresh token value' })
  refresh_token: string;
}

export class DefaultChgPswrdDto {
  @ApiProperty({
    type: String,
    description: 'Password',
    default: 'pass',
  })
  @IsNotEmpty({ message: 'Old password should not be empty' })
  @Length(8, 255)
  old_password: string;

  @ApiProperty({
    type: String,
    description: 'Password',
    default: 'newpass',
  })
  @IsNotEmpty({ message: 'New password should not be empty' })
  @Length(8, 255)
  new_password: string;

  @ApiProperty({
    type: String,
    description: 'Password',
    default: 'newpass',
  })
  @IsNotEmpty({ message: 'Confirm password should not be empty' })
  @Length(8, 255)
  confirm_password: string;
}

export class EmailMobileVerifyStatusDto {
  @ApiProperty({
    type: Number,
    description: 'User mobile status',
    default: 1,
  })
  @IsNotEmpty({ message: 'User mobile status should not be empty' })
  @IsEnum(StatusEnum)
  @IsInt()
  status: number;
}

export class DefaultUpdateNamePhone {
  @ApiProperty({
    type: String,
    description: 'Fullname',
    default: '',
  })
  // @IsNotEmpty({ message: 'Full name should not be empty' })
  // @Length(2, 255)
  full_name?: string;

  @ApiProperty({
    type: String,
    description: 'dob',
    default: '20-09-1962',
  })
  // @IsNotEmpty({ message: 'DOB should not be empty' })
  dob?: string;
}

export class RefreshDto {
  // @ApiProperty({
  //   type: String,
  //   description: 'ID',
  // })
  // @IsString()
  // @IsNotEmpty()
  // id: string;

  @ApiProperty({
    type: String,
    description: 'Refresh Token',
    default: '',
  })
  @IsString()
  @IsNotEmpty()
  refresh_token: string;
}

export class CampiagnDto {
  @ApiProperty({
    type: String,
    description: 'Referral Code',
    default: 'BF34ER',
  })
  @IsNotEmpty({ message: 'Referral Code should not be empty' })
  referal_code: string;

  @ApiProperty({
    type: Number,
    description: 'Usage limit',
    default: 10,
  })
  @IsNotEmpty({ message: 'Usage limit' })
  usage_limt: number;

  @ApiProperty({
    type: Date,
    description: 'Start date',
    default: '2022-02-22',
  })
  @IsNotEmpty({ message: 'Start date should not be empty' })
  start_date: Date;

  @ApiProperty({
    type: Date,
    description: 'End date',
    default: '2022-02-25',
  })
  @IsNotEmpty({ message: 'End date should not be empty' })
  end_date: Date;

  @ApiProperty({
    type: Number,
    description: 'User status (0=blocked, 1=active)',
    default: 1,
  })
  @IsNotEmpty({ message: 'User status should not be empty' })
  @IsEnum(StatusEnum)
  @IsInt()
  status: number;
}


export class DefaultReferalDto {
  @ApiProperty({
    type: String,
    description: 'Referral Code',
    default: 'BF34ER',
  })
  @IsNotEmpty({ message: 'Referral Code should not be empty' })
  referal_code: string;
}

export class DefaultReferralResDto {
  @ApiProperty({
    type: String,
    description: 'message',
    default: 'Referral code has been updated successfully'
  })
  message: string
}

export class ContactUsDto {
  @ApiProperty({
    type: String,
    description: 'Name',
  })
  @IsNotEmpty({ message: 'Name should not be empty' })
  @Length(2, 255)
  name: string;

  @ApiProperty({
    type: String,
    description: 'Email',
    default: 'roy@gmail.com',
  })
  @IsEmail({}, { message: 'Please enter a valid email id' })
  @IsNotEmpty({ message: 'Email should not be empty' })
  @Length(2, 255)
  email: string;

  @ApiProperty({
    type: String,
    description: 'Message',
    default: 'Need to know more about PDPA',
  })
  @IsNotEmpty({ message: 'Message should not be empty' })
  @Length(2, 255)
  message: string;
}

export class RegisterResDto {
  @ApiProperty({
    type: String,
    description: 'id',
    default: 'uuid',
  })
  id: string;

  @ApiProperty({
    type: String,
    description: 'full_name ',
    default: 'string',
  })
  full_name: string;

  @ApiProperty({
    type: String,
    description: 'email',
    default: 'string',
  })
  email: string;

  @ApiProperty({
    type: String,
    description: 'country_code',
    default: 'number',
  })
  country_code: string;

  @ApiProperty({
    type: String,
    description: 'mobile',
    default: 'number',
  })
  mobile: string;

  @ApiProperty({
    type: String,
    description: 'dob',
    default: 'date',
  })
  dob: string;

  @ApiProperty({
    type: Number,
    description: 'agreed_term',
    default: 1,
  })
  agreed_term: number;

  @ApiProperty({
    type: String,
    description: 'SMSToken',
    default: 'string',
  })
  SMSToken: string;
}

export class UserDetailResDto {
  @ApiProperty({
    type: String,
    description: 'id',
    default: 'eaf5e3a5-b58c-4300-b7f2-269516476f57'
  })
  id: string

  @ApiProperty({
    type: String,
    description: 'full_name',
    default: 'string'
  })
  full_name: string

  @ApiProperty({
    type: String,
    description: 'email',
    default: 'string'
  })
  email: string

  @ApiProperty({
    type: String,
    description: 'stripe_customer_id',
    default: 'string'
  })
  stripe_customer_id: string

  @ApiProperty({
    type: String,
    description: 'stripe_payment_method_id',
    default: 'string'
  })
  stripe_payment_method_id: string

  @ApiProperty({
    type: String,
    description: 'stripe_price_id',
    default: 'string'
  })
  stripe_price_id: string

  @ApiProperty({
    type: String,
    description: 'country_code',
    default: '91'
  })
  country_code: string

  @ApiProperty({
    type: String,
    description: 'mobile',
    default: '98847839567'
  })
  mobile: string

  @ApiProperty({
    type: String,
    description: 'whole_number',
    default: '9198847839567'
  })
  whole_number: string

  @ApiProperty({
    type: String,
    description: 'phone',
    default: null
  })
  phone: string

  @ApiProperty({
    type: String,
    description: 'user_type',
    default: 'agent'
  })
  user_type: string

  @ApiProperty({
    type: String,
    description: 'membership_type',
    default: 'basic'
  })
  membership_type: string

  @ApiProperty({
    type: String,
    description: 'photo',
    default: null
  })
  photo: string

  @ApiProperty({
    type: String,
    description: 'verification_code_send_datetime',
    default: '2022-05-03T16:57:34.755Z'
  })
  verification_code_send_datetime: string
  
  @ApiProperty({
    type: Number,
    description: 'status',
    default: 1
  })
  status: number

  @ApiProperty({
    type: String,
    description: 'created_user_id',
    default: 'eaf5e3a5-b58c-4300-b7f2-269516476f57'
  })
  created_user_id: string

  @ApiProperty({
    type: String,
    description: 'created_at',
    default: '2022-05-03T11:13:06.882Z'
  })
  created_at: string

  @ApiProperty({
    type: String,
    description: 'updated_at',
    default: '2022-05-16T04:14:50.829Z'
  })
  updated_at: string

  @ApiProperty({
    type: String,
    description: 'dob',
    default: '20-09-1962'
  })
  dob: string

  @ApiProperty({
    type: String,
    description: 'device',
    default: null
  })
  device: string

  @ApiProperty({
    type: String,
    description: 'ip',
    default: null
  })
  ip: string

  @ApiProperty({
    type: String,
    description: 'location',
    default: null
  })
  location: string

  @ApiProperty({
    type: String,
    description: 'occupation',
    default: 'Devloper'
  })
  occupation: string

  @ApiProperty({
    type: String,
    description: 'postal_code',
    default: null
  })
  postal_code: string

  @ApiProperty({
    type: Number,
    description: 'is_mobile_verified',
    default: 1
  })
  is_mobile_verified: number

  @ApiProperty({
    type: Number,
    description: 'is_email_verified',
    default: 1
  })
  is_email_verified: number

  @ApiProperty({
    type: String,
    description: 'address',
    default: null
  })
  address: string  

  @ApiProperty({
    type: String,
    description: 'twoFactorAuthenticationSecret',
    default: null
  })
  twoFactorAuthenticationSecret: string 
  
  @ApiProperty({
    type: String,
    description: 'twoFactorAuthenticationSecret',
    default: false
  })
  isTwoFactorAuthenticationEnabled:  string
  
  @ApiProperty({
    type: String,
    description: 'register_type',
    default: 'manual'
  })
  register_type: string  

  @ApiProperty({
    type: String,
    description: 'country_code_label',
    default: 'IN'
  })
  country_code_label: string

  @ApiProperty({
    type: String,
    description: 'refresh_token',
    default: 'string'
  })
  refresh_token: string

  @ApiProperty({
    type: String,
    description: 'referal_code',
    default: 'ZMW6LY'
  })
  referal_code: string

  @ApiProperty({
    type: Number,
    description: 'cobroke_status',
    default: 0
  })
  cobroke_status: number
}

export class Chk2FAStatusResDto {
  @ApiProperty({
    type: String,
    description: 'message',
    default: '2FA is updated successfully!'
  })
  message: string
}

export class Disable2FAResDto {
  @ApiProperty({
    type: String,
    description: 'message',
    default: '2FA is disabled successfully'
  })
  message: string
}

export class OccupationSet {
  @ApiProperty({
    type: String,
    description: 'id',
    default: 'eaf5e3a5-b58c-4300-b7f2-269516476f57'
  })
  id: string

  @ApiProperty({
    type: String,
    description: 'full_name',
    default: 'string'
  })
  full_name: string

  @ApiProperty({
    type: String,
    description: 'email',
    default: 'string'
  })
  email: string

  @ApiProperty({
    type: String,
    description: 'country_code',
    default: '91'
  })
  country_code: string

  @ApiProperty({
    type: String,
    description: 'mobile',
    default: '8971170210'
  })
  mobile: string

  @ApiProperty({
    type: String,
    description: 'dob',
    default: '20-09-1962'
  })
  dob: string

  @ApiProperty({
    type: String,
    description: 'occupation',
    default: 'Devloper'
  })
  occupation: string
}

export class UpdateOccupationResDto {
  @ApiProperty({
    type: String,
    description: 'Suucess message',
    default: 'Your Profile has been updated successfully.'
  })
  message: string

  @ApiProperty({
    description: "updated occupation",
    type: OccupationSet,
  })
  showData: OccupationSet;
}


export class AddBankdetailResDto {
  @ApiProperty({
    type: String,
    description: 'userId',
    default: 'eaf5e3a5-b58c-4300-b7f2-269516476f57'
  })
  userId: string

  @ApiProperty({
    type: String,
    description: 'bank_name',
    default: 'string'
  })
  bank_name: string

  @ApiProperty({
    type: String,
    description: 'bank_ac_number',
    default: 'string'
  })
  bank_ac_number: string
}

export class UpdateBankdetailResDto {
  @ApiProperty({
    type: String,
    description: 'userId',
    default: 'eaf5e3a5-b58c-4300-b7f2-269516476f57'
  })
  userId: string

  @ApiProperty({
    type: String,
    description: 'bank_name',
    default: 'string'
  })
  bank_name: string

  @ApiProperty({
    type: String,
    description: 'bank_ac_number',
    default: 'string'
  })
  bank_ac_number: string

  @ApiProperty({
    type: String,
    description: 'created_at',
    default: '2022-05-17T05:36:59.497Z' 
  })
  created_at: string
}

export class DeleteBankdetailResDto {
  @ApiProperty({
    type: String,
    description: 'raw',
    default: '[]'
  })
  raw: string

  @ApiProperty({
    type: Number,
    description: 'affected',
    default: 1
  })
  affected: number
}

export class ChgEmailResDto {
  @ApiProperty({
    type: String,
    description: 'message',
    default: 'Please verify the email adress'
  })
  message: string
}

export class ChgPassResDto {
  @ApiProperty({
    type: String,
    description: 'message',
    default: 'Congratulations ! Password has been updated successfully'
  })
  message: string
}

export class NameDateSet {
  @ApiProperty({
    type: String,
    description: 'id',
    default: 'eaf5e3a5-b58c-4300-b7f2-269516476f57'
  })
  id: string

  @ApiProperty({
    type: String,
    description: 'full_name',
    default: 'string'
  })
  full_name: string

  @ApiProperty({
    type: String,
    description: 'email',
    default: 'string'
  })
  email: string

  @ApiProperty({
    type: String,
    description: 'country_code',
    default: '91'
  })
  country_code: string

  @ApiProperty({
    type: String,
    description: 'mobile',
    default: '8971170210'
  })
  mobile: string

  @ApiProperty({
    type: String,
    description: 'dob',
    default: '20-09-1962'
  })
  dob: string
}

export class UpdateNameDobResDto {
  @ApiProperty({
    type: String,
    description: 'Suucess message',
    default: 'Your Profile has been updated successfully.'
  })
  message: string

  @ApiProperty({
    description: "updated name and dob",
    type: NameDateSet,
  })
  showData: NameDateSet;
}

export class UserRefreshTokenResDto {
  @ApiProperty({
    type: String,
    description: 'access_token',
    default: 'eyJhbGciOiJFUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJ4WC1pOXA0X0pUUUxHMkZtQXhmVVduVGliQXRERUd4aDh3NXRoVlpKa3ZjIn0.eyJleHAiOjE2NTI3MDUwOTAsImlhdCI6MTY1MjcwMzI5MCwianRpIjoiZTlhZTZjNjAtYzFmNi00YWQ4LWE3NjEtYmU4NzYzZjdlMWY5IiwiaXNzIjoiaHR0cDovLzU0LjI1NC4xMS41MTo4MDgwL2F1dGgvcmVhbG1zL0FnZW50cyIsImF1ZCI6ImFjY291bnQiLCJzdWIiOiJlYWY1ZTNhNS1iNThjLTQzMDAtYjdmMi0yNjk1MTY0NzZmNTciLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJhZ2VudCIsInNlc3Npb25fc3RhdGUiOiI0ODljMWU0Mi0yMTg2LTRkNjgtOTMxMi1h'
  })
  access_token: string

  @ApiProperty({
    type: Number,
    description: 'expires_in',
    default: 1800
  })
  expires_in: number

  @ApiProperty({
    type: String,
    description: 'refresh_token',
    default: 'eyJhbGciOiJFUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJ4WC1pOXA0X0pUUUxHMkZtQXhmVVduVGliQXRERUd4aDh3NXRoVlpKa3ZjIn0.eyJleHAiOjE2NTI3MDUwOTAsImlhdCI6MTY1MjcwMzI5MCwianRpIjoiZTlhZTZjNjAtYzFmNi00YWQ4LWE3NjEtYmU4NzYzZjdlMWY5IiwiaXNzIjoiaHR0cDovLzU0LjI1NC4xMS41MTo4MDgwL2F1dGgvcmVhbG1zL0FnZW50cyIsImF1ZCI6ImFjY291bnQiLCJzdWIiOiJlYWY1ZTNhNS1iNThjLTQzMDAtYjdmMi0yNjk1MTY0NzZmNTciLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJhZ2VudCIsInNlc3Npb25fc3RhdGUiOiI0ODljMWU0Mi0yMTg2LTRkNjgtOTMxMi1h'
  })
  refresh_token: string

  @ApiProperty({
    type: Number,
    description: 'refresh_expires_in',
    default: 1800
  })
  refresh_expires_in: number
}

export class ContactUSResDto {
  @ApiProperty({
    type: String,
    description: 'name',
    default: 'string'
  })
  name: string

  @ApiProperty({
    type: String,
    description: 'email',
    default: 'string'
  })
  email: string

  @ApiProperty({
    type: String,
    description: 'message',
    default: 'string'
  })
  message: string

  @ApiProperty({
    type: String,
    description: 'created_at',
    default: '2022-05-16T12:13:31.263Z'
  })
  created_at: string

  @ApiProperty({
    type: String,
    description: 'created_user_id',
    default: 'eaf5e3a5-b58c-4300-b7f2-269516476f57'
  })
  created_user_id: string

  @ApiProperty({
    type: String,
    description: 'updated_user_id',
    default: null
  })
  updated_user_id: string

  @ApiProperty({
    type: String,
    description: 'updated_at',
    default: null
  })
  updated_at: string

  @ApiProperty({
    type: Number,
    description: 'id',
    default: null
  })
  id: number
}

// BuyerDto.ts
export class BuyerDto {
  @ApiProperty({
    type: String,
    description: 'updated_user_id',
    default: null
  })
  updated_user_id: string

  @ApiProperty({
    type: String,
    description: 'updated_at',
    default: null
  })
  updated_at: string

  @ApiProperty({
    type: Number,
    description: 'id',
    default: null
  })
  id: number
}


export class NewBookingDto {

  @ApiProperty({
    type: String,
    description: 'created_user_id',
    default: 'eaf5e3a5-b58c-4300-b7f2-269516476f57'
  })
  created_user_id: string

  @ApiProperty({
    description: "List of buyers",
    type: BuyerDto,
  })
  buyers1: BuyerDto;

  @ApiProperty({
    description: "List of buyers",
    type: BuyerDto,
  })
  buyers2: BuyerDto;
}

export class AdminRegisterResDto {
  @ApiProperty({
    type: String,
    description: 'id',
    default: 'uuid',
  })
  id: string;

  @ApiProperty({
    type: String,
    description: 'full_name ',
    default: 'string',
  })
  full_name: string;

  @ApiProperty({
    type: String,
    description: 'email',
    default: 'string',
  })
  email: string;

  @ApiProperty({
    type: String,
    description: 'mobile',
    default: 'number',
  })
  mobile: string;

  @ApiProperty({
    type: String,
    description: 'dob',
    default: 'date',
  })
  dob: string;

  @ApiProperty({
    type: Number,
    description: 'agreed_term',
    default: 1,
  })
  agreed_term: number;
}

export class ChkValidAdminResDto {
  @ApiProperty({
    type: String,
    description: 'message',
    default: 'Authorized admin',
  })
  message: string;
}

export class ChgAdminPassResDto {
  @ApiProperty({
    type: String,
    description: 'message',
    default: 'Your password has been updated successfully!',
  })
  message: string;
}

export class ChgStatusResDto {
  @ApiProperty({
    type: String,
    description: 'id',
    default: 'uuid',
  })
  id: string;

  @ApiProperty({
    type: String,
    description: 'full_name ',
    default: 'string',
  })
  full_name: string;

  @ApiProperty({
    type: String,
    description: 'email',
    default: 'string',
  })
  email: string;

  @ApiProperty({
    type: String,
    description: 'country_code',
    default: 'number',
  })
  country_code: string;

  @ApiProperty({
    type: String,
    description: 'mobile',
    default: 'number',
  })
  mobile: string;

  @ApiProperty({
    type: String,
    description: 'dob',
    default: 'date',
  })
  dob: string;

  @ApiProperty({
    type: Number,
    description: 'status',
    default: 1,
  })
  status: number;
}

export class SendResetPassResDto {
  @ApiProperty({
    type: String,
    description: 'message',
    default: 'We have sent a password recover instructions to your email address.',
  })
  message: string;
}

///
export class BankSet {
  @ApiProperty({
    type: String,
    description: 'bank name',
    default: 'string'
  })
  bank_name: string

  @ApiProperty({
    type: String,
    description: 'bank a/c number',
    default: 'string'
  })
  bank_ac_number: string
}

export class AdminUserSet {
  @ApiProperty({
    type: String,
    description: 'id',
    default: 'eaf5e3a5-b58c-4300-b7f2-269516476f57'
  })
  id: string

  @ApiProperty({
    type: String,
    description: 'full_name',
    default: 'string'
  })
  full_name: string

  @ApiProperty({
    type: String,
    description: 'email',
    default: 'string'
  })
  email: string

  @ApiProperty({
    type: String,
    description: 'stripe customer id',
    default: 'string'
  })
  stripe_customer_id: string

  @ApiProperty({
    type: String,
    description: 'stripe payment method id',
    default: 'string'
  })
  stripe_payment_method_id: string

  @ApiProperty({
    type: String,
    description: 'stripe subscription id',
    default: 'string'
  })
  stripe_subscription_id: string

  @ApiProperty({
    type: String,
    description: 'mobile',
    default: '8971170210'
  })
  mobile: string

  @ApiProperty({
    type: String,
    description: 'membership type',
    default: 'basic'
  })
  membership_type: string

  @ApiProperty({
    type: String,
    description: 'status',
    default: 1
  })
  status: string

  @ApiProperty({
    type: String,
    description: 'dob',
    default: '20-09-1962'
  })
  dob: string

  @ApiProperty({
    type: String,
    description: 'isTwoFactorAuthenticationEnabled',
    default: true
  })
  isTwoFactorAuthenticationEnabled: string

  @ApiProperty({
    type: String,
    description: 'cobroke status',
    default: 0
  })
  cobroke_status: string

  @ApiProperty({
    description: "Users list",
    type: [BankSet],
  })
  bankers: BankSet[];
}

export class AdminAllUserListResDto {
  @ApiProperty({
    description: "User list",
    type: [AdminUserSet],
  })
  data: AdminUserSet[];
}

export class AdminUserListResDto {
  @ApiProperty({
    description: "User list",
    type: AdminUserSet,
  })
  data: AdminUserSet;
}

export class SubInvoicesResDto {
  @ApiProperty({
    type: String,
    description: 'id',
    default: 'in_1L3ClMFSgVYpzNjnbnLg1k7e'
  })
  id: string

  @ApiProperty({
    type: Number,
    description: 'amount_paid',
    default: 300
  })
  amount_paid: number

  @ApiProperty({
    type: Number,
    description: 'amount_remaining',
    default: 0
  })
  amount_remaining: number

  @ApiProperty({
    type: String,
    description: 'collection_method',
    default: 'charge_automatically'
  })
  collection_method: string

  @ApiProperty({
    type: String,
    description: 'invoice_pdf',
    default: 'https://pay.stripe.com/invoice/acct_1KThJAFSgVYpzNjn/test_YWNjdF8xS1RoSkFGU2dWWXB6TmpuLF9Ma2lCc3J4dXRQME1XYkVVaUxDRWJtT3ZBcXJackxYLDQ0MDk0Mjk20200oB51XpIp/pdf?s=ap'
  })
  invoice_pdf: string

  @ApiProperty({
    type: String,
    description: 'number',
    default: '90E35B09-0001'
  })
  number: string

  @ApiProperty({
    type: Number,
    description: 'period_start',
    default: 1653456484
  })
  period_start: number

  @ApiProperty({
    type: Number,
    description: 'period_end',
    default: 1653456484
  })
  period_end: number

  @ApiProperty({
    type: Number,
    description: 'due_date',
    default: null
  })
  due_date: number

  @ApiProperty({
    type: Boolean,
    description: 'paid',
    default: true
  })
  paid: boolean

  @ApiProperty({
    type: Number,
    description: 'paid_at',
    default: 1653456484
  })
  paid_at: number

  @ApiProperty({
    type: String,
    description: 'status',
    default: 'paid'
  })
  status: string

  @ApiProperty({
    type: Number,
    description: 'total',
    default: 300
  })
  total: number
}

export class TaxSet {
  @ApiProperty({
    type: Boolean,
    description: 'enabled',
    // default: 'string'
  })
  enabled: boolean

  @ApiProperty({
    type: Number,
    description: 'status',
    default: null
  })
  status: number
}

export class PeriodSet {
  @ApiProperty({
    type: Number,
    description: 'end',
    default: '1653811084'
  })
  end: number

  @ApiProperty({
    type: Number,
    description: 'start',
    default: '1653811084'
  })
  start: number
}

export class ProrationSet {
  @ApiProperty({
    type: String,
    description: 'credited_items',
    default: null
  })
  credited_items: string
}

export class PlanSet {
  @ApiProperty({
    type: String,
    description: 'id',
    default: 'price_1KdVsnFSgVYpzNjnUzRI80gL'
  })
  id: string

  @ApiProperty({
    type: String,
    description: 'object',
  })
  object: number

  @ApiProperty({
    type: Boolean,
    description: 'active',
  })
  active: boolean

  @ApiProperty({
    type: Number,
    description: 'aggregate_usage',
    default: null
  })
  aggregate_usage: number

  @ApiProperty({
    type: Number,
    description: 'amount',
    default: 300
  })
  amount: number

  @ApiProperty({
    type: Number,
    description: 'amount_decimal',
    default: 300
  })
  amount_decimal: number

  @ApiProperty({
    type: String,
    description: 'billing_scheme',
  })
  billing_scheme: string

  @ApiProperty({
    type: Number,
    description: 'created',
    default: 1647333453
  })
  created: number

  @ApiProperty({
    type: String,
    description: 'currency',
    default: 'sgd'
  })
  currency: string

  @ApiProperty({
    type: String,
    description: 'interval',
    default: 'day'
  })
  interval: string

  @ApiProperty({
    type: Number,
    description: 'interval_count',
  })
  interval_count: number

  @ApiProperty({
    type: Boolean,
    description: 'livemode',
  })
  livemode: boolean

  @ApiProperty({
    type: Object,
    description: 'metadata',
  })
  metadata: object

  @ApiProperty({
    type: String,
    description: 'nickname',
    default: null
  })
  nickname: string

  @ApiProperty({
    type: String,
    description: 'product',
    default: 'prod_LALXE85NL8ZM4U'
  })
  product: string

  @ApiProperty({
    type: String,
    description: 'tiers_mode',
    default: null
  })
  tiers_mode: string

  @ApiProperty({
    type: String,
    description: 'transform_usage',
    default: null
  })
  transform_usage: string

  @ApiProperty({
    type: Number,
    description: 'trial_period_days',
    default: null
  })
  trial_period_days: number

  @ApiProperty({
    type: String,
    description: 'usage_type',
    default: 'licensed'
  })
  usage_type: string
}

export class RecurringSet {
  @ApiProperty({
    type: String,
    description: 'aggregate_usage',
    default: null
  })
  aggregate_usage: string

  @ApiProperty({
    type: String,
    description: 'interval',
    default: 'day'
  })
  interval: string

  @ApiProperty({
    type: Number,
    description: 'interval_count',
    default: 3
  })
  interval_count: number

  @ApiProperty({
    type: Number,
    description: 'trial_period_days',
    default: null
  })
  trial_period_days: number

  @ApiProperty({
    type: String,
    description: 'usage_type',
    default: 'licensed'
  })
  usage_type: string
}

export class PriceSet {
  @ApiProperty({
    type: String,
    description: 'id',
    default: 'price_1KdVsnFSgVYpzNjnUzRI80gL'
  })
  id: string

  @ApiProperty({
    type: String,
    description: 'object',
    default: 'price'
  })
  object: string

  @ApiProperty({
    type: Boolean,
    description: 'active',
  })
  active: boolean

  @ApiProperty({
    type: String,
    description: 'billing_scheme',
    default: 'per_unit'
  })
  billing_scheme: string

  @ApiProperty({
    type: String,
    description: 'created',
    default: '1647333453'
  })
  created: string

  @ApiProperty({
    type: String,
    description: 'currency',
    default: 'sgd'
  })
  currency: string

  @ApiProperty({
    type: Boolean,
    description: 'livemode',
  })
  livemode: boolean

  @ApiProperty({
    type: String,
    description: 'lookup_key',
    default: null
  })
  lookup_key: string

  @ApiProperty({
    type: Object,
    description: 'metadata',
  })
  metadata: object

  @ApiProperty({
    type: String,
    description: 'nickname',
    default: null
  })
  nickname: string

  @ApiProperty({
    type: String,
    description: 'product',
    default: 'prod_LALXE85NL8ZM4U'
  })
  product: string

  @ApiProperty({
    description: "price list",
    type: [RecurringSet],
  })
  recurring: RecurringSet[];

  @ApiProperty({
    type: String,
    description: 'tax_behavior',
    default: 'unspecified'
  })
  tax_behavior: string

  @ApiProperty({
    type: String,
    description: 'tiers_mode',
    default: null
  })
  tiers_mode: string

  @ApiProperty({
    type: String,
    description: 'transform_quantity',
    default: null
  })
  transform_quantity: string


  @ApiProperty({
    type: String,
    description: 'type',
    default: 'recurring'
  })
  type: string

  @ApiProperty({
    type: Number,
    description: 'unit_amount',
    default: 300
  })
  unit_amount: number

  @ApiProperty({
    type: String,
    description: 'unit_amount_decimal',
    default: '300'
  })
  unit_amount_decimal: string
}

export class DataSet {
  @ApiProperty({
    type: String,
    description: 'id',
    default: 'il_1L3bf3FSgVYpzNjnEuccpGNw'
  })
  id: string

  @ApiProperty({
    type: String,
    description: 'object',
  })
  object: string

  @ApiProperty({
    type: Number,
    description: 'id',
    default: '300'
  })
  amount: number

  @ApiProperty({
    type: String,
    description: 'currency',
    default: 'sgd'
  })
  currency: string
  
  @ApiProperty({
    type: String,
    description: 'description',
  })
  description: string

  @ApiProperty({
    type: [String],
    description: 'discount_amounts',
    default: []
  })
  discount_amounts: string[]

  @ApiProperty({
    type: Boolean,
    description: 'discountable',
    default: 'il_1L3bf3FSgVYpzNjnEuccpGNw'
  })
  discountable: boolean

  @ApiProperty({
    type: [Number],
    description: 'discounts',
    default: []
  })
  discounts: number[]

  @ApiProperty({
    type: Boolean,
    description: 'livemode',
  })
  livemode: boolean

  @ApiProperty({
    type: Object,
    description: 'metadata',
  })
  metadata: object

  @ApiProperty({
    description: "period list",
    type: [PeriodSet],
  })
  period: PeriodSet[];

  @ApiProperty({
    description: "plan list",
    type: [PlanSet],
  })
  plan: PlanSet[];

  @ApiProperty({
    description: "price list",
    type: [PriceSet],
  })
  price: PriceSet[];

  @ApiProperty({
    type: Boolean,
    description: 'proration',
  })
  proration: boolean

  @ApiProperty({
    description: "Proration details",
    type: [ProrationSet],
  })
  proration_details: ProrationSet[];

  @ApiProperty({
    type: Number,
    description: 'quantity',
    default: 1
  })
  quantity: number

  @ApiProperty({
    type: String,
    description: 'subscription',
    default: 'sub_1KhBHCFSgVYpzNjn6A2lAylx'
  })
  subscription: string

  @ApiProperty({
    type: String,
    description: 'subscription_item',
    default: 'si_LNxBGTJxyA4K18'
  })
  subscription_item: string

  @ApiProperty({
    type: [Number],
    description: 'tax_amounts',
    default: []
  })
  tax_amounts: number[]

  @ApiProperty({
    type: [Number],
    description: 'tax_rates',
    default: []
  })
  tax_rates: number[] 

  @ApiProperty({
    type: String,
    description: 'type',
    default: 'subscription'
  })
  type: string
}

export class PaymentSet {
  @ApiProperty({
    type: String,
    description: 'payment_method_options',
    default: null
  })
  payment_method_options: string

  @ApiProperty({
    type: String,
    description: 'payment_method_types',
    default: null
  })
  payment_method_types: string
}

export class LinesSet {
  @ApiProperty({
    type: String,
    description: 'object',
    default: 'list'
  })
  object: string

  @ApiProperty({
    description: "Users list",
    type: [DataSet],
  })
  data: DataSet[];

  @ApiProperty({
    type: Boolean,
    description: 'has_more',
    default: 'in_1L3bf3FSgVYpzNjnlpwYq2zY'
  })
  has_more: boolean

  @ApiProperty({
    type: Number,
    description: 'total_count',
    default: 1
  })
  id: number

  @ApiProperty({
    type: String,
    description: 'url',
    default: '/v1/invoices/in_1L3bf3FSgVYpzNjnlpwYq2zY/lines'
  })
  url: string
}

export class TransitionsSet {
  @ApiProperty({
    type: String,
    description: 'finalized_at',
    default: null
  })
  finalized_at: string

  @ApiProperty({
    type: String,
    description: 'marked_uncollectible_at',
    default: null
  })
  marked_uncollectible_at: string

  @ApiProperty({
    type: String,
    description: 'paid_at',
    default: null
  })
  paid_at: string

  @ApiProperty({
    type: String,
    description: 'voided_at',
    default: null
  })
  voided_at: string
}

export class SubInvoiceResDto {
  @ApiProperty({
    type: String,
    description: 'id',
    default: 'in_1L3bf3FSgVYpzNjnlpwYq2zY'
  })
  id: string

  @ApiProperty({
    type: String,
    description: 'object',
    // default: 'string'
  })
  object: string

  @ApiProperty({
    type: String,
    description: 'account_country',
    default: 'SG'
  })
  account_country: string

  @ApiProperty({
    type: String,
    description: 'account_name',
    default: 'string'
  })
  account_name: string

  @ApiProperty({
    type: String,
    description: 'account_tax_ids',
    default: null
  })
  account_tax_ids: string

  @ApiProperty({
    type: Number,
    description: 'amount_due',
    // default: null
  })
  amount_due: number

  @ApiProperty({
    type: Number,
    description: 'amount_paid',
    // default: null
  })
  amount_paid: number

  @ApiProperty({
    type: Number,
    description: 'amount_remaining',
    // default: null
  })
  amount_remaining: number

  @ApiProperty({
    type: String,
    description: 'application',
    default: null
  })
  application: string

  @ApiProperty({
    type: Number,
    description: 'application_fee_amount',
    default: null
  })
  application_fee_amount: number

  @ApiProperty({
    type: Number,
    description: 'attempt_count',
    default: 0
  })
  attempt_count: number

  @ApiProperty({
    type: Boolean,
    description: 'attempted',
    default: false
  })
  attempted: boolean

  @ApiProperty({
    type: Boolean,
    description: 'auto_advance',
    default: true
  })
  auto_advance: boolean

  @ApiProperty({
    description: "automatic tax",
    type: TaxSet,
  })
  automatic_tax: TaxSet;
  
  @ApiProperty({
    type: String,
    description: 'billing_reason',
  })
  billing_reason: string

  @ApiProperty({
    type: String,
    description: 'charge',
    default: null
  })
  charge: string

  @ApiProperty({
    type: String,
    description: 'collection_method',
  })
  collection_method: string

  @ApiProperty({
    type: Number,
    description: 'created',
    default: 1653552193
  })
  created: number

  @ApiProperty({
    type: String,
    description: 'currency',
    default: 'sgd'
  })
  currency: string

  @ApiProperty({
    type: String,
    description: 'custom_fields',
    default: null
  })
  custom_fields: string

  @ApiProperty({
    type: String,
    description: 'customer',
    default: 'cus_LNx0g8uQ7qmPjt'
  })
  customer: string

  @ApiProperty({
    type: String,
    description: 'customer_address',
    default: null
  })
  customer_address: string

  @ApiProperty({
    type: String,
    description: 'customer_email',
  })
  customer_email: string

  @ApiProperty({
    type: String,
    description: 'customer_name',
  })
  customer_name: string

  @ApiProperty({
    type: String,
    description: 'customer_phone',
    default: null
  })
  customer_phone: string

  @ApiProperty({
    type: String,
    description: 'customer_shipping',
    default: null
  })
  customer_shipping: string

  @ApiProperty({
    type: String,
    description: 'customer_tax_exempt',
    default: 'none'
  })
  customer_tax_exempt: string

  @ApiProperty({
    type: [String],
    description: 'customer_tax_ids',
    default: null
  })
  customer_tax_ids: string[]

  @ApiProperty({
    type: String,
    description: 'default_payment_method',
    default: null
  })
  default_payment_method: string

  @ApiProperty({
    type: String,
    description: 'default_source',
    default: null
  })
  default_source: string

  @ApiProperty({
    type: [String],
    description: 'default_tax_rates',
    default: null
  })
  default_tax_rates: string[]

  @ApiProperty({
    type: String,
    description: 'description',
    default: null
  })
  description: string

  @ApiProperty({
    type: String,
    description: 'discount',
    default: null
  })
  discount: string

  @ApiProperty({
    type: [String],
    description: 'discounts',
    default: null
  })
  discounts: string[]

  @ApiProperty({
    type: Number,
    description: 'due_date',
    default: null
  })
  due_date: number

  @ApiProperty({
    type: String,
    description: 'footer',
    default: null
  })
  footer: string

  @ApiProperty({
    type: String,
    description: 'hosted_invoice_url',
    default: null
  })
  hosted_invoice_url: string

  @ApiProperty({
    type: String,
    description: 'invoice_pdf',
    default: null
  })
  invoice_pdf: string

  @ApiProperty({
    type: String,
    description: 'last_finalization_error',
    default: null
  })
  last_finalization_error: string

  @ApiProperty({
    description: "Users list",
    type: LinesSet,
  })
  lines: LinesSet;


  @ApiProperty({
    type: Boolean,
    description: 'livemode',
    default: null
  })
  livemode: boolean

  @ApiProperty({
    type: Object,
    description: 'metadata',
  })
  metadata: object

  @ApiProperty({
    type: Number,
    description: 'next_payment_attempt',
    default: null
  })
  next_payment_attempt: number

  @ApiProperty({
    type: Number,
    description: 'number',
    default: null
  })
  number: number

  @ApiProperty({
    type: String,
    description: 'on_behalf_of',
    default: null
  })
  on_behalf_of: string

  @ApiProperty({
    type: Boolean,
    description: 'paid',
  })
  paid: boolean

  @ApiProperty({
    type: Boolean,
    description: 'paid_out_of_band',
  })
  paid_out_of_band: boolean

  @ApiProperty({
    type: String,
    description: 'payment_intent',
    default: null
  })
  payment_intent: string

  @ApiProperty({
    description: "Users list",
    type: PaymentSet,
  })
  payment_settings: PaymentSet;

  @ApiProperty({
    type: Number,
    description: 'period_end',
    default: 1653551884
  })
  period_end: number

  @ApiProperty({
    type: Number,
    description: 'period_start',
    default: 1653292684
  })
  period_start: number

  @ApiProperty({
    type: Number,
    description: 'post_payment_credit_notes_amount',
    default: 0
  })
  post_payment_credit_notes_amount: number

  @ApiProperty({
    type: Number,
    description: 'pre_payment_credit_notes_amount',
    default: 0
  })
  pre_payment_credit_notes_amount: number

  @ApiProperty({
    type: String,
    description: 'quote',
    default: null
  })
  quote: string

  @ApiProperty({
    type: Number,
    description: 'receipt_number',
    default: null
  })
  receipt_number: number

  @ApiProperty({
    type: Boolean,
    description: 'starting_balance',
  })
  starting_balance: boolean

  @ApiProperty({
    type: String,
    description: 'statement_descriptor',
    default: null
  })
  statement_descriptor: string

  @ApiProperty({
    type: String,
    description: 'status',
  })
  status: string
  
  @ApiProperty({
    description: "Users list",
    type: TransitionsSet,
  })
  status_transitions: TransitionsSet;

  @ApiProperty({
    type: String,
    description: 'subscription',
    default: 'sub_1KhBHCFSgVYpzNjn6A2lAylx'
  })
  subscription: string

  @ApiProperty({
    type: Number,
    description: 'subtotal',
    default: 300
  })
  subtotal: number

  @ApiProperty({
    type: Number,
    description: 'tax',
    default: null
  })
  tax: number

  @ApiProperty({
    type: String,
    description: 'test_clock',
    default: null
  })
  test_clock: string

  @ApiProperty({
    type: Number,
    description: 'total',
    default: 300
  })
  total: number

  @ApiProperty({
    type: [Number],
    description: 'total_discount_amounts',
    default: []
  })
  total_discount_amounts: number

  @ApiProperty({
    type: [Number],
    description: 'total_tax_amounts',
    default: []
  })
  total_tax_amounts: number

  @ApiProperty({
    type: String,
    description: 'transfer_data',
    default: null
  })
  transfer_data: string

  @ApiProperty({
    type: Number,
    description: 'webhooks_delivered_at',
    default: 1653552193
  })
  webhooks_delivered_at: number
}


export class AdminRefreshTokenResDto {
  @ApiProperty({
    type: String,
    description: 'access_token',
  })
  access_token: string

  @ApiProperty({
    type: Number,
    description: 'expires_in',
    default: 1800
  })
  expires_in: number

  @ApiProperty({
    type: String,
    description: 'refresh_token',
  })
  refresh_token: string
  
  @ApiProperty({
    type: Number,
    description: 'refresh_expires_in',
    default: 1800
  })
  refresh_expires_in: number
}

export class AddCampaignResDto {
  @ApiProperty({
    type: String,
    description: 'message',
    default: 'Campaign added successfully'
  })
  message: string
}

export class CampaignSet {
  @ApiProperty({
    type: Number,
    description: 'id',
    default: 1
  })
  id: number

  @ApiProperty({
    type: String,
    description: 'referal_code',
  })
  referal_code: string

  @ApiProperty({
    type: Number,
    description: 'usage_limt',
    default: 5
  })
  usage_limt: number

  @ApiProperty({
    type: Date,
    description: 'start_date',
  })
  start_date: Date

  @ApiProperty({
    type: Date,
    description: 'end_date',
  })
  end_date: Date

  @ApiProperty({
    type: Number,
    description: 'status',
    default: 1
  })
  status: string

  @ApiProperty({
    type: String,
    description: 'created_user_id',
  })
  created_user_id: string

  @ApiProperty({
    type: String,
    description: 'updated_user_id',
  })
  updated_user_id: string

  @ApiProperty({
    type: String,
    description: 'created_at',
  })
  created_at: string

  @ApiProperty({
    type: String,
    description: 'updated_at',
  })
  updated_at: string
}

export class AllCampaignResDto {
  @ApiProperty({
    description: "Users list",
    type: [CampaignSet],
  })
  data: CampaignSet[];
}

export class UpdateCampaignResDto {
  @ApiProperty({
    type: String,
    description: 'Suucess message',
    default: 'Campaign detail updated successfully'
  })
  message: string

  @ApiProperty({
    description: "Updated campaign list",
    type: CampaignSet,
  })
  data: CampaignSet;
}

export class DeleteCampaignResDto {
  @ApiProperty({
    type: String,
    description: 'Suucess message',
    default: 'Campaign deleted successfully'
  })
  message: string
}

export class CobrokeStatuResDto {
  @ApiProperty({
    type: String,
    description: 'Suucess message',
    default: 'CoBroke status has been changed successfully'
  })
  message: string
}

export class ContactSet {
  @ApiProperty({
    type: Number,
    description: 'id',
    default: 1
  })
  id: number

  @ApiProperty({
    type: String,
    description: 'name',
  })
  name: string

  @ApiProperty({
    type: String,
    description: 'email',
  })
  email: string

  @ApiProperty({
    type: String,
    description: 'message',
  })
  message: string

  @ApiProperty({
    type: String,
    description: 'created_user_id',
  })
  created_user_id: string

  @ApiProperty({
    type: String,
    description: 'updated_user_id',
  })
  updated_user_id: string

  @ApiProperty({
    type: String,
    description: 'created_at',
  })
  created_at: string

  @ApiProperty({
    type: String,
    description: 'updated_at',
  })
  updated_at: string
}

export class AllContactusResDto {
  @ApiProperty({
    description: "Contact us list",
    type: [ContactSet],
  })
  data: ContactSet[];
}

export class AdminContactListResDto {
  @ApiProperty({
    description: "Contact list",
    type: ContactSet,
  })
  data: ContactSet;
}

export class ExportItem {
  @ApiProperty({
    type: Object,
    description: 'Export Item',
    default: '["all"]',
  })
  // @IsNotEmpty({ message: 'Atleast one field is must' })
  exportItem: string[];
}

  // export user summary details to csv
export class ExportSubscriptionItem {
  @ApiProperty({
    type: Number,
    description: 'startMonth',
    default: '2',
  })
  @IsNotEmpty({ message: 'startMonth should not be empty' })
  startMonth: number;

  @ApiProperty({
    type: Number,
    description: 'startYear',
    default: '2022',
  })
  @IsNotEmpty({ message: 'startYear should not be empty' })
  startYear: number;

  @ApiProperty({
    type: Number,
    description: 'endMonth',
    default: '6',
  })
  @IsNotEmpty({ message: 'endMonth should not be empty' })
  endMonth: number;

  @ApiProperty({
    type: Number,
    description: 'endYear',
    default: '2022',
  })
  @IsNotEmpty({ message: 'endYear should not be empty' })
  endYear: number;
}

// export user summary details to csv
export class ExportSRevennueItem {
  @ApiProperty({
    type: Number,
    description: 'startMonth',
    default: '2',
  })
  @IsNotEmpty({ message: 'startMonth should not be empty' })
  startMonth: number;

  @ApiProperty({
    type: Number,
    description: 'startYear',
    default: '2022',
  })
  @IsNotEmpty({ message: 'startYear should not be empty' })
  startYear: number;

  @ApiProperty({
    type: Number,
    description: 'endMonth',
    default: '6',
  })
  @IsNotEmpty({ message: 'endMonth should not be empty' })
  endMonth: number;

  @ApiProperty({
    type: Number,
    description: 'endYear',
    default: '2022',
  })
  @IsNotEmpty({ message: 'endYear should not be empty' })
  endYear: number;

  @ApiProperty({
    type: String,
    description: 'Token value',
    default: '',
  })
  @IsNotEmpty({ message: 'Token should not be empty' })
  token: string;
}

export class DefaultUpdateAdminDto {
  @ApiProperty({
    type: String,
    description: 'Full name',
    default: 'Jackson',
  })
  @IsDefined()
  @IsNotEmpty({ message: 'Full name should not be empty' })
  full_name: string;

  @ApiProperty({
    type: String,
    description: 'Mobil number',
    default: '9884567345',
  })
  @IsDefined()
  @IsNotEmpty({ message: 'Mobile number should not be empty' })
  mobile: string;

  @ApiProperty({
    type: String,
    description: 'country_code',
    default: '65',
  })
  @IsNotEmpty({ message: 'Country code should not be empty' })
  @Length(1, 5, {
    message: 'Enter valid country code',
  })
  country_code: string;

  @ApiProperty({
    type: String,
    description: 'country_code_label',
    default: 'SG',
  })
  @IsNotEmpty({ message: 'Country code label should not be empty' })
  @Length(1, 5, {
    message: 'Enter valid country  label',
  })
  country_code_label: string;

  @ApiProperty({
    type: String,
    description: 'Email Address',
    default: 'jackson@gmail.com',
  })
  @IsDefined()
  @IsNotEmpty({ message: 'Email address should not be empty' })
  email: string;

  @ApiProperty({
    type: Number,
    description: 'Status (0=blocked, 1=active)',
    default: '1',
  })
  @IsNotEmpty({ message: 'Status should not be empty' })
  @IsEnum(StatusEnum)
  @IsInt()
  status: number;

  @ApiProperty({
    type: String,
    description: 'Password',
    default: 'roy',
  })
  password?: string;
}

export class AdminDto {
  @ApiProperty({
    type: String,
    description: 'Fullname',
  })
  @IsNotEmpty({ message: 'Full name should not be empty' })
  @Length(2, 255)
  full_name: string;

  @ApiProperty({
    type: String,
    description: 'Email',
    default: 'roy@gmail.com',
  })
  @IsEmail({}, { message: 'Please enter a valid email id' })
  @IsNotEmpty({ message: 'Email should not be empty' })
  email: string;

  @ApiProperty({
    type: String,
    description: 'country_code',
    default: '65',
  })
  @IsNotEmpty({ message: 'Country code should not be empty' })
  @Length(1, 5, {
    message: 'Enter valid country code',
  })
  country_code: string;

  @ApiProperty({
    type: String,
    description: 'country_code_label',
    default: 'SG',
  })
  @IsNotEmpty({ message: 'Country code label should not be empty' })
  @Length(1, 5, {
    message: 'Enter valid country  label',
  })
  country_code_label: string;

  @ApiProperty({
    type: String,
    description: 'mobile',
    default: '1234123456',
  })
  @IsNotEmpty({ message: 'Mobile should not be empty' })
  @Length(8, 255, {
    message: 'Enter valid phone number with country code',
  })
  mobile: string;

  @ApiProperty({
    type: String,
    description: 'Password',
    default: 'roy',
  })
  @IsNotEmpty({ message: 'Password should not be empty' })
  @Length(8, 255)
  password: string;

  @ApiProperty({
    type: Number,
    description: 'Status',
    default: 1,
  })
  @IsNotEmpty({ message: 'Status should not be empty' })
  @IsIn([1], { message: 'Status must be Active only' })
  status: number;
}

export class AdminEmailDto {
  @ApiProperty({
    type: String,
    description: 'Fullname',
  })
  @IsNotEmpty({ message: 'Full name should not be empty' })
  full_name: string;

  @ApiProperty({
    type: String,
    description: 'Email',
  })
  @IsNotEmpty({ message: 'Email should not be empty' })
  @IsEmail({}, { message: 'Please enter a valid email id' })
  email: string;

  @ApiProperty({
    type: String,
    description: 'Password',
  })
  @IsNotEmpty({ message: 'Password should not be empty' })
  pswrd: string;

  @ApiProperty({
    type: String,
    description: 'template For',
  })
  @IsNotEmpty({ message: 'Template for should not be empty' })
  templateFor: string;
}

export class DefaultSetAdminRole {
  @ApiProperty({
    type: String,
    description: 'Set role',
    default: 'SET',
  })
  @IsNotEmpty({ message: 'Set role should not be empty' })
  @IsEnum(SetRoleName)
  role_status: string;

  @ApiProperty({
    type: String,
    description: 'Role name',
    default: 'VIEW_DASHBOARD',
  })
  @IsNotEmpty({ message: 'Role name should not be empty' })
  role_name: string;
}

export class DefaultUpdateMyProfileDto {
  @ApiProperty({
    type: String,
    description: 'Full name',
    default: 'Jackson',
  })
  @IsDefined()
  @IsNotEmpty({ message: 'Full name should not be empty' })
  full_name: string;

  @ApiProperty({
    type: String,
    description: 'Mobil number',
    default: '9884567345',
  })
  @IsDefined()
  @IsNotEmpty({ message: 'Mobile number should not be empty' })
  mobile: string;

  @ApiProperty({
    type: String,
    description: 'country_code',
    default: '65',
  })
  @IsNotEmpty({ message: 'Country code should not be empty' })
  @Length(1, 5, {
    message: 'Enter valid country code',
  })
  country_code: string;

  @ApiProperty({
    type: String,
    description: 'country_code_label',
    default: 'SG',
  })
  @IsNotEmpty({ message: 'Country code label should not be empty' })
  @Length(1, 5, {
    message: 'Enter valid country  label',
  })
  country_code_label: string;

  @ApiProperty({
    type: String,
    description: 'Password',
    default: '',
  })
  password?: string;
}

export class DeleteUserResDto {
  @ApiProperty({
    type: String,
    description: 'Suucess message',
    default: 'User deleted successfully'
  })
  message: string
}

export class reinstateUserResDto {
  @ApiProperty({
    type: String,
    description: 'Suucess message',
    default: 'User has been reinstated successfully'
  })
  message: string
}

export class AddPushNotifyResDto {
  @ApiProperty({
    type: String,
    description: 'message',
    default: 'Push notification added successfully'
  })
  message: string
}

export class PushNotifyDto {
  @ApiProperty({
    type: String,
    description: 'Title',
  })
  @IsNotEmpty({ message: 'Title should not be empty' })
  title: string

  @ApiProperty({
    type: String,
    description: 'message',
  })
  @IsNotEmpty({ message: 'Message role should not be empty' })
  message: string

  @ApiProperty({
    type: Object,
    description: 'Target audience',
    default: '["basic","premium"]',
  })
  target_audience?: string[];

  @ApiProperty({
    type: Date,
    description: 'Notification date',
    default: '2022-09-26',
  })
  @IsNotEmpty({ message: 'Notification date should not be empty' })
  notify_date: Date;

  @ApiProperty({
    type: String,
    description: 'Notification time',
  })
  @IsNotEmpty({ message: 'Notification time should not be empty' })
  notify_time: string;

  @ApiProperty({
    type: String,
    description: 'Notification date with time',
    default: '2022-09-26T09:01:14.253Z',
  })
  @IsNotEmpty({ message: 'Notification date with time should not be empty' })
  notify_date_time: string;
}

export class SendPushNotifyDto {
  @ApiProperty({
    type: String,
    description: 'Device id',
  })
  @IsNotEmpty({ message: 'Device id should not be empty' })
  device_id: string;

  @ApiProperty({
    type: String,
    description: 'User id',
  })
  @IsNotEmpty({ message: 'User id should not be empty' })
  user_id: string;

  @ApiProperty({
    type: String,
    description: 'Membership type',
  })
  @IsNotEmpty({ message: 'Membership type should not be empty' })
  membership_type: string;
}

export class SendPushNotifyResDto {
  @ApiProperty({
    type: String,
    description: 'message',
    default: 'User device Id added/updated successfully'
  })
  message: string
}