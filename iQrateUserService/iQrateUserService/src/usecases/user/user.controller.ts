import {
  Controller,
  Get,
  Post,
  Body,
  Headers,
  Put,
  Param,
  Patch,
  Req,
  HttpException,
  HttpStatus, Res,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import Debug from 'debug';
// import { conditionalExpression } from '@babel/types';
import { Request } from 'express';
// import { Roles, Unprotected } from 'nest-keycloak-connect';
import { UserService } from './user.service';
import {
  DefaultOccupationDto,
  DefaultBankDto,
  UserEmailDto,
  UserMobileDto,
  DefaultChgPswrdDto,
  DefaultUpdateNamePhone,
  ContactUsDto,
  UserMobileResDto,
  UserDetailResDto,
  Disable2FAResDto,
  Chk2FAStatusResDto,
  UpdateOccupationResDto,
  AddBankdetailResDto,
  DeleteBankdetailResDto,
  UpdateBankdetailResDto,
  ChgEmailResDto,
  ChgPassResDto,
  UpdateNameDobResDto,
  ContactUSResDto,
  DeleteUserResDto,
  SendPushNotifyDto,
  UserRefreshTokenResDto,
  SendPushNotifyResDto,
} from './dto/user.dto';
import {TwoFactorAuthenticationService} from "./twoFactorAuthentication.service";
import { MemberShipAclService } from 'src/middlewares/acl_module/membership_acl_service';
import { ConfigService } from '../../config/config.service';



const debug = Debug('user-service:UserController');

@ApiTags('Users')
@Controller('user')
export class UserController {
  constructor(private readonly membership_acl : MemberShipAclService , private readonly userService: UserService,private readonly twoFactorAuthenticationService:TwoFactorAuthenticationService, private config: ConfigService) {}

  @Get('/')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: UserDetailResDto,
  })
  @ApiOkResponse({ description: 'all users has been returned' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async fetchAll(@Req() req: Request) {
    try {
      debug('headers : ', req.headers);

      if(req.headers.usertype==='agent' && req.headers.userid) {
        const profileRes = await this.userService.getFullProfile(req);
        // profileRes.data['appid'] = 'sdfsdf';
        profileRes.data['appId'] = this.config.envConfig.ONE_SIGNAL_APP_ID;
        const acl_datas = await this.membership_acl.getAclList(profileRes.data['membership_type']);
        if(profileRes.status===1) {
          delete profileRes.data['password'];
          delete profileRes.data['otp'];
          delete profileRes.data['token'];
          delete profileRes.data['agreed_term'];
          delete profileRes.data['verification_code'];
          delete profileRes.data['is_verified'];
          delete profileRes.data['resendcode_email_count'];
          delete profileRes.data['resendcode_sms_count'];
          delete profileRes.data['refresh_token'];
          profileRes.data['acl_data'] = acl_datas;
          // delete profileRes.data['is_email_verified'];
          // delete profileRes.data['is_mobile_verified'];
          // delete profileRes.data['bankers'][0]['id'];
          debug('dob:',profileRes.data['dob']);
          // profileRes.data['dob'] = profileRes.data['dob'].split('-').reverse().join('-'); // to reverse dob
          debug('reversed dob:',profileRes.data['dob']);
          return profileRes.data;
        }
      }
    } catch(err) {
      if(err && err.response && err.response.status>=400 && err.response.status<500) {
        throw new HttpException(err.response.data,err.response.status);
      } else {
        console.error('login err : ', err);
        throw err;
      }
    }
    const builder: any = await this.userService.showUserBasedOnsrch('users', req);
  } 

  // send verification code to specific user email id
  @Put('sendCode/:email')
  @ApiOkResponse({ description: 'code has been sent' })
  @ApiNotFoundResponse({ description: 'not found' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async sendCode(@Param('email') email: string) {
    return this.userService.sendVerificationLinkWithCode(email);
  }

  //Enable with Authenticator
  @Post('enable_mfa')
  @ApiBearerAuth()
  async mfa_register(@Res() response: any, @Headers() headers:any) {
    if(!headers['userid']) {
      throw new HttpException({message:"Invalid Token",name:"USERID_NOT_FOUND"},HttpStatus.UNAUTHORIZED);
    }
    const { secret,otpauthUrl } = await this.twoFactorAuthenticationService.generateTwoFactorAuthenticationSecret(headers['userid']);
    // otpauthUrl.

    if(headers['content-type'] && headers['content-type'] === 'application/json') {
      response.send({secret,otpauthUrl});
      return;
    } else {
      return this.twoFactorAuthenticationService.pipeQrCodeStream(response, otpauthUrl);
    }
  }

  // Add or update occupation w.r.t user id
  @Put('chk2FAstatus/:mfaotp')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: Chk2FAStatusResDto,
  })
  @ApiOkResponse({ description: '2FA status' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async chk2FAstatus(
      @Headers() headers: Headers,
      @Param('mfaotp') mfaotp: string
  ) {
    const userId = headers['userid'];
    const userObj: any = {id:userId};
    if(!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message:'Token is invalid'
      },HttpStatus.UNAUTHORIZED);
    }
    const mfaSattus = await this.twoFactorAuthenticationService.isTwoFactorAuthenticationCodeValid(mfaotp, userObj);
    if (mfaSattus === false) {
      throw new HttpException({
        message: "Wrong authorization code"
      },HttpStatus.UNAUTHORIZED);
    } 
    return await this.userService.enablemfa(userObj.id);
  }

   // START set isTwoFactorAuthenticationEnabled: false
   @Put('disable2FA')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: Disable2FAResDto,
  })
  @ApiOkResponse({
    description: 'Disable google 2FA',
  })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async disable2FA(@Headers() headers: Headers) {
    //  debug("Entering Register controller...");
    const userId = headers['userid'];
    if(!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message:'Token is invalid'
      },HttpStatus.UNAUTHORIZED);
    }
    return this.twoFactorAuthenticationService.disable2FA(userId);
  }

  // Add or update occupation w.r.t user id
  @Put('updateOccupation')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: UpdateOccupationResDto
  })

  @ApiOkResponse({ description: 'User occupation has been updated' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async updateOccupation(
      @Headers() headers: Headers,
    @Body() defaultVal: DefaultOccupationDto,
  ) {
    //  debug("Entering Register controller...");
    const userId = headers['userid'];
    if(!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message:'Token is invalid'
      },HttpStatus.UNAUTHORIZED);
    }
    return this.userService.updateOccupation(userId, defaultVal);
  }

  // Add bank details w.r.t user id
  @Post('addBankdetail')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: AddBankdetailResDto,
  })
  @ApiOkResponse({ description: 'User bank detail has been added' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async addBankDet(
      @Headers() headers: Headers,
    @Body() defaultVal: DefaultBankDto,
  ) {
    //  debug("Entering Register controller...");
    const userId = headers['userid'];
    if(!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message:'Token is invalid'
      },HttpStatus.UNAUTHORIZED);
    }
    return this.userService.addBankDet(userId, defaultVal);
  }

  // update bank details w.r.t user id
  @Put('updateBankdetail')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: UpdateBankdetailResDto,
  })
  @ApiOkResponse({ description: 'User bank detail has been updated' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async updateBankDet(
    @Headers() headers: Headers,
    @Body() defaultVal: DefaultBankDto,
  ) {
    //  debug("Entering Register controller...");
    const userId = headers['userid'];
    if(!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message:'Token is invalid'
      },HttpStatus.UNAUTHORIZED);
    }
    return this.userService.updateBankDet(userId, defaultVal);
  }

  // delete bank details w.r.t user id
  @Patch('deleteBankdetail')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: DeleteBankdetailResDto,
  })
  @ApiOkResponse({ description: 'Delete bank detail has been updated' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async deleteBankDet(@Headers() headers: Headers) {
    //  debug("Entering Register controller...");
    const userId = headers['userid'];
    if(!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message:'Token is invalid'
      },HttpStatus.UNAUTHORIZED);
    }
    return this.userService.deleteBankDet(userId);
  }

  // delete user details w.r.t user id
  @Patch('deleteUser')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: DeleteUserResDto,
  })
  @ApiOkResponse({ description: 'User deleted' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async deleteUser(@Headers() headers: Headers) {
    //  debug("Entering Register controller...");
    const userId = headers['userid'];
    if(!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message:'Token is invalid'
      },HttpStatus.UNAUTHORIZED);
    }
    return this.userService.deleteUser(userId);
  }

  // chnge email id w.r.t user id
  @Put('changeEmail')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: ChgEmailResDto,
  })
  @ApiOkResponse({ description: 'User email id has been changed' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async chgUserEmail(
      @Headers() headers: Headers,
    @Body() defaultVal: UserEmailDto,
  ) {
    const userId = headers['userid'];
    if(!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message:'Token is invalid'
      },HttpStatus.UNAUTHORIZED);
    }
    return this.userService.chgUserEmail(userId, defaultVal);
  }

  // chnge email id w.r.t user id
  @Put('changeMobile')
  @ApiCreatedResponse({
    type: UserMobileResDto,
  })
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'User mobile has been changed' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async chgUserMobile(
    @Headers() headers: Headers,
    @Body() defaultVal: UserMobileDto,
  ) {
    //  debug("Entering Register controller...");
    const userId = headers['userid'];
    if(!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message:'Token is invalid'
      },HttpStatus.UNAUTHORIZED);
    }
    return this.userService.chgUserMobile(userId, defaultVal);
  }
  
  @Put('changePassword')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: ChgPassResDto,
  })
  @ApiOkResponse({ description: 'User password has been changed' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async chgPassword(
    @Headers() headers: Headers,
    @Body() defaultVal: DefaultChgPswrdDto,
  ) {
    const userId = headers['userid'];
    if(!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message:'Token is invalid'
      },HttpStatus.UNAUTHORIZED);
    }
    //  debug("Entering Register controller...");
    return this.userService.chgPassword(userId, defaultVal);
  }

   // Change password based on user id
   @Put('updateNameDOB')
   @ApiBearerAuth()
   @ApiCreatedResponse({
    type: UpdateNameDobResDto,
  })
   @ApiOkResponse({ description: 'User profile has been updated' })
   @ApiForbiddenResponse({ description: 'forbidden' })
   async chgNamePhone(
     @Headers() headers: Headers,
     @Body() defaultVal: DefaultUpdateNamePhone,
   ) {
     const userId = headers['userid'];
     if(!userId) {
       new HttpException({
         name: "USERID_NOT_FOUND",
         message:'Token is invalid'
       },HttpStatus.UNAUTHORIZED);
     }
     //  debug("Entering Register controller...");
     return this.userService.chgNamePhone(userId, defaultVal);
   }

  // add contact us
  @Put('contactUs')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: ContactUSResDto,
  })
  @ApiOkResponse({ description: 'Added contact us' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async contactUs(
    @Headers() headers: Headers,
    @Body() defaultVal: ContactUsDto,
  ) {
    const userId = headers['userid'];
    if(!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message:'Token is invalid'
      },HttpStatus.UNAUTHORIZED);
    }

    const regexEmail = /^\w+([+\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    const emailVal = defaultVal.email;
    if (emailVal.match(regexEmail)) {
      return this.userService.contactUs(userId, defaultVal);
    } else {
      throw new HttpException({ message: "Invalid email id", name: "RESET_INVALID_EMAIL" }, 427);
    }
  }



  // Get user delay seconds for filling verification code
  @Get('sendDelaySeconds')
  @ApiOkResponse({ description: 'Returned delay seconds to send code' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async getDelaySeconds() {
    //  debug("Entering Register controller...");
    return this.userService.getDelaySeconds();
  }

  // Get user delay seconds for filling verification code
  @Get('maxResendTime')
  @ApiOkResponse({
    description: 'Returned maximum time to resend an email and sms',
  })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async maxResend() {
    //  debug("Entering Register controller...");
    return this.userService.maxResend();
  }

  // push notification
  @Post('sendPushNotification')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: SendPushNotifyResDto,
  })
  @ApiOkResponse({ description: 'Push notification' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async pushNotification( @Body() defaultVal: SendPushNotifyDto) {
    return this.userService.sendPushNotification(defaultVal);
  }
} // Main End