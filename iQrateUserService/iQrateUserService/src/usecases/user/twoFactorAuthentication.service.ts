import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { authenticator } from 'otplib';
import { UserEntity } from '../../entities/users';
import { UserService } from './user.service';
import { ConfigService } from '../../config/config.service';
import { toFileStream } from 'qrcode';


import Debug from 'debug';
const debug = Debug('user-service:TwoFactorAuthenticationService');

@Injectable()
export class TwoFactorAuthenticationService {
    constructor (
        private readonly usersService: UserService,
        private readonly config: ConfigService
    ) {}

    public async pipeQrCodeStream(stream: any, otpauthUrl: string) {
        return toFileStream(stream, otpauthUrl);
    }

    public async turnOnTwoFactorAuthentication(userId: string) {
        return UserEntity.update(userId, {
            isTwoFactorAuthenticationEnabled: true,
            updated_at: new Date(),
            updated_user_id: userId,
        });
    }

    public async isTwoFactorAuthenticationCodeValid(twoFactorAuthenticationCode: string, user: UserEntity) {
        if(!user.twoFactorAuthenticationSecret) {
            user = await UserEntity.findOne(user.id);
        }
        debug('user.twoFactorAuthenticationSecret : ', user.twoFactorAuthenticationSecret);
        debug('user.id __________: ', user.id);
        debug('twoFactorAuthenticationCode:',twoFactorAuthenticationCode);
        debug('user.twoFactorAuthenticationSecret',user.twoFactorAuthenticationSecret);
        return authenticator.verify({
            token: twoFactorAuthenticationCode,
            secret: user.twoFactorAuthenticationSecret
        });
    }

    public async generateTwoFactorAuthenticationSecret(userid:string) {
        const secret = authenticator.generateSecret();

        const user = await UserEntity.findOne(userid);

        const MFA_APP_NAME = this.config.envConfig.TWO_FACTOR_AUTHENTICATION_APP_NAME || "IQRATE";

        const otpauthUrl = authenticator.keyuri(user.email, MFA_APP_NAME, secret);

        await this.usersService.setTwoFactorAuthenticationSecret(secret, user.id);

        return {
            secret,
            otpauthUrl
        }
    }

    public async disable2FA(userId: string) {
        debug('enter disable2FA')
        const userExist = await UserEntity.findOne({ id:userId });
      if (!userExist) {
        debug('Record doesnot exist');
        // return 'Record doesnot exist';
        throw new HttpException({
          name : 'recordNotFound',
          message :  'Record doesnot exist'
        },HttpStatus.BAD_REQUEST);
      }

      if(userExist.deleted === 1) {
        throw new HttpException({
          name : 'ACCOUNT_DELETED',
          message :  'Account was deleted by the user'
        },HttpStatus.BAD_REQUEST);
      }

        const dbUpdate = await UserEntity.update(userId, {
            isTwoFactorAuthenticationEnabled: false,
            updated_at: new Date(),
            updated_user_id: userId,
        });
        debug('dbUpdate:',dbUpdate);
        if(dbUpdate){
            return {message:'2FA is disabled successfully'};
        }

    }
}