import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import Debug from 'debug';
import { HttpService } from '@nestjs/axios';
import { UserService } from './user.service';
import { ConfigService } from '../../config/config.service';
import { UserController } from './user.controller';
import { ConfigModule } from '../../config/config.module';
import { TwoFactorAuthenticationService } from './twoFactorAuthentication.service';

const debug = Debug('connector:UserController:Spec');

describe('UserController', () => {
  let userController: UserController;
  let twoFactorAuthenticationService: TwoFactorAuthenticationService;
  const userService = new UserService(new ConfigService(), new HttpService());

  beforeAll(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRootAsync({
          imports: [ConfigModule],
          useFactory: (config: ConfigService) => config.getTypeORMConfig(),
          inject: [ConfigService],
        }),
      ],
      controllers: [UserController],
      providers: [UserService, TwoFactorAuthenticationService],
    })
      .overrideProvider(UserService)
      .useValue(userService)
      .compile();

    userController = app.get<UserController>(UserController);
  });

  // it('should be defined', () => {
  //   expect(userController).toBeDefined();
  // });

  it('create a user', async () => {
      debug('TEST RESULT:1');
      const dto = {
        full_name: 'chaithra karanth',
        email: 'chaithrakaranth26@digitalprizm.net',
        country_code: '91',
        mobile: '8971170026',
        password: 'password',
        dob: '25-09-1988',
        agreed_term: 0,
        country_code_label: 'IN',
        referal_code:''
      };
      const testRes = await userService.register(dto, 0, 'manaul');
      debug('testRes:',testRes);
      expect(testRes.email).toEqual('chaithrakaranth26@digitalprizm.net');
      debug(testRes.id);
      const delres = await userService.delete(testRes.id);
      debug(`deleted item:${testRes.id}`);
      debug(delres);
  });

  it('Resend email', async () => {
    debug('TEST RESULT:2');
    // const dto = {
    //   email: 'vana@digitalprizm.net',
    //   password: 'password',
    //   twoFactorAuthenticationCode: '25-09-1988',
    // };
    const testRes = await userService.resendEmailWithCode('chaithra@digitalprizm.net', 'VerifyEmail');
    debug('testRes:',testRes);
    expect(testRes.message).toEqual('Verify email link has been sent to your email chaithra@digitalprizm.net');
});

it('Resend sms', async () => {
  debug('TEST RESULT 3:');
  const testRes = await userService.resendSmsWithCode('9189711702130');
  debug('testRes mobile:',testRes);
  expect(testRes.message).toEqual('Please verify the mobile');
});

it('forget password', async () => {
  debug('TEST RESULT 5:');
  const testRes = await userService.resendEmailWithCode('chaithra@digitalprizm.net', 'ResetPassword');
  debug('testRes resetpass:',testRes);
  expect(testRes.message).toEqual('We have sent a password recover instructions to your email address.');
});

it('Update reset password', async () => {
  debug('TEST RESULT:6');
  const dto = {
    password: 'password',
    token: 'eyJ2ZXJpZmljYXRpb25fdG9rZW4iOiJleUpoYkdjaU9pSkZVekkxTmlJc0luUjVjQ0k2SWtwWFZDSjkuZXlKMWJuRnlaV1JwYzJ0bGVTSTZJbVZoWmpWbE0yRTFMV0kxT0dNdE5ETXdNQzFpTjJZeUxUSTJPVFV4TmpRM05tWTFOMTlGYldGcGJDSXNJbWxrSWpvaVpXRm1OV1V6WVRVdFlqVTRZeTAwTXpBd0xXSTNaakl0TWpZNU5URTJORGMyWmpVM0lpd2lhV0YwSWpveE5qVXhOVGszTXpreWZRLkZOdUNObEI5MDVhMUw0U3MzSHZKWlNycC05N2l0dTUtUGFYSURFZG0xdUJQNkZjOXM2cWRSLXY5ZUkzUkJaX1JBWVM2VjR6cFRLNUwzSGV2aXZlTElnIiwidmVyaWZpY2F0aW9uX2NvZGUiOiIwMjEzODkiLCJlbWFpbHR5cGUiOiJSZXNldFBhc3N3b3JkIn0='
  };
  const testRes = await userService.updatePasswordWithToken(dto);
  debug('testRes:',testRes);
  expect(testRes.message).toEqual('Congratulations ! Password has been updated successfully.');
});

it('chk email status', async () => {
  debug('TEST RESULT 7:');
  const testRes = await userService.chkEmailStatus('chaithra@digitalprizm.net');
  debug('testRes email status:',testRes);
  expect(testRes).toMatchObject({'is_email_verified':1});
});

it('Update occupation', async () => {
      const dto = {
        occupation: 's/w engineer',
      };
      debug('TEST RESULT:8');
      const testRes = await userService.updateOccupation('eaf5e3a5-b58c-4300-b7f2-269516476f57', dto);
      debug(testRes);
      expect(testRes.showData.id).toEqual('eaf5e3a5-b58c-4300-b7f2-269516476f57');
});

it('Add Bank details', async () => {
  const dto = {
    bank_name: 'ICICI',
    bank_ac_number: 'IC353453',
  };
  debug('TEST RESULT:9');
  const testRes = await userService.addBankDet('eaf5e3a5-b58c-4300-b7f2-269516476f57', dto);
  debug('add=bank:', testRes);
  expect(testRes.userId).toEqual('eaf5e3a5-b58c-4300-b7f2-269516476f57');
});

it('Update Bank details', async () => {
    const dto = {
      bank_name: 'ICICI',
      bank_ac_number: 'IC353453',
    };
    debug('TEST RESULT:9');
    const testRes = await userService.updateBankDet('eaf5e3a5-b58c-4300-b7f2-269516476f57', dto);
    debug('updatea=bank:', testRes);
    expect(testRes['res']['userId']).toEqual('eaf5e3a5-b58c-4300-b7f2-269516476f57');
});

it('Delete Bank details', async () => {
  debug('TEST RESULT:10');
  const testRes = await userService.deleteBankDet('eaf5e3a5-b58c-4300-b7f2-269516476f57');
  debug('delete=bank:', testRes);
  expect(testRes.affected).toBe(1);
});

it('Change Email', async () => {
  debug('TEST RESULT:11');
  const dto = {
    email: 'chaithra+115@digitalprizm.net',
  };
  const testRes = await userService.chgUserEmail('fe6ec0be-e10b-4da3-ad90-a092a3ddba1f',dto);
  debug('chg email:', testRes);
  expect(testRes.message).toEqual('Please verify the email adress');
});

it('Change Mobile', async () => {
  debug('TEST RESULT:12');
  const dto = {
    country_code: '91',
    country_code_label: 'IN',
    mobile:'8971170213'
  };
  const testRes = await userService.chgUserMobile('eaf5e3a5-b58c-4300-b7f2-269516476f57',dto);
  debug('chg phone:', testRes);
  expect(testRes.message).toEqual('Please verify the mobile');
});

it('Change password', async () => {
  debug('TEST RESULT:13');
  const dto = {
    old_password: 'password2',
    new_password: 'password4',
    confirm_password:'password4'
  };
  const testRes = await userService.chgPassword('fe6ec0be-e10b-4da3-ad90-a092a3ddba1f',dto);
  debug('chg password:', testRes);
  expect(testRes.message).toEqual('Congratulations ! Password has been updated successfully.');
});

it('Update referal code', async () => {
  debug('TEST RESULT:13');
  const dto = {
    referal_code: 'Q5M2ZG',
  };
  const testRes = await userService.updateReferalCode('fe6ec0be-e10b-4da3-ad90-a092a3ddba1f',dto);
  debug('chg referal code:', testRes);
  expect(testRes.message).toEqual('Referal code has been updated successfully');
});

it('create contact', async () => {
  debug('TEST RESULT:13');
  const dto = {
    name: 'jhon',
    email: 'jhon@gmail.com',
    message: 'need info on subscription plan'
  };
  const testRes = await userService.contactUs('fe6ec0be-e10b-4da3-ad90-a092a3ddba1f',dto);
  debug('contact us:', testRes);
  expect(testRes.message).toEqual('Contact us has been added successfully');
});


  // it('Resend email to get email verified', async () => {
  //   try {
  //     debug('TEST RESULT:2');
  //     const testRes = await userService.resendEmailWithCode('chaithra@digitalprizm.net', 'VerifyEmail');
  //     debug(testRes);
  //   } catch (err) {
  //     debug('err:', err);
  //   }
  // });

  // it('Resend sms to get code to verify', async () => {
  //   try {
  //     debug('TEST RESULT:3');
  //     const testRes = await userService.resendSmsWithCode('918971170213');
  //     debug(testRes);
  //   } catch (err) {
  //     debug('err:', err);
  //   }
  // });

  // it('Send email to reset password', async () => {
  //   try {
  //     debug('TEST RESULT:4');
  //     const testRes = await userService.resendEmailWithCode('chaithra@digitalprizm.net', 'ResetPassword');
  //     debug(testRes);
  //     expect(testRes.message).toEqual('ResetPassword Verification code has been sent to your email chaithra@digitalprizm.net');
  //   } catch (err) {
  //     debug('err:', err);
  //   }
  // });

  // it('Check email status', async () => {
  //   try {
  //     debug('TEST RESULT:5');
  //     const testRes = await userService.chkEmailStatus('chaithra@digitalprizm.net');
  //     debug(testRes);
  //     expect(testRes.is_email_verified).toBe(1);
  //   } catch (err) {
  //     debug('err:', err);
  //   }
  // });

  


  // it('Delete bank detaild', async () => {
  //   try {
  //     debug('TEST RESULT:8');
  //     const testRes = await userService.deleteBankDet('8e380c07-69a8-4123-84a4-25c0f98f4668');
  //     debug(testRes);
  //     expect(testRes.affected).toBe(1);
  //   } catch (err) {
  //     debug('err:', err);
  //   }
  // });
}); // Main end
