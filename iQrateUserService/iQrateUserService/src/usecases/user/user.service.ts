import {HttpException, HttpStatus, Injectable, NotFoundException, Req} from '@nestjs/common';
import Debug from 'debug';
import DeviceDetector = require('device-detector-js');
import * as bcrypt from 'bcrypt';
import { getConnection, IsNull } from 'typeorm';
import { HttpService } from '@nestjs/axios';
import path = require('path');
import { UserBankEntity } from '../../entities/userbank';
import { ConfigService } from '../../config/config.service';
import { JWT } from '../utils/jwt';
//'../usecases/utils/jwt';
import { UserEntity } from '../../entities/users';
import { CampaignEntity } from '../../entities/campaignentity';
import { ContactUs } from '../../entities/contact_us';
import { AssignedCobrokeEntity } from '../../entities/cobroke_id_assigned';
import {
  UpdateCustomerDto,
 
} from '../subscription/subscription.dto';


import {
  DefaultBankDto,
  DefaultOccupationDto,
  DefaultPassword,
  DefaultUserStatusDto,
  DefaultVerificationCode,
  UserDto,
  UserEmailDto,
  UserMobileDto,
  DefaultChgPswrdDto,
  EmailMobileVerifyStatusDto,
  DefaultUpdateNamePhone, RedisDto, RefreshDto, DefaultReferalDto, ContactUsDto, ExportItem, SendPushNotifyDto, ExportSubscriptionItem, ExportSRevennueItem,
} from './dto/user.dto';

import RedisClient from '../utils/redis';
import {LoginService} from "../login/login.service";
import {SubscriptionService} from "../subscription/subscription.service";
import { AdminEntity } from '../../entities/admin';
import { EventEmitter2, OnEvent } from "@nestjs/event-emitter";

const Json2csvParser = require("json2csv").Parser;
const fs = require("fs");
import * as AWS from 'aws-sdk';
import { Cron } from '@nestjs/schedule';
import { AdminService } from '../admin/admin.service';


// START to get device info
// START to get device info
const deviceDetector = new DeviceDetector();
const userAgent =
  'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Safari/537.36';
const device = deviceDetector.parse(userAgent);

const deviceinfo = `{ "OS_name":"${device.os.name}", "OS_version":"${device.os.version}", "client_type":"${device.client.type}", "client_name":"${device.client.name}", "client_version":"${device.client.version}" }`;


const debug = Debug('user-service:UserService');

@Injectable()
export class UserService {

  constructor(
    private config: ConfigService,
    private readonly http: HttpService,
    private eventEmitter: EventEmitter2,
    private adminService: AdminService

  ) {
    //this.eventEmitter = new EventEmitter2();
   // this.subscriptionService =  SubscriptionService;
   
  }

  // cron job run at every seconds
  @Cron('* * * 30 * *')
  log() {
    console.log('Hello world!');
  }

  async setTwoFactorAuthenticationSecret(secret: string, userId: string) {
    return UserEntity.update(userId, {
      twoFactorAuthenticationSecret: secret,
      // isTwoFactorAuthenticationEnabled: true
      isTwoFactorAuthenticationEnabled: false,
      updated_at: new Date(),
      updated_user_id: userId,
    });
  }

  async enablemfa(userId: string) {
    UserEntity.update(userId, {
      isTwoFactorAuthenticationEnabled: true,
      updated_at: new Date(),
      updated_user_id: userId,
    });
    return {message: "2FA is updated successfully!"};
  }

  async getFullProfile(req:any) {

    debug('req.headers.userid : ', req.headers.userid);
    const AGENT_TYPE_VAL = this.config.envConfig.AGENT_TYPE_VAL || 'agent';

    if(req.headers && req.headers.usertype && req.headers.usertype===AGENT_TYPE_VAL) {
      // const user = await UserEntity.findByIds([req.headers.userid]);
      const user = await UserEntity.findByIds([req.headers.userid],{ relations: ['bankers']});
      // user["appid"] = this.config.envConfig.ONE_SIGNAL_APP_ID;
      debug(' user : ', user);
      return {status : 1 , data : user[0]};
    } else {
      return {status : 0, data : {}};
    }
  }

  async adminGetFullProfile(id: string) {
      const singleUser = await UserEntity.findOne({ id },{ relations: ['bankers']});
      if (!singleUser) {
        throw new HttpException({
          name : 'NOT_FOUND',
          message : 'Record does not exist'
        },HttpStatus.BAD_REQUEST);
      }

      // debug('singleUser:',singleUser.bankers[0]['bank_name']);
      debug('singleUser:',singleUser);
      const { full_name, email, country_code, mobile, dob, is_email_verified, is_mobile_verified, status, isTwoFactorAuthenticationEnabled, cobroke_status, bankers, membership_type, stripe_customer_id, stripe_payment_method_id, stripe_subscription_id, deleted } = singleUser;
      const showData = { id, full_name, email, country_code, mobile, dob, is_email_verified, is_mobile_verified, status, isTwoFactorAuthenticationEnabled, cobroke_status, membership_type, stripe_customer_id, stripe_payment_method_id, stripe_subscription_id, bankers, deleted};
      return showData;
  }

  showAllUsers(filter_req: any) {
    
    debug('filter_req : ', filter_req);
    return UserEntity.find({
      order: {
        created_at: 'ASC',
        // id: "ASC"
      },
    });
  }

  // Redis Api endpoints
  async getRedisVal(redisKey: string) {
    return await RedisClient.get(redisKey);
  }

  // Redis Api endpoints
  async setRedisVal(redisObj: RedisDto) {
    return await RedisClient.set(redisObj.redisKey, redisObj.redisVal);
  }
  
  // Show user based on search and sort , if its there
  async showUserBasedOnsrch(alias: string, req) {
    try {
        const builder: any = UserEntity.createQueryBuilder(alias);
        /* All search field need to be placed in diff diff variable  ex:http://localhost:3001/user?name=chaithra&email=chaithra@digitalprizm&sort=asc  */
          // full_name search query
      if (
        req.query.name !== '' ||
        req.query.email !== '' ||
        req.query.phone !== '' ||
        req.query.fromdate !== '' ||
        req.query.todate !== '' ||
        req.query.code !== '' || 
        req.query.coBroke !== '' ||
        req.query.deleted !== ''
      ) {
        // search for ful_name using name=
        if (req.query.name) {
          builder.where('users.full_name ILIKE :s', {
            s: `%${req.query.name}%`,
          });
        }

        // search for email using email=
        if (req.query.email) {
          builder.andWhere('users.email ILIKE :email', {
            email: `%${req.query.email}%`,
          });
        }

        // search for mobile using phone=
        if (req.query.phone) {
          builder.andWhere('users.mobile ILIKE :mobile', {
            mobile: `${req.query.phone}%`,
          });
        }

        // search for status using status=       [0-inactive, 1-active]
        if (req.query.status) {
          builder.andWhere('users.status = :status', {
            status: req.query.status,
          });
        }

        // search for user is deleetd or not using deleted=       [0-not deleetd, 1-deleted]
        if (req.query.deleted) {
          builder.andWhere('users.deleted = :deleted', {
            deleted: req.query.deleted,
          });
        }

        // search for membershiptype using subscription=       [0-inactive, 1-active]
        if (req.query.subscription) {
          builder.andWhere('users.membership_type = :membership_type', {
            membership_type: req.query.subscription,
          });
          // builder.andWhere('users.membership_type ILIKE :membership', {
          //   membership: `%${req.query.subscription}%`,
          // });
        }

        // search for cobroke case using status=       [0-inactive, 1-active]
        if (req.query.coBroke) {
          builder.andWhere('users.cobroke_status = :cobroke_status', {
            cobroke_status: req.query.coBroke,
          });
        }

        if (req.query.fromdate && req.query.todate) {
          // const date = '03-11-2014';
          // const date = `'${req.query.fromdate}'`;
          // const newdate = date.split('-').reverse().join('-');

          // add hours to end date in order to include enddate in date range
          const enddate = convertYMD(req.query.todate);
          const includeEndDate = `${enddate} 23:55:55`;
          builder.andWhere('"created_at" BETWEEN :startDate AND :endDate', {
            startDate: convertYMD(req.query.fromdate),
            endDate: includeEndDate,
          });
        } else {
          // search for fromdate using fromdate=
          if (req.query.fromdate) {
            builder.andWhere(`DATE_TRUNC('day', "created_at") >= :date`, {
              date: convertYMD(req.query.fromdate),
            });
          }

          // search for todate using todate=
          if (req.query.todate) {
            // add hours to end date in order to include enddate in date range
            const enddate = convertYMD(req.query.todate);
            const includeEndDate = `${enddate} 23:55:55`;

            // debug(`todate:${convertYMD(req.query.todate)}`);
            builder.andWhere(`DATE_TRUNC('day', "created_at") <= :date`, {
              date: includeEndDate,
            });
          }
        }

        // search for bank name using bank=
        if (req.query.bank) {
          builder.andWhere('users.bank_name = :bank_name', {
            bank_name: req.query.bank,
          });
        }

        // search for postal code using code=
        if (req.query.code) {
          builder.andWhere('users.postal_code = :postal_code', {
            postal_code: req.query.code,
          });
        }
        builder.leftJoinAndSelect("users.bankers", "bank");
        const actualSqlQuery = await builder.getQuery();
        debug(`=================${actualSqlQuery}`);
        // return builder.getMany();
      }

      // SORT by full_name using sort = asc or sort = desc
      // const sort: any = req.query.sort;
      const { sort }: any = req.query;
      if (sort) {
        builder.orderBy('users.full_name', sort.toUpperCase());
      } else {
        builder.orderBy('users.full_name', 'ASC'.toUpperCase());
      }

      // pagination
      const page: number = parseInt(req.query.page as any, 10) || 1;
      const perPage = 10;
      const total = await builder.getCount();
      builder.offset((page - 1) * perPage).limit(perPage);
      if(total <= 0 ) {
        throw new HttpException({
          name : 'NOT_FOUND',
          message : 'Record does not exist'
        },HttpStatus.BAD_REQUEST);
      }

      return {
        data: await builder.select(['users.id', 'users.full_name', 'users.email', 'users.dob', 'users.mobile', 'users.status', 'users.isTwoFactorAuthenticationEnabled', 'users.cobroke_status', 'users.deleted', 'users.membership_type', 'users.stripe_customer_id', 'users.stripe_payment_method_id', 'users.stripe_subscription_id', 'bank.bank_name','bank.bank_ac_number']).getMany(),

        total,
        page,
        lastPage: Math.ceil(total / perPage),
      };
    } catch (err) {
      if(err && err.response && err.response.status>=400 && err.response.status<500) {
        throw new HttpException(err.response.data,err.response.status);
      } else {
        console.error('login err : ', err);
        throw err;
      }
    }
  }

  public async findByEmail(email: string): Promise<UserEntity> {
    // const userRepo = getConnection().getRepository(UserEntity);
    const user = await UserEntity.findOne({
      where: {
        email,
      },
    });

    if (!user) {
      // throw new NotFoundException(`User ${email} not found`);
    }

    return user;
  }

  // get id
  public async findById(id: string): Promise<UserEntity> {
    // const userRepo = getConnection().getRepository(UserEntity);
    debug('userid', id);
    const user = await UserEntity.findOne({
      where: {
        id,
      },
    });

    if (!user) {
      // throw new NotFoundException(`User #${userId} not found`);
    }

    return user;
  }

  getDate(): Date {
    // return new Date().toString();
    const date = new Date();
    const today = `${date.getFullYear()}-${
      date.getMonth() + 1
    }-${date.getDate()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;

    const date1 = new Date(today);

    return date1;
  }

  public async delete(id: string) {
    try {
      const res = await UserEntity.delete(id);
      return res;
    } catch (err) {
      console.error('Register Err:', err);
      throw err;
    }
  }

  public async register(reqBody: UserDto, emailVerify: number, registerType: string) {
    try {
      
      debug(JSON.stringify({emailVerify}));
      debug('emailVerify',emailVerify);
      debug('Entered user service');
      // const userRepo = getConnection().getRepository(UserEntity);

      // chk user with email already exist
      const emailRecordExist = await UserEntity.findOne({
        email: reqBody.email,
        user_type: this.config.envConfig.USERTYPE, // reqBody.user_type,
      });
      if (emailRecordExist) {
        throw new HttpException({
          name : 'emailRecordExist',
          message : 'User with email already exist'
        },HttpStatus.BAD_REQUEST);

      }

      // chk valid mail server
      const legit = require('legit');
      try {
          const res = await legit(reqBody.email);
          // return res;
          if(!res.isValid){
            throw new HttpException({
              name : 'INVALID_EMAIL',
              message : 'You have entered an invalid email address. Please check your email address'
            },HttpStatus.BAD_REQUEST);
          }
      } catch (err) {
        console.error('err:', err.Error);
        
          throw new HttpException({
            name : 'INVALID_EMAIL',
            message : 'You have entered an invalid email address. Please check your email address'
          },HttpStatus.BAD_REQUEST);
       
      }


      if(!reqBody.mobile) {
        debug('Math.floor(Math.random()*1000).toString() :', Math.floor(Math.random()*1000).toString());
        reqBody.mobile = Math.floor(Math.random() * 1000000).toString();
      }
      // chk user with mobile already exist
      const mobileRecordExist = await UserEntity.findOne({
        mobile: reqBody.mobile,
        // user_type: this.config.envConfig.USERTYPE, // reqBody.user_type,
      });

      let referalVal = '';
      if(emailVerify !== 1) { // if it is not signup with google
        if (mobileRecordExist) {
          debug('entered mobile exist');
          throw new HttpException({
            name : 'mobileRecordExist',
            message : 'We have noticed that you are using an existing IQrate user’s mobile number for registration. Kindly use another mobile number. Thank you.'
          },HttpStatus.BAD_REQUEST);
        }
        // to chk valid mobile or not
        const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();
        var test = phoneUtil.parseAndKeepRawInput(reqBody.mobile, reqBody.country_code_label);
        const validMobile = phoneUtil.isValidNumber(test);
        if (!validMobile) {
          debug('entered mobile exist');
          throw new HttpException({
            name : 'INVALID_MOBILE',
            message : 'Invalid mobile number'
          },HttpStatus.BAD_REQUEST);
        }

        debug('reqBody.referal_code:', reqBody.referal_code);
        if(reqBody.referal_code !== '') { 
          // chk refercode exist or not
          const codeRecordExist = await CampaignEntity.findOne({
            referal_code: reqBody.referal_code
          });
          if (!codeRecordExist) {
            throw new HttpException({
              name : 'INVALID_REFERRAL_CODE',
              message : 'Invalid referral code'
            },HttpStatus.BAD_REQUEST);
          }
          // in campaign tbl referal code usage_limit=0
          if( codeRecordExist.usage_limt === 0 ) {
            throw new HttpException({
              name : 'ZERO_USAGE_LIMIT',
              message : 'This referral code usage limit set it as zero'
            },HttpStatus.BAD_REQUEST);
          }

          if(codeRecordExist){
            const data = await CampaignEntity.findOne({
              referal_code: reqBody.referal_code
            });
            debug('data:', data);
            debug('usageLimt in campaign:', data.usage_limt);
            
            // chk referal code exist in user tbl
            const UserReferalExist = await UserEntity.findOne({
              referal_code: reqBody.referal_code
            });
            if(UserReferalExist) {
              // chk referal code usage limt exceeds or not
              const { usedCodeCount } = await UserEntity
              .createQueryBuilder('c')
              .select("COUNT(c.referal_code)", "usedCodeCount")
              .addSelect("c.status", "campStatus")
              .where("c.referal_code = :code", { code: reqBody.referal_code })
              .groupBy('c.status')
              .getRawOne();
              debug('usedCodeCount in register:', usedCodeCount)
              if( data.usage_limt < usedCodeCount  || data.usage_limt == usedCodeCount  ) {
                throw new HttpException({
                  name : 'EXCEED_USAGE_LIMT',
                  message : 'Referral code usage limit is exceeded'
                },HttpStatus.BAD_REQUEST);
              }
            }
  
            
            if( data.status === 0 ) {
              throw new HttpException({
                name : 'REFERRAL_CODE_EXPIRED',
                message : 'Invalid referral code'
              },HttpStatus.BAD_REQUEST);
            }
  
            // chk referal code is expired or not
            const start = new Date(data.start_date);
            start.setHours(24, 0, 0, 0);
            const end = new Date(data.end_date);
            end.setHours(24, 0, 0, 0);
            const current = new Date();
            current.setHours(24, 0, 0, 0); // next midnight
            debug('start:', start);
            debug('end:', end);
            debug('current:', current);
            if (current < start)
            { 
              debug('entered date');
              throw new HttpException({
                name: 'REFERRAL_CODE_EXPIRED',
                message: 'Referral code can be used in future',
              },HttpStatus.BAD_REQUEST);
            }
          }
  
          referalVal = reqBody.referal_code;
        }
      }

      if(!reqBody.dob) {
        reqBody.dob = '01-05-1971';
      }

      if(!reqBody.password) {
        reqBody.password = 'GOOGLE_DHEGFUWY#&*$#&'; //(await getRandomNumber(10)).toString();
      }

      if(!reqBody.agreed_term) {
        reqBody.agreed_term = 1;
      }

      // age should be above 21
      // const ageVal = calculateAge(reqBody.dob);
      // debug(ageVal);
      // if (calculateAge(reqBody.dob) < 21) {
      // debug('less than 21', reqBody.dob);
      // return newDate;
      // } else {
      //   debug('not less than 21', reqBody.dob);
      //   return ageVal;
      // }

      if (calculateAge(reqBody.dob) < 21) {
        debug('enterd 21 below age');
        throw new HttpException({
          name : 'AgeValidationError',
          message : 'You must be 21 year old and above to access iqrate services'
        },HttpStatus.BAD_REQUEST);
      }

      // START get device location
      const DEVICE_LOCATION_API_URL = this.config.envConfig.DEVICE_LOCATION_API_URL || 'http://ip-api.com/json/';
      const deviceLocationRes = await this.http.get(DEVICE_LOCATION_API_URL).toPromise();
      debug('deviceLocation : ', deviceLocationRes.data);
      const deviceLocation = deviceLocationRes.data.regionName;

      const keycloakToken = (await this.getKeycloak()).token;
      debug('keycloakToken : ', keycloakToken);

      // START save user in keycloak
      const headers = {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${keycloakToken}`,
      };

      let datas = {};
      const kc_emailVerified = emailVerify===1?true:false;
      const kc_enabled = emailVerify===1?true:false;
      datas = {
        username: reqBody.email,
        enabled: kc_enabled,
        totp: false,
        emailVerified: kc_emailVerified,
        firstName: reqBody.full_name,
        // lastName: 'Strange',
        email: reqBody.email,
        credentials: [
          {
            type: 'password',
            value: reqBody.password,
            temporary: false,
          },
        ]
      };

      datas['attributes'] = {
        membershiptype: (this.config.envConfig.MEMBERSHIPTYPE || 'basic' ),
      };

      let addRes:any = {};

      try {
        addRes = await this.http
            .post(
                `${this.config.envConfig.KEYCLOAK_BASE_URL}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_AGENT}/users`,
                datas,
                {
                  headers,
                },
            ).toPromise();
      } catch (err) {
        if(err && err.response && err.response.status>=400 && err.response.status<500) {
          throw new HttpException(err.response.data,err.response.status);
        } else {
          console.error('login err : ', err);
          throw err;
        }
      }

      debug('addRes : ', addRes.data);

      // To get keycloak id
      const IDLocation = addRes.headers.location;
      const segments = IDLocation.split('/');
      const keyLockID = segments[segments.length - 1];
      // END

      if (keyLockID === '') {
        throw new HttpException({
          name : 'UnknownErrorFromKeycloak',
          message :  'Error occured while adding user'
        },HttpStatus.BAD_REQUEST);
      }
      // END keycloak implement

      // let registerType = '';
      // if(emailVerify === 1) { 
      //   registerType = 'gmail';
      // } else {
      //   registerType = 'manual';
      // }
      // hash the passowrd
      // const hashPass = await gethasnumber(reqBody.password, 12);
      const user = new UserEntity();
      user.id = keyLockID;
      user.full_name = reqBody.full_name;
      user.email = reqBody.email;
      // user.password = hashPass; // hashPass;
      user.country_code = reqBody.country_code;
      user.country_code_label = reqBody.country_code_label;
      user.mobile = reqBody.mobile;
      user.whole_number = reqBody.country_code + reqBody.mobile;
      user.agreed_term = reqBody.agreed_term;
      // const dateFormatDob = reqBody.dob.split('-').reverse().join('-');
      const dateFormatDob = reqBody.dob;
      user.dob = dateFormatDob;
      user.created_at = new Date();
      user.created_user_id = keyLockID;
      // user.device = deviceinfo;
      // user.location = await deviceLocation;
      user.is_email_verified = emailVerify;
      user.is_mobile_verified = 0;
      user.status = emailVerify;
      user.user_type = this.config.envConfig.USERTYPE; // reqBody.user_type;
      user.membership_type = this.config.envConfig.MEMBERSHIPTYPE;
      user.register_type = registerType;
      user.referal_code = referalVal;
      user.cobroke_status = 0;
      const res = await UserEntity.save(user);
      const { id, full_name, email, mobile, dob, country_code, agreed_term } = res;
      const { password } = reqBody;
      const showData = {
        id,
        full_name,
        email,
        country_code,
        mobile,
        dob,
        agreed_term,
      };
      debug('showData : ', showData);

      const verificationCode = await getRandomNumber(6);
      debug(verificationCode);

      const sendNofiyStatus = this.config.envConfig.ENABLE_NOTIFY_SERVICE;

      console.log('sendNofiyStatus',sendNofiyStatus);

      debug(JSON.stringify({sendNofiyStatus}));
     
      if (sendNofiyStatus === 'true') {
        debug('*******--IF');
        debug('sendNofiyStatus is true');
        console.log('sendNofiyStatus is true');
        // this.eventEmitter.emit(
        //   'user.notification',
        //  {emailVerify,reqBody,type:"Email",id},
        // );


        if(emailVerify !== 1) {
          // START send email to specific user with verification code
          // await this.sendEmailCode(res.email, verificationCode,'VerifyEmail');
          try {
            const notiData = {fieldValue:email, type:"Email"}; //{fieldValue:bodyParams.email,type:"Email"};
            const notificationRes = await this.sentNotification(id, notiData);
            debug(' notificationRes :', notificationRes);
          } catch(err) {
            console.error('notificationRes error:', err);
          }
          // END
        }


      }

      // this.eventEmitter.emit(
      //   'user.notification',
      //   {emailVerify,reqBody,type:"ResendCodeMobile",id}
      // );

      if(emailVerify !== 1) {
        try {
          const whole = reqBody.country_code + reqBody.mobile;
          const notiDatamobile = {fieldValue:whole, type:"ResendCodeMobile"};
          const getSMSToken = await this.sentNotification(id, notiDatamobile);
          showData['SMSToken'] = getSMSToken.verification_token;
        } catch(err) {
          console.error('getSMSToken error:', err);
        }
      
      }

      await UserEntity.update(
        { id: res.id },
        { resendcode_email_count: 0, resendcode_sms_count: 0, verification_code_send_datetime: null, updated_at: new Date(), updated_user_id: res.id },
      );
      return showData;
      debug('END:');

    } catch (err) {
      if(err && err.response && err.response.status>=400 && err.response.status<500) {
        // throw new HttpException(err.response.data,err.response.status);
        throw new HttpException({
          message :  'We have encountered a technical difficulty onboarding you. Please try again later.',
        },HttpStatus.BAD_REQUEST);
      } else {
        console.error('Register Err:', err);
        throw err;
      }
    }
  }

  // @OnEvent('user.notification')
  // async handleNotificationEvent(eventBody: any) {
  //   console.log('handleNotificationEvent');
  //   debug('handleNotificationEvent');
  //   const {emailVerify,reqBody,type,id} = eventBody;
  //   debug('handleNotificationEvent:emailVerify',emailVerify);
  //   console.log('handleNotificationEvent:emailVerify',emailVerify);
  //   debug('handleNotificationEvent:type',type);
  //   console.log('handleNotificationEvent:type',type);
  //   debug(`{emailVerify,reqBody,type} :`, JSON.stringify({emailVerify,reqBody,type}));
  //   if(type==='ResendCodeMobile') {
  //     if(emailVerify !== 1) {
  //       try {
  //         const whole = reqBody.country_code + reqBody.mobile;
  //         const notiDatamobile = {fieldValue:whole, type:"ResendCodeMobile"};
  //         const getSMSToken = await this.sentNotification(id, notiDatamobile);
  //         debug(`getSMSToken :`, JSON.stringify(getSMSToken));
  //         // showData['SMSToken'] = getSMSToken.verification_token;
  //       } catch(err) {
  //         console.error('getSMSToken error:', err);
  //       }
  //     }
  //   } else {
  //     if(emailVerify !== 1) {
  //       // START send email to specific user with verification code
  //       // await this.sendEmailCode(res.email, verificationCode,'VerifyEmail');
  //       try {
  //         const notiData = {fieldValue:reqBody.email, type:"Email"}; //{fieldValue:bodyParams.email,type:"Email"};
  //         debug('handleNotificationEvent:call sentNotification',notiData);
  //         console.log('handleNotificationEvent:call sentNotification',notiData);
  //         const notificationRes = await this.sentNotification(id, notiData);
  //         debug(' notificationRes :', JSON.stringify(notificationRes));
  //         console.log(' notificationRes :', JSON.stringify(notificationRes));
  //       } catch(err) {
  //         console.error('notificationRes error:', err);
  //         if(err && err.response && err.response.data) {
  //           debug('notificationRes error if:', err);
  //           console.error('notificationRes if:', err);
  //           throw new HttpException(err.response.data,err.response.status);
  //         }
  //         throw err;
  //       }
  //       // END
  //     }
  //   }
  // }

  async sendVerificationLinkWithCode(email: string) {
    try {
      debug('Entered send verify code to specific email id');

      const emailExist = await UserEntity.findOne({ email });
      if (!emailExist) {
        debug('Record doesnot exist');
        // return 'Record doesnot exist';
        throw new HttpException({
          name : 'NOT_FOUND',
          message :  'Record does not exist',
        },HttpStatus.BAD_REQUEST);
      }

      if (
        emailExist.resendcode_email_count >=
        Number(this.config.envConfig.MAX_RESENDCODE_EMAIL_RETRY)
      ) {
        debug('You exceeded the maximum tries. Please Signup again.');
        // return 'you have reached your maximum resend option';
        throw new HttpException({
          name : 'LIMIT_EXCEDED',
          message :  'You exceeded the maximum tries. Please Signup again.',
        },HttpStatus.BAD_REQUEST);
      }

      // START chk last sent code time
      const resp = await this.chkResendCodeTime(emailExist.verification_code_send_datetime);
      const sendNofiyStatus = this.config.envConfig.ENABLE_NOTIFY_SERVICE;
      if (sendNofiyStatus === 'true') {
        // START send email to specific user with verification code
        // await this.sendEmailCode(email, verificationCode,'VerifyEmail');
        // END

        const notiData = {fieldValue:emailExist.email, type:"Email"}; //{fieldValue:bodyParams.email,type:"Email"};
        const notificationRes = await this.sentNotification(emailExist.id, notiData);
        debug('notificationRes : ', notificationRes);
        const codeSentTimes = emailExist.resendcode_email_count + 1;
        await UserEntity.update(
          { id: emailExist.id },
          {
            resendcode_email_count: codeSentTimes,
            // verification_code: verificationCode,
            verification_code_send_datetime: new Date(),
            updated_at: new Date(),
            updated_user_id: emailExist.id,
          },
        );
      }
      return {message:`Verification code has been sent to your email ${email}`};
    } catch (err) {
      console.error('Send verify to an email Err:', err);
      throw err;
    }
  }


  // START verify verification code which was sent to mobile baesd on mobile number
  async verifySmsCode(mobile: string, requestPara: DefaultVerificationCode) {
    try {
      debug('Entered verify code based on mobile');

      const mobileExist = await UserEntity.findOne({ whole_number: mobile });
      if (!mobileExist) {
        debug('Record doesnot exist');
        throw new HttpException({
          name : 'recordNotFound',
          message :  'Record does not exist'
        },HttpStatus.BAD_REQUEST);
      }

      return await this.verifyWithCode(requestPara);
    } // catch (err) {
    //   console.error('Verify code based on mobile err:', err);
    //   throw err;
    // }
    catch (err) {
      const mobileExistCatch = await UserEntity.findOne({ whole_number: mobile });
      debug('err val:', err);
      debug('err.response val:', err.response);
      debug('err.response.stattus val:', err.response.status);
      debug('err.stattus val:', err.status);
      debug('err.response.name val:', err.response.name);
      if (err && err.response && err.status >= 400 && err.status < 500) {
        debug('if entered:');
        // throw new HttpException(err.response.data, err.response.status);
        if(err.response.name === 'wrongCode_AcDelted')
        {
          debug('wrong verification and attempted all 3 possibilites entered:');
          // deleting the a/c
          // START get keycloak token
          const keycloakToken = (await this.getKeycloak()).token;
          // END

          // START update email in keycloak
          const headers = {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${keycloakToken}`,
          };
          try {
            const addRes = await this.http
              .delete(
                `${(await this.getKeycloak()).ip}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_AGENT}/users/${mobileExistCatch.id}`,
                {
                  headers,
                },
              )
              .toPromise();
          } catch (err) {
            if(err && err.response && err.response.status>=400 && err.response.status<500) {
              throw new HttpException(err.response.data,err.response.status);
            } else {
              console.error('keycloak ac delete err : ', err);
              throw err;
            }
          }
          // END
          // END keycloak implement

          // delete a/c frm db
          const res = await UserEntity.delete({ id: mobileExistCatch.id });
          throw new HttpException({
            message :  'Wrong verification code. You have tried all your possibilites. Account has been deleted. Please signup',
          },HttpStatus.BAD_REQUEST);
        } else if(err.response.name === 'INVALID_SMS_TOKEN') {
          debug('wrong sms token entered:');
          throw new HttpException({
            message :  'Invalid sms token',
          },HttpStatus.BAD_REQUEST);
        }
        else {
          debug('wrong verication code entered:');
          throw new HttpException({
            message :  'Wrong verification code',
          },HttpStatus.BAD_REQUEST);
        }
      } else {
        debug('if-eles-else entered:');
        console.error('OTP verification  err : ', err);
        throw err;
      }
    }
  }
  // END

  // START resend VerifyCode to an email
  async resendEmailWithCode(email: string, emailtype:string) {
    try {
      debug('Entered resend verification code to an email');
      const emailExist = await UserEntity.findOne({ email });
      if (!emailExist) {
        debug('Record doesnot exist');
        // return 'Record doesnot exist';
        throw new HttpException({
          name : 'recordNotFound',
          message :  'Record does not exist'
        },HttpStatus.BAD_REQUEST);
      }

      if(emailExist.deleted === 1) {
        throw new HttpException({
          name : 'ACCOUNT_DELETED',
          message :  'Account was deleted by the user'
        },HttpStatus.BAD_REQUEST);
      }

      if(emailExist.register_type === 'gmail' || emailExist.register_type === 'apple'){
        throw new HttpException({
          name : 'CANNOT_RESET_PASSWORD',
          message :  `Registerd from ${emailExist.register_type}. So can not reset your password`
        },HttpStatus.BAD_REQUEST);
      }

      if(emailtype === 'VerifyEmail')
      {
        if(emailExist.is_email_verified===1) {
          throw new HttpException({message:"Email is already verified."},HttpStatus.METHOD_NOT_ALLOWED);
        }
        if (
          emailExist.resendcode_email_count >=
          Number(this.config.envConfig.MAX_RESENDCODE_EMAIL_RETRY)
        ) {
          debug('You exceeded the maximum tries. Please Signup again.');
          
          // deleting the a/c
          // START get keycloak token
          const keycloakToken = (await this.getKeycloak()).token;
          // END

          // START update email in keycloak
          const headers = {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${keycloakToken}`,
          };

          // http://127.0.0.1:8180/auth/admin/realms/heroes/users/83c72e88-7ac9-4fc7-a7fb-97736d67d261
          try {
            const addRes = await this.http
              .delete(
                `${(await this.getKeycloak()).ip}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_AGENT}/users/${emailExist.id}`,
                {
                  headers,
                },
              )
              .toPromise();
          } catch (err) {
            if(err && err.response && err.response.status>=400 && err.response.status<500) {
              throw new HttpException(err.response.data,err.response.status);
            } else {
              console.error('login err : ', err);
              throw err;
            }
          }
          // END
          // END keycloak implement

          // delete the a/c from db
          const res = await UserEntity.delete({ id: emailExist.id });

          return {
            name: 'ACCOUNT_DELETED',
            message: 'You exceeded the maximum tries. Please Signup again',
          };

          // // make user accpunat block
          // // START get keycloak token
          // const keycloakToken = (await this.getKeycloak()).token;
          // // END

          // // START update email in keycloak
          // const headers = {
          //   'Content-Type': 'application/json',
          //   Authorization: `Bearer ${keycloakToken}`,
          // };

          // const datas = {
          //   enabled: false,
          // };

          // const addRes = await this.http
          //   .put(
          //     `${(await this.getKeycloak()).ip}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_AGENT}/users/${emailExist.id}`,
          //     datas,
          //     {
          //       headers,
          //     },
          //   )
          //   .toPromise();
          // // END
          // // END keycloak implement

          // await UserEntity.update(
          //   { id: emailExist.id },
          //   {
          //     status: 0,
          //     updated_at: new Date(),
          //     updated_user_id: emailExist.id,
          //   },
          // );

          // throw new HttpException({
          //   name : 'excedSendCode',
          //   message :  'you have reached your maximum resend option'
          // },HttpStatus.BAD_REQUEST);
        }

        // START chk last sent code time
        const resp = await this.chkResendCodeTime(emailExist.verification_code_send_datetime);
    }

      // START Resend email to specific user with verification code . max 3 times

      const sendNofiyStatus = this.config.envConfig.ENABLE_NOTIFY_SERVICE;
      if (sendNofiyStatus === 'true') {
        const subject = (emailtype==='VerifyEmail'?'Email Verification':'Reset Password Verification');
        const notiData = {fieldValue:emailExist.email, type:"Email", emailtype}; //{fieldValue:bodyParams.email,type:"Email"};
        const notificationRes = await this.sentNotification(emailExist.id, notiData);
        debug('notificationRes :', notificationRes);
        const codeSentTimes = emailExist.resendcode_email_count + 1;
        if(emailtype === 'ResetPassword')
        {
          await UserEntity.update(
            { id: emailExist.id },
            {
              // verification_code: verificationCode,
              updated_at: new Date(),
              updated_user_id: emailExist.id,
            },
          );
        } else {
          await UserEntity.update(
            { id: emailExist.id },
            {
              resendcode_email_count: codeSentTimes,
              // verification_code: verificationCode,
              verification_code_send_datetime: new Date(),
              updated_at: new Date(),
              updated_user_id: emailExist.id,
            },
          );
        }
      } else {
        throw new HttpException({
          message:"System error. Notification service is not enabled. Please contact your system support",
          name: "NOTIFICATIOn_SERVICE_DISABLED"
        },HttpStatus.METHOD_NOT_ALLOWED);
      }

      // return {message:`${emailtype} Verification code has been sent to your email ${email}`};
      const str = emailtype;
      const properEmailMsg = str.replace(/([A-Z])/g, ' $1').trim();
      debug('properEmailMsg:',properEmailMsg)

      if(emailtype === 'ResetPassword')
      {
        return {message:`We have sent a password recover instructions to your email address`};
      } else {
        return {message:`${properEmailMsg} link has been sent to your email ${email}`};
      }
    } catch (err) {
      console.error('resend verification code to specific email Err:', err);
      throw err;
    }
  }
  // END

   /////
   async chkEmailStatus(email: string) {
    try {
      debug('Entered send verify code to specific email id');

      const emailExist = await UserEntity.findOne({ email });
      if (!emailExist) {
        debug('Record doesnot exist');
        throw new HttpException({
          name : 'NOT_FOUND',
          message :  'Record does not exist',
        },HttpStatus.BAD_REQUEST);
      }
     
      const { is_email_verified } = emailExist;
      const showData = { is_email_verified };
      return showData;
    } catch (err) {
      console.error('check email status Err:', err);
      throw err;
    }
  }
  //////

   /////
   async checkUserType(email: string) {
    try {
      debug('Entered chk user type');

      const emailExist = await UserEntity.findOne({ email });
      if (!emailExist) {
        debug('Record doesnot exist');
        throw new HttpException({
          name : 'NOT_FOUND',
          message :  'Record does not exist',
        },HttpStatus.BAD_REQUEST);
      }
     
      const { membership_type } = emailExist;
      const showData = { membership_type };
      return showData;
    } catch (err) {
      console.error('check user type Err:', err);
      throw err;
    }
  }
  //////

  // START resend VerifyCode to Mobile
  async resendSmsWithCode(mobile: string) {
    try {
      debug('Entered resend verification code to mobile service');

      const mobileExist = await UserEntity.findOne({ whole_number: mobile });
      if (!mobileExist) {
        debug('Record doesnot exist');
        throw new HttpException({
          name : 'recordNotFound',
          message :  'Record does not exist'
        },HttpStatus.BAD_REQUEST);
      }
      if (
        mobileExist.resendcode_sms_count >=
        Number(this.config.envConfig.MAX_RESENDCODE_SMS_RETRY)
      ) {
        debug('You exceeded the maximum tries. Please Signup again.');
       
        // deleting the a/c
        // START get keycloak token
        const keycloakToken = (await this.getKeycloak()).token;
        // END

        // START update email in keycloak
        const headers = {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${keycloakToken}`,
        };

        // http://127.0.0.1:8180/auth/admin/realms/heroes/users/83c72e88-7ac9-4fc7-a7fb-97736d67d261
        try 
        {
          const addRes = await this.http
            .delete(
              `${(await this.getKeycloak()).ip}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_AGENT}/users/${mobileExist.id}`,
              {
                headers,
              },
            )
            .toPromise();
        } catch (err) {
          if(err && err.response && err.response.status>=400 && err.response.status<500) {
            throw new HttpException(err.response.data,err.response.status);
          } else {
            console.error('login err : ', err);
            throw err;
          }
        }
        // END
        // END keycloak implement

        // delete a/c frm db
        const res = await UserEntity.delete({ id: mobileExist.id });
        return {
          name: 'ACCOUNT_DELETED',
          message: 'You exceeded the maximum tries. Please Signup again',
        };

        
        // // make user accounat block
        // // START update keycloak user enabled=false w.r.t mobile
        // const keycloakToken = (await this.getKeycloak()).token;
        // // END

        // // START update email in keycloak
        // const headers = {
        //   'Content-Type': 'application/json',
        //   Authorization: `Bearer ${keycloakToken}`,
        // };

        // const datas = {
        //   enabled: false,
        // };

        // const addRes = await this.http
        //   .put(
        //     `${(await this.getKeycloak()).ip}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_AGENT}/users/${mobileExist.id}`,
        //     datas,
        //     {
        //       headers,
        //     },
        //   )
        //   .toPromise();
        // // END
        // // END keycloak implement

        // await UserEntity.update(
        //   { id: mobileExist.id },
        //   {
        //     status: 0,
        //     updated_at: new Date(),
        //     updated_user_id: mobileExist.id,
        //   },
        // );

        
        // throw new HttpException({
        //   name : 'excedSendCode',
        //   message :  'you have reached your maximum resend option'
        // },HttpStatus.BAD_REQUEST);
      }

      // START chk last sent code time
      const resp = await this.chkResendCodeTime(mobileExist.verification_code_send_datetime);

      const verificationCode = await getRandomNumber(6);
      debug(verificationCode);

      // START Resend sms to specific user with verification code . max 3 times
      const sendNofiyStatus = this.config.envConfig.ENABLE_NOTIFY_SERVICE;
      if (sendNofiyStatus === 'true') {
        const notiData = {fieldValue:mobile, type:"ResendCodeMobile"}; //{fieldValue:bodyParams.email,type:"Email"};
        const notificationRes = await this.sentNotification(mobileExist.id, notiData);
        debug('notificationRes :', notificationRes);
        // END

        const codeSentTimes = mobileExist.resendcode_sms_count + 1;
        await UserEntity.update(
          { id: mobileExist.id },
          {
            resendcode_sms_count: codeSentTimes,
            verification_code: verificationCode,
            verification_code_send_datetime: new Date(),
            updated_at: new Date(),
            updated_user_id: mobileExist.id,
          },
        );
        return notificationRes;
      }
    } catch (err) {
      console.error('resend verification code to specific mobile Err:', err);
      throw err;
    }
  }
  // END

  async sendEmailLinkToResetPswrd(email: string) {
    try {
      debug('Entered send email link to reset password service');


      const emailExist = await UserEntity.findOne({ email });
      if (!emailExist) {
        debug('Record doesnot exist');
        // return 'Record doesnot exist';
        throw new HttpException({
          name : 'recordNotFound',
          message :  'Record does not exist'
        },HttpStatus.BAD_REQUEST);
      }

      // START send email to specific user to reset password
      const sendNofiyStatus = this.config.envConfig.ENABLE_NOTIFY_SERVICE;
      if (sendNofiyStatus === 'true') {
        debug('enter');
        await this.http
          .post(
            `${this.config.envConfig.NOTIFY_SERVICE_URL}/notify/sendResetpassword`,
            {
              full_name: emailExist.full_name,
              to: emailExist.email,
              subject: 'Reset Password',
              // url: 'http://localhost:3001/user/updatePasswordFrmEmailResponse',
            },
          )
          .subscribe((res) => {
            // debug(res);
          });
      }
      // END

      const res = await UserEntity.findOne({ email });
      const { id, full_name, country_code, mobile, dob, agreed_term } = res;
      const showData = { id, full_name, email, country_code, mobile, dob, agreed_term };

      return showData;
    } catch (err) {
      console.error('send link to reset password Err:', err);
      throw err;
    }
  }

  async updatePasswordWithToken(
    setPassword: DefaultPassword
  ) {
    try {
      debug('Entered update password service');
      //JWT.decodeAndVerify() setPassword.token;

      let tokenObj:any = Buffer.from(setPassword.token,'base64').toString('ascii');

      debug('tokenObj :', tokenObj);
      if(tokenObj) {
        tokenObj = JSON.parse(tokenObj);
      }

      const decodedVal:any = JWT.decodeAndVerify(this.config.envConfig.JWT_VERIFY_PUBLIC_KEY, tokenObj.verification_token);
      debug('TokendecodedVal : ', decodedVal);
      const email = decodedVal.email;
      const id = decodedVal.id;
      const emailExist = await UserEntity.findOne({ id });
      if (!emailExist) {
        debug('Record doesnot exist');
        // return 'Record doesnot exist';
        throw new HttpException({
          name : 'recordNotFound',
          message :  'Record does not exist'
        },HttpStatus.BAD_REQUEST);
      }

      // START Keycloak update
      // START get keycloak token
      const keycloakToken = (await this.getKeycloak()).token;
      // END

      // START update password in keycloak
      const headers = {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${keycloakToken}`,
      };

      const datas = {
        type: 'password',
        value: setPassword.password,
        temporary: false,
      };

      // update password in keycloak w.r.t keyclock id
      // http://54.251.164.144:8080/auth/admin/realms/master/users/4da73c7d-7689-41b3-acbd-929696ca9ca9/reset-password
      const addRes = await this.http
        .put(
          `${(await this.getKeycloak()).ip}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_AGENT}/users/${emailExist.id}/reset-password`,
          datas,
          {
            headers,
          },
        )
        .toPromise();
      debug('*****UPDATE OASS');
      debug(addRes);
      if (addRes.status !== 204) {
        throw new HttpException({
          name : 'errUpdatePassword',
          message :  'Error occured while updating password'
        },HttpStatus.BAD_REQUEST);
      }
      debug('pasword updated successfully in keycloak');
      return {message : "Congratulations ! Password has been updated successfully."};

    } catch (err) {
      console.error('Update password Err:', err);
      debug("update password err msg: ", err.message); // Invalid data: No property: name
      debug("update pass err name: ",err.name); // PropertyRequired
      if(err.message){ 
        debug('has err message');
        const pswrdMainString = err.message;
        const pswrdtheWord  = "Unexpected token";
        if (pswrdMainString.indexOf(pswrdtheWord) !== -1 || err.name === 'SyntaxError') {
          console.log('The word "' + pswrdtheWord + '" exists in given string.');
          debug('wrong verification');
              throw new HttpException({
                name : 'INVALID_PSWRD_TOKEN',
                message :  'Invalid password token'
              },HttpStatus.BAD_REQUEST);
        }
      }
      throw err;
    }
  }

  async updateOccupation(id: string, setOccupation: DefaultOccupationDto) {
    try {
      debug('Entered update occupation service');

      const userExist = await UserEntity.findOne({ id });
      if (!userExist) {
        debug('Record doesnot exist');
        // return 'Record doesnot exist';
        throw new HttpException({
          name : 'recordNotFound',
          message :  'Record does not exist'
        },HttpStatus.BAD_REQUEST);
      }

      await UserEntity.update(
        { id },
        { occupation: setOccupation.occupation, updated_at: new Date(), updated_user_id: id },
      );

      const res = await UserEntity.findOne({ id });
      const { full_name, country_code, mobile, email, dob, occupation } = res;
      const showData = { id, full_name, email, country_code, mobile, dob, occupation };
      // return showData;
      return {message : "Your Profile has been updated successfully.", showData}; 
    } catch (err) {
      console.error('Update occupation Err:', err);
      throw err;
    }
  }

  //
  async addBankDet(id: string, setBank: DefaultBankDto) {
    try {
      debug('Entered add bank detail service');
      debug(id);

      const userExist = await UserEntity.findOne({ id });
      if (!userExist) {
        debug('Record doesnot exist');
        // return 'Record doesnot exist';
        throw new HttpException({
          name : 'recordNotFound',
          message :  'Record does not exist'
        },HttpStatus.BAD_REQUEST);
      }

      const bank = new UserBankEntity();
      bank.userId = id;
      bank.bank_name = setBank.bank_name;
      bank.bank_ac_number = setBank.bank_ac_number;
      bank.created_at = new Date();
      bank.created_user_id = id;
      const res = await UserBankEntity.save(bank);

      const RESULT = await UserBankEntity.findOne({ userId: id });
      const {
        userId,
        bank_name,
        bank_ac_number,
      } = res;
      const showData = {
        userId,
        bank_name,
        bank_ac_number,
      };

      return showData;
    } catch (err) {
      console.error('Add bank detail Err:', err);
      throw err;
    }
  }
  //

  async updateBankDet(id: string, setBank: DefaultBankDto) {
    try {
      const userExist = await UserBankEntity.findOne({ userId: id });
      if (!userExist) {
        debug('Record doesnot exist');
        const tblName = dbConnect();
        const bankRes = await tblName.bankRepo.save({
              userId: id,
              bank_name: setBank.bank_name,
              bank_ac_number: setBank.bank_ac_number,
              created_user_id: id,
              created_at: new Date()
        });
        return bankRes;
      }

      await UserBankEntity.update(
        { userId: id },
        {
          bank_name: setBank.bank_name,
          bank_ac_number: setBank.bank_ac_number,
          updated_user_id: id,
          updated_at: new Date(),
        },
      );

      const res = await UserBankEntity.findOne({ userId: id });
      delete res.id;
      delete res.bank_branch_code;
      delete res.bank_nation;
      delete res.created_user_id;
      delete res.bank_nation_id;
      delete res.created_user_id;
      delete res.updated_user_id;
      delete res.updated_at;

      // return res;
      return {message:'Congratulations! Bank details has been updated successfully.',res};
    } catch (err) {
      console.error('Update bank detail Err:', err);
      throw err;
    }
  }

  async deleteBankDet(id: string) {
    try {
      const res = await UserBankEntity.delete({ userId: id });
      return res;
    } catch (err) {
      console.error('delete bank detail Err:', err);
      throw err;
    }
  }

  async deleteUser(id: string) {
    try {
      const userExist = await UserEntity.findOne({ id });
      if (!userExist) {
        debug('Record doesnot exist');
        // return 'Record doesnot exist';
        throw new HttpException({
          name : 'recordNotFound',
          message :  'Record does not exist'
        },HttpStatus.BAD_REQUEST);
      }

      if(userExist.deleted === 1) {
        throw new HttpException({
          name : 'ALREDY_DELETED',
          message :  'This account has already been deleted'
        },HttpStatus.BAD_REQUEST);
      }
      
    
      // START chk record exist in keycloak
      // START get keycloak token
      try {
        const keycloakToken = (await this.getKeycloak()).token;
        // END

        const headers = {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${keycloakToken}`,
        };

        const isExist = await this.isUserExistKeycloak(
          userExist.user_type,
          userExist.id,
        );
        // END

        // START Keycloak update
        // START set status in keycloak

        const setststaus = {
          enabled: 0,
        };

        // set status in keycloak w.r.t keyclock id
        const addRes = await this.http
          .put(
            `${this.config.envConfig.KEYCLOAK_BASE_URL}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_AGENT}/users/${userExist.id}`,
            setststaus,
            {
              headers,
            },
          )
          .toPromise();
        // debug('*****UPDATE status');
        // debug(addRes);
        if (addRes.status !== 204) {
          return {
            resultcode: 400,
            errMessage: 'Error occured while updating user status in keycloak',
          };
        }
        debug('status updated successfully in keycloak');

        // END
        // END keycloak implement

        await UserEntity.update(
          { id },
          {
            status: 0,
            deleted: 1,
            deleted_at: new Date(),
            updated_at: new Date(),
            updated_user_id: id,
          },
        );

        // START update assigned_cobrokeid tbl, if u upadte status= 0 in users tbl for specific id
        // if admin set cobroke = 0 for specific user before it was in active state, then need to set last_assigned_user_id = 'false' for single latest record w.r.t specific id and need to set last_assigned_user_id = 'true' for other latest record i.e exxcept this specific user id in assigned_cobrokeid table
        const cobrokeExist = await AssignedCobrokeEntity.findOne({
          where: {
            assigned_user_id: id,
            last_assigned_user_id: true,
          },
          order: {
            id: "DESC"
          }
        });

        // if record exist in assigned_cobrokeid table
        debug('id', id);
        debug('cobrokeExist', cobrokeExist);
        if(cobrokeExist) {
          debug('user id exist in assigned_cobrokeid table:', cobrokeExist.id)

          // delete those records from assigned_cobrokeid tbl
          const res = await AssignedCobrokeEntity.delete({ id: cobrokeExist.id });
          debug('id deleted frm assigned_cobrokeid:', cobrokeExist.id );

          // after del get latest record frm assigned_cobrokeid tbl
          const afterDelCobrokeTbl = await AssignedCobrokeEntity.findOne({
            order: {
              id: "DESC"
            }
          });

          debug('latest cobrokeid afterDelCobrokeTbl:', afterDelCobrokeTbl);
          debug('latest cobrokeid:', afterDelCobrokeTbl.assigned_user_id);

          // if above selected id is active for cobroke in users tbl
          const existUSerTbl = await UserEntity.findOne({
            where: {
              cobroke_status: 1,
              status: 1,
              id: afterDelCobrokeTbl.assigned_user_id
            }
          });

          debug('existUSerTbl:', existUSerTbl);
          // set last_assigned_user_id = true for previous latest record if cobroke status set to inactive by admin for current id
          if(existUSerTbl) {
            debug('has record')
            await AssignedCobrokeEntity.update(
              { id: afterDelCobrokeTbl.id },
              { last_assigned_user_id: true, updated_at: new Date(), updated_user_id: id },
            );
            debug('updated in assigned_cobrokeid table with true for latest record');
          }

          // // delete all entry from assigned_cobrokeid tbl w.r.t id, means that id we made it as cobroke_status = 0
          // const delCobroke = await AssignedCobrokeEntity.delete({ assigned_user_id: id });
        } else {
          debug('user id not exist in assigned_cobrokeid table')
        }
        // END


        return {message: "User deleted successfully"};
      } catch (err) {
        if(err && err.response && err.response.status>=400 && err.response.status<500) {
          throw new HttpException(err.response.data,err.response.status);
        } else {
          console.error('User reinstate err : ', err);
          throw err;
        }
      }      
    } catch (err) {
      console.error('update user status  Err:', err);
      throw err;
    }
  }

  async isExists(userExist,bodyParams,type) {

    // START get keycloak token
    const keycloakToken = (await this.getKeycloak()).token;
    // END

    const headers = {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${keycloakToken}`,
    };

    const isExist = await this.isUserExistKeycloak(
      userExist.user_type,
      userExist.id,
    );
    // END

    if(type === "Email"){
    // chk user with email already exist
    const emailRecordExist = await UserEntity.findOne({
      email: bodyParams.email,
      user_type: userExist.user_type,
    });
    if (emailRecordExist) {
      debug('entered email exist');
      // return 'User with email already exist';
      throw new HttpException({
        name: "EMAIL_EXISTS",
        errMessage: 'User with email already exist',
      },HttpStatus.BAD_REQUEST);
    }
  } else {
    // chk user with email already exist
    const mobileRecordExist = await UserEntity.findOne({
      whole_number: bodyParams,
      user_type: userExist.user_type,
    });
    if (mobileRecordExist) {
      debug('entered mobile exist');
      // return 'User with email already exist';
      throw new HttpException({
        name: "MOBILE_EXISTS",
        errMessage: 'We have noticed that you are using an existing IQrate user’s mobile number for registration. Kindly use another mobile number. Thank you.',
      },HttpStatus.BAD_REQUEST);
    }
  }

    // START Keycloak update
    // START update email in keycloak
    const datas = {
      email: bodyParams.email,
      username: bodyParams.email,
      emailVerified: false,
    };

    // START chk email already exist in keycloak
    if(type === "Email"){
      const foundEmail = await this.http
        .get(
          // 'http://54.255.196.41:8080/auth/admin/realms/master/users',
          `${(await this.getKeycloak()).ip}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_AGENT}/users?email=${bodyParams.email}`,
          {
            headers,
          },
        )
        .toPromise();

      if (foundEmail.data.length > 0) {
        debug(' EXIST');
        throw new HttpException({
          resultcode: 'EMAIL_EXISTS',
          errMessage: 'User with email already exist',
        },HttpStatus.BAD_REQUEST);
      }
    }

    return true;

  }

  // START Change user email based on user id
  async chgUserEmail(id: string, bodyParams: UserEmailDto) {
    try {
      const userExist = await UserEntity.findOne({ id });
      if (!userExist) {
        debug('Record doesnot exist');
        // return 'Record doesnot exist';
        throw new HttpException({
          name: 'USER_NOT_FOUND',
          message: 'Record does not exist',
        },HttpStatus.BAD_REQUEST);
      }

      if(userExist.register_type === 'gmail' || userExist.register_type === 'apple'){
        throw new HttpException({
          name : 'CANNOT_CHG_EMAIL',
          message :  `Registerd from ${userExist.register_type}. So can not change your email`
        },HttpStatus.BAD_REQUEST);
      }

      // chk valid mail server
      const legit = require('legit');
      try {
          const res = await legit(bodyParams.email);
          // return res;
          console.log("res",res);
          if(!res.isValid){
            throw new HttpException({
              name : 'INVALID_EMAIL',
              message : 'You have entered an invalid email address. Please check your email address'
            },HttpStatus.BAD_REQUEST);
          }
        
          

          //this.subscriptionService.updateCustomer(userExist.stripe_customer_id, updateCustomerDto);

      } catch (err) {
        debug('err:', err.Error);
        
          throw new HttpException({
            name : 'INVALID_EMAIL',
            message : 'You have entered an invalid email address. Please check your email address'
          },HttpStatus.BAD_REQUEST);
       
      }


      const data = {fieldValue:bodyParams.email,type:"Email"};

      await this.isExists(userExist,bodyParams,'Email');

    //   const dbFieldToUpdate = {
    //     is_email_verified: 0,
    //     status: 0,
    //     updated_at: new Date(),
    //     updated_user_id: id,
    //   };

    // const kcFieldToUpdate = {
    //   emailVerified: false,
    //   enabled: false,
    // };

    // await UserEntity.update(
    //     { id: id },
    //     dbFieldToUpdate,
    // );

    // // START update keycloak emailverified=true w.r.t email
    // // START get keycloak token
    // const keycloakToken = (await this.getKeycloak()).token;
    // // END
    // // START update email in keycloak
    // const headers = {
    //   'Content-Type': 'application/json',
    //   Authorization: `Bearer ${keycloakToken}`,
    // };

    // const addRes = await this.http
    //   .put(
    //       `${(await this.getKeycloak()).ip}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_AGENT}/users/${id}`,
    //       kcFieldToUpdate,
    //       {
    //         headers,
    //       },
    //   )
    //   .toPromise();
    //   // END keycloak implement

          // Update email in stripe
          console.log("updating to stripe account");
          console.log("stripe customer id",userExist.stripe_customer_id);
          //let updateCustomerDto:UpdateCustomerDto;
          //updateCustomerDto.email = bodyParams.email;
          //let subscriptionService: SubscriptionService;

          const resPayment = await this.http
          .put(
            `${this.config.envConfig.PAYMENT_SERVICE_URL}/customer/${userExist.stripe_customer_id}`,
            {
              email:  bodyParams.email
            },
          )
          .toPromise();


    return await this.sentNotification(id,data);
    } catch (err) {
      console.error('Change user email id Err:', err);
      throw err;
    }
  }

  private redisSetUrl = `${this.config.envConfig.USER_SERVICE_BASEURL || 'http://localhost'}/redis/setRedisVal`;
  private redisGetUrl = `${this.config.envConfig.USER_SERVICE_BASEURL || 'http://localhost'}/redis/getRedisVal`
  async sentNotification(id,bodyParams) {
    const sendNofiyStatus = this.config.envConfig.ENABLE_NOTIFY_SERVICE;
    console.log('sentNotification:sendNofiyStatus',sendNofiyStatus);
    debug('sentNotification:sendNofiyStatus',sendNofiyStatus);
    if (sendNofiyStatus === 'true') {
      debug('sentNotification:inside');
      const verificationCode = await getRandomNumber(6);

      const unqrediskey = `${id}_${bodyParams.type}`;
      let value = ''
      if(bodyParams.type === "Email"){
        value = JSON.stringify({verificationCode,fieldValue:bodyParams.fieldValue});
      } else if(bodyParams.type === "Mobile"){
        value = JSON.stringify({verificationCode,fieldValue:{mobile:bodyParams.fieldValue.mobile, country:bodyParams.fieldValue.country_code, label:bodyParams.fieldValue.country_code_label}}); 
      }  else if(bodyParams.type === "ResendCodeMobile"){
        value = JSON.stringify({verificationCode,fieldValue:{mobile:bodyParams.fieldValue, country:''}});
      }
      debug('sentNotification:unqrediskey',unqrediskey);
      debug('sentNotification:value',value);
      const redisData = {
        "redisKey": unqrediskey,
        // "redisVal": JSON.stringify({verificationCode,fieldValue:bodyParams.fieldValue})
        "redisVal": value
      };
      debug('sentNotification:setredisRes',redisData);
      debug('sentNotification:this.redisSetUrl',this.redisSetUrl);
      const setredisRes = await this.http.post(this.redisSetUrl,redisData).toPromise();

      debug('setredisRes :', setredisRes.data);
      console.log('setredisRes :', setredisRes.data);
      // START send email to specific user with verification code
      try {
        debug('entered tryblock');
        console.log('entered tryblock');
        debug('this.config.envConfig.JWT_VERIFY_PRIVATE_KEY :', this.config.envConfig.JWT_VERIFY_PRIVATE_KEY);
        const verification_token = JWT.encodeAndSign(this.config.envConfig.JWT_VERIFY_PRIVATE_KEY,{unqrediskey,id});
        // await this.sendEmailCode(bodyParams.fieldValue, {verification_token,verification_code:verificationCode},undefined);
        debug('verification_token code:',verification_token);
        debug('bodyParams.type:', bodyParams.type);
        console.log('bodyParams.type:', bodyParams.type);
        if(bodyParams.type === "Mobile" || bodyParams.type === "ResendCodeMobile") {
          debug('Entered if:');
          debug('bodyParams val:',bodyParams);
          let mobileNum =';'
          if(bodyParams.type === "Mobile"){
            debug('TYPE:',bodyParams.type);
            mobileNum = bodyParams.fieldValue.country_code + bodyParams.fieldValue.mobile;
          } else {
            debug('TYPE1:',bodyParams.type);
            debug('bodyParams.fieldValue',bodyParams.fieldValue);
            mobileNum = bodyParams.fieldValue;
          }

          debug('mobileNum:', mobileNum);
          try {
            await this.sendSmsCode(mobileNum, verificationCode);
          } catch(err){
            debug('Err frm send sms:', err);
          }
          return {verification_token,message : "Please verify the mobile"}; 
        } else if(bodyParams.type === "Email"){
          debug('sentNotification:sendEmailCode',bodyParams);
          console.log('sentNotification:sendEmailCode',bodyParams);
          await this.sendEmailCode(id, bodyParams.fieldValue, {verification_token,verification_code:verificationCode,emailtype: bodyParams.emailtype},undefined);
          return {message : "Verification link has been sent successfully"};
        }
      } catch(err) {
        debug('entered catchblock:');
        console.log('entered catchblock:');
        throw err;
        console.error(err);
      }
    } else {
      throw new HttpException({
        name: "NOTIFICATION_DISABLED",
        message : "System error.  Plese contact your system admin to enable notification service"
      },HttpStatus.NOT_IMPLEMENTED);
    }
  }

  async verifyWithCode(requestBody: DefaultVerificationCode) {
    try {
      debug('Entered verify code based on email');

      const decodedVal:any = JWT.decodeAndVerify(this.config.envConfig.JWT_VERIFY_PUBLIC_KEY,requestBody.verification_token);

      debug('TokendecodedVal : ', decodedVal);

      const unqrediskey = decodedVal.unqrediskey;

      // if(requestBody.type==="Email") {
        const getredisRes = await this.http.get(`${this.redisGetUrl}/${unqrediskey}`).toPromise();
        debug('getredisRes :', getredisRes.data);
        debug('redis veri',getredisRes.data.verificationCode);
        debug('request body veri',requestBody.verification_code);

        if(getredisRes && getredisRes.data) {
          if(getredisRes.data.verificationCode===requestBody.verification_code) {
            console.log('getredisRes.data.fieldValue:',getredisRes.data.fieldValue);
            const updateRes = await this.updateField(decodedVal.id,getredisRes.data.fieldValue, requestBody.type);
            return updateRes;
          } 
          else {
            const userData = await UserEntity.findOne({
              where: {
                id: decodedVal.id,
              },
            });
            let sentCount = null;
            let maxSendCount = '';
            if(requestBody.type === 'Mobile'){
              sentCount = userData.resendcode_sms_count;
              maxSendCount = this.config.envConfig.MAX_RESENDCODE_SMS_RETRY;
            } 
            // else {
            //   sentCount = userData.resendcode_email_count;
            //   maxSendCount = this.config.envConfig.MAX_RESENDCODE_EMAIL_RETRY;
            // }
            
            if (sentCount >= Number(maxSendCount)) {
              debug('You exceeded the maximum tries. a/c deleted Please Signup again.');         
              throw new HttpException({
                name : 'wrongCode_AcDelted',
                message :  'Wrong verification code. u tried all ur possibilites. a/c has been deleted'
              },HttpStatus.BAD_REQUEST);     
            } else {
              debug('wrong verification');
              throw new HttpException({
                name : 'wrongCode',
                message :  'Wrong verification code'
              },HttpStatus.BAD_REQUEST);
            }
          }
        }
      // }     
    } catch (err) {
      console.error('Verify code based on email err:', err);
      debug("Invalid data: ", err.message); // Invalid data: No property: name
      debug("Invalid data1: ",err.name); // PropertyRequiredError

      if(err.message){ 
        debug('has err message');
        const mainString = err.message;
        const theWord  = "ES256";
        if (mainString.indexOf(theWord) !== -1 || err.name === 'TypeError' || err.name === 'JsonWebTokenError') {
          console.log('The word "' + theWord + '" exists in given string.');
          debug('wrong verification');
              throw new HttpException({
                name : 'INVALID_SMS_TOKEN',
                message :  'Invalid sms token'
              },HttpStatus.BAD_REQUEST);
        }
      }
      if(err && err.response && err.response.data) {
        throw new HttpException(err.response.data,err.response.status);
      }
      throw err;
    }
  }

  async updateField(id,fieldVal,type) {
    debug('email verified');
    debug('update field fieldVal:',fieldVal);
    let dbFieldToUpdate:any = {};

    let kcFieldToUpdate:any = {};
    let message = '';

    if(type === 'Email') {
      dbFieldToUpdate = {
          is_email_verified: 1,
          status: 1,
          email:fieldVal,
          resendcode_email_count: 0,
          updated_at: new Date(),
          updated_user_id: id,
      };

      kcFieldToUpdate = {
        emailVerified: true,
        enabled: true,
        email:fieldVal,
        username: fieldVal,
      };
      message = "Congratulations ! Your email address has been successfully verified."
    } else if(type === 'Mobile') {
      debug('ELSEIF:');
      if(fieldVal.country === ''){
        dbFieldToUpdate = {
          is_mobile_verified: 1,
          status: 1,
          resendcode_sms_count: 0,
          updated_at: new Date(),
          updated_user_id: id,
        };
      } else {
        const number = fieldVal.country + fieldVal.mobile;
        debug('update db:', fieldVal.label);
        dbFieldToUpdate = {
          is_mobile_verified: 1,
          status: 1,
          mobile:fieldVal.mobile,
          country_code:fieldVal.country,
          country_code_label:fieldVal.label,
          whole_number: number,
          resendcode_sms_count: 0,
          updated_at: new Date(),
          updated_user_id: id,
        };
      }
      kcFieldToUpdate = {
        enabled: true,
      };
      message = "Congratulations ! Your mobile number has been successfully verified. ";
    }
    await UserEntity.update(
        { id: id },
        dbFieldToUpdate,
    );

    // START update keycloak emailverified=true w.r.t email
    // START get keycloak token
    const keycloakToken = (await this.getKeycloak()).token;
    // END
    // START update email in keycloak
    const headers = {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${keycloakToken}`,
    };

    const addRes = await this.http
        .put(
            `${(await this.getKeycloak()).ip}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_AGENT}/users/${id}`,
            kcFieldToUpdate,
            {
              headers,
            },
        )
        .toPromise();
    // END
    // END keycloak implement
    return {message};
  }

  // START Change user mobile based on user id
  async chgUserMobile(id: string, bodyParams: UserMobileDto) {
    try {
      debug('Entered change user mobile service');

      const userExist = await UserEntity.findOne({ id });
      if (!userExist) {
        debug('Record doesnot exist');
        throw new HttpException({
          name: 'userExist',
          message: 'Record does not exist',
        },HttpStatus.BAD_REQUEST);
      }

      // to chk valid mobile or not
      const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();
      var test = phoneUtil.parseAndKeepRawInput(bodyParams.mobile, bodyParams.country_code_label);
      const validMobile = phoneUtil.isValidNumber(test);
      if (!validMobile) {
        debug('entered mobile exist');
        throw new HttpException({
          name : 'INVALID_MOBILE',
          message : 'Invalid mobile number'
        },HttpStatus.BAD_REQUEST);
      }

      const getWholeNum = bodyParams.country_code + bodyParams.mobile;
      const data = {fieldValue:bodyParams,type:"Mobile"};

      await this.isExists(userExist,getWholeNum, 'Mobile');
     
       const dbFieldToUpdate = {
            // is_mobile_verified: 0,
            // status: 0,
            // updated_at: new Date(),
            // updated_user_id: id,
        };
  
        // const kcFieldToUpdate = {
        //   enabled: false,
        // };
        
      // await UserEntity.update(
      //     { id: id },
      //     dbFieldToUpdate,
      // );

    //   // START update keycloak emailverified=true w.r.t email
    // // START get keycloak token
    // const keycloakToken = (await this.getKeycloak()).token;
    // // END
    // // START update email in keycloak
    // const headers = {
    //   'Content-Type': 'application/json',
    //   Authorization: `Bearer ${keycloakToken}`,
    // };

    // const addRes = await this.http
    //     .put(
    //         `${(await this.getKeycloak()).ip}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_AGENT}/users/${id}`,
    //         kcFieldToUpdate,
    //         {
    //           headers,
    //         },
    //     )
    //     .toPromise();
    //   // END
    //   // END keycloak implement

      return await this.sentNotification(id,data);
    } catch (err) {
      console.error('Change user mobile Err:', err);
      throw err;
    }
  }
  // END

  // change user password based on id
  async chgPassword(id: string, bodyParams: DefaultChgPswrdDto) {
    try {
      debug('Entered change user password service');

      const userExist = await UserEntity.findOne({ id });
      if(userExist.register_type === 'gmail' || userExist.register_type === 'apple'){
        throw new HttpException({
          name : 'CANNOT_RESET_PASSWORD',
          message :  `Registerd from ${userExist.register_type}. So can not reset your password`
        },HttpStatus.BAD_REQUEST);
      }

      if (bodyParams.new_password !== bodyParams.confirm_password) {
        debug('Both password should match');
        throw new HttpException({
          name : 'passordShouldMatch',
          message :  'New & Confirm Password do not match.'
        },HttpStatus.BAD_REQUEST);
      }

      // const userExist = await UserEntity.findOne({ id });
      if (!userExist) {
        debug('Record doesnot exist');
        // return 'Record doesnot exist';
        throw new HttpException({
          name : 'recordNotFound',
          message :  'Record does not exist'
        },HttpStatus.BAD_REQUEST);
      }

      const loginObj = {email:userExist.email,password:bodyParams.old_password,twoFactorAuthenticationCode:0};

      let isOldPasswordCorrect = false;
      try {
        // const loginRes:any = await this.http.post(`${this.config.envConfig.USER_SERVICE_URL}/auth/login`,loginObj).toPromise();
        const loginRes:any = await this.http.post(`${this.config.envConfig.USER_SERVICE_BASEURL}/auth/login`,loginObj).toPromise();
        debug('loginRes: ', loginRes);
        if(loginRes && loginRes.data.access_token) {
          isOldPasswordCorrect = true;
        }
      } catch (err) {
        debug('err: ', err);
        if(err && err.response && err.response.data && err.response.data.name==='OTP_ERROR') {
          isOldPasswordCorrect = true;
        }
      }

      if(isOldPasswordCorrect === false) {
        throw new HttpException({
          // message:"You have provided an invalid current password.  Please check and try again",
          message:"Invalid current password",
          "name":"INVALID_CURRENT_PASSWORD"
        },HttpStatus.BAD_REQUEST);
      }

      const keycloakToken = (await this.getKeycloak()).token;

      const headers = {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${keycloakToken}`,
      };

      const datas = {
        type: 'password',
        value: bodyParams.new_password,
        temporary: false,
      };
      const addRes = await this.http
          .put(
              `${(await this.getKeycloak()).ip}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_AGENT}/users/${userExist.id}/reset-password`,
              datas,
              {
                headers,
              },
          )
          .toPromise();
      debug('*****Change password');
      debug(addRes);
      debug('pasword changed successfully in keycloak');

      return {message: "Congratulations ! Password has been updated successfully."};

    } catch (err) {
      console.error('Change password Err:', err);
      if(err && err.response && err.response.status>=400 && err.response.status<500) {
        throw new HttpException(err.response.data,err.response.status);
      }
      throw err;
    }
  }

  // START
  // START Change user name and dob based on user id
  async chgNamePhone(id: string, bodyParams: DefaultUpdateNamePhone) {
    try {
      const userExist = await UserEntity.findOne({ id });
      if (!userExist) {
        debug('Record doesnot exist');
        // return 'Record doesnot exist';
        throw new HttpException({
          name: 'USER_NOT_FOUND',
          message: 'Record does not exist',
        },HttpStatus.BAD_REQUEST);
      }
      if(!bodyParams.full_name && !bodyParams.dob)
      {
        throw new HttpException({
          name: 'ALL_EMPTY',
          message: 'All fields should not be empty',
        },HttpStatus.BAD_REQUEST);
      }

      //chk age
      if (calculateAge(bodyParams.dob) < 21) {
        debug('enterd 21 below age');
        throw new HttpException({
          name : 'AgeValidationError',
          message : 'You must be 21 year old and above to access iqrate services'
        },HttpStatus.BAD_REQUEST);
      }

      // START chk record exist in keycloak
      // START get keycloak token
      const keycloakToken = (await this.getKeycloak()).token;
      // END

      const headers = {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${keycloakToken}`,
      };

      const isExist = await this.isUserExistKeycloak(
        userExist.user_type,
        userExist.id,
      );
      // END

      // if fullname is not blank
      if(bodyParams.full_name)
      {
        // START Keycloak update
        // START update fullname in keycloak
        const datas = {
          firstName: bodyParams.full_name,
        };

        // update name in keycloak w.r.t id
        try {
          const addRes = await this.http
            .put(
              `${(await this.getKeycloak()).ip}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_AGENT}/users/${userExist.id}`,
              datas,
              {
                headers,
              },
            )
            .toPromise();
        } catch (err) {
          if(err && err.response && err.response.status>=400 && err.response.status<500) {
            throw new HttpException(err.response.data,err.response.status);
          } else {
            console.error('name err : ', err);
            throw err;
          }
        }
        // END
        // END keycloak implement

        // update db
        await UserEntity.update(
          { id },
          {
            full_name: bodyParams.full_name
          },
        );
      }
      
      // if dob is not blank
      if(bodyParams.dob)
      {
        // const dateFormatDob = bodyParams.dob.split('-').reverse().join('-');
        const dateFormatDob = bodyParams.dob;
        await UserEntity.update(
          { id },
          {
            dob: dateFormatDob,
            updated_at: new Date(),
            updated_user_id: id,
          },
        );
      }

      // if anyone field is not empty
      // if dob is not blank
      if(bodyParams.dob || bodyParams.full_name)
      {
        await UserEntity.update(
          { id },
          {
            updated_at: new Date(),
            updated_user_id: id,
          },
        );
      }

      const res = await UserEntity.findOne({ id });
      const { full_name, email, country_code, mobile, dob } = res;
      const showData = { id, full_name, email, country_code, mobile, dob };
      // showData.dob = showData.dob.split('-').reverse().join('-'); // to reverse dob
      // return showData;
      return {message : "Your Profile has been updated successfully.", showData}; 
    } catch (err) {
      console.error('Change user name id Err:', err);
      throw err;
    }
  }
  // END

  // Admin can Toggle Email Verification status
  async activateEmail(id: string, bodyParm: EmailMobileVerifyStatusDto) {
    try {
      debug('Entered toggle email verification status');

      const userExist = await UserEntity.findOne({ id });
      if (!userExist) {
        debug('Record doesnot exist');
        // return 'Record doesnot exist';
        throw new HttpException({
          name : 'recordNotFound',
          message :  'Record does not exist'
        },HttpStatus.BAD_REQUEST);
      }

      if(userExist.deleted === 1) {
        throw new HttpException({
          name : 'ACCOUNT_DELETED',
          message :  'Account was deleted by the user'
        },HttpStatus.BAD_REQUEST);
      }

      // START chk record exist in keycloak
      // START get keycloak token
      const keycloakToken = (await this.getKeycloak()).token;
      // END

      const headers = {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${keycloakToken}`,
      };

      const isExist = await this.isUserExistKeycloak(
        userExist.user_type,
        userExist.id,
      );
      // END

      await UserEntity.update(
        { id },
        {
          is_email_verified: bodyParm.status,
          status: bodyParm.status,
          resendcode_email_count: 0,
          updated_at: new Date(),
          updated_user_id: id,
        },
      );

      // START update keycloak emailverified=true w.r.t id

      // START update email in keycloak
      const datas = {
        emailVerified: bodyParm.status,
        enabled: bodyParm.status,
      };

      const addRes = await this.http
        .put(
          `${(await this.getKeycloak()).ip}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_AGENT}/users/${userExist.id}`,
          datas,
          {
            headers,
          },
        )
        .toPromise();
      // END
      // END keycloak implement

      // START update assigned_cobrokeid tbl, if u update status= 0 in users tbl for specific id
      // if admin set cobroke = 0 for specific user before it was in active state, then need to set last_assigned_user_id = 'false' for single latest record w.r.t specific id and need to set last_assigned_user_id = 'true' for other latest record i.e exxcept this specific user id in assigned_cobrokeid table
      debug('bodyDeatials.status:', bodyParm.status);
      if(bodyParm.status === 0)
      {
        const cobrokeExist = await AssignedCobrokeEntity.findOne({
          where: {
            assigned_user_id: id,
            last_assigned_user_id: true,
          },
          order: {
            id: "DESC"
          }
        });

        // if record exist in assigned_cobrokeid table
        debug('id', id);
        debug('cobrokeExist', cobrokeExist);
        if(cobrokeExist) {
          debug('user id exist in assigned_cobrokeid table:', cobrokeExist.id)

          // delete those records from assigned_cobrokeid tbl
          const res = await AssignedCobrokeEntity.delete({ id: cobrokeExist.id });
          debug('id deleted frm assigned_cobrokeid:', cobrokeExist.id );

          // after del get latest record frm assigned_cobrokeid tbl
          const afterDelCobrokeTbl = await AssignedCobrokeEntity.findOne({
            order: {
              id: "DESC"
            }
          });

          debug('latest cobrokeid afterDelCobrokeTbl:', afterDelCobrokeTbl);
          debug('latest cobrokeid:', afterDelCobrokeTbl.assigned_user_id);

          // if above selected id is active for cobroke in users tbl
          const existUSerTbl = await UserEntity.findOne({
            where: {
              cobroke_status: 1,
              status: 1,
              id: afterDelCobrokeTbl.assigned_user_id
            }
          });

          debug('existUSerTbl:', existUSerTbl);
          // set last_assigned_user_id = true for previous latest record if cobroke status set to inactive by admin for current id
          if(existUSerTbl) {
            debug('has record')
            await AssignedCobrokeEntity.update(
              { id: afterDelCobrokeTbl.id },
              { last_assigned_user_id: true, updated_at: new Date(), updated_user_id: id },
            );
            debug('updated in assigned_cobrokeid table with true for latest record');
          }

          // // delete all entry from assigned_cobrokeid tbl w.r.t id, means that id we made it as cobroke_status = 0
          // const delCobroke = await AssignedCobrokeEntity.delete({ assigned_user_id: id });
        } else {
          debug('user id not exist in assigned_cobrokeid table')
        }
      }
      // END

      const res = await UserEntity.findOne({ id });
      const { full_name, email, country_code, mobile, dob, status } = res;
      const showData = { id, full_name, email, country_code, mobile, dob, status };

      return showData;
    } catch (err) {
      console.error('Toggle email verifiction Err:', err);
      throw err;
    }
  }

  // Admin can Toggle Phone Verification status
  async activatePhone(id: string, req: EmailMobileVerifyStatusDto) {
    try {
      debug('Entered toggle phone verification status');

      const userExist = await UserEntity.findOne({ id });
      if (!userExist) {
        debug('Record doesnot exist');
        // return 'Record doesnot exist';
        throw new HttpException({
          name : 'recordNotFound',
          message :  'Record does not exist'
        },HttpStatus.BAD_REQUEST);
      }

      if(userExist.deleted === 1) {
        throw new HttpException({
          name : 'ACCOUNT_DELETED',
          message :  'Account was deleted by the user'
        },HttpStatus.BAD_REQUEST);
      }

      if(userExist.mobile === '1234123456') {
        debug('mobile not verified');
        // return 'Record doesnot exist';
        throw new HttpException({
          name : 'GOOGLE_SIGNUP_USER',
          message :  'Invalid phone number'
        },HttpStatus.BAD_REQUEST);
      }
      // START chk record exist in keycloak
      // START get keycloak token
      const keycloakToken = (await this.getKeycloak()).token;
      // END

      const headers = {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${keycloakToken}`,
      };
      const isExist = await this.isUserExistKeycloak(
        userExist.user_type,
        userExist.id,
      );
      // END

      await UserEntity.update(
        { id },
        {
          is_mobile_verified: req.status,
          // status: req.status,
          resendcode_sms_count: 0,
          updated_at: new Date(),
          updated_user_id: id,
        },
      );

      // START update keycloak user enabled=true w.r.t id
      // // START update status in keycloak

      // const datas = {
      //   enabled: req.status,
      // };

      // const addRes = await this.http
      //   .put(
      //     `${(await this.getKeycloak()).ip}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_AGENT}/users/${userExist.id}`,
      //     datas,
      //     {
      //       headers,
      //     },
      //   )
      //   .toPromise();
      // // END
      // END keycloak implement

      const res = await UserEntity.findOne({ id });
      const { full_name, email, country_code, mobile, dob, status } = res;
      const showData = { id, full_name, email, country_code, mobile, dob, status };

      return showData;
    } catch (err) {
      console.error('Toggle phone verifiction Err:', err);
      throw err;
    }
  }

  // google register
  async googleLogin(req) {
    debug('**********enterd service');
    if (!req.user) {
      return 'No user from google';
    }
    debug('Google req.user :',req.user);
    // req.user.is_email_verified = 1;
    return this.register(req.user, 1, 'gmail');
    // return {
    //   message: 'User Info from Google',
    //   user: req.user,
    // };
  }

  // Get user delay seconds for filling verification code
  async getDelaySeconds() {
    try {
      debug(
        'Entered get user delay seconds for filling verification code',
      );
      return {
        resultcode: 200,
        delayTime: Number(this.config.envConfig.CODE_DELAY_SECONDS),
      };
    } catch (err) {
      console.error(
        'User max time to resend verification code Err:',
        err,
      );
      throw err;
    }
  }

  // Get maximum time to resend an email ans sms
  async maxResend() {
    try {
      debug(
        'Entered get user delay seconds for filling verification code',
      );
      return {
        resultcode: 200,
        sms_max_resend: Number(this.config.envConfig.MAX_RESENDCODE_SMS_RETRY),
        email_max_resend: Number(
          this.config.envConfig.MAX_RESENDCODE_EMAIL_RETRY,
        ),
      };
    } catch (err) {
      console.error(
        'User delay seconds for filling verification code Err:',
        err,
      );
      throw err;
    }
  }

  // START set user status (0=blocked, 1=active)

  async setUserStatus(id: string, bodyDeatials: DefaultUserStatusDto) {
    try {
      debug('Entered set user status service');

      const userExist = await UserEntity.findOne({ id });
      if (!userExist) {
        debug('Record doesnot exist');
        // return 'Record doesnot exist';
        throw new HttpException({
          name : 'recordNotFound',
          message :  'Record does not exist'
        },HttpStatus.BAD_REQUEST);
      }

      if(userExist.deleted === 1) {
        throw new HttpException({
          name : 'ACCOUNT_DELETED',
          message :  'Account was deleted by the user'
        },HttpStatus.BAD_REQUEST);
      }

      // START chk record exist in keycloak
      // START get keycloak token
      const keycloakToken = (await this.getKeycloak()).token;
      // END

      const headers = {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${keycloakToken}`,
      };

      const isExist = await this.isUserExistKeycloak(
        userExist.user_type,
        userExist.id,
      );
      // END

      // START Keycloak update
      // START set status in keycloak

      const setststaus = {
        enabled: bodyDeatials.status,
      };

      // set status in keycloak w.r.t keyclock id
      const addRes = await this.http
        .put(
          `${(await this.getKeycloak()).ip}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_AGENT}/users/${userExist.id}`,
          setststaus,
          {
            headers,
          },
        )
        .toPromise();
      // debug('*****UPDATE status');
      // debug(addRes);
      if (addRes.status !== 204) {
        return {
          resultcode: 400,
          errMessage: 'Error occured while updating user status in keycloak',
        };
      }
      debug('pasword updated successfully in keycloak');

      // END
      // END keycloak implement

      await UserEntity.update(
        { id },
        {
          status: bodyDeatials.status,
          updated_at: new Date(),
          updated_user_id: id,
        },
      );

      // START update assigned_cobrokeid tbl, if u update status= 0 in users tbl for specific id
      // if admin set cobroke = 0 for specific user before it was in active state, then need to set last_assigned_user_id = 'false' for single latest record w.r.t specific id and need to set last_assigned_user_id = 'true' for other latest record i.e exxcept this specific user id in assigned_cobrokeid table
      debug('bodyDeatials.status:', bodyDeatials.status);
      if(bodyDeatials.status === 0)
      {
        const cobrokeExist = await AssignedCobrokeEntity.findOne({
          where: {
            assigned_user_id: id,
            last_assigned_user_id: true,
          },
          order: {
            id: "DESC"
          }
        });

        // if record exist in assigned_cobrokeid table
        debug('id', id);
        debug('cobrokeExist', cobrokeExist);
        if(cobrokeExist) {
          debug('user id exist in assigned_cobrokeid table:', cobrokeExist.id)

          // delete those records from assigned_cobrokeid tbl
          const res = await AssignedCobrokeEntity.delete({ id: cobrokeExist.id });
          debug('id deleted frm assigned_cobrokeid:', cobrokeExist.id );

          // after del get latest record frm assigned_cobrokeid tbl
          const afterDelCobrokeTbl = await AssignedCobrokeEntity.findOne({
            order: {
              id: "DESC"
            }
          });

          debug('latest cobrokeid afterDelCobrokeTbl:', afterDelCobrokeTbl);
          debug('latest cobrokeid:', afterDelCobrokeTbl.assigned_user_id);

          // if above selected id is active for cobroke in users tbl
          const existUSerTbl = await UserEntity.findOne({
            where: {
              cobroke_status: 1,
              status: 1,
              id: afterDelCobrokeTbl.assigned_user_id
            }
          });

          debug('existUSerTbl:', existUSerTbl);
          // set last_assigned_user_id = true for previous latest record if cobroke status set to inactive by admin for current id
          if(existUSerTbl) {
            debug('has record')
            await AssignedCobrokeEntity.update(
              { id: afterDelCobrokeTbl.id },
              { last_assigned_user_id: true, updated_at: new Date(), updated_user_id: id },
            );
            debug('updated in assigned_cobrokeid table with true for latest record');
          }

          // // delete all entry from assigned_cobrokeid tbl w.r.t id, means that id we made it as cobroke_status = 0
          // const delCobroke = await AssignedCobrokeEntity.delete({ assigned_user_id: id });
        } else {
          debug('user id not exist in assigned_cobrokeid table')
        }
      }
      // END

      const res = await UserEntity.findOne({ id });
      const { full_name, email, country_code, mobile, dob, status } = res;
      const showData = { id, full_name, email, country_code, mobile, dob, status };

      return showData;
    } catch (err) {
      console.error('Update user current status err:', err);
      throw err;
    }
  }

  // get another service port i.e notifyService
  async getPort() {
    // const notifyPort = this.config.envConfig.NOTIFY_API_URL;
    const notifyPort = this.config.envConfig.NOTIFY_SERVICE_URL;
    return notifyPort;
  }

  async sendEmailCode(id, email, verificationObj:any,subject:string) {

    if(!subject) {
      subject = 'Email Verification';
    }

    const userName = await UserEntity.findOne({
      where: {
        id,
      },
    });

    const NOTIFY_SERVICE_URL = this.config.envConfig.NOTIFY_SERVICE_URL;
    console.log("NOTIFY_SERVICE_URL",NOTIFY_SERVICE_URL);
    const APP_BASE_RESET_PASSWORD_REDIRECT_URL = this.config.envConfig.APP_BASE_RESET_PASSWORD_REDIRECT_URL || 'https://dev.iqrate.io/reset-password?token=';

    const buff = Buffer.from(JSON.stringify(verificationObj));
    const tempToken = buff.toString('base64');

    let verificationLink = '';
    // let verificationLink = `http://localhost:3001/auth/verify_email/${tempToken}`;
    // let verificationLink = `${this.config.envConfig.USER_SERVICE_BASEURL}/auth/verify_email/${tempToken}`;

    // let verificationLink = `${this.config.envConfig.APP_BASE_API_URL}/auth/verify_email/${tempToken}`;
    const APP_BASE_EMAIL_VERIFIED_REDIRECT_URL = this.config.envConfig.APP_BASE_EMAIL_VERIFIED_REDIRECT_URL || 'https://dev.iqrate.io/email-verified?token=';

   debug('tempToken1:', tempToken);
    let emailTemplatName = '';
    let emailSubject = ''
    if(verificationObj.emailtype === 'ResetPassword') {
      verificationLink = `${APP_BASE_RESET_PASSWORD_REDIRECT_URL}${encodeURI(tempToken)}`;
      emailTemplatName = 'ResetPassword';
      emailSubject = 'Reset Password';
    } else {
      verificationLink = `${this.config.envConfig.APP_BASE_API_URL}/auth/verify_email/${tempToken}`;
      emailTemplatName = 'EmailVerification';
      emailSubject = 'Email Verification';
    }
    debug('emailSubject:',emailSubject);

    debug('verificationLink : ', verificationLink);
    console.log('verificationLink : ', verificationLink);

    try {
      console.log("Notify service /notify/sendVerifyEmail");
      return await this.http
          .post(`${NOTIFY_SERVICE_URL}/notify/sendVerifyEmail`, {
            to: email,
            verification_code: verificationObj.verificationCode,
            link: verificationLink,
            subject: emailSubject,
            template:emailTemplatName,
            name : userName.full_name,
          }).toPromise();
    } catch (err) {
      console.error('sendEmailCode : ',err);
      throw new HttpException({message:"System error. Failed to send notification. Please contact system support",name:"NOTIFICATION_FAILED"},HttpStatus.METHOD_NOT_ALLOWED);

    }
  }

  async sendSmsCode(mobile, verificationCode) {
    debug('sent sms');
    try {
      const output = await this.http
          // .post('http://localhost:3002/notify/sendVerifyMobile', {
          .post(`${await this.getPort()}/notify/sendVerifyMobile`, {
            to: mobile,
            verification_code: verificationCode,
            subject: 'Mobile Verification',
          }).toPromise();
      debug(output);
      return output;
    } catch (err) {
      if(err && err.data) {
        throw new HttpException(err.data,err.status);
      }
      throw err;
    }
  }

  async getKeycloak() {
    // START get keycloak token
    const keyCloakBaseUrl = this.config.envConfig.KEYCLOAK_BASE_URL;
    const keyCloakRealmMaster = this.config.envConfig.KEYCLOAK_REALM_ADMIN;

    // Keycloak connection
    const keyCloakClientID = this.config.envConfig.KEYCLOCK_CLIENT_ID;
    const keyCloakAdminClient = this.config.envConfig.KEYCLOCK_CLIENT_NAME_ADMIN;
    const keyCloakGrantType = this.config.envConfig.KEYCLOCK_GRANT_TYPE;
    const keyCloakScope = this.config.envConfig.KEYCLOCK_SCOPE;
    const keyCloakUsername = this.config.envConfig.KEYCLOCK_USERNAME;
    const keyCloakPassword = this.config.envConfig.KEYCLOCK_PASSWORD;

    const keyCloakRoleID = this.config.envConfig.KEYCLOCK_ROLE_ID;
    const keyCloakRoleName = this.config.envConfig.KEYCLOCK_ROLE_NAME;

    debug('ENV VARIABLE : ', {keyCloakClientID, keyCloakAdminClient, keyCloakGrantType, keyCloakScope, keyCloakUsername, keyCloakPassword});

    const bodyData = `client_id=${keyCloakAdminClient}&grant_type=${keyCloakGrantType}&scope=${keyCloakScope}&username=${keyCloakUsername}&password=${keyCloakPassword}`;

    debug('bodyData : ', bodyData);

    try {
      const tokenRes = await this.http
          .post(
              `${keyCloakBaseUrl}/realms/${keyCloakRealmMaster}/protocol/openid-connect/token`,
              bodyData,
              {
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded',
                },
              },
          )
          .toPromise();
      return {
        token: tokenRes.data.access_token,
        ip: keyCloakBaseUrl,
      };
    } catch(err) {
      if ( err && err.response && err.response.status >= 400 && err.response.status < 500 ) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('login err : ', err);
        throw err;
      }
    }
  }

  async getKeycloakRealm(userType) {
    let keyCloakRealm = '';
    if (userType === 'admin') {
      keyCloakRealm = this.config.envConfig.KEYCLOAK_REALM_ADMIN;
    } else {
      keyCloakRealm = this.config.envConfig.KEYCLOAK_REALM_AGENT;
    }
    debug(keyCloakRealm);
    return {
      relam: keyCloakRealm,
    };
  }

  async loginKeycloak(userType, username, pass) {
    // let loginRes = {};
    let resCode = null;
    const adminClientID = this.config.envConfig.KEYCLOCK_CLIENT_NAME_ADMIN;
    const agentClientID = this.config.envConfig.KEYCLOCK_CLIENT_NAME_AGENT;
    const grantType = this.config.envConfig.KEYCLOCK_GRANT_TYPE;
    if (userType === 'admin') {
      const bodyData = `client_id=${adminClientID}&grant_type=${grantType}&username=${username}&password=${pass}`;

      try {
        const loginRes = await this.http
            .post(
                `${this.config.envConfig.KEYCLOAK_BASE_URL}/realms/${this.config.envConfig.KEYCLOAK_REALM_ADMIN}/protocol/openid-connect/token`,
                bodyData,
                {
                  headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                  },
                },
            )
            .toPromise();

        return loginRes.data;

      } catch(err) {
          if ( err && err.response && err.response.status >= 400 && err.response.status < 500 ) {
            throw new HttpException(err.response.data, err.response.status);
          } else {
            console.error('login err : ', err);
            throw err;
          }
      }
    } else {
      // if it is agent
      const bodyData = `client_id=${agentClientID}&grant_type=${grantType}&username=${username}&password=${pass}`;

      try {
        const loginRes = await this.http
            .post(
                `${this.config.envConfig.KEYCLOAK_BASE_URL}/realms/${this.config.envConfig.KEYCLOAK_REALM_AGENT}/protocol/openid-connect/token`,
                bodyData,
                {
                  headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                  },
                },
            )
            .toPromise();

        return loginRes.data;
      } catch(err) {
      if ( err && err.response && err.response.status >= 400 && err.response.status < 500 ) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('login err : ', err);
        throw err;
      }
    }
    }
  }

  async isUserExistKeycloak(usertype, userid) {
    // START get keycloak token
    const keycloakToken = (await this.getKeycloak()).token;
    // END

    const headers = {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${keycloakToken}`,
    };

    const found = await this.http
      .get(
        // 'http://54.255.196.41:8080/auth/admin/realms/master/users',
        `${(await this.getKeycloak()).ip}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_AGENT}/users/${userid}`,
        {
          headers,
        },
      )
      // .toPromise();
      .toPromise()
      .then(({ data }) => {})
      .catch((err) => {
        // throw new NotFoundException(`Record doesnot exist in keycloak`);
        if ( err && err.response && err.response.status >= 400 && err.response.status < 500 ) {
          throw new HttpException(err.response.data, err.response.status);
        } else {
          console.error('login err : ', err);
          throw err;
        }
      });
  }

  // chk resend code after 1 min
  async chkResendCodeTime(value) {
    if(value)
    {
      let dbsenttime = value || 0; 
      if(value ==null || value=="")
      {
        dbsenttime = 0;
      }
      const nowdate = new Date();
      debug('nowTime:',nowdate.getTime());
      debug('dbTime:', dbsenttime.getTime());

      console.log('nowTime:',nowdate.getTime());
      console.log('dbTime:', dbsenttime.getTime());
      const difference = nowdate.getTime() - dbsenttime.getTime(); // This will give difference in milliseconds
      // var resultInMinutes = Math.round(difference / 60000);
      const resultInsseconds = difference / 1000;
      if (resultInsseconds < Number(this.config.envConfig.CODE_DELAY_SECONDS))
      {
        throw new HttpException({
          name : 'CANNOT_RESEND',
          message :  'Try after 1 minute'
        },HttpStatus.BAD_REQUEST);  
      } 
    }
  }

  async getRefreshToken(reqParam: RefreshDto) {
    try {
      let userExist;
      let decodeToken = JSON.parse(Buffer.from(reqParam.refresh_token.split('.')[1], 'base64').toString());

      debug('tokenval :', decodeToken);
      const id = decodeToken.sub;
      const usrType = decodeToken.azp;
      if(decodeToken.typ !== 'Refresh') {
        throw new HttpException({
          name: 'NOT_REFRESH_TOKEN',
          message: 'Given token is not a refresh token',
        },HttpStatus.BAD_REQUEST);
      }

      if(usrType === 'agent'){
       userExist = await UserEntity.findOne({ id });
      } else {
        userExist = await AdminEntity.findOne({ id });
      }
     
      if (!userExist) {
        debug('Record doesnot exist');
        // return 'Record doesnot exist';
        throw new HttpException({
          name: 'USER_NOT_FOUND',
          message: 'Record does not exist',
        },HttpStatus.BAD_REQUEST);
      } 

      // if(userExist.refresh_token !== reqParam.refresh_token){
        const dbAllRefreshToken = userExist.refresh_token
        debug('dbAllRefreshToken:', dbAllRefreshToken);
        debug('entered token:', reqParam.refresh_token);
        debug('valp:', `'${reqParam.refresh_token}`);
        if(!dbAllRefreshToken.includes(reqParam.refresh_token)) {
        debug('invalid  refresh token');
        // return 'Record doesnot exist';
        throw new HttpException({
          name: 'INVALID_REFRESH_TOKEN',
          message: 'Invalid refresh token',
        },HttpStatus.BAD_REQUEST);
      } else {
        debug('token includes');
      }

      // Keycloak connection
      const keyCloakBaseUrl = this.config.envConfig.KEYCLOAK_BASE_URL;
      let keyCloakClientId = '';
      let keyCloakRealm = '';
      if(usrType === 'agent')
      {
        keyCloakClientId = this.config.envConfig.KEYCLOCK_CLIENT_NAME_AGENT;
        keyCloakRealm = this.config.envConfig.KEYCLOAK_REALM_AGENT;
      } else {
        keyCloakClientId = this.config.envConfig.KEYCLOCK_USERNAME;
        keyCloakRealm = this.config.envConfig.KEYCLOAK_REALM_ADMIN;
      }

      // const keyCloakAgentClient = this.config.envConfig.KEYCLOCK_CLIENT_NAME_AGENT;
      // const keyCloakAdminClient = this.config.envConfig.KEYCLOCK_CLIENT_NAME_ADMIN;
      const keyCloakRefreshGrantType = this.config.envConfig.KEYCLOCK_REFRESH_GRANT_TYPE || 'refresh_token';

      const bodyData = `client_id=${keyCloakClientId}&grant_type=${keyCloakRefreshGrantType}&refresh_token=${reqParam.refresh_token}`;

      debug('bodyData : ', bodyData);
  
      try {
        const tokenRes = await this.http
            .post(
                `${keyCloakBaseUrl}/realms/${keyCloakRealm}/protocol/openid-connect/token`,
                bodyData,
                {
                  headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                  },
                },
            )
            .toPromise();

            // update refresh token in db
            let dbRefreshTokenArr = userExist.refresh_token;
            debug('tokenLen:', dbRefreshTokenArr); 
            const index = dbRefreshTokenArr.indexOf(reqParam.refresh_token); // it gives the index of the array
            if (index !== -1) {
              dbRefreshTokenArr[index] = tokenRes.data.refresh_token;
            }
            if(usrType === 'agent')
            {
              await UserEntity.update(
                { id: id },
                { refresh_token: dbRefreshTokenArr, updated_at: new Date(), updated_user_id: id },
              );
            } else {
              await AdminEntity.update(
                { id: id },
                { refresh_token: dbRefreshTokenArr, updated_at: new Date(), updated_user_id: id },
              );
            }
        return {
          // data: tokenRes.data,
          access_token: tokenRes.data.access_token,
          expires_in: tokenRes.data.expires_in,
          refresh_token: tokenRes.data.refresh_token,
          refresh_expires_in: tokenRes.data.refresh_expires_in,
        };
      } catch(err) {
        if ( err && err.response && err.response.status >= 400 && err.response.status < 500 ) {
          throw new HttpException(err.response.data, err.response.status);
        } else {
          console.error('refresh err : ', err);
          throw err;
        }
      }
    } catch(err) {
      throw err;
    }
  }

  // update referal code
  async updateReferalCode(email: string, reqParm: DefaultReferalDto) {
    try {
      debug('Entered update referal code service');

      const userExist = await UserEntity.findOne({
        where: {
          email,
        },
      });
      if (!userExist) {
        debug('Record doesnot exist');
        // return 'Record doesnot exist';
        throw new HttpException({
          name : 'recordNotFound',
          message :  'Record does not exist'
        },HttpStatus.BAD_REQUEST);
      }
 
      // chk for refercode exist
      debug('reqBody.referal_code:', reqParm.referal_code);
      if(reqParm.referal_code !== '') {
       
        // chk refercode exist or not
        const codeRecordExist = await CampaignEntity.findOne({
          referal_code: reqParm.referal_code
        });
        if (!codeRecordExist) {
          throw new HttpException({
            name : 'INVALID_REFERRAL_CODE',
            message : 'Invalid referral code'
          },HttpStatus.BAD_REQUEST);
        }
        if(codeRecordExist){
          const data = await CampaignEntity.findOne({
            referal_code: reqParm.referal_code
          });
          debug('data:', data);
          debug('usageLimt in campaign:', data.usage_limt);

          // chk referal code usage limt exceeds or not
          // chk referal code exist in user tbl
          const UserReferalExist = await UserEntity.findOne({
            referal_code: reqParm.referal_code
          });
          if(UserReferalExist) {
            const { usedCodeCount } = await UserEntity
            .createQueryBuilder('c')
            .select("COUNT(c.referal_code)", "usedCodeCount")
            .where("c.referal_code = :code", { code: reqParm.referal_code })
            .getRawOne();
            debug('usedCodeCount in register:', usedCodeCount)
            if( data.usage_limt < usedCodeCount  || data.usage_limt == usedCodeCount  ) {
              throw new HttpException({
                name : 'EXCEED_USAGE_LIMT',
                message : 'Referral code usage limit is exceeded'
              },HttpStatus.BAD_REQUEST);
            }
          }

          if( data.status === 0 ) {
            throw new HttpException({
              name : 'REFERRAL_CODE_EXPIRED',
              message : 'Invalid referral code'
            },HttpStatus.BAD_REQUEST);
          }

          // chk referal code is expired or not
          const start = new Date(data.start_date);
          start.setHours(24, 0, 0, 0);
          const end = new Date(data.end_date);
          end.setHours(24, 0, 0, 0);
          const current = new Date();
          current.setHours(24, 0, 0, 0); // next midnight
          debug('start:', start);
          debug('end:', end);
          debug('current:', current);
          if (current < start)
          {
            debug('entered date');
            throw new HttpException({
              name: 'REFERRAL_CODE_EXPIRED',
              message: 'Referral code can be used in future',
            },HttpStatus.BAD_REQUEST);
          }
        }
        await UserEntity.update(
          { id: userExist.id },
          { referal_code: reqParm.referal_code, updated_at: new Date(), updated_user_id: userExist.id },
        );
        return {message : "Referral code has been updated successfully"}; 
      }
    } catch (err) {
      console.error('Update referal code Err:', err);
      throw err;
    }
  }

  // add contact us
  public async contactUs(id: string, reqPar: ContactUsDto) {
    try {
      debug('Entered contact us');
      const contact = new ContactUs();
      contact.name = reqPar.name;
      contact.email = reqPar.email;
      contact.message = reqPar.message;
      contact.created_at = new Date();
      contact.created_user_id = id;
      const res = await ContactUs.save(contact);
      debug('res : ', res);

      // send out an email
      try {
        console.log("Notify service /notify/sendVerifyEmail");
        const sendContact =  await this.http
            .post(`${this.config.envConfig.NOTIFY_SERVICE_URL}/notify/sendVerifyEmail`, {
              to: reqPar.email,
              subject: 'Contact Us',
              template: 'ContactUsTemplate',
              name : reqPar.name,
            }).toPromise();
      } catch (err) {
        console.error('sendcontactus : ',err);
        throw new HttpException({message:"System error. Failed to send notification. Please contact system support",name:"NOTIFICATION_FAILED"},HttpStatus.METHOD_NOT_ALLOWED);
  
      }
      return {message: 'We have sent an email to your email id ', data: res};
    } catch (err) {
        console.error('contact us Err:', err);
        throw err;
    }
  }

  // assign cobroke id
  public async assign_coBrokeId() {
    try {
      const mappedCobrokeUsers = await UserEntity.find({
        where: {
          cobroke_status: 1,
          status: 1,
        },
        order: {
          created_at: 'ASC',
        },
      });
      debug('mappedCobroke:', mappedCobrokeUsers);
      if(!mappedCobrokeUsers[0]){
        debug('No cobroke user');
            throw new HttpException({
              name: 'COBROKE_NOT_FOUND',
              message: 'Cobroke user list is empty. Please contact admin to set cobroke',
            },HttpStatus.BAD_REQUEST);
      }
      
      const assignedCobroke = await AssignedCobrokeEntity.find({
        where: {
          last_assigned_user_id: true,
        }
      });
      debug('assignedCobroke data:', assignedCobroke);
      let getUserIdforcoBroke;
      if(!assignedCobroke[0]) {
        debug('mappedCobroke:', mappedCobrokeUsers);
        debug('mappedCobroke[0]:', mappedCobrokeUsers[0]);        
        getUserIdforcoBroke = mappedCobrokeUsers[0].id;
      } else {
        let obj = mappedCobrokeUsers.find(o => o.id === assignedCobroke[0].assigned_user_id);
        getUserIdforcoBroke = obj;
        debug('else part');
        for(let i = 0; i < mappedCobrokeUsers.length; i++)
        {
           if(mappedCobrokeUsers[i].id === assignedCobroke[0].assigned_user_id) {
            debug('enterd if array length:', i);
            debug('mappedCobrokeUsers.length:', mappedCobrokeUsers.length-1);
            if(i === mappedCobrokeUsers.length-1){
              i = -1;
              debug('i val:', i);
              debug('next array id:', mappedCobrokeUsers[i+1]); 
            }
            debug('next id is:', mappedCobrokeUsers[i+1].id);
            getUserIdforcoBroke = mappedCobrokeUsers[i+1].id;
            break;
           }
        }
        // return getUserIdforcoBroke;
      }

      // update assigned_cobrokeid tbl with false for all previous record
      if(assignedCobroke[0]) {
        await AssignedCobrokeEntity.update({ assigned_user_id: assignedCobroke[0].assigned_user_id},
          {last_assigned_user_id: false},
        );
      }

     

      // // chk userid already assigned or not
      // const existAssignedCobroke = await AssignedCobrokeEntity.find({
      //   where: {
      //     assigned_user_id: getUserIdforcoBroke,
      //   }
      // });

      // debug('existAssignedCobroke:', existAssignedCobroke);
      // // if already assigned just update his record with true
      // if(existAssignedCobroke[0]){
      //   debug('already in assigned tbl');
      //   debug('getUserIdforcoBroke:', getUserIdforcoBroke);
      //   await AssignedCobrokeEntity.update({ assigned_user_id: getUserIdforcoBroke},
      //     { last_assigned_user_id: true,
      //       assigned_at: new Date(),
      //     },
      //   );
      // } else {

        // insert into assigned_cobrokeid tbl
        const addCobroke = new AssignedCobrokeEntity();
        addCobroke.assigned_user_id = getUserIdforcoBroke;
        addCobroke.last_assigned_user_id = true;
        addCobroke.assigned_at = new Date();
        const res = await AssignedCobrokeEntity.save(addCobroke);
      // }
      
      return getUserIdforcoBroke;

      
      // return mappedCobroke;
    } catch (err) {
      console.error('assign_coBrokeId Err:', err);
      throw err;
    }
  }

  //upload user export csv to s3 bucket
async uploadCSV(bucketName, fileName, filePath, resp) {
  try {
  return new Promise((resolve, reject) => {
    fs.readFile(filePath, (err, datauser) => { // to read file from folder
      if (err) reject(err);
        // Setting up S3 upload parameters
        // const AWS_EXPORT_USER_BUCKET_NAME =  this.config.envConfig.AWS_EXPORT_USER_BUCKET_NAME || 'iqrate-exportusers';
        const AWS_EXPORT_USER_BUCKET_NAME =  bucketName || 'iqrate-exportusers';
        const AWS_ACCESS_KEY_ID =  this.config.envConfig.AWS_ACCESS_KEY_ID || 'AKIARMFLUTLWW52WJ2CB';
        const AWS_SECRET_ACCESS_KEY =  this.config.envConfig.AWS_SECRET_ACCESS_KEY || '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';

        const fileContent = Buffer.from(datauser, 'base64');
        const d=new Date();
        // const myFileName = 'userlist'+ '_' +d.getTime()+ '.csv'; 
        const myFileName = fileName+ '_' +d.getTime()+ '.csv'; 

        const params = {
          Bucket: AWS_EXPORT_USER_BUCKET_NAME,
          Key: myFileName, // File name you want to save as in S3
          Body: fileContent,
          ContentDisposition: 'attachment'
        };

        const s3 = new AWS.S3({
          accessKeyId: AWS_ACCESS_KEY_ID,
          secretAccessKey: AWS_SECRET_ACCESS_KEY
        });

        // Uploading files to the bucket
        return new Promise((res, rej) => {
          s3.upload(params, function (err, data) {
            if (err) {
              return rej(err);
            }
            console.log(`user list csv uploaded to s3 successfully. ${data.Location}`);
            // return res.send(`File backup to s3 successfully. ${data.Location}`);
            fs.unlinkSync(filePath);
            resp.send({ url: data.Location});
          });
        });
    });
  });
} catch (err) {
  throw err;
}
}

  // export userlist to csv for admin
  public async exportUserCSV(reqParam: ExportItem, resp) {
    try {
      debug('enterd csv service');

      const userSelect = reqParam.exportItem;
      const arrayLen = userSelect.length;
      debug('userSelect:', userSelect);
      debug('lengt:', arrayLen);
      if(arrayLen === 0){
        throw new HttpException({
          name : 'EXPORT_FIELD_EMPTY',
          message : 'To export user, should select atleast 1 value'
        },HttpStatus.BAD_REQUEST);
      }
      const getVal = reqParam.exportItem;
      let userDetails;

      if(getVal.includes("all")) {
        if(arrayLen > 1){
          throw new HttpException({
            name : 'CANNOT_CHK_ALLCHKBOX',
            message : 'Please select only one checkbox to export users list'
          },HttpStatus.BAD_REQUEST);
        }
      }

      // if export for "all"
      if(getVal.includes("all")) {
        debug('enterd if');
        userDetails = await UserEntity
        .createQueryBuilder('u')
        .leftJoinAndSelect("u.bankers", "bank")
        .select(['u.full_name', 'u.mobile', 'u.email', 'u.dob', 'u.membership_type','u.status', 'bank.bank_name','bank.bank_ac_number'] )
        .orderBy('u.full_name', 'ASC')
        .getMany();
      } else
      {
        try {
          userDetails = await UserEntity
          .createQueryBuilder('u')
          .leftJoinAndSelect("u.bankers", "bank")
          .where("u.id IN (:...ids)", { ids: userSelect })
          .select(['u.full_name', 'u.mobile', 'u.email', 'u.dob', 'u.membership_type','u.status', 'bank.bank_name','bank.bank_ac_number'] )
          .orderBy('u.full_name', 'ASC')
          .getMany();
          if(userDetails.length === 0){
            debug('no user');
            throw new HttpException({
              name : 'USER_NOT_FOUND',
              message : 'User does not exist'
            },HttpStatus.BAD_REQUEST);
          } else {
            debug('hav user');
          }
        } catch (err) {
          throw new HttpException({
            name : 'USER_NOT_EXIST',
            message : 'User does not exist'
          },HttpStatus.BAD_REQUEST);
        }
      }

      var userArr = [];
      var len = userDetails.length;
      for (var i = 0; i < len; i++) {
        let bkac = '-----';
        let bkname = '-----';
        let userStatus = '';
        debug(`${i} length:${userDetails[i].bankers.length}`)
        if(userDetails[i].bankers.length>0) {
          debug(`enterred at ${i}`);
          bkname = userDetails[i].bankers[0]['bank_name'];
          bkac = userDetails[i].bankers[0]['bank_ac_number'];
          debug('bansk:', bkname);
        }
        
        if(userDetails[i].status === 0){
          userStatus = 'Inactive';
        } else {
          userStatus = 'Active';
        }
        
        userArr.push({
            Name: userDetails[i].full_name,
            Mobile: userDetails[i].mobile,
            Email: userDetails[i].email,
            DOB: userDetails[i].dob,
            Subscription: userDetails[i].membership_type,
            Status: userStatus,
            Bank: bkname,
            AccountNo: bkac,
        });
      }
      const json2csvParser = new Json2csvParser({ header: true});
      const csv = json2csvParser.parse(userArr);
      // fs.writeFile("export/bezkoder_mysql_fs_new.csv", csv, async function(error) {
      //   if (error) throw error;
      //   console.log("Write to bezkoder_mysql_fs.csv successfully!");
      //   return await uploadCSV('export/bezkoder_mysql_fs_new.csv', resp);
      // });

      const dt=new Date();
      const csvName = 'fetcheduserlist'+ '_' +dt.getTime()+ '.csv';
      const absPath = path.join(__dirname, csvName);
      const parent_this = this;
      const s3buktName = this.config.envConfig.AWS_EXPORT_USER_BUCKET_NAME;
      const fileNme = "userlist";
      fs.writeFile(absPath, csv, async function(error) {
        if (error) throw error;
        console.log("Write fetcheduserlist.csv to folder successfully!");
        // return await uploadCSV(absPath, resp);
        return await parent_this.uploadCSV(s3buktName, fileNme, absPath, resp);

      });
    } catch (err) {
      throw err;
    }
  }

   // export user subscription info into csv
   public async exportsubscriptionDetCSV(reqParam: ExportSubscriptionItem, resp) {
    try {
      debug('enterd user subscription export csv service');
      const planName = ['BASIC', 'PREMIUM', 'PREMIUM+'];

      // const userSelect = reqParam.ExportSubscriptionItem;
      let AggregateMonthlyUsers, fectdMonthYr;
      AggregateMonthlyUsers = await getConnection().getRepository('AggregateUsersView').find();
      // req.query.startMonth = '03';
      // req.query.startYear = '2022';
      // req.query.endMonth = '05';
      // req.query.endYear = '2022';
      debug('before AggregateMonthlyUsers:', AggregateMonthlyUsers);
        // get year and month
        let monthVal =  reqParam.startMonth;
        let yearVal =  reqParam.startYear;
        let endMnt =  reqParam.endMonth;
        let endYr =  reqParam.endYear;
        // debug(typeof reqParam.startMonth);
      if(reqParam.startMonth !== null && reqParam.startMonth !== undefined  && reqParam.startYear !== null && reqParam.startYear !== undefined && reqParam.endMonth !== null && reqParam.endMonth !== undefined && reqParam.endYear !== null && reqParam.endYear !== undefined)
      {
        // get all month and yr and add it in array for monthly subscription info
        fectdMonthYr = await this.adminService.getMonthYr(monthVal,yearVal,endMnt,endYr);
        debug('fectdYrs:', fectdMonthYr);
        
        // get all yr and add it in array for yearly subscription info
        let y;
        let fecthedYr = [];
        for(y=yearVal; y<=endYr; y++) 
        {
          let a = ''+y;
          fecthedYr.push(a);
        }
        debug('len:', fecthedYr.length);
        // fecthedYr.map((element) => element.replace(/[^a-zA-Z ]/, ' '));
        debug('fecthedYr:', fecthedYr);
        
        // for monthly user
        const stringfiedArr =  JSON.stringify(AggregateMonthlyUsers);
        debug('stringfied AggregateMonthlyUsers:', stringfiedArr);
        const parsedArr = JSON.parse(stringfiedArr);
        debug('parased AggregateMonthlyUsers:', parsedArr);
        if (fectdMonthYr.length > 0)
        {
          const monthYrList = new Set(fectdMonthYr);
          debug('monthYrList:', monthYrList);
          AggregateMonthlyUsers= parsedArr.filter(item=>monthYrList.has(item.JoinedMonth)===true);
          debug('hai:',AggregateMonthlyUsers);
        }

        debug('fetched for month:', AggregateMonthlyUsers);
      }
     
      var subscriotionArr = [];
      var monthSublen = AggregateMonthlyUsers.length;
      let plans, yrName;
      debug('fectdMonthYr:', fectdMonthYr);
      debug('fectdMonthYr.len:', fectdMonthYr.length);
      for (var k = 0; k < fectdMonthYr.length; k++) {  //[2022_06, 2022_07, 2022_08]
        debug('for-1:', k);
        plans = ['BASIC', 'PREMIUM', 'PREMIUM+'];
        yrName = ['month', 'year'];
        for (var p = 0; p < plans.length; p++) { 
          let monthlySubAmt = null;
          let yearlySubAmt = null;
          let monthlySubUSer = null;
          let yearlySubUser = null;
          let monthFilled = false;
          let yearFilled = false;
          let loopTime = 0;
          let fullYear;
          let fullMonth;
          for (var i = 0; i < monthSublen; i++) {
            debug('for-2:', i);
            loopTime++;
            //to fetch year details
            debug('AggregateMonthlyUsers[i].JoinedMonth:', AggregateMonthlyUsers[i].JoinedMonth);
            debug('fectdMonthYr[k]:', fectdMonthYr[k]);
            if(AggregateMonthlyUsers[i].JoinedMonth === fectdMonthYr[k])
            {
              debug('if-1:');
              
              debug('AggregateMonthlyUsers[i].MembershipType:', AggregateMonthlyUsers[i].MembershipType);
              debug('plans[p]:', plans[p]);
              if(AggregateMonthlyUsers[i].MembershipType === plans[p])
              {
                debug('if-2');
                // for (var y = 0; y < yrName.length; y++) { // 2 times loop[month, year]
                  debug('AggregateMonthlyUsers[i].plan:', AggregateMonthlyUsers[i].plan);
                  debug('monthFilled:', monthFilled);
                  if(AggregateMonthlyUsers[i].plan === 'month' && !monthFilled) {
                    debug('enter month:');
                    monthFilled = true;
                    monthlySubAmt = AggregateMonthlyUsers[i].totalAmount
                    monthlySubUSer =  AggregateMonthlyUsers[i].totalUsers;
                    fullMonth =  AggregateMonthlyUsers[i].monthname;
                    fullYear =  AggregateMonthlyUsers[i].year;

                  }
                  debug('yearFilled:', yearFilled);
                  if(AggregateMonthlyUsers[i].plan === 'year' && !yearFilled) {
                    debug('enter yr:');
                    yearFilled = true;
                    yearlySubAmt = AggregateMonthlyUsers[i].totalAmount;
                    yearlySubUser =  AggregateMonthlyUsers[i].totalUsers;
                    fullMonth =  AggregateMonthlyUsers[i].monthname;
                    fullYear =  AggregateMonthlyUsers[i].year;
                  }
                // }
              }
            }
            if(loopTime === monthSublen || (yearFilled && monthFilled))
            {
              debug('AggregateMonthlyUsers:',AggregateMonthlyUsers[i]);
              // const yrname = AggregateMonthlyUsers[i].monthname +' - '+ AggregateMonthlyUsers[i].year;
              const yrname = fullMonth +' - '+ fullYear;
              debug('yrname', yrname);
              if (monthlySubUSer == null) {
                debug('monthlySubUSer');
                monthlySubUSer = 0;
              }
              if (yearlySubUser == null) {
                debug('yearlySubUser');
                yearlySubUser = 0;
              }
              if (monthlySubAmt == null) {
                debug('monthlySubAmt');
                monthlySubAmt = 0;
              }
              if (yearlySubAmt == null) {
                debug('yearlySubAmt');
                yearlySubAmt = 0;
              }
              subscriotionArr.push({
                // Date: yrname,
                Year_Month: fectdMonthYr[k],
                User_Type: plans[p],
                // SubscribedPlan: AggregateMonthlyUsers[i].plan,
                Total_User:  parseInt(monthlySubUSer) + parseInt(yearlySubUser), 
                // TotalUser:  monthlySubUSer + yearlySubUser, 
                Monthly_Subscriber_User:  monthlySubUSer, 
                Yearly_Subscribed_User: yearlySubUser, 
                Monthly_Subscription_Fee:  monthlySubAmt, 
                Yeraly_Subscription_Fee: yearlySubAmt, 
                Toltl_Collected_Fees:  parseInt(monthlySubAmt) + parseInt(yearlySubAmt), 
                // ToltlFees:  monthlySubAmt + yearlySubAmt, 
                // TotalFeesCollected:  parseInt(AggregateMonthlyUsers[i].totalAmount) + parseInt(AggregateYearlyUsers[j].totalAmount),
              });
              break;
            }
          }
          
        }
      }
      const json2csvParser = new Json2csvParser({ header: true});
      debug('subscriotionArr:', subscriotionArr);
      const csv = json2csvParser.parse(subscriotionArr);

      const dt=new Date();
      const csvName = 'subscriptioninfo'+ '_' +dt.getTime()+ '.csv';
      const absPath = path.join(__dirname, csvName);
      const parent_this = this;
      const s3buktName = this.config.envConfig.AWS_EXPORT_USER_BUCKET_NAME;
      const fileNme = "subscriptioninfo";
      fs.writeFile(absPath, csv, async function(error) {
        if (error) throw error;
        console.log("Write subscriptioninfo.csv to folder successfully!");
        // return await uploadCSV(absPath, resp);
        return await parent_this.uploadCSV(s3buktName, fileNme, absPath, resp);

      });
    } catch (err) {
      throw err;
    }
  }

  // export revenue info into csv
  public async exportRevenueDetCSV(reqParam: ExportSRevennueItem, resp) {
    try {
      debug('enterd revenue export csv service');
      const planName = ['BASIC', 'PREMIUM', 'PREMIUM+'];

      // const userSelect = reqParam.ExportSubscriptionItem;
      let MonthlySubRevenueReprt, fectdMonthYr, MonthCleareanceRevenueReprt;
      MonthlySubRevenueReprt = await getConnection().getRepository('UsersSubscriptionRevenueView').find();

      //START
      const headers = {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${reqParam.token}`,
      };
      try {
        MonthCleareanceRevenueReprt = await this.http
          .get(
            `${this.config.envConfig.CUSTOMER_SERVICE_URL}/customer-admin/dashboard/clearance_fee`,
            // 'https://dev-api.iqrate.io/customer-admin/dashboard/clearance_fee',
            {
              headers,
            },
          )
          .toPromise();
      } catch (err) {
        if(err && err.response && err.response.status>=400 && err.response.status<500) {
          throw new HttpException(err.response.data,err.response.status);
        } else {
          console.error('customer service clearance report err : ', err);
          throw err;
        }
      }
    
      debug('MonthlyCleareanceRevenueReprt:', MonthCleareanceRevenueReprt.data);
      //END

      // req.query.startMonth = '03';
      // req.query.startYear = '2022';
      // req.query.endMonth = '05';
      // req.query.endYear = '2022';
      debug('before MonthlySubRevenueReprt:', MonthlySubRevenueReprt);
        // get year and month
        let monthVal =  reqParam.startMonth;
        let yearVal =  reqParam.startYear;
        let endMnt =  reqParam.endMonth;
        let endYr =  reqParam.endYear;
        // debug(typeof reqParam.startMonth);
      if(reqParam.startMonth !== null && reqParam.startMonth !== undefined  && reqParam.startYear !== null && reqParam.startYear !== undefined && reqParam.endMonth !== null && reqParam.endMonth !== undefined && reqParam.endYear !== null && reqParam.endYear !== undefined)
      {
        // get all month and yr and add it in array for monthly subscription info
        fectdMonthYr = await this.adminService.getMonthYr(monthVal,yearVal,endMnt,endYr);
        debug('fectdYrs:', fectdMonthYr);
        
      
        // for monthly user
        const stringfiedArr =  JSON.stringify(MonthlySubRevenueReprt);
        debug('stringfied MonthlySubRevenueReprt:', stringfiedArr);
        const parsedArr = JSON.parse(stringfiedArr);
        debug('parased MonthlySubRevenueReprt:', parsedArr);
        if (fectdMonthYr.length > 0)
        {
          const monthYrList = new Set(fectdMonthYr);
          debug('monthYrList:', monthYrList);
          MonthlySubRevenueReprt= parsedArr.filter(item=>monthYrList.has(item.JoinedMonth)===true);
          debug('hai:',MonthlySubRevenueReprt);
        }
        debug('fetched for month:', MonthlySubRevenueReprt);
      }
     
      var subscriotionArr = [];
      var monthSublen = MonthlySubRevenueReprt.length;
      for (var i = 0; i < monthSublen; i++) {
        // const result = MonthCleareanceRevenueReprt.data.find(({ JoinedMonth }) => JoinedMonth === MonthlySubRevenueReprt[i].JoinedMonth);
      let clearanceFee = 0;
        for (var j = 0; j < MonthCleareanceRevenueReprt.data.length; j++) {
          if(MonthlySubRevenueReprt[i].month == MonthCleareanceRevenueReprt.data[j].month && MonthlySubRevenueReprt[i].year == MonthCleareanceRevenueReprt.data[j].year) {
            clearanceFee = MonthCleareanceRevenueReprt.data[j].totalAmount;
          }
        }
        debug('clearanceFee:', clearanceFee);
        subscriotionArr.push({
          // Date: yrname,
          Year_Month: MonthlySubRevenueReprt[i].JoinedMonth,
          Subscription_Fees:  MonthlySubRevenueReprt[i].totalAmount, 
          Clearance_Fees:  clearanceFee, 
        });
      }
    
      const json2csvParser = new Json2csvParser({ header: true});
      debug('subscriotionArr:', subscriotionArr);
      const csv = json2csvParser.parse(subscriotionArr);

      const dt=new Date();
      const csvName = 'revenueinfo'+ '_' +dt.getTime()+ '.csv';
      const absPath = path.join(__dirname, csvName);
      const parent_this = this;
      const s3buktName = this.config.envConfig.AWS_EXPORT_USER_BUCKET_NAME;
      const fileNme = "revenueinfo";
      fs.writeFile(absPath, csv, async function(error) {
        if (error) throw error;
        console.log("Write revenueninfo.csv to folder successfully!");
        // return await uploadCSV(absPath, resp);
        return await parent_this.uploadCSV(s3buktName, fileNme, absPath, resp);

      });
    } catch (err) {
      throw err;
    }
  }

  // export user summary into csv
  public async exportUserSummaryCSV(reqParam: ExportSubscriptionItem, resp) {
    try {
      debug('enterd user subscription export csv service');
      const planName = ['BASIC', 'PREMIUM', 'PREMIUM+'];

      // const userSelect = reqParam.ExportSubscriptionItem;
      let UserSummaryDet, fectdMonthYr;
      UserSummaryDet = await getConnection().getRepository('UserSummaryView').find();
      // req.query.startMonth = '03';
      // req.query.startMonth = '03';
      // req.query.startYear = '2022';
      // req.query.endMonth = '05';
      // req.query.endYear = '2022';
      debug('before UserSummaryDet:', UserSummaryDet);
        // get year and month
        let monthVal =  reqParam.startMonth;
        let yearVal =  reqParam.startYear;
        let endMnt =  reqParam.endMonth;
        let endYr =  reqParam.endYear;
        // debug(typeof reqParam.startMonth);

        // START get cumulative count of user based on membershiptype
         // for monthly user
       const stringfiedArr =  JSON.stringify(UserSummaryDet);
       debug('stringfied UserSummaryDet:', stringfiedArr);
       const parsedArr = JSON.parse(stringfiedArr);
       debug('parased UserSummaryDet-1:', parsedArr[1].id);

       var firstItem = parsedArr[0];
      var lastItem = parsedArr[parsedArr.length-1];
      const firstYr = firstItem.year;
      const lastYr = lastItem.year;
       
      let allMonthYr = [];
      for (var n = 0; n < parsedArr.length; n++) {  
        allMonthYr.push(
          parsedArr[n].JoinedMonth,
        );
      }
      debug('allMonthYr:', allMonthYr);

       
       // START get cumulative count of user based on membershiptype
       const basicPlan = ['BASIC'];
       const basicPlanWthoutArr = 'BASIC';
       const BasicList = new Set(basicPlan);
       debug('BasicList:', BasicList);
       const newArrayBasic= parsedArr.filter(item=>BasicList.has(item.MembershipType)===true);
       debug('hai:',newArrayBasic);
 
       debug('newArrayBasic:', newArrayBasic);
       let basicArr = [];
       let totalBasicUser = 0 ;
       for (var yr = firstYr; yr <= lastYr; yr++) {
         for (var mn = 1; mn <= 12; mn++) {
           const mnFrmt = await this.adminService.addZero(mn)
           const yrFrmt = yr+'_'+mnFrmt;
           debug('yrFrmt:', yrFrmt);
           let getdata = newArrayBasic.filter(member => member.JoinedMonth == yrFrmt)
           debug('getdata:', getdata);
           if(getdata.length>0){
             totalBasicUser = totalBasicUser + parseInt(getdata[0].totalUsers);
             basicArr.push({
               // id: getdata[k].id,
               MembershipType: getdata[0].MembershipType,
               JoinedMonth: getdata[0].JoinedMonth,
               totalUsers: totalBasicUser,
               month: getdata[0].month,
               year: getdata[0].year,
               monthname: getdata[0].monthname
             });
           } else {
             const getMnthName = await this.adminService.monthName(mn)
             basicArr.push({
               // id: getdata[k].id,
               MembershipType: basicPlanWthoutArr,
               JoinedMonth: yrFrmt,
               totalUsers: totalBasicUser,
               month: mnFrmt,
               year: yr,
               monthname: getMnthName
             });
           }
         }
       }
       debug('basicArr:', basicArr);
       // return basicArr;
 
       const premiumPlan = ['PREMIUM'];
       const premiumPlannWthoutArr = 'PREMIUM';
       const premiumList = new Set(premiumPlan);
       debug('premiumList:', premiumList);
       const newArrayPremium= parsedArr.filter(item=>premiumList.has(item.MembershipType)===true);
       debug('newArrayPremium:', newArrayPremium);
       let premiumArr = [];
       let totalPremiumUser = 0 ;
       for (var yr = firstYr; yr <= lastYr; yr++) {
         for (var mn = 1; mn <= 12; mn++) {
           const mnFrmt = await this.adminService.addZero(mn)
           const yrFrmt = yr+'_'+mnFrmt;
           debug('yrFrmt:', yrFrmt);
           let getdata = newArrayPremium.filter(member => member.JoinedMonth == yrFrmt)
           debug('getdata:', getdata);
           if(getdata.length>0){
             totalPremiumUser = totalPremiumUser + parseInt(getdata[0].totalUsers);
             premiumArr.push({
               // id: getdata[k].id,
               MembershipType: getdata[0].MembershipType,
               JoinedMonth: getdata[0].JoinedMonth,
               totalUsers: totalPremiumUser,
               month: getdata[0].month,
               year: getdata[0].year,
               monthname: getdata[0].monthname
             });
           } else {
             const getMnthName = await this.adminService.monthName(mn)
             premiumArr.push({
               // id: getdata[k].id,
               MembershipType: premiumPlannWthoutArr,
               JoinedMonth: yrFrmt,
               totalUsers: totalPremiumUser,
               month: mnFrmt,
               year: yr,
               monthname: getMnthName
             });
           }
         }
       }
       debug('premiumArr:', premiumArr);
 
       const premiumPlusPlan = ['PREMIUM+'];
       const premiumPlusPlanWthoutArr = 'PREMIUM+';
       const premiumPlusList = new Set(premiumPlusPlan);
       debug('premiumPlusList:', premiumPlusList);
       const newArrayPremiumPlus= parsedArr.filter(item=>premiumPlusList.has(item.MembershipType)===true);
       debug('newArrayPremiumPlus:', newArrayPremiumPlus);
       let premiumPlusArr = [];
       let totalPremiumPlusUser = 0 ;
       for (var yr = firstYr; yr <= lastYr; yr++) {
         for (var mn = 1; mn <= 12; mn++) {
           const mnFrmt = await this.adminService.addZero(mn)
           const yrFrmt = yr+'_'+mnFrmt;
           debug('yrFrmt:', yrFrmt);
           let getdata = newArrayPremiumPlus.filter(member => member.JoinedMonth == yrFrmt)
           debug('getdata:', getdata);
           if(getdata.length>0){
             totalPremiumPlusUser = totalPremiumPlusUser + parseInt(getdata[0].totalUsers);
             premiumPlusArr.push({
               // id: getdata[k].id,
               MembershipType: getdata[0].MembershipType,
               JoinedMonth: getdata[0].JoinedMonth,
               totalUsers: totalPremiumPlusUser,
               month: getdata[0].month,
               year: getdata[0].year,
               monthname: getdata[0].monthname
             });
           } else {
             const getMnthName = await this.adminService.monthName(mn)
             premiumPlusArr.push({
               // id: getdata[k].id,
               MembershipType: premiumPlusPlanWthoutArr,
               JoinedMonth: yrFrmt,
               totalUsers: totalPremiumPlusUser,
               month: mnFrmt,
               year: yr,
               monthname: getMnthName
             });
           }
         }
       }
       debug('premiumPlusArr:', premiumPlusArr);
       let userSummary = [
         ...basicArr,
         ...premiumArr,
         ...premiumPlusArr
       ]
      // userSummary.sort((a, b) => a.id - b.id);
        // END

      if(reqParam.startMonth !== null && reqParam.startMonth !== undefined  && reqParam.startYear !== null && reqParam.startYear !== undefined && reqParam.endMonth !== null && reqParam.endMonth !== undefined && reqParam.endYear !== null && reqParam.endYear !== undefined)
      {
        // get all month and yr and add it in array for monthly subscription info
        fectdMonthYr = await this.adminService.getMonthYr(monthVal,yearVal,endMnt,endYr);
        debug('fectdYrs:', fectdMonthYr);
        
        // get all yr and add it in array for yearly subscription info
        let y;
        let fecthedYr = [];
        for(y=yearVal; y<=endYr; y++) 
        {
          let a = ''+y;
          fecthedYr.push(a);
        }
        debug('len:', fecthedYr.length);
        // fecthedYr.map((element) => element.replace(/[^a-zA-Z ]/, ' '));
        debug('fecthedYr:', fecthedYr);
        
        // for monthly user
        if (fectdMonthYr.length > 0)
        {
          const monthYrList = new Set(fectdMonthYr);
          debug('monthYrList:', monthYrList);
          userSummary= userSummary.filter(item=>monthYrList.has(item.JoinedMonth)===true);
          debug('user summary-1:',userSummary);
        }

        debug('fetched for month:', userSummary);
      }
     
      var subscriotionArr = [];
      var monthSublen = userSummary.length;
      let plans, yrName;
      debug('fectdMonthYr:', fectdMonthYr);
      debug('fectdMonthYr.len:', fectdMonthYr.length);
      for (var k = 0; k < fectdMonthYr.length; k++) {  //[2022_06, 2022_07, 2022_08]
        debug('for-1:', k);
        plans = ['BASIC', 'PREMIUM', 'PREMIUM+'];
        // for (var p = 0; p < plans.length; p++) { 
          let basicUser = null;
          let premiumUser = null;
          let premiumPlusUser = null;
          let basicFilled = false;
          let premiumFilled = false;
          let premiumPlusFilled = false;
          let loopTime = 0;
          for (var i = 0; i < monthSublen; i++) {
            debug('for-2:', i);
            loopTime++;
            //to fetch year details
            debug('AggregateMonthlyUsers[i].JoinedMonth:', userSummary[i].JoinedMonth);
            debug('fectdMonthYr[k]:', fectdMonthYr[k]);
            if(userSummary[i].JoinedMonth === fectdMonthYr[k])
            {
              debug('if-1:');
              
              debug('AggregateMonthlyUsers[i].MembershipType:', userSummary[i].MembershipType);
                // for (var y = 0; y < yrName.length; y++) { // 2 times loop[month, year]
                  debug('AggregateMonthlyUsers[i].plan:', userSummary[i].plan);
                  debug('basicFilled:', basicFilled);
                  if(userSummary[i].MembershipType === 'BASIC' && !basicFilled) {
                    debug('enter month:');
                    basicFilled = true;
                    basicUser =  userSummary[i].totalUsers;
                  }
                  if(userSummary[i].MembershipType === 'PREMIUM' && !premiumFilled) {
                    debug('enter month:');
                    premiumFilled = true;
                    premiumUser =  userSummary[i].totalUsers;
                  }
                  if(userSummary[i].MembershipType === 'PREMIUM+' && !premiumPlusFilled) {
                    debug('enter premium+:');
                    premiumPlusFilled = true;
                    premiumPlusUser =  userSummary[i].totalUsers;
                  }
                 
                // }
            }
            if(loopTime === monthSublen || (basicFilled && premiumFilled && premiumPlusFilled))
            {
              subscriotionArr.push({
                // Date: yrname,
                Year_Month: fectdMonthYr[k],
                Basic_User: basicUser,
                Premium_User: premiumUser,
                Premium_Plus_User: premiumPlusUser,
              });
              break;
            }
          }
          
        // }
      }
      const json2csvParser = new Json2csvParser({ header: true});
      debug('subscriotionArr:', subscriotionArr);
      const csv = json2csvParser.parse(subscriotionArr);

      const dt=new Date();
      const csvName = 'usersummaryreport'+ '_' +dt.getTime()+ '.csv';
      const absPath = path.join(__dirname, csvName);
      const parent_this = this;
      const s3buktName = this.config.envConfig.AWS_EXPORT_USER_BUCKET_NAME;
      const fileNme = "usersummaryreport";
      fs.writeFile(absPath, csv, async function(error) {
        if (error) throw error;
        console.log("Write usersummaryreport.csv to folder successfully!");
        // return await uploadCSV(absPath, resp);
        return await parent_this.uploadCSV(s3buktName, fileNme, absPath, resp);

      });
    } catch (err) {
      throw err;
    }
  }

  // reinstate user within 30 days
  async reinstateUser(id: string) {
    try {
      const userExist = await UserEntity.findOne({ id });
      if (!userExist) {
        debug('Record doesnot exist');
        // return 'Record doesnot exist';
        throw new HttpException({
          name : 'recordNotFound',
          message :  'Record does not exist'
        },HttpStatus.BAD_REQUEST);
      }


      // chk days within 30 days
      const then = new Date(userExist.deleted_at);
      const now = new Date();
      const msBetweenDates = Math.abs(then.getTime() - now.getTime());

      // 👇️ convert ms to days                 hour   min  sec   ms
      const daysBetweenDates = msBetweenDates / (24 * 60 * 60 * 1000);
      debug('dys:', daysBetweenDates);
      if (daysBetweenDates < 30) {
        debug('date is within 30 days');
       
        // set statu = 0 w.r.t to id
        try {
          // START get keycloak token
          const keycloakToken = (await this.getKeycloak()).token;
          // END

          const headers = {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${keycloakToken}`,
          };

          const isExist = await this.isUserExistKeycloak(
            userExist.user_type,
            userExist.id,
          );

          // START set status in keycloak
          const setststaus = {
            enabled: 1,
          };

          // set status in keycloak w.r.t keyclock id
          const addRes = await this.http
            .put(
              `${this.config.envConfig.KEYCLOAK_BASE_URL}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_AGENT}/users/${userExist.id}`,
              setststaus,
              {
                headers,
              },
            )
            .toPromise();
          if (addRes.status !== 204) {
            return {
              resultcode: 400,
              errMessage: 'Error occured while updating user status in keycloak',
            };
          }
          debug('status updated successfully in keycloak');
          // END keycloak implement

          await UserEntity.update(
            { id },
            {
              status: 1,
              deleted: 0,
              updated_at: new Date(),
              updated_user_id: id,
            },
          );
          return {message: "User has been reinstated successfully"};
        } catch (err) {
          if(err && err.response && err.response.status>=400 && err.response.status<500) {
            throw new HttpException(err.response.data,err.response.status);
          } else {
            console.error('User reinstate err : ', err);
            throw err;
          }
        }
      } else {
        debug('date is NOT within 30 days');
        throw new HttpException({
          name : '30_DAYS_AGO',
          message :  'User deleted his/her account 30 days ago'
        },HttpStatus.BAD_REQUEST);
      }         
    } catch (err) {
      console.error('user reinstate Err:', err);
      throw err;
    }
  }

  // sendPushNotification
  async sendPushNotification(reqParam: SendPushNotifyDto) {
    try {
      await this.http
          .post(
            `${this.config.envConfig.NOTIFY_SERVICE_URL}/notify/init-notification`,
            {
              device_id: reqParam.device_id,
              user_id: reqParam.user_id,
              membership_type: reqParam.membership_type,
            },
          )
          .subscribe((res) => {
            // debug(res);
          });
          debug('added into send_push_notification table:');
          return { message: 'User device Id added/updated successfully' };
    } catch (err) {
      console.error('sendPushNotification Err:', err);
      throw err;
    }
  }
  //

} // end of main export class



async function getRandomNumber(digit) {
  const val = await Math.random().toFixed(digit).split('.')[1];
  return val;
}

function dbConnect() {
  const userRepo = getConnection().getRepository(UserEntity);
  const bankRepo = getConnection().getRepository(UserBankEntity);
  return { userRepo, bankRepo };
}

// get age
function getAge(dateString) {
  // const birth = new Date(dateString); // input should be in getAge('1981-09-20')
  const birth = new Date(dateString.split('-').reverse().join('-')); // input should be in getAge('20-09-1981')
  const now = new Date();
  const beforeBirth =
    (() => {
      birth.setDate(now.getDate());
      birth.setMonth(now.getMonth());
      return birth.getTime();
    })() < birth.getTime()
      ? 0
      : 1;
  return now.getFullYear() - birth.getFullYear() - beforeBirth;
}

// age should be greater 21
function calculateAge(dateString) {
  //convert dd-mm-yyyy to mm-dd-yyyy
  const [day, month, year] = dateString.split("-");
  const reverseMonth = `${month}-${day}-${year}`
  console.log(reverseMonth)
   // convert user input value into date object
	 var birthDate = new Date(reverseMonth);
	  console.log(" birthDate"+ birthDate);
	 // get difference from current date;
	 var difference=Date.now() - birthDate.getTime(); 
	 	 
	 var  ageDate = new Date(difference); 
	 var calculatedAge=   Math.abs(ageDate.getUTCFullYear() - 1970);
   if( isNaN(calculatedAge)){
    throw new HttpException({
      name : 'INVALID_DOB',
      message : 'Inavlid DOB'
    },HttpStatus.BAD_REQUEST);
  } else {
    return calculatedAge;
  }
}

function convertYMD(val) {
  const date = val;
  // const newdate = date.split('-').reverse().join('-');
  const newdate = date;
  return newdate;
}