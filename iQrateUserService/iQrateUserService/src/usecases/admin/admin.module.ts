import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { AdminController } from './admin.controller';
import { AdminService } from './admin.service';
import { ConfigModule } from '../../config/config.module';
import { UserService } from '../user/user.service';
import { SubscriptionService } from '../subscription/subscription.service';

import { TwoFactorAuthenticationService } from '../user/twoFactorAuthentication.service';
import { MemberShipAclService } from 'src/middlewares/acl_module/membership_acl_service';
import { ScheduleModule } from '@nestjs/schedule'; // to run cron job

// import { keycloakModule } from '../login/login.keycloak.module';

// keycloakModule

@Module({
  imports: [ConfigModule, HttpModule, ScheduleModule.forRoot()],
  controllers: [AdminController],
  providers: [AdminService, UserService, TwoFactorAuthenticationService, SubscriptionService, MemberShipAclService],
  // exports: [UserService],
})
export class AdminModule { }
