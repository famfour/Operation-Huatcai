import {
  BadRequestException,
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
  Req,
  Res,
} from '@nestjs/common';
import Debug from 'debug';
import DeviceDetector = require('device-detector-js');
import * as bcrypt from 'bcrypt';
import { getConnection, In } from 'typeorm';
import { HttpService } from '@nestjs/axios';
import { ConfigModule } from 'src/config/config.module';
import { UserBankEntity } from '../../entities/userbank';
import { ConfigService } from '../../config/config.service';
import { UserEntity } from '../../entities/users';
import { CampaignEntity } from '../../entities/campaignentity';
import { PushNotifiyEntity } from '../../entities/push_notify_entity';
import { ContactUs } from '../../entities/contact_us';
import { AdminAcl } from '../../entities/admin_acl';
import { JWT } from '../utils/jwt';
import RedisClient from '../utils/redis';
import { AdminDto, AdminEmailDto, CampiagnDto, DefaultChgPswrdDto, DefaultPassword, DefaultSetAdminRole, DefaultUpdateAdminDto, DefaultUpdateMyProfileDto, DefaultUserStatusDto, ExportSubscriptionItem, PushNotifyDto, UserDto } from '../user/dto/user.dto';
import { LoginDto } from '../login/login.dto';
import { AdminEntity } from '../../entities/admin';
const fs = require("fs");
import * as AWS from 'aws-sdk';
import { Cron } from '@nestjs/schedule';
import { AssignedCobrokeEntity } from 'src/entities/cobroke_id_assigned';

const debug = Debug('admin-service:AdminService');

@Injectable()
export class AdminService {
  constructor(
    private config: ConfigService,
    private readonly http: HttpService,
  ) {}

    // run cron job at every seconds to inactive campus status if date is overdue
    @Cron('* * * * * *')
    CronUpdateCampStatusinactive() {
      this.updateCampStatus();
      debug('campaign status updated by cron!');
    }

  async getAggregateUsers(req) {
    try {
      let AggregateMonthlyUsers;
      AggregateMonthlyUsers = await getConnection().getRepository('AggregateUsersView').find();

      // START search filter
       // req.query.startMonth = 4;
      // req.query.startYear = 2022;
      // req.query.endMonth = 12;
      // req.query.endYear = 2022;

      // get year and month
      let monthVal = Number(req.query.startMonth);
      let yearVal = Number(req.query.startYear);
      let endMnt = Number(req.query.endMonth);
      let endYr = Number(req.query.endYear);
      debug('hai');
      debug('yearVal:', yearVal);
      debug('monthVal:', monthVal);
      debug('endMnt:', endMnt);
      debug('endYr:', endYr);

      debug('before AggregateMonthlyUsers:', AggregateMonthlyUsers);
      // if search parameter is not empty
      if(req.query.startMonth !== '' && req.query.startMonth !== undefined  && req.query.startYear !== '' && req.query.startYear !== undefined && req.query.endMonth !== '' && req.query.endMonth !== undefined && req.query.endYear !== '' && req.query.endYear !== undefined)
      {
        debug('hai'); 
        // get all month and yr and add it in array for monthly subscription info
        const fectdMonthYr = await this.getMonthYr(monthVal,yearVal,endMnt,endYr);
        debug('fectdYrs:', fectdMonthYr);
        
        // get all yr and add it in array for yearly subscription info
        let y;
        let fecthedYr = [];
        for(y=yearVal; y<=endYr; y++) 
        {
          let a = ''+y;
          fecthedYr.push(a);
        }
        debug('len:', fecthedYr.length);
        // fecthedYr.map((element) => element.replace(/[^a-zA-Z ]/, ' '));
        debug('fecthedYr:', fecthedYr);


        // for monthly report
        const stringfiedArr =  JSON.stringify(AggregateMonthlyUsers);
        debug('stringfied AggregateMonthlyUsers:', stringfiedArr);
        const parsedArr = JSON.parse(stringfiedArr);
        debug('parased AggregateMonthlyUsers:', parsedArr);
        if (fectdMonthYr.length > 0)
        {
          const monthYrList = new Set(fectdMonthYr);
          debug('monthYrList:', monthYrList);
          AggregateMonthlyUsers= parsedArr.filter(item=>monthYrList.has(item.JoinedMonth)===true);
          debug('hai:',AggregateMonthlyUsers);
        }
      }
      // END
      return {
        AggregateMonthlyUsers
      };
    } catch(err) {
      console.error('getAggregateUsers:',err);
      throw new HttpException({message:"Oops! Enountered temporary error. Please try again later!",status:400,name:'AggregateUsers_Catch_Error1'},HttpStatus.BAD_REQUEST)
    }
  }

  // get revenue report for subscription
  async getRevuneSubscription(req) {
    try {
      let MonthlySubRevenueReprt;
      MonthlySubRevenueReprt = await getConnection().getRepository('UsersSubscriptionRevenueView').find();

      // START search filter
       // req.query.startMonth = 4;
      // req.query.startYear = 2022;
      // req.query.endMonth = 12;
      // req.query.endYear = 2022;

      // get year and month
      let monthVal = Number(req.query.startMonth);
      let yearVal = Number(req.query.startYear);
      let endMnt = Number(req.query.endMonth);
      let endYr = Number(req.query.endYear);
      debug('hai');
      debug('yearVal:', yearVal);
      debug('monthVal:', monthVal);
      debug('endMnt:', endMnt);
      debug('endYr:', endYr);

      debug('before MonthlySubRevenueReprt:', MonthlySubRevenueReprt);
      // if search parameter is not empty
      if(req.query.startMonth !== '' && req.query.startMonth !== undefined  && req.query.startYear !== '' && req.query.startYear !== undefined && req.query.endMonth !== '' && req.query.endMonth !== undefined && req.query.endYear !== '' && req.query.endYear !== undefined)
      {
        debug('hai'); 
        // get all month and yr and add it in array for monthly subscription info
        const fectdMonthYr = await this.getMonthYr(monthVal,yearVal,endMnt,endYr);
        debug('fectdYrs:', fectdMonthYr);
        
        // get all yr and add it in array for yearly subscription info
        let y;
        let fecthedYr = [];
        for(y=yearVal; y<=endYr; y++) 
        {
          let a = ''+y;
          fecthedYr.push(a);
        }
        debug('len:', fecthedYr.length);
        // fecthedYr.map((element) => element.replace(/[^a-zA-Z ]/, ' '));
        debug('fecthedYr:', fecthedYr);


        // for monthly report
        const stringfiedArr =  JSON.stringify(MonthlySubRevenueReprt);
        debug('stringfied MonthlySubRevenueReprt:', stringfiedArr);
        const parsedArr = JSON.parse(stringfiedArr);
        debug('parased MonthlySubRevenueReprt:', parsedArr);
        if (fectdMonthYr.length > 0)
        {
          const monthYrList = new Set(fectdMonthYr);
          debug('monthYrList:', monthYrList);
          MonthlySubRevenueReprt= parsedArr.filter(item=>monthYrList.has(item.JoinedMonth)===true);
          debug('hai:',MonthlySubRevenueReprt);
        }
      }
      // END
      return {
        MonthlySubRevenueReprt
      };
    } catch(err) {
      console.error('Err in monthly revenue report:',err);
      throw new HttpException({message:"Oops! Enountered temporary error. Please try again later!",status:400,name:'AggregateUsers_Catch_Error1'},HttpStatus.BAD_REQUEST)
    }
  }

  // START
  // export user subscription info into csv
  public async getUserSummary(req) {
    try {
      debug('enterd user summary service');
      
      // const userSelect = reqParam.ExportSubscriptionItem;
      let UserSummaryDet, fectdMonthYr;
      UserSummaryDet = await getConnection().getRepository('UserSummaryView').find();
      // req.query.startMonth = '03';
      // req.query.startYear = '2022';
      // req.query.endMonth = '05';
      // req.query.endYear = '2022';
      debug('before UserSummaryDet:', UserSummaryDet);

       // get year and month
       let monthVal = Number(req.query.startMonth);
       let yearVal = Number(req.query.startYear);
       let endMnt = Number(req.query.endMonth);
       let endYr = Number(req.query.endYear);

       // for monthly user
       const stringfiedArr =  JSON.stringify(UserSummaryDet);
       debug('stringfied UserSummaryDet:', stringfiedArr);
       const parsedArr = JSON.parse(stringfiedArr);
       debug('parased UserSummaryDet:', parsedArr);
      
      var firstItem = parsedArr[0];
      var lastItem = parsedArr[parsedArr.length-1];
      const firstYr = firstItem.year;
      const lastYr = lastItem.year;

       // START get cumulative count of user based on membershiptype
      const basicPlan = ['BASIC'];
      const basicPlanWthoutArr = 'BASIC';
      const BasicList = new Set(basicPlan);
      debug('BasicList:', BasicList);
      const newArrayBasic= parsedArr.filter(item=>BasicList.has(item.MembershipType)===true);
      debug('hai:',newArrayBasic);

      debug('newArrayBasic:', newArrayBasic);
      let basicArr = [];
      let totalBasicUser = 0 ;
      for (var yr = firstYr; yr <= lastYr; yr++) {
        for (var mn = 1; mn <= 12; mn++) {
          const mnFrmt = await this.addZero(mn)
          const yrFrmt = yr+'_'+mnFrmt;
          debug('yrFrmt:', yrFrmt);
          let getdata = newArrayBasic.filter(member => member.JoinedMonth == yrFrmt)
          debug('getdata:', getdata);
          if(getdata.length>0){
            totalBasicUser = totalBasicUser + parseInt(getdata[0].totalUsers);
            basicArr.push({
              // id: getdata[k].id,
              MembershipType: getdata[0].MembershipType,
              JoinedMonth: getdata[0].JoinedMonth,
              totalUsers: totalBasicUser,
              month: getdata[0].month,
              year: getdata[0].year,
              monthname: getdata[0].monthname
            });
          } else {
            const getMnthName = await this.monthName(mn)
            basicArr.push({
              // id: getdata[k].id,
              MembershipType: basicPlanWthoutArr,
              JoinedMonth: yrFrmt,
              totalUsers: totalBasicUser,
              month: mnFrmt,
              year: yr,
              monthname: getMnthName
            });
          }
        }
      }
      debug('basicArr:', basicArr);
      // return basicArr;

      const premiumPlan = ['PREMIUM'];
      const premiumPlannWthoutArr = 'PREMIUM';
      const premiumList = new Set(premiumPlan);
      debug('premiumList:', premiumList);
      const newArrayPremium= parsedArr.filter(item=>premiumList.has(item.MembershipType)===true);
      debug('newArrayPremium:', newArrayPremium);
      let premiumArr = [];
      let totalPremiumUser = 0 ;
      for (var yr = firstYr; yr <= lastYr; yr++) {
        for (var mn = 1; mn <= 12; mn++) {
          const mnFrmt = await this.addZero(mn)
          const yrFrmt = yr+'_'+mnFrmt;
          debug('yrFrmt:', yrFrmt);
          let getdata = newArrayPremium.filter(member => member.JoinedMonth == yrFrmt)
          debug('getdata:', getdata);
          if(getdata.length>0){
            totalPremiumUser = totalPremiumUser + parseInt(getdata[0].totalUsers);
            premiumArr.push({
              // id: getdata[k].id,
              MembershipType: getdata[0].MembershipType,
              JoinedMonth: getdata[0].JoinedMonth,
              totalUsers: totalPremiumUser,
              month: getdata[0].month,
              year: getdata[0].year,
              monthname: getdata[0].monthname
            });
          } else {
            const getMnthName = await this.monthName(mn)
            premiumArr.push({
              // id: getdata[k].id,
              MembershipType: premiumPlannWthoutArr,
              JoinedMonth: yrFrmt,
              totalUsers: totalPremiumUser,
              month: mnFrmt,
              year: yr,
              monthname: getMnthName
            });
          }
        }
      }
      debug('premiumArr:', premiumArr);

      const premiumPlusPlan = ['PREMIUM+'];
      const premiumPlusPlanWthoutArr = 'PREMIUM+';
      const premiumPlusList = new Set(premiumPlusPlan);
      debug('premiumPlusList:', premiumPlusList);
      const newArrayPremiumPlus= parsedArr.filter(item=>premiumPlusList.has(item.MembershipType)===true);
      debug('newArrayPremiumPlus:', newArrayPremiumPlus);
      let premiumPlusArr = [];
      let totalPremiumPlusUser = 0 ;
      for (var yr = firstYr; yr <= lastYr; yr++) {
        for (var mn = 1; mn <= 12; mn++) {
          const mnFrmt = await this.addZero(mn)
          const yrFrmt = yr+'_'+mnFrmt;
          debug('yrFrmt:', yrFrmt);
          let getdata = newArrayPremiumPlus.filter(member => member.JoinedMonth == yrFrmt)
          debug('getdata:', getdata);
          if(getdata.length>0){
            totalPremiumPlusUser = totalPremiumPlusUser + parseInt(getdata[0].totalUsers);
            premiumPlusArr.push({
              // id: getdata[k].id,
              MembershipType: getdata[0].MembershipType,
              JoinedMonth: getdata[0].JoinedMonth,
              totalUsers: totalPremiumPlusUser,
              month: getdata[0].month,
              year: getdata[0].year,
              monthname: getdata[0].monthname
            });
          } else {
            const getMnthName = await this.monthName(mn)
            premiumPlusArr.push({
              // id: getdata[k].id,
              MembershipType: premiumPlusPlanWthoutArr,
              JoinedMonth: yrFrmt,
              totalUsers: totalPremiumPlusUser,
              month: mnFrmt,
              year: yr,
              monthname: getMnthName
            });
          }
        }
      }
      debug('premiumPlusArr:', premiumPlusArr);
      let userSummary = [
        ...basicArr,
        ...premiumArr,
        ...premiumPlusArr
      ]
      // userSummary.sort((a, b) => a.id - b.id);
      // END

      // filter
      if(req.query.startMonth !== '' && req.query.startMonth !== undefined  && req.query.startYear !== '' && req.query.startYear !== undefined && req.query.endMonth !== '' && req.query.endMonth !== undefined && req.query.endYear !== '' && req.query.endYear !== undefined)
      {
        debug('hai'); 
        // get all month and yr and add it in array for monthly subscription info
        const fectdMonthYr = await this.getMonthYr(monthVal,yearVal,endMnt,endYr);
        debug('fectdYrs:', fectdMonthYr);
        
        // get all yr and add it in array for yearly subscription info
        let y;
        let fecthedYr = [];
        for(y=yearVal; y<=endYr; y++) 
        {
          let a = ''+y;
          fecthedYr.push(a);
        }
        debug('len:', fecthedYr.length);
        // fecthedYr.map((element) => element.replace(/[^a-zA-Z ]/, ' '));
        debug('fecthedYr:', fecthedYr);


        // for monthly report
        if (fectdMonthYr.length > 0)
        {
          const monthYrList = new Set(fectdMonthYr);
          debug('monthYrList:', monthYrList);
          userSummary= userSummary.filter(item=>monthYrList.has(item.JoinedMonth)===true);
          debug('after filter:',userSummary);
        }
      }
      //end
      return {userSummary}
    } catch (err) {
      throw err;
    }
  }
  // END

  public async addZero(n){
    return n > 9 ? "" + n: "0" + n;
  }

  public async monthName(mon) {
    return ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'][mon - 1];
 }
 


  public async getMonthYr(monthVal,yearVal,endMnt,endYr)
  {
    const filtereddDate = [];
    let doLoop = true;
    while(doLoop){
      debug('doLoop');
      // debug(typeof monthVal);
      // debug(typeof endMnt);
      // debug(typeof yearVal);
      // debug(typeof endYr);
      if(monthVal<=endMnt || yearVal<endYr){
        debug('firstif');
        if(monthVal>12 && yearVal<=endYr)
        {
          debug('second if');
          monthVal = 1;
          yearVal++; 
        }
        debug('added into array')
        const crctMnthFrmt = await this.addZero(monthVal)
        const addyr = yearVal+'_'+crctMnthFrmt;
        filtereddDate.push(addyr);
        monthVal++;
        debug('nxt month:', monthVal);
        debug('endMnt in lopp:', endMnt);
        debug('yearVal in lopp:', yearVal);
        debug('endYr in lopp:', endYr);

        if(monthVal>endMnt && yearVal>=endYr){
          debug('third if');
          doLoop = false;
        }
      } else {
        throw new HttpException({
          name : 'INVALID_DATE',
          status:400,
          message : 'Invalid date range'
        },HttpStatus.BAD_REQUEST);
      }
    }
    console.log('hai');
    console.log('filtereddDate:', filtereddDate);
    return filtereddDate;
  }

  public async register(reqBody: AdminDto, emailVerify: number) {
    try {
      debug(emailVerify);
      debug('Entered user service');
      // chk user with email already exist
      const emailRecordExist = await AdminEntity.findOne({
        email: reqBody.email,
        // user_type: this.config.envConfig.USERTYPE, // reqBody.user_type,
      });
      if (emailRecordExist) {
        debug('entered email exist');

        throw new HttpException(
          {
            name: 'emailRecordExist',
            message: 'User with email already exist',
          },
          HttpStatus.BAD_REQUEST,
        );
      }

      // chk valid mail server
      const legit = require('legit');
      try {
          const res = await legit(reqBody.email);
          // return res;
          if(!res.isValid){
            throw new HttpException({
              name : 'INVALID_EMAIL',
              message : 'You have entered an invalid email address. Please check your email address'
            },HttpStatus.BAD_REQUEST);
          }
      } catch (err) {
        console.error('err:', err.Error);
        
          throw new HttpException({
            name : 'INVALID_EMAIL',
            message : 'You have entered an invalid email address. Please check your email address'
          },HttpStatus.BAD_REQUEST);
      }

      
      // chk user with mobile already exist
      const mobileRecordExist = await AdminEntity.findOne({
        mobile: reqBody.mobile,
        // user_type: this.config.envConfig.USERTYPE, // reqBody.user_type,
      });
      if (mobileRecordExist) {
        debug('entered mobile exist');
        throw new HttpException(
          {
            name: 'mobileRecordExist',
            message: 'User with mobile number already exist',
          },
          HttpStatus.BAD_REQUEST,
        );
      }

      // to chk valid mobile or not
      const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();
      var test = phoneUtil.parseAndKeepRawInput(reqBody.mobile, reqBody.country_code_label);
      const validMobile = phoneUtil.isValidNumber(test);
      if (!validMobile) {
        debug('entered mobile exist');
        throw new HttpException({
          name : 'INVALID_MOBILE',
          message : 'Invalid mobile number'
        },HttpStatus.BAD_REQUEST);
      }

      // START get device location
      const DEVICE_LOCATION_API_URL =
        this.config.envConfig.DEVICE_LOCATION_API_URL ||
        'http://ip-api.com/json/';
      const deviceLocationRes = await this.http
        .get(DEVICE_LOCATION_API_URL)
        .toPromise();
      debug('deviceLocation : ', deviceLocationRes.data);
      const deviceLocation = deviceLocationRes.data.regionName;

      const keycloakToken = (await this.getKeycloak()).token;
      debug('keycloakToken : ', keycloakToken);

      // START save user in keycloak
      const headers = {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${keycloakToken}`,
      };

      let datas = {};

      datas = {
        username: reqBody.email,
        enabled: true,
        totp: false,
        emailVerified: false,
        firstName: reqBody.full_name,
        // lastName: 'Strange',
        email: reqBody.email,
        credentials: [
          {
            type: 'password',
            value: reqBody.password,
            temporary: false,
          },
        ],
      };

      // datas['attributes'] = {
      //   membershiptype: this.config.envConfig.MEMBERSHIPTYPE || 'basic',
      // };

      let addRes: any = {};

      try {
        addRes = await this.http
          .post(
            `${this.config.envConfig.KEYCLOAK_BASE_URL}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_ADMIN}/users`,
            datas,
            {
              headers,
            },
          )
          .toPromise();
      } catch (err) {
        if (
          err &&
          err.response &&
          err.response.status >= 400 &&
          err.response.status < 500
        ) {
          throw new HttpException(err.response.data, err.response.status);
        } else {
          console.error('login err : ', err);
          throw err;
        }
      }

      debug('addRes : ', addRes.data);

      // To get keycloak id
      const IDLocation = addRes.headers.location;
      const segments = IDLocation.split('/');
      const keyLockID = segments[segments.length - 1];
      // END

      if (keyLockID === '') {
        throw new HttpException(
          {
            name: 'UnknownErrorFromKeycloak',
            message: 'Error occured while adding user',
          },
          HttpStatus.BAD_REQUEST,
        );
      }
      // END keycloak implement

      // hash the passowrd
      // const hashPass = await gethasnumber(reqBody.password, 12);

      const user = new AdminEntity();
      user.id = keyLockID;
      user.full_name = reqBody.full_name;
      user.email = reqBody.email;
      // user.password = hashPass; // hashPass;
      user.mobile = reqBody.mobile;
      user.country_code = reqBody.country_code;
      user.created_at = new Date();
      // user.device = deviceinfo;
      // user.location = await deviceLocation;
      user.is_email_verified = 0;
      user.is_mobile_verified = 0;
      user.status = 1;
      user.user_type = 'admin'; // reqBody.user_type;
      // user.membership_type = this.config.envConfig.MEMBERSHIPTYPE;
      const res = await AdminEntity.save(user);

      // set admin role
      const roleId = this.config.envConfig.KEYCLOCK_ROLE_ID_ADMIN;
      const roleName = this.config.envConfig.KEYCLOCK_ROLE_NAME_ADMIN;

      debug('role id:',roleId);
      debug('roleName:',roleName);

      const req = [
        {
          id: roleId,
          name: roleName,
        },
      ];
      try {
      const result = await this.http
        .post(
          // 'http://54.251.187.218:8080/auth/admin/realms/master/users/c51b90c8-cb7f-4ad0-80e3-14b6b079e128/role-mappings/realm',
          `${(await this.getKeycloak()).ip}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_ADMIN}/users/${keyLockID}/role-mappings/realm`,
          req,
          {
            headers,
          },
        )
        .toPromise();
      } catch (err) {
        if(err && err.response && err.response.status>=400 && err.response.status<500) {
          throw new HttpException(err.response.data,err.response.status);
        } else {
          console.error('set role err : ', err);
          throw err;
        }
      }
      //
      const pswrd = reqBody.password;
      const templateFor = 'created';
      const { id, full_name, email, mobile, status } = res;
    
      const showData = {
        full_name,
        email,
        pswrd,
        templateFor,
      };
      debug('showData : ', showData);

      return { message: "Admin has been added successfully", showData }
    } catch (err) {
      console.error('Register Err:', err);
      throw err;
    }
  }

  //check valid admin
  async chkValidAdmin(id: string) 
  {
    const adminExist = await AdminEntity.findOne({ id });
      if (!adminExist) {
        const keycloakToken = (await this.getKeycloak()).token;

        const headers = {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${keycloakToken}`,
        };
      
        try {
          const addRes = await this.http
            .get(
                `${(await this.getKeycloak()).ip}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_ADMIN}/users/${id}`,
                {
                  headers,
                },
            )
            .toPromise();
          if(addRes.data.id){
            return { message: "Authorized admin"}
          } else {
            debug('addres:data:',addRes.data.id);
            throw new HttpException({
                name : 'NOT_FOUND',
                message : 'Admin does not exist'
              },HttpStatus.BAD_REQUEST);
          }
        } catch (err) {
          if(err && err.response && err.response.status>=400 && err.response.status<500) {
            throw new HttpException(err.response.data,err.response.status);
          } else {
            console.error('valid admin err : ', err);
            throw err;
          }
        }
      } else {
        debug('admin record exist');
        return { message: "Authorized admin"
        }
      }
    }

  // START login
  public async login(
    loginDto: LoginDto
  ): Promise<any> {

    const KEYCLOAK_BASE_URL = this.config.envConfig.KEYCLOAK_BASE_URL;
    const KEYCLOAK_REALM_ADMIN = this.config.envConfig.KEYCLOAK_REALM_ADMIN;

    debug("grant", this.config.envConfig.KEYCLOAK_GRANT);
    const keyCloakLoginRequestPayload = {
      // client_id: this.config.envConfig.KEYCLOCK_CLIENT_NAME_ADMIN,
      client_id: this.config.envConfig.KEYCLOCK_USERNAME,
      grant_type: this.config.envConfig.KEYCLOCK_GRANT_TYPE,
      username: loginDto.email,
      password: loginDto.password,
    };
    debug("keyCloakLoginRequestPayload : ", keyCloakLoginRequestPayload);

    const keyCloakLoginRequestFormBody = Object.keys(keyCloakLoginRequestPayload).map((key) => {
      return encodeURIComponent(key) + '=' + encodeURIComponent(keyCloakLoginRequestPayload[key]);
    }).join('&');

    debug("KEYCLOAK_BASE_URL :", KEYCLOAK_BASE_URL);

    //Not sure the purpose of this calling. Please explain the rational as per the logic so we can enable back and apply in the response accordingly
    // const device = await this.http.get('http://ip-api.com/json/').toPromise();
    // debug('device : ', device.data);

    const fullUrl = `${KEYCLOAK_BASE_URL}/realms/${KEYCLOAK_REALM_ADMIN}/protocol/openid-connect/token`;
    debug("keyCloakLoginRequestFormBody : ", keyCloakLoginRequestFormBody);
    debug("fullUrl : ", fullUrl);
    try {
    const tokenRes = await this.http.post(
          fullUrl, keyCloakLoginRequestFormBody, {
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).toPromise();

          // update refresh token
          const admin = await AdminEntity.findOne({
            where: {
              email: loginDto.email,
            }});
    
          if (admin) {
            // update refresh token in db
            let dbRefreshTokenArr = []
            dbRefreshTokenArr = admin.refresh_token;
            debug('beforepush:', dbRefreshTokenArr);
            dbRefreshTokenArr.unshift(tokenRes.data.refresh_token); //add new token at the beginnig of the array
            debug('afterpush:', dbRefreshTokenArr);
            debug('id:', admin.id);
            await AdminEntity.update(
              { id: admin.id },
              { refresh_token: dbRefreshTokenArr, updated_at: new Date(), updated_user_id: admin.id },
            );
          }
      return tokenRes.data;
    } catch (err) {
      if(err && err.response && err.response.status>=400 && err.response.status<500) {
        if(err.response.data.error_description === 'Account disabled') {
          throw new HttpException({
            message :  'Login Failed : Your account has been locked',
          },HttpStatus.BAD_REQUEST);
        } else { 
          throw new HttpException({
            message :  'Login Failed : Your email or password is incorrect',
          },HttpStatus.BAD_REQUEST);
        }
      } else {
        console.error('login err : ', err);
        throw err;
      }
    }
  }

  // END

   // START login
   public async adminlogout(bearerToken:string,refresh_token:string) {
    const KEYCLOAK_BASE_URL = this.config.envConfig.KEYCLOAK_BASE_URL;
    const KEYCLOAK_REALM_ADMIN = this.config.envConfig.KEYCLOAK_REALM_ADMIN;
    debug("logout KEYCLOAK_BASE_URL :", KEYCLOAK_BASE_URL);
    bearerToken
    const fullUrl = `${KEYCLOAK_BASE_URL}/realms/${KEYCLOAK_REALM_ADMIN}/protocol/openid-connect/logout`;
    const logoutRequestBody = `client_id=${this.config.envConfig.KEYCLOCK_USERNAME}&refresh_token=${refresh_token}`;
    try {
      const tokenRes = await this.http.post(
        fullUrl, logoutRequestBody, {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': bearerToken
          }
        }).toPromise();
      debug('adminlogout:',tokenRes);
      return tokenRes.data;
    } catch (err) {
      if(err && err.response && err.response.status>=400 && err.response.status<500) {
        throw new HttpException(err.response.data,err.response.status);
      } else {
        console.error('adminlogin err : ', err);
        throw err;
      }
    }

  }
  // END

  // get another service port i.e notifyService
  async getPort() {
    // const notifyPort = this.config.envConfig.NOTIFY_API_URL;
    const notifyPort = this.config.envConfig.NOTIFY_SERVICE_URL;
    return notifyPort;
  }

  async sendEmailCode(email, verificationCode, subject: string) {
    if (!subject) {
      subject = 'Email Verification';
    }

    await this.http
      // .post('http://localhost:3002/notify/sendVerifyEmail', {
      .post(`${await this.getPort()}/notify/sendVerifyEmail`, {
        to: email,
        verification_code: verificationCode,
        subject,
      })
      .subscribe((output) => {
        debug(output);
      });
  }

  async sendSmsCode(mobile, verificationCode) {
    debug('sent sms');
    await this.http
      // .post('http://localhost:3002/notify/sendVerifyMobile', {
      .post(`${await this.getPort()}/notify/sendVerifyMobile`, {
        to: mobile,
        verification_code: verificationCode,
        subject: 'Mobile Verification',
      })
      .subscribe((output) => {
        debug(output);
      });
  }

  async getKeycloak() {
    // START get keycloak token
    const keyCloakBaseUrl = this.config.envConfig.KEYCLOAK_BASE_URL;
    const keyCloakRealmMaster = this.config.envConfig.KEYCLOAK_REALM_ADMIN;

    // Keycloak connection
    const keyCloakClientID = this.config.envConfig.KEYCLOCK_CLIENT_ID;
    const keyCloakAdminClient =
      this.config.envConfig.KEYCLOCK_CLIENT_NAME_ADMIN;
    const keyCloakGrantType = this.config.envConfig.KEYCLOCK_GRANT_TYPE;
    const keyCloakScope = this.config.envConfig.KEYCLOCK_SCOPE;
    const keyCloakUsername = this.config.envConfig.KEYCLOCK_USERNAME;
    const keyCloakPassword = this.config.envConfig.KEYCLOCK_PASSWORD;

    const keyCloakRoleID = this.config.envConfig.KEYCLOCK_ROLE_ID;
    const keyCloakRoleName = this.config.envConfig.KEYCLOCK_ROLE_NAME;

    debug('ENV VARIABLE : ', {
      keyCloakClientID,
      keyCloakAdminClient,
      keyCloakGrantType,
      keyCloakScope,
      keyCloakUsername,
      keyCloakPassword,
    });

    const bodyData = `client_id=${keyCloakAdminClient}&grant_type=${keyCloakGrantType}&scope=${keyCloakScope}&username=${keyCloakUsername}&password=${keyCloakPassword}`;

    debug('bodyData : ', bodyData);

    try {
      const tokenRes = await this.http
        .post(
          `${keyCloakBaseUrl}/realms/${keyCloakRealmMaster}/protocol/openid-connect/token`,
          bodyData,
          {
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
            },
          },
        )
        .toPromise();
      return {
        token: tokenRes.data.access_token,
        ip: keyCloakBaseUrl,
      };
    } catch (err) {
      if (
        err &&
        err.response &&
        err.response.status >= 400 &&
        err.response.status < 500
      ) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('login err : ', err);
        throw err;
      }
    }
  }

  async getKeycloakRealm(userType) {
    let keyCloakRealm = '';
    if (userType === 'admin') {
      keyCloakRealm = this.config.envConfig.KEYCLOAK_REALM_ADMIN;
    } else {
      keyCloakRealm = this.config.envConfig.KEYCLOAK_REALM_AGENT;
    }
    debug(keyCloakRealm);
    return {
      relam: keyCloakRealm,
    };
  }

  async loginKeycloak(userType, username, pass) {
    // let loginRes = {};
    const resCode = null;
    const adminClientID = this.config.envConfig.KEYCLOCK_CLIENT_NAME_ADMIN;
    const agentClientID = this.config.envConfig.KEYCLOCK_CLIENT_NAME_AGENT;
    const grantType = this.config.envConfig.KEYCLOCK_GRANT_TYPE;
    if (userType === 'admin') {
      const bodyData = `client_id=${adminClientID}&grant_type=${grantType}&username=${username}&password=${pass}`;

      try {
        const loginRes = await this.http
          .post(
            `${this.config.envConfig.KEYCLOAK_BASE_URL}/realms/${this.config.envConfig.KEYCLOAK_REALM_ADMIN}/protocol/openid-connect/token`,
            bodyData,
            {
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
              },
            },
          )
          .toPromise();

        return loginRes.data;
      } catch (err) {
        if (
          err &&
          err.response &&
          err.response.status >= 400 &&
          err.response.status < 500
        ) {
          throw new HttpException(err.response.data, err.response.status);
        } else {
          console.error('login err : ', err);
          throw err;
        }
      }
    } else {
      // if it is agent
      const bodyData = `client_id=${agentClientID}&grant_type=${grantType}&username=${username}&password=${pass}`;

      try {
        const loginRes = await this.http
          .post(
            `${this.config.envConfig.KEYCLOAK_BASE_URL}/realms/${this.config.envConfig.KEYCLOAK_REALM_ADMIN}/protocol/openid-connect/token`,
            bodyData,
            {
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
              },
            },
          )
          .toPromise();

        return loginRes.data;
      } catch (err) {
        if (
          err &&
          err.response &&
          err.response.status >= 400 &&
          err.response.status < 500
        ) {
          throw new HttpException(err.response.data, err.response.status);
        } else {
          console.error('login err : ', err);
          throw err;
        }
      }
    }
  }

  async isUserExistKeycloak(usertype, userid) {
    // START get keycloak token
    const keycloakToken = (await this.getKeycloak()).token;
    // END

    const headers = {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${keycloakToken}`,
    };

    const found = await this.http
      .get(
        // 'http://54.255.196.41:8080/auth/admin/realms/master/users',
        `${(await this.getKeycloak()).ip}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_ADMIN}/users/${userid}`,
        {
          headers,
        },
      )
      // .toPromise();
      .toPromise()
      .then(({ data }) => {
        // return data;
        // return res.status(201).json(data);
      })
      .catch((err) => {
        throw new NotFoundException(`Record does not exist in keycloak`);
        // return {
        //   resultcode: 400,
        //   errMessage: 'Record doesnot exist in keycloak',
        // };
      });
  }

  //Forget password
  async sendEmailForgetPass(email: string, emailtype:string) {
    try {
      debug('Entered Forget password');
      const emailExist = await AdminEntity.findOne({ email });
      if (!emailExist) {
        debug('Record does not exist');
        throw new HttpException({
          name : 'recordNotFound',
          message :  'Record does not exist'
        },HttpStatus.BAD_REQUEST);
      }
     

      const sendNofiyStatus = this.config.envConfig.ENABLE_NOTIFY_SERVICE;
      if (sendNofiyStatus === 'true') {
        const notiData = {fieldValue:emailExist.email, type:"Email", emailtype}; //{fieldValue:bodyParams.email,type:"Email"};
        const notificationRes = await this.sentNotification(emailExist.id, notiData);
        debug('notificationRes :', notificationRes);
      } else {
        throw new HttpException({
          message:"System error. Notification service is not enabled. Please contact your system support",
          name: "NOTIFICATIOn_SERVICE_DISABLED"
        },HttpStatus.METHOD_NOT_ALLOWED);
      }
      return {message:`${emailtype} admin Verification code has been sent to your email ${email}`};
    } catch (err) {
      console.error('resend verification code to specific email Err:', err);
      throw err;
    }
  }
  // END

  private redisSetUrl = `${this.config.envConfig.USER_SERVICE_BASEURL || 'http://localhost'}/redis/setRedisVal`;
  private redisGetUrl = `${this.config.envConfig.USER_SERVICE_BASEURL || 'http://localhost'}/redis/getRedisVal`
  async sentNotification(id,bodyParams) {
    const sendNofiyStatus = this.config.envConfig.ENABLE_NOTIFY_SERVICE;
    if (sendNofiyStatus === 'true') {
      const verificationCode = await getRandomNumber(6);
      const unqrediskey = `${id}_${bodyParams.type}`;
      const value = JSON.stringify({verificationCode,fieldValue:bodyParams.fieldValue});

      const redisData = {
        "redisKey": unqrediskey,
        "redisVal": value
      };

      const setredisRes = await this.http.post(this.redisSetUrl,redisData).toPromise();

      debug('setredisRes :', setredisRes.data);

      // START send email to specific user with verification code
      try {
        debug('this.config.envConfig.JWT_VERIFY_PRIVATE_KEY :', this.config.envConfig.JWT_VERIFY_PRIVATE_KEY);
        const verification_token = JWT.encodeAndSign(this.config.envConfig.JWT_VERIFY_PRIVATE_KEY,{unqrediskey,id});
        // await this.sendEmailCode(bodyParams.fieldValue, {verification_token,verification_code:verificationCode},undefined);
        debug('verification_token code:',verification_token);
        debug('bodyParams.type:', bodyParams.type);
        if(bodyParams.type === "Email"){
          await this.resetPassLink(bodyParams.fieldValue, {verification_token,verification_code:verificationCode,emailtype: bodyParams.emailtype},bodyParams.emailtype);
          return {message : "Please verify the email adress"};
        }
      } catch(err) {
        throw err;
        console.error(err);
      }
    } else {
      throw new HttpException({
        name: "NOTIFICATION_DISABLED",
        message : "System error.  Plese contact your system admin to enable notification service"
      },HttpStatus.NOT_IMPLEMENTED);
    }
  }

  async resetPassLink(email, verificationObj:any,subject:string) {
    const userName = await AdminEntity.findOne({
      where: {
        email,
      },
    });
    debug('userName:',userName);

    const NOTIFY_SERVICE_URL = this.config.envConfig.NOTIFY_SERVICE_URL;
    const APP_BASE_RESET_PASSWORD_REDIRECT_URL = this.config.envConfig.APP_BASE_RESET_PASSWORD_REDIRECT_URL || 'https://dev.iqrate.io/reset-password?token=';

    const buff = Buffer.from(JSON.stringify(verificationObj));
    const tempToken = buff.toString('base64');
    const verificationLink = `${APP_BASE_RESET_PASSWORD_REDIRECT_URL}${encodeURI(tempToken)}`;
    debug('verificationLink : ', verificationLink);

    try {
      return await this.http
        .post(`${NOTIFY_SERVICE_URL}/notify/sendVerifyEmail`, {
          to: email,
          verification_code: verificationObj.verificationCode,
          link: verificationLink,
          subject: subject,
          template:'ResetPassword',
          name : userName.full_name,
        }).toPromise();
    } catch (err) {
      console.error('sendEmailCode : ',err);
      throw new HttpException({message:"System error. Failed to send notification. Please contact system support",name:"NOTIFICATION_FAILED"},HttpStatus.METHOD_NOT_ALLOWED);

    }
  }

  async updateResetPasswordWithToken(
    setPassword: DefaultPassword
  ) {
    try {
      debug('Entered update password service');

      let tokenObj:any = Buffer.from(setPassword.token,'base64').toString('ascii');

      debug('tokenObj :', tokenObj);
      if(tokenObj) {
        tokenObj = JSON.parse(tokenObj);
      }

      const decodedVal:any = JWT.decodeAndVerify(this.config.envConfig.JWT_VERIFY_PUBLIC_KEY, tokenObj.verification_token);
      debug('TokendecodedVal : ', decodedVal);
      const email = decodedVal.email;
      const id = decodedVal.id;
      const emailExist = await AdminEntity.findOne({ id });
      if (!emailExist) {
        debug('Record does not exist');
        throw new HttpException({
          name : 'recordNotFound',
          message :  'Record does not exist'
        },HttpStatus.BAD_REQUEST);
      }

      // START Keycloak update
      // START get keycloak token
      const keycloakToken = (await this.getKeycloak()).token;
      // END

      // START update password in keycloak
      const headers = {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${keycloakToken}`,
      };

      const datas = {
        type: 'password',
        value: setPassword.password,
        temporary: false,
      };

      // update password in keycloak w.r.t keyclock id
      // http://54.251.164.144:8080/auth/admin/realms/master/users/4da73c7d-7689-41b3-acbd-929696ca9ca9/reset-password
      const addRes = await this.http
        .put(
          `${(await this.getKeycloak()).ip}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_ADMIN}/users/${emailExist.id}/reset-password`,
          datas,
          {
            headers,
          },
        )
        .toPromise();
      debug('*****UPDATE OASS');
      debug(addRes);
      if (addRes.status !== 204) {
        throw new HttpException({
          name : 'errUpdatePassword',
          message :  'Error occured while updating password'
        },HttpStatus.BAD_REQUEST);
      }
      debug('pasword updated successfully in keycloak');
      return {message : "pasword updated successfully"};

    } catch (err) {
      console.error('Update password Err:', err);
      throw err;
    }
  }

  //chg admin password
   async chgAdminPassword(id: string, bodyParams: DefaultChgPswrdDto) {
    try {
      debug('Entered change admin password service');

      if (bodyParams.new_password !== bodyParams.confirm_password) {
        debug('Both password should match');
        throw new HttpException({
          name : 'passordShouldMatch',
          message :  'Both password should match'
        },HttpStatus.BAD_REQUEST);
      }
      
      const userExist = await AdminEntity.findOne({ id });

      let adminEmail = '';
      let adminId = '';

      // if (!userExist) {
      if( typeof(userExist) === 'undefined' ) {
          debug('enter if:');
        const keycloakToken = (await this.getKeycloak()).token;

        const headers = {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${keycloakToken}`,
        };
        try {
          debug('enter try');
          const addRes = await this.http
            .get(
                `${(await this.getKeycloak()).ip}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_ADMIN}/users/${id}`,
                {
                  headers,
                },
            )
            .toPromise();
          if(addRes.data.id){
            debug('record exist');
            debug('addres:data:',addRes.data);
            adminEmail = addRes.data.email;
            adminId = addRes.data.id;
          } else {
            debug('addres:data:id',addRes.data.id);
            throw new HttpException({
                name : 'NOT_FOUND',
                message : 'Record does not exist'
              },HttpStatus.BAD_REQUEST);
          }
        } catch (err) {
          console.log('enter catch:');
          if(err && err.response && err.response.status>=400 && err.response.status<500) {
            throw new HttpException(err.response.data,err.response.status);
          } else {
            console.error('chg admin pswrd err : ', err);
            throw err;
          }
        }
        // debug('Record doesnot exist');
        // // return 'Record doesnot exist';
        // throw new HttpException({
        //   name : 'recordNotFound',
        //   message :  'Record doesnot exist'
        // },HttpStatus.BAD_REQUEST);
      }

      debug('userExist val:',userExist);
      if(!adminEmail && !adminId)
      {
        if(userExist.email && userExist.id){
          adminEmail = userExist.email;
          adminId = userExist.id;
        }
      }
      
      debug('adminEmail val:',adminEmail);
      const loginObj = {email:adminEmail,password:bodyParams.old_password};

      let isOldPasswordCorrect = false;
      try {
        // const loginRes:any = await this.http.post(`${this.config.envConfig.USER_SERVICE_URL}/auth/login`,loginObj).toPromise();
        const loginRes:any = await this.http.post(`${this.config.envConfig.USER_SERVICE_BASEURL}/auth/adminlogin`,loginObj).toPromise();
        debug('loginRes: ', loginRes);
        if(loginRes && loginRes.data.access_token) {
          isOldPasswordCorrect = true;
        }
      } catch (err) {
        throw err;
      }

      if(isOldPasswordCorrect === false) {
        throw new HttpException({
          message:"You have provided an invalid current password.  Please check and try again",
          "name":"INVALID_CURRENT_PASSWORD"
        },HttpStatus.BAD_REQUEST);
      }

      const keycloakToken = (await this.getKeycloak()).token;

      const headers = {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${keycloakToken}`,
      };

      const datas = {
        type: 'password',
        value: bodyParams.new_password,
        temporary: false,
      };
      const addRes = await this.http
          .put(
              `${(await this.getKeycloak()).ip}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_ADMIN}/users/${adminId}/reset-password`,
              datas,
              {
                headers,
              },
          )
          .toPromise();
      debug('*****Change password');
      debug(addRes);
      debug('pasword changed successfully in keycloak');

      return {message: "Your password has been updated successfully!"};

    } catch (err) {
      console.error('Change password Err:', err);
      if(err && err.response && err.response.status>=400 && err.response.status<500) {
        throw new HttpException(err.response.data,err.response.status);
      }
      throw err;
    }
  }

  // add campaign

  public async addCampaign(reqParm: CampiagnDto, createby) {
    try {
     const startDate = new Date(reqParm.start_date).getTime();
     const endDate = new Date(reqParm.end_date).getTime();
     const diff = endDate - startDate;
     debug('startDate:', startDate);
     debug('endDate:', endDate);
     debug('diff:', diff);
    //  if(diff < 0 || diff === 0){
      if(diff < 0 ){
       debug('err in date');
       throw new HttpException({
          name : 'DATE_ERR',
          message : 'End date need to be greater than start date'
        },HttpStatus.BAD_REQUEST);
     }

      const campiagn = new CampaignEntity();
      campiagn.referal_code = reqParm.referal_code;
      campiagn.usage_limt = reqParm.usage_limt;
      campiagn.start_date = reqParm.start_date;
      campiagn.end_date = reqParm.end_date;
      // const startDateFormatDob = reqParm.start_date.split('-').reverse().join('-');
      // campiagn.start_date = startDateFormatDob;
      // const endDateFormatDob = reqParm.end_date.split('-').reverse().join('-');
      // campiagn.end_date = endDateFormatDob;
      campiagn.created_at = new Date();
      campiagn.created_user_id = createby;
      campiagn.status = reqParm.status;
      const res = await CampaignEntity.save(campiagn);
      // update campaign status based on status
      // this.updateCampStatus(createby);
      //
      return { message: 'Campaign added successfully' };
    } catch (err){
      debug('err is there:');
      debug('err:',err);
      if (err.name === 'QueryFailedError') {
        if (/^duplicate key value violates unique constraint/.test(err.message)) {
          err.detail = 'Referal code already exist';
          throw new BadRequestException(err.detail);
        } else if (/violates foreign key constraint/.test(err.message)) {
          throw new BadRequestException(err.detail);
        } else {
          throw err;
        }
      } else {
        throw err;
      }
    }
  }

  // list all campaign based on filter
  // Show user based on search and sort , if its there
  async showCampaignBasedOnsrch(alias: string, req, updtBy) {
    try {
      // update campaign status based on status
      // this.updateCampStatus(updtBy);
      //

        const builder: any = CampaignEntity.createQueryBuilder(alias);
        /* All search field need to be placed in diff diff variable  ex:http://localhost:3001/user?name=chaithra&email=chaithra@digitalprizm&sort=asc  */
          // full_name search query
      if (
        req.query.code !== '' ||
        req.query.status !== ''
      ) {
        // search for referal_code using code=
        if (req.query.code) {
          builder.where('campaignTbl.referal_code LIKE :matchCode', {
            matchCode: `%${req.query.code}%`,
          });
        }

        // search for status using status=       [0-inactive, 1-active]
        if (req.query.status) {
          builder.andWhere('campaignTbl.status = :status', {
            status: req.query.status,
          });
        }
        // builder.leftJoinAndSelect("users.bankers", "bank");

        builder.leftJoinAndSelect("campaignTbl.userInfo", "userInfo");
        // builder.select('COUNT(userInfo)', 'totalVideos');

        const actualSqlQuery = await builder.getQuery();
        debug(`=================${actualSqlQuery}`);
        // return builder.getMany();
      }

      // SORT by full_name using sort = asc or sort = desc
      // const sort: any = req.query.sort;
      const { sort }: any = req.query;
      if (sort) {
        builder.orderBy('campaigTbl.referal_code', sort.toUpperCase());
      } else {
        builder.orderBy("campaignTbl.created_at", 'DESC');
      }

      // pagination
      const page: number = parseInt(req.query.page as any, 10) || 1;
      const perPage = 10;
      const total = await builder.getCount();
      // builder.offset((page - 1) * perPage).limit(perPage);
      builder.skip((page - 1) * perPage).take(perPage);
      if(total <= 0 ) {
        throw new HttpException({
          name : 'NOT_FOUND',
          message : 'Record does not exist'
        },HttpStatus.BAD_REQUEST);
      }

      return {
        data: await builder.select(["campaignTbl", "userInfo.id", "userInfo.referal_code"]).getMany(),
        // data: await builder.getMany(),  
        total,
        page,
        lastPage: Math.ceil(total / perPage),
      };
    } catch (err) {
      if(err && err.response && err.response.status>=400 && err.response.status<500) {
        throw new HttpException(err.response.data,err.response.status);
      } else {
        console.error('get all campaign err : ', err);
        throw err;
      }
    }
  }

  async getSingleCampaign(id: number, req, updteby) {
    // const singleCampaign = await CampaignEntity.findOne({ id });
    // update campaign status based on status
    // this.updateCampStatus(updteby);
    //
    const queryData: any = CampaignEntity.createQueryBuilder('camptbl');
    queryData.where("camptbl.id = :id_camp", { id_camp: id });
    queryData.leftJoinAndSelect("camptbl.userInfo", "userInfo");

    const actualSqlQuery1 = await queryData.getQuery();
    debug(`=================${actualSqlQuery1}`);
    // const singleCampaign = await queryData.getMany();

    // pagination
    const page: number = parseInt(req.query.page as any, 10) || 1;
    const perPage = 10;
    queryData.offset((page - 1) * perPage).limit(perPage);
    // queryData.skip((page - 1) * perPage).take(perPage);

    const singleCampaign = await queryData.select(["camptbl", "userInfo.id","userInfo.full_name", "userInfo.email", "userInfo.mobile", "userInfo.country_code", "userInfo.membership_type", "userInfo.status", "userInfo.created_at", "userInfo.referal_code"]).getMany()

  const getRefercodeCode = singleCampaign[0]['referal_code']

  const productCount = await UserEntity.count({ referal_code:getRefercodeCode });
  console.log('productCount:', productCount);
  const total = productCount;


// if (!singleCampaign) {
    if (!singleCampaign[0]) {
      throw new HttpException({
        name : 'NOT_FOUND',
        message : 'Record does not exist'
      },HttpStatus.BAD_REQUEST);
    }

    debug('singleCampaign:', singleCampaign);
    // return singleCampaign;
    return {
      data: singleCampaign,
      // data: await builder.getMany(),  
      total,
      page,
      lastPage: Math.ceil(total / perPage),
    };
}

  // update campaign details
  async updateCampaign(id: number, bodyDeatials: CampiagnDto, updateby) {
    try {
      debug('Entered update campaign');

      const campaignExist = await CampaignEntity.findOne({ id });
      if (!campaignExist) {
        debug('Record does not exist');
        // return 'Record doesnot exist';
        throw new HttpException({
          name : 'recordNotFound',
          message :  'Record does not exist'
        },HttpStatus.BAD_REQUEST);
      }

      const startDate = new Date(bodyDeatials.start_date).getTime();
      const endDate = new Date(bodyDeatials.end_date).getTime();
      const diff = endDate - startDate;
      debug('startDate:', startDate);
      debug('endDate:', endDate);
      debug('diff:', diff);
      if(diff < 0 || diff === 0){
        debug('err in date');
        throw new HttpException({
           name : 'DATE_ERR',
           message : 'End date need to be greater than start date'
         },HttpStatus.BAD_REQUEST);
      }

      await CampaignEntity.update(
        { id },
        {
          referal_code: bodyDeatials.referal_code,
          usage_limt: bodyDeatials.usage_limt,
          // start_date: bodyDeatials.start_date.split('-').reverse().join('-'),
          // end_date: bodyDeatials.end_date.split('-').reverse().join('-'),
          start_date: bodyDeatials.start_date,
          end_date: bodyDeatials.end_date,
          status: bodyDeatials.status,
          updated_at: new Date(),
          updated_user_id: updateby,
        },
      );

      var res = await CampaignEntity.findOne({ id });
      // update campaign status based on status
      // this.updateCampStatus(updateby);
      //
      return {
        message: 'Campaign detail updated successfully',
        data: res
      }
    } catch (err) {
      console.error('Update campaign detail err:', err);
      if (err.name === 'QueryFailedError') {
        if (/^duplicate key value violates unique constraint/.test(err.message)) {
          throw new BadRequestException(err.detail);
        } else if (/violates foreign key constraint/.test(err.message)) {
          throw new BadRequestException(err.detail);
        } else {
          throw err;
        }
      } else {
        throw err;
      }
    }
  }

  //delete campaign details based on id
  public async deleteCampaign(id: number) {
    try {
      debug('Entered update campaign');

      const campaignExist = await CampaignEntity.findOne({ id });
      if (!campaignExist) {
        debug('Record does not exist');
        // return 'Record doesnot exist';
        throw new HttpException({
          name : 'recordNotFound',
          message :  'Record does not exist'
        },HttpStatus.BAD_REQUEST);
      }
      const res = await CampaignEntity.delete(id);
      return { message: 'Campaign deleted successfully' };
    } catch (err) {
      console.error('Register Err:', err);
      throw err;
    }
  }

  //set cobroke status (0=blocked, 1=active)

  async setCoBrokeStatus(id: string, bodyDeatials: DefaultUserStatusDto, updtBy) {
    try {
      debug('Entered set cobroke status service');

      const userExist = await UserEntity.findOne({ id });
      if (!userExist) {
        debug('Record does not exist');
        // return 'Record doesnot exist';
        throw new HttpException({
          name : 'recordNotFound',
          message :  'Record does not exist'
        },HttpStatus.BAD_REQUEST);
      }

      if(userExist.deleted === 1) {
        throw new HttpException({
          name : 'ACCOUNT_DELETED',
          message :  'Account was deleted by the user'
        },HttpStatus.BAD_REQUEST);
      }

      await UserEntity.update(
        { id },
        {
          cobroke_status: bodyDeatials.status,
          updated_at: new Date(),
          updated_user_id: updtBy,
        },
      );

      // START update assigned_cobrokeid tbl, if u set cobrokeP_status= 1 in users tbl for specific id
      // if admin set cobroke = 0 for specific user before it was in active state, then need to set last_assigned_user_id = 'false' for single latest record w.r.t specific id and need to set last_assigned_user_id = 'true' for other latest record i.e exxcept this specific user id in assigned_cobrokeid table
      debug('bodyDeatials.status:', bodyDeatials.status);
      if(bodyDeatials.status === 0)
      {
        const cobrokeExist = await AssignedCobrokeEntity.findOne({
          where: {
            assigned_user_id: id,
            last_assigned_user_id: true,
          },
          order: {
            id: "DESC"
          }
        });

        // if record exist in assigned_cobrokeid table
        debug('id', id);
        debug('cobrokeExist', cobrokeExist);
        if(cobrokeExist) {
          debug('user id exist in assigned_cobrokeid table:', cobrokeExist.id)

          // delete those records from assigned_cobrokeid tbl
          const res = await AssignedCobrokeEntity.delete({ id: cobrokeExist.id });
          debug('id deleted frm assigned_cobrokeid:', cobrokeExist.id );

          // after del get latest record frm assigned_cobrokeid tbl
          const afterDelCobrokeTbl = await AssignedCobrokeEntity.findOne({
            order: {
              id: "DESC"
            }
          });

          debug('latest cobrokeid afterDelCobrokeTbl:', afterDelCobrokeTbl);
          debug('latest cobrokeid:', afterDelCobrokeTbl.assigned_user_id);

          // if above selected id is active for cobroke in users tbl
          const existUSerTbl = await UserEntity.findOne({
            where: {
              cobroke_status: 1,
              status: 1,
              id: afterDelCobrokeTbl.assigned_user_id
            }
          });

          debug('existUSerTbl:', existUSerTbl);
          // set last_assigned_user_id = true for previous latest record if cobroke status set to inactive by admin for current id
          if(existUSerTbl) {
            debug('has record')
            await AssignedCobrokeEntity.update(
              { id: afterDelCobrokeTbl.id },
              { last_assigned_user_id: true, updated_at: new Date(), updated_user_id: updtBy },
            );
            debug('updated in assigned_cobrokeid table with true for latest record');
          }

          // // delete all entry from assigned_cobrokeid tbl w.r.t id, means that id we made it as cobroke_status = 0
          // const delCobroke = await AssignedCobrokeEntity.delete({ assigned_user_id: id });
        } else {
          debug('user id not exist in assigned_cobrokeid table')
        }
      }
      // END

      return {message: 'CoBroke status has been changed successfully'};
    } catch (err) {
      console.error('Update user current status err:', err);
      throw err;
    }
  }


  // auto generate password
  async autoGenCode() {
    const pass = generatePassword(7);
    debug('pass--',pass);
    return pass;
  }


  // list all contact us enquires based on filter
  // Show user based on search and sort , if its there
  async showcontactUsBasedOnsrch(alias: string, req) {
    try {
        const builder: any = ContactUs.createQueryBuilder(alias);
        /* All search field need to be placed in diff diff variable  ex:http://localhost:3001/allContactus?name=chaithra&email=chaithra@digitalprizm&sort=asc  */
          // full_name search query
      if (
        req.query.name !== '' ||
        req.query.email !== ''
      ) {
        // search for referal_code using code=
        if (req.query.name) {
          builder.where('contactTbl.name ILIKE :matchName', {
            matchName: `%${req.query.name}%`,
          });
        }

        // search for email using email=
        if (req.query.email) {
          builder.andWhere('contactTbl.email ILIKE :matchEmail', {
            matchEmail: `${req.query.email}%`,
          });
        }
      }

      // SORT by id using sort = asc or sort = desc
      // const sort: any = req.query.sort;
      const { sort }: any = req.query;
      if (sort) {
        builder.orderBy('contactTbl.id', sort.toUpperCase());
      } else {
        builder.orderBy('contactTbl.id', 'DESC');
      }

      // pagination
      const page: number = parseInt(req.query.page as any, 10) || 1;
      const perPage = 10;
      const total = await builder.getCount();
      // builder.offset((page - 1) * perPage).limit(perPage);
      builder.skip((page - 1) * perPage).take(perPage);
      if(total <= 0 ) {
        throw new HttpException({
          name : 'NOT_FOUND',
          message : 'Record does not exist'
        },HttpStatus.BAD_REQUEST);
      }

      return {
        data: await builder.select(["contactTbl"]).getMany(),
        // data: await builder.getMany(),  
        total,
        page,
        lastPage: Math.ceil(total / perPage),
      };
    } catch (err) {
      if(err && err.response && err.response.status>=400 && err.response.status<500) {
        throw new HttpException(err.response.data,err.response.status);
      } else {
        console.error('get all contact us err : ', err);
        throw err;
      }
    }
  }

  // get single contact us
  async getSingleContactUS(id: number) {
    const singleContact = await ContactUs.findOne({ id });
    if (!singleContact) {
      throw new HttpException({
        name : 'NOT_FOUND',
        message : 'Record does not exist'
      },HttpStatus.BAD_REQUEST);
    }

    // debug('singleUser:',singleUser.bankers[0]['bank_name']);
    debug('singleContact:',singleContact);
    return singleContact;
  }

  // list all admin bassed on search
  async showAdminBasedOnsrch(alias: string, req) {
    try {
        const builder: any = AdminEntity.createQueryBuilder(alias);
        /* All search field need to be placed in diff diff variable  ex:http://localhost:3001/user?name=chaithra&email=chaithra@digitalprizm&sort=asc  */
          // full_name search query
      if (
        req.query.name !== '' ||
        req.query.email !== '' ||
        req.query.phone !== '' ||
        req.query.status !== ''
        // req.query.fromdate !== '' ||
        // req.query.todate !== '' ||
        // req.query.code !== '' || 
        // req.query.coBroke !== ''
      ) {
        // search for ful_name using name=
        if (req.query.name) {
          builder.where('admin.full_name ILIKE :s', {
            s: `${req.query.name}%`,
          });
        }

        // search for email using email=
        if (req.query.email) {
          builder.andWhere('admin.email ILIKE :email', {
            email: `${req.query.email}%`,
          });
        }

        // search for mobile using phone=
        if (req.query.phone) {
          builder.andWhere('admin.mobile ILIKE :mobile', {
            mobile: `${req.query.phone}%`,
          });
        }

        // search for status using status=       [0-inactive, 1-active]
        if (req.query.status) {
          builder.andWhere('admin.status = :status', {
            status: req.query.status,
          });
        }

        const actualSqlQuery = await builder.getQuery();
        debug(`=================${actualSqlQuery}`);
        // return builder.getMany();
      }

      // SORT by full_name using sort = asc or sort = desc
      // const sort: any = req.query.sort;
      const { sort }: any = req.query;
      if (sort) {
        builder.orderBy('admin.full_name', sort.toUpperCase());
      } else {
        builder.orderBy('admin.created_at', 'DESC');
      }

      // pagination
      const page: number = parseInt(req.query.page as any, 10) || 1;
      const perPage = 10;
      const total = await builder.getCount();
      builder.offset((page - 1) * perPage).limit(perPage);
      if(total <= 0 ) {
        throw new HttpException({
          name : 'NOT_FOUND',
          message : 'Record does not exist'
        },HttpStatus.BAD_REQUEST);
      }

      return {
        data: await builder.select(['admin.id', 'admin.full_name', 'admin.email', 'admin.mobile', 'admin.status']).getMany(),

        total,
        page,
        lastPage: Math.ceil(total / perPage),
      };
    } catch (err) {
      if(err && err.response && err.response.status>=400 && err.response.status<500) {
        throw new HttpException(err.response.data,err.response.status);
      } else {
        console.error('all admin list err : ', err);
        throw err;
      }
    }
  }

  // get single admin detail
  async getSingleAdminInfo(id: string) {
    const singleAdmin = await AdminEntity.findOne({ id });
    if (!singleAdmin) {
      throw new HttpException({
        name : 'NOT_FOUND',
        message : 'Record does not exist'
      },HttpStatus.BAD_REQUEST);
    }

    debug('singleadmin:', singleAdmin);
    const { full_name, email, mobile, country_code, status } = singleAdmin;
    const showData = { full_name, email, mobile, country_code, status };
    return showData;
}

// update single admin info
async updateAdminInfo(id: string, params: DefaultUpdateAdminDto, updtedBy) {
  try {
    const getAdmin = await AdminEntity.findOne({ id });
    if (!getAdmin) {
      throw new HttpException({
        name : 'NOT_FOUND',
        message : 'Record does not exist'
      },HttpStatus.BAD_REQUEST);
    }
    
    // chk valid mail server
    const legit = require('legit');
    try {
      const res = await legit(params.email);
      // return res;
      if(!res.isValid){
        throw new HttpException({
          name : 'INVALID_EMAIL',
          message : 'You have entered an invalid email address. Please check your email address'
        },HttpStatus.BAD_REQUEST);
      }
    } catch (err) {
      console.error('err:', err.Error);
      
        throw new HttpException({
          name : 'INVALID_EMAIL',
          message : 'You have entered an invalid email address. Please check your email address'
        },HttpStatus.BAD_REQUEST);
    }

    // to chk valid mobile or not
    const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();
    var test = phoneUtil.parseAndKeepRawInput(params.mobile, params.country_code_label);
    const validMobile = phoneUtil.isValidNumber(test);
    if (!validMobile) {
      debug('entered mobile exist');
      throw new HttpException({
        name : 'INVALID_MOBILE',
        message : 'Invalid mobile number'
      },HttpStatus.BAD_REQUEST);
    }

    let emailChgd = false;
    let showData = {};
    debug('current email:', params.email);
    debug('exist email:', getAdmin.email);
    if(getAdmin.email !== params.email) {
      emailChgd = true;
    }
    if(emailChgd) {
      if(params.password === '') {
        throw new HttpException({
          name : 'INVALID_MOBILE',
          message : 'Password should not be empty for new email'
        },HttpStatus.BAD_REQUEST);
      }
    }
    
    if(params.password !== '') {
      const full_name = params.full_name;
      const email = params.email;
      const pswrd = params.password;
      const templateFor = 'updated';
      showData = {
        full_name,
        email,
        pswrd,
        templateFor,
      };
    }

    // START get keycloak token
    const keycloakToken = (await this.getKeycloak()).token;
    // END

    const headers = {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${keycloakToken}`,
    };

    //if password field is not empty
    if(params.password !== '') {
      // keycloak update password
      const keycloakToken = (await this.getKeycloak()).token;
      const headers = {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${keycloakToken}`,
      };
      const datas = {
        type: 'password',
        value: params.password,
        temporary: false,
      };
      const updatePass = await this.http
        .put(
            `${this.config.envConfig.KEYCLOAK_BASE_URL}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_ADMIN}/users/${getAdmin.id}/reset-password`,
            datas,
            {
              headers,
            },
        )
        .toPromise();
    }   

    // update in db
    await AdminEntity.update(
      { id: getAdmin.id },
      { full_name:  params.full_name, mobile: params.mobile, country_code: params.country_code, email: params.email, status: params.status, updated_at: new Date(), updated_user_id: updtedBy},
    );

    // START Keycloak update
    const updateVal = {
      username: params.email,
      firstName: params.full_name,
      enabled: params.status,
      email: params.email,
    };

    // set status in keycloak w.r.t keyclock id
    const updateRes = await this.http
      .put(
        `${this.config.envConfig.KEYCLOAK_BASE_URL}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_ADMIN}/users/${getAdmin.id}`,
        updateVal,
        {
          headers,
        },
      )
      .toPromise();
    // debug('*****UPDATE status');
    // debug(addRes);
    if (updateRes.status !== 204) {
      return {
        resultcode: 400,
        errMessage: 'Error occured while updating in keycloak',
      };
    }
    debug('pasword updated successfully in keycloak');
    debug('showData : ', showData);
    return { message : "Admin detail has been updated successfully.", showData };
  } catch (err){
    debug('err is there:');
    debug('err:',err);
    if (err.name === 'QueryFailedError') {
      if (/^duplicate key value violates unique constraint/.test(err.message)) {
        err.detail = 'We have noticed that you are using an existing IQrate admin’s mobile number/email for registration. Kindly use another email/mobile number. Thank you.';
        throw new BadRequestException(err.detail);
      } else if (/violates foreign key constraint/.test(err.message)) {
        throw new BadRequestException(err.detail);
      } else {
        throw err;
      }
    } else {
      throw err;
    }
  }
}

// send admin email with login credentials
async sendAdminEmail(reqBody: AdminEmailDto) {
  try {
    // if (!subject) {
    //   subject = 'Email Verification';
    // }

    await this.http
    .post(`${await this.getPort()}/notify/sendAdminCredential`, {
      name: reqBody.full_name,
      username: reqBody.email,
      password: reqBody.pswrd,
      template: "AdminCredentials",
      to: reqBody.email,
      templateFor: reqBody.templateFor,
    })
    .subscribe((output) => {
      debug(output);
    });
    return { message: `Login credential has been sent to ${reqBody.email} successfully` }
  } catch (err) {
    if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
      throw new HttpException(err.response.data, err.response.status);
    } else {
      debug('send email err : ', err);
      throw err;
    }
  }
}

// delete admin detail
// delete campaign details based on id
public async deleteAdmin(id: string) {
  try {
    debug('Entered delete admin');
    const adminExist = await AdminEntity.findOne({ id });
    if (!adminExist) {
      debug('Record does not exist');
      // return 'Record doesnot exist';
      throw new HttpException({
        name : 'recordNotFound',
        message :  'Record does not exist'
      },HttpStatus.BAD_REQUEST);
    }

    //delete frm keycloak
    const keycloakToken = (await this.getKeycloak()).token;

      const headers = {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${keycloakToken}`,
      };
      const delUser = await this.http
        .delete(
            `${this.config.envConfig.KEYCLOAK_BASE_URL}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_ADMIN}/users/${adminExist.id}`,
            {
              headers,
            },
        )
        .toPromise();

    const res = await AdminEntity.delete(id);
    return { message: 'Admin deleted successfully' };
  } catch (err) {
    console.error('Admin delete Err:', err);
    throw err;
  }
}

// get all admin module
public async getAllModule(id: string) {
  try {
    debug('enterd get module:');

    // get user roles frm keycloak
    const keycloakToken = (await this.getKeycloak()).token;
      debug('keycloakToken : ', keycloakToken);
    const headers = {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${keycloakToken}`,
    };

    let getRoles;
    try {
      getRoles = await this.http
        .get(
          // http://13.229.205.94:8080/auth/admin/realms/master/users/28599616-4cd2-4d0f-98b2-d40ddb304ee9/role-mappings/realm
          `${this.config.envConfig.KEYCLOAK_BASE_URL}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_ADMIN}/users/${id}/role-mappings/realm`,
          {
            headers,
          },
        ).toPromise();
      // debug(`Set Role Mapping :`, JSON.stringify({req,result}));
    } catch (err) {
      if(err && err.response && err.response.status>=400 && err.response.status<500) {
        throw new HttpException(err.response.data,err.response.status);
      } else {
        console.error('get acl modules err : ', err);
        throw err;
      }
    }
    debug('getRoles:', getRoles.data);
    let getAllRoles = [];
    for(var i = 0; i < getRoles.data.length; i++)
    {
         var a = getRoles.data[i].name;
         getAllRoles.push(a);
     }
    debug('getAllRolesval:', getAllRoles);

    const allModule = await AdminAcl
    .createQueryBuilder('a')
    .select(['a.principalid', 'a.module_name', 'a.role_type'])
    .where('a.module_name != :module', {
      module: 'test',
    })
    .orderBy({ 'a.order': 'ASC', 'a.role_type': 'DESC' })
    .groupBy('a.principalid')
    .addGroupBy('a.module_name')
    .addGroupBy('a.role_type')
    .addGroupBy('a.order')
    .getRawMany();
    debug('allModule:', allModule);
    // return allModule;
    let newModules = {}
    let roleStatus;
    allModule.forEach(singleModule => {
      debug('singleModule.a_principalid:', singleModule.a_principalid);
      if(getAllRoles.includes(singleModule.a_principalid)){
        roleStatus = 'yes';
      } else {
        roleStatus = 'no';
      }
      newModules[singleModule.a_module_name] ? 
      newModules[singleModule.a_module_name].push({roleType: singleModule.a_role_type, roleName: singleModule.a_principalid, roleStatus:roleStatus})  // just push
      : (newModules[singleModule.a_module_name] = [], newModules[singleModule.a_module_name].push({roleType: singleModule.a_role_type, roleName: singleModule.a_principalid, roleStatus:roleStatus})) // create a new array and push     
    })
  return newModules;
  } catch(err) {
    console.error('Get all acl modules Err:', err);
    throw err;
  }
}

// setAdminRole
public async setAdminRole(id: string, params: DefaultSetAdminRole) {
  try {
    debug('entered set role to admin service:')
    const keycloakToken = (await this.getKeycloak()).token;
      debug('keycloakToken : ', keycloakToken);

      // START save user in keycloak
      const headers = {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${keycloakToken}`,
      };

      // chk View role is set or not before set Manage role
      const parts = params.role_name.split('_');
      const lastPart = parts.pop();
      let fetchRole;
      debug('params.role_status:', params.role_status);
      if(params.role_status === 'SET') 
      {
        debug('entered if:');
        if(parts[0] === 'MANAGE') {
          try {
            fetchRole = await this.http
              .get(
                `${this.config.envConfig.KEYCLOAK_BASE_URL}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_ADMIN}/users/${id}/role-mappings/realm`,
                {
                  headers,
                },
              ).toPromise();
            // debug(`Set Role Mapping :`, JSON.stringify({req,result}));
          } catch (err) {
            if(err && err.response && err.response.status>=400 && err.response.status<500) {
              throw new HttpException(err.response.data,err.response.status);
            } else {
              console.error('get acl modules err : ', err);
              throw err;
            }
          }
          debug('fetchRole:', fetchRole.data);
          let roleArr = [];
          for(var i = 0; i < fetchRole.data.length; i++)
          {
              var a = fetchRole.data[i].name;
              roleArr.push(a);
          }
          debug('getAllRolesval:', roleArr);
          if(roleArr.includes(`VIEW_${lastPart}`)) {
          } else {
            throw new HttpException({
              name : 'VIEW_IS_NOT_SET',
              message :  'To set Manage, first set View role'
            },HttpStatus.BAD_REQUEST);
          }
        }
      } else {
        debug('enterd else');
        if(parts[0] === 'VIEW') {
          try {
            fetchRole = await this.http
              .get(
                `${this.config.envConfig.KEYCLOAK_BASE_URL}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_ADMIN}/users/${id}/role-mappings/realm`,
                {
                  headers,
                },
              ).toPromise();
            // debug(`Set Role Mapping :`, JSON.stringify({req,result}));
          } catch (err) {
            if(err && err.response && err.response.status>=400 && err.response.status<500) {
              throw new HttpException(err.response.data,err.response.status);
            } else {
              console.error('get acl modules err : ', err);
              throw err;
            }
          }
          debug('fetchRole:', fetchRole.data);
          let roleArr = [];
          for(var i = 0; i < fetchRole.data.length; i++)
          {
              var a = fetchRole.data[i].name;
              roleArr.push(a);
          }
          debug('getAllRolesval:', roleArr);
          if(roleArr.includes(`MANAGE_${lastPart}`)) {
            throw new HttpException({
              name : 'MANAGE_IS_SET',
              message :  'To unset View, first unset manage role'
            },HttpStatus.BAD_REQUEST);
          }
        }
      }
      ////

      
    // // set admin role
    // GET THE ROLE DYNAMICALLY BY THE NAME
    const req = await getConnection().query(`SELECT id,name FROM public.keycloak_role where name='${params.role_name}';`);

    debug('setAdminRole req : ', req);

      try {
        if(params.role_status === 'SET') {
          const result = await this.http
            .post(
              // 'http://54.251.187.218:8080/auth/admin/realms/master/users/c51b90c8-cb7f-4ad0-80e3-14b6b079e128/role-mappings/realm',
              `${this.config.envConfig.KEYCLOAK_BASE_URL}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_ADMIN}/users/${id}/role-mappings/realm`,
              req,
              {
                headers,
              },
            ).toPromise();
          // debug(`Set Role Mapping :`, JSON.stringify({req,result}));
          if(result.config.data === '[]') {
            throw new HttpException({
              name : 'ROLES_NOT_EXIST',
              message :  'Role is not set. Please provide available role'
            },HttpStatus.BAD_REQUEST);
          }
        } else {
          const result = await this.http
            .delete(
              `${this.config.envConfig.KEYCLOAK_BASE_URL}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_ADMIN}/users/${id}/role-mappings/realm`,
              {
                data:req,
                headers,
              },
            ).toPromise();
          // debug(`Reset Role Mapping :`, JSON.stringify({req,result}));
          if(result.config.data === '[]') {
            throw new HttpException({
              name : 'ROLES_NOT_EXIST',
              message :  'Role is not unset. Please provide existinng role'
            },HttpStatus.BAD_REQUEST);
          }
        }
        return { message: "Role has been updated successfully"}
      } catch (err) {
        if(err && err.response && err.response.status>=400 && err.response.status<500) {
          throw new HttpException(err.response.data,err.response.status);
        } else {
          console.error('set role err : ', err);
          throw err;
        }
      }
  } catch (err) {
    console.error('Set Admin role Err:', err);
    throw err;
  }
}

async updateCampStatus() {
  const curDte = new Date();
    const getCamp = await CampaignEntity
    .createQueryBuilder('c')
    .select(['c.end_date', 'c.id', 'c.status'])
    .where('c.end_date < :sdate', {
      sdate: curDte,
    })
    .andWhere('c.status = :statusVal', {
      statusVal: 1,
    })
    .getRawMany();
    
    debug('getCamp:', getCamp);
    let campIds = [];
    for(var i = 0; i < getCamp.length; i++)
    {
        var a = getCamp[i].c_id;
        campIds.push(a);
    }
    debug('campIds:', campIds);
    await CampaignEntity.update(
      {id: In(campIds)},
      {
        status: 0,
        updated_at: new Date(),
        // updated_user_id: updtBy,
      },
    );   
}

// get admin profile by passing token
async getAdminProfile(id) {
  // const user = await UserEntity.findByIds([req.headers.userid]);
  const user = await AdminEntity.findByIds(id);
  debug(' user : ', user);
  return {status : 1 , data : user[0]};
}

// update admin image
async uploadphoto(id, content) {
  let emailTemplate: any = {};
  try {
    debug("Entered admin image upload");

    const adminExist = await AdminEntity.findOne({ id });
      if (!adminExist) {
        throw new HttpException({
          name: 'recordNotFound',
          message: 'Record does not exist'
        }, HttpStatus.BAD_REQUEST);
      }

      debug('fileexist:', content.length);
      if(content.length === 0){
        throw new HttpException({
          name: 'NOT_EMPTY',
          message: 'File should not be empty'
        }, HttpStatus.BAD_REQUEST);
      } 

    const imageUrl = await this.uploadToS3(content);
    debug('img url:', imageUrl);
    if (typeof imageUrl === 'string') {
      await AdminEntity.update(
        { id },
        {
          photo: imageUrl,
          updated_at: new Date(),
          updated_user_id: id,
        },
      );
      return { message: "Profile image updated successfully"}
    }
  } catch (err) {
    console.error('Upload image err :', err);
    throw err;
  }
}

async uploadToS3(files) {
  for (const file of files) {
    const a = await this.uploadFile(file.filename, file);
    return a;
  }
}

async uploadFile(fileName, file) {
  // Setting up S3 upload parameters
  const AWS_S3_BUCKET_NAME = this.config.envConfig.AWS_S3_BUCKET_NAME || 'iqrate-images';
  const AWS_ACCESS_KEY_ID = this.config.envConfig.AWS_ACCESS_KEY_ID || 'AKIARMFLUTLWW52WJ2CB';
  const AWS_SECRET_ACCESS_KEY = this.config.envConfig.AWS_SECRET_ACCESS_KEY || '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';

  const fileContent = Buffer.from(file.content, 'base64');

  const params = {
    Bucket: AWS_S3_BUCKET_NAME,
    Key: fileName, // File name you want to save as in S3
    Body: fileContent,
    ContentType: 'image/jpeg',
    ContentDisposition: 'inline',
  };

  const s3 = new AWS.S3({
    accessKeyId: AWS_ACCESS_KEY_ID,
    secretAccessKey: AWS_SECRET_ACCESS_KEY
  });

  // Uploading files to the bucket
  return new Promise((res, rej) => {
    s3.upload(params, function (err, data) {
      if (err) {
        return rej(err);
      }
      console.log(`File backup to s3 successfully. ${data.Location}`);
      return res(data.Location);
    });
  });
};

// update my profile
async updtaeMyProfile(id: string, params: DefaultUpdateMyProfileDto) {
  try {
    const getAdmin = await AdminEntity.findOne({ id });
    if (!getAdmin) {
      throw new HttpException({
        name : 'NOT_FOUND',
        message : 'Record does not exist'
      },HttpStatus.BAD_REQUEST);
    }
    
    // to chk valid mobile or not
    const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();
    var test = phoneUtil.parseAndKeepRawInput(params.mobile, params.country_code_label);
    const validMobile = phoneUtil.isValidNumber(test);
    if (!validMobile) {
      debug('entered mobile exist');
      throw new HttpException({
        name : 'INVALID_MOBILE',
        message : 'Invalid mobile number'
      },HttpStatus.BAD_REQUEST);
    }

    // START get keycloak token
    const keycloakToken = (await this.getKeycloak()).token;
    // END

    const headers = {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${keycloakToken}`,
    };

    //if password field is not empty
    if(params.password !== '') {
      // keycloak update password
      const keycloakToken = (await this.getKeycloak()).token;
      const headers = {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${keycloakToken}`,
      };
      const datas = {
        type: 'password',
        value: params.password,
        temporary: false,
      };
      const updatePass = await this.http
        .put(
            `${this.config.envConfig.KEYCLOAK_BASE_URL}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_ADMIN}/users/${getAdmin.id}/reset-password`,
            datas,
            {
              headers,
            },
        )
        .toPromise();
    }   

    // update in db
    await AdminEntity.update(
      { id: getAdmin.id },
      { full_name:  params.full_name, mobile: params.mobile, country_code: params.country_code, updated_at: new Date(), updated_user_id: id},
    );

    // START Keycloak update
    const updateVal = {
      firstName: params.full_name,
    };

    // set status in keycloak w.r.t keyclock id
    const updateRes = await this.http
      .put(
        `${this.config.envConfig.KEYCLOAK_BASE_URL}/admin/realms/${this.config.envConfig.KEYCLOAK_REALM_ADMIN}/users/${getAdmin.id}`,
        updateVal,
        {
          headers,
        },
      )
      .toPromise();
    // debug('*****UPDATE status');
    // debug(addRes);
    if (updateRes.status !== 204) {
      return {
        resultcode: 400,
        errMessage: 'Error occured while updating in keycloak',
      };
    }
    debug('pasword updated successfully in keycloak');
    return { message : "Profile detail has been updated successfully." };
  } catch (err){
    debug('err is there:');
    debug('err:',err);
    if (err.name === 'QueryFailedError') {
      if (/^duplicate key value violates unique constraint/.test(err.message)) {
        err.detail = 'We have noticed that you are using an existing IQrate adminn’s mobile number for registration. Kindly use another mobile number. Thank you.';
        throw new BadRequestException(err.detail);
      } else if (/violates foreign key constraint/.test(err.message)) {
        throw new BadRequestException(err.detail);
      } else {
        throw err;
      }
    } else {
      throw err;
    }
  }
}

// addPushNotification
public async addPushNotification(reqParm: PushNotifyDto, createby) {
  try {
    const a = reqParm.target_audience;
      const arrayLen = a.length;
      debug('lengt:', arrayLen);
      if(arrayLen === 0){
        throw new HttpException({
          name : 'TARGET_AUDIENCE_NOT_BLANK',
          message : 'Please select atleast one target audience'
        },HttpStatus.BAD_REQUEST);
      }

   const notifyDate = new Date(reqParm.notify_date).getTime();
   const notifyTime = reqParm.notify_time;

   const notifydt = new Date(reqParm.notify_date_time); // reqParm.notify_date_time SHOULD BE IN THIS FORMAT(UTC) 2022-09-25T14:21:13.965Z
  //  const localTime = notifydt.toLocaleString();
  const localTime = notifydt.toLocaleString('en-US', {hour12: false});
  //   var date1 = new Date('2012-11-29 17:00:34 UTC');
  // const b = date1.toLocaleString('en-GBC');
  // const c = date('07-25-2012','%m-%d-%Y');

   /// call notify service to add into table
   await this.http
   .post(
     `${this.config.envConfig.NOTIFY_SERVICE_URL}/notify/add-push-notification`,
     {
       title: reqParm.title,
       message: reqParm.message,
       target_audience: reqParm.target_audience,
       notify_date: reqParm.notify_date,
       notify_time: reqParm.notify_time,
       notify_date_time: localTime,
       createby: createby,
     },
   )
   .subscribe((res) => {
   });
   debug('added into table:');
   return { message: 'Push notification added successfully' };
  } catch (err){
    debug('err is there:');
    debug('err:',err);
    throw err;
  }
}

// admin can send push notification by manaully run cron job where sent_status = 0 and notify_dat_time < pastdate in notification service all_push_notify tbl
public async runCron() {
  try {
    await this.http
   .post(
     `${this.config.envConfig.NOTIFY_SERVICE_URL}/notify/runCronToSendPushNotification`,
     {
     },
   )
   .subscribe((res) => {
   });
   debug('Cron job manully ran and sent an push notification successfully');
   return { message: 'Cron job manully ran and sent an push notification successfully' };
  } catch (err){
    debug('err is there in manual cron job run:');
    debug('err:',err);
    throw err;
  }
}
   
} // end of main export class

async function getRandomNumber(digit) {
  const val = await Math.random().toFixed(digit).split('.')[1];
  return val;
}

// get age
function getAge(dateString) {
  // const birth = new Date(dateString); // input should be in getAge('1981-09-20')
  const birth = new Date(dateString.split('-').reverse().join('-')); // input should be in getAge('20-09-1981')
  const now = new Date();
  const beforeBirth =
    (() => {
      birth.setDate(now.getDate());
      birth.setMonth(now.getMonth());
      return birth.getTime();
    })() < birth.getTime()
      ? 0
      : 1;
  return now.getFullYear() - birth.getFullYear() - beforeBirth;
}

// auto generate password
function generatePassword(passwordLength) {
  var numberChars = "0123456789";
  var upperChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  const allChars = numberChars + upperChars;
  var randPasswordArray = Array(passwordLength);
  randPasswordArray[0] = numberChars;
  randPasswordArray[1] = upperChars;
  randPasswordArray = randPasswordArray.fill(allChars, 3);
  return shuffleArray(randPasswordArray.map(function(x) { return x[Math.floor(Math.random() * x.length)] })).join('');
}

function shuffleArray(array) {
  for (var i = array.length - 1; i > 0; i--) {
    var j = Math.floor(Math.random() * (i + 1));
    var temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
  return array;
}