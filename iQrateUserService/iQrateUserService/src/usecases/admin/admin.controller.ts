import {
  Controller,
  Get,
  Post,
  Req,
  Body,
  Patch,
  Headers,
  HttpException,
  HttpStatus,
  Param,
  Put,
  Delete,
  Res,
  UseInterceptors,
  UploadedFiles
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { Request } from 'express';
import Debug from 'debug';
import { AdminService } from './admin.service';
// import { LoginDto } from './login.dto';
import {
  AdminRegisterResDto,
  AdminAllUserListResDto,
  CampiagnDto,
  ChgAdminPassResDto,
  ChgStatusResDto,
  ChkValidAdminResDto,
  DefaultChgPswrdDto,
  DefaultUserStatusDto,
  Disable2FAResDto,
  EmailMobileVerifyStatusDto,
  LogoutDto,
  RefreshDto,
  SendResetPassResDto,
  UserDto,
  AdminUserListResDto,
  SubInvoicesResDto,
  SubInvoiceResDto,
  AdminRefreshTokenResDto,
  AddCampaignResDto,
  AllCampaignResDto,
  CampaignSet,
  UpdateCampaignResDto,
  DeleteCampaignResDto,
  AllContactusResDto,
  ContactSet,
  ExportItem,
  DefaultUpdateAdminDto,
  AdminDto,
  AdminEmailDto,
  DefaultSetAdminRole,
  DefaultUpdateMyProfileDto,
  reinstateUserResDto,
  AddPushNotifyResDto,
  PushNotifyDto,
  ExportSubscriptionItem,
  ExportSRevennueItem,
} from '../user/dto/user.dto';
import { UserService } from '../user/user.service';
import { TwoFactorAuthenticationService } from '../user/twoFactorAuthentication.service';
import { SubscriptionService } from '../subscription/subscription.service';
import { AclPermissionUpdateDto, AclRatelimitUpdateDto, MemberShipAclService } from 'src/middlewares/acl_module/membership_acl_service';
import { FilesInterceptor } from "@nestjs/platform-express";

const debug = Debug('user-service:AdminController');

@ApiTags('Admin')
@Controller('admin')
export class AdminController {
  constructor(private readonly adminService: AdminService, private userService: UserService, private readonly twoFactorAuthenticationService: TwoFactorAuthenticationService, private readonly subscriptionService: SubscriptionService, private readonly memberShipService : MemberShipAclService) { }

  

  @Post('/register')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: AdminRegisterResDto,
  })
    @ApiForbiddenResponse({ description: 'forbidden' })
  async register(@Body() content: AdminDto) {
    console.log('Entering Register controller...');
    console.log('Admin Register inputs: ', content);
    return this.adminService.register(content, 0);
  }

  @Post('/sendAdminEmail')
  @ApiBearerAuth()
  // @ApiCreatedResponse({
  //   type: AdminRegisterResDto,
  // })
    @ApiForbiddenResponse({ description: 'forbidden' })
  async sendAdminEmail(@Body() content: AdminEmailDto) {
    debug('Entering send admin email controller...');
    return this.adminService.sendAdminEmail(content);
  }

  /////
  // chk user is exist or not
  @Get('chkValidAdmin')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: ChkValidAdminResDto,
  })
  @ApiOkResponse({ description: 'check admin is registerd or not' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async chkValidAdmin(
    @Headers() headers: Headers
  ) {
    //  debug("Entering Register controller...");
    const userId = headers['admin_userid'];
    if (!userId) {
      new HttpException({
        name: "ADMINID_NOT_FOUND",
        message: 'Token is invalid'
      }, HttpStatus.UNAUTHORIZED);
    }
    return this.adminService.chkValidAdmin(userId);
  }
  /////

  // Get AggregatesUsers
  @Get('getAggregateUsers')
  @ApiOkResponse({
    description: 'Returned Aggregated Users By MembershipType and Joined Month',
  })
  @ApiBearerAuth()
  @ApiForbiddenResponse({ description: 'forbidden' })
  async getAggregateUsers(@Req() req: Request,) {
    //  debug("Entering Register controller...");
    return this.adminService.getAggregateUsers(req);
  }

  // export user subbscription summary details to csv
 @Post('userSubscriptionToCSV')
 @ApiBearerAuth()
 @ApiOkResponse({ description: 'export user subbscription   summary details into csv' })
 @ApiForbiddenResponse({ description: 'forbidden' })
 async exportsubscriptionDetCSV(@Body() defaultVal: ExportSubscriptionItem, @Res() resp) {
   return this.userService.exportsubscriptionDetCSV(defaultVal, resp);
 }

 // Get revenue report for subscription
 @Get('getRevuneSubscription')
 @ApiOkResponse({
   description: 'Returned revenue report for subscription',
 })
 @ApiBearerAuth()
 @ApiForbiddenResponse({ description: 'forbidden' })
 async getRevuneSubscription(@Req() req: Request,) {
   //  debug("Entering Register controller...");
   return this.adminService.getRevuneSubscription(req);
 }

  // export revenue details to csv
  @Post('revenueReportToCSV')
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'export revenue report into csv' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async exportRevenueDetCSV(@Body() defaultVal: ExportSRevennueItem, @Res() resp) {
    return this.userService.exportRevenueDetCSV(defaultVal, resp);
  }

  // Get user summary
 @Get('getUsersummary')
 @ApiOkResponse({
   description: 'Returned user summary report',
 })
 @ApiBearerAuth()
 @ApiForbiddenResponse({ description: 'forbidden' })
 async getUserSummary(@Req() req: Request,) {
   //  debug("Entering Register controller...");
   return this.adminService.getUserSummary(req);
 }

 // export revenue details to csv
 @Post('userSummaryToCSV')
 @ApiBearerAuth()
 @ApiOkResponse({ description: 'export user summary into csv' })
 @ApiForbiddenResponse({ description: 'forbidden' })
 async exportUserSummaryCSV(@Body() defaultVal: ExportSubscriptionItem, @Res() resp) {
   return this.userService.exportUserSummaryCSV(defaultVal, resp);
 }


  @Put('changeAdminPassword')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: ChgAdminPassResDto,
  })
  @ApiOkResponse({ description: 'User password has been changed' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async chgAdminPassword(
    @Headers() headers: Headers,
    @Body() defaultVal: DefaultChgPswrdDto,
  ) {
    const userId = headers['admin_userid'];
    if (!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message: 'Token is invalid'
      }, HttpStatus.UNAUTHORIZED);
    }
    //  debug("Entering Register controller...");
    return this.adminService.chgAdminPassword(userId, defaultVal);
  }

  // Set user status i.e active or block (0=blocked, 1=active)
  @Put('block-unblock/:id')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: ChgStatusResDto,
  })
  @ApiOkResponse({ description: 'Updated user current status' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async setUserStatus(
    @Param('id') id: string,
    @Body() defaultVal: DefaultUserStatusDto,
  ) {
    return this.userService.setUserStatus(id, defaultVal);
  }

  /// Admin can Toggle Email Verification status(i.e 0-not verified, 1-verified)
  @Put('activateEmail/:id')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: ChgStatusResDto,
  })
  @ApiOkResponse({ description: 'User Email ID has been activated' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async activateEmail(
    @Param('id') id: string,
    @Body() defaultVal: EmailMobileVerifyStatusDto,
  ) {
    //  debug("Entering Register controller...");
    return this.userService.activateEmail(id, defaultVal);
  }

  // reinstate user within 30 days
  @Put('reinstateUser/:id')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: reinstateUserResDto,
  })
  @ApiOkResponse({ description: 'User has been reinstated' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async reinstateUser(
    @Param('id') id: string,
  ) {
    //  debug("Entering Register controller...");
    return this.userService.reinstateUser(id);
  }

  /// Admin can Toggle Phone Verification status(i.e 0-not verified, 1-verified)
  @Put('activatePhone/:id')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: ChgStatusResDto,
  })
  @ApiOkResponse({ description: 'User phone number has been activated' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async activatePhone(
    @Param('id') id: string,
    @Body() defaultVal: EmailMobileVerifyStatusDto,
  ) {
    //  debug("Entering Register controller...");
    return this.userService.activatePhone(id, defaultVal);
  }

  // send email link to reset password
  @Get('sendResetPassword/:email')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: SendResetPassResDto,
  })
  @ApiOkResponse({ description: 'sent email to reset password' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async resetPassword(@Param('email') email: string) {
    //  debug("Entering Register controller...");
    // return this.userService.sendEmailLinkToResetPswrd(email);
    return this.userService.resendEmailWithCode(email, 'ResetPassword');
  }

  // START set isTwoFactorAuthenticationEnabled: false
  @Put('disable2FA/:id')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: Disable2FAResDto,
  })
  @ApiOkResponse({
    description: 'Disable google 2FA',
  })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async disable2FA(@Param('id') id: string,) {
    //  debug("Entering Register controller...");
    return this.twoFactorAuthenticationService.disable2FA(id);
  }

  // List all user based on filter
  @Get('/users')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: AdminAllUserListResDto,
  })
  @ApiOperation({
    summary:
      '1. To search specific user by full_name,email, try "http://localhost:3001/user?name=cha&email=chaithra@digitalprizm.net" API in postman, 2.To search specific user by more filter , try "http://localhost:3001/user?fromdate=01-12-2021&todate=02-01-2022&phone=234523415&code=564545&sort=asc" API in postman',
  })
  async getAllUsers(@Req() req: Request,
  ) {
    // const userctrl = new UserController(new UserService(new ConfigService()));
    return await this.userService.showUserBasedOnsrch('users', req);
  }

  @Get('user/:id')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: AdminUserListResDto,
  })
  @ApiOkResponse({ description: 'Fetched user profile' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async getSingleUser(@Param('id') id: string) {
    return this.userService.adminGetFullProfile(id);
  }

  @Get('subscription/invoices/:id?')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: SubInvoicesResDto,
  })
  @ApiOkResponse({ description: 'get users invoices' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async getInvoices(
    @Headers() headers: Headers,
    @Param('id') customer_id: string,
    @Req() req: Request
  ) {
    const userId = headers['admin_userid'];
    if (!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message: 'Token is invalid'
      }, HttpStatus.UNAUTHORIZED);
    }
    //  debug("Entering Register controller...");
    return this.subscriptionService.getInvoices(customer_id, req);
  }

  @Get('subscription/invoice/:id')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: SubInvoiceResDto,
  })
  @ApiOkResponse({ description: 'get invoice' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async getInvoice(
    @Headers() headers: Headers,
    @Param('id') invoice_id: string
  ) {
    const userId = headers['admin_userid'];
    if (!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message: 'Token is invalid'
      }, HttpStatus.UNAUTHORIZED);
    }
    //  debug("Entering Register controller...");
    return this.subscriptionService.getInvoiceDetails(invoice_id);
  }

  /**
   * Get Payments
   * @param customer_id
   * @returns 
   */
  @Get('subscription/payments/:id')
  @ApiBearerAuth()
  @ApiCreatedResponse({ description: 'Get Payments/Transactions' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async getPayments(@Headers() headers: Headers, @Param('id') customer_id: string, @Req() req: Request) {
    const userId = headers['admin_userid'];
    if (!userId) {
      new HttpException({
        name: 'USERID_NOT_FOUND',
        message: 'Token is invalid',
      },
        HttpStatus.UNAUTHORIZED,
      );
    }
    return this.subscriptionService.getPayments(customer_id, req);
  }

  /**
   * Get payment details
   * @param payment_id
   * @returns 
   */
  @Get('subscription/payment/:id')
  @ApiBearerAuth()
  @ApiCreatedResponse({ description: 'Get Payment details' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async getPaymentDetails(@Headers() headers: Headers, @Param('id') payment_id: string) {
    const userId = headers['admin_userid'];
    if (!userId) {
      new HttpException({
        name: 'USERID_NOT_FOUND',
        message: 'Token is invalid',
      },
        HttpStatus.UNAUTHORIZED,
      );
    }
    return this.subscriptionService.getPaymentDetails(payment_id);
  }

  // campaign register
  @Post('/addCampaign')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: AddCampaignResDto,
  })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async addCampaign(
    @Headers() headers: Headers,
    @Body() content: CampiagnDto) {
    const userId = headers['admin_userid'];
    if (!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message: 'Token is invalid'
      }, HttpStatus.UNAUTHORIZED);
    }
    console.log('Entering campaign controller...');
    console.log('Campaign inputs: ', content);
    return this.adminService.addCampaign(content, userId);
  }

  // list all campaign based on filter
  @Get('/allCampaign')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: AllCampaignResDto,
  })
  @ApiOperation({
    summary:
      '1. To search specific campaign by code,status, try "http://localhost:3001/admin/AllCampaign?code=BF34ER&status=1" API in postman',
  })
  async getAllCampaign(
    @Headers() headers: Headers,
    @Req() req: Request,
) {
    const userId = headers['admin_userid'];
    if (!userId) {
      new HttpException({
        name: "ADMINID_NOT_FOUND",
        message: 'Token is invalid'
      }, HttpStatus.UNAUTHORIZED);
    }
    return await this.adminService.showCampaignBasedOnsrch('campaignTbl', req, userId);
  }

  // Get campaign by id
  @Get('getCampaign/:id')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: CampaignSet,
  })
  @ApiOkResponse({ description: 'Get campaign by id' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async getSingleCampaign(@Headers() headers: Headers, @Param('id') id: number, @Req() req: Request) {
    const userId = headers['admin_userid'];
    if (!userId) {
      new HttpException({
        name: "ADMINID_NOT_FOUND",
        message: 'Token is invalid'
      }, HttpStatus.UNAUTHORIZED);
    }
    return this.adminService.getSingleCampaign(id, req, userId);
  }

  // / update campaign details
  @Put('upadteCampaign/:id')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: UpdateCampaignResDto,
  })
  @ApiOkResponse({ description: 'Updated campaign details' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async updateCampaign(
    @Headers() headers: Headers,
    @Param('id') id: number,
    @Body() defaultVal: CampiagnDto,
  ) {
    const userId = headers['admin_userid'];
    if (!userId) {
      new HttpException({
        name: "ADMINID_NOT_FOUND",
        message: 'Token is invalid'
      }, HttpStatus.UNAUTHORIZED);
    }
    return this.adminService.updateCampaign(id, defaultVal, userId);
  }

  // update campaign details
  @Delete('deleteCampaign/:id')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: DeleteCampaignResDto,
  })
  @ApiOkResponse({ description: 'deleted campaign details' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async deleteCampaign(
    @Headers() headers: Headers,
    @Param('id') id: number
  ) {
    const userId = headers['admin_userid'];
    if (!userId) {
      new HttpException({
        name: "ADMINID_NOT_FOUND",
        message: 'Token is invalid'
      }, HttpStatus.UNAUTHORIZED);
    }
    return this.adminService.deleteCampaign(id);
  }

  // Get All Acl List
  @Get('/getAllAcl')
  @ApiBearerAuth()
  async getAllAcl(@Headers() headers: Headers, @Req() req: Request,
  ) {
    const userId = headers['admin_userid'];
    if (!userId) {
      new HttpException({
        name: 'USERID_NOT_FOUND',
        message: 'Token is invalid',
      },
        HttpStatus.UNAUTHORIZED,
      );
    }
    return await this.memberShipService.getAllAcl();
  }

  // Update Acl Status
  @Put('updateAclPermission/:id')
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'User phone number has been activated' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async updateaclStatus(
    @Headers() headers: Headers,
    @Param('id') id: string,
    @Body() status: AclPermissionUpdateDto
  ) {
    const userId = headers['admin_userid'];
    if (!userId) {
      new HttpException({
        name: 'USERID_NOT_FOUND',
        message: 'Token is invalid',
      },
        HttpStatus.UNAUTHORIZED,
      );
    }
    return this.memberShipService.aclStatusChange(id, status.permission);
  }

  // Update Acl Valuse
  @Put('updateAclRate')
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Acl values has been updated' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async updateaclValues(
    @Headers() headers: Headers,
    @Body() val : AclRatelimitUpdateDto
  ) {   
    const userId = headers['admin_userid'];
    if (!userId) {
      new HttpException({
        name: 'USERID_NOT_FOUND',
        message: 'Token is invalid',
      },
        HttpStatus.UNAUTHORIZED,
      );
    } 
    return this.memberShipService.updateAclValues(val.rateLimit);
  }

  // set cobroke status

  // Set CoBroke status (0=blocked, 1=active)
  @Put('coBroke-status/:id')
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Set co broke status' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async setCoBrokeStatus(
    @Headers() headers: Headers,
    @Param('id') id: string,
    @Body() defaultVal: DefaultUserStatusDto,
  ) {
    const adminId = headers['admin_userid'];
    if (!adminId) {
      new HttpException({
        name: "ADMINID_NOT_FOUND",
        message: 'Token is invalid'
      }, HttpStatus.UNAUTHORIZED);
    }
    return this.adminService.setCoBrokeStatus(id, defaultVal, adminId);
  }

  // list all contact us
  @Get('/allContactusEnquires')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: AllContactusResDto,
  })
  @ApiOperation({
    summary:
      '1. To search specific contact us enquires by name,email, try "http://localhost:3001/admin/allContactus?name=roy&email=" API in postman',
  })
  async getAllContactUs(
    @Headers() headers: Headers,
    @Req() req: Request,
) {
    const userId = headers['admin_userid'];
    if (!userId) {
      new HttpException({
        name: "ADMINID_NOT_FOUND",
        message: 'Token is invalid'
      }, HttpStatus.UNAUTHORIZED);
    }
    return await this.adminService.showcontactUsBasedOnsrch('contactTbl', req);
  }

  // get contact us by id

  @Get('contactUsEnquiry/:id')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: ContactSet,
  })
  @ApiOkResponse({ description: 'Fetched user profile' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async getSingleContactUS(@Param('id') id: number) {
    return this.adminService.getSingleContactUS(id);
  }

 // export csv
 @Post('exportToCSV')
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'export to csv' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async exportUserCSV(@Body() defaultVal: ExportItem, @Res() resp) {
    return this.userService.exportUserCSV(defaultVal, resp);
  }

  // list all admin 
  // List all user based on filter
  @Get('/allAdmin')
  @ApiBearerAuth()
  // @ApiCreatedResponse({
  //   type: AdminAllUserListResDto,
  // })
  @ApiOperation({
    summary:
      '1. To search specific admin by full_name,email, try "http://localhost:3001/allAdmin?name=cha&email=chaithra&status=1&phone=23424" API',
  })
  async getAllAdmin(@Req() req: Request,
  ) {
    // const userctrl = new UserController(new UserService(new ConfigService()));
    return await this.adminService.showAdminBasedOnsrch('admin', req);
  }

  // get single admin details
  @Get('getAdmin/:id')
  @ApiBearerAuth()
  // @ApiCreatedResponse({
  //   type: AdminUserListResDto,
  // })
  @ApiOkResponse({ description: 'Fetched user profile' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async getSingleAdmin(@Param('id') id: string) {
    return this.adminService.getSingleAdminInfo(id);
  }

  // update single admin details based on id
  @Put('upadteAdmin/:id')
  @ApiBearerAuth()
  // @ApiCreatedResponse({
  //   type: UpdateCampaignResDto,
  // })
  @ApiOkResponse({ description: 'Updated admin details' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async updateAdmin(
    @Headers() headers: Headers,
    @Param('id') id: string,
    @Body() defaultVal: DefaultUpdateAdminDto,
  ) {
    const userId = headers['admin_userid'];
    if (!userId) {
      new HttpException({
        name: "ADMINID_NOT_FOUND",
        message: 'Token is invalid'
      }, HttpStatus.UNAUTHORIZED);
    }
    return this.adminService.updateAdminInfo(id, defaultVal, userId);
  }

  // delete admin
  // update campaign details
  @Delete('deleteAdmin/:id')
  @ApiBearerAuth()
  // @ApiCreatedResponse({
  //   type: DeleteCampaignResDto,
  // })
  @ApiOkResponse({ description: 'deleted admin details' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async deleteAdmin(
    @Headers() headers: Headers,
    @Param('id') id: string
  ) {
    const userId = headers['admin_userid'];
    if (!userId) {
      new HttpException({
        name: "ADMINID_NOT_FOUND",
        message: 'Token is invalid'
      }, HttpStatus.UNAUTHORIZED);
    }
    return this.adminService.deleteAdmin(id);
  }

// 
@Get('getACLModule/:id')
  @ApiBearerAuth()
  // @ApiCreatedResponse({
  //   type: ContactSet,
  // })
  @ApiOkResponse({ description: 'Fetched ACL modules' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async getAllModule(@Headers() headers: Headers,
  @Param('id') id: string) {
    const userId = headers['admin_userid'];
    if (!userId) {
      new HttpException({
        name: "ADMINID_NOT_FOUND",
        message: 'Token is invalid'
      }, HttpStatus.UNAUTHORIZED);
    }
    return this.adminService.getAllModule(id);
  }

  // set admin roles in keycloak
  @Put('setAdminRole/:id')
  @ApiBearerAuth()
  // @ApiCreatedResponse({
  //   type: UpdateCampaignResDto,
  // })
  @ApiOkResponse({ description: 'Admin role has been set' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async setAdminRole(
    @Headers() headers: Headers,
    @Param('id') id: string,
    @Body() defaultVal: DefaultSetAdminRole,
  ) {
    const userId = headers['admin_userid'];
    if (!userId) {
      new HttpException({
        name: "ADMINID_NOT_FOUND",
        message: 'Token is invalid'
      }, HttpStatus.UNAUTHORIZED);
    }
    return this.adminService.setAdminRole(id, defaultVal);
  }

  // get admin profile
  // set admin roles in keycloak
  @Get('getMyProfile')
  @ApiBearerAuth()
  // @ApiCreatedResponse({
  //   type: UpdateCampaignResDto,
  // })
  @ApiOkResponse({ description: 'Admin role has been set' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async getAdminProfile(@Req() req: Request,
    @Headers() headers: Headers
  ) {
    try {
      debug('headers : ', req.headers);
      const userId = headers['admin_userid'];
      if (!userId) {
        new HttpException({
          name: "ADMINID_NOT_FOUND",
          message: 'Token is invalid'
        }, HttpStatus.UNAUTHORIZED);
      }
        const profileRes = await this.adminService.getAdminProfile(userId);
        if(profileRes.status===1) {
          delete profileRes.data['password'];
          delete profileRes.data['otp'];
          delete profileRes.data['token'];
          delete profileRes.data['agreed_term'];
          delete profileRes.data['verification_code'];
          delete profileRes.data['is_verified'];
          delete profileRes.data['resendcode_email_count'];
          delete profileRes.data['resendcode_sms_count'];
          return profileRes.data;
        }
      
    } catch(err) {
      if(err && err.response && err.response.status>=400 && err.response.status<500) {
        throw new HttpException(err.response.data,err.response.status);
      } else {
        console.error('get profile  err : ', err);
        throw err;
      }
    }
  }

  // admin update photo
  @Post('imageUpdate')
  @ApiBearerAuth()
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        files: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @UseInterceptors(FilesInterceptor('files'))
  uploadphoto(@UploadedFiles() files: any, @Headers() headers: Headers) {
    //
    try {
      const userId = headers['admin_userid'];
      //  debug("Entering Register controller...");
      if (!userId) {
        new HttpException({
          name: "ADMINID_NOT_FOUND",
          message: 'Token is invalid'
        }, HttpStatus.UNAUTHORIZED);
      }
      debug('files:', files);
      let attachments = [];
      for (const file of files) {
        const attachment = {
          filename: file.originalname,
          content: file.buffer.toString('base64'),
          type: file.mimetype,
        };
        attachments.push(attachment);
      }
      debug('attachments : ', attachments);
      return this.adminService.uploadphoto(userId, attachments);
    } catch(err) {
      if(err && err.response && err.response.status>=400 && err.response.status<500) {
        throw new HttpException(err.response.data,err.response.status);
      } else {
        console.error('get profile  err : ', err);
        throw err;
      }
    }
  }

  // update admin profile detais after self login
  @Put('updtaeMyProfile')
  @ApiBearerAuth()
  // @ApiCreatedResponse({
  //   type: UpdateCampaignResDto,
  // })
  @ApiOkResponse({ description: 'Updated admin details' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async updtaeMyProfile(
    @Headers() headers: Headers,
    @Body() defaultVal: DefaultUpdateMyProfileDto,
  ) {
    const userId = headers['admin_userid'];
    if (!userId) {
      new HttpException({
        name: "ADMINID_NOT_FOUND",
        message: 'Token is invalid'
      }, HttpStatus.UNAUTHORIZED);
    }
    return this.adminService.updtaeMyProfile(userId, defaultVal);
  }


  // add push notification
  @Post('/addPushNotify')
  @ApiBearerAuth()
  @ApiCreatedResponse({
    type: AddPushNotifyResDto,
  })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async addPushNotification(
    @Headers() headers: Headers,
    @Body() content: PushNotifyDto) {
    const userId = headers['admin_userid'];
    if (!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message: 'Token is invalid'
      }, HttpStatus.UNAUTHORIZED);
    }
    console.log('Entering push notification...');
    console.log('push notify inputs: ', content);
    return this.adminService.addPushNotification(content, userId);
  }

  // Run cron job manually to send push notification which are in past date and sent_status=0
  @Post('/runCronToSendPushNotification')
  @ApiBearerAuth()
  @ApiForbiddenResponse({ description: 'forbidden' })
  async runCron(
    @Headers() headers: Headers) {
    const userId = headers['admin_userid'];
    if (!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message: 'Token is invalid'
      }, HttpStatus.UNAUTHORIZED);
    }
    console.log('Entering manual cron job run for push notification...');
    return this.adminService.runCron();
  }

}

