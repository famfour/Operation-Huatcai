import {ApiForbiddenResponse, ApiNotFoundResponse, ApiOkResponse, ApiTags} from "@nestjs/swagger";
import {Body, Controller, Get, Param, Post} from "@nestjs/common";
import {UserService} from "../user/user.service";
import Debug from 'debug';
import {RedisDto} from "../user/dto/user.dto";
const debug = Debug('user-service:RedisController');

@ApiTags('Redis')
@Controller('redis')
export class RedisController {
    constructor(private readonly userService: UserService) {
    }

    // send verification code to specific user email id
    @Get('getRedisVal/:redisKey')
    // @Unprotected()
    @ApiOkResponse({
        description: 'To extract the redis value for the redis key from the server',
    })
    @ApiNotFoundResponse({ description: 'not found' })
    @ApiForbiddenResponse({ description: 'forbidden' })
    async getRedisVal(@Param('redisKey') redisKey: string) {
        //  debug("Entering Register controller...");
        debug('getRedisVal redisKey : ', redisKey);
        return this.userService.getRedisVal(redisKey);
    }

    @Post('setRedisVal')
    // @Unprotected()
    @ApiOkResponse({
        description: 'To extract the redis value for the redis key from the server',
    })
    @ApiNotFoundResponse({ description: 'not found' })
    @ApiForbiddenResponse({ description: 'forbidden' })
    async setRedisVal(@Body() redisObj: RedisDto) {
        //  debug("Entering Register controller...");
        debug('setRedisVal redisObj : ', redisObj);
        return this.userService.setRedisVal(redisObj);
    }
}