import { HttpService } from '@nestjs/axios';
import { Test, TestingModule } from '@nestjs/testing';
import { response } from 'express';
import { ImageController } from './image.controller';
import { ImageService } from './image.service';
import { ConfigService } from '../../config/config.service';
import { ConfigModule } from '../../config/config.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserService } from '../user/user.service';
describe('ImageController', () => {
  let controller: ImageController;
  const imageService = new UserService(new ConfigService(), new HttpService())
  beforeAll(async () => {

    const module: TestingModule = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRootAsync({
          imports: [ConfigModule],
          useFactory: (config: ConfigService) => config.getTypeORMConfig(),
          inject: [ConfigService],
        }),
      ],
      controllers: [ImageController],
      providers: [ImageService],
    })
      .overrideProvider(ImageService)
      .useValue(imageService)
      .compile();

    controller = module.get<ImageController>(ImageController);
  });


  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
  it('fileupload', async () => {
    console.log('Update address testing...');
    const dto = {
      photo: 'https://iqrate-images.s3.amazonaws.com/1643274957432%20-%20test.jpg',
    };
    const output = await controller.create('1', dto, response);

    console.log("Output", output);
    // const obj = JSON.stringify(output);
    // const result = JSON.parse(obj);
    //console.log(result);

    // expect(output).toEqual(dto.photo);

  });
});
