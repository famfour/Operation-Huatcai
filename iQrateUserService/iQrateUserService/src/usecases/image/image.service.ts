import { HttpException, HttpStatus, Injectable, Logger, Req, Res } from '@nestjs/common';
import * as AWS from 'aws-sdk';
import { ConfigService } from '../../config/config.service';
import * as multer from 'multer';
import * as multerS3 from 'multer-s3';
import { getConnection } from 'typeorm';
import { UserEntity } from '../../entities/users';
import { UserService } from '../user/user.service';
import { UserDto } from '../user/dto/user.dto';


// 

@Injectable()
export class ImageService {
  userRepo: any;
  constructor(private config: ConfigService, private usersService: UserService) { }
  AWS_S3_BUCKET = this.config.envConfig.AWS_S3_BUCKET_NAME;
  s3 = new AWS.S3
    ({
      accessKeyId: this.config.envConfig.AWS_ACCESS_KEY_ID,
      secretAccessKey: this.config.envConfig.AWS_SECRET_ACCESS_KEY,
    });
  public async fileupload(id, @Req() req, @Res() res): Promise<UserDto> {
    const userRepo = getConnection().getRepository(UserEntity);
    console.log("ID", id);

    try {
      //const user = await this.usersService.findById(id);
      const userExist = await UserEntity.findOne({ id });
      if (!userExist) {
        //debug('Record doesnot exist');
        // return 'Record doesnot exist';
        throw new HttpException({
          name: 'recordNotFound',
          message: 'Record doesnot exist'
        }, HttpStatus.BAD_REQUEST);
      }
      this.upload(req, res, function (error) {
        if (error) {
          console.log(error);
          return res.status(404).json(`Failed to upload image file: ${error}`);
        }
        userExist.photo = req.files[0].location;
        //console.log(user);

        userRepo.save(userExist);
        return res.status(201).json(req.files[0].location);
      });
    } catch (error) {
      //console.log(error);
      return res.status(500).json(`Failed to upload image file: ${error}`);
      throw new HttpException(error, HttpStatus.BAD_REQUEST);

      //  return res.status(500);
    }
  }
  public async fileupdate(id, @Req() req, @Res() res): Promise<UserDto> {
    const userRepo = getConnection().getRepository(UserEntity);
    console.log("ID", id);

    try {
      //const user = await this.usersService.findById(id);
      const userExist = await UserEntity.findOne({ id });
      if (!userExist) {
        //debug('Record doesnot exist');
        // return 'Record doesnot exist';
        throw new HttpException({
          name: 'recordNotFound',
          message: 'Record doesnot exist'
        }, HttpStatus.BAD_REQUEST);
      }
      this.upload(req, res, async function (error) {
        if (error) {
          console.log(error);
          return res.status(404).json(`Failed to upload image file: ${error}`);
        }
        userExist.photo = req.files[0].location;
        //console.log(user);
        await UserEntity.update(
          { id },
          {
            photo: userExist.photo,
            updated_at: new Date(),
          },
        );

        // userRepo.update(userExist);
        return res.status(201).json(req.files[0].location);
      });
    } catch (error) {
      //console.log(error);
      return res.status(500).json(`Failed to upload image file: ${error}`);
      throw new HttpException(error, HttpStatus.BAD_REQUEST);

      //  return res.status(500);
    }
  }

  upload = multer({
    storage: multerS3({
      s3: this.s3,
      bucket: this.AWS_S3_BUCKET,
      acl: 'public-read',
      key: function (request, file, cb) {
        cb(null, `${Date.now().toString()} - ${file.originalname}`);
      },
    }),
  }).array('upload', 1);
  public async deletePhoto(id: string) {
    const userRepo = getConnection().getRepository(UserEntity);
    try {
      console.log('Delete Photo');

      const userExist = await userRepo.findOne({ id });
      if (!userExist) {
        console.log('Record doesnot exist');
        return 'Record doesnot exist';
      }

      await userRepo.update(
        { id },
        {
          photo: '',
          updated_at: new Date(),
        },
      );
      return {
        resultcode: 200,
        data: await userRepo.findOne({ id }),
      };
    } catch (err) {
      console.error('delete photo detail Err:', err);
      throw err;
    }
  }
}

