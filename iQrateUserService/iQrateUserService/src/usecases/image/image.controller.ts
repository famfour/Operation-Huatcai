import { Controller, Headers, HttpException, HttpStatus, Param, Patch, Post, Put, Req, Res, UseGuards } from '@nestjs/common';
import { ImageService } from './image.service';
import { AuthGuard } from '@nestjs/passport';
import {
  Public,
  Roles,
  Unprotected,
} from 'nest-keycloak-connect';
import {
  ApiCreatedResponse,
  ApiExcludeEndpoint,
  ApiForbiddenResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
@Controller('user/image')
export class ImageController {
  constructor(private readonly imageUploadService: ImageService) { }

  @Post("")
  @ApiOkResponse({ description: 'Upload Photo' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async create(@Headers() headers: Headers, @Req() request, @Res() response) {
    try {
      const userId = headers['userid'];
      if (!userId) {
        new HttpException({
          name: "USERID_NOT_FOUND",
          message: 'Token is invalid'
        }, HttpStatus.UNAUTHORIZED);
      }
      await this.imageUploadService.fileupload(userId, request, response);
    } catch (error) {
      return response
        .status(500)
        .json(`Failed to upload image file: ${error.message}`);
    }
  }
  @Put('update')
  @ApiOkResponse({ description: 'User Photo has been updated' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async updatePhoto(@Headers() headers: Headers, @Req() request, @Res() response) {
    try {
      const userId = headers['userid'];
      //  debug("Entering Register controller...");
      if (!userId) {
        new HttpException({
          name: "USERID_NOT_FOUND",
          message: 'Token is invalid'
        }, HttpStatus.UNAUTHORIZED);
      }
      await this.imageUploadService.fileupdate(userId, request, response);
    }
    catch (error) {
      return response
        .status(500)
        .json(`Failed to upload image file: ${error.message}`);
    }
  }
  @Patch('deletePhoto')
  @ApiOkResponse({ description: 'Delete Photo' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async deletePhoto(@Headers() headers: Headers) {
    const userId = headers['userid'];
    if (!userId) {
      new HttpException({
        name: "USERID_NOT_FOUND",
        message: 'Token is invalid'
      }, HttpStatus.UNAUTHORIZED);
    }
    return this.imageUploadService.deletePhoto(userId);
  }
}