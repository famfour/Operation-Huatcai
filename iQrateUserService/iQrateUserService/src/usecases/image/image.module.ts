import { HttpModule, Module } from '@nestjs/common';
import { ImageController } from './image.controller';
import { ImageService } from './image.service';
import { UserService } from '../user/user.service';
import { AdminService } from '../admin/admin.service';
@Module({
  imports: [HttpModule],
  controllers: [ImageController],
  providers: [ImageService, UserService, AdminService],
  exports: [ImageService],
})
export class ImageModule { }
