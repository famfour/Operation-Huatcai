import {
  HttpException,
  HttpStatus,
  Injectable,
  NestMiddleware,
} from '@nestjs/common';
import { JWT } from '../usecases/utils/jwt';
import {Request, Response, NextFunction} from 'express';
import { ConfigService } from '../config/config.service';

import { KcTokenData } from './KcTokenData.dto';

import { AdminAclService } from './acl_module/admin_acl_service';

const debug = require('debug')('connector:AuthMiddleware');

@Injectable()
export class AdminAuth implements NestMiddleware {
  private config;
  constructor() {
    this.config = new ConfigService();
  }

  async use(req: Request, res: Response, next: NextFunction) {
    if (req.headers.autherization && !req.headers.authorization) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      req.headers.authorization = req.headers.autherization;
    }

    if(req.headers.strapiauth) {
      req.headers.authorization = req.headers.strapiauth.toString();
    }

    if (req.headers.authorization) {
      try {

        const kcToken = req.headers.authorization.split('Bearer ')[1];

        const verifiedDecodedTokenData = await this.verifyAndDecode(
            kcToken,
        );
        debug('verifiedDecodedTokenData : ', verifiedDecodedTokenData);

        req.headers.admin_userid = verifiedDecodedTokenData.sub;
        req.headers.usertype = verifiedDecodedTokenData.azp;
        debug('req.headers : ', req.headers);
        debug(' realm_access : ', verifiedDecodedTokenData.realm_access.roles);
        return next();
      } catch (err) {
        console.error(err);
          if (err.expiredAt) {
          throw new HttpException(
              {
                status: HttpStatus.UNAUTHORIZED,
                name:'TOKEN_EXPIRED',
                // message: `jwt token has expired at ${err.expiredAt}`,
                message: `Token has been expired`,
                errorcode: 'AuthMiddleware_100',
              },
              HttpStatus.UNAUTHORIZED,
          );
        } else if(err.status>=400 && err.status<500) {
              throw err;
        }
        throw new HttpException(
          {
            status: HttpStatus.UNAUTHORIZED,
            message: 'Unauthorized Access',
            errorcode: 'AuthMiddleware_100',
          },
          HttpStatus.UNAUTHORIZED,
        );
      }
    } else {
      throw new HttpException(
        {
          status: HttpStatus.UNAUTHORIZED,
          message: 'Unauthorized Access',
          errorcode: 'AuthMiddleware_TOKEN_MISSING',
        },
        HttpStatus.UNAUTHORIZED,
      );
    }

    // next();
  }

  async verifyAndDecode(kcToken: string) {
    const publicKeyB64 = this.config.envConfig.KC_ADMIN_JWT_PUBLIC_KEY_BASE64;
    return JWT.decodeAndVerify<KcTokenData>(publicKeyB64, kcToken);
  }
}
