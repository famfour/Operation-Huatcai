import { ApiProperty } from "@nestjs/swagger";
import { IsAlphanumeric, IsEnum, IsInt, IsNotEmpty, IsNumber, Length } from "class-validator";
import { HttpException, HttpStatus } from "@nestjs/common";
import { UserEntity } from '../../entities/users';
import { MembershipAcl } from '../../entities/membership_acl';
import { Brackets, getConnection } from "typeorm";
import { AgentLeadsSummary } from "../../entities/agent_lead_summary";

import { ConfigService, GlobalService } from "../../config/config.service";
import e from "express";

const debug = require('debug')('membership_acl:MemberShipAclService');

export class MemberShipAclService {



    static async verify_proxy_acl(membershipacl: MembershipAclDto) {
        const config = new ConfigService();
        debug('membershipacl :', JSON.stringify(membershipacl));

        const api_endpoint_arr = membershipacl.api_endpoint.split('?');
        membershipacl.api_endpoint = api_endpoint_arr.length > 0 ? api_endpoint_arr[0] : membershipacl.api_endpoint;


        // handle potential trailing slash
        const parts = membershipacl.api_endpoint.split('/');
        console.log("url length", parts.length);
       
        // if url has subscription and length has more than 3 slash
        const find = 'subscription';
        const found = membershipacl.api_endpoint.match(find);

        if(found !== null && found[0] === find){
            debug('url has subscription');
            // based on url length
            if (parts.length > 4) {
                const lastPart = parts.pop();
                membershipacl.api_endpoint = membershipacl.api_endpoint.replace(lastPart, '');
            } 
        } else {
        debug('url has doesnot have subscription');
        }

        // if url has submission/email-bank and length has more than 7 slash
        const findsub = '/customer/submission';
        const foundsub = membershipacl.api_endpoint.match(findsub);
        debug('findsub:', findsub);
        debug('foundsub:', foundsub);
        if(foundsub !== null && foundsub[0] === findsub){
            debug('url has /customer/submission');
            debug('parts.length:', parts.length);
            // based on url length
            if(parts.length > 5) {
                debug('length is more 5')
                parts.length -= 2;
                debug('All parts:', parts);
                membershipacl.api_endpoint = parts.join("/");
                debug('all endpooint:', membershipacl.api_endpoint);
            } 
        } else {
        debug('url doesnot has customer/submission');
        }

        // if url has /customer/heatmap/calendar and length has more than 7 slash
        const findcal = '/customer/heatmap/calendar';
        const foundcal = membershipacl.api_endpoint.match(findcal);
        debug('findcal:', findcal);
        debug('foundcal:', foundcal);
        if(foundcal !== null && foundcal[0] === findcal){
            debug('url has /customer/heatmap/calendar');
            debug('parts.length:', parts.length);
            // based on url length
            if(parts.length > 5) {
                debug('length is more 5')
                parts.length -= 2;
                debug('All parts:', parts);
                membershipacl.api_endpoint = parts.join("/");
                debug('all endpooint:', membershipacl.api_endpoint);
            } 
        } else {
        debug('url doesnot has /customer/heatmap/calendar');
        }

        // if url has leadpackages/delete and length has more than 8 slash
        const findlead = 'leadpackage/delete';
        const foundlead = membershipacl.api_endpoint.match(findlead);
        debug('findlead:', findlead);
        debug('foundlead:', foundlead);
        if(foundlead !== null && foundlead[0] === findlead){
            debug('url has /leadpackage/delete');
            debug('parts.length:', parts.length);
            // based on url length
            if(parts.length > 5) {
                debug('length is more 6')
                parts.length -= 2;
                debug('All parts:', parts);
                membershipacl.api_endpoint = parts.join("/");
                debug('all endpooint:', membershipacl.api_endpoint);
            } 
        } else {
        debug('url doesnot has leadpackage/delete');
        }
        //
        
        // based on numeric id
        const lastPart = parts.pop();
        if (/^\d+$/.test(lastPart)) {
            console.log('its num');
            membershipacl.api_endpoint = membershipacl.api_endpoint.replace(lastPart, '');
        } else {
            console.log('its string');
        }

        // based on customer drawer id(pattern will be:'0572fbdc-824c-4fb9-b52d-c2a88363f118')
        debug(lastPart);
        const search = 'drawer';
        const result = membershipacl.api_endpoint.match(search);

        if(result !== null && result[0] === search){
            if (/[a-zA-Z]/.test(lastPart)) {
                debug('enterd it has alpha');
                if (/[0-9]/.test(lastPart)) {
                    debug('it has number');
                    if (/[-]/.test(lastPart)) {
                        debug('it has -');
                        membershipacl.api_endpoint =  membershipacl.api_endpoint.replace(lastPart,'');
                    }
                }
            } else {
                debug('simple number');
            }
        }

        //////
        
        // // based on subscription id(pattern will be:'cus_LFHuBaMj1HJLYj')
        // debug(lastPart);
        // if (/[a-zA-Z]/.test(lastPart)) {
        //     debug('enterd it has alpha');
        //     if (/[0-9]/.test(lastPart)) {
        //         debug('it has number');
        //         if (/[_]/.test(lastPart)) {
        //             debug('it has _');
        //             membershipacl.api_endpoint =  membershipacl.api_endpoint.replace(lastPart,'');
        //         }
        //     }
        // } else {
        //     debug('simple number');
        // }

        // // based on subscription card type id(pattern will be:'cusLFHuBaMj1HJLYj')
        // debug(lastPart);
        // if (/[a-zA-Z]/.test(lastPart)) {
        //     debug('enterd it has alpha');
        //     membershipacl.api_endpoint =  membershipacl.api_endpoint.replace(lastPart,'');    
        // } else {
        //     debug('simple number or specialcharate');
        // }

        const whereFilter = {
            where: {
                "api_endpoint": membershipacl.api_endpoint,
                // "accesstype": membershipacl.accesstype,
            },
            membershiptypeWhere: {
                "membershiptype": membershipacl.membershiptype,
                "or_membershiptype": '*',
            },
            accesstypeWhere: {
                "accesstype": membershipacl.accesstype,
                "or_accesstype": '*',
            }
        };

        debug('whereFilter :', JSON.stringify(whereFilter));

        debug('membershipAcls : ', GlobalService.membershipAcls);
        // debug('adminAcls : ', GlobalService.adminAcls);

        const aclQueryBuilder: any = MembershipAcl.createQueryBuilder('membership_acl');
        aclQueryBuilder.where(whereFilter.where);

        aclQueryBuilder.andWhere(new Brackets(qb => {
            qb.where('membershiptype=:membershiptype', { membershiptype: whereFilter.membershiptypeWhere.membershiptype })
                .orWhere('membershiptype=:or_membershiptype', { or_membershiptype: whereFilter.membershiptypeWhere.or_membershiptype })
        }));

        aclQueryBuilder.andWhere(new Brackets(qb => {
            qb.where('accesstype=:accesstype', { accesstype: whereFilter.accesstypeWhere.accesstype })
                .orWhere('accesstype=:or_accesstype', { or_accesstype: whereFilter.accesstypeWhere.or_accesstype })
        }));

        debug('aclQueryBuilder SQL :', aclQueryBuilder.getSql());
        debug('aclQueryBuilder getParameter :', aclQueryBuilder.getParameters());

        // aclQueryBuilder.where('(membershiptype=:membershiptype or membershiptype=:or_membershiptype) and api_endpoint=:api_endpoint and accesstype=:accesstype',whereFilter);

        const fetch_aclcheck_object = await aclQueryBuilder.select().getOne();
        //const fetch_aclcheck_object = await MembershipAcl.findOne(whereFilter);

        // START to chk user is active or not
        debug('ACLCHK ctrl in to chk user status');
        debug('ACLCHK userid:', membershipacl.userid)
        const userExist = await UserEntity.findOne({ id: membershipacl.userid });
        debug('ACLCHK userExist:', userExist);
        debug('ACLCHK userExist.status', userExist.status);
        if (userExist.status === 0) {
            debug('ACLCHK  entered userExist.status if funct', userExist.status);
            throw new HttpException({
                name: "MEMBERSHIP_ACL_CHECK",
                message: `This user account is inactive. Please contact your system administrator `
            }, HttpStatus.FORBIDDEN);
          }
        // END

        debug('fetch_aclcheck_object:', fetch_aclcheck_object);

        if (fetch_aclcheck_object && fetch_aclcheck_object['permission'] && fetch_aclcheck_object['permission'].toUpperCase() === 'ALLOW') {

            // check rate limit
            if (fetch_aclcheck_object['ratelimit'] !== null) {
                const agent_userid = membershipacl.userid;
                debug('agent_userid :', agent_userid);
                let agentLeadsSummaryData : any = await getConnection().getRepository(AgentLeadsSummary).findOne({ where: { agent_userid } });
                debug('agentLeadsSummaryData :', JSON.stringify(agentLeadsSummaryData));
                // if undefined, set it to 0 for ratelimit check
                if(!agentLeadsSummaryData) {
                    agentLeadsSummaryData = {total_leads:0};
                }
                debug(`check ${agentLeadsSummaryData['total_leads']} < ${fetch_aclcheck_object['ratelimit']} : `, agentLeadsSummaryData['total_leads'] < fetch_aclcheck_object['ratelimit']);
                
                if (fetch_aclcheck_object['ratelimit']=='0' || agentLeadsSummaryData['total_leads'] < fetch_aclcheck_object['ratelimit']) {
                    return { access: true, fetch_aclcheck_object };
                } else {
                    var upgrade_msg="";
                    if(membershipacl.membershiptype!="premium+") {
                        upgrade_msg = "Please upgrade your membership.";
                    }
                    throw new HttpException({
                        name: "MEMBERSHIP_ACL_CHECK",
                        message: `Access denied, Your account has reached the maximum allowed limit of ${fetch_aclcheck_object['ratelimit']} ${fetch_aclcheck_object['name']} feature for your ${membershipacl.membershiptype} membership package. ${upgrade_msg} `
                    }, HttpStatus.FORBIDDEN);
                }

            } else {
                return { access: true, fetch_aclcheck_object }; //true;
            }
        } else {
            throw new HttpException({
                name: "MEMBERSHIP_ACL_CHECK",
                message: `access denied, your membership package ${membershipacl.membershiptype} do not have access to this feature. `
            }, HttpStatus.FORBIDDEN);
        }
    }

    // Get User`s Acl list
    async getAclList(membertype){
        try{
            const product = await MembershipAcl
                .createQueryBuilder('p')
                .select(['p.microservice', 'p.api_endpoint', 'p.permission', 'p.ratelimit', 'p.name', 'p.accesstype'])
                .where('p.microservice != :microservice', {
                    microservice: 'strapi',
                })
                .andWhere('p.microservice != :microservice1', {
                    microservice1: 'test',
                })
                .andWhere('p.membershiptype = :membershiptype', {
                    membershiptype: membertype,
                })
                // .andWhere('p.permission = :permission', {
                //     permission: 'ALLOW',
                // })
                .orderBy({ 'p.microservice': 'ASC' })
                .addOrderBy('p.api_endpoint', 'ASC')
                .addOrderBy('p.accesstype', 'ASC')
                .getMany();
            console.log("user acl =", product);
            
            let acl_array = [];
            product.forEach(element => {
                let single_api = {};
                single_api[element['api_endpoint']] = {};
                single_api[element['api_endpoint']]['permission']   = element['permission'];
                single_api[element['api_endpoint']]['ratelimit']    = element['ratelimit'];
                single_api[element['api_endpoint']]['accesstype']   = element['accesstype'];
                single_api[element['api_endpoint']]['microservice'] = element['microservice'];
                
                acl_array.push(single_api);
            });
            return acl_array;
            //return product;
        } catch (err) {
            if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
                throw new HttpException(err.response.data, err.response.status);
            } else {
                console.error('login err : ', err);
                throw err;
            }
        }
    }

    // Get All Acl list
    async getAllAcl() {
        try {
          //const product = await MembershipAcl.find();
          const product = await MembershipAcl
          .createQueryBuilder('p')
          .select(['p.id', 'p.microservice','p.api_endpoint', 'p.permission' ,'p.membershiptype','p.ratelimit', 'p.permision_desc', 'p.ratelimit_desc', 'p.accesstype', 'p.sub_description'])
          .where('p.microservice != :microservice', {
                microservice: 'strapi',
          })
          .andWhere('p.microservice != :microservice1', {
              microservice1: 'test',
          })
          .andWhere('p.editable != :editVal', {
            editVal: 'false',
        })
          .orderBy({'p.microservice': 'ASC'})
          .addOrderBy('p.api_endpoint', 'ASC')
          .addOrderBy('p.accesstype', 'ASC')
          .addOrderBy('p.membershiptype', 'ASC')
          .getMany();
            //debug(product);
            let api_list = [];
            let rate_limit_list = [];
            let api_array = {};
            let rate_limit_array = {};
            console.time("start");
            product.forEach(element => {
                let perm = (element.membershiptype == '*')?  'basic' : element.membershiptype;
                let temp_obj = {};
                let temp_array = [];
                temp_obj[perm] = {};
                temp_obj[perm]['permission'] = element.permission;
                temp_obj[perm]['id'] = element.id;
                if(element.microservice != 'config'){
                    if(api_list.includes(element.microservice+" - "+element.api_endpoint+" - "+element.accesstype))
                    {
                        temp_array = api_array[element.microservice + " - " + element.api_endpoint + " - " + element.accesstype];
                        temp_array.push(temp_obj);
                        api_array[element.microservice + " - " + element.api_endpoint + " - " + element.accesstype] = temp_array;
                    }else{
                        let temp_desc = {};
                        let data_type = "integer";
                        if (element.api_endpoint.indexOf('%') > 0 ) {
                            data_type = "float";
                        }
                        temp_desc['description'] = (element.permision_desc != null)? element.permision_desc : "";
                        temp_desc['sub_description'] = (element.sub_description != null) ? element.sub_description : "";
                        temp_desc['data_type'] = data_type;
                        temp_array.push(temp_desc);
                        temp_array.push(temp_obj);
                        api_list.push(element.microservice + " - " + element.api_endpoint + " - " + element.accesstype);
                        api_array[element.microservice + " - " + element.api_endpoint + " - " + element.accesstype] = temp_array;
                    }
                }
                temp_obj = {};
                temp_array = [];
                temp_obj[perm] = {};
                temp_obj[perm]['id'] = element.id;

                if (rate_limit_list.includes(element.microservice + " - " + element.api_endpoint + " - " + element.accesstype)) {
                    let limitStr = element.ratelimit;
                    //if (limitStr !== null) {
                        temp_obj[perm]['rate_limit'] = element.ratelimit;
                        temp_array = rate_limit_array[element.microservice + " - " + element.api_endpoint + " - " + element.accesstype];
                        temp_array.push(temp_obj);
                    rate_limit_array[element.microservice + " - " + element.api_endpoint + " - " + element.accesstype] = temp_array;
                    //}
                } else {
                    let limitStr = element.ratelimit;
                    if (limitStr !== null)
                    {
                        /*
                        {
                            "membershiptype": "basic",
                            "clearance_fee_percentage": 10,
                            "co-broker_fee_percentage": 1.2,
                            "self-co-broker_fee_percentage": 15.5,
                            "lead_storage": "1"
                        }
                        */
                        temp_obj[perm]['rate_limit'] = element.ratelimit;
                        let temp_desc = {};
                        let data_type = "integer";
                        if (element.api_endpoint.indexOf('%') > 0 ) {
                            data_type = "float";
                        }
                        temp_desc['description'] = (element.ratelimit_desc != null) ? element.ratelimit_desc : "";
                        temp_desc['sub_description'] = (element.sub_description != null) ? element.sub_description : "";
                        temp_desc['data_type'] = data_type;
                        temp_array.push(temp_desc);
                        temp_array.push(temp_obj);
                        rate_limit_list.push(element.microservice + " - " + element.api_endpoint + " - " + element.accesstype);
                        rate_limit_array[element.microservice + " - " + element.api_endpoint + " - " + element.accesstype] = temp_array;
                    }
                }
            });
            debug(JSON.stringify(api_array));
            console.timeEnd("start");
            return { message: 'fetched suucessfully', data: api_array, rateLimit: rate_limit_array };
        } catch (err) {
            if(err && err.response && err.response.status>=400 && err.response.status<500) {
              throw new HttpException(err.response.data,err.response.status);
            } else {
              console.error('login err : ', err);
              throw err;
            }
        }
      }

    // Update Acl status
    async aclStatusChange(id, status){

        
        debug('id length = ', id.length);
        if (id.length > 9 ) {
            throw new HttpException({
                name: 'Not an Integer!',
                message: 'Please enter valid id'
            }, HttpStatus.BAD_REQUEST);
        }

        if (/^\d+$/.test(id)) {
            const product = await MembershipAcl
                .createQueryBuilder('p')
                .select(['p.id', 'p.microservice', 'p.api_endpoint', 'p.permission', 'p.membershiptype', 'p.ratelimit'])
                .where({id})
                .getOne();

            if(!product)
            {
                debug('Record not available!');
                throw new HttpException({
                    name: 'Record not Exist',
                    message: 'Please enter valid id'
                }, HttpStatus.BAD_REQUEST);
            }
        }else{
            throw new HttpException({
                name: 'Not an Integer!',
                message: 'Please enter valid id'
            }, HttpStatus.BAD_REQUEST);
        }
        const acl = new MembershipAcl;
        acl.permission = status;
        try {
            await MembershipAcl.update({id}, acl);
            //return 'success';
            return { message: "Acl permission has been changed!" };
        } catch (err) {
            if(err && err.response && err.response.status>=400 && err.response.status<500) {
              throw new HttpException(err.response.data,err.response.status);
            } else {
              console.error('login err : ', err);
              throw err;
            }
        }
    }
   
    // Update Acl values
    async updateAclValues(val : any){
        try {
            debug(val);
            debug(typeof(val));
            
            let a = await this.checkData(val);
            debug("valid_msg = ", a);
            if(a.state == true)
            {
                for (let i = 0; i < val.length; i++) {
                    let acl = new MembershipAcl;
                    acl.ratelimit = val[i]['value'];
                    let id = val[i].id;
                    MembershipAcl.update({id}, acl);
                }
            }else{
                throw new HttpException({
                    name: 'Failed',
                    message: a.msg
                }, HttpStatus.BAD_REQUEST);
            }
            //return 'success';
            return { message: "Acl rate limit has been updated!" };
        } catch (err) {
            if(err && err.response && err.response.status>=400 && err.response.status<500) {
              throw new HttpException(err.response.data,err.response.status);
            } else {
              console.error('login err : ', err);
              throw err;
            }
        }
    }

    async checkData(val){
        let valid_states = true;
        let valid_msg = "";
        
        for(let i=0; i < val.length; i++)
        {
            let element = val[i];
            let rate = element.value;
            let rate_id = element.id;
            // if ((/^\d+$/.test(rate)) && (/^\d+$/.test(rate_id))) {
            if ((/^[0-9]\d*(\.\d+)?$/.test(rate)) && (/^\d+$/.test(rate_id))) {
 
                if (rate.toString().length > 6){
                    valid_states = false;
                    valid_msg = "Value must be less than 6 digit!";
                    return { 'state': valid_states, 'msg': valid_msg };
                }

                let id = element.id;
                let member = await MembershipAcl.find({ id });
                debug("length = ", member.length);
                if (member.length == 0) {
                    valid_states = false;
                    valid_msg = "Record not exist!";
                }else{
                    
                    if (member[0].api_endpoint.indexOf('%') > 0 && rate > 100) {
                        debug("Percentage value =", member[0].api_endpoint);
                        debug("Rate =", rate);
                        valid_states = false;
                        valid_msg = "Rate limit value maximum 100%!";
                    }
                    const result = (rate - Math.floor(rate)) !== 0; 
                  
                    if (member[0].api_endpoint.indexOf('%') === -1 && result) {
                        debug("Percentage value =", member[0].api_endpoint);
                        debug("Rate =", rate);
                        valid_states = false;
                        valid_msg = "Rate limit value must be integer!";
                    }
                }
            }else{
                valid_states = false;
                valid_msg = "Value must be number!";
            }
        }
        return {'state':valid_states, 'msg':valid_msg};
    }

}

export class MembershipAclDto {

    @ApiProperty({
        type: String,
        description: 'membershiptype',
    })
    @IsNotEmpty({ message: 'Please provide membershiptype value' })
    @Length(1, 20)
    membershiptype: string;

    @ApiProperty({
        type: String,
        description: 'api_endpoint',
    })
    @IsNotEmpty({ message: 'Please provide api_endpoint value' })
    api_endpoint: string;

    @ApiProperty({
        type: String,
        description: 'userid',
    })
    @IsNotEmpty({ message: 'Please provide userid value' })
    userid: string;

    @ApiProperty({
        type: String,
        description: 'accesstype',
    })
    @IsNotEmpty({ message: 'Please provide accesstype value' })
    accesstype: string;

}

export class AclRatelimitUpdateDto {

    @ApiProperty({
        type: Object,
        description: 'ratelimit',
        default: '[{"id":109, "value":5},{"id":110, "value":50},{"id":111, "value":500}]'
    })
    @IsNotEmpty({ message: 'Please provide ratelimit value' })
    rateLimit: Object
}

export enum ChangeTypeEnum {
    ENABLE = 'ALLOW',
    DISABLE = 'DENY'
}
export class AclPermissionUpdateDto {

    @ApiProperty({
        type: Object,
        description: 'permission',
        default: 'ALLOW'
    })
    @IsNotEmpty({ message: 'Please provide permission value' })
    //@IsEnum(ChangeTypeEnum)
    @IsEnum(['ALLOW','DENY'], { message: 'Status value is only ALLOW or DENY.'})
    permission: Object
}


