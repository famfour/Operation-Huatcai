import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import Debug from 'debug';
import { ConfigService } from '../../config/config.service';
import { ConfigModule } from '../../config/config.module';
import { AdminAclDto, AdminAclService } from './admin_acl_service';
import { async } from 'rxjs';
const debug = Debug('connector:Admin Acl :Spec');
describe('AdminAclTest', () => {
    const adminAcl = new AdminAclService();
    beforeAll(async () => {
        const app: TestingModule = await Test.createTestingModule({
            imports: [
                TypeOrmModule.forRootAsync({
                    imports: [ConfigModule],
                    useFactory: (config: ConfigService) => config.getTypeORMConfig(),
                    inject: [ConfigService],
                }),
            ],
            //   controllers: [AdminController],
            providers: [ AdminAclService],
        })
            .overrideProvider(AdminAclService)
            .useValue(adminAcl)
            .compile();
    });

    it('Check Valid User', async() =>{
        try {
            debug('TEST RESULT:1');

            // check first condition
            // const dto = {
            //     roles: ['SUPER USER','VIEW_USERS'],
            //     api_endpoint: '/customer/bank/all',
            //     userid: '15',
            //     accesstype: '*'
            // };

            const dto = {
                roles: ['VIEW_USERS'],
                api_endpoint: '/customer/bank/324324-all',
                userid: '15',
                accesstype: '*'
            };
            const testRes = await AdminAclService.verify_admin_acl(dto);
            debug('testRes:', testRes);
            expect(testRes.permission).toEqual(true);
        } catch (err) {
            debug('err:', err);
        }
    })
   
}); // Main end
