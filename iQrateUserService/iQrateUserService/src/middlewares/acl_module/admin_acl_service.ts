import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { HttpException, HttpStatus } from '@nestjs/common';
import { AdminAcl } from '../../entities/admin_acl';
import {Brackets, In} from "typeorm";
import {MembershipAcl} from "../../entities/membership_acl";
const debug = require('debug')('admin_acl:admin-acl-service');

export class AdminAclService {

    static async verify_admin_acl(adminacl: AdminAclDto) {

        debug('adminacl :', adminacl);

        // GRANT ALL for SUPER USER
        if(adminacl.roles.includes('SUPER USER')) {
            return { permission:true };
        }

        // ignore param after ?
        const api_endpoint_arr = adminacl.api_endpoint.split('?');
        adminacl.api_endpoint = api_endpoint_arr.length > 0 ? api_endpoint_arr[0] : adminacl.api_endpoint;

        /////////
        const parts = adminacl.api_endpoint.split('/');
        // const lastPart = parts.pop() || parts.pop();  // handle potential trailing slash
        const lastPart = parts.pop();

        // based on uuid
        debug(lastPart);
        if (/[a-zA-Z]/.test(lastPart)) {
            debug('enterd it has alpha');
            if (/[0-9]/.test(lastPart)) {
                debug('it has number');
                if (/[-]/.test(lastPart)) {
                    debug('it has -');
                    adminacl.api_endpoint =  adminacl.api_endpoint.replace(lastPart,'');
                }
            }
        } else {
            debug('simple number');
        }

        // if url has subscription and length has more than 3 slash
        const find = 'subscription';
        const found = adminacl.api_endpoint.match(find);

        if(found !== null && found[0] === find){
            debug('url has subscription');
            // based on url length
            debug('endpoint:', adminacl.api_endpoint);
            debug('parts.length:', parts.length);
            if (parts.length >= 4) {
                adminacl.api_endpoint = adminacl.api_endpoint.replace(lastPart, '');
            } 
        } else {
        debug('url has doesnot have subscription');
        }

        //// if url has kvstore and its not 'all' then neglect last part
        const findKv = 'kvstore';
        const foundKv = adminacl.api_endpoint.match(findKv);

        if(foundKv !== null && foundKv[0] === findKv){
            debug('url has kvstore');
            if (lastPart !== 'all') {
                adminacl.api_endpoint = adminacl.api_endpoint.replace(lastPart, '');
            } 
        } else {
        debug('url has doesnot have kvstore');
        }

        // based on email
        debug(lastPart);
        if (/[@]/.test(lastPart)) {
            debug('enterd it has @');
            adminacl.api_endpoint =  adminacl.api_endpoint.replace(lastPart,'');
        } else {
            debug('simple string');
        }
        //

        // based on numeric id
        if (/^\d+$/.test(lastPart)) {
            console.log('its num');
            adminacl.api_endpoint = adminacl.api_endpoint.replace(lastPart, '');
        } else {
            console.log('its string');
        }

        ////////

        const whereFilter ={
            where: {
                "principalid": In(adminacl.roles),
                "permission": "ALLOW"
            },
            api_endpointWhere : {
                "api_endpoint": adminacl.api_endpoint,
                "or_api_endpoint": "*"
            },
            accesstypeWhere : {
                "accesstype": adminacl.accesstype,
                "or_accesstype": "*"
            }
        };

        debug('whereFilter :', JSON.stringify(whereFilter));

        const adminAclQueryBuilder: any = AdminAcl.createQueryBuilder('admin_acl');
        adminAclQueryBuilder.where(whereFilter.where);

        adminAclQueryBuilder.andWhere(new Brackets(qb => {
            qb.where('api_endpoint=:api_endpoint',{api_endpoint:whereFilter.api_endpointWhere.api_endpoint})
                .orWhere('api_endpoint=:or_api_endpoint',{or_api_endpoint:whereFilter.api_endpointWhere.or_api_endpoint})
        }));

        adminAclQueryBuilder.andWhere(new Brackets(qb => {
            qb.where('accesstype=:accesstype',{accesstype:whereFilter.accesstypeWhere.accesstype})
                .orWhere('accesstype=:or_accesstype',{or_accesstype:whereFilter.accesstypeWhere.or_accesstype})
        }));

        debug('adminAclQueryBuilder SQL :',adminAclQueryBuilder.getSql());
        debug('adminAclQueryBuilder getParameter :',adminAclQueryBuilder.getParameters());

        const fetch_aclcheck_object = await adminAclQueryBuilder.select().getOne();
        // const fetch_aclcheck_object = await AdminAcl.findOne(whereFilter);

        debug('fetch_aclcheck_object:', JSON.stringify(fetch_aclcheck_object?fetch_aclcheck_object:{}));

        if(fetch_aclcheck_object && fetch_aclcheck_object['permission'] && fetch_aclcheck_object['permission'].toUpperCase()==='ALLOW') {
            return {permission:true,microservice:fetch_aclcheck_object.microservice};
        } else {
            throw new HttpException({
                name:"ADMIN_ACL_CHECK",
                // message:`access denied, your account do not have access to this feature. Please contact your system administrator`
                message:`Ask administrator to set specific role to access it`
            }, HttpStatus.FORBIDDEN);
        }
    }
}

export class AdminAclDto {

    @ApiProperty({
        type: [String],
        description: 'roles',
    })
    @IsNotEmpty({ message: 'Please provide roles array' })
    roles: string[];

    @ApiProperty({
        type: String,
        description: 'api_endpoint',
    })
    @IsNotEmpty({ message: 'Please provide api_endpoint value' })
    api_endpoint: string;

    @ApiProperty({
        type: String,
        description: 'userid',
    })
    @IsNotEmpty({ message: 'Please provide userid value' })
    userid: string;

    @ApiProperty({
        type: String,
        description: 'accesstype',
    })
    @IsNotEmpty({ message: 'Please provide accesstype value' })
    accesstype: string;

}