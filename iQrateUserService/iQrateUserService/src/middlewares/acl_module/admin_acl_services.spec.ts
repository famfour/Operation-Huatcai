import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import Debug from 'debug';
import { ConfigService } from '../../config/config.service';
import { ConfigModule } from '../../config/config.module';
import { MemberShipAclService } from '../../middlewares/acl_module/membership_acl_service';
const debug = Debug('connector:UserController:Spec');
describe('UserController', () => {
  const membership_acl_service = new MemberShipAclService();
  beforeAll(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRootAsync({
          imports: [ConfigModule],
          useFactory: (config: ConfigService) => config.getTypeORMConfig(),
          inject: [ConfigService],
        }),
      ],
      //   controllers: [AdminController],
      providers: [MemberShipAclService],
    })
      .overrideProvider(MemberShipAclService)
      .useValue(membership_acl_service)
      .compile();
  });
  it('Get All ACL', async () => {
    try {
      debug('TEST RESULT:1');
      const testRes = await membership_acl_service.getAllAcl();
      debug('testRes:', testRes);
      expect(testRes.message).toEqual('fetched suucessfully');
    } catch (err) {
      debug('err:', err);
    }
  });
  it('Update Acl status', async () => {
    try {

      debug('TEST RESULT:2');
      const testRes = await membership_acl_service.aclStatusChange(4, "DENY");
      debug(testRes);
      expect(testRes).toBe('success');
    } catch (err) {
      debug('err:', err);
    }
  });
  it('Update Acl Values', async () => {
    try {
      const dto = [{ "id": 109, "value": 5 }, { "id": 110, "value": 50 }, { "id": 111, "value": 500 }];
      debug('TEST RESULT:3');
      const testRes = await membership_acl_service.updateAclValues(dto);
      debug(testRes);
      expect(testRes).toBe('success');
    } catch (err) {
      debug('err:', err);
    }
  });
}); // Main end
