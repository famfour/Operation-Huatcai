import {
  Injectable,
  NestMiddleware,
  Inject,
  LoggerService,
} from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { WINSTON_MODULE_NEST_PROVIDER } from 'nest-winston';
@Injectable()
export class HTTPLogger implements NestMiddleware {
  constructor(
      @Inject(WINSTON_MODULE_NEST_PROVIDER)
      private readonly logger: LoggerService,
  ) {}
  use(request: Request, response: Response, next: NextFunction) {
    const { method, baseUrl, body } = request;
    const reqBody = Object.keys(body).length
        ? `[body]: ${JSON.stringify(body)}`
        : '';
    // log request
    this.logger.log(`${method} ${baseUrl} request ${reqBody}`);
    // intercept response writing to write to chunks as well.
    const chunks = [];
    const oldWrite = response.write;
    const oldEnd = response.end;
    response.write = function pushChunk(chunk) {
      chunks.push(chunk);
      // eslint-disable-next-line prefer-rest-params
      return oldWrite.apply(response, arguments);
    };
    response.end = function pushChunk(chunk) {
      if (chunk) chunks.push(chunk);
      // eslint-disable-next-line prefer-rest-params
      oldEnd.apply(response, arguments);
    } as any;
    // log response
    response.on('close', () => {
      const { statusCode } = response;
      try {
        const respBody = `[body]: ${Buffer.concat(chunks).toString('utf8')}`;
        this.logger.log(
            `${method} ${baseUrl} ${statusCode} response ${respBody}`,
            'HTTP',
        );
        // eslint-disable-next-line no-empty
      } catch (error) {}
    });
    return next();
  }
}