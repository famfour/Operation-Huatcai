import {
    HttpException,
    HttpStatus,
    Injectable,
    NestMiddleware,
  } from '@nestjs/common';

import * as express from 'express';


  import { JWT } from '../usecases/utils/jwt';
  import { Request, Response, NextFunction } from 'express';
  import { ConfigService } from '../config/config.service';
  import { MemberShipAclService } from './acl_module/membership_acl_service';
  
  import { KcTokenData } from './KcTokenData.dto';
  import * as moment from 'moment';

  const debug = require('debug')('membership_acl:AuthProxy');
  
  @Injectable()
  export class AuthProxy implements NestMiddleware {
    private config;
    private proxyApp = express();

    constructor() {
      this.config = new ConfigService();
    }

     getBaseUrlByMicroService(microservice:string) {
        debug('microservice : ', microservice);
        switch (microservice) {
            case 'customer':
                return this.config.envConfig.CUSTOMER_SERVICE_BASEURL ||  'http://customer-service.local';
                break;
            case 'strapi':
                return this.config.envConfig.STRAPI_SERVICE_BASEURL ||  'http://strapi.local';
                break;
            case 'TEST':
                return this.config.envConfig.TEST_SERVICE_BASEURL ||  'https://reqres.in';
                break;
            case 'notification':
                return this.config.envConfig.TEST_NOTIFICATION_BASEURL ||  'http://localhost:3003';
                break;
            default:
                return undefined;
                break;
        }
    }
  
    async use(req: Request, res: Response, next: NextFunction) {

        const startTimeStamp = (new Date()).getTime();

        if (!req.headers.authorization) {
            throw new HttpException(
                {
                    status: HttpStatus.UNAUTHORIZED,
                    message: 'Unauthorized Access',
                    errorcode: 'AuthMiddleware_TOKEN_MISSING',
                },
                HttpStatus.UNAUTHORIZED,
            );
        }

        let request = require('request');

        try {
          const kcToken = req.headers.authorization.split('Bearer ')[1];

          const verifiedDecodedTokenData = await this.verifyAndDecode(
              kcToken,
          );
          debug('verifiedDecodedTokenData : ', verifiedDecodedTokenData);

          req.headers.userid = verifiedDecodedTokenData.sub;
          req.headers.membershiptype = verifiedDecodedTokenData.membershiptype;
          req.headers.usertype = verifiedDecodedTokenData.azp;
          debug('req.headers : ', req.headers);

          const aclcheck = {
              membershiptype: verifiedDecodedTokenData.membershiptype,
              api_endpoint: req.originalUrl,
              userid: verifiedDecodedTokenData.sub,
              accesstype: req.method
          };

          const acl_verification_flag = await MemberShipAclService.verify_proxy_acl(aclcheck);
          if(acl_verification_flag.access === true) {
              try {
                  if(acl_verification_flag.fetch_aclcheck_object['isheaderauthdelete']) {
                      delete req.headers.authorization;
                  }
                  if(acl_verification_flag.fetch_aclcheck_object['isbaserootpathdelete']) {
                      req.originalUrl = req.originalUrl.replace('/'+req.originalUrl.split('/')[1],'');
                  }
                  if(acl_verification_flag.fetch_aclcheck_object['url_param'] && acl_verification_flag.fetch_aclcheck_object['url_param']!==null) {
                      let url_param = acl_verification_flag.fetch_aclcheck_object['url_param'];
                      for(var tokenPropertyKey in verifiedDecodedTokenData){
                          debug(tokenPropertyKey + ': ', verifiedDecodedTokenData[tokenPropertyKey]);
                          url_param = url_param.replace('{{'+tokenPropertyKey+'}}',verifiedDecodedTokenData[tokenPropertyKey]);
                      }
                      req.originalUrl = req.originalUrl + url_param;
                  }
                  debug('acl_verification_flag : ', acl_verification_flag);
                  var newurl = this.getBaseUrlByMicroService(acl_verification_flag.fetch_aclcheck_object['microservice']);
                  var service_url = newurl + req.originalUrl;
                  debug('req.url : ', req.url);
                  debug('req.body : ', req.body);
                  debug('req.headers : ', req.headers);
                  debug('service_url : ', service_url);
                  debug('req.method.toLowerCase() : ', req.method.toLowerCase());
                  debug(`Time consumed for acl : ${((new Date()).getTime()-startTimeStamp)/1000} secs`);
              } catch (e) {
                  console.error('req.pipe :', e);
                  throw new HttpException(
                      {
                          status: HttpStatus.FORBIDDEN,
                          name:'TARGET_SVC_NOT_AVAILABLE',
                          message: `Target service ${acl_verification_flag.fetch_aclcheck_object['microservice']} is not available`,
                          errorcode: 'AuthMiddleware_100',
                      },
                      HttpStatus.UNAUTHORIZED,
                  );
              }
          } else {
              // throw 'No response';

              throw new HttpException(
                  {
                      status: HttpStatus.BAD_GATEWAY,
                      name:'TARGET_SVC_NOT_AVAILABLE',
                      message: `Target service ${acl_verification_flag.fetch_aclcheck_object['microservice']} is not available`,
                      errorcode: 'AuthMiddleware_100',
                  },
                  HttpStatus.BAD_GATEWAY,
              );

          }
        } catch (err) {
          console.error(err);
          if(err.status>=400 && err.status<500) {
              throw err;
          } else if (err.expiredAt) {
              throw new HttpException(
                  {
                      status: HttpStatus.UNAUTHORIZED,
                      name:'TOKEN_EXPIRED',
                    //   message: `jwt token has expired at ${moment(err.expiredAt).format('YYYY-MM-DD HH:mm:ss')}`,
                    message: `Token has been expired`,
                      errorcode: 'TOKEN_EXPIRED',
                  },
                  HttpStatus.UNAUTHORIZED,
              );
          }
          throw new HttpException(
            {
              status: HttpStatus.UNAUTHORIZED,
              message: 'Unauthorized Access',
              errorcode: 'AuthMiddleware_100',
            },
            HttpStatus.UNAUTHORIZED,
          );
        }
    }
  
    async verifyAndDecode(kcToken: string) {
      const publicKeyB64 = this.config.envConfig.KC_JWT_PUBLIC_KEY_BASE64;
      return JWT.decodeAndVerify<KcTokenData>(publicKeyB64, kcToken);
    }
  }

// process.on('uncaughtException',  (err)=> {
//     console.error(err.stack);
//     console.log("Node NOT Exiting...");
// });
  