import { HttpModule, MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { WinstonModule } from 'nest-winston';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigService } from './config/config.service';
import { Auth } from './middlewares/auth';

import { ConfigModule } from './config/config.module';
import { UserModule } from './usecases/user/user.module';
import { EnumModule } from './usecases/enum/enum.module';
import { LoginModule } from './usecases/login/login.module';
import { AddressModule } from './usecases/address/address.module';
import { StatusModule } from './usecases/status/status.module';
import { ImageModule } from './usecases/image/image.module';
import { AdminModule } from './usecases/admin/admin.module';
import { SubscriptionModule } from './usecases/subscription/subscription.module';
import { AdminAuth } from './middlewares/auth_admin';
import { CalculatorModule } from './usecases/calculator/calculator.module';


@Module({
  imports: [
    EventEmitterModule.forRoot(),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (config: ConfigService) => config.getTypeORMConfig(),
      inject: [ConfigService],
    }),
    ConfigModule,
    WinstonModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (config: ConfigService) => config.getLoggerConfig(),
      inject: [ConfigService],
    }),
    UserModule,
    EnumModule,
    LoginModule,
    AddressModule,
    HttpModule,
    StatusModule,
    ImageModule,
    AdminModule,
    SubscriptionModule,
    CalculatorModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  async configure(consumer: MiddlewareConsumer) {
    // consumer.apply(HTTPLogger).forRoutes('*');
    consumer.apply(Auth).forRoutes('/user');

    consumer.apply(AdminAuth).forRoutes('/admin');
    // consumer.apply(AuthProxy).forRoutes('/customer');
    // consumer.apply(AuthProxy).forRoutes('/notify');
    // consumer.apply(AuthProxy).forRoutes('/api');
    // consumer.apply(AuthProxy).forRoutes('/strapi');
    // consumer.apply(Auth).forRoutes('/tnc');
  }
}
