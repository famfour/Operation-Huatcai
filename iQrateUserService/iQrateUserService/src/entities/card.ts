import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { UserEntity } from './users';

@Entity('card', { schema: 'public' })
export class CardEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  // id: string;
  id: number;
  // @PrimaryGeneratedColumn({ type: 'bigint' })
  // id: string;
  // id: number;
  // id: bigint;

  @Column({ type: 'varchar', nullable: true })
  card_type: string;


  @Column('text', { name: 'logo', nullable: true })
  logo: string;

}
