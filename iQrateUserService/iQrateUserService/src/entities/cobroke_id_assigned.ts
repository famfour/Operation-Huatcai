import { Exclude } from 'class-transformer';
import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryColumn,
} from 'typeorm';

@Entity('assigned_cobrokeid', { schema: 'public' })
export class AssignedCobrokeEntity extends BaseEntity {
  @PrimaryColumn('text', { name: 'id' })
  id: number;

  @Column('text', { name: 'assigned_user_id', nullable: true })
  assigned_user_id: string;

  @Column({ default: false })
  public last_assigned_user_id: boolean;

  @Column('timestamp', { name: 'assigned_at', nullable: true })
  assigned_at: Date;

  @Column('text', { name: 'created_user_id', nullable: true })
  created_user_id: string;

  @Column('text', { name: 'updated_user_id', nullable: true })
  updated_user_id: string;

  @Column('timestamp', { name: 'created_at', nullable: true })
  created_at: Date;

  @Column('timestamp', { name: 'updated_at', nullable: true })
  updated_at: Date;

}
