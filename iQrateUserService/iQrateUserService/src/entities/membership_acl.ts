import {
    BaseEntity,
    Entity,
    PrimaryGeneratedColumn,
    Column, CreateDateColumn,UpdateDateColumn
} from 'typeorm';

@Entity('membership_acl', { schema: 'public' })
export class MembershipAcl extends BaseEntity {
    @PrimaryGeneratedColumn( { name: 'id' })
        // id: string;
    id: number;

    @Column( { length:50, name: 'name',nullable:false })
    name: string;

    @Column( { length:20, name: 'membershiptype',nullable:false })
    membershiptype: string;

    @Column( { length:50, name: 'microservice',nullable:false })
    microservice: string;

    @Column( { type:'text',  name: 'api_endpoint',nullable:false })
    api_endpoint: string;

    @Column( { type:'text',  name: 'accesstype',nullable:false })
    accesstype: string;

    @Column( { type:'text',  name: 'permission',nullable:false })
    permission: string;

    @Column( { type:'numeric',  name: 'ratelimit',nullable:true })
    ratelimit: number;

    @Column( { type:'text',  name: 'principaltype',nullable:false })
    principaltype: string;

    @Column( { type:'text',  name: 'principalid',nullable:false })
    principalid: string;

    @Column( { type:'boolean',  name: 'isheaderauthdelete',nullable:false,default:false })
    isheaderauthdelete: boolean;

    @Column( { type:'boolean',  name: 'isbaserootpathdelete',nullable:false,default:false })
    isbaserootpathdelete: boolean;

    @Column( { type:'text',  name: 'url_param',nullable:true })
    url_param: string;

    @CreateDateColumn( { name: 'createdat',nullable:false })
    createdat: Date;

    @UpdateDateColumn( { name: 'modifiedat',nullable:false })
    modifiedat: Date;

    @Column( { length:50, default:'db_manual_entry',  name: 'createdby',nullable:false })
    createdby: string;

    @Column( { length:50, default:'db_manual_entry',  name: 'modifiedby',nullable:false })
    modifiedby: string;

    @Column({ type: 'text', name: 'permision_desc', nullable: true })
    permision_desc: string;

    @Column({ type: 'text', name: 'sub_description', nullable: true })
    sub_description: string;

    @Column({ type: 'text', name: 'ratelimit_desc', nullable: true })
    ratelimit_desc: string;

    @Column( { type:'boolean',  name: 'editable', nullable:true, default:false })
    editable: boolean;
}