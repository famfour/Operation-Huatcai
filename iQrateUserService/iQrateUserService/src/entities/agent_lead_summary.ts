import { ViewColumn, ViewEntity} from "typeorm";

import { ConfigService } from "../config/config.service";
import Debug from 'debug';
const debug = Debug('user-service:UserService');

const config = new ConfigService();

const dblinkconn_str = `dbname=${config.envConfig.TYPEORM_CUST_DATABASE || 'customer'} port=${config.envConfig.TYPEORM_PORT || 5432} host=${config.envConfig.TYPEORM_HOST} user=${config.envConfig.TYPEORM_USERNAME} password=${config.envConfig.TYPEORM_PASSWORD}`;

debug('dblinkconn_str :', dblinkconn_str);

const customerViewSQL = `SELECT cam.agent_applied agent_userid,
        count(1) AS total_leads
       FROM lead_lead cam 
      WHERE to_char(cam.created, ''YYYYMM''::text) = to_char(CURRENT_DATE::timestamp with time zone, ''YYYYMM''::text)
      GROUP BY cam.agent_applied;`;

debug('customerViewSQL :', customerViewSQL);

const AgentLeadsSummaryViewSql = `
        select
        clcam.agent_userid,
        clcam.total_leads
        FROM dblink('${dblinkconn_str}'::text,'${customerViewSQL}'::text) clcam (agent_userid text,total_leads integer)
    `;

debug('AgentLeadsSummaryViewSql :', AgentLeadsSummaryViewSql);

@ViewEntity({
    expression: AgentLeadsSummaryViewSql,
    schema: 'public'
})
export class AgentLeadsSummary {

    @ViewColumn()
    agent_userid: string;

    @ViewColumn()
    total_leads: number;

}