import {
  BaseEntity,
  Column,
  Entity, PrimaryColumn,
} from 'typeorm';

@Entity({ name: 'aggregate_users_view' })
export class AggregateUsersView extends BaseEntity {

  @PrimaryColumn({
    type: 'bigint',
    nullable: false,
    name: 'id',
  })
  id: number;

  //@PrimaryColumn({ primary: false })
  @Column({ type: 'text', nullable: false, name: 'membership_type' })
  MembershipType: string;

  @Column({ type: 'text', nullable: false, name: 'joined_month' })
  JoinedMonth: string;

  @Column({
    type: 'bigint',
    nullable: false,
    name: 'total_users',
  })
  totalUsers: number;

  @Column({
    type: 'numeric',
    nullable: false,
    name: 'total_amount',
  })
  totalAmount: number;


  @Column({ type: 'text', nullable: false, name: 'month' })
  month: string;

  @Column({ type: 'text', nullable: false, name: 'year' })
  year: string;

  @Column({ type: 'text', nullable: false, name: 'monthname' })
  monthname: string;

    @Column({ type: 'text', nullable: false, name: 'plan' })
    plan: string;

}

@Entity({ name: 'aggregate_yearly_users_view' })
export class AggregateYearlyUsersView extends BaseEntity {

  @PrimaryColumn({
    type: 'bigint',
    nullable: false,
    name: 'id',
  })
  id: number;

  //@PrimaryColumn({ primary: false })
  @Column({ type: 'text', nullable: false, name: 'membership_type' })
  MembershipType: string;

  @Column({ type: 'text', nullable: false, name: 'joined_year' })
  JoinedYear: string;

  @Column({
    type: 'bigint',
    nullable: false,
    name: 'total_users',
  })
  totalUsers: number;

  @Column({
    type: 'numeric',
    nullable: false,
    name: 'total_amount',
  })
  totalAmount: number;
}


// revenue report
@Entity({ name: 'revenue_view' })
export class UsersSubscriptionRevenueView extends BaseEntity {

  @PrimaryColumn({
    type: 'bigint',
    nullable: false,
    name: 'id',
  })
  id: number;

  @Column({ type: 'text', nullable: false, name: 'joined_month' })
  JoinedMonth: string;

  @Column({
    type: 'numeric',
    nullable: false,
    name: 'total_amount',
  })
  totalAmount: number;


  @Column({ type: 'text', nullable: false, name: 'month' })
  month: string;

  @Column({ type: 'text', nullable: false, name: 'year' })
  year: string;

  @Column({ type: 'text', nullable: false, name: 'monthname' })
  monthname: string;
}

@Entity({ name: 'users_summary' })
export class UserSummaryView extends BaseEntity {

  @PrimaryColumn({
    type: 'bigint',
    nullable: false,
    name: 'id',
  })
  id: number;

  //@PrimaryColumn({ primary: false })
  @Column({ type: 'text', nullable: false, name: 'membership_type' })
  MembershipType: string;

  @Column({ type: 'text', nullable: false, name: 'joined_month' })
  JoinedMonth: string;

  @Column({
    type: 'bigint',
    nullable: false,
    name: 'total_users',
  })
  totalUsers: number;

  @Column({ type: 'text', nullable: false, name: 'month' })
  month: string;

  @Column({ type: 'text', nullable: false, name: 'year' })
  year: string;

  @Column({ type: 'text', nullable: false, name: 'monthname' })
  monthname: string;
}