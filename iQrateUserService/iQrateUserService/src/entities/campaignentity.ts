import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  PrimaryColumn,
} from 'typeorm';
import { UserEntity } from './users';

@Entity('campaign', { schema: 'public' })
export class CampaignEntity extends BaseEntity {
  @PrimaryColumn('text', { name: 'id' })
  // id: string;
  id: number;

  // @PrimaryGeneratedColumn({ type: 'bigint' })
  // id: string;
  // id: number;
  // id: bigint;
  // @Column('text', { name: 'keycloak_id', nullable: true })
  // keycloak_id: string;

  @Column('text', { name: 'referal_code', nullable: true, unique: true })
  referal_code: string;

  @Column('integer', { name: 'usage_limt', nullable: true })
  usage_limt: number;

  // @Column('text', { name: 'usage_limt', nullable: true })
  // usage_limt: string;

  // @Column({ type: 'varchar', nullable: true })
  // start_date: string;

  // @Column({ type: 'varchar', nullable: true })
  // end_date: string;

  @Column('date', { name: 'start_date', nullable: true }) 
  start_date: Date;

  @Column('date', { name: 'end_date', nullable: true })
  end_date: Date;

  @Column('int4', { name: 'status', nullable: true })
  status: number;

  @Column('text', { name: 'created_user_id', nullable: true })
  created_user_id: string;

  @Column('text', { name: 'updated_user_id', nullable: true })
  updated_user_id: string;

  @Column('timestamp', { name: 'created_at', nullable: true })
  created_at: Date;

  @Column('timestamp', { name: 'updated_at', nullable: true })
  updated_at: Date;

  @OneToMany((type) => UserEntity, (userInfo) => userInfo.compaignInfo)
  userInfo: UserEntity[];

}
