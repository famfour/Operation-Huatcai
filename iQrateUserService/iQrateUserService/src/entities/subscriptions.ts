import { Exclude } from 'class-transformer';
import {
  BaseEntity,
  Column,
  Entity,
  OneToMany,
  PrimaryColumn,
  PrimaryGeneratedColumn
} from 'typeorm';

@Entity('subscriptions', { schema: 'public' })
export class SubscriptionEntity extends BaseEntity {
  // @PrimaryGeneratedColumn()
  @PrimaryColumn()
  id: string;

  @Column('text', { name: 'stripe_subscription_id', nullable: true })
  stripe_subscription_id: string;

  @Column('text', { name: 'stripe_customer_id', nullable: true })
  stripe_customer_id: string;

  @Column('text', { name: 'keycloak_user_id', nullable: true })
  keycloak_user_id: string;

  @Exclude()
  @Column('text', { name: 'stripe_email', nullable: true })
  stripe_email: string;

  @Column('text', { name: 'stripe_phone', nullable: true })
  stripe_phone: string;

  @Column('text', { name: 'stripe_product_id', nullable: true })
  stripe_product_id: string;

  @Column('text', { name: 'stripe_price_id', nullable: true })
  stripe_price_id: string;

  @Column('timestamp', { name: 'plan_start_date', nullable: true })
  plan_start_date: Date;

  @Column('text', { name: 'membership_type', nullable: true })
  membership_type: string;

  @Column('text', { name: 'collection_method', nullable: true })
  collection_method: string;

  @Column('text', { name: 'status', nullable: true })
  status: string;

  @Column('text', { name: 'created_user_id', nullable: true })
  created_user_id: string;

  @Column('text', { name: 'updated_user_id', nullable: true })
  updated_user_id: string;

  @Column('timestamp', { name: 'created_at', nullable: true })
  created_at: Date;

  @Column('timestamp', { name: 'updated_at', nullable: true })
  updated_at: Date;

  @Column('integer', { name: 'amount', nullable: true })
  amount: number;

  @Column('text', { name: 'plan', nullable: true })
  plan: string;

}
