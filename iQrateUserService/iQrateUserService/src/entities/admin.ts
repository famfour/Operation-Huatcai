import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('admin', { schema: 'public' })
export class AdminEntity extends BaseEntity {
  @PrimaryColumn('text', { name: 'id' })
  // id: string;
  id: string;

  // @PrimaryGeneratedColumn({ type: 'bigint' })
  // id: string;
  // id: number;
  // id: bigint;
  // @Column('text', { name: 'keycloak_id', nullable: true })
  // keycloak_id: string;

  @Column('text', { name: 'full_name', nullable: true })
  full_name: string;

  @Column('text', { name: 'email', nullable: true, unique: true })
  email: string;

  @Column('text', { name: 'password', nullable: true })
  password: string;

  @Column('text', { name: 'country_code', nullable: true })
  country_code: string;

  @Column('text', { name: 'mobile', nullable: true, unique: true })
  mobile: string;

  @Column('text', { name: 'phone', nullable: true })
  phone: string;

  @Column('text', { name: 'user_type', nullable: true })
  user_type: string;

  @Column('text', { name: 'membership_type', nullable: true })
  membership_type: string;

  @Column('text', { name: 'photo', nullable: true })
  photo: string;

  @Column('text', { name: 'otp', nullable: true })
  otp: string;

  // @Column('text', { name: 'token' } )
  // token: string | null;
  @Column({
    length: 500,
    nullable: true,
  })
  token: string;

  @Column('int4', { name: 'agreed_term', nullable: true })
  agreed_term: number;

  @Column('text', { name: 'verification_code', nullable: true })
  verification_code: string;

  @Column('int4', { name: 'is_verified', nullable: true })
  is_verified: number;

  @Column('int4', { name: 'status', nullable: true })
  status: number;

  @Column('text', { name: 'created_user_id', nullable: true })
  created_user_id: string;

  @Column('text', { name: 'updated_user_id', nullable: true })
  updated_user_id: string;

  @Column('timestamp', { name: 'created_at', nullable: true })
  created_at: Date;

  @Column('timestamp', { name: 'updated_at', nullable: true })
  updated_at: Date;

  @Column('integer', { name: 'resendcode_email_count', nullable: true })
  resendcode_email_count: number;

  @Column('integer', { name: 'resendcode_sms_count', nullable: true })
  resendcode_sms_count: number;

  // @Column({ type: 'date', nullable: true })
  // dob: Date;

  @Column({ type: 'varchar', nullable: true })
  dob: string;

  // @Column("text", { array: true })
  // ip: string[];

  // @Column('lseg', { nullable: true })
  // device: string | string[];

  @Column('json', { nullable: true })
  device: string | string[];

  @Column({ type: 'varchar', nullable: true })
  ip: string;

  @Column({ type: 'varchar', nullable: true })
  location: string;

  @Column({ type: 'varchar', nullable: true })
  occupation: string;

  @Column({ type: 'varchar', nullable: true })
  postal_code: string;

  @Column('int4', { name: 'is_email_verified', nullable: true })
  is_email_verified: number;

  @Column('int4', { name: 'is_mobile_verified', nullable: true })
  is_mobile_verified: number;

  @Column({ type: 'varchar', nullable: true })
  address: string;

  // @Column('text', { name: 'refresh_token', nullable: true })
  // refresh_token: string;
  @Column("text", { array: true, default: "{}", nullable: false })
  refresh_token: string[];
}

