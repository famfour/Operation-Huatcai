export const entityDefaults = {
  database: 'my_db',
  schema: 'public',
  synchronize: false
};