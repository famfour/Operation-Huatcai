import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { UserEntity } from './users';

@Entity('user_bank', { schema: 'public' })
export class UserBankEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  // id: string;
  id: number;

  // @PrimaryGeneratedColumn({ type: 'bigint' })
  // id: string;
  // id: number;
  // id: bigint;
  @Column('text', { name: 'userId', nullable: true })
  userId: string;

  @Column({ type: 'varchar', nullable: true })
  bank_name: string;

  @Column({ type: 'varchar', nullable: true })
  bank_ac_number: string;

  @Column({ type: 'varchar', nullable: true })
  bank_branch_code: string;

  @Column({ type: 'varchar', nullable: true })
  bank_nation: string;

  @Column({ type: 'varchar', nullable: true })
  bank_nation_id: string;

  @Column('text', { name: 'created_user_id', nullable: true })
  created_user_id: string;

  @Column('text', { name: 'updated_user_id', nullable: true })
  updated_user_id: string;

  @Column('timestamp', { name: 'created_at', nullable: true })
  created_at: Date;

  @Column('timestamp', { name: 'updated_at', nullable: true })
  updated_at: Date;

  // @ManyToOne((type) => UserEntity, (user) => user.banksKey)
  // @JoinColumn({ name: 'user_id' })
  // userKey: UserEntity;

  // @Column('text', { name: 'keycloak_id', nullable: true })
  // keycloak_id: string;

  @ManyToOne((type) => UserEntity, (user) => user.bankers)
  @JoinColumn({ name: 'userId' })
  user: UserEntity;
}
