import { Exclude } from 'class-transformer';
import {
  BaseEntity,
  Column,
  Entity,
  OneToMany,
  PrimaryColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('login_session', { schema: 'public' })
export class LoginSession extends BaseEntity {
  @PrimaryGeneratedColumn()
  // id: string;
  id: number;

  @Column('text', { name: 'user_membershiptype', nullable: true })
  user_membershiptype: string;

  @Column('integer', { name: 'allow_device', nullable: true })
  allow_device: number;
}
