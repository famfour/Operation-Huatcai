import {
  BaseEntity,
  Column,
  Entity,
  Index,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({ name: 'enums' })
export default class Enum extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', nullable: true, length: 255 })
  description: string;

  @Column({ type: 'text', nullable: false, name: 'code_value' })
  codeValue: string;

  @Column({ type: 'json', nullable: false, name: 'code_value_json' })
  codeValueJson: string;

  @Index()
  @Column({ type: 'varchar', nullable: false, length: 50, name: 'type' })
  type: string;

  @UpdateDateColumn({ type: 'date', name: 'modified_at' })
  modifiedAt: Date;
}
