import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  PrimaryColumn,
} from 'typeorm';

@Entity('push_notify', { schema: 'public' })
export class PushNotifiyEntity extends BaseEntity {
  @PrimaryColumn('text', { name: 'id' })
  id: number;

  @Column('text', { name: 'title', nullable: true})
  title: string;

  @Column('text', { name: 'message', nullable: true})
  message: string;

  @Column("text", { array: true, default: "{}", nullable: false })
  target_audience: string[];

  @Column('date', { name: 'notify_date', nullable: true }) 
  notify_date: Date;

  @Column('text', { name: 'notify_time', nullable: true }) 
  notify_time: string;

  @Column('text', { name: 'notify_date_time', nullable: true }) 
  notify_date_time: string;

  @Column('text', { name: 'created_user_id', nullable: true })
  created_user_id: string;

  @Column('text', { name: 'updated_user_id', nullable: true })
  updated_user_id: string;

  @Column('timestamp', { name: 'created_at', nullable: true })
  created_at: Date;

  @Column('timestamp', { name: 'updated_at', nullable: true })
  updated_at: Date;
}
