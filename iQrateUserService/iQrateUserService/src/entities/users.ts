import { Exclude } from 'class-transformer';
import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryColumn,
} from 'typeorm';
import { CampaignEntity } from './campaignentity';
import { UserBankEntity } from './userbank';

@Entity('users', { schema: 'public' })
export class UserEntity extends BaseEntity {
  @PrimaryColumn('text', { name: 'id' })
  // id: string;
  id: string;

  // @PrimaryGeneratedColumn({ type: 'bigint' })
  // id: string;
  // id: number;
  // id: bigint;
  // @Column('text', { name: 'keycloak_id', nullable: true })
  // keycloak_id: string;

  @Column('text', { name: 'full_name', nullable: true })
  full_name: string;

  @Column('text', { name: 'email', nullable: true })
  email: string;

  @Exclude()
  @Column('text', { name: 'password', nullable: true })
  password: string;

  @Column('text', { name: 'stripe_customer_id', nullable: true })
  stripe_customer_id: string;

  @Column('text', { name: 'stripe_payment_method_id', nullable: true })
  stripe_payment_method_id: string;

  @Column('text', { name: 'stripe_subscription_id', nullable: true })
  stripe_subscription_id: string;

  @Column('text', { name: 'stripe_price_id', nullable: true })
  stripe_price_id: string;

  @Column('text', { name: 'country_code', nullable: true })
  country_code: string;

  @Column('text', { name: 'mobile', nullable: true })
  mobile: string;

  @Column('text', { name: 'whole_number', nullable: true })
  whole_number: string;

  @Column('text', { name: 'phone', nullable: true })
  phone: string;

  @Column('text', { name: 'user_type', nullable: true })
  user_type: string;

  @Column('text', { name: 'membership_type', nullable: true })
  membership_type: string;

  @Column('text', { name: 'photo', nullable: true })
  photo: string;

  @Column('text', { name: 'otp', nullable: true })
  otp: string;

  // @Column('text', { name: 'token' } )
  // token: string | null;
  @Column({
    length: 500,
    nullable: true,
  })
  token: string;

  @Column('int4', { name: 'agreed_term', nullable: true })
  agreed_term: number;

  @Column('text', { name: 'verification_code', nullable: true })
  verification_code: string;

  @Column('timestamp', { name: 'verification_code_send_datetime', nullable: true })
  verification_code_send_datetime: Date;

  @Column('int4', { name: 'is_verified', nullable: true })
  is_verified: number;

  @Column('int4', { name: 'status', nullable: true })
  status: number;

  @Column('text', { name: 'created_user_id', nullable: true })
  created_user_id: string;

  @Column('text', { name: 'updated_user_id', nullable: true })
  updated_user_id: string;

  @Column('timestamp', { name: 'created_at', nullable: true })
  created_at: Date;

  @Column('timestamp', { name: 'updated_at', nullable: true })
  updated_at: Date;

  @Column('integer', { name: 'resendcode_email_count', nullable: true })
  resendcode_email_count: number;

  @Column('integer', { name: 'resendcode_sms_count', nullable: true })
  resendcode_sms_count: number;

  // @Column({ type: 'date', nullable: true })
  // dob: Date;

  @Column({ type: 'varchar', nullable: true })
  dob: string;

  // @Column("text", { array: true })
  // ip: string[];

  // @Column('lseg', { nullable: true })
  // device: string | string[];

  @Column('json', { nullable: true })
  device: string | string[];

  @Column({ type: 'varchar', nullable: true })
  ip: string;

  @Column({ type: 'varchar', nullable: true })
  location: string;

  @Column({ type: 'varchar', nullable: true })
  occupation: string;

  @Column({ type: 'varchar', nullable: true })
  postal_code: string;

  @Column('int4', { name: 'is_email_verified', nullable: true })
  is_email_verified: number;

  @Column('int4', { name: 'is_mobile_verified', nullable: true })
  is_mobile_verified: number;

  @Column({ type: 'varchar', nullable: true })
  address: string;

  @Column({ nullable: true })
  public twoFactorAuthenticationSecret?: string;

  @Column({ default: false })
  public isTwoFactorAuthenticationEnabled: boolean;

  @Column('text', { name: 'register_type', nullable: true })
  register_type: string;

  @Column('text', { name: 'country_code_label', nullable: true })
  country_code_label: string;

  // @Column('text', { name: 'refresh_token', nullable: true })
  // refresh_token: string;
  @Column("text", { array: true, default: "{}", nullable: false })
  refresh_token: string[];

  @Column('text', { name: 'referal_code', nullable: true })
  referal_code: string;

  @Column('int4', { name: 'cobroke_status', nullable: true })
  cobroke_status: number;

  @Column('timestamp', { name: 'deleted_at', nullable: true })
  deleted_at: Date;

  @Column('int4', { name: 'deleted', default:0, nullable: false })
  deleted: number;

  @OneToMany((type) => UserBankEntity, (bank) => bank.user)
  bankers: UserBankEntity[];

  @ManyToOne((type) => CampaignEntity, (compaignInfo) => compaignInfo.userInfo)
  @JoinColumn({ name: 'referal_code', referencedColumnName: 'referal_code' })
  compaignInfo: CampaignEntity;
}
