import { Exclude } from 'class-transformer';
import {
  BaseEntity,
  Column,
  Entity,
  OneToMany,
  PrimaryColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('contact_us', { schema: 'public' })
export class ContactUs extends BaseEntity {
  @PrimaryGeneratedColumn()
  // id: string;
  id: number;

  @Column('text', { name: 'name', nullable: true })
  name: string;

  @Column('text', { name: 'email', nullable: true })
  email: string;

  @Column('text', { name: 'message', nullable: true })
  message: string;

  @Column('text', { name: 'created_user_id', nullable: true })
  created_user_id: string;

  @Column('text', { name: 'updated_user_id', nullable: true })
  updated_user_id: string;

  @Column('timestamp', { name: 'created_at', nullable: true })
  created_at: Date;

  @Column('timestamp', { name: 'updated_at', nullable: true })
  updated_at: Date;
}
