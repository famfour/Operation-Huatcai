import {
    BaseEntity,
    Entity,
    PrimaryGeneratedColumn,
    Column, CreateDateColumn,UpdateDateColumn
} from 'typeorm';

@Entity('admin_acl', { schema: 'public' })
export class AdminAcl extends BaseEntity {
    @PrimaryGeneratedColumn( { name: 'id' })
        // id: string;
    id: number;

    @Column( { length:50, name: 'name',nullable:false })
    name: string;

    @Column( { length:50, name: 'microservice',nullable:false })
    microservice: string;

    @Column( { type:'text',  name: 'api_endpoint',nullable:false })
    api_endpoint: string;

    @Column( { type:'text',  name: 'accesstype',nullable:false })
    accesstype: string;

    @Column( { type:'text',  name: 'permission',nullable:false })
    permission: string;

    @Column( { type:'integer',  name: 'ratelimit',nullable:true })
    ratelimit: number;

    @Column( { type:'text',  name: 'principaltype',nullable:false })
    principaltype: string;

    @Column( { type:'text',  name: 'principalid',nullable:false })
    principalid: string;

    @CreateDateColumn( { name: 'createdat',nullable:false })
    createdat: Date;

    @UpdateDateColumn( { name: 'modifiedat',nullable:false })
    modifiedat: Date;

    @Column( { length:50, default:'db_manual_entry',  name: 'createdby',nullable:false })
    createdby: string;

    @Column( { length:50, default:'db_manual_entry',  name: 'modifiedby',nullable:false })
    modifiedby: string;

    @Column( { type:'text',  name: 'module_name',nullable:false })
    module_name: string;

    @Column( { type:'text',  name: 'role_type',nullable:false })
    role_type: string;

    @Column( { type:'text',  name: 'order',nullable:true })
    order: string;

}