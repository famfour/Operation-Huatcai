import * as dotenv from 'dotenv';
import { dotEnvOptions } from '../config/dotenv-options';

// Make sure dbConfig is imported only after dotenv.config
import * as dbConfig from '../config/db.config';

dotenv.config(dotEnvOptions);

module.exports = dbConfig.default;
