import django_filters
from payout.models import PayoutDetail


class AdminClearanceFeeFilter(django_filters.FilterSet):
    """
    Filter for clearance fee
    """
    date_from = django_filters.DateFilter(field_name='iqrate_payout_date', lookup_expr="gte")
    date_till = django_filters.DateFilter(field_name='iqrate_payout_date', lookup_expr="lte")

    class Meta:
        model = PayoutDetail
        fields = ['date_from', 'date_till']
