from django.urls import path, include
from .views import admin
from .views import agent

app_name = "dashboard"

admin_patterns = [
    path('all', admin.AdminDashBoardAPIView.as_view(), name="admin_dashboard"),
    path('clearance_fee', admin.AdminClearanceFeeAPIView.as_view(), name="admin_clearance_fee"),
]

agent_patterns = [
    path('all', agent.AgentDashBoardAPIView.as_view(), name="agent_dashboard"),
]
