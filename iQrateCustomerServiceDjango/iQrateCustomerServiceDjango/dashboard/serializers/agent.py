from rest_framework import serializers


class AgentDashboardItemSerializer(serializers.Serializer):
    name = serializers.CharField()
    cases = serializers.IntegerField(default=0)
    amount = serializers.FloatField(default=0)
