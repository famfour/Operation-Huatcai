from drf_spectacular.utils import extend_schema_field
from rest_framework import serializers
import calendar


class AdminDashboardItemSerializer(serializers.Serializer):
    name = serializers.CharField()
    cases = serializers.IntegerField(default=None)
    amount = serializers.FloatField(default=None)


class AdminClearanceFeeSerializer(serializers.Serializer):
    id = serializers.IntegerField(source='oid')
    JoinedMonth = serializers.SerializerMethodField()
    totalAmount = serializers.FloatField(default=0.0)
    month = serializers.IntegerField()
    year = serializers.IntegerField()
    monthname = serializers.SerializerMethodField()

    @extend_schema_field(field=serializers.CharField(max_length=16))
    def get_JoinedMonth(self, obj):
        return f"{obj.get('month')}_{obj.get('year')}"

    @extend_schema_field(field=serializers.CharField(max_length=16))
    def get_monthname(self, obj):
        return calendar.month_name[obj.get('month')]
