import django_filters
from django.db.models import Sum, F, Count
from django.db.models import Window, Q
from django.db.models.functions import DenseRank
from django.utils.timezone import localdate
from drf_spectacular.utils import extend_schema
from rest_framework.generics import ListAPIView
from rest_framework.response import Response

from dashboard.filters.admin import AdminClearanceFeeFilter
from dashboard.serializers.admin import AdminDashboardItemSerializer, AdminClearanceFeeSerializer
from kvstore.choices import LeadStatusChoices, PayoutStatusChoices
from lead.models import Client
from lead.models import Lead
from payout.models import Payout, PayoutDetail, Loan


@extend_schema(tags=["Admin - Dashboard"], summary="Data for Admin Dashboard",
               responses=AdminDashboardItemSerializer)
class AdminDashBoardAPIView(ListAPIView):
    pagination_class = None

    def get(self, *args, **kwargs):
        pending_bank_payout = Payout.objects.filter(
            status=PayoutStatusChoices.pending_banks_payment
        ).aggregate(amount=Sum('gross_commission'), cases=Count('id'))

        pending_iqrate_payout = Payout.objects.filter(
            status=PayoutStatusChoices.pending_user_payout
        ).aggregate(amount=Sum('gross_commission'), cases=Count('id'))

        pending_user_payout = Payout.objects.filter(
            Q(status=PayoutStatusChoices.pending_user_payout) & Q(details__broker_payout_date__isnull=True)
        ).aggregate(amount=Sum('details__broker_commission'), cases=Count('id'))

        pending_co_broker_payout = Payout.objects.filter(
            Q(status=PayoutStatusChoices.pending_user_payout) &
            Q(lead__co_broke_agent__isnull=False) &
            Q(details__co_broker_payout_date__isnull=True)
        ).aggregate(amount=Sum('details__co_broker_commission'), cases=Count('id'))
        # Total Won Cases -> What it is.
        total_won_cases = Payout.objects.filter(
            lead__lead_status=LeadStatusChoices.won
        ).aggregate(amount=Sum('gross_commission'), cases=Count('id'))

        # Conversion Rate -> (Total Leads / Won Leads) * 100
        total_lead = Lead.objects.filter(clients__isnull=False).count()
        won_leads = Lead.objects.filter(lead_status=LeadStatusChoices.won).count()
        conversion_rate = (won_leads / total_lead) * 100

        # Work-In-Progress Leads -> Pending Leads
        work_in_progress_leads = Lead.objects.filter(
            lead_status=LeadStatusChoices.pending
        ).aggregate(amount=Sum('loan_amount'), cases=Count('id'))

        # Rework Leads -> Rework Leads
        rework_leads = Payout.objects.filter(
            status=LeadStatusChoices.rework
        ).aggregate(amount=Sum('gross_commission'), cases=Count('id'))

        # Renewal for the Month -> Loan.next_review_date is this month
        today = localdate()
        renewals_for_the_month = Loan.objects.filter(
            Q(next_review_date__month=today.month) & Q(next_review_date__year=today.year)
        ).count()

        # Leads Birthday for the Month -> Leads with birthday current month.
        birthdays_for_the_month = Client.objects.filter(
            Q(main_applicant=True) & Q(dob__month=today.month) & Q(dob__year=today.year)
        ).count()

        data = [
            dict(
                name="Pending Bank Payout",
                cases=pending_bank_payout.get('cases', 0),
                amount=pending_bank_payout.get('amount', 0)
            ),
            dict(
                name="Pending IQrate Payout",
                cases=pending_iqrate_payout.get('cases', 0),
                amount=pending_iqrate_payout.get('amount', 0)
            ),
            dict(
                name="Pending User Payout",
                cases=pending_user_payout.get('cases', 0),
                amount=pending_user_payout.get('amount', 0)
            ),
            dict(
                name="Pending Co-Broker Payout",
                cases=pending_co_broker_payout.get('cases', 0),
                amount=pending_co_broker_payout.get('amount', 0)
            ),
            dict(
                name="Total Won Cases",
                cases=total_won_cases.get('cases', 0),
                amount=total_won_cases.get('amount', 0)
            ),
            dict(
                name="Conversion Rate",
                amount=conversion_rate
            ),
            dict(
                name="Work-In-Progress Leads",
                cases=work_in_progress_leads.get('cases', 0),
                amount=work_in_progress_leads.get('amount', 0) * 0.15 / 100
            ),
            dict(
                name="Rework Leads",
                cases=rework_leads.get('cases', 0),
                amount=rework_leads.get('amount', 0),
            ),
            dict(
                name="Renewal for the Month",
                amount=renewals_for_the_month,
            ),
            dict(
                name="Leads Birthday for the Month",
                amount=birthdays_for_the_month
            ),
        ]
        return Response(data=AdminDashboardItemSerializer(instance=data, many=True).data)


@extend_schema(tags=["Admin - Dashboard"], summary="Revenue Report",
               responses=AdminClearanceFeeSerializer)
class AdminClearanceFeeAPIView(ListAPIView):
    pagination_class = None
    serializer_class = AdminClearanceFeeSerializer
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    filterset_class = AdminClearanceFeeFilter

    def get_queryset(self):
        return PayoutDetail.objects.annotate(
            month=F('iqrate_payout_date__month'),
            year=F('iqrate_payout_date__year'),
            oid=Window(expression=DenseRank())
        ).values('month', 'year', 'oid').annotate(
            totalAmount=Sum('iqrate_clearance_fee')
        ).filter(month__isnull=False, year__isnull=False).order_by('month', 'year')
