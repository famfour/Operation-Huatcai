from django.db.models import Sum, F, Q, Case, When, FloatField, Value
from django.utils.timezone import localdate
from drf_spectacular.utils import extend_schema
from rest_framework.generics import ListAPIView
from rest_framework.response import Response

from dashboard.serializers.agent import AgentDashboardItemSerializer
from kvstore.choices import LeadStatusChoices, PayoutStatusChoices
from lead.models import Lead, Client
from payout.models import PayoutDetail, Loan


@extend_schema(tags=["Agent - Dashboard"], summary="Data for Agent Dashboard",
               responses=AgentDashboardItemSerializer)
class AgentDashBoardAPIView(ListAPIView):
    pagination_class = None

    def get(self, *args, **kwargs):
        user = self.request.user
        # Total won count
        total_won_count = Lead.objects.filter(lead_status=LeadStatusChoices.won).for_user(user).count()
        # Total won amount.
        total_won = PayoutDetail.objects.filter(
            Q(payout__lead__agent_applied=user.id) | Q(payout__lead__co_broke_agent=user.id)
        ).annotate(
            amount=Case(
                When(Q(payout__lead__agent_applied=user.id), then=F('broker_commission')),
                When(Q(payout__lead__co_broke_agent=user.id), then=F('co_broker_commission')),
                default=Value(0),
                output_field=FloatField()
            )
        ).aggregate(total_amount=Sum('amount'))
        # Work in progress.
        work_in_progress = Lead.objects.exclude_empty().filter(
            lead_status=LeadStatusChoices.pending
        ).for_user(user).count()
        # Pending Rework
        pending_rework = Lead.objects.with_owner(user).filter(lead_status=LeadStatusChoices.rework).count()

        # Pending Payout.
        pending_payout_count = PayoutDetail.objects.filter(
            payout__status=PayoutStatusChoices.pending_user_payout
        ).filter(
            Q(payout__lead__agent_applied=user.id) | Q(payout__lead__co_broke_agent=user.id)
        ).count()
        # Pending payout amount.
        pending_payout_amount = PayoutDetail.objects.filter(
            payout__status=PayoutStatusChoices.pending_user_payout
        ).filter(
            Q(payout__lead__agent_applied=user.id) | Q(payout__lead__co_broke_agent=user.id)
        ).annotate(
            amount=Case(
                When(Q(payout__lead__agent_applied=user.id), then=F('broker_commission')),
                When(Q(payout__lead__co_broke_agent=user.id), then=F('co_broker_commission')),
                default=Value(0),
                output_field=FloatField()
            )
        ).aggregate(total_amount=Sum('amount'))

        # Renewals for the month
        # Loan for which the current user is  the agent_applied and next_review_date is now
        today = localdate()
        renewals_for_the_month = Loan.objects.filter(
            Q(payout__lead__agent_applied=user.id) & Q(next_review_date__month=today.month) & Q(next_review_date__year=today.year)
        ).count()
        # Clients added or Leads that are added by the current user.
        birthday_for_month = Client.objects.filter(
            Q(main_applicant=True) & Q(lead__agent_applied=user.id) & Q(dob__year=today.year) & Q(dob__month=today.month)
        ).count()
        data = [
            dict(
                name="Total Wons",
                cases=total_won_count,
                amount=total_won.get('total_amount', 0)
            ),
            dict(
                name="Work In Progress",
                cases=work_in_progress
            ),
            dict(
                name="Pending Rework",
                cases=pending_rework
            ),
            dict(
                name="Pending Payout",
                cases=pending_payout_count,
                amount=pending_payout_amount.get('total_amount', 0)
            ),
            dict(
                name="Renewals for the Month",
                cases=renewals_for_the_month
            ),
            dict(
                name="Birthday For the Month",
                cases=birthday_for_month
            )
        ]
        return Response(data=AgentDashboardItemSerializer(instance=data, many=True).data)
