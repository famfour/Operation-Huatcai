from django.urls import path
from .views import agent, admin

app_name = "lead"

agent_patterns = [
    # Token
    path("client/token/<int:client_id>", agent.GetClientToken.as_view(), name="pdpa_token"),

    path('clients/create', agent.ClientCreateAPIView.as_view(), name="clients_create"),
    path('client/<int:pk>', agent.ClientDetailAPIView.as_view(), name="clients_rud"),

    # Property
    path('property/create', agent.PropertyCreateAPIView.as_view(), name="property_create"),
    path('property/<int:pk>', agent.PropertyAPIView.as_view(), name="property_rd"),

    # Leads
    path("all", agent.LeadListAPIView.as_view(), name="leads"),
    path("export", agent.LeadExportAPIView.as_view(), name="leads_export"),
    path('create', agent.LeadCreateAPIView.as_view(), name="leads_create"),
    path("<int:pk>", agent.LeadDetailAPIView.as_view(), name="leads_rud"),

    # LeadPackage
    path("packages/generate/<int:lead>", agent.LeadPackageGenerateView.as_view(), name="generate_packages"),
    path("packages/view-more/<int:lead>", agent.LeadPackageMoreView.as_view(), name="view_more_packages"),
    path("leadpackage/create", agent.LeadPackageCreateView.as_view(), name="lead_packages_create"),
    path("leadpackage/delete/<int:lead_id>/<int:package_id>", agent.LeadPackageDeleteView.as_view(), name="lead_packages_delete"),
    path("leadpackages/<int:lead_id>", agent.LeadPackageListView.as_view(), name="lead_packages_list"),

    # Co Broke
    path("request-co-broke/<int:lead_id>", agent.CoBrokeCreateView.as_view(), name="request_co_broke"),

    # Mark as lost
    path("mark-as-lost", agent.MarkAsLostApiView.as_view(), name="mark_lead_as_lost")
]

admin_patterns = [
    path("all", admin.LeadListAPIView.as_view(), name="lead_list"),
    path("export", admin.AdminLeadExportAPIView.as_view(), name="lead_export"),
    path("<int:pk>", admin.LeadDetailView.as_view(), name="lead_detail"),
    path("user/<uuid:user>", admin.LeadListUserAPIView.as_view(), name="lead_user"),
]
