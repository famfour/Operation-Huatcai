from django.dispatch import receiver
from fieldsignals import pre_save_changed, post_save_changed
from django.db.models.signals import post_save

from .models import Client
from .utils import send_pdpa_message, PDPAException, TwilioException
from rest_framework.exceptions import PermissionDenied
from rest_framework import status
from kvstore.choices import LeadStatusChoices


def send_pdpa_on_phone_change(sender, instance, changed_fields, **kwargs):
    """
    When either phone number or country code is updated, set the PDPA status to False.
    Clients must verify via SMS again if the phone number is updated.
    """
    if instance.lead.lead_status == LeadStatusChoices.rework:
        raise PermissionDenied(detail="Phone number for clients cannot be edited after bank submission")
    instance.pdpa_status = False
    try:
        send_pdpa_message(instance)
    except PDPAException as e:
        raise PermissionDenied(detail=str(e), code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    except TwilioException as e:
        raise PermissionDenied(detail=str(e), code=status.HTTP_500_INTERNAL_SERVER_ERROR)


pre_save_changed.connect(send_pdpa_on_phone_change, sender=Client, fields=['country_code', 'phone_number'])


@receiver(post_save, sender=Client)
def send_pdpa_on_creation(sender, instance, created, **kwargs):
    """
    Send PDPA on client creation
    """
    if not created:
        return
    try:
        send_pdpa_message(instance)
    except PDPAException as e:
        raise PermissionDenied(detail=str(e), code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    except TwilioException as e:
        raise PermissionDenied(detail=str(e), code=status.HTTP_500_INTERNAL_SERVER_ERROR)
