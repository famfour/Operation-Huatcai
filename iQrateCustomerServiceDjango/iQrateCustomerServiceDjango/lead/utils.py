import csv
from datetime import timedelta
from io import StringIO
from typing import Union, List, Iterable
from uuid import uuid4

from django.db.models import QuerySet
from django.utils import timezone
from django.conf import settings

from common.s3client import S3Client
from common.utils import send_sms
from magic.models import PDPAVerification
from lead.models import Client
from user.models import UserBank, User


class PDPAException(Exception):
    pass


class TwilioException(Exception):
    pass


def send_pdpa_message(client: Client) -> PDPAVerification:
    """
    Send PDPA message to client
    """
    # If client is already verified.
    if client.pdpa_status:
        raise PDPAException(f"The lead ({client.name}) has already verified the PDPA")

    # Create the token
    token, created = PDPAVerification.objects.get_or_create(client=client)
    # if token retry time is in the future return an error
    if token.retry_by >= timezone.now():
        raise PDPAException(f"Please retry in {int((token.retry_by - timezone.now()).total_seconds())} seconds")

    # Create a new token and update the 'retry_by' time
    token.code = PDPAVerification.get_unique_code()
    token.used = False  # Set the new token to be not used.
    token.retry_by = timezone.now() + timedelta(seconds=60)
    token.save()
    # Send the SMS
    try:
        send_sms(
            phone=f"+{client.country_code}{client.phone_number}",
            message=f"Your IQRATE PDPA verification url is:\n {settings.PDPA_URL}/{client.id}-{token.code}.\nDo not share this URL with anyone, our employees "
                    f"will never ask for the code"
        )
    except:
        raise TwilioException("Error sending SMS, Please try again")

    return token


def export_leads_to_csv(records: Union[List, Iterable, QuerySet]) -> str:
    headers = [
        "Lead Status",
        "Lead Name",
        "Lead Mobile No.",
        "Lead Email Address",
        "PDPA Status",
        "Date of Birth",
        "Nationality",
        "Annual Income",
        "Joint Applicant #2 Name",
        "Joint Applicant #2 Mobile No.",
        "Joint Applicant #2 Email Address",
        "Joint Applicant #2 PDPA Status",
        "Joint Applicant #2 Date of Birth",
        "Joint Applicant #2 Nationality",
        "Joint Applicant #2 Annual Income",
        "Joint Applicant #3 Name",
        "Joint Applicant #3 Mobile No.",
        "Joint Applicant #3 Email Address",
        "Joint Applicant #3 PDPA Status",
        "Joint Applicant #3 Date of Birth",
        "Joint Applicant #3 Nationality",
        "Joint Applicant #3 Annual Income",
        "Joint Applicant #4 Name",
        "Joint Applicant #4 Mobile No.",
        "Joint Applicant #4 Email Address",
        "Joint Applicant #4 PDPA Status",
        "Joint Applicant #4 Date of Birth",
        "Joint Applicant #4 Nationality",
        "Joint Applicant #4 Annual Income",
        "Property Type",
        "Property Purchase Price",
        "Property Status",
        "Postal Code",
        "Street Name",
        "Unit Number",
        "Project Name",
        "Broker Name",
        "Broker Mobile",
        "Broker Email",
        "Co Broker Name",
        "Co Broker Mobile",
        "Co Broker Email"
    ]

    file_obj = StringIO()
    writer = csv.DictWriter(file_obj, fieldnames=headers)
    writer.writeheader()

    for lead in records:
        data = {
            "Lead Status": lead.lead_status,

            "Lead Name": "",
            "Lead Mobile No.": "",
            "Lead Email Address": "",
            "PDPA Status": "",
            "Date of Birth": "",
            "Nationality": "",
            "Annual Income": "",
            "Joint Applicant #2 Name": "",
            "Joint Applicant #2 Mobile No.": "",
            "Joint Applicant #2 Email Address": "",
            "Joint Applicant #2 PDPA Status": "",
            "Joint Applicant #2 Date of Birth": "",
            "Joint Applicant #2 Nationality": "",
            "Joint Applicant #2 Annual Income": "",
            "Joint Applicant #3 Name": "",
            "Joint Applicant #3 Mobile No.": "",
            "Joint Applicant #3 Email Address": "",
            "Joint Applicant #3 PDPA Status": "",
            "Joint Applicant #3 Date of Birth": "",
            "Joint Applicant #3 Nationality": "",
            "Joint Applicant #3 Annual Income": "",
            "Joint Applicant #4 Name": "",
            "Joint Applicant #4 Mobile No.": "",
            "Joint Applicant #4 Email Address": "",
            "Joint Applicant #4 PDPA Status": "",
            "Joint Applicant #4 Date of Birth": "",
            "Joint Applicant #4 Nationality": "",
            "Joint Applicant #4 Annual Income": "",

            "Broker Name": "",
            "Broker Mobile": "",
            "Broker Email": "",
            "Co Broker Name": "",
            "Co Broker Mobile": "",
            "Co Broker Email": "",
        }

        # Clients
        for index, client in enumerate(lead.clients.all(), start=1):
            if index == 5:
                break
            if index == 1:
                data.update({
                    "Lead Name": client.name,
                    "Lead Mobile No.": f"{client.country_code} {client.phone_number}",
                    "Lead Email Address": client.email,
                    "PDPA Status": "Verified" if client.pdpa_status else "Not Verified",
                    "Date of Birth": str(client.dob),
                    "Nationality": client.nationality,
                    "Annual Income": client.annual_income,
                })
                continue
            data.update({
                f"Joint Applicant #{index} Name": client.name,
                f"Joint Applicant #{index} Mobile No.": f"{client.country_code} {client.phone_number}",
                f"Joint Applicant #{index} Email Address": client.email,
                f"Joint Applicant #{index} PDPA Status": "Verified" if client.pdpa_status else "Not Verified",
                f"Joint Applicant #{index} Date of Birth": str(client.dob),
                f"Joint Applicant #{index} Nationality": client.nationality,
                f"Joint Applicant #{index} Annual Income": client.annual_income,
            })

        if getattr(lead, 'property', None):
            data.update({
                "Property Type": lead.property.get_property_type_display(),
                "Property Purchase Price": lead.property.purchase_price,
                "Property Status": lead.property.get_property_status_display(),
                "Postal Code": lead.property.postal_code,
                "Street Name": lead.property.street_name,
                "Unit Number": lead.property.unit_number,
                "Project Name": lead.property.project_name,
            })

        # Broker
        broker = User.objects.filter(id=lead.agent_applied).first()
        if not broker:
            break
        data.update({
            "Broker Name": broker.full_name,
            "Broker Mobile": f"{broker.country_code} {broker.mobile}",
            "Broker Email": broker.email
        })

        # Co-Broker
        if lead.co_broke_agent:
            co_broker = User.objects.filter(id=lead.co_broke_agent).first()
            if not co_broker:
                break
            data.update({
                "Co Broker Name": co_broker.full_name,
                "Co Broker Mobile": f"{co_broker.country_code} {co_broker.mobile}",
                "Co Broker Email": co_broker.email
            })
        writer.writerow(data)

    # upload the file to S3
    client = S3Client(settings.AWS_TEMP_BUCKET_NAME)
    file_key = f"{str(uuid4())}.pdf"
    client.upload_file(
        file_key=file_key,
        file_obj=file_obj.getvalue(),
        content_type='text/csv'
    )
    file_obj.close()
    url = client.get_presigned_url(
        file_key=file_key,
        expire_in=600, content_disposition=f"attachment;filename=exported-leads.csv"
    )
    return url
