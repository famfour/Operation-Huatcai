import django_filters

from bank.models import Banker
from kvstore.choices import LeadStatusChoices, PayoutStatusChoices
from lawfirm.models import LawFirm
from lead.models import Lead
from user.models import User


class LeadFilter(django_filters.FilterSet):
    lead_name = django_filters.CharFilter(
        field_name="clients__name",
        lookup_expr="icontains"
    )
    bankers = django_filters.ModelMultipleChoiceFilter(
        queryset=Banker.objects.all(),
        field_name="payout__banker"
    )
    co_broker = django_filters.MultipleChoiceFilter(
        #queryset=User.objects.filter(cobroke_status=1).all(),
        #method='in',
        choices=User.objects.filter(cobroke_status=1).values_list("id", 'full_name').all(),
        field_name="co_broke_agent",
        distinct=True,
        help_text="Values for this drop down can be obtained from the endpoint -> /customer-admin/users/co-brokers"
    )
    law_firm = django_filters.ModelMultipleChoiceFilter(
        queryset=LawFirm.objects.all(),
        field_name="payout__law_firm"
    )
    lead_status = django_filters.MultipleChoiceFilter(
        choices=LeadStatusChoices.choices,
        field_name="lead_status",
        #method='in',
        distinct=True
    )
    co_broke = django_filters.BooleanFilter(method="filter_co_broke")
    payout_status = django_filters.BooleanFilter(method="filter_payout_status")

    def filter_co_broke(self, queryset, name, value):
        if value is None:
            return queryset
        return queryset.filter(co_broke_agent__isnull=not value)

    def filter_payout_status(self, queryset, name, value):
        if value is None:
            return queryset
        if value:
            return queryset.filter(payout__status=PayoutStatusChoices.paid)
        return queryset.exclude(payout__status=PayoutStatusChoices.paid)

    class Meta:
        model = Lead
        fields = [
            'lead_name',
            'bankers',
            'law_firm',
            'lead_status',
            'co_broke',
            'payout_status',
            'co_broker'
        ]
