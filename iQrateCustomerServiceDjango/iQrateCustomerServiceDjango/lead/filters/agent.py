import django_filters
from django.db.models import Q

from bank.models import Bank
from common.filters import NumberInFilter, CharInFilter
from kvstore.choices import CoBrokeFilterChoices, RateTypeChoices, LeadStatusChoices
from lead.models import Lead


class LeadFilter(django_filters.FilterSet):
    """
    Filter Leads for agent.
    bank: Accepts multiple bank ids separated by a comma. This field refers
    """
    # bank = NumberInFilter(lookup_expr='in', field_name="approved_package__banker__bank_id", distinct=True)
    bank = NumberInFilter(distinct=True, field_name="payout__bank_id")
    clients_name = django_filters.CharFilter(lookup_expr="icontains", field_name="clients__name", distinct=True)
    lead_status = django_filters.MultipleChoiceFilter(choices=LeadStatusChoices.choices)
    co_broke = django_filters.MultipleChoiceFilter(choices=CoBrokeFilterChoices.choices, method='co_broke_filter', distinct=True)

    def co_broke_filter(self, queryset, name, value):
        if not value:
            return queryset

        filters = []

        if CoBrokeFilterChoices.no_co_broke in value:
            filters.append(Q(co_broke_agent__isnull=True))

        if CoBrokeFilterChoices.request_received in value:
            filters.append(Q(co_broke_agent=self.request.user.id))

        if CoBrokeFilterChoices.request_sent in value:
            filters.append(Q(agent_applied=self.request.user.id) & Q(co_broke_agent__isnull=False))

        q_objects = Q()
        for f in filters:
            q_objects |= f
        return queryset.filter(q_objects)

    class Meta:
        model = Lead
        fields = ['bank', 'clients_name', 'lead_status', 'co_broke']


class LeadPackageFilter(django_filters.FilterSet):
    bank = django_filters.ModelMultipleChoiceFilter(
        queryset=Bank.objects.all(),
        field_name="bank"
    )
    rate_type = django_filters.MultipleChoiceFilter(
        field_name="rate_type",
        lookup_expr="contains",
        distinct=True,
        choices=RateTypeChoices.choices
    )
