from rest_framework.exceptions import PermissionDenied, NotFound
from rest_framework.serializers import ValidationError

from kvstore.choices import LeadTypeChoices
from lead.models import Lead, Client
from user.models import User


class ValidateLeadOwnership():
    """
    Make sure the current user is the owner of the lead.
    """
    requires_context = True

    def __call__(self, value, serializer_field, *args, **kwargs):
        user = serializer_field.context['request'].user
        try:
            lead = Lead.objects.filter(pk=value.pk).with_owner(user).get()
        except Lead.DoesNotExist:
            raise NotFound(detail="You do not have permission to edit this lead.")
        return value


class ValidateLeadAccess():
    """
    Make sure the current user has permission to update the lead id
    """
    requires_context = True

    def __call__(self, value, serializer_field, *args, **kwargs):
        user = serializer_field.context['request'].user
        try:
            lead = Lead.objects.filter(pk=value.pk).for_user(user).annotate_can_edit(user).get()
        except Lead.DoesNotExist:
            raise NotFound(detail="You do not have permission to edit this lead.")
        if not lead.can_edit:
            raise PermissionDenied(detail="You can no longer update this lead")
        return value


class ValidateCanAddLeadPackages():
    """
    Make sure the current user can add packages to the lead. Conditions are.
    1. The user is the co-broker
    2. The lead type is yourself and user is the applied agent and no co broke
    3. The lead type is others and there is no co broke.
    4. Lead type co broke and the user is a co-broke.
    """
    requires_context = True

    def __call__(self, value, serializer_field, *args, **kwargs):
        user = serializer_field.context['request'].user
        lead = Lead.objects.filter(pk=value.pk).annotate_can_add_package(user=user).first()
        if not lead.can_add_package:
            raise PermissionDenied(detail="You can no longer add packages to this lead.")
        return value


class ValidatePostalCode():
    """
    Validate the postal code.
    """

    def __call__(self, value):
        if not value:
            return value
        for item in value:
            if not item.isdigit():
                raise ValidationError(code="postal_code", detail="The postal code should only contain digits")
        return value


class ValidateAgentLeadType():
    """
    Make sure the agent is not adding himself as a client, if the lead is not 'yourself'
    """
    requires_context = True

    def __call__(self, attrs, serializer):
        lead = attrs['lead']
        user = serializer.context.get("request").user
        user = User.objects.get(id=user.id)

        # Condition - agent_applied is the logged-in user & user phone number is same as client phone & lead type is not 'yourself'
        same_phone_number = (attrs['phone_number'] == int(user.mobile)) and (attrs['country_code'] == int(user.country_code))
        same_email = (attrs['email'] == user.email)
        if (lead.agent_applied == user.id) and (same_phone_number or same_email) and not lead.lead_type == LeadTypeChoices.yourself:
            raise PermissionDenied(
                detail="You can't add yourself as a client. Please select Apply Loan for Yourself and proceed")
        return attrs


class ConfirmPDPAApproval():
    """
    Make sure the PDPA is approved before personal data fields can be added
    """
    requires_context = True

    def __call__(self, attrs, serializer):
        fields = ['annual_income', 'dob', 'nationality', 'street_name', 'block_number', 'unit_number',
                  'project_name', 'postal_code']

        # No further validation if PDPA is validated
        if serializer.instance.pdpa_status:
            return attrs
        for field in fields:
            if not attrs.get(field, None):
                continue
            raise PermissionDenied(detail=f"PDPA needs to be approved before {field} can be added/updated")
        return attrs


class ConfirmPDPAMainApplicant():
    """
    Make sure that the main applicant PDPA is approved before adding/editing sub applicant.
    """
    requires_context = True

    def __call__(self, attrs, serializer):
        # If the applicant is the main applicant then no issues.
        if attrs['main_applicant']:
            return attrs

        # Check if main applicant PDPA is approved.
        lead = attrs['lead']
        main_applicant = lead.clients.filter(main_applicant=True).first()
        if not main_applicant.pdpa_status:
            raise PermissionDenied(
                detail="Main applicant needs to confirm PDPA before co-applicant can be added"
            )
        return attrs


class ConfirmMainApplicantCount():
    """
    1.Make sure there is one main applicant.
    2.Make sure there is only one main applicant.
    """
    requires_context = True

    def __call__(self, attrs, serializer):
        lead = attrs['lead']
        is_main_applicant = attrs['main_applicant']

        if serializer.instance and getattr(serializer.instance, "main_applicant", False):
            return attrs

        main_applicant = lead.clients.filter(main_applicant=True).first()

        # if there are no main applicants and current one is not main applicant.
        if not is_main_applicant and not main_applicant:
            raise PermissionDenied(detail="Add at least one main applicant")

        # If there is main applicant and current one is a main applicant
        if is_main_applicant and main_applicant:
            raise PermissionDenied(detail="There should be only one main applicant for a lead.")

        return attrs


class ConfirmUniquePhoneNumber():
    """
    Make sure the all clients for a lead has unique phone numbers
    """
    requires_context = True

    def __call__(self, attrs, serializer):
        lead = attrs['lead']
        query = lead.clients.filter(country_code=attrs['country_code'], phone_number=attrs['phone_number'])
        if serializer.instance:
            query = query.exclude(pk=serializer.instance.pk)
        if query.exists():
            raise PermissionDenied(
                detail="This phone number is added under another client, use a different phone number")

        return attrs


class LeadPDPAValidation():
    """
    Main applicant need to validate PDPA before adding or updating
    1. Loan Details
    2.Property Details
    3. Generate Package
    4. Bank submission
    5. Submit for payout.
    """
    requires_context = True

    def __call__(self, attrs, serializer):
        lead = attrs['lead']
        main_applicant = lead.clients.filter(main_applicant=True).first()
        if not main_applicant:
            raise PermissionDenied(
                detail="Please add a main applicant for this lead."
            )
        if main_applicant.pdpa_status:
            return attrs
        raise PermissionDenied(
            detail="Main applicant need to verify PDPA first."
        )
