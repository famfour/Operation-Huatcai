from django.apps import AppConfig


class LeadConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'lead'

    def ready(self):
        from . import receivers
        from . import signals
        return super(LeadConfig, self).ready()
