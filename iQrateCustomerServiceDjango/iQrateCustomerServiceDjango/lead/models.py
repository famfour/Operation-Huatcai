from django.db import models

from bank.models import Bank, Banker
from bank.models import Package
from common.models import ModelMixin
from kvstore.choices import PropertyTypeChoices, PropertyStatusChoices, LeadTypeChoices, LoanCategoryChoices
from user.models import User
from .queryset import LeadQuerySet


class Lead(ModelMixin):
    lead_type = models.CharField(max_length=128, choices=LeadTypeChoices.choices)
    lead_status = models.CharField(max_length=128)
    reason_for_loss = models.CharField(max_length=254, null=True, blank=True)
    agent_applied = models.UUIDField()
    co_broke_agent = models.UUIDField(null=True, blank=True)
    sell_within_three_years = models.BooleanField(default=False)
    has_200k_down = models.BooleanField(default=False)
    loan_category = models.CharField(max_length=128, null=True, choices=LoanCategoryChoices.choices)
    loan_amount = models.FloatField(default=0)
    existing_bank = models.ForeignKey(Bank, related_name="leads", on_delete=models.PROTECT, null=True)
    current_interest_rate = models.FloatField(null=True, blank=True)

    # Custom manager.
    objects = LeadQuerySet.as_manager()

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return self.lead_type

    def can_add_packages(self, user):
        """
        If the lead_type in ['yourself','co-broke'] and current user is not the
        co broker then not permitted to add packages to lead.
        @todo - lead type yourself can add packages before submitting for co-broke
        """
        return not (self.lead_type in [LeadTypeChoices.yourself, LeadTypeChoices.co_broke] and not user.id == self.co_broke_agent)

    def get_main_agent(self):
        agent_id = self.agent_applied
        if self.co_broke_agent:
            agent_id = self.co_broke_agent
        try:
            return User.objects.filter(id=agent_id).first()
        except User.DoesNotExist:
            return None


class Property(ModelMixin):
    lead = models.OneToOneField(Lead, related_name="property", on_delete=models.CASCADE)
    property_type = models.CharField(max_length=128, choices=PropertyTypeChoices.choices)
    purchase_price = models.FloatField(null=True, blank=True)
    property_status = models.CharField(max_length=128, choices=PropertyStatusChoices.choices)
    postal_code = models.CharField(max_length=32, null=True, blank=True)
    street_name = models.CharField(max_length=254, null=True, blank=True)
    unit_number = models.CharField(max_length=32, null=True, blank=True)
    project_name = models.CharField(max_length=254, null=True, blank=True)

    def __str__(self):
        return f"{self.street_name}, {self.unit_number}, {self.postal_code}"


class Client(ModelMixin):
    lead = models.ForeignKey(Lead, on_delete=models.PROTECT, related_name="clients")
    name = models.CharField(max_length=254)
    country_code = models.SmallIntegerField(null=True, help_text="Country code for the phone number")
    phone_number = models.PositiveBigIntegerField(null=True, help_text="Phone number")
    main_applicant = models.BooleanField(default=False, help_text="is this client the main applicant for the lead")
    pdpa_status = models.BooleanField(default=False)
    email = models.EmailField()
    annual_income = models.FloatField(null=True, blank=True)
    dob = models.DateField(null=True, blank=True)
    nationality = models.CharField(max_length=64, null=True, blank=True)
    street_name = models.CharField(max_length=254, null=True)
    block_number = models.CharField(max_length=128, null=True)
    unit_number = models.CharField(max_length=64, null=True, blank=True)
    project_name = models.CharField(max_length=254, null=True)
    postal_code = models.CharField(max_length=32, null=True)

    class Meta:
        ordering = ['id']

    @property
    def obfuscated_name(self):
        return f"XXXXXXXXXX {self.name.split().pop().title()}"

    def __str__(self):
        return self.name


class LeadPackage(ModelMixin):
    """
    Packages selected by the agent for the lead.
    The Package data and rates need to saved on selection as the rates could change in a later time.
    """
    lead = models.ForeignKey(Lead, related_name="packages", on_delete=models.PROTECT)
    package = models.ForeignKey(Package, related_name="lead_packages", on_delete=models.PROTECT)
    data = models.JSONField(default=dict,
                            help_text="copied data from the selected package, as it can be changed after the submission.")
    submitted = models.BooleanField(
        default=False,
        help_text="Has this package been submitted to either bank or client"
    )

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return str(self.id)
