import django_filters
from django.db.models import F
from django.db.models import Prefetch
from drf_spectacular.utils import extend_schema
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from lead.filters.admin import LeadFilter
from lead.models import Lead, Client
from lead.serializers.admin import AdminLeadListSerializer, AdminLeadDetailSerializer, AdminLeadCSVExportSerializer
from common.views import FileResponseSerializer
from lead.utils import export_leads_to_csv

"""
1. Lead list view with user filter.
2. Lead detail view
"""


@extend_schema(tags=["Admin - Lead"], summary="Get a list of Leads", responses=AdminLeadListSerializer)
class LeadListAPIView(ListAPIView):
    queryset = Lead.objects.none()
    serializer_class = AdminLeadListSerializer
    filterset_class = LeadFilter
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]

    def get_queryset(self):
        return Lead.objects.filter(clients__isnull=False).annotate(
            bank=F('payout__bank__name'),
            banker=F('payout__banker__name'),
            banker_email=F('payout__banker__email'),
            payout_status=F('payout__status'),
            approved_loan_amount=F('payout__loan_amount'),
            payout_amount=F('payout__gross_commission'),
            law_firm=F('payout__law_firm__name'),
            payout_date=F('payout__details__banks_payout_date')
        ).prefetch_related(
            "payout",
            "payout__details",
            Prefetch('clients', queryset=Client.objects.filter(main_applicant=True))
        ).distinct()


@extend_schema(tags=["Admin - Lead"], summary="Lead Detail", responses=AdminLeadDetailSerializer)
class LeadDetailView(RetrieveAPIView):
    model = Lead
    serializer_class = AdminLeadDetailSerializer

    def get_queryset(self):
        return Lead.objects.prefetch_related("clients", "property", "payout", "payout__loans")


@extend_schema(tags=["Admin - Lead"], summary="List of Leads for a User", responses=LeadListAPIView)
class LeadListUserAPIView(ListAPIView):
    queryset = Lead.objects.none()
    serializer_class = AdminLeadListSerializer
    filterset_class = LeadFilter
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]

    def get_queryset(self):
        return Lead.objects.filter(
            clients__isnull=False
        ).filter(
            agent_applied=self.kwargs.get("user")
        ).annotate(
            bank=F('payout__bank__name'),
            banker=F('payout__banker__name'),
            banker_email=F('payout__banker__email'),
            payout_status=F('payout__status'),
            approved_loan_amount=F('payout__loan_amount'),
            payout_amount=F('payout__gross_commission'),
            law_firm=F('payout__law_firm__name'),
            payout_date=F('payout__details__banks_payout_date')
        ).prefetch_related(
            "payout",
            "payout__details",
            Prefetch('clients', queryset=Client.objects.filter(main_applicant=True))
        ).distinct()


class AdminLeadExportAPIView(APIView):

    @extend_schema(tags=["Admin - Lead"], summary="Export Leads to CSV",
                   request=AdminLeadCSVExportSerializer, responses=FileResponseSerializer)
    def post(self, request):
        # validate the data
        serializer = AdminLeadCSVExportSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        queryset = Lead.objects.prefetch_related('clients', 'property')
        if serializer.validated_data.get('lead_ids', None):
            queryset = queryset.filter(pk__in=serializer.validated_data['lead_ids'])

        url = export_leads_to_csv(queryset.all())
        resp_serializer = FileResponseSerializer(instance=dict(
            url=url
        ))
        return Response(data=resp_serializer.data)
