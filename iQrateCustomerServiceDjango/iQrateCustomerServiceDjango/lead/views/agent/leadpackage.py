import django_filters
from django.conf import settings
from django.db.models import Q
from drf_spectacular.utils import extend_schema
from rest_framework import status
from rest_framework.exceptions import NotFound
from rest_framework.exceptions import PermissionDenied
from rest_framework.generics import ListAPIView, CreateAPIView, DestroyAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from lead.filters.agent import LeadPackageFilter
from lead.models import Package, Lead, LeadPackage
from lead.serializers.agent import LeadPackageCreateSerializer, LeadPackageBaseSerializer, \
    LeadPackageGroupSerializer
from submission.models import LeadBanker


class LeadPackageGenerateView(APIView):
    """
    View to return 4 package for the given lead.
    Lowest 2 fixed and floating.
    """

    @extend_schema(tags=["Customer - Lead - Package"], summary="Generate 4 Packages for the lead.",
                   responses=LeadPackageGroupSerializer)
    def get(self, request, lead):
        """
        Generate Packages for a given lead.  \n
        First two would be the lowest fixed and last two would be the lowest floating.  \n
        """
        serializer = LeadPackageGroupSerializer(instance=self.get_queryset())
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    def get_queryset(self):
        # Get the lead and make sure the user has access to the lead.
        # @todo co-broke queryset.
        lead_id = self.kwargs.get("lead")
        try:
            lead = Lead.objects.filter(
                Q(agent_applied=self.request.user.id) | Q(co_broke_agent=self.request.user.id)
            ).filter(pk=lead_id).prefetch_related("property", "clients").get()
        except Lead.DoesNotExist:
            raise PermissionDenied(detail="You do not have access to the lead")

        if not hasattr(lead, "property"):
            raise NotFound(detail="This lead does not have a property added to it.")

        fixed = list()
        floating = list()

        # Create the base query
        base_query = Package.objects.published_only().common_filter(
            property_type=lead.property.property_type,
            property_status=lead.property.property_status,
            loan_amount=lead.loan_amount,
            loan_category=lead.loan_category,
            excluded_bank=lead.existing_bank
        ).order_by_rate().prefetch_related("bank", "rates", "rates__reference_rate")

        # No exclusive packages for the basic member.
        if self.request.user.membership_type == settings.BASIC_MEMBERSHIP:
            base_query = base_query.no_exclusive()

        # Get the first packages.
        first_fixed = base_query.fixed_only().first()
        first_floating = base_query.floating_only().first()

        fixed.append(first_fixed) if first_fixed else None
        floating.append(first_floating) if first_floating else None

        # Base query for the second package.
        if lead.has_200k_down:
            base_query = base_query.has_200_k()
        if lead.sell_within_three_years:
            base_query = base_query.plan_to_sell_in_3_years()

        if first_fixed:
            second_fixed = base_query.fixed_only().exclude(pk=first_fixed.pk).first()
            fixed.append(second_fixed)
        if first_floating:
            second_floating = base_query.floating_only().exclude(pk=first_floating.pk).first()
            floating.append(second_floating)

        return dict(
            fixed=[item for item in fixed if item],
            floating=[item for item in floating if item]
        )


@extend_schema(tags=["Customer - Lead - Package"], summary="View More Packages", responses=LeadPackageBaseSerializer)
class LeadPackageMoreView(ListAPIView):
    """
    View more packages for a given lead.  \n
    """
    serializer_class = LeadPackageBaseSerializer
    queryset = Package.objects.none()
    filterset_class = LeadPackageFilter
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]

    def get_queryset(self):
        """
         # @todo co-broke queryset.
        """
        # Get the lead and make sure the user has access to the lead.
        lead_id = self.kwargs.get("lead")
        try:
            lead = Lead.objects.filter(
                Q(agent_applied=self.request.user.id) | Q(co_broke_agent=self.request.user.id)
            ).filter(pk=lead_id).prefetch_related("property", "clients").get()
        except Exception:
            raise PermissionDenied(detail="You do not have access to the lead")
        if not hasattr(lead, "property"):
            raise NotFound("This lead does not have a property added to it")

        # Build the common query for filtering.
        query = Package.objects.published_only().common_filter(
            property_type=lead.property.property_type,
            property_status=lead.property.property_status,
            loan_amount=lead.loan_amount,
            loan_category=lead.loan_category,
            excluded_bank=lead.existing_bank
        )
        if lead.has_200k_down:
            query = query.has_200_k()
        if lead.sell_within_three_years:
            query = query.plan_to_sell_in_3_years()
        if self.request.user.membership_type == settings.BASIC_MEMBERSHIP:
            query = query.no_exclusive()

        return query.order_by_rate().all()


@extend_schema(tags=["Customer - Lead - Lead Package"],
               summary="Create a new Lead package",
               request=LeadPackageCreateSerializer, responses=LeadPackageCreateSerializer)
class LeadPackageCreateView(CreateAPIView):
    """
    Add selected packages to the database.  \n
    """
    serializer_class = LeadPackageCreateSerializer

    def create(self, request, *args, **kwargs):
        """
        Overriding the create method to modify the response serializer.
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        lead = serializer.validated_data['lead']

        self.perform_create(serializer)
        # Respond with all the packages added to this lead.
        packages = [p.package for p in lead.packages.all()]
        resp_data = dict(
            lead=lead,
            packages=packages
        )
        res_serializer = LeadPackageCreateSerializer(instance=resp_data)
        return Response(res_serializer.data, status=status.HTTP_201_CREATED)


@extend_schema(tags=["Customer - Lead - Lead Package"], summary="Delete a lead package")
class LeadPackageDeleteView(DestroyAPIView):
    """
    Delete a lead package.
    """

    def perform_destroy(self, instance):
        """
        Once all the packages from a particular bank is deleted, remove \n
        the banker for that bank from the lead as well.
        """
        bank = instance.package.bank
        lead = instance.lead
        super(LeadPackageDeleteView, self).perform_destroy(instance)

    def get_object(self):
        """
        Lead package can be deleted, when
        1. The package is expired.
        2. The package is not expired and has not been submitted.

        Lead must be editable by current user.
        params: lead_id:int
        params: package_id:int
        """
        lead_id = self.kwargs.get("lead_id")
        package_id = self.kwargs.get("package_id")

        # Get the lead.
        lead = Lead.objects.filter(pk=lead_id).with_owner(user=self.request.user).first()
        if not lead:
            raise NotFound(detail="The lead is not found.")
        # Get the leadpackage
        try:
            lead_package = LeadPackage.objects.get(lead=lead, package_id=package_id)
        except:
            raise NotFound(detail="The lead package is not found")

        # If package was already submitted and published, prevent deletion.
        if lead_package.submitted and lead_package.package.publish:
            raise PermissionDenied(detail="This package can't be deleted as it was submitted before.")

        return lead_package


@extend_schema(tags=["Customer - Lead - Lead Package"],
               summary="List of lead package", responses=LeadPackageBaseSerializer)
class LeadPackageListView(ListAPIView):
    """
    Get a list of packages for the given lead.
    """
    pagination_class = None
    serializer_class = LeadPackageBaseSerializer
    queryset = LeadPackage.objects.none()

    def get_queryset(self):
        """
        Make sure the lead belongs to the user.
        params: lead_id:int
        """
        lead = Lead.objects.filter(
            pk=self.kwargs.get("lead_id")
        ).with_owner(user=self.request.user).first()
        if not lead:
            raise NotFound(detail="Lead is not found")
        leadpackages = LeadPackage.objects.filter(lead=lead).prefetch_related("package").all()
        return [l.package for l in leadpackages]
