from drf_spectacular.utils import extend_schema
from rest_framework.exceptions import NotFound, PermissionDenied
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from lead.models import Property, Lead
from lead.serializers.agent import PropertySerializer


@extend_schema(tags=["Customer - Lead - Property"], summary="Add new property for a lead",
               request=PropertySerializer, responses=PropertySerializer)
class PropertyCreateAPIView(CreateAPIView):
    """
    Add property to lead
    """
    serializer_class = PropertySerializer


class PropertyAPIView(APIView):
    serializer_class = PropertySerializer

    def get_object(self, pk):
        try:
            property = Property.objects.get(pk=pk)
            # Make sure user has access to lead.
            lead = Lead.objects.filter(pk=property.lead_id).editable_by(self.request.user).first()
            if not lead or not lead.can_edit:
                raise PermissionDenied(detail="You can't edit this lead.")
            return property
        except Property.DoesNotExist:
            raise NotFound()

    @extend_schema(tags=["Customer - Lead - Property"], summary="Update a Property", request=PropertySerializer)
    def put(self, request, pk):
        """
        Update a property
        """
        property = self.get_object(pk)
        serializer = PropertySerializer(property, data=request.data, context=self.get_renderer_context())
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)
