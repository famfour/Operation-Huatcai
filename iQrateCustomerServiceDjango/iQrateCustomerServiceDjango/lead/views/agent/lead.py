import django_filters
from drf_spectacular.utils import extend_schema
from rest_framework.exceptions import NotFound, PermissionDenied
from rest_framework.generics import ListAPIView, CreateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from lead.filters.agent import LeadFilter
from lead.models import Lead
from lead.serializers.agent import LeadCreateSerializer, LeadDetailSerializer, \
    LeadUpdateSerializer, MarkAsLostSerializer, LeadCSVExportSerializer
from common.serializers import SuccessDetailSerializer, FileResponseSerializer
from lead.utils import export_leads_to_csv


@extend_schema(tags=["Customer - Lead"], summary="Get a list of Leads", responses=LeadDetailSerializer)
class LeadListAPIView(ListAPIView):
    serializer_class = LeadDetailSerializer
    filterset_class = LeadFilter
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    queryset = Lead.objects.none()

    def get_queryset(self):
        user = self.request.user
        return Lead.objects.for_user(user).annotate_can_edit(user).annotate_co_broke_status(user).prefetch_related(
            "clients", "packages", "property").exclude_empty().distinct()


@extend_schema(tags=["Customer - Lead"], summary="Create a new Lead", request=LeadCreateSerializer)
class LeadCreateAPIView(CreateAPIView):
    """
    Create a new lead
    """
    serializer_class = LeadCreateSerializer


class LeadDetailAPIView(APIView):
    """
    Retrieve, update or delete a lead instance.
    """
    serializer_class = LeadCreateSerializer

    def get_object(self, pk):
        """
        1. Filter for the logged in user and lead type.
        2. Prefetch related.
        """
        user = self.request.user
        try:
            return Lead.objects.filter(pk=pk).for_user(user).annotate_can_edit(
                user).annotate_co_broke_status(user).prefetch_related("clients", "packages", "property").get()
        except Lead.DoesNotExist:
            raise NotFound()

    @extend_schema(tags=["Customer - Lead"], summary="Get details of a Lead", responses=LeadDetailSerializer)
    def get(self, request, pk):
        """
        Get the lead
        """
        lead = self.get_object(pk)
        serializer = LeadDetailSerializer(lead, context=self.get_renderer_context())
        return Response(serializer.data)

    @extend_schema(tags=["Customer - Lead"], summary="Update a Lead", request=LeadUpdateSerializer)
    def put(self, request, pk):
        """
        Update the lead
        """
        lead = self.get_object(pk)
        # Check if the user is allowed to edit the lead.
        if not lead.can_edit:
            raise PermissionDenied(detail="You can no longer edit this lead")
        serializer = LeadUpdateSerializer(lead, data=request.data, context=self.get_renderer_context())
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(data=serializer.data)


class MarkAsLostApiView(APIView):
    """
    Mark a lead as lost  \n
    Dropdown values for `reason_for_loss` can be obtained from kvstrore, code - `lead_loss_reason`  \n
    """

    @extend_schema(tags=["Customer - Lead"], summary="Mark Lead as Lost", request=MarkAsLostSerializer,
                   responses=SuccessDetailSerializer)
    def post(self, request):
        serializer = MarkAsLostSerializer(data=request.data, context=self.get_renderer_context())
        serializer.is_valid(raise_exception=True)
        serializer.save()
        success = SuccessDetailSerializer(instance=dict(
            detail="Lead Marked as Lost"
        ))
        return Response(data=success.data)


class LeadExportAPIView(APIView):

    @extend_schema(tags=["Customer - Lead"], summary="Export Leads to CSV",
                   request=LeadCSVExportSerializer, responses=FileResponseSerializer)
    def post(self, request):
        # validate the data
        serializer = LeadCSVExportSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        queryset = Lead.objects.for_user(self.request.user).prefetch_related('clients', 'property')
        if serializer.validated_data.get('lead_ids', None):
            queryset = queryset.filter(pk__in=serializer.validated_data['lead_ids'])

        url = export_leads_to_csv(queryset.all())
        resp_serializer = FileResponseSerializer(instance=dict(
            url=url
        ))
        return Response(data=resp_serializer.data)
