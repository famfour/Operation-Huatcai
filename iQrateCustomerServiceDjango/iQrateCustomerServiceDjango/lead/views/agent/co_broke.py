from drf_spectacular.utils import extend_schema
from rest_framework.response import Response
from rest_framework.exceptions import PermissionDenied

from common.serializers import SuccessResponseSerializer
from lead.models import Lead, Client
from rest_framework.views import APIView
from requests import Session
from django.conf import settings
from rest_framework.exceptions import NotFound
from uuid import UUID
from django.db.models import Q
from kvstore.choices import LeadStatusChoices


@extend_schema(tags=["Lead - Submit Co-Broke"], summary="Submit a lead for co-broke.", responses=SuccessResponseSerializer)
class CoBrokeCreateView(APIView):
    tries = 0

    def _get_co_broker(self, exclude_user_id):
        if self.tries > 5:
            raise PermissionDenied(detail="There are no co-brokers available.")
        session = Session()
        resp = session.put(settings.CO_BROKE_POOL_URL)
        session.close()
        co_broke_agent = UUID(resp.text)
        if co_broke_agent == exclude_user_id:
            self.tries += 1
            self._get_co_broker(exclude_user_id)
        return co_broke_agent

    def get_object(self, lead_id):
        """
        Conditions.
        1. The user has to be the agent_applied
        2. There must be no co broker.
        3. The lead must not have started submission.
        """
        try:
            return Lead.objects.filter(
                Q(agent_applied=self.request.user.id) & Q(co_broke_agent__isnull=True) & Q(lead_status=LeadStatusChoices.pending)
            ).get(pk=lead_id)
        except Lead.DoesNotExist:
            raise NotFound(detail="You do not have access to this lead.")

    def post(self, request, lead_id):
        # Get the lead.
        lead = self.get_object(lead_id=lead_id)

        # Make sure the PDPA for all clients were approved and a property is added to the lead.
        if Client.objects.filter(lead=lead, pdpa_status=False).exists():
            raise PermissionDenied(detail="All clients has to verify PDPA before the lead can submitted for co-broke.")

        # Get the co-broker.
        lead.co_broke_agent = self._get_co_broker(exclude_user_id=lead.agent_applied)
        lead.save()

        # Return the response
        serializer = SuccessResponseSerializer(instance=dict(
            message=f"Co-Broke request sent"
        ))
        return Response(data=serializer.data)
