from django.db.models import Q, ProtectedError
from drf_spectacular.utils import extend_schema
from rest_framework.exceptions import NotFound, PermissionDenied
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.status import HTTP_202_ACCEPTED

from common.responses import ProtectedInstanceResponse
from lead.models import Client, Lead
from lead.serializers.agent import ClientDetailSerializer, ClientCreateSerializer
from common.serializers import SuccessDetailSerializer
from kvstore.choices import LeadStatusChoices


@extend_schema(tags=["Customer - Lead - Client"], summary="Create a new Client for a Lead",
               request=ClientCreateSerializer, responses=ClientDetailSerializer)
class ClientCreateAPIView(CreateAPIView):
    """
    PDPA SMS will be sent to client as soon as the client is created
    """
    serializer_class = ClientCreateSerializer


class ClientDetailAPIView(APIView):
    """
    Retrieve, update or delete a client instance.
    """
    serializer_class = ClientDetailSerializer

    def get_object(self, pk):
        """
        Make sure the logged in user has permission to access the client.
        @todo - Add the ACL here
        """
        try:
            user_id = self.request.user.id
            return Client.objects.filter(
                Q(lead__agent_applied=user_id) | Q(lead__co_broke_agent=user_id)
            ).filter(pk=pk).prefetch_related("lead").get()
        except Client.DoesNotExist:
            raise NotFound()

    @extend_schema(tags=["Customer - Lead - Client"], summary="Get details of a Client")
    def get(self, request, pk):
        client = self.get_object(pk)
        serializer = ClientDetailSerializer(client)
        return Response(serializer.data)

    @extend_schema(tags=["Customer - Lead - Client"], summary="Update a Client")
    def put(self, request, pk):
        client = self.get_object(pk)
        serializer = ClientDetailSerializer(client, data=request.data, context=self.get_renderer_context())
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(data=serializer.data)

    @extend_schema(tags=["Customer - Lead - Client"], summary="Delete a Client")
    def delete(self, request, pk):
        """
        Only co applicants can be deleted.
        """
        client = self.get_object(pk)
        # Make sure the agent can edit the lead.
        lead = Lead.objects.filter(pk=client.lead_id).annotate_can_edit(self.request.user).first()
        if not lead:
            raise PermissionDenied(detail="You can no longer edit this lead.")
        if lead.lead_status == LeadStatusChoices.rework:
            raise PermissionDenied(detail="This client cannot be deleted")
        # Main applicant cannot be deleted
        if client.main_applicant:
            raise PermissionDenied(detail="Main applicant cannot be deleted")
        client_name = client.name
        try:
            client.delete()
        except ProtectedError:
            return ProtectedInstanceResponse(instance=client)
        serializer = SuccessDetailSerializer(instance=dict(
            detail=f"The lead({client_name}) deleted successfully"
        ))
        return Response(status=HTTP_202_ACCEPTED, data=serializer.data)
