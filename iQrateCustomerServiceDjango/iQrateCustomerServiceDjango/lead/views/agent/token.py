from django.db.models import Q
from drf_spectacular.utils import extend_schema
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from lead.models import Client
from lead.serializers.agent import ClientPDPATokenSerializer
from lead.utils import TwilioException, PDPAException, send_pdpa_message


class GetClientToken(APIView):
    serializer_class = ClientPDPATokenSerializer

    @extend_schema(tags=["Customer - Lead - PDPA"], summary="Get a new token for PDPA ")
    def get(self, request, client_id):
        """
        Send PDPA Confirmation URL via  SMS to clients.
        """
        # Get the client and make sure the client belongs to the agent.
        # @todo Add the ACL here.
        client = Client.objects.filter(
            Q(lead__agent_applied=self.request.user.id) | Q(lead__co_broke_agent=self.request.user.id)
        ).filter(pk=client_id).first()

        # If no client found
        if not client:
            return Response(status=status.HTTP_404_NOT_FOUND, data=dict(
                detail="No client found or you don't have access to the client."
            ))

        try:
            token = send_pdpa_message(client)
        except PDPAException as e:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=dict(detail=str(e)))
        except TwilioException as e:
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data=dict(detail=str(e)))

        serializer = ClientPDPATokenSerializer(dict(
            client=client_id,
            message="PDPA SMS sent to lead",
            status=token.client.pdpa_status,
            retry_by=int(token.retry_by.timestamp())
        ))
        return Response(serializer.data, status=status.HTTP_200_OK)
