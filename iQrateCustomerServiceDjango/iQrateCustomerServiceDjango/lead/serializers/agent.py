from django.conf import settings
from django.db import IntegrityError
from django.db.models import Q
from django.utils import timezone
from drf_spectacular.utils import extend_schema_field
from rest_framework import serializers
from rest_framework.exceptions import PermissionDenied

from bank.models import Bank, Banker
from bank.models import Package
from bank.serializers.agent import CustomerPackageRateSerializer, CustomerBankSerializer
from common.serializers import CommonModelSerializer
from kvstore.choices import LeadStatusChoices
from kvstore.models import KVStore
from lead.models import Client, Lead, LeadPackage, Property
from lead.validators import ValidateLeadAccess, ValidatePostalCode, ValidateAgentLeadType, ConfirmPDPAApproval, \
    ConfirmMainApplicantCount, ConfirmPDPAMainApplicant, ConfirmUniquePhoneNumber, LeadPDPAValidation, ValidateCanAddLeadPackages, \
    ValidateLeadOwnership
from submission.models import LeadBanker
from user.models import User
from user.serializer import UserSerializer
from payout.serializers.agent import AgentPayoutDetailSerializer


class PropertySerializer(CommonModelSerializer):
    lead = serializers.PrimaryKeyRelatedField(
        queryset=Lead.objects.all(),
        validators=[ValidateLeadAccess()]
    )
    postal_code = serializers.CharField(validators=[ValidatePostalCode()], allow_blank=True, allow_null=True)

    class Meta:
        model = Property
        fields = ['id', 'lead', 'property_type', 'purchase_price', 'property_status', 'postal_code',
                  'street_name', 'unit_number', 'project_name']
        read_only_fields = ['created_by', 'updated_by', 'created', 'updated']
        validators = [
            LeadPDPAValidation()
        ]


class ClientHyperlinkedSerializer(serializers.HyperlinkedModelSerializer):
    """
    Clients from this lead.
    """
    lead = serializers.PrimaryKeyRelatedField(queryset=Lead.objects.all())
    nationality = serializers.ChoiceField(choices=KVStore.get_choices("nationality"), required=False)

    class Meta:
        model = Client
        fields = ['id', 'lead', 'name', 'country_code', 'phone_number', 'main_applicant', 'pdpa_status',
                  'email', 'annual_income', 'dob', 'nationality', 'street_name', 'block_number', 'unit_number',
                  'project_name', 'postal_code', 'created_by', 'updated_by', 'created', 'updated']
        read_only_fields = ['created_by', 'updated_by', 'created', 'updated', 'pdpa_status']


class ClientCreateSerializer(CommonModelSerializer):
    """
    Serializer to create clients.
    Only includes field that can be added pre pdpa
    """
    lead = serializers.PrimaryKeyRelatedField(
        queryset=Lead.objects.all(),
        validators=[ValidateLeadAccess()],
        required=True
    )

    class Meta:
        model = Client
        fields = ['id', 'lead', 'name', 'country_code', 'phone_number', 'main_applicant', 'pdpa_status', 'email']
        read_only_fields = ['pdpa_status']
        validators = [
            ValidateAgentLeadType(),
            ConfirmMainApplicantCount(),
            ConfirmPDPAMainApplicant(),
            ConfirmUniquePhoneNumber()
        ]


class ClientDetailSerializer(CommonModelSerializer):
    """
    Serializer to update and list clients.
    """
    lead = serializers.PrimaryKeyRelatedField(
        queryset=Lead.objects.all(),
        required=True,
        validators=[ValidateLeadAccess()]
    )
    nationality = serializers.ChoiceField(choices=KVStore.get_choices("nationality"), required=False, allow_null=True)
    postal_code = serializers.CharField(validators=[ValidatePostalCode()], required=False, allow_null=True)

    class Meta:
        model = Client
        fields = ['id', 'lead', 'name', 'country_code', 'phone_number', 'main_applicant', 'pdpa_status',
                  'email', 'annual_income', 'dob', 'nationality', 'street_name', 'block_number', 'unit_number',
                  'project_name', 'postal_code', 'created_by', 'updated_by', 'created', 'updated']
        read_only_fields = ['created_by', 'updated_by', 'created', 'updated', 'pdpa_status']
        validators = [
            ValidateAgentLeadType(),
            ConfirmPDPAApproval(),
            ConfirmMainApplicantCount(),
            ConfirmPDPAMainApplicant(),
            ConfirmUniquePhoneNumber()
        ]


class LeadPackageDetailSerializer(CommonModelSerializer):
    class Meta:
        model = LeadPackage
        fields = ['lead', 'package', 'data']
        read_only_fields = ['created_by', 'updated_by', 'created', 'updated']


class LeadPackageCreateSerializer(CommonModelSerializer):
    lead = serializers.PrimaryKeyRelatedField(
        queryset=Lead.objects.all(),
        validators=[ValidateCanAddLeadPackages()]
    )
    packages = serializers.ListField(child=serializers.PrimaryKeyRelatedField(queryset=Package.objects.all()))

    def save(self, **kwargs):
        """
        Save the current package details as json in the data field as there is a chance that the package could be \n
        updated after user submission.
        """
        packages = self.validated_data["packages"]
        lead = self.validated_data["lead"]

        # get the agent applied.
        try:
            agent_applied = User.objects.get(id=lead.agent_applied)
        except:
            raise PermissionDenied(detail="Applied agent account is deleted!")

        # Create the leadpackages.
        for package in packages:
            if LeadPackage.objects.filter(lead=lead, package=package).exists():
                continue
            LeadPackage.objects.create(lead=lead, package=package, data=package.data())
            # Update the submission status for the lead banker and agent subscription data
            LeadBanker.objects.filter(lead=lead, banker__bank=package.bank).update(
                email_to_lead=False,
                email_to_bank=False,
                agent_data=agent_applied.get_rates()
            )

        # Get the unique banks
        banks = list(set([package.package.bank for package in lead.packages.all()]))

        # If there is already a banker from one of these banks attached to this lead, then use the same banker. \n
        # And reset the submission status.
        for bank in banks:
            lead_banker = LeadBanker.objects.filter(lead=lead, banker__bank=bank).first()
            if lead_banker:
                banks.remove(bank)

        now = timezone.localtime()
        # Find one banker for each of the banks.
        for bank in banks:
            try:
                # Find a banker who (does not have colliding block date ) & (last_assignment older than others.)
                banker = Banker.objects.exclude(
                    Q(blocked_dates__block_from__gte=now.date()) & Q(blocked_dates__block_from__lte=now.date())
                ).filter(bank=bank, status=True).order_by("last_assignment").first()
            except Banker.DoesNotExist:
                # Delete all the packages added for the lead from this bank.
                lead.packages.filter(bank=bank).all().delete()
                # Raise the exception
                raise serializers.ValidationError(
                    detail=f"There are no bankers available for {bank.name}",
                    code="packages"
                )
            try:
                LeadBanker.objects.create(
                    banker=banker,
                    lead=lead,
                    agent_data=agent_applied.get_rates()
                )
                # Add the last_assignment to the banker.
                banker.last_assignment = now
                banker.save()
            except IntegrityError:  # If the banker exists for the lead.
                continue
        return lead

    class Meta:
        model = LeadPackage
        fields = ['lead', 'packages']
        read_only_fields = ['created_by', 'updated_by', 'created', 'updated', 'data', "package"]
        validators = [
            LeadPDPAValidation()
        ]


class LeadDetailSerializer(CommonModelSerializer):
    lead_type = serializers.ChoiceField(choices=KVStore.get_choices("lead_type"))
    lead_status = serializers.ChoiceField(choices=KVStore.get_choices("lead_status"))
    loan_category = serializers.ChoiceField(choices=KVStore.get_choices("loan_category"))

    clients = ClientHyperlinkedSerializer(many=True)
    packages = LeadPackageDetailSerializer(many=True)
    property = PropertySerializer()
    existing_bank = CustomerBankSerializer()

    co_broke_agent = serializers.SerializerMethodField(read_only=True)

    document_drawer_url = serializers.SerializerMethodField(
        read_only=True,
        allow_null=True,
        help_text="Agent can share this URL with the customer for them to upload files.")
    document_drawer_id = serializers.UUIDField(
        source='document_drawer.id',
        help_text="ID of the document drawer."
    )
    can_edit = serializers.BooleanField(read_only=True)
    co_broke_status = serializers.CharField(read_only=True)
    payout = AgentPayoutDetailSerializer()
    payout_amount = serializers.SerializerMethodField(default=0.0)

    # @todo - Loan amount should change when the lead is won.

    @extend_schema_field(field=serializers.FloatField)
    def get_payout_amount(self, obj):
        user = self.context['request'].user
        if not getattr(obj, "payout", None):
            return 0.0
        if obj.agent_applied == user.id:
            return obj.payout.details.broker_commission

        if obj.co_broke_agent == user.id:
            return obj.payout.details.co_broker_commission
        return 0.0

    @extend_schema_field(field=serializers.URLField)
    def get_document_drawer_url(self, obj):
        """
        Create an absolute URL from the document drawer ID
        """
        return f"{settings.UI_HOSTNAME}/magic/ui/drawer/{obj.document_drawer.id}"

    @extend_schema_field(field=UserSerializer)
    def get_co_broke_agent(self, obj):
        """
        For the agent_applied display co_broke_agent
        For co_broke_agent display the agent applied
        """
        current_user = self.context['request'].user
        if current_user.id == obj.agent_applied:
            user = User.objects.filter(id=obj.co_broke_agent).first()
        else:
            user = User.objects.filter(id=obj.agent_applied).first()
        return UserSerializer(instance=user).data

    class Meta:
        model = Lead
        fields = ['id', 'lead_type', 'lead_status', 'reason_for_loss', 'agent_applied', 'co_broke_agent', 'sell_within_three_years',
                  'has_200k_down', 'loan_category', 'loan_amount', 'existing_bank', 'current_interest_rate',
                  'clients', 'packages', 'property', 'document_drawer_url', 'can_edit', 'co_broke_status', 'payout',
                  'payout_amount', 'document_drawer_id', 'created_by', 'updated_by', 'created', 'updated']
        read_only_fields = ['created_by', 'updated_by', 'created', 'updated']


class LeadCreateSerializer(CommonModelSerializer):
    lead_type = serializers.ChoiceField(choices=KVStore.get_choices("lead_type"))

    def create(self, validated_data):
        instance = super(LeadCreateSerializer, self).create(validated_data)
        instance.lead_status = LeadStatusChoices.pending
        # Set the current user as the agent_applied.
        instance.agent_applied = self.context['request'].user.id
        instance.save()
        return instance

    class Meta:
        model = Lead
        fields = ['id', 'lead_type', 'agent_applied', 'created_by', 'updated_by', 'created', 'updated']
        read_only_fields = ['id', 'created_by', 'updated_by', 'created', 'updated']


class LeadUpdateSerializer(CommonModelSerializer):
    lead_type = serializers.ChoiceField(choices=KVStore.get_choices("lead_type"))
    loan_category = serializers.ChoiceField(choices=KVStore.get_choices("loan_category"))
    existing_bank = serializers.PrimaryKeyRelatedField(
        queryset=Bank.objects.all(), required=False, default=None, allow_null=True)
    can_edit = serializers.BooleanField(read_only=True)
    co_broke_status = serializers.CharField(read_only=True)

    def validate(self, attrs):
        # Make sure there is a main client and PDPA is validated.
        try:
            Client.objects.filter(
                main_applicant=True, pdpa_status=True, lead=self.instance
            ).get()
        except Client.DoesNotExist:
            raise PermissionDenied(detail="Main applicant need to verify PDPA first.")
        return attrs

    class Meta:
        model = Lead
        fields = ['id', 'lead_type', 'agent_applied', 'co_broke_agent', 'sell_within_three_years',
                  'has_200k_down', 'loan_category', 'loan_amount', 'existing_bank', 'can_edit', 'co_broke_status',
                  'current_interest_rate', 'created_by', 'updated_by', 'created', 'updated', ]
        read_only_fields = ['created_by', 'updated_by', 'created', 'updated', 'agent_applied',
                            'co_broke_agent']


class ClientPDPATokenSerializer(serializers.Serializer):
    client = serializers.IntegerField(read_only=True)
    message = serializers.CharField(default='', read_only=True)
    status = serializers.BooleanField(default=False, read_only=True)
    retry_by = serializers.IntegerField(read_only=True)


class PDPATokenVerifySerializer(serializers.Serializer):
    token = serializers.CharField()

    class Meta:
        fields = ['token']


class LeadPackageBaseSerializer(serializers.ModelSerializer):
    """
   BaseResponse schema for packages generated for a lead.
    """
    bank = serializers.SerializerMethodField()
    rates = CustomerPackageRateSerializer(many=True)

    @extend_schema_field(field=CustomerBankSerializer)
    def get_bank(self, obj):
        if obj.mask:
            return CustomerBankSerializer(instance=obj.bank, masked=True).data
        return CustomerBankSerializer(instance=obj.bank).data

    class Meta:
        model = Package
        fields = ['id', 'bank', 'mask', 'rate_category', 'rate_type',
                  'property_types', 'property_status', 'loan_category', 'lock_in_period', 'min_loan_amount',
                  'deposit_to_place', 'valuation_subsidy', 'fire_insurance_subsidy', 'cash_rebate_legal_subsidy',
                  'cash_rebate_subsidy_clawback_period_years', 'partial_repayment_penalty',
                  'partial_repayment_penalty_remarks', 'full_repayment_penalty', 'full_repayment_penalty_remarks', 'rates',
                  'cancellation_fee', 'cancellation_fee_remarks', 'deposit_to_place_remarks', 'remarks_for_client',
                  'interest_offsetting', 'interest_reset_date', 'processing_fee', 'remarks_for_broker', 'new_purchase_referral_fee',
                  'refinance_referral_fee', 'publish', 'created_by', 'updated_by', 'created', 'updated']


class LeadPackageGroupSerializer(serializers.Serializer):
    """
    Response class for the first 4 packages for the lead.
    """
    fixed = LeadPackageBaseSerializer(many=True, read_only=True)
    floating = LeadPackageBaseSerializer(many=True, read_only=True)


class MarkAsLostSerializer(serializers.Serializer):
    lead = serializers.PrimaryKeyRelatedField(
        queryset=Lead.objects.all(),
        validators=[
            ValidateLeadOwnership()
        ]
    )
    reason_for_loss = serializers.ChoiceField(
        choices=KVStore.get_choices("lead_loss_reason"), required=False,
        help_text="From KVStore - 'lead_loss_reason'"
    )

    def validate(self, attrs):
        lead = attrs['lead']
        if lead.lead_status == LeadStatusChoices.won:
            raise PermissionDenied(detail="Won leads cannot be set to lost")
        return attrs

    def save(self, **kwargs):
        lead = self.validated_data['lead']
        lead.lead_status = LeadStatusChoices.lost
        lead.reason_for_loss = self.validated_data.get('reason_for_loss', None)
        lead.save()


class LeadCSVExportSerializer(serializers.Serializer):
    lead_ids = serializers.ListField(
        child=serializers.IntegerField(),
        allow_null=True,
        allow_empty=True,
        help_text="List of lead ids selected, if none selected empty array"
    )