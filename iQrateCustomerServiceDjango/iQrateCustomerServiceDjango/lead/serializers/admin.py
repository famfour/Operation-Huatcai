import django_filters
from drf_spectacular.utils import extend_schema_field
from rest_framework import serializers
from lead.models import Lead, Client, Property
from user.serializer import UserSerializer, AdminUserSerializer
from bank.serializers.admin import BankHyperlinkedSerializer, BankerSerializer
from user.models import User
from payout.models import Payout, PayoutDetail, Loan, LoanRate
from magic.serializers.admin import AdminDocumentDrawerDetailSerializer


class AdminLoanRateDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = LoanRate
        exclude = ['created_by', 'updated_by', 'created', 'updated', 'loan']


class AdminLoanDetailSerializer(serializers.ModelSerializer):
    rates = AdminLoanRateDetailSerializer(many=True)

    class Meta:
        model = Loan
        exclude = ['created_by', 'updated_by', 'created', 'updated', 'payout']


class AdminPayoutDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = PayoutDetail
        exclude = ['created_by', 'updated_by', 'created', 'updated']


class AdminPayoutSerializer(serializers.ModelSerializer):
    bank = BankHyperlinkedSerializer()
    banker = BankerSerializer()
    details = AdminPayoutDetailSerializer()
    loans = AdminLoanDetailSerializer(many=True)

    class Meta:
        model = Payout
        exclude = ['created_by', 'updated_by', 'created', 'updated']


class AdminClientDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        exclude = ['created_by', 'updated_by', 'created', 'updated']


class AdminPropertyDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Property
        exclude = ['created_by', 'updated_by', 'created', 'updated']


class AdminLeadListSerializer(serializers.ModelSerializer):
    lead_name = serializers.SerializerMethodField()
    lead_phone = serializers.SerializerMethodField()
    loan_amount = serializers.SerializerMethodField()
    lead_status = serializers.CharField()
    bank = serializers.CharField(read_only=True, allow_null=True)
    banker = serializers.CharField(read_only=True, allow_null=True)
    banker_email = serializers.EmailField(read_only=True, allow_null=True)
    agent_applied = serializers.SerializerMethodField(allow_null=True)
    co_broker = serializers.SerializerMethodField(allow_null=True)
    payout_amount = serializers.FloatField(allow_null=True)
    payout_status = serializers.CharField(read_only=True, allow_null=True)
    payout_date = serializers.DateField(allow_null=True)
    law_firm = serializers.CharField(allow_null=True)

    @extend_schema_field(field=serializers.CharField())
    def get_lead_name(self, obj):
        client = obj.clients.first()
        try:
            return client.name
        except:
            return " - "

    @extend_schema_field(field=serializers.CharField())
    def get_lead_phone(self, obj):
        client = obj.clients.first()
        try:
            return f"+{client.country_code} {client.phone_number}"
        except:
            return " - "

    @extend_schema_field(field=serializers.FloatField())
    def get_loan_amount(self, obj):
        if obj.approved_loan_amount:
            return obj.approved_loan_amount
        return obj.loan_amount

    @extend_schema_field(field=UserSerializer())
    def get_co_broker(self, obj):
        if not obj.co_broke_agent:
            return None
        user = User.objects.filter(id=obj.co_broke_agent).first()
        if not user:
            return None
        return UserSerializer(instance=user).data

    @extend_schema_field(field=UserSerializer())
    def get_agent_applied(self, obj):
        user = User.objects.filter(id=obj.agent_applied).first()
        if not user:
            return None
        return UserSerializer(instance=user).data

    class Meta:
        model = Lead
        fields = ['created', 'lead_name', 'lead_phone', 'loan_amount', 'lead_status', 'bank', 'banker', 'banker_email',
                  'agent_applied', 'co_broker', 'payout_amount', 'payout_status', 'payout_date', 'law_firm', 'id']


class AdminLeadDetailSerializer(serializers.ModelSerializer):
    clients = AdminClientDetailSerializer(many=True)
    property = AdminPropertyDetailSerializer()
    agent_applied = serializers.SerializerMethodField()
    co_broke_agent = serializers.SerializerMethodField()
    payout = AdminPayoutSerializer()
    document_drawer = AdminDocumentDrawerDetailSerializer()

    @extend_schema_field(field=AdminUserSerializer)
    def get_agent_applied(self, obj):
        user = User.objects.get(id=obj.agent_applied)
        return AdminUserSerializer(instance=user).data

    @extend_schema_field(field=AdminUserSerializer)
    def get_co_broke_agent(self, obj):
        if not obj.co_broke_agent:
            return None
        user = User.objects.get(id=obj.co_broke_agent)
        return AdminUserSerializer(instance=user).data

    class Meta:
        model = Lead
        exclude = ['created_by', 'updated_by', 'created', 'updated']


class AdminLeadCSVExportSerializer(serializers.Serializer):
    lead_ids = serializers.ListField(
        child=serializers.IntegerField(),
        allow_null=True,
        allow_empty=True,
        help_text="List of lead ids selected, if none selected empty array"
    )
