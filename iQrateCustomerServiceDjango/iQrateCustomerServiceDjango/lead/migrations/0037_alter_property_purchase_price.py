# Generated by Django 4.0.5 on 2022-07-02 15:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lead', '0036_alter_client_unit_number_alter_property_unit_number'),
    ]

    operations = [
        migrations.AlterField(
            model_name='property',
            name='purchase_price',
            field=models.FloatField(blank=True, null=True),
        ),
    ]
