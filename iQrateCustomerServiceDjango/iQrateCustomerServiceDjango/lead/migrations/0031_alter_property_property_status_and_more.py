# Generated by Django 4.0.4 on 2022-05-25 19:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lead', '0030_alter_property_project_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='property',
            name='property_status',
            field=models.CharField(choices=[('completed', 'Completed'), ('under_construction_to_obtain_top_within_2_years', 'Under-construction (To Obtain TOP within 2 years)'), ('under_construction_to_obtain_top_more_than_2_years', 'Under-construction (To Obtain TOP more than 2 years)')], max_length=128),
        ),
        migrations.AlterField(
            model_name='property',
            name='property_type',
            field=models.CharField(choices=[('hdb', 'HDB'), ('condominium_apartment', 'Condominium/ Apartment'), ('strata_housing_townhouses', 'Strata Housing/ Townhouses'), ('ec_resale_out_of_mop', 'EC (Resale/ out of MOP)'), ('ec_from_developer_within_mop', 'EC (From Developer/ within MOP)'), ('landed', 'Landed')], max_length=128),
        ),
    ]
