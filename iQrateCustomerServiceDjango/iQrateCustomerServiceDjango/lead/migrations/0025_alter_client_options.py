# Generated by Django 4.0.3 on 2022-04-21 09:35

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lead', '0024_remove_approvedpackage_won_lead_approvedpackage_lead_and_more'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='client',
            options={'ordering': ['id']},
        ),
    ]
