# Generated by Django 4.0.5 on 2022-06-27 13:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lead', '0035_alter_property_unit_number'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='unit_number',
            field=models.CharField(blank=True, max_length=64, null=True),
        ),
        migrations.AlterField(
            model_name='property',
            name='unit_number',
            field=models.CharField(blank=True, max_length=32, null=True),
        ),
    ]
