# Generated by Django 4.0.4 on 2022-05-25 10:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lead', '0029_alter_property_purchase_price'),
    ]

    operations = [
        migrations.AlterField(
            model_name='property',
            name='project_name',
            field=models.CharField(blank=True, max_length=254, null=True),
        ),
    ]
