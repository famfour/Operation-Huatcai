# Generated by Django 4.0.3 on 2022-04-01 11:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lead', '0020_remove_lead_lead_status_approvedpackage_won_lead'),
    ]

    operations = [
        migrations.AddField(
            model_name='lead',
            name='lead_status',
            field=models.CharField(default='', max_length=128),
            preserve_default=False,
        ),
    ]
