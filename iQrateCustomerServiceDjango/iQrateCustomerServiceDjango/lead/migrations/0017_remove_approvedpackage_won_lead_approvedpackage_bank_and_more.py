# Generated by Django 4.0.3 on 2022-04-01 09:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bank', '0002_remove_banker_phone_banker_country_code_and_more'),
        ('lawfirm', '0001_initial'),
        ('lead', '0016_property_created_property_created_by_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='approvedpackage',
            name='won_lead',
        ),
        migrations.AddField(
            model_name='approvedpackage',
            name='bank',
            field=models.ForeignKey(default='', on_delete=django.db.models.deletion.PROTECT, related_name='approved_package', to='bank.bank'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='approvedpackage',
            name='banker',
            field=models.ForeignKey(default='', on_delete=django.db.models.deletion.PROTECT, related_name='approved_package', to='bank.banker'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='approvedpackage',
            name='law_firm',
            field=models.ForeignKey(default='', on_delete=django.db.models.deletion.PROTECT, related_name='approved_package', to='lawfirm.lawfirm'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='approvedpackage',
            name='legal_fees',
            field=models.FloatField(null=True),
        ),
        migrations.AddField(
            model_name='approvedpackage',
            name='loan_acceptance_date',
            field=models.DateField(null=True),
        ),
        migrations.AddField(
            model_name='approvedpackage',
            name='rate_type',
            field=models.CharField(default='', max_length=128),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='lead',
            name='lead_status',
            field=models.CharField(default='', max_length=128),
            preserve_default=False,
        ),
    ]
