from django.db.models import QuerySet, When, Q, Case, Value, BooleanField, CharField

from kvstore.choices import LeadTypeChoices, LeadStatusChoices


class LeadQuerySet(QuerySet):
    """
    Custom queryset for Lead.
    """

    def exclude_empty(self):
        return self.exclude(clients__isnull=True)

    def annotate_can_add_package(self, user):
        """
        Make sure the current user can add packages to the lead. Conditions are.
        1. The user is the co-broker
        2. The lead type is yourself and user is the applied agent and no co broke
        3. The lead type is others and there is no co broke.
        4. Lead type co broke and the user is a co-broke.
        5. Lead status does not matter
        """
        return self.annotate(
            can_add_package=Case(
                When(
                    Q(co_broke_agent=user.id),
                    then=Value(True)
                ),
                When(
                    Q(lead_type__in=[
                        LeadTypeChoices.yourself,
                        LeadTypeChoices.others
                    ]) & Q(agent_applied=user.id) & Q(co_broke_agent__isnull=True),
                    then=Value(True)
                ),
                default=Value(False),
                output_field=BooleanField()
            )
        )

    def annotate_can_access_document_drawer(self, user):
        """
        Agent can access document drawer if -
        1. The agent is co_broke_agent.
        2. The Agent is the agent_applied and there is no co-broker.
        3. The lead_type is yourself and the agent_applied is the current user.
        """
        return self.annotate(
            can_access_document_drawer=Case(
                When(
                    Q(co_broke_agent=user.id),
                    then=Value(True)
                ),
                When(
                    Q(lead_type=LeadTypeChoices.yourself) & Q(agent_applied=user.id),
                    then=Value(True)
                ),
                When(
                    Q(agent_applied=user.id) & Q(co_broke_agent__isnull=True),
                    then=Value(True)
                ),
                default=Value(False),
                output_field=BooleanField()
            )
        )

    def annotate_can_edit(self, user):
        """
        Annotate can_edit=True|False to the queryset.
        Users can update the Lead only when.
        1. The current user is the 'agent_applied' and there is no 'co_broke_agent' for the lead & lead status is 'pending'
        Or
        2. The current user is the 'co_broke_agent' for the lead & lead status is 'pending'.
        """
        return self.annotate(
            can_edit=Case(
                When(
                    Q(agent_applied=user.id) & Q(co_broke_agent__isnull=True) & Q(lead_status__in=[LeadStatusChoices.pending, LeadStatusChoices.rework]),
                    then=Value(True)
                ),
                When(
                    Q(co_broke_agent=user.id) & Q(lead_status__in=[LeadStatusChoices.pending, LeadStatusChoices.rework]), then=Value(True)
                ),
                default=Value(False),
                output_field=BooleanField()
            )
        )

    def annotate_co_broke_status(self, user):
        """
        Annotate 'co_broke_status' text for display in lead details.
        When there is no status the field will be an empty string.
        # [('yourself', 'Yourself'), ('co_broke', 'Co-Broke'), ('others', 'Others')]
        Conditions.
            1. When the 'lead_type' is 'yourself' or 'co_broke' and there is no 'co_broke_agent' and lead_status=pending and agent_applied is the
                current user, then -> "Please Submit"
            2. When there is a 'co_broke_agent' then -> Co-Broke Sent
        """
        return self.annotate(
            co_broke_status=Case(
                When(
                    Q(lead_type__in=[LeadTypeChoices.yourself, LeadTypeChoices.co_broke])
                    & Q(co_broke_agent__isnull=True)
                    & Q(lead_status=LeadStatusChoices.pending)
                    & Q(agent_applied=user.id),
                    then=Value("Please Submit")
                ),
                When(
                    Q(co_broke_agent__isnull=False) & Q(agent_applied=user.id), then=Value("Co-Broke Sent")
                ),
                When(Q(co_broke_agent=user.id), then=Value("Co-Broke Received")),
                default=Value(""),
                output_field=CharField()
            )
        )

    def with_owner(self, user):
        """
        This method checks whether the user is the owner of the lead, base on conditions.
        1. The current user is the 'agent_applied' and there is no 'co_broke_agent'
        2. The current user is the co-broker for the lead.
        """
        return self.filter(Q(agent_applied=user.id, co_broke_agent__isnull=True) | Q(co_broke_agent=user.id))

    def for_user(self, user):
        """
        Return all leads where the current user is either 'agent_applied' or 'co_broke_agent'
        """
        return self.filter(
            Q(agent_applied=user.id) | Q(co_broke_agent=user.id)
        )

    def editable_by(self, user):
        """
        Filter out all the un editable leads for the user.
        Useful for validating leads in serializers.
        """
        return self.annotate_can_edit(user=user).filter(can_edit=True)
