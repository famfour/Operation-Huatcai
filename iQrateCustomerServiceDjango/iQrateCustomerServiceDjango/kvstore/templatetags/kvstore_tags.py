from django import template
from django.utils.safestring import mark_safe

from kvstore.models import KVStore

register = template.Library()


@register.simple_tag()
def display_value(code, key):
    value = KVStore.get_display_value(code, key)
    return mark_safe(value)
