from django.db.models import TextChoices


class RateTypeChoices(TextChoices):
    fixed = ('fixed', "Fixed")
    sora = ('sora', "SORA")
    board = ("board", "Board")
    fixed_deposit = ("fixed_deposit", "Fixed Deposit")
    sibor = ('sibor', 'SIBOR')


class EquationChoices(TextChoices):
    plus = ("+", "+")
    minus = ("-", "-")
    equals = ("=", "=")


class RateCategoryChoices(TextChoices):
    standard = ("standard", "Standard")
    exclusive = ('exclusive', 'Exclusive')


class PropertyTypeChoices(TextChoices):
    hdb = ("hdb", "HDB")
    condominium_apartment = ("condominium_apartment", "Condominium/ Apartment")
    strata_housing_townhouses = ("strata_housing_townhouses", "Strata Housing/ Townhouses")
    ec_resale_out_of_mop = ("ec_resale_out_of_mop", "EC (Resale/ out of MOP)")
    ec_from_developer_within_mop = ("ec_from_developer_within_mop", "EC (From Developer/ within MOP)")
    landed = ("landed", "Landed")


class PropertyStatusChoices(TextChoices):
    completed = ("completed", "Completed")
    under_construction_to_obtain_top_within_2_years = (
        "under_construction_to_obtain_top_within_2_years", "Under-construction (To Obtain TOP within 2 years)")
    under_construction_to_obtain_top_more_than_2_years = (
        "under_construction_to_obtain_top_more_than_2_years", "Under-construction (To Obtain TOP more than 2 years)")


class LoanCategoryChoices(TextChoices):
    new_purchase_with_option_to_purchase = ("new_purchase_with_option_to_purchase", "New Purchase (With Option to Purchase)")
    new_purchase_approval_in_principle = ("new_purchase_approval_in_principle", "New Purchase (Approval in Principle)")
    refinance = ("refinance", "Refinance")
    cash_out_term_loan = ("cash_out_term_loan", "Cash Out (Term Loan)")
    part_purchase_decoupling_divorce = ("part_purchase_decoupling_divorce", "Part Purchase/ Decoupling/ Divorce")


class NationalityChoices(TextChoices):
    sg_citizens = ("Singapore Citizens", "Singapore Citizens")
    sg_prs = ("Singapore Permanent Residents", "Singapore Permanent Residents")
    iceland_citizens_pr = ("Iceland Citizens / Permanent Residents", "Iceland Citizens / Permanent Residents")
    liechtenstein_citizens_pr = ("Liechtenstein Citizens / Permanent Residents", "Liechtenstein Citizens / Permanent Residents")
    norway_citizens_pr = ("Norway Citizens / Permanent Residents", "Norway Citizens / Permanent Residents")
    switzerland_citizens_pr = ("Switzerland Citizens / Permanent Residents", "Switzerland Citizens / Permanent Residents")
    usa_citizens_pr = ("USA Citizens", "USA Citizens")
    others = ("Others", "Others")


class LeadTypeChoices(TextChoices):
    yourself = ("yourself", "Yourself")
    co_broke = ("co_broke", "Co-Broke")
    others = ("others", "Others")


class LeadStatusChoices(TextChoices):
    pending = ("pending", "Pending")
    submitted = ("submitted", "Submitted")
    won = ("won", "Won")
    lost = ("lost", "Lost")
    rework = ("rework", "Rework")


class CoBrokeFilterChoices(TextChoices):
    request_sent = ('request_sent', 'Request Sent')
    request_received = ('request_received', "Request Received")
    no_co_broke = ('no_co_broke', "No Co-Broke")


class PayoutRemarksChoices(TextChoices):
    wrong_lo_uploaded = ('wrong_lo_uploaded', 'Wrong LO Uploaded')
    wrong_name = ('wrong_name', "Client's Name not as per LO")
    wrong_loan_amount = ('wrong_loan_amount', "Loan Amount not as per LO- Reference Number not as per LO")
    duplicate = ('duplicate', "Duplicate submission")


class PayoutStatusChoices(TextChoices):
    pending_verification = ("pending_verification", "Pending Verification")
    pending_banks_payment = ("pending_banks_payment", "Pending Bank's Payment")
    rework_required = ("rework_required", "Rework Required")
    pending_user_payout = ("pending_user_payout", "Pending User Payout")
    paid = ("paid", "Paid")


kvstore_static_data = [
    dict(
        name="Co Broke Filter Choices",
        slug="co_broke_choices",
        type="json",
        data_type="string",
        values=dict(CoBrokeFilterChoices.choices),
        editable=False,
        prepend="",
        append=""
    ),
    dict(
        name="Rate Type",
        slug="rate_type",
        type="json",
        data_type="string",
        values=dict(RateTypeChoices.choices),
        editable=False,
        prepend="",
        append=""
    ),
    dict(
        name="Equation",
        slug="equation",
        type="list",
        data_type="string",
        values=[choice[0] for choice in EquationChoices.choices],
        editable=False,
        prepend="",
        append=""
    ),
    dict(
        name="Rate Category",
        slug="rate_category",
        type="json",
        data_type="string",
        values=dict(RateCategoryChoices.choices),
        editable=False,
        prepend="",
        append=""
    ),
    dict(
        name="Property Type",
        slug="property_type",
        type="json",
        data_type="string",
        values=dict(PropertyTypeChoices.choices),
        editable=False,
        prepend="",
        append=""
    ),
    dict(
        name="Property Status",
        slug="property_status",
        type="json",
        data_type="string",
        values=dict(PropertyStatusChoices.choices),
        editable=False,
        prepend="",
        append=""
    ),
    dict(
        name="Loan Category",
        slug="loan_category",
        type="json",
        data_type="string",
        values=dict(LoanCategoryChoices.choices),
        editable=False,
        prepend="",
        append=""
    ),
    dict(
        name="Nationality",
        slug="nationality",
        type="list",
        data_type="string",
        values=[choice[0] for choice in NationalityChoices.choices],
        editable=False,
        prepend="",
        append=""
    ),
    dict(
        name="Lead Type",
        slug="lead_type",
        type="json",
        data_type="string",
        values=dict(LeadTypeChoices.choices),
        editable=False,
        prepend="",
        append=""
    ),
    dict(
        name="Lead Status",
        slug="lead_status",
        type="json",
        data_type="string",
        values=dict(LeadStatusChoices.choices),
        editable=False,
        prepend="",
        append=""
    ),
    dict(
        name="Payout Status",
        slug="payout_status",
        type="json",
        data_type="string",
        values=dict(PayoutStatusChoices.choices),
        editable=False,
        prepend="",
        append=""
    )
]
