from django.urls import path
from .views import admin, agent

app_name = "kvstore"

admin_patterns = [
    path('all', admin.KVListAPIView.as_view(), name="values"),
    path('<str:code>', admin.KVDetailAPIView.as_view(), name="values_ru"),
]

agent_patterns = [
    path('all', agent.KVListAPIView.as_view(), name="values"),
    path('<str:code>', agent.KVDetailAPIView.as_view(), name="values_ru"),
]
