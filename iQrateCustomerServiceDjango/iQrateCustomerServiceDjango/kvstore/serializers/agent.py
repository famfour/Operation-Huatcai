from rest_framework import serializers
from kvstore.models import KVStore, DataTypeChoices, ValueTypeChoices


class KVStoreAgentSerializer(serializers.Serializer):
    key = serializers.CharField(max_length=255)
    code = serializers.SlugField(read_only=True)
    value_type = serializers.ChoiceField(choices=ValueTypeChoices.choices, read_only=True)
    data_type = serializers.ChoiceField(
        choices=DataTypeChoices.choices,
        read_only=True,
        help_text="If the value_type is list, then the members of the list should be of this data type. "
                  "If the value_type is json, then the json values should be of this data type"
    )
    value = serializers.JSONField()
    prepend = serializers.CharField(read_only=True)
    append = serializers.CharField(read_only=True)
    editable = serializers.BooleanField(default=True, read_only=True)

    class Meta:
        model = KVStore
        fields = ['key', 'code', 'value_type', 'data_type', 'value', 'prepend', 'append', 'editable']
