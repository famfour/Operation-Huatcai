from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from kvstore.models import KVStore, ValueTypeChoices, DataTypeChoices
from kvstore.utils import converters


class KVStoreAdminSerializer(serializers.Serializer):
    key = serializers.CharField(max_length=255, help_text="Name of this item.")
    code = serializers.SlugField(read_only=True, help_text="The unique identifier for the item.")
    value_type = serializers.ChoiceField(
        choices=ValueTypeChoices.choices,
        read_only=True,
        help_text="This denotes the value types of this item. could be list/dict(json). "
    )
    data_type = serializers.ChoiceField(
        choices=DataTypeChoices.choices,
        read_only=True,
        help_text="If the value_type is list, then the members of the list should be of this data type \n "
                  "If the value_type is json, then the json values should be of this data type"
    )
    value = serializers.JSONField()
    prepend = serializers.CharField(read_only=True, help_text="Text to prepend to the value before displaying.For ex. '$' ")
    append = serializers.CharField(read_only=True, help_text="Text to append to the value before displaying.For ex. '%' ")
    editable = serializers.BooleanField(default=True, help_text="Can the values for this item be updated?", read_only=True)

    class Meta:
        model = KVStore
        fields = ['key', 'code', 'value_type', 'data_type', 'value', 'prepend', 'append', 'editable']

    def update(self, instance, validated_data):
        key = validated_data.get("key", None)
        value = validated_data.get('value')

        # Make sure the posted data is the same as value type.
        if instance.value_type == "list" and not type(value).__name__ == "list":
            raise ValidationError(detail=f"The value for {instance.code} should be list")
        if instance.value_type == "json" and not type(value).__name__ == "dict":
            raise ValidationError(detail=f"The value for {instance.code} should be json/dictionary not {type(value)}")

        # Validate all the values for the correct datatype before saving.
        values_to_check = validated_data.get("value").values() if instance.value_type == "json" else validated_data.get("value")
        converter = converters[f"convert_to_{instance.data_type}"]
        for item in values_to_check:
            try:
                converter(item)
            except:
                raise ValidationError(detail=f"All the values should be of the type - {instance.data_type}", code="value")

        if key:
            setattr(instance, "key", key)
        setattr(instance, f"value_{instance.value_type}", value)
        instance.save()
        return instance
