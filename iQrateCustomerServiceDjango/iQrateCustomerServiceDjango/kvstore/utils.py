converters = dict(
    convert_to_boolean=lambda x: bool(x),
    convert_to_integer=lambda x: int(x),
    convert_to_float=lambda x: float(x),
    convert_to_string=lambda x: str(x)
)
