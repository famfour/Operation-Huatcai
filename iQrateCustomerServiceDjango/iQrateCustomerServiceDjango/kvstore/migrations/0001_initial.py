# Generated by Django 4.0.2 on 2022-02-22 14:48

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='KVStore',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_by', models.UUIDField(null=True)),
                ('updated_by', models.UUIDField(null=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('key', models.CharField(max_length=255)),
                ('code', models.SlugField(unique=True)),
                ('value_type', models.CharField(choices=[('list', 'list'), ('bool', 'bool'), ('json', 'json'), ('str', 'str')], max_length=16)),
                ('value_list', django.contrib.postgres.fields.ArrayField(base_field=models.CharField(blank=True, max_length=128), blank=True, default=list, size=None)),
                ('value_json', models.JSONField(default=dict)),
                ('value_bool', models.BooleanField(default=False)),
                ('value_str', models.CharField(blank=True, max_length=254, null=True)),
            ],
            options={
                'ordering': ['-created'],
            },
        ),
    ]
