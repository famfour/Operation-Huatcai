# Generated by Django 4.0.4 on 2022-05-23 17:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kvstore', '0008_kvstore_append_kvstore_prepend'),
    ]

    operations = [
        migrations.AddField(
            model_name='kvstore',
            name='editable',
            field=models.BooleanField(default=False),
        ),
    ]
