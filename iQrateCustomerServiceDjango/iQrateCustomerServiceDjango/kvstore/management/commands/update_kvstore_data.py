from django.core.management import BaseCommand
from kvstore.models import KVStore
import json
from django.conf import settings
import os
from slugify import slugify
from kvstore.utils import converters
from kvstore.choices import kvstore_static_data
from pathlib import Path


class Command(BaseCommand):
    """
    Update the kvstore table, if.
    1. There are new items added to the kvdata.json
    2. All static kvstore values from choices.
    """
    help = "Re populate the Kvstore data table."

    def add_list_data(self, item):
        converter = converters[f"convert_to_{item['data_type']}"]
        values = [converter(value) for value in item.get("values")]
        try:
            KVStore.objects.create(
                key=item.get("name"),
                code=item.get("slug"),
                editable=item.get("editable"),
                value_list=values,
                value_type="list",
                data_type=item.get("data_type"),
                append=item.get("append"),
                prepend=item.get("prepend")
            )
        except Exception as e:
            print(values)
            print(e)

    def add_json_data(self, item):
        values = dict()
        converter = converters[f"convert_to_{item['data_type']}"]

        for k, v in item.get("values").items():
            values[slugify(k, separator="_")] = converter(v)
        try:
            KVStore.objects.create(
                key=item.get("name"),
                code=item.get("slug"),
                editable=item.get("editable"),
                value_json=values,
                value_type="json",
                data_type=item.get("data_type"),
                append=item.get("append"),
                prepend=item.get("prepend")
            )
        except Exception as e:
            print(values)
            print(e)

    def handle(self, *args, **options):
        # load the data file.
        content = Path(os.path.join(settings.BASE_DIR, "kvstore", 'data', "kvdata.json")).read_text()
        data = json.loads(content)

        # Delete all the values from KVStore.
        # KVStore.objects.all().delete()

        # Repopulate the values. from the json file.
        for item in data:
            # To manage items that are moved from static to editable.
            KVStore.objects.filter(code=item['slug'], editable=False).delete()
            if KVStore.objects.filter(code=item['slug']).exists():
                continue
            if item['type'] == "list":
                self.add_list_data(item)

            if item["type"] == "json":
                self.add_json_data(item)

        # Add values from the static choices
        data = kvstore_static_data
        for item in data:
            KVStore.objects.filter(code=item['slug']).delete() #
            if item['type'] == "list":
                self.add_list_data(item)

            if item["type"] == "json":
                self.add_json_data(item)
