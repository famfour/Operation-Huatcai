from django.core.management import BaseCommand
from kvstore.models import KVStore
import json
from django.conf import settings
import os
from slugify import slugify


class Command(BaseCommand):

    def handle(self, *args, **options):
        file_path = os.path.join(settings.BASE_DIR, "kvstore", 'data', "kvdata.json")
        with open(file_path) as file:
            data = json.load(file)
            for item in data:
                # Check if the key exists in KVStore.
                if KVStore.objects.filter(code=item["slug"]).exists():
                    continue
                print(f"Adding {item.get('name')} to the database")
                # Add the item to db
                if item['type'] == "list":
                    try:
                        KVStore.objects.create(
                            key=item.get("name"),
                            code=item.get("slug"),
                            value_list=item.get("values"),
                            value_type="list"
                        )
                    except Exception as e:
                        print(e)
                    continue
                if item["type"] == "json":
                    values = dict()
                    for i in item.get("values"):
                        values[slugify(i, separator="_")] = i
                    try:
                        KVStore.objects.create(
                            key=item.get("name"),
                            code=item.get("slug"),
                            value_json=values,
                            value_type="json"
                        )
                    except Exception as e:
                        print(e)
                    continue
