from django.db import models
from common.models import ModelMixin
from django.contrib.postgres.fields import ArrayField
from jsonfield import JSONField
from .utils import converters


class ValueTypeChoices(models.TextChoices):
    list = "list"
    json = "json"


class DataTypeChoices(models.TextChoices):
    string = "string"
    boolean = "boolean"
    integer = "integer"
    float = "float"


class LazyKVStore():
    """
    This class is to dynamically load Kvstore values on call for serializers.
    """

    def __init__(self, code):
        self.code = code

    def __iter__(self):
        values = []
        try:
            kv = KVStore.objects.get(code=self.code)
            if kv.value_type == "json":
                values = [[k, v] for k, v in kv.value_json.items()]
            if kv.value_type == "list":
                values = [[i, i] for i in kv.value_list]
        except:
            pass
        yield from values


class KVStore(ModelMixin):
    key = models.CharField(max_length=255)
    code = models.SlugField(unique=True)
    data_type = models.CharField(
        max_length=32,
        choices=DataTypeChoices.choices,
        default=DataTypeChoices.string,
        help_text="If the value_type is list, then the members of the list should be of this data type \n "
                  "If the value_type is json, then the json values(keys:values) should be of this data type"
    )
    prepend = models.CharField(max_length=32, null=True, blank=True)
    append = models.CharField(max_length=32, null=True, blank=True)
    editable = models.BooleanField(default=True, help_text="true -> value can be updated, false -> can't")
    value_type = models.CharField(max_length=16, choices=ValueTypeChoices.choices)
    value_list = ArrayField(base_field=models.CharField(max_length=128, blank=True), default=list, blank=True)
    value_json = JSONField(default=dict)
    value_bool = models.BooleanField(default=False)
    value_str = models.CharField(max_length=254, null=True, blank=True)

    def __str__(self):
        return self.key

    @property
    def value(self):
        """
        Return the value based on the value type from the object.
        """
        ret = ""
        converter = converters[f"convert_to_{self.data_type}"]
        values = getattr(self, f"value_{self.value_type}")
        if self.value_type == "list":
            ret = [converter(value) for value in values]
        if self.value_type == "json":
            ret = dict()
            for k, v in values.items():
                ret[k] = converter(v)
        return ret

    class Meta:
        ordering = ['-created']

    @staticmethod
    def get_choices(code):
        """
        Send the lazy class that is evaluated on every call.
        """
        return LazyKVStore(code)

    @staticmethod
    def get_display_value(code, key):
        try:
            kv = KVStore.objects.get(code=code)
            return kv.value.get(key)
        except:
            return None
