from drf_spectacular.utils import extend_schema
from rest_framework.response import Response

from kvstore.models import KVStore
from kvstore.serializers.agent import KVStoreAgentSerializer
from rest_framework.exceptions import NotFound
from rest_framework.generics import RetrieveAPIView
from rest_framework.views import APIView


@extend_schema(tags=["Customer - KVList"], summary="Get a list of all KV Values")
class KVListAPIView(APIView):
    """
    List all Key Value Data
    """

    def get(self, request):
        kvs = KVStore.objects.all()
        serializer = KVStoreAgentSerializer(kvs, many=True)
        return Response(serializer.data)


@extend_schema(tags=["Customer - KVList"], summary="Get a values for a key")
class KVDetailAPIView(RetrieveAPIView):
    """
    Retrieve, update or delete a KV instance.
    """
    serializer_class = KVStoreAgentSerializer

    def get_object(self):
        try:
            return KVStore.objects.get(code=self.kwargs.get('code'))
        except KVStore.DoesNotExist:
            raise NotFound()
