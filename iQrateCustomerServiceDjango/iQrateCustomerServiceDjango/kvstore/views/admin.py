from drf_spectacular.utils import extend_schema

from kvstore.models import KVStore
from kvstore.serializers.admin import KVStoreAdminSerializer
from rest_framework.exceptions import NotFound
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


@extend_schema(tags=["Admin - KVList"], summary="Get a list of all KV Values")
class KVListAPIView(APIView):
    """
    List all the KV Values
    """
    serializer_class = KVStoreAdminSerializer

    def get(self, request):
        kvs = KVStore.objects.all()
        serializer = KVStoreAdminSerializer(kvs, many=True)
        return Response(serializer.data)


class KVDetailAPIView(APIView):
    """
    Retrieve, update or delete a KV instance.
    """
    serializer_class = KVStoreAdminSerializer

    def get_object(self, code):
        try:
            return KVStore.objects.get(code=code)
        except KVStore.DoesNotExist:
            raise NotFound()

    @extend_schema(
        tags=["Admin - KVList"],
        summary="Get a values for a key",
        description="Get the values for a given code.")
    def get(self, request, code):
        kv = self.get_object(code)
        serializer = KVStoreAdminSerializer(kv)
        return Response(serializer.data)

    @extend_schema(
        tags=["Admin - KVList"],
        summary="Update values for a key",
        description="If the `value_type` is list, then the members of the list should be of this data type  \n  \n"
                    "If the `value_type` is json, then the json values should be of this data type")
    def put(self, request, code):
        kv = self.get_object(code)
        if not kv.editable:
            return Response(
                data={
                    "detail": "This entry is not editable"
                },
                status=status.HTTP_401_UNAUTHORIZED
            )
        serializer = KVStoreAdminSerializer(kv, data=request.data, context=self.get_renderer_context())
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
