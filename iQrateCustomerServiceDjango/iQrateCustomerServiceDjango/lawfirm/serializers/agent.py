from rest_framework import serializers
from lawfirm.models import LawFirm, Product
from bank.serializers.admin import BankHyperlinkedSerializer


class ProductsAgentHyperLinkedSerializer(serializers.HyperlinkedModelSerializer):
    banks = BankHyperlinkedSerializer(many=True)

    class Meta:
        model = Product
        fields = ['id', 'loan_category', 'property_types', 'banks', 'loan_range_from', 'loan_range_to',
                  'fixed_legal_fee', 'legal_fee']
        read_only_fields = fields


class LawFirmAgentSerializer(serializers.ModelSerializer):
    products = ProductsAgentHyperLinkedSerializer(many=True, read_only=True)

    class Meta:
        model = LawFirm
        fields = ['id', 'name', 'email', 'priority', 'remarks', 'products']
        read_only_fields = fields


class LawFirmAgentHyperLinkedSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = LawFirm
        fields = ['id', 'name', 'email', 'priority', 'remarks']
