from rest_framework import serializers
from lawfirm.models import LawFirm, Product
from kvstore.models import KVStore
from common.serializers import CommonModelSerializer
from bank.serializers.admin import BankHyperlinkedSerializer


class ProductSerializer(CommonModelSerializer):
    law_firm = serializers.PrimaryKeyRelatedField(queryset=LawFirm.objects.all())
    loan_category = serializers.MultipleChoiceField(
        choices=KVStore.get_choices("loan_category"),
        help_text="Values can be obtained from kvstore 'loan_category'"
    )
    property_types = serializers.MultipleChoiceField(
        choices=KVStore.get_choices("property_type"),
        help_text="Values can be obtained from kvstore 'property_type'"
    )

    def validate_property_types(self, value):
        return list(value)
    
    def validate_loan_category(self, value):
        return list(value)

    class Meta:
        model = Product
        fields = ['id', 'law_firm', 'loan_category', 'property_types', 'banks', 'loan_range_from', 'loan_range_to',
                  'fixed_legal_fee', 'legal_fee', 'created_by', 'updated_by', 'created', 'updated']

        read_only_fields = ['created_by', 'updated_by', 'created', 'updated']


class LawFirmSerializer(CommonModelSerializer):

    def validate_priority(self, value):
        # Make sure priority is unique
        queryset = LawFirm.objects.filter(priority=value)
        if self.instance:
            queryset = queryset.exclude(pk=self.instance.pk)
        if queryset.exists():
            raise serializers.ValidationError(detail="The priority has to be unique. There is another lawfirm with the same priority")
        return value

    class Meta:
        model = LawFirm
        fields = ['id', 'name', 'email', 'priority', 'remarks', 'status', 'created_by', 'updated_by', 'created', 'updated']
        read_only_fields = ['created_by', 'updated_by', 'created', 'updated']
