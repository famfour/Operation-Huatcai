import django_filters

from rest_framework.exceptions import NotFound
from drf_spectacular.utils import extend_schema

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import ListAPIView, CreateAPIView

from lawfirm.filters.admin import ProductFilter
from lawfirm.models import Product
from lawfirm.serializers.admin import ProductSerializer
from django.db.models.deletion import ProtectedError
from common.responses import ProtectedInstanceResponse


# Create your views here.

@extend_schema(tags=["Admin - LawFirm - Product"], summary="Products List")
class ProductListAPIView(ListAPIView):
    """
    Get a paginated/filtered list of all Products
    """
    serializer_class = ProductSerializer
    filterset_class = ProductFilter
    queryset = Product.objects.order_by("-created").all()
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]


@extend_schema(tags=["Admin - LawFirm - Product"], summary="Add Product")
class ProductCreateAPIView(CreateAPIView):
    """
    Create a new productt
    """
    serializer_class = ProductSerializer


class ProductDetailAPIView(APIView):
    """
    Retrieve, update or delete a Product instance.
    """
    serializer_class = ProductSerializer

    def get_object(self, pk):
        try:
            return Product.objects.get(pk=pk)
        except Product.DoesNotExist:
            raise NotFound()

    @extend_schema(tags=["Admin - LawFirm - Product"], summary="Product Details")
    def get(self, request, pk):
        """
        Get the details of a product
        """
        product = self.get_object(pk)
        serializer = ProductSerializer(product)
        return Response(serializer.data)

    @extend_schema(tags=["Admin - LawFirm - Product"], summary="Update Product")
    def put(self, request, pk):
        """
        update a product
        """
        product = self.get_object(pk)
        serializer = ProductSerializer(product, data=request.data, context=self.get_renderer_context())
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @extend_schema(tags=["Admin - LawFirm - Product"], summary="Delete Product")
    def delete(self, request, pk):
        """
        Delete a product
        """
        product = self.get_object(pk)
        try:
            product.delete()
        except ProtectedError:
            return ProtectedInstanceResponse(instance=product)
        return Response(status=status.HTTP_204_NO_CONTENT)
