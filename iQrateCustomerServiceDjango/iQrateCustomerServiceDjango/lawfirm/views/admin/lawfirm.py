import django_filters

from rest_framework.exceptions import NotFound
from drf_spectacular.utils import extend_schema

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import ListAPIView, CreateAPIView

from lawfirm.filters.admin import LawFirmFilter
from lawfirm.models import LawFirm
from lawfirm.serializers.admin import LawFirmSerializer
from django.db.models.deletion import ProtectedError
from common.responses import ProtectedInstanceResponse


@extend_schema(tags=["Admin - LawFirm"], summary="Get a paginated/filtered list of all LawFirms")
class LawFirmListAPIView(ListAPIView):
    """
    Paginated list of law firms
    """
    serializer_class = LawFirmSerializer
    filterset_class = LawFirmFilter
    queryset = LawFirm.objects.order_by('priority').all()
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]


@extend_schema(tags=["Admin - LawFirm"], summary="Create a new LawFirm")
class LawFirmCreateAPIView(CreateAPIView):
    """
    Create a new lawfirm.
    """
    serializer_class = LawFirmSerializer


class LawFirmDetailAPIView(APIView):
    """
    Retrieve, update or delete a LawFirm instance.
    """
    serializer_class = LawFirmSerializer

    def get_object(self, pk):
        try:
            return LawFirm.objects.get(pk=pk)
        except LawFirm.DoesNotExist:
            raise NotFound()

    @extend_schema(tags=["Admin - LawFirm"], summary="Get LawFirm details")
    def get(self, request, pk):
        """
        Get the details of a lawfirm with a given 'id'
        """
        law_firm = self.get_object(pk)
        serializer = LawFirmSerializer(law_firm)
        return Response(serializer.data)

    @extend_schema(tags=["Admin - LawFirm"], summary="Update a LawFirm")
    def put(self, request, pk):
        """
        Update a lawfirm
        """
        law_firm = self.get_object(pk)
        serializer = LawFirmSerializer(law_firm, data=request.data, context=self.get_renderer_context())
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @extend_schema(tags=["Admin - LawFirm"], summary="Delete a LawFirm")
    def delete(self, request, pk):
        """
        Delete a lawfirm.
        """
        law_firm = self.get_object(pk)
        try:
            law_firm.delete()
        except ProtectedError:
            return ProtectedInstanceResponse(instance=law_firm)
        return Response(status=status.HTTP_204_NO_CONTENT)
