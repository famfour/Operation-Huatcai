from rest_framework.exceptions import NotFound
from drf_spectacular.utils import extend_schema
from rest_framework.generics import ListAPIView, RetrieveAPIView
from lawfirm.models import LawFirm
from lawfirm.serializers.agent import LawFirmAgentSerializer


@extend_schema(tags=["Agent - LawFirm"], summary="Lawfirms List")
class LawFirmListAPIView(ListAPIView):
    """
    Paginated list of lawfirm for agent.  \n
    This endpoint will only return lawfirms that are set to be active by the admin.  \n

    """
    serializer_class = LawFirmAgentSerializer
    queryset = LawFirm.objects.filter(status=True).order_by('priority').all()


@extend_schema(tags=["Agent - LawFirm"], summary="Lawfirms Details")
class LawFirmDetailView(RetrieveAPIView):
    """
    Lawfirm details with products.  \n
    This endpoint will only return lawfirms that are set to be active by the admin.
    """
    serializer_class = LawFirmAgentSerializer

    def get_object(self):
        try:
            return LawFirm.objects.filter(
                pk=self.kwargs.get("pk"), status=True
            ).prefetch_related("products").get()
        except LawFirm.DoesNotExist:
            raise NotFound()
