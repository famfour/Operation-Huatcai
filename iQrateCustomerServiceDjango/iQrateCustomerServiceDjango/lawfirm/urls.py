from django.urls import path
from .views import admin, agent

app_name = "law_firm"

admin_patterns = [
    # Product
    path('products', admin.ProductListAPIView.as_view(), name="products"),
    path('product', admin.ProductCreateAPIView.as_view(), name="products_create"),
    path('product/<int:pk>', admin.ProductDetailAPIView.as_view(), name="products_rud"),
    # LawFirm
    path('<int:pk>', admin.LawFirmDetailAPIView.as_view(), name="lawfirms_rud"),
    path('list', admin.LawFirmListAPIView.as_view(), name="lawfirms"),
    path('create', admin.LawFirmCreateAPIView.as_view(), name="lawfirms_create"),
]

agent_patterns = [
    path('<int:pk>', agent.LawFirmDetailView.as_view(), name="lawfirm"),
    path('all', agent.LawFirmListAPIView.as_view(), name="lawfirms"),
]
