from django.contrib.postgres.fields import ArrayField

from django.db import models
from common.models import ModelMixin
from bank.models import Bank
from kvstore.choices import PropertyTypeChoices, LoanCategoryChoices


class LawFirm(ModelMixin):
    name = models.CharField(max_length=254)
    email = models.EmailField()
    priority = models.PositiveIntegerField(default=1)
    remarks = models.TextField(null=True)
    status = models.BooleanField(
        default=False,
        help_text="Should this lawfirm be published? If the status is false, this lawfirm won't be displayed to the customers")

    class Meta:
        ordering = ['-priority', '-created']

    def __str__(self):
        return self.name


class Product(ModelMixin):
    law_firm = models.ForeignKey(LawFirm, on_delete=models.PROTECT, related_name="products")
    loan_category = ArrayField(
        base_field=models.CharField(max_length=128, choices=LoanCategoryChoices.choices),
        null=True,
        blank=True,
        help_text="Values for this field can be obtained from KVstore - 'loan_category'"
    )
    property_types = ArrayField(
        base_field=models.CharField(max_length=128, choices=PropertyTypeChoices.choices),
        null=True,
        blank=True,
        help_text="Values for this field can be obtained from KVstore"
    )
    banks = models.ManyToManyField(Bank, related_name="banks")
    loan_range_from = models.FloatField(default=0)
    loan_range_to = models.FloatField(default=0)
    fixed_legal_fee = models.BooleanField(default=False, help_text="If this field is true, 'legal_fee column must contain a value.'")
    legal_fee = models.FloatField(
        null=True,
        help_text="The fixed legal fee this lawfirm is charging for this particular product")  # @todo validate based on the fixed_legal_fee value

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return self.law_firm.name
