# Generated by Django 4.0.6 on 2022-08-12 21:59

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lawfirm', '0007_alter_product_loan_range_from_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='property_type',
        ),
        migrations.AddField(
            model_name='product',
            name='property_types',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=128), blank=True, choices=[('hdb', 'HDB'), ('condominium_apartment', 'Condominium/ Apartment'), ('strata_housing_townhouses', 'Strata Housing/ Townhouses'), ('ec_resale_out_of_mop', 'EC (Resale/ out of MOP)'), ('ec_from_developer_within_mop', 'EC (From Developer/ within MOP)'), ('landed', 'Landed')], help_text='Values for this field can be obtained from KVstore', null=True, size=None),
        ),
    ]
