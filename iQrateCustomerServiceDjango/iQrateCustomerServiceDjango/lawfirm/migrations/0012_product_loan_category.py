# Generated by Django 4.1 on 2022-08-29 08:29

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("lawfirm", "0011_remove_product_loan_category"),
    ]

    operations = [
        migrations.AddField(
            model_name="product",
            name="loan_category",
            field=django.contrib.postgres.fields.ArrayField(
                base_field=models.CharField(
                    choices=[
                        (
                            "new_purchase_with_option_to_purchase",
                            "New Purchase (With Option to Purchase)",
                        ),
                        (
                            "new_purchase_approval_in_principle",
                            "New Purchase (Approval in Principle)",
                        ),
                        ("refinance", "Refinance"),
                        ("cash_out_term_loan", "Cash Out (Term Loan)"),
                        (
                            "part_purchase_decoupling_divorce",
                            "Part Purchase/ Decoupling/ Divorce",
                        ),
                    ],
                    max_length=128,
                ),
                blank=True,
                help_text="Values for this field can be obtained from KVstore - 'loan_category'",
                null=True,
                size=None,
            ),
        ),
    ]
