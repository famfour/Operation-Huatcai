from lawfirm.models import LawFirm, Product
import django_filters


class LawFirmFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr="icontains")
    email = django_filters.CharFilter(lookup_expr="icontains")


    class Meta:
        model = LawFirm
        fields = ['name', 'email']


class ProductFilter(django_filters.FilterSet):
    law_firm_id = django_filters.NumberFilter(lookup_expr="exact")

    class Meta:
        model = Product
        fields = ['law_firm_id']
