# Generated by Django 4.0.4 on 2022-05-31 18:46

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('submission', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='method',
            old_name='slug',
            new_name='identifier',
        ),
    ]
