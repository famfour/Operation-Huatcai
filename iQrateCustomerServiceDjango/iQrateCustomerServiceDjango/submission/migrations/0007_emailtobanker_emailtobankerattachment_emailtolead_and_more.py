# Generated by Django 4.0.5 on 2022-06-05 21:45

import django.contrib.postgres.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bank', '0022_banker_last_assignment'),
        ('lead', '0033_remove_lead_banker'),
        ('submission', '0006_alter_leadbanker_banker_alter_leadbanker_lead'),
    ]

    operations = [
        migrations.CreateModel(
            name='EmailToBanker',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_by', models.UUIDField(help_text='User ID of the agent or admin who created this record.', null=True)),
                ('updated_by', models.UUIDField(help_text='User ID of the agent or admin who last updated this record.', null=True)),
                ('created', models.DateTimeField(auto_now_add=True, help_text='Date and time at which this record is created')),
                ('updated', models.DateTimeField(auto_now=True, help_text='Date and time at which this record is last updated.')),
                ('email_type', models.CharField(choices=[('banker', 'Banker'), ('lead', 'Lead')], max_length=16)),
                ('email_to', models.EmailField(max_length=254)),
                ('email_cc', django.contrib.postgres.fields.ArrayField(base_field=models.EmailField(max_length=254), blank=True, default=list, size=None)),
                ('email_content', models.TextField()),
                ('banker', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='banker_emails', to='bank.banker')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='EmailToBankerAttachment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_by', models.UUIDField(help_text='User ID of the agent or admin who created this record.', null=True)),
                ('updated_by', models.UUIDField(help_text='User ID of the agent or admin who last updated this record.', null=True)),
                ('created', models.DateTimeField(auto_now_add=True, help_text='Date and time at which this record is created')),
                ('updated', models.DateTimeField(auto_now=True, help_text='Date and time at which this record is last updated.')),
                ('name', models.CharField(max_length=254)),
                ('file', models.URLField()),
                ('email', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='attachments', to='submission.emailtobanker')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='EmailToLead',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_by', models.UUIDField(help_text='User ID of the agent or admin who created this record.', null=True)),
                ('updated_by', models.UUIDField(help_text='User ID of the agent or admin who last updated this record.', null=True)),
                ('created', models.DateTimeField(auto_now_add=True, help_text='Date and time at which this record is created')),
                ('updated', models.DateTimeField(auto_now=True, help_text='Date and time at which this record is last updated.')),
                ('email_type', models.CharField(choices=[('banker', 'Banker'), ('lead', 'Lead')], max_length=16)),
                ('email_to', models.EmailField(max_length=254)),
                ('email_cc', django.contrib.postgres.fields.ArrayField(base_field=models.EmailField(max_length=254), blank=True, default=list, size=None)),
                ('email_content', models.TextField()),
                ('lead', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='lead_emails', to='lead.lead')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='EmailToLeadAttachment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_by', models.UUIDField(help_text='User ID of the agent or admin who created this record.', null=True)),
                ('updated_by', models.UUIDField(help_text='User ID of the agent or admin who last updated this record.', null=True)),
                ('created', models.DateTimeField(auto_now_add=True, help_text='Date and time at which this record is created')),
                ('updated', models.DateTimeField(auto_now=True, help_text='Date and time at which this record is last updated.')),
                ('name', models.CharField(max_length=254)),
                ('file', models.URLField()),
                ('email', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='attachments', to='submission.emailtolead')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.RemoveField(
            model_name='emailattachment',
            name='email',
        ),
        migrations.DeleteModel(
            name='Email',
        ),
        migrations.DeleteModel(
            name='EmailAttachment',
        ),
    ]
