import django_filters
from drf_spectacular.utils import extend_schema
from rest_framework.exceptions import NotFound
from rest_framework.generics import ListAPIView, RetrieveAPIView

from submission.serializers.agent import EmailSerializer
from submission.models import Email
from submission.filters import EmailFilter
from lead.models import Lead


@extend_schema(tags=["Customer - Submissions"], summary="Email Logs", responses=EmailSerializer)
class EmailLogsListView(ListAPIView):
    """
    List of submission emails sent for a lead.
    """
    serializer_class = EmailSerializer
    filterset_class = EmailFilter
    queryset = Email.objects.none()
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    pagination_class = None

    def get_queryset(self):
        # Make sure the user has access to the
        try:
            lead = Lead.objects.filter(pk=self.kwargs.get("lead_id")).with_owner(user=self.request.user).get()
        except Lead.DoesNotExist:
            raise NotFound
        return Email.objects.filter(lead=lead).all()


@extend_schema(tags=["Customer - Submissions"], summary="Email Detail", responses=EmailSerializer)
class EmailDetailView(RetrieveAPIView):
    """
    List of submission emails sent for a lead.
    """
    serializer_class = EmailSerializer

    def get_object(self):
        email = Email.objects.prefetch_related("lead").get(pk=self.kwargs.get("email_id"))
        # Make sure the user has access to the email with the given ID
        try:
            _ = Lead.objects.filter(pk=email.lead_id).with_owner(user=self.request.user).get()
        except Lead.DoesNotExist:
            raise NotFound
        return email
