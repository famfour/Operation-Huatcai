from itertools import chain

from django.db.models import Q, F, Count, Prefetch
from drf_spectacular.utils import extend_schema
from rest_framework import status
from rest_framework.exceptions import NotFound, PermissionDenied
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from common.serializers import SuccessResponseSerializer
from lawfirm.models import LawFirm, Product
from lead.models import Lead, Client, LeadPackage
from submission.models import Email, LeadBanker, EmailType, EmailAttachment
from submission.serializers.agent import LeadBanksSerializer, EmailLeadRequestSerializer, \
    EmailLeadResponseSerializer, EmailLawFirmSerializer, EmailBankerResponseSerializer, EmailBankerRequestSerializer
from kvstore.choices import LeadStatusChoices


@extend_schema(tags=["Customer - Submissions"], summary="List of banks for the lead", responses=LeadBanksSerializer)
class LeadBanksAPIView(ListAPIView):
    """
    Returns the unique banks from the selected packages.  \n
    Status of actions for the lead is returned as well.
    """
    serializer_class = LeadBanksSerializer
    pagination_class = None

    def get_queryset(self):
        try:
            lead = Lead.objects.with_owner(user=self.request.user).get(pk=self.kwargs.get("lead_id"))
        except Lead.DoesNotExist:
            raise NotFound
        # @todo , find a cleaner query
        packages = [p.package for p in lead.packages.all()]

        banks = set([package.bank for package in packages])
        return LeadBanker.objects.filter(lead=lead).filter(banker__bank__in=banks).distinct()


class EmailToLeadView(APIView):

    def get_object(self, lead_id, bank_id):
        # Get the lead.
        try:
            lead = Lead.objects.with_owner(
                user=self.request.user
            ).prefetch_related('packages', 'packages__package').get(pk=lead_id)
        except Lead.DoesNotExist:
            raise NotFound(detail="Lead is not found")

        # Check for unpublished packages
        expired = LeadPackage.objects.filter(lead=lead, package__bank_id=bank_id, package__publish=False).exists()
        if expired:
            raise PermissionDenied(
                detail=f"There are inactive packages in your selected list, please remove them."
            )

        # Make sure PDPA  is approved for  all clients.
        if Client.objects.filter(lead=lead, pdpa_status=False).exists():
            raise PermissionDenied(detail="All clients has to verify PDPA before the lead can submitted.")

        # Make sure a property was added
        if not getattr(lead, 'property', None):
            raise PermissionDenied(detail="Property for this lead must be added before it can be submitted.")

        try:
            return LeadBanker.objects.filter(
                Q(lead=lead) & Q(banker__bank_id=bank_id)
            ).prefetch_related("lead", "banker", "lead__document_drawer", "banker", "banker__bank").get()
        except LeadBanker.DoesNotExist:
            raise NotFound(detail="No Bankers")

    def get_main_client(self, lead):
        return Client.objects.filter(lead=lead, main_applicant=True).first()

    @extend_schema(tags=["Customer - Submissions"], summary="Email to Lead", responses=EmailLeadResponseSerializer)
    def get(self, request, lead_id, bank_id):
        # Get the lead banker
        lead_banker = self.get_object(lead_id, bank_id)

        # get the method
        method = lead_banker.banker.bank.workflow.get_class()
        method.set_object(lead_banker)

        # Get the main client.
        client = self.get_main_client(lead_banker.lead)

        # Populate
        data = dict(
            name=client.name,
            email=client.email,
            subject=method.lead_email_subject(),
            content=method.lead_email_content(),
            attachments=method.lead_attachment_names()
        )
        serializer = EmailLeadResponseSerializer(instance=data)
        return Response(status=status.HTTP_200_OK, data=serializer.data)

    @extend_schema(tags=["Customer - Submissions"], summary="Email to the Lead",
                   request=EmailLeadRequestSerializer, responses=SuccessResponseSerializer)
    def post(self, request, lead_id, bank_id):
        serializer = EmailLeadRequestSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        # get the posted data
        remarks = serializer.validated_data.get('remarks')

        # Get Lead Banker
        lead_banker = self.get_object(lead_id, bank_id)
        # get the method
        method = lead_banker.banker.bank.workflow.get_class()
        method.set_object(lead_banker)

        # Get the main client.
        client = self.get_main_client(lead_banker.lead)

        # Create the email record.
        email = Email.objects.create(
            lead=lead_banker.lead,
            email_type=EmailType.lead,
            name=client.name,
            to_email=client.email,
            cc_emails=[lead_banker.banker.email],
            subject=method.lead_email_subject(),
            content=method.lead_email_content(remarks=remarks)
        )
        # Save the attachments.
        for file in method.lead_attachment_files():
            EmailAttachment.objects.create(
                email=email,
                name=file[0],
                file_key=file[1]
            )

        # Send the email
        email.send_email()

        # Change the lead status to be submitted.
        lead = lead_banker.lead
        lead.lead_status = LeadStatusChoices.submitted
        lead.save()

        # Update the lead banker status
        lead_banker.email_to_lead = True
        lead_banker.save()

        # Set all the packages for the lead with the same bank as submitted.
        LeadPackage.objects.filter(lead=lead, package__bank_id=bank_id).update(submitted=True)

        # Return the success response.
        resp_serializer = SuccessResponseSerializer(instance={
            "message": f"Email sent to {client.email}"
        })
        return Response(status=status.HTTP_200_OK, data=resp_serializer.data)


class EmailToBankerView(APIView):

    def get_object(self, lead_id, bank_id):
        # Get the lead.
        try:
            lead = Lead.objects.with_owner(
                user=self.request.user
            ).prefetch_related('packages', 'packages__package').get(pk=lead_id)
        except Lead.DoesNotExist:
            raise NotFound

        # Check for unpublished packages
        expired = LeadPackage.objects.filter(
            lead=lead,
            package__bank_id=bank_id,
            package__publish=False
        ).exists()
        if expired:
            raise PermissionDenied(
                detail=f"There are inactive packages in your selected list, please remove them."
            )

        # Make sure the PDPA for all clients were approved and a property is added to the lead.
        if Client.objects.filter(lead=lead, pdpa_status=False).exists():
            raise PermissionDenied(detail="All clients has to verify PDPA before the lead can submitted.")

        # Make sure a property was added
        if not getattr(lead, 'property', None):
            raise PermissionDenied(detail="Property for this lead must be added before it can be submitted.")

        try:
            return LeadBanker.objects.filter(
                Q(lead=lead) & Q(banker__bank_id=bank_id)
            ).prefetch_related("lead", "banker", "lead__document_drawer", "banker", "banker__bank").get()
        except LeadBanker.DoesNotExist:
            raise NotFound

    def get_main_client(self, lead):
        return Client.objects.filter(lead=lead, main_applicant=True).first()

    @extend_schema(tags=["Customer - Submissions"], summary="Email to the Banker", responses=EmailBankerResponseSerializer)
    def get(self, request, lead_id, bank_id):
        """
        This endpoint return the data for populating email data screens for the bank
        """
        # Get the lead banker
        lead_banker = self.get_object(lead_id, bank_id)

        # Get the method.
        method = lead_banker.banker.bank.workflow.get_class()
        method.set_object(lead_banker)

        # Populate
        data = dict(
            banker=lead_banker.banker.name,
            email=lead_banker.banker.email,
            subject=method.banker_email_subject(),
            content=method.banker_email_content(),
            attachments=method.banker_attachment_names(),
            law_firm=lead_banker.law_firm,
            own_law_firm=lead_banker.own_law_firm
        )
        serializer = EmailBankerResponseSerializer(instance=data)
        return Response(status=status.HTTP_200_OK, data=serializer.data)

    @extend_schema(tags=["Customer - Submissions"], summary="Email to the Banker",
                   request=EmailBankerRequestSerializer, responses=SuccessResponseSerializer)
    def post(self, request, lead_id, bank_id):
        serializer = EmailBankerRequestSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        # get the posted data
        remarks = serializer.validated_data.get('remarks', "")
        law_firm = serializer.validated_data.get('law_firm')

        # Get the lead banker
        lead_banker = self.get_object(lead_id, bank_id)

        # Get the method.
        method = lead_banker.banker.bank.workflow.get_class()
        method.set_object(lead_banker)

        # Add the law firm to the table.
        if law_firm == 0:
            lead_banker.law_firm = None
            lead_banker.own_law_firm = True
        elif law_firm is None:
            lead_banker.law_firm = None
            lead_banker.own_law_firm = False
        else:
            law_firm_obj = LawFirm.objects.get(pk=law_firm)
            lead_banker.law_firm = law_firm_obj

        lead_banker.save()

        # Create the email record.
        email = Email.objects.create(
            lead=lead_banker.lead,
            email_type=EmailType.banker,
            name=lead_banker.banker.name,
            to_email=lead_banker.banker.email,
            subject=method.banker_email_subject(),
            content=method.banker_email_content(remarks=remarks)
        )
        # Save the attachments.
        for file in method.banker_attachment_files(serializer.validated_data.get("documents")):
            EmailAttachment.objects.create(
                email=email,
                name=file[0],
                file_key=file[1]
            )

        # Send the email
        email.send_email()

        # Set all the packages for the lead with the same bank as submitted.
        LeadPackage.objects.filter(lead_id=lead_id, package__bank_id=bank_id).update(submitted=True)

        # Change the lead status to submit.
        lead = lead_banker.lead
        lead.lead_status = LeadStatusChoices.submitted
        lead.save()

        # Update the lead banker status
        lead_banker.email_to_bank = True
        lead_banker.save()

        # Return the success message.
        resp_serializer = SuccessResponseSerializer(instance={
            "message": f"Email sent to {lead_banker.banker.email}"
        })
        return Response(status=status.HTTP_200_OK, data=resp_serializer.data)


@extend_schema(tags=["Customer - Submissions"], summary="Law Firms for The Lead", responses=EmailLawFirmSerializer)
class LeadLawFirmView(ListAPIView):
    serializer_class = EmailLawFirmSerializer
    pagination_class = None

    def get(self, request, *args, **kwargs):
        # This was added to accommodate the own law firm requirement.
        own_law_firm = [dict(id=0, name="Own Law Firm")]
        law_firms = list(self.get_queryset().values("id", "name"))
        return Response(data=list(chain(law_firms, own_law_firm)))

    def get_queryset(self):
        # Get the lead or fail.
        try:
            lead = Lead.objects.with_owner(user=self.request.user).get(pk=self.kwargs.get("lead_id"))
        except Lead.DoesNotExist:
            raise NotFound
        if not getattr(lead, "property", None):
            raise NotFound(detail="Please add a property to the lead.")
        return LawFirm.objects.filter(
            Q(products__loan_category__contains=[lead.loan_category]) &
            Q(products__property_types__contains=[lead.property.property_type]) &
            Q(products__loan_range_from__lte=lead.loan_amount) &
            Q(products__loan_range_to__gte=lead.loan_amount)
        ).order_by("priority").distinct().all()
