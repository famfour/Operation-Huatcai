from io import BytesIO
from uuid import uuid4
import pyzipper

import requests
from django.conf import settings
from django.template.loader import render_to_string

from rest_framework.exceptions import NotAcceptable

from submission.abstract_submission import AbstractSubmission
from kvstore.models import KVStore


class SendWithDocuments(AbstractSubmission):
    details = "This method sends email to banks with the documents"
    identifier = "with_docs"
    name = "With Docs"
    display_document_drawer = True

    def lead_email_content(self, remarks=None):
        """
        Create the content with the lead template for the method.
        """
        context = dict(
            name=self.main_client.name,
            agent_name=self.agent.full_name,
            agent_phone=f"{self.agent.country_code} {self.agent.mobile}",
            bank_name=self.bank.name,
            bank_form=True if self.bank.forms.exists() else False,
            loan_type=KVStore.get_display_value('loan_category', self.lead.loan_category),
            loan_amount="$ {:,.2f}".format(self.lead.loan_amount),
            magic_link=f"{settings.UI_HOSTNAME}/magic/ui/drawer/{self.lead.document_drawer.id}",
            remarks=remarks if remarks else "",
            banker=self.banker
        )

        return render_to_string("submission/with_documents/lead.html", context=context)

    def lead_attachment_names(self):
        """
        Attachment names depends on the banker
        """
        resp = list()
        mask = any([p.data['mask'] for p in  self.lead.packages.all()])
        if mask:
            resp.append(f"{self.bank.name_masked}-package.pdf".upper())
        else:
            resp.append(f"{self.bank.name}-package.pdf".upper())
        resp.extend([f"{form.name}-{self.bank.name}.pdf".upper() for form in self.bank.forms.all()])
        return resp

    def lead_attachment_files(self):
        """
        return format - [(file_name, amazone_file_key)]
        """
        files = []
        # Add the package pdf.
        files.extend(self.create_package_pdf())
        if not self.bank.forms.exists():
            return files
        for form in self.bank.forms.all():
            # Download all the bank forms and upload to the private bucket.
            file_obj = BytesIO()
            content = requests.get(form.url).content
            file_obj.write(content)
            # upload the PDF to S3 for the email log.
            file_key = f'submission_files/{str(uuid4())}.pdf'
            self.s3_private.upload_file(
                file_key=file_key,
                file_obj=file_obj.getvalue(),
                content_type="application/pdf"
            )
            file_obj.close()
            files.append([f"{form.name}-{self.bank.name}.pdf".upper(), file_key])
        return files

    def banker_email_content(self, remarks=None):
        """
        Create the content for email with the bank tempalte for this method.
        """
        context = dict(
            banker_name=self.banker.name,
            agent_name=self.agent.full_name,
            agent_phone=f"{self.agent.country_code} {self.agent.mobile}",
            lead_name=self.main_client.name,
            lead_phone=f"{self.main_client.country_code} {self.main_client.phone_number}",
            lead_email=self.main_client.email,
            loan_type=KVStore.get_display_value('loan_category', self.lead.loan_category),
            loan_amount="$ {:,.2f}".format(self.lead.loan_amount),
            remarks=remarks if remarks else "",
            law_firm_name=self.lead_banker.get_law_firm_name(),
            has_additional_notice=self.has_additional_notice
        )

        return render_to_string("submission/with_documents/banker.html", context=context)

    def banker_attachment_names(self):
        """
        Get the attachment names for the email.
        """
        return ['LOAN-DETAILS.ZIP']

    def banker_attachment_files(self, documents=None):
        """
        return format - [(file_name, file_path)]
        """
        # Raise exception if no documents are selected.
        if not len(documents):
            raise NotAcceptable(detail="Please select documents to send to the banker.")
        files = list()
        files.extend(self.create_package_pdf())

        # Create the zip buffer and file.
        zip_buffer = BytesIO()

        # If the bank requires encryption, create encrypted zip with banker's phone number as password.
        if self.bank.encrypt_attachments:
            zip_file = pyzipper.AESZipFile(zip_buffer, 'a', pyzipper.ZIP_STORED, encryption=pyzipper.WZ_AES)
            # Set the password
            password = str(self.banker.phone_number).encode()
            zip_file.setpassword(password)
        else:
            zip_file = pyzipper.ZipFile(zip_buffer, 'a', pyzipper.ZIP_STORED)

        # Download all the documents and write to the zip file.
        for doc in documents:
            obj = BytesIO()
            filename = f"{doc.filename}.{doc.extension}"
            self.s3_private.download_object(doc.file_key, obj)
            zip_file.writestr(filename, obj.getvalue())
            obj.close()
        zip_file.close()

        # Upload the zip file to the private bucket.
        file_key = f'submission_files/{uuid4()}.zip'
        self.s3_private.upload_file(
            file_key=file_key,
            file_obj=zip_buffer.getvalue(),
            content_type="application/zip"
        )
        zip_buffer.close()

        files.append(('LOAN-DETAILS.ZIP', file_key))
        return files
