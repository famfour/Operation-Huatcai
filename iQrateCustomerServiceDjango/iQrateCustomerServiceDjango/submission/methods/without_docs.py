from django.conf import settings
from django.template.loader import render_to_string

from kvstore.models import KVStore
from submission.abstract_submission import AbstractSubmission


class SendWithoutDocuments(AbstractSubmission):
    details = "This method sends email to banks without the documents"
    identifier = "without_docs"
    name = "Without Docs"
    display_document_drawer = False

    def lead_email_content(self, remarks=None):
        context = dict(
            name=self.main_client.name,
            agent_name=self.agent.full_name,
            agent_phone=f"{self.agent.country_code} {self.agent.mobile}",
            bank_name=self.bank.name,
            bank_form=False,  # this method does not have the bank form attached to the
            loan_type=KVStore.get_display_value('loan_category', self.lead.loan_category),
            loan_amount="$ {:,.2f}".format(self.lead.loan_amount),
            magic_link=f"{settings.UI_HOSTNAME}/magic/ui/drawer/{self.lead.document_drawer.id}",
            banker_referral_link=self.lead_banker.banker.referral_link,
            remarks=remarks if remarks else "",
            banker=self.banker
        )
        return render_to_string("submission/without_documents/lead.html", context=context)

    def lead_attachment_names(self):
        mask = any([p.data['mask'] for p in self.lead.packages.all()])
        if mask:
            return [f"{self.bank.name_masked}-package.pdf".upper()]
        else:
            return [f"{self.bank.name}-package.pdf".upper()]

    def lead_attachment_files(self):
        """
        return format - [(file_name, file_path)]
        """
        return self.create_package_pdf()

    def banker_email_content(self, remarks=None):
        context = dict(
            banker_name=self.banker.name,
            agent_name=self.agent.full_name,
            agent_phone=f"{self.agent.country_code} {self.agent.mobile}",
            lead_name=self.main_client.name,
            lead_phone=f"{self.main_client.country_code} {self.main_client.phone_number}",
            lead_email=self.main_client.email,
            loan_type=KVStore.get_display_value('loan_category', self.lead.loan_category),
            loan_amount="$ {:,.2f}".format(self.lead.loan_amount),
            remarks=remarks if remarks else "",
            law_firm_name=self.lead_banker.get_law_firm_name(),
            has_additional_notice=self.has_additional_notice
        )
        return render_to_string("submission/without_documents/banker.html", context=context)

    def banker_attachment_names(self):
        """
        Get the attachment names for the email.
        """
        return [f"{self.bank.name}-package.pdf".upper()]

    def banker_attachment_files(self, documents=None):
        """
        return format - [(file_name, file_path)]
        """
        return self.create_package_pdf()
