import django_filters
from submission.models import EmailType


class BankFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr="icontains")


class EmailFilter(django_filters.FilterSet):
    email_type = django_filters.ChoiceFilter(choices=EmailType.choices, field_name="email_type")
