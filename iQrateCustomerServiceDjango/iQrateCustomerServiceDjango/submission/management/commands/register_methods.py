import inspect
from django.core.management import BaseCommand
from bank.models import SubmissionMethod
from submission import methods


class Command(BaseCommand):
    help = "Insert the methods in database"

    def handle(self, *args, **options):
        classes = []
        for name, obj in inspect.getmembers(methods):
            if inspect.isclass(obj):
                classes.append(obj)

        for c in classes:
            obj = c()
            SubmissionMethod.objects.update_or_create(identifier=obj.identifier, defaults=dict(
                name=obj.name,
                classpath=f"{c.__module__}.{c.__name__}",
                details=obj.details
            ))
