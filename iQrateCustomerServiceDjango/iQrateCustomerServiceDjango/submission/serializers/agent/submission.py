from drf_spectacular.utils import extend_schema_field
from rest_framework import serializers

from bank.serializers.agent import CustomerBankSerializer
from lawfirm.models import LawFirm
from submission.models import LeadBanker, Email, EmailType
from magic.models import Document


class LeadBanksSerializer(serializers.ModelSerializer):
    bank = serializers.SerializerMethodField(help_text="Bank Data")
    display_document_drawer = serializers.SerializerMethodField(
        help_text="Should there be a document selection for this bank"
    )

    @extend_schema_field(field=CustomerBankSerializer)
    def get_bank(self, obj):
        return CustomerBankSerializer(instance=obj.banker.bank).data

    @extend_schema_field(field=serializers.BooleanField)
    def get_display_document_drawer(self, obj):
        method = obj.banker.bank.workflow.get_class()
        return method.display_document_drawer

    class Meta:
        model = LeadBanker
        fields = ['bank', 'email_to_bank', 'email_to_lead', 'display_document_drawer']
        read_only_fields = fields


class EmailLeadRequestSerializer(serializers.Serializer):
    remarks = serializers.CharField(allow_null=True, allow_blank=True, help_text="Remarks")


class EmailLeadResponseSerializer(serializers.Serializer):
    name = serializers.CharField(help_text="Name of the lead client's name")
    email = serializers.EmailField(help_text="Main client's email")
    subject = serializers.CharField(help_text="Email Subject")
    content = serializers.CharField(help_text="Email body")
    attachments = serializers.ListField(
        child=serializers.CharField(), default=list(),
        help_text="Name of all the attachments"
    )


class EmailBankerResponseSerializer(serializers.Serializer):
    """
    Email data for Banker emails
    """
    banker = serializers.CharField(help_text="Name of the banker")
    email = serializers.EmailField(help_text="Banker email")
    subject = serializers.CharField(help_text="Email subject")
    content = serializers.CharField(help_text="Email content")
    attachments = serializers.ListField(
        child=serializers.CharField(), default=list(),
        help_text="Name of the attachments")
    law_firm = serializers.SerializerMethodField(allow_null=True, method_name='get_law_firm')

    @extend_schema_field(field=serializers.IntegerField)
    def get_law_firm(self, obj):
        """
        law firm null & no own loan firm - No law firm selected.
        law firm null &  own law firm - Own Law firm
        """
        if not obj.get('law_firm', None) and not obj.get('own_law_firm', None):
            return None
        if not obj.get('law_firm', None) and obj.get('own_law_firm', None):
            return 0
        return obj.get('law_firm').pk


class EmailBankerRequestSerializer(serializers.Serializer):
    law_firm = serializers.IntegerField(
        help_text="Selected law firm ID",
        allow_null=True
    )
    remarks = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        help_text="Content from the remarks field."
    )
    documents = serializers.ListField(
        child=serializers.PrimaryKeyRelatedField(
            queryset=Document.objects.all(),
        ),
        default=list(),
        help_text="List of documents pks to be zipped and attached to the email."
    )

    def validate_law_firm(self, value):
        if not value in [None, 0]:
            try:
                LawFirm.objects.get(pk=value)
            except LawFirm.DoesNotExist:
                raise serializers.ValidationError("Invalid value for lawfirm")
        return value


class EmailLawFirmSerializer(serializers.ModelSerializer):
    class Meta:
        model = LawFirm
        fields = ['id', 'name']
