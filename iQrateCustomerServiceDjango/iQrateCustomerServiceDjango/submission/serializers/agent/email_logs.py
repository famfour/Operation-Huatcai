from drf_spectacular.utils import extend_schema_field
from rest_framework import serializers

from django.conf import settings

from submission.models import Email, EmailType, EmailAttachment
from common.serializers import CommonModelSerializer
from common.s3client import S3Client


class EmailAttachmentSerializer(serializers.HyperlinkedModelSerializer):
    """
    Email attachment files.
    """
    file = serializers.SerializerMethodField(help_text="Presigned temporary S3 URL")

    @extend_schema_field(field=serializers.URLField)
    def get_file(self, obj):
        client = S3Client(bucket_name=settings.AWS_PRIVATE_BUCKET_NAME)
        return client.get_presigned_url(
            obj.file_key,
            expire_in=600,
            content_disposition=f"attachment;filename={obj.name}"
        )

    class Meta:
        model = EmailAttachment
        fields = ['name', 'file']


class EmailSerializer(CommonModelSerializer):
    """
    Serlializer for Email Logs
    """
    attachments = EmailAttachmentSerializer(many=True)

    class Meta:
        model = Email
        fields = ['id', 'email_type', 'name', 'subject', 'to_email', 'content',
                  'sent', 'attachments', 'created', 'updated']
