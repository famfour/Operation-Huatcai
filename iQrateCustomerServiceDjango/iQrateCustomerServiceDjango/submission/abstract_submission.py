from abc import abstractmethod
from io import BytesIO
from uuid import uuid4

from django.conf import settings
from django.template.loader import render_to_string
from xhtml2pdf import pisa

from common.s3client import S3Client
from lead.models import LeadPackage
from submission.models import LeadBanker
from kvstore.choices import LeadTypeChoices, LoanCategoryChoices


class AbstractSubmission():
    """
    Abstract class for creating bank submission methods.
    """

    def __init__(self):
        self.s3_public = None
        self.s3_private = None
        self.bank = None
        self.banker = None
        self.agent = None
        self.main_client = None
        self.lead = None
        self.lead_banker = None
        self.law_firm = None

    def set_object(self, lead_banker: LeadBanker = None):
        self.lead_banker = lead_banker
        self.lead = self.lead_banker.lead
        self.agent = self.lead.get_main_agent()
        self.main_client = self.lead.clients.filter(main_applicant=True).first()
        self.bank = self.lead_banker.banker.bank
        self.banker = self.lead_banker.banker
        self.law_firm = self.lead_banker.law_firm
        self.own_law_firm = self.lead_banker.own_law_firm
        self.has_additional_notice = (self.lead.lead_type == LeadTypeChoices.yourself) and (
                    self.lead.loan_category in [LoanCategoryChoices.new_purchase_with_option_to_purchase,
                                                LoanCategoryChoices.new_purchase_approval_in_principle])

        # Create s3 clients
        self.s3_public = S3Client(settings.AWS_STORAGE_BUCKET_NAME)
        self.s3_private = S3Client(settings.AWS_PRIVATE_BUCKET_NAME)

    @property
    @abstractmethod
    def name(self):
        """
        This will be the name of the method.
        """
        pass

    @property
    @abstractmethod
    def identifier(self):
        """
        The identifier for this method, Should be a slug.
        """
        pass

    @property
    @abstractmethod
    def display_document_drawer(self):
        """
        The identifier for this method, Should be a slug.
        """
        pass

    @property
    @abstractmethod
    def details(self):
        """
        details such as of steps in this method, returns string.
        """
        pass

    @abstractmethod
    def lead_email_content(self, remarks=None):
        """
        Email content for sending mail to lead.
        Should return html.
        """
        pass

    def lead_email_subject(self):
        """
        Get the email subject for lead.
        """
        return f"IQrate-Home Loan Application - {self.main_client.obfuscated_name}"

    @abstractmethod
    def lead_attachment_names(self):
        """
        Get the attachment names for the email.
        """
        pass

    @abstractmethod
    def lead_attachment_files(self):
        """
        Downlaod all the required files to the disk and
        return format: [(file_name_verbose, s3_bucket_key)]
        """
        pass

    @abstractmethod
    def banker_email_content(self, remarks=None):
        """
        Email content for sending email to banker.
        Should return html
        """
        pass

    def banker_email_subject(self):
        """
        Get the email subject for banker.
        """
        return f"IQrate-Home Loan Application - {self.main_client.obfuscated_name}"

    @abstractmethod
    def banker_attachment_names(self):
        """
        Get the attachment names for the email.
        """
        pass

    @abstractmethod
    def banker_attachment_files(self, documents=None):
        """
        Downlaod all the required files to the disk and
        return format: [(file_name_verbose, amazone_file_key)]
        """
        pass

    def create_package_pdf(self):
        # Write the PDF to memory
        file_obj = BytesIO()
        # Get all the packages for the current bank and create the PDF from it.
        package_objets = LeadPackage.objects.filter(lead=self.lead, package__bank=self.bank).all()
        package_data = [p.data for p in package_objets]
        data = dict()
        keys = package_data[0].keys()
        for key in keys:
            data[key] = [item.get(key, "") for item in package_data]

        html = render_to_string("submission/pdf/packages.html", context=dict(packages=data))
        pisa_status = pisa.CreatePDF(html, dest=file_obj)
        # upload the PDF to S3 for the email log.
        file_key = f'submission_files/{uuid4()}.pdf'
        self.s3_private.upload_file(
            file_key=file_key,
            file_obj=file_obj.getvalue(),
            content_type="application/pdf"
        )
        file_obj.close()
        if pisa_status.err:
            return None
        package_name = f"{self.bank.name}-package.pdf".upper()
        if any([package['mask'] for package in package_data]):
            package_name = f"{self.bank.name_masked}-package.pdf".upper()
        return [(package_name, file_key)]
