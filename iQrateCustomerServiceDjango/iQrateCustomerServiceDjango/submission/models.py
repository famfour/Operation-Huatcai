from io import BytesIO

from django.conf import settings
from django.contrib.postgres.fields import ArrayField
from django.core.mail import EmailMessage
from django.db import models

import mimefinder
from bank.models import Banker
from common.models import ModelMixin
from common.s3client import S3Client
from lawfirm.models import LawFirm
from lead.models import Lead


class LeadBanker(ModelMixin):
    lead = models.ForeignKey(Lead, related_name='bankers', on_delete=models.PROTECT)
    banker = models.ForeignKey(Banker, related_name='leads', on_delete=models.PROTECT)
    law_firm = models.ForeignKey(LawFirm, related_name="lead_bankers", on_delete=models.PROTECT, null=True)
    own_law_firm = models.BooleanField(default=False)
    email_to_lead = models.BooleanField(default=False, help_text="Submission step - Email to Lead")
    email_to_bank = models.BooleanField(default=False, help_text="Submission step - Email to Banker")
    agent_data = models.JSONField(
        null=True,
        help_text="Agent's commission data at the time of submission to bank"
    )

    def get_law_firm_name(self):
        if not self.law_firm and self.own_law_firm:
            return "Own Law Firm"
        if not self.law_firm and not self.own_law_firm:
            return "-"
        return self.law_firm.name

    class Meta:
        unique_together = ['lead', 'banker']  # There should be one unique banker per lead.


class EmailType(models.TextChoices):
    banker = ("banker", "Banker")
    lead = ("lead", "Lead")
    law_firm = ("law_firm", "Law Firm")


class Email(ModelMixin):
    lead = models.ForeignKey(Lead, related_name="lead_emails", on_delete=models.PROTECT, null=True)
    email_type = models.CharField(
        max_length=16,
        choices=EmailType.choices,
        help_text="Sent to which party ? Banker/Client/LawFirm"
    )
    name = models.CharField(max_length=254, help_text="To name")
    subject = models.CharField(max_length=254, help_text="Email Subject")
    to_email = models.EmailField(help_text="Recipient Email")
    cc_emails = ArrayField(
        base_field=models.EmailField(),
        null=True,
        blank=True,
        help_text="Emails to which this emails is ccd to"
    )
    content = models.TextField(help_text="Email body")
    sent = models.BooleanField(default=False, help_text="Has this email been sent")

    class Meta:
        ordering = ['-created']

    def send_email(self):
        """
        Send the email
        """
        # Load the mime finder
        mime = mimefinder.Magic(mime=True)

        # Remove to email from cc emails just in case
        to_email = self.to_email.lower()
        cc_emails = [e.lower() for e in self.cc_emails] if self.cc_emails else list()
        if to_email in cc_emails:
            cc_emails.remove(to_email)

        # Create the email.
        mail = EmailMessage(
            from_email=settings.DEFAULT_FROM_EMAIL,
            to=[to_email],
            cc=cc_emails,
            subject=self.subject
        )
        mail.template_id = settings.SENDGRID_TEMPLATE_NAME
        mail.dynamic_template_data = dict(
            body=self.content,
            subject=self.subject,
        )

        # Download and add the atachments.
        client = S3Client(bucket_name=settings.AWS_PRIVATE_BUCKET_NAME)
        for attachment in self.attachments.all():
            data = BytesIO()
            client.download_object(attachment.file_key, data)
            file_object = data.getvalue()
            mime_type = mime.from_buffer(file_object)
            mail.attach(attachment.name, file_object, mime_type)

        mail.send()
        self.sent = True
        self.save()


class EmailAttachment(ModelMixin):
    email = models.ForeignKey(Email, related_name="attachments", on_delete=models.PROTECT)
    name = models.CharField(max_length=254, help_text="Name of the file")
    file_key = models.CharField(max_length=254, null=True, help_text="File key in S3 bucket.")

    def __str__(self):
        return self.name
