from django.urls import path
from .views import agent

app_name = "submission"

agent_patterns = [
    path("banks/<int:lead_id>", agent.LeadBanksAPIView.as_view(), name="banks"),
    path("email-lead/<int:lead_id>/<int:bank_id>", agent.EmailToLeadView.as_view(), name="email_lead"),
    path("email-bank/<int:lead_id>/<int:bank_id>", agent.EmailToBankerView.as_view(), name="email_banker"),
    path("lawfirms/<int:lead_id>", agent.LeadLawFirmView.as_view(), name="law_firms"),
    path("email-logs/<int:lead_id>", agent.EmailLogsListView.as_view(), name="email_logs"),
    path("email-logs/detail/<int:email_id>", agent.EmailDetailView.as_view(), name="email_detail"),
]
