from drf_spectacular.utils import extend_schema
from rest_framework.generics import ListAPIView
from .models import User
from .serializer import AdminCoBrokeUserSerializer


@extend_schema(tags=["Admin - User"], summary="Get a list Co Brokers", responses=AdminCoBrokeUserSerializer)
class AdminCoBrokeUserListView(ListAPIView):
    serializer_class = AdminCoBrokeUserSerializer
    pagination_class = None

    def get_queryset(self):
        return User.objects.filter(cobroke_status=1).all()
