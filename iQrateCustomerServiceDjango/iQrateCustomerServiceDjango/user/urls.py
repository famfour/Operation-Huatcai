from django.urls import path
from .views import AdminCoBrokeUserListView

app_name = "user"

admin_patterns = [
    path("co-brokers", AdminCoBrokeUserListView.as_view(), name="co_brokers"),
]
