from django.conf import settings
from django.db import models
from requests import Session
from rest_framework.exceptions import PermissionDenied


class User(models.Model):
    """
    This model connects to the user_data view created from keycloak.

    """
    id = models.UUIDField(primary_key=True, unique=True)
    full_name = models.CharField(max_length=254)
    email = models.EmailField()
    mobile = models.CharField(max_length=32, null=True)
    country_code = models.CharField(null=True, max_length=12)
    membership_type = models.CharField(max_length=64, null=True)
    user_type = models.CharField(max_length=64, null=True)
    cobroke_status = models.IntegerField(null=True)

    class Meta:
        """
        Django shouldn't create any migrations for this model as this is a view\
        and not a database table.
        """
        managed = False
        db_table = 'user_data'

    def __str__(self):
        return self.full_name

    @property
    def is_authenticated(self):
        """
        Users are authenticated before arriving at the app.
        """
        return True

    def get_rates(self):
        """
        Gets the rates from user service and returns the rates based \n
        on the user's membership type.
        """
        try:
            rates = dict()
            session = Session()
            data = session.get(settings.RATES_ENDPOINT).json()
            session.close()

            for item in data[0].get("codeValueJson"):
                rates[item.get('membershiptype')] = {
                    'clearance_fee_percentage': float(item.get("clearance_fee_percentage")),
                    'co_broker_fee_percentage': float(item.get('co-broker_fee_percentage')),
                    'self_co_broker_fee_percentage': float(item.get('self-co-broker_fee_percentage'))
                }
            return rates[self.membership_type]
        except:
            raise PermissionDenied(detail="Unable to get rates, please try again")

    @property
    def pk(self):
        return self.id

    def get_username(self):
        return self.email


class UserBank(models.Model):
    id = models.IntegerField(primary_key=True)
    user_id = models.CharField(db_column="userId", max_length=64)
    bank_name = models.CharField(max_length=254, null=True, blank=True)
    bank_ac_number = models.CharField(max_length=254, null=True, blank=True)
    bank_branch_code = models.CharField(max_length=254, null=True, blank=True)
    bank_nation = models.CharField(max_length=254, null=True, blank=True)
    bank_nation_id = models.CharField(max_length=254, null=True, blank=True)

    class Meta:
        managed = False
        db_table = 'user_bank'
        read_only_model = True
