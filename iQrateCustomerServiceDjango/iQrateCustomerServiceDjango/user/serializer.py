from drf_spectacular.utils import extend_schema, extend_schema_field
from rest_framework import serializers
from .models import User, UserBank


class UserSerializer(serializers.HyperlinkedModelSerializer):
    name = serializers.SerializerMethodField(method_name="get_name")
    phone_number = serializers.SerializerMethodField()

    def get_name(self, obj) -> str:
        return obj.full_name

    def get_phone_number(self, obj) -> str:
        return f"{obj.country_code} {obj.mobile}"

    class Meta:
        model = User
        fields = ['name', 'phone_number']


class AdminUserBankSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserBank
        fields = ['id', 'bank_name', 'bank_ac_number', 'bank_branch_code',
                  'bank_nation', 'bank_nation_id']


class AdminUserSerializer(serializers.ModelSerializer):
    bank = serializers.SerializerMethodField()

    @extend_schema_field(field=AdminUserBankSerializer)
    def get_bank(self, obj):
        bank = UserBank.objects.using('keycloak').filter(user_id=obj.id).first()
        if not bank:
            return None
        return AdminUserBankSerializer(instance=bank).data

    class Meta:
        model = User
        fields = ['id', 'full_name', 'email', 'mobile', 'country_code', 'membership_type', 'user_type', 'bank']


class AdminCoBrokeUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'full_name']
