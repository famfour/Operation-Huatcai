from django.dispatch import receiver
from django.db.models.signals import post_save
from fieldsignals import pre_save_changed, post_save_changed

from payout.models import Payout, LoanRate, PayoutDetail
from kvstore.choices import LeadStatusChoices, PayoutStatusChoices


@receiver(post_save, sender=Payout)
def update_lead_status(sender, instance, created, **kwargs):
    """
    Update the lead status to 'Won' on adding the payout request.
    """
    if not created:
        return  # Not the first submission.
    lead = instance.lead
    lead.lead_status = LeadStatusChoices.won
    lead.save()


@receiver(post_save, sender=LoanRate)
def update_total_interest_rate(sender, instance, created, **kwargs):
    if created:
        instance.recalculate_total_rate()


@receiver(post_save, sender=Payout)
def create_payout_detail_instance(sender, instance, created, **kwargs):
    """
    Update the lead status to 'Won' on adding the payout request.
    """
    if not created:
        return  # Not the first submission.

    PayoutDetail.objects.create(payout=instance)

def update_lead_status_rework(sender, instance, **kwargs):
    """
    Updated Lead status when, payout status is rework required.
    """
    if instance.status == PayoutStatusChoices.rework_required:
        instance.lead.lead_status = LeadStatusChoices.rework
        instance.lead.save()
    if instance.status in [
        PayoutStatusChoices.pending_user_payout,
        PayoutStatusChoices.pending_user_payout,
        PayoutStatusChoices.pending_banks_payment]:
        instance.lead.lead_status = LeadStatusChoices.won
        instance.lead.save()


post_save_changed.connect(update_lead_status_rework, sender=Payout, fields=['status'])