from computedfields.models import ComputedFieldsModel, computed
from django.db import models

from bank.choices import PackageYearChoices
from bank.models import Bank, Banker, Rate
from common.models import ModelMixin
from kvstore.choices import RateTypeChoices, PayoutStatusChoices
from lawfirm.models import LawFirm
from lead.models import Lead


class Payout(ComputedFieldsModel):
    lead = models.OneToOneField(Lead, related_name="payout", on_delete=models.PROTECT)
    bank = models.ForeignKey(Bank, related_name="payouts", on_delete=models.PROTECT)
    banker = models.ForeignKey(Banker, related_name="payouts", on_delete=models.PROTECT)
    loan_acceptance_date = models.DateField()
    lo_reference_number = models.CharField(max_length=128)
    bank_referral_fee = models.FloatField()

    law_firm = models.ForeignKey(LawFirm, related_name="payouts", on_delete=models.PROTECT, null=True, blank=True)
    own_law_firm_name = models.CharField(max_length=128, null=True, blank=True)
    legal_fee = models.FloatField()

    # Fields for admin
    status = models.CharField(
        max_length=254,
        choices=PayoutStatusChoices.choices,
        default=PayoutStatusChoices.pending_verification
    )
    remarks = models.CharField(
        max_length=254,
        null=True,
        help_text="Get the values from kvstore key - payout_remarks"
    )
    other_remarks = models.TextField(null=True, blank=True)

    # fields Modelmixin
    created_by = models.UUIDField(null=True, help_text="User ID of the agent or admin who created this record.")
    updated_by = models.UUIDField(null=True, help_text="User ID of the agent or admin who last updated this record.")
    created = models.DateTimeField(auto_now_add=True, help_text="Date and time at which this record is created")
    updated = models.DateTimeField(auto_now=True, help_text="Date and time at which this record is last updated.")

    @computed(models.FloatField(default=0), depends=[('self', ['loan_amount']), ('self', ['bank_referral_fee'])])
    def gross_commission(self):
        return self.loan_amount * self.bank_referral_fee / 100

    @computed(models.FloatField(default=0), depends=[('loans', ['loan_amount'])])
    def loan_amount(self):
        if not self.pk:
            return 0
        return sum([loan.loan_amount for loan in self.loans.all()])

    class Meta:
        ordering = ['-created']


class PayoutDetail(ModelMixin):
    payout = models.OneToOneField(Payout, related_name="details", on_delete=models.CASCADE)
    banks_payout_date = models.DateField(null=True)
    iqrate_clearance_fee = models.FloatField(default=0)
    iqrate_payout_date = models.DateField(null=True)
    broker_commission = models.FloatField(default=0)
    broker_payout_date = models.DateField(null=True)
    co_broker_commission = models.FloatField(default=0)
    co_broker_payout_date = models.DateField(null=True)


class Loan(ModelMixin):
    payout = models.ForeignKey(Payout, related_name="loans", on_delete=models.CASCADE)
    loan_amount = models.FloatField(default=0)
    rate_type = models.CharField(choices=RateTypeChoices.choices, default=RateTypeChoices.fixed, max_length=64)

    lock_in_period = models.PositiveSmallIntegerField()
    clawback_period = models.PositiveSmallIntegerField(null=True, blank=True)
    cash_rebate_subsidy = models.FloatField(null=True, blank=True)
    next_review_date = models.DateField()


class LoanRate(ModelMixin):
    loan = models.ForeignKey(Loan, related_name="rates", on_delete=models.CASCADE)
    year = models.CharField(max_length=32, choices=PackageYearChoices.choices)
    reference_rate = models.ForeignKey(Rate, on_delete=models.PROTECT, related_name="loan_rates")
    bank_spread = models.FloatField(default=0)
    total_interest_rate = models.FloatField(
        default=0,
        help_text="Total interest rate calculated from reference rate's interest rate and bank spread"
    )

    def recalculate_total_rate(self, save=True):
        """
        Calculate total interest rate based on
        1. Interest Rate of 'reference_rate'
        2. Equation of 'reference_rate'
        3. 'bank_spread'
        """
        self.total_interest_rate = self.reference_rate.interest_rate
        if self.reference_rate.equation == "+":
            self.total_interest_rate = self.reference_rate.interest_rate + self.bank_spread
        if self.reference_rate.equation == "-":
            self.total_interest_rate = self.reference_rate.interest_rate - self.bank_spread
        self.total_interest_rate = round(self.total_interest_rate, 2)
        if save:
            self.save()
