# Generated by Django 4.1 on 2022-09-12 06:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("payout", "0008_loan_created_loan_created_by_loan_updated_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="payout",
            name="loan_amount",
            field=models.FloatField(default=0, editable=False),
        ),
    ]
