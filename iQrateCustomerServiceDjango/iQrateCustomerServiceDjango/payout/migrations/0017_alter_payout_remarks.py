# Generated by Django 4.1 on 2022-10-14 07:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("payout", "0016_auto_20220914_0729"),
    ]

    operations = [
        migrations.AlterField(
            model_name="payout",
            name="remarks",
            field=models.CharField(
                choices=[
                    ("wrong_lo_uploaded", "Wrong LO Uploaded"),
                    ("wrong_name", "Client's Name not as per LO"),
                    (
                        "wrong_loan_amount",
                        "Loan Amount not as per LO- Reference Number not as per LO",
                    ),
                    ("duplicate", "Duplicate submission"),
                ],
                help_text="Get the values from kvstore key - payout_remarks",
                max_length=254,
                null=True,
            ),
        ),
    ]
