from drf_spectacular.utils import extend_schema_field
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from bank.models import Bank, Banker
from bank.serializers.agent import CustomerBankSerializer, CustomerBankerSerializer
from common.serializers import CommonModelSerializer
from kvstore.models import KVStore
from lawfirm.models import LawFirm
from lawfirm.serializers.agent import LawFirmAgentHyperLinkedSerializer
from lead.models import Lead
from lead.validators import ValidateLeadOwnership
from magic.serializers.agent import AgentDocumentHyperLinkedSerializer
from payout.models import Payout, Loan, LoanRate
from payout.validators import ValidateBanker, ValidateLawFirmName, ValidateLeadStatus


class AgentLoanRateSerializer(CommonModelSerializer):
    class Meta:
        model = LoanRate
        exclude = ['created_by', 'updated_by', 'loan', 'id', 'created', 'updated']
        read_only_fields = ['total_interest_rate']


class AgentLoanSerializer(CommonModelSerializer):
    rate_type = serializers.ChoiceField(
        choices=KVStore.get_choices("rate_type"),
        help_text="From KVStore - 'rate_type'"
    )
    lock_in_period = serializers.ChoiceField(
        choices=KVStore.get_choices("lock_in_period_years"), required=False,
        help_text="From KVStore - 'lock_in_period_years'"
    )

    rates = AgentLoanRateSerializer(many=True, allow_null=True)

    class Meta:
        model = Loan
        exclude = ['created', 'updated', 'created_by', 'updated_by', 'payout', 'id']


class AgentPayoutCreateSerializer(CommonModelSerializer):
    """
    Serializer for creating payouts
    """
    lead = serializers.PrimaryKeyRelatedField(
        queryset=Lead.objects.all(),
        validators=[ValidateLeadOwnership(), ValidateLeadStatus()]
    )
    bank = serializers.PrimaryKeyRelatedField(
        queryset=Bank.objects.all()
    )
    banker = serializers.PrimaryKeyRelatedField(
        queryset=Banker.objects.all()
    )
    law_firm_id = serializers.IntegerField(
        allow_null=True,
    )

    loans = AgentLoanSerializer(many=True, required=False, allow_null=True)

    def validate_law_firm_id(self, value):
        if value == 0:
            return None
        try:
            LawFirm.objects.get(pk=value)
        except:
            raise ValidationError(detail="Invalid lawfirm ID")
        return value

    def create(self, validated_data):
        loans = validated_data.pop("loans")
        payout = super(AgentPayoutCreateSerializer, self).create(validated_data)
        # Delete all the loans and recreate them
        payout.loans.all().delete()
        for loan in loans:
            rates = loan.pop("rates")
            loan = Loan.objects.create(payout=payout, **loan)
            for rate in rates:
                LoanRate.objects.create(loan=loan, **rate)
        return payout

    def update(self, instance, validated_data):
        loans = validated_data.pop("loans")
        payout = super(AgentPayoutCreateSerializer, self).update(instance=instance, validated_data=validated_data)
        # Delete all the loans and recreate them
        payout.loans.all().delete()
        for loan in loans:
            rates = loan.pop("rates")
            loan = Loan.objects.create(payout=payout, **loan)
            if not rates:
                continue
            for rate in rates:
                LoanRate.objects.create(loan=loan, **rate)
        return payout

    class Meta:
        model = Payout
        exclude = ['created', 'updated', 'created_by', 'updated_by']
        read_only_fields = ['status', 'remarks']
        validators = [
            ValidateBanker(),
            ValidateLawFirmName()
        ]


class AgentPayoutDetailSerializer(CommonModelSerializer):
    """
    Serializer for payout details.
    """
    bank = CustomerBankSerializer()
    banker = CustomerBankerSerializer()
    loans = AgentLoanSerializer(many=True)
    documents = serializers.SerializerMethodField()
    law_firm_id = serializers.IntegerField()
    law_firm = LawFirmAgentHyperLinkedSerializer()

    @extend_schema_field(field=AgentDocumentHyperLinkedSerializer)
    def get_documents(self, obj):
        return AgentDocumentHyperLinkedSerializer(instance=obj.lead.document_drawer.documents, many=True).data

    class Meta:
        model = Payout
        exclude = ['created', 'updated', 'created_by', 'updated_by']
