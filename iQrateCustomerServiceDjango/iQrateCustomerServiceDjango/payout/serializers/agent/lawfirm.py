from drf_spectacular.utils import extend_schema_field
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from bank.models import Bank, Banker
from bank.serializers.agent import CustomerBankSerializer, CustomerBankerSerializer
from common.serializers import CommonModelSerializer
from kvstore.models import KVStore
from lawfirm.models import LawFirm
from lead.models import Lead
from lead.validators import ValidateLeadOwnership
from magic.serializers.agent import AgentDocumentHyperLinkedSerializer
from payout.models import Payout, Loan, LoanRate
from payout.validators import ValidateBanker, ValidateLawFirmName, ValidateLeadStatus
from magic.models import Document


class PayoutLawFirmListResponseSerializer(serializers.ModelSerializer):
    legal_fee = serializers.SerializerMethodField()

    @extend_schema_field(field=serializers.FloatField)
    def get_legal_fee(self, obj):
        return 1000

    class Meta:
        model = LawFirm
        fields = ["id", "name", "email", "legal_fee"]


class PayoutLawfirmEmailResponseSerializer(serializers.Serializer):
    """
    Response schema for prepare email view for law firm.
    """
    email = serializers.EmailField(allow_blank=True, allow_null=True)
    subject = serializers.CharField(help_text="Email Subject")
    content = serializers.CharField(help_text="Email Body")


class PayoutLawfirmEmailRequestSerializer(serializers.Serializer):
    """
    Request schema to send email tp law firm.
    """
    law_firm_id = serializers.IntegerField(
        help_text="ID of the selected Law Firm, If using own lawfirm, the value for this field should be 0"
    )
    own_law_firm_name = serializers.CharField(required=False, allow_null=True, allow_blank=True)
    own_law_firm_email = serializers.EmailField(required=False, allow_null=True, allow_blank=True)
    legal_fee = serializers.FloatField(required=False, default=0)
    remarks = serializers.CharField(required=False, allow_null=True, allow_blank=True)
    document_ids = serializers.ListField(
        child=serializers.PrimaryKeyRelatedField(queryset=Document.objects.all()),
        help_text="ID (UUID) of the documents selected by the agent."
    )

    def validate_law_firm(self, value):
        if value == 0:
            return None
        try:
            return LawFirm.objects.get(pk=value)
        except:
            raise ValidationError(detail="Invalid Law Firm")
