from drf_spectacular.utils import extend_schema_field
from rest_framework import serializers

from bank.serializers.admin import BankerResponseSerializer
from common.serializers import CommonModelSerializer
from kvstore.models import KVStore
from lead.serializers.admin import AdminClientDetailSerializer, AdminPropertyDetailSerializer
from magic.serializers.admin import AdminDocumentDrawerDetailSerializer
from payout.models import Payout, Loan, LoanRate, PayoutDetail
from user.models import User
from user.serializer import AdminUserSerializer


class NamePhoneSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=254)
    phone = serializers.CharField(max_length=32)


class AdminPayoutListSerializer(CommonModelSerializer):
    client = serializers.SerializerMethodField()
    agent = serializers.SerializerMethodField()
    co_broke_agent = serializers.SerializerMethodField(allow_null=True)
    bank = serializers.SerializerMethodField()
    banker = serializers.SerializerMethodField()

    @extend_schema_field(field=NamePhoneSerializer)
    def get_client(self, obj):
        client = obj.lead.clients.first()
        return NamePhoneSerializer(instance=dict(
            name=client.name,
            phone=f"+{client.country_code} {client.phone_number}"
        )).data

    @extend_schema_field(field=NamePhoneSerializer)
    def get_agent(self, obj):
        agent = User.objects.filter(id=obj.lead.agent_applied).first()
        if not agent:
            return None
        return NamePhoneSerializer(instance=dict(
            name=agent.full_name,
            phone=f"+{agent.country_code} {agent.mobile}"
        )).data

    @extend_schema_field(field=NamePhoneSerializer)
    def get_co_broke_agent(self, obj):
        if not obj.lead.co_broke_agent:
            return None
        agent = User.objects.filter(id=obj.lead.co_broke_agent).first()
        if not agent:
            return None
        return NamePhoneSerializer(instance=dict(
            name=agent.full_name,
            phone=f"+{agent.country_code} {agent.mobile}"
        )).data

    @extend_schema_field(field=serializers.CharField)
    def get_bank(self, obj):
        return obj.bank.name

    @extend_schema_field(field=NamePhoneSerializer)
    def get_banker(self, obj):
        return NamePhoneSerializer(instance=dict(
            name=obj.banker.name,
            phone=f"+{obj.banker.country_code} {obj.banker.phone_number}"
        )).data

    class Meta:
        model = Payout
        exclude = ['created_by', 'updated_by', 'created', 'updated']


class AdminLoanRateSerializer(CommonModelSerializer):
    reference_rate_name = serializers.SerializerMethodField()

    @extend_schema_field(field=serializers.CharField)
    def get_reference_rate_name(self, obj):
        return obj.reference_rate.reference

    class Meta:
        model = LoanRate
        exclude = ['created_by', 'updated_by', 'created', 'updated', 'id']


class AdminLoanDetailSerializer(CommonModelSerializer):
    rates = AdminLoanRateSerializer(many=True, allow_null=True)

    class Meta:
        model = Loan
        exclude = ['created_by', 'updated_by', 'created', 'updated', 'id']


class AdminPayoutPaymentDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = PayoutDetail
        exclude = ['created_by', 'updated_by', 'created', 'updated', 'payout']


class AdminPayoutPayoutDetailSerializer(CommonModelSerializer):
    clients = serializers.SerializerMethodField()
    property = serializers.SerializerMethodField()
    agent_applied = serializers.SerializerMethodField()
    co_broke_agent = serializers.SerializerMethodField(allow_null=True)
    banker = serializers.SerializerMethodField()
    loans = AdminLoanDetailSerializer(many=True)
    details = AdminPayoutPaymentDetailSerializer()
    document_drawer = serializers.SerializerMethodField()

    @extend_schema_field(field=AdminDocumentDrawerDetailSerializer)
    def get_document_drawer(self, obj):
        return AdminDocumentDrawerDetailSerializer(instance=obj.lead.document_drawer).data

    @extend_schema_field(field=AdminClientDetailSerializer)
    def get_clients(self, obj):
        return AdminClientDetailSerializer(instance=obj.lead.clients.all(), many=True).data

    @extend_schema_field(field=AdminPropertyDetailSerializer)
    def get_property(self, obj):
        return AdminPropertyDetailSerializer(instance=obj.lead.property).data

    @extend_schema_field(field=AdminUserSerializer)
    def get_agent_applied(self, obj):
        user = User.objects.filter(id=obj.lead.agent_applied).first()
        if not user:
            return None
        return AdminUserSerializer(instance=user).data

    @extend_schema_field(field=AdminUserSerializer)
    def get_co_broke_agent(self, obj):
        if not obj.lead.co_broke_agent:
            return None
        user = User.objects.filter(id=obj.lead.agent_applied).first()
        if not user:
            return None
        return AdminUserSerializer(instance=user).data

    @extend_schema_field(field=BankerResponseSerializer)
    def get_banker(self, obj):
        return BankerResponseSerializer(instance=obj.banker).data

    class Meta:
        model = Payout
        exclude = ['created_by', 'updated_by', 'created', 'updated']


class AdminPayoutUpdateSerializer(CommonModelSerializer):
    class Meta:
        model = PayoutDetail
        fields = ['banks_payout_date', 'iqrate_clearance_fee', 'iqrate_payout_date', 'broker_commission',
                  'broker_payout_date', 'co_broker_commission', 'co_broker_payout_date']


class AdminPayoutReviewSerializer(CommonModelSerializer):
    remarks = serializers.ChoiceField(
        choices=KVStore.get_choices("payout_remarks"),
        required=False,
        help_text="From KVStore - 'payout_remarks'"
    )

    class Meta:
        model = Payout
        fields = ['status', 'remarks', 'other_remarks']


class AdminPayoutCSVExportSerializer(serializers.Serializer):
    payout_ids = serializers.ListField(
        child=serializers.IntegerField(),
        allow_null=True,
        allow_empty=True
    )
