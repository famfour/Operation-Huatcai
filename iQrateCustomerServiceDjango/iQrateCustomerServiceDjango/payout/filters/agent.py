import django_filters
from rest_framework.exceptions import PermissionDenied

from bank.models import Bank


class LawFirmListFilter(django_filters.FilterSet):
    bank = django_filters.NumberFilter(
        method="bank_filter",
        distinct=True
    )

    def bank_filter(self, queryset, name, value):
        try:
            bank = Bank.objects.get(pk=value)
        except:
            raise PermissionDenied(detail="Invalid bank ID")
        return queryset.filter(products__banks=bank)
