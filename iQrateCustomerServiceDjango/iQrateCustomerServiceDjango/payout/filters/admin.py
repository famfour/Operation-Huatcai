import django_filters
from django.db.models import Q

from user.models import User
from kvstore.choices import PayoutStatusChoices


class AdminPayoutFilter(django_filters.FilterSet):
    """
    Filter for admin payout list screen
    """
    lead_id = django_filters.NumberFilter(field_name="lead_id")
    lead_name = django_filters.CharFilter(method='lead_name_filter', distinct=True)
    broker_name = django_filters.CharFilter(method='broker_name_filter', distinct=True)
    co_broker_name = django_filters.CharFilter(method='co_broker_name_filter', distinct=True)
    bank = django_filters.CharFilter(field_name="bank__name", lookup_expr="icontains")
    postal_code = django_filters.CharFilter(field_name="lead__property__postal_code", lookup_expr="exact")

    loan_amount_from = django_filters.NumberFilter(lookup_expr='gte', field_name='loan_amount')
    loan_amount_to = django_filters.NumberFilter(lookup_expr='lte', field_name='loan_amount')

    gross_commission_from = django_filters.NumberFilter(lookup_expr="gte", field_name="gross_commission")
    gross_commission_to = django_filters.NumberFilter(lookup_expr="lte", field_name="gross_commission")

    status = django_filters.MultipleChoiceFilter(lookup_expr='icontains', choices=PayoutStatusChoices.choices, distinct=True)

    def lead_name_filter(self, queryset, name, value):
        """
        The client has to be the main applicant and the name should contain the value.
        """
        return queryset.filter(Q(lead__clients__main_applicant=True) & Q(lead__clients__name__icontains=value))

    def broker_name_filter(self, queryset, name, value):
        """
        This method has to be hacky as there are no foreign key relationship between the user table.
        """
        users = User.objects.filter(full_name__icontains=value).values_list("id", flat=True).all()
        return queryset.filter(lead__agent_applied__in=list(users))

    def co_broker_name_filter(self, queryset, name, value):
        """
        Same as the broker name filter
        """
        users = User.objects.filter(full_name__icontains=value).values_list("id", flat=True).all()
        return queryset.filter(lead__co_broke_agent__in=list(users))
