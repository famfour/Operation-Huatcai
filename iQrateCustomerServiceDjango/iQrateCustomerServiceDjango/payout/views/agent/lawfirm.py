from typing import List, Union

from django.db.models import Prefetch, Q
from django.template.loader import render_to_string
from drf_spectacular.utils import extend_schema, OpenApiParameter
from rest_framework.exceptions import PermissionDenied, NotFound
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from django_filters.rest_framework import DjangoFilterBackend

from bank.models import Bank
from common.serializers import SuccessDetailSerializer
from kvstore.choices import LeadStatusChoices
from lawfirm.models import LawFirm, Product
from lead.models import Lead, Client
from payout.serializers.agent import PayoutLawfirmEmailResponseSerializer, \
    PayoutLawfirmEmailRequestSerializer, PayoutLawFirmListResponseSerializer
from submission.models import Email, EmailType, EmailAttachment
from user.models import User
from payout.filters.agent import LawFirmListFilter


@extend_schema(tags=["Agent - Payout"], summary="Law Firms for DropDown", responses=PayoutLawFirmListResponseSerializer)
class LawFirmListView(ListAPIView):
    pagination_class = None
    serializer_class = PayoutLawFirmListResponseSerializer
    queryset = LawFirm.objects.none()

    filterset_class = LawFirmListFilter
    filter_backends = [DjangoFilterBackend]

    def get(self, request, *args, **kwargs):
        # This was added to accommodate the own law firm requirement.
        resp_data = []
        own_law_firm = [dict(id=0, name="Own Law Firm", email="", legal_fee=0)]
        law_firms = self.get_queryset()

        for law_firm in law_firms:
            if not law_firm.products.count():
                continue
            product = law_firm.products.first()
            resp_data.append(dict(
                id=law_firm.id,
                name=law_firm.name,
                email=law_firm.email,
                legal_fee=product.legal_fee
            ))

        resp_data.extend(own_law_firm)
        return Response(data=resp_data)

    def get_queryset(self):
        # Get the lead or fail.
        try:
            lead = Lead.objects.with_owner(user=self.request.user).get(pk=self.kwargs.get("lead_id"))
        except Lead.DoesNotExist:
            raise NotFound()

        # make sure there is a property for the lead.
        if not getattr(lead, 'property', None):
            raise PermissionDenied(detail="Please complete bank submission to proceed")

        prefetch_query = Product.objects.filter(
            Q(loan_category__contains=[lead.loan_category]) &
            Q(property_types__contains=[lead.property.property_type]) &
            Q(loan_range_from__lte=lead.loan_amount) &
            Q(loan_range_to__gte=lead.loan_amount)
        )
        # if there is a payout for the lead. use the bank from payout to filter.
        payout = getattr(lead, "payout", None)
        if payout:
            prefetch_query = prefetch_query.filter(banks=payout.bank)

        # If the is bank is url args use that as well, as filter class is not called in this view.
        bank_id = self.request.GET.get("bank", None)
        if bank_id:
            try:
                bank = Bank.objects.get(pk=int(bank_id))
                prefetch_query = prefetch_query.filter(banks=bank)
            except:
                raise PermissionDenied(detail="Invalid bank ID")

        return LawFirm.objects.prefetch_related(
            Prefetch("products", queryset=prefetch_query)
        ).order_by("priority").all()


class EmailToLawFirmView(APIView):
    """
    Send email to lawfirm.
    """

    def get_object(self, lead_id):
        try:
            lead = Lead.objects.with_owner(
                user=self.request.user
            ).prefetch_related(
                Prefetch("clients", queryset=Client.objects.filter(main_applicant=True).all())
            ).get(pk=lead_id)
        except Lead.DoesNotExist:
            raise NotFound()
        if not lead.lead_status == LeadStatusChoices.won:
            raise PermissionDenied(detail="Please submit for payout before emailing the Law Firm")
        return lead

    def _get_law_firm_details(self) -> [str, str, Union[str, None]]:
        # Get the LawFirm Details.
        law_firm_id = int(self.request.GET.get('law_firm_id', 0))
        law_firm_name = self.request.GET.get('own_law_firm_name', "")
        legal_fee = float(self.request.GET.get('legal_fee', 0))
        law_firm_email = ''

        if not law_firm_id and not law_firm_name:
            raise PermissionDenied(detail="Please select a law firm")

        # Get the law firm from database
        if not law_firm_id == 0:
            try:
                law_firm = LawFirm.objects.get(pk=law_firm_id)
                law_firm_name = law_firm.name
                law_firm_email = law_firm.email
            except LawFirm.DoesNotExist:
                raise NotFound(detail="The Law Firm does not exist.")
        return [law_firm_name, legal_fee, law_firm_email]

    @extend_schema(
        tags=["Agent - Payout"], summary="Law Firm Email Content",
        responses=PayoutLawfirmEmailResponseSerializer,
        parameters=[
            OpenApiParameter(name='law_firm_id', location=OpenApiParameter.QUERY,
                             description='LawFirm ID, 0 if own law firm.  \n List of lawfirms for the lead can be obtained from the below url  \n'
                                         '*GET* `/customer/payout/lawfirms/{lead_id}`',
                             required=True, type=int),
            OpenApiParameter(name='own_law_firm_name', location=OpenApiParameter.QUERY,
                             description='LawFirm Name, If Own LawFirm is selected', required=False, type=str),
            OpenApiParameter(name='legal_fee', location=OpenApiParameter.QUERY,
                             description='Legal Fee', required=True, type=float, allow_blank=True),
        ])
    def get(self, request, lead_id, *args, **kwargs):
        law_firm_name, legal_fee, law_firm_email = self._get_law_firm_details()
        # Get the lead.
        lead = self.get_object(lead_id)
        # Get the main applicant
        client = lead.clients.first()

        context = dict(
            agent=User.objects.get(id=self.request.user.id),
            client=client,
            payout=lead.payout,
            law_firm_name=law_firm_name,
            legal_fee=legal_fee
        )
        serializer = PayoutLawfirmEmailResponseSerializer(instance=dict(
            content=render_to_string("payout/emails/law_firm.html", context=context),
            subject=f"IQrate-Home Loan Application - {client.obfuscated_name}",
            email=law_firm_email
        ))
        return Response(data=serializer.data)

    @extend_schema(tags=["Agent - Payout"], summary="Send Email to LawFirm", request=PayoutLawfirmEmailRequestSerializer)
    def post(self, request, lead_id):
        lead = self.get_object(lead_id)
        serializer = PayoutLawfirmEmailRequestSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        # get the posted data
        law_firm_id = serializer.validated_data.get('law_firm_id')
        if law_firm_id:
            law_firm = LawFirm.objects.get(pk=law_firm_id)
            to_email = law_firm.email
            name = law_firm.name
        else:
            to_email = serializer.validated_data.get('own_law_firm_email')
            name = serializer.validated_data.get('own_law_firm_name')
        client = lead.clients.first()

        # Create the email record.
        context = dict(
            agent=User.objects.get(id=self.request.user.id),
            client=client,
            remarks=serializer.validated_data.get('remarks', ""),
            law_firm_name=name,
            legal_fee=serializer.validated_data.get('legal_fee', 0)
        )
        email = Email.objects.create(
            lead=lead,
            email_type=EmailType.law_firm,
            name=name,
            to_email=to_email,
            subject=f"IQrate-Home Loan Application - {client.obfuscated_name}",
            content=render_to_string("payout/emails/law_firm.html", context=context)
        )
        # Save the attachments.
        for doc in serializer.validated_data.get('document_ids'):
            EmailAttachment.objects.create(
                email=email,
                name=doc.filename,
                file_key=doc.file_key
            )

        # Send the email
        email.send_email()
        serializer = SuccessDetailSerializer(instance=dict(
            detail=f"Email sent to {name}"
        ))
        return Response(serializer.data)
