from django.db.models import Prefetch
from drf_spectacular.utils import extend_schema
from rest_framework.exceptions import PermissionDenied, NotFound
from rest_framework.generics import CreateAPIView, RetrieveAPIView, UpdateAPIView

from lead.models import Lead
from magic.models import Document
from magic.models import DocumentTypes
from payout.models import Payout
from payout.serializers.agent import AgentPayoutCreateSerializer, AgentPayoutDetailSerializer


@extend_schema(tags=["Agent - Payout"], summary="Create Payout")
class AgentPayoutCreateView(CreateAPIView):
    """
    Endpoints for obtaining data for the below endpoint.  \n
    1. **bank** -  ```/customer/bank/banks```  \n
    2. **banker** - ```/customer/bank/bankers``` - Filter using the selected bank_id.  \n
    3. **reference_rate** - ```/customer/bank/rates``` - Filter using the selected bank_id.  \n
    4. **law_firm** - ```/customer/payout/lawfirms/{lead_id}```.  \n
    **Rates should be in the following format**  \n
    ```
    [
        {
            "year": "year_1",
            "bank_spread": 0,
            "reference_rate": 0
        },
        {
            "year": "year_2",
            "bank_spread": 0,
            "reference_rate": 0
        },
        {
            "year": "year_3",
            "bank_spread": 0,
            "reference_rate": 0
        },
        {
            "year": "year_4",
            "bank_spread": 0,
            "reference_rate": 0
        },
        {
            "year": "year_5",
            "bank_spread": 0,
            "reference_rate": 0
        },
        {
            "year": "thereafter",
            "bank_spread": 0,
            "reference_rate": 0
        }
    ]
    ```
    """
    serializer_class = AgentPayoutCreateSerializer


@extend_schema(tags=["Agent - Payout"], summary="Payout Details")
class AgentPayoutDetailView(RetrieveAPIView):
    serializer_class = AgentPayoutDetailSerializer

    def get_object(self):
        payout = self.get_queryset().first()
        if not payout:
            raise NotFound(detail="Payout is not created for this lead")
        return payout

    def get_queryset(self):
        lead_id = self.kwargs.get("lead_id")

        # Make sure the agent has access to the lead.
        lead = Lead.objects.filter(pk=lead_id).with_owner(self.request.user).first()
        if not lead:
            raise PermissionDenied(detail="No Access")

        return Payout.objects.filter(lead=lead).prefetch_related(
            Prefetch(
                "lead__document_drawer__documents",
                queryset=Document.objects.filter(doc_type=DocumentTypes.LETTER_OF_OFFER)
            ),
            'lead', 'loans', 'loans__rates', 'loans__rates__reference_rate'
        )


@extend_schema(tags=["Agent - Payout"], summary="Update Payout")
class AgentPayoutUpdateView(UpdateAPIView):
    """
    Endpoints for obtaining data for the below endpoint.  \n
    1. **bank** -  ```/customer/bank/banks```  \n
    2. **banker** - ```/customer/bank/bankers``` - Filter using the selected bank_id.  \n
    3. **reference_rate** - ```/customer/bank/rates``` - Filter using the selected bank_id.  \n
    4. **law_firm** - ```/customer/payout/lawfirms/{lead_id}```.  \n
    **Rates should be in the following format**  \n
    ```
    [
        {
            "year": "year_1",
            "bank_spread": 0,
            "reference_rate": 0
        },
        {
            "year": "year_2",
            "bank_spread": 0,
            "reference_rate": 0
        },
        {
            "year": "year_3",
            "bank_spread": 0,
            "reference_rate": 0
        },
        {
            "year": "year_4",
            "bank_spread": 0,
            "reference_rate": 0
        },
        {
            "year": "year_5",
            "bank_spread": 0,
            "reference_rate": 0
        },
        {
            "year": "thereafter",
            "bank_spread": 0,
            "reference_rate": 0
        }
    ]
    ```
    """
    serializer_class = AgentPayoutCreateSerializer

    def get_object(self):
        return self.get_queryset().first()

    def get_queryset(self):
        lead_id = self.kwargs.get("lead_id")

        # Make sure the agent has access to the lead.
        lead = Lead.objects.filter(pk=lead_id).with_owner(self.request.user).first()
        if not lead:
            raise PermissionDenied(detail="No Access")

        return Payout.objects.filter(lead=lead).prefetch_related(
            Prefetch(
                "lead__document_drawer__documents",
                queryset=Document.objects.filter(doc_type=DocumentTypes.LETTER_OF_OFFER),
                to_attr="lo_docs"
            ),
            'lead', 'loans', 'loans__rates', 'loans__rates__reference_rate'
        )
