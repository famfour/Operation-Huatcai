import django_filters
from django.db.models import Prefetch, When, Case, Value, IntegerField, Q
from drf_spectacular.utils import extend_schema
from rest_framework.exceptions import NotFound
from rest_framework.generics import ListAPIView, RetrieveAPIView, UpdateAPIView, GenericAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from bank.choices import PackageYearChoices
from lead.models import Client
from payout.filters.admin import AdminPayoutFilter
from payout.models import Payout, PayoutDetail, Loan, LoanRate
from payout.serializers.admin import AdminPayoutListSerializer, AdminPayoutPayoutDetailSerializer, \
    AdminPayoutReviewSerializer, AdminPayoutUpdateSerializer, AdminPayoutCSVExportSerializer
from common.serializers import FileResponseSerializer
from payout.utils.export_csv import export_payouts_csv


@extend_schema(tags=["Admin - Payout"], summary="List Payouts", responses=AdminPayoutListSerializer)
class AdminPayoutListView(ListAPIView):
    """
    List of all the submitted payout requests.
    """
    serializer_class = AdminPayoutListSerializer
    filterset_class = AdminPayoutFilter
    queryset = Payout.objects.none()
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]

    def get_queryset(self):
        """
        Ordering should follow the leads order.
        """
        return Payout.objects.prefetch_related(
            Prefetch("lead__clients", queryset=Client.objects.filter(main_applicant=True)),
            "bank", "banker", "lead"
        )


@extend_schema(tags=["Admin - Payout"], summary="Payout Details", responses=AdminPayoutPayoutDetailSerializer)
class AdminPayoutDetailView(RetrieveAPIView):
    """
    Details of the payout along with update for status and remarks.
    including the
    """
    serializer_class = AdminPayoutPayoutDetailSerializer
    queryset = Payout.objects.all()

    def get_queryset(self):
        qs = super(AdminPayoutDetailView, self).get_queryset()
        return qs.prefetch_related(
            "lead", "lead__clients", "lead__property", "bank", "banker", "lead__document_drawer", "lead__document_drawer__documents"
        ).all()


@extend_schema(tags=["Admin - Payout"], summary="Payout Review Update", request=AdminPayoutReviewSerializer)
class AdminPayoutReviewUpdateAPIView(UpdateAPIView):
    serializer_class = AdminPayoutReviewSerializer

    def get_object(self):
        try:
            return Payout.objects.get(pk=self.kwargs.get("pk"))
        except:
            raise NotFound()


@extend_schema(tags=["Admin - Payout"], summary="Payout Details Update", request=AdminPayoutUpdateSerializer)
class AdminPayoutDetailUpdateAPIView(UpdateAPIView):
    serializer_class = AdminPayoutUpdateSerializer

    def get_object(self):
        try:
            return PayoutDetail.objects.get(payout_id=self.kwargs.get("pk"))
        except:
            raise NotFound()


class AdminPayoutExportAPIView(APIView):

    @extend_schema(tags=["Admin - Payout"], summary="Export Payout Details to CSV",
                   request=AdminPayoutCSVExportSerializer, responses=FileResponseSerializer)
    def post(self, request):
        # validate the data
        serializer = AdminPayoutCSVExportSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        loan_rate_qs = LoanRate.objects.annotate(
            order=Case(
                When(Q(year=PackageYearChoices.year_1), Value(1)),
                When(Q(year=PackageYearChoices.year_2), Value(2)),
                When(Q(year=PackageYearChoices.year_3), Value(3)),
                When(Q(year=PackageYearChoices.year_4), Value(4)),
                When(Q(year=PackageYearChoices.year_5), Value(5)),
                When(Q(year=PackageYearChoices.thereafter), Value(6)),
                output_field=IntegerField()
            )
        ).order_by('order')
        queryset = Payout.objects.prefetch_related(
            'lead',
            Prefetch("lead__clients", queryset=Client.objects.order_by('-main_applicant').all()),
            'loans',
            Prefetch(
                "loans__rates",
                queryset=loan_rate_qs
            ),
            'loans__rates__reference_rate',
            'bank',
            'banker',
            'law_firm',
            'details'
        )
        if serializer.validated_data.get('payout_ids', None):
            queryset = queryset.filter(pk__in=serializer.validated_data['payout_ids'])

        url = export_payouts_csv(queryset.all())
        resp_serializer = FileResponseSerializer(instance=dict(
            url=url
        ))
        return Response(data=resp_serializer.data)
