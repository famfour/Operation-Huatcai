from submission.models import LeadBanker
from lawfirm.models import LawFirm


class OwnLawFirm():
    name: str = ""
    email: str = ""


class LawFirmSubmission():

    def __init__(self, lead, law_firm=None):
        self.lead = lead
        self.law_firm = law_firm

    def get_email_subject(self):
        pass

    def get_email_content(self):
        pass
