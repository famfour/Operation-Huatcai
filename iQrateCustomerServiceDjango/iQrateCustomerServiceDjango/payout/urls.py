from django.urls import path
from .views import agent
from .views import admin

app_name = "payout"

agent_patterns = [
    path('create', agent.AgentPayoutCreateView.as_view(), name="payout_create"),
    path('details/<int:lead_id>', agent.AgentPayoutDetailView.as_view(), name="payout_details"),
    path('update/<int:lead_id>', agent.AgentPayoutUpdateView.as_view(), name="payout_update"),
    path('lawfirms/<int:lead_id>', agent.LawFirmListView.as_view(), name="payout_update"),
    path('email-lawfirm/<int:lead_id>', agent.EmailToLawFirmView.as_view(), name="email_lawfirm")
]

admin_patterns = [
    path('all', admin.AdminPayoutListView.as_view(), name="all_payouts"),
    path('export', admin.AdminPayoutExportAPIView.as_view(), name="export_payouts"),
    path('details/<int:pk>', admin.AdminPayoutDetailView.as_view(), name="payout_detail"),
    path('review/<int:pk>', admin.AdminPayoutReviewUpdateAPIView.as_view(), name="payout_review"),
    path('update/<int:pk>', admin.AdminPayoutDetailUpdateAPIView.as_view(), name="update_payout"),
    # path('payout/<int:pk>', admin.AdminPayoutDetailView.as_view(), name="detail_export")
]
