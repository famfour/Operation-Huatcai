from rest_framework.serializers import ValidationError

from lead.models import Lead
from kvstore.choices import LeadStatusChoices


class ValidateBanker():
    """
    Make sure the selected banker is from the selected bank.
    """
    requires_context = True

    def __call__(self, attrs, serializer, *args, **kwargs):
        bank = attrs['bank']
        banker = attrs['banker']
        if not banker.bank == bank:
            raise ValidationError(detail=dict(banker="The banker must be from the selected bank"))
        return attrs


class ValidatePayoutOwner():
    """
    Make sure that the payout belongs to the logged in user.
    """
    requires_context = True

    def __call__(self, value, serializer_field, *args, **kwargs):
        user = serializer_field.context['request'].user
        if not Lead.objects.filter(pk=value.lead_id).with_owner(user).exists():
            raise ValidationError(detail="Wrong payout")
        return value


class ValidateLawFirmName():
    """
    Make sure either a lawfirm from the database or own lawfirm is entered.
    """
    requires_context = True

    def __call__(self, attrs, serializer, *args, **kwargs):
        if not attrs['law_firm_id'] and not attrs['own_law_firm_name'].strip():
            raise ValidationError(dict(
                law_firm="Please choose a Law Firm or enter the name of your Law Firm"
            ))
        return attrs


class ValidateLeadStatus():
    """
    Makec sure the lead status is not pending.
    It has to be submitted to bank first.
    """
    requires_context = True

    def __call__(self, value, serializer_field, *args, **kwargs):
        if value.lead_status == LeadStatusChoices.pending:
            raise ValidationError(detail="This lead has not been submitted to the bank.")
        return value
