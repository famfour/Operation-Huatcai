import uuid
from datetime import datetime
import importlib

from django.conf import settings
from django.contrib.postgres.fields import ArrayField
from django.db import models
from common.models import ModelMixin
from bank.queryset import PackageQuerySet
from kvstore.choices import RateTypeChoices, RateCategoryChoices
from common.s3client import S3Client
from .choices import PackageYearChoices
from common.validators import format_float


class SubmissionMethod(models.Model):
    """
    Registered submission methods.
    """
    name = models.CharField(max_length=64)
    identifier = models.CharField(max_length=64, unique=True)
    classpath = models.CharField(max_length=254)
    details = models.TextField(null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def get_class(self):
        """
        Get the method class for this particular method.
        """
        parts = self.classpath.split(".")
        class_name = parts.pop()
        module_path = ".".join(parts)
        module = importlib.import_module(module_path)
        method = getattr(module, class_name)
        return method()


class Bank(ModelMixin):
    """
    Banks model
    """
    name = models.CharField(max_length=254)
    name_masked = models.CharField(max_length=64)
    logo = models.URLField(null=True, blank=True)
    status = models.BooleanField(default=False)
    workflow = models.ForeignKey(SubmissionMethod, on_delete=models.PROTECT, null=True)
    encrypt_attachments = models.BooleanField(default=True)

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return self.name


class BankForm(ModelMixin):
    id = models.UUIDField(default=uuid.uuid4, primary_key=True, editable=False, unique=True, db_index=True)
    bank = models.ForeignKey(Bank, related_name="forms", on_delete=models.CASCADE, null=True)
    name = models.CharField(
        max_length=254,
        help_text="The name of the form")
    extension = models.CharField(max_length=16, default="pdf", help_text="Only PDF forms are allowed")
    file_key = models.CharField(max_length=254, help_text="Relative path of the object in s3 bucket")

    def __str__(self):
        return self.name

    @property
    def url(self):
        client = S3Client(bucket_name=settings.AWS_STORAGE_BUCKET_NAME)
        return client.get_direct_url(self.file_key)


class Banker(ModelMixin):
    """
    Banker model.
    Bankers review the loan applications sent to them.
    """
    bank = models.ForeignKey(Bank, on_delete=models.PROTECT, related_name="bankers")
    name = models.CharField(max_length=254)
    country_code = models.SmallIntegerField(null=True, help_text="Country Code for Phone Number")
    phone_number = models.PositiveBigIntegerField(null=True, help_text="Phone Number")
    email = models.EmailField()
    referral_link = models.URLField(null=True, blank=True)
    remarks = models.TextField(null=True, blank=True)
    status = models.BooleanField(default=False)
    last_assignment = models.DateTimeField(
        null=True,
        blank=True,
        help_text="The last time a lead was assigned to this banker. To be used for the round robin.")

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return self.name


class BlockDate(ModelMixin):
    """
    Block dates are the dates a particular banker is unavailable for business.  \n
    Think of it as the days in which a banker is on leave.  \n
    So no emails should be sent to them during these days.
    """
    block_from = models.DateField()
    block_till = models.DateField()
    banker = models.ForeignKey(Banker, on_delete=models.CASCADE, related_name="blocked_dates")

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return str(f"From {self.block_from} to {self.block_till}")


class Rate(ModelMixin):
    """
    The loan rates  \n
    These rates are only used as reference rates for the package rates.
    """
    banks = models.ManyToManyField(Bank, related_name="rates", help_text="Array of bank ids")
    rate_type = models.CharField(max_length=64, help_text="Populated from KVStore", null=True)
    reference = models.CharField(max_length=64, help_text="Name of rate")
    interest_rate = models.FloatField(help_text="Interest rate")
    equation = models.CharField(max_length=16, help_text="The equation for calculating the package rate")
    remarks = models.TextField(null=True, blank=True, help_text='Remarks if any')

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return str(f"{self.pk}")


class Package(ModelMixin):
    bank = models.ForeignKey(Bank, related_name="packages", on_delete=models.PROTECT)
    mask = models.BooleanField(default=False)  # If True, return the masked name of the bank
    rate_category = models.CharField(max_length=64, help_text="rate_category",
                                     choices=RateCategoryChoices.choices)  # From KVstore - Standard/Exclusive
    rate_type = models.CharField(max_length=64, help_text="Populated from KVStore", null=True, choices=RateTypeChoices.choices)
    property_types = ArrayField(base_field=models.CharField(max_length=128, blank=True), default=list, blank=True)
    property_status = ArrayField(base_field=models.CharField(max_length=128, blank=True), default=list, blank=True)
    loan_category = ArrayField(
        base_field=models.CharField(max_length=128, blank=True),
        default=list,
        blank=True,
        help_text="In design its named as loan_type"
    )
    lock_in_period = models.PositiveSmallIntegerField(default=0, help_text="From KVStore - lock_in_period_years")
    min_loan_amount = models.FloatField()
    deposit_to_place = models.FloatField(default=0)
    valuation_subsidy = ArrayField(
        base_field=models.CharField(max_length=128, blank=True),
        default=list,
        blank=True
    )
    fire_insurance_subsidy = models.CharField(max_length=254, null=True)
    cash_rebate_legal_subsidy = ArrayField(
        base_field=models.CharField(max_length=128, blank=True),
        max_length=254,
        default=list,
        help_text="Get the values from kvstore value - cash_rebate_legal_subsidy_remarks"
    )
    cash_rebate_subsidy_clawback_period_years = models.PositiveSmallIntegerField(default=0)
    partial_repayment_penalty = models.FloatField(default=0)
    partial_repayment_penalty_remarks = models.CharField(max_length=254, null=True)
    full_repayment_penalty = models.FloatField(default=0)
    full_repayment_penalty_remarks = models.CharField(max_length=254, null=True)
    cancellation_fee = models.CharField(max_length=254, null=True)
    cancellation_fee_remarks = models.CharField(null=True, help_text="from kvstore key cancellation_fee_remarks", max_length=254)
    deposit_to_place_remarks = models.CharField(max_length=254, null=True)
    remarks_for_client = ArrayField(
        base_field=models.CharField(max_length=128, blank=True),
        default=list,
        blank=True,
        help_text="From KVStore - remarks_for_client"
    )
    interest_offsetting = models.CharField(max_length=254, null=True)

    interest_reset_date = models.CharField(
        max_length=254,
        null=True,
        help_text="Values from Kvstore key - 'interest_reset_date'"
    )
    processing_fee = models.CharField(
        max_length=254,
        null=True,
        help_text="Values from Kvstore key - 'processing_fee'"
    )
    remarks_for_broker = models.TextField(null=True, blank=True)
    new_purchase_referral_fee = models.FloatField(default=0)
    refinance_referral_fee = models.FloatField(default=0)
    publish = models.BooleanField(default=False)

    # Custom manager for the model
    objects = PackageQuerySet.as_manager()

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return str(self.id)

    def data(self):
        data = dict()
        exclude_fields = ["id", "created", "created_by", "updated_by"]
        fields = [f.name for f in self._meta.fields if not f.name in exclude_fields]
        for field in fields:
            value = getattr(self, field)
            # Setup the bank logos
            if type(value) == Bank:
                if self.mask:
                    data[field] = settings.IQRATE_LOGO
                else:
                    data[field] = self.bank.logo
                continue
            if type(value) == datetime:
                data[field] = value.strftime("%-d %B %Y")
                continue
            data[field] = value
        # Add the rates to the data as well.
        for rate in self.rates.all():
            data[rate.year] = dict(
                rate_type=rate.reference_rate.rate_type,
                equation=rate.reference_rate.equation,
                reference=rate.reference_rate.reference,
                interest_rate=rate.reference_rate.interest_rate,
                bank_spread=rate.bank_spread,
                total_interest_rate=rate.total_interest_rate
            )
        return data


class PackageRate(ModelMixin):
    """
    The rates assigned to individual packages.
    """
    package = models.ForeignKey(Package, on_delete=models.PROTECT, related_name="rates")
    year = models.CharField(max_length=32, choices=PackageYearChoices.choices)
    reference_rate = models.ForeignKey(Rate, on_delete=models.PROTECT, related_name="package_rates")
    bank_spread = models.FloatField(default=0)
    total_interest_rate = models.FloatField(
        default=0,
        help_text="Total interest rate calculated from reference rate's interest rate and bank spread")

    def recalculate_total_rate(self, save=True):
        """
        Calculate total interest rate based on
        1. Interest Rate of 'reference_rate'
        2. Equation of 'reference_rate'
        3. 'bank_spread'
        """
        self.total_interest_rate = self.reference_rate.interest_rate
        if self.reference_rate.equation == "+":
            self.total_interest_rate = self.reference_rate.interest_rate + self.bank_spread
        if self.reference_rate.equation == "-":
            self.total_interest_rate = self.reference_rate.interest_rate - self.bank_spread
        self.total_interest_rate = round(self.total_interest_rate, 2)
        if save:
            self.save()

    class Meta:
        ordering = ['-created']
        unique_together = ["package", "year"]

    def __str__(self):
        return f"{self.pk}"
