from django.db import models


class PackageYearChoices(models.TextChoices):
    year_1 = ("year_1", "Year 1")
    year_2 = ("year_2", "Year 2")
    year_3 = ("year_3", "Year 3")
    year_4 = ("year_4", "Year 4")
    year_5 = ("year_5", "Year 5")
    thereafter = ("thereafter", "ThereAfter")
