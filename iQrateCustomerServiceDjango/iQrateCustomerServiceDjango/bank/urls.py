from django.urls import path, include
from .views import admin
from .views import agent

app_name = "bank"

admin_patterns = [
    # BlockDates
    path('banker/blockdates', admin.BlockDateListAPIView.as_view(), name="blockdates"),
    path('banker/blockdate/create', admin.BlockDateCreateAPIView.as_view(), name="blockdates_create"),
    path('banker/blockdate/<int:pk>', admin.BlockDateDetailAPIView.as_view(), name="blockdates_rud"),

    # Bankers
    path('bankers', admin.BankerListAPIView.as_view(), name="bankers"),
    path('banker/create', admin.BankerCreateAPIView.as_view(), name="bankers_create"),
    path('banker/<int:pk>', admin.BankerDetailAPIView.as_view(), name="bankers_rud"),

    # Rates
    path('rates', admin.RateListAPIView.as_view(), name="rates"),
    path('rate/create', admin.RateCreateAPIView.as_view(), name="rates_create"),
    path('rate/<int:pk>', admin.RateDetailAPIView.as_view(), name="rates_rud"),

    # Package
    path('packages', admin.PackageListAPIView.as_view(), name="packages"),
    path('packages/create', admin.PackageCreateAPIView.as_view(), name="packages_create"),
    path('packages/export', admin.PackageCSVExportView.as_view(), name="packages_export"),
    path('package/<int:pk>', admin.PackageDetailAPIView.as_view(), name="packages_rud"),

    # Workflows
    path('workflows', admin.WorkFlowListAPIView.as_view(), name="workflows"),

    # Bank forms
    path('bankform/upload', admin.BankFormCreateView.as_view(), name="upload_bank_form"),
    path('bankform/<uuid:pk>', admin.BankFormDestroyView.as_view(), name="delete_bank_form"),

    # Banks
    path('all', admin.BankListAPIView.as_view(), name="banks"),
    path('create', admin.BankCreateAPIView.as_view(), name="banks_create"),
    path('<int:pk>', admin.BankDetailAPIView.as_view(), name="banks_rud"),
]

agent_patterns = [
    path('packages', agent.PackageListAPIView.as_view(), name="packages"),
    path('packages/calculator', agent.RefinanceCalculatorPackageFilterAPIView.as_view(), name="packages_calculator_refinance"),
    path('packages/buc_calculator', agent.BUCCalculatorPackageFilterAPIView.as_view(), name="packages_calculator_buc"),
    path('packages/new_purchase_calculator', agent.NewPurchaseCalculatorApiView.as_view(), name="packages_calculator_new_purchase"),
    path('packages/export-to-pdf', agent.PackageExportView.as_view(), name='export_package'),
    path('banks', agent.BankListAPIView.as_view(), name="banks"),
    path('bankers', agent.BankerListAPIView.as_view(), name="bankers"),
    path('rates', agent.RateListAPIView.as_view(), name="rates"),

    # Remove after dev @todo remove this
    # path('packages/html', agent.PackageHTMLView.as_view(), name='package_html')
]
