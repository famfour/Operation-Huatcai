from django.core.management import BaseCommand
from bank.models import Package, Rate, PackageRate, Bank
import itertools
from kvstore.choices import PropertyTypeChoices, PropertyStatusChoices, LoanCategoryChoices, RateCategoryChoices, RateTypeChoices
import random


class Command(BaseCommand):
    help = "Create mock packages with all options"

    def handle(self, *args, **options):
        """
        Create all possible combinations of the packages for testing.
        1. All values are added for fields with multiple values.
        """

        property_types = [
            PropertyTypeChoices.hdb,
            PropertyTypeChoices.condominium_apartment,
            PropertyTypeChoices.strata_housing_townhouses,
            PropertyTypeChoices.ec_resale_out_of_mop,
            PropertyTypeChoices.ec_from_developer_within_mop,
            PropertyTypeChoices.landed,
        ]  # All for all
        property_status = [
            PropertyStatusChoices.completed,
            PropertyStatusChoices.under_construction_to_obtain_top_within_2_years,
            PropertyStatusChoices.under_construction_to_obtain_top_more_than_2_years
        ]  # All for all
        loan_category = [
            LoanCategoryChoices.new_purchase_with_option_to_purchase,
            LoanCategoryChoices.new_purchase_approval_in_principle,
            LoanCategoryChoices.refinance_cash_out_term_loan,
            LoanCategoryChoices.part_purchase_decoupling_divorce
        ]  # All for all

        # Create more rates in case.
        rate_types = ['fixed', 'sora', 'board', 'fixed_deposit', 'sibor']
        banks = Bank.objects.all()
        rates = []
        for rate_type in rate_types:
            for id in range(1, 10):
                rate = Rate.objects.create(
                    rate_type=rate_type,
                    reference=f"{rate_type.upper()}-{random.randint(1, 1000000)}",
                    interest_rate=random.randint(200, 1000) / 100,
                    equation=random.choice(["+", "-", "="]),
                    remarks=None
                )
                rate.banks.set(banks)
                rates.append(rate)

        for count in range(1, 200):
            rate = random.choice(rates)
            package = Package.objects.create(
                bank=rate.banks.order_by("?").first(),
                mask=False,
                rate_category=random.choice([RateCategoryChoices.standard, RateCategoryChoices.exclusive]),
                rate_type=rate.rate_type,
                property_types=property_types,
                property_status=property_status,
                loan_category=loan_category,
                lock_in_period=0,
                min_loan_amount=random.choice(list(range(100000, 10000000, 500000))),
                deposit_to_place=random.choice([0, 200000]),
                valuation_subsidy=[
                    "NA"
                ],
                fire_insurance_subsidy="NA",
                cash_rebate_legal_subsidy=[
                    "NA"
                ],
                cash_rebate_subsidy_clawback_period_years=3,
                partial_repayment_penalty=1.5,
                partial_repayment_penalty_remarks="NA",
                full_repayment_penalty=1.5,
                full_repayment_penalty_remarks="NA",
                cancellation_fee="0.75%",
                deposit_to_place_remarks="NA",
                remarks_for_client=[
                    "NA"
                ],
                interest_offsetting="NA",
                interest_reset_date=False,
                processing_fee=False,
                remarks_for_broker=None,
                new_purchase_referral_fee=0.15,
                refinance_referral_fee=0.15,
                publish=True

            )

            for year in ['year_1', 'year_2', 'year_3', 'year_4', 'year_5', 'thereafter']:
                PackageRate.objects.create(
                    package=package,
                    year=year,
                    reference_rate=rate,
                    bank_spread=1.5
                )
