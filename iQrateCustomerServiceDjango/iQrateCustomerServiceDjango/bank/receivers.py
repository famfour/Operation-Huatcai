from datetime import timedelta
from django.utils import timezone
from django.conf import settings
from fieldsignals import pre_save_changed, post_save_changed

from django.db.models.signals import post_save, post_delete, pre_save
from django.dispatch import receiver
from bank.models import Banker, BankForm, Rate, PackageRate
from common.s3client import S3Client


@receiver(post_save, sender=Banker)
def post_save_create_heatmap(sender, instance, created, **kwargs):
    """
    Add an earlier time to the last_assignment value so that the banker lookup \n
    at adding lead package won't fail.
    """
    if not created:
        return
    instance.last_assigned = timezone.localtime() - timedelta(days=3650)
    instance.save()


@receiver(post_delete, sender=BankForm)
def delete_s3_file(sender, instance, **kwargs):
    """
    Delete the file from bucket when the database entry is deleted.
    """
    client = S3Client(settings.AWS_STORAGE_BUCKET_NAME)
    client.delete_object(instance.file_key)


def update_package_rate(sender, instance, **kwargs):
    """
    Update package rate when a reference rate is updated.
    """
    for rates in instance.package_rates.all():
        rates.recalculate_total_rate()


pre_save_changed.connect(update_package_rate, sender=Rate, fields=['interest_rate', 'equation'])


@receiver(pre_save, sender=PackageRate)
def create_total_interest_rate(sender, instance, **kwargs):
    """
    Recalculate the total rate on every save.
    """
    instance.recalculate_total_rate(save=False)
