# Generated by Django 4.0.3 on 2022-04-11 08:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bank', '0003_rate_bank_alter_rate_equation_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='rate',
            name='bank',
        ),
        migrations.AddField(
            model_name='rate',
            name='banks',
            field=models.ManyToManyField(related_name='rates', to='bank.bank'),
        ),
        migrations.AlterField(
            model_name='package',
            name='bank',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='packages', to='bank.bank'),
        ),
    ]
