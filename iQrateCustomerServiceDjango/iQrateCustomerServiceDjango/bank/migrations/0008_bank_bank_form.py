# Generated by Django 4.0.4 on 2022-04-25 13:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bank', '0007_alter_banker_phone_number'),
    ]

    operations = [
        migrations.AddField(
            model_name='bank',
            name='bank_form',
            field=models.CharField(max_length=254, null=True),
        ),
    ]
