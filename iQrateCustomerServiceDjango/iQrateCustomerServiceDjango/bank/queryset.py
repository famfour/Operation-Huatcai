from django.db.models import QuerySet, Q, Sum, Count
from kvstore.choices import RateTypeChoices, RateCategoryChoices
from bank.choices import PackageYearChoices


class PackageQuerySet(QuerySet):

    def published_only(self):
        return self.filter(publish=True)

    def fixed_only(self):
        return self.filter(rate_type=RateTypeChoices.fixed)

    def floating_only(self):
        return self.exclude(rate_type=RateTypeChoices.fixed)

    def order_by_rate(self):
        """
        Order packages by the 'total_interest_rate' of the package rate in the \n
        descending order of year_1,  year_2 ...... thereafter
        """
        return self.annotate(
            year_1_rate=Sum('rates__total_interest_rate', filter=Q(rates__year=PackageYearChoices.year_1)),
            year_2_rate=Sum('rates__total_interest_rate', filter=Q(rates__year=PackageYearChoices.year_2)),
            year_3_rate=Sum('rates__total_interest_rate', filter=Q(rates__year=PackageYearChoices.year_3)),
            year_4_rate=Sum('rates__total_interest_rate', filter=Q(rates__year=PackageYearChoices.year_4)),
            year_5_rate=Sum('rates__total_interest_rate', filter=Q(rates__year=PackageYearChoices.year_5)),
            thereafter=Sum('rates__total_interest_rate', filter=Q(rates__year=PackageYearChoices.thereafter)),
        ).order_by("year_1_rate", "year_2_rate", "year_3_rate", "year_4_rate", "year_5_rate", "thereafter")

    def common_filter(self, property_type, property_status, loan_amount, loan_category, excluded_bank=None):
        """
        Common filter based on the lead properties
        """
        query = self.filter(
            Q(property_types__contains=[property_type]) &
            Q(property_status__contains=[property_status]) &
            Q(min_loan_amount__lte=loan_amount) &
            Q(loan_category__contains=[loan_category])
        )
        if excluded_bank:
            query = query.exclude(bank=excluded_bank)
        return query

    def has_200_k(self):
        return self.filter(deposit_to_place__range=[1, 200000])

    def plan_to_sell_in_3_years(self):
        return self.exclude(full_repayment_penalty_remarks__iexact="NA")

    def no_exclusive(self):
        """
        Exclusive packages are only for premium users
        """
        return self.exclude(rate_category=RateCategoryChoices.exclusive)
