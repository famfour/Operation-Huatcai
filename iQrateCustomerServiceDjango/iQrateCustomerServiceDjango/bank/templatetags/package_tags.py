from django import template
from django.utils.safestring import mark_safe
from kvstore.choices import RateTypeChoices

register = template.Library()


@register.filter
def index(iterable, i):
    return iterable[i]


@register.filter
def get_value(dictionary, key):
    return dictionary[key]


@register.filter
def strip_decimal(number):
    return f"{int(number):,}"


@register.filter
def two_decimal(number):
    return "{:.2f}".format(number)


@register.simple_tag
def display_year_rate(rate):
    if rate['rate_type'] == RateTypeChoices.fixed:
        return mark_safe(f"{rate['reference'].title()} (0.00) {rate['total_interest_rate']}")
    html = f"""
        <div class='block'>{rate['reference']}</div>
        <div class='block'>
            ({two_decimal(rate['interest_rate'])})% + {two_decimal(rate['bank_spread'])}% = {two_decimal(rate['total_interest_rate'])}%
        </div>
    """
    return mark_safe(html)
