from drf_spectacular.utils import extend_schema_field

from bank.models import Bank, Banker, PackageRate, Rate, Package, BankForm
from rest_framework import serializers
from django.conf import settings


class CustomerHyperlinkedBankFormSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = BankForm
        fields = ['name', 'url']


class CustomerBankSerializer(serializers.ModelSerializer):

    def __init__(self, *args, **kwargs):
        self.masked = kwargs.pop("masked", False)
        super().__init__(*args, **kwargs)

    name = serializers.SerializerMethodField()
    logo = serializers.SerializerMethodField()
    forms = CustomerHyperlinkedBankFormSerializer(many=True)

    def get_name(self, obj):
        if self.masked:
            return obj.name_masked
        return obj.name

    def get_logo(self, obj):
        if self.masked:
            return settings.IQRATE_LOGO_URL
        return obj.logo

    class Meta:
        model = Bank
        fields = ['id', 'name', 'name_masked', 'logo', 'forms']
        read_only = fields


class CustomerBankerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Banker
        fields = ["id", "name"]
        read_only = fields


class CustomerRateHyperlinkedModelSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Rate
        fields = ['rate_type', 'reference', 'interest_rate', 'equation', 'remarks']


class CustomerPackageRateSerializer(serializers.ModelSerializer):
    reference_rate = CustomerRateHyperlinkedModelSerializer()

    class Meta:
        model = PackageRate
        fields = ['year', 'reference_rate', 'bank_spread', 'total_interest_rate']


class CustomerPackageDetailSerializer(serializers.ModelSerializer):
    bank = serializers.SerializerMethodField()
    rates = CustomerPackageRateSerializer(many=True)

    @extend_schema_field(field=CustomerBankSerializer)
    def get_bank(self, obj):
        if obj.mask:
            return CustomerBankSerializer(instance=obj.bank, masked=True).data
        return CustomerBankSerializer(instance=obj.bank).data

    class Meta:
        model = Package
        fields = ['id', 'bank', 'mask', 'rate_category', 'rate_type',
                  'property_types', 'property_status', 'loan_category', 'lock_in_period', 'min_loan_amount',
                  'deposit_to_place', 'valuation_subsidy', 'fire_insurance_subsidy', 'cash_rebate_legal_subsidy',
                  'cash_rebate_subsidy_clawback_period_years', 'partial_repayment_penalty',
                  'partial_repayment_penalty_remarks', 'full_repayment_penalty', 'full_repayment_penalty_remarks', 'rates',
                  'cancellation_fee', 'cancellation_fee_remarks', 'deposit_to_place_remarks', 'remarks_for_client', 'interest_offsetting',
                  'interest_reset_date', 'processing_fee', 'remarks_for_broker', 'new_purchase_referral_fee', 'refinance_referral_fee',
                  'created_by', 'updated_by', 'created', 'updated']


class PackageExportRequestSerilizer(serializers.Serializer):
    packages = serializers.ListField(
        child=serializers.PrimaryKeyRelatedField(queryset=Package.objects.filter(publish=True).all()),
        allow_null=False,
        allow_empty=False
    )


class PackageExportResponseSerilizer(serializers.Serializer):
    url = serializers.URLField()


class CalculatorPackageListSerializer(serializers.Serializer):
    fixed = CustomerPackageDetailSerializer()
    floating = CustomerPackageDetailSerializer()


class AgentRateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rate
        fields = ['id', 'rate_type', 'reference', 'interest_rate', 'equation']
