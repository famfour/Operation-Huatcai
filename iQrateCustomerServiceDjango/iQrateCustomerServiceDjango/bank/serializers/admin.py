from uuid import uuid4

from django.conf import settings
from django.core.validators import FileExtensionValidator
from django.db.models import Q
from django.utils import timezone
from rest_framework import serializers, status
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError
from sanitize_filename import sanitize

from bank.models import BlockDate, Banker, Bank, PackageRate, Package, Rate, SubmissionMethod, BankForm
from common.s3client import S3Client
from common.serializers import CommonModelSerializer
from kvstore.models import KVStore


class WorkFlowSerializer(serializers.ModelSerializer):
    class Meta:
        model = SubmissionMethod
        fields = ['id', 'name']


class AdminBankFormCreateSerializer(CommonModelSerializer):
    file = serializers.FileField(
        validators=[FileExtensionValidator(allowed_extensions=["pdf"])],
        help_text="Only PDF forms are allowed"
    )

    def save(self, **kwargs):
        file = self.validated_data['file']
        name = sanitize(self.validated_data['name'])
        ext = sanitize(file.name.split(".").pop())
        file_key = f"bank-forms/{str(uuid4())}/{name}.{ext}"
        client = S3Client(settings.AWS_STORAGE_BUCKET_NAME)
        upload = client.upload_file(
            file_key=file_key,
            file_obj=file,
            content_type=file.content_type,
            attachment=True
        )
        if not upload:
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data=dict(
                detail="Error uploading the file"
            ))
        return self.create(dict(
            name=name,
            extension=ext,
            file_key=file_key
        ))

    class Meta:
        model = BankForm
        fields = ["name", "file"]


class AdminBankFormResponseSerializer(CommonModelSerializer):
    """
    Response serializer after uploading the bank form
    """
    url = serializers.URLField(help_text="Open S3 bucket link to the file")

    class Meta:
        model = BankForm
        fields = ['id', 'name', 'url']
        read_only_fields = fields


class AdminBankDetailSerializer(CommonModelSerializer):
    workflow = serializers.PrimaryKeyRelatedField(queryset=SubmissionMethod.objects.all())
    forms = AdminBankFormResponseSerializer(many=True)

    class Meta:
        model = Bank
        fields = ['id', 'name', 'name_masked', 'status', 'logo', 'forms', 'workflow',
                  'encrypt_attachments', 'created_by', 'updated_by', 'created', 'updated']
        read_only_fields = ['created_by', 'updated_by', 'created', 'updated']


class AdminBankCreateSerializer(CommonModelSerializer):
    """
    Schema to add a new bank
    """
    workflow = serializers.PrimaryKeyRelatedField(queryset=SubmissionMethod.objects.all())
    forms = serializers.ListField(
        child=serializers.PrimaryKeyRelatedField(queryset=BankForm.objects.all()),
        help_text="List of ids of uploaded forms"
    )
    logo = serializers.URLField(required=True)

    def create(self, validated_data):
        """
        Create the bank and add the bank to the forms.
        """
        forms = validated_data.pop("forms")
        bank = super(AdminBankCreateSerializer, self).create(validated_data)
        for form in forms:
            form.bank = bank
            form.save()
        return bank

    class Meta:
        model = Bank
        fields = ['id', 'name', 'name_masked', 'status', 'logo', 'forms', 'workflow',
                  'encrypt_attachments', 'created_by', 'updated_by', 'created', 'updated']
        read_only_fields = ['created_by', 'updated_by', 'created', 'updated']


class BankHyperlinkedSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Bank
        fields = ['id', 'name', 'name_masked', 'status', 'logo']


class BankerSerializer(CommonModelSerializer):
    """
    Serializer for creating banker records.
    """
    bank = serializers.PrimaryKeyRelatedField(queryset=Bank.objects.all())

    class Meta:
        model = Banker
        fields = ['id', 'bank', 'name', 'country_code', 'phone_number', 'email', 'referral_link', 'remarks', 'status',
                  'created_by',
                  'updated_by', 'created', 'updated']
        read_only_fields = ['created_by', 'updated_by', 'created', 'updated']


class BankerResponseSerializer(serializers.ModelSerializer):
    """
    Response serializer for banker views with bank data attached.
    """
    bank = BankHyperlinkedSerializer()

    class Meta:
        model = Banker
        fields = ['id', 'bank', 'name', 'country_code', 'phone_number', 'email', 'referral_link', 'remarks', 'status',
                  'created_by',
                  'updated_by', 'created', 'updated']
        read_only_fields = fields


class BlockDateSerializer(CommonModelSerializer):
    banker = serializers.PrimaryKeyRelatedField(queryset=Banker.objects.all())
    block_from = serializers.DateField()
    block_till = serializers.DateField()

    def validate_block_from(self, value):
        # Make sure the start date is in the future.
        if value < timezone.localdate():
            raise serializers.ValidationError(detail="The start date must be in the future.")
        return value

    def validate_block_till(self, value):
        return value

    def validate(self, data):
        # Make sure the the end date is after start date
        if data['block_till'] < data['block_from']:
            raise serializers.ValidationError(detail="The end date must be after the start date")

        # Make sure the start date does not fall within any existing block dates.
        if BlockDate.objects.filter(banker=data['banker']).filter(
                Q(block_from__lte=data['block_from']) & Q(block_till__gte=data['block_from'])).exists():
            raise serializers.ValidationError(detail=dict(
                block_from="The start date falls within the range of the another blockdate."
            ))

        # Make sure the end date does not collide with existing block dates.
        if BlockDate.objects.filter(banker=data['banker']).filter(
                Q(block_from__lte=data['block_till']) & Q(block_till__gte=data['block_till'])).exists():
            raise serializers.ValidationError(detail=dict(
                block_till="The end date falls within the range of the another blockdate."
            ))

        return super(BlockDateSerializer, self).validate(data)

    class Meta:
        model = BlockDate
        fields = ['id', 'block_from', 'block_till', 'banker', 'created_by', 'updated_by', 'created', 'updated']
        read_only_fields = ['created_by', 'updated_by', 'created', 'updated']


class PackageRateSerializer(CommonModelSerializer):
    reference_rate = serializers.PrimaryKeyRelatedField(queryset=Rate.objects.all())

    class Meta:
        model = PackageRate
        fields = ['year', 'reference_rate', 'bank_spread']


class AdminRateHyperLinkedSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Rate
        fields = ['id', 'rate_type', 'reference', 'interest_rate', 'equation', 'remarks']


class AdminPackageRateHyperLinkedSerializer(serializers.HyperlinkedModelSerializer):
    reference_rate = AdminRateHyperLinkedSerializer()

    class Meta:
        model = PackageRate
        fields = ['year', 'reference_rate', 'bank_spread', 'total_interest_rate']
        read_only_fields = ['total_interest_rate']


class PackageDetailSerializer(CommonModelSerializer):
    bank = BankHyperlinkedSerializer()
    rates = AdminPackageRateHyperLinkedSerializer(many=True)

    class Meta:
        model = Package
        fields = ['id', 'bank', 'mask', 'rate_category', 'rate_type',
                  'property_types', 'property_status', 'loan_category', 'lock_in_period', 'min_loan_amount',
                  'deposit_to_place', 'valuation_subsidy', 'fire_insurance_subsidy', 'cash_rebate_legal_subsidy',
                  'cash_rebate_subsidy_clawback_period_years', 'partial_repayment_penalty',
                  'partial_repayment_penalty_remarks', 'full_repayment_penalty', 'full_repayment_penalty_remarks',
                  'rates',
                  'cancellation_fee', 'cancellation_fee_remarks', 'deposit_to_place_remarks', 'remarks_for_client',
                  'interest_offsetting',
                  'interest_reset_date', 'processing_fee', 'remarks_for_broker', 'new_purchase_referral_fee',
                  'refinance_referral_fee', 'publish',
                  'created_by', 'updated_by', 'created', 'updated']


class PackageSerializer(CommonModelSerializer):
    """
    Serializer for creating a new package.
    """
    bank = serializers.PrimaryKeyRelatedField(queryset=Bank.objects.all())
    mask = serializers.BooleanField(default=False)
    rate_category = serializers.ChoiceField(
        choices=KVStore.get_choices("rate_category"), required=True,
        help_text="From KVStore - 'rate_category'"
    )
    rate_type = serializers.ChoiceField(
        choices=KVStore.get_choices("rate_type"), required=True,
        help_text="From KVStore - 'rate_type'"
    )
    property_types = serializers.MultipleChoiceField(
        choices=KVStore.get_choices("property_type"), required=True,
        help_text="From KVStore - 'property_type'"
    )
    property_status = serializers.MultipleChoiceField(
        choices=KVStore.get_choices("property_status"), required=True,
        help_text="From KVStore - 'property_status'"
    )
    loan_category = serializers.MultipleChoiceField(
        choices=KVStore.get_choices("loan_category"), required=True,
        help_text="From KVStore - 'loan_category'"
    )
    lock_in_period = serializers.ChoiceField(
        choices=KVStore.get_choices("lock_in_period_years"), required=True,
        help_text="From KVStore - 'lock_in_period_years'"
    )
    min_loan_amount = serializers.FloatField()
    deposit_to_place = serializers.ChoiceField(
        choices=KVStore.get_choices("deposit_to_place"),
        help_text="From KVStore - 'deposit_to_place'"
    )
    valuation_subsidy = serializers.MultipleChoiceField(
        choices=KVStore.get_choices("valuation_subsidy"),
        help_text="From KVStore - 'valuation_subsidy'"
    )
    fire_insurance_subsidy = serializers.ChoiceField(
        choices=KVStore.get_choices("fire_insurance_subsidy"),
        help_text="From KVStore - 'fire_insurance_subsidy'"
    )
    cash_rebate_legal_subsidy = serializers.MultipleChoiceField(
        choices=KVStore.get_choices("cash_rebate_legal_subsidy_remarks"),
        required=True,
        help_text="From KVStore - 'cash_rebate_legal_subsidy_remarks'"
    )
    cash_rebate_subsidy_clawback_period_years = serializers.ChoiceField(
        choices=KVStore.get_choices("cash_rebate_subsidy_clawback_period_years"),
        help_text="From KVStore - 'cash_rebate_subsidy_clawback_period_years'"
    )
    partial_repayment_penalty = serializers.ChoiceField(
        choices=KVStore.get_choices("partial_repayment_penalty"),
        help_text="From KVStore - 'partial_repayment_penalty'"
    )
    partial_repayment_penalty_remarks = serializers.ChoiceField(
        choices=KVStore.get_choices("partial_repayment_penalty_remarks"),
        help_text="From KVStore - 'partial_repayment_penalty_remarks'"
    )
    full_repayment_penalty = serializers.ChoiceField(
        choices=KVStore.get_choices("full_repayment_penalty"),
        help_text="From KVStore - 'full_repayment_penalty'"
    )
    full_repayment_penalty_remarks = serializers.ChoiceField(
        choices=KVStore.get_choices("full_repayment_penalty_remarks"),
        help_text="From KVStore - 'full_repayment_penalty_remarks'"
    )
    cancellation_fee = serializers.ChoiceField(
        choices=KVStore.get_choices("cancellation_fee"),
        help_text="From KVStore - 'cancellation_fee'"
    )
    cancellation_fee_remarks = serializers.ChoiceField(
        choices=KVStore.get_choices("cancellation_fee_remarks"),
        help_text="Values from kvstore - 'cancellation_fee_remarks'"
    )
    deposit_to_place_remarks = serializers.ChoiceField(
        choices=KVStore.get_choices("deposit_to_place_remarks"),
        help_text="From KVStore - 'deposit_to_place_remarks'"
    )
    remarks_for_client = serializers.MultipleChoiceField(
        choices=KVStore.get_choices("remarks_for_client"),
        help_text="From KVStore - 'remarks_for_client'"
    )
    interest_offsetting = serializers.ChoiceField(
        choices=KVStore.get_choices("interest_offsetting"),
        help_text="From KVStore - 'interest_offsetting'"
    )
    interest_reset_date = serializers.ChoiceField(
        required=False,
        allow_blank=True,
        choices=KVStore.get_choices("interest_reset_date"),
        help_text="Fron KVStore - 'interest_reset_date'"
    )
    processing_fee = serializers.ChoiceField(
        required=False,
        allow_blank=True,
        choices=KVStore.get_choices("processing_fee"),
        help_text="Fron KVStore - 'processing_fee'"
    )
    remarks_for_broker = serializers.CharField(required=False, allow_blank=True, allow_null=True)
    new_purchase_referral_fee = serializers.ChoiceField(
        choices=KVStore.get_choices("new_purchase_referral_fee"),
        help_text="From KVStore - 'new_purchase_referral_fee'"
    )
    refinance_referral_fee = serializers.ChoiceField(
        choices=KVStore.get_choices("refinance_referral_fee"),
        help_text="From KVStore - 'refinance_referral_fee'"
    )
    publish = serializers.BooleanField(default=False)

    # rates table
    rates = PackageRateSerializer(many=True)

    def validate(self, attrs):
        """
        This is a hack to make sure no sets are there in place of list
        """
        ret = dict()
        for k, v in attrs.items():
            if type(v) == set:
                ret[k] = list(v)
                continue
            ret[k] = v
        return ret

    def validate_rates(self, value):
        """
        Make sure rates for all years needs to be entered.
        """
        year_values = {'year_1', 'year_2', 'year_3', 'year_4', 'year_5', 'thereafter'}
        diff = year_values.difference({v['year'] for v in value})
        if diff:
            fields = ",".join([y.replace("_", " ").title() for y in diff])
            raise ValidationError(
                detail=f"Rates for '{fields}' need to be entered"
            )

        return value

    def create(self, validated_data):
        """
        Create the package first, then add the rates to the package.
        """
        rates = validated_data.pop("rates")
        package = super(PackageSerializer, self).create(validated_data)
        for rate in rates:
            PackageRate.objects.create(package=package, **rate)
        return package

    def update(self, instance, validated_data):
        """
        Update the instance first, then save the rates on by one
        """
        rates = validated_data.pop("rates")
        package = super(PackageSerializer, self).update(instance, validated_data)
        for rate in rates:
            PackageRate.objects.update_or_create(package=package, year=rate.pop("year"), defaults=rate)
        return package

    class Meta:
        model = Package
        fields = ['id', 'bank', 'mask', 'rate_category', 'rate_type', 'property_types', 'property_status',
                  'loan_category', 'rates',
                  'lock_in_period', 'min_loan_amount', 'deposit_to_place', 'valuation_subsidy',
                  'fire_insurance_subsidy',
                  'cash_rebate_legal_subsidy', 'cash_rebate_subsidy_clawback_period_years',
                  'partial_repayment_penalty', 'partial_repayment_penalty_remarks', 'full_repayment_penalty',
                  'full_repayment_penalty_remarks', 'cancellation_fee', 'cancellation_fee_remarks',
                  'deposit_to_place_remarks', 'remarks_for_client',
                  'interest_offsetting', 'interest_reset_date', 'processing_fee', 'remarks_for_broker',
                  'new_purchase_referral_fee',
                  'refinance_referral_fee', 'publish', 'created_by', 'updated_by', 'created', 'updated']

        read_only_fields = ['created_by', 'updated_by', 'created', 'updated']


class RateCreateSerializer(CommonModelSerializer):
    rate_type = serializers.ChoiceField(choices=KVStore.get_choices("rate_type"))
    equation = serializers.ChoiceField(choices=KVStore.get_choices("equation"))
    banks = serializers.PrimaryKeyRelatedField(
        many=True,
        allow_empty=False,
        queryset=Bank.objects.all(),
        help_text="An array of bank ids"
    )

    class Meta:
        model = Rate
        fields = ['id', 'banks', 'rate_type', 'reference', 'interest_rate',
                  'equation', 'remarks', 'created_by', 'updated_by', 'created', 'updated', ]
        read_only_fields = ['created_by', 'updated_by', 'created', 'updated']


class RateDetailSerializer(CommonModelSerializer):
    rate_type = serializers.ChoiceField(choices=KVStore.get_choices("rate_type"))
    equation = serializers.ChoiceField(choices=KVStore.get_choices("equation"))
    banks = BankHyperlinkedSerializer(many=True, help_text="List of bank attached to this rate")

    class Meta:
        model = Rate
        fields = ['id', 'banks', 'rate_type', 'reference', 'interest_rate',
                  'equation', 'remarks', 'created_by', 'updated_by', 'created', 'updated', ]
        read_only_fields = ['created_by', 'updated_by', 'created', 'updated']


class PackageCSVExportSerializer(serializers.Serializer):
    packages = serializers.ListField(
        child=serializers.IntegerField(),
        allow_null=True,
        allow_empty=True
    )
    all_packages = serializers.BooleanField(default=False)
