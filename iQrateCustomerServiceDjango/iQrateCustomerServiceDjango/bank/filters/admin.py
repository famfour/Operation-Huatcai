import django_filters
from bank.models import Bank, Banker, BlockDate, Rate, Package, PackageRate
from common.filters import NumberInFilter, CharInFilter
from kvstore.choices import RateTypeChoices


class BankFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr="icontains", label="Full or partial name of the bank")
    name_masked = django_filters.CharFilter(lookup_expr="icontains", label="Search inside the masked name")
    status = django_filters.BooleanFilter(lookup_expr='exact', label="Bank status true/false")

    class Meta:
        model = Bank
        fields = ['name', 'name_masked', 'status']


class BankerFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr="icontains")
    bank_id = django_filters.NumberFilter(lookup_expr='exact')
    phone_number = django_filters.CharFilter(lookup_expr="icontains")
    email = django_filters.CharFilter(lookup_expr="icontains")
    status = django_filters.BooleanFilter(lookup_expr='exact')

    class Meta:
        model = Banker
        fields = ['name', "bank_id", "phone_number", "email", "status"]


class RateFilter(django_filters.FilterSet):
    rate_type = CharInFilter(
        field_name="rate_type",
        lookup_expr="in", distinct=True,
        help_text="*'rate_type'* values obtained from the kvstore  \nTo search with multiple values, separate them with a comma")
    equation = CharInFilter(
        field_name="equation",
        lookup_expr="in",
        distinct=True,
        help_text="*'equation'* values obtained from the kvstore  \nTo search with multiple values, separate them with a comma")
    bank = NumberInFilter(
        lookup_expr='in',
        field_name="banks",
        distinct=True,
        help_text="*'id'* field of banks. List of banks can be obtained from api.  \nTo search with multiple values, separate them with a comma")

    class Meta:
        model = Rate
        fields = ["bank", 'rate_type', "equation"]


class BlockDateFilter(django_filters.FilterSet):
    banker_id = django_filters.NumberFilter(lookup_expr="exact")

    class Meta:
        model = BlockDate
        fields = ['banker_id']


class AdminPackageFilter(django_filters.FilterSet):
    bank_id = NumberInFilter(lookup_expr="in", field_name="bank_id", distinct=True)
    property_types = CharInFilter(lookup_expr="contains", field_name="property_types", distinct=True)
    property_status = CharInFilter(lookup_expr="contains", field_name="property_status", distinct=True)
    rate_category = CharInFilter(lookup_expr="in", field_name="rate_category", distinct=True)
    rate_type = CharInFilter(lookup_expr="in",field_name="rate_type", distinct=True)
    loan_amount = django_filters.NumberFilter(lookup_expr="lte", field_name='min_loan_amount')
    status = django_filters.BooleanFilter(field_name='publish')

    class Meta:
        model = Package
        fields = ['bank_id']
