import django_filters
from drf_spectacular.utils import extend_schema, extend_schema_field

from common.filters import NumberInFilter, CharInFilter
from kvstore.choices import PropertyTypeChoices, PropertyStatusChoices, RateTypeChoices


class BankFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr="icontains")
    has_form = django_filters.BooleanFilter(method="has_form_filter")

    def has_form_filter(self, queryset, name, value):
        return queryset.filter(forms__isnull=not value)


class BankerFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr="icontains")
    bank = django_filters.NumberFilter(lookup_expr="exact", field_name="bank_id")


class PackageFilter(django_filters.FilterSet):
    bank_id = NumberInFilter(lookup_expr="in", field_name="bank_id", distinct=True)
    property_types = CharInFilter(lookup_expr="contains", field_name="property_types", distinct=True)
    property_status = CharInFilter(lookup_expr="contains", field_name="property_status", distinct=True)
    rate_type = CharInFilter(lookup_expr="in", field_name="rate_type", distinct=True)
    loan_amount = django_filters.NumberFilter(lookup_expr="lte", field_name='min_loan_amount')
    loan_category = CharInFilter(lookup_expr="contains", field_name="loan_category", distinct=True)
    sell_property_in_3_years = django_filters.BooleanFilter(method='sell_property_in_3_years_filter')
    has_200k = django_filters.BooleanFilter(method='deposit_to_place_filter', distinct=True)

    def deposit_to_place_filter(self, queryset, name, value):
        """
        Checks whether the package requires
        """
        if not value:
            return queryset
        return queryset.has_200_k()

    def sell_property_in_3_years_filter(self, queryset, name, value):
        """
        If value is true, then exclude packages with full_repayment_penalty_remarks
        """
        if not value:
            return queryset
        return queryset.plan_to_sell_in_3_years()


class RefinanceCalculatorPackageFilter(django_filters.FilterSet):
    property_type = django_filters.ChoiceFilter(
        field_name="property_types",
        method='filter_property_type',
        choices=PropertyTypeChoices.choices,
        required=True,
        help_text="Values for this can be obtained from kvstore -> 'property_type'. Only one value is accepted")
    property_status = django_filters.ChoiceFilter(
        choices=PropertyStatusChoices.choices,
        field_name="property_status",
        method='filter_property_status',
        required=True,
        help_text="Values for this can be obtained from kvstore -> 'property_status'. Only one value is accepted"
    )
    loan_amount = django_filters.NumberFilter(
        field_name="min_loan_amount",
        lookup_expr="lte", required=True)
    exising_bank = django_filters.NumberFilter(
        method="exclude_bank",
        distinct=True,
        help_text="'id' of the bank, bank list can be obtained from banks API")
    has_200k = django_filters.BooleanFilter(
        method='deposit_to_place_filter', distinct=True, required=True)
    sell_within_3_years = django_filters.BooleanFilter(
        method='sell_property_in_3_years_filter',
        required=True)

    def filter_property_status(self, queryset, name, value):
        return queryset.filter(property_status__contains=[value]).distinct()

    def filter_property_type(self, queryset, name, value):
        return queryset.filter(property_types__contains=[value]).distinct()

    def exclude_bank(self, queryset, name, value):
        return queryset.exclude(bank_id=value)

    def deposit_to_place_filter(self, queryset, name, value):
        if not value:
            return queryset
        return queryset.has_200_k()

    def sell_property_in_3_years_filter(self, queryset, name, value):
        if not value:
            return queryset
        return queryset.plan_to_sell_in_3_years()


class BUCCalculatorPackageFilter(django_filters.FilterSet):
    loan_amount = django_filters.NumberFilter(
        field_name="min_loan_amount",
        lookup_expr="lte", required=True)


class NewPurchaseCalculatorFilter(django_filters.FilterSet):
    property_type = django_filters.ChoiceFilter(
        field_name="property_types",
        method='filter_property_type',
        choices=PropertyTypeChoices.choices,
        required=True
    )
    property_status = django_filters.ChoiceFilter(
        choices=PropertyStatusChoices.choices,
        field_name="property_status",
        method='filter_property_status',
        required=True
    )
    loan_amount = django_filters.NumberFilter(
        field_name="min_loan_amount",
        lookup_expr="lte", required=True
    )

    def filter_property_status(self, queryset, name, value):
        return queryset.filter(property_status__contains=[value]).distinct()

    def filter_property_type(self, queryset, name, value):
        return queryset.filter(property_types__contains=[value]).distinct()


class RateFilter(django_filters.FilterSet):
    bank = django_filters.NumberFilter(
        field_name='banks',
        distinct=True,
        help_text="'id' of the bank"
    )
