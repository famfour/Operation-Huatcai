from drf_spectacular.utils import extend_schema

from bank.models import Banker
from rest_framework.generics import ListAPIView
from bank.serializers.agent import CustomerBankerSerializer
from bank.filters.agent import BankerFilter
import django_filters


@extend_schema(tags=["Customer - Bankers"], summary="Get a list of Bankers")
class BankerListAPIView(ListAPIView):
    """
    List all banks
    """
    serializer_class = CustomerBankerSerializer
    filterset_class = BankerFilter
    queryset = Banker.objects.filter(status=True).all()  # Only enabled banker should be displayed
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
