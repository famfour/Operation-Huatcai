from drf_spectacular.utils import extend_schema

from bank.models import Bank
from rest_framework.generics import ListAPIView
from bank.serializers.agent import CustomerBankSerializer
from bank.filters.agent import BankFilter
import django_filters


@extend_schema(tags=["Customer - Bank"], summary="Get a list of Banks")
class BankListAPIView(ListAPIView):
    """
    List all banks
    """
    serializer_class = CustomerBankSerializer
    filterset_class = BankFilter
    queryset = Bank.objects.filter(status=True).distinct()  # Only enabled banks should be displayed.
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
