import django_filters
from drf_spectacular.utils import extend_schema
from rest_framework.generics import ListAPIView

from bank.models import Rate
from bank.filters.agent import RateFilter
from bank.serializers.agent import AgentRateSerializer


@extend_schema(tags=["Agent - Payout"], summary="Rates For Payout Dropdown", responses=AgentRateSerializer)
class RateListAPIView(ListAPIView):
    serializer_class = AgentRateSerializer
    filterset_class = RateFilter
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    queryset = Rate.objects.all()
