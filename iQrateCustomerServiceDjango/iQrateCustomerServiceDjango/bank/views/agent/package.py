from io import BytesIO
from uuid import uuid4

import django_filters
from django.conf import settings
from django.template.loader import render_to_string
from drf_spectacular.utils import extend_schema
from rest_framework.exceptions import server_error
from rest_framework.generics import ListAPIView, GenericAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from xhtml2pdf import pisa

from bank.filters.agent import PackageFilter, RefinanceCalculatorPackageFilter, BUCCalculatorPackageFilter, NewPurchaseCalculatorFilter
from bank.models import Package
from bank.serializers.agent import CustomerPackageDetailSerializer, PackageExportRequestSerilizer, \
    PackageExportResponseSerilizer, CalculatorPackageListSerializer
from common.s3client import S3Client
from kvstore.choices import RateCategoryChoices, RateTypeChoices, PropertyStatusChoices


@extend_schema(tags=["Agent - Bank - Package"], summary="List all packages", responses=CustomerPackageDetailSerializer)
class PackageListAPIView(ListAPIView):
    serializer_class = CustomerPackageDetailSerializer
    filterset_class = PackageFilter
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]

    def get_queryset(self):
        return Package.objects.filter(publish=True).all()


@extend_schema(tags=['Agent - Bank - Package'], summary="Export Packages to PDF", request=PackageExportRequestSerilizer,
               responses=PackageExportResponseSerilizer)
class PackageExportView(APIView):

    def post(self, request):
        serializer = PackageExportRequestSerilizer(data=request.data)
        serializer.is_valid(raise_exception=True)

        # Write the PDF to memory
        file_obj = BytesIO()

        # Prep the data for PDF
        package_data = [p.data() for p in serializer.validated_data['packages']]
        data = dict()
        keys = package_data[0].keys()
        for key in keys:
            data[key] = [item.get(key, "") for item in package_data]
        # create the PDF
        html = render_to_string("bank/pdf/packages.html", context=dict(packages=data))
        pisa_status = pisa.CreatePDF(html, dest=file_obj)
        if pisa_status.err:
            raise server_error(request=request)
        # Upload to the S3 bucket.
        client = S3Client(settings.AWS_TEMP_BUCKET_NAME)
        file_key = f"{str(uuid4())}.pdf"
        client.upload_file(
            file_key=file_key,
            file_obj=file_obj.getvalue(),
            content_type='application/pdf'
        )
        file_obj.close()

        url = client.get_presigned_url(
            file_key=file_key,
            expire_in=600, content_disposition=f"attachment;filename=IQRATE-PACKAGES.PDF"
        )
        resp_serializer = PackageExportResponseSerilizer(instance=dict(
            url=url
        ))
        return Response(data=resp_serializer.data)


"""
from django.views.generic import TemplateView, View
from django.http.response import HttpResponse

class PackageHTMLView(View):
    # @todo - Remove this view
    template_name = "bank/pdf/packages.html"

    def get(self, *args, **kwargs):
        cd = dict()
        packages = Package.objects.order_by("-pk").all()[:2]
        package_data = [p.data() for p in packages]
        print("============================================================================================")
        print([p.pk for p in packages])
        print("============================================================================================")
        data = dict()
        keys = package_data[0].keys()
        for key in keys:
            data[key] = [item.get(key, "") for item in package_data]
        cd['packages'] = data

        # Write the PDF to memory
        file_obj = BytesIO()
        html = render_to_string("bank/pdf/packages.html", context=cd)
        pisa_status = pisa.CreatePDF(html, dest=file_obj)
        if pisa_status.err:
            raise server_error(request=self.request)
        return HttpResponse(content=file_obj.getvalue(), content_type="application/pdf")
"""


@extend_schema(tags=["Agent - Bank - Package"], summary="Filter packages For calculator Refinance",
               responses=CalculatorPackageListSerializer, filters=True)
class RefinanceCalculatorPackageFilterAPIView(GenericAPIView):
    """
    This view responds with
    """
    serializer_class = CalculatorPackageListSerializer
    filterset_class = RefinanceCalculatorPackageFilter
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    queryset = Package.objects.none()

    def get(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        # Get both fixed and floating packages
        fixed = queryset.fixed_only().first()
        floating = queryset.floating_only().first()
        serializer = self.get_serializer(instance=dict(
            fixed=fixed,
            floating=floating
        ))
        return Response(serializer.data)

    def get_queryset(self):
        """
        All published packages, filtered based on the agent's subscription type.
        """
        query = Package.objects.published_only()
        if self.request.user.membership_type == settings.BASIC_MEMBERSHIP:
            query = query.no_exclusive()
        return query.order_by_rate()


@extend_schema(tags=["Agent - Bank - Package"], summary="Filter packages For calculator BUC",
               responses=CustomerPackageDetailSerializer, filters=True,
               description="Response only contains packages with -  \n property_status: under construction  \n package_type: floating")
class BUCCalculatorPackageFilterAPIView(GenericAPIView):
    """
    Packages for BUC calculator
    """
    serializer_class = CustomerPackageDetailSerializer
    filterset_class = BUCCalculatorPackageFilter
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    queryset = Package.objects.none()

    def get(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = CustomerPackageDetailSerializer(
            instance=queryset.first(), context=self.get_serializer_context())
        return Response(serializer.data)

    def get_queryset(self):
        """
        BUC only needs floating packages and property status under construction
        """
        query = Package.objects.published_only().floating_only()
        if self.request.user.membership_type == settings.BASIC_MEMBERSHIP:
            query = query.no_exclusive()
        return query.exclude(property_status__contains=[PropertyStatusChoices.completed]).order_by_rate()


@extend_schema(tags=["Agent - Bank - Package"], summary="Filter packages for new purchase calculator",
               responses=CalculatorPackageListSerializer, filters=True)
class NewPurchaseCalculatorApiView(GenericAPIView):
    """
    Returns one fixed and One floating package.
    """
    serializer_class = CalculatorPackageListSerializer
    filterset_class = NewPurchaseCalculatorFilter
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    queryset = Package.objects.none()

    def get(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        fixed = queryset.fixed_only().first()
        floating = queryset.floating_only().first()
        serializer = self.get_serializer(
            instance=dict(
                fixed=fixed,
                floating=floating
            ),
            context=self.get_serializer_context()
        )
        return Response(serializer.data)

    def get_queryset(self):
        query = Package.objects.published_only()
        if self.request.user.membership_type == settings.BASIC_MEMBERSHIP:
            query = query.no_exclusive()
        return query.order_by_rate()
