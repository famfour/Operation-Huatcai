import csv
from io import StringIO
from uuid import uuid4

import django_filters
from django.conf import settings
from django.db.models.deletion import ProtectedError
from drf_spectacular.utils import extend_schema
from rest_framework import status
from rest_framework.exceptions import NotFound, PermissionDenied
from rest_framework.generics import ListAPIView, CreateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from bank.filters.admin import AdminPackageFilter
from bank.models import Package
from bank.serializers.admin import PackageSerializer, PackageDetailSerializer, PackageCSVExportSerializer
from common.responses import ProtectedInstanceResponse
from common.s3client import S3Client
from common.serializers import FileResponseSerializer
from kvstore.choices import PropertyTypeChoices, PropertyStatusChoices, LoanCategoryChoices


@extend_schema(tags=["Admin - Bank - Package"], summary="List all packages", responses=PackageDetailSerializer)
class PackageListAPIView(ListAPIView):
    serializer_class = PackageDetailSerializer
    filterset_class = AdminPackageFilter
    queryset = Package.objects.all()
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


@extend_schema(tags=["Admin - Bank - Package"], summary="Create a new package", request=PackageSerializer, responses=PackageDetailSerializer)
class PackageCreateAPIView(CreateAPIView):
    """
    Create a new package.  \n
    Please note that the `rates` value should be in the following format  \n
    The `year` value should be unique and rates for all years should be included.  \n
    The `reference_rate` value must be from rates API.  \n
    ```json
    {
      "year": "year_1","reference_rate": int,"bank_spread": float
    },
    {
      "year": "year_2","reference_rate": int,"bank_spread": float
    },
    {
      "year": "year_3","reference_rate": int,"bank_spread": float
    },
    {
      "year": "year_4","reference_rate": int,"bank_spread": float
    },
    {
      "year": "year_5","reference_rate": int,"bank_spread": float
    },
    {
      "year": "thereafter","reference_rate": int,"bank_spread": float
    }
    ```
    """
    serializer_class = PackageSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        res_serializer = PackageDetailSerializer(serializer.instance)
        headers = self.get_success_headers(res_serializer.data)
        return Response(res_serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class PackageDetailAPIView(APIView):
    serializer_class = PackageSerializer

    def get_object(self, pk):
        try:
            return Package.objects.prefetch_related("rates", "rates__reference_rate", "bank").get(pk=pk)
        except Package.DoesNotExist:
            raise NotFound()

    @extend_schema(tags=["Admin - Bank - Package"], summary="Get package details", responses=PackageDetailSerializer)
    def get(self, request, pk):
        package = self.get_object(pk)
        serializer = PackageDetailSerializer(package)
        return Response(serializer.data)

    @extend_schema(tags=["Admin - Bank - Package"], summary="Update Package", request=PackageSerializer, responses=PackageDetailSerializer)
    def put(self, request, pk):
        package = self.get_object(pk)
        serializer = PackageSerializer(package, data=request.data, context=self.get_renderer_context())
        if serializer.is_valid():
            obj = serializer.save()
            resp_serializer = PackageDetailSerializer(instance=obj)
            return Response(resp_serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @extend_schema(tags=["Admin - Bank - Package"], summary="Delete Package")
    def delete(self, request, pk):
        package = self.get_object(pk)
        try:
            package.delete()
        except ProtectedError:
            return ProtectedInstanceResponse(instance=package)
        return Response(status=status.HTTP_204_NO_CONTENT)


class PackageCSVExportView(APIView):
    serializer_class = FileResponseSerializer

    @extend_schema(tags=["Admin - Bank - Package"], summary="Export to CSV",
                   responses=FileResponseSerializer, request=PackageCSVExportSerializer)
    def post(self, request):
        # validate the data
        serializer = PackageCSVExportSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        # ByteSteam to write the data to.
        file_obj = StringIO()

        # Create the queryset
        if serializer.validated_data['all_packages']:
            packages = Package.objects.prefetch_related("bank", "rates").all()
        else:
            packages = Package.objects.filter(
                pk__in=serializer.validated_data['packages']
            ).prefetch_related("bank", "rates").all()
            if not len(packages):
                raise PermissionDenied(detail="Please select at least one package")

        # Prepare the kvstore values to save on the mysql queries
        kvstore_values = dict()
        kvstore_values['property_type'] = dict(PropertyTypeChoices.choices)
        kvstore_values['property_status'] = dict(PropertyStatusChoices.choices)
        kvstore_values['loan_category'] = dict(LoanCategoryChoices.choices)

        headers = ['id', 'bank', 'Year 1', 'Year 2', 'Year 3', 'Year 4', 'Year 5', 'Thereafter', 'Mask', 'Rate Category',
                   'Rate Type', 'Property Types', 'Property Status', 'Loan Category', 'Lock In Period',
                   'Min Loan Amount', 'Deposit To Place', 'Valuation Subsidy', 'Fire Insurance Subsidy',
                   'Cash Rebate-Legal Subsidy', 'Cash Rebate Subsidy Clawback Period Years', 'Partial Repayment Penalty',
                   'Partial Repayment Penalty Remarks', 'Full Repayment Penalty', 'Full Repayment Penalty Remarks',
                   'Cancellation Fee', 'Cancellation Fee Remarks', 'Deposit To Place Remarks', 'Remarks For Client',
                   'Interest Offsetting', 'Interest Reset Date', 'Processing Fee', 'Remarks For Broker',
                   'New Purchase Referral Fee', 'Refinance Referral Fee', 'Publish']

        writer = csv.DictWriter(file_obj, fieldnames=headers)
        writer.writeheader()
        for package in packages:
            rates = dict()
            for rate in package.rates.all():
                rates[rate.year] = rate.total_interest_rate

            writer.writerow({
                "id": package.id,
                "bank": package.bank.name,
                "Year 1": f"{rates.get('year_1', '')}%",
                "Year 2": f"{rates.get('year_2', '')}%",
                "Year 3": f"{rates.get('year_3', '')}%",
                "Year 4": f"{rates.get('year_4', '')}%",
                "Year 5": f"{rates.get('year_5', '')}%",
                "Thereafter": f"{rates.get('thereafter', '')}%",
                "Mask": "Yes" if package.mask else "No",
                "Rate Category": package.get_rate_category_display(),
                "Rate Type": package.get_rate_type_display(),
                "Property Types": ",".join([kvstore_values['property_type'][item] for item in package.property_types]),
                "Property Status": ",".join([kvstore_values['property_status'][item] for item in package.property_status]),
                "Loan Category": ",".join([kvstore_values['loan_category'][item] for item in package.loan_category]),
                "Lock In Period": package.lock_in_period,
                "Min Loan Amount": package.min_loan_amount,
                "Deposit To Place": package.deposit_to_place,
                "Valuation Subsidy": ",".join(package.valuation_subsidy),
                "Fire Insurance Subsidy": package.fire_insurance_subsidy,
                "Cash Rebate-Legal Subsidy": ",".join(package.cash_rebate_legal_subsidy),
                "Cash Rebate Subsidy Clawback Period Years": package.cash_rebate_subsidy_clawback_period_years,
                "Partial Repayment Penalty": package.partial_repayment_penalty,
                "Partial Repayment Penalty Remarks": package.partial_repayment_penalty_remarks,
                "Full Repayment Penalty": package.full_repayment_penalty,
                "Full Repayment Penalty Remarks": package.full_repayment_penalty_remarks,
                "Cancellation Fee": package.cancellation_fee,
                "Cancellation Fee Remarks": package.cancellation_fee_remarks,
                "Deposit To Place Remarks": package.deposit_to_place_remarks,
                "Remarks For Client": ",".join(package.remarks_for_client),
                "Interest Offsetting": package.interest_offsetting,
                "Interest Reset Date": package.interest_reset_date,
                "Processing Fee": package.processing_fee,
                "Remarks For Broker": package.remarks_for_broker,
                "New Purchase Referral Fee": f"{package.new_purchase_referral_fee}%",
                "Refinance Referral Fee": f"{package.refinance_referral_fee}%",
                "Publish": "Yes" if package.publish else "No",
            })

        # upload the file to S3
        client = S3Client(settings.AWS_TEMP_BUCKET_NAME)
        file_key = f"{str(uuid4())}.pdf"
        client.upload_file(
            file_key=file_key,
            file_obj=file_obj.getvalue(),
            content_type='text/csv'
        )
        file_obj.close()
        url = client.get_presigned_url(
            file_key=file_key,
            expire_in=600, content_disposition=f"attachment;filename=exported-packages.csv"
        )
        resp_serializer = FileResponseSerializer(instance=dict(
            url=url
        ))
        return Response(data=resp_serializer.data)
