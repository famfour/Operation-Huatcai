import django_filters

from rest_framework.exceptions import NotFound
from django.db.models.deletion import ProtectedError
from drf_spectacular.utils import extend_schema

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import ListAPIView, CreateAPIView

from bank.filters.admin import BankerFilter
from bank.models import Banker
from bank.serializers.admin import BankerSerializer, BankerResponseSerializer
from common.responses import ProtectedInstanceResponse


@extend_schema(tags=["Admin - Bank - Banker"], summary="Get a list of Bankers",
               description="Get a paginated, filtered list of all bankers", responses=BankerResponseSerializer)
class BankerListAPIView(ListAPIView):
    serializer_class = BankerResponseSerializer
    filterset_class = BankerFilter
    queryset = Banker.objects.prefetch_related("bank").all()
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]


@extend_schema(tags=["Admin - Bank - Banker"], summary="Create bankers", request=BankerSerializer,
               responses=BankerResponseSerializer)
class BankerCreateAPIView(CreateAPIView):
    """
    Create a new banker for an existing bank
    """
    serializer_class = BankerSerializer

    def create(self, request, *args, **kwargs):
        """
        Overriding the create method to modify the response serializer.
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        res_serializer = BankerResponseSerializer(serializer.instance)
        headers = self.get_success_headers(res_serializer.data)
        return Response(res_serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class BankerDetailAPIView(APIView):
    serializer_class = BankerSerializer

    def get_object(self, pk):
        try:
            return Banker.objects.get(pk=pk)
        except Banker.DoesNotExist:
            raise NotFound()

    @extend_schema(tags=["Admin - Bank - Banker"], summary="Banker Details", responses=BankerResponseSerializer)
    def get(self, request, pk):
        """
        Get details of a banker with ID
        """
        banker = self.get_object(pk)
        serializer = BankerResponseSerializer(banker)
        return Response(serializer.data)

    @extend_schema(tags=["Admin - Bank - Banker"], summary="Update Banker", request=BankerSerializer, responses=BankerResponseSerializer)
    def put(self, request, pk):
        banker = self.get_object(pk)
        serializer = BankerSerializer(banker, data=request.data, context=self.get_renderer_context())
        if serializer.is_valid():
            instance = serializer.save()
            res_serializer = BankerResponseSerializer(instance=instance)
            return Response(res_serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @extend_schema(tags=["Admin - Bank - Banker"], summary="Delete Banker")
    def delete(self, request, pk):
        banker = self.get_object(pk)
        try:
            banker.delete()
        except ProtectedError:
            return ProtectedInstanceResponse(instance=banker)
        return Response(status=status.HTTP_204_NO_CONTENT)
