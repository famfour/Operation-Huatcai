import django_filters

from rest_framework.exceptions import NotFound
from drf_spectacular.utils import extend_schema

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import ListAPIView, CreateAPIView

from bank.filters.admin import BlockDateFilter
from bank.models import BlockDate
from bank.serializers.admin import BlockDateSerializer


@extend_schema(tags=["Admin - Bank - Banker - Block Date"], summary="Get a paginated/filtered list of banker blocked dates")
class BlockDateListAPIView(ListAPIView):
    serializer_class = BlockDateSerializer
    filterset_class = BlockDateFilter
    queryset = BlockDate.objects.all()
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]


@extend_schema(tags=["Admin - Bank - Banker - Block Date"], summary="Add a new blockdate for a banker")
class BlockDateCreateAPIView(CreateAPIView):
    serializer_class = BlockDateSerializer


class BlockDateDetailAPIView(APIView):
    """
    Retrieve, update or delete a BlockDate instance.
    """
    serializer_class = BlockDateSerializer

    def get_object(self, pk):
        try:
            return BlockDate.objects.get(pk=pk)
        except BlockDate.DoesNotExist:
            raise NotFound()

    @extend_schema(tags=["Admin - Bank - Banker - Block Date"], summary="Get blockdate details")
    def get(self, request, pk):
        block_dates = self.get_object(pk)
        serializer = BlockDateSerializer(block_dates)
        return Response(serializer.data)

    @extend_schema(tags=["Admin - Bank - Banker - Block Date"], summary="Update blockdate details")
    def put(self, request, pk):
        block_dates = self.get_object(pk)
        serializer = BlockDateSerializer(block_dates, data=request.data, context=self.get_renderer_context())
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @extend_schema(tags=["Admin - Bank - Banker - Block Date"], summary="Delete a blockdate")
    def delete(self, request, pk):
        block_dates = self.get_object(pk)
        block_dates.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
