from drf_spectacular.utils import extend_schema

from rest_framework.generics import CreateAPIView, DestroyAPIView
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from rest_framework import status
from rest_framework.exceptions import NotFound

from bank.models import BankForm
from bank.serializers.admin import AdminBankFormResponseSerializer, AdminBankFormCreateSerializer


@extend_schema(tags=["Admin - Bank"], summary="Upload Bank Form",
               request=AdminBankFormCreateSerializer, responses=AdminBankFormResponseSerializer)
class BankFormCreateView(CreateAPIView):
    model = BankForm
    serializer_class = AdminBankFormCreateSerializer
    parser_classes = [MultiPartParser, FormParser]

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = serializer.save()
        headers = self.get_success_headers(serializer.data)
        resp_serializer = AdminBankFormResponseSerializer(instance=instance)
        return Response(resp_serializer.data, status=status.HTTP_201_CREATED, headers=headers)


@extend_schema(tags=["Admin - Bank"], summary="Delete Bank Form")
class BankFormDestroyView(DestroyAPIView):
    model = BankForm
    queryset = BankForm.objects.none()

    def get_object(self):
        try:
            return BankForm.objects.get(pk=self.kwargs.get("pk"))
        except BankForm.DoesNotExist:
            raise NotFound(detail="Bank form does not exist or deleted before.")
