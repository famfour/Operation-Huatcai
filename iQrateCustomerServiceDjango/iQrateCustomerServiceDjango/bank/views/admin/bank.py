import django_filters

from rest_framework.exceptions import NotFound
from django.db.models.deletion import ProtectedError
from drf_spectacular.utils import extend_schema

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import ListAPIView, CreateAPIView

from bank.filters.admin import BankFilter
from bank.models import Bank
from bank.serializers.admin import AdminBankDetailSerializer, AdminBankCreateSerializer
from common.responses import ProtectedInstanceResponse


@extend_schema(tags=["Admin - Bank"], summary="Get a list of Banks")
class BankListAPIView(ListAPIView):
    """
    List all banks
    """
    serializer_class = AdminBankDetailSerializer
    filterset_class = BankFilter
    queryset = Bank.objects.all()
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]


@extend_schema(tags=["Admin - Bank"], summary="Add a new Bank",
               request=AdminBankCreateSerializer, responses=AdminBankDetailSerializer)
class BankCreateAPIView(CreateAPIView):
    """
    Create bank
    """
    serializer_class = AdminBankCreateSerializer

    def create(self, request, *args, **kwargs):
        serializer = AdminBankCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        obj = serializer.save()
        resp_serializer = AdminBankDetailSerializer(instance=obj)
        return Response(resp_serializer.data, status=status.HTTP_201_CREATED)


class BankDetailAPIView(APIView):
    """
    Retrieve, update or delete a bank instance.
    """
    serializer_class = AdminBankDetailSerializer

    def get_object(self, pk):
        try:
            return Bank.objects.get(pk=pk)
        except Bank.DoesNotExist:
            raise NotFound()

    @extend_schema(tags=["Admin - Bank"], summary="Get details of a Bank",
                   description="Get the details of the bank for the given id", responses=AdminBankDetailSerializer)
    def get(self, request, pk):
        bank = self.get_object(pk)
        serializer = AdminBankDetailSerializer(bank, context=self.get_renderer_context())
        return Response(serializer.data)

    @extend_schema(tags=["Admin - Bank"], summary="Update a Bank",
                   description="Update bank details", request=AdminBankCreateSerializer, responses=AdminBankDetailSerializer)
    def put(self, request, pk):
        bank = self.get_object(pk)
        serializer = AdminBankCreateSerializer(bank, data=request.data, context=self.get_renderer_context())
        if serializer.is_valid():
            obj = serializer.save()
            resp_serializer = AdminBankDetailSerializer(instance=obj)
            return Response(resp_serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @extend_schema(tags=["Admin - Bank"], summary="Delete Bank", description="Delete a bank")
    def delete(self, request, pk):
        bank = self.get_object(pk)
        try:
            bank.delete()
        except ProtectedError:
            return ProtectedInstanceResponse(instance=bank)
        return Response(status=status.HTTP_204_NO_CONTENT)
