import django_filters

from rest_framework.exceptions import NotFound
from drf_spectacular.utils import extend_schema

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import ListAPIView, CreateAPIView

from bank.filters.admin import RateFilter
from bank.models import Rate
from bank.serializers.admin import RateCreateSerializer, RateDetailSerializer
from django.db.models.deletion import ProtectedError
from common.responses import ProtectedInstanceResponse


@extend_schema(tags=["Admin - Bank - Rate"], summary="Get a paginated/filtered list of all Rates", responses=RateDetailSerializer)
class RateListAPIView(ListAPIView):
    """
    A paginated list of rates
    """
    serializer_class = RateDetailSerializer
    filterset_class = RateFilter
    queryset = Rate.objects.all()
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]


@extend_schema(tags=["Admin - Bank - Rate"], summary="Create a new Rate", request=RateCreateSerializer, responses=RateDetailSerializer)
class RateCreateAPIView(CreateAPIView):
    """
    Create a new rate.
    """
    serializer_class = RateCreateSerializer

    def create(self, request, *args, **kwargs):
        """
        Overriding the create method to modify the response serializer.
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        res_serializer = RateDetailSerializer(serializer.instance)
        headers = self.get_success_headers(res_serializer.data)
        return Response(res_serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class RateDetailAPIView(APIView):
    serializer_class = RateDetailSerializer

    def get_object(self, pk):
        try:
            return Rate.objects.get(pk=pk)
        except Rate.DoesNotExist:
            raise NotFound()

    @extend_schema(tags=["Admin - Bank - Rate"], summary="Get Rate details", responses=RateDetailSerializer)
    def get(self, request, pk):
        """
        Get the details of a rate
        """
        rate = self.get_object(pk)
        serializer = RateDetailSerializer(rate)
        return Response(serializer.data)

    @extend_schema(tags=["Admin - Bank - Rate"], summary="Update a Rate", request=RateCreateSerializer, responses=RateDetailSerializer)
    def put(self, request, pk):
        """
        Update a rate.
        """
        rate = self.get_object(pk)
        serializer = RateCreateSerializer(rate, data=request.data, context=self.get_renderer_context())
        if serializer.is_valid():
            instance = serializer.save()
            resp_serializer = RateDetailSerializer(instance=instance)
            return Response(resp_serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @extend_schema(tags=["Admin - Bank - Rate"], summary="Delete Rate")
    def delete(self, request, pk):
        """
        Delete a rate
        """
        rate = self.get_object(pk)
        try:
            rate.delete()
        except ProtectedError:
            return ProtectedInstanceResponse(instance=rate)
        return Response(status=status.HTTP_204_NO_CONTENT)
