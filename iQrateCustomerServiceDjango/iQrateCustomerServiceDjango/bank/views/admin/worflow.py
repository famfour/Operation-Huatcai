from drf_spectacular.utils import extend_schema

from bank.models import SubmissionMethod
from rest_framework.generics import ListAPIView
from bank.serializers.admin import WorkFlowSerializer


@extend_schema(tags=["Admin - Bank - Worflows"], summary="Get a list of WorkFlows")
class WorkFlowListAPIView(ListAPIView):
    """
    Get a list of submission workflows.
    """
    pagination_class = None
    serializer_class = WorkFlowSerializer
    queryset = SubmissionMethod.objects.all()
