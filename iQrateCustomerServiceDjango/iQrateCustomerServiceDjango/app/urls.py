"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib.staticfiles.views import serve
from django.urls import path, include
from drf_spectacular.views import SpectacularAPIView, SpectacularRedocView, SpectacularSwaggerView
import bank.urls
import common.urls
import kvstore.urls
import lawfirm.urls
import heatmap.urls
import lead.urls
import submission.urls
import payout.urls
import user.urls
from magic.urls import agent_patterns as agent_drawer
import dashboard.urls

urlpatterns = [
    path('customer-service-swagger/schema/', SpectacularAPIView.as_view(), name='schema'),
    path('customer-service-swagger/', SpectacularSwaggerView.as_view(url_name='schema'), name='swagger-ui'),

    # Rapidoc
    path('customer-service-swagger/rapidoc/', include((common.urls.rapidoc_patterns, "rapidoc"), namespace="common")),

    # Magic Links
    path('magic/', include('magic.urls', namespace="magic")),

    # Agent drawer
    path("customer/drawer/", include((agent_drawer, "drawer"), namespace="customer_drawer")),

    # Bank URLs
    path('customer/bank/', include((bank.urls.agent_patterns, "banks"), namespace="customer_bank")),
    path('customer-admin/bank/', include((bank.urls.admin_patterns, "banks"), namespace="admin_bank")),
    # KVStore
    path('customer/kvstore/', include((kvstore.urls.agent_patterns, "kvstore"), namespace="customer_kvstore")),
    path('customer-admin/kvstore/', include((kvstore.urls.admin_patterns, "kvstore"), namespace="admin_kvstore")),
    # lawfirm
    path('customer/lawfirm/', include((lawfirm.urls.agent_patterns, "lawfirm"), namespace="customer_lawfirm")),
    path('customer-admin/lawfirm/', include((lawfirm.urls.admin_patterns, "lawfirm"), namespace="admin_lawfirm")),

    # Lead
    path('customer/lead/', include((lead.urls.agent_patterns, "lead"), namespace="customer_lead")),
    path('customer-admin/lead/', include((lead.urls.admin_patterns, "lead"), namespace="admin_lead")),

    path('customer-admin/common/', include('common.urls', namespace="common")),

    path('static/<path:path>', serve),

    # Heat Map
    path('customer/heatmap/', include((heatmap.urls.agent_patterns, "heatmap"), namespace="customer_heatmap")),

    # Submission.
    path('customer/submission/', include((submission.urls.agent_patterns, "submission"), namespace="customer_submission")),

    # Payout
    path('customer/payout/', include((payout.urls.agent_patterns, "payout"), namespace="customer_payout")),
    path('customer-admin/payout/', include((payout.urls.admin_patterns, "payout"), namespace="admin_payout")),

    # Users
    path('customer-admin/users/', include((user.urls.admin_patterns, "user"), namespace="admin_user")),

    # Dashboard
    path('customer/dashboard/', include((dashboard.urls.agent_patterns, "dashboard"), namespace="customer_dashboard")),
    path('customer-admin/dashboard/', include((dashboard.urls.admin_patterns, "dashboard"), namespace="admin_dashboard")),
]
