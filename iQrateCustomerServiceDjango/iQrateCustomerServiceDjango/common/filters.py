from django_filters.filters import BaseInFilter, NumberFilter, BaseCSVFilter, CharFilter


class NumberInFilter(BaseInFilter, NumberFilter):
    """
    Filter for comma separated numbers
    """
    pass


class CharInFilter(BaseInFilter, CharFilter):
    """
    Filter for comma separated strings
    """
    pass
