from twilio.rest import Client
from django.conf import settings


def send_sms(phone, message):
    client = Client(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN)
    client.messages.create(
        to=phone,
        from_=settings.TWILIO_PHONE_NUMBER,
        body=message
    )
