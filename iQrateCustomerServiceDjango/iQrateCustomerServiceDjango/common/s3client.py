import boto3
import botocore
from botocore.awsrequest import create_request_object, AWSRequest
from botocore.config import Config
from django.conf import settings


class S3Client:
    """
    Common class to upload, delete and create pre signed URLs from S3
    Initialize with bucket name
    """

    def __init__(self, bucket_name):
        self.client = boto3.client(
            "s3",
            endpoint_url=settings.AWS_ENDPOINT_URL,
            region_name=settings.AWS_S3_REGION_NAME,
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY
        )
        self.bucket = bucket_name

    def upload_file(self, file_key, file_obj, content_type=None, attachment=False):
        """
        Common endpoint to upload file to S3
        If the content type is not set, the response content type will be binary/octet-stream & -
        the file will be downloaded to the browser rather than displaying in the browser.
        Keyword arguments:
            file_key -- Location in the bucket to which the file is uploaded including filename
            file_obj -- File object to upload should be either bytes or BufferedReader
            content_type -- Content type of the file.
        """
        params = dict(
            Bucket=self.bucket,
            Key=file_key,
            Body=file_obj,
            ContentType=content_type if content_type else "binary/octet-stream",
            ContentDisposition="attachment" if attachment else "inline"
        )

        try:
            res = self.client.put_object(**params)
            return res['ResponseMetadata']['HTTPStatusCode'] == 200
        except Exception as e:
            print(e)
            return False

    def get_presigned_url(self, file_key, expire_in=300, content_disposition=None):
        """
        Create a presigned url to access private resources.
        Keyword arguments:
            file_key -- The file key for which the url is generated
            expire_in -- expiry in seconds for the presigned URL, default is 5 mins
            content_disposition -- content dispostition headers for the file
        """
        params = {
            'Bucket': self.bucket,
            'Key': file_key
        }
        if content_disposition:
            params.update({
                "ResponseContentDisposition": content_disposition
            })
        return self.client.generate_presigned_url(
            'get_object',
            Params=params,
            ExpiresIn=expire_in,
            HttpMethod="GET"
        )

    def get_direct_url(self, file_key):
        """
        Create direct url for the public objects.
        """
        params = {"Bucket": self.bucket, "Key": file_key}

        return boto3.client(
            "s3", config=Config(signature_version=botocore.UNSIGNED)
        ).generate_presigned_url("get_object", ExpiresIn=0, Params=params)

    def delete_object(self, file_key):
        """
        Delete an object from the bucket.
        Keyword arguments:
            bucket -- Bucket from which the file is deleted.
            file_key -- THe file to be deleted
        """
        try:
            res = self.client.delete_object(
                Bucket=self.bucket, Key=file_key
            )
            return res['ResponseMetadata']['HTTPStatusCode'] == 204
        except:
            return False

    def download_object(self, file_key, path):
        """
        Download the file to local file system.
        """
        try:
            self.client.download_fileobj(self.bucket, file_key, path)
            return True
        except Exception:
            return False
