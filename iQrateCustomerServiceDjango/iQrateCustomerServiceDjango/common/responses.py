from rest_framework.response import Response
from rest_framework.status import HTTP_423_LOCKED


class ProtectedInstanceResponse(Response):
    """
    Custom error message for model instances with children
    """

    def __init__(self, data=None, status=None, template_name=None, headers=None, exception=False, content_type=None, instance=None):
        super(ProtectedInstanceResponse, self).__init__(
            data, status, template_name, headers, exception, content_type)
        self.status_code = HTTP_423_LOCKED
        # Format the message from instance model name and string representation of the instance.
        self.data = dict(
            detail=f"{instance.__class__.__name__} - {str(instance)} can't be deleted as it is connected to other records"
        )
