from django.urls import path
from . import views

app_name = "common"

urlpatterns = [
    path('file-upload', views.FileUploadView.as_view(), name="upload_file"),
    path('upload', views.ImageUploadView.as_view(), name="upload_image"),
]

rapidoc_patterns = [
    path("", views.RapidocView.as_view(), name="rapidoc")
]
