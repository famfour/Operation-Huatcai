from django.core.validators import FileExtensionValidator
from rest_framework import serializers


class CommonModelSerializer(serializers.ModelSerializer):
    def save(self, **kwargs):
        try:
            user = self.context['request'].user
            # Set the created by and updated by columns.
            if self.instance:
                self.validated_data.update(dict(
                    updated_by=user.id
                ))
            else:
                self.validated_data.update(dict(
                    created_by=user.id
                ))
        except Exception as e:
            pass
        return super(CommonModelSerializer, self).save(**kwargs)


class ImageCreateSerializer(serializers.Serializer):
    image = serializers.ImageField(required=True)

    class Meta:
        fields = ['image']


class ImageResponseSerializer(serializers.Serializer):
    url = serializers.URLField()

    class Meta:
        fields = ['url']


class FileCreateSerializer(serializers.Serializer):
    """
    Only pdf files are allowed.
    """
    file = serializers.FileField(validators=[FileExtensionValidator(allowed_extensions=["pdf"])])

    class Meta:
        fields = ['file']


class FileResponseSerializer(serializers.Serializer):
    url = serializers.URLField()

    class Meta:
        fields = ['url']


class SuccessResponseSerializer(serializers.Serializer):
    message = serializers.CharField(read_only=True)


class SuccessDetailSerializer(serializers.Serializer):
    detail = serializers.CharField(read_only=True)