from django.db import models


class ModelMixin(models.Model):
    created_by = models.UUIDField(null=True, help_text="User ID of the agent or admin who created this record.")
    updated_by = models.UUIDField(null=True, help_text="User ID of the agent or admin who last updated this record.")
    created = models.DateTimeField(auto_now_add=True, help_text="Date and time at which this record is created")
    updated = models.DateTimeField(auto_now=True, help_text="Date and time at which this record is last updated.")

    class Meta:
        abstract = True
