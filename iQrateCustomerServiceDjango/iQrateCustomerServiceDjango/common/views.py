import uuid

from django.conf import settings
from django.shortcuts import render
from django.views import View
from django.views.generic import TemplateView
from drf_spectacular.utils import extend_schema
from rest_framework import status
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from rest_framework.views import APIView

from common.s3client import S3Client
from .serializers import ImageCreateSerializer, ImageResponseSerializer, FileResponseSerializer, FileCreateSerializer


class IndexView(TemplateView):
    template_name = "common/common.html"


class RapidocView(View):

    def get(self, request, *args, **kwargs):
        return render(template_name="common/rapidoc.html", request=self.request)


class ImageUploadView(APIView):
    """
    This view is for uploading the images to the server such as logo and other non sensitive files.  \n
    Post image/file to this endpoint with -
    'ContentType': 'multipart/form-data' & image
    It will return the s3 url upon successful upload
    """
    parser_classes = [MultiPartParser, FormParser]
    serializer_class = ImageCreateSerializer

    @extend_schema(tags=["Common / Upload"], summary="Upload non-sensitive images", request=ImageCreateSerializer, responses=ImageResponseSerializer)
    def post(self, request):
        serializer = ImageCreateSerializer(data=request.data)
        if serializer.is_valid():
            # get the uploaded file
            file = serializer.validated_data["image"]
            ext = file.name.split(".").pop()
            filename = f"cs/{str(uuid.uuid4())}.{ext}"

            client = S3Client(settings.AWS_STORAGE_BUCKET_NAME)
            uploaded = client.upload_file(
                file_key=filename,
                file_obj=file,
                content_type=file.content_type
            )
            if uploaded:
                return Response(status=status.HTTP_201_CREATED, data=dict(
                    url=f"https://{settings.AWS_STORAGE_BUCKET_NAME}.s3.{settings.AWS_S3_REGION_NAME}.amazonaws.com/{filename}"
                ))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data=dict(
                error='Error with uploading the file'
            ))
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class FileUploadView(APIView):
    """
    This view is for uploading publicly accessible files to S3  \n
    Post file to this endpoint with -
    'ContentType': 'multipart/form-data' & file
    It will return the s3 url upon successful upload.
    Only PDF files are allowed.
    """
    parser_classes = [MultiPartParser, FormParser]
    serializer_class = FileCreateSerializer

    @extend_schema(tags=["Common / Upload"], summary="Upload non-sensitive files", request=FileCreateSerializer, responses=FileResponseSerializer)
    def post(self, request):
        serializer = FileCreateSerializer(data=request.data)
        if serializer.is_valid():
            # get the uploaded file
            file = serializer.validated_data["file"]
            ext = file.name.split(".").pop()
            filename = f"files/{str(uuid.uuid4())}.{ext}"

            client = S3Client(settings.AWS_STORAGE_BUCKET_NAME)
            uploaded = client.upload_file(
                file_key=filename,
                file_obj=file,
                content_type=file.content_type,
                attachment=True
            )
            if uploaded:
                return Response(status=status.HTTP_201_CREATED, data=dict(
                    url=f"https://{settings.AWS_STORAGE_BUCKET_NAME}.s3.{settings.AWS_S3_REGION_NAME}.amazonaws.com/{filename}"
                ))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data=dict(
                detail='Error with uploading the file'
            ))
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
