from django.conf import settings


class HackMiddleWare:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        """
        This middlware is here because Kong is a cunt and does not forward headers.
        """
        if not settings.DEBUG:
            request.META['HTTP_X_FORWARDED_PROTO'] = "https"
        response = self.get_response(request)

        return response
