from django.core.signing import TimestampSigner, SignatureExpired, BadSignature
from drf_spectacular.extensions import OpenApiAuthenticationExtension
from rest_framework.authentication import TokenAuthentication
import jwt
from uuid import UUID
from django.conf import settings
from rest_framework.exceptions import AuthenticationFailed


class User():
    """
    This is just a mock class as there is no local user.
    """
    is_authenticated = True

    def __init__(self, id, username, usertype, membership_type):
        self.id = UUID(id, version=4)
        self.usertype = usertype
        self.username = username
        self.membership_type = membership_type

    @property
    def pk(self):
        return self.id

    def get_username(self):
        return self.username

    @property
    def email(self):
        return self.username


class DocUser():
    """
    Mock user class to satisfy the client login for document drawer
    """
    is_authenticated = True

    def __init__(self, id):
        self.id = UUID(id, version=4)
        self.usertype = id
        self.username = id
        self.membership_type = id

    @property
    def pk(self):
        return self.id

    def get_username(self):
        return self.username

    @property
    def email(self):
        return self.username


class CustomTokenAuthentication(TokenAuthentication):
    """
    Custom token auth backend without creating a local user in database.
    Get the data from the JWT token, not verifying the token as it passes -
    though the auth before reaching the customer service app.
    """
    keyword = 'Bearer'
    model = None

    def authenticate_credentials(self, key):
        # decode JWT.
        customer = jwt.decode(key, options={"verify_signature": False})

        # cast the user.
        user = User(
            id=customer.get("sub"),
            username=customer.get("preferred_username"),
            usertype=customer.get("azp"),
            membership_type=customer.get("membershiptype")
        )
        return user, key


class DocumentDrawerAuthentication(TokenAuthentication):
    """
    Custom authentication backend for document drawer.
    """
    keyword = settings.DRAWER_TOKEN_NAME
    model = None

    def authenticate(self, request):
        token = request.META.get(f"HTTP_{self.keyword.upper()}", None)
        if not token:
            raise AuthenticationFailed(f"{self.keyword} is missing in the header")
        return self.authenticate_credentials(token)

    def authenticate_credentials(self, token):
        signer = TimestampSigner()
        try:
            # Validate the token
            decoded = signer.unsign(token, max_age=settings.DRAWER_TOKEN_EXPIRY)
        except SignatureExpired:
            raise AuthenticationFailed("Token expired. Please login again")
        except BadSignature:
            raise AuthenticationFailed("Invalid token, please login again")
        user = DocUser(id=decoded)
        return user, token


class DocumentDrawerAuthenticationScheme(OpenApiAuthenticationExtension):
    target_class = 'common.authentication.DocumentDrawerAuthentication'  # full import path OR class ref
    name = 'DocumentDrawerAuthentication'  # name used in the schema

    def get_security_definition(self, auto_schema):
        return {
            'type': 'apiKey',
            'in': 'header',
            'name': settings.DRAWER_TOKEN_NAME,
        }
