from rest_framework import serializers

from common.serializers import CommonModelSerializer
from heatmap.models import HeatMap
from lead.models import Lead
from lead.validators import ValidateLeadOwnership
from lead.serializers.agent import LeadDetailSerializer


class HeatMapCreateSerializer(CommonModelSerializer):
    lead = serializers.PrimaryKeyRelatedField(
        queryset=Lead.objects.all(),
        validators=[ValidateLeadOwnership()]
    )

    class Meta:
        model = HeatMap
        fields = ['id', 'lead', 'heatmap', 'status', 'comments', 'follow_up_date']


class HeatMapResponseSerializer(serializers.ModelSerializer):
    class Meta:
        model = HeatMap
        fields = ['id', 'heatmap', 'status', 'lead', 'comments',
                  'follow_up_date', 'created_by', 'updated_by', 'created', 'updated']


class CalendarResponseSerializer(serializers.ModelSerializer):
    date = serializers.DateField(source='follow_up_date')
    event = serializers.CharField(allow_null=True, source='status')
    event_type = serializers.CharField(source='heatmap')
    lead_id = serializers.IntegerField()

    class Meta:
        model = HeatMap
        fields = [
            'date',
            'event',
            'event_type',
            'lead_id'
        ]
