from common.models import ModelMixin
from lead.models import *


class HeatMap(ModelMixin):
    heatmap_value = (
        ('New', 'New'),
        ('Hot', 'Hot'),
        ('Warm', 'Warm'),
        ('Cool', 'Cool'),
        ('Lost', 'Lost'),
        ('Won', 'Won'),
        ('Rework', 'Rework'),
        ('BirthDay', 'BirthDay'),
    )

    heatmap = models.CharField(max_length=200, choices=heatmap_value)
    status = models.CharField(max_length=200)
    lead = models.ForeignKey(Lead, on_delete=models.CASCADE, related_name="heatmaps")
    # lead_id = models.ForeignKey(Lead,on_delete=models.CASCADE)
    comments = models.TextField(null=True, blank=True)
    follow_up_date = models.DateField()

    class Meta:
        ordering = ['-created'] 

    def __str__(self):
        return self.status
