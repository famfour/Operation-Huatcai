from django.db.models.signals import post_save
from django.dispatch import receiver
from lead.models import Lead
from .models import HeatMap
from django.utils import timezone


@receiver(post_save, sender=Lead)
def post_save_create_heatmap(sender, instance, created, **kwargs):
    if created:
        HeatMap.objects.create(lead=instance, heatmap='New', status='New Lead Created', follow_up_date=timezone.now(), created_by=instance.created_by)
