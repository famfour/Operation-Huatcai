from django.urls import path
from heatmap.views import agent

app_name = "heatmap"

agent_patterns = [
    path('<int:pk>', agent.HeatMapDetailAPIView.as_view(), name="heatmap_rud"),
    path('list/<int:lead_id>', agent.HeatMapListAPIView.as_view(), name="heatmaps"),
    path('create', agent.HeatMapCreateAPIView.as_view(), name="heatmaps_create"),
    path('calendar/<int:year>/<int:month>', agent.CalendarListAPIView.as_view(), name="calendar_data"),
]
