from rest_framework.exceptions import NotFound
from drf_spectacular.utils import extend_schema

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import ListAPIView, CreateAPIView

from heatmap.models import HeatMap
from heatmap.serializers.agent import HeatMapResponseSerializer, HeatMapCreateSerializer
from django.db.models.deletion import ProtectedError
from common.responses import ProtectedInstanceResponse
from django.db.models import Q


@extend_schema(tags=["Agent - HeatMap"], summary="Get a paginated/filtered list of all Heatmap", responses=HeatMapResponseSerializer)
class HeatMapListAPIView(ListAPIView):
    """
    Paginated list of heatmap
    """
    serializer_class = HeatMapResponseSerializer

    def get_queryset(self):
        user_id = self.request.user.id
        return HeatMap.objects.filter(
            Q(lead__agent_applied=user_id) | Q(lead__co_broke_agent=user_id)
        ).filter(lead_id=self.kwargs.get("lead_id")).all()


@extend_schema(tags=["Agent - HeatMap"], summary="Create a new HeatMap", request=HeatMapCreateSerializer)
class HeatMapCreateAPIView(CreateAPIView):
    """
    Create a new Heatmap.
    """
    serializer_class = HeatMapCreateSerializer


class HeatMapDetailAPIView(APIView):
    """
    Retrieve, update or delete a HeatMap instance.
    """
    serializer_class = HeatMapResponseSerializer

    def get_object(self, pk):
        user_id = self.request.user.id
        try:
            return HeatMap.objects.filter(
                Q(lead__agent_applied=user_id) | Q(lead__co_broke_agent=user_id)
            ).get(pk=pk)
        except HeatMap.DoesNotExist:
            raise NotFound()

    @extend_schema(tags=["Agent - HeatMap"], summary="Get HeatMap details")
    def get(self, request, pk):
        """
        Get the details of a heatmap with a given 'id'
        """
        heatmap = self.get_object(pk)
        serializer = HeatMapResponseSerializer(heatmap)
        return Response(serializer.data)

    @extend_schema(tags=["Agent - HeatMap"], summary="Update a Heatmap", request=HeatMapCreateSerializer, responses=HeatMapResponseSerializer)
    def put(self, request, pk):
        """
        Update a heatmap entry
        """
        heatmap = self.get_object(pk)
        serializer = HeatMapCreateSerializer(heatmap, data=request.data, context=self.get_renderer_context())
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @extend_schema(tags=["Agent - HeatMap"], summary="Delete a HeatMap")
    def delete(self, request, pk):
        """
        Delete a HeatMap.
        """
        heatmap = self.get_object(pk)
        try:
            heatmap.delete()
        except ProtectedError:
            return ProtectedInstanceResponse(instance=heatmap)
        return Response(status=status.HTTP_204_NO_CONTENT)
