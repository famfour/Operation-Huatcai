from rest_framework.exceptions import NotFound
from drf_spectacular.utils import extend_schema

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import ListAPIView, CreateAPIView

from heatmap.models import HeatMap
from heatmap.serializers.agent import CalendarResponseSerializer
from django.db.models.deletion import ProtectedError
from common.responses import ProtectedInstanceResponse
from django.db.models import Q


@extend_schema(tags=["Agent - Calendar"], summary="Calendar Data Endpoint",
               responses=CalendarResponseSerializer)
class CalendarListAPIView(ListAPIView):
    """
    Calendar Data
    """
    serializer_class = CalendarResponseSerializer
    pagination_class = None

    def get_queryset(self):
        return HeatMap.objects.filter(
            Q(lead__agent_applied=self.request.user.id, lead__co_broke_agent__isnull=True) | Q(lead__co_broke_agent=self.request.user.id)
        ).filter(
            follow_up_date__year=self.kwargs.get('year'), follow_up_date__month=self.kwargs.get('month')
        ).all()
