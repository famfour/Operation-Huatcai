# Generated by Django 4.1 on 2022-10-16 18:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("heatmap", "0002_alter_heatmap_options"),
    ]

    operations = [
        migrations.AlterField(
            model_name="heatmap",
            name="heatmap",
            field=models.CharField(
                choices=[
                    ("New", "New"),
                    ("Hot", "Hot"),
                    ("Warm", "Warm"),
                    ("Cool", "Cool"),
                    ("Lost", "Lost"),
                    ("Won", "Won"),
                    ("Rework", "Rework"),
                    ("BirthDay", "BirthDay"),
                ],
                max_length=200,
            ),
        ),
    ]
