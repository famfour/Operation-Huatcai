from uuid import uuid4

from django.conf import settings
from django.db import IntegrityError
from django.db import models
from django.utils import timezone

from common.models import ModelMixin
from common.s3client import S3Client
from lead.models import Client
from lead.models import Lead
from .helpers import random_code
from .queryset import DocumentDrawerQuerySet


class PDPAVerification(ModelMixin):
    """
    This model stores PDPA verification token.
    Only one record per client and update the code on retry.
    Only 1 retry per minute.
    """
    client = models.OneToOneField(Client, related_name='verification_code', on_delete=models.CASCADE)
    code = models.CharField(max_length=16, default=random_code, unique=True)
    used = models.BooleanField(default=False)
    retry_by = models.DateTimeField(default=timezone.now)

    class Meta:
        ordering = ["-created"]

    def __str__(self):
        return self.code

    @staticmethod
    def get_unique_code():
        code = random_code(length=6)
        if PDPAVerification.objects.filter(code=code).exists():
            PDPAVerification.get_unique_code()
        return code


class DocumentDrawer(models.Model):
    id = models.UUIDField(default=uuid4, primary_key=True, editable=False, unique=True, db_index=True)
    lead = models.OneToOneField(Lead, on_delete=models.CASCADE, related_name="document_drawer")
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    objects = DocumentDrawerQuerySet.as_manager()

    def __str__(self):
        return str(self.id)


class DocumentTypes(models.TextChoices):
    BANK_FORMS = "Bank Forms"
    NRIC = "NRIC"
    OPTION_TO_PURCHASE = "Option to Purchase"
    IRAS_STATEMENTS = "IRAS Statements"
    CPF_STATEMENTS = "CPF Statements"
    HDB_STATEMENTS = "HDB Statements"
    PAYSLIPS = "Payslips"
    VALUATION_REPORT = "Valuation Reports"
    OUTSTANDING_LOAN_STATEMENTS = "Outstanding Loan Statements"
    TENANCY_AGREEMENT = "Tenancy Agreement"
    OTHERS = "Others"
    LETTER_OF_OFFER = "Letter Of Offer"
    CALC_REPORTS = "Calculator Reports"


class Document(models.Model):
    id = models.UUIDField(default=uuid4, primary_key=True, unique=True, db_index=True, editable=False)
    drawer = models.ForeignKey(DocumentDrawer, on_delete=models.CASCADE, related_name="documents")
    filename = models.CharField(max_length=128, help_text="Name of the file.")
    doc_type = models.CharField(max_length=64, choices=DocumentTypes.choices, help_text="Type of document")
    extension = models.CharField(max_length=32, null=True)
    file_key = models.CharField(max_length=254, help_text="Location inside the S3 bucket.")
    created_by = models.UUIDField(
        null=True,
        help_text="NULL usually, agent id if the agent uploads the document.")
    updated_by = models.UUIDField(
        null=True,
        help_text="NULL usually, agent id if the agent updated the document."
    )
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.filename

    def s3_link(self, expiry=300):
        """
        Get the pre signed S3 URL for the document.
        """
        client = S3Client(settings.AWS_PRIVATE_BUCKET_NAME)
        return client.get_presigned_url(
            self.file_key,
            expire_in=expiry,
            content_disposition=f"attachment;filename={self.filename}.{self.extension}")

    def delete(self, using=None, keep_parents=False):
        """
        Delete the file from the bucket when the DB entry is deleted.
        """
        client = S3Client(settings.AWS_PRIVATE_BUCKET_NAME)
        client.delete_object(self.file_key)
        super(Document, self).delete(using, keep_parents)


class DocumentOTP(models.Model):
    drawer = models.ForeignKey(DocumentDrawer, related_name="otps", on_delete=models.CASCADE)
    code = models.CharField(max_length=16, unique=True, editable=False, null=True)
    used = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.code

    @staticmethod
    def create_new_otp(drawer):
        """
        Create a new instance.
        """
        try:
            return DocumentOTP.objects.create(drawer=drawer, code=random_code(length=6))
        except IntegrityError:
            # In a rare case in which the code already exists in the table.
            DocumentOTP.create_new_otp(drawer)
