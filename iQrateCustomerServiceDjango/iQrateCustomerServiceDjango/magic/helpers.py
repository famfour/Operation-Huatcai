import random
import string


def random_code(length: int = 7):
    ret = []
    count = 0
    alnums = string.ascii_uppercase + string.digits
    while count < length:
        ret.append(random.choice(alnums))
        count += 1
    return "".join(ret)
