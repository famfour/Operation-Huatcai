from django.urls import path
from .views import open, agent

app_name = "magic"

urlpatterns = [
    # Token
    path("validate_pdpa", open.VerifyTokenAPIView.as_view(), name="validate_pdpa"),
    path("drawer/get-otp", open.GetOTPView.as_view(), name="get_drawer_otp"),
    path("drawer/validate-otp", open.LoginToDrawerView.as_view(), name="login_to_drawer"),
    path("drawer/<uuid:id>", open.DocumentDrawerView.as_view(), name="drawer"),
    path("drawer/upload", open.DocumentUploadView.as_view(), name="upload_file"),
    path("drawer/update-file/<uuid:id>", open.DocumentUpdateView.as_view(), name="update_file"),
    path("drawer/delete-file/<uuid:id>", open.DocumentDeleteView.as_view(), name="delete_file"),
    path("drawer/download-file/<uuid:id>", open.DocumentDownloadView.as_view(), name="download_file"),
]

agent_patterns = [
    path("<uuid:id>", agent.DocumentDrawerView.as_view(), name="drawer"),
    path("upload", agent.DocumentUploadView.as_view(), name="upload_file"),
    path("update-file/<uuid:id>", agent.DocumentUpdateView.as_view(), name="update_file"),
    path("delete-file/<uuid:id>", agent.DocumentDeleteView.as_view(), name="delete_file"),
    path("download-file/<uuid:id>", agent.DocumentDownloadView.as_view(), name="download_file"),
]
