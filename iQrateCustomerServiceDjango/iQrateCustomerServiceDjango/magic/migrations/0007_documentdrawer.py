# Generated by Django 4.0.4 on 2022-05-09 07:37

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('lead', '0026_alter_approvedpackage_created_and_more'),
        ('magic', '0006_documentotp'),
    ]

    operations = [
        migrations.CreateModel(
            name='DocumentDrawer',
            fields=[
                ('id', models.UUIDField(db_index=True, default=uuid.uuid4, editable=False, primary_key=True, serialize=False, unique=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('lead', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='document_drawer', to='lead.lead')),
            ],
        ),
    ]
