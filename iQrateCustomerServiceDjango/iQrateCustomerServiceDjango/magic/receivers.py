from django.db.models.signals import post_save
from lead.models import Lead
from magic.models import DocumentDrawer


def create_document_drawer(sender, instance, created, *args, **kwargs):
    """
    Create a document drawer when the lead is created.
    """
    if not created:
        return
    DocumentDrawer.objects.create(
        lead=instance
    )


post_save.connect(create_document_drawer, sender=Lead)
