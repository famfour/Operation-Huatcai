from django.conf import settings
from django.core.validators import FileExtensionValidator
from drf_spectacular.utils import extend_schema_field
from rest_framework import serializers
from rest_framework.exceptions import PermissionDenied

from common.s3client import S3Client
from magic.models import Document, DocumentDrawer, DocumentTypes


class AgentDocumentHyperLinkedSerializer(serializers.HyperlinkedModelSerializer):
    """
    Schema for document drawer view.
    """

    class Meta:
        model = Document
        fields = ['id', 'filename', 'doc_type', 'created', 'updated']


class AgentDocumentModelSerializer(serializers.ModelSerializer):
    """
    Schema for Document Details.
    """

    class Meta:
        model = Document
        fields = ['id', 'filename', 'doc_type', 'created', 'updated']


class AgentDocumentCreateSerializer(serializers.Serializer):
    """
    Schema for uploading documents to the drawer.
    """
    file = serializers.FileField(
        validators=[FileExtensionValidator(allowed_extensions=["pdf", "png", "jpg", "jpeg", "git", "tiff"])]
    )
    drawer = serializers.PrimaryKeyRelatedField(queryset=DocumentDrawer.objects.all())
    doc_type = serializers.ChoiceField(choices=DocumentTypes.choices)
    filename = serializers.CharField(max_length=128)

    def validate_drawer(self, value):
        request = self.context['request']

        # Make sure the agent has access to the lead.
        if not DocumentDrawer.objects.filter(pk=value.pk).accessible_for(user=request.user).exists():
            raise PermissionDenied(detail="Unauthorized access to drawer.")

        return value

    class Meta:
        fields = ['file', 'doc_type', 'filename']


class AgentDocumentUpdateSerializer(serializers.Serializer):
    """
    Schema for updating document.
    """
    filename = serializers.CharField(max_length=128)

    class Meta:
        fields = ['filename']


class AgentDocumentDownloadSerializer(serializers.ModelSerializer):
    """
    Schema for the document download link.
    """
    url = serializers.SerializerMethodField(read_only=True)
    filename = serializers.SerializerMethodField(read_only=True)

    @extend_schema_field(field=serializers.URLField)
    def get_url(self, obj):
        client = S3Client(bucket_name=settings.AWS_PRIVATE_BUCKET_NAME)
        return client.get_presigned_url(
            obj.file_key,
            expire_in=300,
            content_disposition=f"attachment;filename={obj.filename}.{obj.extension}",
        )

    @extend_schema_field(serializers.CharField)
    def get_filename(self, obj):
        return f"{obj.filename}.{obj.extension}"

    class Meta:
        model = Document
        fields = ["id", "url", "filename"]


class AgentDocumentDrawerDetailSerializer(serializers.ModelSerializer):
    documents = AgentDocumentHyperLinkedSerializer(many=True, read_only=True)

    class Meta:
        model = DocumentDrawer
        fields = ['id', 'documents']
