from django.conf import settings
from django.core.validators import FileExtensionValidator
from django.db.models import Q
from drf_spectacular.utils import extend_schema_field
from rest_framework import serializers

from common.s3client import S3Client
from magic.models import Document, DocumentOTP, DocumentDrawer, DocumentTypes


class OTPCreateSerializer(serializers.Serializer):
    """
    Schema OTP request by client.
    """
    drawer = serializers.UUIDField()

    class Meta:
        fields = ['drawer']


class DrawerLoginSerializer(serializers.Serializer):
    """
    Request schema for logging into document drawer with OTP and drawer ID
    """
    drawer = serializers.UUIDField()
    otp = serializers.CharField(max_length=6)

    def validate_drawer(self, value):
        """
        Make sure there is a drawer with the UUID exists
        """
        if not DocumentDrawer.objects.filter(pk=value).exists():
            raise serializers.ValidationError("Document drawer does not exist")
        return value

    class Meta:
        fields = ['drawer', 'otp']


class DrawerLoggedInSerializer(serializers.Serializer):
    """
    Schema for returning token once the OTP is verified.
    """
    token = serializers.CharField()

    class Meta:
        fields = ['token']


class DocumentHyperLinkedSerializer(serializers.HyperlinkedModelSerializer):
    """
    Schema for document drawer view.
    """

    class Meta:
        model = Document
        fields = ['id', 'filename', 'doc_type', 'created', 'updated']


class DocumentModelSerializer(serializers.ModelSerializer):
    """
    Schema for Document Details.
    """

    class Meta:
        model = Document
        fields = ['id', 'filename', 'doc_type', 'created', 'updated']


class DocumentCreateSerializer(serializers.Serializer):
    """
    Schema for uploading documents to the drawer.
    """
    file = serializers.FileField(
        validators=[FileExtensionValidator(allowed_extensions=["pdf", "png", "jpg", "jpeg", "git", "tiff"])]
    )
    drawer = serializers.PrimaryKeyRelatedField(queryset=DocumentDrawer.objects.all())
    doc_type = serializers.ChoiceField(choices=DocumentTypes.choices)
    filename = serializers.CharField(max_length=128)

    def validate_drawer(self, value):
        request = self.context['request']
        if not request.user.id == value.id:
            raise serializers.ValidationError("Unauthorized access to drawer.")
        return value

    class Meta:
        fields = ['file', 'doc_type', 'filename']


class DocumentUpdateSerializer(serializers.Serializer):
    """
    Schema for updating document.
    """
    filename = serializers.CharField(max_length=128)

    class Meta:
        fields = ['filename']


class DocumentDownloadSerializer(serializers.ModelSerializer):
    """
    Schema for the document download link.
    """
    url = serializers.SerializerMethodField(read_only=True)
    filename = serializers.SerializerMethodField(read_only=True)

    @extend_schema_field(field=serializers.URLField)
    def get_url(self, obj):
        client = S3Client(bucket_name=settings.AWS_PRIVATE_BUCKET_NAME)
        return client.get_presigned_url(
            obj.file_key,
            expire_in=300,
            content_disposition=f"attachment;filename={obj.filename}.{obj.extension}",)

    @extend_schema_field(serializers.CharField)
    def get_filename(self, obj):
        return f"{obj.filename}.{obj.extension}"

    class Meta:
        model = Document
        fields = ["id", "url", "filename"]


class DocumentDrawerDetailSerializer(serializers.ModelSerializer):
    documents = DocumentHyperLinkedSerializer(many=True, read_only=True)

    class Meta:
        model = DocumentDrawer
        fields = ['id', 'documents']
