from django.conf import settings
from drf_spectacular.utils import extend_schema_field
from rest_framework import serializers

from common.s3client import S3Client
from magic.models import Document, DocumentDrawer


class AdminDocumentHyperLinkedSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.SerializerMethodField(read_only=True)
    filename = serializers.SerializerMethodField(read_only=True)

    @extend_schema_field(field=serializers.URLField)
    def get_url(self, obj):
        client = S3Client(bucket_name=settings.AWS_PRIVATE_BUCKET_NAME)
        return client.get_presigned_url(
            obj.file_key,
            expire_in=900,
            content_disposition="inline",
        )

    @extend_schema_field(serializers.CharField)
    def get_filename(self, obj):
        return f"{obj.filename}.{obj.extension}"

    class Meta:
        model = Document
        fields = ['id', 'filename', 'url', 'doc_type', 'created', 'updated']


class AdminDocumentDrawerDetailSerializer(serializers.ModelSerializer):
    documents = AdminDocumentHyperLinkedSerializer(many=True, read_only=True)

    class Meta:
        model = DocumentDrawer
        fields = ['id', 'documents']
