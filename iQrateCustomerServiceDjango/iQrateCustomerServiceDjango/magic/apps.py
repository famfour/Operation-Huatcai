from django.apps import AppConfig


class MagicConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'magic'

    def ready(self):
        from . import receivers
        return super(MagicConfig, self).ready()
