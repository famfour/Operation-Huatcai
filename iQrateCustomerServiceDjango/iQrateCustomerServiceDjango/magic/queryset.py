from django.db.models import QuerySet, When, Case, Value, Q, BooleanField
from kvstore.choices import LeadTypeChoices, LeadStatusChoices


class DocumentDrawerQuerySet(QuerySet):

    def annotate_can_user_access(self, user):
        """
        1. Apply for yourself full access.
        2. Co broker - full access
        3. Application Type Co-Broke and the user is the agent applied, no access
        4. Apply for others and no existing co broker - full access.
        """
        return self.annotate(
            can_user_access=Case(
                When(
                    Q(lead__co_broke_agent=user.id),
                    then=Value(True)
                ),
                When(
                    Q(lead__lead_type=LeadTypeChoices.yourself) & Q(lead__agent_applied=user.id),
                    then=Value(True)
                ),
                When(
                    Q(lead__lead_type=LeadTypeChoices.co_broke) & Q(lead__agent_applied=user.id),
                    then=Value(False) # Lead type co-broke, applied agents does not have access.
                ),
                When(
                    Q(lead__agent_applied=user.id) & Q(lead__co_broke_agent__isnull=True),
                    then=Value(True)
                ),
                default=Value(False),
                output_field=BooleanField()
            )
        )

    def accessible_for(self, user):
        return self.annotate_can_user_access(user).filter(can_user_access=True)
