from django.conf import settings
from django.core import signing
from datetime import timedelta


class Signer():

    @staticmethod
    def sign(value: str):
        return signing.dumps(obj=value, salt=settings.SIGNING_SALT)

    @staticmethod
    def validate(signature: str, expiry_in: timedelta):
        value = signing.loads(
            signature,
            salt=settings.SIGNING_SALT,
            max_age=expiry_in,
        )
        return value
