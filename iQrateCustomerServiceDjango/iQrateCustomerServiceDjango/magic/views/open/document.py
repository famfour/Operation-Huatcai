import uuid
from datetime import timedelta

from django.conf import settings
from django.core.mail import EmailMessage
from django.core.signing import TimestampSigner
from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.utils import timezone
from drf_spectacular.utils import extend_schema
from rest_framework import status
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.exceptions import ParseError, Throttled, server_error
from rest_framework.generics import RetrieveAPIView
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from rest_framework.views import APIView

from common.authentication import DocumentDrawerAuthentication
from common.s3client import S3Client
from lead.models import Client
from magic.models import DocumentOTP, Document, DocumentDrawer
from magic.serializers.open import OTPCreateSerializer, DrawerLoginSerializer, DrawerLoggedInSerializer, DocumentDrawerDetailSerializer, \
    DocumentCreateSerializer, DocumentModelSerializer, DocumentUpdateSerializer, DocumentDownloadSerializer
from common.utils import send_sms


@extend_schema(tags=["Document Drawer"], summary="Send OTP", request=OTPCreateSerializer)
@authentication_classes([])
@permission_classes([])
class GetOTPView(APIView):
    """
    This endpoint sends an OTP to the main client for logging into the
    document drawer.  \n
    PDPA verification must be completed for the main client before documents can be uploaded.  \n
    The OTP expires after 5 minutes.  \n
    """

    def post(self, request):
        serializer = OTPCreateSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        drawer_id = serializer.validated_data['drawer']
        # get the drawer
        drawer = get_object_or_404(DocumentDrawer, id=drawer_id)
        # Get the client
        client = Client.objects.filter(Q(main_applicant=True) & Q(lead=drawer.lead)).first()
        # Make Sure the clients phone number is PDPA verified before sending the OTP
        if not client.pdpa_status:
            return Response(status=status.HTTP_401_UNAUTHORIZED, data=dict(
                detail="Client needs to verify PDPA Before "
            ))
        # Return error if there is an OTP created less than a minute ago.
        existing_otp = DocumentOTP.objects.filter(
            created__gte=(timezone.localtime() - timedelta(seconds=60)), drawer=drawer).first()
        if existing_otp:
            time_left = ((existing_otp.created + timedelta(seconds=60)) - timezone.now()).seconds
            raise Throttled(detail=f"Please try after {time_left} seconds")

        # Create OTP
        otp = DocumentOTP.create_new_otp(drawer)

        # Send the SMS
        try:
            send_sms(
                phone=f"+{client.country_code}{client.phone_number}",
                message=f"Your IQRATE OTP is: {otp.code}.\nDo not share this code with anyone; our employees will never ask for the code"
            )
        except:
            return Response(data=dict(detail="Error sending SMS, Please try again"), status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        # Send The email
        try:
            mail = EmailMessage(
                from_email=settings.DEFAULT_FROM_EMAIL,
                to=[client.email],
                subject="Verify Your Email Address"
            )
            mail.template_id = settings.DOCUMENT_OTP_EMAIL_TEMPLATE
            mail.dynamic_template_data = dict(
                code=f"{otp.code}"
            )
            mail.send()
        except Exception as e:
            raise ParseError(detail="Error sending email, Please try again")  # not a parse error

        return Response(status=status.HTTP_200_OK)


@extend_schema(tags=["Document Drawer"], summary="Login with OTP", request=DrawerLoginSerializer, responses=DrawerLoggedInSerializer)
@authentication_classes([])
@permission_classes([])
class LoginToDrawerView(APIView):
    """
    Login to the document drawer with the OTP.  \n
    Response contains the token which should be included in document requests
    as DocumentDrawerToken in the header for authorization.  \n
    """

    def post(self, request):
        serializer = DrawerLoginSerializer(data=request.data, context={"request": request})
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        # Validate the OTP
        # OTP object with both drawer_id and otp exits & not used & not more than 5 minutes old
        obj = DocumentOTP.objects.filter(
            Q(drawer_id=serializer.validated_data['drawer']) &
            Q(code=serializer.validated_data["otp"]) &
            Q(used=False)
        ).first()
        # If OTP cannot be found.
        if not obj:
            return Response(data=dict(
                detail="Invalid OTP"
            ), status=status.HTTP_404_NOT_FOUND)

        # If the otp is older than 5 minutes.
        if obj.created < (timezone.now() - timedelta(minutes=5)):
            return Response(data=dict(
                detail="The OTP is expired. Please request another OTP"
            ), status=status.HTTP_400_BAD_REQUEST)

        # Set the token as used.
        obj.used = True
        obj.save()

        # Create the token using signer.
        signer = TimestampSigner()
        token = signer.sign(obj.drawer_id)
        res_serializer = DrawerLoggedInSerializer(data=dict(token=token))
        res_serializer.is_valid(raise_exception=True)
        return Response(data=res_serializer.data, status=status.HTTP_200_OK)


@extend_schema(tags=["Document Drawer"], summary="Login with OTP", responses=DocumentDrawerDetailSerializer)
@authentication_classes([DocumentDrawerAuthentication])
class DocumentDrawerView(RetrieveAPIView):
    """
    This endpoint returns a list of files
    """
    serializer_class = DocumentDrawerDetailSerializer

    def get_object(self):
        """
        Make sure the user is logged in and the logged in drawer is the one that the user is accessing.
        """
        if not self.request.user.id == self.kwargs.get("id"):
            return None
        return DocumentDrawer.objects.prefetch_related("documents").filter(pk=self.kwargs.get("id")).get()


@extend_schema(tags=["Document Drawer"], summary="Upload File", request=DocumentCreateSerializer, responses=DocumentModelSerializer)
@authentication_classes([DocumentDrawerAuthentication])
class DocumentUploadView(APIView):
    """
    Upload document.  \n
    Document formats are limited to "pdf", "png", "jpg", "jpeg", "git", "tiff".  \n

    """
    parser_classes = [MultiPartParser, FormParser]

    def post(self, request):
        serializer = DocumentCreateSerializer(data=request.data, context=dict(request=request))
        if serializer.is_valid():
            # get the uploaded file
            file = serializer.validated_data["file"]
            ext = file.name.split(".").pop()
            file_key = f"{str(serializer.validated_data['drawer'].id)}/{str(uuid.uuid4())}.{ext}"
            # Create the S3 client and upload the file.
            client = S3Client(settings.AWS_PRIVATE_BUCKET_NAME)
            uploaded = client.upload_file(
                file_key=file_key,
                file_obj=file,
                content_type=file.content_type
            )
            if uploaded:
                # Create the record in the database.
                obj = Document.objects.create(
                    drawer=serializer.validated_data['drawer'],
                    filename=serializer.validated_data['filename'],
                    doc_type=serializer.validated_data['doc_type'],
                    extension=ext,
                    file_key=file_key
                )
                serializer = DocumentModelSerializer(instance=obj)
                return Response(status=status.HTTP_201_CREATED, data=serializer.data)
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data=dict(
                error='Error with uploading the file'
            ))
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@extend_schema(tags=["Document Drawer"], summary="Update File", request=DocumentUpdateSerializer, responses=DocumentModelSerializer)
@authentication_classes([DocumentDrawerAuthentication])
class DocumentUpdateView(APIView):
    """
    Update the filename of the uploaded document.
    """

    def put(self, request, id):
        document = get_object_or_404(Document, pk=id)
        # Make sure fileid belongs to the document drawer the client logged into.
        if not document.drawer_id == request.user.id:
            return Response(status=status.HTTP_401_UNAUTHORIZED, data=dict(
                detail="Unauthorized Access"
            ))
        serializer = DocumentUpdateSerializer(data=self.request.data)
        if not serializer.is_valid():
            return Response(status=status.HTTP_400_BAD_REQUEST, data=serializer.errors)
        document.filename = serializer.validated_data['filename']
        document.save()
        resp_serializer = DocumentModelSerializer(instance=document)
        return Response(resp_serializer.data)


@extend_schema(tags=["Document Drawer"], summary="Delete File")
@authentication_classes([DocumentDrawerAuthentication])
class DocumentDeleteView(APIView):
    """
    Delete file
    """

    def delete(self, request, id):
        document = get_object_or_404(Document, pk=id)
        # Make sure fileid belongs to the document drawer the client logged into.
        if not document.drawer_id == request.user.id:
            return Response(status=status.HTTP_401_UNAUTHORIZED, data=dict(
                detail="Unauthorized Access"
            ))
        document.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@extend_schema(tags=["Document Drawer"], summary="Download File", responses=DocumentDownloadSerializer)
@authentication_classes([DocumentDrawerAuthentication])
class DocumentDownloadView(APIView):
    """
    View to get the download links for documents.  \n
    The S3 urls has an expiration time of 5 minutes.
    """

    def get(self, request, id):
        document = get_object_or_404(Document, pk=id)
        if not document.drawer_id == request.user.id:
            return Response(status=status.HTTP_401_UNAUTHORIZED, data=dict(
                detail="Unauthorized Access"
            ))
        serializer = DocumentDownloadSerializer(instance=document)
        return Response(data=serializer.data, status=status.HTTP_200_OK)
