from drf_spectacular.utils import extend_schema
from rest_framework import status
from rest_framework.response import Response
from lead.serializers.agent import ClientPDPATokenSerializer, PDPATokenVerifySerializer
from rest_framework.views import APIView
from magic.models import PDPAVerification
from rest_framework.decorators import authentication_classes, permission_classes


@authentication_classes([])
@permission_classes([])
class VerifyTokenAPIView(APIView):
    serializer_class = ClientPDPATokenSerializer

    @extend_schema(tags=["Customer - Lead - PDPA"], summary="Verify PDPA Token", request=PDPATokenVerifySerializer,
                   responses=ClientPDPATokenSerializer)
    def post(self, request):
        """
        This endpoint validates the token  \n
        Token should be in the format - client_id-token  \n
        For example 1-3MO91QG
        """
        serializer = PDPATokenVerifySerializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        try:
            client_id, code = serializer.validated_data.get("token").split("-")
            token = PDPAVerification.objects.filter(code=code, client_id=client_id, used=False).first()
            # Set the token as used.
            token.used = True
            token.save()
            # Update the PDPA status.
            token.client.pdpa_status = True
            token.client.save()
            serializer = ClientPDPATokenSerializer(dict(
                client=token.client_id,
                message="PDPA has been validated",
                status=token.client.pdpa_status,
                token=code
            ))
        except Exception as e:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=dict(
                detail="Invalid token"
            ))

        return Response(serializer.data)
