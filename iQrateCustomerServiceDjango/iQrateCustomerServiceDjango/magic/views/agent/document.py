import uuid

from django.conf import settings
from django.shortcuts import get_object_or_404
from drf_spectacular.utils import extend_schema
from rest_framework import status
from rest_framework.exceptions import PermissionDenied
from rest_framework.generics import RetrieveAPIView
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from rest_framework.views import APIView

from common.s3client import S3Client
from magic.models import Document, DocumentDrawer
from magic.serializers.agent import AgentDocumentDrawerDetailSerializer, \
    AgentDocumentCreateSerializer, AgentDocumentModelSerializer, AgentDocumentUpdateSerializer, AgentDocumentDownloadSerializer


@extend_schema(tags=["Document Drawer Agent"], summary="View drawer", responses=AgentDocumentDrawerDetailSerializer)
class DocumentDrawerView(RetrieveAPIView):
    """
    This endpoint returns a list of files
    """
    serializer_class = AgentDocumentDrawerDetailSerializer

    def get_object(self):
        """
        Make sure the agent has access to the document drawer.
        """
        try:
            return DocumentDrawer.objects.filter(
                pk=self.kwargs.get("id")
            ).accessible_for(user=self.request.user).prefetch_related("documents").get()
        except DocumentDrawer.DoesNotExist:
            raise PermissionDenied(
                detail="You do not have access to this document drawer"
            )


@extend_schema(tags=["Document Drawer Agent"], summary="Upload File",
               request=AgentDocumentCreateSerializer, responses=AgentDocumentModelSerializer)
class DocumentUploadView(APIView):
    """
    View to upload documents to
    """
    parser_classes = [MultiPartParser, FormParser]

    def post(self, request):
        serializer = AgentDocumentCreateSerializer(data=request.data, context=dict(request=request))
        if serializer.is_valid():
            # get the uploaded file
            file = serializer.validated_data["file"]
            ext = file.name.split(".").pop()
            file_key = f"{str(serializer.validated_data['drawer'].id)}/{str(uuid.uuid4())}.{ext}"

            client = S3Client(settings.AWS_PRIVATE_BUCKET_NAME)
            uploaded = client.upload_file(
                file_key=file_key,
                file_obj=file,
                content_type=file.content_type
            )
            if uploaded:
                # Create the record in the database.
                obj = Document.objects.create(
                    drawer=serializer.validated_data['drawer'],
                    filename=serializer.validated_data['filename'],
                    doc_type=serializer.validated_data['doc_type'],
                    extension=ext,
                    file_key=file_key
                )
                serializer = AgentDocumentModelSerializer(instance=obj)
                return Response(status=status.HTTP_201_CREATED, data=serializer.data)
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data=dict(
                error='Error with uploading the file'
            ))
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@extend_schema(tags=["Document Drawer Agent"], summary="Update File", request=AgentDocumentUpdateSerializer, responses=AgentDocumentModelSerializer)
class DocumentUpdateView(APIView):
    """
    Update the filename of the uploaded document.
    """

    def put(self, request, id):
        document = get_object_or_404(Document, pk=id)
        # make sure the agent has access to the document.
        if not DocumentDrawer.objects.filter(pk=document.drawer_id).accessible_for(user=self.request.user).exists():
            raise PermissionDenied(detail="Unauthorized access")
        serializer = AgentDocumentUpdateSerializer(data=self.request.data)
        if not serializer.is_valid():
            return Response(status=status.HTTP_400_BAD_REQUEST, data=serializer.errors)
        document.filename = serializer.validated_data['filename']
        document.save()
        resp_serializer = AgentDocumentModelSerializer(instance=document)
        return Response(resp_serializer.data)


@extend_schema(tags=["Document Drawer Agent"], summary="Delete File")
class DocumentDeleteView(APIView):
    """
    Delete file
    """

    def delete(self, request, id):
        document = get_object_or_404(Document, pk=id)
        # Make sure the agent has access to the document.
        if not DocumentDrawer.objects.filter(pk=document.drawer_id).accessible_for(user=self.request.user).exists():
            raise PermissionDenied(detail="Unauthorized Access")
        document.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@extend_schema(tags=["Document Drawer Agent"], summary="Download File", responses=AgentDocumentDownloadSerializer)
class DocumentDownloadView(APIView):
    """
    View to get the download links for documents
    """

    def get(self, request, id):
        document = get_object_or_404(Document, pk=id)
        # Make sure the agent has access to the document.
        if not DocumentDrawer.objects.filter(pk=document.drawer_id).accessible_for(user=self.request.user).exists():
            raise PermissionDenied(detail="Unauthorized Access")

        serializer = AgentDocumentDownloadSerializer(instance=document)
        return Response(data=serializer.data, status=status.HTTP_200_OK)
