#!/bin/sh

echo "Starting up app"
python3 manage.py migrate
python3 manage.py update_kvstore_data
python3 manage.py register_methods
python3 manage.py collectstatic --no-input
gunicorn --workers 5 --bind 0.0.0.0:80 --access-logfile - app.wsgi