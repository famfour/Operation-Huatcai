import {
  HttpException,
  HttpStatus,
  Injectable,
  NestMiddleware,
} from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';

const debug = require('debug')('connector:AuthMiddleware');

@Injectable()
export class Auth implements NestMiddleware {
  constructor() {}

  async use(req: Request, res: Response, next: NextFunction) {
    if (req.headers.autherization && !req.headers.authorization) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      req.headers.authorization = req.headers.autherization;
    }
    if (req.headers.authorization) {
      try {
        debug('req.headers : ', req.headers);
      } catch (err) {
        console.error(err);
        throw new HttpException(
          {
            status: HttpStatus.UNAUTHORIZED,
            message: 'Unauthorized Access',
            errorcode: 'AuthMiddleware_100',
          },
          HttpStatus.UNAUTHORIZED,
        );
      }
    } else {
      throw new HttpException(
        {
          status: HttpStatus.UNAUTHORIZED,
          message: 'Unauthorized Access',
          errorcode: 'AuthMiddleware_100',
        },
        HttpStatus.UNAUTHORIZED,
      );
    }

    next();
  }
}
