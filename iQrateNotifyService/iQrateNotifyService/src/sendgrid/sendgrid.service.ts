import { Injectable } from '@nestjs/common';
//import { ConfigService } from '@nestjs/config';
import { ConfigService } from '../config/config.service';
import * as SendGrid from '@sendgrid/mail';

@Injectable()
export class SendgridService {

    constructor(private readonly configService: ConfigService) {
        // Don't forget this one.
        // The apiKey is required to authenticate our
        // request to SendGrid API.
        // SendGrid.setApiKey(this.configService.get<string>('SEND_GRID_KEY'));
        // this.conf = this.configService.getSendGridConfig();
        let conf = this.configService.getSendGridConfig();
        SendGrid.setApiKey(conf.SEND_GRID_KEY);

    }
    /*
    async send(mail: SendGrid.MailDataRequired) {
        const transport = await SendGrid.send(mail);
        // avoid this on production. use log instead :)
        console.log(`E-Mail sent to ${mail.to}`);
        return transport;
    }
    */
    public static async send(mail: SendGrid.MailDataRequired) {
        let configService = new ConfigService;
        var conf = configService.getSendGridConfig();
        console.log("MAIL FROM ADDESSS " + conf.MAIL_FROM_ADDRESS);
        mail.from = conf.MAIL_FROM_ADDRESS;

        const transport = await SendGrid.send(mail).then((response) => {
            console.log(response[0].statusCode);
            console.log(response[0].headers);

            if (response[0].statusCode == 202) {
                console.log(`E-Mail sent to ${mail.to}`);
                return true
            } else {
                return false;
            }

        })
            .catch((error) => {
                console.error(error)
                return false;
            });

        // avoid this on production. use log instead :)
        console.log("Print result");
        console.log(transport);
        return transport;
        //return false;
    }

}