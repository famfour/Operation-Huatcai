import { HttpModule, MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { WinstonModule } from 'nest-winston';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigService } from './config/config.service';
import { ConfigModule } from './config/config.module';
import { EnumModule } from './usecases/enum/enum.module';
import { NotifyModule } from './usecases/notify/notify.module';

@Module({
  imports: [
    EventEmitterModule.forRoot(),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (config: ConfigService) => config.getTypeORMConfig(),
      inject: [ConfigService],
    }),
    ConfigModule,
    WinstonModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (config: ConfigService) => config.getLoggerConfig(),
      inject: [ConfigService],
    }),
    EnumModule,
    NotifyModule,
    HttpModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  async configure(consumer: MiddlewareConsumer) {
    // consumer.apply(HTTPLogger).forRoutes('*');
    // consumer.apply(Auth).forRoutes('/tnc');
  }
}
