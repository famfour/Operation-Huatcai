import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import Debug from 'debug';
import { v4 as uuIdv4 } from 'uuid';

import { getConnection } from 'typeorm';
import { ConfigService } from '../../config/config.service';
import { AuditLog } from '../../entities/audit_log';
import EmailLogs from '../../entities/email-logs';
import SMSLogs from '../../entities/sms-logs';
import { EmailLogsSubscriber } from './log.subscriber';

const debug = Debug('connector:LogController');
const advancedFormat = require('dayjs/plugin/advancedFormat');

@Injectable()
export class LogService {
  constructor(private config: ConfigService) {
    debug('LogService initialized and listen to emaillogs events')
    new EmailLogsSubscriber().listenTo();
  }

  async extractData(responseBody: any) {
    const insertObj = {}
    insertObj['clientIp'] = responseBody.client_ip;
    insertObj['serviceId'] = responseBody.service.id;
    insertObj['latency'] = JSON.stringify(responseBody.latencies);
    insertObj['name'] = responseBody.route.name;
    insertObj['methods'] = responseBody.request.method;
    insertObj['queryString'] = JSON.stringify(responseBody.request.querystring);
    insertObj['statusCode'] = responseBody.response.status;
    insertObj['response'] = JSON.stringify(responseBody.response);
    insertObj['url'] = responseBody.request.url
    insertObj['createdAt'] = responseBody.started_at;

    return await this.insertDataIntoLog(insertObj)
  }

  async insertDataIntoLog(insertObj: any) {
    try {
      const gatewayConnection = getConnection('gatewayConnection');
      const auditLogRepo = gatewayConnection.getRepository(AuditLog);
      let audit_log = new AuditLog();
      audit_log.id = uuIdv4();
      audit_log = { ...audit_log, ...insertObj }

      const response = await auditLogRepo.save(audit_log);

      return response;
    } catch (err) {
      console.error(err);
      throw err;
    }
  }


  //@OnEvent('email.save')
  async saveEmailResponse(emailObj) {
    try {
      console.log(emailObj);
      return await EmailLogs.save(emailObj);
    } catch (error) {
      debug('Email Save Failed : ', error);
      throw error;
    }
  }

  //@OnEvent('mobile.save')
  async saveMobileResponse(mobileObj) {
    try {
      debug(mobileObj);
      await SMSLogs.save(mobileObj);
      console.log("SMS saved");
    } catch (error) {
      debug('SMS Save Failed : ', error);
    }
  }



}
