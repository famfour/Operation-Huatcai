import { Body, Controller, Param, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ConfigService } from '../../config/config.service';
import { LogService } from './log.service';

@ApiTags('log')
@Controller('/log')
export class LogController {
  constructor(
    private readonly logService: LogService,
    private config: ConfigService,
  ) { }
  /*
    @Post('/save/:id')
    async saveLogs(@Param('id') id: string, @Body() body: any) {
      await this.logService.extractData(body);
      // console.info(JSON.stringify({ body }));
      return { id, message: 'success' };
    }
    */

}
