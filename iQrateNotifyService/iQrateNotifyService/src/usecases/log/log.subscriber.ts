// Logger for printing out errors to the console
import { Logger } from '@nestjs/common';

import Debug from 'debug';
const debug = Debug('notification-service:EmailLogsSubscriber');

// The decorator that plugs in the subscription functionality
import { EventSubscriber } from 'typeorm/decorator/listeners/EventSubscriber';

// The interface that our subscriber class must implement
import { EntitySubscriberInterface } from 'typeorm/subscriber/EntitySubscriberInterface';

// The events you would like to listen to
import { UpdateEvent } from 'typeorm/subscriber/event/UpdateEvent';
import { RemoveEvent } from 'typeorm/subscriber/event/RemoveEvent';

// The entity we will be monitoring
import EmailLogs from '../../entities/email-logs';
import {InsertEvent} from "typeorm";

@EventSubscriber()
export class EmailLogsSubscriber implements EntitySubscriberInterface<EmailLogs> {
    constructor(/* inject services in here if you want to */) {}


    listenTo() {
        return EmailLogs;
    }


    beforeInsert(event: InsertEvent<EmailLogs>) {
        try {
            /*
              Saving feature, depends on your vision of logging

            */
            debug('Printing EmailLogsSubscriber beforeInsert: ', event);
        } catch (error) {
            Logger.error(error);
        }
    }

    beforeUpdate(event: UpdateEvent<EmailLogs>) {
        try {
            /*
              Saving feature, depends on your vision of logging

            */
            debug('Printing EmailLogsSubscriber beforeUpdate ');
        } catch (error) {
            Logger.error(error);
        }
    }


    beforeRemove(event: RemoveEvent<EmailLogs>) {
        try {
            // const audit = new Audit();
            // audit.methodName = 'delete';
            // audit.modelName = Supplier.name;
            // audit.oldObject = event.databaseEntity;
            /* log the audit object */
        } catch (error) {
            Logger.error(error);
        }
    }
}