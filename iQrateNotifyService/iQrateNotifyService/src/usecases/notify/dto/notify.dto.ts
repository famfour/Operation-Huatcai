import { ApiProperty } from '@nestjs/swagger';
import { isNotEmpty, IsNotEmpty } from 'class-validator';

export class VerifyEmailInfo {
  [x: string]: any;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    type: String,
    description: 'To',
    default: 'tamilselvan@digitalprizm.net'
  })
  to: string;

  @ApiProperty({
    description: 'verification_code',
    default: '123456'
  })
  verification_code: string;

  @ApiProperty({
    description: 'link',
    default: ''
  })
  link?: string;

  @ApiProperty({
    description: 'subject',
    default: 'EmailVerification'
  })
  subject?: string;

  data?: string;

  @ApiProperty({
    description: 'template',
    default: 'EmailVerification'
  })
  template?: string;

  name?: string;

}

export class EmailBodyType {
  [x: string]: any;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    type: String,
    description: 'To',
    default: 'tamilselvan@digitalprizm.net'
  })
  to: string;

  @ApiProperty({
    description: 'subject',
    default: 'IQRATE Email'
  })
  subject?: string;

  data?: string;

  template?: string;

  attatchments?: any;

  name?: string;
}


/*
export type VerifyEmailInfo = {
  templateName?: string;
  subjectPrefix?: string;
  to: string;
  verification_code: string;
  subject?: string;
  data?: string;
};
*/
export class VerifySMSInfo {
  @ApiProperty({
    description: 'to',
    default: '919962679318'
  })
  @IsNotEmpty()
  to: string;
  @ApiProperty({
    description: 'verification_code',
    default: '123456'
  })
  @IsNotEmpty()
  verification_code: string;

  data?: string;
  name?: string;
};

export class VerifyResetPassword {
  @ApiProperty({
    description: 'full_name',
    default: 'Tamil'
  })
  full_name: string;

  @ApiProperty({
    description: 'to',
    default: 'tamilselvan@digitalprizm.net'
  })
  to: string;

  @ApiProperty({
    description: 'subject',
    default: 'ResetPassword'
  })
  subject?: string;
  @ApiProperty({
    description: 'url',
    default: 'http://localhost:3001/reset-password'
  })
  url?: string;
  data?: string;
  name?: string;

  @ApiProperty({
    description: 'template',
    default: 'ResetPassword'
  })
  template?: string

};

export class VerifyResetPasswordSuccess {
  @ApiProperty({
    description: 'full_name',
    default: 'Tamil'
  })
  full_name: string;

  @ApiProperty({
    description: 'to',
    default: 'tamilselvan@digitalprizm.net'
  })
  to: string;

  @ApiProperty({
    description: 'subject',
    default: 'ResetPasswordResponse'
  })
  subject?: string;

  url?: string;

  data?: string;
  name?: string;

};

export class PaymentFailure {

  @ApiProperty({
    description: 'name',
    default: 'tamil'
  })
  name: string;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'email',
    default: 'tamilselvan@digitalprizm.net'
  })
  email: string;

  @ApiProperty({
    description: 'phone',
    default: '919962679318'
  })
  phone: string;

  @ApiProperty({
    description: 'subject',
    default: 'Payment Failure'
  })
  subject?: string;

  @ApiProperty({
    description: 'amount',
    default: '0'
  })
  amount: number;

  @ApiProperty({
    description: 'message',
    default: 'Payment declined'
  })
  message: string;

  template?: string;

  data?: string;


};
export class VerifyPDPA {
  @ApiProperty({
    description: 'to',
    default: '919962679318'
  })
  @IsNotEmpty()
  to: string;

  @ApiProperty({
    description: 'pdpa_link',
    default: 'http://hafary-admin.dprizm.dev/pdpa-approve'
  })
  @IsNotEmpty()
  pdpa_link: string;

  data?: string;
  name?: string;
};

export class SentSmsResDto {
  @ApiProperty({
    description: 'id',
    default: 'VerificationMobile'
  })
  id: string;

  @ApiProperty({
    description: 'status',
    default: 'SUCCESS'
  })
  status: string;
}

export class VerifyAdminCredentialInfo {
  [x: string]: any;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    type: String,
    description: 'To',
    default: 'tamilselvan@digitalprizm.net'
  })
  to: string;

  @ApiProperty({
    description: 'username',
    default: 'tamilselvan123@digitalprizm.net'
  })
  username: string;

  @ApiProperty({
    description: 'password',
    default: ''
  })
  password: string;

  @ApiProperty({
    description: 'subject',
    default: 'Admin User Login Credentials'
  })
  subject?: string;

  data?: string;

  @ApiProperty({
    description: 'template',
    default: 'AdminCredentials'
  })
  template?: string;

  name?: string;
  
  templateFor?: string

}


export class SubscriptionStatus {

  @ApiProperty({
    description: 'name',
    default: 'tamil'
  })
  name: string;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'email',
    default: 'tamilselvan@digitalprizm.net'
  })
  email: string;

  @ApiProperty({
    description: 'phone',
    default: '919962679318'
  })
  phone: string;

  @ApiProperty({
    description: 'subject',
    default: 'Payment Failure'
  })
  subject?: string;

 
  @ApiProperty({
    description: 'message',
    default: 'Payment declined'
  })
  message: string;

  template?: string;

  data?: string;

};



export class InitPushNotifyDto {

  @ApiProperty({
    type: String,
    description: 'Device id',
    default: 'adsfsghjk'
  })
  @IsNotEmpty({ message: 'Device id should not be empty' })
  device_id: string;

  @ApiProperty({
    type: String,
    description: 'User id',
    default: 'adsfsghjk'
  })
  @IsNotEmpty({ message: 'User id should not be empty' })
  user_id: string;

  @ApiProperty({
    type: String,
    description: 'Membership type',
    default: 'adsfsghjk'
  })
  @IsNotEmpty({ message: 'Membership type should not be empty' })
  membership_type: string;
}

export class AddPushNotifyDto {
  @ApiProperty({
    type: String,
    description: 'Title',
  })
  @IsNotEmpty({ message: 'Title should not be empty' })
  title: string

  @ApiProperty({
    type: String,
    description: 'message',
  })
  @IsNotEmpty({ message: 'Message role should not be empty' })
  message: string

  @ApiProperty({
    type: Object,
    description: 'Target audience',
    default: '["basic","premium"]',
  })
  target_audience?: string[];

  @ApiProperty({
    type: Date,
    description: 'Notification date',
    default: '2022-09-25',
  })
  @IsNotEmpty({ message: 'Notification date should not be empty' })
  notify_date: Date;

  @ApiProperty({
    type: String,
    description: 'Notification time',
  })
  @IsNotEmpty({ message: 'Notification time should not be empty' })
  notify_time: string;

  @ApiProperty({
    type: String,
    description: 'Notification date with time',
    default: '2022-09-25T10:21:13.965Z',
  })
  @IsNotEmpty({ message: 'Notification date with time should not be empty' })
  notify_date_time: string;

  @ApiProperty({
    type: String,
    description: 'Created by',
    default: 'sd24_d433-2342wewrwe_werw',
  })
  // @IsNotEmpty({ message: 'Cre with time should not be empty' })
  createby: string;
}