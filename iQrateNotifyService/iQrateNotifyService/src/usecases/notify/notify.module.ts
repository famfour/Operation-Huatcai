import { Module } from '@nestjs/common';
import { NotifyController } from './notify.controller';
import { NotifyService } from './notify.service';
import { ConfigModule } from '../../config/config.module';
import { ConfigService } from '../../config/config.service';
import { LogService } from '../log/log.service';
import { SendgridService } from '../../sendgrid/sendgrid.service';
import { HttpModule } from '@nestjs/axios';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [ConfigModule, HttpModule, ScheduleModule.forRoot()],
  controllers: [NotifyController],
  providers: [NotifyService, LogService, SendgridService],
  exports: [NotifyService, LogService, SendgridService]
})
export class NotifyModule { }
