import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import Debug from 'debug';
import DeviceDetector = require('device-detector-js');
import { ConfigService } from '../../config/config.service';
import { UserEntity } from '../../entities/users';
import { Any, getConnection, In } from "typeorm";
import { join } from 'path';
//import { NotifyDto } from "./dto/notify.dto";
import {
  VerifyEmailInfo, VerifyResetPassword, VerifySMSInfo, VerifyResetPasswordSuccess,
  PaymentFailure, EmailBodyType, VerifyPDPA,VerifyAdminCredentialInfo,SubscriptionStatus, InitPushNotifyDto, AddPushNotifyDto
} from './dto/notify.dto';
import { SendgridService } from '../../sendgrid/sendgrid.service';
import { LogService } from '../log/log.service';
import { Twilio } from 'twilio';
import * as fs from 'fs';
import * as hbs from 'handlebars';
import EmailTemplate from "../../entities/email_templates";
import { PushNotificationToUserEntity } from '../../entities/push_notification_users';
import { AdminPushNotifiyEntity } from '../../entities/push_notify_entity';
import { HttpService } from '@nestjs/axios';
import { Cron } from '@nestjs/schedule';

import * as AWS from 'aws-sdk';

// START to get device info
const deviceDetector = new DeviceDetector();
const userAgent =
  'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Safari/537.36';
const device = deviceDetector.parse(userAgent);
// console.log(device);
// END

const debug = Debug('notification:NotifyService');

@Injectable()
export class NotifyService {

  constructor(
    private config: ConfigService,
    private logService: LogService,
    private readonly http: HttpService,
  ) {
  }


  /***
    * Send email to user to verify email
    * @return json
    */

  async sendEmail(content: EmailBodyType) {
    let emailTemplate: any = {};
    try {
      debug("Entering sedngrid mail config");
      emailTemplate = await EmailTemplate.findOne(content.template);

      if (!emailTemplate || !content.template || !emailTemplate.template) {
        throw new HttpException({ message: 'Invalid email template settings. Please configure email template for this email' }, HttpStatus.BAD_REQUEST);
      }

      const templateId = emailTemplate.template_id;
      const result = "";
      const dynamic_template_data = {
        name: content.name,
        verification_code: content.verification_code,
        link: content.link
      };
      debug("FROM : ", this.config.envConfig.MAIL_FROM_ADDRESS);
      const mail: any = {
        to: content.to,
        subject: content.subject,
        from: this.config.envConfig.MAIL_FROM_ADDRESS,
        templateId,
        //text: 'Hello this is test mail',
        // html: result,
        dynamic_template_data
      };

      /*
      const EMAIL_VERIFICATION_TEMPLATE_IN_BASE64 = emailTemplate.template;
      const body = Buffer.from(EMAIL_VERIFICATION_TEMPLATE_IN_BASE64, 'base64').toString('ascii');

      const template = hbs.compile(body);
      // var dataInput = { name: content.to, verification_code: content.verification_code };
      const result = template(content.data);
      debug("FROM : ", this.config.envConfig.MAIL_FROM_ADDRESS);
      const mail: any = {
        to: content.to,
        subject: content.subject,
        from: this.config.envConfig.MAIL_FROM_ADDRESS,
        //text: 'Hello this is test mail',
        html: result,
      };
      if (content.cc) {
        mail.cc = content.cc;
      }
      */

      const attachments = [];
      if (content.attachments && content.attachments.length > 0) {
        mail.attachments = content.attachments;
        for (const file of mail.attachments) {
          attachments.push(file.filename);
        }
      }
      const info = await SendgridService.send(mail);
      debug('SendgridService.send result :', info);
      const emailLog = await this.logService.saveEmailResponse({
        //eventEmitter.emit('email.save', {
        emailType: emailTemplate.name || 'Generic',
        base64Data: "No",
        senderEmails: '',
        receiverEmails: content.to,
        subject: content.subject,
        body: result,
        data: content.data,
        status: 'SUCCESS',
        attachments: JSON.stringify(attachments),
      });

      await this.uploadToS3(mail.attachments, emailLog.id);

      return info;

    } catch (err) {
      console.error('sendEmail :', err);
      await this.logService.saveEmailResponse({
        emailType: (emailTemplate ? emailTemplate.name : 'Generic'),
        base64Data: "No",
        subject: content.subject,
        body: JSON.stringify(err),
        data: JSON.stringify(content.data),
        status: 'FAILED',
      });
      throw err;
      //return { id: "VerificationEmail", "status": "FAILED" };
    }
  }

  async uploadToS3(files, emailLogId) {
    // S3 Bucket = iqrate-emailattachments
    for (const file of files) {
      await this.uploadFile(emailLogId + '_' + file.filename, file);
    }
  }

  async uploadFile(fileName, file) {
    // Read content from the file
    // const fileContent = fs.readFileSync(fileName);

    // Setting up S3 upload parameters
    const AWS_S3_BUCKET_NAME = this.config.envConfig.AWS_S3_BUCKET_NAME || 'iqrate-emailattachments';
    const AWS_ACCESS_KEY_ID = this.config.envConfig.AWS_ACCESS_KEY_ID || 'AKIARMFLUTLWW52WJ2CB';
    const AWS_SECRET_ACCESS_KEY = this.config.envConfig.AWS_SECRET_ACCESS_KEY || '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';

    const fileContent = Buffer.from(file.content, 'base64');

    const params = {
      Bucket: AWS_S3_BUCKET_NAME,
      Key: fileName, // File name you want to save as in S3
      Body: fileContent
    };

    const s3 = new AWS.S3({
      accessKeyId: AWS_ACCESS_KEY_ID,
      secretAccessKey: AWS_SECRET_ACCESS_KEY
    });

    // Uploading files to the bucket
    return new Promise((res, rej) => {
      s3.upload(params, function (err, data) {
        if (err) {
          return rej(err);
        }
        console.log(`File backup to s3 successfully. ${data.Location}`);
        return res(`File backup to s3 successfully. ${data.Location}`);
      });
    });
  };

  async sendVerifyEmail(content: VerifyEmailInfo) {
    try {
      debug("Entering sedngrid mail config");
      console.log('Entering sedngrid mail config ');
      /*
      const emailTemplate = await EmailTemplate.findOne(content.template);
      const EMAIL_VERIFICATION_TEMPLATE_IN_BASE64 = emailTemplate.template || `PGRpdj4KICAgIERlYXIge3sgbmFtZX19LCA8YnI+PGJyPgoKICAgIDxkaXYgc3R5bGU9Im1hcmdpbi1sZWZ0OiAxMHB4OzsiPgogICAgICAgIFBsZWFzZSB1c2UgYmVsb3cgdmVyaWZpY2F0aW9uIGNvZGUgdG8gYWN0aXZlIGVtYWlsLiA8YnI+PGJyPgogICAgPC9kaXY+CiAgICA8ZGl2IHN0eWxlPSJtYXJnaW4tbGVmdDogMTBweDtmb250LXNpemU6IDIwcHg7bWFyZ2luLWJvdHRvbToxMHB4OyI+CiAgICAgICAge3sgdmVyaWZpY2F0aW9uX2NvZGV9fQogICAgPC9kaXY+CjwvZGl2Pg==`;

      const body = Buffer.from(EMAIL_VERIFICATION_TEMPLATE_IN_BASE64, 'base64').toString('ascii');
      */

      const emailTemplate = await EmailTemplate.findOne(content.template);
      const templateId = emailTemplate.template_id;
      console.log('templateId',templateId);
      //const template = hbs.compile(body);
      // var dataInput = { name: content.to, verification_code: content.verification_code };
      //const result = template(content);
      const result = "";
      const dynamic_template_data = {
        name: content.name,
        verification_code: content.verification_code,
        link: content.link
      };
      debug("FROM : ", this.config.envConfig.MAIL_FROM_ADDRESS);
      const mail: any = {
        to: content.to,
        subject: content.subject,
        from: this.config.envConfig.MAIL_FROM_ADDRESS,
        templateId,
        //text: 'Hello this is test mail',
        // html: result,
        dynamic_template_data
      };
      if (content.cc) {
        mail.cc = content.cc;
      }



      debug(result);
      debug("Call send mail for verify email...");

      const info = await SendgridService.send(mail);

      debug('SendgridService.send result :', info);

      await this.logService.saveEmailResponse({
        //eventEmitter.emit('email.save', {
        emailType: 'EmailVerification',
        base64Data: "No",
        senderEmails: '',
        receiverEmails: content.to,
        subject: content.subject,
        body: result,
        data: content.data,
        status: 'SUCCESS',
      });

      return info;

    } catch (err) {
      console.error('sendEmail :', err);
      await this.logService.saveEmailResponse({
        emailType: 'EmailVerification',
        base64Data: "No",
        subject: content.subject,
        body: JSON.stringify(err),
        data: JSON.stringify(content.data),
        status: 'FAILED',
      });
      //throw err;
      return { id: "VerificationEmail", "status": "FAILED" };
    }
  }
  /***
   * Send SMS to user to verify mobile
   * @return json
   */
  async sendSMS(content: VerifySMSInfo) {
    try {

      console.log("Entering Twillio sms config");
      if (content.to || content.to === '') {
        new HttpException({ id: "VerificationMobile", "status": "FAILED", "message": "Error: Mobile number should not be empty" }, HttpStatus.BAD_REQUEST);
      }

      // if(!content.data || content.data==='')
      //const ejs = require(“ejs”);
      const senderPhoneNumber = this.config.getTwillioConfig().TWILIO_SENDER_PHONE_NUMBER;
      debug(senderPhoneNumber);

      const client = require('twilio')(this.config.getTwillioConfig().TWILIO_ACCOUNT_SID, this.config.getTwillioConfig().TWILIO_AUTH_TOKEN);
      // var msg = "Your iQrate verification code is " + content.verification_code;

      // var msg = "[iQrate] Verification Code:  " + content.verification_code + ". Do not share this code with anyone. If it was not you, please inform Support.";
      var msg = "Your IQRATE verification code is: " + content.verification_code + ". Do not share this code with anyone; our employees will never ask for the code.";

      if (!content.data) {
        content.data = msg;
      }

      const firstDigitMobile = String(content.to)[0];
      debug('firstDigitMobile:',firstDigitMobile);
      if(firstDigitMobile !== '+')
      {
        content.to = '+'+content.to;
      }
      debug('mobile number:', content.to);

      const message = await client.messages
        .create({
          to: content.to,
          body: msg,
          from: senderPhoneNumber
        });

      debug('SMS message :', message);

      await this.logService.saveMobileResponse({
        sms_type: 'MobileVerification',
        receiver_mobile: content.to,
        data: msg,
        status: 'SUCCESS',
      });
      return { id: "VerificationMobile", "status": "SUCCESS" };
    } catch (err) {
      console.error('sendEmail :', err);
      await this.logService.saveMobileResponse({
        sms_type: 'MobileVerification',
        receiver_mobile: content.to,
        data: content.data,
        status: 'FAILED',
      });

      if (err && err.status) {
        throw new HttpException({ error: err.sendEmail, name: err.moreinfo, message: "Permission denied" }, err.status);
      }

      throw err;
      // return { id: "VerificationMobile", "status": "FAILED" };
    }
  }


  /***
   * Send reset-password link to user 
   * @return json
   */
  async sendResetPassword(content: VerifyResetPassword) {
    try {

      console.log("Entering reset password....");
      if (content.to == "") {
        return { id: "ResetPassword", "status": "FAILED", "Message": "Error: To (Email) should not empty" };
      }
      //const ejs = require(“ejs”);
      // const senderPhoneNumber = this.config.envConfig.TWILIO_SENDER_PHONE_NUMBER;

      const emailTemplate = await EmailTemplate.findOne(content.template);
      const templateId = emailTemplate.template_id;

      //const template = hbs.compile(body);
      // var dataInput = { name: content.to, verification_code: content.verification_code };
      //const result = template(content);
      const result = "";
      const dynamic_template_data = {
        name: content.name,
        url: content.url
      };

      var logService = this.logService;
      var config = this.config;
      let res = new Promise((resolve, reject) => {
        //fs.readFile(
        //  "../../../assets/templates/reset-password.hbs",
        //  (error: NodeJS.ErrnoException | null, data: Buffer) => {
        //    var body = data.toString();

        //  const RESET_PASSWORD_TEMPLATE_IN_BASE64 = `PGRpdj4KICAgIERlYXIge3sgZnVsbF9uYW1lfX0sIDxicj48YnI+CgogICAgPGRpdiBzdHlsZT0ibWFyZ2luLWxlZnQ6IDEwcHg7OyI+CiAgICAgICAgUGxlYXNlIHVzZSBiZWxvdyBsaW5rIHRvIHJlc2V0IHlvdXIgcGFzc3dvcmQuIDxicj48YnI+CiAgICA8L2Rpdj4KICAgIDxkaXYgc3R5bGU9Im1hcmdpbi1sZWZ0OiAxMHB4O2ZvbnQtc2l6ZTogMjBweDttYXJnaW4tYm90dG9tOjEwcHg7Ij4KICAgICAgICA8YSBocmVmPSJ7eyB1cmwgfX0iPlJlc2V0IFBhc3N3b3JkPC9hPgogICAgPC9kaXY+CjwvZGl2Pg==`;
        //  var body = Buffer.from(RESET_PASSWORD_TEMPLATE_IN_BASE64, 'base64').toString('ascii');




        //  var template = hbs.compile(body);
        //  var dataInput = { full_name: content.full_name, url: content.url };
        //  var result = template(dataInput);
        const mail = {
          to: content.to,
          from: config.envConfig.MAIL_FROM_ADDRESS,
          subject: content.subject,
          //html: result,
          templateId,
          dynamic_template_data
        };
        console.log(result);

        // let sendgridService: SendgridService;
        console.log("Call send mail for reset password");

        // Sending mail....
        if (!SendgridService.send(mail)) {
          console.log("Mail send failed.");
          resolve({ id: "ResetPassword", "status": "FAILED" });
        }

        //let logService: LogService;
        logService.saveEmailResponse({
          //eventEmitter.emit('email.save', {
          emailType: 'ResetPassword',
          base64Data: "No",
          senderEmails: '',
          receiverEmails: content.to,
          subject: content.subject,
          body: result,
          data: content.data,
          status: 'SUCCESS',
        });

        let out = { id: "ResetPassword", "status": "SUCCESS" };
        //reject(`Still too large: ${theNumber}`);
        //  resolve(out);
        resolve(out);

        //  });


      });

      return res;



    } catch (err) {
      console.error('sendEmail :', err);
      logService.saveEmailResponse({
        sms_type: 'ResetPassword',
        receiver_mobile: content.to,
        data: content.data,
        status: 'FAILED',
      });
      //throw err;
      return { id: "ResetPassword", "status": "FAILED" };
    }
  }


  /***
   * Send reset-password success message  to user 
   * @return json
   */
  async sendResetPasswordSuccess(content: VerifyResetPasswordSuccess) {
    try {

      console.log("Entering reset password success....");
      if (content.to == "") {
        return { id: "ResetPasswordResponse", "status": "FAILED", "Message": "Error: To (Email) should not empty" };
      }
      //const ejs = require(“ejs”);
      //  const senderPhoneNumber = this.config.envConfig.TWILIO_SENDER_PHONE_NUMBER;

      var logService = this.logService;
      var config = this.config;

      let res = new Promise((resolve, reject) => {
        //fs.readFile(
        // "../../../assets/templates/reset-password-success.hbs",
        // (error: NodeJS.ErrnoException | null, data: Buffer) => {
        //   var body = data.toString();

        const RESET_PASSWORD_RESPONSE_TEMPLATE_IN_BASE64 = `PGRpdj4KICAgIERlYXIge3sgZnVsbF9uYW1lfX0sIDxicj48YnI+CgogICAgPGRpdiBzdHlsZT0ibWFyZ2luLWxlZnQ6IDEwcHg7OyI+CiAgICAgICAgWW91ciBwYXNzd29yZCBoYXMgYmVlbiBjaGFuZ2VkLiBQbGVhc2UgbG9naW4gdG8geW91ciBhY2NvdW50IDxicj48YnI+CiAgICA8L2Rpdj4KICAgIDxkaXYgc3R5bGU9Im1hcmdpbi1sZWZ0OiAxMHB4O2ZvbnQtc2l6ZTogMjBweDttYXJnaW4tYm90dG9tOjEwcHg7Ij4KICAgICAgICA8YSBocmVmPSJ7eyB1cmwgfX0iPkxvZ2luPC9hPgogICAgPC9kaXY+CjwvZGl2Pg==`;
        var body = Buffer.from(RESET_PASSWORD_RESPONSE_TEMPLATE_IN_BASE64, 'base64').toString('ascii');


        var template = hbs.compile(body);
        var dataInput = { full_name: content.full_name, url: content.url };
        var result = template(dataInput);
        const mail = {
          to: content.to,
          from: config.envConfig.MAIL_FROM_ADDRESS,
          subject: content.subject,
          html: result,
        };
        console.log(result);

        // let sendgridService: SendgridService;
        console.log("Call send mail for reset password response");

        // Sending mail....
        if (!SendgridService.send(mail)) {
          console.log("Mail send failed.");
          resolve({ id: "ResetPasswordResponse", "status": "FAILED" });
        }

        //let logService: LogService;
        logService.saveEmailResponse({
          //eventEmitter.emit('email.save', {
          emailType: 'ResetPasswordResponse',
          base64Data: "No",
          senderEmails: '',
          receiverEmails: content.to,
          subject: content.subject,
          body: result,
          data: content.data,
          status: 'SUCCESS',
        });

        let out = { id: "ResetPasswordResponse", "status": "SUCCESS" };
        //reject(`Still too large: ${theNumber}`);
        //  resolve(out);
        resolve(out);

        // });


      });

      return res;



    } catch (err) {
      console.error('sendEmail :', err);
      logService.saveEmailResponse({
        sms_type: 'ResetPasswordResponse',
        receiver_mobile: content.to,
        data: content.data,
        status: 'FAILED',
      });
      //throw err;
      return { id: "ResetPasswordResponse", "status": "FAILED" };
    }
  }


  /***
   * Send payment failure message to user 
   * @return json
   */
  /*
  async sendPaymentFailure(content: PaymentFailure) {
    try {

      debug("Entering payment failure mail....");

      var logService = this.logService;
      var config = this.config;
      const emailTemplate = await EmailTemplate.findOne(content.template);
      const PAYMENT_FAILURE_RESPONSE_TEMPLATE_IN_BASE64 = emailTemplate.template || `PGRpdj4KICAgIERlYXIge3sgbmFtZX19LCA8YnI+PGJyPgoKICAgIDxkaXYgc3R5bGU9Im1hcmdpbi1sZWZ0OiAxMHB4OzsiPgogICAgICAgIHt7IG1lc3NhZ2UgfX0gPGJyPjxicj4KICAgIDwvZGl2PgoKPC9kaXY+`;

      var body = "";
      let res = new Promise((resolve, reject) => {

        var body = Buffer.from(PAYMENT_FAILURE_RESPONSE_TEMPLATE_IN_BASE64, 'base64').toString('ascii');

        var template = hbs.compile(body);
        var dataInput = { name: content.name, message: content.message };
        var body = template(dataInput);
        const mail = {
          to: content.email,
          from: config.envConfig.MAIL_FROM_ADDRESS,
          subject: content.subject,
          html: body,
        };

        // Sending mail....
        if (!SendgridService.send(mail)) {
          console.log("Mail send failed.");
          throw new HttpException({
            name: 'Payment Failure',
            message: 'Mail send failed'
          }, HttpStatus.BAD_REQUEST);
          //resolve({ id: "ResetPasswordResponse", "status": "FAILED" });
        }

        //let logService: LogService;
        logService.saveEmailResponse({
          //eventEmitter.emit('email.save', {
          emailType: 'PaymentFailure',
          base64Data: "No",
          senderEmails: '',
          receiverEmails: content.email,
          subject: content.subject,
          body: body,
          data: "",
          status: 'SUCCESS',
        });

        let out = { id: "PaymentFailure", "status": "SUCCESS" };
        resolve(out);

      });

      return res;


    } catch (err) {

      logService.saveEmailResponse({
        sms_type: 'ResetPasswordResponse',
        receiver_mobile: content.email,
        data: body,
        status: 'FAILED',
      });
      //throw err;
      debug('err:', err);
      if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }
  }
*/

  async sendPaymentFailure(content: PaymentFailure) {
    try {
      debug("Entering sendAdminCredential sedngrid mail config");
      console.log('Entering sedngrid mail config ');

      const emailTemplate = await EmailTemplate.findOne(content.template);
      const templateId = emailTemplate.template_id;
      console.log('templateId',templateId);

      const result = "";
      const dynamic_template_data = {
        name: content.name,
        email: content.email
      };
      debug("FROM : ", this.config.envConfig.MAIL_FROM_ADDRESS);
      console.log(dynamic_template_data);

      const mail: any = {
        to: content.email,
        subject: content.subject,
        from: this.config.envConfig.MAIL_FROM_ADDRESS,
        templateId,
        //text: 'Hello this is test mail',
        // html: result,
        dynamic_template_data
      };
      /*
      if (content.cc) {
        mail.cc = content.cc;
      }*/
   
      debug("Call sendAdminCredential send mail for verify email...");

      const info = await SendgridService.send(mail);

      debug('SendgridService.send result :', info);

      await this.logService.saveEmailResponse({
        //eventEmitter.emit('email.save', {
        emailType: 'PaymentFailure',
        base64Data: "No",
        senderEmails: '',
        receiverEmails: content.email,
        subject: content.subject,
        body: result,
        data: content.data,
        status: 'SUCCESS',
      });

      return info;

    } catch (err) {
      console.error('sendEmail :', err);
      await this.logService.saveEmailResponse({
        emailType: 'PaymentFailure',
        base64Data: "No",
        subject: content.subject,
        body: JSON.stringify(err),
        data: JSON.stringify(content.data),
        status: 'FAILED',
      });
      //throw err;
      return { id: "AdminCredentialEmail", "status": "FAILED" };
    }
  }

  /***
  * Send SMS to user to verify PDPA
  * @return json
  */
  async sendPDPA_SMS(content: VerifyPDPA) {
    try {

      console.log("Entering Twillio sms config");
      if (content.to || content.to === '') {
        new HttpException({ id: "VerificationMobile", "status": "FAILED", "message": "Error: Mobile number should not be empty" }, HttpStatus.BAD_REQUEST);
      }

      // if(!content.data || content.data==='')
      //const ejs = require(“ejs”);
      const senderPhoneNumber = this.config.getTwillioConfig().TWILIO_SENDER_PHONE_NUMBER;
      debug(senderPhoneNumber);

      const client = require('twilio')(this.config.getTwillioConfig().TWILIO_ACCOUNT_SID, this.config.getTwillioConfig().TWILIO_AUTH_TOKEN);
      // var msg = "Your iQrate verification code is " + content.verification_code;

      var msg = "[iQrate] Please click to accept the PDPA to allow iQrate to process your home loan \n" + content.pdpa_link + "";

      content.data = msg;

      const message = await client.messages
        .create({
          to: content.to,
          body: msg,
          from: senderPhoneNumber
        });

      debug('SMS message :', message);

      await this.logService.saveMobileResponse({
        sms_type: 'SEND_PDPA',
        receiver_mobile: content.to,
        data: msg,
        status: 'SUCCESS',
      });
      return { id: "SendPDPA", "status": "SUCCESS" };
    } catch (err) {
      console.error('sendEmail :', err);
      await this.logService.saveMobileResponse({
        sms_type: 'SEND_PDPA',
        receiver_mobile: content.to,
        data: content.data,
        status: 'FAILED',
      });

      if (err && err.status) {
        throw new HttpException({ error: err.sendEmail, name: err.moreinfo, message: "Permission denied" }, err.status);
      }

      throw err;
      // return { id: "VerificationMobile", "status": "FAILED" };
    }
  }


  getDate(): Date {
    //return new Date().toString();
    const date = new Date();
    var today = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();

    var date1 = new Date(today);

    return date1;
  }

  /*
   public async delete(id: number) {
    try {


      const userRepo = getConnection().getRepository(UserEntity);
      const res = await userRepo.delete(id);
      return res;
    } catch (err) {
      console.error('Register Err:', err);
      throw err;
    }
  }
  public async register(responseBody: UserDto) {
    try {
      debug('Entered user service');
      console.log("connection name: " + getConnection().driver.database);
      const userRepo = getConnection().getRepository(UserEntity);
      const user = new UserEntity();
      user.full_name = responseBody.full_name;
      user.email = responseBody.email;
      user.password = 'TestPass2021'; // hashPass;
      user.mobile = responseBody.mobile;
      user.phone = responseBody.phone;
      user.agreed_term = responseBody.agreed_term;
      user.dob = responseBody.dob;
      user.user_type = "basic";
      //user.created_at = new Date();
      //  user.created_at = new Date('1980-09-10');
      const res = await userRepo.save(user);
      return res;
    } catch (err) {
      console.error('Register Err:', err);
      throw err;
    }
  }
  */

  async sendAdminCredential(content: VerifyAdminCredentialInfo) {
    try {
      debug("Entering sendAdminCredential sedngrid mail config");
      console.log('Entering sedngrid mail config ');

      const emailTemplate = await EmailTemplate.findOne(content.template);
      const templateId = emailTemplate.template_id;
      console.log('templateId',templateId);

      const result = "";
      const dynamic_template_data = {
        name: content.name,
        username: content.username,
        password: content.password,
        templateFor:content.templateFor
      };
      debug("FROM : ", this.config.envConfig.MAIL_FROM_ADDRESS);
      console.log(dynamic_template_data);

      const mail: any = {
        to: content.to,
        subject: content.subject,
        from: this.config.envConfig.MAIL_FROM_ADDRESS,
        templateId,
        //text: 'Hello this is test mail',
        // html: result,
        dynamic_template_data
      };
      if (content.cc) {
        mail.cc = content.cc;
      }


   
      debug("Call sendAdminCredential send mail for verify email...");

      const info = await SendgridService.send(mail);

      debug('SendgridService.send result :', info);

      await this.logService.saveEmailResponse({
        //eventEmitter.emit('email.save', {
        emailType: 'EmailVerification',
        base64Data: "No",
        senderEmails: '',
        receiverEmails: content.to,
        subject: content.subject,
        body: result,
        data: content.data,
        status: 'SUCCESS',
      });

      return info;

    } catch (err) {
      console.error('sendEmail :', err);
      await this.logService.saveEmailResponse({
        emailType: 'EmailVerification',
        base64Data: "No",
        subject: content.subject,
        body: JSON.stringify(err),
        data: JSON.stringify(content.data),
        status: 'FAILED',
      });
      //throw err;
      return { id: "AdminCredentialEmail", "status": "FAILED" };
    }
  }

  async sendSubscriptionStatus(content: SubscriptionStatus) {
    try {
      debug("Entering subscription status sedngrid mail config");
      console.log('Entering sedngrid mail config ');

      const emailTemplate = await EmailTemplate.findOne(content.template);
      const templateId = emailTemplate.template_id;
      console.log('templateId',templateId);

      const result = "";
      const dynamic_template_data = {
        name: content.name,
        email: content.email
      };
      debug("FROM : ", this.config.envConfig.MAIL_FROM_ADDRESS);
      console.log(dynamic_template_data);

      const mail: any = {
        to: content.email,
        subject: content.subject,
        from: this.config.envConfig.MAIL_FROM_ADDRESS,
        templateId,
        //text: 'Hello this is test mail',
        // html: result,
        dynamic_template_data
      };
      /*
      if (content.cc) {
        mail.cc = content.cc;
      }*/
   
      debug("Call subscription status send mail for verify email...");

      const info = await SendgridService.send(mail);

      debug('SendgridService.send result :', info);

      await this.logService.saveEmailResponse({
        //eventEmitter.emit('email.save', {
        emailType: 'SubscriptionStatus',
        base64Data: "No",
        senderEmails: '',
        receiverEmails: content.email,
        subject: content.subject,
        body: result,
        data: content.data,
        status: 'SUCCESS',
      });

      return info;

    } catch (err) {
      console.error('sendEmail :', err);
      await this.logService.saveEmailResponse({
        emailType: 'SubscriptionStatus',
        base64Data: "No",
        subject: content.subject,
        body: JSON.stringify(err),
        data: JSON.stringify(content.data),
        status: 'FAILED',
      });
      //throw err;
      return { id: "SubscriptionStatus", "status": "FAILED" };
    }
  }


  ////////////
  // for adding push notification from admin side
  async addPushNotify(content: AddPushNotifyDto) {
    try {
      debug("Entering adding pushNotify");
      const addPushNotify = new AdminPushNotifiyEntity();
      addPushNotify.title = content.title;
      addPushNotify.message = content.message;
      addPushNotify.target_audience = content.target_audience;
      addPushNotify.notify_date = content.notify_date;
      addPushNotify.notify_time = content.notify_time;
      addPushNotify.notify_date_time = content.notify_date_time;
      addPushNotify.sent_status = 0;
      addPushNotify.created_at = new Date();
      addPushNotify.created_user_id = content.createby;
      await AdminPushNotifiyEntity.save(addPushNotify);
    } catch (err) {
      debug('err in push notification :', err);
      // return { id: "VerificationEmail", "status": "FAILED" };
    }
  }

  // for sending push notification
  async initPushNotify(content: InitPushNotifyDto) {
    try {
      debug("Entering init pushNotify");
      
      // debug("token : ", content.token);
      // const message = {
      //   data: {
      //     score: '850',
      //     time: '2.45',
      //   },
      //   token: content.token
      // };
      

      // // registration token
      // getMessaging().send(message)
      // .then((response) => {
      //   // Response is a message ID string.
      //   debug('Successfully sent push notification message:', response);
      // })
      // .catch((error) => {
      //   debug('Error sending in push notification message:', error);
      // });

      const PushNotifyUserExist = await PushNotificationToUserEntity.findOne({ user_id: content.user_id });
      if(PushNotifyUserExist){
        debug('user exist');
        await PushNotificationToUserEntity.update(
          { user_id: content.user_id },
          {
            membership_type: content.membership_type,
            device_id: content.device_id,
            // message_send_status: 0,
            updated_user_id: content.user_id,
            updated_at: new Date(),
          },
        );
      } else {
        debug('user doesnt exist, create a new user');
        const userPushNotify = new PushNotificationToUserEntity();
        userPushNotify.user_id = content.user_id;
        userPushNotify.membership_type = content.membership_type;
        userPushNotify.device_id = content.device_id;
        userPushNotify.sent_notification_id = [];
        userPushNotify.created_at = new Date();
        userPushNotify.created_user_id = content.user_id;
        await PushNotificationToUserEntity.save(userPushNotify);
        debug('user added into notification table');
      }      
    } catch (err) {
      debug('err in push notification :', err);
    }
  }

  //schedule job for push notification

  // send push notification
  // @Cron('20 * * * * *') // every 20 seconds
  @Cron('*/15 * * * *') // every 15 mints
  async CronSenndPushNotify(noOfRun = 2) {
    for(var run = 1; run <= noOfRun; run++)
    {
      const manualRun = run;
      debug('cron job is running:', manualRun);
      let pushNotifyExist;
      if(manualRun === 1) {
        debug('fectching all data where status = 0')
        const presentTime = new Date().toLocaleString('en-US', {hour12: false});
        debug('presentTime:', presentTime);
        pushNotifyExist = await AdminPushNotifiyEntity
          .createQueryBuilder('p')
          .where('p.notify_date_time < :currentTime', { currentTime: presentTime,})
          .andWhere('p.sent_status = :status', {status: 0,})
          .getMany();
      } else {
        debug('fectching all data b/n 15 mints interval');
        // const currentTime = new Date().toLocaleString();
        const currentTime = new Date().toLocaleString('en-US', {hour12: false});
        const current_ms = new Date().getTime();
        const add5min = new Date(current_ms + (1000*60*15)); // add 15 minutes to current time
        // const futureTime = add5min.toLocaleString();
        const futureTime = add5min.toLocaleString('en-US', {hour12: false});
        // const futureTime = '23/09/2022, 17:55:00';
        debug('cuurent:', currentTime);
        debug('futureTime:', futureTime);

        pushNotifyExist = await AdminPushNotifiyEntity
          .createQueryBuilder('p')
          // .where('"notify_date_time" BETWEEN :currentTime AND :futureTime', {
            .where('p.notify_date_time > :currentTime', { 
            currentTime: currentTime,
            // futureTime: futureTime,
          })
            .andWhere('p.notify_date_time < :futureTime', { 
            // currentTime: currentTime,
            futureTime: futureTime,
          })
          .getMany();
      }
      
      debug('pushNotifyExist:', pushNotifyExist);

      // if record exist in that 30 minutes interval
      if(pushNotifyExist[0]){
        for(var i = 0; i < pushNotifyExist.length; i++)
        {
          const all_membership = pushNotifyExist[i].target_audience;
          const sendingId = pushNotifyExist[i].id;
          const getNotifyTitle = pushNotifyExist[i].title;
          const getNotifyMsg = pushNotifyExist[i].message;
          const getNotifyTime = pushNotifyExist[i].notify_date_time;
          const exactSchedularTime = new Date(getNotifyTime); // convert to UTC format
          debug('all_membership:', all_membership);
          debug('all_membership.length:', all_membership.length);
          debug('all_membership.length[0]:', all_membership[0]);

          const getUserDevice = await PushNotificationToUserEntity
          .createQueryBuilder('p')
          .where("p.membership_type IN (:...users)", { users: all_membership }) // table 1 target audience value should be available in table 2 membership_type
          .andWhere("NOT(:id = ANY(p.sent_notification_id))", { id: sendingId }) // table 1 id should  NOT be available in table 2 sent_notification_id
          .getMany();
          debug('getUserDevice:', getUserDevice);

          // if(getUserDevice) { //if user device exist
          if(getUserDevice[0]) { //if user device exist
            debug('device exist');
            debug('getUserDevice.length:', getUserDevice);
            debug('getUserDevice.length:', getUserDevice);
            var getDeviceId = [];
            
            for(var j = 0; j < getUserDevice.length; j++)
            {
              getDeviceId.push(getUserDevice[j].user_id);
            }
            debug('allDevice:', getDeviceId);
            this.sendPushNotification(getDeviceId, getNotifyTitle, getNotifyMsg, exactSchedularTime, manualRun);

            // update sent_notification_id col in table 2 with table 1 id after sent push notification based on device id
            for(var k = 0; k < getDeviceId.length; k++)
            {
              const deviceExixt = await PushNotificationToUserEntity.findOne({
                where: {
                  user_id: getDeviceId[k],
                }});
        
              if (deviceExixt) {
                // update sent_notification_id  in table 2(PushNotificationToUserEntity)
                let dbSentNotifyIdArr = []
                dbSentNotifyIdArr = deviceExixt.sent_notification_id;
                debug('dbSentNotifyIdArr:', dbSentNotifyIdArr);
                dbSentNotifyIdArr.push(sendingId); //add new token at the beginnig of the array
                debug('afterpush:', dbSentNotifyIdArr);
                debug('device_id:', getDeviceId[k]);
                await PushNotificationToUserEntity.update(
                  { user_id: getDeviceId[k] },
                  { 
                    sent_notification_id: dbSentNotifyIdArr, updated_at: new Date(),
                  });
              }
            }

            // update table 1(push_notify) col sent_status = 1 after sending the notification
            await AdminPushNotifiyEntity.update(
              { id: sendingId },
              { 
                sent_status: 1, updated_at: new Date(),
              });
          }
        }
      }     
      debug('sendPushNotification function ran number:', run);
    }
  }

  async sendPushNotification(device, title, msg, sendTime, cronType) {
    try {
      debug('entered function')
      // var deviceId = [];
      // deviceId.push(device);
       
        // pushNotifyExist = await AdminPushNotifiyEntity
        //   .createQueryBuilder('p')
        //   .where("u.basic IN (:...ids)", { ids: userSelect })
        //   .select(['u.full_name', 'u.mobile', 'u.email', 'u.dob', 'u.membership_type','u.status', 'bank.bank_name','bank.bank_ac_number'] )
        //   .orderBy('u.full_name', 'ASC')
        //   .getMany();


      // debug('after deviceID:', deviceId);
      debug('after device:', device);
      const restAPiKey = this.config.envConfig.ONE_SIGNAL_REST_API_KEY
      const headers = {
        'Content-Type': 'application/json',
        Authorization: `Basic ${restAPiKey}`, // rest api key in one signal a/c
      };

      debug('title:', title);
      const setTiltle = {"en": title};
      const setMsg = {"en": msg};
      let datas = {};
      if(cronType == 1) {
        const current_ms1 = new Date().getTime();
        const add1min = new Date(current_ms1 + (1000*60*1));
        debug('add1min:', add1min);
        debug('manual cron job run');
        datas = {
          app_id: this.config.envConfig.ONE_SIGNAL_APP_ID, //from one signal a/c
          // include_player_ids: deviceId, // from FE 
         // include_player_ids: device, // from FE 
         include_external_user_ids : device,
          contents: setTiltle,
          channel_for_external_user_ids:"push",
          headings: setMsg,
          // data: {'foo': 'bar'},
          // send_after: 'Sept 23 2022 12:00:00 GMT-0700'
          send_after: add1min
        };
      } else {
        debug('automatic cron job run');
        datas = {
          app_id: this.config.envConfig.ONE_SIGNAL_APP_ID, //from one signal a/c
          // include_player_ids: deviceId, // from FE 
          //include_player_ids: device, // from FE 
          include_external_user_ids : device,
          channel_for_external_user_ids:"push",
          contents: setTiltle,
          headings: setMsg,
          // data: {'foo': 'bar'},
          // send_after: 'Sept 23 2022 12:00:00 GMT-0700'
          send_after: sendTime
        };
      }
      

      const addRes = await this.http
        .post(`https://onesignal.com/api/v1/notifications`,
          datas,
          {
            headers,
          },
        )
        .toPromise();
        debug('after hit api:', addRes.data);
        return addRes.data;
    } catch (err) {
      debug('it has err')
      if (
        err &&
        err.response &&
        err.response.status >= 400 &&
        err.response.status < 500
      ) {
        debug('if err');
        throw new HttpException(err.response.data, err.response.status);
      } else {
        debug('else err');
        console.error('login err : ', err);
        throw err;
      }
    }

  }

}
