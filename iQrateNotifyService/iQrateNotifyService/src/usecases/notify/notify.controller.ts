import {
  Controller,
  Body,
  // HttpException,
  UseInterceptors, UploadedFiles, Post, HttpException
} from '@nestjs/common';
import {
  ApiBody,
  ApiConsumes, ApiCreatedResponse, ApiForbiddenResponse,
  // ApiMultiFile,
  ApiTags,
} from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import Debug from 'debug';
// import { conditionalExpression } from '@babel/types';
import { NotifyService } from './notify.service';
import {
  VerifyEmailInfo, VerifyResetPassword, VerifySMSInfo, VerifyResetPasswordSuccess,
  PaymentFailure, EmailBodyType, VerifyPDPA, SentSmsResDto,VerifyAdminCredentialInfo,SubscriptionStatus, InitPushNotifyDto, AddPushNotifyDto
} from './dto/notify.dto';
import { FilesInterceptor } from "@nestjs/platform-express";
import { ApiMultiFile } from "../utils/api_multi_file";
import { AttachmentJSON } from "@sendgrid/helpers/classes/attachment";

const debug = Debug('connector:NotifyController');

@ApiTags('Notification')
@Controller('/notify')
export class NotifyController {
  constructor(private readonly notifyService: NotifyService) { }

  @Post('/send_email_with_attatchments')
  @ApiConsumes('multipart/form-data')
  @ApiMultiFile()
  @UseInterceptors(FilesInterceptor('files'))
  sendEmailWithAttatchments(@UploadedFiles() files: any, @Body() body: any) {
    debug('files:', files);
    debug('body.emailBody : ', body.emailBody);
    let attachments = [];
    for (const file of files) {
      const attachment: AttachmentJSON = {
        filename: file.originalname,
        content: file.buffer.toString('base64'),
        type: file.mimetype,
      };
      attachments.push(attachment);
    }
    let emailBody: EmailBodyType = JSON.parse(body.emailBody);
    emailBody.attachments = attachments;
    debug('emailBody : ', emailBody);
    return this.notifyService.sendEmail(emailBody);
  }


  @Post('/sendVerifyEmail')
  @ApiCreatedResponse({ description: 'Success' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async sendVerifyEmail(@Body() content: VerifyEmailInfo) {
    console.log('Entering Register controller...');
    debug('User Register inputs: ', content);
    console.log('User Register inputs: ', content);
    //return this.userService.register(content);
    return this.notifyService.sendVerifyEmail(content);
    // return true;
  }

  @Post('/sendVerifyMobile')
  @ApiCreatedResponse({
    type: SentSmsResDto,
  })  
  @ApiForbiddenResponse({ description: 'forbidden' })
  async sendVerifyMobile(@Body() content: VerifySMSInfo) {
    debug('sendNotificationMobile content : ', content);
    const emailContent = content;

    try {
      return this.notifyService.sendSMS(emailContent);
    } catch (err) {
      if (err && err.response && err.response.data) {
        new HttpException(err.response.data, err.response.status);
      }
      throw err;
    }
  }

  // @UseGuards(AuthGuard("jwt"))
  @Post('/sendResetpassword')
  async sendResetPassword(@Body() content: VerifyResetPassword) {
    return this.notifyService.sendResetPassword(content);
  }

  // @UseGuards(AuthGuard("jwt"))
  @Post('/sendResetpasswordSuccess')
  async sendResetPasswordSuccess(@Body() content: VerifyResetPasswordSuccess) {
    return this.notifyService.sendResetPasswordSuccess(content);
  }

  // @UseGuards(AuthGuard("jwt"))
  @Post('/sendPaymentFailure')
  async sendPaymentFailure(@Body() content: PaymentFailure) {
    return this.notifyService.sendPaymentFailure(content);
  }

  @Post('/sendPDPA')
  async sendPDPA(@Body() content: VerifyPDPA) {
    debug('send PDPA content : ', content);
    const emailContent = content;

    try {
      return this.notifyService.sendPDPA_SMS(emailContent);
    } catch (err) {
      if (err && err.response && err.response.data) {
        new HttpException(err.response.data, err.response.status);
      }
      throw err;
    }
  }

  @Post('/sendAdminCredential')
@ApiCreatedResponse({ description: 'Success' })
@ApiForbiddenResponse({ description: 'forbidden' })
async sendAdminEmail(@Body() content: VerifyAdminCredentialInfo) {
   debug('User Register inputs: ', content);
  console.log('Adminsendmail inputs: ', content);
  //return this.userService.register(content);
  return this.notifyService.sendAdminCredential(content);
  // return true;
}
 // @UseGuards(AuthGuard("jwt"))
 @Post('/sendSubscriptionStatus')
 async sendSubscriptionStatus(@Body() content: SubscriptionStatus) {
   return this.notifyService.sendSubscriptionStatus(content);
 }

 // push notification
 @Post('/add-push-notification')
  @ApiCreatedResponse({ description: 'Success' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async addPushNotify(@Body() content: AddPushNotifyDto) {
    console.log('Entering push notification...');
    debug('Entering push notification: ', content);
    //return this.userService.register(content);
    return this.notifyService.addPushNotify(content);
    // return true;
  }

 // push notification
 @Post('/init-notification')
  @ApiCreatedResponse({ description: 'Success' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async pushNotify(@Body() content: InitPushNotifyDto) {
    console.log('Entering push notification...');
    debug('Entering push notification: ', content);
    //return this.userService.register(content);
    return this.notifyService.initPushNotify(content);
    // return true;
  }


  @Post('/runCronToSendPushNotification')
  @ApiCreatedResponse({ description: 'Success' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async CronSenndPushNotify() {
    console.log('Entering Register controller...');
    //return this.userService.register(content);
    return this.notifyService.CronSenndPushNotify(1);
    // return true;
  }

}
