import { Test, TestingModule } from '@nestjs/testing';
import { NotifyService } from './notify.service';
import { ConfigService } from '../../config/config.service';
import { NotifyController } from './notify.controller';
import { ConfigModule } from '../../config/config.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LogService } from '../log/log.service';
import Debug from 'debug';
const debug = Debug('connector:UserController:Spec');


describe('NotifyController', () => {
  let notifyController: NotifyController;
  const notifyService = new NotifyService(new ConfigService(), new LogService(new ConfigService()));

  beforeAll(async () => {

    const app: TestingModule = await Test.createTestingModule({
      imports: [TypeOrmModule.forRootAsync({
        imports: [ConfigModule],
        useFactory: (config: ConfigService) => config.getTypeORMConfig(),
        inject: [ConfigService],
      })],
      controllers: [NotifyController],
      providers: [NotifyService]
    }).overrideProvider(NotifyService).useValue(notifyService).compile();

    notifyController = app.get<NotifyController>(NotifyController);
  });


  it('should be defined', () => {
    expect(notifyController).toBeDefined();
  });


  it('Send Verify Mail', async () => {
    const mailInfo = {
      to: "tamilselvan@digitalprizm.net",
      subject: "Email Verification",
      data: "my test data",
      verification_code: "123456"
    };
    const expected = new Promise(function (resolve, reject) {
      resolve({ "id": "VerificationEmail", "status": "SUCCESS" });
    }
    );
    //expect(await controller.sendVerifyEmail(mailInfo)).toEqual(expect.arrayContaining(expected));
    console.log("Unit Testing for verify email");

    let output = await notifyController.sendVerifyEmail(mailInfo).then((info) => {
      console.log("INFO-result");
      let obj = JSON.stringify(info);
      let result = JSON.parse(obj);
      console.log(result.status);

      expect(result.status).toEqual('SUCCESS');
      return info;
    });


    // const output = await controller.sendVerifyEmail(mailInfo);
    //jest.spyOn(service, 'sendVerifyEmail').mockResolvedValue(expected);
    // console.log("print output");
    // console.log(output);

    //  expect(output).resolves.toEqual(expected);

    // console.log(response);
  });


  it('Send Verify Mobile', async () => {
    const mobileInfo = {
      to: "919962679318",
      verification_code: "121212"
    };
    const expected = new Promise(function (resolve, reject) {
      resolve({ "id": "VerificationMobile", "status": "SUCCESS" });
    }
    );
    //expect(await controller.sendVerifyEmail(mailInfo)).toEqual(expect.arrayContaining(expected));
    console.log("Unit Testing for verify mobile");

    const output = await notifyController.sendVerifyMobile(mobileInfo);
    let obj = JSON.stringify(output);
    let result = JSON.parse(obj);
    console.log(result.status);
    expect(result.status).toEqual('SUCCESS');


  });


  it('Send Reset Password', async () => {
    const input = {
      "full_name": "Tamil",
      "to": "tamilselvan@digitalprizm.net",
      "subject": "Reset Password",
      "url": "http://localhost:3001/resetpassword"
    };
    const expected = new Promise(function (resolve, reject) {
      resolve({ "id": "ResetPassword", "status": "SUCCESS" });
    }
    );
    //expect(await controller.sendVerifyEmail(mailInfo)).toEqual(expect.arrayContaining(expected));
    console.log("Unit Testing for Reset password");

    const output = await notifyController.sendResetPassword(input);
    let obj = JSON.stringify(output);
    let result = JSON.parse(obj);
    console.log(result.status);
    expect(result.status).toEqual('SUCCESS');


  });

  it('Send Reset Password Response', async () => {
    const input = {
      "full_name": "Tamil",
      "to": "tamilselvan@digitalprizm.net",
      "subject": "Reset Password Response",
      "url": "http://localhost:3001/login"
    };
    const expected = new Promise(function (resolve, reject) {
      resolve({ "id": "ResetPasswordResponse", "status": "SUCCESS" });
    }
    );
    //expect(await controller.sendVerifyEmail(mailInfo)).toEqual(expect.arrayContaining(expected));
    console.log("Unit Testing for Reset password response");

    const output = await notifyController.sendResetPasswordSuccess(input);
    console.log("OUTPUT");
    console.log(output);
    let obj = JSON.stringify(output);
    let result = JSON.parse(obj);
    console.log(result.status);
    expect(result.status).toEqual('SUCCESS');


  });


  /*
  it('create a user', async () => {
    const dto = {
      full_name: 'susheela',
      email: 'susheela@gmail.com',
      mobile: '1231231',
      phone: '1231231',
      password: 'susheela',
      dob: new Date('1980-09-10'),
      agreed_term: 0,
    };
    const res = await notifyController.register(dto);
    console.log(res);
    expect(res.full_name).toEqual('susheela');
    //const delres = await notifyService.delete(res.id);
    //console.log(delres);
  });
  */

});
