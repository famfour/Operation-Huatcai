import { JsonToHtml } from './json-to-html';

describe('Pug Test', () => {
  it('Generate HTML', () => {
    const resp = JsonToHtml.generate([
      { a: 1, b: 2, c: 3 },
      { a: 4, b: 5, c: 6 },
    ]);

    console.log(resp);

    expect(resp).toContain('<!DOCTYPE html>');
  });
});
