import {ApiBody} from "@nestjs/swagger";
import {VerifyEmailInfo} from "../notify/dto/notify.dto";

export const ApiMultiFile = (fileName: string = 'files'): MethodDecorator => (
    target: any,
    propertyKey: string,
    descriptor: PropertyDescriptor,
) => {
    ApiBody({
        type: 'multipart/form-data',
        required: true,
        schema: {
            type: 'object',
            properties: {
                [fileName]: {
                    type: 'array',
                    items: {
                        type: 'string',
                        format: 'binary',
                    },
                },
                emailBody: {
                    type: 'string'
                }
            },
        },
    })(target, propertyKey, descriptor);
};