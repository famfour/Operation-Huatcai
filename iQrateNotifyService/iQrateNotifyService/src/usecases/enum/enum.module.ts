import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EnumService } from './enum.service';
import Enum from '../../entities/enum';
import { EnumController } from './enum.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Enum])],
  controllers: [EnumController],
  providers: [EnumService],
})
export class EnumModule {}
