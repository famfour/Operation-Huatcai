import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import Enum from '../../entities/enum';

@Injectable()
export class EnumService extends TypeOrmCrudService<Enum> {
  // eslint-disable-next-line @typescript-eslint/no-useless-constructor
  constructor(@InjectRepository(Enum) repo) {
    super(repo);
  }
}
