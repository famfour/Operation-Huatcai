import { ApiProperty } from '@nestjs/swagger';

export class EnumDto {
  @ApiProperty()
  relID: string;

  @ApiProperty()
  version?: string;
}
