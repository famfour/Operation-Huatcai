import {
  BaseEntity,
  Column,
  Entity,
  Index,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({ name: 'enums' })
export default class Enum extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', nullable: true, length: 255 })
  description: string;

  @Column({ type: 'varchar', nullable: false, length: 100, name: 'code_value' })
  codeValue: string;

  @Index()
  @Column({ type: 'varchar', nullable: false, length: 50, name: 'type' })
  type: string;

  @UpdateDateColumn({ type: 'date', name: 'modified_at' })
  modifiedAt: Date;
}
