import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  PrimaryColumn,
} from 'typeorm';

@Entity('push_notification_users', { schema: 'public' })
export class PushNotificationToUserEntity extends BaseEntity {
  @PrimaryColumn('text', { name: 'id' })
  id: number;

  @Column('text', { name: 'user_id', nullable: true })
  user_id: string;

  @Column('text', { name: 'membership_type', nullable: true })
  membership_type: string;

  @Column('text', { name: 'device_id', nullable: true })
  device_id: string;

  @Column('integer', { name: 'sent_notification_id', nullable: true })
  sent_notification_id: number[];

  @Column('text', { name: 'created_user_id', nullable: true })
  created_user_id: string;

  @Column('text', { name: 'updated_user_id', nullable: true })
  updated_user_id: string;

  @Column('timestamp', { name: 'created_at', nullable: true })
  created_at: Date;

  @Column('timestamp', { name: 'updated_at', nullable: true })
  updated_at: Date;
}
