import {
    BaseEntity,
    Column,
    Entity,
    PrimaryColumn,
} from 'typeorm';

@Entity({ name: 'email_templates' })
export default class EmailTemplate extends BaseEntity {
    @PrimaryColumn()
    name: string;

    @Column({ type: 'varchar', nullable: true, length: 255 })
    template: string;

    @Column({ type: 'varchar', nullable: true, length: 255 })
    template_id: string;
}