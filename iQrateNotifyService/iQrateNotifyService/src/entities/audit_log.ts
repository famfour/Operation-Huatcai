import {
    BaseEntity,
    Column,
    CreateDateColumn,
    Entity,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';

@Entity('audit_log', { schema: 'public' })
export class AuditLog extends BaseEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column('text', { name: 'client_ip' })
    clientIp: string | null;

    @Column('text', { name: 'serviceid' })
    serviceId: string;

    @Column('text', { name: 'latency' })
    latency: string | null;

    @Column('text', { name: 'name' })
    name: string | null;

    @Column('text', { name: 'methods' })
    methods: string | null;

    @Column('text', { name: 'querystring' })
    queryString: string | null;

    @Column('text', { name: 'status' })
    status: string | null;

    @Column('int4', { name: 'status_code' })
    statusCode: number | null;

    @Column('text', { name: 'response' })
    response: string | null;
    
    @Column('text', { name: 'url' })
    url: string | null;
    
    @Column('text', { name: 'customerid' })
    customerId: string | null;

    @Column('int8range',{name: 'createdat' })
    createdAt: Date | null;
}
