import { IsEmail, IsNotEmpty, Length } from 'class-validator';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'sms_logs' })
export default class SMSLogs extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'text', name: "sms_type" })
  sms_type: string;

  @Column({ type: 'text', name: "receiver_mobile" })
  @IsNotEmpty({ message: 'Should not be empty' })
  @Length(2, 15)
  receiver_mobile: string;

  @Column({ type: 'text', name: 'data' })
  data: string;

  @CreateDateColumn({ type: 'timestamp', name: 'created_at' })
  created_at: Date;
}
