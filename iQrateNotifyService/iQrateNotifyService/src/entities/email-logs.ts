import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'email_logs' })
export default class EmailLogs extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'text', nullable: true, name: "emailtype" })
  emailType: string;

  @Column({ type: 'text', nullable: true, name: 'base64_data' })
  base64Data: string;

  @Column({ type: 'text', nullable: true, name: 'sender_email' })
  senderEmails: string;

  @Column({ type: 'text', nullable: true, name: 'receiver_emails' })
  receiverEmails: string;

  @Column({ type: 'text', nullable: true, name: 'subject' })
  subject: string;

  @Column({ type: 'text', nullable: true, name: 'body' })
  body: string;

  @Column({ type: 'text', nullable: true, name: 'data' })
  data: string;

  @Column({ type: 'text', nullable: true, name: 'status' })
  status: string;

  @Column({ type: 'text', nullable: true, name: 'attachments' })
  attachments: string;

  @CreateDateColumn({ type: 'timestamp', name: 'created_at' })
  createdAt: Date;
}
