
-- Drop all views to remove the dependancy to change column type
DROP VIEW public.portfolio_product_holding_view_daily;

DROP VIEW public.transactionview;

DROP VIEW public.daily_trans_summary_view;


--Change column type

ALTER TABLE Transaction
ALTER COLUMN transactiondate TYPE date;

ALTER TABLE Transaction
ALTER COLUMN confirmationdate TYPE date;

--Create new Views

CREATE OR REPLACE VIEW public.portfolio_product_holding_view_daily
 AS
 SELECT transaction.portfolioid,
    transaction.investmentproductid,
    transaction.customerid,
    transaction.confirmationdate,
    sum(
        CASE
            WHEN transaction.transactiontype = 'buy'::text AND (lower(transaction.transactionstatus) = ANY (ARRAY['confirmed'::text, 'settled'::text])) THEN transaction.transactionamount
            ELSE 0.00
        END) AS totalinvestedamount,
    sum(
        CASE
            WHEN transaction.transactiontype = 'sell'::text AND (lower(transaction.transactionstatus) = ANY (ARRAY['confirmed'::text, 'settled'::text])) THEN transaction.transactionamount
            ELSE 0::numeric
        END) AS totalredemptionamount,
    sum(
        CASE
            WHEN transaction.transactiontype = 'earnedInterest'::text THEN transaction.transactionamount
            ELSE 0::numeric
        END) AS totalearnedinterest,
    sum(
        CASE
            WHEN lower(btrim(transaction.transactiontype)) = 'sell'::text AND (lower(transaction.transactionstatus) <> ALL (ARRAY['settled'::text, 'payment failed'::text, 'confirmed'::text])) THEN transaction.requestamount
            ELSE 0::numeric
        END) AS totalblockedunits
   FROM transaction
  GROUP BY transaction.portfolioid, transaction.investmentproductid, transaction.customerid, transaction.confirmationdate;

  CREATE OR REPLACE VIEW public.transactionview
 AS
 SELECT transaction.id,
    transaction.transactionrequestid,
    transaction.customerid,
    transaction.goalid,
    transaction.portfolioid,
    transaction.transactionstatus,
    transaction.transactiontype,
    transaction.requestamount,
    transaction.requestunits,
    transaction.transactionamount,
    transaction.transactionunits,
    transaction.transactiondate,
    transaction.investmentpartnerproductid,
    transaction.investmentproductid,
    transaction.switchorredeempartnerproductid,
    transaction.switchorredeemproductid,
    transaction.partnertransactionno,
    transaction.agentid,
    transaction.sortingorder,
    transaction.status,
    transaction.isnotionalamount,
    transaction.cashprocessed,
    transaction.createdat,
    transaction.modifiedat,
    transaction.createdby,
    transaction.modifiedby,
    transaction.batchrequestid,
    transaction.sourceoffund,
    transaction.originoffund,
    transaction.sourceoffundselfdeclare,
    transaction.infoaccuracyselfdeclare,
    transaction.partneraccountno,
    transaction.currency,
    transaction.accountid,
    transaction.transactiontype_ref,
    transaction.queuedat,
    transaction.confirmationdate,
    transaction.redemption_amount_debitat,
    transaction.settlementdate,
    transaction.othersinjson,
    transaction.adminfee,
    to_char(transaction.createdat::timestamp without time zone, 'yyyymm'::text) AS filterbymonth,
    transaction.termsandconditionsflag
   FROM transaction
  WHERE lower(transaction.transactionstatus) = 'settled'::text;

  CREATE OR REPLACE VIEW public.daily_trans_summary_view
 AS
 SELECT transaction.customerid,
    transaction.portfolioid,
    transaction.accountid,
    transaction.investmentproductid,
    to_char(transaction.confirmationdate::timestamp with time zone, 'yyyy-mm-dd'::text) AS confirmedat,
    sum(
        CASE
            WHEN transaction.transactiontype = 'buy'::text THEN transaction.transactionamount
            ELSE NULL::numeric
        END) AS totalinvested,
    sum(
        CASE
            WHEN transaction.transactiontype = 'sell'::text THEN transaction.transactionamount
            ELSE NULL::numeric
        END) AS totalredeemed,
    sum(
        CASE
            WHEN transaction.transactiontype = 'interest_earned'::text THEN transaction.transactionamount
            ELSE NULL::numeric
        END) AS totalinterestearned
   FROM transaction
  WHERE transaction.confirmationdate IS NOT NULL AND (lower(transaction.transactionstatus) = ANY (ARRAY['settled'::text, 'confirmed'::text, 'pending settlement'::text]))
  GROUP BY transaction.customerid, transaction.portfolioid, transaction.accountid, transaction.investmentproductid, (to_char(transaction.confirmationdate::timestamp with time zone, 'yyyy-mm-dd'::text));


