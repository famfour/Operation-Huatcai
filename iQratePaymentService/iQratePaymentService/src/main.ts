import * as http from 'http';
import * as express from 'express';
import * as helmet from 'helmet';
import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import * as cookieParser from 'cookie-parser';
import { ValidationPipe } from '@nestjs/common';
import { ExpressAdapter } from '@nestjs/platform-express';
import { WINSTON_MODULE_NEST_PROVIDER } from 'nest-winston';
import { ConfigService } from './config/config.service';
import { AppModule } from './app.module';
import rawBodyMiddleware from './middlewares/rawBody.middleware';

async function bootstrap() {
  const server = express();
  const configService = new ConfigService();
  const app = await NestFactory.create(AppModule, new ExpressAdapter(server));

  app.useLogger(app.get(WINSTON_MODULE_NEST_PROVIDER));
  app.useGlobalPipes(new ValidationPipe());
  app.use(cookieParser());
  app.use(helmet());
  app.enableCors();
  app.use(rawBodyMiddleware());

  // Swagger UI hosted at /api path
  const swaggerDoc = getSwaggerDoc(app);
  SwaggerModule.setup('api', app, swaggerDoc);

  await app.listen(configService.envConfig.HTTP_PORT);
}

function getSwaggerDoc(app) {
  const config = new DocumentBuilder()
    .setTitle('Payment Service')
    .setDescription('Wrapper for Payment APIs')
    .setVersion('1.0')
    .addBearerAuth()
    .build();

  return SwaggerModule.createDocument(app, config);
}

bootstrap();
