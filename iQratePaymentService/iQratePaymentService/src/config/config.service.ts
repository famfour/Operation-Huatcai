import * as dotenv from 'dotenv';
import * as fs from 'fs';
import { Injectable } from '@nestjs/common';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import * as path from 'path';
import { LoggerOptions } from 'winston';
import { loggerOptions } from './logger-options';

interface IEnvConfigInterface {
  [key: string]: string;
}

@Injectable()
export class ConfigService {
  [x: string]: any;
  public readonly envConfig: IEnvConfigInterface;

  constructor() {
    if (
      !['prod', 'production'].includes(
        (process.env.NODE_ENV || '').toLowerCase(),
      )
    ) {
      const filePath = `${process.env.NODE_ENV || 'dev'}.env`;
      const config = dotenv.parse(fs.readFileSync(filePath));
      ConfigService.validateInput(config);
      this.envConfig = config;
    } else {
      this.envConfig = process.env;
    }
  }

  public getTypeORMConfig(): TypeOrmModuleOptions {
    const baseDir = path.join(__dirname, '../');
    const entitiesPath = `${baseDir}/entities/*{.ts,.js}`;
    return {
      type: 'postgres',
      host: this.envConfig.TYPEORM_HOST,
      username: this.envConfig.TYPEORM_USERNAME,
      password: this.envConfig.TYPEORM_PASSWORD,
      database: this.envConfig.TYPEORM_DATABASE,
      port: Number.parseInt(this.envConfig.TYPEORM_PORT, 10),
      logging: false,
      synchronize: true,
      entities: [entitiesPath],
    };
  }

  public getSmtpConfig() {
    return {
      host: this.envConfig.SMTP_HOST || 'smtp.gmail.com',
      port: this.envConfig.SMTP_PORT || 465,
      secure: this.envConfig.SMTP_ISSECURE || true,
      requireTLS: this.envConfig.SMTP_ISREQUIRETLS || false,
      logger: true,
    };
  }

  public getLoggerConfig(): LoggerOptions {
    return loggerOptions;
  }

  private static validateInput(envConfig: IEnvConfigInterface) {
    this.validateDbConfig(envConfig);
  }

  private static validateDbConfig(envConfig: IEnvConfigInterface) {
    const {
      TYPEORM_HOST,
      TYPEORM_USERNAME,
      TYPEORM_PASSWORD,
      TYPEORM_DATABASE,
      TYPEORM_PORT,
    } = envConfig;

    if (
      !TYPEORM_HOST ||
      !TYPEORM_USERNAME ||
      !TYPEORM_PASSWORD ||
      !TYPEORM_DATABASE ||
      !TYPEORM_PORT
    ) {
      throw new Error('Missing TYPE_ORM Config');
    }
  }



}
