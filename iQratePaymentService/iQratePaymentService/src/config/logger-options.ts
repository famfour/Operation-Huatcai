import { LoggerOptions } from 'winston';
import * as winston from 'winston';

const { format, transports } = winston;

// specify winston logger options here.
export const loggerOptions: LoggerOptions = {
  level: 'info',
  format: format.json(),
  defaultMeta: { service: 'user-service' },
  transports: [
    new transports.Console({
      format: format.combine(
        format.timestamp(),
        format.colorize(),
        format.ms(),
        format.simple(),
      ),
    }),
  ],
};
