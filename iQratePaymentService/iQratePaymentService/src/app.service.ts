import { Injectable } from '@nestjs/common';
import RedisClient from './usecases/utils/redis';
import { ConfigService } from './config/config.service';

@Injectable()
export class AppService {
  constructor(config: ConfigService) {
    RedisClient.init(config);
  }

  getDate(): string {
    return new Date().toString();
  }
}
