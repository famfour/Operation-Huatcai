import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AppService } from './app.service';

@ApiTags('Utils')
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) { }

  @Get('healthCheck')
  getDate(): string {
    return this.appService.getDate();
  }

  @Get('mytest')
  getSDate(): string {
    return "welcome test";
  }
  
}
