import { Injectable, NestMiddleware } from '@nestjs/common';
import { Response, NextFunction } from 'express';

@Injectable()
export class SCBMtls implements NestMiddleware {
  use(request: any, response: Response, next: NextFunction) {
    // const certificate = request.socket
    //   .getPeerCertificate(true)
    //   .raw.toString('base64');

    // console.log({ certificate })

    const req = request;
    console.log(`${new Date()} ${req.ip} ${+' '}${req.method} ${req.url}`);

    return next();
  }
}
