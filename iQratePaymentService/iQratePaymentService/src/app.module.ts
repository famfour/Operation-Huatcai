import { HttpModule, MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { WinstonModule } from 'nest-winston';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigService } from './config/config.service';
// import { EBBSService } from './usecases/scb/ebbs/ebbs.service';
/* ------------------------------- Middlewares ------------------------------- */
import { HTTPLogger } from './middlewares/logger';
import { Auth } from './middlewares/auth';

/* --------------------------------- Modules --------------------------------- */
import { ConfigModule } from './config/config.module';
import { UtilModule } from './usecases/utils/util.module';
// import { AuthModule } from './usecases/scb/auth/auth.module';
import { PaymentModule } from './usecases/payment/payment.module';
// import * as Joi from '@hapi/joi';

@Module({
  imports: [
    EventEmitterModule.forRoot(),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (config: ConfigService) => config.getTypeORMConfig(),
      inject: [ConfigService],
    }),
    ConfigModule,
    UtilModule,
    WinstonModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (config: ConfigService) => config.getLoggerConfig(),
      inject: [ConfigService],
    }),
    PaymentModule,
    HttpModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  async configure(consumer: MiddlewareConsumer) {
    consumer.apply(HTTPLogger).forRoutes('*');
    consumer.apply(Auth).forRoutes('/scb');
    consumer.apply(Auth).forRoutes('/tnc');
  }
}
