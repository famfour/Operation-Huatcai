import * as pug from 'pug';

export class JsonToHtml {
  public static generate(data: any) {
    const template = JsonToHtml.getTemplate();

    if (typeof data !== 'object') {
      console.log('Input is neither Object or Array. Returning as it is.');
      return data;
    }

    const formattedData = JsonToHtml.getFormattedData(data);

    const fn = pug.compile(template);
    return fn(formattedData);
  }

  private static getFormattedData(data: any) {
    const dataArr = [].concat(data);

    const headers = [];
    // eslint-disable-next-line guard-for-in
    for (const key in dataArr[0]) {
      headers.push(key);
    }

    const body = [];
    for (const item of dataArr) {
      const ItemArray = [];
      for (const headerItem of headers) {
        ItemArray.push(item[headerItem]);
      }
      body.push(ItemArray);
    }

    return { headers, body };
  }

  // Do not change the indentation for refactoring. This is the only indentation with works for this current template
  private static getTemplate() {
    return `doctype html
html(lang="en")
            head
                title= 'Debug Mode'
            body
                main
                    .container
                        table.table(style='width:100%', border='1')
                            tr
                                each val in headers
                                    th #{val}
                            each val in body
                                tr
                                    each row in val
                                        td #{row}`;
  }
}
