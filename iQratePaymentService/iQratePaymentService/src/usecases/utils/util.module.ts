import { Global, Module } from '@nestjs/common';
import { UtilService } from './util.service';
import { ConfigService } from '../../config/config.service';

@Global()
@Module({
  providers: [
    {
      provide: UtilService,
      useValue: new UtilService(new ConfigService()),
    },
  ],
  exports: [UtilService],
})
export class UtilModule { }
