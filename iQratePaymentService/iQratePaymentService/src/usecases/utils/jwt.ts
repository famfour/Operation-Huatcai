import * as jwt from 'jsonwebtoken';
import jwtDecode from 'jwt-decode';

const ALGO = 'ES256';

export namespace JWT {
  export function encodeAndSign<T>(privateKeyB64: string, data: T): string {
    const privateKey = Buffer.from(privateKeyB64, 'base64').toString();
    const token = jwt.sign(data as any, privateKey, {
      algorithm: ALGO,
    });

    return token;
  }

  export function decodeAndVerify<T>(publicKeyB64: string, token: string): T {
    const publicKey = Buffer.from(publicKeyB64, 'base64').toString();
    const data = jwt.verify(token, publicKey, { algorithms: [ALGO] }) as T;

    return data;
  }

  export function decode<T>(token: string): T {
    const data = jwtDecode<T>(token);
    return data;
  }

  export function decodeHeader(token: string): jwt.JwtHeader {
    const data = jwtDecode<jwt.JwtHeader>(token, { header: true });
    return data;
  }
}
