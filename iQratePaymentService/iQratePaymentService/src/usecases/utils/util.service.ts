import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { ConfigService } from '../../config/config.service';
import { getConnection } from 'typeorm';
import Debug from 'debug';
import Stripe from 'stripe';
import { prependOnceListener } from 'process';

const debug = Debug('connector:Stripe');

@Injectable()
export class UtilService {

    constructor(
        private config: ConfigService
    ) {

    }
    /**
     * Get date format
     * @param timestamp
     * @returns 
     */
    public getDate(timestamp: any) {
        /*
        const date = new Date(timestamp);
        var dt = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
        // var date1 = new Date(today);
        return dt;
        */

        const milliseconds = timestamp * 1000
        /*
        const date = new Date(milliseconds)
        
        const humanDateFormat = date.toLocaleString() //2019-12-9 10:30:15
        console.log("dateformat", humanDateFormat);
        return humanDateFormat;
        */

        var d_t = new Date(milliseconds);
        let year = d_t.getFullYear();
        let month = ("0" + (d_t.getMonth() + 1)).slice(-2);
        let day = ("0" + d_t.getDate()).slice(-2);
        let hour = d_t.getHours();
        let minute = d_t.getMinutes();
        let seconds = d_t.getSeconds();

         var res = day + "/" + month + "/" + year + " " + hour + ":" + minute + ":" + seconds;   
         console.log("res date",res);
         return res;
       // const date = new Date(inputDate);
        /*
        //extract the parts of the date
        const day = date.getDate();
        const month = date.getMonth() + 1;
        const year = date.getFullYear();    
    
        var format = 'MM-dd-yyyy';
        //replace the month
        format = format.replace("MM", month.toString().padStart(2,"0"));        
    
        //replace the year
        if (format.indexOf("yyyy") > -1) {
            format = format.replace("yyyy", year.toString());
        } else if (format.indexOf("yy") > -1) {
            format = format.replace("yy", year.toString().substr(2,2));
        }
    
        //replace the day
        format = format.replace("dd", day.toString().padStart(2,"0"));
        console.log("dateformat", format);
        return format;
        */


    }
}
