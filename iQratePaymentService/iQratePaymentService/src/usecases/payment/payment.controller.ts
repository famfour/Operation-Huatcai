import { Controller, Get, UseGuards, Post, Body, Put, Param, Patch, Headers, Request, Req, BadRequestException } from '@nestjs/common';
import {
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import Debug from 'debug';
// import { conditionalExpression } from '@babel/types';
import { PaymentService } from './payment.service';
import { WebhookService } from './webhook.service';
import {
  createCustomerDto, chargeCustomerDto, updateCustomerDto,
  createPaymentMethod, detachPaymentMethod,
  createSubscription,
  setPaymentMethod, updatePaymentMethod, setUpgradeDowngrade,
  WebhookDto, ListPaymentMethod, SubscriptionPreview, ListCards
} from './dto/payment.dto';
import RequestWithRawBody from '../../middlewares/requestWithRawBody.interface';
const debug = Debug('connector:PaymentController');

@ApiTags('Payment')
@Controller('/')
export class PaymentController {
  constructor(private readonly paymentService: PaymentService, private readonly webhookService: WebhookService) { }

  /**
   * Creating stripe customers
   * @param createCustomerDto 
   * @returns 
   */
  @Post('customer')
  @ApiCreatedResponse({ description: 'Create Customer' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async createCustomer(@Body() content: createCustomerDto) {
    debug('Create customer inputs: ', content);
    return this.paymentService.createCustomer(content);
  }
  /**
   * Get customer details
   * @param customer_id 
   * @returns 
   */
  @Get('customer/:id')
  @ApiCreatedResponse({ description: 'Get Customer' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async getCustomer(@Param('id') customer_id: string) {
    debug('Get customer inputs: ', customer_id);
    return this.paymentService.getCustomer(customer_id);
  }

  /**
  * Update customer details
  * @param updateCustomerDto 
  * @returns 
  */
  @Put('customer/:id')
  @ApiCreatedResponse({ description: 'Update Customer' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async updateCustomer(@Param('id') customer_id: string, @Body() content: updateCustomerDto) {
    debug('Update customer inputs: ', customer_id);
    return this.paymentService.updateCustomer(customer_id, content);
  }

  /**
   * Charging customer payment
   * @param content 
   * @returns 
   */
  /*
  @Post('charge-customer')
  @ApiCreatedResponse({ description: 'Charge Customer' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async chargeCustomer(@Body() content: chargeCustomerDto) {
    debug('Charge customer inputs: ', content);
    return this.paymentService.chargeCustomer(content);
  }
*/
  /**
 * List All Products
 * @returns 
 */
  @Get('products')
  @ApiCreatedResponse({ description: 'List products' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async listProducts() {
    return this.paymentService.listProducts();
  }

  /**
* List All plans
* @returns 
*/
  /*
    @Get('plans')
    @ApiCreatedResponse({ description: 'List plans' })
    @ApiForbiddenResponse({ description: 'forbidden' })
    async listPlans() {
      return this.paymentService.listPlans();
    }
  */
  /**
* Create Payment method
* @param content 
* @returns 
*/
  @Post('payment-method')
  @ApiCreatedResponse({ description: 'Create payment method' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async createPaymentMethod(@Body() content: createPaymentMethod) {
    return this.paymentService.createPaymentMethod(content);
  }

  /**
  * Attach Payment method to customer
  * @param content 
  * @returns 
  */

  @Post('set-payment-method')
  @ApiCreatedResponse({ description: 'Set payment method' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async setPaymentMethod(@Body() content: setPaymentMethod) {
    return this.paymentService.setPaymentMethod(content);
  }

  /**
  * List Payment methods
  * @param content 
  * @returns 
  */
  @Post('payment-methods')
  @ApiCreatedResponse({ description: 'List payment method' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async listPaymentMethods(@Body() content: ListPaymentMethod) {
    return this.paymentService.listPaymentMethods(content.customer_id, content.payment_method_id);
  }
  /**
 * List cards
 * @param content 
 * @returns 
 */
  @Post('cards')
  @ApiCreatedResponse({ description: 'List cards' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async listCards(@Body() content: ListCards) {
    return this.paymentService.listCards(content.customer_id);
  }
  /**
  * Get default Payment method
  * @param content 
  * @returns 
  */
  @Post('customer')
  @ApiCreatedResponse({ description: 'Get customer' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async getSubscriptionCustomer(@Body() content: any) {
    return this.paymentService.getSubscriptionCustomer(content.customer_id);
  }
  /**
  * Detach Payment method to customer
  * @param content 
  * @returns 
  */
  @Post('remove-payment-method')
  @ApiCreatedResponse({ description: 'Set payment method' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async detachPaymentMethod(@Body() content: detachPaymentMethod) {
    return this.paymentService.detachPaymentMethod(content);
  }

  /**
  * Update Payment method
  * @param content 
  * @returns 
  */
  @Post('payment-method/:id')
  @ApiCreatedResponse({ description: 'Create payment method' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async updatePaymentMethod(@Param('id') payment_method_id: string, @Body() content: updatePaymentMethod) {
    return this.paymentService.updatePaymentMethod(payment_method_id, content);
  }


  /**
 * List Coupons
 * @returns 
 */
  /*
    @Get('coupons')
    @ApiCreatedResponse({ description: 'List Coupons' })
    @ApiForbiddenResponse({ description: 'forbidden' })
    async getCoupons() {
      return this.paymentService.getCoupons();
    }
  */
  /**
 * Create Subscriptions
 * @param content 
 * @returns 
 */
  @Post('subscription')
  @ApiCreatedResponse({ description: 'Create subscription' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async createSubscription(@Body() content: createSubscription) {
    return this.paymentService.createSubscription(content);
  }

  /**
  * Get Subscriptions
  * @param content 
  * @returns 
  */
  @Get('subscription/:id')
  @ApiCreatedResponse({ description: 'Get subscription' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async getSubscription(@Param('id') subscription_id: string) {
    return this.paymentService.getSubscription(subscription_id);
  }
  /**
    * List All Subscriptions
    * @returns 
    */
  @Get('subscriptions/:id')
  @ApiCreatedResponse({ description: 'List subscriptions' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async listSubscriptions(@Param('id') customer_id: string) {
    return this.paymentService.listSubscriptions(customer_id);
  }
  /**
   * Get Invoices
   * @returns 
   */
  @Get('invoices/:id?')
  @ApiCreatedResponse({ description: 'Get Invoices' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async getInvoices(@Param('id') customer_id: string, @Req() req: Request) {
    return this.paymentService.getInvoices(customer_id, req);
  }

  /**
  * Get Invoices Details
  * @returns 
  */
  @Get('invoice/:id')
  @ApiCreatedResponse({ description: 'Get Invoice details' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async getInvoiceDetails(@Param('id') invoice_id: string) {
    return this.paymentService.getInvoiceDetails(invoice_id);
  }

  /**
 * Get Payments
 * @param customer_id
 * @returns 
 */
  @Get('payments/:id')
  @ApiCreatedResponse({ description: 'Get Invoices' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async getPayments(@Param('id') customer_id: string, @Req() req: Request) {
    return this.paymentService.getPayments(customer_id, req);
  }

  /**
 * Get payment details
 * @param payment_id
 * @returns 
 */
  @Get('payment/:id')
  @ApiCreatedResponse({ description: 'Get Payment details' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async getPaymentDetails(@Param('id') payment_id: string) {
    return this.paymentService.getPaymentDetails(payment_id);
  }

  /**
  * Get transactions
  * @returns 
  */
  @Get('transaction/:id')
  @ApiCreatedResponse({ description: 'Get Transactions' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async getTransaction(@Param('id') transaction_id: string) {
    return this.paymentService.getTransaction(transaction_id);
  }
  /**
 * Upgrade & downgrade Subscriptions
 * @param content 
 * @returns 
 */
  @Post('upgrade-downgrade/:id')
  @ApiCreatedResponse({ description: 'Upgrade / downgrade subscription' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async setUpgradedowngrade(@Param('id') customer_id: string, @Body() content: setUpgradeDowngrade) {
    return this.paymentService.setUpgradeDowngrade(customer_id, content);
  }

  /**
   * Get transaction response from stripe using webhook
   * @param content 
   * @returns 
   */
  @Post('webhook')
  @ApiCreatedResponse({ description: 'Webhook' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async webhook(@Headers('stripe-signature') signature: string,
    @Req() request: RequestWithRawBody) {
    if (!signature) {
      throw new BadRequestException('Missing stripe-signature header');
    }
    return await this.webhookService.webhook(signature, request.rawBody);
  }

  /**
* Get transaction response from stripe using webhook
* @param content 
* @returns 
*/
  /*
    @Post('webhook')
    @ApiCreatedResponse({ description: 'Webhook' })
    @ApiForbiddenResponse({ description: 'forbidden' })
    async webhook(@Body() content: WebhookDto) {
      return await this.webhookService.stripeMonitor(content.signature, content.payload);
    }
    */

  /**
   * SubscriptionsPreview
   * @param content 
   * @returns 
   */
  @Post('preview')
  @ApiCreatedResponse({ description: 'Upgrade / downgrade subscription' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async preview(@Body() content: SubscriptionPreview) {
    return this.paymentService.preview(content);
  }
 /**
 * Get product name
 * @param product_id
 * @returns 
 */
  @Get('get-product-name/:id')
  @ApiCreatedResponse({ description: 'Get product name' })
  @ApiForbiddenResponse({ description: 'forbidden' })
  async getProductName(@Param('id') product_id: string,) {
    return this.paymentService.getProductName(product_id);
  }
}
