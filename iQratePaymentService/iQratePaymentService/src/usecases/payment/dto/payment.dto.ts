import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsEmail, IsInt, IsNotEmpty, Length, IsEnum } from 'class-validator';

export enum PaymentMethod {
  card = 'card',
  acss_debit = 'acss_debit'
}

export enum CollectionMethod {
  charge_automatically = 'charge_automatically',
  send_invoice = 'send_invoice'
}

export class createCustomerDto {
  [x: string]: any;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    type: String,
    description: 'name',
    default: 'Tamilstripe'
  })
  name: string;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'email',
    default: 'tamilselvan@digitalprizm.net'
  })
  email: string;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'user_id',
    default: '1'
  })
  user_id: string;

  @ApiProperty({
    description: 'phone',
    default: '9962679318'
  })
  phone: string;

}

export class updateCustomerDto {
  [x: string]: any;

  @ApiProperty({
    type: String,
    description: 'name',
    default: ''
  })
  name: string;

  @ApiProperty({
    description: 'email',
    default: ''
  })
  email: string;

  @ApiProperty({
    description: 'phone',
    default: ''
  })
  phone: string;


}

export class customerDto {
  [x: string]: any;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    type: String,
    description: 'customer_id',
    default: 'cus_L5ChpsVGQGoGTL'
  })
  customer_id: string;

}

export class chargeCustomerDto {
  [x: string]: any;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    type: Number,
    description: 'amount',
    default: '1'
  })
  amount: number;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'customer_id',
    default: 'cus_L5ChpsVGQGoGTL'
  })
  customer_id: string;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'payment_method_id',
    default: 'testpay12'
  })
  payment_method_id: string;


  @ApiProperty({
    description: 'confirm',
    default: true
  })
  confirm: boolean;

}

export class createPaymentMethod {
  [x: string]: any;

  // @IsNotEmpty({ message: 'Should not be empty' })
  @IsEnum(PaymentMethod)
  @ApiProperty({
    description: 'type',
    default: 'card'
  })
  type: PaymentMethod;

  // @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'card_number',
    default: '4242424242424242'
  })
  card_number: string;

  @ApiProperty({
    description: 'card_exp_month',
    default: 5
  })
  card_exp_month: number;

  // @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'card_exp_year',
    default: 2022
  })
  card_exp_year: number;

  // @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'card_cvc',
    default: '314'
  })
  card_cvc: string;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'customer_id',
    default: 'cus_L5ChpsVGQGoGTL'
  })
  customer_id: string;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'payment_method_token',
    default: ''
  })
  payment_method_token: string;

}

export class updatePaymentMethod {
  [x: string]: any;

  @IsNotEmpty({ message: 'Type should not be empty' })
  @IsEnum(PaymentMethod)
  @ApiProperty({
    description: 'type',
    default: 'card'
  })
  type: string;

  @IsNotEmpty({ message: 'Id should not be empty' })
  @ApiProperty({
    description: 'customer_id',
    default: '4242424242424242'
  })
  customer_id: string;

  @IsNotEmpty({ message: 'Token should not be empty' })
  @ApiProperty({
    description: 'payment_method_token',
    default: 5
  })
  payment_method_token: string;

}

export class setPaymentMethod {
  [x: string]: any;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'customer_id',
    default: 'cus_L5ChpsVGQGoGTL'
  })
  customer_id: string;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'payment_method_id',
    default: 'pm_1KQVgtB9aacV2G5wr96Z9Pyg'
  })
  payment_method_id: string;

}
export class ListPaymentMethod {
  [x: string]: any;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'customer_id',
    default: 'cus_L5ChpsVGQGoGTL'
  })
  customer_id: string;


  @ApiProperty({
    description: 'payment_method_id',
    default: 'pm_1KQVgtB9aacV2G5wr96Z9Pyg'
  })
  payment_method_id?: string;

}

export class ListCards {
  [x: string]: any;

  @IsNotEmpty({ message: 'Customer id should not be empty' })
  @ApiProperty({
    description: 'customer_id',
    default: 'cus_L5ChpsVGQGoGTL'
  })
  customer_id: string;


}

export class detachPaymentMethod {
  [x: string]: any;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'customer_id',
    default: 'cus_L5ChpsVGQGoGTL'
  })
  customer_id: string;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'payment_method_id',
    default: 'pm_1KQVgtB9aacV2G5wr96Z9Pyg'
  })
  payment_method_id: string;

}

export class createSubscription {
  [x: string]: any;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'customer_id',
    default: 'cus_L5ZbSNa8fBeMIm'
  })
  customer_id: string;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'price_id',
    default: 'price_1KOzL6B9aacV2G5wfgJWIwEJ'
  })
  price_id: string;

  @IsNotEmpty({ message: 'Should not be empty' })
  @IsEnum(CollectionMethod)
  @ApiProperty({
    description: 'collection_method',
    default: 'charge_automatically'
  })
  collection_method: CollectionMethod;

  @ApiProperty({
    description: 'promo_code',
    default: "FEST"
  })
  promo_code: string;

  @ApiProperty({
    description: 'product',
    default: "basic"
  })
  product: string;


}
export class setUpgradeDowngrade {
  [x: string]: any;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'price_id',
    default: 'price_1KRC91B9aacV2G5wcNxyXxht'
  })
  price_id: string;
}
export class WebhookDto {
  [x: string]: any;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'signature',
    default: 'KRC91B9aacV2G5wcNxyXxht'
  })
  signature: string;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'payload',
    default: 'price_1KRC91B9aacV2G5wcNxyXxht'
  })
  payload: Buffer;

}
export class SubscriptionPreview {
  [x: string]: any;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'customer_id',
    default: 'cus_LNx0g8uQ7qmPjt'
  })
  customer_id: string;

  @ApiProperty({
    description: 'subscription_id',
    default: 'sub_1KhBHCFSgVYpzNjn6A2lAylx'
  })
  subscription_id?: string;

  @IsNotEmpty({ message: 'Should not be empty' })
  @ApiProperty({
    description: 'price_id',
    default: 'price_1KRC91B9aacV2G5wcNxyXxht'
  })
  price_id: string;

}

