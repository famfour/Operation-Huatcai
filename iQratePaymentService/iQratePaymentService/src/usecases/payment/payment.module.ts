import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { PaymentController } from './payment.controller';
import { PaymentService } from './payment.service';
import { UtilService } from '../utils/util.service';
import { WebhookService } from './webhook.service';
import { ConfigModule } from '../../config/config.module';
import { ConfigService } from '../../config/config.service';


@Module({
  imports: [ConfigModule, HttpModule],
  controllers: [PaymentController],
  providers: [PaymentService, WebhookService, UtilService],
  exports: [PaymentService]
})
export class PaymentModule { }
