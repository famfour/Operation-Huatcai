import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { ConfigService } from '../../config/config.service';
import { getConnection } from 'typeorm';
import Debug from 'debug';
import Stripe from 'stripe';
import {
  createCustomerDto, chargeCustomerDto, updateCustomerDto, createSubscription,
  createPaymentMethod, PaymentMethod, setPaymentMethod, CollectionMethod,
  updatePaymentMethod, detachPaymentMethod, setUpgradeDowngrade
} from './dto/payment.dto';
// import { UserEntity } from '../../entities/users';
// import { SubscriptionEntity } from '../../entities/subscriptions';
// import { SubscriptionItemEntity } from '../../entities/subscription_items';
import { prependOnceListener } from 'process';
import { UtilService } from '../utils/util.service';
import { networkInterfaces } from 'os';


const debug = Debug('connector:PaymentService');

@Injectable()
export class PaymentService {
  private stripe: Stripe;


  constructor(
    private config: ConfigService,
    private util: UtilService
  ) {
    this.stripe = new Stripe(config.envConfig.STRIPE_SECRET, {
      apiVersion: '2020-08-27',
    });

    console.log(config.envConfig.STRIPE_SECRET);
  }
  /**
   * Creating customer in stripe
   * @param createCustomerDto 
   * @returns 
   */
  public async createCustomer(param: createCustomerDto) {

    const name = param.name;
    const email = param.email;

    try {

      const res = await this.stripe.customers.create({
        name,
        email
      });

      const result = {
        "stripe_id": res.id
      }
      return result;

    } catch (err) {
      debug('err:', err);
      if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Create Customer err : ', err);
        throw err;
      }

    }

  }
  /**
   * Get the customer
   * @param customerId 
   * @returns 
   */
  public async getCustomer(customer_id: string) {

    try {

      debug('Customer ID:', customer_id);

      const res = await this.stripe.customers.retrieve(customer_id);

      return res;

    } catch (err) {
      debug('err:', err);
      if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }

    }

  }

  /**
   * Update Customer
   * @param customer_id 
   * @param updateCustomerDto 
   * @returns 
   */
  public async updateCustomer(customer_id: string, param: updateCustomerDto) {

    const phone = param.phone;
    const name = param.name;
    const email = param.email;
    try {

      let data = {};
      if(email!="") {
        data['email'] = email;
      }
      if(phone!="") {
        data['phone'] = phone;
      }
      if(name!="") {
        data['name'] = name;
      }

      const res = await this.stripe.customers.update(
        customer_id,
        data
      );


      return res;

    } catch (err) {
      debug('err:', err);
      if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }

  }



  /**
   * Charging the customer
   * @param chargeCustomerDto 
   * @returns 
   */
  /*
  public async chargeCustomer(param: chargeCustomerDto) {
    const amount = param.amount;
    const customer_id = param.customer_id;
    const payment_method_id = param.payment_method_id;
    const confirm = param.confirm;
    try {

      const res = await this.stripe.paymentIntents.create({
        amount,
        customer: customer_id,
        payment_method: payment_method_id,
        currency: this.configService.envConfig.STRIPE_CURRENCY,
        confirm: confirm
      })

      return { status: "Success", data: res };

    } catch (err) {
      debug('err:', err);
      return { status: "Fail", data: null };
    }


  }
  */
  /**
   * List All Products
   * @returns 
   */
  public async listProducts() {

    try {

      const resProducts = await this.stripe.products.list(
        {
          active:true
        }
      );
      const stripe = this.stripe;

    //  console.log(resProducts);

      let result = [];

      for (const product of resProducts.data) {

        let proList = {
          product: product.name,
          id: product.id,
          plan: null

        }
        const resPlans = await stripe.plans.list(
          { product: product.id,
            active:true
          }
        );
          console.log("products list = ",resPlans);
        let planList = [];
        resPlans.data.forEach(async function (plan) {
          planList.push(
            {
              plan_id: plan.id,
              plan: plan.interval,
              amount: plan.amount
            }
          )

        });

        proList.plan = planList;

        /*
        await Promise.all(resPlans.data.map(async (plan) => {
          console.log(plan.id)
        }));
*/
        result.push(proList);
      }

      console.log(result);
      console.log("COMPLETED");
      return result;

    } catch (err) {
      debug('err:', err);
      if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }


  }
  /**
   * Get Basic plan Id
   * @returns 
   */
   public async getBasicPlanId() {

    try {

    
      const resProducts = await this.stripe.products.list(
        {
          active:true,
          
        }
      );
      const stripe = this.stripe;

    //  console.log(resProducts);
        var plan_id="";
   
      for (const product of resProducts.data) {

        let proList = {
          product: product.name,
          id: product.id,
          plan: null

        }
        if(product.name=='Basic') {
          const resPlans = await stripe.plans.list(
            { product: product.id,
              active:true,
            }
          );
          // console.log("products list = ",resPlans);
            
          resPlans.data.forEach(async function (plan) {
            if(plan.interval=='month') {
              plan_id = plan.id;
              console.log("inside plan_id",plan_id);
              return false; 
            }
            
          });
      }
        

      }

      console.log("COMPLETED",plan_id);
      return plan_id;

    } catch (err) {
      debug('err:', err);
      if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }


  }
  /**
   * List All subscriptions
   * @returns 
   */
  public async listSubscriptions(customer_id) {

    try {

      const res = await this.stripe.subscriptions.list(
        { customer: customer_id }
      );

      return res;

    } catch (err) {
      debug('err:', err);
      if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }


  }

  /**
   * List All Planes
   * @returns 
   */
  /*
  public async listPlanes() {

    try {

      const res = await this.stripe.plans.list();

      return { status: "Success", data: res };

    } catch (err) {
      debug('err:', err);
      if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }


  }
*/
  /**
     * Create payment method
     * @param createPaymentMethod 
     * @returns 
     */
  public async createPaymentMethod(param: createPaymentMethod) {

    try {
      /*
      const res = await this.stripe.paymentMethods.create(
        {
          type: param.type,
          card: {
            number: param.card_number,
            exp_month: param.card_exp_month,
            exp_year: param.card_exp_year,
            cvc: param.card_cvc
          }

        }
      );
       */
      const customer_id = param.customer_id;
      const payment_method_token = param.payment_method_token;

      const res = await this.stripe.setupIntents.create({
        customer: customer_id,
        payment_method: payment_method_token
      });

      const resConfirm = await this.stripe.setupIntents.confirm(res.id, {
        payment_method: payment_method_token
      });

      debug("payment method id", resConfirm.payment_method.toString());
      console.log(resConfirm.payment_method.toString());

      const setInput = {
        customer_id: customer_id,
        payment_method_id: resConfirm.payment_method.toString()
      };

      const resAttach = await this.setPaymentMethod(setInput);


      return resAttach;

    } catch (err) {
      debug('err:', err);
      if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
        throw new HttpException("Error", err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }


  }
  /**
    * Update payment method
    * @param paymentMethodId 
    * @param content 
    * @returns 
    */
  public async updatePaymentMethod(payment_method_id: string, param: updatePaymentMethod) {

    try {

      const res = await this.stripe.setupIntents.update(
        payment_method_id,
        {
          customer: param.customer_id,
          payment_method: param.payment_method_token
        }
      );

      const resConfirm = await this.stripe.setupIntents.confirm(res.id, {
        payment_method: param.payment_method_token
      });

      return res;

    } catch (err) {
      //console.log("error type", err.type);
      debug('err:', err);
      if (err.type) {
        throw new HttpException(err.type + ": " + err.raw.message, err.raw.statusCode);
      }
      if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Update payment err : ', err);
        throw err;
      }
    }


  }
  /**
     * Set payment method to customer
     * @param amount 
     * @param paymentMethodId 
     * @param customerId 
     * @returns 
     */
  public async setPaymentMethod(param: setPaymentMethod) {

    const customer_id = param.customer_id;
    const payment_method_id = param.payment_method_id;

    try {

      const res = await this.stripe.paymentMethods.attach(
        payment_method_id,
        {
          customer: customer_id
        }

      );
      debug("Attach response", res);

      const customerUpdate = await this.stripe.customers.update(
        customer_id,
        {
          invoice_settings: {
            default_payment_method
              : payment_method_id
          }
        }
      );

      return customerUpdate;

    } catch (err) {
      debug('err:', err);
      if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }


  }
  /**
     * Get customer with payment method
     * @param customerId 
     * @returns 
     */
  public async getSubscriptionCustomer(customer_id) {


    try {

      const customer = await this.stripe.customers.retrieve(
        customer_id
      );
      console.log("customer", customer);
      if (customer) {
        return customer;
      }
      debug("Attach response", customer);

      return "There is no customer available";

    } catch (err) {
      debug('err:', err);
      if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }


  }
  /**
   * List All Payment methods
   * @returns 
   */
  public async listPaymentMethods(customer_id, payment_method_id) {

    try {
      /*
      const res = await this.stripe.paymentIntents.list({
        customer: customer_id,
        limit: 3,
      });
      */
      const res = await this.stripe.customers.listPaymentMethods(
        customer_id,
        { type: 'card' }
      );

      return res;

    } catch (err) {
      debug('err:', err);
      if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }


  }
  /**
   * List cards
   * @returns 
   */
  public async listCards(customer_id) {

    try {

      const res = await this.stripe.setupIntents.list(
        {
          customer: customer_id
        }
      );

      return res;

    } catch (err) {
      debug('err:', err);
      if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }


  }
  /**
   * Get Payment method
   * @returns 
   */
  public async getPaymentMethod(customer_id, payment_method_id) {

    try {

      const res = await this.stripe.paymentMethods.retrieve(
        payment_method_id
      );

      return res;

    } catch (err) {
      debug('err:', err);
      if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }


  }
  /**
     * Detach payment method to customer
     * @param paymentMethodId 
     * @param customerId 
     * @returns 
     */
  public async detachPaymentMethod(param: detachPaymentMethod) {

    const customer_id = param.customer_id;
    const payment_method_id = param.payment_method_id;

    try {

      const res = await this.stripe.paymentMethods.detach(
        payment_method_id
      );

      return res;

    } catch (err) {
      debug('err:', err);
      if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }


  }
  /**
   * Creating subscriptions
   * @param createSubscription 
   * @returns 
   */
  public async createSubscription(param: createSubscription) {
    try {

      console.log("get basic plan id");
      var price_id = await this.getBasicPlanId();
      if(price_id==""){
         price_id= this.config.envConfig.BASIC_PRICE_ID;
      }
      console.log("under create subscription", price_id );
      
      //let coupon_id = "";
      let promotion_code_id = "";
      if (param.promo_code != "") {
        const promotionCodes = await this.stripe.promotionCodes.list({
          code: param.promo_code
        });
        if (promotionCodes.data[0]) {
          promotion_code_id = promotionCodes.data[0].id;
        }
        // coupon_id = promotionCodes.data[0].coupon.id;
      }
   
     // var price_id= this.config.envConfig.BASIC_PRICE_ID;

      var input = {
        customer: param.customer_id,
        off_session: true,
        enable_incomplete_payments: false,
        collection_method: param.collection_method,
      //  trial_from_plan:true,
        //coupon: coupon_id,
        //promotion_code: promotion_code_id,
        items: [{
          price: price_id
        }]

      };

      debug("Promocode Id", promotion_code_id);
     /*
      if (promotion_code_id != "") {
        input['promotion_code'] = promotion_code_id;
      }
      */
      /*
      if (param.product.toLowerCase()== "basic") {
        var oneYearFromNow = new Date();
        oneYearFromNow.setFullYear(oneYearFromNow.getFullYear() + 1);
        var trial_time = Math.round(oneYearFromNow.getTime()/1000)+10;
        input['trial_end'] = trial_time;
      }*/
      console.log("input",input);
     // return false;
      const res = await this.stripe.subscriptions.create(input);

      return res;

    } catch (err) {
      debug('err:', err);
      if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }


  }

  /**
   * Get subscriptions
   * @param customerId 
   * @returns 
   */
  public async getSubscription(subscription_id) {

    try {

      const res = await this.stripe.subscriptions.retrieve(subscription_id);
      // const res = null;
      console.log("subscription", res);
      const result = {};

      result['product'] = await this.getProductName(res.items.data[0].plan.product);
      result['interval'] = res.items.data[0].plan.interval;
      result['interval_count'] = res.items.data[0].plan.interval_count;
      result['current_plan_amount'] = res.items.data[0].plan.amount;
      result['currency'] = res.items.data[0].plan.currency;
      result['current_period_start'] = this.util.getDate(res.current_period_start);
      result['current_period_end'] = this.util.getDate(res.current_period_end);


      var invoice_id = "" + res.latest_invoice;
      //var invoice_id = 'in_1KmCepFSgVYpzNjn4hvbM2Hc';
      const resInvoice = await this.stripe.invoices.retrieve(invoice_id);
      console.log("resInvoice", resInvoice);
      var invoice = {
        amount_due: resInvoice.amount_due,
        amount_paid: resInvoice.amount_paid,
        amount_remaining: resInvoice.amount_remaining,
        period_start: this.util.getDate(resInvoice.period_start),
        period_end: this.util.getDate(resInvoice.period_end),
        status: resInvoice.status
      }
      result['invoice'] = invoice;

      return result;


      // return res;

    } catch (err) {
      debug('err:', err);
      if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }


  }

  /**
   * List coupons
   * @returns 
   */
  public async getCoupons() {


    try {

      const res = await this.stripe.coupons.list();
      // const res = null;
      return res;

    } catch (err) {
      debug('err:', err);
      if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }


  }

  /**
   * Get Invoices
   * @returns 
   */
  public async getInvoices(customer_id, req) {


    try {
      var query = {};

      if (customer_id != "" && customer_id != undefined && customer_id != "undefined") {
        query['customer'] = customer_id;
      }

      if (req.query.starting_after != "") {
        query['starting_after'] = req.query.starting_after;
      }
      if (req.query.ending_before != "") {
        query['ending_before'] = req.query.ending_before;
      }

      if (req.query.start_date != "" || req.query.end_date != "") {


        if (req.query.start_date != "" && req.query.start_date != undefined && req.query.start_date != "undefined") {
          query['period_start'] = {
            "gte": req.query.start_date,
          };
        }

        if (req.query.end_date != "" && req.query.end_date != undefined && req.query.end_date != "undefined") {
          query['period_end'] = {
            "lte": req.query.end_date
          };
        }


      } else {
        /*
         query = {
           customer: customer_id
         }*/
      }

      /*
      const count_query = query;
      console.log("countQuery", count_query);

      var cnt=0;
      const countRes = await this.stripe.invoices.list(
        count_query
      ).autoPagingEach(function(obj) {
        // Do something with customer
        cnt++;
      });
      console.log("count=",cnt);

     // var total_count = countRes.data.length;
     
      var total_count = cnt;
      */
      console.log("req.query.limit",req.query.limit);
      if (req.query.limit != "" && req.query.limit != undefined && req.query.limit != "undefined") {
        console.log("yes here");
        query['limit'] = req.query.limit;
      }else {
        query['limit'] = 500;
      }
 

      query['total'] ={
        "gt": 0
      };

    

      console.log("Query", query);
      
      const res = await this.stripe.invoices.list(
        query
      
      );
      

      

      const result = [];
      
      for (const invoice of res.data) {

        if(invoice.amount_paid>0) {
          let inv = {
            id: invoice.id,
            amount_paid: invoice.amount_paid,
            amount_remaining: invoice.amount_remaining,
            collection_method: invoice.collection_method,
            invoice_pdf: invoice.invoice_pdf,
            number: invoice.number,
            period_start: invoice.period_start,
            period_end: invoice.period_end,
            due_date: invoice.due_date,
            paid: invoice.paid,
            paid_at: invoice.status_transitions.paid_at,
            status: invoice.status,
            total: invoice.total
          }
          result.push(inv);
      }

      }
      
      var pagination=
           {
             'total_count' : result.length,
             'has_more':res.has_more
          };
          
          if(res.data[0] && res.data[0]!=null) {
            var cnt =  Object.keys(res.data).length;
            pagination['starting_after']= res.data[cnt-1].id;
            pagination['ending_before']= res.data[0].id;
            
          }
          var final_result = {
            'pagination': pagination,
            'data' : result
          };
          

     // return result;
      return final_result;

    } catch (err) {
      debug('err:', err);
      if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }


  }

  /**
   * Get Invoice Details
   * @returns 
   */
  public async getInvoiceDetails(invoice_id) {

    try {

      const res = await this.stripe.invoices.retrieve(
        invoice_id
      );
      // const res = null;
      return res;

    } catch (err) {
      debug('err:', err);
      if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }


  }

  /**
  * Get Payments
  * @returns 
  */
  public async getPayments(customer_id, req) {

    try {

      var query = {};

      if (customer_id != "" && customer_id != undefined && customer_id != "undefined") {
        query['customer'] = customer_id;
      }

      if (req.query.starting_after != "") {
        query['starting_after'] = req.query.starting_after;
      }
      if (req.query.ending_before != "") {
        query['ending_before'] = req.query.ending_before;
      }

      if (req.query.start_date != "" || req.query.end_date != "") {

        var cont = {};
        if (req.query.start_date != "" && req.query.start_date != undefined && req.query.start_date != "undefined") {
          cont['gte'] = req.query.start_date;

        }

        if (req.query.end_date != "" && req.query.end_date != undefined && req.query.end_date != "undefined") {
          //query['created'] = {
          //  "lte": req.query.end_date
          //};
          cont['lte'] = req.query.end_date;
        }

        if (cont) {
          query['created'] = cont;
        }

      } else {
        /*
         query = {
           customer: customer_id
         }*/
      }
      /*
      const count_query = query;
      console.log("countQuery", count_query);

      var cnt=0;
      const countRes = await this.stripe.paymentIntents.list(
        count_query
      ).autoPagingEach(function(obj) {
        // Do something with customer
        cnt++;
      });
      console.log("count=",cnt);

     // var total_count = countRes.data.length;
      var total_count = cnt;
      */

      if (req.query.limit != "" && req.query.limit != undefined && req.query.limit != "undefined") {
        query['limit'] = req.query.limit;
      }else {
        query['limit'] = 500;
      }
   
      console.log("Query", query);

      const res = await this.stripe.paymentIntents.list(
        query
      );
      // const res = null;

        var result = {};
        result['data'] = res.data;

       const resInvoices = await this.stripe.invoices.list(
          {
           customer: customer_id
         }
        );

        var invoices = {};
        if (typeof resInvoices.data !== 'undefined') {
          for (const invoice of resInvoices.data) {
            invoices[invoice.id] = invoice.number;
          }
      }

        for (var payment of result['data']) {
          var inv =  payment.charges.data[0].invoice;
          if (typeof invoices[inv] !== 'undefined') {
            payment.invoice_number = invoices[inv];
          }
        }
        
        var pagination = {
          'total_count': result['data'].length,
          'has_more':res.has_more
        }
        
          if(res.data[0] && res.data[0]!=null) {
            var cnt =  Object.keys(res.data).length;
            pagination['starting_after']= res.data[cnt-1].id;
            pagination['ending_before']= res.data[0].id;
            
          }
          result = {
            'pagination': pagination,
            'data' : result['data']
          };
        

      //  console.log(result);

      return result;

    } catch (err) {
      debug('err:', err);
      if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }

  }

  /**
  * Get Transactions
  * @returns 
  */
  public async getPaymentDetails(payment_id) {

    try {

      const res = await this.stripe.paymentIntents.retrieve(
        payment_id
      );
      // const res = null;
      return res;

    } catch (err) {
      debug('err:', err);
      if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }

  }
  /**
  * Get Transaction details
  * @returns 
  */
  public async getTransaction(transaction_id) {

    try {

      const res = await this.stripe.balanceTransactions.retrieve(
        transaction_id
      );
      // const res = null;
      return res;

    } catch (err) {
      debug('err:', err);
      if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }

  }
  /**
    * Creating subscriptions
    * @param setUpgradedowngrade 
    * @returns 
    */
  public async setUpgradeDowngrade(customer_id, param: setUpgradeDowngrade) {

    const price_id = param.price_id;

    try {

      // const res = await this.stripe.subscriptions.create(input);
      // const res = null;

      const subscription = await this.stripe.subscriptions.retrieve(param.subscription_id);
      this.stripe.subscriptions.update(param.subscription_id, {
        cancel_at_period_end: false,
        proration_behavior: 'create_prorations',
        items: [{
          id: subscription.items.data[0].id,
          price: price_id,
        }]
      });
      console.log("subscription", subscription);
      return subscription;

    } catch (err) {
      debug('err:', err);
      if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }


  }

  /**
   *Subscription Preview
   * @returns 
   */
  public async preview(req) {


    try {
      var query = {};

      const proration_date = Math.floor(Date.now() / 1000);

      const subscription = await this.stripe.subscriptions.retrieve(req.subscription_id);
      const items = [{
        id: subscription.items.data[0].id,
        price: req.price_id, // Switch to new price
      }];

      const invoices = await this.stripe.invoices.retrieveUpcoming({
        customer: req.customer_id,
        subscription: req.subscription_id,
        subscription_items: items,
        subscription_proration_date: proration_date,
      });


      console.log("preview", invoices);

      const result = {};

      result['total'] = invoices.total;
      result['subtotal'] = invoices.subtotal;
      result['lines'] = [];
      for (const invoice of invoices.lines.data) {

        let inv = {
          amount: invoice.amount,
          description: invoice.description,
          period: invoice.period,
          plan_interval: invoice.plan.interval,
          plan_interval_count: invoice.plan.interval_count,
          //  plan_type: this.getPlanType(invoice.plan.id),
          price: invoice.plan.amount,
          product: await this.getProductName(invoice.plan.product),
        }
        result['lines'].push(inv);

      }


      return result;

    } catch (err) {
      debug('err:', err);
      if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Customer err : ', err);
        throw err;
      }
    }


  }

  /**
    * Get Product name
    * @returns 
    */
  public async getProductName(product_id) {

    try {

      const res = await this.stripe.products.retrieve(
        product_id
      );
      // const res = null;
      console.log("product name", res);
      return res.name;

    } catch (err) {
      debug('err:', err);
      if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Product name err : ', err);
        throw err;
      }
    }

  }

  /**
    * Get Plan type
    * @returns 
    */
  public async getPlanType(price_id) {

    try {

      const res = await this.stripe.prices.retrieve(
        price_id
      );
      // const res = null;
      return res.recurring.interval;

    } catch (err) {
      debug('err:', err);
      if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
        throw new HttpException(err.response.data, err.response.status);
      } else {
        console.error('Get Product name err : ', err);
        throw err;
      }
    }

  }






}
