import { Injectable, HttpException, HttpStatus, HttpService } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { PaymentService } from './payment.service';
import { WebhookService } from './webhook.service';
import { ConfigService } from '../../config/config.service';
import { PaymentController } from './payment.controller';
import { ConfigModule } from '../../config/config.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UtilService } from '../utils/util.service';
import Debug from 'debug';
const debug = Debug('connector:UserController:Spec');

describe('PaymentController', () => {
  let paymentController: PaymentController;
  const paymentService1 = new PaymentService(new ConfigService(), new UtilService(new ConfigService()));

  beforeAll(async () => {

    const app: TestingModule = await Test.createTestingModule({
      imports: [TypeOrmModule.forRootAsync({
        imports: [ConfigModule],
        useFactory: (config: ConfigService) => config.getTypeORMConfig(),
        inject: [ConfigService],
      })],
      controllers: [PaymentController],
      providers: [PaymentService, WebhookService]
    }).overrideProvider(PaymentService).useValue(paymentService1).compile();

    // paymentController = app.get<PaymentController>(PaymentController);
    let paymentService = new PaymentService(new ConfigService(), new UtilService(new ConfigService()));
    let webhookService = new WebhookService(new ConfigService(), new UtilService(new ConfigService()), new HttpService());

    paymentController = new PaymentController(paymentService, webhookService);
  });


  it('should be defined', () => {
    expect(paymentController).toBeDefined();
  });

  it('create a stripe customer', async () => {
    debug('TEST RESULT:1');
    const dto = {
      name: 'Tamil-unit-test',
      email: 'tamilselvan@digitalprizm.net',
      phone: '919962679318',
      user_id: '4afae787-8d80-4025-95cb-27f233463e03',
    };
    const testRes = await paymentController.createCustomer(dto);
    debug(testRes);
    /*
    if (testRes.errMessage === 'User with email already exist') {
      expect(testRes.errMessage).toEqual('User with email already exist');
    } else if (testRes.errMessage === 'Email already exist in keycloak') {
      expect(testRes.errMessage).toEqual('Email already exist in keycloak');
    } else if (testRes.errMessage === 'User with mobile already exist') {
      expect(testRes.errMessage).toEqual('User with mobile already exist');
    } else if (testRes.errMessage === 'Mobile already exist in keycloak') {
      expect(testRes.errMessage).toEqual('Mobile already exist in keycloak');
    } else if (
      testRes.errMessage === 'Sorry! User age should be 21 year old to register'
    ) {
      expect(testRes.errMessage).toEqual(
        'Sorry! User age should be 21 year old to register',
      );
    } else {
      expect(testRes.data.full_name).toEqual('chaithra karanth');
      debug(testRes.data.id);
      const delres = await userService.delete(testRes.data.id);
      debug(`deleted item:${testRes.data.id}`);
      debug(delres);
    }
    */
  });


});
