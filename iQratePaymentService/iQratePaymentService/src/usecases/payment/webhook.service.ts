import { Injectable, HttpException, HttpStatus, HttpService } from '@nestjs/common';
import { ConfigService } from '../../config/config.service';
import { getConnection } from 'typeorm';
import Debug from 'debug';
import Stripe from 'stripe';
import {
    createCustomerDto, chargeCustomerDto, updateCustomerDto, createSubscription,
    createPaymentMethod, PaymentMethod, setPaymentMethod, CollectionMethod,
    updatePaymentMethod, detachPaymentMethod
} from './dto/payment.dto';
// import { UserEntity } from '../../entities/users';
// import { SubscriptionEntity } from '../../entities/subscriptions';
// import { SubscriptionItemEntity } from '../../entities/subscription_items';
import { prependOnceListener } from 'process';
import { UtilService } from '../utils/util.service';
import { isPhoneNumber } from 'class-validator';

const debug = Debug('connector:WebhookService');

@Injectable()
export class WebhookService {
    private stripe: Stripe;


    constructor(
        private config: ConfigService,
        private util: UtilService,
        private http: HttpService
    ) {
        this.stripe = new Stripe(config.envConfig.STRIPE_SECRET, {
            apiVersion: '2020-08-27',
        });

        console.log(config.envConfig.STRIPE_SECRET);
    }

    /**
    * Get Webhook stripe response
    * @returns 
    */
    public async webhook(signature: string, payload: Buffer) {

        console.log("entering stripe monitor");
        debug("signature", signature);
        debug("request output", payload);
        // const sig = payload.headers['stripe-signature'];
        let event;
        try {

            event = this.stripe.webhooks.constructEvent(payload, signature, this.config.envConfig.STRIPE_WEBHOOK_SECRET);
            // Handle the event
            debug("Event ", event);

            const res = await this.http
                .post(`${this.config.envConfig.USER_SERVICE_URL}/auth/webhook`, {
                    event: event,
                })
                .toPromise();

            return null;
            /*
            switch (event.type) {
                case 'charge.failed':
                    this.getFailureTransaction(event.data.object);
                    break;
                case 'price.updated':
                    const price = event.data.object;
                    // Then define and call a function to handle the event price.updated
                    break;
                case 'subscription_schedule.completed':
                    // const subscriptionSchedule = event.data.object;
                    // Then define and call a function to handle the event subscription_schedule.completed
                    break;
                case 'subscription_schedule.created':
                    const subscriptionSchedule = event.data.object;
                    // Then define and call a function to handle the event subscription_schedule.created
                    break;
                // ... handle other event types
                default:
                    console.log(`Unhandled event type ${event.type}`);
            }
                


            return null;
            */

        } catch (err) {
            debug('err:', err);
            if (err && err.response && err.response.status >= 400 && err.response.status < 500) {
                throw new HttpException(err.response.data, err.response.status);
            } else {
                console.error('Webhook err : ', err);
                throw err;
            }
        }

    }



}