# AWS Secrets Manager

## AWS Credentials

`aws-sdk` by **default** loads `[default]` credentials and config from `.aws/credentials` and `.aws/config`. For our
application, AWS credentials are loaded using **environment variables**. The environment variables required for `aws-sdk` to
work requires the following keys:

```
AWS_SECRETSMANAGER_ACCESSKEYID = <YOUR_ACCESS_KEY>
AWS_SECRETSMANAGER_SECRETACCESSKEY = <YOUR_SECRET_ACCESS_KEY>
AWS_SECRETSMANAGER_REGION = <AWS_REGION>
```


If environment variables are not set, the application will look for the files in `.aws` to configure AWS credentials.

## Secrets loaded

Currently, our application uses the `SecretsLoaderService.getSecrets()` to load the following **secrets**:


