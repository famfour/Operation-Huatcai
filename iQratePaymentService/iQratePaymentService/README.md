#Payment Service

## Introduction

This service deals with `Payment Service Subscription Module` specific logics

|      Tool      |      Type    |   Version    |
| :------------  | :----------  | :----------  |
| NodeJs         | Runtime      | v14.15.1     |
| NestJs         | Framework    | ^7.6.13      |
| TypeORM        | ORM          | ^0.2.31      |
| Jest           | Testing      | ^26.6.3      |

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

###### Swagger UI available at: http://localhost:3000/api

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## DEV ENV Deployment , Prod configuration maintained in the master branch

- Git commit to dev branch will trigger the deployment pipeline to dev environment.
- Git commit to uat branch will trigger the deployment pipeline to uat environment.
- Git commit to master branch will trigger the deployment pipeline to production environment.
- The pipeline will build the image, push it to the ECR repo and deploy the image to AWS ECS
- Github actions work flow added for dev branch 
- Commits or Pull request merge