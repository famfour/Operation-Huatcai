module.exports = ({ env }) => ({
    // Step 1: Configure the redis connection
    // @see https://github.com/derrickmehaffy/strapi-plugin-redis
    redis: {
        config: {
            connections: {
                default: {
                    connection: {
                        host: env('REDIS_HOST', '127.0.0.1'),
                        port: env('REDIS_PORT', 6379),
                        db: 0,
                    },
                    settings: {
                        debug: false,
                        cluster: false,
                    },
                },
            },
        },
    },
    // Step 2: Configure the redis cache plugin
    "strapi-plugin-rest-cache": {
        config: {
            provider: {
                name: "redis",
                options: {
                    max: 32767,
                    connection: "default",
                },
            },
            strategy: {
                contentTypes: [
                    // list of Content-Types UID to cache
                    //  "api::bank-form.bank-form",
                    "api::training.training",
                    "api::membershiptype.membershiptype",
                    "api::faq.faq",
                ],
            },
        },
    },
    upload: {
        config: {
          provider: 'aws-s3',
          providerOptions: {
            accessKeyId: env('AWS_ACCESS_KEY_ID'),
            secretAccessKey: env('AWS_ACCESS_SECRET'),
            region: env('AWS_REGION'),
            params: {
              Bucket: env('AWS_BUCKET_NAME'),
            },
          },
        },
      }

});