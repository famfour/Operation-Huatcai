module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', 'b852c15ec49ffeef306ff00b1c693290'),
  },
  url: env('ADMIN_ROOT_ROUTE', '/admin'),
});
