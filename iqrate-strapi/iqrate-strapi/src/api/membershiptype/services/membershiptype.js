'use strict';

/**
 * membershiptype service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::membershiptype.membershiptype');
