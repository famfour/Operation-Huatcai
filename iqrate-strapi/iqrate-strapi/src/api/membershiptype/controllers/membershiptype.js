'use strict';

/**
 *  membershiptype controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::membershiptype.membershiptype');
