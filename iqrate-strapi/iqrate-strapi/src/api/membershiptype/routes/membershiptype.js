'use strict';

/**
 * membershiptype router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::membershiptype.membershiptype');
