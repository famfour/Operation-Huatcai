'use strict';

/**
 *  resource controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

//module.exports = createCoreController('api::resource.resource');
const modelUID = 'api::resource.resource';
module.exports = createCoreController(modelUID, ({ strapi }) => ({
    async find(ctx) {

        const qs = require('qs');
        const cust_query = {
            populate: {
                resource_category: true,
            },
            publicationState: 'live'
        };

        const entries = await strapi.entityService.findMany(modelUID, cust_query);
        return entries;
    },

    async findOne(ctx) {

        const { id } = ctx.params;

        const qs = require('qs');
        const cust_query = {
            populate: {
                resource_category: true,
                document: true
            },
            publicationState: 'live'
        };

        const entries = await strapi.entityService.findOne(modelUID, id, cust_query);
        return entries;


    },
    async search(ctx) {
        if (!ctx.request.query) {
            return false;
        }

        var name = ctx.request.query.name;
        var link = ctx.request.query.link;
        var resource_category = ctx.request.query.resource_category;

        // console.log("keyword", keyword);
        console.log("name", name);
        console.log("link", link);
        console.log("resource_category", resource_category);

        var nameQry = {};
        var linkQry = {};
        var resourceCategoryQry = {};

        if (name != "" && name != undefined) {

            nameQry = {
                name: {
                    $containsi: name
                }
            }

        }
        if (link != "" && link != undefined) {
            linkQry = {
                link: {
                    $containsi: link
                }
            }
        }

        if (resource_category != "" && resource_category != undefined) {
            resourceCategoryQry = {
                resource_category: {
                    category_name: resource_category

                }
            }
        }


        //  console.log("keyword", keyword);
        console.log("nameQry", nameQry);
        console.log("linkQry", linkQry);
        /*
        const qs = require('qs');
        const query = qs.stringify({
            sort: ['display_order:asc'],
            filters: {
                $or: [
                    nameQry,
                    linkQry,
                    resourceCategoryQry
                ]
            },

            populate: '*',
            //  populate: ['membership_types', 'category'],
            //  fields: ['title'],
            pagination: {
                pageSize: 100,
                page: 1,
            },
            publicationState: 'live',
            locale: ['en'],
        }, {
            encodeValuesOnly: true, // prettify url
        });
        const request = require('request');
        console.log("protocal", ctx.request.protocol);
        const host = ctx.request.get('Host');
        console.log("host", host);
        //const http = require('http');
        const url = ctx.request.protocol + "://" + host + "/api/resources?" + query;
        console.log("url", url);
        //return await http.get(`${url}`);
        return await request(`${url}`);
        */
        const cust_query = {
            populate: {
                resource_category: true,
                document: true
            },
            sort: ['display_order:asc'],
            filters: {
                $or: [
                    nameQry,
                    linkQry,
                    resourceCategoryQry
                ]
            },
            publicationState: 'live'
        };

        const entries = await strapi.entityService.findMany(modelUID, cust_query);
        return entries;


    }
}));