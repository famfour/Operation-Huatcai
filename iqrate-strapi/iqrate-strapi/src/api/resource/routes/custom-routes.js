'use strict';

/**
 *  custom - router.
 */

module.exports = {

    routes: [{
        method: "GET",
        path: "/resources/search",
        handler: "resource.search",
        config: {
            policies: []
        }
    }]


}