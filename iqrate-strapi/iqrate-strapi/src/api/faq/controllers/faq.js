'use strict';

/**
 *  faq controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

//module.exports = createCoreController('api::faq.faq');
const modelUID = 'api::faq.faq';
module.exports = createCoreController(modelUID, ({ strapi }) => ({

    async find(ctx) {

        const cust_query = {
            populate: {
                media: true,
                faq_category: true,
            },
            publicationState: 'live'
        };

        const entries = await strapi.entityService.findMany(modelUID, cust_query);
        return entries;
    },

    async findOne(ctx) {

        const { id } = ctx.params;

        const qs = require('qs');
        const cust_query = {
            populate: {
                media: true,
                faq_category: true,
            },
            publicationState: 'live'
        };

        const entries = await strapi.entityService.findOne(modelUID, id, cust_query);
        return entries;


    },

    async search(ctx) {
        if (!ctx.request.query) {
            return false;
        }
        //const { keyword, title, description } = ctx.request.query;
        // var keyword = ctx.request.query.keyword;
        var title = ctx.request.query.title;
        var description = ctx.request.query.description;
        var faq_category = ctx.request.query.faq_category;
        // console.log("keyword", keyword);
        console.log("title", title);
        console.log("description", description);

        var titleQry = {};
        var descriptionQry = {};
        var faq_categoryQry = {};

        if (title != "" && title != undefined) {

            titleQry = {
                title: {
                    $containsi: title
                }
            }

        }
        if (description != "" && description != undefined) {
            descriptionQry = {
                description: {
                    $containsi: description
                }
            }
        }

        if (faq_category != "" && faq_category != undefined) {

            faq_categoryQry = {
                faq_category: {
                    name: {
                        $eq: faq_category
                    }
                }
            }

        }

        //  console.log("keyword", keyword);
        console.log("titleQry", titleQry);
        console.log("descriptionQry", descriptionQry);

        // console.log("query", fieldQry);
        /*
        const qs = require('qs');
        const query = qs.stringify({
            sort: ['DisplayOrder:asc'],
            
            filters: {
                $or: [
                    titleQry,
                    descriptionQry
                ]
            },

            populate: '*',
            //  fields: ['title'],
            pagination: {
                pageSize: 100,
                page: 1,
            },
            publicationState: 'live',
            locale: ['en'],
        }, {
            encodeValuesOnly: true, // prettify url
        });
        const request = require('request');
        console.log("protocal", ctx.request.protocol);
        const host = ctx.request.get('Host');
        console.log("host", host);
        //const http = require('http');
        const url = ctx.request.protocol + "://" + host + "/api/faqs?" + query;
        console.log("url", url);
        //return await http.get(`${url}`);
        return await request(`${url}`);
        */

        const cust_query = {
            populate: {
                media: true,
                pagination:true,
                faq_category:true
            },
            sort: ['display_order:asc'],
            filters: {
                $and: [
                    titleQry,
                    descriptionQry,
                    faq_categoryQry
                ]
            },
            publicationState: 'live'
        };

        const entries = await strapi.entityService.findMany(modelUID, cust_query);
        return entries;


    }
}));