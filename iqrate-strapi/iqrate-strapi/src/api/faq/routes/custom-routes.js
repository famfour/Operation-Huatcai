'use strict';

/**
 *  custom - router.
 */

module.exports = {

    routes: [{
        method: "GET",
        path: "/faqs/search",
        handler: "faq.search",
        config: {
            policies: []
        }
    }]


}