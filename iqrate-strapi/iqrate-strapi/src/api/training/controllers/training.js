'use strict';

/**
 *  training controller
 */

const { createCoreController } = require('@strapi/strapi').factories;
// const request = require('request');
require('dotenv').config()

//module.exports = createCoreController('api::training.training');
const modelUID = "api::training.training";
module.exports = createCoreController(modelUID, ({ strapi }) => ({

    async find(ctx) {
        console.log(ctx);
        const { id } = ctx.params;
        const { query } = ctx;

        //  const entity = await strapi.service('api::restaurant.restaurant').findOne(id, query);
        //  const sanitizedEntity = await this.sanitizeOutput(entity, ctx);

        // return this.transformResponse(sanitizedEntity);

        const qs = require('qs');
        const cust_query = {
            populate: {
                training_category: true,
                membership_types: true,
                Thumbnail:true
            },
            publicationState: 'live'
        };

        const entries = await strapi.entityService.findMany(modelUID, cust_query);

        const sanitizedEntity = await this.sanitizeOutput(entries, ctx);
        // return sanitizedEntity;
        //return this.transformResponse(sanitizedEntity);
        return entries;

        /*
        const request = require('request');
        console.log("protocal", ctx.request.protocol);
        const host = ctx.request.get('Host');
        console.log("host", host);
        //const http = require('http');
        const url = ctx.request.protocol + "://" + host + "/api/trainings?" + cust_query;
        console.log("url", url);
        //return await http.get(`${url}`);
        return await request(`${url}`);
        */

    },

    async findOne(ctx) {
        console.log(ctx);
        const { id } = ctx.params;
        const { query } = ctx;

        const qs = require('qs');
        const cust_query = {
            populate: {
                training_category: true,
                membership_types: true,
                Media: true,
                Thumbnail:true
            },
            publicationState: 'live'
        };

        const entries = await strapi.entityService.findOne(modelUID, id, cust_query);
        const sanitizedEntity = await this.sanitizeOutput(entries, ctx);
        return entries;



    },

    async search(ctx) {
        if (!ctx.request.query) {
            return false;
        }
        //const { keyword, title, description } = ctx.request.query;
        // var keyword = ctx.request.query.keyword;
        var title = ctx.request.query.title;
        var membership_types = ctx.request.query.membership_types;
        var training_category = ctx.request.query.training_category;
        console.log("training_category", training_category);
        var page = ctx.request.query.page;
        var page_limit = ctx.request.query.page_limit;
        var start = 0;
        if(page!==undefined) {
            start = (page * page_limit) -page_limit;
        }
        // console.log("keyword", keyword);
        console.log("title", title);
        console.log("membership_types", membership_types);
        console.log("page", page);
        console.log("page_limit", page_limit);

        var titleQry = {};
        var membership_typeQry = {};
        var training_categoryQry = {};

        if (title != "" && title != undefined) {

            titleQry = {
                title: {
                    $containsi: title
                }
            }

        }
        if (membership_types != "" && membership_types != undefined) {
            var result = [];
            console.log(membership_types.indexOf(","));
            if (membership_types.indexOf(",") == -1) {
                result.push(membership_types);
            } else {
                result = membership_types.split(",");
            }
            console.log("result", result);
            membership_typeQry = {
                membership_types: {
                    name: {
                        $in: result
                    }
                }
            }
        }

        if (training_category != "" && training_category != undefined) {

            training_categoryQry = {
                training_category: {
                    category: {
                        $eq: training_category
                    }
                }
            }

        }

        //  console.log("keyword", keyword);
        console.log("titleQry", titleQry);
        console.log(membership_typeQry);
        /*
        const qs = require('qs');
        const query = qs.stringify({
            sort: ['display_order:asc'],
            filters: {
                $or: [
                    titleQry,
                    membership_typeQry,
                    training_categoryQry
                ]
            },

            // populate: '*',
            populate: ['membership_types', 'category'],
            //  fields: ['title'],
            pagination: {
                pageSize: 100,
                page: 1,
            },
            publicationState: 'live',
            locale: ['en'],
        }, {
            encodeValuesOnly: true, // prettify url
        });
        const request = require('request');
        console.log("protocal", ctx.request.protocol);
        const host = ctx.request.get('Host');
        console.log("host", host);
        //const http = require('http');
        const url = ctx.request.protocol + "://" + host + "/api/trainings?" + query;
        console.log("url", url);
        //return await http.get(`${url}`);
        return await request(`${url}`);
        */


        /*
        const query = qs.stringify({
            sort: ['display_order:asc'],
            filters: {
                $or: [
                    titleQry,
                    membership_typeQry,
                    training_categoryQry
                ]
            },

            // populate: '*',
            populate: ['membership_types', 'category'],
            //  fields: ['title'],
            pagination: {
                pageSize: 100,
                page: 1,
            },
            publicationState: 'live',
            locale: ['en'],
        }, {
            encodeValuesOnly: true, // prettify url
        });
        const entries = await strapi.entityService.findMany(modelUID, {
            populate: '*',
        });
        */

        /*
        const qs = require('qs');
        const res = await strapi.db.query(modelUID).findMany({
            sort: ['display_order:asc'],
            populate: ['membership_types'],
            where: {
                $or: [
                    titleQry,
                    membership_typeQry

                ]
            }
        });

        return res;
        */


        const cust_query = {
            populate: {
                training_category: true,
                membership_types: true,
                Media: true,
                Thumbnail:true,
                pagination:true

            },
            sort: ['display_order:asc'],
            filters: {

                $and: [
                    membership_typeQry,
                    training_categoryQry,
                    titleQry,
                ],

            },
            start: start,
            limit: page_limit,
            withCount:true,
            publicationState: 'live'
        };

        const entries = await strapi.entityService.findMany(modelUID, cust_query);
      //  const sanitizedEntity = await this.sanitizeOutput(entries, ctx);
      //  return this.transformResponse(sanitizedEntity);
      //  console.log(entries);
        return entries;


    }
}));