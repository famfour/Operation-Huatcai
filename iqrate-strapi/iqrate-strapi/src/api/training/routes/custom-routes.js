'use strict';

/**
 * tutorial custom - router.
 */

module.exports = {

    routes: [{
        method: "GET",
        path: "/trainings/search",
        handler: "training.search",
        config: {
            policies: []
        }
    }]


}