'use strict';

/**
 *  resource-category controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

//module.exports = createCoreController('api::resource-category.resource-category');


const modelUID = 'api::resource-category.resource-category';
module.exports = createCoreController(modelUID, ({ strapi }) => ({

    async find(ctx) {

        const cust_query = {
            populate: "*",
            publicationState: 'live'
        };

        const entries = await strapi.entityService.findMany(modelUID, cust_query);
        return entries;
    },

    async findOne(ctx) {

        const { id } = ctx.params;

        const qs = require('qs');
        const cust_query = {
            populate: "*",
            publicationState: 'live'
        };

        const entries = await strapi.entityService.findOne(modelUID, id, cust_query);
        return entries;


    },

   
}));