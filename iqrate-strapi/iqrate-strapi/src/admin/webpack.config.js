'use strict';

/* eslint-disable no-unused-vars */
module.exports = (config, webpack) => {
    // Note: we provide webpack above so you should not `require` it
    // Perform customizations to webpack config
    // Important: return the modified config
    config.plugins.push(
        new webpack.DefinePlugin({
            // ENVS that you want to use in frontend
            CUSTOMER_BANK_API: JSON.stringify(process.env.CUSTOMER_BANK_API),


        })
    );
    return config;
};