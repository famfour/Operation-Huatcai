/// <reference types="cypress" />

describe("SignUp Page Test", () => {
  beforeEach(() => {
    cy.viewport(1280, 720);

    cy.visit("http://localhost:5000/signin");
  });

  it("Checkng for Content on the page", () => {
    cy.findByText("Easy & Simple Loan Process");
    cy.findByText("Sign up to IQrate");
    cy.findByText("Don't have an account?");
    cy.findByText("Sign up").click();
    //cy.url().should("include", "/blog");
  });

  it("Checking for Input field labels", () => {
    cy.findByText("email address");
    cy.findByText("password");
  });

  it("Checking for Input fields", () => {
    cy.get("[id=emailAddress]").type("kishore@digitalprizm.net");
    cy.get('[type="password"]').type("9845655343");
    cy.get("#scatterPlot").get("g").click({ force: true });
    cy.get('[type="checkbox"]').check();
    // cy.get('[type="checkbox"]').uncheck();
    cy.contains("Sign In").click();
    //cy.contains("Continue with Google").click();
  });
});
