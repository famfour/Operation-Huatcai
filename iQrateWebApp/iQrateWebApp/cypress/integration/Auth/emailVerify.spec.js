/// <reference types="cypress" />

describe("VerifyEmail Page Test", () => {
  beforeEach(() => {
    cy.viewport(1280, 720);
    //1. it wil open url
    cy.visit("http://localhost:5000/verify-email");
  });

  it("Checkng for Content on the page", () => {
    cy.findByText("Verify Your Email");
    cy.findByText("Don't have an account?");
    cy.findByText("Sign up").click();
  });

  it("Checking for Input fields", () => {
    //cy.contains("Sign up").click();
    cy.get("[id=firstNum]").type("1");
    cy.get("[id=secondNum]").type("2");
    cy.get("[id=thirdNum]").type("3");
    cy.get("[id=fourthNum]").type("4");
    //cy.contains("Resend Code").click();
    cy.contains("Proceed").click();
  });
});
