/// <reference types="cypress" />

describe("SignUp Page Test", () => {
  beforeEach(() => {
    cy.viewport(1280, 720);
    //1. it wil open url
    cy.visit("http://localhost:5000/signup");
  });

  it("Checkng for Content on the page", () => {
    cy.findByText("Easy & Simple Loan Process");
    cy.findByText("Sign up to IQrate");
    cy.findByText("Already have an account?");
    cy.findByText("Sign in").click();
    //cy.url().should("include", "/blog");
  });

  it("Checking for Input field labels", () => {
    cy.findByText("first name");
    cy.findByText("email address");
    cy.findByText("Mobile No");
    cy.findByText("Date of Birth");
    cy.findByText("password");
  });

  it("Checking for Input fields", () => {
    //cy.contains("Sign up").click();
    cy.get("[id=firstName]").type("Kishor Jadav B");
    cy.get("[id=emailAddress]").type("kishore@digitalprizm.net");
    // cy.get("[id=mobileNo]").type("9845575343");
    // cy.get('[id="country"]').trigger("mousemove");

    //cy.get('[id="ca"]').trigger("mousemove").click();
    //cy.get('[name="country"]').click();
    cy.get("[name=date]").click();
    cy.contains("24").click();
    cy.get('[type="password"]').type("9845655343");
    cy.get('[type="checkbox"]').check();
    // cy.get('[type="checkbox"]').uncheck();
    //cy.contains("Sign Up").click();
  });
});
