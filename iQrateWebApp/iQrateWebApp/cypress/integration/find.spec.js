/// <reference types="cypress" />

describe("home page test", () => {
  it("check button text", () => {
    cy.visit("http://localhost:5000/");
    cy.findByText("daisyUI Button");
  });
});
