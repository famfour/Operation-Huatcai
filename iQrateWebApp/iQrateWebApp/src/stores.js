/* eslint-disable import/no-extraneous-dependencies, import/prefer-default-export */
import { writable } from 'svelte/store';
import { getCookie } from './utils/cookie';

export const userToken = writable(getCookie('cookie') || '');
export const userDetail = writable(
    JSON.parse(localStorage.getItem('user_info')) || null,
);

export const pendingOtpEmail = writable(
    localStorage.getItem('pendingOtpEmail') || null,
);
export const pendingOtpPhone = writable(
    localStorage.getItem('pendingOtpPhone') || null,
);
export const forgotPasswordOtp = writable(
    localStorage.getItem('forgotPasswordOtp') || null,
);
export const resetPasswordEmail = writable(
    localStorage.getItem('resetPasswordEmail') || null,
);
export const allKv = writable(JSON.parse(localStorage.getItem('kv')) || null);
export const storePdpaStatus = writable(false)
export const PdpaStatus = writable(false)
export const lead_type = writable(0)
export const CobrokeStatus = writable(0)
export const generatePackage = writable([])
export const refinanceReport = writable([])

export const leadListCheckboxes = writable(
    localStorage.getItem('leadChecklist') || [],
);