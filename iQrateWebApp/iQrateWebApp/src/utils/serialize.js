export const serialize = (obj) => {
    var str = [];
    for (var p in obj)
        if (obj.hasOwnProperty(p)) {
            str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
        }
    return str.join('&');
};

export const serializeCrmFilterBank = (obj) => {
    var str = [];
    for (var p in obj)
        if (obj.hasOwnProperty(p)) {
            if (p === 'bank' || p === 'rate_type' || p === 'co_broke' || p === 'lead_status') {
                obj[p].forEach((v) => {
                    str.push(encodeURIComponent(p) + '=' + encodeURIComponent(v));
                });
            } else {
                str.push(encodeURIComponent(p));
            }
        }
    return str.join('&');
};