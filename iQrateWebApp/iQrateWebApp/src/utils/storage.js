import { userDetail } from '../stores';

export const setUserInfo = (data) => {
  return localStorage.setItem('user_info', JSON.stringify(data));
};

export const getUserInfo = () => {
  return JSON.parse(localStorage.getItem('user_info'));
};

export const updateUserInfo = (data) => {
  let cur = JSON.parse(localStorage.getItem('user_info'));
  Object.assign(cur, data);
  setUserInfo(cur);
  userDetail.set(cur);
};

export const setKvInfo = (data) => {
  return localStorage.setItem('kv', JSON.stringify(data));
};
