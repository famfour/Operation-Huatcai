import Toastify from 'toastify-js';
import 'toastify-js/src/toastify.css';

export const toastAlert = (type, name) => {
    let style = {
        background: '#E8F4FE',
        color: '#2094F3',
    };
    switch (type) {
        case 'success':
            style = {
                background: '#E5F4F3',
                color: '#009485',
            };
            break;
        case 'info':
            style = {
                background: '#E8F4FE',
                color: '#2094F3',
            };
            break;
        case 'warning':
            style = {
                background: '#FFF5E5',
                color: '#FF9900',
            };
            break;
        case 'error':
            style = {
                background: '#FFEEE9',
                color: '#FF5724',
            };
            break;
        default:
            style = {
                background: '#E8F4FE',
                color: '#2094F3',
            };
            break;
    }

    Toastify({
        text: name,
        duration: 4000,
        close: true,
        gravity: 'top', // `top` or `bottom`
        position: 'right', // `left`, `center` or `right`
        stopOnFocus: true, // Prevents dismissing of toast on hover
        style: style,
    }).showToast();
};