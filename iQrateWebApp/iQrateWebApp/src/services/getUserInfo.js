import { fetchApi } from '../services/api';
import { setUserInfo } from '../utils/storage';
import { toastAlert } from '../utils/toaster';
import { userDetail, userToken } from '../stores';
import { deleteCookie } from '../utils/cookie';

const logUserOut = (hideToast) => {
  userToken.set(null);
  if (!hideToast) {
    toastAlert('error', 'Logging out.');
  }
  deleteCookie('cookie');
  window.location = '/';
};

export const getUserDetail = async () => {
  // get userInfo
  const res = await fetchApi('GET', `user`);
  const json = await res.json();

  if (res.status >= 200 && res.status <= 300) {
    // if not p+, will log user out
    if (json.status === 0) {
      logUserOut(true);
      window.location = '/locked';
      toastAlert('error', 'Your account is locked. Please contact support.');
    } else if (json.membership_type?.toLowerCase() === 'premium+') {
      // set user data in localStorage
      setUserInfo(json);
      userDetail.set(json);
      if (json.is_mobile_verified !== 1) {
        if (window.location.pathname !== '/profile') {
          window.location = '/profile';
        }
        toastAlert(
          'warning',
          'Please verify your mobile number to have full access.',
        );
      } else if (json.is_email_verified !== 1) {
        if (window.location.pathname !== '/profile') {
          window.location = '/profile';
        }
        toastAlert('warning', 'Please verify your email to have full access.');
      }
    } else {
      logUserOut();
      window.location = '/restricted';
      // redirect to 401 page - await vamsi on UI
    }
  } else {
    // kick user
    console.log('Error', json.message ? json.message : 'backend err msg.');
    logUserOut();
  }
};
